package com.truecaller.flashsdk.assist;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.net.Uri;
import android.support.a.a;
import android.util.DisplayMetrics;
import c.g.b.k;
import java.io.File;

public final class l
  implements FlashBitmapConverter
{
  private final DisplayMetrics a;
  private final Context b;
  private final j c;
  
  public l(Context paramContext, j paramj)
  {
    b = paramContext;
    c = paramj;
    paramContext = b.getResources();
    k.a(paramContext, "context.resources");
    paramContext = paramContext.getDisplayMetrics();
    a = paramContext;
  }
  
  private static int a(String paramString)
  {
    a locala = new android/support/a/a;
    locala.<init>(paramString);
    paramString = "Orientation";
    int i = locala.a(paramString);
    int j = 3;
    if (i != j)
    {
      j = 6;
      if (i != j)
      {
        j = 8;
        if (i != j) {
          return 0;
        }
        return 270;
      }
      return 90;
    }
    return 180;
  }
  
  /* Error */
  private final Uri a(Bitmap paramBitmap, File paramFile, int paramInt, BitmapFactory.Options paramOptions)
  {
    // Byte code:
    //   0: new 65	java/io/FileOutputStream
    //   3: astore 5
    //   5: aload 5
    //   7: aload_2
    //   8: invokespecial 68	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   11: aload 4
    //   13: getfield 74	android/graphics/BitmapFactory$Options:outMimeType	Ljava/lang/String;
    //   16: astore 6
    //   18: aload 6
    //   20: ifnull +48 -> 68
    //   23: aload 4
    //   25: getfield 74	android/graphics/BitmapFactory$Options:outMimeType	Ljava/lang/String;
    //   28: astore 4
    //   30: ldc 75
    //   32: astore 6
    //   34: aload 4
    //   36: aload 6
    //   38: invokestatic 39	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   41: ldc 77
    //   43: astore 6
    //   45: aload 4
    //   47: aload 6
    //   49: iconst_0
    //   50: invokestatic 82	c/n/m:c	(Ljava/lang/String;Ljava/lang/String;Z)Z
    //   53: istore 7
    //   55: iload 7
    //   57: ifeq +11 -> 68
    //   60: getstatic 88	android/graphics/Bitmap$CompressFormat:PNG	Landroid/graphics/Bitmap$CompressFormat;
    //   63: astore 4
    //   65: goto +8 -> 73
    //   68: getstatic 91	android/graphics/Bitmap$CompressFormat:JPEG	Landroid/graphics/Bitmap$CompressFormat;
    //   71: astore 4
    //   73: aload 5
    //   75: astore 6
    //   77: aload 5
    //   79: checkcast 93	java/io/OutputStream
    //   82: astore 6
    //   84: aload_1
    //   85: aload 4
    //   87: iload_3
    //   88: aload 6
    //   90: invokevirtual 99	android/graphics/Bitmap:compress	(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    //   93: pop
    //   94: aload 5
    //   96: invokevirtual 102	java/io/FileOutputStream:flush	()V
    //   99: aload 5
    //   101: invokevirtual 105	java/io/FileOutputStream:close	()V
    //   104: aload_0
    //   105: getfield 29	com/truecaller/flashsdk/assist/l:c	Lcom/truecaller/flashsdk/assist/j;
    //   108: astore 8
    //   110: aload 8
    //   112: aload_2
    //   113: invokeinterface 110 2 0
    //   118: astore_2
    //   119: goto +18 -> 137
    //   122: astore_2
    //   123: aload_1
    //   124: invokevirtual 113	android/graphics/Bitmap:recycle	()V
    //   127: aload 5
    //   129: invokevirtual 105	java/io/FileOutputStream:close	()V
    //   132: aload_2
    //   133: athrow
    //   134: pop
    //   135: aconst_null
    //   136: astore_2
    //   137: aload_1
    //   138: invokevirtual 113	android/graphics/Bitmap:recycle	()V
    //   141: aload 5
    //   143: invokevirtual 105	java/io/FileOutputStream:close	()V
    //   146: aload_2
    //   147: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	148	0	this	l
    //   0	148	1	paramBitmap	Bitmap
    //   0	148	2	paramFile	File
    //   0	148	3	paramInt	int
    //   0	148	4	paramOptions	BitmapFactory.Options
    //   3	139	5	localFileOutputStream	java.io.FileOutputStream
    //   16	73	6	localObject	Object
    //   53	3	7	bool	boolean
    //   108	3	8	localj	j
    //   134	1	9	localIOException	java.io.IOException
    // Exception table:
    //   from	to	target	type
    //   11	16	122	finally
    //   23	28	122	finally
    //   36	41	122	finally
    //   49	53	122	finally
    //   60	63	122	finally
    //   68	71	122	finally
    //   77	82	122	finally
    //   88	94	122	finally
    //   94	99	122	finally
    //   99	104	122	finally
    //   104	108	122	finally
    //   112	118	122	finally
    //   11	16	134	java/io/IOException
    //   23	28	134	java/io/IOException
    //   36	41	134	java/io/IOException
    //   49	53	134	java/io/IOException
    //   60	63	134	java/io/IOException
    //   68	71	134	java/io/IOException
    //   77	82	134	java/io/IOException
    //   88	94	134	java/io/IOException
    //   94	99	134	java/io/IOException
    //   99	104	134	java/io/IOException
    //   104	108	134	java/io/IOException
    //   112	118	134	java/io/IOException
  }
  
  /* Error */
  private final File b(Uri paramUri)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 27	com/truecaller/flashsdk/assist/l:b	Landroid/content/Context;
    //   4: invokevirtual 123	android/content/Context:getCacheDir	()Ljava/io/File;
    //   7: astore_2
    //   8: ldc 117
    //   10: ldc 119
    //   12: aload_2
    //   13: invokestatic 128	c/f/e:a	(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;
    //   16: astore_3
    //   17: aload_1
    //   18: invokestatic 131	com/truecaller/flashsdk/assist/l:c	(Landroid/net/Uri;)Lcom/truecaller/flashsdk/assist/FlashBitmapConverter$Scheme;
    //   21: astore 4
    //   23: getstatic 136	com/truecaller/flashsdk/assist/m:a	[I
    //   26: astore_2
    //   27: aload 4
    //   29: invokevirtual 142	com/truecaller/flashsdk/assist/FlashBitmapConverter$Scheme:ordinal	()I
    //   32: istore 5
    //   34: aload_2
    //   35: iload 5
    //   37: iaload
    //   38: istore 5
    //   40: iload 5
    //   42: tableswitch	default:+22->64, 1:+58->100, 2:+32->74
    //   64: new 144	c/l
    //   67: astore_1
    //   68: aload_1
    //   69: invokespecial 145	c/l:<init>	()V
    //   72: aload_1
    //   73: athrow
    //   74: new 147	java/io/FileInputStream
    //   77: astore 4
    //   79: aload_1
    //   80: invokevirtual 153	android/net/Uri:getPath	()Ljava/lang/String;
    //   83: astore_1
    //   84: aload 4
    //   86: aload_1
    //   87: invokespecial 154	java/io/FileInputStream:<init>	(Ljava/lang/String;)V
    //   90: aload 4
    //   92: checkcast 156	java/io/InputStream
    //   95: astore 4
    //   97: goto +16 -> 113
    //   100: aload_0
    //   101: getfield 27	com/truecaller/flashsdk/assist/l:b	Landroid/content/Context;
    //   104: invokevirtual 160	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   107: aload_1
    //   108: invokevirtual 166	android/content/ContentResolver:openInputStream	(Landroid/net/Uri;)Ljava/io/InputStream;
    //   111: astore 4
    //   113: new 65	java/io/FileOutputStream
    //   116: astore_1
    //   117: aload_1
    //   118: aload_3
    //   119: invokespecial 68	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   122: ldc -88
    //   124: astore_2
    //   125: aload 4
    //   127: aload_2
    //   128: invokestatic 39	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   131: aload_1
    //   132: astore_2
    //   133: aload_1
    //   134: checkcast 93	java/io/OutputStream
    //   137: astore_2
    //   138: aload 4
    //   140: aload_2
    //   141: invokestatic 173	com/truecaller/utils/extensions/m:a	(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    //   144: pop2
    //   145: aload_1
    //   146: invokevirtual 102	java/io/FileOutputStream:flush	()V
    //   149: aload_1
    //   150: invokevirtual 105	java/io/FileOutputStream:close	()V
    //   153: aload 4
    //   155: invokevirtual 174	java/io/InputStream:close	()V
    //   158: aload_3
    //   159: areturn
    //   160: astore_3
    //   161: aload_1
    //   162: invokevirtual 105	java/io/FileOutputStream:close	()V
    //   165: aload 4
    //   167: invokevirtual 174	java/io/InputStream:close	()V
    //   170: aload_3
    //   171: athrow
    //   172: pop
    //   173: aload_1
    //   174: invokevirtual 105	java/io/FileOutputStream:close	()V
    //   177: aload 4
    //   179: invokevirtual 174	java/io/InputStream:close	()V
    //   182: aconst_null
    //   183: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	184	0	this	l
    //   0	184	1	paramUri	Uri
    //   7	134	2	localObject1	Object
    //   16	143	3	localFile	File
    //   160	11	3	localObject2	Object
    //   21	157	4	localObject3	Object
    //   32	9	5	i	int
    //   172	1	7	localIOException	java.io.IOException
    // Exception table:
    //   from	to	target	type
    //   127	131	160	finally
    //   133	137	160	finally
    //   140	145	160	finally
    //   145	149	160	finally
    //   127	131	172	java/io/IOException
    //   133	137	172	java/io/IOException
    //   140	145	172	java/io/IOException
    //   145	149	172	java/io/IOException
  }
  
  private static FlashBitmapConverter.Scheme c(Uri paramUri)
  {
    Object localObject1 = paramUri.getScheme();
    Object localObject2 = FlashBitmapConverter.Scheme.FILE.getValue();
    boolean bool1 = k.a(localObject1, localObject2);
    if (bool1) {
      return FlashBitmapConverter.Scheme.FILE;
    }
    localObject2 = FlashBitmapConverter.Scheme.CONTENT.getValue();
    boolean bool2 = k.a(localObject1, localObject2);
    if (bool2) {
      return FlashBitmapConverter.Scheme.CONTENT;
    }
    localObject1 = new java/lang/IllegalArgumentException;
    localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("Uri scheme: ");
    paramUri = paramUri.getScheme();
    ((StringBuilder)localObject2).append(paramUri);
    ((StringBuilder)localObject2).append(" is not supported");
    paramUri = ((StringBuilder)localObject2).toString();
    ((IllegalArgumentException)localObject1).<init>(paramUri);
    throw ((Throwable)localObject1);
  }
  
  /* Error */
  private final BitmapFactory.Options d(Uri paramUri)
  {
    // Byte code:
    //   0: new 70	android/graphics/BitmapFactory$Options
    //   3: astore_2
    //   4: aload_2
    //   5: invokespecial 210	android/graphics/BitmapFactory$Options:<init>	()V
    //   8: aload_2
    //   9: iconst_1
    //   10: putfield 215	android/graphics/BitmapFactory$Options:inJustDecodeBounds	Z
    //   13: aload_1
    //   14: invokestatic 131	com/truecaller/flashsdk/assist/l:c	(Landroid/net/Uri;)Lcom/truecaller/flashsdk/assist/FlashBitmapConverter$Scheme;
    //   17: astore_3
    //   18: getstatic 217	com/truecaller/flashsdk/assist/m:b	[I
    //   21: astore 4
    //   23: aload_3
    //   24: invokevirtual 142	com/truecaller/flashsdk/assist/FlashBitmapConverter$Scheme:ordinal	()I
    //   27: istore 5
    //   29: aload 4
    //   31: iload 5
    //   33: iaload
    //   34: istore 5
    //   36: iload 5
    //   38: tableswitch	default:+22->60, 1:+86->124, 2:+25->63
    //   60: goto +75 -> 135
    //   63: aload_0
    //   64: getfield 27	com/truecaller/flashsdk/assist/l:b	Landroid/content/Context;
    //   67: invokevirtual 160	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   70: aload_1
    //   71: invokevirtual 166	android/content/ContentResolver:openInputStream	(Landroid/net/Uri;)Ljava/io/InputStream;
    //   74: checkcast 219	java/io/Closeable
    //   77: astore_1
    //   78: iconst_0
    //   79: istore 5
    //   81: aconst_null
    //   82: astore_3
    //   83: aload_1
    //   84: astore 4
    //   86: aload_1
    //   87: checkcast 156	java/io/InputStream
    //   90: astore 4
    //   92: aload 4
    //   94: aconst_null
    //   95: aload_2
    //   96: invokestatic 225	android/graphics/BitmapFactory:decodeStream	(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    //   99: pop
    //   100: aload_1
    //   101: aconst_null
    //   102: invokestatic 230	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   105: goto +30 -> 135
    //   108: astore_2
    //   109: goto +8 -> 117
    //   112: astore_2
    //   113: aload_2
    //   114: astore_3
    //   115: aload_2
    //   116: athrow
    //   117: aload_1
    //   118: aload_3
    //   119: invokestatic 230	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   122: aload_2
    //   123: athrow
    //   124: aload_1
    //   125: invokevirtual 153	android/net/Uri:getPath	()Ljava/lang/String;
    //   128: astore_1
    //   129: aload_1
    //   130: aload_2
    //   131: invokestatic 234	android/graphics/BitmapFactory:decodeFile	(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    //   134: pop
    //   135: aload_2
    //   136: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	137	0	this	l
    //   0	137	1	paramUri	Uri
    //   3	93	2	localOptions1	BitmapFactory.Options
    //   108	1	2	localObject1	Object
    //   112	24	2	localOptions2	BitmapFactory.Options
    //   17	102	3	localObject2	Object
    //   21	72	4	localObject3	Object
    //   27	53	5	i	int
    // Exception table:
    //   from	to	target	type
    //   115	117	108	finally
    //   86	90	112	finally
    //   95	100	112	finally
  }
  
  public final Uri a(Uri paramUri)
  {
    l locall = this;
    Object localObject1 = paramUri;
    k.b(paramUri, "fileUri");
    try
    {
      BitmapFactory.Options localOptions = d(paramUri);
      localObject1 = b(paramUri);
      if (localObject1 == null) {
        return null;
      }
      Object localObject2 = c;
      Object localObject3 = ".jpg";
      localObject2 = ((j)localObject2).a((String)localObject3);
      int i = outWidth;
      int j = 1280;
      float f1 = 1.794E-42F;
      int k = 0;
      Bitmap localBitmap = null;
      int m = 1;
      if (i <= j)
      {
        i = outHeight;
        if (i <= j)
        {
          i = 0;
          f2 = 0.0F;
          localObject3 = null;
          break label118;
        }
      }
      i = 1;
      float f2 = Float.MIN_VALUE;
      label118:
      int n;
      if (i == 0)
      {
        localObject1 = ((File)localObject1).getPath();
        k.a(localObject1, "file.path");
        inJustDecodeBounds = false;
        inSampleSize = m;
        localObject3 = BitmapFactory.decodeFile((String)localObject1, localOptions);
        j = outWidth;
        k = outHeight;
        localObject3 = Bitmap.createScaledBitmap((Bitmap)localObject3, j, k, m);
        if (localObject3 != null)
        {
          n = a((String)localObject1);
          localObject1 = com.truecaller.utils.extensions.b.a((Bitmap)localObject3, n);
          if (localObject1 != null) {
            return locall.a((Bitmap)localObject1, (File)localObject2, 100, localOptions);
          }
        }
        return null;
      }
      localObject1 = ((File)localObject1).getPath();
      localObject3 = "file.path";
      k.a(localObject1, (String)localObject3);
      inJustDecodeBounds = m;
      BitmapFactory.decodeFile((String)localObject1, localOptions);
      i = outWidth;
      j = outHeight;
      f2 = i;
      float f3 = 1280.0F;
      float f4 = f2 / f3;
      f1 = j;
      f3 = f1 / f3;
      f3 = Math.max(f4, f3);
      f2 /= f3;
      i = (int)f2;
      f1 /= f3;
      j = (int)f1;
      inJustDecodeBounds = false;
      k = outHeight;
      int i1 = outWidth;
      Runtime localRuntime = Runtime.getRuntime();
      long l1 = localRuntime.totalMemory();
      long l2 = localRuntime.freeMemory();
      l1 -= l2;
      l2 = 1048576L;
      l1 /= l2;
      long l3 = localRuntime.maxMemory() / l2;
      l1 = l3 - l1;
      double d1 = Math.ceil(-c.h.b.a(Math.sqrt(l1))) + 1.0D;
      double d2 = 2.0D;
      d1 = Math.pow(d2, d1);
      int i2 = (int)d1;
      i2 = Math.max(m, i2);
      if ((k > j) || (i1 > i))
      {
        k /= 2;
        i1 /= 2;
        for (;;)
        {
          int i3 = k / i2;
          if (i3 <= j) {
            break;
          }
          i3 = i1 / i2;
          if (i3 <= i) {
            break;
          }
          i2 *= 2;
        }
      }
      inSampleSize = i2;
      localBitmap = BitmapFactory.decodeFile((String)localObject1, localOptions);
      String str1 = "BitmapFactory.decodeFile(path, options)";
      k.a(localBitmap, str1);
      i1 = localBitmap.getWidth();
      if (i1 == i)
      {
        i1 = localBitmap.getHeight();
        if (i1 == j)
        {
          localObject3 = localBitmap;
          break label606;
        }
      }
      localObject3 = Bitmap.createScaledBitmap(localBitmap, i, j, m);
      String str2 = "Bitmap.createScaledBitma…idth, targetHeight, true)";
      k.a(localObject3, str2);
      localBitmap.recycle();
      label606:
      if (localObject3 != null)
      {
        n = a((String)localObject1);
        localObject1 = com.truecaller.utils.extensions.b.a((Bitmap)localObject3, n);
        if (localObject1 != null) {
          return locall.a((Bitmap)localObject1, (File)localObject2, 80, localOptions);
        }
      }
      return null;
    }
    catch (Exception localException) {}
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.assist.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */