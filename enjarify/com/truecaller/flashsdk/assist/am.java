package com.truecaller.flashsdk.assist;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.v4.content.b;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import c.g.b.k;
import c.g.b.z;
import com.d.b.ab;
import com.d.b.ai;
import com.d.b.w;
import com.truecaller.common.h.aq.d;
import com.truecaller.flashsdk.R.string;
import com.truecaller.flashsdk.models.Payload;
import com.truecaller.log.d;
import java.util.Arrays;

public final class am
  implements al
{
  private final Context a;
  private final w b;
  
  public am(Context paramContext, w paramw)
  {
    a = paramContext;
    b = paramw;
  }
  
  private final int c(int paramInt)
  {
    float f = paramInt;
    Object localObject = a.getResources();
    k.a(localObject, "context.resources");
    localObject = ((Resources)localObject).getDisplayMetrics();
    return (int)TypedValue.applyDimension(1, f, (DisplayMetrics)localObject);
  }
  
  public final Bitmap a(String paramString, boolean paramBoolean)
  {
    Object localObject = "imgUrl";
    k.b(paramString, (String)localObject);
    try
    {
      localObject = b;
      paramString = ((w)localObject).a(paramString);
      paramString = paramString.b();
      if (paramBoolean)
      {
        paramBoolean = true;
        paramBoolean = c(paramBoolean);
        localObject = aq.d.b();
        localObject = (ai)localObject;
        localObject = paramString.a((ai)localObject);
        ((ab)localObject).b(paramBoolean, paramBoolean);
      }
      else
      {
        paramBoolean = true;
        paramBoolean = c(paramBoolean);
        paramString.b(paramBoolean, paramBoolean);
      }
      paramString = paramString.d();
    }
    catch (Exception localException)
    {
      paramString = new java/lang/RuntimeException;
      String str = "could not get contact image for flash";
      paramString.<init>(str);
      d.a((Throwable)paramString);
      paramString = null;
    }
    return paramString;
  }
  
  public final Drawable a(int paramInt)
  {
    int i = Build.VERSION.SDK_INT;
    int j = 21;
    if (i >= j)
    {
      localDrawable = a.getDrawable(paramInt);
      k.a(localDrawable, "context.getDrawable(drawableRes)");
      return localDrawable;
    }
    Drawable localDrawable = a.getResources().getDrawable(paramInt);
    k.a(localDrawable, "context.resources.getDrawable(drawableRes)");
    return localDrawable;
  }
  
  public final SpannableString a(String paramString, int paramInt1, int paramInt2, int paramInt3, Context paramContext)
  {
    k.b(paramString, "string");
    Object localObject1 = "activityContext";
    k.b(paramContext, (String)localObject1);
    Drawable localDrawable = b.a(paramContext, paramInt3);
    if (localDrawable != null)
    {
      localDrawable = localDrawable.mutate();
    }
    else
    {
      paramInt3 = 0;
      localDrawable = null;
    }
    paramContext = null;
    if (localDrawable != null) {
      localDrawable.setBounds(0, 0, paramInt1, paramInt1);
    }
    if (localDrawable != null)
    {
      localObject2 = PorterDuff.Mode.SRC_IN;
      localDrawable.setColorFilter(paramInt2, (PorterDuff.Mode)localObject2);
    }
    Object localObject2 = new android/text/style/ImageSpan;
    ((ImageSpan)localObject2).<init>(localDrawable, 0);
    Object localObject3 = z.a;
    paramInt3 = 1;
    localObject1 = new Object[paramInt3];
    localObject1[0] = paramString;
    paramString = Arrays.copyOf((Object[])localObject1, paramInt3);
    paramString = String.format("   %s", paramString);
    k.a(paramString, "java.lang.String.format(format, *args)");
    paramString = (CharSequence)paramString;
    localObject3 = new android/text/SpannableString;
    ((SpannableString)localObject3).<init>(paramString);
    ((SpannableString)localObject3).setSpan(localObject2, 0, paramInt3, 33);
    return (SpannableString)localObject3;
  }
  
  public final String a(int paramInt, Object... paramVarArgs)
  {
    k.b(paramVarArgs, "formatArgs");
    Context localContext = a;
    int i = paramVarArgs.length;
    paramVarArgs = Arrays.copyOf(paramVarArgs, i);
    return localContext.getString(paramInt, paramVarArgs);
  }
  
  public final String a(Payload paramPayload)
  {
    k.b(paramPayload, "payload");
    String str1 = paramPayload.a();
    if (str1 != null)
    {
      i = str1.hashCode();
      String str2;
      switch (i)
      {
      default: 
        break;
      case 3035641: 
        str2 = "busy";
        boolean bool1 = str1.equals(str2);
        if (bool1)
        {
          paramPayload = a;
          int j = R.string.is_busy;
          paramPayload = paramPayload.getString(j);
          k.a(paramPayload, "context.getString(R.string.is_busy)");
          return paramPayload;
        }
        break;
      case 1772609: 
        str2 = "📞";
        boolean bool2 = str1.equals(str2);
        if (bool2)
        {
          paramPayload = a;
          int k = R.string.calling_you_back;
          paramPayload = paramPayload.getString(k);
          k.a(paramPayload, "context.getString(R.string.calling_you_back)");
          return paramPayload;
        }
        break;
      case 3548: 
        str2 = "ok";
        boolean bool3 = str1.equals(str2);
        if (bool3)
        {
          paramPayload = a;
          int m = R.string.sfc_ok;
          paramPayload = paramPayload.getString(m);
          k.a(paramPayload, "context.getString(R.string.sfc_ok)");
          return paramPayload;
        }
        break;
      case -934710369: 
        str2 = "reject";
        boolean bool4 = str1.equals(str2);
        if (bool4)
        {
          paramPayload = a;
          int n = R.string.reject;
          paramPayload = paramPayload.getString(n);
          k.a(paramPayload, "context.getString(R.string.reject)");
          return paramPayload;
        }
        break;
      case -1073880421: 
        str2 = "missed";
        boolean bool5 = str1.equals(str2);
        if (bool5)
        {
          paramPayload = a;
          int i1 = R.string.missed_your_flash;
          paramPayload = paramPayload.getString(i1);
          k.a(paramPayload, "context.getString(R.string.missed_your_flash)");
          return paramPayload;
        }
        break;
      case -1423461112: 
        str2 = "accept";
        boolean bool6 = str1.equals(str2);
        if (bool6)
        {
          paramPayload = a;
          i2 = R.string.accept;
          paramPayload = paramPayload.getString(i2);
          k.a(paramPayload, "context.getString(R.string.accept)");
          return paramPayload;
        }
        break;
      }
    }
    paramPayload = paramPayload.b();
    k.a(paramPayload, "payload.message");
    paramPayload = (CharSequence)paramPayload;
    int i2 = paramPayload.length();
    int i = 1;
    int i3 = i2 - i;
    i2 = 0;
    str1 = null;
    int i4 = 0;
    while (i2 <= i3)
    {
      if (i4 == 0) {
        i5 = i2;
      } else {
        i5 = i3;
      }
      int i5 = paramPayload.charAt(i5);
      int i6 = 32;
      if (i5 <= i6) {
        i5 = 1;
      } else {
        i5 = 0;
      }
      if (i4 == 0)
      {
        if (i5 == 0) {
          i4 = 1;
        } else {
          i2 += 1;
        }
      }
      else
      {
        if (i5 == 0) {
          break;
        }
        i3 += -1;
      }
    }
    i3 += i;
    return paramPayload.subSequence(i2, i3).toString();
  }
  
  public final int b(int paramInt)
  {
    return b.c(a, paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.assist.am
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */