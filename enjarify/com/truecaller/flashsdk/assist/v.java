package com.truecaller.flashsdk.assist;

import c.g.b.k;
import com.google.gson.f;

public final class v
  implements u
{
  private final f a;
  
  public v(f paramf)
  {
    a = paramf;
  }
  
  public final Object a(String paramString, Class paramClass)
  {
    k.b(paramString, "json");
    k.b(paramClass, "classOfT");
    return a.a(paramString, paramClass);
  }
  
  public final String a(Object paramObject)
  {
    k.b(paramObject, "src");
    paramObject = a.b(paramObject);
    k.a(paramObject, "gson.toJson(src)");
    return (String)paramObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.assist.v
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */