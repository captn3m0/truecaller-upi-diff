package com.truecaller.flashsdk.assist;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.text.SpannableString;
import com.truecaller.flashsdk.models.Payload;

public abstract interface al
{
  public abstract Bitmap a(String paramString, boolean paramBoolean);
  
  public abstract Drawable a(int paramInt);
  
  public abstract SpannableString a(String paramString, int paramInt1, int paramInt2, int paramInt3, Context paramContext);
  
  public abstract String a(int paramInt, Object... paramVarArgs);
  
  public abstract String a(Payload paramPayload);
  
  public abstract int b(int paramInt);
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.assist.al
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */