package com.truecaller.flashsdk.assist;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.text.TextUtils;
import c.a.ae;
import c.a.m;
import c.g.b.k;
import c.k.i;
import com.truecaller.flashsdk.R.string;
import com.truecaller.log.d;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public final class FetchAddressIntentService
  extends IntentService
{
  public static final FetchAddressIntentService.a a;
  private ResultReceiver b;
  
  static
  {
    FetchAddressIntentService.a locala = new com/truecaller/flashsdk/assist/FetchAddressIntentService$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  public FetchAddressIntentService()
  {
    super(str);
  }
  
  private final void a(int paramInt, String paramString, Address paramAddress)
  {
    ResultReceiver localResultReceiver = b;
    if (localResultReceiver == null) {
      return;
    }
    if (paramAddress == null) {
      return;
    }
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putString("com.truecaller.flashsdk.assist.RESULT_DATA_KEY", paramString);
    String str = paramAddress.getSubLocality();
    localBundle.putString("com.truecaller.flashsdk.assist.LOCATION_DATA_AREA", str);
    str = paramAddress.getLocality();
    localBundle.putString("com.truecaller.flashsdk.assist.LOCATION_DATA_CITY", str);
    paramAddress = paramAddress.getAddressLine(0);
    localBundle.putString("com.truecaller.flashsdk.assist.LOCATION_DATA_STREET", paramAddress);
    localResultReceiver.send(paramInt, localBundle);
  }
  
  protected final void onHandleIntent(Intent paramIntent)
  {
    Object localObject1 = "";
    if (paramIntent != null)
    {
      Object localObject2 = (ResultReceiver)paramIntent.getParcelableExtra("com.truecaller.flashsdk.assist.RECEIVER");
      if (localObject2 != null)
      {
        b = ((ResultReceiver)localObject2);
        localObject2 = "com.truecaller.flashsdk.assist.LOCATION_DATA_EXTRA";
        paramIntent = (Location)paramIntent.getParcelableExtra((String)localObject2);
        int i = 1;
        int j = 0;
        Object localObject3 = null;
        if (paramIntent == null)
        {
          m = R.string.no_location_data_provided;
          paramIntent = getString(m);
          k.a(paramIntent, "getString(R.string.no_location_data_provided)");
          a(i, paramIntent, null);
          return;
        }
        Object localObject4 = new android/location/Geocoder;
        Object localObject5 = this;
        localObject5 = (Context)this;
        Locale localLocale = Locale.getDefault();
        ((Geocoder)localObject4).<init>((Context)localObject5, localLocale);
        int i1;
        try
        {
          double d1 = paramIntent.getLatitude();
          double d2 = paramIntent.getLongitude();
          int n = 1;
          paramIntent = ((Geocoder)localObject4).getFromLocation(d1, d2, n);
        }
        catch (Exception paramIntent)
        {
          i1 = R.string.service_not_available;
          localObject1 = getString(i1);
          localObject4 = "getString(R.string.service_not_available)";
          k.a(localObject1, (String)localObject4);
          d.a((Throwable)paramIntent);
          m = 0;
          paramIntent = null;
        }
        int i2 = 0;
        localObject4 = null;
        if (paramIntent != null)
        {
          boolean bool2 = paramIntent.isEmpty();
          if (!bool2)
          {
            paramIntent = (Address)paramIntent.get(0);
            i1 = paramIntent.getMaxAddressLineIndex();
            localObject1 = (Iterable)i.b(0, i1);
            localObject2 = new java/util/ArrayList;
            j = m.a((Iterable)localObject1, 10);
            ((ArrayList)localObject2).<init>(j);
            localObject2 = (Collection)localObject2;
            localObject1 = ((Iterable)localObject1).iterator();
            for (;;)
            {
              boolean bool1 = ((Iterator)localObject1).hasNext();
              if (!bool1) {
                break;
              }
              localObject3 = localObject1;
              int k = ((ae)localObject1).a();
              localObject3 = paramIntent.getAddressLine(k);
              ((Collection)localObject2).add(localObject3);
            }
            localObject2 = (List)localObject2;
            localObject1 = (CharSequence)System.getProperty("line.separator");
            localObject2 = (Iterable)localObject2;
            localObject1 = TextUtils.join((CharSequence)localObject1, (Iterable)localObject2);
            k.a(localObject1, "TextUtils.join(System.ge…ator\"), addressFragments)");
            a(0, (String)localObject1, paramIntent);
            return;
          }
        }
        paramIntent = (Intent)localObject1;
        paramIntent = (CharSequence)localObject1;
        int m = paramIntent.length();
        if (m == 0) {
          i2 = 1;
        }
        if (i2 != 0)
        {
          m = R.string.no_address_found;
          localObject1 = getString(m);
          paramIntent = "getString(R.string.no_address_found)";
          k.a(localObject1, paramIntent);
        }
        a(i, (String)localObject1, null);
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.assist.FetchAddressIntentService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */