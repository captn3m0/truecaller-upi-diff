package com.truecaller.flashsdk.assist;

import c.g.b.k;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.truecaller.flashsdk.R.drawable;

public final class p
{
  public static final void a(GoogleMap paramGoogleMap, double paramDouble1, double paramDouble2)
  {
    k.b(paramGoogleMap, "receiver$0");
    paramGoogleMap.b();
    LatLng localLatLng = new com/google/android/gms/maps/model/LatLng;
    localLatLng.<init>(paramDouble1, paramDouble2);
    Object localObject = new com/google/android/gms/maps/model/MarkerOptions;
    ((MarkerOptions)localObject).<init>();
    localObject = ((MarkerOptions)localObject).a(localLatLng);
    BitmapDescriptor localBitmapDescriptor = BitmapDescriptorFactory.a(R.drawable.ic_flash_map_pin);
    localObject = ((MarkerOptions)localObject).a(localBitmapDescriptor);
    k.a(localObject, "MarkerOptions()\n        …awable.ic_flash_map_pin))");
    paramGoogleMap.a((MarkerOptions)localObject).a();
    paramGoogleMap.d();
    localObject = paramGoogleMap.c();
    k.a(localObject, "uiSettings");
    ((UiSettings)localObject).b();
    ((UiSettings)localObject).a();
    ((UiSettings)localObject).b();
    ((UiSettings)localObject).c();
    ((UiSettings)localObject).d();
    ((UiSettings)localObject).e();
    ((UiSettings)localObject).a(false);
    localObject = CameraUpdateFactory.a(localLatLng);
    paramGoogleMap.a((CameraUpdate)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.assist.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */