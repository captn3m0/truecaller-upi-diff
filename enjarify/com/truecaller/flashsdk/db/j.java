package com.truecaller.flashsdk.db;

import android.net.Uri;

public final class j
{
  public static final String a = "com.truecaller.flashsdk.provider";
  public static final Uri b;
  public static final Uri c;
  public static final Uri d;
  public static final Uri e;
  private static final String f = "flash_state";
  private static final String g = "flash_cache";
  
  static
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("content://");
    String str1 = a;
    localStringBuilder.append(str1);
    char c1 = '/';
    localStringBuilder.append(c1);
    String str2 = f;
    localStringBuilder.append(str2);
    b = Uri.parse(localStringBuilder.toString());
    localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("content://");
    str2 = a;
    localStringBuilder.append(str2);
    localStringBuilder.append(c1);
    str1 = g;
    localStringBuilder.append(str1);
    c = Uri.parse(localStringBuilder.toString());
    localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("content://");
    str1 = a;
    localStringBuilder.append(str1);
    localStringBuilder.append("/currentFlashes");
    d = Uri.parse(localStringBuilder.toString());
    localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("content://");
    str1 = a;
    localStringBuilder.append(str1);
    localStringBuilder.append("/cacheNumbers");
    e = Uri.parse(localStringBuilder.toString());
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.db.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */