package com.truecaller.flashsdk.db;

import android.content.Context;
import c.d.f;
import c.g.a.m;
import c.g.b.k;
import com.truecaller.flashsdk.models.Flash;
import com.truecaller.flashsdk.models.Payload;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;

public final class i
  implements h
{
  final android.support.v4.f.g a;
  final android.support.v4.f.g b;
  final Map c;
  final Context d;
  final f e;
  private final f f;
  
  public i(Context paramContext, f paramf1, f paramf2)
  {
    d = paramContext;
    e = paramf1;
    f = paramf2;
    paramContext = new android/support/v4/f/g;
    int i = 256;
    paramContext.<init>(i);
    a = paramContext;
    paramContext = new android/support/v4/f/g;
    paramContext.<init>(i);
    b = paramContext;
    paramContext = new java/util/LinkedHashMap;
    paramContext.<init>();
    paramContext = (Map)paramContext;
    c = paramContext;
    paramContext = (ag)bg.a;
    paramf1 = f;
    paramf2 = new com/truecaller/flashsdk/db/i$b;
    paramf2.<init>(this, null);
    paramf2 = (m)paramf2;
    kotlinx.coroutines.e.b(paramContext, paramf1, paramf2, 2);
  }
  
  public final g a(String paramString)
  {
    k.b(paramString, "phone");
    g localg = (g)a.get(paramString);
    if (localg == null)
    {
      localg = new com/truecaller/flashsdk/db/g;
      long l = 0L;
      Object localObject1 = localg;
      Object localObject2 = paramString;
      localg.<init>(paramString, l, null, null);
      a.put(paramString, localg);
      localObject1 = (ag)bg.a;
      localObject2 = f;
      Object localObject3 = new com/truecaller/flashsdk/db/i$a;
      ((i.a)localObject3).<init>(this, paramString, null);
      localObject3 = (m)localObject3;
      int i = 2;
      kotlinx.coroutines.e.b((ag)localObject1, (f)localObject2, (m)localObject3, i);
    }
    return localg;
  }
  
  public final void a(l paraml)
  {
    k.b(paraml, "listener");
    Object localObject1 = ((Iterable)c.values()).iterator();
    boolean bool1;
    Object localObject2;
    for (;;)
    {
      bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      localObject2 = (Set)((Iterator)localObject1).next();
      ((Set)localObject2).remove(paraml);
    }
    paraml = c;
    localObject1 = new java/util/LinkedHashMap;
    ((LinkedHashMap)localObject1).<init>();
    paraml = paraml.entrySet().iterator();
    for (;;)
    {
      bool1 = paraml.hasNext();
      if (!bool1) {
        break;
      }
      localObject2 = (Map.Entry)paraml.next();
      Object localObject3 = (Set)((Map.Entry)localObject2).getValue();
      boolean bool2 = ((Set)localObject3).isEmpty();
      if (bool2)
      {
        localObject3 = ((Map.Entry)localObject2).getKey();
        localObject2 = ((Map.Entry)localObject2).getValue();
        ((LinkedHashMap)localObject1).put(localObject3, localObject2);
      }
    }
    localObject1 = (Map)localObject1;
    paraml = ((Iterable)((Map)localObject1).keySet()).iterator();
    for (;;)
    {
      boolean bool3 = paraml.hasNext();
      if (!bool3) {
        break;
      }
      localObject1 = (String)paraml.next();
      localObject2 = c;
      ((Map)localObject2).remove(localObject1);
    }
  }
  
  public final void a(l paraml, String... paramVarArgs)
  {
    k.b(paraml, "listener");
    String str1 = "numbersWithoutPlus";
    k.b(paramVarArgs, str1);
    int i = paramVarArgs.length;
    int j = 0;
    while (j < i)
    {
      String str2 = paramVarArgs[j];
      Object localObject1 = (Set)c.get(str2);
      if (localObject1 == null)
      {
        localObject1 = this;
        localObject1 = (i)this;
        Object localObject2 = new java/util/LinkedHashSet;
        ((LinkedHashSet)localObject2).<init>();
        localObject2 = (Set)localObject2;
        localObject1 = c;
        ((Map)localObject1).put(str2, localObject2);
        boolean bool = ((Set)localObject2).add(paraml);
        Boolean.valueOf(bool);
      }
      j += 1;
    }
  }
  
  public final void a(String paramString, long paramLong, Flash paramFlash)
  {
    k.b(paramString, "phone");
    k.b(paramFlash, "flash");
    g localg = (g)a.get(paramString);
    if (localg == null) {
      return;
    }
    Object localObject1 = "historyCache.get(phone) ?: return";
    k.a(localg, (String)localObject1);
    long l = b;
    boolean bool = l < paramLong;
    if (bool)
    {
      localg = new com/truecaller/flashsdk/db/g;
      localObject1 = paramFlash.f();
      String str1 = "flash.payload";
      k.a(localObject1, str1);
      String str2 = ((Payload)localObject1).a();
      String str3 = paramFlash.e();
      localg.<init>(paramString, paramLong, str2, str3);
      paramString = a;
      Object localObject2 = a;
      paramString.put(localObject2, localg);
      paramString = (ag)bg.a;
      localObject2 = f;
      Object localObject3 = new com/truecaller/flashsdk/db/i$c;
      paramFlash = null;
      ((i.c)localObject3).<init>(this, localg, null);
      localObject3 = (m)localObject3;
      int i = 2;
      kotlinx.coroutines.e.b(paramString, (f)localObject2, (m)localObject3, i);
    }
  }
  
  public final void a(List paramList)
  {
    k.b(paramList, "flashStateList");
    ag localag = (ag)bg.a;
    f localf = f;
    Object localObject = new com/truecaller/flashsdk/db/i$d;
    ((i.d)localObject).<init>(this, paramList, null);
    localObject = (m)localObject;
    kotlinx.coroutines.e.b(localag, localf, (m)localObject, 2);
  }
  
  public final com.truecaller.flashsdk.models.e b(String paramString)
  {
    k.b(paramString, "phoneWithoutPlus");
    android.support.v4.f.g localg = b;
    paramString = (com.truecaller.flashsdk.models.e)localg.get(paramString);
    if (paramString == null)
    {
      paramString = new com/truecaller/flashsdk/models/e;
      paramString.<init>();
    }
    return paramString;
  }
  
  /* Error */
  final java.util.Collection b(List paramList)
  {
    // Byte code:
    //   0: new 78	java/util/ArrayList
    //   3: astore_2
    //   4: aload_2
    //   5: invokespecial 79	java/util/ArrayList:<init>	()V
    //   8: aload_2
    //   9: checkcast 106	java/util/List
    //   12: astore_2
    //   13: aload_1
    //   14: checkcast 76	java/lang/Iterable
    //   17: astore_1
    //   18: new 78	java/util/ArrayList
    //   21: astore_3
    //   22: aload_3
    //   23: invokespecial 79	java/util/ArrayList:<init>	()V
    //   26: aload_3
    //   27: checkcast 81	java/util/Collection
    //   30: astore_3
    //   31: aload_1
    //   32: invokeinterface 85 1 0
    //   37: astore_1
    //   38: aload_1
    //   39: invokeinterface 91 1 0
    //   44: istore 4
    //   46: iload 4
    //   48: ifeq +34 -> 82
    //   51: aload_1
    //   52: invokeinterface 95 1 0
    //   57: checkcast 97	com/truecaller/flashsdk/models/e
    //   60: getfield 100	com/truecaller/flashsdk/models/e:a	Ljava/lang/String;
    //   63: astore 5
    //   65: aload 5
    //   67: ifnull -29 -> 38
    //   70: aload_3
    //   71: aload 5
    //   73: invokeinterface 104 2 0
    //   78: pop
    //   79: goto -41 -> 38
    //   82: aload_3
    //   83: checkcast 106	java/util/List
    //   86: astore_3
    //   87: aconst_null
    //   88: astore_1
    //   89: new 108	java/lang/StringBuilder
    //   92: astore 5
    //   94: aload 5
    //   96: invokespecial 109	java/lang/StringBuilder:<init>	()V
    //   99: getstatic 113	com/truecaller/flashsdk/db/a$b:c	Ljava/lang/String;
    //   102: astore 6
    //   104: aload 5
    //   106: aload 6
    //   108: invokevirtual 117	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   111: pop
    //   112: ldc 119
    //   114: astore 6
    //   116: aload 5
    //   118: aload 6
    //   120: invokevirtual 117	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   123: pop
    //   124: aload 5
    //   126: invokevirtual 123	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   129: astore 7
    //   131: aload_0
    //   132: getfield 33	com/truecaller/flashsdk/db/i:d	Landroid/content/Context;
    //   135: astore 5
    //   137: aload 5
    //   139: invokevirtual 129	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   142: astore 8
    //   144: getstatic 276	com/truecaller/flashsdk/db/j:e	Landroid/net/Uri;
    //   147: astore 9
    //   149: aload_3
    //   150: checkcast 81	java/util/Collection
    //   153: astore_3
    //   154: iconst_0
    //   155: istore 4
    //   157: aconst_null
    //   158: astore 5
    //   160: iconst_0
    //   161: anewarray 136	java/lang/String
    //   164: astore 6
    //   166: aload_3
    //   167: aload 6
    //   169: invokeinterface 140 2 0
    //   174: astore_3
    //   175: aload_3
    //   176: ifnull +157 -> 333
    //   179: aconst_null
    //   180: astore 10
    //   182: aload_3
    //   183: astore 11
    //   185: aload_3
    //   186: checkcast 142	[Ljava/lang/String;
    //   189: astore 11
    //   191: aload 8
    //   193: aload 9
    //   195: aconst_null
    //   196: aload 7
    //   198: aload 11
    //   200: aconst_null
    //   201: invokevirtual 280	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   204: astore_3
    //   205: aload_3
    //   206: ifnull +114 -> 320
    //   209: aload_3
    //   210: astore 6
    //   212: aload_3
    //   213: checkcast 282	java/io/Closeable
    //   216: astore 6
    //   218: aload 6
    //   220: astore 8
    //   222: aload 6
    //   224: checkcast 284	android/database/Cursor
    //   227: astore 8
    //   229: aload 8
    //   231: invokeinterface 287 1 0
    //   236: istore 12
    //   238: iload 12
    //   240: ifeq +37 -> 277
    //   243: aload 8
    //   245: iconst_0
    //   246: invokeinterface 291 2 0
    //   251: astore 9
    //   253: ldc_w 293
    //   256: astore 10
    //   258: aload 9
    //   260: aload 10
    //   262: invokestatic 242	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   265: aload_2
    //   266: aload 9
    //   268: invokeinterface 294 2 0
    //   273: pop
    //   274: goto -45 -> 229
    //   277: getstatic 299	c/x:a	Lc/x;
    //   280: astore 5
    //   282: aload 6
    //   284: aconst_null
    //   285: invokestatic 304	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   288: goto +32 -> 320
    //   291: astore 5
    //   293: goto +6 -> 299
    //   296: astore_1
    //   297: aload_1
    //   298: athrow
    //   299: aload 6
    //   301: aload_1
    //   302: invokestatic 304	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   305: aload 5
    //   307: athrow
    //   308: astore_2
    //   309: aload_3
    //   310: astore_1
    //   311: goto +81 -> 392
    //   314: pop
    //   315: aload_3
    //   316: astore_1
    //   317: goto +37 -> 354
    //   320: aload_3
    //   321: ifnull +66 -> 387
    //   324: aload_3
    //   325: invokeinterface 307 1 0
    //   330: goto +57 -> 387
    //   333: new 150	c/u
    //   336: astore_3
    //   337: ldc -104
    //   339: astore 5
    //   341: aload_3
    //   342: aload 5
    //   344: invokespecial 155	c/u:<init>	(Ljava/lang/String;)V
    //   347: aload_3
    //   348: athrow
    //   349: astore_2
    //   350: goto +42 -> 392
    //   353: pop
    //   354: new 157	com/truecaller/log/UnmutedException$d
    //   357: astore_3
    //   358: ldc -97
    //   360: astore 5
    //   362: aload_3
    //   363: aload 5
    //   365: invokespecial 160	com/truecaller/log/UnmutedException$d:<init>	(Ljava/lang/String;)V
    //   368: aload_3
    //   369: checkcast 162	java/lang/Throwable
    //   372: astore_3
    //   373: aload_3
    //   374: invokestatic 167	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   377: aload_1
    //   378: ifnull +9 -> 387
    //   381: aload_1
    //   382: invokeinterface 307 1 0
    //   387: aload_2
    //   388: checkcast 81	java/util/Collection
    //   391: areturn
    //   392: aload_1
    //   393: ifnull +9 -> 402
    //   396: aload_1
    //   397: invokeinterface 307 1 0
    //   402: aload_2
    //   403: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	404	0	this	i
    //   0	404	1	paramList	List
    //   3	263	2	localObject1	Object
    //   308	1	2	localObject2	Object
    //   349	54	2	localObject3	Object
    //   21	353	3	localObject4	Object
    //   44	112	4	bool1	boolean
    //   63	218	5	localObject5	Object
    //   291	15	5	localObject6	Object
    //   339	25	5	str1	String
    //   102	198	6	localObject7	Object
    //   129	68	7	str2	String
    //   142	102	8	localObject8	Object
    //   147	120	9	localObject9	Object
    //   180	81	10	str3	String
    //   183	16	11	localObject10	Object
    //   236	3	12	bool2	boolean
    //   314	1	17	localException1	Exception
    //   353	1	18	localException2	Exception
    // Exception table:
    //   from	to	target	type
    //   297	299	291	finally
    //   222	227	296	finally
    //   229	236	296	finally
    //   245	251	296	finally
    //   260	265	296	finally
    //   266	274	296	finally
    //   277	280	296	finally
    //   212	216	308	finally
    //   284	288	308	finally
    //   301	305	308	finally
    //   305	308	308	finally
    //   212	216	314	java/lang/Exception
    //   284	288	314	java/lang/Exception
    //   301	305	314	java/lang/Exception
    //   305	308	314	java/lang/Exception
    //   89	92	349	finally
    //   94	99	349	finally
    //   99	102	349	finally
    //   106	112	349	finally
    //   118	124	349	finally
    //   124	129	349	finally
    //   131	135	349	finally
    //   137	142	349	finally
    //   144	147	349	finally
    //   149	153	349	finally
    //   160	164	349	finally
    //   167	174	349	finally
    //   185	189	349	finally
    //   200	204	349	finally
    //   333	336	349	finally
    //   342	347	349	finally
    //   347	349	349	finally
    //   354	357	349	finally
    //   363	368	349	finally
    //   368	372	349	finally
    //   373	377	349	finally
    //   89	92	353	java/lang/Exception
    //   94	99	353	java/lang/Exception
    //   99	102	353	java/lang/Exception
    //   106	112	353	java/lang/Exception
    //   118	124	353	java/lang/Exception
    //   124	129	353	java/lang/Exception
    //   131	135	353	java/lang/Exception
    //   137	142	353	java/lang/Exception
    //   144	147	353	java/lang/Exception
    //   149	153	353	java/lang/Exception
    //   160	164	353	java/lang/Exception
    //   167	174	353	java/lang/Exception
    //   185	189	353	java/lang/Exception
    //   200	204	353	java/lang/Exception
    //   333	336	353	java/lang/Exception
    //   342	347	353	java/lang/Exception
    //   347	349	353	java/lang/Exception
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.db.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */