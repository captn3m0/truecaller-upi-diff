package com.truecaller.flashsdk.db;

import android.content.ContentValues;
import android.database.Cursor;

public final class d
  implements c
{
  private final b a;
  
  public d(b paramb)
  {
    a = paramb;
  }
  
  public final int a(String paramString, ContentValues paramContentValues)
  {
    String str1 = "phoneNumber";
    c.g.b.k.b(paramString, str1);
    int i = -1;
    if (paramContentValues == null) {
      return i;
    }
    b localb = a;
    Long localLong = paramContentValues.getAsLong("timestamp");
    if (localLong != null)
    {
      long l = localLong.longValue();
      String str2 = paramContentValues.getAsString("type");
      if (str2 == null) {
        return i;
      }
      String str3 = paramContentValues.getAsString("history");
      return localb.a(l, str2, str3, paramString);
    }
    return i;
  }
  
  public final long a(ContentValues paramContentValues)
  {
    if (paramContentValues == null) {
      return -1;
    }
    f localf = new com/truecaller/flashsdk/db/f;
    String str1 = paramContentValues.getAsString("phone");
    c.g.b.k.a(str1, "contentValues.getAsStrin…istoryTable.COLUMN_PHONE)");
    Object localObject1 = paramContentValues.getAsString("type");
    if (localObject1 == null) {
      localObject1 = "";
    }
    Object localObject2 = localObject1;
    localObject1 = paramContentValues.getAsLong("timestamp");
    c.g.b.k.a(localObject1, "contentValues.getAsLong(…ryTable.COLUMN_TIMESTAMP)");
    long l = ((Long)localObject1).longValue();
    String str2 = paramContentValues.getAsString("history");
    localf.<init>(str1, (String)localObject2, l, str2);
    return a.a(localf);
  }
  
  public final Cursor a()
  {
    return a.a("call_me_back");
  }
  
  public final Cursor a(String paramString)
  {
    c.g.b.k.b(paramString, "phoneNumber");
    return a.b(paramString);
  }
  
  public final Cursor a(String[] paramArrayOfString)
  {
    if (paramArrayOfString == null) {
      return null;
    }
    return a.a(paramArrayOfString);
  }
  
  public final int b(String paramString)
  {
    c.g.b.k.b(paramString, "phoneNumber");
    return a.c(paramString);
  }
  
  public final int b(String paramString, ContentValues paramContentValues)
  {
    Object localObject = "phoneNumber";
    c.g.b.k.b(paramString, (String)localObject);
    if (paramContentValues == null) {
      return -1;
    }
    localObject = a;
    Integer localInteger = paramContentValues.getAsInteger("flash_enabled");
    int i = 0;
    int j;
    if (localInteger != null)
    {
      j = localInteger.intValue();
    }
    else
    {
      j = 0;
      localInteger = null;
    }
    String str = "version";
    paramContentValues = paramContentValues.getAsInteger(str);
    if (paramContentValues != null) {
      i = paramContentValues.intValue();
    }
    return ((b)localObject).a(j, i, paramString);
  }
  
  public final int b(String[] paramArrayOfString)
  {
    if (paramArrayOfString == null) {
      return -1;
    }
    return a.b(paramArrayOfString);
  }
  
  public final long b(ContentValues paramContentValues)
  {
    if (paramContentValues == null) {
      return -1;
    }
    k localk = new com/truecaller/flashsdk/db/k;
    String str1 = paramContentValues.getAsString("phone");
    c.g.b.k.a(str1, "contentValues.getAsStrin….StateTable.COLUMN_PHONE)");
    Integer localInteger = paramContentValues.getAsInteger("flash_enabled");
    c.g.b.k.a(localInteger, "contentValues.getAsInteg…tateTable.COLUMN_ENABLED)");
    int i = localInteger.intValue();
    String str2 = "version";
    paramContentValues = paramContentValues.getAsInteger(str2);
    int j;
    if (paramContentValues != null)
    {
      j = paramContentValues.intValue();
    }
    else
    {
      j = 0;
      paramContentValues = null;
    }
    localk.<init>(str1, i, j);
    return a.a(localk);
  }
  
  public final Cursor b()
  {
    return a.a();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.db.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */