package com.truecaller.flashsdk.db;

import android.arch.persistence.room.b.b.a;
import android.arch.persistence.room.b.b.d;
import android.arch.persistence.room.f.b;
import android.arch.persistence.room.h.a;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

final class FlashDatabase_Impl$1
  extends h.a
{
  FlashDatabase_Impl$1(FlashDatabase_Impl paramFlashDatabase_Impl)
  {
    super(5);
  }
  
  public final void a(android.arch.persistence.db.b paramb)
  {
    paramb.c("DROP TABLE IF EXISTS `flash_state`");
    paramb.c("DROP TABLE IF EXISTS `flash_cache`");
  }
  
  public final void b(android.arch.persistence.db.b paramb)
  {
    paramb.c("CREATE TABLE IF NOT EXISTS `flash_state` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT, `phone` TEXT NOT NULL, `type` TEXT NOT NULL, `timestamp` INTEGER NOT NULL, `history` TEXT)");
    paramb.c("CREATE UNIQUE INDEX `index_flash_state_phone` ON `flash_state` (`phone`)");
    paramb.c("CREATE TABLE IF NOT EXISTS `flash_cache` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT, `phone` TEXT NOT NULL, `flash_enabled` INTEGER NOT NULL, `version` INTEGER NOT NULL)");
    paramb.c("CREATE UNIQUE INDEX `index_flash_cache_phone` ON `flash_cache` (`phone`)");
    paramb.c("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
    paramb.c("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, \"8068506873f25b1a7d7eda895dd84d81\")");
  }
  
  public final void c(android.arch.persistence.db.b paramb)
  {
    FlashDatabase_Impl.a(b, paramb);
    FlashDatabase_Impl.b(b, paramb);
    List localList1 = FlashDatabase_Impl.d(b);
    if (localList1 != null)
    {
      int i = 0;
      localList1 = null;
      List localList2 = FlashDatabase_Impl.e(b);
      int j = localList2.size();
      while (i < j)
      {
        f.b localb = (f.b)FlashDatabase_Impl.f(b).get(i);
        localb.b(paramb);
        i += 1;
      }
    }
  }
  
  public final void d(android.arch.persistence.db.b paramb)
  {
    List localList1 = FlashDatabase_Impl.a(b);
    if (localList1 != null)
    {
      int i = 0;
      localList1 = null;
      List localList2 = FlashDatabase_Impl.b(b);
      int j = localList2.size();
      while (i < j)
      {
        f.b localb = (f.b)FlashDatabase_Impl.c(b).get(i);
        localb.a(paramb);
        i += 1;
      }
    }
  }
  
  public final void e(android.arch.persistence.db.b paramb)
  {
    Object localObject1 = new java/util/HashMap;
    ((HashMap)localObject1).<init>(5);
    Object localObject2 = new android/arch/persistence/room/b/b$a;
    int i = 1;
    List localList1 = null;
    ((b.a)localObject2).<init>("_id", "INTEGER", false, i);
    ((HashMap)localObject1).put("_id", localObject2);
    localObject2 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject2).<init>("phone", "TEXT", i, 0);
    ((HashMap)localObject1).put("phone", localObject2);
    localObject2 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject2).<init>("type", "TEXT", i, 0);
    ((HashMap)localObject1).put("type", localObject2);
    localObject2 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject2).<init>("timestamp", "INTEGER", i, 0);
    ((HashMap)localObject1).put("timestamp", localObject2);
    localObject2 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject2).<init>("history", "TEXT", false, 0);
    ((HashMap)localObject1).put("history", localObject2);
    Object localObject3 = new java/util/HashSet;
    ((HashSet)localObject3).<init>(0);
    localObject2 = new java/util/HashSet;
    ((HashSet)localObject2).<init>(i);
    Object localObject4 = new android/arch/persistence/room/b/b$d;
    List localList2 = Arrays.asList(new String[] { "phone" });
    ((b.d)localObject4).<init>("index_flash_state_phone", i, localList2);
    ((HashSet)localObject2).add(localObject4);
    localObject4 = new android/arch/persistence/room/b/b;
    String str = "flash_state";
    ((android.arch.persistence.room.b.b)localObject4).<init>(str, (Map)localObject1, (Set)localObject3, (Set)localObject2);
    localObject1 = android.arch.persistence.room.b.b.a(paramb, "flash_state");
    boolean bool1 = ((android.arch.persistence.room.b.b)localObject4).equals(localObject1);
    if (bool1)
    {
      localObject1 = new java/util/HashMap;
      int j = 4;
      ((HashMap)localObject1).<init>(j);
      localObject2 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject2).<init>("_id", "INTEGER", false, i);
      ((HashMap)localObject1).put("_id", localObject2);
      localObject2 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject2).<init>("phone", "TEXT", i, 0);
      ((HashMap)localObject1).put("phone", localObject2);
      localObject2 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject2).<init>("flash_enabled", "INTEGER", i, 0);
      ((HashMap)localObject1).put("flash_enabled", localObject2);
      localObject2 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject2).<init>("version", "INTEGER", i, 0);
      ((HashMap)localObject1).put("version", localObject2);
      localObject3 = new java/util/HashSet;
      ((HashSet)localObject3).<init>(0);
      localObject2 = new java/util/HashSet;
      ((HashSet)localObject2).<init>(i);
      localObject4 = new android/arch/persistence/room/b/b$d;
      localList1 = Arrays.asList(new String[] { "phone" });
      ((b.d)localObject4).<init>("index_flash_cache_phone", i, localList1);
      ((HashSet)localObject2).add(localObject4);
      localObject4 = new android/arch/persistence/room/b/b;
      str = "flash_cache";
      ((android.arch.persistence.room.b.b)localObject4).<init>(str, (Map)localObject1, (Set)localObject3, (Set)localObject2);
      localObject1 = "flash_cache";
      paramb = android.arch.persistence.room.b.b.a(paramb, (String)localObject1);
      boolean bool2 = ((android.arch.persistence.room.b.b)localObject4).equals(paramb);
      if (bool2) {
        return;
      }
      localObject1 = new java/lang/IllegalStateException;
      localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>("Migration didn't properly handle flash_cache(com.truecaller.flashsdk.db.FlashState).\n Expected:\n");
      ((StringBuilder)localObject3).append(localObject4);
      ((StringBuilder)localObject3).append("\n Found:\n");
      ((StringBuilder)localObject3).append(paramb);
      paramb = ((StringBuilder)localObject3).toString();
      ((IllegalStateException)localObject1).<init>(paramb);
      throw ((Throwable)localObject1);
    }
    paramb = new java/lang/IllegalStateException;
    localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>("Migration didn't properly handle flash_state(com.truecaller.flashsdk.db.FlashHistory).\n Expected:\n");
    ((StringBuilder)localObject3).append(localObject4);
    ((StringBuilder)localObject3).append("\n Found:\n");
    ((StringBuilder)localObject3).append(localObject1);
    localObject1 = ((StringBuilder)localObject3).toString();
    paramb.<init>((String)localObject1);
    throw paramb;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.db.FlashDatabase_Impl.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */