package com.truecaller.flashsdk.db;

import android.arch.persistence.room.b.a;
import android.arch.persistence.room.c;
import android.arch.persistence.room.i;
import android.arch.persistence.room.j;
import android.database.Cursor;

public final class e
  implements b
{
  private final android.arch.persistence.room.f a;
  private final c b;
  private final c c;
  private final j d;
  private final j e;
  private final j f;
  
  public e(android.arch.persistence.room.f paramf)
  {
    a = paramf;
    Object localObject = new com/truecaller/flashsdk/db/e$1;
    ((e.1)localObject).<init>(this, paramf);
    b = ((c)localObject);
    localObject = new com/truecaller/flashsdk/db/e$2;
    ((e.2)localObject).<init>(this, paramf);
    c = ((c)localObject);
    localObject = new com/truecaller/flashsdk/db/e$3;
    ((e.3)localObject).<init>(this, paramf);
    d = ((j)localObject);
    localObject = new com/truecaller/flashsdk/db/e$4;
    ((e.4)localObject).<init>(this, paramf);
    e = ((j)localObject);
    localObject = new com/truecaller/flashsdk/db/e$5;
    ((e.5)localObject).<init>(this, paramf);
    f = ((j)localObject);
  }
  
  public final int a(int paramInt1, int paramInt2, String paramString)
  {
    android.arch.persistence.db.f localf = f.b();
    android.arch.persistence.room.f localf1 = a;
    localf1.d();
    int i = 1;
    long l1 = paramInt1;
    try
    {
      localf.a(i, l1);
      paramInt1 = 2;
      long l2 = paramInt2;
      localf.a(paramInt1, l2);
      paramInt1 = 3;
      if (paramString == null) {
        localf.a(paramInt1);
      } else {
        localf.a(paramInt1, paramString);
      }
      paramInt1 = localf.a();
      android.arch.persistence.room.f localf2 = a;
      localf2.f();
      return paramInt1;
    }
    finally
    {
      a.e();
      f.a(localf);
    }
  }
  
  public final int a(long paramLong, String paramString1, String paramString2, String paramString3)
  {
    android.arch.persistence.db.f localf = e.b();
    android.arch.persistence.room.f localf1 = a;
    localf1.d();
    int i = 1;
    try
    {
      localf.a(i, paramLong);
      int j = 2;
      if (paramString1 == null) {
        localf.a(j);
      } else {
        localf.a(j, paramString1);
      }
      j = 3;
      if (paramString2 == null) {
        localf.a(j);
      } else {
        localf.a(j, paramString2);
      }
      j = 4;
      if (paramString3 == null) {
        localf.a(j);
      } else {
        localf.a(j, paramString3);
      }
      j = localf.a();
      android.arch.persistence.room.f localf2 = a;
      localf2.f();
      return j;
    }
    finally
    {
      a.e();
      e.a(localf);
    }
  }
  
  public final long a(f paramf)
  {
    Object localObject = a;
    ((android.arch.persistence.room.f)localObject).d();
    try
    {
      localObject = b;
      long l = ((c)localObject).b(paramf);
      paramf = a;
      paramf.f();
      return l;
    }
    finally
    {
      a.e();
    }
  }
  
  public final long a(k paramk)
  {
    Object localObject = a;
    ((android.arch.persistence.room.f)localObject).d();
    try
    {
      localObject = c;
      long l = ((c)localObject).b(paramk);
      paramk = a;
      paramk.f();
      return l;
    }
    finally
    {
      a.e();
    }
  }
  
  public final Cursor a()
  {
    i locali = i.a("SELECT * FROM flash_cache", 0);
    return a.a(locali);
  }
  
  public final Cursor a(String paramString)
  {
    int i = 2;
    i locali = i.a("SELECT * FROM flash_state WHERE timestamp > ? AND type != ? ORDER BY timestamp DESC", i);
    locali.a(1, 0L);
    locali.a(i, paramString);
    return a.a(locali);
  }
  
  public final Cursor a(String[] paramArrayOfString)
  {
    Object localObject1 = a.a();
    String str = "SELECT phone FROM flash_cache WHERE phone IN (";
    ((StringBuilder)localObject1).append(str);
    int i = paramArrayOfString.length;
    a.a((StringBuilder)localObject1, i);
    ((StringBuilder)localObject1).append(")");
    localObject1 = ((StringBuilder)localObject1).toString();
    int j = 0;
    i += 0;
    localObject1 = i.a((String)localObject1, i);
    i = paramArrayOfString.length;
    int k = 1;
    int m = 1;
    while (j < i)
    {
      Object localObject2 = paramArrayOfString[j];
      if (localObject2 == null)
      {
        localObject2 = e;
        localObject2[m] = k;
      }
      else
      {
        ((i)localObject1).a(m, (String)localObject2);
      }
      m += 1;
      j += 1;
    }
    return a.a((android.arch.persistence.db.e)localObject1);
  }
  
  public final int b(String[] paramArrayOfString)
  {
    Object localObject = a.a();
    ((StringBuilder)localObject).append("DELETE FROM flash_cache WHERE phone IN (");
    int i = paramArrayOfString.length;
    a.a((StringBuilder)localObject, i);
    ((StringBuilder)localObject).append(")");
    localObject = ((StringBuilder)localObject).toString();
    android.arch.persistence.room.f localf = a;
    localObject = localf.a((String)localObject);
    i = paramArrayOfString.length;
    int j = 1;
    int k = 0;
    while (k < i)
    {
      String str = paramArrayOfString[k];
      if (str == null) {
        ((android.arch.persistence.db.f)localObject).a(j);
      } else {
        ((android.arch.persistence.db.f)localObject).a(j, str);
      }
      j += 1;
      k += 1;
    }
    paramArrayOfString = a;
    paramArrayOfString.d();
    try
    {
      int m = ((android.arch.persistence.db.f)localObject).a();
      localObject = a;
      ((android.arch.persistence.room.f)localObject).f();
      return m;
    }
    finally
    {
      a.e();
    }
  }
  
  public final Cursor b(String paramString)
  {
    int i = 1;
    i locali = i.a("SELECT * FROM flash_state WHERE phone = ?", i);
    if (paramString == null)
    {
      paramString = e;
      paramString[i] = i;
    }
    else
    {
      locali.a(i, paramString);
    }
    return a.a(locali);
  }
  
  public final int c(String paramString)
  {
    android.arch.persistence.db.f localf = d.b();
    android.arch.persistence.room.f localf1 = a;
    localf1.d();
    int i = 1;
    if (paramString == null) {}
    try
    {
      localf.a(i);
      break label44;
      localf.a(i, paramString);
      label44:
      int j = localf.a();
      localf1 = a;
      localf1.f();
      return j;
    }
    finally
    {
      a.e();
      d.a(localf);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.db.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */