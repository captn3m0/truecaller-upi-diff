package com.truecaller.flashsdk.db;

import c.d.c;
import c.g.a.m;
import c.x;
import kotlinx.coroutines.ag;

final class i$b
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  i$b(i parami, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/flashsdk/db/i$b;
    i locali = b;
    localb.<init>(locali, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return localb;
  }
  
  /* Error */
  public final Object a(Object paramObject)
  {
    // Byte code:
    //   0: getstatic 38	c/d/a/a:a	Lc/d/a/a;
    //   3: astore_2
    //   4: aload_0
    //   5: getfield 40	com/truecaller/flashsdk/db/i$b:a	I
    //   8: istore_3
    //   9: iload_3
    //   10: ifne +367 -> 377
    //   13: aload_1
    //   14: instanceof 42
    //   17: istore_3
    //   18: iload_3
    //   19: ifne +350 -> 369
    //   22: aconst_null
    //   23: astore_1
    //   24: aload_0
    //   25: getfield 14	com/truecaller/flashsdk/db/i$b:b	Lcom/truecaller/flashsdk/db/i;
    //   28: astore_2
    //   29: aload_2
    //   30: getfield 48	com/truecaller/flashsdk/db/i:d	Landroid/content/Context;
    //   33: astore_2
    //   34: aload_2
    //   35: invokevirtual 54	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   38: astore 4
    //   40: getstatic 59	com/truecaller/flashsdk/db/j:c	Landroid/net/Uri;
    //   43: astore 5
    //   45: getstatic 64	com/truecaller/flashsdk/db/a$b:a	[Ljava/lang/String;
    //   48: astore 6
    //   50: iconst_0
    //   51: istore 7
    //   53: aconst_null
    //   54: astore 8
    //   56: iconst_0
    //   57: istore 9
    //   59: aconst_null
    //   60: astore 10
    //   62: aconst_null
    //   63: astore 11
    //   65: aload 4
    //   67: aload 5
    //   69: aload 6
    //   71: aconst_null
    //   72: aconst_null
    //   73: aconst_null
    //   74: invokevirtual 70	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   77: astore_2
    //   78: aload_2
    //   79: ifnull +223 -> 302
    //   82: aload_2
    //   83: astore 4
    //   85: aload_2
    //   86: checkcast 72	java/io/Closeable
    //   89: astore 4
    //   91: aload 4
    //   93: astore 5
    //   95: aload 4
    //   97: checkcast 74	android/database/Cursor
    //   100: astore 5
    //   102: aload 5
    //   104: invokeinterface 78 1 0
    //   109: istore 12
    //   111: iload 12
    //   113: ifeq +140 -> 253
    //   116: ldc 80
    //   118: astore 6
    //   120: aload 5
    //   122: aload 6
    //   124: invokeinterface 84 2 0
    //   129: istore 12
    //   131: aload 5
    //   133: iload 12
    //   135: invokeinterface 88 2 0
    //   140: istore 12
    //   142: ldc 90
    //   144: astore 8
    //   146: aload 5
    //   148: aload 8
    //   150: invokeinterface 84 2 0
    //   155: istore 7
    //   157: aload 5
    //   159: iload 7
    //   161: invokeinterface 94 2 0
    //   166: astore 8
    //   168: ldc 96
    //   170: astore 10
    //   172: aload 5
    //   174: aload 10
    //   176: invokeinterface 84 2 0
    //   181: istore 9
    //   183: aload 5
    //   185: iload 9
    //   187: invokeinterface 88 2 0
    //   192: istore 9
    //   194: aload_0
    //   195: getfield 14	com/truecaller/flashsdk/db/i$b:b	Lcom/truecaller/flashsdk/db/i;
    //   198: astore 11
    //   200: aload 11
    //   202: getfield 99	com/truecaller/flashsdk/db/i:b	Landroid/support/v4/f/g;
    //   205: astore 11
    //   207: new 101	com/truecaller/flashsdk/models/e
    //   210: astore 13
    //   212: iload 12
    //   214: ifeq +9 -> 223
    //   217: iconst_1
    //   218: istore 12
    //   220: goto +9 -> 229
    //   223: iconst_0
    //   224: istore 12
    //   226: aconst_null
    //   227: astore 6
    //   229: aload 13
    //   231: aload 8
    //   233: iload 9
    //   235: iload 12
    //   237: invokespecial 105	com/truecaller/flashsdk/models/e:<init>	(Ljava/lang/String;IZ)V
    //   240: aload 11
    //   242: aload 8
    //   244: aload 13
    //   246: invokevirtual 111	android/support/v4/f/g:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   249: pop
    //   250: goto -148 -> 102
    //   253: getstatic 116	c/x:a	Lc/x;
    //   256: astore 5
    //   258: aload 4
    //   260: aconst_null
    //   261: invokestatic 121	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   264: goto +38 -> 302
    //   267: astore 5
    //   269: goto +6 -> 275
    //   272: astore_1
    //   273: aload_1
    //   274: athrow
    //   275: aload 4
    //   277: aload_1
    //   278: invokestatic 121	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   281: aload 5
    //   283: athrow
    //   284: astore_1
    //   285: aload_2
    //   286: astore 14
    //   288: aload_1
    //   289: astore_2
    //   290: aload 14
    //   292: astore_1
    //   293: goto +64 -> 357
    //   296: pop
    //   297: aload_2
    //   298: astore_1
    //   299: goto +21 -> 320
    //   302: aload_2
    //   303: ifnull +50 -> 353
    //   306: aload_2
    //   307: invokeinterface 125 1 0
    //   312: goto +41 -> 353
    //   315: astore_2
    //   316: goto +41 -> 357
    //   319: pop
    //   320: new 127	com/truecaller/log/UnmutedException$d
    //   323: astore_2
    //   324: ldc -127
    //   326: astore 4
    //   328: aload_2
    //   329: aload 4
    //   331: invokespecial 132	com/truecaller/log/UnmutedException$d:<init>	(Ljava/lang/String;)V
    //   334: aload_2
    //   335: checkcast 134	java/lang/Throwable
    //   338: astore_2
    //   339: aload_2
    //   340: invokestatic 139	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   343: aload_1
    //   344: ifnull +9 -> 353
    //   347: aload_1
    //   348: invokeinterface 125 1 0
    //   353: getstatic 116	c/x:a	Lc/x;
    //   356: areturn
    //   357: aload_1
    //   358: ifnull +9 -> 367
    //   361: aload_1
    //   362: invokeinterface 125 1 0
    //   367: aload_2
    //   368: athrow
    //   369: aload_1
    //   370: checkcast 42	c/o$b
    //   373: getfield 142	c/o$b:a	Ljava/lang/Throwable;
    //   376: athrow
    //   377: new 144	java/lang/IllegalStateException
    //   380: astore_1
    //   381: aload_1
    //   382: ldc -110
    //   384: invokespecial 147	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
    //   387: aload_1
    //   388: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	389	0	this	b
    //   0	389	1	paramObject	Object
    //   3	304	2	localObject1	Object
    //   315	1	2	localObject2	Object
    //   323	45	2	localObject3	Object
    //   8	2	3	i	int
    //   17	2	3	bool1	boolean
    //   38	292	4	localObject4	Object
    //   43	214	5	localObject5	Object
    //   267	15	5	localObject6	Object
    //   48	180	6	localObject7	Object
    //   51	109	7	j	int
    //   54	189	8	str1	String
    //   57	177	9	k	int
    //   60	115	10	str2	String
    //   63	178	11	localObject8	Object
    //   109	3	12	bool2	boolean
    //   129	107	12	m	int
    //   210	35	13	locale	com.truecaller.flashsdk.models.e
    //   286	5	14	localObject9	Object
    //   296	1	20	localException1	Exception
    //   319	1	21	localException2	Exception
    // Exception table:
    //   from	to	target	type
    //   273	275	267	finally
    //   95	100	272	finally
    //   102	109	272	finally
    //   122	129	272	finally
    //   133	140	272	finally
    //   148	155	272	finally
    //   159	166	272	finally
    //   174	181	272	finally
    //   185	192	272	finally
    //   194	198	272	finally
    //   200	205	272	finally
    //   207	210	272	finally
    //   235	240	272	finally
    //   244	250	272	finally
    //   253	256	272	finally
    //   85	89	284	finally
    //   260	264	284	finally
    //   277	281	284	finally
    //   281	284	284	finally
    //   85	89	296	java/lang/Exception
    //   260	264	296	java/lang/Exception
    //   277	281	296	java/lang/Exception
    //   281	284	296	java/lang/Exception
    //   24	28	315	finally
    //   29	33	315	finally
    //   34	38	315	finally
    //   40	43	315	finally
    //   45	48	315	finally
    //   73	77	315	finally
    //   320	323	315	finally
    //   329	334	315	finally
    //   334	338	315	finally
    //   339	343	315	finally
    //   24	28	319	java/lang/Exception
    //   29	33	319	java/lang/Exception
    //   34	38	319	java/lang/Exception
    //   40	43	319	java/lang/Exception
    //   45	48	319	java/lang/Exception
    //   73	77	319	java/lang/Exception
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.db.i.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */