package com.truecaller.flashsdk.db;

import android.arch.persistence.room.e;
import android.arch.persistence.room.f.a;
import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import c.g.b.k;

public final class FlashProvider
  extends ContentProvider
{
  private final UriMatcher a;
  private c b;
  
  public FlashProvider()
  {
    UriMatcher localUriMatcher = new android/content/UriMatcher;
    localUriMatcher.<init>(-1);
    a = localUriMatcher;
    localUriMatcher = a;
    String str1 = j.a;
    Object localObject = j.a();
    localUriMatcher.addURI(str1, (String)localObject, 100);
    localUriMatcher = a;
    str1 = j.a;
    localObject = j.b();
    localUriMatcher.addURI(str1, (String)localObject, 200);
    localUriMatcher = a;
    str1 = j.a;
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    String str2 = j.a();
    ((StringBuilder)localObject).append(str2);
    ((StringBuilder)localObject).append("/*");
    localObject = ((StringBuilder)localObject).toString();
    localUriMatcher.addURI(str1, (String)localObject, 101);
    localUriMatcher = a;
    str1 = j.a;
    localUriMatcher.addURI(str1, "currentFlashes", 102);
    localUriMatcher = a;
    str1 = j.a;
    localUriMatcher.addURI(str1, "cacheNumbers", 201);
  }
  
  public final int delete(Uri paramUri, String paramString, String[] paramArrayOfString)
  {
    k.b(paramUri, "uri");
    paramString = a;
    int i = paramString.match(paramUri);
    int j = 200;
    String str1;
    if (i != j)
    {
      j = 0;
      str1 = null;
      switch (i)
      {
      default: 
        paramUri = new java/lang/IllegalArgumentException;
        paramUri.<init>("Unknown URI");
        throw ((Throwable)paramUri);
      case 101: 
        paramString = paramUri.getLastPathSegment();
        if (paramString == null) {
          return 0;
        }
        paramArrayOfString = b;
        if (paramArrayOfString == null)
        {
          str1 = "flashDaoManager";
          k.a(str1);
        }
        i = paramArrayOfString.b(paramString);
        break;
      case 100: 
        paramString = b;
        if (paramString == null)
        {
          String str2 = "flashDaoManager";
          k.a(str2);
        }
        if (paramArrayOfString != null)
        {
          paramArrayOfString = paramArrayOfString[0];
          if (paramArrayOfString != null)
          {
            i = paramString.b(paramArrayOfString);
            break;
          }
        }
        return 0;
      }
    }
    else
    {
      paramString = b;
      if (paramString == null)
      {
        str1 = "flashDaoManager";
        k.a(str1);
      }
      i = paramString.b(paramArrayOfString);
    }
    paramArrayOfString = getContext();
    if (paramArrayOfString != null)
    {
      paramArrayOfString = paramArrayOfString.getContentResolver();
      if (paramArrayOfString != null)
      {
        j = 0;
        str1 = null;
        paramArrayOfString.notifyChange(paramUri, null);
      }
    }
    return i;
  }
  
  public final String getType(Uri paramUri)
  {
    k.b(paramUri, "uri");
    return null;
  }
  
  public final Uri insert(Uri paramUri, ContentValues paramContentValues)
  {
    k.b(paramUri, "uri");
    Object localObject1 = a;
    int i = ((UriMatcher)localObject1).match(paramUri);
    int j = 100;
    String str;
    long l;
    if (i != j)
    {
      j = 200;
      if (i == j)
      {
        localObject1 = b;
        if (localObject1 == null)
        {
          str = "flashDaoManager";
          k.a(str);
        }
        l = ((c)localObject1).b(paramContentValues);
        paramContentValues = j.b();
      }
      else
      {
        paramUri = new java/lang/IllegalArgumentException;
        paramUri.<init>("Unknown URI");
        throw ((Throwable)paramUri);
      }
    }
    else
    {
      localObject1 = b;
      if (localObject1 == null)
      {
        str = "flashDaoManager";
        k.a(str);
      }
      l = ((c)localObject1).a(paramContentValues);
      paramContentValues = j.a();
    }
    Object localObject2 = getContext();
    if (localObject2 != null)
    {
      localObject2 = ((Context)localObject2).getContentResolver();
      if (localObject2 != null) {
        ((ContentResolver)localObject2).notifyChange(paramUri, null);
      }
    }
    paramUri = new java/lang/StringBuilder;
    paramUri.<init>();
    paramUri.append(paramContentValues);
    paramUri.append('/');
    paramUri.append(l);
    return Uri.parse(paramUri.toString());
  }
  
  public final boolean onCreate()
  {
    Object localObject1 = e.a(getContext(), FlashDatabase.class, "flashSdkDb").a().b();
    k.a(localObject1, "Room.databaseBuilder(\n  …on()\n            .build()");
    localObject1 = (FlashDatabase)localObject1;
    Object localObject2 = new com/truecaller/flashsdk/db/d;
    localObject1 = ((FlashDatabase)localObject1).h();
    ((d)localObject2).<init>((b)localObject1);
    localObject2 = (c)localObject2;
    b = ((c)localObject2);
    return true;
  }
  
  public final Cursor query(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2)
  {
    k.b(paramUri, "uri");
    paramArrayOfString1 = a;
    int i = paramArrayOfString1.match(paramUri);
    paramString1 = null;
    switch (i)
    {
    default: 
      switch (i)
      {
      default: 
        paramArrayOfString1 = new java/lang/IllegalArgumentException;
        paramArrayOfString2 = "Unknown URI";
        paramArrayOfString1.<init>(paramArrayOfString2);
        com.truecaller.log.d.a((Throwable)paramArrayOfString1);
        i = 0;
        paramArrayOfString1 = null;
        break;
      case 201: 
        paramArrayOfString1 = b;
        if (paramArrayOfString1 == null)
        {
          paramString2 = "flashDaoManager";
          k.a(paramString2);
        }
        paramArrayOfString1 = paramArrayOfString1.a(paramArrayOfString2);
        break;
      case 200: 
        paramArrayOfString1 = b;
        if (paramArrayOfString1 == null)
        {
          paramArrayOfString2 = "flashDaoManager";
          k.a(paramArrayOfString2);
        }
        paramArrayOfString1 = paramArrayOfString1.b();
      }
      break;
    case 102: 
      paramArrayOfString1 = b;
      if (paramArrayOfString1 == null)
      {
        paramArrayOfString2 = "flashDaoManager";
        k.a(paramArrayOfString2);
      }
      paramArrayOfString1 = paramArrayOfString1.a();
      break;
    case 101: 
      paramArrayOfString1 = b;
      if (paramArrayOfString1 == null)
      {
        paramArrayOfString2 = "flashDaoManager";
        k.a(paramArrayOfString2);
      }
      paramArrayOfString2 = paramUri.getLastPathSegment();
      if (paramArrayOfString2 == null) {
        return null;
      }
      paramArrayOfString1 = paramArrayOfString1.a(paramArrayOfString2);
      break;
    case 100: 
      paramArrayOfString1 = b;
      if (paramArrayOfString1 == null)
      {
        paramString2 = "flashDaoManager";
        k.a(paramString2);
      }
      if (paramArrayOfString2 == null) {
        break label310;
      }
      paramString2 = null;
      paramArrayOfString2 = paramArrayOfString2[0];
      if (paramArrayOfString2 == null) {
        break label310;
      }
      paramArrayOfString1 = paramArrayOfString1.a(paramArrayOfString2);
    }
    if (paramArrayOfString1 != null)
    {
      paramArrayOfString2 = getContext();
      if (paramArrayOfString2 != null) {
        paramString1 = paramArrayOfString2.getContentResolver();
      }
      paramArrayOfString1.setNotificationUri(paramString1, paramUri);
    }
    return paramArrayOfString1;
    label310:
    return null;
  }
  
  public final int update(Uri paramUri, ContentValues paramContentValues, String paramString, String[] paramArrayOfString)
  {
    k.b(paramUri, "uri");
    paramString = a;
    int i = paramString.match(paramUri);
    int j = 200;
    String str;
    int k;
    if (i != j)
    {
      switch (i)
      {
      default: 
        paramUri = new java/lang/IllegalArgumentException;
        paramUri.<init>("Unknown URI");
        throw ((Throwable)paramUri);
      case 101: 
        paramString = paramUri.getLastPathSegment();
        if (paramString == null) {
          return 0;
        }
        paramArrayOfString = b;
        if (paramArrayOfString == null)
        {
          str = "flashDaoManager";
          k.a(str);
        }
        k = paramArrayOfString.a(paramString, paramContentValues);
        break;
      case 100: 
        paramString = b;
        if (paramString == null)
        {
          str = "flashDaoManager";
          k.a(str);
        }
        if (paramArrayOfString != null)
        {
          paramArrayOfString = paramArrayOfString[0];
          if (paramArrayOfString != null)
          {
            k = paramString.a(paramArrayOfString, paramContentValues);
            break;
          }
        }
        return 0;
      }
    }
    else
    {
      paramString = b;
      if (paramString == null)
      {
        str = "flashDaoManager";
        k.a(str);
      }
      if (paramArrayOfString == null) {
        break label247;
      }
      paramArrayOfString = paramArrayOfString[0];
      if (paramArrayOfString == null) {
        break label247;
      }
      k = paramString.b(paramArrayOfString, paramContentValues);
    }
    paramString = getContext();
    if (paramString != null)
    {
      paramString = paramString.getContentResolver();
      if (paramString != null)
      {
        paramArrayOfString = null;
        paramString.notifyChange(paramUri, null);
      }
    }
    return k;
    label247:
    return 0;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.db.FlashProvider
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */