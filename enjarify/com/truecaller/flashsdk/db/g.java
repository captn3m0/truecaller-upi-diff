package com.truecaller.flashsdk.db;

import android.content.ContentValues;
import android.net.Uri;
import c.g.b.k;

public final class g
{
  final String a;
  public long b;
  private Uri c;
  private String d;
  private String e;
  private String f;
  
  public g(String paramString1, long paramLong, String paramString2, String paramString3)
  {
    a = paramString1;
    b = paramLong;
    c = null;
    d = null;
    e = paramString2;
    f = paramString3;
  }
  
  public final ContentValues a()
  {
    ContentValues localContentValues = new android/content/ContentValues;
    localContentValues.<init>();
    Object localObject = a;
    localContentValues.put("phone", (String)localObject);
    localObject = Long.valueOf(b);
    localContentValues.put("timestamp", (Long)localObject);
    localObject = e;
    localContentValues.put("type", (String)localObject);
    localObject = f;
    localContentValues.put("history", (String)localObject);
    return localContentValues;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof g;
      if (bool2)
      {
        paramObject = (g)paramObject;
        Object localObject1 = a;
        Object localObject2 = a;
        bool2 = k.a(localObject1, localObject2);
        if (bool2)
        {
          long l1 = b;
          long l2 = b;
          bool2 = l1 < l2;
          if (!bool2)
          {
            bool2 = true;
          }
          else
          {
            bool2 = false;
            localObject1 = null;
          }
          if (bool2)
          {
            localObject1 = c;
            localObject2 = c;
            bool2 = k.a(localObject1, localObject2);
            if (bool2)
            {
              localObject1 = d;
              localObject2 = d;
              bool2 = k.a(localObject1, localObject2);
              if (bool2)
              {
                localObject1 = e;
                localObject2 = e;
                bool2 = k.a(localObject1, localObject2);
                if (bool2)
                {
                  localObject1 = f;
                  paramObject = f;
                  boolean bool3 = k.a(localObject1, paramObject);
                  if (bool3) {
                    return bool1;
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    String str = a;
    int i = 0;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    j *= 31;
    long l1 = b;
    int k = 32;
    long l2 = l1 >>> k;
    l1 ^= l2;
    int m = (int)l1;
    int j = (j + m) * 31;
    Object localObject = c;
    int n;
    if (localObject != null)
    {
      n = localObject.hashCode();
    }
    else
    {
      n = 0;
      localObject = null;
    }
    j = (j + n) * 31;
    localObject = d;
    if (localObject != null)
    {
      n = localObject.hashCode();
    }
    else
    {
      n = 0;
      localObject = null;
    }
    j = (j + n) * 31;
    localObject = e;
    if (localObject != null)
    {
      n = localObject.hashCode();
    }
    else
    {
      n = 0;
      localObject = null;
    }
    j = (j + n) * 31;
    localObject = f;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("FlashInfo(phone=");
    Object localObject = a;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", timestamp=");
    long l = b;
    localStringBuilder.append(l);
    localStringBuilder.append(", imageUri=");
    localObject = c;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", displayName=");
    localObject = d;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", type=");
    localObject = e;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", history=");
    localObject = f;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.db.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */