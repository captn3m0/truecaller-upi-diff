package com.truecaller.flashsdk.db;

import c.d.c;
import c.g.a.m;
import c.x;
import kotlinx.coroutines.ag;

final class i$c
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  i$c(i parami, g paramg, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    c localc = new com/truecaller/flashsdk/db/i$c;
    i locali = b;
    g localg = c;
    localc.<init>(locali, localg, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return localc;
  }
  
  /* Error */
  public final Object a(Object paramObject)
  {
    // Byte code:
    //   0: getstatic 42	c/d/a/a:a	Lc/d/a/a;
    //   3: astore_2
    //   4: aload_0
    //   5: getfield 44	com/truecaller/flashsdk/db/i$c:a	I
    //   8: istore_3
    //   9: iload_3
    //   10: ifne +309 -> 319
    //   13: aload_1
    //   14: instanceof 46
    //   17: istore_3
    //   18: iload_3
    //   19: ifne +292 -> 311
    //   22: aload_0
    //   23: getfield 16	com/truecaller/flashsdk/db/i$c:b	Lcom/truecaller/flashsdk/db/i;
    //   26: astore_1
    //   27: aload_0
    //   28: getfield 18	com/truecaller/flashsdk/db/i$c:c	Lcom/truecaller/flashsdk/db/g;
    //   31: astore_2
    //   32: iconst_1
    //   33: istore 4
    //   35: iload 4
    //   37: anewarray 49	java/lang/String
    //   40: astore 5
    //   42: iconst_0
    //   43: istore 6
    //   45: aconst_null
    //   46: astore 7
    //   48: aload_2
    //   49: getfield 54	com/truecaller/flashsdk/db/g:a	Ljava/lang/String;
    //   52: astore 8
    //   54: aload 5
    //   56: iconst_0
    //   57: aload 8
    //   59: aastore
    //   60: aconst_null
    //   61: astore 9
    //   63: aload_1
    //   64: getfield 59	com/truecaller/flashsdk/db/i:d	Landroid/content/Context;
    //   67: astore 7
    //   69: aload 7
    //   71: invokevirtual 65	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   74: astore 7
    //   76: getstatic 70	com/truecaller/flashsdk/db/j:b	Landroid/net/Uri;
    //   79: astore 8
    //   81: getstatic 75	com/truecaller/flashsdk/db/a$a:a	[Ljava/lang/String;
    //   84: astore 10
    //   86: getstatic 77	com/truecaller/flashsdk/db/a$a:b	Ljava/lang/String;
    //   89: astore 11
    //   91: aload 7
    //   93: aload 8
    //   95: aload 10
    //   97: aload 11
    //   99: aload 5
    //   101: aconst_null
    //   102: invokevirtual 83	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   105: astore 9
    //   107: aload 9
    //   109: ifnull +110 -> 219
    //   112: aload 9
    //   114: invokeinterface 89 1 0
    //   119: istore 6
    //   121: iload 6
    //   123: ifle +96 -> 219
    //   126: aload_2
    //   127: getfield 92	com/truecaller/flashsdk/db/g:b	J
    //   130: lstore 12
    //   132: lconst_0
    //   133: lstore 14
    //   135: lload 12
    //   137: lload 14
    //   139: lcmp
    //   140: istore 16
    //   142: iload 16
    //   144: ifne +35 -> 179
    //   147: aload_1
    //   148: getfield 59	com/truecaller/flashsdk/db/i:d	Landroid/content/Context;
    //   151: astore_1
    //   152: aload_1
    //   153: invokevirtual 65	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   156: astore_1
    //   157: getstatic 70	com/truecaller/flashsdk/db/j:b	Landroid/net/Uri;
    //   160: astore_2
    //   161: getstatic 77	com/truecaller/flashsdk/db/a$a:b	Ljava/lang/String;
    //   164: astore 7
    //   166: aload_1
    //   167: aload_2
    //   168: aload 7
    //   170: aload 5
    //   172: invokevirtual 96	android/content/ContentResolver:delete	(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    //   175: pop
    //   176: goto +71 -> 247
    //   179: aload_1
    //   180: getfield 59	com/truecaller/flashsdk/db/i:d	Landroid/content/Context;
    //   183: astore_1
    //   184: aload_1
    //   185: invokevirtual 65	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   188: astore_1
    //   189: getstatic 70	com/truecaller/flashsdk/db/j:b	Landroid/net/Uri;
    //   192: astore 7
    //   194: aload_2
    //   195: invokevirtual 99	com/truecaller/flashsdk/db/g:a	()Landroid/content/ContentValues;
    //   198: astore_2
    //   199: getstatic 77	com/truecaller/flashsdk/db/a$a:b	Ljava/lang/String;
    //   202: astore 8
    //   204: aload_1
    //   205: aload 7
    //   207: aload_2
    //   208: aload 8
    //   210: aload 5
    //   212: invokevirtual 103	android/content/ContentResolver:update	(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    //   215: pop
    //   216: goto +31 -> 247
    //   219: aload_1
    //   220: getfield 59	com/truecaller/flashsdk/db/i:d	Landroid/content/Context;
    //   223: astore_1
    //   224: aload_1
    //   225: invokevirtual 65	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   228: astore_1
    //   229: getstatic 70	com/truecaller/flashsdk/db/j:b	Landroid/net/Uri;
    //   232: astore 5
    //   234: aload_2
    //   235: invokevirtual 99	com/truecaller/flashsdk/db/g:a	()Landroid/content/ContentValues;
    //   238: astore_2
    //   239: aload_1
    //   240: aload 5
    //   242: aload_2
    //   243: invokevirtual 107	android/content/ContentResolver:insert	(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    //   246: pop
    //   247: aload 9
    //   249: ifnull +44 -> 293
    //   252: goto +34 -> 286
    //   255: astore_1
    //   256: goto +41 -> 297
    //   259: pop
    //   260: new 109	com/truecaller/log/UnmutedException$d
    //   263: astore_1
    //   264: ldc 111
    //   266: astore_2
    //   267: aload_1
    //   268: aload_2
    //   269: invokespecial 114	com/truecaller/log/UnmutedException$d:<init>	(Ljava/lang/String;)V
    //   272: aload_1
    //   273: checkcast 116	java/lang/Throwable
    //   276: astore_1
    //   277: aload_1
    //   278: invokestatic 121	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   281: aload 9
    //   283: ifnull +10 -> 293
    //   286: aload 9
    //   288: invokeinterface 125 1 0
    //   293: getstatic 130	c/x:a	Lc/x;
    //   296: areturn
    //   297: aload 9
    //   299: ifnull +10 -> 309
    //   302: aload 9
    //   304: invokeinterface 125 1 0
    //   309: aload_1
    //   310: athrow
    //   311: aload_1
    //   312: checkcast 46	c/o$b
    //   315: getfield 133	c/o$b:a	Ljava/lang/Throwable;
    //   318: athrow
    //   319: new 135	java/lang/IllegalStateException
    //   322: astore_1
    //   323: aload_1
    //   324: ldc -119
    //   326: invokespecial 138	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
    //   329: aload_1
    //   330: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	331	0	this	c
    //   0	331	1	paramObject	Object
    //   3	266	2	localObject1	Object
    //   8	2	3	i	int
    //   17	2	3	bool1	boolean
    //   33	3	4	j	int
    //   40	201	5	localObject2	Object
    //   43	79	6	k	int
    //   46	160	7	localObject3	Object
    //   52	157	8	localObject4	Object
    //   61	242	9	localCursor	android.database.Cursor
    //   84	12	10	arrayOfString	String[]
    //   89	9	11	str	String
    //   130	6	12	l1	long
    //   133	5	14	l2	long
    //   140	3	16	bool2	boolean
    //   259	1	16	localException	Exception
    // Exception table:
    //   from	to	target	type
    //   63	67	255	finally
    //   69	74	255	finally
    //   76	79	255	finally
    //   81	84	255	finally
    //   86	89	255	finally
    //   101	105	255	finally
    //   112	119	255	finally
    //   126	130	255	finally
    //   147	151	255	finally
    //   152	156	255	finally
    //   157	160	255	finally
    //   161	164	255	finally
    //   170	176	255	finally
    //   179	183	255	finally
    //   184	188	255	finally
    //   189	192	255	finally
    //   194	198	255	finally
    //   199	202	255	finally
    //   210	216	255	finally
    //   219	223	255	finally
    //   224	228	255	finally
    //   229	232	255	finally
    //   234	238	255	finally
    //   242	247	255	finally
    //   260	263	255	finally
    //   268	272	255	finally
    //   272	276	255	finally
    //   277	281	255	finally
    //   63	67	259	java/lang/Exception
    //   69	74	259	java/lang/Exception
    //   76	79	259	java/lang/Exception
    //   81	84	259	java/lang/Exception
    //   86	89	259	java/lang/Exception
    //   101	105	259	java/lang/Exception
    //   112	119	259	java/lang/Exception
    //   126	130	259	java/lang/Exception
    //   147	151	259	java/lang/Exception
    //   152	156	259	java/lang/Exception
    //   157	160	259	java/lang/Exception
    //   161	164	259	java/lang/Exception
    //   170	176	259	java/lang/Exception
    //   179	183	259	java/lang/Exception
    //   184	188	259	java/lang/Exception
    //   189	192	259	java/lang/Exception
    //   194	198	259	java/lang/Exception
    //   199	202	259	java/lang/Exception
    //   210	216	259	java/lang/Exception
    //   219	223	259	java/lang/Exception
    //   224	228	259	java/lang/Exception
    //   229	232	259	java/lang/Exception
    //   234	238	259	java/lang/Exception
    //   242	247	259	java/lang/Exception
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (c)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((c)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.db.i.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */