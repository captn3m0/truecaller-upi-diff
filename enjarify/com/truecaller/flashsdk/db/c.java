package com.truecaller.flashsdk.db;

import android.content.ContentValues;
import android.database.Cursor;

public abstract interface c
{
  public abstract int a(String paramString, ContentValues paramContentValues);
  
  public abstract long a(ContentValues paramContentValues);
  
  public abstract Cursor a();
  
  public abstract Cursor a(String paramString);
  
  public abstract Cursor a(String[] paramArrayOfString);
  
  public abstract int b(String paramString);
  
  public abstract int b(String paramString, ContentValues paramContentValues);
  
  public abstract int b(String[] paramArrayOfString);
  
  public abstract long b(ContentValues paramContentValues);
  
  public abstract Cursor b();
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.db.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */