package com.truecaller.flashsdk.db;

import android.database.Cursor;

public abstract interface b
{
  public abstract int a(int paramInt1, int paramInt2, String paramString);
  
  public abstract int a(long paramLong, String paramString1, String paramString2, String paramString3);
  
  public abstract long a(f paramf);
  
  public abstract long a(k paramk);
  
  public abstract Cursor a();
  
  public abstract Cursor a(String paramString);
  
  public abstract Cursor a(String[] paramArrayOfString);
  
  public abstract int b(String[] paramArrayOfString);
  
  public abstract Cursor b(String paramString);
  
  public abstract int c(String paramString);
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.db.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */