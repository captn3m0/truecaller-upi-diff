package com.truecaller.flashsdk.db;

import c.a.aa;
import c.d.a.a;
import c.d.c;
import c.o.b;
import c.x;
import com.truecaller.flashsdk.models.e;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlinx.coroutines.ag;

final class i$d$1
  extends c.d.b.a.k
  implements c.g.a.m
{
  int a;
  private ag e;
  
  i$d$1(i.d paramd, Collection paramCollection, List paramList, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    1 local1 = new com/truecaller/flashsdk/db/i$d$1;
    i.d locald = b;
    Collection localCollection = c;
    List localList = d;
    local1.<init>(locald, localCollection, localList, paramc);
    paramObject = (ag)paramObject;
    e = ((ag)paramObject);
    return local1;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        paramObject = (Iterable)c;
        localObject1 = new java/util/ArrayList;
        ((ArrayList)localObject1).<init>();
        localObject1 = (Collection)localObject1;
        paramObject = ((Iterable)paramObject).iterator();
        boolean bool2;
        Object localObject2;
        Map localMap;
        for (;;)
        {
          bool2 = ((Iterator)paramObject).hasNext();
          if (!bool2) {
            break;
          }
          localObject2 = (String)((Iterator)paramObject).next();
          localMap = b.i.c;
          localObject2 = (Set)localMap.get(localObject2);
          if (localObject2 == null) {
            localObject2 = (Set)aa.a;
          }
          localObject2 = (Iterable)localObject2;
          c.a.m.a((Collection)localObject1, (Iterable)localObject2);
        }
        localObject1 = (Iterable)localObject1;
        paramObject = ((Iterable)c.a.m.k((Iterable)localObject1)).iterator();
        for (;;)
        {
          bool1 = ((Iterator)paramObject).hasNext();
          if (!bool1) {
            break;
          }
          localObject1 = (l)((Iterator)paramObject).next();
          ((l)localObject1).a();
        }
        paramObject = (Iterable)d;
        localObject1 = new java/util/ArrayList;
        ((ArrayList)localObject1).<init>();
        localObject1 = (Collection)localObject1;
        paramObject = ((Iterable)paramObject).iterator();
        for (;;)
        {
          bool2 = ((Iterator)paramObject).hasNext();
          if (!bool2) {
            break;
          }
          localObject2 = nexta;
          if (localObject2 != null) {
            ((Collection)localObject1).add(localObject2);
          }
        }
        localObject1 = (Iterable)localObject1;
        paramObject = new java/util/ArrayList;
        ((ArrayList)paramObject).<init>();
        paramObject = (Collection)paramObject;
        localObject1 = ((Iterable)localObject1).iterator();
        for (;;)
        {
          bool2 = ((Iterator)localObject1).hasNext();
          if (!bool2) {
            break;
          }
          localObject2 = (String)((Iterator)localObject1).next();
          localMap = b.i.c;
          localObject2 = (Set)localMap.get(localObject2);
          if (localObject2 == null) {
            localObject2 = (Set)aa.a;
          }
          localObject2 = (Iterable)localObject2;
          c.a.m.a((Collection)paramObject, (Iterable)localObject2);
        }
        paramObject = ((Iterable)c.a.m.k((Iterable)paramObject)).iterator();
        for (;;)
        {
          bool1 = ((Iterator)paramObject).hasNext();
          if (!bool1) {
            break;
          }
          localObject1 = (l)((Iterator)paramObject).next();
          ((l)localObject1).a();
        }
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (1)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((1)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.db.i.d.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */