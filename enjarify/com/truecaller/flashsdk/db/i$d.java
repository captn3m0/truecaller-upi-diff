package com.truecaller.flashsdk.db;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import c.d.a.a;
import c.d.c;
import c.d.f;
import c.o.b;
import c.x;
import com.truecaller.flashsdk.models.e;
import com.truecaller.log.UnmutedException.d;
import com.truecaller.log.d;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlinx.coroutines.ag;

final class i$d
  extends c.d.b.a.k
  implements c.g.a.m
{
  Object a;
  Object b;
  Object c;
  Object d;
  Object e;
  Object f;
  Object g;
  int h;
  private ag k;
  
  i$d(i parami, List paramList, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    d locald = new com/truecaller/flashsdk/db/i$d;
    i locali = i;
    List localList = j;
    locald.<init>(locali, localList, paramc);
    paramObject = (ag)paramObject;
    k = ((ag)paramObject);
    return locald;
  }
  
  public final Object a(Object paramObject)
  {
    d locald = this;
    Object localObject1 = paramObject;
    a locala = a.a;
    int m = h;
    switch (m)
    {
    default: 
      localObject1 = new java/lang/IllegalStateException;
      ((IllegalStateException)localObject1).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)localObject1);
    case 1: 
      boolean bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label1072;
      }
      localObject1 = (Iterable)j;
      Object localObject2 = new java/util/ArrayList;
      ((ArrayList)localObject2).<init>();
      localObject2 = (Collection)localObject2;
      localObject1 = ((Iterable)localObject1).iterator();
      boolean bool4;
      for (;;)
      {
        boolean bool3 = ((Iterator)localObject1).hasNext();
        if (!bool3) {
          break;
        }
        localObject3 = ((Iterator)localObject1).next();
        localObject4 = localObject3;
        localObject4 = (e)localObject3;
        bool4 = c;
        if (bool4) {
          ((Collection)localObject2).add(localObject3);
        }
      }
      localObject2 = (List)localObject2;
      localObject1 = (Iterable)j;
      Object localObject3 = new java/util/ArrayList;
      ((ArrayList)localObject3).<init>();
      localObject3 = (Collection)localObject3;
      localObject1 = ((Iterable)localObject1).iterator();
      boolean bool5;
      for (;;)
      {
        bool4 = ((Iterator)localObject1).hasNext();
        bool5 = true;
        if (!bool4) {
          break;
        }
        localObject4 = ((Iterator)localObject1).next();
        localObject5 = localObject4;
        localObject5 = (e)localObject4;
        boolean bool6 = c;
        bool5 ^= bool6;
        if (bool5) {
          ((Collection)localObject3).add(localObject4);
        }
      }
      localObject3 = (List)localObject3;
      localObject1 = i.b((List)localObject3);
      i.a(i, (List)localObject3);
      Object localObject4 = i.b((List)localObject2);
      Object localObject5 = localObject2;
      localObject5 = (Iterable)localObject2;
      Object localObject6 = new java/util/ArrayList;
      ((ArrayList)localObject6).<init>();
      localObject6 = (Collection)localObject6;
      Object localObject7 = ((Iterable)localObject5).iterator();
      boolean bool7;
      Object localObject10;
      for (;;)
      {
        bool7 = ((Iterator)localObject7).hasNext();
        if (!bool7) {
          break;
        }
        localObject8 = ((Iterator)localObject7).next();
        localObject9 = localObject8;
        localObject9 = (e)localObject8;
        localObject10 = localObject4;
        localObject10 = (Iterable)localObject4;
        localObject9 = a;
        bool8 = c.a.m.a((Iterable)localObject10, localObject9);
        if (bool8) {
          ((Collection)localObject6).add(localObject8);
        }
      }
      localObject6 = (List)localObject6;
      localObject7 = new java/util/ArrayList;
      ((ArrayList)localObject7).<init>();
      localObject7 = (Collection)localObject7;
      localObject5 = ((Iterable)localObject5).iterator();
      for (;;)
      {
        bool7 = ((Iterator)localObject5).hasNext();
        if (!bool7) {
          break;
        }
        localObject8 = ((Iterator)localObject5).next();
        localObject9 = localObject8;
        localObject9 = (e)localObject8;
        localObject10 = localObject4;
        localObject10 = (Iterable)localObject4;
        localObject9 = a;
        bool8 = c.a.m.a((Iterable)localObject10, localObject9) ^ bool5;
        if (bool8) {
          ((Collection)localObject7).add(localObject8);
        }
      }
      localObject7 = (List)localObject7;
      localObject5 = new java/util/ArrayList;
      ((ArrayList)localObject5).<init>();
      Object localObject8 = localObject7;
      localObject8 = ((Iterable)localObject7).iterator();
      Object localObject11;
      for (;;)
      {
        bool8 = ((Iterator)localObject8).hasNext();
        if (!bool8) {
          break;
        }
        localObject9 = (e)((Iterator)localObject8).next();
        localObject10 = a;
        if (localObject10 != null)
        {
          localObject11 = i.b;
          ((android.support.v4.f.g)localObject11).put(localObject10, localObject9);
        }
        localObject10 = ContentProviderOperation.newInsert(j.c);
        localObject9 = ((e)localObject9).a();
        localObject9 = ((ContentProviderOperation.Builder)localObject10).withValues((ContentValues)localObject9).build();
        ((ArrayList)localObject5).add(localObject9);
      }
      localObject8 = localObject6;
      localObject8 = ((Iterable)localObject6).iterator();
      for (;;)
      {
        bool8 = ((Iterator)localObject8).hasNext();
        if (!bool8) {
          break;
        }
        localObject9 = (e)((Iterator)localObject8).next();
        localObject10 = a;
        if (localObject10 != null)
        {
          localObject11 = i.b;
          ((android.support.v4.f.g)localObject11).put(localObject10, localObject9);
        }
        localObject10 = ContentProviderOperation.newUpdate(j.c);
        localObject11 = a.b.b;
        String[] arrayOfString = new String[bool5];
        localObject12 = a;
        arrayOfString[0] = localObject12;
        localObject12 = ((ContentProviderOperation.Builder)localObject10).withSelection((String)localObject11, arrayOfString);
        localObject9 = ((e)localObject9).a();
        localObject12 = ((ContentProviderOperation.Builder)localObject12).withValues((ContentValues)localObject9).build();
        ((ArrayList)localObject5).add(localObject12);
        bool5 = true;
      }
      Object localObject12 = localObject3;
      localObject12 = ((Iterable)localObject3).iterator();
      for (;;)
      {
        bool7 = ((Iterator)localObject12).hasNext();
        if (!bool7) {
          break;
        }
        localObject8 = nexta;
        if (localObject8 != null)
        {
          localObject9 = i.b;
          ((android.support.v4.f.g)localObject9).remove(localObject8);
        }
      }
      try
      {
        localObject12 = i;
        localObject12 = d;
        localObject12 = ((Context)localObject12).getContentResolver();
        localObject8 = j.a;
        ((ContentResolver)localObject12).applyBatch((String)localObject8, (ArrayList)localObject5);
      }
      catch (Exception localException)
      {
        localObject12 = new com/truecaller/log/UnmutedException$d;
        localObject8 = "Failed to Query in Flash";
        ((UnmutedException.d)localObject12).<init>((String)localObject8);
        localObject12 = (Throwable)localObject12;
        d.a((Throwable)localObject12);
      }
      localObject12 = i.e;
      localObject8 = new com/truecaller/flashsdk/db/i$d$1;
      boolean bool8 = false;
      Object localObject9 = null;
      ((i.d.1)localObject8).<init>(locald, (Collection)localObject1, (List)localObject7, null);
      localObject8 = (c.g.a.m)localObject8;
      a = localObject2;
      b = localObject3;
      c = localObject1;
      d = localObject4;
      e = localObject6;
      f = localObject7;
      g = localObject5;
      int n = 1;
      h = n;
      localObject1 = kotlinx.coroutines.g.a((f)localObject12, (c.g.a.m)localObject8, locald);
      if (localObject1 == locala) {
        return locala;
      }
      break;
    }
    return x.a;
    label1072:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (d)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((d)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.db.i.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */