package com.truecaller.flashsdk.receiver;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import c.g.b.k;
import c.u;
import com.truecaller.flashsdk.core.b;
import com.truecaller.flashsdk.core.c;

public final class PhoneStateReceiver
  extends BroadcastReceiver
{
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    k.b(paramContext, "context");
    k.b(paramIntent, "intent");
    Object localObject1 = paramIntent.getAction();
    String str1 = "android.intent.action.NEW_OUTGOING_CALL";
    boolean bool1 = k.a(localObject1, str1);
    if (bool1)
    {
      paramIntent = paramIntent.getExtras();
      localObject1 = "android.intent.extra.PHONE_NUMBER";
      paramIntent = paramIntent.getString((String)localObject1);
    }
    else
    {
      paramIntent = paramIntent.getExtras();
      localObject1 = "incoming_number";
      paramIntent = paramIntent.getString((String)localObject1);
    }
    localObject1 = paramIntent;
    localObject1 = (CharSequence)paramIntent;
    int j = 1;
    if (localObject1 != null)
    {
      i = ((CharSequence)localObject1).length();
      if (i != 0)
      {
        i = 0;
        localObject1 = null;
        break label106;
      }
    }
    int i = 1;
    label106:
    if (i == 0)
    {
      localObject1 = c.a();
      long l1 = ((b)localObject1).e(paramIntent);
      Object localObject2 = String.valueOf(l1);
      long l2 = -1;
      boolean bool2 = l1 < l2;
      if (bool2)
      {
        int m = ((String)localObject2).length();
        int n = 7;
        if (m > n)
        {
          m = paramIntent.length();
          if (m > n)
          {
            m = paramIntent.length() - n;
            int k = paramIntent.length();
            if (paramIntent != null)
            {
              paramIntent = paramIntent.substring(m, k);
              String str2 = "(this as java.lang.Strin…ing(startIndex, endIndex)";
              k.a(paramIntent, str2);
              m = ((String)localObject2).length() - n;
              n = ((String)localObject2).length();
              if (localObject2 != null)
              {
                localObject2 = ((String)localObject2).substring(m, n);
                str2 = "(this as java.lang.Strin…ing(startIndex, endIndex)";
                k.a(localObject2, str2);
                paramIntent = (CharSequence)paramIntent;
                localObject2 = (CharSequence)localObject2;
                boolean bool3 = TextUtils.equals(paramIntent, (CharSequence)localObject2);
                if (bool3)
                {
                  paramIntent = paramContext.getSystemService("notification");
                  if (paramIntent != null)
                  {
                    paramIntent = (NotificationManager)paramIntent;
                    long l3 = l1 % 1000000000L;
                    m = (int)l3;
                    paramIntent.cancel(m);
                    paramIntent = paramContext.getPackageManager();
                    localObject2 = new android/content/ComponentName;
                    str2 = "com.truecaller.flashsdk.receiver.PhoneStateReceiver";
                    ((ComponentName)localObject2).<init>(paramContext, str2);
                    int i1 = 2;
                    paramIntent.setComponentEnabledSetting((ComponentName)localObject2, i1, j);
                    ((b)localObject1).b(l1);
                  }
                  else
                  {
                    paramContext = new c/u;
                    paramContext.<init>("null cannot be cast to non-null type android.app.NotificationManager");
                    throw paramContext;
                  }
                }
              }
              else
              {
                paramContext = new c/u;
                paramContext.<init>("null cannot be cast to non-null type java.lang.String");
                throw paramContext;
              }
            }
            else
            {
              paramContext = new c/u;
              paramContext.<init>("null cannot be cast to non-null type java.lang.String");
              throw paramContext;
            }
          }
        }
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.receiver.PhoneStateReceiver
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */