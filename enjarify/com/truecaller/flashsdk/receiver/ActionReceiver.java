package com.truecaller.flashsdk.receiver;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.a;
import android.support.v4.app.ac;
import android.support.v4.content.d;
import android.text.TextUtils;
import c.g.b.k;
import c.g.b.z;
import c.u;
import com.truecaller.flashsdk.R.id;
import com.truecaller.flashsdk.R.string;
import com.truecaller.flashsdk.core.b;
import com.truecaller.flashsdk.core.i;
import com.truecaller.flashsdk.models.Flash;
import com.truecaller.flashsdk.models.Payload;
import com.truecaller.flashsdk.models.Sender;
import java.util.Arrays;

public final class ActionReceiver
  extends BroadcastReceiver
{
  private final Intent a;
  private final b b;
  private final i c;
  
  public ActionReceiver()
  {
    Object localObject = new android/content/Intent;
    ((Intent)localObject).<init>("response_sent");
    a = ((Intent)localObject);
    localObject = com.truecaller.flashsdk.core.c.a();
    b = ((b)localObject);
    localObject = b.e();
    c = ((i)localObject);
  }
  
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    ActionReceiver localActionReceiver = this;
    Context localContext = paramContext;
    Intent localIntent = paramIntent;
    k.b(paramContext, "context");
    k.b(paramIntent, "intent");
    Object localObject1 = "flash";
    boolean bool1 = paramIntent.hasExtra((String)localObject1);
    if (bool1)
    {
      localObject1 = paramIntent.getParcelableExtra("flash");
      Object localObject2 = localObject1;
      localObject2 = (Flash)localObject1;
      localObject1 = "bundleFlash";
      k.a(localObject2, (String)localObject1);
      bool1 = ((Flash)localObject2).k();
      if (!bool1) {
        return;
      }
      Flash localFlash = new com/truecaller/flashsdk/models/Flash;
      localFlash.<init>();
      localObject1 = ((Flash)localObject2).a();
      k.a(localObject1, "incomingFlash.sender");
      localObject1 = ((Sender)localObject1).a();
      k.a(localObject1, "incomingFlash.sender.phone");
      long l1 = ((Long)localObject1).longValue();
      localFlash.a(l1);
      localFlash.i();
      localFlash.j();
      localObject1 = "com.truecaller.flashsdk.receiver.ACTION_DISMISS";
      Object localObject3 = paramIntent.getAction();
      bool1 = k.a(localObject1, localObject3);
      int k = 2;
      int m = 1;
      boolean bool4 = false;
      localObject3 = null;
      Object localObject4;
      int i1;
      Object localObject5;
      int i2;
      if (bool1)
      {
        localObject1 = new com/truecaller/flashsdk/models/Payload;
        localObject4 = "busy";
        i1 = R.string.is_busy;
        localObject5 = paramContext.getString(i1);
        ((Payload)localObject1).<init>((String)localObject4, (String)localObject5, null, null);
        localObject3 = "final";
        localFlash.a((String)localObject3);
      }
      else
      {
        localObject1 = "com.truecaller.flashsdk.receiver.ACTION_DISMISS_SILENTLY";
        localObject4 = paramIntent.getAction();
        bool1 = k.a(localObject1, localObject4);
        if (bool1)
        {
          localObject1 = ac.a(paramContext);
          i2 = R.id.call_me_back_notification_id;
          ((ac)localObject1).a(i2);
          localObject1 = new com/truecaller/flashsdk/models/Payload;
          i1 = R.string.is_busy;
          localObject5 = paramContext.getString(i1);
          ((Payload)localObject1).<init>("busy", (String)localObject5, null, null);
          localFlash.a("final");
          localObject4 = c;
          if (localObject4 != null) {
            ((i)localObject4).a(m, null, null);
          }
        }
        else
        {
          localObject1 = "com.truecaller.flashsdk.ACTION_FLASH";
          localObject4 = paramIntent.getAction();
          bool1 = k.a(localObject1, localObject4);
          Object localObject6;
          Object localObject7;
          long l2;
          Object localObject8;
          if (bool1)
          {
            localObject1 = ac.a(paramContext);
            i2 = R.id.call_me_back_notification_id;
            ((ac)localObject1).a(i2);
            localObject6 = new com/truecaller/flashsdk/models/Payload;
            i2 = R.string.is_busy;
            localObject4 = paramContext.getString(i2);
            ((Payload)localObject6).<init>("busy", (String)localObject4, null, null);
            localFlash.a("final");
            localObject1 = c;
            if (localObject1 != null) {
              ((i)localObject1).a(k, null, null);
            }
            localObject7 = localIntent.getStringExtra("name");
            localObject1 = b;
            localObject3 = ((Flash)localObject2).a();
            k.a(localObject3, "incomingFlash.sender");
            localObject3 = ((Sender)localObject3).a();
            localObject4 = "incomingFlash.sender.phone";
            k.a(localObject3, (String)localObject4);
            l2 = ((Long)localObject3).longValue();
            localObject8 = "notification";
            localObject3 = paramContext;
            ((b)localObject1).a(paramContext, l2, (String)localObject7, (String)localObject8);
            localObject1 = localObject6;
          }
          else
          {
            localObject1 = "com.truecaller.flashsdk.receiver.ACTION_CALL_PHONE";
            localObject4 = paramIntent.getAction();
            bool1 = k.a(localObject1, localObject4);
            i2 = 268435456;
            long l3;
            int i3;
            String str1;
            if (bool1)
            {
              localObject1 = "android.permission.CALL_PHONE";
              int i = a.a(paramContext, (String)localObject1);
              if (i != 0)
              {
                localObject1 = new android/content/Intent;
                localObject4 = "android.intent.action.VIEW";
                i1 = R.string.tel_num;
                localObject7 = new Object[m];
                localObject8 = ((Flash)localObject2).a();
                localObject6 = "incomingFlash.sender";
                k.a(localObject8, (String)localObject6);
                l3 = ((Sender)localObject8).a().longValue();
                localObject8 = String.valueOf(l3);
                localObject7[0] = localObject8;
                localObject5 = Uri.parse(paramContext.getString(i1, (Object[])localObject7));
                ((Intent)localObject1).<init>((String)localObject4, (Uri)localObject5);
                paramContext.startActivity((Intent)localObject1);
              }
              else
              {
                localObject1 = new android/content/Intent;
                localObject5 = "android.intent.action.CALL";
                i3 = R.string.tel_num;
                localObject8 = new Object[m];
                localObject6 = ((Flash)localObject2).a();
                str1 = "incomingFlash.sender";
                k.a(localObject6, str1);
                l3 = ((Sender)localObject6).a().longValue();
                localObject6 = String.valueOf(l3);
                localObject8[0] = localObject6;
                localObject7 = Uri.parse(paramContext.getString(i3, (Object[])localObject8));
                ((Intent)localObject1).<init>((String)localObject5, (Uri)localObject7);
                ((Intent)localObject1).setFlags(i2);
                paramContext.startActivity((Intent)localObject1);
              }
              localObject1 = new com/truecaller/flashsdk/models/Payload;
              localObject4 = "call";
              i1 = R.string.calling_you_back;
              localObject5 = localContext.getString(i1);
              ((Payload)localObject1).<init>((String)localObject4, (String)localObject5, null, null);
              localObject3 = "final";
              localFlash.a((String)localObject3);
            }
            else
            {
              localObject1 = "com.truecaller.flashsdk.receiver.ACTION_CALL_PHONE_CALL_ME";
              localObject5 = paramIntent.getAction();
              boolean bool2 = k.a(localObject1, localObject5);
              if (bool2)
              {
                localObject1 = ac.a(paramContext);
                i1 = R.id.call_me_back_notification_id;
                ((ac)localObject1).a(i1);
                localObject1 = "android.permission.CALL_PHONE";
                int j = a.a(paramContext, (String)localObject1);
                if (j != 0)
                {
                  localObject1 = new android/content/Intent;
                  localObject4 = "android.intent.action.VIEW";
                  i1 = R.string.tel_num;
                  localObject7 = new Object[m];
                  localObject8 = ((Flash)localObject2).a();
                  localObject6 = "incomingFlash.sender";
                  k.a(localObject8, (String)localObject6);
                  l3 = ((Sender)localObject8).a().longValue();
                  localObject8 = String.valueOf(l3);
                  localObject7[0] = localObject8;
                  localObject5 = Uri.parse(paramContext.getString(i1, (Object[])localObject7));
                  ((Intent)localObject1).<init>((String)localObject4, (Uri)localObject5);
                  paramContext.startActivity((Intent)localObject1);
                }
                else
                {
                  localObject1 = new android/content/Intent;
                  localObject5 = "android.intent.action.CALL";
                  i3 = R.string.tel_num;
                  localObject8 = new Object[m];
                  localObject6 = ((Flash)localObject2).a();
                  str1 = "incomingFlash.sender";
                  k.a(localObject6, str1);
                  l3 = ((Sender)localObject6).a().longValue();
                  localObject6 = String.valueOf(l3);
                  localObject8[0] = localObject6;
                  localObject7 = Uri.parse(paramContext.getString(i3, (Object[])localObject8));
                  ((Intent)localObject1).<init>((String)localObject5, (Uri)localObject7);
                  ((Intent)localObject1).setFlags(i2);
                  paramContext.startActivity((Intent)localObject1);
                }
                localObject1 = c;
                if (localObject1 != null) {
                  ((i)localObject1).a(0, null, null);
                }
                localObject1 = new com/truecaller/flashsdk/models/Payload;
                localObject4 = "busy";
                i1 = R.string.is_busy;
                localObject5 = localContext.getString(i1);
                ((Payload)localObject1).<init>((String)localObject4, (String)localObject5, null, null);
                localObject3 = "final";
                localFlash.a((String)localObject3);
              }
              else
              {
                localObject1 = "com.truecaller.flashsdk.PAYMENT_REQUEST";
                localObject4 = paramIntent.getAction();
                boolean bool3 = k.a(localObject1, localObject4);
                if (bool3)
                {
                  localObject1 = paramIntent.getExtras();
                  str1 = ((Bundle)localObject1).getString("vpa", "");
                  String str2 = ((Bundle)localObject1).getString("amount", "");
                  String str3 = ((Bundle)localObject1).getString("comment", "");
                  String str4 = ((Bundle)localObject1).getString("name", "");
                  String str5 = ((Bundle)localObject1).getString("number", "");
                  localObject1 = ac.a(paramContext);
                  localObject4 = ((Flash)localObject2).a();
                  localObject5 = "incomingFlash.sender";
                  k.a(localObject4, (String)localObject5);
                  localObject4 = ((Sender)localObject4).a();
                  l2 = ((Long)localObject4).longValue();
                  long l4 = 1000000000L;
                  l2 %= l4;
                  i1 = (int)l2 + 101;
                  ((ac)localObject1).a(i1);
                  localObject6 = c;
                  if (localObject6 != null) {
                    ((i)localObject6).a(str1, str2, str5, str3, str4);
                  }
                  localObject1 = new com/truecaller/flashsdk/models/Payload;
                  localObject4 = "busy";
                  i1 = R.string.is_busy;
                  localObject5 = localContext.getString(i1);
                  ((Payload)localObject1).<init>((String)localObject4, (String)localObject5, null, null);
                  localObject3 = "final";
                  localFlash.a((String)localObject3);
                }
                else
                {
                  bool3 = false;
                  localObject1 = null;
                }
              }
            }
          }
        }
      }
      if (localObject1 != null)
      {
        localFlash.a((Payload)localObject1);
        localObject3 = (CharSequence)((Payload)localObject1).a();
        localObject4 = (CharSequence)"emoji";
        bool4 = TextUtils.equals((CharSequence)localObject3, (CharSequence)localObject4);
        if (bool4) {
          localObject1 = ((Payload)localObject1).b();
        } else {
          localObject1 = com.truecaller.flashsdk.assist.c.a(((Payload)localObject1).a());
        }
        localObject3 = (CharSequence)((Flash)localObject2).e();
        bool4 = TextUtils.isEmpty((CharSequence)localObject3);
        if (!bool4)
        {
          localObject3 = z.a;
          localObject4 = new Object[k];
          localObject5 = ((Flash)localObject2).e();
          localObject4[0] = localObject5;
          localObject4[m] = localObject1;
          localObject1 = Arrays.copyOf((Object[])localObject4, k);
          localObject1 = String.format("%s %s", (Object[])localObject1);
          localObject3 = "java.lang.String.format(format, *args)";
          k.a(localObject1, (String)localObject3);
        }
        localFlash.b((String)localObject1);
        localObject1 = b;
        ((b)localObject1).a(localFlash);
      }
      localObject1 = localContext.getSystemService("notification");
      if (localObject1 != null)
      {
        localObject1 = (NotificationManager)localObject1;
        i2 = -1;
        int n = localIntent.getIntExtra("notification_id", i2);
        ((NotificationManager)localObject1).cancel(n);
        localObject1 = new android/content/Intent;
        ((Intent)localObject1).<init>("android.intent.action.CLOSE_SYSTEM_DIALOGS");
        localContext.sendBroadcast((Intent)localObject1);
        a.putExtra("response_sent", m);
        localObject1 = d.a(paramContext);
        localObject3 = a;
        ((d)localObject1).a((Intent)localObject3);
      }
      else
      {
        localObject1 = new c/u;
        ((u)localObject1).<init>("null cannot be cast to non-null type android.app.NotificationManager");
        throw ((Throwable)localObject1);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.receiver.ActionReceiver
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */