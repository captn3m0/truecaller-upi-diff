package com.truecaller.flashsdk.c;

import android.graphics.Bitmap;
import com.truecaller.flashsdk.models.Flash;
import com.truecaller.flashsdk.models.ImageFlash;

public abstract interface a
{
  public abstract void a(Flash paramFlash);
  
  public abstract void a(Flash paramFlash, Bitmap paramBitmap);
  
  public abstract void a(ImageFlash paramImageFlash);
  
  public abstract void b(Flash paramFlash);
  
  public abstract void b(ImageFlash paramImageFlash);
  
  public abstract void c(Flash paramFlash);
  
  public abstract void c(ImageFlash paramImageFlash);
  
  public abstract void d(ImageFlash paramImageFlash);
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */