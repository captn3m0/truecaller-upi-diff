package com.truecaller.flashsdk.c;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Parcelable;
import android.support.v4.app.z.c;
import android.support.v4.app.z.d;
import android.support.v4.app.z.g;
import c.a.y;
import c.d.f;
import c.u;
import com.truecaller.flashsdk.R.color;
import com.truecaller.flashsdk.R.drawable;
import com.truecaller.flashsdk.R.id;
import com.truecaller.flashsdk.R.string;
import com.truecaller.flashsdk.assist.al;
import com.truecaller.flashsdk.assist.d;
import com.truecaller.flashsdk.assist.g;
import com.truecaller.flashsdk.core.FlashMediaService;
import com.truecaller.flashsdk.models.Contact;
import com.truecaller.flashsdk.models.Flash;
import com.truecaller.flashsdk.models.ImageFlash;
import com.truecaller.flashsdk.models.Payload;
import com.truecaller.flashsdk.models.Sender;
import com.truecaller.flashsdk.receiver.ActionReceiver;
import java.util.Collection;
import java.util.List;
import java.util.ListIterator;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;

public final class b
  implements a
{
  final Context a;
  final d b;
  final g c;
  final com.truecaller.notificationchannels.e d;
  private final f e;
  private final f f;
  private final al g;
  
  public b(f paramf, Context paramContext, d paramd, g paramg, al paramal, com.truecaller.notificationchannels.e parame)
  {
    f = paramf;
    a = paramContext;
    b = paramd;
    c = paramg;
    g = paramal;
    d = parame;
    paramf = f;
    e = paramf;
  }
  
  private final Bitmap b(String paramString)
  {
    if (paramString != null)
    {
      Object localObject = paramString;
      localObject = (CharSequence)paramString;
      boolean bool = c.n.m.a((CharSequence)localObject);
      if (!bool) {
        return g.a(paramString, true);
      }
    }
    paramString = a.getResources();
    int i = R.drawable.ic_notification_avatar;
    return BitmapFactory.decodeResource(paramString, i);
  }
  
  final z.d a(String paramString)
  {
    if (paramString == null)
    {
      paramString = new android/support/v4/app/z$d;
      localObject = a;
      paramString.<init>((Context)localObject);
      return paramString;
    }
    Object localObject = new android/support/v4/app/z$d;
    Context localContext = a;
    ((z.d)localObject).<init>(localContext, paramString);
    return (z.d)localObject;
  }
  
  public final void a(Flash paramFlash)
  {
    c.g.b.k.b(paramFlash, "flash");
    Object localObject1 = new android/content/Intent;
    ((Intent)localObject1).<init>("com.truecaller.flashsdk.PAYMENT_RECEIVED");
    Object localObject2 = a;
    int i = 120;
    localObject1 = PendingIntent.getBroadcast((Context)localObject2, i, (Intent)localObject1, 134217728);
    localObject2 = paramFlash.a();
    c.g.b.k.a(localObject2, "flash.sender");
    localObject2 = ((Sender)localObject2).b();
    Object localObject3 = paramFlash.a();
    c.g.b.k.a(localObject3, "flash.sender");
    localObject3 = ((Sender)localObject3).c();
    Object localObject4 = c;
    boolean bool2 = ((g)localObject4).f();
    int k = 1;
    Object localObject5;
    Object localObject6;
    long l1;
    if (bool2)
    {
      localObject4 = b;
      localObject5 = paramFlash.a();
      localObject6 = "flash.sender";
      c.g.b.k.a(localObject5, (String)localObject6);
      l1 = ((Sender)localObject5).a().longValue();
      localObject5 = String.valueOf(l1);
      localObject4 = ((d)localObject4).b((String)localObject5);
      boolean bool3;
      if (localObject4 != null)
      {
        localObject5 = (CharSequence)((Contact)localObject4).getImageUrl();
        if (localObject5 != null)
        {
          bool3 = c.n.m.a((CharSequence)localObject5);
          if (!bool3)
          {
            bool3 = false;
            localObject5 = null;
            break label192;
          }
        }
        bool3 = true;
        label192:
        if (!bool3) {
          localObject3 = ((Contact)localObject4).getImageUrl();
        }
      }
      if (localObject4 != null)
      {
        localObject5 = (CharSequence)((Contact)localObject4).getName();
        bool3 = c.n.m.a((CharSequence)localObject5);
        if (!bool3) {
          localObject2 = ((Contact)localObject4).getName();
        }
      }
    }
    localObject4 = localObject2;
    localObject4 = (CharSequence)localObject2;
    if (localObject4 != null)
    {
      bool2 = c.n.m.a((CharSequence)localObject4);
      if (!bool2)
      {
        bool2 = false;
        localObject4 = null;
        break label278;
      }
    }
    bool2 = true;
    label278:
    if (!bool2)
    {
      localObject4 = paramFlash.f();
      c.g.b.k.a(localObject4, "flash.payload");
      localObject4 = ((Payload)localObject4).d();
      localObject5 = d.j();
      localObject5 = a((String)localObject5);
      int n = R.drawable.ic_stat_flash;
      localObject5 = ((z.d)localObject5).a(n);
      localObject6 = a;
      int i1 = R.color.truecolor;
      n = android.support.v4.content.b.c((Context)localObject6, i1);
      localObject5 = ((z.d)localObject5).f(n);
      localObject6 = a;
      i1 = R.string.truecaller_pay;
      localObject6 = (CharSequence)((Context)localObject6).getString(i1);
      localObject5 = ((z.d)localObject5).a((CharSequence)localObject6).e();
      n = -65536;
      localObject5 = ((z.d)localObject5).a(n, k, k);
      localObject1 = ((z.d)localObject5).a((PendingIntent)localObject1);
      l1 = System.currentTimeMillis();
      localObject1 = ((z.d)localObject1).a(l1).a();
      localObject3 = b((String)localObject3);
      localObject1 = ((z.d)localObject1).a((Bitmap)localObject3);
      localObject3 = localObject4;
      localObject3 = (CharSequence)localObject4;
      if (localObject3 != null)
      {
        bool1 = c.n.m.a((CharSequence)localObject3);
        if (!bool1)
        {
          bool1 = false;
          localObject3 = null;
          break label497;
        }
      }
      boolean bool1 = true;
      label497:
      if (bool1)
      {
        localObject3 = a;
        int j = R.string.sent_you_money;
        Object[] arrayOfObject = new Object[k];
        arrayOfObject[0] = localObject2;
        localObject2 = (CharSequence)((Context)localObject3).getString(j, arrayOfObject);
        ((z.d)localObject1).b((CharSequence)localObject2);
      }
      else
      {
        localObject3 = a;
        int m = R.string.sent_you_amount;
        n = 2;
        localObject6 = new Object[n];
        localObject6[0] = localObject2;
        localObject6[k] = localObject4;
        localObject2 = (CharSequence)((Context)localObject3).getString(m, (Object[])localObject6);
        ((z.d)localObject1).b((CharSequence)localObject2);
      }
      localObject2 = a;
      localObject3 = "notification";
      localObject2 = ((Context)localObject2).getSystemService((String)localObject3);
      if (localObject2 != null)
      {
        localObject2 = (NotificationManager)localObject2;
        localObject1 = ((z.d)localObject1).h();
        paramFlash = paramFlash.a();
        localObject3 = "flash.sender";
        c.g.b.k.a(paramFlash, (String)localObject3);
        paramFlash = paramFlash.a();
        long l2 = paramFlash.longValue();
        long l3 = 1000000000L;
        l2 %= l3;
        int i2 = (int)l2 + 100;
        ((NotificationManager)localObject2).notify(i2, (Notification)localObject1);
      }
      else
      {
        paramFlash = new c/u;
        paramFlash.<init>("null cannot be cast to non-null type android.app.NotificationManager");
        throw paramFlash;
      }
    }
  }
  
  public final void a(Flash paramFlash, Bitmap paramBitmap)
  {
    c.g.b.k.b(paramFlash, "flash");
    ag localag = (ag)bg.a;
    f localf = e;
    Object localObject = new com/truecaller/flashsdk/c/b$a;
    ((b.a)localObject).<init>(this, paramFlash, paramBitmap, null);
    localObject = (c.g.a.m)localObject;
    kotlinx.coroutines.e.b(localag, localf, (c.g.a.m)localObject, 2);
  }
  
  public final void a(ImageFlash paramImageFlash)
  {
    c.g.b.k.b(paramImageFlash, "flash");
    Object localObject1 = new android/content/Intent;
    Object localObject2 = a;
    ((Intent)localObject1).<init>((Context)localObject2, FlashMediaService.class);
    ((Intent)localObject1).setAction("action_image_flash_retry");
    Object localObject3 = paramImageFlash;
    localObject3 = (Parcelable)paramImageFlash;
    ((Intent)localObject1).putExtra("extra_image_flash", (Parcelable)localObject3);
    localObject2 = a;
    int i = R.id.flash_image_uploading_notification_id;
    localObject1 = PendingIntent.getService((Context)localObject2, i, (Intent)localObject1, 134217728);
    localObject2 = g;
    i = R.string.flash_failed;
    Object localObject4 = new Object[0];
    localObject2 = ((al)localObject2).a(i, (Object[])localObject4);
    localObject3 = g;
    int j = R.string.tap_to_retry;
    Object[] arrayOfObject1 = new Object[0];
    localObject3 = ((al)localObject3).a(j, arrayOfObject1);
    localObject4 = d.h();
    localObject4 = a((String)localObject4);
    localObject2 = (CharSequence)localObject2;
    ((z.d)localObject4).a((CharSequence)localObject2);
    localObject3 = (CharSequence)localObject3;
    ((z.d)localObject4).b((CharSequence)localObject3);
    localObject2 = a;
    i = R.color.truecolor;
    int k = android.support.v4.content.b.c((Context)localObject2, i);
    ((z.d)localObject4).f(k);
    k = R.drawable.ic_flash;
    ((z.d)localObject4).a(k);
    ((z.d)localObject4).a((PendingIntent)localObject1);
    ((z.d)localObject4).e();
    long l1 = System.currentTimeMillis();
    ((z.d)localObject4).a(l1);
    ((z.d)localObject4).a();
    k = R.drawable.ic_notification_retry;
    localObject3 = g;
    int m = R.string.retry;
    Object[] arrayOfObject2 = new Object[0];
    localObject3 = (CharSequence)((al)localObject3).a(m, arrayOfObject2);
    ((z.d)localObject4).a(k, (CharSequence)localObject3, (PendingIntent)localObject1);
    localObject1 = a;
    localObject2 = "notification";
    localObject1 = (NotificationManager)((Context)localObject1).getSystemService((String)localObject2);
    if (localObject1 != null)
    {
      l1 = R.id.flash_image_uploading_notification_id;
      long l2 = paramImageFlash.b();
      int n = (int)(l1 + l2);
      localObject2 = ((z.d)localObject4).h();
      ((NotificationManager)localObject1).notify(n, (Notification)localObject2);
      return;
    }
  }
  
  public final void b(Flash paramFlash)
  {
    c.g.b.k.b(paramFlash, "flash");
    Object localObject1 = paramFlash.a();
    c.g.b.k.a(localObject1, "flash.sender");
    localObject1 = ((Sender)localObject1).b();
    Object localObject2 = paramFlash.a();
    c.g.b.k.a(localObject2, "flash.sender");
    localObject2 = ((Sender)localObject2).c();
    Object localObject3 = c;
    boolean bool1 = ((g)localObject3).f();
    int i = 1;
    if (bool1)
    {
      localObject3 = b;
      localObject4 = paramFlash.a();
      localObject5 = "flash.sender";
      c.g.b.k.a(localObject4, (String)localObject5);
      long l1 = ((Sender)localObject4).a().longValue();
      localObject4 = String.valueOf(l1);
      localObject3 = ((d)localObject3).b((String)localObject4);
      if (localObject3 != null)
      {
        localObject4 = (CharSequence)((Contact)localObject3).getImageUrl();
        if (localObject4 != null)
        {
          bool2 = c.n.m.a((CharSequence)localObject4);
          if (!bool2)
          {
            bool2 = false;
            localObject4 = null;
            break label159;
          }
        }
        bool2 = true;
        label159:
        if (!bool2) {
          localObject2 = ((Contact)localObject3).getImageUrl();
        }
      }
      if (localObject3 != null)
      {
        localObject4 = (CharSequence)((Contact)localObject3).getName();
        bool2 = c.n.m.a((CharSequence)localObject4);
        if (!bool2) {
          localObject1 = ((Contact)localObject3).getName();
        }
      }
    }
    localObject3 = paramFlash.f();
    c.g.b.k.a(localObject3, "flash.payload");
    localObject3 = ((Payload)localObject3).d();
    Object localObject4 = new android/content/Intent;
    boolean bool3 = false;
    Object localObject6 = a;
    Object localObject7 = ActionReceiver.class;
    ((Intent)localObject4).<init>("com.truecaller.flashsdk.PAYMENT_REQUEST", null, (Context)localObject6, (Class)localObject7);
    Object localObject5 = paramFlash.f();
    c.g.b.k.a(localObject5, "flash.payload");
    localObject5 = ((Payload)localObject5).b();
    Object localObject8 = "0";
    localObject3 = (CharSequence)localObject3;
    if (localObject3 != null)
    {
      bool4 = c.n.m.a((CharSequence)localObject3);
      if (!bool4)
      {
        bool4 = false;
        localObject6 = null;
        break label319;
      }
    }
    boolean bool4 = true;
    label319:
    int k;
    if (!bool4)
    {
      localObject8 = ",";
      localObject6 = new c/n/k;
      ((c.n.k)localObject6).<init>((String)localObject8);
      localObject3 = ((c.n.k)localObject6).a((CharSequence)localObject3, 0);
      bool3 = ((List)localObject3).isEmpty();
      int m;
      if (!bool3)
      {
        k = ((List)localObject3).size();
        localObject8 = ((List)localObject3).listIterator(k);
        do
        {
          bool4 = ((ListIterator)localObject8).hasPrevious();
          if (!bool4) {
            break;
          }
          localObject6 = (CharSequence)((ListIterator)localObject8).previous();
          m = ((CharSequence)localObject6).length();
          if (m == 0)
          {
            m = 1;
          }
          else
          {
            m = 0;
            localObject6 = null;
          }
        } while (m != 0);
        localObject3 = (Iterable)localObject3;
        k = ((ListIterator)localObject8).nextIndex() + i;
        localObject3 = c.a.m.d((Iterable)localObject3, k);
      }
      else
      {
        localObject3 = (List)y.a;
      }
      localObject3 = (Collection)localObject3;
      if (localObject3 != null)
      {
        localObject8 = new String[0];
        localObject3 = ((Collection)localObject3).toArray((Object[])localObject8);
        if (localObject3 != null)
        {
          localObject3 = (String[])localObject3;
          localObject8 = localObject3[0];
          m = localObject3.length;
          if (m > i) {
            localObject3 = localObject3[i];
          } else {
            localObject3 = "";
          }
          localObject6 = "vpa";
          ((Intent)localObject4).putExtra((String)localObject6, (String)localObject3);
        }
        else
        {
          paramFlash = new c/u;
          paramFlash.<init>("null cannot be cast to non-null type kotlin.Array<T>");
          throw paramFlash;
        }
      }
      else
      {
        paramFlash = new c/u;
        paramFlash.<init>("null cannot be cast to non-null type java.util.Collection<T>");
        throw paramFlash;
      }
    }
    localObject3 = paramFlash.a();
    localObject6 = "flash.sender";
    c.g.b.k.a(localObject3, (String)localObject6);
    long l2 = ((Sender)localObject3).a().longValue();
    localObject3 = String.valueOf(l2);
    int n = ((String)localObject3).length();
    int i1 = 10;
    if (n >= i1)
    {
      i1 = n + -10;
      if (localObject3 != null)
      {
        localObject3 = ((String)localObject3).substring(i1, n);
        c.g.b.k.a(localObject3, "(this as java.lang.Strin…ing(startIndex, endIndex)");
        localObject6 = "number";
        ((Intent)localObject4).putExtra((String)localObject6, (String)localObject3);
      }
      else
      {
        paramFlash = new c/u;
        paramFlash.<init>("null cannot be cast to non-null type java.lang.String");
        throw paramFlash;
      }
    }
    else
    {
      localObject6 = "number";
      ((Intent)localObject4).putExtra((String)localObject6, (String)localObject3);
    }
    ((Intent)localObject4).putExtra("amount", (String)localObject8);
    ((Intent)localObject4).putExtra("comment", (String)localObject5);
    ((Intent)localObject4).putExtra("readOnly", i);
    localObject6 = paramFlash;
    localObject6 = (Parcelable)paramFlash;
    ((Intent)localObject4).putExtra("flash", (Parcelable)localObject6);
    localObject3 = a;
    n = 140;
    i1 = 134217728;
    localObject3 = PendingIntent.getBroadcast((Context)localObject3, n, (Intent)localObject4, i1);
    localObject4 = localObject5;
    localObject4 = (CharSequence)localObject5;
    if (localObject4 != null)
    {
      bool2 = c.n.m.a((CharSequence)localObject4);
      if (!bool2)
      {
        bool2 = false;
        localObject4 = null;
        break label854;
      }
    }
    boolean bool2 = true;
    label854:
    n = 2;
    if (!bool2)
    {
      localObject4 = "payment_request";
      bool2 = c.g.b.k.a(localObject4, localObject5);
      if (!bool2)
      {
        localObject4 = a;
        i1 = R.string.requested_money;
        localObject6 = new Object[n];
        localObject6[0] = localObject1;
        localObject6[i] = localObject8;
        localObject4 = ((Context)localObject4).getString(i1, (Object[])localObject6);
        localObject8 = new java/lang/StringBuilder;
        ((StringBuilder)localObject8).<init>();
        ((StringBuilder)localObject8).append((String)localObject4);
        localObject4 = a;
        n = R.string.requested_money_comment;
        localObject7 = new Object[i];
        localObject7[0] = localObject5;
        localObject4 = ((Context)localObject4).getString(n, (Object[])localObject7);
        ((StringBuilder)localObject8).append((String)localObject4);
        localObject4 = ((StringBuilder)localObject8).toString();
        break label1049;
      }
    }
    localObject4 = a;
    int i2 = R.string.requested_money;
    localObject6 = new Object[n];
    localObject6[0] = localObject1;
    localObject6[i] = localObject8;
    localObject4 = ((Context)localObject4).getString(i2, (Object[])localObject6);
    localObject5 = "managerContext.getString…sted_money, name, amount)";
    c.g.b.k.a(localObject4, (String)localObject5);
    label1049:
    localObject1 = (CharSequence)localObject1;
    if (localObject1 != null)
    {
      bool5 = c.n.m.a((CharSequence)localObject1);
      if (!bool5)
      {
        bool5 = false;
        localObject1 = null;
        break label1083;
      }
    }
    boolean bool5 = true;
    label1083:
    if (!bool5)
    {
      localObject1 = d.j();
      localObject1 = a((String)localObject1);
      i2 = R.drawable.tc_notification_logo;
      localObject1 = ((z.d)localObject1).a(i2);
      localObject5 = a;
      k = R.color.truecolor;
      i2 = android.support.v4.content.b.c((Context)localObject5, k);
      localObject1 = ((z.d)localObject1).f(i2);
      localObject5 = a;
      k = R.string.truecaller_pay;
      localObject5 = (CharSequence)((Context)localObject5).getString(k);
      localObject1 = ((z.d)localObject1).a((CharSequence)localObject5);
      localObject4 = (CharSequence)localObject4;
      localObject1 = ((z.d)localObject1).b((CharSequence)localObject4).e();
      i2 = -65536;
      localObject1 = ((z.d)localObject1).a(i2, i, i);
      long l3 = System.currentTimeMillis();
      localObject1 = ((z.d)localObject1).a(l3).a();
      Object localObject9 = new android/support/v4/app/z$c;
      ((z.c)localObject9).<init>();
      localObject9 = (z.g)((z.c)localObject9).b((CharSequence)localObject4);
      localObject1 = ((z.d)localObject1).a((z.g)localObject9);
      localObject9 = a;
      int j = R.string.payments_pay;
      localObject9 = (CharSequence)((Context)localObject9).getString(j);
      localObject1 = ((z.d)localObject1).a(0, (CharSequence)localObject9, (PendingIntent)localObject3).a((PendingIntent)localObject3);
      localObject2 = b((String)localObject2);
      localObject1 = ((z.d)localObject1).a((Bitmap)localObject2);
      localObject2 = a;
      localObject3 = "notification";
      localObject2 = ((Context)localObject2).getSystemService((String)localObject3);
      if (localObject2 != null)
      {
        localObject2 = (NotificationManager)localObject2;
        localObject1 = ((z.d)localObject1).h();
        paramFlash = paramFlash.a();
        localObject3 = "flash.sender";
        c.g.b.k.a(paramFlash, (String)localObject3);
        paramFlash = paramFlash.a();
        long l4 = paramFlash.longValue();
        long l5 = 1000000000L;
        l4 %= l5;
        int i3 = (int)l4 + 101;
        ((NotificationManager)localObject2).notify(i3, (Notification)localObject1);
      }
      else
      {
        paramFlash = new c/u;
        paramFlash.<init>("null cannot be cast to non-null type android.app.NotificationManager");
        throw paramFlash;
      }
    }
  }
  
  public final void b(ImageFlash paramImageFlash)
  {
    c.g.b.k.b(paramImageFlash, "flash");
    Object localObject1 = new android/content/Intent;
    ((Intent)localObject1).<init>();
    Object localObject2 = paramImageFlash;
    localObject2 = (Parcelable)paramImageFlash;
    ((Intent)localObject1).putExtra("extra_image_flash", (Parcelable)localObject2);
    Object localObject3 = a;
    int i = R.id.flash_image_uploading_notification_id;
    localObject1 = PendingIntent.getService((Context)localObject3, i, (Intent)localObject1, 134217728);
    localObject3 = g;
    i = R.string.flash_text;
    Object[] arrayOfObject = new Object[0];
    localObject3 = ((al)localObject3).a(i, arrayOfObject);
    localObject2 = g;
    int j = R.string.sending_flash;
    Object localObject4 = new Object[0];
    localObject2 = ((al)localObject2).a(j, (Object[])localObject4);
    localObject4 = d.h();
    localObject4 = a((String)localObject4);
    localObject3 = (CharSequence)localObject3;
    ((z.d)localObject4).a((CharSequence)localObject3);
    localObject2 = (CharSequence)localObject2;
    ((z.d)localObject4).b((CharSequence)localObject2);
    int k = 100;
    ((z.d)localObject4).a(k, k);
    localObject3 = new android/support/v4/app/z$c;
    ((z.c)localObject3).<init>();
    localObject3 = (z.g)((z.c)localObject3).b((CharSequence)localObject2);
    ((z.d)localObject4).a((z.g)localObject3);
    localObject3 = a;
    i = R.color.truecolor;
    k = android.support.v4.content.b.c((Context)localObject3, i);
    ((z.d)localObject4).f(k);
    k = R.drawable.ic_flash;
    ((z.d)localObject4).a(k);
    ((z.d)localObject4).b();
    ((z.d)localObject4).a((PendingIntent)localObject1);
    long l1 = System.currentTimeMillis();
    ((z.d)localObject4).a(l1);
    ((z.d)localObject4).a();
    localObject1 = a;
    localObject3 = "notification";
    localObject1 = (NotificationManager)((Context)localObject1).getSystemService((String)localObject3);
    if (localObject1 != null)
    {
      long l2 = R.id.flash_image_uploading_notification_id;
      long l3 = paramImageFlash.b();
      int m = (int)(l2 + l3);
      localObject3 = ((z.d)localObject4).h();
      ((NotificationManager)localObject1).notify(m, (Notification)localObject3);
      return;
    }
  }
  
  public final void c(Flash paramFlash)
  {
    c.g.b.k.b(paramFlash, "flash");
    Object localObject = a;
    String str = "notification";
    localObject = (NotificationManager)((Context)localObject).getSystemService(str);
    if (localObject != null)
    {
      long l1 = R.id.flash_image_uploading_notification_id;
      long l2 = paramFlash.b();
      int i = (int)(l1 + l2);
      ((NotificationManager)localObject).cancel(i);
      return;
    }
  }
  
  public final void c(ImageFlash paramImageFlash)
  {
    c.g.b.k.b(paramImageFlash, "flash");
    Object localObject1 = new android/content/Intent;
    ((Intent)localObject1).<init>();
    Object localObject2 = paramImageFlash;
    localObject2 = (Parcelable)paramImageFlash;
    ((Intent)localObject1).putExtra("extra_image_flash", (Parcelable)localObject2);
    Object localObject3 = a;
    int i = R.id.flash_image_uploading_notification_id;
    localObject1 = PendingIntent.getService((Context)localObject3, i, (Intent)localObject1, 134217728);
    localObject3 = g;
    i = R.string.flash_text;
    Object[] arrayOfObject = new Object[0];
    localObject3 = ((al)localObject3).a(i, arrayOfObject);
    localObject2 = g;
    int j = R.string.uploading_image;
    Object localObject4 = new Object[0];
    localObject2 = ((al)localObject2).a(j, (Object[])localObject4);
    localObject4 = d.h();
    localObject4 = a((String)localObject4);
    localObject3 = (CharSequence)localObject3;
    ((z.d)localObject4).a((CharSequence)localObject3);
    localObject2 = (CharSequence)localObject2;
    ((z.d)localObject4).b((CharSequence)localObject2);
    int k = 100;
    ((z.d)localObject4).a(k, k);
    localObject3 = new android/support/v4/app/z$c;
    ((z.c)localObject3).<init>();
    localObject3 = (z.g)((z.c)localObject3).b((CharSequence)localObject2);
    ((z.d)localObject4).a((z.g)localObject3);
    localObject3 = a;
    i = R.color.truecolor;
    k = android.support.v4.content.b.c((Context)localObject3, i);
    ((z.d)localObject4).f(k);
    k = R.drawable.ic_flash;
    ((z.d)localObject4).a(k);
    ((z.d)localObject4).a((PendingIntent)localObject1);
    long l1 = System.currentTimeMillis();
    ((z.d)localObject4).a(l1);
    ((z.d)localObject4).b();
    ((z.d)localObject4).a();
    localObject1 = a;
    localObject3 = "notification";
    localObject1 = (NotificationManager)((Context)localObject1).getSystemService((String)localObject3);
    if (localObject1 != null)
    {
      long l2 = R.id.flash_image_uploading_notification_id;
      long l3 = paramImageFlash.b();
      int m = (int)(l2 + l3);
      localObject3 = ((z.d)localObject4).h();
      ((NotificationManager)localObject1).notify(m, (Notification)localObject3);
      return;
    }
  }
  
  public final void d(ImageFlash paramImageFlash)
  {
    c.g.b.k.b(paramImageFlash, "flash");
    Object localObject1 = new android/content/Intent;
    Object localObject2 = a;
    ((Intent)localObject1).<init>((Context)localObject2, FlashMediaService.class);
    ((Intent)localObject1).setAction("action_image_flash_retry");
    Object localObject3 = paramImageFlash;
    localObject3 = (Parcelable)paramImageFlash;
    ((Intent)localObject1).putExtra("extra_image_flash", (Parcelable)localObject3);
    localObject2 = a;
    int i = R.id.flash_image_uploading_notification_id;
    localObject1 = PendingIntent.getService((Context)localObject2, i, (Intent)localObject1, 134217728);
    localObject2 = g;
    i = R.string.upload_failed;
    Object localObject4 = new Object[0];
    localObject2 = ((al)localObject2).a(i, (Object[])localObject4);
    localObject3 = g;
    int j = R.string.tap_to_retry;
    Object[] arrayOfObject1 = new Object[0];
    localObject3 = ((al)localObject3).a(j, arrayOfObject1);
    localObject4 = d.h();
    localObject4 = a((String)localObject4);
    localObject2 = (CharSequence)localObject2;
    ((z.d)localObject4).a((CharSequence)localObject2);
    localObject3 = (CharSequence)localObject3;
    ((z.d)localObject4).b((CharSequence)localObject3);
    localObject2 = a;
    i = R.color.truecolor;
    int k = android.support.v4.content.b.c((Context)localObject2, i);
    ((z.d)localObject4).f(k);
    k = R.drawable.ic_flash;
    ((z.d)localObject4).a(k);
    ((z.d)localObject4).a((PendingIntent)localObject1);
    long l1 = System.currentTimeMillis();
    ((z.d)localObject4).a(l1);
    ((z.d)localObject4).a();
    k = R.drawable.ic_notification_retry;
    localObject3 = g;
    int m = R.string.retry;
    Object[] arrayOfObject2 = new Object[0];
    localObject3 = (CharSequence)((al)localObject3).a(m, arrayOfObject2);
    ((z.d)localObject4).a(k, (CharSequence)localObject3, (PendingIntent)localObject1);
    localObject1 = a;
    localObject2 = "notification";
    localObject1 = (NotificationManager)((Context)localObject1).getSystemService((String)localObject2);
    if (localObject1 != null)
    {
      l1 = R.id.flash_image_uploading_notification_id;
      long l2 = paramImageFlash.b();
      int n = (int)(l1 + l2);
      localObject2 = ((z.d)localObject4).h();
      ((NotificationManager)localObject1).notify(n, (Notification)localObject2);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.c.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */