package com.truecaller.flashsdk.c;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Parcelable;
import android.support.v4.app.z.c;
import android.support.v4.app.z.d;
import android.support.v4.app.z.g;
import c.d.a.a;
import c.g.a.m;
import c.o.b;
import c.u;
import c.x;
import com.truecaller.flashsdk.R.color;
import com.truecaller.flashsdk.R.drawable;
import com.truecaller.flashsdk.R.id;
import com.truecaller.flashsdk.R.string;
import com.truecaller.flashsdk.assist.d;
import com.truecaller.flashsdk.assist.g;
import com.truecaller.flashsdk.core.i;
import com.truecaller.flashsdk.models.Contact;
import com.truecaller.flashsdk.models.Flash;
import com.truecaller.flashsdk.models.Sender;
import com.truecaller.flashsdk.receiver.ActionReceiver;
import com.truecaller.notificationchannels.e;
import kotlinx.coroutines.ag;

final class b$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag e;
  
  b$a(b paramb, Flash paramFlash, Bitmap paramBitmap, c.d.c paramc)
  {
    super(2, paramc);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/flashsdk/c/b$a;
    b localb = b;
    Flash localFlash = c;
    Bitmap localBitmap = d;
    locala.<init>(localb, localFlash, localBitmap, paramc);
    paramObject = (ag)paramObject;
    e = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        paramObject = c.a();
        c.g.b.k.a(paramObject, "flash.sender");
        paramObject = ((Sender)paramObject).b();
        localObject1 = c.a();
        Object localObject2 = "flash.sender";
        c.g.b.k.a(localObject1, (String)localObject2);
        localObject1 = ((Sender)localObject1).a();
        if (localObject1 != null)
        {
          long l1 = ((Long)localObject1).longValue();
          Object localObject3 = String.valueOf(l1);
          Object localObject4 = "+".concat((String)localObject3);
          localObject3 = b.c;
          boolean bool2 = ((g)localObject3).f();
          int k = 1;
          int m = 0;
          String str = null;
          if (bool2)
          {
            localObject3 = b.b;
            localObject5 = c.a();
            localObject6 = "flash.sender";
            c.g.b.k.a(localObject5, (String)localObject6);
            long l2 = ((Sender)localObject5).a().longValue();
            localObject5 = String.valueOf(l2);
            localObject3 = ((d)localObject3).b((String)localObject5);
            if (localObject3 != null)
            {
              localObject5 = (CharSequence)((Contact)localObject3).getName();
              int n = ((CharSequence)localObject5).length();
              if (n == 0)
              {
                n = 1;
              }
              else
              {
                n = 0;
                localObject5 = null;
              }
              if (n == 0) {
                paramObject = ((Contact)localObject3).getName();
              }
            }
          }
          else
          {
            bool2 = false;
            localObject3 = null;
          }
          Object localObject5 = b.d.h();
          localObject5 = b.a((String)localObject5);
          Object localObject6 = paramObject;
          localObject6 = (CharSequence)paramObject;
          if (localObject6 != null)
          {
            i1 = ((CharSequence)localObject6).length();
            if (i1 != 0)
            {
              i1 = 0;
              localObject6 = null;
              break label308;
            }
          }
          int i1 = 1;
          label308:
          if (i1 == 0)
          {
            localObject6 = new android/content/Intent;
            Object localObject7 = b.a;
            Class localClass1 = ActionReceiver.class;
            ((Intent)localObject6).<init>("com.truecaller.flashsdk.receiver.ACTION_CALL_PHONE_CALL_ME", null, (Context)localObject7, localClass1);
            ((Intent)localObject6).putExtra("number", l1);
            localObject7 = (Parcelable)c;
            ((Intent)localObject6).putExtra("flash", (Parcelable)localObject7);
            Object localObject8 = b.a;
            int i2 = R.id.call_me_back_notification_id;
            int i3 = 134217728;
            localObject6 = PendingIntent.getBroadcast((Context)localObject8, i2, (Intent)localObject6, i3);
            localObject8 = new android/content/Intent;
            Object localObject9 = b.a;
            Class localClass2 = ActionReceiver.class;
            ((Intent)localObject8).<init>("com.truecaller.flashsdk.receiver.ACTION_DISMISS_SILENTLY", null, (Context)localObject9, localClass2);
            localObject9 = (Parcelable)c;
            ((Intent)localObject8).putExtra("flash", (Parcelable)localObject9);
            localObject7 = b.a;
            int i4 = R.id.call_me_back_notification_id;
            localObject8 = PendingIntent.getBroadcast((Context)localObject7, i4, (Intent)localObject8, i3);
            localObject7 = com.truecaller.flashsdk.core.c.a().e();
            if (localObject7 != null)
            {
              boolean bool3 = ((i)localObject7).b((String)localObject4);
              if (bool3)
              {
                localObject4 = new android/content/Intent;
                localObject7 = "com.truecaller.flashsdk.ACTION_FLASH";
                localObject9 = b.a;
                localClass2 = ActionReceiver.class;
                ((Intent)localObject4).<init>((String)localObject7, null, (Context)localObject9, localClass2);
                str = "number";
                ((Intent)localObject4).putExtra(str, l1);
                ((Intent)localObject4).putExtra("name", (String)paramObject);
                ((Intent)localObject4).putExtra("name", (String)paramObject);
                localObject1 = b.a;
                i6 = R.id.flash_me_back_notification_id;
                localObject1 = PendingIntent.getBroadcast((Context)localObject1, i6, (Intent)localObject4, i3);
                i6 = R.drawable.ic_flash;
                localObject4 = b.a;
                m = R.string.missed_call_notification_flash;
                localObject4 = (CharSequence)((Context)localObject4).getString(m);
                ((z.d)localObject5).a(i6, (CharSequence)localObject4, (PendingIntent)localObject1);
              }
            }
            localObject1 = b.a;
            int i6 = R.string.tap_to_call;
            localObject4 = new Object[k];
            localObject4[0] = paramObject;
            paramObject = ((Context)localObject1).getString(i6, (Object[])localObject4);
            localObject1 = b.a;
            i6 = R.string.call_me_back_title;
            localObject1 = (CharSequence)((Context)localObject1).getString(i6);
            localObject1 = ((z.d)localObject5).a((CharSequence)localObject1);
            paramObject = (CharSequence)paramObject;
            localObject1 = ((z.d)localObject1).b((CharSequence)paramObject);
            localObject2 = new android/support/v4/app/z$c;
            ((z.c)localObject2).<init>();
            paramObject = (z.g)((z.c)localObject2).b((CharSequence)paramObject);
            paramObject = ((z.d)localObject1).a((z.g)paramObject);
            localObject1 = b.a;
            i6 = R.color.truecolor;
            int j = android.support.v4.content.b.c((Context)localObject1, i6);
            paramObject = ((z.d)paramObject).f(j).c(-1);
            j = R.drawable.tc_notification_logo;
            paramObject = ((z.d)paramObject).a(j).a((PendingIntent)localObject6);
            l1 = System.currentTimeMillis();
            paramObject = ((z.d)paramObject).a(l1).a();
            j = R.drawable.ic_reply_call;
            localObject2 = b.a;
            int i5 = R.string.missed_call_notification_call_back;
            localObject2 = (CharSequence)((Context)localObject2).getString(i5);
            ((z.d)paramObject).a(j, (CharSequence)localObject2, (PendingIntent)localObject6).e().b((PendingIntent)localObject8);
            paramObject = d;
            if (paramObject == null)
            {
              paramObject = b;
              if (localObject3 != null)
              {
                localObject1 = ((Contact)localObject3).getImageUrl();
                if (localObject1 != null) {}
              }
              else
              {
                localObject1 = c.a();
                localObject2 = "flash.sender";
                c.g.b.k.a(localObject1, (String)localObject2);
                localObject1 = ((Sender)localObject1).c();
              }
              paramObject = b.a((b)paramObject, (String)localObject1);
            }
            ((z.d)localObject5).a((Bitmap)paramObject);
            paramObject = b.a;
            localObject1 = "notification";
            paramObject = ((Context)paramObject).getSystemService((String)localObject1);
            if (paramObject != null)
            {
              paramObject = (NotificationManager)paramObject;
              localObject1 = ((z.d)localObject5).h();
              i6 = R.id.call_me_back_notification_id;
              ((NotificationManager)paramObject).notify(i6, (Notification)localObject1);
            }
            else
            {
              paramObject = new c/u;
              ((u)paramObject).<init>("null cannot be cast to non-null type android.app.NotificationManager");
              throw ((Throwable)paramObject);
            }
          }
          return x.a;
        }
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.c.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */