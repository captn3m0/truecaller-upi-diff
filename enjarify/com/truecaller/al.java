package com.truecaller;

import android.content.Context;
import com.truecaller.data.access.m;
import dagger.a.d;
import dagger.a.g;
import javax.inject.Provider;

public final class al
  implements d
{
  private final c a;
  private final Provider b;
  
  private al(c paramc, Provider paramProvider)
  {
    a = paramc;
    b = paramProvider;
  }
  
  public static al a(c paramc, Provider paramProvider)
  {
    al localal = new com/truecaller/al;
    localal.<init>(paramc, paramProvider);
    return localal;
  }
  
  public static m a(Context paramContext)
  {
    return (m)g.a(c.c(paramContext), "Cannot return null from a non-@Nullable @Provides method");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.al
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */