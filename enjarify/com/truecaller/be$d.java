package com.truecaller;

import c.d.f;
import com.truecaller.analytics.d;
import com.truecaller.androidactors.k;
import com.truecaller.calling.dialer.ax;
import com.truecaller.calling.recorder.CallRecordingFloatingButton;
import com.truecaller.calling.recorder.CallRecordingFloatingButtonPresenter;
import com.truecaller.calling.recorder.CallRecordingManager;
import com.truecaller.calling.recorder.CallRecordingOnBoardingActivity;
import com.truecaller.calling.recorder.ad;
import com.truecaller.calling.recorder.ag;
import com.truecaller.calling.recorder.ah;
import com.truecaller.calling.recorder.ai;
import com.truecaller.calling.recorder.al;
import com.truecaller.calling.recorder.ar;
import com.truecaller.calling.recorder.au;
import com.truecaller.calling.recorder.ba;
import com.truecaller.calling.recorder.bj;
import com.truecaller.calling.recorder.bk;
import com.truecaller.calling.recorder.bl;
import com.truecaller.calling.recorder.bp;
import com.truecaller.calling.recorder.bt;
import com.truecaller.calling.recorder.bx;
import com.truecaller.calling.recorder.x;
import com.truecaller.calling.recorder.z;
import com.truecaller.network.search.e;
import com.truecaller.premium.br;
import com.truecaller.startup_dialogs.fragments.CallRecordingOnBoardingDialog;
import com.truecaller.util.af;
import com.truecaller.utils.l;
import com.truecaller.utils.n;
import javax.inject.Provider;

final class be$d
  implements com.truecaller.calling.recorder.b
{
  private Provider b;
  private Provider c;
  private Provider d;
  private Provider e;
  private Provider f;
  private Provider g;
  private Provider h;
  private Provider i;
  private Provider j;
  private Provider k;
  private Provider l;
  
  private be$d(be parambe)
  {
    parambe = be.X(a);
    Provider localProvider1 = be.bb(a);
    parambe = com.truecaller.callhistory.u.a(parambe, localProvider1);
    b = parambe;
    parambe = dagger.a.c.a(b);
    c = parambe;
    parambe = c;
    localProvider1 = be.K(a);
    parambe = dagger.a.c.a(ah.a(parambe, localProvider1));
    d = parambe;
    parambe = dagger.a.c.a(com.truecaller.calling.dialer.cd.a());
    e = parambe;
    parambe = dagger.a.c.a(ai.a(be.X(a)));
    f = parambe;
    localProvider1 = d;
    Provider localProvider2 = e;
    Object localObject = be.at(a);
    Provider localProvider3 = f;
    Provider localProvider4 = be.u(a);
    Provider localProvider5 = be.K(a);
    parambe = bp.a(localProvider1, localProvider2, (Provider)localObject, localProvider3, localProvider4, localProvider5);
    g = parambe;
    parambe = dagger.a.c.a(g);
    h = parambe;
    parambe = dagger.a.c.a(z.a());
    i = parambe;
    parambe = be.o(a);
    localProvider1 = h;
    parambe = dagger.a.c.a(ag.a(parambe, localProvider1));
    j = parambe;
    localProvider1 = be.u(a);
    localProvider2 = be.at(a);
    localProvider3 = h;
    localProvider4 = be.p(a);
    localProvider5 = h;
    Provider localProvider6 = be.u(a);
    localObject = localProvider3;
    parambe = au.a(localProvider1, localProvider2, localProvider3, localProvider3, localProvider4, localProvider5, localProvider6);
    k = parambe;
    parambe = dagger.a.c.a(k);
    l = parambe;
  }
  
  public final void a(CallRecordingFloatingButton paramCallRecordingFloatingButton)
  {
    CallRecordingFloatingButtonPresenter localCallRecordingFloatingButtonPresenter = new com/truecaller/calling/recorder/CallRecordingFloatingButtonPresenter;
    Object localObject1 = be.u(a).get();
    Object localObject2 = localObject1;
    localObject2 = (CallRecordingManager)localObject1;
    localObject1 = dagger.a.g.a(be.L(a).d(), "Cannot return null from a non-@Nullable component method");
    Object localObject3 = localObject1;
    localObject3 = (com.truecaller.utils.a)localObject1;
    localObject1 = dagger.a.g.a(be.f(a).r(), "Cannot return null from a non-@Nullable component method");
    Object localObject4 = localObject1;
    localObject4 = (f)localObject1;
    localObject1 = dagger.a.g.a(be.f(a).s(), "Cannot return null from a non-@Nullable component method");
    Object localObject5 = localObject1;
    localObject5 = (f)localObject1;
    ba localba = new com/truecaller/calling/recorder/ba;
    localObject1 = (com.truecaller.callhistory.a)be.k(a).get();
    localba.<init>((com.truecaller.callhistory.a)localObject1);
    localObject1 = be.ba(a).get();
    Object localObject6 = localObject1;
    localObject6 = (bx)localObject1;
    localObject1 = localCallRecordingFloatingButtonPresenter;
    localCallRecordingFloatingButtonPresenter.<init>((CallRecordingManager)localObject2, (com.truecaller.utils.a)localObject3, (f)localObject4, (f)localObject5, localba, (bx)localObject6);
    a = localCallRecordingFloatingButtonPresenter;
  }
  
  public final void a(CallRecordingOnBoardingActivity paramCallRecordingOnBoardingActivity)
  {
    al localal = new com/truecaller/calling/recorder/al;
    Object localObject1 = dagger.a.g.a(be.f(a).c(), "Cannot return null from a non-@Nullable component method");
    Object localObject2 = localObject1;
    localObject2 = (com.truecaller.common.g.a)localObject1;
    localObject1 = dagger.a.g.a(be.L(a).g(), "Cannot return null from a non-@Nullable component method");
    Object localObject3 = localObject1;
    localObject3 = (n)localObject1;
    localObject1 = dagger.a.g.a(be.L(a).d(), "Cannot return null from a non-@Nullable component method");
    Object localObject4 = localObject1;
    localObject4 = (com.truecaller.utils.a)localObject1;
    localObject1 = be.p(a).get();
    Object localObject5 = localObject1;
    localObject5 = (com.truecaller.common.f.c)localObject1;
    localObject1 = dagger.a.g.a(be.I(a).c(), "Cannot return null from a non-@Nullable component method");
    Object localObject6 = localObject1;
    localObject6 = (com.truecaller.analytics.b)localObject1;
    localObject1 = dagger.a.g.a(be.L(a).b(), "Cannot return null from a non-@Nullable component method");
    Object localObject7 = localObject1;
    localObject7 = (l)localObject1;
    localObject1 = be.u(a).get();
    Object localObject8 = localObject1;
    localObject8 = (CallRecordingManager)localObject1;
    localObject1 = localal;
    localal.<init>((com.truecaller.common.g.a)localObject2, (n)localObject3, (com.truecaller.utils.a)localObject4, (com.truecaller.common.f.c)localObject5, (com.truecaller.analytics.b)localObject6, (l)localObject7, (CallRecordingManager)localObject8);
    a = localal;
  }
  
  public final void a(bj parambj)
  {
    Object localObject1 = parambj;
    Object localObject2 = (bl)h.get();
    a = ((bl)localObject2);
    localObject2 = new com/truecaller/calling/recorder/ad;
    bk localbk = (bk)h.get();
    com.truecaller.common.h.aj localaj = (com.truecaller.common.h.aj)dagger.a.g.a(be.f(a).q(), "Cannot return null from a non-@Nullable component method");
    n localn = (n)dagger.a.g.a(be.L(a).g(), "Cannot return null from a non-@Nullable component method");
    af localaf = (af)be.z(a).get();
    ax localax = (ax)be.aC(a).get();
    com.truecaller.calling.dialer.t localt = (com.truecaller.calling.dialer.t)h.get();
    com.truecaller.calling.recorder.u localu = (com.truecaller.calling.recorder.u)be.aY(a).get();
    x localx = (x)i.get();
    com.truecaller.calling.recorder.cd localcd = (com.truecaller.calling.recorder.cd)be.aZ(a).get();
    bt localbt = (bt)h.get();
    com.truecaller.calling.dialer.a locala = (com.truecaller.calling.dialer.a)h.get();
    localObject1 = dagger.a.g.a(be.I(a).c(), "Cannot return null from a non-@Nullable component method");
    Object localObject3 = localObject1;
    localObject3 = (com.truecaller.analytics.b)localObject1;
    localObject1 = be.K(a).get();
    Object localObject4 = localObject1;
    localObject4 = (k)localObject1;
    localObject1 = j.get();
    Object localObject5 = localObject1;
    localObject5 = (e)localObject1;
    localObject1 = dagger.a.g.a(be.f(a).r(), "Cannot return null from a non-@Nullable component method");
    Object localObject6 = localObject1;
    localObject6 = (f)localObject1;
    localObject1 = be.u(a).get();
    Object localObject7 = localObject1;
    localObject7 = (com.truecaller.calling.recorder.g)localObject1;
    localObject1 = be.ba(a).get();
    Object localObject8 = localObject1;
    localObject8 = (bx)localObject1;
    localObject1 = be.u(a).get();
    Object localObject9 = localObject1;
    localObject9 = (CallRecordingManager)localObject1;
    localObject1 = dagger.a.g.a(be.f(a).s(), "Cannot return null from a non-@Nullable component method");
    Object localObject10 = localObject1;
    localObject10 = (f)localObject1;
    ((ad)localObject2).<init>(localbk, localaj, localn, localaf, localax, localt, localu, localx, localcd, localbt, locala, (com.truecaller.analytics.b)localObject3, (k)localObject4, (e)localObject5, (f)localObject6, (com.truecaller.calling.recorder.g)localObject7, (bx)localObject8, (CallRecordingManager)localObject9, (f)localObject10);
    localObject1 = parambj;
    b = ((ad)localObject2);
    localObject2 = (ar)l.get();
    c = ((ar)localObject2);
    localObject2 = new com/truecaller/premium/br;
    ((br)localObject2).<init>();
    d = ((br)localObject2);
    localObject2 = new com/truecaller/calling/recorder/aj;
    ((com.truecaller.calling.recorder.aj)localObject2).<init>();
    e = ((com.truecaller.calling.recorder.aj)localObject2);
  }
  
  public final void a(CallRecordingOnBoardingDialog paramCallRecordingOnBoardingDialog)
  {
    Object localObject = (CallRecordingManager)be.u(a).get();
    a = ((CallRecordingManager)localObject);
    localObject = (n)dagger.a.g.a(be.L(a).g(), "Cannot return null from a non-@Nullable component method");
    b = ((n)localObject);
    localObject = new com/truecaller/premium/br;
    ((br)localObject).<init>();
    c = ((br)localObject);
    localObject = new com/truecaller/calling/recorder/aj;
    ((com.truecaller.calling.recorder.aj)localObject).<init>();
    d = ((com.truecaller.calling.recorder.aj)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.be.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */