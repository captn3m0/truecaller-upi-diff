package com.truecaller.tcpermissions;

import c.a.m;
import c.g.b.k;
import c.u;
import com.truecaller.bb;
import com.truecaller.utils.l;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public final class h
  extends bb
  implements g.a
{
  private List a;
  private PermissionRequestOptions c;
  private Set d;
  private d e;
  private final o f;
  private final l g;
  
  public h(o paramo, l paraml)
  {
    f = paramo;
    g = paraml;
    paramo = new com/truecaller/tcpermissions/d;
    paramo.<init>(false);
    e = paramo;
  }
  
  private final boolean b()
  {
    g.b localb = (g.b)b;
    if (localb == null) {
      return false;
    }
    Object localObject1 = a;
    String str;
    if (localObject1 == null)
    {
      str = "permissions";
      k.a(str);
    }
    localObject1 = ((Iterable)localObject1).iterator();
    boolean bool1;
    boolean bool2;
    do
    {
      boolean bool3;
      do
      {
        Object localObject3;
        do
        {
          bool1 = ((Iterator)localObject1).hasNext();
          if (!bool1) {
            break;
          }
          str = (String)((Iterator)localObject1).next();
          localObject2 = g;
          bool2 = true;
          localObject3 = new String[bool2];
          localObject3[0] = str;
          bool3 = ((l)localObject2).a((String[])localObject3);
        } while (bool3);
        Object localObject2 = d;
        if (localObject2 == null)
        {
          localObject3 = "deniedPermissionCandidates";
          k.a((String)localObject3);
        }
        bool3 = ((Set)localObject2).contains(str);
      } while (!bool3);
      bool1 = localb.a(str);
    } while (bool1);
    return bool2;
    return false;
  }
  
  private final boolean c()
  {
    Object localObject1 = g;
    Object localObject2 = a;
    Object localObject3;
    if (localObject2 == null)
    {
      localObject3 = "permissions";
      k.a((String)localObject3);
    }
    localObject2 = (Collection)localObject2;
    if (localObject2 != null)
    {
      int i = 0;
      localObject3 = new String[0];
      localObject2 = ((Collection)localObject2).toArray((Object[])localObject3);
      if (localObject2 != null)
      {
        localObject2 = (String[])localObject2;
        i = localObject2.length;
        localObject2 = (String[])Arrays.copyOf((Object[])localObject2, i);
        return ((l)localObject1).a((String[])localObject2);
      }
      localObject1 = new c/u;
      ((u)localObject1).<init>("null cannot be cast to non-null type kotlin.Array<T>");
      throw ((Throwable)localObject1);
    }
    localObject1 = new c/u;
    ((u)localObject1).<init>("null cannot be cast to non-null type java.util.Collection<T>");
    throw ((Throwable)localObject1);
  }
  
  public final void a()
  {
    new String[1][0] = "Finishing.";
    o localo = f;
    d locald = e;
    localo.a(locald);
  }
  
  public final void a(int paramInt)
  {
    int i = 5433;
    if (paramInt != i) {
      return;
    }
    Object localObject = e;
    boolean bool = c();
    paramInt = b;
    d locald = new com/truecaller/tcpermissions/d;
    locald.<init>(bool, paramInt);
    e = locald;
    localObject = (g.b)b;
    if (localObject != null)
    {
      ((g.b)localObject).finish();
      return;
    }
  }
  
  public final void a(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    k.b(paramArrayOfString, "permissions");
    Object localObject = "grantResults";
    k.b(paramArrayOfInt, (String)localObject);
    int i = 5432;
    if (paramInt != i) {
      return;
    }
    g.b localb = (g.b)b;
    if (localb == null) {
      return;
    }
    paramArrayOfInt = new com/truecaller/tcpermissions/d;
    boolean bool1 = c();
    boolean bool2 = b();
    paramArrayOfInt.<init>(bool1, bool2);
    e = paramArrayOfInt;
    paramArrayOfInt = c;
    if (paramArrayOfInt == null)
    {
      localObject = "options";
      k.a((String)localObject);
    }
    paramArrayOfInt = c;
    if (paramArrayOfInt != null)
    {
      paramArrayOfInt = (Number)paramArrayOfInt;
      i = paramArrayOfInt.intValue();
      localObject = g;
      int j = paramArrayOfString.length;
      paramArrayOfString = (String[])Arrays.copyOf(paramArrayOfString, j);
      bool3 = ((l)localObject).a(paramArrayOfString);
      if (!bool3)
      {
        paramArrayOfString = (g.b)b;
        if (paramArrayOfString != null) {
          paramArrayOfString.a(i);
        }
      }
    }
    paramArrayOfString = c;
    if (paramArrayOfString == null)
    {
      paramArrayOfInt = "options";
      k.a(paramArrayOfInt);
    }
    boolean bool3 = a;
    if (bool3)
    {
      paramArrayOfString = e;
      bool3 = b;
      if (bool3)
      {
        paramArrayOfString = "At least one of the permissions is denied permanently. Showing permission settings.";
        new String[1][0] = paramArrayOfString;
        paramInt = localb.a();
        if (paramInt != 0) {
          return;
        }
        new String[1][0] = "Can't show permission settings screen. Finishing.";
        localb = (g.b)b;
        if (localb != null) {
          localb.finish();
        }
        return;
      }
    }
    localb = (g.b)b;
    if (localb != null)
    {
      localb.finish();
      return;
    }
  }
  
  public final void a(List paramList, PermissionRequestOptions paramPermissionRequestOptions, boolean paramBoolean)
  {
    Object localObject1 = (g.b)b;
    if (localObject1 == null) {
      return;
    }
    if (paramList == null)
    {
      new String[1][0] = "Permissions are not provided. Finishing.";
      paramList = (g.b)b;
      if (paramList != null)
      {
        paramList.finish();
        return;
      }
      return;
    }
    a = paramList;
    if (paramPermissionRequestOptions == null)
    {
      paramPermissionRequestOptions = new com/truecaller/tcpermissions/PermissionRequestOptions;
      localObject2 = null;
      int i = 7;
      paramPermissionRequestOptions.<init>(false, false, null, i);
    }
    c = paramPermissionRequestOptions;
    int j = 1;
    Object localObject2 = new String[j];
    Object localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>("Permissions are set ");
    ((StringBuilder)localObject3).append(paramList);
    Object localObject4 = ", onRestore = ";
    ((StringBuilder)localObject3).append((String)localObject4);
    ((StringBuilder)localObject3).append(paramBoolean);
    localObject3 = ((StringBuilder)localObject3).toString();
    localObject2[0] = localObject3;
    localObject2 = paramList;
    localObject2 = (Iterable)paramList;
    localObject3 = new java/util/ArrayList;
    ((ArrayList)localObject3).<init>();
    localObject3 = (Collection)localObject3;
    localObject2 = ((Iterable)localObject2).iterator();
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject2).hasNext();
      if (!bool1) {
        break;
      }
      localObject4 = ((Iterator)localObject2).next();
      Object localObject5 = localObject4;
      localObject5 = (String)localObject4;
      boolean bool2 = ((g.b)localObject1).a((String)localObject5) ^ j;
      if (bool2) {
        ((Collection)localObject3).add(localObject4);
      }
    }
    localObject3 = (Iterable)localObject3;
    localObject1 = m.i((Iterable)localObject3);
    d = ((Set)localObject1);
    if (!paramBoolean)
    {
      paramPermissionRequestOptions = new String[j];
      localObject1 = String.valueOf(paramList);
      Object localObject6 = "Requesting permissions ".concat((String)localObject1);
      paramPermissionRequestOptions[0] = localObject6;
      paramPermissionRequestOptions = (g.b)b;
      if (paramPermissionRequestOptions != null)
      {
        paramList = (Collection)paramList;
        localObject6 = new String[0];
        paramList = paramList.toArray((Object[])localObject6);
        if (paramList != null)
        {
          paramList = (String[])paramList;
          paramPermissionRequestOptions.requestPermissions(paramList, 5432);
          return;
        }
        paramList = new c/u;
        paramList.<init>("null cannot be cast to non-null type kotlin.Array<T>");
        throw paramList;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tcpermissions.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */