package com.truecaller.tcpermissions;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.res.Resources.Theme;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.a;
import android.support.v7.app.AppCompatActivity;
import com.truecaller.log.d;
import com.truecaller.utils.extensions.i;
import java.util.List;

public final class TcPermissionsHandlerActivity
  extends AppCompatActivity
  implements g.b
{
  public static final TcPermissionsHandlerActivity.a b;
  public g.a a;
  
  static
  {
    TcPermissionsHandlerActivity.a locala = new com/truecaller/tcpermissions/TcPermissionsHandlerActivity$a;
    locala.<init>((byte)0);
    b = locala;
  }
  
  public final void a(int paramInt)
  {
    i.a(this, paramInt, null, 1, 2);
  }
  
  public final boolean a()
  {
    boolean bool;
    try
    {
      localObject1 = new android/content/Intent;
      localObject2 = "android.settings.APPLICATION_DETAILS_SETTINGS";
      ((Intent)localObject1).<init>((String)localObject2);
      localObject2 = "package";
      String str = getPackageName();
      localObject2 = Uri.fromParts((String)localObject2, str, null);
      localObject1 = ((Intent)localObject1).setData((Uri)localObject2);
      int i = 5433;
      startActivityForResult((Intent)localObject1, i);
      bool = true;
    }
    catch (ActivityNotFoundException localActivityNotFoundException)
    {
      Object localObject1 = (Throwable)localActivityNotFoundException;
      Object localObject2 = "App settings page couldn't be opened.";
      d.a((Throwable)localObject1, (String)localObject2);
      bool = false;
      localObject1 = null;
    }
    return bool;
  }
  
  public final boolean a(String paramString)
  {
    c.g.b.k.b(paramString, "permission");
    return a.a((Activity)this, paramString);
  }
  
  public final void finish()
  {
    super.finish();
    overridePendingTransition(0, 0);
  }
  
  public final void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    g.a locala = a;
    if (locala == null)
    {
      paramIntent = "presenter";
      c.g.b.k.a(paramIntent);
    }
    locala.a(paramInt1);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Object localObject1 = getTheme();
    int i = R.style.Theme_Truecaller;
    boolean bool = false;
    ((Resources.Theme)localObject1).applyStyle(i, false);
    localObject1 = f.a;
    f.a.a().d().a(this);
    localObject1 = getIntent();
    i = 0;
    PermissionRequestOptions localPermissionRequestOptions = null;
    if (localObject1 != null)
    {
      localObject2 = "permissions";
      localObject1 = ((Intent)localObject1).getStringArrayListExtra((String)localObject2);
    }
    else
    {
      localObject1 = null;
    }
    Object localObject2 = getIntent();
    if (localObject2 != null) {
      localPermissionRequestOptions = (PermissionRequestOptions)((Intent)localObject2).getParcelableExtra("options");
    }
    localObject2 = a;
    String str;
    if (localObject2 == null)
    {
      str = "presenter";
      c.g.b.k.a(str);
    }
    ((g.a)localObject2).a(this);
    localObject2 = a;
    if (localObject2 == null)
    {
      str = "presenter";
      c.g.b.k.a(str);
    }
    localObject1 = (List)localObject1;
    if (paramBundle != null) {
      bool = true;
    }
    ((g.a)localObject2).a((List)localObject1, localPermissionRequestOptions, bool);
  }
  
  public final void onDestroy()
  {
    boolean bool = isFinishing();
    if (bool)
    {
      g.a locala = a;
      if (locala == null)
      {
        String str = "presenter";
        c.g.b.k.a(str);
      }
      locala.a();
    }
    super.onDestroy();
  }
  
  public final void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    c.g.b.k.b(paramArrayOfString, "permissions");
    c.g.b.k.b(paramArrayOfInt, "grantResults");
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    new String[1][0] = "Permission result is received";
    g.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    locala.a(paramInt, paramArrayOfString, paramArrayOfInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tcpermissions.TcPermissionsHandlerActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */