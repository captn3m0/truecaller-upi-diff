package com.truecaller.tcpermissions;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class PermissionRequestOptions
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  final boolean a;
  final boolean b;
  final Integer c;
  
  static
  {
    PermissionRequestOptions.a locala = new com/truecaller/tcpermissions/PermissionRequestOptions$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public PermissionRequestOptions()
  {
    this(false, false, null, 7);
  }
  
  public PermissionRequestOptions(boolean paramBoolean1, boolean paramBoolean2, Integer paramInteger)
  {
    a = paramBoolean1;
    b = paramBoolean2;
    c = paramInteger;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof PermissionRequestOptions;
      if (bool2)
      {
        paramObject = (PermissionRequestOptions)paramObject;
        bool2 = a;
        boolean bool3 = a;
        Integer localInteger;
        if (bool2 == bool3)
        {
          bool2 = true;
        }
        else
        {
          bool2 = false;
          localInteger = null;
        }
        if (bool2)
        {
          bool2 = b;
          bool3 = b;
          if (bool2 == bool3)
          {
            bool2 = true;
          }
          else
          {
            bool2 = false;
            localInteger = null;
          }
          if (bool2)
          {
            localInteger = c;
            paramObject = c;
            boolean bool4 = k.a(localInteger, paramObject);
            if (bool4) {
              return bool1;
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    boolean bool = a;
    int j = 1;
    if (bool) {
      bool = true;
    }
    bool *= true;
    int k = b;
    if (k == 0) {
      j = k;
    }
    int i = (i + j) * 31;
    Integer localInteger = c;
    if (localInteger != null)
    {
      j = localInteger.hashCode();
    }
    else
    {
      j = 0;
      localInteger = null;
    }
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("PermissionRequestOptions(showPermissionSettings=");
    boolean bool = a;
    localStringBuilder.append(bool);
    localStringBuilder.append(", showAccessContacts=");
    bool = b;
    localStringBuilder.append(bool);
    localStringBuilder.append(", permissionsDeniedExplanation=");
    Integer localInteger = c;
    localStringBuilder.append(localInteger);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    paramInt = a;
    paramParcel.writeInt(paramInt);
    paramInt = b;
    paramParcel.writeInt(paramInt);
    Integer localInteger = c;
    if (localInteger != null)
    {
      int i = 1;
      paramParcel.writeInt(i);
      paramInt = localInteger.intValue();
    }
    for (;;)
    {
      paramParcel.writeInt(paramInt);
      return;
      paramInt = 0;
      localInteger = null;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tcpermissions.PermissionRequestOptions
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */