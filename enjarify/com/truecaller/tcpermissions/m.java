package com.truecaller.tcpermissions;

import android.os.Build.VERSION;
import c.a.f;
import c.u;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public final class m
  implements l
{
  private final com.truecaller.utils.l a;
  
  public m(com.truecaller.utils.l paraml)
  {
    a = paraml;
  }
  
  private static boolean i()
  {
    int i = Build.VERSION.SDK_INT;
    int j = 23;
    return i < j;
  }
  
  public final String[] a()
  {
    boolean bool = i();
    String[] arrayOfString = null;
    if (bool) {
      return new String[0];
    }
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    localObject = (List)localObject;
    ((List)localObject).add("android.permission.READ_PHONE_STATE");
    ((List)localObject).add("android.permission.READ_CALL_LOG");
    ((List)localObject).add("android.permission.WRITE_CALL_LOG");
    ((List)localObject).add("android.permission.CALL_PHONE");
    String str = "android.permission.PROCESS_OUTGOING_CALLS";
    ((List)localObject).add(str);
    int i = Build.VERSION.SDK_INT;
    int j = 28;
    if (i >= j)
    {
      str = "android.permission.ANSWER_PHONE_CALLS";
      ((List)localObject).add(str);
    }
    localObject = (Collection)localObject;
    arrayOfString = new String[0];
    localObject = ((Collection)localObject).toArray(arrayOfString);
    if (localObject != null) {
      return (String[])localObject;
    }
    localObject = new c/u;
    ((u)localObject).<init>("null cannot be cast to non-null type kotlin.Array<T>");
    throw ((Throwable)localObject);
  }
  
  public final String[] b()
  {
    boolean bool = i();
    if (bool) {
      return new String[0];
    }
    return new String[] { "android.permission.READ_CONTACTS", "android.permission.WRITE_CONTACTS" };
  }
  
  public final String[] c()
  {
    boolean bool = i();
    if (bool) {
      return new String[0];
    }
    String[] arrayOfString1 = a();
    String[] arrayOfString2 = b();
    return (String[])f.a(arrayOfString1, arrayOfString2);
  }
  
  public final String[] d()
  {
    String[] tmp4_1 = new String[3];
    String[] tmp5_4 = tmp4_1;
    String[] tmp5_4 = tmp4_1;
    tmp5_4[0] = "android.permission.READ_SMS";
    tmp5_4[1] = "android.permission.SEND_SMS";
    tmp5_4[2] = "android.permission.RECEIVE_SMS";
    return tmp5_4;
  }
  
  public final String[] e()
  {
    boolean bool = i();
    if (bool) {
      return new String[0];
    }
    return new String[] { "android.permission.RECORD_AUDIO" };
  }
  
  public final boolean f()
  {
    com.truecaller.utils.l locall = a;
    String[] arrayOfString = a();
    int i = arrayOfString.length;
    arrayOfString = (String[])Arrays.copyOf(arrayOfString, i);
    boolean bool = locall.a(arrayOfString);
    if (bool)
    {
      locall = a;
      arrayOfString = b();
      i = arrayOfString.length;
      arrayOfString = (String[])Arrays.copyOf(arrayOfString, i);
      bool = locall.a(arrayOfString);
      if (bool) {
        return true;
      }
    }
    return false;
  }
  
  public final boolean g()
  {
    com.truecaller.utils.l locall = a;
    String[] arrayOfString = (String[])Arrays.copyOf(d(), 3);
    return locall.a(arrayOfString);
  }
  
  public final boolean h()
  {
    com.truecaller.utils.l locall = a;
    String[] arrayOfString = e();
    int i = arrayOfString.length;
    arrayOfString = (String[])Arrays.copyOf(arrayOfString, i);
    return locall.a(arrayOfString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tcpermissions.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */