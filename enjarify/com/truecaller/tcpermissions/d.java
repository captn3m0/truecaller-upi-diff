package com.truecaller.tcpermissions;

import c.g.b.k;
import c.l.g;

public final class d
{
  public final boolean a;
  public final boolean b;
  
  public d(boolean paramBoolean1, boolean paramBoolean2)
  {
    a = paramBoolean1;
    b = paramBoolean2;
  }
  
  public final boolean a(g paramg)
  {
    k.b(paramg, "property");
    return a;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof d;
      if (bool2)
      {
        paramObject = (d)paramObject;
        bool2 = a;
        boolean bool3 = a;
        if (bool2 == bool3) {
          bool2 = true;
        } else {
          bool2 = false;
        }
        if (bool2)
        {
          bool2 = b;
          boolean bool4 = b;
          if (bool2 == bool4)
          {
            bool4 = true;
          }
          else
          {
            bool4 = false;
            paramObject = null;
          }
          if (bool4) {
            return bool1;
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    boolean bool = a;
    int j = 1;
    if (bool) {
      bool = true;
    }
    int i;
    bool *= true;
    int k = b;
    if (k == 0) {
      j = k;
    }
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("PermissionRequestResult(granted=");
    boolean bool = a;
    localStringBuilder.append(bool);
    localStringBuilder.append(", deniedPermanently=");
    bool = b;
    localStringBuilder.append(bool);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tcpermissions.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */