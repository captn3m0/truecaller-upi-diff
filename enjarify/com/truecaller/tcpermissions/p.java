package com.truecaller.tcpermissions;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import c.d.f;
import c.g.b.k;
import com.truecaller.common.g.a;
import com.truecaller.common.h.ac;
import com.truecaller.log.AssertionUtil;
import com.truecaller.utils.l;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public final class p
  implements o
{
  c.g.a.b a;
  final l b;
  private c.g.a.b c;
  private final kotlinx.coroutines.e.b d;
  private final f e;
  private final Context f;
  private final ac g;
  private final com.truecaller.common.h.c h;
  private final a i;
  
  public p(f paramf, Context paramContext, l paraml, ac paramac, com.truecaller.common.h.c paramc, a parama)
  {
    e = paramf;
    f = paramContext;
    b = paraml;
    g = paramac;
    h = paramc;
    i = parama;
    paramf = kotlinx.coroutines.e.d.a();
    d = paramf;
  }
  
  private final boolean a(List paramList)
  {
    paramList = ((Iterable)paramList).iterator();
    Object localObject2;
    boolean bool2;
    do
    {
      bool1 = paramList.hasNext();
      if (!bool1) {
        break;
      }
      localObject1 = paramList.next();
      localObject2 = localObject1;
      localObject2 = (String)localObject1;
      String str = "android.permission.READ_CONTACTS";
      bool2 = k.a(localObject2, str);
    } while (!bool2);
    break label62;
    boolean bool1 = false;
    Object localObject1 = null;
    label62:
    localObject1 = (String)localObject1;
    boolean bool3 = false;
    paramList = null;
    if (localObject1 == null) {
      return false;
    }
    localObject1 = h;
    bool1 = ((com.truecaller.common.h.c)localObject1).c();
    if (bool1)
    {
      localObject1 = g;
      bool1 = ((ac)localObject1).a();
      if (!bool1)
      {
        localObject1 = i;
        localObject2 = "backup";
        bool1 = ((a)localObject1).a((String)localObject2, false);
        if (!bool1) {
          bool3 = true;
        }
      }
    }
    return bool3;
  }
  
  /* Error */
  public final Object a(PermissionRequestOptions paramPermissionRequestOptions, String[] paramArrayOfString, c.d.c paramc)
  {
    // Byte code:
    //   0: aload_3
    //   1: instanceof 243
    //   4: istore 4
    //   6: iload 4
    //   8: ifeq +56 -> 64
    //   11: aload_3
    //   12: astore 5
    //   14: aload_3
    //   15: checkcast 243	com/truecaller/tcpermissions/p$e
    //   18: astore 5
    //   20: aload 5
    //   22: getfield 244	com/truecaller/tcpermissions/p$e:b	I
    //   25: istore 6
    //   27: iconst_m1
    //   28: iconst_m1
    //   29: ishl
    //   30: istore 7
    //   32: iload 6
    //   34: iload 7
    //   36: iand
    //   37: istore 6
    //   39: iload 6
    //   41: ifeq +23 -> 64
    //   44: aload 5
    //   46: getfield 244	com/truecaller/tcpermissions/p$e:b	I
    //   49: iload 7
    //   51: isub
    //   52: istore 8
    //   54: aload 5
    //   56: iload 8
    //   58: putfield 244	com/truecaller/tcpermissions/p$e:b	I
    //   61: goto +18 -> 79
    //   64: new 243	com/truecaller/tcpermissions/p$e
    //   67: astore 5
    //   69: aload 5
    //   71: aload_0
    //   72: aload_3
    //   73: checkcast 199	c/d/c
    //   76: invokespecial 245	com/truecaller/tcpermissions/p$e:<init>	(Lcom/truecaller/tcpermissions/p;Lc/d/c;)V
    //   79: aload 5
    //   81: getfield 246	com/truecaller/tcpermissions/p$e:a	Ljava/lang/Object;
    //   84: astore_3
    //   85: getstatic 129	c/d/a/a:a	Lc/d/a/a;
    //   88: astore 9
    //   90: aload 5
    //   92: getfield 244	com/truecaller/tcpermissions/p$e:b	I
    //   95: istore 7
    //   97: bipush 46
    //   99: istore 10
    //   101: iconst_1
    //   102: istore 11
    //   104: iload 7
    //   106: tableswitch	default:+34->140, 0:+304->410, 1:+222->328, 2:+148->254, 3:+90->196, 4:+46->152
    //   140: new 207	java/lang/IllegalStateException
    //   143: astore_1
    //   144: aload_1
    //   145: ldc -47
    //   147: invokespecial 210	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
    //   150: aload_1
    //   151: athrow
    //   152: aload 5
    //   154: getfield 248	com/truecaller/tcpermissions/p$e:h	Ljava/lang/Object;
    //   157: checkcast 96	com/truecaller/tcpermissions/p$a
    //   160: astore_1
    //   161: aload 5
    //   163: getfield 249	com/truecaller/tcpermissions/p$e:d	Ljava/lang/Object;
    //   166: checkcast 2	com/truecaller/tcpermissions/p
    //   169: astore_2
    //   170: aload_3
    //   171: instanceof 212
    //   174: istore 4
    //   176: iload 4
    //   178: ifne +6 -> 184
    //   181: goto +1029 -> 1210
    //   184: aload_3
    //   185: checkcast 212	c/o$b
    //   188: astore_3
    //   189: aload_3
    //   190: getfield 215	c/o$b:a	Ljava/lang/Throwable;
    //   193: astore_3
    //   194: aload_3
    //   195: athrow
    //   196: aload 5
    //   198: getfield 248	com/truecaller/tcpermissions/p$e:h	Ljava/lang/Object;
    //   201: checkcast 96	com/truecaller/tcpermissions/p$a
    //   204: astore_1
    //   205: aload 5
    //   207: getfield 249	com/truecaller/tcpermissions/p$e:d	Ljava/lang/Object;
    //   210: checkcast 2	com/truecaller/tcpermissions/p
    //   213: astore_2
    //   214: aload_3
    //   215: instanceof 212
    //   218: istore 4
    //   220: iload 4
    //   222: ifne +6 -> 228
    //   225: goto +911 -> 1136
    //   228: aload_3
    //   229: checkcast 212	c/o$b
    //   232: astore_3
    //   233: aload_3
    //   234: getfield 215	c/o$b:a	Ljava/lang/Throwable;
    //   237: astore_3
    //   238: aload_3
    //   239: athrow
    //   240: astore_3
    //   241: aload_2
    //   242: astore 12
    //   244: goto +1101 -> 1345
    //   247: astore_3
    //   248: aload_2
    //   249: astore 12
    //   251: goto +1076 -> 1327
    //   254: aload 5
    //   256: getfield 248	com/truecaller/tcpermissions/p$e:h	Ljava/lang/Object;
    //   259: checkcast 96	com/truecaller/tcpermissions/p$a
    //   262: astore_1
    //   263: aload 5
    //   265: getfield 250	com/truecaller/tcpermissions/p$e:g	Ljava/lang/Object;
    //   268: checkcast 252	java/util/List
    //   271: astore_2
    //   272: aload 5
    //   274: getfield 253	com/truecaller/tcpermissions/p$e:f	Ljava/lang/Object;
    //   277: checkcast 255	[Ljava/lang/String;
    //   280: astore 13
    //   282: aload 5
    //   284: getfield 256	com/truecaller/tcpermissions/p$e:e	Ljava/lang/Object;
    //   287: checkcast 258	com/truecaller/tcpermissions/PermissionRequestOptions
    //   290: astore 14
    //   292: aload 5
    //   294: getfield 249	com/truecaller/tcpermissions/p$e:d	Ljava/lang/Object;
    //   297: checkcast 2	com/truecaller/tcpermissions/p
    //   300: astore 12
    //   302: aload_3
    //   303: instanceof 212
    //   306: istore 15
    //   308: iload 15
    //   310: ifne +6 -> 316
    //   313: goto +693 -> 1006
    //   316: aload_3
    //   317: checkcast 212	c/o$b
    //   320: astore_3
    //   321: aload_3
    //   322: getfield 215	c/o$b:a	Ljava/lang/Throwable;
    //   325: astore_2
    //   326: aload_2
    //   327: athrow
    //   328: aload 5
    //   330: getfield 248	com/truecaller/tcpermissions/p$e:h	Ljava/lang/Object;
    //   333: checkcast 96	com/truecaller/tcpermissions/p$a
    //   336: astore_1
    //   337: aload 5
    //   339: getfield 250	com/truecaller/tcpermissions/p$e:g	Ljava/lang/Object;
    //   342: checkcast 252	java/util/List
    //   345: astore_2
    //   346: aload 5
    //   348: getfield 253	com/truecaller/tcpermissions/p$e:f	Ljava/lang/Object;
    //   351: checkcast 255	[Ljava/lang/String;
    //   354: astore 13
    //   356: aload 5
    //   358: getfield 256	com/truecaller/tcpermissions/p$e:e	Ljava/lang/Object;
    //   361: checkcast 258	com/truecaller/tcpermissions/PermissionRequestOptions
    //   364: astore 14
    //   366: aload 5
    //   368: getfield 249	com/truecaller/tcpermissions/p$e:d	Ljava/lang/Object;
    //   371: checkcast 2	com/truecaller/tcpermissions/p
    //   374: astore 12
    //   376: aload_3
    //   377: instanceof 212
    //   380: istore 15
    //   382: iload 15
    //   384: ifne +6 -> 390
    //   387: goto +393 -> 780
    //   390: aload_3
    //   391: checkcast 212	c/o$b
    //   394: astore_3
    //   395: aload_3
    //   396: getfield 215	c/o$b:a	Ljava/lang/Throwable;
    //   399: astore_2
    //   400: aload_2
    //   401: athrow
    //   402: astore_3
    //   403: goto +942 -> 1345
    //   406: astore_3
    //   407: goto +920 -> 1327
    //   410: aload_3
    //   411: instanceof 212
    //   414: istore 7
    //   416: iload 7
    //   418: ifne +1023 -> 1441
    //   421: getstatic 263	android/os/Build$VERSION:SDK_INT	I
    //   424: istore 8
    //   426: bipush 23
    //   428: istore 7
    //   430: iload 8
    //   432: iload 7
    //   434: if_icmpge +9 -> 443
    //   437: iconst_1
    //   438: istore 8
    //   440: goto +8 -> 448
    //   443: iconst_0
    //   444: istore 8
    //   446: aconst_null
    //   447: astore_3
    //   448: iload 8
    //   450: ifeq +15 -> 465
    //   453: new 217	com/truecaller/tcpermissions/d
    //   456: astore_1
    //   457: aload_1
    //   458: iload 11
    //   460: invokespecial 238	com/truecaller/tcpermissions/d:<init>	(Z)V
    //   463: aload_1
    //   464: areturn
    //   465: new 140	java/util/ArrayList
    //   468: astore_3
    //   469: aload_3
    //   470: invokespecial 265	java/util/ArrayList:<init>	()V
    //   473: aload_3
    //   474: checkcast 142	java/util/Collection
    //   477: astore_3
    //   478: aload_2
    //   479: arraylength
    //   480: istore 7
    //   482: iconst_0
    //   483: istore 16
    //   485: aconst_null
    //   486: astore 14
    //   488: iload 16
    //   490: iload 7
    //   492: if_icmpge +65 -> 557
    //   495: aload_2
    //   496: iload 16
    //   498: aaload
    //   499: astore 12
    //   501: aload_0
    //   502: getfield 50	com/truecaller/tcpermissions/p:b	Lcom/truecaller/utils/l;
    //   505: astore 17
    //   507: iload 11
    //   509: anewarray 87	java/lang/String
    //   512: astore 18
    //   514: aload 18
    //   516: iconst_0
    //   517: aload 12
    //   519: aastore
    //   520: aload 17
    //   522: aload 18
    //   524: invokeinterface 270 2 0
    //   529: iload 11
    //   531: ixor
    //   532: istore 15
    //   534: iload 15
    //   536: ifeq +12 -> 548
    //   539: aload_3
    //   540: aload 12
    //   542: invokeinterface 274 2 0
    //   547: pop
    //   548: iload 16
    //   550: iconst_1
    //   551: iadd
    //   552: istore 16
    //   554: goto -66 -> 488
    //   557: aload_3
    //   558: checkcast 252	java/util/List
    //   561: astore_3
    //   562: aload_3
    //   563: invokeinterface 277 1 0
    //   568: istore 7
    //   570: iload 7
    //   572: ifeq +24 -> 596
    //   575: iconst_1
    //   576: anewarray 87	java/lang/String
    //   579: iconst_0
    //   580: ldc_w 279
    //   583: aastore
    //   584: new 217	com/truecaller/tcpermissions/d
    //   587: astore_1
    //   588: aload_1
    //   589: iload 11
    //   591: invokespecial 238	com/truecaller/tcpermissions/d:<init>	(Z)V
    //   594: aload_1
    //   595: areturn
    //   596: iload 11
    //   598: anewarray 87	java/lang/String
    //   601: astore 13
    //   603: new 89	java/lang/StringBuilder
    //   606: astore 14
    //   608: aload 14
    //   610: ldc_w 281
    //   613: invokespecial 94	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   616: aload 14
    //   618: aload_3
    //   619: invokevirtual 103	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   622: pop
    //   623: ldc_w 283
    //   626: astore 12
    //   628: aload 14
    //   630: aload 12
    //   632: invokevirtual 286	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   635: pop
    //   636: aload 14
    //   638: aload_1
    //   639: invokevirtual 103	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   642: pop
    //   643: aload 14
    //   645: invokevirtual 111	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   648: astore 14
    //   650: aload 13
    //   652: iconst_0
    //   653: aload 14
    //   655: aastore
    //   656: new 96	com/truecaller/tcpermissions/p$a
    //   659: astore 13
    //   661: aload 13
    //   663: aload_0
    //   664: aload_3
    //   665: invokespecial 224	com/truecaller/tcpermissions/p$a:<init>	(Lcom/truecaller/tcpermissions/p;Ljava/util/List;)V
    //   668: aload_0
    //   669: getfield 63	com/truecaller/tcpermissions/p:d	Lkotlinx/coroutines/e/b;
    //   672: astore 14
    //   674: aload 14
    //   676: invokeinterface 289 1 0
    //   681: istore 16
    //   683: iload 16
    //   685: ifeq +16 -> 701
    //   688: ldc_w 291
    //   691: astore 14
    //   693: iconst_1
    //   694: anewarray 87	java/lang/String
    //   697: iconst_0
    //   698: aload 14
    //   700: aastore
    //   701: aload_0
    //   702: getfield 63	com/truecaller/tcpermissions/p:d	Lkotlinx/coroutines/e/b;
    //   705: astore 14
    //   707: aload 5
    //   709: aload_0
    //   710: putfield 249	com/truecaller/tcpermissions/p$e:d	Ljava/lang/Object;
    //   713: aload 5
    //   715: aload_1
    //   716: putfield 256	com/truecaller/tcpermissions/p$e:e	Ljava/lang/Object;
    //   719: aload 5
    //   721: aload_2
    //   722: putfield 253	com/truecaller/tcpermissions/p$e:f	Ljava/lang/Object;
    //   725: aload 5
    //   727: aload_3
    //   728: putfield 250	com/truecaller/tcpermissions/p$e:g	Ljava/lang/Object;
    //   731: aload 5
    //   733: aload 13
    //   735: putfield 248	com/truecaller/tcpermissions/p$e:h	Ljava/lang/Object;
    //   738: aload 5
    //   740: iload 11
    //   742: putfield 244	com/truecaller/tcpermissions/p$e:b	I
    //   745: aload 14
    //   747: aload 5
    //   749: invokeinterface 294 2 0
    //   754: astore 14
    //   756: aload 14
    //   758: aload 9
    //   760: if_acmpne +6 -> 766
    //   763: aload 9
    //   765: areturn
    //   766: aload_0
    //   767: astore 12
    //   769: aload_1
    //   770: astore 14
    //   772: aload 13
    //   774: astore_1
    //   775: aload_2
    //   776: astore 13
    //   778: aload_3
    //   779: astore_2
    //   780: aload 14
    //   782: getfield 296	com/truecaller/tcpermissions/PermissionRequestOptions:b	Z
    //   785: istore 8
    //   787: iload 8
    //   789: ifeq +355 -> 1144
    //   792: aload 12
    //   794: aload_2
    //   795: invokespecial 299	com/truecaller/tcpermissions/p:a	(Ljava/util/List;)Z
    //   798: istore 8
    //   800: iload 8
    //   802: ifeq +342 -> 1144
    //   805: aload 5
    //   807: aload 12
    //   809: putfield 249	com/truecaller/tcpermissions/p$e:d	Ljava/lang/Object;
    //   812: aload 5
    //   814: aload 14
    //   816: putfield 256	com/truecaller/tcpermissions/p$e:e	Ljava/lang/Object;
    //   819: aload 5
    //   821: aload 13
    //   823: putfield 253	com/truecaller/tcpermissions/p$e:f	Ljava/lang/Object;
    //   826: aload 5
    //   828: aload_2
    //   829: putfield 250	com/truecaller/tcpermissions/p$e:g	Ljava/lang/Object;
    //   832: aload 5
    //   834: aload_1
    //   835: putfield 248	com/truecaller/tcpermissions/p$e:h	Ljava/lang/Object;
    //   838: iconst_2
    //   839: istore 8
    //   841: aload 5
    //   843: iload 8
    //   845: putfield 244	com/truecaller/tcpermissions/p$e:b	I
    //   848: new 65	kotlinx/coroutines/k
    //   851: astore_3
    //   852: aload 5
    //   854: invokestatic 70	c/d/a/b:a	(Lc/d/c;)Lc/d/c;
    //   857: astore 17
    //   859: aload_3
    //   860: aload 17
    //   862: iload 11
    //   864: invokespecial 74	kotlinx/coroutines/k:<init>	(Lc/d/c;I)V
    //   867: aload_3
    //   868: astore 17
    //   870: aload_3
    //   871: checkcast 76	kotlinx/coroutines/j
    //   874: astore 17
    //   876: new 302	com/truecaller/tcpermissions/p$c
    //   879: astore 18
    //   881: aload 18
    //   883: aload 17
    //   885: aload 12
    //   887: invokespecial 305	com/truecaller/tcpermissions/p$c:<init>	(Lkotlinx/coroutines/j;Lcom/truecaller/tcpermissions/p;)V
    //   890: aload 18
    //   892: checkcast 83	c/g/a/b
    //   895: astore 18
    //   897: aload 12
    //   899: aload 18
    //   901: putfield 85	com/truecaller/tcpermissions/p:c	Lc/g/a/b;
    //   904: ldc_w 307
    //   907: astore 17
    //   909: iconst_1
    //   910: anewarray 87	java/lang/String
    //   913: iconst_0
    //   914: aload 17
    //   916: aastore
    //   917: new 133	android/content/Intent
    //   920: astore 17
    //   922: aload 12
    //   924: getfield 48	com/truecaller/tcpermissions/p:f	Landroid/content/Context;
    //   927: astore 18
    //   929: ldc_w 309
    //   932: astore 19
    //   934: aload 17
    //   936: aload 18
    //   938: aload 19
    //   940: invokespecial 312	android/content/Intent:<init>	(Landroid/content/Context;Ljava/lang/Class;)V
    //   943: ldc_w 313
    //   946: istore 20
    //   948: aload 17
    //   950: iload 20
    //   952: invokevirtual 318	android/content/Intent:setFlags	(I)Landroid/content/Intent;
    //   955: pop
    //   956: aload 12
    //   958: getfield 48	com/truecaller/tcpermissions/p:f	Landroid/content/Context;
    //   961: astore 18
    //   963: aload 18
    //   965: aload 17
    //   967: invokevirtual 324	android/content/Context:startActivity	(Landroid/content/Intent;)V
    //   970: aload_3
    //   971: invokevirtual 124	kotlinx/coroutines/k:h	()Ljava/lang/Object;
    //   974: astore_3
    //   975: getstatic 129	c/d/a/a:a	Lc/d/a/a;
    //   978: astore 17
    //   980: aload_3
    //   981: aload 17
    //   983: if_acmpne +14 -> 997
    //   986: ldc -125
    //   988: astore 17
    //   990: aload 5
    //   992: aload 17
    //   994: invokestatic 30	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   997: aload_3
    //   998: aload 9
    //   1000: if_acmpne +6 -> 1006
    //   1003: aload 9
    //   1005: areturn
    //   1006: aload_3
    //   1007: checkcast 217	com/truecaller/tcpermissions/d
    //   1010: astore_3
    //   1011: iload 11
    //   1013: anewarray 87	java/lang/String
    //   1016: astore 17
    //   1018: new 89	java/lang/StringBuilder
    //   1021: astore 18
    //   1023: ldc_w 326
    //   1026: astore 19
    //   1028: aload 18
    //   1030: aload 19
    //   1032: invokespecial 94	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   1035: aload 18
    //   1037: aload_3
    //   1038: invokevirtual 103	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1041: pop
    //   1042: aload 18
    //   1044: iload 10
    //   1046: invokevirtual 107	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
    //   1049: pop
    //   1050: aload 18
    //   1052: invokevirtual 111	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1055: astore 18
    //   1057: aload 17
    //   1059: iconst_0
    //   1060: aload 18
    //   1062: aastore
    //   1063: aload 5
    //   1065: aload 12
    //   1067: putfield 249	com/truecaller/tcpermissions/p$e:d	Ljava/lang/Object;
    //   1070: aload 5
    //   1072: aload 14
    //   1074: putfield 256	com/truecaller/tcpermissions/p$e:e	Ljava/lang/Object;
    //   1077: aload 5
    //   1079: aload 13
    //   1081: putfield 253	com/truecaller/tcpermissions/p$e:f	Ljava/lang/Object;
    //   1084: aload 5
    //   1086: aload_2
    //   1087: putfield 250	com/truecaller/tcpermissions/p$e:g	Ljava/lang/Object;
    //   1090: aload 5
    //   1092: aload_1
    //   1093: putfield 248	com/truecaller/tcpermissions/p$e:h	Ljava/lang/Object;
    //   1096: aload 5
    //   1098: aload_3
    //   1099: putfield 328	com/truecaller/tcpermissions/p$e:i	Ljava/lang/Object;
    //   1102: iconst_3
    //   1103: istore 21
    //   1105: aload 5
    //   1107: iload 21
    //   1109: putfield 244	com/truecaller/tcpermissions/p$e:b	I
    //   1112: aload 12
    //   1114: aload 14
    //   1116: aload_3
    //   1117: aload_1
    //   1118: aload 5
    //   1120: invokevirtual 332	com/truecaller/tcpermissions/p:a	(Lcom/truecaller/tcpermissions/PermissionRequestOptions;Lcom/truecaller/tcpermissions/d;Lcom/truecaller/tcpermissions/p$a;Lc/d/c;)Ljava/lang/Object;
    //   1123: astore_3
    //   1124: aload_3
    //   1125: aload 9
    //   1127: if_acmpne +6 -> 1133
    //   1130: aload 9
    //   1132: areturn
    //   1133: aload 12
    //   1135: astore_2
    //   1136: aload_3
    //   1137: checkcast 217	com/truecaller/tcpermissions/d
    //   1140: astore_3
    //   1141: goto +74 -> 1215
    //   1144: aload 5
    //   1146: aload 12
    //   1148: putfield 249	com/truecaller/tcpermissions/p$e:d	Ljava/lang/Object;
    //   1151: aload 5
    //   1153: aload 14
    //   1155: putfield 256	com/truecaller/tcpermissions/p$e:e	Ljava/lang/Object;
    //   1158: aload 5
    //   1160: aload 13
    //   1162: putfield 253	com/truecaller/tcpermissions/p$e:f	Ljava/lang/Object;
    //   1165: aload 5
    //   1167: aload_2
    //   1168: putfield 250	com/truecaller/tcpermissions/p$e:g	Ljava/lang/Object;
    //   1171: aload 5
    //   1173: aload_1
    //   1174: putfield 248	com/truecaller/tcpermissions/p$e:h	Ljava/lang/Object;
    //   1177: iconst_4
    //   1178: istore 21
    //   1180: aload 5
    //   1182: iload 21
    //   1184: putfield 244	com/truecaller/tcpermissions/p$e:b	I
    //   1187: aload 12
    //   1189: aload 14
    //   1191: aload_1
    //   1192: aload 5
    //   1194: invokespecial 235	com/truecaller/tcpermissions/p:a	(Lcom/truecaller/tcpermissions/PermissionRequestOptions;Lcom/truecaller/tcpermissions/p$a;Lc/d/c;)Ljava/lang/Object;
    //   1197: astore_3
    //   1198: aload_3
    //   1199: aload 9
    //   1201: if_acmpne +6 -> 1207
    //   1204: aload 9
    //   1206: areturn
    //   1207: aload 12
    //   1209: astore_2
    //   1210: aload_3
    //   1211: checkcast 217	com/truecaller/tcpermissions/d
    //   1214: astore_3
    //   1215: aload_2
    //   1216: aconst_null
    //   1217: putfield 85	com/truecaller/tcpermissions/p:c	Lc/g/a/b;
    //   1220: aload_2
    //   1221: getfield 63	com/truecaller/tcpermissions/p:d	Lkotlinx/coroutines/e/b;
    //   1224: astore 5
    //   1226: aload 5
    //   1228: invokeinterface 289 1 0
    //   1233: istore 4
    //   1235: iload 4
    //   1237: ifeq +14 -> 1251
    //   1240: aload_2
    //   1241: getfield 63	com/truecaller/tcpermissions/p:d	Lkotlinx/coroutines/e/b;
    //   1244: astore_2
    //   1245: aload_2
    //   1246: invokeinterface 335 1 0
    //   1251: iload 11
    //   1253: anewarray 87	java/lang/String
    //   1256: astore_2
    //   1257: new 89	java/lang/StringBuilder
    //   1260: astore 5
    //   1262: aload 5
    //   1264: ldc_w 337
    //   1267: invokespecial 94	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   1270: aload_1
    //   1271: invokevirtual 221	com/truecaller/tcpermissions/p$a:a	()Z
    //   1274: istore 6
    //   1276: aload 5
    //   1278: iload 6
    //   1280: invokevirtual 340	java/lang/StringBuilder:append	(Z)Ljava/lang/StringBuilder;
    //   1283: pop
    //   1284: aload 5
    //   1286: iload 10
    //   1288: invokevirtual 107	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
    //   1291: pop
    //   1292: aload 5
    //   1294: invokevirtual 111	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1297: astore 5
    //   1299: aload_2
    //   1300: iconst_0
    //   1301: aload 5
    //   1303: aastore
    //   1304: aload_1
    //   1305: invokevirtual 341	com/truecaller/tcpermissions/p$a:b	()V
    //   1308: aload_3
    //   1309: areturn
    //   1310: astore_3
    //   1311: aload_0
    //   1312: astore 12
    //   1314: aload 13
    //   1316: astore_1
    //   1317: goto +28 -> 1345
    //   1320: astore_3
    //   1321: aload_0
    //   1322: astore 12
    //   1324: aload 13
    //   1326: astore_1
    //   1327: ldc_w 343
    //   1330: astore_2
    //   1331: iconst_1
    //   1332: anewarray 87	java/lang/String
    //   1335: iconst_0
    //   1336: aload_2
    //   1337: aastore
    //   1338: aload_3
    //   1339: checkcast 345	java/lang/Throwable
    //   1342: astore_3
    //   1343: aload_3
    //   1344: athrow
    //   1345: aload 12
    //   1347: aconst_null
    //   1348: putfield 85	com/truecaller/tcpermissions/p:c	Lc/g/a/b;
    //   1351: aload 12
    //   1353: getfield 63	com/truecaller/tcpermissions/p:d	Lkotlinx/coroutines/e/b;
    //   1356: astore_2
    //   1357: aload_2
    //   1358: invokeinterface 289 1 0
    //   1363: istore 21
    //   1365: iload 21
    //   1367: ifeq +15 -> 1382
    //   1370: aload 12
    //   1372: getfield 63	com/truecaller/tcpermissions/p:d	Lkotlinx/coroutines/e/b;
    //   1375: astore_2
    //   1376: aload_2
    //   1377: invokeinterface 335 1 0
    //   1382: iload 11
    //   1384: anewarray 87	java/lang/String
    //   1387: astore_2
    //   1388: new 89	java/lang/StringBuilder
    //   1391: astore 5
    //   1393: aload 5
    //   1395: ldc_w 337
    //   1398: invokespecial 94	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   1401: aload_1
    //   1402: invokevirtual 221	com/truecaller/tcpermissions/p$a:a	()Z
    //   1405: istore 6
    //   1407: aload 5
    //   1409: iload 6
    //   1411: invokevirtual 340	java/lang/StringBuilder:append	(Z)Ljava/lang/StringBuilder;
    //   1414: pop
    //   1415: aload 5
    //   1417: iload 10
    //   1419: invokevirtual 107	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
    //   1422: pop
    //   1423: aload 5
    //   1425: invokevirtual 111	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1428: astore 5
    //   1430: aload_2
    //   1431: iconst_0
    //   1432: aload 5
    //   1434: aastore
    //   1435: aload_1
    //   1436: invokevirtual 341	com/truecaller/tcpermissions/p$a:b	()V
    //   1439: aload_3
    //   1440: athrow
    //   1441: aload_3
    //   1442: checkcast 212	c/o$b
    //   1445: getfield 215	c/o$b:a	Ljava/lang/Throwable;
    //   1448: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1449	0	this	p
    //   0	1449	1	paramPermissionRequestOptions	PermissionRequestOptions
    //   0	1449	2	paramArrayOfString	String[]
    //   0	1449	3	paramc	c.d.c
    //   4	1232	4	bool1	boolean
    //   12	1421	5	localObject1	Object
    //   25	15	6	j	int
    //   1274	136	6	bool2	boolean
    //   30	75	7	k	int
    //   414	3	7	bool3	boolean
    //   428	65	7	m	int
    //   568	3	7	bool4	boolean
    //   52	397	8	n	int
    //   785	16	8	bool5	boolean
    //   839	5	8	i1	int
    //   88	1117	9	locala	c.d.a.a
    //   99	1319	10	c1	char
    //   102	1281	11	i2	int
    //   242	1129	12	localObject2	Object
    //   280	1045	13	localObject3	Object
    //   290	900	14	localObject4	Object
    //   306	229	15	bool6	boolean
    //   483	70	16	i3	int
    //   681	3	16	bool7	boolean
    //   505	553	17	localObject5	Object
    //   512	549	18	localObject6	Object
    //   932	99	19	localObject7	Object
    //   946	5	20	i4	int
    //   1103	80	21	i5	int
    //   1363	3	21	bool8	boolean
    // Exception table:
    //   from	to	target	type
    //   184	188	240	finally
    //   189	193	240	finally
    //   194	196	240	finally
    //   228	232	240	finally
    //   233	237	240	finally
    //   238	240	240	finally
    //   1136	1140	240	finally
    //   1210	1214	240	finally
    //   184	188	247	java/util/concurrent/CancellationException
    //   189	193	247	java/util/concurrent/CancellationException
    //   194	196	247	java/util/concurrent/CancellationException
    //   228	232	247	java/util/concurrent/CancellationException
    //   233	237	247	java/util/concurrent/CancellationException
    //   238	240	247	java/util/concurrent/CancellationException
    //   1136	1140	247	java/util/concurrent/CancellationException
    //   1210	1214	247	java/util/concurrent/CancellationException
    //   316	320	402	finally
    //   321	325	402	finally
    //   326	328	402	finally
    //   390	394	402	finally
    //   395	399	402	finally
    //   400	402	402	finally
    //   780	785	402	finally
    //   794	798	402	finally
    //   807	812	402	finally
    //   814	819	402	finally
    //   821	826	402	finally
    //   828	832	402	finally
    //   834	838	402	finally
    //   843	848	402	finally
    //   848	851	402	finally
    //   852	857	402	finally
    //   862	867	402	finally
    //   870	874	402	finally
    //   876	879	402	finally
    //   885	890	402	finally
    //   890	895	402	finally
    //   899	904	402	finally
    //   909	917	402	finally
    //   917	920	402	finally
    //   922	927	402	finally
    //   938	943	402	finally
    //   950	956	402	finally
    //   956	961	402	finally
    //   965	970	402	finally
    //   970	974	402	finally
    //   975	978	402	finally
    //   992	997	402	finally
    //   1006	1010	402	finally
    //   1011	1016	402	finally
    //   1018	1021	402	finally
    //   1030	1035	402	finally
    //   1037	1042	402	finally
    //   1044	1050	402	finally
    //   1050	1055	402	finally
    //   1060	1063	402	finally
    //   1065	1070	402	finally
    //   1072	1077	402	finally
    //   1079	1084	402	finally
    //   1086	1090	402	finally
    //   1092	1096	402	finally
    //   1098	1102	402	finally
    //   1107	1112	402	finally
    //   1118	1123	402	finally
    //   1146	1151	402	finally
    //   1153	1158	402	finally
    //   1160	1165	402	finally
    //   1167	1171	402	finally
    //   1173	1177	402	finally
    //   1182	1187	402	finally
    //   1192	1197	402	finally
    //   1331	1338	402	finally
    //   1338	1342	402	finally
    //   1343	1345	402	finally
    //   316	320	406	java/util/concurrent/CancellationException
    //   321	325	406	java/util/concurrent/CancellationException
    //   326	328	406	java/util/concurrent/CancellationException
    //   390	394	406	java/util/concurrent/CancellationException
    //   395	399	406	java/util/concurrent/CancellationException
    //   400	402	406	java/util/concurrent/CancellationException
    //   780	785	406	java/util/concurrent/CancellationException
    //   794	798	406	java/util/concurrent/CancellationException
    //   807	812	406	java/util/concurrent/CancellationException
    //   814	819	406	java/util/concurrent/CancellationException
    //   821	826	406	java/util/concurrent/CancellationException
    //   828	832	406	java/util/concurrent/CancellationException
    //   834	838	406	java/util/concurrent/CancellationException
    //   843	848	406	java/util/concurrent/CancellationException
    //   848	851	406	java/util/concurrent/CancellationException
    //   852	857	406	java/util/concurrent/CancellationException
    //   862	867	406	java/util/concurrent/CancellationException
    //   870	874	406	java/util/concurrent/CancellationException
    //   876	879	406	java/util/concurrent/CancellationException
    //   885	890	406	java/util/concurrent/CancellationException
    //   890	895	406	java/util/concurrent/CancellationException
    //   899	904	406	java/util/concurrent/CancellationException
    //   909	917	406	java/util/concurrent/CancellationException
    //   917	920	406	java/util/concurrent/CancellationException
    //   922	927	406	java/util/concurrent/CancellationException
    //   938	943	406	java/util/concurrent/CancellationException
    //   950	956	406	java/util/concurrent/CancellationException
    //   956	961	406	java/util/concurrent/CancellationException
    //   965	970	406	java/util/concurrent/CancellationException
    //   970	974	406	java/util/concurrent/CancellationException
    //   975	978	406	java/util/concurrent/CancellationException
    //   992	997	406	java/util/concurrent/CancellationException
    //   1006	1010	406	java/util/concurrent/CancellationException
    //   1011	1016	406	java/util/concurrent/CancellationException
    //   1018	1021	406	java/util/concurrent/CancellationException
    //   1030	1035	406	java/util/concurrent/CancellationException
    //   1037	1042	406	java/util/concurrent/CancellationException
    //   1044	1050	406	java/util/concurrent/CancellationException
    //   1050	1055	406	java/util/concurrent/CancellationException
    //   1060	1063	406	java/util/concurrent/CancellationException
    //   1065	1070	406	java/util/concurrent/CancellationException
    //   1072	1077	406	java/util/concurrent/CancellationException
    //   1079	1084	406	java/util/concurrent/CancellationException
    //   1086	1090	406	java/util/concurrent/CancellationException
    //   1092	1096	406	java/util/concurrent/CancellationException
    //   1098	1102	406	java/util/concurrent/CancellationException
    //   1107	1112	406	java/util/concurrent/CancellationException
    //   1118	1123	406	java/util/concurrent/CancellationException
    //   1146	1151	406	java/util/concurrent/CancellationException
    //   1153	1158	406	java/util/concurrent/CancellationException
    //   1160	1165	406	java/util/concurrent/CancellationException
    //   1167	1171	406	java/util/concurrent/CancellationException
    //   1173	1177	406	java/util/concurrent/CancellationException
    //   1182	1187	406	java/util/concurrent/CancellationException
    //   1192	1197	406	java/util/concurrent/CancellationException
    //   668	672	1310	finally
    //   674	681	1310	finally
    //   693	701	1310	finally
    //   701	705	1310	finally
    //   709	713	1310	finally
    //   715	719	1310	finally
    //   721	725	1310	finally
    //   727	731	1310	finally
    //   733	738	1310	finally
    //   740	745	1310	finally
    //   747	754	1310	finally
    //   668	672	1320	java/util/concurrent/CancellationException
    //   674	681	1320	java/util/concurrent/CancellationException
    //   693	701	1320	java/util/concurrent/CancellationException
    //   701	705	1320	java/util/concurrent/CancellationException
    //   709	713	1320	java/util/concurrent/CancellationException
    //   715	719	1320	java/util/concurrent/CancellationException
    //   721	725	1320	java/util/concurrent/CancellationException
    //   727	731	1320	java/util/concurrent/CancellationException
    //   733	738	1320	java/util/concurrent/CancellationException
    //   740	745	1320	java/util/concurrent/CancellationException
    //   747	754	1320	java/util/concurrent/CancellationException
  }
  
  public final Object a(String[] paramArrayOfString, c.d.c paramc)
  {
    PermissionRequestOptions localPermissionRequestOptions = new com/truecaller/tcpermissions/PermissionRequestOptions;
    localPermissionRequestOptions.<init>(false, false, null, 7);
    int j = paramArrayOfString.length;
    paramArrayOfString = (String[])Arrays.copyOf(paramArrayOfString, j);
    return a(localPermissionRequestOptions, paramArrayOfString, paramc);
  }
  
  public final void a(c.g.a.b paramb)
  {
    k.b(paramb, "callback");
    a = paramb;
  }
  
  public final void a(d paramd)
  {
    k.b(paramd, "result");
    c.g.a.b localb = c;
    if (localb == null) {
      return;
    }
    c = null;
    localb.invoke(paramd);
  }
  
  public final boolean a()
  {
    Object localObject1 = new android/content/Intent;
    Object localObject2 = "miui.intent.action.APP_PERM_EDITOR";
    ((Intent)localObject1).<init>((String)localObject2);
    int j = 268435456;
    boolean bool = true;
    Object localObject3 = "com.miui.securitycenter";
    Object localObject4 = "com.miui.permcenter.permissions.PermissionsEditorActivity";
    try
    {
      ((Intent)localObject1).setClassName((String)localObject3, (String)localObject4);
      ((Intent)localObject1).setFlags(j);
      localObject3 = "extra_pkgname";
      localObject4 = f;
      localObject4 = ((Context)localObject4).getPackageName();
      ((Intent)localObject1).putExtra((String)localObject3, (String)localObject4);
      localObject3 = f;
      ((Context)localObject3).startActivity((Intent)localObject1);
      return bool;
    }
    catch (RuntimeException localRuntimeException1)
    {
      localObject3 = "com.miui.securitycenter";
      localObject4 = "com.miui.permcenter.permissions.AppPermissionsEditorActivity";
      try
      {
        ((Intent)localObject1).setClassName((String)localObject3, (String)localObject4);
        localObject3 = f;
        ((Context)localObject3).startActivity((Intent)localObject1);
      }
      catch (RuntimeException localRuntimeException2)
      {
        try
        {
          localObject1 = new android/content/Intent;
          localObject3 = "android.settings.APPLICATION_DETAILS_SETTINGS";
          ((Intent)localObject1).<init>((String)localObject3);
          localObject1 = ((Intent)localObject1).setFlags(j);
          localObject2 = "package";
          localObject3 = f;
          localObject3 = ((Context)localObject3).getPackageName();
          localObject4 = null;
          localObject2 = Uri.fromParts((String)localObject2, (String)localObject3, null);
          localObject1 = ((Intent)localObject1).setData((Uri)localObject2);
          localObject2 = "appSettingsIntent()";
          k.a(localObject1, (String)localObject2);
          localObject2 = f;
          ((Context)localObject2).startActivity((Intent)localObject1);
        }
        catch (RuntimeException localRuntimeException3)
        {
          localObject1 = (Throwable)localRuntimeException3;
          AssertionUtil.reportThrowableButNeverCrash((Throwable)localObject1);
          bool = false;
        }
      }
    }
    return bool;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tcpermissions.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */