package com.truecaller.tcpermissions;

import c.d.f;
import c.g.a.m;
import com.truecaller.ba;
import kotlinx.coroutines.e;

public final class b
  extends ba
  implements a.a
{
  private d c;
  private final f d;
  private final o e;
  
  public b(f paramf, o paramo)
  {
    super(paramf);
    d = paramf;
    e = paramo;
  }
  
  public final void a()
  {
    Object localObject = new com/truecaller/tcpermissions/b$a;
    ((b.a)localObject).<init>(this, null);
    localObject = (m)localObject;
    e.b(this, null, (m)localObject, 3);
  }
  
  public final void b()
  {
    new String[1][0] = "Access contact is denied";
    a.b localb = (a.b)b;
    if (localb != null)
    {
      localb.finish();
      return;
    }
  }
  
  public final void c()
  {
    new String[1][0] = "Finishing.";
    o localo = e;
    d locald = c;
    if (locald == null)
    {
      locald = new com/truecaller/tcpermissions/d;
      locald.<init>(false);
    }
    localo.a(locald);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tcpermissions.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */