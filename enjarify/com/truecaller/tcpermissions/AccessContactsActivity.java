package com.truecaller.tcpermissions;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import java.util.HashMap;

public final class AccessContactsActivity
  extends AppCompatActivity
  implements a.b
{
  public a.a a;
  private HashMap b;
  
  private View a(int paramInt)
  {
    Object localObject1 = b;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      b = ((HashMap)localObject1);
    }
    localObject1 = b;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = findViewById(paramInt);
      localObject2 = b;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final a.a a()
  {
    a.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    return locala;
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = f.a;
    f.a.a().d().a(this);
    int i = R.layout.activity_access_contacts;
    setContentView(i);
    paramBundle = a;
    if (paramBundle == null)
    {
      localObject = "presenter";
      c.g.b.k.a((String)localObject);
    }
    paramBundle.a(this);
    i = R.id.allow_button;
    paramBundle = (Button)a(i);
    Object localObject = new com/truecaller/tcpermissions/AccessContactsActivity$a;
    ((AccessContactsActivity.a)localObject).<init>(this);
    localObject = (View.OnClickListener)localObject;
    paramBundle.setOnClickListener((View.OnClickListener)localObject);
    i = R.id.deny_button;
    paramBundle = (Button)a(i);
    localObject = new com/truecaller/tcpermissions/AccessContactsActivity$b;
    ((AccessContactsActivity.b)localObject).<init>(this);
    localObject = (View.OnClickListener)localObject;
    paramBundle.setOnClickListener((View.OnClickListener)localObject);
  }
  
  public final void onDestroy()
  {
    boolean bool = isFinishing();
    if (bool)
    {
      a.a locala = a;
      if (locala == null)
      {
        String str = "presenter";
        c.g.b.k.a(str);
      }
      locala.c();
    }
    super.onDestroy();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tcpermissions.AccessContactsActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */