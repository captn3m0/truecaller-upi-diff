package com.truecaller.tcpermissions;

import android.content.Context;
import c.g.b.k;
import c.u;
import com.truecaller.common.b.a;
import com.truecaller.common.h.c;

public abstract class i
{
  public static final i.a a;
  
  static
  {
    i.a locala = new com/truecaller/tcpermissions/i$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  public static final c a(Context paramContext)
  {
    String str = "context";
    k.b(paramContext, str);
    paramContext = paramContext.getApplicationContext();
    if (paramContext != null)
    {
      paramContext = ((a)paramContext).e();
      k.a(paramContext, "(context.applicationCont…licationBase).buildHelper");
      return paramContext;
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type com.truecaller.common.app.ApplicationBase");
    throw paramContext;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tcpermissions.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */