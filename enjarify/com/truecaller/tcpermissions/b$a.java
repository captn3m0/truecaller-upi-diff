package com.truecaller.tcpermissions;

import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import kotlinx.coroutines.ag;

final class b$a
  extends c.d.b.a.k
  implements m
{
  Object a;
  int b;
  private ag d;
  
  b$a(b paramb, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/tcpermissions/b$a;
    b localb = c;
    locala.<init>(localb, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = b;
    int j = 1;
    boolean bool;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      localObject1 = (b)a;
      bool = paramObject instanceof o.b;
      if (bool) {
        throw a;
      }
      break;
    case 0: 
      bool = paramObject instanceof o.b;
      if (bool) {
        break label232;
      }
      new String[1][0] = "Requesting for contact access.";
      paramObject = c;
      localObject2 = b.b((b)paramObject);
      String[] arrayOfString = { "android.permission.READ_CONTACTS" };
      a = paramObject;
      b = j;
      localObject2 = ((o)localObject2).a(arrayOfString, this);
      if (localObject2 == localObject1) {
        return localObject1;
      }
      localObject1 = paramObject;
      paramObject = localObject2;
    }
    paramObject = (d)paramObject;
    b.a((b)localObject1, (d)paramObject);
    paramObject = new String[j];
    localObject1 = null;
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("Access contact is requested ");
    d locald = b.a(c);
    ((StringBuilder)localObject2).append(locald);
    localObject2 = ((StringBuilder)localObject2).toString();
    paramObject[0] = localObject2;
    paramObject = b.c(c);
    if (paramObject != null) {
      ((a.b)paramObject).finish();
    }
    return x.a;
    label232:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tcpermissions.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */