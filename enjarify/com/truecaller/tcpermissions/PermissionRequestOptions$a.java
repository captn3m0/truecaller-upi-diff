package com.truecaller.tcpermissions;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class PermissionRequestOptions$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    PermissionRequestOptions localPermissionRequestOptions = new com/truecaller/tcpermissions/PermissionRequestOptions;
    int i = paramParcel.readInt();
    boolean bool = true;
    int j = 0;
    if (i != 0) {
      i = 1;
    } else {
      i = 0;
    }
    int k = paramParcel.readInt();
    if (k == 0) {
      bool = false;
    }
    j = paramParcel.readInt();
    int m;
    if (j != 0)
    {
      m = paramParcel.readInt();
      paramParcel = Integer.valueOf(m);
    }
    else
    {
      m = 0;
      paramParcel = null;
    }
    localPermissionRequestOptions.<init>(i, bool, paramParcel);
    return localPermissionRequestOptions;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new PermissionRequestOptions[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tcpermissions.PermissionRequestOptions.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */