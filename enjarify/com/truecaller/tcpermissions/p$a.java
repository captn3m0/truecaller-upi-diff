package com.truecaller.tcpermissions;

import c.f;
import c.g.a.a;
import c.g.b.t;
import c.g.b.u;
import c.g.b.w;
import c.l.b;
import java.util.List;

final class p$a
{
  final List b;
  private final f d;
  
  static
  {
    c.l.g[] arrayOfg = new c.l.g[1];
    Object localObject = new c/g/b/u;
    b localb = w.a(a.class);
    ((u)localObject).<init>(localb, "grantedPermissions", "getGrantedPermissions()Ljava/util/List;");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[0] = localObject;
    a = arrayOfg;
  }
  
  public p$a(p paramp, List paramList)
  {
    b = paramList;
    paramp = new com/truecaller/tcpermissions/p$a$a;
    paramp.<init>(this);
    paramp = c.g.a((a)paramp);
    d = paramp;
  }
  
  private final List c()
  {
    return (List)d.b();
  }
  
  public final boolean a()
  {
    List localList1 = b;
    int i = localList1.size();
    List localList2 = c();
    int j = localList2.size();
    return i == j;
  }
  
  public final void b()
  {
    Object localObject1 = new String[1];
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("Broadcasting granted permissions: ");
    List localList = c();
    ((StringBuilder)localObject2).append(localList);
    localObject2 = ((StringBuilder)localObject2).toString();
    localList = null;
    localObject1[0] = localObject2;
    localObject1 = c();
    boolean bool = ((List)localObject1).isEmpty();
    if (!bool)
    {
      localObject1 = c;
      localObject2 = c();
      p.a((p)localObject1, (List)localObject2);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tcpermissions.p.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */