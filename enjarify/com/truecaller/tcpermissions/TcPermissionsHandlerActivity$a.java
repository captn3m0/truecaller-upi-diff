package com.truecaller.tcpermissions;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import c.g.b.k;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class TcPermissionsHandlerActivity$a
{
  public static void a(Context paramContext, PermissionRequestOptions paramPermissionRequestOptions, List paramList)
  {
    k.b(paramContext, "context");
    k.b(paramPermissionRequestOptions, "options");
    k.b(paramList, "permissions");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, TcPermissionsHandlerActivity.class);
    localIntent.setFlags(268435456);
    ArrayList localArrayList = new java/util/ArrayList;
    paramList = (Collection)paramList;
    localArrayList.<init>(paramList);
    localIntent.putStringArrayListExtra("permissions", localArrayList);
    paramPermissionRequestOptions = (Parcelable)paramPermissionRequestOptions;
    localIntent.putExtra("options", paramPermissionRequestOptions);
    paramContext.startActivity(localIntent);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tcpermissions.TcPermissionsHandlerActivity.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */