package com.truecaller.analytics;

import android.os.Bundle;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.facebook.GraphRequest.b;
import com.facebook.appevents.g;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import kotlinx.coroutines.ag;

final class al$c
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  al$c(Map paramMap, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    c localc = new com/truecaller/analytics/al$c;
    Map localMap = b;
    localc.<init>(localMap, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return localc;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        paramObject = g.e();
        if (paramObject == null)
        {
          paramObject = UUID.randomUUID().toString();
          g.b((String)paramObject);
        }
        paramObject = new android/os/Bundle;
        ((Bundle)paramObject).<init>();
        localObject1 = b.entrySet().iterator();
        for (;;)
        {
          boolean bool2 = ((Iterator)localObject1).hasNext();
          if (!bool2) {
            break;
          }
          Object localObject2 = (Map.Entry)((Iterator)localObject1).next();
          String str = (String)((Map.Entry)localObject2).getKey();
          localObject2 = (String)((Map.Entry)localObject2).getValue();
          ((Bundle)paramObject).putString(str, (String)localObject2);
        }
        localObject1 = (GraphRequest.b)al.c.1.a;
        g.a((Bundle)paramObject, (GraphRequest.b)localObject1);
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (c)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((c)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.al.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */