package com.truecaller.analytics;

import dagger.a.d;
import javax.inject.Provider;

public final class m
  implements d
{
  private final f a;
  private final Provider b;
  
  private m(f paramf, Provider paramProvider)
  {
    a = paramf;
    b = paramProvider;
  }
  
  public static m a(f paramf, Provider paramProvider)
  {
    m localm = new com/truecaller/analytics/m;
    localm.<init>(paramf, paramProvider);
    return localm;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */