package com.truecaller.analytics;

import dagger.a.d;
import javax.inject.Provider;

public final class i
  implements d
{
  private final f a;
  private final Provider b;
  
  private i(f paramf, Provider paramProvider)
  {
    a = paramf;
    b = paramProvider;
  }
  
  public static i a(f paramf, Provider paramProvider)
  {
    i locali = new com/truecaller/analytics/i;
    locali.<init>(paramf, paramProvider);
    return locali;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */