package com.truecaller.analytics;

import dagger.a.d;
import javax.inject.Provider;

public final class k
  implements d
{
  private final f a;
  private final Provider b;
  private final Provider c;
  
  private k(f paramf, Provider paramProvider1, Provider paramProvider2)
  {
    a = paramf;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static k a(f paramf, Provider paramProvider1, Provider paramProvider2)
  {
    k localk = new com/truecaller/analytics/k;
    localk.<init>(paramf, paramProvider1, paramProvider2);
    return localk;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */