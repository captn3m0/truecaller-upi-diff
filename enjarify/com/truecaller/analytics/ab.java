package com.truecaller.analytics;

import c.g.b.k;
import com.truecaller.log.AssertionUtil;
import com.truecaller.utils.a;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public final class ab
  implements au
{
  private final HashMap a;
  private final a b;
  private final b c;
  
  public ab(a parama, b paramb)
  {
    b = parama;
    c = paramb;
    parama = new java/util/HashMap;
    parama.<init>();
    a = parama;
  }
  
  private static String a(double paramDouble)
  {
    Object[] arrayOfObject = new Object[1];
    Double localDouble = Double.valueOf(paramDouble);
    arrayOfObject[0] = localDouble;
    return String.format("%.2f", arrayOfObject);
  }
  
  private static String a(double paramDouble, long[] paramArrayOfLong)
  {
    int i = paramArrayOfLong.length;
    int j = 0;
    while (j < i)
    {
      long l1 = paramArrayOfLong[j];
      double d = l1;
      boolean bool = paramDouble < d;
      int k;
      if (bool) {
        k = 1;
      } else {
        k = 0;
      }
      if (k != 0)
      {
        localObject = Long.valueOf(l1);
        break label70;
      }
      j += 1;
    }
    Object localObject = null;
    label70:
    if (localObject != null)
    {
      long l2 = ((Long)localObject).longValue();
      localObject = String.valueOf(l2);
      if (localObject != null) {}
    }
    else
    {
      localObject = "MAX";
    }
    return (String)localObject;
  }
  
  private final void a(e parame)
  {
    Object localObject1 = parame.b();
    Object localObject2 = null;
    String str1;
    if (localObject1 != null)
    {
      str1 = (String)((Map)localObject1).get("Event");
    }
    else
    {
      c1 = '\000';
      str1 = null;
    }
    String str2;
    if (localObject1 != null) {
      str2 = (String)((Map)localObject1).get("Type");
    } else {
      str2 = null;
    }
    Object localObject3 = parame.c();
    if (localObject3 != null)
    {
      double d = ((Double)localObject3).doubleValue();
      localObject3 = a(d);
    }
    else
    {
      localObject3 = null;
    }
    String str3;
    if (localObject1 != null) {
      str3 = (String)((Map)localObject1).get("GranularValue");
    } else {
      str3 = null;
    }
    String str4;
    if (localObject1 != null) {
      str4 = (String)((Map)localObject1).get("Count");
    } else {
      str4 = null;
    }
    String str5;
    if (localObject1 != null) {
      str5 = (String)((Map)localObject1).get("State");
    } else {
      str5 = null;
    }
    if (localObject1 != null)
    {
      localObject1 = ((Map)localObject1).get("Parameters");
      localObject2 = localObject1;
      localObject2 = (String)localObject1;
    }
    localObject1 = new String[1];
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(str1);
    char c1 = ' ';
    localStringBuilder.append(c1);
    localStringBuilder.append(str2);
    localStringBuilder.append(c1);
    localStringBuilder.append((String)localObject3);
    localStringBuilder.append("ms [");
    localStringBuilder.append(str3);
    localStringBuilder.append("] Count:");
    localStringBuilder.append(str4);
    localStringBuilder.append(c1);
    localStringBuilder.append(str5);
    localStringBuilder.append(c1);
    localStringBuilder.append((String)localObject2);
    localObject2 = localStringBuilder.toString();
    localObject1[0] = localObject2;
    c.a(parame);
  }
  
  public final String a(TimingEvent paramTimingEvent, String paramString1, String paramString2)
  {
    String str = "event";
    k.b(paramTimingEvent, str);
    boolean bool = paramTimingEvent.getUnique();
    if (bool)
    {
      str = paramTimingEvent.getEvent();
    }
    else
    {
      str = UUID.randomUUID().toString();
      localObject = "UUID.randomUUID().toString()";
      k.a(str, (String)localObject);
    }
    Object localObject = (Map)a;
    ab.a locala = new com/truecaller/analytics/ab$a;
    long l = b.c();
    locala.<init>(paramTimingEvent, paramString1, paramString2, l);
    ((Map)localObject).put(str, locala);
    return str;
  }
  
  public final void a(TimingEvent paramTimingEvent)
  {
    k.b(paramTimingEvent, "event");
    boolean bool = paramTimingEvent.getUnique();
    String[] arrayOfString = { "Only unique events can be finished without passing key" };
    AssertionUtil.isTrue(bool, arrayOfString);
    paramTimingEvent = paramTimingEvent.getEvent();
    a(paramTimingEvent, 0);
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "key");
    a.remove(paramString);
  }
  
  public final void a(String paramString, int paramInt)
  {
    k.b(paramString, "key");
    a locala = b;
    long l1 = locala.c();
    Object localObject1 = a;
    paramString = (ab.a)((HashMap)localObject1).remove(paramString);
    if (paramString != null)
    {
      long l2 = d;
      l1 -= l2;
      double d1 = l1;
      l2 = 4696837146684686336L;
      double d2 = 1000000.0D;
      Double.isNaN(d1);
      d1 /= d2;
      localObject1 = new com/truecaller/analytics/e$a;
      ((e.a)localObject1).<init>("Timing");
      String str = a.getEvent();
      ((e.a)localObject1).a("Event", str);
      str = "Full";
      ((e.a)localObject1).a("Type", str);
      Object localObject2 = b;
      if (localObject2 != null)
      {
        str = "State";
        ((e.a)localObject1).a(str, (String)localObject2);
      }
      localObject2 = c;
      if (localObject2 != null)
      {
        str = "Parameters";
        ((e.a)localObject1).a(str, (String)localObject2);
      }
      if (paramInt > 0)
      {
        localObject2 = "Count";
        ((e.a)localObject1).a((String)localObject2, paramInt);
      }
      localObject2 = a.getEventGranularity();
      if (localObject2 != null)
      {
        str = "GranularValue";
        localObject2 = a(d1, (long[])localObject2);
        ((e.a)localObject1).a(str, (String)localObject2);
      }
      localObject2 = Double.valueOf(d1);
      ((e.a)localObject1).a((Double)localObject2);
      localObject1 = ((e.a)localObject1).a();
      localObject2 = "with(AnalyticsEvent.Buil…    build()\n            }";
      k.a(localObject1, (String)localObject2);
      a((e)localObject1);
      if (paramInt > 0)
      {
        d2 = paramInt;
        Double.isNaN(d2);
        d1 /= d2;
        Object localObject3 = new com/truecaller/analytics/e$a;
        ((e.a)localObject3).<init>("Timing");
        localObject2 = a.getEvent();
        ((e.a)localObject3).a("Event", (String)localObject2);
        localObject2 = "PerItem";
        ((e.a)localObject3).a("Type", (String)localObject2);
        localObject1 = b;
        if (localObject1 != null)
        {
          localObject2 = "State";
          ((e.a)localObject3).a((String)localObject2, (String)localObject1);
        }
        localObject1 = c;
        if (localObject1 != null)
        {
          localObject2 = "Parameters";
          ((e.a)localObject3).a((String)localObject2, (String)localObject1);
        }
        paramString = a.getItemGranularity();
        if (paramString != null)
        {
          localObject1 = "GranularValue";
          paramString = a(d1, paramString);
          ((e.a)localObject3).a((String)localObject1, paramString);
        }
        paramString = Double.valueOf(d1);
        ((e.a)localObject3).a(paramString);
        paramString = ((e.a)localObject3).a();
        localObject3 = "with(AnalyticsEvent.Buil…build()\n                }";
        k.a(paramString, (String)localObject3);
        a(paramString);
      }
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.ab
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */