package com.truecaller.analytics;

public enum EventsUploadResult
{
  static
  {
    EventsUploadResult[] arrayOfEventsUploadResult = new EventsUploadResult[4];
    EventsUploadResult localEventsUploadResult = new com/truecaller/analytics/EventsUploadResult;
    localEventsUploadResult.<init>("SUCCESS", 0);
    SUCCESS = localEventsUploadResult;
    arrayOfEventsUploadResult[0] = localEventsUploadResult;
    localEventsUploadResult = new com/truecaller/analytics/EventsUploadResult;
    int i = 1;
    localEventsUploadResult.<init>("FAILURE", i);
    FAILURE = localEventsUploadResult;
    arrayOfEventsUploadResult[i] = localEventsUploadResult;
    localEventsUploadResult = new com/truecaller/analytics/EventsUploadResult;
    i = 2;
    localEventsUploadResult.<init>("QUEUED", i);
    QUEUED = localEventsUploadResult;
    arrayOfEventsUploadResult[i] = localEventsUploadResult;
    localEventsUploadResult = new com/truecaller/analytics/EventsUploadResult;
    i = 3;
    localEventsUploadResult.<init>("INVALID_PARAMS", i);
    INVALID_PARAMS = localEventsUploadResult;
    arrayOfEventsUploadResult[i] = localEventsUploadResult;
    $VALUES = arrayOfEventsUploadResult;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.EventsUploadResult
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */