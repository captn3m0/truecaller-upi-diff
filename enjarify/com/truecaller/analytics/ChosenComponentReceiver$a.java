package com.truecaller.analytics;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import c.g.b.k;

public final class ChosenComponentReceiver$a
{
  public static PendingIntent a(Context paramContext, String paramString)
  {
    k.b(paramContext, "context");
    k.b(paramString, "source");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, ChosenComponentReceiver.class);
    localIntent.putExtra("EXTRA_SOURCE", paramString);
    paramContext = PendingIntent.getBroadcast(paramContext, 0, localIntent, 134217728);
    k.a(paramContext, "PendingIntent.getBroadca…tent.FLAG_UPDATE_CURRENT)");
    return paramContext;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.ChosenComponentReceiver.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */