package com.truecaller.analytics;

import dagger.a.d;
import javax.inject.Provider;

public final class ao
  implements d
{
  private final Provider a;
  private final Provider b;
  
  private ao(Provider paramProvider1, Provider paramProvider2)
  {
    a = paramProvider1;
    b = paramProvider2;
  }
  
  public static ao a(Provider paramProvider1, Provider paramProvider2)
  {
    ao localao = new com/truecaller/analytics/ao;
    localao.<init>(paramProvider1, paramProvider2);
    return localao;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.ao
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */