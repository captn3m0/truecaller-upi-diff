package com.truecaller.analytics;

import android.os.Bundle;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.facebook.appevents.g;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import kotlinx.coroutines.ag;

final class al$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  al$a(al paramal, e parame, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/analytics/al$a;
    al localal = b;
    e locale = c;
    locala.<init>(localal, locale, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        paramObject = new android/os/Bundle;
        ((Bundle)paramObject).<init>();
        localObject1 = c.b();
        Object localObject2;
        String str;
        if (localObject1 != null)
        {
          localObject1 = ((Map)localObject1).entrySet().iterator();
          for (;;)
          {
            boolean bool2 = ((Iterator)localObject1).hasNext();
            if (!bool2) {
              break;
            }
            localObject2 = (Map.Entry)((Iterator)localObject1).next();
            str = (String)((Map.Entry)localObject2).getKey();
            localObject2 = (String)((Map.Entry)localObject2).getValue();
            ((Bundle)paramObject).putString(str, (String)localObject2);
          }
        }
        localObject1 = c.c();
        if (localObject1 == null)
        {
          localObject1 = al.a(b);
          localObject2 = c.a();
          ((g)localObject1).a((String)localObject2, (Bundle)paramObject);
        }
        else
        {
          localObject2 = al.a(b);
          str = c.a();
          double d1 = ((Double)localObject1).doubleValue();
          ((g)localObject2).a(str, d1, (Bundle)paramObject);
        }
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.al.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */