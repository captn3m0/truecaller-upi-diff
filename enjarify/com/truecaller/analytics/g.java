package com.truecaller.analytics;

import dagger.a.d;
import javax.inject.Provider;

public final class g
  implements d
{
  private final f a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  
  private g(f paramf, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramf;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
  }
  
  public static g a(f paramf, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    g localg = new com/truecaller/analytics/g;
    localg.<init>(paramf, paramProvider1, paramProvider2, paramProvider3);
    return localg;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */