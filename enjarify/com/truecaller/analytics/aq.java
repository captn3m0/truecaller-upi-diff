package com.truecaller.analytics;

import android.text.TextUtils;
import c.g.b.k;
import com.truecaller.androidactors.f;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.tracking.events.ae.a;
import com.truecaller.tracking.events.al;
import com.truecaller.tracking.events.al.a;
import com.truecaller.tracking.events.ax;
import com.truecaller.tracking.events.ax.a;
import com.truecaller.tracking.events.v;
import com.truecaller.tracking.events.v.a;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.apache.a.d.d;

public final class aq
  implements ap
{
  private final f a;
  private final b b;
  
  public aq(f paramf, b paramb)
  {
    a = paramf;
    b = paramb;
  }
  
  public final void a(e parame)
  {
    k.b(parame, "event");
    b.a(parame);
  }
  
  public final void a(String paramString1, String paramString2)
  {
    k.b(paramString1, "transport");
    k.b(paramString2, "context");
    Object localObject1 = com.truecaller.tracking.events.ae.b();
    Object localObject2 = paramString1;
    localObject2 = (CharSequence)paramString1;
    localObject1 = ((ae.a)localObject1).a((CharSequence)localObject2);
    localObject2 = paramString2;
    localObject2 = (CharSequence)paramString2;
    localObject1 = ((ae.a)localObject1).b((CharSequence)localObject2).a();
    localObject2 = (ae)a.a();
    localObject1 = (d)localObject1;
    ((ae)localObject2).a((d)localObject1);
    localObject1 = new com/truecaller/analytics/e$a;
    ((e.a)localObject1).<init>("SwitchMessageTransport");
    paramString1 = ((e.a)localObject1).a("Transport", paramString1).a("Context", paramString2).a();
    paramString2 = b;
    k.a(paramString1, "it");
    paramString2.b(paramString1);
  }
  
  public final void a(String paramString1, String paramString2, String paramString3, Participant[] paramArrayOfParticipant)
  {
    k.b(paramString1, "userInteraction");
    k.b(paramString2, "analyticsId");
    k.b(paramString3, "transportName");
    k.b(paramArrayOfParticipant, "participants");
    Object localObject1 = new java/util/ArrayList;
    int i = paramArrayOfParticipant.length;
    ((ArrayList)localObject1).<init>(i);
    localObject1 = (Collection)localObject1;
    i = paramArrayOfParticipant.length;
    int j = 0;
    while (j < i)
    {
      Object localObject2 = paramArrayOfParticipant[j];
      k.b(localObject2, "participant");
      Object localObject3 = al.b();
      boolean bool1 = ((Participant)localObject2).d();
      localObject3 = ((al.a)localObject3).a(bool1);
      Object localObject4 = (CharSequence)m;
      bool1 = TextUtils.isEmpty((CharSequence)localObject4);
      int i2 = 1;
      bool1 ^= i2;
      localObject3 = ((al.a)localObject3).b(bool1);
      int k = j;
      if (k == i2)
      {
        k = 1;
      }
      else
      {
        k = 0;
        localObject4 = null;
      }
      localObject4 = Boolean.valueOf(k);
      localObject3 = ((al.a)localObject3).a((Boolean)localObject4);
      localObject4 = Boolean.valueOf(k);
      localObject3 = ((al.a)localObject3).b((Boolean)localObject4);
      int m = j;
      int i3 = 2;
      if (m == i3)
      {
        m = 1;
      }
      else
      {
        m = 0;
        localObject4 = null;
      }
      localObject4 = Boolean.valueOf(m);
      localObject3 = ((al.a)localObject3).c((Boolean)localObject4);
      int n = q;
      int i4 = 10;
      if (n >= i4)
      {
        n = 1;
      }
      else
      {
        n = 0;
        localObject4 = null;
      }
      localObject4 = Boolean.valueOf(n);
      localObject3 = ((al.a)localObject3).d((Boolean)localObject4);
      int i1 = o & 0x40;
      boolean bool2;
      if (i1 == 0)
      {
        bool2 = false;
        localCharSequence = null;
      }
      localObject4 = Boolean.valueOf(bool2);
      localObject3 = ((al.a)localObject3).e((Boolean)localObject4);
      i1 = q;
      i1 = Math.max(0, i1);
      localObject4 = Integer.valueOf(i1);
      localObject3 = ((al.a)localObject3).a((Integer)localObject4).a();
      localObject4 = ax.b();
      CharSequence localCharSequence = (CharSequence)((Participant)localObject2).j();
      localObject4 = ((ax.a)localObject4).a(localCharSequence);
      localObject2 = (CharSequence)((Participant)localObject2).k();
      localObject2 = ((ax.a)localObject4).b((CharSequence)localObject2).a((al)localObject3).a();
      localObject3 = "com.truecaller.tracking.…nfo)\n            .build()";
      k.a(localObject2, (String)localObject3);
      ((Collection)localObject1).add(localObject2);
      j += 1;
    }
    localObject1 = (List)localObject1;
    paramArrayOfParticipant = v.b().a((List)localObject1);
    paramString1 = (CharSequence)paramString1;
    paramString1 = paramArrayOfParticipant.a(paramString1);
    paramString3 = (CharSequence)paramString3;
    paramString1 = paramString1.b(paramString3);
    paramString2 = (CharSequence)paramString2;
    paramString1 = paramString1.c(paramString2).a();
    paramString2 = (ae)a.a();
    paramString1 = (d)paramString1;
    paramString2.a(paramString1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.aq
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */