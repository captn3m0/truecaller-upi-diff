package com.truecaller.analytics;

import com.truecaller.analytics.storage.a;
import com.truecaller.analytics.storage.c;
import com.truecaller.analytics.storage.d;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import okhttp3.t;
import okhttp3.y;

final class aj
  implements ai
{
  private static final long a = TimeUnit.SECONDS.toMillis(5);
  private static final long b = TimeUnit.SECONDS.toMillis(10);
  private final int c;
  private final int d;
  private final long e;
  private final d f;
  private final a g;
  
  aj(a parama, d paramd)
  {
    g = parama;
    int i = 100;
    int j = parama.a("uploadEventsMaxBatchSize", i);
    c = j;
    j = parama.a("uploadEventsMinBatchSize", i);
    d = j;
    long l = parama.a("uploadEventsRetryJitter", 10000L);
    e = l;
    f = paramd;
  }
  
  private int a(int paramInt, long paramLong, boolean paramBoolean)
  {
    if (paramBoolean)
    {
      long l = b;
      paramBoolean = paramLong < l;
      if (!paramBoolean)
      {
        l = a;
        paramBoolean = paramLong < l;
        if (paramBoolean)
        {
          i = paramInt * 133 / 100;
          j = c;
          i = Math.min(i, j);
          break label98;
        }
        i = paramInt;
        break label98;
      }
    }
    int i = paramInt * 66 / 100;
    int j = d;
    i = Math.max(i, j);
    label98:
    if (paramInt != i)
    {
      a locala = g;
      String str = "analyticsUploadEnhancedBatchSize";
      locala.b(str, i);
    }
    return i;
  }
  
  /* Error */
  private aj.b a(s params, y paramy, ArrayList paramArrayList)
  {
    // Byte code:
    //   0: new 84	com/truecaller/analytics/aj$b
    //   3: astore 4
    //   5: aload 4
    //   7: aload_0
    //   8: iconst_0
    //   9: invokespecial 87	com/truecaller/analytics/aj$b:<init>	(Lcom/truecaller/analytics/aj;B)V
    //   12: aload_3
    //   13: invokevirtual 93	java/util/ArrayList:isEmpty	()Z
    //   16: istore 5
    //   18: iload 5
    //   20: ifeq +6 -> 26
    //   23: aload 4
    //   25: areturn
    //   26: aload_3
    //   27: invokevirtual 97	java/util/ArrayList:listIterator	()Ljava/util/ListIterator;
    //   30: astore 6
    //   32: aload 6
    //   34: invokeinterface 102 1 0
    //   39: istore 7
    //   41: iload 7
    //   43: ifeq +27 -> 70
    //   46: aload 6
    //   48: invokeinterface 106 1 0
    //   53: astore 8
    //   55: aload 8
    //   57: ifnonnull -25 -> 32
    //   60: aload 6
    //   62: invokeinterface 109 1 0
    //   67: goto -35 -> 32
    //   70: aload_1
    //   71: invokeinterface 114 1 0
    //   76: astore 6
    //   78: aload_1
    //   79: invokeinterface 117 1 0
    //   84: astore_1
    //   85: aload_1
    //   86: ifnonnull +14 -> 100
    //   89: iconst_1
    //   90: anewarray 121	java/lang/String
    //   93: iconst_0
    //   94: ldc 119
    //   96: aastore
    //   97: aload 4
    //   99: areturn
    //   100: invokestatic 127	java/lang/System:currentTimeMillis	()J
    //   103: lstore 9
    //   105: iconst_1
    //   106: istore 11
    //   108: iload 11
    //   110: anewarray 121	java/lang/String
    //   113: astore 12
    //   115: new 130	java/lang/StringBuilder
    //   118: astore 13
    //   120: aload 13
    //   122: ldc -124
    //   124: invokespecial 135	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   127: aload 13
    //   129: aload 6
    //   131: invokevirtual 139	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   134: pop
    //   135: ldc -115
    //   137: astore 14
    //   139: aload 13
    //   141: aload 14
    //   143: invokevirtual 144	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   146: pop
    //   147: aload_3
    //   148: invokevirtual 148	java/util/ArrayList:size	()I
    //   151: istore 15
    //   153: aload 13
    //   155: iload 15
    //   157: invokevirtual 151	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   160: pop
    //   161: aload 13
    //   163: invokevirtual 155	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   166: astore 13
    //   168: aload 12
    //   170: iconst_0
    //   171: aload 13
    //   173: aastore
    //   174: new 157	com/truecaller/analytics/aj$a
    //   177: astore 12
    //   179: invokestatic 162	com/truecaller/tracking/events/PacketVersionedV2:b	()Lcom/truecaller/tracking/events/PacketVersionedV2$a;
    //   182: aload_3
    //   183: invokevirtual 167	com/truecaller/tracking/events/PacketVersionedV2$a:a	(Ljava/util/List;)Lcom/truecaller/tracking/events/PacketVersionedV2$a;
    //   186: invokevirtual 170	com/truecaller/tracking/events/PacketVersionedV2$a:a	()Lcom/truecaller/tracking/events/PacketVersionedV2;
    //   189: astore_3
    //   190: aload 12
    //   192: aload_3
    //   193: invokespecial 173	com/truecaller/analytics/aj$a:<init>	(Lorg/apache/a/d/d;)V
    //   196: new 175	okhttp3/ab$a
    //   199: astore_3
    //   200: aload_3
    //   201: invokespecial 176	okhttp3/ab$a:<init>	()V
    //   204: aload_3
    //   205: ldc -78
    //   207: aload 12
    //   209: invokevirtual 181	okhttp3/ab$a:a	(Ljava/lang/String;Lokhttp3/ac;)Lokhttp3/ab$a;
    //   212: aload_1
    //   213: invokevirtual 184	okhttp3/ab$a:a	(Lokhttp3/t;)Lokhttp3/ab$a;
    //   216: astore_1
    //   217: ldc -70
    //   219: astore 12
    //   221: ldc -68
    //   223: astore 13
    //   225: aload_1
    //   226: aload 12
    //   228: aload 13
    //   230: invokevirtual 191	okhttp3/ab$a:b	(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/ab$a;
    //   233: aload 6
    //   235: invokevirtual 194	okhttp3/ab$a:a	(Lokhttp3/u;)Lokhttp3/ab$a;
    //   238: pop
    //   239: aload_3
    //   240: invokevirtual 197	okhttp3/ab$a:a	()Lokhttp3/ab;
    //   243: astore_1
    //   244: aload_2
    //   245: aload_1
    //   246: iconst_0
    //   247: invokestatic 202	okhttp3/aa:a	(Lokhttp3/y;Lokhttp3/ab;Z)Lokhttp3/aa;
    //   250: invokestatic 208	com/google/firebase/perf/network/FirebasePerfOkHttpClient:execute	(Lokhttp3/e;)Lokhttp3/ad;
    //   253: astore_1
    //   254: aconst_null
    //   255: astore_2
    //   256: invokestatic 127	java/lang/System:currentTimeMillis	()J
    //   259: lload 9
    //   261: lsub
    //   262: lstore 16
    //   264: aload 4
    //   266: lload 16
    //   268: putfield 209	com/truecaller/analytics/aj$b:b	J
    //   271: aload_1
    //   272: ifnull +34 -> 306
    //   275: aload_1
    //   276: invokevirtual 213	okhttp3/ad:c	()Z
    //   279: istore 18
    //   281: aload 4
    //   283: iload 18
    //   285: putfield 216	com/truecaller/analytics/aj$b:a	Z
    //   288: aload_1
    //   289: getfield 217	okhttp3/ad:c	I
    //   292: istore 18
    //   294: iload 18
    //   296: invokestatic 223	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   299: astore_3
    //   300: aload 4
    //   302: aload_3
    //   303: putfield 226	com/truecaller/analytics/aj$b:c	Ljava/lang/Integer;
    //   306: aload_1
    //   307: ifnull +7 -> 314
    //   310: aload_1
    //   311: invokevirtual 229	okhttp3/ad:close	()V
    //   314: aload 4
    //   316: areturn
    //   317: astore_3
    //   318: goto +6 -> 324
    //   321: astore_2
    //   322: aload_2
    //   323: athrow
    //   324: aload_1
    //   325: ifnull +27 -> 352
    //   328: aload_2
    //   329: ifnull +19 -> 348
    //   332: aload_1
    //   333: invokevirtual 229	okhttp3/ad:close	()V
    //   336: goto +16 -> 352
    //   339: astore_1
    //   340: aload_2
    //   341: aload_1
    //   342: invokevirtual 235	java/lang/Throwable:addSuppressed	(Ljava/lang/Throwable;)V
    //   345: goto +7 -> 352
    //   348: aload_1
    //   349: invokevirtual 229	okhttp3/ad:close	()V
    //   352: aload_3
    //   353: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	354	0	this	aj
    //   0	354	1	params	s
    //   0	354	2	paramy	y
    //   0	354	3	paramArrayList	ArrayList
    //   3	312	4	localb	aj.b
    //   16	3	5	bool1	boolean
    //   30	204	6	localObject1	Object
    //   39	3	7	bool2	boolean
    //   53	3	8	localObject2	Object
    //   103	157	9	l1	long
    //   106	3	11	i	int
    //   113	114	12	localObject3	Object
    //   118	111	13	localObject4	Object
    //   137	5	14	str	String
    //   151	5	15	j	int
    //   262	5	16	l2	long
    //   279	5	18	bool3	boolean
    //   292	3	18	k	int
    // Exception table:
    //   from	to	target	type
    //   322	324	317	finally
    //   256	259	321	finally
    //   266	271	321	finally
    //   275	279	321	finally
    //   283	288	321	finally
    //   288	292	321	finally
    //   294	299	321	finally
    //   302	306	321	finally
    //   332	336	339	finally
  }
  
  private boolean a(int paramInt, aj.b paramb)
  {
    Object localObject = c;
    boolean bool = true;
    if (localObject != null)
    {
      paramb = c;
      i = paramb.intValue();
      int j = 500;
      if (i >= j)
      {
        i = 1;
        break label46;
      }
    }
    int i = 0;
    paramb = null;
    label46:
    if (i != 0)
    {
      double d1 = 1000.0D;
      long l1 = 4611686018427387904L;
      double d2 = 2.0D;
      double d3 = paramInt;
      try
      {
        d3 = Math.pow(d2, d3) * d1;
        d1 = Math.random();
        l1 = e;
        d2 = l1;
        Double.isNaN(d2);
        d1 *= d2;
        d3 += d1;
        long l2 = d3;
        localObject = new String[bool];
        String str1 = "Upload postponed in ms: ";
        String str2 = String.valueOf(l2);
        str1 = str1.concat(str2);
        localObject[0] = str1;
        Thread.sleep(l2);
        return bool;
      }
      catch (InterruptedException localInterruptedException)
      {
        new String[1][0] = "Upload failed. Can't postponed execution.";
        return false;
      }
    }
    return false;
  }
  
  public final boolean a(s params, y paramy, u paramu)
  {
    aj localaj = this;
    u localu = paramu;
    try
    {
      Object localObject2 = g;
      Object localObject3 = "analyticsUploadEnhancedBatchSize";
      int i = c;
      int j = ((a)localObject2).a((String)localObject3, i);
      localObject3 = new java/util/ArrayList;
      ((ArrayList)localObject3).<init>();
      t localt = params.f();
      if (localt == null)
      {
        localObject2 = "Upload headers were not provided";
        new String[1][0] = localObject2;
        localObject2 = "MissingHeaders";
        paramu.a((String)localObject2);
        return false;
      }
      i = 0;
      localt = null;
      int k = 0;
      int i6;
      label593:
      do
      {
        Object localObject4;
        int i1;
        for (;;)
        {
          localObject4 = f;
          localObject4 = ((d)localObject4).a(j);
          m = ((List)localObject4).isEmpty();
          i1 = 1;
          if (m != 0) {
            break label614;
          }
          ((ArrayList)localObject3).clear();
          Object localObject5 = ((List)localObject4).iterator();
          for (;;)
          {
            boolean bool2 = ((Iterator)localObject5).hasNext();
            if (!bool2) {
              break;
            }
            localObject6 = ((Iterator)localObject5).next();
            localObject6 = (c)localObject6;
            localObject6 = b;
            localObject6 = ad.a((byte[])localObject6);
            if (localObject6 != null) {
              ((ArrayList)localObject3).add(localObject6);
            }
          }
          m = ((ArrayList)localObject3).isEmpty();
          if (m == 0) {
            break;
          }
          localObject5 = f;
          i2 = ((List)localObject4).size() - i1;
          localObject4 = ((List)localObject4).get(i2);
          localObject4 = (c)localObject4;
          long l1 = a;
          ((d)localObject5).a(l1);
        }
        int m = j;
        j = 0;
        localObject2 = null;
        int i2 = 0;
        Object localObject6 = null;
        int n;
        for (;;)
        {
          int i3 = 3;
          if (j >= i3) {
            break;
          }
          localObject6 = localaj.a(params, paramy, (ArrayList)localObject3);
          i4 = a;
          if (i4 != 0)
          {
            localObject2 = "Successfully uploaded";
            new String[1][0] = localObject2;
            localObject2 = f;
            int i5 = ((List)localObject4).size() - i1;
            localObject4 = ((List)localObject4).get(i5);
            localObject4 = (c)localObject4;
            l2 = a;
            ((d)localObject2).a(l2);
            k += m;
            l2 = b;
            j = localaj.a(m, l2, i1);
            i6 = ((ArrayList)localObject3).size();
            i += i6;
            localu.a(i);
            break label593;
          }
          String[] arrayOfString = new String[i1];
          StringBuilder localStringBuilder = new java/lang/StringBuilder;
          Object localObject7 = "Upload failed. Server response: ";
          localStringBuilder.<init>((String)localObject7);
          localObject7 = c;
          if (localObject7 == null) {
            localObject7 = "none";
          } else {
            localObject7 = c;
          }
          localStringBuilder.append(localObject7);
          localObject7 = localStringBuilder.toString();
          arrayOfString[0] = localObject7;
          long l2 = b;
          n = localaj.a(m, l2, false);
          localObject7 = c;
          localu.a((Integer)localObject7);
          boolean bool1 = localaj.a(j, (aj.b)localObject6);
          if (!bool1) {
            return false;
          }
          j += 1;
          i2 = i4;
          bool1 = true;
        }
        j = n;
        int i4 = i2;
        if (i4 == 0) {
          return false;
        }
        i6 = 2000;
      } while (k < i6);
      label614:
      if (i == 0)
      {
        localObject2 = "NoEvents";
        localu.a((String)localObject2);
      }
      return true;
    }
    finally {}
  }
  
  public final boolean a(s params, y paramy, ArrayList paramArrayList, u paramu)
  {
    int i = 0;
    for (;;)
    {
      int j = 3;
      if (i < j) {
        try
        {
          aj.b localb = a(params, paramy, paramArrayList);
          boolean bool2 = a;
          boolean bool3 = true;
          if (bool2)
          {
            params = "Successfully uploaded";
            new String[1][0] = params;
            int k = paramArrayList.size();
            paramu.a(k);
            return bool3;
          }
          Object localObject1 = new String[bool3];
          Object localObject2 = new java/lang/StringBuilder;
          Object localObject3 = "Upload failed. Server response: ";
          ((StringBuilder)localObject2).<init>((String)localObject3);
          localObject3 = c;
          if (localObject3 == null) {
            localObject3 = "none";
          } else {
            localObject3 = c;
          }
          ((StringBuilder)localObject2).append(localObject3);
          localObject2 = ((StringBuilder)localObject2).toString();
          localObject1[0] = localObject2;
          localObject1 = c;
          paramu.a((Integer)localObject1);
          boolean bool1 = a(i, localb);
          if (!bool1) {
            return false;
          }
          i += 1;
        }
        finally {}
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.aj
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */