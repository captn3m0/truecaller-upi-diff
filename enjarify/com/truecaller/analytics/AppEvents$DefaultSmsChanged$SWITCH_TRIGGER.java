package com.truecaller.analytics;

public enum AppEvents$DefaultSmsChanged$SWITCH_TRIGGER
{
  static
  {
    Object localObject = new com/truecaller/analytics/AppEvents$DefaultSmsChanged$SWITCH_TRIGGER;
    ((SWITCH_TRIGGER)localObject).<init>("NONE", 0);
    NONE = (SWITCH_TRIGGER)localObject;
    localObject = new com/truecaller/analytics/AppEvents$DefaultSmsChanged$SWITCH_TRIGGER;
    int i = 1;
    ((SWITCH_TRIGGER)localObject).<init>("PERSONAL_SMS", i);
    PERSONAL_SMS = (SWITCH_TRIGGER)localObject;
    localObject = new com/truecaller/analytics/AppEvents$DefaultSmsChanged$SWITCH_TRIGGER;
    int j = 2;
    ((SWITCH_TRIGGER)localObject).<init>("PERSONAL_SPAM", j);
    PERSONAL_SPAM = (SWITCH_TRIGGER)localObject;
    localObject = new com/truecaller/analytics/AppEvents$DefaultSmsChanged$SWITCH_TRIGGER;
    int k = 3;
    ((SWITCH_TRIGGER)localObject).<init>("UNKNOWN_SMS", k);
    UNKNOWN_SMS = (SWITCH_TRIGGER)localObject;
    localObject = new com/truecaller/analytics/AppEvents$DefaultSmsChanged$SWITCH_TRIGGER;
    int m = 4;
    ((SWITCH_TRIGGER)localObject).<init>("UNKNOWN_SPAM", m);
    UNKNOWN_SPAM = (SWITCH_TRIGGER)localObject;
    localObject = new SWITCH_TRIGGER[5];
    SWITCH_TRIGGER localSWITCH_TRIGGER = NONE;
    localObject[0] = localSWITCH_TRIGGER;
    localSWITCH_TRIGGER = PERSONAL_SMS;
    localObject[i] = localSWITCH_TRIGGER;
    localSWITCH_TRIGGER = PERSONAL_SPAM;
    localObject[j] = localSWITCH_TRIGGER;
    localSWITCH_TRIGGER = UNKNOWN_SMS;
    localObject[k] = localSWITCH_TRIGGER;
    localSWITCH_TRIGGER = UNKNOWN_SPAM;
    localObject[m] = localSWITCH_TRIGGER;
    $VALUES = (SWITCH_TRIGGER[])localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.AppEvents.DefaultSmsChanged.SWITCH_TRIGGER
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */