package com.truecaller.analytics;

import dagger.a.d;
import javax.inject.Provider;

public final class p
  implements d
{
  private final f a;
  private final Provider b;
  
  private p(f paramf, Provider paramProvider)
  {
    a = paramf;
    b = paramProvider;
  }
  
  public static p a(f paramf, Provider paramProvider)
  {
    p localp = new com/truecaller/analytics/p;
    localp.<init>(paramf, paramProvider);
    return localp;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */