package com.truecaller.analytics;

import android.content.ComponentName;
import android.content.Context;
import android.os.Bundle;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.log.AssertionUtil;

public final class ChosenComponentReceiver
  extends z
{
  public static final ChosenComponentReceiver.a a;
  
  static
  {
    ChosenComponentReceiver.a locala = new com/truecaller/analytics/ChosenComponentReceiver$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  protected final void a(Context paramContext, ComponentName paramComponentName, Bundle paramBundle)
  {
    String str = "context";
    k.b(paramContext, str);
    if (paramComponentName == null) {
      return;
    }
    paramContext = null;
    if (paramBundle != null)
    {
      str = "EXTRA_SOURCE";
      paramContext = paramBundle.getString(str, null);
    }
    paramBundle = new com/truecaller/analytics/e$a;
    paramBundle.<init>("ANDROID_MAIN_ShareTruecaller");
    str = "PackageSelected";
    paramComponentName = paramComponentName.getPackageName();
    paramBundle.a(str, paramComponentName);
    paramComponentName = new String[] { " Source Param cannot be null." };
    AssertionUtil.isNotNull(paramContext, paramComponentName);
    paramComponentName = "Source";
    if (paramContext == null) {
      k.a();
    }
    paramBundle.a(paramComponentName, paramContext);
    paramContext = TrueApp.y();
    k.a(paramContext, "TrueApp.getApp()");
    paramContext = paramContext.a().c();
    paramComponentName = paramBundle.a();
    k.a(paramComponentName, "event.build()");
    paramContext.a(paramComponentName);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.ChosenComponentReceiver
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */