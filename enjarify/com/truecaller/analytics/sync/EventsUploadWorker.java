package com.truecaller.analytics.sync;

import android.content.Context;
import androidx.work.ListenableWorker.a;
import androidx.work.WorkerParameters;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.b;
import com.truecaller.androidactors.w;
import com.truecaller.bp;
import com.truecaller.common.background.TrackedWorker;
import com.truecaller.log.AssertionUtil;
import com.truecaller.utils.extensions.c;
import okhttp3.y;

public final class EventsUploadWorker
  extends TrackedWorker
{
  public static final EventsUploadWorker.a e;
  public b b;
  public ae c;
  public y d;
  
  static
  {
    EventsUploadWorker.a locala = new com/truecaller/analytics/sync/EventsUploadWorker$a;
    locala.<init>((byte)0);
    e = locala;
  }
  
  public EventsUploadWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    paramContext = TrueApp.y();
    k.a(paramContext, "TrueApp.getApp()");
    paramContext.a().a(this);
  }
  
  public final b b()
  {
    b localb = b;
    if (localb == null)
    {
      String str = "analytics";
      k.a(str);
    }
    return localb;
  }
  
  public final boolean c()
  {
    return TrueApp.y().p();
  }
  
  public final ListenableWorker.a d()
  {
    Object localObject1;
    try
    {
      localObject1 = c;
      if (localObject1 == null)
      {
        localObject2 = "eventsTracker";
        k.a((String)localObject2);
      }
      localObject2 = d;
      if (localObject2 == null)
      {
        String str = "client";
        k.a(str);
      }
      localObject1 = ((ae)localObject1).a((y)localObject2);
      localObject1 = ((w)localObject1).d();
      localObject1 = (Boolean)localObject1;
      boolean bool = c.a((Boolean)localObject1);
      if (bool)
      {
        localObject1 = ListenableWorker.a.a();
        localObject2 = "Result.success()";
        k.a(localObject1, (String)localObject2);
      }
      else
      {
        localObject1 = ListenableWorker.a.b();
        localObject2 = "Result.retry()";
        k.a(localObject1, (String)localObject2);
      }
    }
    catch (InterruptedException localInterruptedException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localInterruptedException);
      localObject1 = ListenableWorker.a.c();
      Object localObject2 = "Result.failure()";
      k.a(localObject1, (String)localObject2);
    }
    return (ListenableWorker.a)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.sync.EventsUploadWorker
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */