package com.truecaller.analytics.a;

import c.g.b.k;
import c.n.m;
import java.util.UUID;

public final class b
  implements a
{
  private final com.truecaller.analytics.storage.a a;
  
  public b(com.truecaller.analytics.storage.a parama)
  {
    a = parama;
  }
  
  public final String a()
  {
    Object localObject1 = a;
    Object localObject2 = "analyticsID";
    localObject1 = ((com.truecaller.analytics.storage.a)localObject1).a((String)localObject2);
    if (localObject1 == null)
    {
      localObject1 = UUID.randomUUID().toString();
      k.a(localObject1, "UUID.randomUUID().toString()");
      localObject1 = (CharSequence)localObject1;
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      localObject2 = (Appendable)localObject2;
      int i = 0;
      int j = ((CharSequence)localObject1).length();
      while (i < j)
      {
        char c = ((CharSequence)localObject1).charAt(i);
        boolean bool = Character.isLetterOrDigit(c);
        if (bool) {
          ((Appendable)localObject2).append(c);
        }
        i += 1;
      }
      localObject1 = ((StringBuilder)localObject2).toString();
      k.a(localObject1, "filterTo(StringBuilder(), predicate).toString()");
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      i = 7;
      String str = m.a((String)localObject1, i);
      ((StringBuilder)localObject2).append(str);
      j = 45;
      ((StringBuilder)localObject2).append(j);
      localObject1 = m.b((String)localObject1, i);
      ((StringBuilder)localObject2).append((String)localObject1);
      localObject1 = ((StringBuilder)localObject2).toString();
      a((String)localObject1);
    }
    return (String)localObject1;
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "id");
    a.a("analyticsID", paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */