package com.truecaller.analytics;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.content.b;

final class an
  implements am
{
  private final Context a;
  private final LocationManager b;
  
  an(Context paramContext, LocationManager paramLocationManager)
  {
    a = paramContext;
    b = paramLocationManager;
  }
  
  public final Location a()
  {
    boolean bool = false;
    for (;;)
    {
      try
      {
        localObject1 = a;
        localObject2 = "android.permission.ACCESS_FINE_LOCATION";
        int i = b.a((Context)localObject1, (String)localObject2);
        if (i == 0)
        {
          try
          {
            localObject1 = b;
            localObject2 = "gps";
            localObject1 = ((LocationManager)localObject1).getLastKnownLocation((String)localObject2);
          }
          catch (SecurityException localSecurityException1) {}
        }
        else
        {
          i = 0;
          localObject1 = null;
        }
        localObject2 = a;
        str = "android.permission.ACCESS_COARSE_LOCATION";
        int j = b.a((Context)localObject2, str);
        if (j != 0) {}
      }
      catch (RuntimeException localRuntimeException)
      {
        Object localObject1;
        Object localObject2;
        String str;
        return null;
      }
      try
      {
        localObject2 = b;
        str = "network";
        localObject2 = ((LocationManager)localObject2).getLastKnownLocation(str);
        if (localObject1 == null)
        {
          localObject1 = localObject2;
        }
        else if (localObject2 != null)
        {
          long l1 = ((Location)localObject2).getElapsedRealtimeNanos();
          long l2 = ((Location)localObject1).getElapsedRealtimeNanos();
          l1 -= l2;
          l2 = 0L;
          bool = l1 < l2;
          if (bool) {
            localObject1 = localObject2;
          }
        }
      }
      catch (SecurityException localSecurityException2) {}
    }
    return (Location)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.an
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */