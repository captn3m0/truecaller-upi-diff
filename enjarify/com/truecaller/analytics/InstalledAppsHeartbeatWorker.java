package com.truecaller.analytics;

import android.content.Context;
import android.content.pm.PackageManager;
import androidx.work.ListenableWorker.a;
import androidx.work.WorkerParameters;
import c.a.m;
import c.g.b.k;
import c.m.i;
import c.m.l;
import com.truecaller.TrueApp;
import com.truecaller.ads.installedapps.h;
import com.truecaller.bp;
import com.truecaller.common.background.TrackedWorker;
import com.truecaller.featuretoggles.e.a;
import com.truecaller.tracking.events.av;
import com.truecaller.tracking.events.t;
import com.truecaller.tracking.events.t.a;
import e.r;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class InstalledAppsHeartbeatWorker
  extends TrackedWorker
{
  public static final InstalledAppsHeartbeatWorker.a f;
  public b b;
  public ae c;
  public com.truecaller.featuretoggles.e d;
  public com.truecaller.ads.installedapps.e e;
  private final Context g;
  
  static
  {
    InstalledAppsHeartbeatWorker.a locala = new com/truecaller/analytics/InstalledAppsHeartbeatWorker$a;
    locala.<init>((byte)0);
    f = locala;
  }
  
  public InstalledAppsHeartbeatWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    g = paramContext;
    paramContext = TrueApp.y();
    k.a(paramContext, "TrueApp.getApp()");
    paramContext.a().a(this);
  }
  
  private final void a(List paramList1, List paramList2)
  {
    paramList1 = (Iterable)paramList1;
    Object localObject1 = new java/util/ArrayList;
    int i = m.a(paramList1, 10);
    ((ArrayList)localObject1).<init>(i);
    localObject1 = (Collection)localObject1;
    paramList1 = paramList1.iterator();
    for (;;)
    {
      boolean bool = paramList1.hasNext();
      if (!bool) {
        break;
      }
      Object localObject2 = (com.truecaller.ads.installedapps.d)paramList1.next();
      av localav = new com/truecaller/tracking/events/av;
      Object localObject3 = a;
      Object localObject4 = localObject3;
      localObject4 = (CharSequence)localObject3;
      localObject3 = b;
      Object localObject5 = localObject3;
      localObject5 = (CharSequence)localObject3;
      int j = c;
      localObject3 = String.valueOf(j);
      Object localObject6 = localObject3;
      localObject6 = (CharSequence)localObject3;
      long l1 = d;
      localObject3 = String.valueOf(l1);
      Object localObject7 = localObject3;
      localObject7 = (CharSequence)localObject3;
      long l2 = e;
      localObject2 = String.valueOf(l2);
      Object localObject8 = localObject2;
      localObject8 = (CharSequence)localObject2;
      localObject3 = localav;
      localav.<init>((CharSequence)localObject4, (CharSequence)localObject5, (CharSequence)localObject6, (CharSequence)localObject7, (CharSequence)localObject8);
      ((Collection)localObject1).add(localav);
    }
    localObject1 = (List)localObject1;
    paramList1 = t.b().a((List)localObject1).b(paramList2).a();
    paramList2 = c;
    if (paramList2 == null)
    {
      localObject1 = "eventsTracker";
      k.a((String)localObject1);
    }
    paramList1 = (org.apache.a.d.d)paramList1;
    paramList2.a(paramList1);
  }
  
  public final b b()
  {
    b localb = b;
    if (localb == null)
    {
      String str = "analytics";
      k.a(str);
    }
    return localb;
  }
  
  public final boolean c()
  {
    Object localObject1 = TrueApp.y();
    boolean bool = ((TrueApp)localObject1).p();
    if (bool)
    {
      localObject1 = d;
      if (localObject1 == null)
      {
        localObject2 = "featuresRegistry";
        k.a((String)localObject2);
      }
      Object localObject2 = c;
      Object localObject3 = com.truecaller.featuretoggles.e.a;
      int i = 8;
      localObject3 = localObject3[i];
      localObject1 = ((e.a)localObject2).a((com.truecaller.featuretoggles.e)localObject1, (c.l.g)localObject3);
      bool = ((com.truecaller.featuretoggles.b)localObject1).a();
      if (bool) {
        return true;
      }
    }
    return false;
  }
  
  public final ListenableWorker.a d()
  {
    try
    {
      Object localObject1 = h.a;
      localObject1 = h.a();
      localObject1 = ((e.b)localObject1).c();
      if (localObject1 == null)
      {
        localObject1 = ListenableWorker.a.c();
        localObject3 = "Result.failure()";
        k.a(localObject1, (String)localObject3);
        return (ListenableWorker.a)localObject1;
      }
      boolean bool1 = ((r)localObject1).d();
      int k;
      if (!bool1)
      {
        i = ((r)localObject1).b();
        int j = 404;
        if (i == j)
        {
          localObject1 = ListenableWorker.a.a();
          localObject3 = "Result.success()";
          k.a(localObject1, (String)localObject3);
          return (ListenableWorker.a)localObject1;
        }
        i = 599;
        j = 500;
        k = ((r)localObject1).b();
        if ((j <= k) && (i >= k))
        {
          localObject1 = ListenableWorker.a.b();
          localObject3 = "Result.retry()";
          k.a(localObject1, (String)localObject3);
          return (ListenableWorker.a)localObject1;
        }
        localObject1 = ListenableWorker.a.c();
        localObject3 = "Result.failure()";
        k.a(localObject1, (String)localObject3);
        return (ListenableWorker.a)localObject1;
      }
      localObject1 = ((r)localObject1).e();
      localObject1 = (com.truecaller.ads.installedapps.g)localObject1;
      if (localObject1 == null)
      {
        localObject1 = ListenableWorker.a.a();
        localObject3 = "Result.success()";
        k.a(localObject1, (String)localObject3);
        return (ListenableWorker.a)localObject1;
      }
      int i = a;
      if (i <= 0)
      {
        localObject1 = ListenableWorker.a.a();
        localObject3 = "Result.success()";
        k.a(localObject1, (String)localObject3);
        return (ListenableWorker.a)localObject1;
      }
      Object localObject3 = c;
      if (localObject3 == null)
      {
        localObject1 = ListenableWorker.a.a();
        localObject3 = "Result.success()";
        k.a(localObject1, (String)localObject3);
        return (ListenableWorker.a)localObject1;
      }
      Object localObject4 = g;
      localObject4 = ((Context)localObject4).getPackageManager();
      if (localObject4 != null)
      {
        int m = 0;
        Object localObject5 = null;
        localObject4 = ((PackageManager)localObject4).getInstalledPackages(0);
        if (localObject4 != null)
        {
          localObject4 = (Iterable)localObject4;
          localObject4 = m.n((Iterable)localObject4);
          Object localObject6 = new com/truecaller/analytics/InstalledAppsHeartbeatWorker$b;
          ((InstalledAppsHeartbeatWorker.b)localObject6).<init>((List)localObject3);
          localObject6 = (c.g.a.b)localObject6;
          localObject3 = l.b((i)localObject4, (c.g.a.b)localObject6);
          localObject4 = InstalledAppsHeartbeatWorker.c.a;
          localObject4 = (c.g.a.b)localObject4;
          localObject3 = l.c((i)localObject3, (c.g.a.b)localObject4);
          localObject3 = l.d((i)localObject3);
          if (localObject3 != null)
          {
            boolean bool3 = ((List)localObject3).isEmpty();
            localObject6 = null;
            if (bool3)
            {
              i = 0;
              localObject3 = null;
            }
            if (localObject3 != null)
            {
              localObject4 = e;
              if (localObject4 == null)
              {
                localObject7 = "installedPackagesDao";
                k.a((String)localObject7);
              }
              localObject4 = ((com.truecaller.ads.installedapps.e)localObject4).a();
              Object localObject7 = localObject3;
              localObject7 = (Iterable)localObject3;
              Object localObject8 = new java/util/ArrayList;
              ((ArrayList)localObject8).<init>();
              localObject8 = (Collection)localObject8;
              localObject7 = ((Iterable)localObject7).iterator();
              Object localObject10;
              for (;;)
              {
                boolean bool4 = ((Iterator)localObject7).hasNext();
                if (!bool4) {
                  break;
                }
                Object localObject9 = ((Iterator)localObject7).next();
                localObject10 = localObject9;
                localObject10 = (com.truecaller.ads.installedapps.d)localObject9;
                boolean bool5 = ((List)localObject4).contains(localObject10);
                if (!bool5) {
                  ((Collection)localObject8).add(localObject9);
                }
              }
              localObject8 = (List)localObject8;
              localObject8 = (Iterable)localObject8;
              int i2 = a;
              localObject7 = m.d((Iterable)localObject8, i2);
              localObject3 = (Iterable)localObject3;
              localObject8 = new java/util/ArrayList;
              int n = 10;
              int i1 = m.a((Iterable)localObject3, n);
              ((ArrayList)localObject8).<init>(i1);
              localObject8 = (Collection)localObject8;
              localObject3 = ((Iterable)localObject3).iterator();
              boolean bool6;
              for (;;)
              {
                bool6 = ((Iterator)localObject3).hasNext();
                if (!bool6) {
                  break;
                }
                localObject10 = ((Iterator)localObject3).next();
                localObject10 = (com.truecaller.ads.installedapps.d)localObject10;
                localObject10 = a;
                ((Collection)localObject8).add(localObject10);
              }
              localObject8 = (List)localObject8;
              localObject4 = (Iterable)localObject4;
              localObject3 = new java/util/ArrayList;
              ((ArrayList)localObject3).<init>();
              localObject3 = (Collection)localObject3;
              localObject4 = ((Iterable)localObject4).iterator();
              for (;;)
              {
                bool6 = ((Iterator)localObject4).hasNext();
                if (!bool6) {
                  break;
                }
                localObject10 = ((Iterator)localObject4).next();
                Object localObject11 = localObject10;
                localObject11 = (com.truecaller.ads.installedapps.d)localObject10;
                localObject11 = a;
                boolean bool7 = ((List)localObject8).contains(localObject11);
                if (!bool7) {
                  ((Collection)localObject3).add(localObject10);
                }
              }
              localObject3 = (List)localObject3;
              localObject4 = e;
              if (localObject4 == null)
              {
                localObject8 = "installedPackagesDao";
                k.a((String)localObject8);
              }
              ((com.truecaller.ads.installedapps.e)localObject4).a((List)localObject7);
              localObject4 = e;
              if (localObject4 == null)
              {
                localObject8 = "installedPackagesDao";
                k.a((String)localObject8);
              }
              ((com.truecaller.ads.installedapps.e)localObject4).b((List)localObject3);
              localObject7 = (Iterable)localObject7;
              k = b;
              localObject1 = m.e((Iterable)localObject7, k);
              localObject1 = (Iterable)localObject1;
              localObject1 = ((Iterable)localObject1).iterator();
              for (;;)
              {
                bool3 = ((Iterator)localObject1).hasNext();
                if (!bool3) {
                  break;
                }
                localObject4 = ((Iterator)localObject1).next();
                localObject4 = (List)localObject4;
                if (m == 0)
                {
                  localObject5 = localObject3;
                  localObject5 = (Iterable)localObject3;
                  localObject7 = new java/util/ArrayList;
                  int i3 = m.a((Iterable)localObject5, n);
                  ((ArrayList)localObject7).<init>(i3);
                  localObject7 = (Collection)localObject7;
                  localObject5 = ((Iterable)localObject5).iterator();
                  for (;;)
                  {
                    boolean bool8 = ((Iterator)localObject5).hasNext();
                    if (!bool8) {
                      break;
                    }
                    localObject8 = ((Iterator)localObject5).next();
                    localObject8 = (com.truecaller.ads.installedapps.d)localObject8;
                    localObject8 = a;
                    ((Collection)localObject7).add(localObject8);
                  }
                  localObject7 = (List)localObject7;
                  a((List)localObject4, (List)localObject7);
                  m = 1;
                }
                else
                {
                  a((List)localObject4, null);
                }
              }
              localObject1 = ListenableWorker.a.a();
              localObject3 = "Result.success()";
              k.a(localObject1, (String)localObject3);
              return (ListenableWorker.a)localObject1;
            }
          }
        }
      }
      localObject1 = ListenableWorker.a.a();
      localObject3 = "Result.success()";
      k.a(localObject1, (String)localObject3);
      return (ListenableWorker.a)localObject1;
    }
    catch (Exception localException)
    {
      boolean bool2 = localException instanceof RuntimeException;
      if (!bool2)
      {
        bool2 = localException instanceof IOException;
        if (!bool2)
        {
          localObject2 = (Throwable)localException;
          com.truecaller.log.d.a((Throwable)localObject2);
        }
      }
      Object localObject2 = ListenableWorker.a.c();
      k.a(localObject2, "Result.failure()");
      return (ListenableWorker.a)localObject2;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.InstalledAppsHeartbeatWorker
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */