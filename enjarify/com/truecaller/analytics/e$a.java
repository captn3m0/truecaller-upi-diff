package com.truecaller.analytics;

import java.util.HashMap;
import java.util.Map;

public final class e$a
{
  public Double a;
  private final String b;
  private Map c;
  
  public e$a(String paramString)
  {
    b = paramString;
  }
  
  public final a a(Double paramDouble)
  {
    a = paramDouble;
    return this;
  }
  
  public final a a(String paramString, int paramInt)
  {
    String str = String.valueOf(paramInt);
    return a(paramString, str);
  }
  
  public final a a(String paramString, long paramLong)
  {
    String str = String.valueOf(paramLong);
    return a(paramString, str);
  }
  
  public final a a(String paramString1, String paramString2)
  {
    Object localObject = c;
    if (localObject == null)
    {
      localObject = new java/util/HashMap;
      ((HashMap)localObject).<init>();
      c = ((Map)localObject);
    }
    c.put(paramString1, paramString2);
    return this;
  }
  
  public final a a(String paramString, boolean paramBoolean)
  {
    String str = String.valueOf(paramBoolean);
    return a(paramString, str);
  }
  
  public final e a()
  {
    e.a.a locala = new com/truecaller/analytics/e$a$a;
    String str = b;
    Double localDouble = a;
    Map localMap = c;
    locala.<init>(str, localDouble, localMap, (byte)0);
    return locala;
  }
  
  public final void a(String paramString)
  {
    Map localMap = c;
    if (localMap != null) {
      localMap.remove(paramString);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.e.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */