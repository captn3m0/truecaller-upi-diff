package com.truecaller.analytics;

public final class R$id
{
  public static final int action0 = 2131361821;
  public static final int action_container = 2131361851;
  public static final int action_divider = 2131361862;
  public static final int action_image = 2131361871;
  public static final int action_text = 2131361938;
  public static final int actions = 2131361945;
  public static final int adjust_height = 2131361997;
  public static final int adjust_width = 2131361998;
  public static final int async = 2131362051;
  public static final int auto = 2131362056;
  public static final int blocking = 2131362124;
  public static final int bottom = 2131362131;
  public static final int cancel_action = 2131362429;
  public static final int chronometer = 2131362468;
  public static final int dark = 2131362653;
  public static final int end = 2131362978;
  public static final int end_padder = 2131362980;
  public static final int forever = 2131363142;
  public static final int icon = 2131363301;
  public static final int icon_group = 2131363304;
  public static final int icon_only = 2131363307;
  public static final int info = 2131363451;
  public static final int italic = 2131363495;
  public static final int left = 2131363629;
  public static final int light = 2131363636;
  public static final int line1 = 2131363637;
  public static final int line3 = 2131363638;
  public static final int media_actions = 2131363732;
  public static final int none = 2131363812;
  public static final int normal = 2131363813;
  public static final int notification_background = 2131363818;
  public static final int notification_main_column = 2131363820;
  public static final int notification_main_column_container = 2131363821;
  public static final int right = 2131364176;
  public static final int right_icon = 2131364178;
  public static final int right_side = 2131364179;
  public static final int standard = 2131364584;
  public static final int start = 2131364585;
  public static final int status_bar_latest_event_content = 2131364594;
  public static final int tag_transition_group = 2131364660;
  public static final int tag_unhandled_key_event_manager = 2131364661;
  public static final int tag_unhandled_key_listeners = 2131364662;
  public static final int text = 2131364681;
  public static final int text2 = 2131364683;
  public static final int time = 2131364859;
  public static final int title = 2131364884;
  public static final int top = 2131364929;
  public static final int wide = 2131365541;
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.R.id
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */