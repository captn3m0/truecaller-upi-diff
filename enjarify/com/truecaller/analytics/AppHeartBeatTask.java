package com.truecaller.analytics;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.telephony.CellIdentityCdma;
import android.telephony.CellIdentityGsm;
import android.telephony.CellIdentityLte;
import android.telephony.CellIdentityWcdma;
import android.telephony.CellInfo;
import android.telephony.CellInfoCdma;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoWcdma;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import c.g.b.k;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.truecaller.TrueApp;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.w;
import com.truecaller.bp;
import com.truecaller.common.account.r;
import com.truecaller.common.background.PersistentBackgroundTask;
import com.truecaller.common.background.PersistentBackgroundTask.RunResult;
import com.truecaller.common.background.e;
import com.truecaller.common.h.aa;
import com.truecaller.common.h.ac;
import com.truecaller.common.h.am;
import com.truecaller.common.h.c;
import com.truecaller.log.AssertionUtil;
import com.truecaller.multisim.SimInfo;
import com.truecaller.multisim.h;
import com.truecaller.old.data.access.Settings;
import com.truecaller.tracking.events.aj;
import com.truecaller.tracking.events.aj.a;
import com.truecaller.tracking.events.an;
import com.truecaller.tracking.events.an.a;
import com.truecaller.tracking.events.ap;
import com.truecaller.tracking.events.ap.a;
import com.truecaller.tracking.events.aq;
import com.truecaller.tracking.events.aq.a;
import com.truecaller.tracking.events.ar.a;
import com.truecaller.tracking.events.as;
import com.truecaller.tracking.events.as.a;
import com.truecaller.tracking.events.au;
import com.truecaller.tracking.events.au.a;
import com.truecaller.tracking.events.aw;
import com.truecaller.tracking.events.aw.a;
import com.truecaller.tracking.events.az;
import com.truecaller.tracking.events.az.a;
import com.truecaller.tracking.events.o;
import com.truecaller.tracking.events.o.a;
import com.truecaller.utils.i;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import okhttp3.y;
import org.apache.a.d.d;

public final class AppHeartBeatTask
  extends PersistentBackgroundTask
{
  public static final AppHeartBeatTask.a k;
  public f a;
  public com.truecaller.calling.ar b;
  public i c;
  public h d;
  public com.truecaller.common.h.u e;
  public ac f;
  public r g;
  public b h;
  public c i;
  public y j;
  
  static
  {
    AppHeartBeatTask.a locala = new com/truecaller/analytics/AppHeartBeatTask$a;
    locala.<init>((byte)0);
    k = locala;
  }
  
  public AppHeartBeatTask()
  {
    TrueApp localTrueApp = TrueApp.y();
    k.a(localTrueApp, "TrueApp.getApp()");
    localTrueApp.a().a(this);
  }
  
  private final o a(Context paramContext, String paramString)
  {
    o.a locala = o.b();
    Object localObject1 = paramContext.getSystemService("phone");
    Object localObject5;
    int n;
    int i2;
    if (localObject1 != null)
    {
      localObject1 = (TelephonyManager)localObject1;
      paramString = (CharSequence)paramString;
      paramString = locala.a(paramString);
      localObject2 = paramContext.getResources();
      k.a(localObject2, "context.resources");
      localObject2 = ((Resources)localObject2).getDisplayMetrics();
      Object localObject3 = an.b();
      Object localObject4 = (CharSequence)Build.MODEL;
      localObject4 = ((an.a)localObject3).b((CharSequence)localObject4);
      localObject5 = (CharSequence)Build.MANUFACTURER;
      localObject4 = ((an.a)localObject4).a((CharSequence)localObject5);
      int m = widthPixels;
      localObject4 = ((an.a)localObject4).b(m);
      m = heightPixels;
      localObject4 = ((an.a)localObject4).a(m);
      n = densityDpi;
      ((an.a)localObject4).c(n);
      localObject2 = "android.permission.READ_PHONE_STATE";
      n = android.support.v4.content.b.a(paramContext, (String)localObject2);
      localObject4 = null;
      if (n == 0)
      {
        localObject2 = paramContext.getSystemService("phone");
        if (localObject2 != null)
        {
          localObject2 = am.n(((TelephonyManager)localObject2).getDeviceId());
        }
        else
        {
          paramContext = new c/u;
          paramContext.<init>("null cannot be cast to non-null type android.telephony.TelephonyManager");
          throw paramContext;
        }
      }
      else
      {
        n = 0;
        localObject2 = null;
      }
      k.a(localObject3, "deviceInfo");
      localObject2 = (CharSequence)localObject2;
      ((an.a)localObject3).c((CharSequence)localObject2);
      localObject2 = ((an.a)localObject3).a();
      k.a(localObject2, "deviceInfo.build()");
      paramString = paramString.a((an)localObject2);
      localObject2 = au.b();
      localObject3 = (CharSequence)"Android";
      localObject2 = ((au.a)localObject2).a((CharSequence)localObject3);
      localObject3 = (CharSequence)Build.VERSION.RELEASE;
      localObject2 = ((au.a)localObject2).b((CharSequence)localObject3).a();
      paramString = paramString.a((au)localObject2);
      localObject2 = f(paramContext);
      paramString = paramString.a((as)localObject2);
      localObject2 = g(paramContext);
      paramString.a((aq)localObject2);
      int i1 = 1;
      n = 3;
      i2 = 0;
      localObject3 = null;
      localObject5 = "android.permission.ACCESS_COARSE_LOCATION";
      try
      {
        m = android.support.v4.content.b.a(paramContext, (String)localObject5);
        if (m == 0)
        {
          localObject5 = "heartBeat";
          k.a(locala, (String)localObject5);
          float f1;
          try
          {
            localObject5 = ((TelephonyManager)localObject1).getAllCellInfo();
          }
          catch (SecurityException localSecurityException1)
          {
            m = 0;
            localObject5 = null;
            f1 = 0.0F;
          }
          if (localObject5 == null)
          {
            localObject5 = ((TelephonyManager)localObject1).getCellLocation();
            boolean bool2 = localObject5 instanceof GsmCellLocation;
            int i6;
            if (bool2)
            {
              localObject6 = ((TelephonyManager)localObject1).getNetworkOperator();
              if (localObject6 != null)
              {
                i6 = ((String)localObject6).length();
                if (i6 > n)
                {
                  try
                  {
                    localObject7 = ((String)localObject6).substring(0, n);
                    localObject8 = "(this as java.lang.Strin…ing(startIndex, endIndex)";
                    k.a(localObject7, (String)localObject8);
                    i6 = Integer.parseInt((String)localObject7);
                    i3 = 0;
                  }
                  catch (NumberFormatException localNumberFormatException1)
                  {
                    try
                    {
                      localObject6 = ((String)localObject6).substring(n);
                      localObject8 = "(this as java.lang.String).substring(startIndex)";
                      k.a(localObject6, (String)localObject8);
                      i3 = Integer.parseInt((String)localObject6);
                    }
                    catch (NumberFormatException localNumberFormatException2)
                    {
                      Object localObject7;
                      Object localObject8;
                      int i3;
                      float f2;
                      float f3;
                      Object localObject9;
                      int i11;
                      boolean bool3;
                      int i4;
                      int i12;
                      boolean bool1;
                      int i15;
                      boolean bool4;
                      boolean bool9;
                      String str;
                      int i13;
                      int i14;
                      int i5;
                      int i10;
                      for (;;) {}
                    }
                    localNumberFormatException1;
                    i6 = 0;
                    f2 = 0.0F;
                    localObject7 = null;
                  }
                  f3 = 0.0F;
                  localObject6 = null;
                  break label533;
                }
              }
              i3 = 0;
              f3 = 0.0F;
              localObject6 = null;
              i6 = 0;
              f2 = 0.0F;
              localObject7 = null;
              label533:
              localObject8 = ap.b();
              localObject9 = localObject5;
              localObject9 = (GsmCellLocation)localObject5;
              i11 = ((GsmCellLocation)localObject9).getCid();
              localObject8 = ((ap.a)localObject8).c(i11);
              localObject5 = (GsmCellLocation)localObject5;
              m = ((GsmCellLocation)localObject5).getLac();
              localObject5 = ((ap.a)localObject8).d(m);
              localObject5 = ((ap.a)localObject5).b(i3);
              localObject5 = ((ap.a)localObject5).a(i6);
              localObject5 = ((ap.a)localObject5).a();
              locala.a((ap)localObject5);
            }
            else
            {
              bool3 = localObject5 instanceof CdmaCellLocation;
              if (bool3)
              {
                localObject6 = aj.b();
                localObject7 = localObject5;
                localObject7 = (CdmaCellLocation)localObject5;
                i6 = ((CdmaCellLocation)localObject7).getBaseStationLatitude();
                f2 = i6;
                localObject6 = ((aj.a)localObject6).a(f2);
                localObject5 = (CdmaCellLocation)localObject5;
                m = ((CdmaCellLocation)localObject5).getBaseStationLongitude();
                f1 = m;
                localObject5 = ((aj.a)localObject6).b(f1);
                localObject5 = ((aj.a)localObject5).a();
                locala.a((aj)localObject5);
              }
            }
          }
          else
          {
            localObject5 = ((List)localObject5).iterator();
            do
            {
              boolean bool8;
              do
              {
                do
                {
                  do
                  {
                    do
                    {
                      do
                      {
                        bool3 = ((Iterator)localObject5).hasNext();
                        if (!bool3) {
                          break;
                        }
                        localObject6 = ((Iterator)localObject5).next();
                        localObject6 = (CellInfo)localObject6;
                        bool5 = ((CellInfo)localObject6).isRegistered();
                      } while (!bool5);
                      boolean bool5 = localObject6 instanceof CellInfoGsm;
                      if (!bool5) {
                        break;
                      }
                      localObject6 = (CellInfoGsm)localObject6;
                      localObject6 = ((CellInfoGsm)localObject6).getCellIdentity();
                    } while (localObject6 == null);
                    localObject5 = ap.b();
                    int i7 = ((CellIdentityGsm)localObject6).getMcc();
                    localObject5 = ((ap.a)localObject5).a(i7);
                    i7 = ((CellIdentityGsm)localObject6).getMnc();
                    localObject5 = ((ap.a)localObject5).b(i7);
                    i7 = ((CellIdentityGsm)localObject6).getLac();
                    localObject5 = ((ap.a)localObject5).d(i7);
                    i4 = ((CellIdentityGsm)localObject6).getCid();
                    localObject5 = ((ap.a)localObject5).c(i4);
                    localObject5 = ((ap.a)localObject5).a();
                    locala.a((ap)localObject5);
                    break label1248;
                    boolean bool6 = localObject6 instanceof CellInfoLte;
                    if (!bool6) {
                      break;
                    }
                    localObject6 = (CellInfoLte)localObject6;
                    localObject6 = ((CellInfoLte)localObject6).getCellIdentity();
                  } while (localObject6 == null);
                  localObject5 = com.truecaller.tracking.events.ar.b();
                  int i8 = ((CellIdentityLte)localObject6).getMcc();
                  localObject5 = ((ar.a)localObject5).a(i8);
                  i8 = ((CellIdentityLte)localObject6).getMnc();
                  localObject5 = ((ar.a)localObject5).b(i8);
                  i8 = ((CellIdentityLte)localObject6).getCi();
                  localObject5 = ((ar.a)localObject5).c(i8);
                  i4 = ((CellIdentityLte)localObject6).getTac();
                  localObject5 = ((ar.a)localObject5).d(i4);
                  localObject5 = ((ar.a)localObject5).a();
                  locala.a((com.truecaller.tracking.events.ar)localObject5);
                  break label1248;
                  boolean bool7 = localObject6 instanceof CellInfoCdma;
                  if (!bool7) {
                    break;
                  }
                  localObject6 = (CellInfoCdma)localObject6;
                  localObject6 = ((CellInfoCdma)localObject6).getCellIdentity();
                } while (localObject6 == null);
                localObject5 = aj.b();
                int i9 = ((CellIdentityCdma)localObject6).getLongitude();
                f2 = i9;
                localObject5 = ((aj.a)localObject5).a(f2);
                i4 = ((CellIdentityCdma)localObject6).getLatitude();
                f3 = i4;
                localObject5 = ((aj.a)localObject5).b(f3);
                localObject5 = ((aj.a)localObject5).a();
                locala.a((aj)localObject5);
                break;
                bool8 = localObject6 instanceof CellInfoWcdma;
              } while (!bool8);
              localObject6 = (CellInfoWcdma)localObject6;
              localObject6 = ((CellInfoWcdma)localObject6).getCellIdentity();
              if (localObject6 == null)
              {
                i4 = 0;
                f3 = 0.0F;
                localObject6 = null;
              }
              else
              {
                localObject7 = ap.b();
                i12 = ((CellIdentityWcdma)localObject6).getMcc();
                localObject7 = ((ap.a)localObject7).a(i12);
                i12 = ((CellIdentityWcdma)localObject6).getMnc();
                localObject7 = ((ap.a)localObject7).b(i12);
                i12 = ((CellIdentityWcdma)localObject6).getLac();
                localObject7 = ((ap.a)localObject7).d(i12);
                i4 = ((CellIdentityWcdma)localObject6).getCid();
                localObject6 = ((ap.a)localObject7).c(i4);
                localObject6 = ((ap.a)localObject6).a();
                locala.a((ap)localObject6);
                i4 = 1;
                f3 = Float.MIN_VALUE;
              }
            } while (i4 == 0);
          }
        }
        label1248:
        i5 = paramString.length();
      }
      catch (RuntimeException|SecurityException localRuntimeException)
      {
        try
        {
          paramContext = AdvertisingIdClient.getAdvertisingIdInfo(paramContext);
          if (paramContext != null)
          {
            bool1 = paramContext.isLimitAdTrackingEnabled();
            if (!bool1)
            {
              paramContext = paramContext.getId();
              paramContext = am.n(paramContext);
            }
          }
        }
        catch (GooglePlayServicesNotAvailableException|GooglePlayServicesRepairableException|IOException|SecurityException localGooglePlayServicesNotAvailableException)
        {
          paramContext = null;
        }
        k.a(locala, "heartBeat");
        paramContext = (CharSequence)paramContext;
        locala.b(paramContext);
        paramContext = new java/util/ArrayList;
        paramContext.<init>();
        localObject5 = d;
        if (localObject5 == null)
        {
          localObject6 = "multiSimManager";
          k.a((String)localObject6);
        }
        bool1 = ((h)localObject5).j();
        if (bool1)
        {
          i15 = 2;
          locala.a(i15);
          localObject1 = d;
          if (localObject1 == null)
          {
            localObject5 = "multiSimManager";
            k.a((String)localObject5);
          }
          localObject1 = ((h)localObject1).h();
          k.a(localObject1, "multiSimManager.allSimInfos");
          localObject5 = b;
          if (localObject5 == null)
          {
            localObject6 = "simSelectionHelper";
            k.a((String)localObject6);
          }
          localObject5 = ((com.truecaller.calling.ar)localObject5).e();
          localObject1 = ((List)localObject1).iterator();
          for (;;)
          {
            bool4 = ((Iterator)localObject1).hasNext();
            if (!bool4) {
              break;
            }
            localObject6 = (SimInfo)((Iterator)localObject1).next();
            localObject7 = az.b();
            k.a(localObject7, "simInfo");
            localObject8 = (CharSequence)am.n(d);
            ((az.a)localObject7).a((CharSequence)localObject8);
            localObject8 = (CharSequence)e;
            i12 = am.f((CharSequence)localObject8);
            i11 = 4;
            if (i12 >= i11)
            {
              localObject8 = e;
              localObject9 = "info.mccMnc";
              k.a(localObject8, (String)localObject9);
              if (localObject8 != null)
              {
                localObject8 = ((String)localObject8).substring(0, n);
                k.a(localObject8, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                i12 = am.f((String)localObject8);
                ((az.a)localObject7).a(i12);
                localObject8 = e;
                localObject9 = "info.mccMnc";
                k.a(localObject8, (String)localObject9);
                if (localObject8 != null)
                {
                  localObject8 = ((String)localObject8).substring(n);
                  localObject9 = "(this as java.lang.String).substring(startIndex)";
                  k.a(localObject8, (String)localObject9);
                  i12 = am.f((String)localObject8);
                  ((az.a)localObject7).b(i12);
                }
                else
                {
                  paramContext = new c/u;
                  paramContext.<init>("null cannot be cast to non-null type java.lang.String");
                  throw paramContext;
                }
              }
              else
              {
                paramContext = new c/u;
                paramContext.<init>("null cannot be cast to non-null type java.lang.String");
                throw paramContext;
              }
            }
            else
            {
              ((az.a)localObject7).a(0);
              ((az.a)localObject7).b(0);
            }
            bool9 = k.a(b, localObject5);
            ((az.a)localObject7).a(bool9);
            ((az.a)localObject7).a();
            localObject8 = (CharSequence)c;
            bool9 = TextUtils.isEmpty((CharSequence)localObject8);
            if (!bool9)
            {
              localObject8 = e;
              if (localObject8 == null)
              {
                localObject9 = "phoneNumberHelper";
                k.a((String)localObject9);
              }
              localObject9 = c;
              if (localObject9 == null) {
                k.a();
              }
              k.a(localObject9, "info.phoneNumber!!");
              localObject6 = b;
              str = "info.simToken";
              k.a(localObject6, str);
              localObject6 = ((com.truecaller.common.h.u)localObject8).b((String)localObject9, (String)localObject6);
              if (localObject6 != null)
              {
                i13 = ((String)localObject6).length();
                if (i13 > i1)
                {
                  i14 = ((String)localObject6).charAt(0);
                  i11 = 43;
                  if (i14 == i11)
                  {
                    localObject6 = ((String)localObject6).substring(i1);
                    localObject8 = "(this as java.lang.String).substring(startIndex)";
                    k.a(localObject6, (String)localObject8);
                  }
                }
              }
              localObject6 = (CharSequence)localObject6;
              ((az.a)localObject7).b((CharSequence)localObject6);
            }
            else
            {
              ((az.a)localObject7).b(null);
            }
            localObject6 = ((az.a)localObject7).b();
            paramContext.add(localObject6);
          }
        }
        locala.a(i1);
        localObject5 = az.b();
        k.a(localObject5, "simInfo");
        ((az.a)localObject5).a(i1);
        paramString = (CharSequence)((TelephonyManager)localObject1).getSimOperatorName();
        ((az.a)localObject5).a(paramString);
        paramString = ((TelephonyManager)localObject1).getSimOperator();
        localObject6 = paramString;
        localObject6 = (CharSequence)paramString;
        bool4 = TextUtils.isEmpty((CharSequence)localObject6);
        if (bool4) {
          break label2089;
        }
      }
      i10 = 5;
      f2 = 7.0E-45F;
      if (i5 >= i10) {
        localObject6 = "mccmnc";
      }
    }
    for (;;)
    {
      try
      {
        k.a(paramString, (String)localObject6);
        if (paramString != null)
        {
          localObject6 = paramString.substring(0, n);
          localObject7 = "(this as java.lang.Strin…ing(startIndex, endIndex)";
          k.a(localObject6, (String)localObject7);
          i5 = Integer.parseInt((String)localObject6);
        }
      }
      catch (NumberFormatException localNumberFormatException3)
      {
        boolean bool10;
        continue;
      }
      try
      {
        paramString = paramString.substring(n);
        localObject2 = "(this as java.lang.String).substring(startIndex)";
        k.a(paramString, (String)localObject2);
        i2 = Integer.parseInt(paramString);
      }
      catch (NumberFormatException localNumberFormatException4) {}
    }
    paramString = new c/u;
    Object localObject2 = "null cannot be cast to non-null type java.lang.String";
    paramString.<init>((String)localObject2);
    throw paramString;
    i5 = 0;
    f3 = 0.0F;
    Object localObject6 = null;
    break label2098;
    label2089:
    i5 = 0;
    f3 = 0.0F;
    localObject6 = null;
    label2098:
    ((az.a)localObject5).a(i5);
    ((az.a)localObject5).b(i2);
    ((az.a)localObject5).a();
    try
    {
      paramString = ((TelephonyManager)localObject1).getLine1Number();
      localObject1 = paramString;
      localObject1 = (CharSequence)paramString;
      bool10 = TextUtils.isEmpty((CharSequence)localObject1);
      if (!bool10)
      {
        paramString = aa.c(paramString, null);
        paramString = (CharSequence)paramString;
        ((az.a)localObject5).b(paramString);
      }
      else
      {
        ((az.a)localObject5).b(null);
      }
    }
    catch (SecurityException localSecurityException2)
    {
      ((az.a)localObject5).b(null);
    }
    paramString = ((az.a)localObject5).b();
    paramContext.add(paramString);
    paramContext = (List)paramContext;
    locala.a(paramContext);
    paramContext = aw.b();
    paramString = i;
    if (paramString == null)
    {
      localObject1 = "buildHelper";
      k.a((String)localObject1);
    }
    paramString = paramString.g();
    if (paramString == null) {
      paramString = "<null>";
    }
    paramString = (CharSequence)paramString;
    paramContext.a(paramString);
    paramString = i;
    if (paramString == null)
    {
      localObject1 = "buildHelper";
      k.a((String)localObject1);
    }
    paramString = paramString.i();
    if (paramString == null) {
      paramString = "<null>";
    }
    paramString = (CharSequence)paramString;
    paramContext.b(paramString);
    paramContext = paramContext.a();
    locala.a(paramContext);
    paramContext = locala.a();
    k.a(paramContext, "heartBeat.build()");
    return paramContext;
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type android.telephony.TelephonyManager");
    throw paramContext;
  }
  
  private final void a(EventsUploadResult paramEventsUploadResult, Context paramContext)
  {
    if (paramEventsUploadResult != null)
    {
      localObject = v.a;
      int m = paramEventsUploadResult.ordinal();
      m = localObject[m];
      switch (m)
      {
      default: 
        break;
      case 3: 
        paramEventsUploadResult = "InvalidParams";
        break;
      case 2: 
        paramEventsUploadResult = "Queued";
        break;
      case 1: 
        paramEventsUploadResult = "Success";
        break;
      }
    }
    paramEventsUploadResult = "Failure";
    long l1 = c(paramContext);
    long l2 = 0L;
    boolean bool = l1 < l2;
    if (bool)
    {
      paramContext = TimeUnit.MILLISECONDS;
      l2 = System.currentTimeMillis() - l1;
      l2 = paramContext.toMinutes(l2);
    }
    paramContext = h;
    if (paramContext == null)
    {
      localObject = "analytics";
      k.a((String)localObject);
    }
    Object localObject = new com/truecaller/analytics/e$a;
    ((e.a)localObject).<init>("AppHeartBeatTask");
    paramEventsUploadResult = ((e.a)localObject).a("Result", paramEventsUploadResult).a("TriggerPeriodMinutes", l2).a();
    k.a(paramEventsUploadResult, "AnalyticsEvent.Builder(A…\n                .build()");
    paramContext.b(paramEventsUploadResult);
  }
  
  public static final void a(com.truecaller.common.background.b paramb)
  {
    k.b(paramb, "scheduler");
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putString("beatType", "upgrade");
    paramb.b(10028, localBundle);
  }
  
  public static final void b(com.truecaller.common.background.b paramb)
  {
    k.b(paramb, "scheduler");
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putString("beatType", "firstactivation");
    paramb.b(10028, localBundle);
  }
  
  public static final void c(com.truecaller.common.background.b paramb)
  {
    k.b(paramb, "scheduler");
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putString("beatType", "active");
    paramb.b(10028, localBundle);
  }
  
  private final as f(Context paramContext)
  {
    as.a locala = as.b();
    k.a(locala, "builder");
    Object localObject1 = c;
    Object localObject2;
    if (localObject1 == null)
    {
      localObject2 = "networkUtil";
      k.a((String)localObject2);
    }
    localObject1 = (CharSequence)((i)localObject1).b();
    locala.a((CharSequence)localObject1);
    localObject1 = "phone";
    paramContext = paramContext.getSystemService((String)localObject1);
    if (paramContext != null)
    {
      paramContext = (CharSequence)((TelephonyManager)paramContext).getNetworkOperatorName();
      locala.b(paramContext);
      paramContext = f;
      if (paramContext == null)
      {
        localObject1 = "regionUtils";
        k.a((String)localObject1);
      }
      boolean bool1 = paramContext.a();
      if (bool1)
      {
        paramContext = locala.a();
        k.a(paramContext, "builder.build()");
        return paramContext;
      }
      paramContext = new java/util/ArrayList;
      paramContext.<init>();
      try
      {
        localObject1 = NetworkInterface.getNetworkInterfaces();
        Object localObject3;
        boolean bool3;
        do
        {
          do
          {
            do
            {
              boolean bool2 = ((Enumeration)localObject1).hasMoreElements();
              if (!bool2) {
                break;
              }
              localObject2 = ((Enumeration)localObject1).nextElement();
              localObject2 = (NetworkInterface)localObject2;
              localObject3 = "iface";
              k.a(localObject2, (String)localObject3);
              bool3 = ((NetworkInterface)localObject2).isUp();
            } while (!bool3);
            bool3 = ((NetworkInterface)localObject2).isLoopback();
          } while (bool3);
          bool3 = ((NetworkInterface)localObject2).isVirtual();
        } while (bool3);
        localObject2 = ((NetworkInterface)localObject2).getInetAddresses();
        for (;;)
        {
          bool3 = ((Enumeration)localObject2).hasMoreElements();
          if (!bool3) {
            break;
          }
          localObject3 = ((Enumeration)localObject2).nextElement();
          boolean bool4 = localObject3 instanceof Inet4Address;
          if (!bool4)
          {
            bool3 = false;
            localObject3 = null;
          }
          localObject3 = (Inet4Address)localObject3;
          if (localObject3 != null)
          {
            localObject3 = ((Inet4Address)localObject3).getHostAddress();
            paramContext.add(localObject3);
          }
        }
        bool5 = paramContext.isEmpty();
      }
      catch (NullPointerException localNullPointerException) {}catch (SocketException localSocketException) {}
      boolean bool5;
      if (!bool5)
      {
        paramContext = (List)paramContext;
        locala.a(paramContext);
      }
      paramContext = locala.a();
      k.a(paramContext, "builder.build()");
      return paramContext;
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type android.telephony.TelephonyManager");
    throw paramContext;
  }
  
  private static aq g(Context paramContext)
  {
    aq.a locala = aq.b();
    Object localObject;
    try
    {
      paramContext = paramContext.getPackageManager();
      localObject = "android";
      paramContext = paramContext.getResourcesForApplication((String)localObject);
      localObject = "context.packageManager\n …ForApplication(\"android\")";
      k.a(paramContext, (String)localObject);
      paramContext = paramContext.getConfiguration();
      paramContext = locale;
      if (paramContext == null)
      {
        paramContext = "language";
        k.a(locala, paramContext);
        paramContext = "unknown";
        paramContext = (CharSequence)paramContext;
        locala.c(paramContext);
      }
      else
      {
        localObject = "language";
        k.a(locala, (String)localObject);
        paramContext = paramContext.getLanguage();
        paramContext = (CharSequence)paramContext;
        locala.c(paramContext);
      }
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      k.a(locala, "language");
      paramContext = (CharSequence)"unknown";
      locala.c(paramContext);
    }
    paramContext = Settings.b("t9_lang");
    if (paramContext != null)
    {
      k.a(paramContext, "it");
      localObject = paramContext;
      localObject = (CharSequence)paramContext;
      int m = ((CharSequence)localObject).length();
      if (m == 0)
      {
        m = 1;
      }
      else
      {
        m = 0;
        localObject = null;
      }
      if (m != 0)
      {
        bool = false;
        paramContext = null;
      }
      if (paramContext != null)
      {
        paramContext = (CharSequence)paramContext;
        break label194;
      }
    }
    paramContext = (CharSequence)"auto";
    label194:
    locala.b(paramContext);
    paramContext = "languageAuto";
    boolean bool = Settings.e(paramContext);
    if (bool) {
      paramContext = "auto";
    } else {
      paramContext = Settings.b("language");
    }
    paramContext = (CharSequence)paramContext;
    locala.a(paramContext);
    paramContext = locala.a();
    k.a(paramContext, "language.build()");
    return paramContext;
  }
  
  public final int a()
  {
    return 10028;
  }
  
  public final PersistentBackgroundTask.RunResult a(Context paramContext, Bundle paramBundle)
  {
    Object localObject1 = "serviceContext";
    k.b(paramContext, (String)localObject1);
    if (paramBundle != null)
    {
      localObject1 = "beatType";
      paramBundle = paramBundle.getString((String)localObject1);
    }
    else
    {
      paramBundle = null;
    }
    localObject1 = paramBundle;
    localObject1 = (CharSequence)paramBundle;
    if (localObject1 != null)
    {
      m = ((CharSequence)localObject1).length();
      if (m != 0)
      {
        m = 0;
        localObject1 = null;
        break label66;
      }
    }
    int m = 1;
    label66:
    if (m != 0)
    {
      paramBundle = EventsUploadResult.INVALID_PARAMS;
      a(paramBundle, paramContext);
      return PersistentBackgroundTask.RunResult.FailedSkip;
    }
    try
    {
      paramBundle = a(paramContext, paramBundle);
      localObject1 = a;
      if (localObject1 == null)
      {
        localObject2 = "eventsTracker";
        k.a((String)localObject2);
      }
      localObject1 = (ae)((f)localObject1).a();
      paramBundle = (d)paramBundle;
      Object localObject2 = j;
      if (localObject2 == null)
      {
        String str = "analyticsHttpClient";
        k.a(str);
      }
      paramBundle = (EventsUploadResult)((ae)localObject1).a(paramBundle, (y)localObject2).d();
      a(paramBundle, paramContext);
      paramContext = EventsUploadResult.SUCCESS;
      if (paramBundle == paramContext) {
        return PersistentBackgroundTask.RunResult.Success;
      }
      return PersistentBackgroundTask.RunResult.FailedRetry;
    }
    catch (Exception localException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localException);
      paramContext = h;
      if (paramContext == null)
      {
        paramBundle = "analytics";
        k.a(paramBundle);
      }
      paramBundle = new com/truecaller/analytics/e$a;
      paramBundle.<init>("AppHeartBeatTask");
      paramBundle = paramBundle.a("Result", "FailedBuildingEvent").a();
      k.a(paramBundle, "AnalyticsEvent.Builder(A…\n                .build()");
      paramContext.b(paramBundle);
    }
    return PersistentBackgroundTask.RunResult.FailedRetry;
  }
  
  public final boolean a(Context paramContext)
  {
    String str = "serviceContext";
    k.b(paramContext, str);
    paramContext = g;
    if (paramContext == null)
    {
      str = "accountManager";
      k.a(str);
    }
    return paramContext.c();
  }
  
  public final e b()
  {
    Object localObject = new com/truecaller/common/background/e$a;
    ((com.truecaller.common.background.e.a)localObject).<init>(1);
    TimeUnit localTimeUnit = TimeUnit.HOURS;
    localObject = ((com.truecaller.common.background.e.a)localObject).a(6, localTimeUnit).a();
    localTimeUnit = TimeUnit.MINUTES;
    localObject = ((com.truecaller.common.background.e.a)localObject).b(20, localTimeUnit).a("beatType", "active").b();
    k.a(localObject, "TaskConfiguration.Builde…IVE)\n            .build()");
    return (e)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.AppHeartBeatTask
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */