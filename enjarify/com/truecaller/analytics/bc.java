package com.truecaller.analytics;

import c.g.b.k;
import java.util.HashMap;
import java.util.Map;

public final class bc
  implements e
{
  private final String a;
  private final String b;
  private final Double c;
  private final Boolean d;
  
  public bc(String paramString)
  {
    this(paramString, null);
  }
  
  public bc(String paramString1, String paramString2)
  {
    this(paramString1, paramString2, null, 8);
  }
  
  private bc(String paramString1, String paramString2, Boolean paramBoolean)
  {
    a = paramString1;
    b = paramString2;
    c = null;
    d = paramBoolean;
  }
  
  public final String a()
  {
    return "ViewVisited";
  }
  
  public final Map b()
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    Object localObject1 = a;
    localHashMap.put("ViewId", localObject1);
    Object localObject2 = b;
    String str;
    if (localObject2 != null)
    {
      localObject1 = localHashMap;
      localObject1 = (Map)localHashMap;
      str = "Context";
      ((Map)localObject1).put(str, localObject2);
    }
    localObject2 = d;
    if (localObject2 != null)
    {
      localObject1 = localHashMap;
      localObject1 = (Map)localHashMap;
      str = "PremiumStatus";
      boolean bool = ((Boolean)localObject2).booleanValue();
      if (bool) {
        localObject2 = "Premium";
      } else {
        localObject2 = "Free";
      }
      ((Map)localObject1).put(str, localObject2);
    }
    return (Map)localHashMap;
  }
  
  public final Double c()
  {
    return c;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = paramObject instanceof bc;
    if (bool1)
    {
      Object localObject1 = a;
      paramObject = (bc)paramObject;
      Object localObject2 = a;
      bool1 = k.a(localObject1, localObject2);
      if (bool1)
      {
        localObject1 = b;
        localObject2 = b;
        bool1 = k.a(localObject1, localObject2);
        if (bool1)
        {
          localObject1 = c;
          localObject2 = c;
          bool1 = k.a((Double)localObject1, (Double)localObject2);
          if (bool1)
          {
            localObject1 = d;
            paramObject = d;
            boolean bool2 = k.a(localObject1, paramObject);
            if (bool2) {
              return true;
            }
          }
        }
      }
    }
    return false;
  }
  
  public final int hashCode()
  {
    String str = a;
    int i = str.hashCode() * 31;
    Object localObject = b;
    int j = 0;
    if (localObject != null)
    {
      k = ((String)localObject).hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    i = (i + k) * 31;
    localObject = d;
    if (localObject != null) {
      j = localObject.hashCode();
    }
    i = (i + j) * 31;
    localObject = c;
    double d1;
    if (localObject != null) {
      d1 = ((Double)localObject).doubleValue();
    } else {
      d1 = 0.0D;
    }
    int k = (int)d1;
    return i + k;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.bc
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */