package com.truecaller.analytics;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import c.g.b.k;
import c.u;
import com.truecaller.aftercall.AfterCallPromotionActivity;
import com.truecaller.androidactors.f;
import com.truecaller.calling.after_call.AfterCallActivity;
import com.truecaller.messaging.notifications.ClassZeroActivity;
import com.truecaller.notifications.SourcedContactListActivity;
import com.truecaller.tracking.events.j;
import com.truecaller.tracking.events.j.a;
import com.truecaller.ui.AfterClipboardSearchActivity;
import com.truecaller.ui.CallMeBackActivity;
import com.truecaller.ui.clicktocall.CallConfirmationActivity;
import com.truecaller.utils.a;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.UUID;
import org.apache.a.d.d;

public final class y
  implements w
{
  public static final y.a a;
  private final Class[] b;
  private final String[] c;
  private int d;
  private long e;
  private boolean f;
  private String g;
  private final a h;
  private final f i;
  private final b j;
  
  static
  {
    y.a locala = new com/truecaller/analytics/y$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  public y(a parama, f paramf, b paramb)
  {
    h = parama;
    i = paramf;
    j = paramb;
    parama = new Class[8];
    parama[0] = AfterCallActivity.class;
    boolean bool = true;
    parama[bool] = AfterCallPromotionActivity.class;
    parama[2] = CallMeBackActivity.class;
    parama[3] = AfterClipboardSearchActivity.class;
    parama[4] = com.truecaller.tag.b.class;
    parama[5] = SourcedContactListActivity.class;
    parama[6] = CallConfirmationActivity.class;
    parama[7] = ClassZeroActivity.class;
    b = parama;
    parama = new String[] { "com.truecaller.flashsdk" };
    c = parama;
    long l = h.c();
    e = l;
    f = bool;
  }
  
  private final void a(boolean paramBoolean)
  {
    a locala = h;
    long l = locala.c();
    e = l;
    if (paramBoolean)
    {
      paramBoolean = false;
      f = false;
    }
  }
  
  private final boolean a()
  {
    long l1 = b();
    long l2 = 300000000000L;
    boolean bool = l1 < l2;
    return !bool;
  }
  
  private final long b()
  {
    long l1 = h.c();
    long l2 = e;
    return l1 - l2;
  }
  
  private final boolean c()
  {
    long l1 = b();
    boolean bool1 = true;
    long l2 = 5000000000L;
    boolean bool2 = l1 < l2;
    if (bool2)
    {
      bool3 = f;
      if (!bool3)
      {
        bool3 = false;
        break label46;
      }
    }
    boolean bool3 = true;
    label46:
    int k = d;
    if (k == 0) {
      k = 1;
    } else {
      k = 0;
    }
    if ((bool3) && (k != 0)) {
      return bool1;
    }
    return false;
  }
  
  private final boolean c(Activity paramActivity)
  {
    Object localObject1 = paramActivity.getClass().getPackage();
    if (localObject1 != null)
    {
      localObject1 = ((Package)localObject1).getName();
      if (localObject1 != null) {}
    }
    else
    {
      localObject1 = "";
    }
    Object localObject2 = c;
    Object localObject3 = new java/util/ArrayList;
    ((ArrayList)localObject3).<init>();
    localObject3 = (Collection)localObject3;
    int k = localObject2.length;
    int i2 = 0;
    String str1 = null;
    while (i2 < k)
    {
      String str2 = localObject2[i2];
      boolean bool3 = c.n.m.b((String)localObject1, str2, false);
      if (bool3) {
        ((Collection)localObject3).add(str2);
      }
      i2 += 1;
    }
    localObject3 = (Iterable)localObject3;
    localObject2 = new java/util/ArrayList;
    k = c.a.m.a((Iterable)localObject3, 10);
    ((ArrayList)localObject2).<init>(k);
    localObject2 = (Collection)localObject2;
    localObject3 = ((Iterable)localObject3).iterator();
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject3).hasNext();
      if (!bool1) {
        break label225;
      }
      localObject4 = (String)((Iterator)localObject3).next();
      int m = ((String)localObject4).length();
      if (localObject1 == null) {
        break;
      }
      localObject4 = ((String)localObject1).substring(m);
      str1 = "(this as java.lang.String).substring(startIndex)";
      k.a(localObject4, str1);
      ((Collection)localObject2).add(localObject4);
    }
    paramActivity = new c/u;
    paramActivity.<init>("null cannot be cast to non-null type java.lang.String");
    throw paramActivity;
    label225:
    localObject2 = (Iterable)localObject2;
    localObject1 = localObject2;
    localObject1 = (Collection)localObject2;
    boolean bool4 = ((Collection)localObject1).isEmpty();
    boolean bool5 = true;
    if (!bool4)
    {
      localObject1 = ((Iterable)localObject2).iterator();
      boolean bool6;
      do
      {
        bool6 = ((Iterator)localObject1).hasNext();
        if (!bool6) {
          break;
        }
        localObject2 = (String)((Iterator)localObject1).next();
        localObject4 = localObject2;
        localObject4 = (CharSequence)localObject2;
        n = TextUtils.isEmpty((CharSequence)localObject4);
        if (n == 0)
        {
          localObject4 = ".";
          bool6 = c.n.m.b((String)localObject2, (String)localObject4, false);
          if (!bool6)
          {
            bool6 = false;
            localObject2 = null;
            continue;
          }
        }
        bool6 = true;
      } while (!bool6);
      bool4 = true;
    }
    else
    {
      bool4 = false;
      localObject1 = null;
    }
    if (bool4) {
      return false;
    }
    localObject1 = b;
    int i3 = localObject1.length;
    int n = 0;
    Object localObject4 = null;
    while (n < i3)
    {
      str1 = localObject1[n];
      boolean bool2 = str1.isInstance(paramActivity);
      if (bool2) {
        return false;
      }
      int i1;
      n += 1;
    }
    return bool5;
  }
  
  private final void d(Activity paramActivity)
  {
    Object localObject1 = paramActivity.getIntent();
    if (localObject1 != null)
    {
      str1 = "AppUserInteraction.Context";
      localObject1 = ((Intent)localObject1).getStringExtra(str1);
    }
    else
    {
      localObject1 = null;
    }
    String str1 = paramActivity.getClass().getSimpleName();
    Object localObject2 = UUID.randomUUID().toString();
    String str2 = "UUID.randomUUID().toString()";
    k.a(localObject2, str2);
    int k = 1;
    Object localObject3 = new String[k];
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Logging appOpen for activity: ");
    localStringBuilder.append(paramActivity);
    localStringBuilder.append(" with context: ");
    localStringBuilder.append((String)localObject1);
    localStringBuilder.append(" and id: ");
    localStringBuilder.append((String)localObject2);
    paramActivity = localStringBuilder.toString();
    localStringBuilder = null;
    localObject3[0] = paramActivity;
    paramActivity = com.truecaller.tracking.events.y.b();
    localObject3 = localObject1;
    localObject3 = (CharSequence)localObject1;
    paramActivity = paramActivity.a((CharSequence)localObject3);
    Object localObject4 = str1;
    localObject4 = (CharSequence)str1;
    paramActivity = paramActivity.b((CharSequence)localObject4);
    localObject4 = localObject2;
    localObject4 = (CharSequence)localObject2;
    paramActivity = paramActivity.c((CharSequence)localObject4).a();
    g = ((String)localObject2);
    localObject2 = (ae)i.a();
    paramActivity = (d)paramActivity;
    ((ae)localObject2).a(paramActivity);
    paramActivity = new com/truecaller/analytics/e$a;
    paramActivity.<init>("AppVisited");
    localObject2 = "View";
    paramActivity.a((String)localObject2, str1);
    if (localObject3 != null)
    {
      int m = ((CharSequence)localObject3).length();
      if (m != 0)
      {
        k = 0;
        str2 = null;
      }
    }
    if (k == 0)
    {
      str1 = "Context";
      paramActivity.a(str1, (String)localObject1);
    }
    localObject1 = j;
    paramActivity = paramActivity.a();
    k.a(paramActivity, "eventBuilder.build()");
    ((b)localObject1).b(paramActivity);
  }
  
  public final void a(int paramInt)
  {
    int k = 20;
    if (paramInt < k) {
      return;
    }
    new String[1][0] = "Setting background state to true";
    f = true;
  }
  
  public final void a(Activity paramActivity)
  {
    String str = "activity";
    k.b(paramActivity, str);
    int k = c(paramActivity);
    if (k == 0) {
      return;
    }
    k = a();
    if (k != 0)
    {
      k = c();
      if (k != 0) {
        d(paramActivity);
      }
    }
    int m = d;
    k = 1;
    int n;
    m += k;
    d = n;
    a(k);
  }
  
  public final void a(Activity paramActivity, Bundle paramBundle)
  {
    String str = "activity";
    k.b(paramActivity, str);
    boolean bool1 = c(paramActivity);
    if (!bool1) {
      return;
    }
    boolean bool2;
    if (paramBundle != null)
    {
      bool2 = a();
      if (!bool2) {}
    }
    else
    {
      bool2 = c();
      if (bool2) {
        d(paramActivity);
      }
    }
    a(true);
  }
  
  public final void b(Activity paramActivity)
  {
    Object localObject = "activity";
    k.b(paramActivity, (String)localObject);
    boolean bool = c(paramActivity);
    if (!bool) {
      return;
    }
    int k = d + -1;
    d = k;
    k = d;
    if (k == 0)
    {
      localObject = g;
      if (localObject != null)
      {
        int m = 1;
        String[] arrayOfString = new String[m];
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        String str = "Logging appClose for activity: ";
        localStringBuilder.<init>(str);
        localStringBuilder.append(paramActivity);
        localStringBuilder.append(" with id: ");
        localStringBuilder.append((String)localObject);
        paramActivity = localStringBuilder.toString();
        arrayOfString[0] = paramActivity;
        paramActivity = j.b();
        localObject = (CharSequence)localObject;
        paramActivity = paramActivity.a((CharSequence)localObject).a();
        k = 0;
        g = null;
        localObject = (ae)i.a();
        paramActivity = (d)paramActivity;
        ((ae)localObject).a(paramActivity);
      }
    }
    a(false);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.y
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */