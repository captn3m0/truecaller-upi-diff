package com.truecaller.analytics;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;

final class e$a$a
  implements e
{
  private final String a;
  private final Map b;
  private final Double c;
  
  private e$a$a(String paramString, Double paramDouble, Map paramMap)
  {
    a = paramString;
    b = paramMap;
    c = paramDouble;
  }
  
  public final String a()
  {
    return a;
  }
  
  public final Map b()
  {
    return b;
  }
  
  public final Double c()
  {
    return c;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = paramObject instanceof e;
    if (!bool1) {
      return false;
    }
    paramObject = (e)paramObject;
    Object localObject1 = a;
    Object localObject2 = ((e)paramObject).a();
    bool1 = Objects.equals(localObject1, localObject2);
    if (bool1)
    {
      localObject1 = b;
      localObject2 = ((e)paramObject).b();
      bool1 = Objects.equals(localObject1, localObject2);
      if (bool1)
      {
        localObject1 = c;
        paramObject = ((e)paramObject).c();
        boolean bool2 = Objects.equals(localObject1, paramObject);
        if (bool2) {
          return true;
        }
      }
    }
    return false;
  }
  
  public final int hashCode()
  {
    Object[] arrayOfObject = new Object[3];
    Object localObject = a;
    arrayOfObject[0] = localObject;
    localObject = b;
    arrayOfObject[1] = localObject;
    localObject = c;
    arrayOfObject[2] = localObject;
    return Objects.hash(arrayOfObject);
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append("AnalyticsEvent(");
    Object localObject1 = a;
    localStringBuilder.append((String)localObject1);
    localStringBuilder.append("){");
    localObject1 = b;
    if (localObject1 != null)
    {
      int i = ((Map)localObject1).size();
      localObject1 = ((Map)localObject1).entrySet().iterator();
      for (;;)
      {
        boolean bool = ((Iterator)localObject1).hasNext();
        if (!bool) {
          break;
        }
        Object localObject2 = (Map.Entry)((Iterator)localObject1).next();
        String str = (String)((Map.Entry)localObject2).getKey();
        localObject2 = (String)((Map.Entry)localObject2).getValue();
        if (localObject2 == null) {
          localObject2 = "<NULL>";
        }
        localStringBuilder.append(str);
        str = ":";
        localStringBuilder.append(str);
        localStringBuilder.append((String)localObject2);
        i += -1;
        if (i > 0)
        {
          localObject2 = ",";
          localStringBuilder.append((String)localObject2);
        }
      }
    }
    localStringBuilder.append("}");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.e.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */