package com.truecaller.analytics;

import dagger.a.d;
import javax.inject.Provider;

public final class n
  implements d
{
  private final f a;
  private final Provider b;
  
  private n(f paramf, Provider paramProvider)
  {
    a = paramf;
    b = paramProvider;
  }
  
  public static n a(f paramf, Provider paramProvider)
  {
    n localn = new com/truecaller/analytics/n;
    localn.<init>(paramf, paramProvider);
    return localn;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */