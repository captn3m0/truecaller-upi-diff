package com.truecaller.analytics;

import android.app.Application;
import android.content.Context;
import com.truecaller.utils.t;
import dagger.a.e;
import javax.inject.Provider;

public final class aa
  implements as
{
  private Provider a;
  private Provider b;
  private Provider c;
  private Provider d;
  private Provider e;
  private Provider f;
  private Provider g;
  private Provider h;
  private Provider i;
  private Provider j;
  private Provider k;
  private Provider l;
  private Provider m;
  private Provider n;
  private Provider o;
  private Provider p;
  private Provider q;
  private Provider r;
  private Provider s;
  private Provider t;
  private Provider u;
  private Provider v;
  
  private aa(f paramf, t paramt, Application paramApplication, Context paramContext, Integer paramInteger, s params)
  {
    paramContext = e.a(paramContext);
    a = paramContext;
    paramContext = a;
    paramContext = dagger.a.c.a(m.a(paramf, paramContext));
    b = paramContext;
    paramApplication = e.a(paramApplication);
    c = paramApplication;
    paramApplication = c;
    paramApplication = dagger.a.c.a(i.a(paramf, paramApplication));
    d = paramApplication;
    paramApplication = a;
    paramApplication = dagger.a.c.a(p.a(paramf, paramApplication));
    e = paramApplication;
    paramApplication = com.truecaller.analytics.a.c.a(e);
    f = paramApplication;
    paramApplication = dagger.a.c.a(f);
    g = paramApplication;
    paramApplication = e.a(params);
    h = paramApplication;
    paramApplication = a;
    paramApplication = dagger.a.c.a(j.a(paramf, paramApplication));
    i = paramApplication;
    paramApplication = i;
    paramApplication = dagger.a.c.a(o.a(paramf, paramApplication));
    j = paramApplication;
    paramApplication = e;
    paramContext = j;
    paramApplication = ak.a(paramApplication, paramContext);
    k = paramApplication;
    paramApplication = new com/truecaller/analytics/aa$b;
    paramApplication.<init>(paramt);
    l = paramApplication;
    paramt = a;
    paramt = n.a(paramf, paramt);
    m = paramt;
    paramt = a;
    paramApplication = m;
    paramt = ao.a(paramt, paramApplication);
    n = paramt;
    paramt = e;
    paramApplication = a;
    paramt = k.a(paramf, paramt, paramApplication);
    o = paramt;
    paramt = a;
    paramt = q.a(paramf, paramt);
    p = paramt;
    Provider localProvider1 = h;
    Provider localProvider2 = e;
    Provider localProvider3 = j;
    Provider localProvider4 = k;
    Provider localProvider5 = l;
    Provider localProvider6 = n;
    Provider localProvider7 = o;
    Provider localProvider8 = p;
    Provider localProvider9 = d;
    paramt = ah.a(localProvider1, localProvider2, localProvider3, localProvider4, localProvider5, localProvider6, localProvider7, localProvider8, localProvider9);
    q = paramt;
    paramt = dagger.a.c.a(q);
    r = paramt;
    paramt = dagger.a.c.a(h.a(paramf));
    s = paramt;
    paramt = e.a(paramInteger);
    t = paramt;
    paramt = a;
    paramApplication = s;
    paramContext = t;
    paramt = g.a(paramf, paramt, paramApplication, paramContext);
    u = paramt;
    paramt = r;
    paramApplication = u;
    paramf = dagger.a.c.a(l.a(paramf, paramt, paramApplication));
    v = paramf;
  }
  
  public static as.a a()
  {
    aa.a locala = new com/truecaller/analytics/aa$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  public final com.truecaller.analytics.a.f b()
  {
    return (com.truecaller.analytics.a.f)b.get();
  }
  
  public final b c()
  {
    return (b)d.get();
  }
  
  public final com.truecaller.analytics.a.a d()
  {
    return (com.truecaller.analytics.a.a)g.get();
  }
  
  public final com.truecaller.analytics.storage.a e()
  {
    return (com.truecaller.analytics.storage.a)e.get();
  }
  
  public final com.truecaller.androidactors.f f()
  {
    return (com.truecaller.androidactors.f)v.get();
  }
  
  public final ae g()
  {
    return (ae)r.get();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.aa
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */