package com.truecaller.analytics;

import android.content.Intent;
import com.truecaller.androidactors.f;
import com.truecaller.log.AssertionUtil;
import com.truecaller.tracking.events.af;
import com.truecaller.tracking.events.af.a;
import org.apache.a.a;

public final class bb
{
  public static void a(Intent paramIntent, String paramString1, String paramString2)
  {
    paramIntent.putExtra("AppUserInteraction.Context", paramString1).putExtra("AppUserInteraction.Action", paramString2);
  }
  
  public static void a(f paramf, String paramString1, String paramString2)
  {
    try
    {
      af.a locala = af.b();
      paramString1 = locala.a(paramString1);
      paramString1 = paramString1.b(paramString2);
      paramString1 = paramString1.a();
      ((ae)paramf.a()).a(paramString1);
      return;
    }
    catch (a locala1)
    {
      AssertionUtil.reportThrowableButNeverCrash(locala1;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.bb
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */