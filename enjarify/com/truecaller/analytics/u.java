package com.truecaller.analytics;

import c.g.b.k;

public final class u
{
  private final e.a a;
  
  public u(String paramString)
  {
    Object localObject = new com/truecaller/analytics/e$a;
    ((e.a)localObject).<init>("AppEventUpload");
    paramString = ((e.a)localObject).a("Result", "Unknown").a("Type", paramString);
    localObject = Double.valueOf(0.0D);
    paramString = paramString.a((Double)localObject);
    k.a(paramString, "AnalyticsEvent.Builder(A…\n        .valueToSum(0.0)");
    a = paramString;
  }
  
  public final e.a a(int paramInt)
  {
    e.a locala = a;
    locala.a("Exception");
    locala.a("ErrorCode");
    locala.a("Result", "Success");
    Double localDouble = Double.valueOf(paramInt);
    locala.a(localDouble);
    return locala;
  }
  
  public final e.a a(Exception paramException)
  {
    k.b(paramException, "e");
    e.a locala = a;
    String str1 = "Exception";
    locala.a("Result", str1);
    String str2 = "Exception";
    paramException = paramException.getClass().getSimpleName();
    if (paramException == null) {
      paramException = "";
    }
    locala.a(str2, paramException);
    return locala;
  }
  
  public final e.a a(Integer paramInteger)
  {
    e.a locala = a;
    String str1 = "ErrorResponse";
    locala.a("Result", str1);
    String str2 = "ErrorCode";
    int i;
    if (paramInteger != null)
    {
      i = paramInteger.intValue();
    }
    else
    {
      i = 0;
      paramInteger = null;
    }
    locala.a(str2, i);
    return locala;
  }
  
  public final e.a a(String paramString)
  {
    k.b(paramString, "result");
    e.a locala = a;
    locala.a("Result", paramString);
    return locala;
  }
  
  public final e a()
  {
    e locale = a.a();
    k.a(locale, "builder.build()");
    return locale;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.u
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */