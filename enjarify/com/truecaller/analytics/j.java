package com.truecaller.analytics;

import dagger.a.d;
import javax.inject.Provider;

public final class j
  implements d
{
  private final f a;
  private final Provider b;
  
  private j(f paramf, Provider paramProvider)
  {
    a = paramf;
    b = paramProvider;
  }
  
  public static j a(f paramf, Provider paramProvider)
  {
    j localj = new com/truecaller/analytics/j;
    localj.<init>(paramf, paramProvider);
    return localj;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */