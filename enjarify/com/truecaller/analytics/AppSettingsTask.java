package com.truecaller.analytics;

import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.os.Build.VERSION;
import android.os.Bundle;
import com.google.android.gms.common.GoogleApiAvailability;
import com.truecaller.TrueApp;
import com.truecaller.androidactors.f;
import com.truecaller.bp;
import com.truecaller.common.account.r;
import com.truecaller.common.background.PersistentBackgroundTask;
import com.truecaller.common.background.PersistentBackgroundTask.RunResult;
import com.truecaller.common.h.u;
import com.truecaller.common.network.country.CountryListDto.a;
import com.truecaller.filters.p;
import com.truecaller.filters.s;
import com.truecaller.notifications.i;
import com.truecaller.old.data.access.Settings;
import com.truecaller.tracking.events.ab;
import com.truecaller.tracking.events.ab.a;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;
import com.truecaller.util.b.at;
import com.truecaller.util.b.j;
import com.truecaller.util.b.j.a;
import com.truecaller.utils.l;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.a.a.n;
import org.a.a.x;

public class AppSettingsTask
  extends PersistentBackgroundTask
{
  public f a;
  public b b;
  public com.truecaller.utils.d c;
  public i d;
  public com.truecaller.common.g.a e;
  public p f;
  public com.truecaller.i.c g;
  public com.truecaller.i.e h;
  public com.truecaller.common.h.ac i;
  public com.truecaller.multisim.h j;
  public com.truecaller.d.b k;
  public com.truecaller.common.h.c l;
  public com.truecaller.push.e m;
  public com.truecaller.messaging.h n;
  public com.truecaller.featuretoggles.e o;
  public android.support.v4.app.ac p;
  public l q;
  public com.truecaller.calling.e.e r;
  public s s;
  public u t;
  public com.truecaller.abtest.c u;
  
  private static String a(boolean paramBoolean1, boolean paramBoolean2)
  {
    if (!paramBoolean1) {
      return "NotSupported";
    }
    if (paramBoolean2) {
      return "Enabled";
    }
    return "Disabled";
  }
  
  public static void a(com.truecaller.common.background.b paramb)
  {
    paramb.b(10001);
  }
  
  private void a(com.truecaller.filters.a.b paramb)
  {
    Object localObject1 = t.b();
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = new java/util/HashSet;
    ((HashSet)localObject2).<init>();
    for (;;)
    {
      boolean bool1 = paramb.moveToNext();
      if (!bool1) {
        break;
      }
      Object localObject3 = paramb.a();
      Object localObject4 = f;
      String str = "PHONE_NUMBER";
      boolean bool2 = ((String)localObject4).equals(str);
      if (bool2)
      {
        localObject4 = t;
        localObject3 = e;
        localObject3 = ((u)localObject4).e((String)localObject3);
        if (localObject3 != null)
        {
          bool2 = ((String)localObject1).equalsIgnoreCase((String)localObject3);
          if (!bool2) {
            ((Set)localObject2).add(localObject3);
          }
        }
      }
    }
    localObject1 = Double.valueOf(((Set)localObject2).size());
    localObject2 = new com/truecaller/analytics/e$a;
    ((e.a)localObject2).<init>("CountOfForeignCountriesWithNumberBlocked");
    a = ((Double)localObject1);
    localObject1 = ((e.a)localObject2).a();
    b.a((e)localObject1);
    paramb.close();
  }
  
  public static void b(com.truecaller.common.background.b paramb)
  {
    paramb.a(10001);
  }
  
  public final int a()
  {
    return 10001;
  }
  
  public final PersistentBackgroundTask.RunResult a(Context paramContext, Bundle paramBundle)
  {
    TrueApp.y().a().a(this);
    paramBundle = new java/util/ArrayList;
    paramBundle.<init>();
    Object localObject1 = ((TrueApp)paramContext.getApplicationContext()).a().I();
    Object localObject2 = ab.b();
    ((ab.a)localObject2).a("notificationsAllowed");
    Object localObject3 = String.valueOf(q.d());
    ((ab.a)localObject2).b((CharSequence)localObject3);
    localObject2 = ((ab.a)localObject2).a();
    paramBundle.add(localObject2);
    localObject2 = ab.b().a("availabilityEnabled");
    localObject3 = String.valueOf(Settings.e("availability_enabled"));
    localObject2 = ((ab.a)localObject2).b((CharSequence)localObject3).a();
    paramBundle.add(localObject2);
    localObject2 = ab.b().a("backupEnabled");
    localObject3 = String.valueOf(((com.truecaller.common.g.a)localObject1).a("backup_enabled", false));
    localObject2 = ((ab.a)localObject2).b((CharSequence)localObject3).a();
    paramBundle.add(localObject2);
    localObject2 = ab.b().a("accountWasBackedUp");
    boolean bool1 = ((com.truecaller.common.g.a)localObject1).a("accountFileWasBackedUpByAutobackup", false);
    localObject3 = String.valueOf(bool1);
    localObject2 = ((ab.a)localObject2).b((CharSequence)localObject3).a();
    paramBundle.add(localObject2);
    long l1 = 0L;
    double d1 = 0.0D;
    long l2 = ((com.truecaller.common.g.a)localObject1).a("key_backup_frequency_hours", l1);
    localObject3 = ab.b();
    Object localObject4 = "backupFrequency";
    localObject3 = ((ab.a)localObject3).a((CharSequence)localObject4);
    localObject1 = String.valueOf(l2);
    localObject1 = ((ab.a)localObject3).b((CharSequence)localObject1).a();
    paramBundle.add(localObject1);
    localObject1 = ab.b().a("region1");
    localObject2 = String.valueOf(i.a());
    localObject1 = ((ab.a)localObject1).b((CharSequence)localObject2).a();
    paramBundle.add(localObject1);
    localObject1 = ab.b().a("TruecallerX");
    boolean bool2 = o.e().a();
    localObject2 = String.valueOf(bool2);
    localObject1 = ((ab.a)localObject1).b((CharSequence)localObject2).a();
    paramBundle.add(localObject1);
    paramBundle = paramBundle.iterator();
    for (;;)
    {
      bool3 = paramBundle.hasNext();
      if (!bool3) {
        break;
      }
      localObject1 = (ab)paramBundle.next();
      localObject2 = (ae)a.a();
      ((ae)localObject2).a((org.apache.a.d.d)localObject1);
    }
    paramBundle = b;
    localObject1 = i;
    boolean bool3 = ((com.truecaller.common.h.ac)localObject1).a();
    localObject2 = l;
    bool2 = ((com.truecaller.common.h.c)localObject2).c();
    bool1 = true;
    if ((bool2) && (!bool3))
    {
      bool2 = true;
    }
    else
    {
      bool2 = false;
      localObject2 = null;
    }
    localObject4 = q;
    boolean bool6 = ((l)localObject4).d();
    Object localObject5 = g;
    Object localObject6 = "merge_by";
    int i6 = 3;
    int i7 = ((com.truecaller.i.c)localObject5).a((String)localObject6, i6);
    if (i7 == i6) {
      localObject5 = "Enabled";
    } else {
      localObject5 = "Disabled";
    }
    localObject6 = g;
    Object localObject7 = "showFrequentlyCalledContacts";
    boolean bool10 = ((com.truecaller.i.c)localObject6).a((String)localObject7, bool1);
    if (bool10) {
      localObject6 = "Enabled";
    } else {
      localObject6 = "Disabled";
    }
    localObject7 = n;
    boolean bool8 = ((com.truecaller.messaging.h)localObject7).b(0);
    if (bool8) {
      localObject7 = "Enabled";
    } else {
      localObject7 = "Disabled";
    }
    Object localObject8 = g;
    Object localObject9 = "showIncomingCallNotifications";
    boolean bool11 = ((com.truecaller.i.c)localObject8).a((String)localObject9, bool1);
    if (bool11) {
      localObject8 = "Enabled";
    } else {
      localObject8 = "Disabled";
    }
    localObject9 = new com/truecaller/analytics/e$a;
    ((e.a)localObject9).<init>("SettingState");
    Object localObject10 = ThemeManager.a().name();
    localObject9 = ((e.a)localObject9).a("Theme", (String)localObject10);
    String str1 = "EnhancedSearch";
    localObject10 = e;
    String str2 = "backup";
    boolean bool12 = ((com.truecaller.common.g.a)localObject10).b(str2);
    if ((bool12) && (!bool3))
    {
      bool3 = true;
    }
    else
    {
      bool3 = false;
      localObject1 = null;
    }
    localObject1 = a(bool2, bool3);
    localObject1 = ((e.a)localObject9).a(str1, (String)localObject1);
    boolean bool13 = f.g();
    localObject9 = a(bool1, bool13);
    localObject1 = ((e.a)localObject1).a("BlockTopSpammers", (String)localObject9);
    bool13 = f.a();
    localObject9 = a(bool1, bool13);
    localObject1 = ((e.a)localObject1).a("BlockHiddenCalls", (String)localObject9);
    localObject9 = Settings.b("callLogTapBehavior");
    localObject1 = ((e.a)localObject1).a("CallHistoryTap", (String)localObject9);
    localObject9 = g;
    str1 = "clipboardSearchEnabled";
    bool13 = ((com.truecaller.i.c)localObject9).b(str1);
    localObject9 = a(bool1, bool13);
    localObject1 = ((e.a)localObject1).a("SearchClipBoard", (String)localObject9);
    bool13 = Settings.j("enhancedNotificationsEnabled");
    localObject9 = a(bool6, bool13);
    localObject1 = ((e.a)localObject1).a("SearchMessagingApps", (String)localObject9);
    bool13 = Settings.j("showMissedCallsNotifications");
    localObject4 = a(bool6, bool13);
    localObject1 = ((e.a)localObject1).a("MissedCallNotification", (String)localObject4);
    bool6 = Settings.j("showMissedCallReminders");
    localObject4 = a(bool1, bool6);
    localObject1 = ((e.a)localObject1).a("MissedCallReminder", (String)localObject4);
    bool6 = g.b("enabledCallerIDforPB");
    localObject4 = a(bool1, bool6);
    localObject1 = ((e.a)localObject1).a("CallerIdPhonebook", (String)localObject4);
    localObject4 = g;
    localObject9 = "afterCall";
    bool6 = ((com.truecaller.i.c)localObject4).b((String)localObject9);
    localObject4 = a(bool1, bool6);
    localObject1 = ((e.a)localObject1).a("AfterCall", (String)localObject4).a("GroupCalls", (String)localObject5);
    bool6 = r.b();
    localObject4 = a(bool1, bool6);
    localObject1 = ((e.a)localObject1).a("WhatsApp", (String)localObject4).a("MostCalled", (String)localObject6);
    bool6 = e.b("backup_enabled");
    localObject4 = a(bool1, bool6);
    localObject1 = ((e.a)localObject1).a("Backup", (String)localObject4);
    localObject4 = e;
    localObject5 = "accountFileWasBackedUpByAutobackup";
    bool6 = ((com.truecaller.common.g.a)localObject4).b((String)localObject5);
    localObject4 = a(bool1, bool6);
    localObject1 = ((e.a)localObject1).a("AccountBackedUp", (String)localObject4);
    bool6 = Settings.g();
    localObject1 = ((e.a)localObject1).a("flashEnabled", bool6).a("SmsDeliveryReport", (String)localObject7).a("IncomingCallNotification", (String)localObject8).a();
    paramBundle.a((e)localObject1);
    paramBundle = new java/util/ArrayList;
    paramBundle.<init>();
    localObject1 = ab.b();
    ((ab.a)localObject1).a("defaultMessagingApp");
    localObject2 = c.k();
    if (localObject2 != null)
    {
      ((ab.a)localObject1).b((CharSequence)localObject2);
    }
    else
    {
      localObject2 = "NotSupported";
      ((ab.a)localObject1).b((CharSequence)localObject2);
    }
    localObject1 = ((ab.a)localObject1).a();
    paramBundle.add(localObject1);
    localObject1 = ab.b();
    ((ab.a)localObject1).a("messageReadPermission");
    localObject2 = q;
    localObject4 = new String[] { "android.permission.READ_SMS" };
    localObject2 = String.valueOf(((l)localObject2).a((String[])localObject4));
    ((ab.a)localObject1).b((CharSequence)localObject2);
    localObject1 = ((ab.a)localObject1).a();
    paramBundle.add(localObject1);
    localObject1 = ab.b();
    ((ab.a)localObject1).a("messageWritePermission");
    bool2 = c.a();
    localObject2 = String.valueOf(bool2);
    ((ab.a)localObject1).b((CharSequence)localObject2);
    localObject1 = ((ab.a)localObject1).a();
    paramBundle.add(localObject1);
    localObject1 = o.b();
    bool3 = ((com.truecaller.featuretoggles.b)localObject1).a();
    if (bool3)
    {
      localObject1 = new org/a/a/n;
      l1 = 1L;
      d1 = Double.MIN_VALUE;
      localObject2 = org.a.a.h.a(l1);
      localObject4 = org.a.a.b.ay_();
      ((n)localObject1).<init>((org.a.a.w)localObject2, (x)localObject4);
      localObject2 = n.E();
      bool2 = ((n)localObject1).a((x)localObject2);
      if (!bool2)
      {
        localObject2 = n.F();
        bool3 = ((n)localObject1).a((x)localObject2);
        if (!bool3)
        {
          bool3 = false;
          localObject1 = null;
          break label1486;
        }
      }
      bool3 = true;
      label1486:
      localObject2 = ab.b();
      localObject4 = "userIm";
      localObject2 = ((ab.a)localObject2).a((CharSequence)localObject4);
      localObject1 = String.valueOf(bool3);
      localObject1 = ((ab.a)localObject2).b((CharSequence)localObject1).a();
      paramBundle.add(localObject1);
    }
    paramBundle = paramBundle.iterator();
    for (;;)
    {
      bool3 = paramBundle.hasNext();
      if (!bool3) {
        break;
      }
      localObject1 = (ab)paramBundle.next();
      localObject2 = (ae)a.a();
      ((ae)localObject2).a((org.apache.a.d.d)localObject1);
    }
    paramBundle = d;
    localObject1 = c;
    int i2 = ((com.truecaller.utils.d)localObject1).h();
    int i1 = 23;
    if (i2 >= i1)
    {
      i2 = 1;
    }
    else
    {
      i2 = 0;
      localObject1 = null;
    }
    localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>();
    localObject4 = c.k();
    if (localObject4 == null) {
      localObject4 = "NotSupported";
    }
    localObject5 = c.j();
    if (localObject5 == null) {
      localObject5 = "NotSupported";
    }
    bool10 = android.support.v4.app.ac.a(paramContext).a();
    localObject7 = new com/truecaller/analytics/e$a;
    ((e.a)localObject7).<init>("PermissionState");
    localObject4 = ((e.a)localObject7).a("SMSApp", (String)localObject4);
    localObject8 = q;
    localObject9 = new String[] { "android.permission.READ_SMS" };
    bool11 = ((l)localObject8).a((String[])localObject9);
    localObject8 = a(i2, bool11);
    localObject4 = ((e.a)localObject4).a("SMSRead", (String)localObject8).a("DialerApp", (String)localObject5);
    localObject6 = a(bool1, bool10);
    localObject4 = ((e.a)localObject4).a("NotificationsShow", (String)localObject6);
    localObject6 = q;
    localObject7 = new String[] { "android.permission.CAMERA" };
    bool10 = ((l)localObject6).a((String[])localObject7);
    localObject6 = a(i2, bool10);
    localObject4 = ((e.a)localObject4).a("Camera", (String)localObject6);
    localObject6 = q;
    localObject7 = new String[] { "android.permission.READ_CONTACTS" };
    bool10 = ((l)localObject6).a((String[])localObject7);
    localObject6 = a(i2, bool10);
    localObject4 = ((e.a)localObject4).a("Contacts", (String)localObject6);
    localObject6 = q;
    localObject7 = new String[] { "android.permission.READ_EXTERNAL_STORAGE" };
    bool10 = ((l)localObject6).a((String[])localObject7);
    localObject6 = a(i2, bool10);
    localObject4 = ((e.a)localObject4).a("Storage", (String)localObject6);
    localObject6 = q;
    localObject7 = new String[] { "android.permission.READ_PHONE_STATE" };
    bool10 = ((l)localObject6).a((String[])localObject7);
    localObject6 = a(i2, bool10);
    localObject4 = ((e.a)localObject4).a("Phone", (String)localObject6);
    localObject6 = q;
    localObject7 = new String[] { "android.permission.ACCESS_FINE_LOCATION" };
    bool10 = ((l)localObject6).a((String[])localObject7);
    localObject6 = a(i2, bool10);
    localObject4 = ((e.a)localObject4).a("Location", (String)localObject6);
    bool10 = q.a();
    localObject6 = a(i2, bool10);
    localObject4 = ((e.a)localObject4).a("DrawOnTop", (String)localObject6);
    boolean bool14 = paramBundle.a();
    paramBundle = a(bool1, bool14);
    paramBundle = ((e.a)localObject4).a("NotificationsAccess", paramBundle);
    boolean bool9 = c.f() ^ bool1;
    localObject5 = a(i2, bool9);
    paramBundle = paramBundle.a("BatteryOptimization", (String)localObject5);
    localObject4 = "SettingsWrite";
    localObject5 = c;
    bool9 = ((com.truecaller.utils.d)localObject5).p();
    localObject1 = a(i2, bool9);
    paramBundle = paramBundle.a((String)localObject4, (String)localObject1).a();
    ((List)localObject2).add(paramBundle);
    paramBundle = ((List)localObject2).iterator();
    for (;;)
    {
      bool4 = paramBundle.hasNext();
      if (!bool4) {
        break;
      }
      localObject1 = (e)paramBundle.next();
      localObject2 = b;
      ((b)localObject2).a((e)localObject1);
    }
    paramBundle = ab.b();
    paramBundle.a("systemNotificationsAllowed");
    boolean bool4 = p.a();
    localObject1 = String.valueOf(bool4);
    paramBundle.b((CharSequence)localObject1);
    paramBundle = Collections.singletonList(paramBundle.a()).iterator();
    for (;;)
    {
      bool4 = paramBundle.hasNext();
      if (!bool4) {
        break;
      }
      localObject1 = (ab)paramBundle.next();
      localObject2 = (ae)a.a();
      ((ae)localObject2).a((org.apache.a.d.d)localObject1);
    }
    paramBundle = b;
    localObject1 = at.a(paramContext).a();
    localObject2 = new com/truecaller/analytics/e$a;
    ((e.a)localObject2).<init>("AppState");
    bool9 = j.j();
    localObject2 = ((e.a)localObject2).a("DualSim", bool9);
    localObject4 = "DomainFronting";
    localObject5 = k;
    bool9 = ((com.truecaller.d.b)localObject5).a();
    ((e.a)localObject2).a((String)localObject4, bool9);
    if (localObject1 != null)
    {
      localObject4 = "CarrierMenu";
      localObject1 = a;
      ((e.a)localObject2).a((String)localObject4, (String)localObject1);
    }
    localObject1 = m.c();
    localObject4 = m.a();
    localObject5 = "Empty";
    if (localObject4 != null)
    {
      bool4 = ((String)localObject4).equals(localObject1);
      if (bool4) {
        localObject1 = "Uploaded";
      } else {
        localObject1 = "NotUploaded";
      }
      localObject5 = localObject1;
    }
    ((e.a)localObject2).a("PushId", (String)localObject5);
    localObject1 = "GoogleServices";
    localObject4 = GoogleApiAvailability.a();
    localObject5 = paramContext.getApplicationContext();
    int i4 = ((GoogleApiAvailability)localObject4).a((Context)localObject5);
    if (i4 == 0)
    {
      i4 = 1;
    }
    else
    {
      i4 = 0;
      localObject4 = null;
    }
    ((e.a)localObject2).a((String)localObject1, i4);
    localObject1 = "Region1";
    localObject4 = i;
    boolean bool7 = ((com.truecaller.common.h.ac)localObject4).a();
    if (bool7) {
      localObject4 = "Enabled";
    } else {
      localObject4 = "Disabled";
    }
    ((e.a)localObject2).a((String)localObject1, (String)localObject4);
    localObject1 = com.truecaller.common.h.h.c(paramContext);
    if (localObject1 != null)
    {
      localObject4 = b;
      if (localObject4 != null)
      {
        localObject4 = "ProfileCountry";
        localObject1 = b;
        ((e.a)localObject2).a((String)localObject4, (String)localObject1);
      }
    }
    localObject1 = e;
    localObject4 = "networkDomain";
    localObject1 = ((com.truecaller.common.g.a)localObject1).a((String)localObject4);
    if (localObject1 != null)
    {
      localObject4 = "Domain";
      ((e.a)localObject2).a((String)localObject4, (String)localObject1);
    }
    localObject1 = "SecurityPatchVersion";
    localObject4 = c.i();
    localObject5 = "Unknown";
    if (localObject4 == null) {
      localObject4 = localObject5;
    }
    ((e.a)localObject2).a((String)localObject1, (String)localObject4);
    localObject1 = c;
    int i3 = ((com.truecaller.utils.d)localObject1).h();
    int i5 = 28;
    if (i3 >= i5)
    {
      localObject1 = "usagestats";
      paramContext = (UsageStatsManager)paramContext.getSystemService((String)localObject1);
      if (paramContext != null)
      {
        localObject1 = "StandbyBucket";
        i9 = paramContext.getAppStandbyBucket();
        i5 = 10;
        if (i9 != i5)
        {
          i5 = 20;
          if (i9 != i5)
          {
            i5 = 30;
            if (i9 != i5)
            {
              i5 = 40;
              if (i9 != i5) {
                paramContext = "Unknown";
              } else {
                paramContext = "Rare";
              }
            }
            else
            {
              paramContext = "Frequent";
            }
          }
          else
          {
            paramContext = "WorkingSet";
          }
        }
        else
        {
          paramContext = "Active";
        }
        ((e.a)localObject2).a((String)localObject1, paramContext);
      }
      else
      {
        paramContext = "StandbyBucket";
        localObject1 = "CouldntGetUsageStatsMgr";
        ((e.a)localObject2).a(paramContext, (String)localObject1);
      }
    }
    paramContext = ((e.a)localObject2).a();
    paramBundle.a(paramContext);
    paramContext = b;
    paramBundle = ar.a;
    double d2 = ar.b();
    l1 = 4652007308841189376L;
    d1 = 1000.0D;
    Double.isNaN(d2);
    d2 /= d1;
    paramBundle = new com/truecaller/analytics/e$a;
    localObject6 = "DevicePerformanceMemory";
    paramBundle.<init>((String)localObject6);
    localObject1 = Double.valueOf(d2);
    a = ((Double)localObject1);
    paramBundle = paramBundle.a();
    paramContext.a(paramBundle);
    int i9 = Build.VERSION.SDK_INT;
    int i8 = 21;
    if (i9 >= i8)
    {
      paramContext = b;
      paramBundle = ar.a;
      paramBundle = ar.a();
      localObject1 = new com/truecaller/analytics/e$a;
      ((e.a)localObject1).<init>("DevicePerformanceCpu");
      localObject2 = "Valid";
      if (paramBundle == null)
      {
        bool1 = false;
        localObject3 = null;
      }
      localObject1 = ((e.a)localObject1).a((String)localObject2, bool1);
      localObject2 = Double.valueOf(0.0D);
      double d3 = ((Double)org.c.a.a.a.h.a(paramBundle, localObject2)).doubleValue() * d1;
      paramBundle = Double.valueOf(d3);
      a = paramBundle;
      paramBundle = ((e.a)localObject1).a();
      paramContext.a(paramBundle);
    }
    paramContext = s.a();
    paramBundle = new com/truecaller/analytics/-$$Lambda$AppSettingsTask$639E84OlVISHNe6dulXNN88C5eQ;
    paramBundle.<init>(this);
    paramContext.a(paramBundle);
    paramContext = new java/util/HashMap;
    paramContext.<init>();
    paramBundle = com.truecaller.utils.h.b();
    if (paramBundle != null)
    {
      localObject1 = "XiaomiMuiVersion";
      paramContext.put(localObject1, paramBundle);
    }
    paramBundle = "TruecallerX";
    localObject1 = o.e();
    boolean bool5 = ((com.truecaller.featuretoggles.b)localObject1).a();
    if (bool5) {
      localObject1 = "True";
    } else {
      localObject1 = "False";
    }
    paramContext.put(paramBundle, localObject1);
    localObject1 = l.f();
    paramContext.put("BuildName", localObject1);
    paramBundle = l.g();
    if (paramBundle == null)
    {
      paramBundle = "<null>";
    }
    else
    {
      localObject1 = "";
      bool5 = paramBundle.equals(localObject1);
      if (bool5) {
        paramBundle = "<empty>";
      }
    }
    paramContext.put("InstallerPackage", paramBundle);
    paramBundle = "LikelyToChurn";
    localObject1 = "true";
    localObject2 = u;
    localObject3 = "user_prop_churn";
    localObject2 = ((com.truecaller.abtest.c)localObject2).a((String)localObject3);
    bool5 = ((String)localObject1).equalsIgnoreCase((String)localObject2);
    if (bool5) {
      localObject1 = "Likely";
    } else {
      localObject1 = "NotLikely";
    }
    paramContext.put(paramBundle, localObject1);
    paramBundle = "LikelyToSpend";
    localObject1 = "true";
    localObject2 = u;
    localObject3 = "user_prop_spend";
    localObject2 = ((com.truecaller.abtest.c)localObject2).a((String)localObject3);
    bool5 = ((String)localObject1).equalsIgnoreCase((String)localObject2);
    if (bool5) {
      localObject1 = "Likely";
    } else {
      localObject1 = "NotLikely";
    }
    paramContext.put(paramBundle, localObject1);
    b.a(paramContext);
    return PersistentBackgroundTask.RunResult.Success;
  }
  
  public final boolean a(Context paramContext)
  {
    return ((com.truecaller.common.b.a)paramContext).u().k().c();
  }
  
  public final com.truecaller.common.background.e b()
  {
    com.truecaller.common.background.e.a locala = new com/truecaller/common/background/e$a;
    locala.<init>(1);
    TimeUnit localTimeUnit = TimeUnit.HOURS;
    locala = locala.a(12, localTimeUnit);
    c = false;
    localTimeUnit = TimeUnit.MINUTES;
    return locala.b(20, localTimeUnit).b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.AppSettingsTask
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */