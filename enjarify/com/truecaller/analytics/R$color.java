package com.truecaller.analytics;

public final class R$color
{
  public static final int common_google_signin_btn_text_dark = 2131099897;
  public static final int common_google_signin_btn_text_dark_default = 2131099898;
  public static final int common_google_signin_btn_text_dark_disabled = 2131099899;
  public static final int common_google_signin_btn_text_dark_focused = 2131099900;
  public static final int common_google_signin_btn_text_dark_pressed = 2131099901;
  public static final int common_google_signin_btn_text_light = 2131099902;
  public static final int common_google_signin_btn_text_light_default = 2131099903;
  public static final int common_google_signin_btn_text_light_disabled = 2131099904;
  public static final int common_google_signin_btn_text_light_focused = 2131099905;
  public static final int common_google_signin_btn_text_light_pressed = 2131099906;
  public static final int common_google_signin_btn_tint = 2131099907;
  public static final int notification_action_color_filter = 2131100296;
  public static final int notification_icon_bg_color = 2131100298;
  public static final int notification_material_background_media_default_color = 2131100299;
  public static final int primary_text_default_material_dark = 2131100417;
  public static final int ripple_material_light = 2131100477;
  public static final int secondary_text_default_material_dark = 2131100487;
  public static final int secondary_text_default_material_light = 2131100488;
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.R.color
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */