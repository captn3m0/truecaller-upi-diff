package com.truecaller.analytics;

import android.os.Debug;
import android.os.Debug.MemoryInfo;
import android.os.Process;
import android.os.SystemClock;
import android.system.Os;
import android.system.OsConstants;
import c.a.m;
import c.f.g.a;
import c.f.l;
import c.g.a.b;
import c.n.d;
import com.truecaller.log.AssertionUtil;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

public final class ar
{
  public static final ar a;
  
  static
  {
    ar localar = new com/truecaller/analytics/ar;
    localar.<init>();
    a = localar;
  }
  
  public static Double a()
  {
    try
    {
      localObject1 = new java/io/File;
      Object localObject2 = new java/lang/StringBuilder;
      Object localObject3 = "/proc/";
      ((StringBuilder)localObject2).<init>((String)localObject3);
      int i = Process.myPid();
      ((StringBuilder)localObject2).append(i);
      localObject3 = "/stat";
      ((StringBuilder)localObject2).append((String)localObject3);
      localObject2 = ((StringBuilder)localObject2).toString();
      ((File)localObject1).<init>((String)localObject2);
      localObject2 = d.a;
      localObject3 = "receiver$0";
      c.g.b.k.b(localObject1, (String)localObject3);
      localObject3 = "charset";
      c.g.b.k.b(localObject2, (String)localObject3);
      localObject3 = new java/util/ArrayList;
      ((ArrayList)localObject3).<init>();
      Object localObject4 = new c/f/g$a;
      ((g.a)localObject4).<init>((ArrayList)localObject3);
      localObject4 = (b)localObject4;
      Object localObject5 = "receiver$0";
      c.g.b.k.b(localObject1, (String)localObject5);
      localObject5 = "charset";
      c.g.b.k.b(localObject2, (String)localObject5);
      localObject5 = "action";
      c.g.b.k.b(localObject4, (String)localObject5);
      localObject5 = new java/io/BufferedReader;
      Object localObject6 = new java/io/InputStreamReader;
      Object localObject7 = new java/io/FileInputStream;
      ((FileInputStream)localObject7).<init>((File)localObject1);
      localObject7 = (InputStream)localObject7;
      ((InputStreamReader)localObject6).<init>((InputStream)localObject7, (Charset)localObject2);
      localObject6 = (Reader)localObject6;
      ((BufferedReader)localObject5).<init>((Reader)localObject6);
      localObject5 = (Reader)localObject5;
      l.a((Reader)localObject5, (b)localObject4);
      localObject3 = (List)localObject3;
      localObject1 = m.e((List)localObject3);
      localObject1 = (String)localObject1;
      if (localObject1 == null)
      {
        localObject1 = null;
      }
      else
      {
        localObject1 = (CharSequence)localObject1;
        localObject2 = new c/n/k;
        localObject3 = "\\s+";
        ((c.n.k)localObject2).<init>((String)localObject3);
        i = 0;
        localObject3 = null;
        localObject1 = ((c.n.k)localObject2).a((CharSequence)localObject1, 0);
        j = ((List)localObject1).size();
        i = 44;
        if (j < i) {
          localObject1 = null;
        }
      }
      if (localObject1 == null) {
        return null;
      }
      int j = OsConstants._SC_CLK_TCK;
      long l1 = Os.sysconf(j);
      long l2 = SystemClock.elapsedRealtime();
      double d1 = l2;
      double d2 = 1000.0D;
      Double.isNaN(d1);
      d1 /= d2;
      int k = 21;
      localObject6 = ((List)localObject1).get(k);
      localObject6 = (String)localObject6;
      d2 = Double.parseDouble((String)localObject6);
      double d3 = l1;
      Double.isNaN(d3);
      d2 /= d3;
      d1 -= d2;
      k = 13;
      localObject6 = ((List)localObject1).get(k);
      localObject6 = (String)localObject6;
      d2 = Double.parseDouble((String)localObject6);
      int m = 14;
      localObject1 = ((List)localObject1).get(m);
      localObject1 = (String)localObject1;
      double d4 = Double.parseDouble((String)localObject1);
      d2 += d4;
      Double.isNaN(d3);
      d2 = d2 / d3 / d1;
      return Double.valueOf(d2);
    }
    catch (IOException localIOException)
    {
      localObject1 = (Throwable)localIOException;
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localObject1);
    }
    catch (RuntimeException localRuntimeException)
    {
      Object localObject1 = (Throwable)localRuntimeException;
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localObject1);
    }
    return null;
  }
  
  public static int b()
  {
    Debug.MemoryInfo localMemoryInfo = new android/os/Debug$MemoryInfo;
    localMemoryInfo.<init>();
    Debug.getMemoryInfo(localMemoryInfo);
    return localMemoryInfo.getTotalPss();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.ar
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */