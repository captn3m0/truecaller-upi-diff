package com.truecaller.analytics;

import dagger.a.d;
import javax.inject.Provider;

public final class ak
  implements d
{
  private final Provider a;
  private final Provider b;
  
  private ak(Provider paramProvider1, Provider paramProvider2)
  {
    a = paramProvider1;
    b = paramProvider2;
  }
  
  public static ak a(Provider paramProvider1, Provider paramProvider2)
  {
    ak localak = new com/truecaller/analytics/ak;
    localak.<init>(paramProvider1, paramProvider2);
    return localak;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.ak
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */