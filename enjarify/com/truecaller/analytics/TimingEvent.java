package com.truecaller.analytics;

public enum TimingEvent
{
  private final String event;
  private final long[] eventGranularity;
  private final long[] itemGranularity;
  private final boolean unique;
  
  static
  {
    TimingEvent[] arrayOfTimingEvent = new TimingEvent[3];
    TimingEvent localTimingEvent1 = new com/truecaller/analytics/TimingEvent;
    long[] arrayOfLong1 = aw.a();
    long[] arrayOfLong2 = aw.b();
    TimingEvent localTimingEvent2 = localTimingEvent1;
    localTimingEvent1.<init>("CALL_LOG_MERGE", 0, "CallLogMerge", arrayOfLong1, arrayOfLong2, false, 8, null);
    CALL_LOG_MERGE = localTimingEvent1;
    arrayOfTimingEvent[0] = localTimingEvent1;
    localTimingEvent2 = new com/truecaller/analytics/TimingEvent;
    long[] arrayOfLong3 = aw.a();
    long[] arrayOfLong4 = aw.b();
    localTimingEvent2.<init>("CALL_LOG_FETCH_AND_MERGE", 1, "CallLogFetchMerge", arrayOfLong3, arrayOfLong4, false, 8, null);
    CALL_LOG_FETCH_AND_MERGE = localTimingEvent2;
    arrayOfTimingEvent[1] = localTimingEvent2;
    localTimingEvent2 = new com/truecaller/analytics/TimingEvent;
    long[] arrayOfLong5 = aw.c();
    localTimingEvent2.<init>("CALL_LOG_STARTUP", 2, "CallLogStartup", arrayOfLong5, null, true, 4, null);
    CALL_LOG_STARTUP = localTimingEvent2;
    arrayOfTimingEvent[2] = localTimingEvent2;
    $VALUES = arrayOfTimingEvent;
  }
  
  private TimingEvent(String paramString1, long[] paramArrayOfLong1, long[] paramArrayOfLong2, boolean paramBoolean)
  {
    event = paramString1;
    eventGranularity = paramArrayOfLong1;
    itemGranularity = paramArrayOfLong2;
    unique = paramBoolean;
  }
  
  public final String getEvent()
  {
    return event;
  }
  
  public final long[] getEventGranularity()
  {
    return eventGranularity;
  }
  
  public final long[] getItemGranularity()
  {
    return itemGranularity;
  }
  
  public final boolean getUnique()
  {
    return unique;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.TimingEvent
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */