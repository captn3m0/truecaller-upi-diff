package com.truecaller.analytics;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.text.TextUtils;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.old.data.access.Settings;
import java.util.Iterator;
import java.util.List;

public class InstallReferrerReceiver
  extends BroadcastReceiver
{
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    if (paramIntent == null) {
      return;
    }
    Object localObject1 = "com.android.vending.INSTALL_REFERRER";
    String str1 = paramIntent.getAction();
    boolean bool1 = ((String)localObject1).equals(str1);
    if (bool1)
    {
      boolean bool2 = true;
      Settings.a("TRACK_ANALYTICS", bool2);
      Settings.a("CAMPAIGN_USER", bool2);
      localObject1 = paramContext.getPackageManager();
      Object localObject2 = new android/content/Intent;
      ((Intent)localObject2).<init>("com.android.vending.INSTALL_REFERRER");
      localObject1 = ((PackageManager)localObject1).queryBroadcastReceivers((Intent)localObject2, 0).iterator();
      for (;;)
      {
        boolean bool3 = ((Iterator)localObject1).hasNext();
        if (!bool3) {
          break;
        }
        localObject2 = (ResolveInfo)((Iterator)localObject1).next();
        Object localObject3 = paramIntent.getAction();
        Object localObject5 = getClass().getName();
        Object localObject6 = activityInfo.name;
        boolean bool4 = ((String)localObject5).equals(localObject6);
        if (!bool4)
        {
          localObject5 = "com.android.vending.INSTALL_REFERRER";
          boolean bool5 = ((String)localObject5).equals(localObject3);
          if (bool5) {
            try
            {
              localObject3 = activityInfo;
              localObject3 = name;
              localObject3 = Class.forName((String)localObject3);
              localObject3 = ((Class)localObject3).newInstance();
              localObject3 = (BroadcastReceiver)localObject3;
              ((BroadcastReceiver)localObject3).onReceive(paramContext, paramIntent);
            }
            finally
            {
              localObject5 = new String[bool2];
              localObject6 = new java/lang/StringBuilder;
              String str2 = "Exception in BroadcastReceiver ";
              ((StringBuilder)localObject6).<init>(str2);
              localObject2 = activityInfo.name;
              ((StringBuilder)localObject6).append((String)localObject2);
              ((StringBuilder)localObject6).append(": ");
              ((StringBuilder)localObject6).append(localObject4);
              localObject2 = ((StringBuilder)localObject6).toString();
              localObject5[0] = localObject2;
            }
          }
        }
      }
    }
    paramContext = paramIntent.getStringExtra("referrer");
    paramIntent = new com/truecaller/analytics/e$a;
    localObject1 = "ANDROID_MAIN_UserAquiredThroughCampaign";
    paramIntent.<init>((String)localObject1);
    bool1 = TextUtils.isEmpty(paramContext);
    if (!bool1)
    {
      localObject1 = "Campaign_ID";
      paramIntent.a((String)localObject1, paramContext);
    }
    paramContext = TrueApp.y().a().c();
    paramIntent = paramIntent.a();
    paramContext.a(paramIntent);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.InstallReferrerReceiver
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */