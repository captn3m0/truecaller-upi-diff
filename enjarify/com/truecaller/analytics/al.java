package com.truecaller.analytics;

import android.app.Application;
import c.g.a.a;
import c.g.a.m;
import c.g.b.k;
import c.g.b.t;
import c.g.b.u;
import c.g.b.w;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import java.util.Map;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ax;
import kotlinx.coroutines.bg;

public final class al
  implements b
{
  private final c.f b;
  private final String c;
  
  static
  {
    c.l.g[] arrayOfg = new c.l.g[1];
    Object localObject = new c/g/b/u;
    c.l.b localb = w.a(al.class);
    ((u)localObject).<init>(localb, "logger", "getLogger()Lcom/facebook/appevents/AppEventsLogger;");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[0] = localObject;
    a = arrayOfg;
  }
  
  public al(Application paramApplication)
  {
    al.d locald = new com/truecaller/analytics/al$d;
    locald.<init>(paramApplication);
    paramApplication = c.g.a((a)locald);
    b = paramApplication;
    c = "facebook";
  }
  
  public final void a(long paramLong, String paramString)
  {
    ag localag = (ag)bg.a;
    c.d.f localf = (c.d.f)ax.b();
    Object localObject = new com/truecaller/analytics/al$b;
    ((al.b)localObject).<init>(this, paramLong, paramString, null);
    localObject = (m)localObject;
    kotlinx.coroutines.e.b(localag, localf, (m)localObject, 2);
  }
  
  public final void a(e parame)
  {
    k.b(parame, "event");
    b(parame);
  }
  
  public final void a(Map paramMap)
  {
    Object localObject1 = "properties";
    k.b(paramMap, (String)localObject1);
    boolean bool = paramMap.isEmpty();
    if (bool) {
      return;
    }
    localObject1 = (ag)bg.a;
    c.d.f localf = (c.d.f)ax.b();
    Object localObject2 = new com/truecaller/analytics/al$c;
    ((al.c)localObject2).<init>(paramMap, null);
    localObject2 = (m)localObject2;
    kotlinx.coroutines.e.b((ag)localObject1, localf, (m)localObject2, 2);
  }
  
  public final void b(e parame)
  {
    k.b(parame, "event");
    Object localObject1 = parame.a();
    int i = ((String)localObject1).length();
    boolean bool = true;
    Object localObject2 = null;
    int k = 40;
    if (i <= k)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject1 = null;
    }
    String[] arrayOfString = { "A Facebook app event name can not be longer than 40 characters" };
    AssertionUtil.OnlyInDebug.isTrue(i, arrayOfString);
    localObject1 = parame.b();
    int j;
    if (localObject1 != null)
    {
      j = ((Map)localObject1).size();
    }
    else
    {
      j = 0;
      localObject1 = null;
    }
    int m = 25;
    if (j > m)
    {
      bool = false;
      localf = null;
    }
    localObject1 = new String[] { "A Facebook app event can't have more than 25 parameters" };
    AssertionUtil.OnlyInDebug.isTrue(bool, (String[])localObject1);
    localObject1 = (ag)bg.a;
    c.d.f localf = (c.d.f)ax.b();
    localObject2 = new com/truecaller/analytics/al$a;
    ((al.a)localObject2).<init>(this, parame, null);
    localObject2 = (m)localObject2;
    kotlinx.coroutines.e.b((ag)localObject1, localf, (m)localObject2, 2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.al
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */