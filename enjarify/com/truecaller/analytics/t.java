package com.truecaller.analytics;

import c.g.b.k;
import com.truecaller.common.account.r;
import com.truecaller.common.g.a;
import com.truecaller.common.network.util.KnownEndpoints;
import okhttp3.u;
import okhttp3.u.a;

public final class t
  implements s
{
  public CharSequence a;
  public r b;
  public a c;
  private final CharSequence d;
  private final CharSequence e;
  
  public t(CharSequence paramCharSequence1, CharSequence paramCharSequence2)
  {
    d = paramCharSequence1;
    e = paramCharSequence2;
  }
  
  public final CharSequence a()
  {
    return d;
  }
  
  public final CharSequence b()
  {
    return e;
  }
  
  public final CharSequence c()
  {
    CharSequence localCharSequence = a;
    if (localCharSequence == null)
    {
      String str = "appBuild";
      k.a(str);
    }
    return localCharSequence;
  }
  
  public final long d()
  {
    a locala = c;
    if (locala == null)
    {
      String str = "coreSettings";
      k.a(str);
    }
    return locala.a("profileUserId", -1);
  }
  
  public final u e()
  {
    u localu = KnownEndpoints.BATCHLOG.url().j().c("/v5/events").b();
    k.a(localu, "KnownEndpoints.BATCHLOG.…ts\")\n            .build()");
    return localu;
  }
  
  public final okhttp3.t f()
  {
    Object localObject1 = b;
    if (localObject1 == null)
    {
      localObject2 = "accountManager";
      k.a((String)localObject2);
    }
    localObject1 = ((r)localObject1).e();
    if (localObject1 == null) {
      return null;
    }
    Object localObject2 = new String[2];
    localObject2[0] = "Authorization";
    localObject1 = String.valueOf(localObject1);
    localObject1 = "Bearer ".concat((String)localObject1);
    localObject2[1] = localObject1;
    return okhttp3.t.a((String[])localObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */