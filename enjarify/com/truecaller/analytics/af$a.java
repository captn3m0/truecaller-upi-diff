package com.truecaller.analytics;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import okhttp3.y;
import org.apache.a.d.d;

final class af$a
  extends u
{
  private final d b;
  private final y c;
  
  private af$a(e parame, d paramd, y paramy)
  {
    super(parame);
    b = paramd;
    c = paramy;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".trackEventImmediately(");
    String str = a(b, 1);
    localStringBuilder.append(str);
    localStringBuilder.append(",");
    str = a(c, 2);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.af.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */