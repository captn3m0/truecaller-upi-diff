package com.truecaller.analytics;

import java.util.concurrent.atomic.AtomicInteger;

public final class ac
{
  public boolean a;
  AtomicInteger b;
  
  public ac()
  {
    AtomicInteger localAtomicInteger = new java/util/concurrent/atomic/AtomicInteger;
    localAtomicInteger.<init>(0);
    b = localAtomicInteger;
  }
  
  public final void a(int paramInt)
  {
    b.addAndGet(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.ac
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */