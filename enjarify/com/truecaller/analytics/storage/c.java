package com.truecaller.analytics.storage;

public final class c
{
  public final long a;
  public final byte[] b;
  
  public c(long paramLong, byte[] paramArrayOfByte)
  {
    a = paramLong;
    b = paramArrayOfByte;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = paramObject instanceof c;
    if (bool1)
    {
      paramObject = (c)paramObject;
      long l1 = a;
      long l2 = a;
      boolean bool2 = l1 < l2;
      if (!bool2) {
        return true;
      }
    }
    return false;
  }
  
  public final int hashCode()
  {
    return Long.valueOf(a).hashCode();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.storage.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */