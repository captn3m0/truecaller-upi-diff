package com.truecaller.analytics.storage;

import android.content.Context;
import android.content.SharedPreferences;
import c.a.an;
import c.g.b.k;
import com.truecaller.log.UnmutedException.i;
import com.truecaller.log.d;
import com.truecaller.utils.a.c.a;

public final class b
  extends com.truecaller.utils.a.a
  implements a
{
  private final int b;
  private final String c;
  private final SharedPreferences d;
  
  public b(SharedPreferences paramSharedPreferences)
  {
    super(paramSharedPreferences);
    d = paramSharedPreferences;
    b = 1;
    c = "analytics";
  }
  
  public final int a()
  {
    return b;
  }
  
  public final void a(int paramInt, Context paramContext)
  {
    Object localObject1 = "context";
    k.b(paramContext, (String)localObject1);
    if (paramInt <= 0)
    {
      Object localObject2 = paramContext.getSharedPreferences("tc.settings", 0);
      k.a(localObject2, "oldSharedPreferences");
      String str1 = "uploadEventsMaxBatchSize";
      String str2 = "uploadEventsMinBatchSize";
      String[] tmp40_37 = new String[4];
      String[] tmp41_40 = tmp40_37;
      String[] tmp41_40 = tmp40_37;
      tmp41_40[0] = "uploadEventsRetryJitter";
      tmp41_40[1] = "analyticsID";
      tmp41_40[2] = str1;
      String[] tmp54_41 = tmp41_40;
      tmp54_41[3] = str2;
      paramContext = an.a(tmp54_41);
      a((SharedPreferences)localObject2, paramContext);
      localObject2 = "analyticsUploadEnhancedBatchSize";
      paramContext = d;
      k.b(localObject2, "key");
      localObject1 = "source";
      k.b(paramContext, (String)localObject1);
      localObject2 = c.a.a(this, (String)localObject2, paramContext);
      if (localObject2 != null)
      {
        paramContext = new com/truecaller/log/UnmutedException$i;
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>();
        str1 = localObject2.getClass().getCanonicalName();
        ((StringBuilder)localObject1).append(str1);
        ((StringBuilder)localObject1).append(": ");
        localObject2 = ((Exception)localObject2).getMessage();
        ((StringBuilder)localObject1).append((String)localObject2);
        localObject2 = ((StringBuilder)localObject1).toString();
        paramContext.<init>((String)localObject2);
        d.a((Throwable)paramContext);
        d("analyticsUploadEnhancedBatchSize");
        return;
      }
    }
  }
  
  public final String b()
  {
    return c;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.storage.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */