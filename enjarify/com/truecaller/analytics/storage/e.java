package com.truecaller.analytics.storage;

import android.arch.persistence.room.i;
import android.arch.persistence.room.j;
import android.database.Cursor;
import java.util.ArrayList;
import java.util.List;

public final class e
  implements d
{
  private final android.arch.persistence.room.f a;
  private final android.arch.persistence.room.c b;
  private final j c;
  
  public e(android.arch.persistence.room.f paramf)
  {
    a = paramf;
    Object localObject = new com/truecaller/analytics/storage/e$1;
    ((e.1)localObject).<init>(this, paramf);
    b = ((android.arch.persistence.room.c)localObject);
    localObject = new com/truecaller/analytics/storage/e$2;
    ((e.2)localObject).<init>(this, paramf);
    c = ((j)localObject);
  }
  
  public final int a(long paramLong)
  {
    android.arch.persistence.db.f localf = c.b();
    android.arch.persistence.room.f localf1 = a;
    localf1.d();
    int i = 1;
    try
    {
      localf.a(i, paramLong);
      int j = localf.a();
      android.arch.persistence.room.f localf2 = a;
      localf2.f();
      return j;
    }
    finally
    {
      a.e();
      c.a(localf);
    }
  }
  
  public final List a(int paramInt)
  {
    int i = 1;
    i locali = i.a("SELECT * FROM persisted_event ORDER BY id ASC LIMIT ?", i);
    long l1 = paramInt;
    locali.a(i, l1);
    Cursor localCursor = a.a(locali);
    String str1 = "id";
    try
    {
      i = localCursor.getColumnIndexOrThrow(str1);
      String str2 = "record";
      int j = localCursor.getColumnIndexOrThrow(str2);
      ArrayList localArrayList = new java/util/ArrayList;
      int k = localCursor.getCount();
      localArrayList.<init>(k);
      for (;;)
      {
        boolean bool = localCursor.moveToNext();
        if (!bool) {
          break;
        }
        long l2 = localCursor.getLong(i);
        byte[] arrayOfByte = localCursor.getBlob(j);
        c localc = new com/truecaller/analytics/storage/c;
        localc.<init>(l2, arrayOfByte);
        localArrayList.add(localc);
      }
      return localArrayList;
    }
    finally
    {
      localCursor.close();
      locali.b();
    }
  }
  
  public final void a(c paramc)
  {
    Object localObject = a;
    ((android.arch.persistence.room.f)localObject).d();
    try
    {
      localObject = b;
      ((android.arch.persistence.room.c)localObject).a(paramc);
      paramc = a;
      paramc.f();
      return;
    }
    finally
    {
      a.e();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.storage.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */