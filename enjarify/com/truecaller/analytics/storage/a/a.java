package com.truecaller.analytics.storage.a;

import android.arch.persistence.room.f.b;
import android.content.Context;

public final class a
  extends f.b
{
  private final Context a;
  
  public a(Context paramContext)
  {
    a = paramContext;
  }
  
  /* Error */
  public final void a(android.arch.persistence.db.b paramb)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc 22
    //   3: invokestatic 14	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: new 24	java/io/File
    //   9: astore_2
    //   10: aload_0
    //   11: getfield 20	com/truecaller/analytics/storage/a/a:a	Landroid/content/Context;
    //   14: invokevirtual 30	android/content/Context:getFilesDir	()Ljava/io/File;
    //   17: astore_3
    //   18: ldc 32
    //   20: astore 4
    //   22: aload_2
    //   23: aload_3
    //   24: aload 4
    //   26: invokespecial 35	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   29: aload_2
    //   30: invokevirtual 39	java/io/File:exists	()Z
    //   33: istore 5
    //   35: iload 5
    //   37: ifne +4 -> 41
    //   40: return
    //   41: new 41	com/truecaller/analytics/storage/a/b
    //   44: astore_3
    //   45: aload_3
    //   46: aload_2
    //   47: invokespecial 44	com/truecaller/analytics/storage/a/b:<init>	(Ljava/io/File;)V
    //   50: aload_3
    //   51: invokevirtual 47	com/truecaller/analytics/storage/a/b:a	()Lcom/truecaller/analytics/storage/a/b$d;
    //   54: astore_3
    //   55: aload_3
    //   56: ifnonnull +9 -> 65
    //   59: aload_2
    //   60: invokevirtual 50	java/io/File:delete	()Z
    //   63: pop
    //   64: return
    //   65: aload_3
    //   66: invokeinterface 55 1 0
    //   71: astore 4
    //   73: aload 4
    //   75: ifnonnull +9 -> 84
    //   78: aload_2
    //   79: invokevirtual 50	java/io/File:delete	()Z
    //   82: pop
    //   83: return
    //   84: new 57	android/content/ContentValues
    //   87: astore 6
    //   89: aload 6
    //   91: invokespecial 58	android/content/ContentValues:<init>	()V
    //   94: ldc 60
    //   96: astore 7
    //   98: aload 6
    //   100: aload 7
    //   102: aload 4
    //   104: invokevirtual 64	android/content/ContentValues:put	(Ljava/lang/String;[B)V
    //   107: ldc 66
    //   109: astore 4
    //   111: aconst_null
    //   112: astore 7
    //   114: aload_1
    //   115: aload 4
    //   117: iconst_0
    //   118: aload 6
    //   120: invokeinterface 71 4 0
    //   125: pop2
    //   126: goto -61 -> 65
    //   129: astore_1
    //   130: goto +35 -> 165
    //   133: astore_1
    //   134: aload_1
    //   135: checkcast 73	java/lang/Throwable
    //   138: astore_1
    //   139: aload_1
    //   140: invokestatic 79	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   143: aload_2
    //   144: invokevirtual 50	java/io/File:delete	()Z
    //   147: pop
    //   148: return
    //   149: astore_1
    //   150: aload_1
    //   151: checkcast 73	java/lang/Throwable
    //   154: astore_1
    //   155: aload_1
    //   156: invokestatic 79	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   159: aload_2
    //   160: invokevirtual 50	java/io/File:delete	()Z
    //   163: pop
    //   164: return
    //   165: aload_2
    //   166: invokevirtual 50	java/io/File:delete	()Z
    //   169: pop
    //   170: aload_1
    //   171: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	172	0	this	a
    //   0	172	1	paramb	android.arch.persistence.db.b
    //   9	157	2	localFile	java.io.File
    //   17	49	3	localObject1	Object
    //   20	96	4	localObject2	Object
    //   33	3	5	bool	boolean
    //   87	32	6	localContentValues	android.content.ContentValues
    //   96	17	7	str	String
    // Exception table:
    //   from	to	target	type
    //   41	44	129	finally
    //   46	50	129	finally
    //   50	54	129	finally
    //   65	71	129	finally
    //   84	87	129	finally
    //   89	94	129	finally
    //   102	107	129	finally
    //   118	126	129	finally
    //   134	138	129	finally
    //   139	143	129	finally
    //   150	154	129	finally
    //   155	159	129	finally
    //   41	44	133	com/truecaller/analytics/storage/a/b$a
    //   46	50	133	com/truecaller/analytics/storage/a/b$a
    //   50	54	133	com/truecaller/analytics/storage/a/b$a
    //   65	71	133	com/truecaller/analytics/storage/a/b$a
    //   84	87	133	com/truecaller/analytics/storage/a/b$a
    //   89	94	133	com/truecaller/analytics/storage/a/b$a
    //   102	107	133	com/truecaller/analytics/storage/a/b$a
    //   118	126	133	com/truecaller/analytics/storage/a/b$a
    //   41	44	149	java/io/IOException
    //   46	50	149	java/io/IOException
    //   50	54	149	java/io/IOException
    //   65	71	149	java/io/IOException
    //   84	87	149	java/io/IOException
    //   89	94	149	java/io/IOException
    //   102	107	149	java/io/IOException
    //   118	126	149	java/io/IOException
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.storage.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */