package com.truecaller.analytics.storage.a;

final class b$b
{
  static final b a;
  final int b;
  final int c;
  final long d;
  
  static
  {
    b localb = new com/truecaller/analytics/storage/a/b$b;
    localb.<init>(0, 0, 0L);
    a = localb;
  }
  
  b$b(int paramInt1, int paramInt2, long paramLong)
  {
    b = paramInt1;
    c = paramInt2;
    d = paramLong;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    String str = getClass().getSimpleName();
    localStringBuilder.append(str);
    localStringBuilder.append("[position = ");
    int i = b;
    localStringBuilder.append(i);
    localStringBuilder.append(", length = ");
    i = c;
    localStringBuilder.append(i);
    localStringBuilder.append(", checksum = ");
    long l = d;
    localStringBuilder.append(l);
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.storage.a.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */