package com.truecaller.analytics.storage.a;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.zip.CRC32;

final class b
  implements Closeable
{
  private final RandomAccessFile a;
  private int b;
  private int c;
  private b.b d;
  private b.b e;
  private final byte[] f;
  private final CRC32 g;
  
  b(File paramFile)
  {
    int i = 16;
    Object localObject1 = new byte[i];
    f = ((byte[])localObject1);
    localObject1 = new java/util/zip/CRC32;
    ((CRC32)localObject1).<init>();
    g = ((CRC32)localObject1);
    boolean bool2 = paramFile.exists();
    long l1 = 0L;
    if (!bool2)
    {
      localObject1 = new java/io/File;
      Object localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      String str = paramFile.getPath();
      ((StringBuilder)localObject2).append(str);
      ((StringBuilder)localObject2).append(".tmp");
      localObject2 = ((StringBuilder)localObject2).toString();
      ((File)localObject1).<init>((String)localObject2);
      localObject2 = new java/io/RandomAccessFile;
      str = "rwd";
      ((RandomAccessFile)localObject2).<init>((File)localObject1, str);
      long l2 = 4096L;
      try
      {
        ((RandomAccessFile)localObject2).setLength(l2);
        ((RandomAccessFile)localObject2).seek(l1);
        localObject3 = new byte[i];
        int m = 4096;
        a((byte[])localObject3, 0, m);
        ((RandomAccessFile)localObject2).write((byte[])localObject3);
        ((RandomAccessFile)localObject2).seek(l1);
        ((RandomAccessFile)localObject2).close();
        boolean bool1 = ((File)localObject1).renameTo(paramFile);
        if (!bool1)
        {
          paramFile = new java/io/IOException;
          paramFile.<init>("Rename failed!");
          throw paramFile;
        }
      }
      finally
      {
        ((RandomAccessFile)localObject2).close();
      }
    }
    paramFile = a(paramFile);
    a = paramFile;
    a.seek(l1);
    paramFile = a;
    Object localObject3 = f;
    paramFile.readFully((byte[])localObject3);
    int n = a(f, 0);
    b = n;
    long l3 = b;
    paramFile = a;
    long l4 = paramFile.length();
    boolean bool3 = l3 < l4;
    if (!bool3)
    {
      int i1 = b;
      if (i1 > 0)
      {
        i1 = a(f, 4);
        c = i1;
        i1 = a(f, 8);
        int j = a(f, 12);
        paramFile = a(i1);
        d = paramFile;
        paramFile = a(j);
        e = paramFile;
        return;
      }
      paramFile = new java/io/IOException;
      localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>("File is corrupt; length stored in header (");
      k = b;
      ((StringBuilder)localObject3).append(k);
      ((StringBuilder)localObject3).append(") is invalid.");
      localObject3 = ((StringBuilder)localObject3).toString();
      paramFile.<init>((String)localObject3);
      throw paramFile;
    }
    paramFile = new java/io/IOException;
    localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>("File is truncated. Expected length: ");
    int k = b;
    ((StringBuilder)localObject3).append(k);
    ((StringBuilder)localObject3).append(", Actual length: ");
    long l5 = a.length();
    ((StringBuilder)localObject3).append(l5);
    localObject3 = ((StringBuilder)localObject3).toString();
    paramFile.<init>((String)localObject3);
    throw paramFile;
  }
  
  private static int a(int paramInt1, int paramInt2)
  {
    while (paramInt1 < paramInt2) {
      paramInt1 <<= 1;
    }
    return paramInt1;
  }
  
  private static int a(byte[] paramArrayOfByte, int paramInt)
  {
    int i = (paramArrayOfByte[paramInt] & 0xFF) << 24;
    int j = paramInt + 1;
    j = (paramArrayOfByte[j] & 0xFF) << 16;
    i += j;
    j = paramInt + 2;
    j = (paramArrayOfByte[j] & 0xFF) << 8;
    i += j;
    paramInt += 3;
    int k = paramArrayOfByte[paramInt] & 0xFF;
    return i + k;
  }
  
  private static long a(byte[] paramArrayOfByte)
  {
    long l1 = (paramArrayOfByte[4] & 0xFF) << 24;
    long l2 = (paramArrayOfByte[5] & 0xFF) << 16;
    l1 += l2;
    l2 = (paramArrayOfByte[6] & 0xFF) << 8;
    l1 += l2;
    l2 = paramArrayOfByte[7] & 0xFF;
    return l1 + l2;
  }
  
  private b.b a(int paramInt)
  {
    if (paramInt == 0) {
      return b.b.a;
    }
    byte[] arrayOfByte = f;
    a(paramInt, arrayOfByte, 8);
    int i = a(f, 0);
    long l = a(f);
    b.b localb = new com/truecaller/analytics/storage/a/b$b;
    localb.<init>(paramInt, i, l);
    return localb;
  }
  
  /* Error */
  private RandomAccessFile a(File paramFile)
  {
    // Byte code:
    //   0: aload_1
    //   1: astore_2
    //   2: new 57	java/io/RandomAccessFile
    //   5: astore_3
    //   6: ldc 59
    //   8: astore 4
    //   10: aload_3
    //   11: aload_1
    //   12: aload 4
    //   14: invokespecial 62	java/io/RandomAccessFile:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   17: lconst_0
    //   18: lstore 5
    //   20: aload_3
    //   21: lload 5
    //   23: invokevirtual 73	java/io/RandomAccessFile:seek	(J)V
    //   26: aload_0
    //   27: getfield 25	com/truecaller/analytics/storage/a/b:f	[B
    //   30: astore 4
    //   32: aload_3
    //   33: aload 4
    //   35: invokevirtual 101	java/io/RandomAccessFile:readFully	([B)V
    //   38: aload_0
    //   39: getfield 25	com/truecaller/analytics/storage/a/b:f	[B
    //   42: astore 4
    //   44: iconst_0
    //   45: istore 7
    //   47: aload 4
    //   49: iconst_0
    //   50: invokestatic 104	com/truecaller/analytics/storage/a/b:a	([BI)I
    //   53: istore 8
    //   55: aload_0
    //   56: getfield 25	com/truecaller/analytics/storage/a/b:f	[B
    //   59: astore 9
    //   61: iconst_4
    //   62: istore 10
    //   64: aload 9
    //   66: iload 10
    //   68: invokestatic 104	com/truecaller/analytics/storage/a/b:a	([BI)I
    //   71: istore 11
    //   73: aload_0
    //   74: getfield 25	com/truecaller/analytics/storage/a/b:f	[B
    //   77: astore 12
    //   79: bipush 8
    //   81: istore 13
    //   83: aload 12
    //   85: iload 13
    //   87: invokestatic 104	com/truecaller/analytics/storage/a/b:a	([BI)I
    //   90: istore 14
    //   92: aload_0
    //   93: getfield 25	com/truecaller/analytics/storage/a/b:f	[B
    //   96: astore 15
    //   98: bipush 12
    //   100: istore 16
    //   102: aload 15
    //   104: iload 16
    //   106: invokestatic 104	com/truecaller/analytics/storage/a/b:a	([BI)I
    //   109: istore 17
    //   111: iload 14
    //   113: iload 17
    //   115: if_icmpgt +506 -> 621
    //   118: iload 17
    //   120: bipush 8
    //   122: iadd
    //   123: istore 18
    //   125: iload 18
    //   127: iload 8
    //   129: if_icmpgt +492 -> 621
    //   132: iload 14
    //   134: ifeq +487 -> 621
    //   137: bipush 16
    //   139: istore 19
    //   141: iload 14
    //   143: iload 19
    //   145: if_icmpeq +476 -> 621
    //   148: iload 8
    //   150: iload 14
    //   152: if_icmpge +6 -> 158
    //   155: goto +466 -> 621
    //   158: iload 17
    //   160: i2l
    //   161: lstore 5
    //   163: aload_3
    //   164: lload 5
    //   166: invokevirtual 73	java/io/RandomAccessFile:seek	(J)V
    //   169: aload_0
    //   170: getfield 25	com/truecaller/analytics/storage/a/b:f	[B
    //   173: astore 20
    //   175: aload_3
    //   176: aload 20
    //   178: iconst_0
    //   179: iload 13
    //   181: invokevirtual 159	java/io/RandomAccessFile:readFully	([BII)V
    //   184: aload_0
    //   185: getfield 25	com/truecaller/analytics/storage/a/b:f	[B
    //   188: astore 20
    //   190: aload 20
    //   192: iconst_0
    //   193: invokestatic 104	com/truecaller/analytics/storage/a/b:a	([BI)I
    //   196: istore 21
    //   198: iload 18
    //   200: iload 21
    //   202: iadd
    //   203: istore 18
    //   205: iload 18
    //   207: iload 8
    //   209: if_icmple +5 -> 214
    //   212: aload_3
    //   213: areturn
    //   214: new 32	java/io/File
    //   217: astore 22
    //   219: new 38	java/lang/StringBuilder
    //   222: astore 4
    //   224: aload 4
    //   226: invokespecial 39	java/lang/StringBuilder:<init>	()V
    //   229: aload_1
    //   230: invokevirtual 162	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   233: astore 23
    //   235: aload 4
    //   237: aload 23
    //   239: invokevirtual 47	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   242: pop
    //   243: ldc 49
    //   245: astore 23
    //   247: aload 4
    //   249: aload 23
    //   251: invokevirtual 47	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   254: pop
    //   255: aload 4
    //   257: invokevirtual 52	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   260: astore 4
    //   262: aload 22
    //   264: aload 4
    //   266: invokespecial 55	java/io/File:<init>	(Ljava/lang/String;)V
    //   269: new 57	java/io/RandomAccessFile
    //   272: astore 23
    //   274: ldc 59
    //   276: astore 4
    //   278: aload 23
    //   280: aload 22
    //   282: aload 4
    //   284: invokespecial 62	java/io/RandomAccessFile:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   287: iload 14
    //   289: bipush -16
    //   291: iadd
    //   292: istore 8
    //   294: sipush 4096
    //   297: istore 16
    //   299: iload 17
    //   301: iload 14
    //   303: isub
    //   304: bipush 16
    //   306: iadd
    //   307: bipush 8
    //   309: iadd
    //   310: istore 24
    //   312: iload 24
    //   314: iload 21
    //   316: iadd
    //   317: istore 21
    //   319: iload 16
    //   321: iload 21
    //   323: invokestatic 165	com/truecaller/analytics/storage/a/b:a	(II)I
    //   326: istore 21
    //   328: iload 21
    //   330: i2l
    //   331: lstore 25
    //   333: aload 23
    //   335: lload 25
    //   337: invokevirtual 70	java/io/RandomAccessFile:setLength	(J)V
    //   340: bipush 16
    //   342: i2l
    //   343: lstore 25
    //   345: aload 23
    //   347: lload 25
    //   349: invokevirtual 73	java/io/RandomAccessFile:seek	(J)V
    //   352: iload 14
    //   354: i2l
    //   355: lstore 25
    //   357: aload_3
    //   358: lload 25
    //   360: invokevirtual 73	java/io/RandomAccessFile:seek	(J)V
    //   363: aload 23
    //   365: invokevirtual 171	java/io/RandomAccessFile:getChannel	()Ljava/nio/channels/FileChannel;
    //   368: astore 27
    //   370: aload_3
    //   371: invokevirtual 171	java/io/RandomAccessFile:getChannel	()Ljava/nio/channels/FileChannel;
    //   374: astore 28
    //   376: bipush 16
    //   378: i2l
    //   379: lstore 29
    //   381: iload 18
    //   383: iload 14
    //   385: isub
    //   386: istore 18
    //   388: iload 18
    //   390: i2l
    //   391: lstore 31
    //   393: aload 27
    //   395: aload 28
    //   397: lload 29
    //   399: lload 31
    //   401: invokevirtual 177	java/nio/channels/FileChannel:transferFrom	(Ljava/nio/channels/ReadableByteChannel;JJ)J
    //   404: pop2
    //   405: aload_0
    //   406: getfield 25	com/truecaller/analytics/storage/a/b:f	[B
    //   409: astore 12
    //   411: aload 12
    //   413: iconst_0
    //   414: iload 21
    //   416: invokestatic 77	com/truecaller/analytics/storage/a/b:a	([BII)V
    //   419: aload_0
    //   420: getfield 25	com/truecaller/analytics/storage/a/b:f	[B
    //   423: astore 20
    //   425: aload 20
    //   427: iload 10
    //   429: iload 11
    //   431: invokestatic 77	com/truecaller/analytics/storage/a/b:a	([BII)V
    //   434: aload_0
    //   435: getfield 25	com/truecaller/analytics/storage/a/b:f	[B
    //   438: astore 20
    //   440: bipush 8
    //   442: istore 7
    //   444: aload 20
    //   446: iload 7
    //   448: iload 19
    //   450: invokestatic 77	com/truecaller/analytics/storage/a/b:a	([BII)V
    //   453: aload_0
    //   454: getfield 25	com/truecaller/analytics/storage/a/b:f	[B
    //   457: astore 20
    //   459: iload 17
    //   461: iload 8
    //   463: isub
    //   464: istore 17
    //   466: bipush 12
    //   468: istore 8
    //   470: aload 20
    //   472: iload 8
    //   474: iload 17
    //   476: invokestatic 77	com/truecaller/analytics/storage/a/b:a	([BII)V
    //   479: lconst_0
    //   480: lstore 33
    //   482: aload 23
    //   484: lload 33
    //   486: invokevirtual 73	java/io/RandomAccessFile:seek	(J)V
    //   489: aload_0
    //   490: getfield 25	com/truecaller/analytics/storage/a/b:f	[B
    //   493: astore 4
    //   495: aload 23
    //   497: aload 4
    //   499: invokevirtual 81	java/io/RandomAccessFile:write	([B)V
    //   502: aload 23
    //   504: invokevirtual 84	java/io/RandomAccessFile:close	()V
    //   507: aload_3
    //   508: invokevirtual 84	java/io/RandomAccessFile:close	()V
    //   511: goto +35 -> 546
    //   514: astore 4
    //   516: goto +16 -> 532
    //   519: astore 4
    //   521: aconst_null
    //   522: astore 23
    //   524: goto +64 -> 588
    //   527: astore 4
    //   529: aconst_null
    //   530: astore 23
    //   532: aload 4
    //   534: invokestatic 183	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   537: aload 23
    //   539: ifnull -32 -> 507
    //   542: goto -40 -> 502
    //   545: pop
    //   546: aload 23
    //   548: ifnull +22 -> 570
    //   551: aload 22
    //   553: aload_2
    //   554: invokevirtual 88	java/io/File:renameTo	(Ljava/io/File;)Z
    //   557: istore 8
    //   559: iload 8
    //   561: ifne +9 -> 570
    //   564: aload 22
    //   566: invokevirtual 186	java/io/File:delete	()Z
    //   569: pop
    //   570: new 57	java/io/RandomAccessFile
    //   573: astore 4
    //   575: aload 4
    //   577: aload_2
    //   578: ldc 59
    //   580: invokespecial 62	java/io/RandomAccessFile:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   583: aload 4
    //   585: areturn
    //   586: astore 4
    //   588: aload 23
    //   590: ifnull +8 -> 598
    //   593: aload 23
    //   595: invokevirtual 84	java/io/RandomAccessFile:close	()V
    //   598: aload_3
    //   599: invokevirtual 84	java/io/RandomAccessFile:close	()V
    //   602: aload 4
    //   604: athrow
    //   605: pop
    //   606: new 188	com/truecaller/analytics/storage/a/b$a
    //   609: astore 4
    //   611: aload 4
    //   613: ldc -66
    //   615: invokespecial 191	com/truecaller/analytics/storage/a/b$a:<init>	(Ljava/lang/String;)V
    //   618: aload 4
    //   620: athrow
    //   621: aload_3
    //   622: areturn
    //   623: pop
    //   624: new 188	com/truecaller/analytics/storage/a/b$a
    //   627: astore 4
    //   629: aload 4
    //   631: ldc -63
    //   633: invokespecial 191	com/truecaller/analytics/storage/a/b$a:<init>	(Ljava/lang/String;)V
    //   636: aload 4
    //   638: athrow
    //   639: pop
    //   640: goto -133 -> 507
    //   643: pop
    //   644: goto -46 -> 598
    //   647: pop
    //   648: goto -46 -> 602
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	651	0	this	b
    //   0	651	1	paramFile	File
    //   1	577	2	localFile1	File
    //   5	617	3	localRandomAccessFile1	RandomAccessFile
    //   8	490	4	localObject1	Object
    //   514	1	4	localIOException1	IOException
    //   519	1	4	localObject2	Object
    //   527	6	4	localIOException2	IOException
    //   573	11	4	localRandomAccessFile2	RandomAccessFile
    //   586	17	4	localObject3	Object
    //   609	28	4	locala	b.a
    //   18	147	5	l1	long
    //   45	402	7	i	int
    //   53	420	8	j	int
    //   557	3	8	bool	boolean
    //   59	6	9	arrayOfByte1	byte[]
    //   62	366	10	k	int
    //   71	359	11	m	int
    //   77	335	12	arrayOfByte2	byte[]
    //   81	99	13	n	int
    //   90	296	14	i1	int
    //   96	7	15	arrayOfByte3	byte[]
    //   100	220	16	i2	int
    //   109	366	17	i3	int
    //   123	266	18	i4	int
    //   139	310	19	i5	int
    //   173	298	20	arrayOfByte4	byte[]
    //   196	219	21	i6	int
    //   217	348	22	localFile2	File
    //   233	361	23	localObject4	Object
    //   310	7	24	i7	int
    //   331	28	25	l2	long
    //   368	26	27	localFileChannel1	java.nio.channels.FileChannel
    //   374	22	28	localFileChannel2	java.nio.channels.FileChannel
    //   379	19	29	l3	long
    //   391	9	31	l4	long
    //   480	5	33	l5	long
    //   545	1	37	localIOException3	IOException
    //   605	1	38	localIOException4	IOException
    //   623	1	39	localIOException5	IOException
    //   639	1	40	localIOException6	IOException
    //   643	1	41	localIOException7	IOException
    //   647	1	42	localIOException8	IOException
    // Exception table:
    //   from	to	target	type
    //   321	326	514	java/io/IOException
    //   335	340	514	java/io/IOException
    //   347	352	514	java/io/IOException
    //   358	363	514	java/io/IOException
    //   363	368	514	java/io/IOException
    //   370	374	514	java/io/IOException
    //   399	405	514	java/io/IOException
    //   405	409	514	java/io/IOException
    //   414	419	514	java/io/IOException
    //   419	423	514	java/io/IOException
    //   429	434	514	java/io/IOException
    //   434	438	514	java/io/IOException
    //   448	453	514	java/io/IOException
    //   453	457	514	java/io/IOException
    //   474	479	514	java/io/IOException
    //   484	489	514	java/io/IOException
    //   489	493	514	java/io/IOException
    //   497	502	514	java/io/IOException
    //   269	272	519	finally
    //   282	287	519	finally
    //   269	272	527	java/io/IOException
    //   282	287	527	java/io/IOException
    //   507	511	545	java/io/IOException
    //   321	326	586	finally
    //   335	340	586	finally
    //   347	352	586	finally
    //   358	363	586	finally
    //   363	368	586	finally
    //   370	374	586	finally
    //   399	405	586	finally
    //   405	409	586	finally
    //   414	419	586	finally
    //   419	423	586	finally
    //   429	434	586	finally
    //   434	438	586	finally
    //   448	453	586	finally
    //   453	457	586	finally
    //   474	479	586	finally
    //   484	489	586	finally
    //   489	493	586	finally
    //   497	502	586	finally
    //   532	537	586	finally
    //   164	169	605	java/io/IOException
    //   169	173	605	java/io/IOException
    //   179	184	605	java/io/IOException
    //   184	188	605	java/io/IOException
    //   192	196	605	java/io/IOException
    //   21	26	623	java/io/IOException
    //   26	30	623	java/io/IOException
    //   33	38	623	java/io/IOException
    //   502	507	639	java/io/IOException
    //   593	598	643	java/io/IOException
    //   598	602	647	java/io/IOException
  }
  
  private void a(int paramInt1, byte[] paramArrayOfByte, int paramInt2)
  {
    paramInt1 = b(paramInt1);
    int i = paramInt1 + paramInt2;
    int j = b;
    RandomAccessFile localRandomAccessFile;
    long l;
    if (i <= j)
    {
      localRandomAccessFile = a;
      l = paramInt1;
      localRandomAccessFile.seek(l);
      a.readFully(paramArrayOfByte, 0, paramInt2);
      return;
    }
    if (paramInt1 <= j)
    {
      j -= paramInt1;
      localRandomAccessFile = a;
      l = paramInt1;
      localRandomAccessFile.seek(l);
      a.readFully(paramArrayOfByte, 0, j);
      a.seek(16);
      localObject = a;
      i = j + 0;
      paramInt2 -= j;
      ((RandomAccessFile)localObject).readFully(paramArrayOfByte, i, paramInt2);
      return;
    }
    Object localObject = new com/truecaller/analytics/storage/a/b$a;
    ((b.a)localObject).<init>("Position is after file end. Queue is broken");
    throw ((Throwable)localObject);
  }
  
  private static void a(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    int i = (byte)(paramInt2 >> 24);
    paramArrayOfByte[paramInt1] = i;
    i = paramInt1 + 1;
    int j = (byte)(paramInt2 >> 16);
    paramArrayOfByte[i] = j;
    i = paramInt1 + 2;
    j = (byte)(paramInt2 >> 8);
    paramArrayOfByte[i] = j;
    paramInt1 += 3;
    paramInt2 = (byte)paramInt2;
    paramArrayOfByte[paramInt1] = paramInt2;
  }
  
  private int b(int paramInt)
  {
    int i = b;
    if (paramInt < i) {
      return paramInt;
    }
    return paramInt + 16 - i;
  }
  
  public final b.d a()
  {
    int i = c;
    if (i == 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localc = null;
    }
    if (i != 0) {
      return null;
    }
    b.c localc = new com/truecaller/analytics/storage/a/b$c;
    int j = c;
    localc.<init>(this, j, (byte)0);
    return localc;
  }
  
  public final void close()
  {
    a.close();
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    Object localObject = getClass().getSimpleName();
    localStringBuilder.append((String)localObject);
    localStringBuilder.append('[');
    localStringBuilder.append("mFileLength=");
    int i = b;
    localStringBuilder.append(i);
    localStringBuilder.append(", size=");
    i = c;
    localStringBuilder.append(i);
    localStringBuilder.append(", mFirst=");
    localObject = d;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", mLast=");
    localObject = e;
    localStringBuilder.append(localObject);
    localStringBuilder.append("]]");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.storage.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */