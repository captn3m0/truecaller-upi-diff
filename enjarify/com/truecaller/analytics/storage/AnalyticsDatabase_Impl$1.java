package com.truecaller.analytics.storage;

import android.arch.persistence.room.b.b.a;
import android.arch.persistence.room.f.b;
import android.arch.persistence.room.h.a;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

final class AnalyticsDatabase_Impl$1
  extends h.a
{
  AnalyticsDatabase_Impl$1(AnalyticsDatabase_Impl paramAnalyticsDatabase_Impl)
  {
    super(1);
  }
  
  public final void a(android.arch.persistence.db.b paramb)
  {
    paramb.c("DROP TABLE IF EXISTS `persisted_event`");
  }
  
  public final void b(android.arch.persistence.db.b paramb)
  {
    paramb.c("CREATE TABLE IF NOT EXISTS `persisted_event` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `record` BLOB NOT NULL)");
    paramb.c("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
    paramb.c("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, \"d7da6f9ec87db063c1cd7123cce032d4\")");
  }
  
  public final void c(android.arch.persistence.db.b paramb)
  {
    AnalyticsDatabase_Impl.a(b, paramb);
    AnalyticsDatabase_Impl.b(b, paramb);
    List localList1 = AnalyticsDatabase_Impl.d(b);
    if (localList1 != null)
    {
      int i = 0;
      localList1 = null;
      List localList2 = AnalyticsDatabase_Impl.e(b);
      int j = localList2.size();
      while (i < j)
      {
        f.b localb = (f.b)AnalyticsDatabase_Impl.f(b).get(i);
        localb.b(paramb);
        i += 1;
      }
    }
  }
  
  public final void d(android.arch.persistence.db.b paramb)
  {
    List localList1 = AnalyticsDatabase_Impl.a(b);
    if (localList1 != null)
    {
      int i = 0;
      localList1 = null;
      List localList2 = AnalyticsDatabase_Impl.b(b);
      int j = localList2.size();
      while (i < j)
      {
        f.b localb = (f.b)AnalyticsDatabase_Impl.c(b).get(i);
        localb.a(paramb);
        i += 1;
      }
    }
  }
  
  public final void e(android.arch.persistence.db.b paramb)
  {
    Object localObject1 = new java/util/HashMap;
    int i = 2;
    ((HashMap)localObject1).<init>(i);
    Object localObject2 = new android/arch/persistence/room/b/b$a;
    int j = 1;
    ((b.a)localObject2).<init>("id", "INTEGER", j, j);
    ((HashMap)localObject1).put("id", localObject2);
    localObject2 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject2).<init>("record", "BLOB", j, 0);
    ((HashMap)localObject1).put("record", localObject2);
    Object localObject3 = new java/util/HashSet;
    ((HashSet)localObject3).<init>(0);
    localObject2 = new java/util/HashSet;
    ((HashSet)localObject2).<init>(0);
    android.arch.persistence.room.b.b localb = new android/arch/persistence/room/b/b;
    String str = "persisted_event";
    localb.<init>(str, (Map)localObject1, (Set)localObject3, (Set)localObject2);
    localObject1 = "persisted_event";
    paramb = android.arch.persistence.room.b.b.a(paramb, (String)localObject1);
    boolean bool = localb.equals(paramb);
    if (bool) {
      return;
    }
    localObject1 = new java/lang/IllegalStateException;
    localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>("Migration didn't properly handle persisted_event(com.truecaller.analytics.storage.PersistedEvent).\n Expected:\n");
    ((StringBuilder)localObject3).append(localb);
    ((StringBuilder)localObject3).append("\n Found:\n");
    ((StringBuilder)localObject3).append(paramb);
    paramb = ((StringBuilder)localObject3).toString();
    ((IllegalStateException)localObject1).<init>(paramb);
    throw ((Throwable)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.storage.AnalyticsDatabase_Impl.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */