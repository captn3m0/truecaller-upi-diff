package com.truecaller.analytics.storage;

import android.arch.persistence.db.c;
import android.arch.persistence.db.c.a;
import android.arch.persistence.db.c.b;
import android.arch.persistence.db.c.b.a;
import android.arch.persistence.db.c.c;
import android.arch.persistence.room.a;
import android.arch.persistence.room.h;
import android.arch.persistence.room.h.a;

public class AnalyticsDatabase_Impl
  extends AnalyticsDatabase
{
  private volatile d g;
  
  public final android.arch.persistence.room.d a()
  {
    android.arch.persistence.room.d locald = new android/arch/persistence/room/d;
    String[] arrayOfString = { "persisted_event" };
    locald.<init>(this, arrayOfString);
    return locald;
  }
  
  public final c b(a parama)
  {
    Object localObject1 = new android/arch/persistence/room/h;
    Object localObject2 = new com/truecaller/analytics/storage/AnalyticsDatabase_Impl$1;
    ((AnalyticsDatabase_Impl.1)localObject2).<init>(this);
    ((h)localObject1).<init>(parama, (h.a)localObject2, "d7da6f9ec87db063c1cd7123cce032d4", "ea3cbe109a25dc3bf0e23f7a16dfea4f");
    localObject2 = c.b.a(b);
    String str = c;
    b = str;
    c = ((c.a)localObject1);
    localObject1 = ((c.b.a)localObject2).a();
    return a.a((c.b)localObject1);
  }
  
  public final d h()
  {
    Object localObject1 = g;
    if (localObject1 != null) {
      return g;
    }
    try
    {
      localObject1 = g;
      if (localObject1 == null)
      {
        localObject1 = new com/truecaller/analytics/storage/e;
        ((e)localObject1).<init>(this);
        g = ((d)localObject1);
      }
      localObject1 = g;
      return (d)localObject1;
    }
    finally {}
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.storage.AnalyticsDatabase_Impl
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */