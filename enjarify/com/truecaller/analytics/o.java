package com.truecaller.analytics;

import dagger.a.d;
import javax.inject.Provider;

public final class o
  implements d
{
  private final f a;
  private final Provider b;
  
  private o(f paramf, Provider paramProvider)
  {
    a = paramf;
    b = paramProvider;
  }
  
  public static o a(f paramf, Provider paramProvider)
  {
    o localo = new com/truecaller/analytics/o;
    localo.<init>(paramf, paramProvider);
    return localo;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */