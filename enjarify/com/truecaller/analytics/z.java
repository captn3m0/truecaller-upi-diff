package com.truecaller.analytics;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import c.g.b.k;
import c.u;

public abstract class z
  extends BroadcastReceiver
{
  protected abstract void a(Context paramContext, ComponentName paramComponentName, Bundle paramBundle);
  
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    k.b(paramContext, "context");
    Object localObject = "intent";
    k.b(paramIntent, (String)localObject);
    paramIntent = paramIntent.getExtras();
    if (paramIntent == null) {
      return;
    }
    localObject = "android.intent.extra.CHOSEN_COMPONENT";
    boolean bool = paramIntent.containsKey((String)localObject);
    if (!bool) {
      return;
    }
    localObject = paramIntent.getParcelable("android.intent.extra.CHOSEN_COMPONENT");
    if (localObject != null)
    {
      localObject = (ComponentName)localObject;
      a(paramContext, (ComponentName)localObject, paramIntent);
      return;
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type android.content.ComponentName");
    throw paramContext;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.z
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */