package com.truecaller.analytics;

import android.database.sqlite.SQLiteException;
import android.location.Location;
import android.os.SystemClock;
import android.telephony.TelephonyManager;
import c.f;
import com.truecaller.analytics.a.e;
import com.truecaller.analytics.storage.c;
import com.truecaller.log.AssertionUtil;
import com.truecaller.tracking.events.a.a;
import com.truecaller.tracking.events.ak;
import com.truecaller.tracking.events.ak.a;
import com.truecaller.tracking.events.am.a;
import com.truecaller.utils.i;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

final class ag
  implements ae
{
  private final s a;
  private final com.truecaller.analytics.storage.a b;
  private final com.truecaller.analytics.storage.d c;
  private final ai d;
  private final i e;
  private final am f;
  private final e g;
  private final TelephonyManager h;
  private final b i;
  private long j = 0L;
  
  ag(s params, com.truecaller.analytics.storage.a parama, com.truecaller.analytics.storage.d paramd, ai paramai, i parami, am paramam, e parame, TelephonyManager paramTelephonyManager, b paramb)
  {
    a = params;
    b = parama;
    c = paramd;
    d = paramai;
    e = parami;
    f = paramam;
    h = paramTelephonyManager;
    g = parame;
    i = paramb;
  }
  
  private void a(u paramu, Exception paramException)
  {
    boolean bool = paramException instanceof IOException;
    if (bool)
    {
      Object localObject = e;
      bool = ((i)localObject).a();
      if (!bool)
      {
        localObject = "NoInternet";
        paramu.a((String)localObject);
        break label50;
      }
    }
    paramu.a((Exception)paramException);
    label50:
    AssertionUtil.reportThrowableButNeverCrash((Throwable)paramException);
  }
  
  private void a(byte[] paramArrayOfByte)
  {
    try
    {
      com.truecaller.analytics.storage.d locald = c;
      c localc = new com/truecaller/analytics/storage/c;
      long l = 0L;
      localc.<init>(l, paramArrayOfByte);
      locald.a(localc);
      return;
    }
    catch (SQLiteException localSQLiteException)
    {
      AssertionUtil.reportThrowableButNeverCrash(localSQLiteException;
    }
  }
  
  private byte[] b(org.apache.a.d.d paramd)
  {
    Object localObject1 = a;
    long l1 = ((s)localObject1).d();
    int k = 1;
    long l2 = -1;
    boolean bool1 = l1 < l2;
    if (!bool1)
    {
      localObject1 = new String[k];
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("Event ");
      paramd = paramd.a().c();
      ((StringBuilder)localObject2).append(paramd);
      ((StringBuilder)localObject2).append(" skipped due empty user id");
      paramd = ((StringBuilder)localObject2).toString();
      localObject1[0] = paramd;
      return null;
    }
    try
    {
      Object localObject3 = com.truecaller.tracking.events.a.b();
      Object localObject4 = a;
      localObject4 = ((s)localObject4).a();
      localObject3 = ((a.a)localObject3).a((CharSequence)localObject4);
      localObject4 = a;
      localObject4 = ((s)localObject4).c();
      localObject3 = ((a.a)localObject3).c((CharSequence)localObject4);
      localObject4 = a;
      localObject4 = ((s)localObject4).b();
      localObject3 = ((a.a)localObject3).b((CharSequence)localObject4);
      localObject3 = ((a.a)localObject3).a();
      localObject4 = ak.b();
      long l3 = j;
      long l4 = 0L;
      boolean bool2 = l3 < l4;
      if (!bool2)
      {
        locala1 = b;
        str = "analyticsLastEventId";
        l4 = System.currentTimeMillis();
        l3 = locala1.a(str, l4);
        j = l3;
      }
      com.truecaller.analytics.storage.a locala1 = b;
      String str = "analyticsLastEventId";
      l4 = j;
      long l5 = 1L;
      l4 += l5;
      j = l4;
      locala1.b(str, l4);
      l3 = j;
      localObject4 = ((ak.a)localObject4).a(l3);
      l3 = System.currentTimeMillis();
      localObject4 = ((ak.a)localObject4).b(l3);
      localObject1 = String.valueOf(l1);
      localObject1 = ((ak.a)localObject4).a((CharSequence)localObject1);
      localObject2 = g;
      localObject2 = b;
      localObject2 = ((f)localObject2).b();
      localObject2 = (String)localObject2;
      localObject1 = ((ak.a)localObject1).b((CharSequence)localObject2);
      localObject1 = ((ak.a)localObject1).a((com.truecaller.tracking.events.a)localObject3);
      localObject2 = e;
      localObject2 = ((i)localObject2).b();
      localObject1 = ((ak.a)localObject1).c((CharSequence)localObject2);
      localObject2 = h;
      localObject2 = ((TelephonyManager)localObject2).getNetworkOperatorName();
      localObject1 = ((ak.a)localObject1).d((CharSequence)localObject2);
      localObject2 = f;
      localObject2 = ((am)localObject2).a();
      if (localObject2 == null)
      {
        localObject2 = null;
      }
      else
      {
        localObject3 = com.truecaller.tracking.events.am.b();
        double d1 = ((Location)localObject2).getLatitude();
        float f1 = (float)d1;
        localObject3 = ((am.a)localObject3).a(f1);
        d1 = ((Location)localObject2).getLongitude();
        f1 = (float)d1;
        localObject3 = ((am.a)localObject3).b(f1);
        localObject4 = TimeUnit.NANOSECONDS;
        l3 = SystemClock.elapsedRealtimeNanos();
        l4 = ((Location)localObject2).getElapsedRealtimeNanos();
        l3 -= l4;
        long l6 = ((TimeUnit)localObject4).toMillis(l3);
        localObject2 = ((am.a)localObject3).a(l6);
        localObject2 = ((am.a)localObject2).a();
      }
      localObject1 = ((ak.a)localObject1).a((com.truecaller.tracking.events.am)localObject2);
      localObject1 = ((ak.a)localObject1).a();
      return ad.a((ak)localObject1, paramd);
    }
    catch (IOException localIOException) {}catch (org.apache.a.a locala) {}
    Object localObject2 = new String[k];
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Event ");
    paramd = paramd.a().c();
    localStringBuilder.append(paramd);
    localStringBuilder.append(" skipped due encoding exception");
    paramd = localStringBuilder.toString();
    localObject2[0] = paramd;
    AssertionUtil.reportThrowableButNeverCrash(locala);
    return null;
  }
  
  /* Error */
  public final com.truecaller.androidactors.w a(okhttp3.y paramy)
  {
    // Byte code:
    //   0: new 61	com/truecaller/analytics/u
    //   3: astore_2
    //   4: ldc_w 276
    //   7: astore_3
    //   8: aload_2
    //   9: aload_3
    //   10: invokespecial 277	com/truecaller/analytics/u:<init>	(Ljava/lang/String;)V
    //   13: aload_0
    //   14: getfield 40	com/truecaller/analytics/ag:d	Lcom/truecaller/analytics/ai;
    //   17: astore_3
    //   18: aload_0
    //   19: getfield 34	com/truecaller/analytics/ag:a	Lcom/truecaller/analytics/s;
    //   22: astore 4
    //   24: aload_3
    //   25: aload 4
    //   27: aload_1
    //   28: aload_2
    //   29: invokeinterface 282 4 0
    //   34: istore 5
    //   36: iload 5
    //   38: invokestatic 287	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   41: astore_1
    //   42: aload_1
    //   43: invokestatic 292	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   46: astore_1
    //   47: aload_0
    //   48: getfield 50	com/truecaller/analytics/ag:i	Lcom/truecaller/analytics/b;
    //   51: astore_3
    //   52: aload_2
    //   53: invokevirtual 295	com/truecaller/analytics/u:a	()Lcom/truecaller/analytics/e;
    //   56: astore_2
    //   57: aload_3
    //   58: aload_2
    //   59: invokeinterface 300 2 0
    //   64: aload_1
    //   65: areturn
    //   66: astore_1
    //   67: goto +64 -> 131
    //   70: astore_1
    //   71: aload_2
    //   72: aload_1
    //   73: invokevirtual 69	com/truecaller/analytics/u:a	(Ljava/lang/Exception;)Lcom/truecaller/analytics/e$a;
    //   76: pop
    //   77: aload_1
    //   78: invokestatic 77	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   81: aload_1
    //   82: athrow
    //   83: astore_1
    //   84: aload_0
    //   85: aload_2
    //   86: aload_1
    //   87: invokespecial 303	com/truecaller/analytics/ag:a	(Lcom/truecaller/analytics/u;Ljava/lang/Exception;)V
    //   90: aload_0
    //   91: getfield 50	com/truecaller/analytics/ag:i	Lcom/truecaller/analytics/b;
    //   94: astore_1
    //   95: aload_2
    //   96: invokevirtual 295	com/truecaller/analytics/u:a	()Lcom/truecaller/analytics/e;
    //   99: astore_2
    //   100: aload_1
    //   101: aload_2
    //   102: invokeinterface 300 2 0
    //   107: goto +17 -> 124
    //   110: astore_1
    //   111: aload_1
    //   112: invokestatic 77	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   115: aload_2
    //   116: aload_1
    //   117: invokevirtual 69	com/truecaller/analytics/u:a	(Ljava/lang/Exception;)Lcom/truecaller/analytics/e$a;
    //   120: pop
    //   121: goto -31 -> 90
    //   124: getstatic 307	java/lang/Boolean:FALSE	Ljava/lang/Boolean;
    //   127: invokestatic 292	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   130: areturn
    //   131: aload_0
    //   132: getfield 50	com/truecaller/analytics/ag:i	Lcom/truecaller/analytics/b;
    //   135: astore_3
    //   136: aload_2
    //   137: invokevirtual 295	com/truecaller/analytics/u:a	()Lcom/truecaller/analytics/e;
    //   140: astore_2
    //   141: aload_3
    //   142: aload_2
    //   143: invokeinterface 300 2 0
    //   148: aload_1
    //   149: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	150	0	this	ag
    //   0	150	1	paramy	okhttp3.y
    //   3	140	2	localObject1	Object
    //   7	135	3	localObject2	Object
    //   22	4	4	locals	s
    //   34	3	5	bool	boolean
    // Exception table:
    //   from	to	target	type
    //   13	17	66	finally
    //   18	22	66	finally
    //   28	34	66	finally
    //   36	41	66	finally
    //   42	46	66	finally
    //   72	77	66	finally
    //   77	81	66	finally
    //   81	83	66	finally
    //   86	90	66	finally
    //   111	115	66	finally
    //   116	121	66	finally
    //   13	17	70	java/lang/Exception
    //   18	22	70	java/lang/Exception
    //   28	34	70	java/lang/Exception
    //   36	41	70	java/lang/Exception
    //   42	46	70	java/lang/Exception
    //   13	17	83	java/io/IOException
    //   18	22	83	java/io/IOException
    //   28	34	83	java/io/IOException
    //   36	41	83	java/io/IOException
    //   42	46	83	java/io/IOException
    //   13	17	110	android/database/sqlite/SQLiteException
    //   18	22	110	android/database/sqlite/SQLiteException
    //   28	34	110	android/database/sqlite/SQLiteException
    //   36	41	110	android/database/sqlite/SQLiteException
    //   42	46	110	android/database/sqlite/SQLiteException
  }
  
  /* Error */
  public final com.truecaller.androidactors.w a(org.apache.a.d.d paramd, okhttp3.y paramy)
  {
    // Byte code:
    //   0: new 61	com/truecaller/analytics/u
    //   3: astore_3
    //   4: ldc_w 309
    //   7: astore 4
    //   9: aload_3
    //   10: aload 4
    //   12: invokespecial 277	com/truecaller/analytics/u:<init>	(Ljava/lang/String;)V
    //   15: aload_0
    //   16: aload_1
    //   17: invokespecial 312	com/truecaller/analytics/ag:b	(Lorg/apache/a/d/d;)[B
    //   20: astore 4
    //   22: aload 4
    //   24: ifnonnull +41 -> 65
    //   27: ldc_w 314
    //   30: astore_1
    //   31: aload_3
    //   32: aload_1
    //   33: invokevirtual 64	com/truecaller/analytics/u:a	(Ljava/lang/String;)Lcom/truecaller/analytics/e$a;
    //   36: pop
    //   37: getstatic 320	com/truecaller/analytics/EventsUploadResult:FAILURE	Lcom/truecaller/analytics/EventsUploadResult;
    //   40: astore_1
    //   41: aload_1
    //   42: invokestatic 292	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   45: astore_1
    //   46: aload_0
    //   47: getfield 50	com/truecaller/analytics/ag:i	Lcom/truecaller/analytics/b;
    //   50: astore_2
    //   51: aload_3
    //   52: invokevirtual 295	com/truecaller/analytics/u:a	()Lcom/truecaller/analytics/e;
    //   55: astore_3
    //   56: aload_2
    //   57: aload_3
    //   58: invokeinterface 300 2 0
    //   63: aload_1
    //   64: areturn
    //   65: iconst_0
    //   66: istore 5
    //   68: aload 4
    //   70: invokestatic 323	com/truecaller/analytics/ad:a	([B)Lcom/truecaller/tracking/events/EventRecordVersionedV2;
    //   73: astore 6
    //   75: new 325	java/util/ArrayList
    //   78: astore 7
    //   80: aload 7
    //   82: invokespecial 326	java/util/ArrayList:<init>	()V
    //   85: aload 7
    //   87: aload 6
    //   89: invokevirtual 330	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   92: pop
    //   93: aload_0
    //   94: getfield 40	com/truecaller/analytics/ag:d	Lcom/truecaller/analytics/ai;
    //   97: astore 6
    //   99: aload_0
    //   100: getfield 34	com/truecaller/analytics/ag:a	Lcom/truecaller/analytics/s;
    //   103: astore 8
    //   105: aload 6
    //   107: aload 8
    //   109: aload_2
    //   110: aload 7
    //   112: aload_3
    //   113: invokeinterface 333 5 0
    //   118: istore 5
    //   120: goto +81 -> 201
    //   123: astore_2
    //   124: goto +4 -> 128
    //   127: astore_2
    //   128: iconst_1
    //   129: istore 9
    //   131: iload 9
    //   133: anewarray 97	java/lang/String
    //   136: astore 6
    //   138: new 99	java/lang/StringBuilder
    //   141: astore 7
    //   143: ldc 101
    //   145: astore 8
    //   147: aload 7
    //   149: aload 8
    //   151: invokespecial 104	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   154: aload_1
    //   155: invokeinterface 109 1 0
    //   160: astore_1
    //   161: aload_1
    //   162: invokevirtual 114	org/apache/a/d:c	()Ljava/lang/String;
    //   165: astore_1
    //   166: aload 7
    //   168: aload_1
    //   169: invokevirtual 118	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   172: pop
    //   173: ldc_w 335
    //   176: astore_1
    //   177: aload 7
    //   179: aload_1
    //   180: invokevirtual 118	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   183: pop
    //   184: aload 7
    //   186: invokevirtual 123	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   189: astore_1
    //   190: aload 6
    //   192: iconst_0
    //   193: aload_1
    //   194: aastore
    //   195: aload_0
    //   196: aload_3
    //   197: aload_2
    //   198: invokespecial 303	com/truecaller/analytics/ag:a	(Lcom/truecaller/analytics/u;Ljava/lang/Exception;)V
    //   201: iload 5
    //   203: ifne +37 -> 240
    //   206: aload_0
    //   207: aload 4
    //   209: invokespecial 338	com/truecaller/analytics/ag:a	([B)V
    //   212: getstatic 341	com/truecaller/analytics/EventsUploadResult:QUEUED	Lcom/truecaller/analytics/EventsUploadResult;
    //   215: astore_1
    //   216: aload_1
    //   217: invokestatic 292	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   220: astore_1
    //   221: aload_0
    //   222: getfield 50	com/truecaller/analytics/ag:i	Lcom/truecaller/analytics/b;
    //   225: astore_2
    //   226: aload_3
    //   227: invokevirtual 295	com/truecaller/analytics/u:a	()Lcom/truecaller/analytics/e;
    //   230: astore_3
    //   231: aload_2
    //   232: aload_3
    //   233: invokeinterface 300 2 0
    //   238: aload_1
    //   239: areturn
    //   240: getstatic 344	com/truecaller/analytics/EventsUploadResult:SUCCESS	Lcom/truecaller/analytics/EventsUploadResult;
    //   243: astore_1
    //   244: aload_1
    //   245: invokestatic 292	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   248: astore_1
    //   249: aload_0
    //   250: getfield 50	com/truecaller/analytics/ag:i	Lcom/truecaller/analytics/b;
    //   253: astore_2
    //   254: aload_3
    //   255: invokevirtual 295	com/truecaller/analytics/u:a	()Lcom/truecaller/analytics/e;
    //   258: astore_3
    //   259: aload_2
    //   260: aload_3
    //   261: invokeinterface 300 2 0
    //   266: aload_1
    //   267: areturn
    //   268: astore_1
    //   269: goto +16 -> 285
    //   272: astore_1
    //   273: aload_3
    //   274: aload_1
    //   275: invokevirtual 69	com/truecaller/analytics/u:a	(Ljava/lang/Exception;)Lcom/truecaller/analytics/e$a;
    //   278: pop
    //   279: aload_1
    //   280: invokestatic 77	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   283: aload_1
    //   284: athrow
    //   285: aload_0
    //   286: getfield 50	com/truecaller/analytics/ag:i	Lcom/truecaller/analytics/b;
    //   289: astore_2
    //   290: aload_3
    //   291: invokevirtual 295	com/truecaller/analytics/u:a	()Lcom/truecaller/analytics/e;
    //   294: astore_3
    //   295: aload_2
    //   296: aload_3
    //   297: invokeinterface 300 2 0
    //   302: aload_1
    //   303: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	304	0	this	ag
    //   0	304	1	paramd	org.apache.a.d.d
    //   0	304	2	paramy	okhttp3.y
    //   3	294	3	localObject1	Object
    //   7	201	4	localObject2	Object
    //   66	136	5	bool	boolean
    //   73	118	6	localObject3	Object
    //   78	107	7	localObject4	Object
    //   103	47	8	localObject5	Object
    //   129	3	9	k	int
    // Exception table:
    //   from	to	target	type
    //   68	73	123	java/io/IOException
    //   75	78	123	java/io/IOException
    //   80	85	123	java/io/IOException
    //   87	93	123	java/io/IOException
    //   93	97	123	java/io/IOException
    //   99	103	123	java/io/IOException
    //   112	118	123	java/io/IOException
    //   68	73	127	org/apache/a/a
    //   75	78	127	org/apache/a/a
    //   80	85	127	org/apache/a/a
    //   87	93	127	org/apache/a/a
    //   93	97	127	org/apache/a/a
    //   99	103	127	org/apache/a/a
    //   112	118	127	org/apache/a/a
    //   16	20	268	finally
    //   32	37	268	finally
    //   37	40	268	finally
    //   41	45	268	finally
    //   68	73	268	finally
    //   75	78	268	finally
    //   80	85	268	finally
    //   87	93	268	finally
    //   93	97	268	finally
    //   99	103	268	finally
    //   112	118	268	finally
    //   131	136	268	finally
    //   138	141	268	finally
    //   149	154	268	finally
    //   154	160	268	finally
    //   161	165	268	finally
    //   168	173	268	finally
    //   179	184	268	finally
    //   184	189	268	finally
    //   193	195	268	finally
    //   197	201	268	finally
    //   207	212	268	finally
    //   212	215	268	finally
    //   216	220	268	finally
    //   240	243	268	finally
    //   244	248	268	finally
    //   274	279	268	finally
    //   279	283	268	finally
    //   283	285	268	finally
    //   16	20	272	java/lang/Exception
    //   32	37	272	java/lang/Exception
    //   37	40	272	java/lang/Exception
    //   41	45	272	java/lang/Exception
    //   68	73	272	java/lang/Exception
    //   75	78	272	java/lang/Exception
    //   80	85	272	java/lang/Exception
    //   87	93	272	java/lang/Exception
    //   93	97	272	java/lang/Exception
    //   99	103	272	java/lang/Exception
    //   112	118	272	java/lang/Exception
    //   131	136	272	java/lang/Exception
    //   138	141	272	java/lang/Exception
    //   149	154	272	java/lang/Exception
    //   154	160	272	java/lang/Exception
    //   161	165	272	java/lang/Exception
    //   168	173	272	java/lang/Exception
    //   179	184	272	java/lang/Exception
    //   184	189	272	java/lang/Exception
    //   193	195	272	java/lang/Exception
    //   197	201	272	java/lang/Exception
    //   207	212	272	java/lang/Exception
    //   212	215	272	java/lang/Exception
    //   216	220	272	java/lang/Exception
    //   240	243	272	java/lang/Exception
    //   244	248	272	java/lang/Exception
  }
  
  public final void a(org.apache.a.d.d paramd)
  {
    byte[] arrayOfByte = b(paramd);
    if (arrayOfByte != null)
    {
      int k = 1;
      String[] arrayOfString = new String[k];
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      String str = paramd.a().c();
      localStringBuilder.append(str);
      str = " ";
      localStringBuilder.append(str);
      paramd = paramd.toString();
      localStringBuilder.append(paramd);
      paramd = localStringBuilder.toString();
      arrayOfString[0] = paramd;
      a(arrayOfByte);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.ag
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */