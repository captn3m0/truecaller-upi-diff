package com.truecaller.analytics;

import com.truecaller.androidactors.f;

public final class ay
  implements ax
{
  private final f a;
  private final b b;
  
  public ay(f paramf, b paramb)
  {
    a = paramf;
    b = paramb;
  }
  
  /* Error */
  public final boolean a()
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_1
    //   2: aload_0
    //   3: getfield 25	com/truecaller/analytics/ay:a	Lcom/truecaller/androidactors/f;
    //   6: astore_2
    //   7: aload_2
    //   8: invokeinterface 32 1 0
    //   13: astore_2
    //   14: aload_2
    //   15: checkcast 34	com/truecaller/messaging/data/o
    //   18: astore_2
    //   19: aload_2
    //   20: aconst_null
    //   21: invokeinterface 37 2 0
    //   26: astore_2
    //   27: aload_2
    //   28: invokevirtual 42	com/truecaller/androidactors/w:d	()Ljava/lang/Object;
    //   31: astore_2
    //   32: aload_2
    //   33: checkcast 44	com/truecaller/messaging/data/a/a
    //   36: astore_2
    //   37: goto +6 -> 43
    //   40: pop
    //   41: aconst_null
    //   42: astore_2
    //   43: aconst_null
    //   44: astore_3
    //   45: aload_2
    //   46: ifnonnull +5 -> 51
    //   49: iconst_0
    //   50: ireturn
    //   51: aload_2
    //   52: astore 4
    //   54: aload_2
    //   55: checkcast 46	java/io/Closeable
    //   58: astore 4
    //   60: iconst_0
    //   61: istore 5
    //   63: iconst_0
    //   64: istore 6
    //   66: aload_2
    //   67: invokeinterface 50 1 0
    //   72: istore 7
    //   74: iload 7
    //   76: ifeq +137 -> 213
    //   79: aload_2
    //   80: invokeinterface 53 1 0
    //   85: astore 8
    //   87: aload 8
    //   89: getfield 59	com/truecaller/messaging/data/types/Conversation:l	[Lcom/truecaller/messaging/data/types/Participant;
    //   92: astore 8
    //   94: ldc 61
    //   96: astore 9
    //   98: aload 8
    //   100: aload 9
    //   102: invokestatic 63	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   105: aload 8
    //   107: arraylength
    //   108: istore 10
    //   110: iload 5
    //   112: iload 10
    //   114: iadd
    //   115: istore 5
    //   117: new 65	java/util/ArrayList
    //   120: astore 9
    //   122: aload 9
    //   124: invokespecial 66	java/util/ArrayList:<init>	()V
    //   127: aload 9
    //   129: checkcast 68	java/util/Collection
    //   132: astore 9
    //   134: aload 8
    //   136: arraylength
    //   137: istore 11
    //   139: iconst_0
    //   140: istore 12
    //   142: iload 12
    //   144: iload 11
    //   146: if_icmpge +41 -> 187
    //   149: aload 8
    //   151: iload 12
    //   153: aaload
    //   154: astore 13
    //   156: aload 13
    //   158: getfield 74	com/truecaller/messaging/data/types/Participant:k	Z
    //   161: istore 14
    //   163: iload 14
    //   165: ifeq +13 -> 178
    //   168: aload 9
    //   170: aload 13
    //   172: invokeinterface 78 2 0
    //   177: pop
    //   178: iload 12
    //   180: iconst_1
    //   181: iadd
    //   182: istore 12
    //   184: goto -42 -> 142
    //   187: aload 9
    //   189: checkcast 80	java/util/List
    //   192: astore 9
    //   194: aload 9
    //   196: invokeinterface 84 1 0
    //   201: istore 7
    //   203: iload 6
    //   205: iload 7
    //   207: iadd
    //   208: istore 6
    //   210: goto -144 -> 66
    //   213: getstatic 89	c/x:a	Lc/x;
    //   216: astore_2
    //   217: aload 4
    //   219: aconst_null
    //   220: invokestatic 94	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   223: iload 5
    //   225: ifle +98 -> 323
    //   228: iload 6
    //   230: i2d
    //   231: dstore 15
    //   233: dload 15
    //   235: invokestatic 104	java/lang/Double:isNaN	(D)Z
    //   238: pop
    //   239: dload 15
    //   241: ldc2_w 97
    //   244: dmul
    //   245: dstore 15
    //   247: iload 5
    //   249: i2d
    //   250: dstore 17
    //   252: dload 17
    //   254: invokestatic 104	java/lang/Double:isNaN	(D)Z
    //   257: pop
    //   258: dload 15
    //   260: dload 17
    //   262: ddiv
    //   263: dstore 15
    //   265: new 106	com/truecaller/analytics/e$a
    //   268: astore_3
    //   269: aload_3
    //   270: ldc 108
    //   272: invokespecial 111	com/truecaller/analytics/e$a:<init>	(Ljava/lang/String;)V
    //   275: aload_3
    //   276: ldc 113
    //   278: ldc 115
    //   280: invokevirtual 118	com/truecaller/analytics/e$a:a	(Ljava/lang/String;Ljava/lang/String;)Lcom/truecaller/analytics/e$a;
    //   283: astore_3
    //   284: dload 15
    //   286: ldc2_w 121
    //   289: dmul
    //   290: invokestatic 126	java/lang/Double:valueOf	(D)Ljava/lang/Double;
    //   293: astore_1
    //   294: aload_3
    //   295: aload_1
    //   296: invokevirtual 129	com/truecaller/analytics/e$a:a	(Ljava/lang/Double;)Lcom/truecaller/analytics/e$a;
    //   299: invokevirtual 132	com/truecaller/analytics/e$a:a	()Lcom/truecaller/analytics/e;
    //   302: astore_1
    //   303: aload_0
    //   304: getfield 27	com/truecaller/analytics/ay:b	Lcom/truecaller/analytics/b;
    //   307: astore_2
    //   308: aload_1
    //   309: ldc -122
    //   311: invokestatic 63	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   314: aload_2
    //   315: aload_1
    //   316: invokeinterface 139 2 0
    //   321: iconst_1
    //   322: ireturn
    //   323: iconst_0
    //   324: ireturn
    //   325: astore_2
    //   326: goto +6 -> 332
    //   329: astore_1
    //   330: aload_1
    //   331: athrow
    //   332: aload 4
    //   334: aload_1
    //   335: invokestatic 94	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   338: aload_2
    //   339: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	340	0	this	ay
    //   1	315	1	localObject1	Object
    //   329	6	1	localThrowable	Throwable
    //   6	309	2	localObject2	Object
    //   325	14	2	localObject3	Object
    //   44	251	3	locala	e.a
    //   52	281	4	localObject4	Object
    //   61	187	5	i	int
    //   64	165	6	j	int
    //   72	3	7	bool1	boolean
    //   201	7	7	k	int
    //   85	65	8	localObject5	Object
    //   96	99	9	localObject6	Object
    //   108	7	10	m	int
    //   137	10	11	n	int
    //   140	43	12	i1	int
    //   154	17	13	localObject7	Object
    //   161	3	14	bool2	boolean
    //   231	54	15	d1	double
    //   250	11	17	d2	double
    //   40	1	20	localInterruptedException	InterruptedException
    // Exception table:
    //   from	to	target	type
    //   2	6	40	java/lang/InterruptedException
    //   7	13	40	java/lang/InterruptedException
    //   14	18	40	java/lang/InterruptedException
    //   20	26	40	java/lang/InterruptedException
    //   27	31	40	java/lang/InterruptedException
    //   32	36	40	java/lang/InterruptedException
    //   330	332	325	finally
    //   66	72	329	finally
    //   79	85	329	finally
    //   87	92	329	finally
    //   100	105	329	finally
    //   105	108	329	finally
    //   117	120	329	finally
    //   122	127	329	finally
    //   127	132	329	finally
    //   134	137	329	finally
    //   151	154	329	finally
    //   156	161	329	finally
    //   170	178	329	finally
    //   187	192	329	finally
    //   194	201	329	finally
    //   213	216	329	finally
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.ay
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */