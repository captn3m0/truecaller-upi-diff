package com.truecaller.analytics;

import android.app.Application;
import android.arch.persistence.room.f.b;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.telephony.TelephonyManager;
import c.u;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.truecaller.analytics.a.g;
import com.truecaller.analytics.storage.AnalyticsDatabase;
import com.truecaller.analytics.storage.d;
import com.truecaller.androidactors.i;

public final class f
{
  public static final f.a a;
  
  static
  {
    f.a locala = new com/truecaller/analytics/f$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  public static LocationManager a(Context paramContext)
  {
    c.g.b.k.b(paramContext, "context");
    String str = "location";
    paramContext = paramContext.getSystemService(str);
    if (paramContext != null) {
      return (LocationManager)paramContext;
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type android.location.LocationManager");
    throw paramContext;
  }
  
  public static com.truecaller.analytics.a.e a(com.truecaller.analytics.storage.a parama, Context paramContext)
  {
    c.g.b.k.b(parama, "settings");
    c.g.b.k.b(paramContext, "context");
    com.truecaller.analytics.a.e locale = new com/truecaller/analytics/a/e;
    paramContext = paramContext.getContentResolver();
    c.g.b.k.a(paramContext, "context.contentResolver");
    locale.<init>(parama, paramContext);
    return locale;
  }
  
  public static b a(Application paramApplication)
  {
    c.g.b.k.b(paramApplication, "application");
    al localal = new com/truecaller/analytics/al;
    localal.<init>(paramApplication);
    return (b)localal;
  }
  
  public static d a(AnalyticsDatabase paramAnalyticsDatabase)
  {
    c.g.b.k.b(paramAnalyticsDatabase, "database");
    return paramAnalyticsDatabase.h();
  }
  
  public static com.truecaller.androidactors.f a(ae paramae, i parami)
  {
    c.g.b.k.b(paramae, "tracker");
    c.g.b.k.b(parami, "thread");
    paramae = parami.a(ae.class, paramae);
    c.g.b.k.a(paramae, "thread.bind(EventsTracker::class.java, tracker)");
    return paramae;
  }
  
  public static i a(Context paramContext, com.truecaller.androidactors.k paramk, int paramInt)
  {
    c.g.b.k.b(paramContext, "context");
    c.g.b.k.b(paramk, "actors");
    paramContext = paramk.a(paramContext, EventsTrackerService.class, paramInt);
    c.g.b.k.a(paramContext, "actors.createThread(cont…::class.java, actorJobId)");
    return paramContext;
  }
  
  public static com.truecaller.androidactors.k a()
  {
    Object localObject = new com/truecaller/analytics/c;
    ((c)localObject).<init>();
    localObject = ((c)localObject).a();
    c.g.b.k.a(localObject, "AnalyticsActorsBuilder().build()");
    return (com.truecaller.androidactors.k)localObject;
  }
  
  public static AnalyticsDatabase b(Context paramContext)
  {
    c.g.b.k.b(paramContext, "context");
    android.arch.persistence.room.f.a locala = android.arch.persistence.room.e.a(paramContext, AnalyticsDatabase.class, "analytics.db");
    Object localObject = new com/truecaller/analytics/storage/a/a;
    ((com.truecaller.analytics.storage.a.a)localObject).<init>(paramContext);
    localObject = (f.b)localObject;
    paramContext = locala.a((f.b)localObject).b();
    c.g.b.k.a(paramContext, "Room.databaseBuilder(con…xt))\n            .build()");
    return (AnalyticsDatabase)paramContext;
  }
  
  public static com.truecaller.analytics.a.f c(Context paramContext)
  {
    c.g.b.k.b(paramContext, "context");
    g localg = new com/truecaller/analytics/a/g;
    paramContext = FirebaseAnalytics.getInstance(paramContext);
    c.g.b.k.a(paramContext, "FirebaseAnalytics.getInstance(context)");
    localg.<init>(paramContext);
    return (com.truecaller.analytics.a.f)localg;
  }
  
  public static com.truecaller.analytics.storage.a d(Context paramContext)
  {
    c.g.b.k.b(paramContext, "context");
    com.truecaller.analytics.storage.b localb = new com/truecaller/analytics/storage/b;
    SharedPreferences localSharedPreferences = paramContext.getSharedPreferences("analytics", 0);
    c.g.b.k.a(localSharedPreferences, "context.getSharedPrefere…ME, Context.MODE_PRIVATE)");
    localb.<init>(localSharedPreferences);
    localb.a(paramContext);
    return (com.truecaller.analytics.storage.a)localb;
  }
  
  public static TelephonyManager e(Context paramContext)
  {
    c.g.b.k.b(paramContext, "context");
    String str = "phone";
    paramContext = paramContext.getSystemService(str);
    if (paramContext != null) {
      return (TelephonyManager)paramContext;
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type android.telephony.TelephonyManager");
    throw paramContext;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */