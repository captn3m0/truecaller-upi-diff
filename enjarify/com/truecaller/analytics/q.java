package com.truecaller.analytics;

import dagger.a.d;
import javax.inject.Provider;

public final class q
  implements d
{
  private final f a;
  private final Provider b;
  
  private q(f paramf, Provider paramProvider)
  {
    a = paramf;
    b = paramProvider;
  }
  
  public static q a(f paramf, Provider paramProvider)
  {
    q localq = new com/truecaller/analytics/q;
    localq.<init>(paramf, paramProvider);
    return localq;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */