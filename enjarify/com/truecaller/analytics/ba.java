package com.truecaller.analytics;

import c.a.ag;
import c.n;
import c.t;
import java.util.Map;

public final class ba
  implements e
{
  private final Map a;
  
  public ba(String paramString1, String paramString2)
  {
    n[] arrayOfn = new n[2];
    paramString2 = t.a("Context", paramString2);
    arrayOfn[0] = paramString2;
    paramString1 = t.a("Link", paramString1);
    arrayOfn[1] = paramString1;
    paramString1 = ag.b(arrayOfn);
    a = paramString1;
  }
  
  public final String a()
  {
    return "UpdateInitiated";
  }
  
  public final Map b()
  {
    return a;
  }
  
  public final Double c()
  {
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.ba
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */