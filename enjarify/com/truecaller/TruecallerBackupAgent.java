package com.truecaller;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.backup.BackupAgent;
import android.app.backup.BackupDataInput;
import android.app.backup.BackupDataOutput;
import android.app.backup.FullBackupDataOutput;
import android.content.Context;
import android.os.ParcelFileDescriptor;
import c.a.f;
import c.g.b.k;
import c.u;
import com.truecaller.backup.d;
import com.truecaller.common.g.a;
import com.truecaller.common.g.c;
import java.io.File;

public final class TruecallerBackupAgent
  extends BackupAgent
{
  public a a;
  
  private final void a(String paramString)
  {
    boolean bool1 = false;
    Object localObject1 = "account.v2.bak";
    boolean bool2;
    try
    {
      localObject1 = getFileStreamPath((String)localObject1);
      localObject2 = "getFileStreamPath(\"account.v2.bak\")";
      k.a(localObject1, (String)localObject2);
      bool2 = ((File)localObject1).exists();
    }
    catch (SecurityException localSecurityException)
    {
      bool2 = false;
      localObject1 = null;
    }
    Object localObject2 = getSystemService("account");
    if (localObject2 != null)
    {
      localObject2 = (AccountManager)localObject2;
      int i = 2131887458;
      Object localObject3 = getString(i);
      localObject3 = ((AccountManager)localObject2).getAccountsByType((String)localObject3);
      String str1 = "accountManager.getAccoun…henticator_account_type))";
      k.a(localObject3, str1);
      localObject3 = (Account)f.c((Object[])localObject3);
      if (localObject3 != null)
      {
        Object localObject4 = ((AccountManager)localObject2).peekAuthToken((Account)localObject3, "installation_id_backup");
        Object localObject5 = ((AccountManager)localObject2).getUserData((Account)localObject3, "normalized_number_backup");
        String str2 = "country_code_backup";
        localObject2 = ((AccountManager)localObject2).getUserData((Account)localObject3, str2);
        localObject4 = (CharSequence)localObject4;
        if (localObject4 != null)
        {
          i = ((CharSequence)localObject4).length();
          if (i != 0)
          {
            i = 0;
            localObject3 = null;
            break label181;
          }
        }
        i = 1;
        label181:
        if (i == 0)
        {
          localObject5 = (CharSequence)localObject5;
          if (localObject5 != null)
          {
            i = ((CharSequence)localObject5).length();
            if (i != 0)
            {
              i = 0;
              localObject3 = null;
              break label227;
            }
          }
          i = 1;
          label227:
          if (i == 0)
          {
            localObject2 = (CharSequence)localObject2;
            if (localObject2 != null)
            {
              j = ((CharSequence)localObject2).length();
              if (j != 0)
              {
                j = 0;
                localObject2 = null;
                break label273;
              }
            }
            int j = 1;
            label273:
            if (j == 0) {
              bool1 = true;
            }
          }
        }
      }
      localObject2 = new com/truecaller/backup/d;
      localObject3 = a;
      if (localObject3 == null)
      {
        str1 = "settings";
        k.a(str1);
      }
      ((d)localObject2).<init>((a)localObject3);
      ((d)localObject2).a(paramString, bool2, bool1);
      return;
    }
    paramString = new c/u;
    paramString.<init>("null cannot be cast to non-null type android.accounts.AccountManager");
    throw paramString;
  }
  
  public final void onBackup(ParcelFileDescriptor paramParcelFileDescriptor1, BackupDataOutput paramBackupDataOutput, ParcelFileDescriptor paramParcelFileDescriptor2)
  {
    paramParcelFileDescriptor1 = a;
    if (paramParcelFileDescriptor1 == null)
    {
      paramBackupDataOutput = "settings";
      k.a(paramBackupDataOutput);
    }
    paramParcelFileDescriptor1.b("accountFileWasBackedUpByAutobackup", true);
    a("onBackup");
  }
  
  public final void onCreate()
  {
    super.onCreate();
    bf.a locala = bf.a();
    c localc = new com/truecaller/common/g/c;
    Object localObject = this;
    localObject = (Context)this;
    localc.<init>((Context)localObject, "tc.settings");
    locala.a(localc).a().a(this);
  }
  
  public final void onFullBackup(FullBackupDataOutput paramFullBackupDataOutput)
  {
    super.onFullBackup(paramFullBackupDataOutput);
    paramFullBackupDataOutput = a;
    if (paramFullBackupDataOutput == null)
    {
      String str = "settings";
      k.a(str);
    }
    paramFullBackupDataOutput.b("accountFileWasBackedUpByAutobackup", true);
    a("onFullBackup");
  }
  
  public final void onRestore(BackupDataInput paramBackupDataInput, int paramInt, ParcelFileDescriptor paramParcelFileDescriptor) {}
  
  public final void onRestoreFinished()
  {
    super.onRestoreFinished();
    a locala = a;
    if (locala == null)
    {
      String str = "settings";
      k.a(str);
    }
    locala.b("accountFileWasRestoredByAutobackup", true);
    a("onRestore");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.TruecallerBackupAgent
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */