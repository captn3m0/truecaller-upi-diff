package com.truecaller.tag;

import android.animation.Animator.AnimatorListener;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.j;
import android.support.v4.app.o;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.view.Window;
import android.view.animation.DecelerateInterpolator;
import com.truecaller.service.DataManagerService;
import com.truecaller.service.h;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;
import com.truecaller.ui.n;

public abstract class b
  extends n
  implements View.OnClickListener
{
  private View a;
  private h b;
  private final ColorDrawable c;
  private b.a d;
  private final ValueAnimator.AnimatorUpdateListener i;
  
  public b()
  {
    Object localObject = new android/graphics/drawable/ColorDrawable;
    ((ColorDrawable)localObject).<init>(0);
    c = ((ColorDrawable)localObject);
    localObject = new com/truecaller/tag/b$1;
    ((b.1)localObject).<init>(this);
    i = ((ValueAnimator.AnimatorUpdateListener)localObject);
  }
  
  protected abstract b.a d();
  
  public void finish()
  {
    Object localObject1 = new float[2];
    float f = a.getHeight();
    localObject1[0] = f;
    localObject1[1] = 0.0F;
    localObject1 = ValueAnimator.ofFloat((float[])localObject1);
    long l = getResources().getInteger(17694720);
    ((ValueAnimator)localObject1).setDuration(l);
    Object localObject2 = new android/view/animation/DecelerateInterpolator;
    ((DecelerateInterpolator)localObject2).<init>(1.0F);
    ((ValueAnimator)localObject1).setInterpolator((TimeInterpolator)localObject2);
    localObject2 = i;
    ((ValueAnimator)localObject1).addUpdateListener((ValueAnimator.AnimatorUpdateListener)localObject2);
    localObject2 = new com/truecaller/tag/b$3;
    ((b.3)localObject2).<init>(this);
    ((ValueAnimator)localObject1).addListener((Animator.AnimatorListener)localObject2);
    ((ValueAnimator)localObject1).reverse();
  }
  
  public void onBackPressed()
  {
    super.onBackPressed();
    b.a locala = d;
    if (locala != null) {
      locala.a();
    }
  }
  
  public void onClick(View paramView)
  {
    int j = paramView.getId();
    int k = 16908290;
    if (j == k)
    {
      paramView = d;
      if (paramView != null)
      {
        paramView.a();
        return;
      }
      finish();
    }
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getTheme();
    int j = aresId;
    b.a locala = null;
    paramBundle.applyStyle(j, false);
    setContentView(2131558474);
    paramBundle = getWindow();
    Object localObject = c;
    paramBundle.setBackgroundDrawable((Drawable)localObject);
    paramBundle = new com/truecaller/service/h;
    paramBundle.<init>(this, DataManagerService.class);
    b = paramBundle;
    int k = 16908290;
    paramBundle = findViewById(k);
    a = paramBundle;
    paramBundle = a.getViewTreeObserver();
    localObject = new com/truecaller/tag/b$2;
    ((b.2)localObject).<init>(this);
    paramBundle.addOnPreDrawListener((ViewTreeObserver.OnPreDrawListener)localObject);
    a.setOnClickListener(this);
    paramBundle = d();
    d = paramBundle;
    paramBundle = d;
    if (paramBundle != null)
    {
      paramBundle = getSupportFragmentManager().a();
      j = 2131363156;
      locala = d;
      paramBundle = paramBundle.a(j, locala);
      paramBundle.c();
    }
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    h localh = b;
    if (localh != null) {
      localh.b();
    }
  }
  
  public void onStart()
  {
    super.onStart();
    b.a();
  }
  
  public void onStop()
  {
    super.onStop();
    b.b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tag.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */