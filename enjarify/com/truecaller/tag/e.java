package com.truecaller.tag;

import android.content.Context;
import android.text.TextUtils;
import com.truecaller.androidactors.w;
import com.truecaller.common.tag.d;
import com.truecaller.data.access.k;
import com.truecaller.data.access.u;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.data.entity.Tag;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

final class e
  implements c
{
  private final Context a;
  
  e(Context paramContext)
  {
    a = paramContext;
  }
  
  public final w a(Contact paramContact, long paramLong1, long paramLong2, int paramInt1, int paramInt2)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Object localObject1 = paramContact.A().iterator();
    Object localObject3;
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      Object localObject2 = ((Number)((Iterator)localObject1).next()).a();
      bool2 = TextUtils.isEmpty((CharSequence)localObject2);
      if (!bool2)
      {
        localObject3 = "\\+";
        String str1 = "";
        try
        {
          localObject2 = ((String)localObject2).replaceFirst((String)localObject3, str1);
          localObject2 = Long.valueOf((String)localObject2);
          localArrayList.add(localObject2);
        }
        catch (NumberFormatException localNumberFormatException) {}
      }
    }
    localObject1 = a;
    d.a((Context)localObject1, localArrayList, paramLong1, paramLong2, paramInt1, paramInt2);
    paramInt1 = com.truecaller.data.access.c.b(paramContact);
    paramInt2 = 0;
    if (paramInt1 == 0)
    {
      localObject4 = new com/truecaller/data/access/c;
      localObject1 = a;
      ((com.truecaller.data.access.c)localObject4).<init>((Context)localObject1);
      paramContact = ((com.truecaller.data.access.c)localObject4).c(paramContact);
      if (paramContact == null) {
        return w.b(null);
      }
    }
    Object localObject4 = new com/truecaller/data/access/u;
    localObject1 = a;
    ((u)localObject4).<init>((Context)localObject1);
    ((u)localObject4).a(paramContact);
    paramContact.ab();
    int i = 16;
    long l1 = 0L;
    boolean bool2 = paramLong1 < l1;
    Object localObject5;
    if (!bool2)
    {
      long l2 = -1;
      boolean bool3 = paramLong1 < l2;
      if (bool3) {}
    }
    else
    {
      localObject3 = new com/truecaller/data/entity/Tag;
      ((Tag)localObject3).<init>();
      ((Tag)localObject3).setSource(i);
      localObject5 = String.valueOf(paramLong1);
      ((Tag)localObject3).a((String)localObject5);
      ((u)localObject4).a(paramContact, (Tag)localObject3);
      paramContact.a((Tag)localObject3);
    }
    boolean bool4 = paramLong2 < l1;
    if (bool4)
    {
      localObject5 = new com/truecaller/data/entity/Tag;
      ((Tag)localObject5).<init>();
      ((Tag)localObject5).setSource(i);
      String str2 = String.valueOf(paramLong2);
      ((Tag)localObject5).a(str2);
      ((u)localObject4).a(paramContact, (Tag)localObject5);
      paramContact.a((Tag)localObject5);
    }
    return w.b(null);
  }
  
  public final w a(Contact paramContact, String paramString, int paramInt)
  {
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    Object localObject2 = paramContact.A().iterator();
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject2).hasNext();
      if (!bool1) {
        break;
      }
      String str = ((Number)((Iterator)localObject2).next()).a();
      boolean bool2 = TextUtils.isEmpty(str);
      if (!bool2) {
        ((List)localObject1).add(str);
      }
    }
    localObject2 = a;
    d.a((Context)localObject2, (List)localObject1, paramString, paramInt);
    paramInt = com.truecaller.data.access.c.b(paramContact);
    if (paramInt == 0)
    {
      localObject3 = new com/truecaller/data/access/c;
      localObject1 = a;
      ((com.truecaller.data.access.c)localObject3).<init>((Context)localObject1);
      localObject3 = ((com.truecaller.data.access.c)localObject3).c(paramContact);
      if (localObject3 == null) {
        return w.b(paramContact);
      }
      paramContact = (Contact)localObject3;
    }
    Object localObject3 = new com/truecaller/data/access/k;
    localObject1 = a;
    ((k)localObject3).<init>((Context)localObject1);
    boolean bool3 = TextUtils.isEmpty(paramString);
    if (bool3) {
      paramString = null;
    }
    return w.b(((k)localObject3).a(paramContact, paramString));
  }
  
  public final void a(Contact paramContact, int paramInt)
  {
    Object localObject1 = paramContact.A().iterator();
    int i = 0;
    Context localContext = null;
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      String str = ((Number)((Iterator)localObject1).next()).a();
      boolean bool2 = TextUtils.isEmpty(str);
      if (!bool2)
      {
        localContext = a;
        d.a(localContext, str, paramInt);
        i = 1;
      }
    }
    if (i != 0)
    {
      paramInt = com.truecaller.data.access.c.b(paramContact);
      if (paramInt == 0)
      {
        localObject2 = new com/truecaller/data/access/c;
        localObject1 = a;
        ((com.truecaller.data.access.c)localObject2).<init>((Context)localObject1);
        ((com.truecaller.data.access.c)localObject2).c(paramContact);
      }
      Object localObject2 = new com/truecaller/data/access/k;
      localObject1 = a;
      ((k)localObject2).<init>((Context)localObject1);
      localObject1 = null;
      ((k)localObject2).a(paramContact, null);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tag.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */