package com.truecaller.tag;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.data.entity.Contact;

final class d$a
  extends u
{
  private final Contact b;
  private final String c;
  private final int d;
  
  private d$a(e parame, Contact paramContact, String paramString, int paramInt)
  {
    super(parame);
    b = paramContact;
    c = paramString;
    d = paramInt;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".suggestNameForContact(");
    Object localObject = b;
    int i = 1;
    localObject = a(localObject, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(c, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(Integer.valueOf(d), 2);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tag.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */