package com.truecaller.tag;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import com.truecaller.common.tag.TagView;

final class j$6
  implements ValueAnimator.AnimatorUpdateListener
{
  j$6(j paramj) {}
  
  public final void onAnimationUpdate(ValueAnimator paramValueAnimator)
  {
    float f = ((Float)paramValueAnimator.getAnimatedValue()).floatValue();
    j.g(a).setScaleX(f);
    j.g(a).setScaleY(f);
    TagView localTagView = j.g(a);
    f = Math.min(1.0F, f);
    localTagView.setAlpha(f);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tag.j.6
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */