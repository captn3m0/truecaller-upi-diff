package com.truecaller.tag;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.v;
import com.truecaller.androidactors.w;
import com.truecaller.data.entity.Contact;

public final class d
  implements c
{
  private final v a;
  
  public d(v paramv)
  {
    a = paramv;
  }
  
  public static boolean a(Class paramClass)
  {
    return c.class.equals(paramClass);
  }
  
  public final w a(Contact paramContact, long paramLong1, long paramLong2, int paramInt1, int paramInt2)
  {
    v localv = a;
    d.b localb = new com/truecaller/tag/d$b;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localb.<init>(locale, paramContact, paramLong1, paramLong2, paramInt1, paramInt2, (byte)0);
    return w.a(localv, localb);
  }
  
  public final w a(Contact paramContact, String paramString, int paramInt)
  {
    v localv = a;
    d.a locala = new com/truecaller/tag/d$a;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locala.<init>(locale, paramContact, paramString, paramInt, (byte)0);
    return w.a(localv, locala);
  }
  
  public final void a(Contact paramContact, int paramInt)
  {
    v localv = a;
    d.c localc = new com/truecaller/tag/d$c;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localc.<init>(locale, paramContact, paramInt, (byte)0);
    localv.a(localc);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tag.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */