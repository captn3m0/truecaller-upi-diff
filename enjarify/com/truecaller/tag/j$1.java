package com.truecaller.tag;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;

final class j$1
  extends AnimatorListenerAdapter
{
  j$1(j paramj) {}
  
  public final void onAnimationEnd(Animator paramAnimator)
  {
    j.a(a).setVisibility(4);
    j.a(a).setTranslationY(0.0F);
    j.a(a).setAlpha(1.0F);
  }
  
  public final void onAnimationStart(Animator paramAnimator)
  {
    j.a(a, true);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tag.j.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */