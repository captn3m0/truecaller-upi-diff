package com.truecaller.tag;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;

final class b$1
  implements ValueAnimator.AnimatorUpdateListener
{
  b$1(b paramb) {}
  
  public final void onAnimationUpdate(ValueAnimator paramValueAnimator)
  {
    float f1 = paramValueAnimator.getAnimatedFraction();
    View localView = b.a(a);
    float f2 = ((Float)paramValueAnimator.getAnimatedValue()).floatValue();
    localView.setTranslationY(f2);
    paramValueAnimator = b.b(a);
    int i = Color.argb((int)(f1 * 255.0F * 0.2F), 0, 0, 0);
    paramValueAnimator.setColor(i);
    b.b(a).invalidateSelf();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tag.b.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */