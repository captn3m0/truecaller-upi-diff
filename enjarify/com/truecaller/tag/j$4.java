package com.truecaller.tag;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;

final class j$4
  extends AnimatorListenerAdapter
{
  j$4(j paramj) {}
  
  public final void onAnimationEnd(Animator paramAnimator)
  {
    j.a(a, false);
  }
  
  public final void onAnimationStart(Animator paramAnimator)
  {
    j.d(a).setVisibility(0);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tag.j.4
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */