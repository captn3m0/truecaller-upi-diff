package com.truecaller.tag;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;
import com.truecaller.common.tag.TagView;

final class j$2
  extends AnimatorListenerAdapter
{
  j$2(j paramj) {}
  
  public final void onAnimationEnd(Animator paramAnimator)
  {
    j.a(a, false);
    paramAnimator = j.b(a);
    if (paramAnimator != null)
    {
      paramAnimator = j.b(a);
      boolean bool = true;
      paramAnimator.a(false, bool);
      paramAnimator = a;
      j.c(paramAnimator);
    }
  }
  
  public final void onAnimationStart(Animator paramAnimator)
  {
    j.a(a).setVisibility(0);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tag.j.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */