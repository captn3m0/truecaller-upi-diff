package com.truecaller.tag;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.view.View;
import android.view.ViewGroup;
import com.truecaller.common.tag.TagView;

final class j$5
  extends AnimatorListenerAdapter
{
  j$5(j paramj) {}
  
  public final void onAnimationEnd(Animator paramAnimator)
  {
    j.f(a);
  }
  
  public final void onAnimationRepeat(Animator paramAnimator)
  {
    j localj = a;
    paramAnimator = (Float)((ValueAnimator)paramAnimator).getAnimatedValue();
    float f = paramAnimator.floatValue();
    int i = 0;
    for (;;)
    {
      Object localObject = b;
      int j = ((ViewGroup)localObject).getChildCount();
      if (i >= j) {
        break;
      }
      localObject = b.getChildAt(i);
      boolean bool = localObject instanceof TagView;
      if (bool)
      {
        TagView localTagView = c;
        if (localObject != localTagView) {
          ((View)localObject).setAlpha(f);
        }
      }
      i += 1;
    }
    a.invalidate();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tag.j.5
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */