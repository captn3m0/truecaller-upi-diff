package com.truecaller.tag;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.truecaller.TrueApp;
import com.truecaller.analytics.bc;
import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.i;
import com.truecaller.androidactors.k;
import com.truecaller.androidactors.w;
import com.truecaller.bp;
import com.truecaller.data.entity.Contact;
import java.util.Objects;

public final class a
  extends b.a
  implements View.OnClickListener
{
  private com.truecaller.androidactors.f a;
  private i b;
  private com.truecaller.analytics.b c;
  private com.truecaller.featuretoggles.e d;
  private TextView e;
  private EditText f;
  private RadioGroup g;
  private Button h;
  private Contact i;
  private String j;
  
  static a a(Contact paramContact, String paramString)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putParcelable("contact", paramContact);
    localBundle.putString("source", paramString);
    paramContact = new com/truecaller/tag/a;
    paramContact.<init>();
    paramContact.setArguments(localBundle);
    return paramContact;
  }
  
  private void a(Contact paramContact)
  {
    android.support.v4.app.f localf = getActivity();
    if (localf != null)
    {
      Intent localIntent = new android/content/Intent;
      localIntent.<init>();
      String str = "contact";
      localIntent.putExtra(str, paramContact);
      int k = -1;
      localf.setResult(k, localIntent);
      localf.finish();
    }
  }
  
  private void b()
  {
    Object localObject1 = f.getText().toString().trim();
    boolean bool = TextUtils.isEmpty((CharSequence)localObject1);
    int m = 0;
    Object localObject2 = null;
    if (bool)
    {
      localObject3 = j;
      bool = TextUtils.isEmpty((CharSequence)localObject3);
      if (bool)
      {
        Toast.makeText(getActivity(), 2131887239, 0).show();
        return;
      }
    }
    f.setEnabled(false);
    g.setEnabled(false);
    h.setEnabled(false);
    Object localObject3 = g;
    int k = ((RadioGroup)localObject3).getCheckedRadioButtonId();
    m = 2131362274;
    int n = 1;
    if (k == m) {
      k = 2;
    } else {
      k = 1;
    }
    localObject2 = (c)a.a();
    Contact localContact = i;
    localObject1 = ((c)localObject2).a(localContact, (String)localObject1, k);
    localObject3 = b;
    localObject2 = new com/truecaller/tag/-$$Lambda$a$s6EwG0vgx9qQlofw1y-N1trW5T4;
    ((-..Lambda.a.s6EwG0vgx9qQlofw1y-N1trW5T4)localObject2).<init>(this);
    ((w)localObject1).a((i)localObject3, (ac)localObject2);
    Toast.makeText(getActivity(), 2131887241, n).show();
  }
  
  final void a() {}
  
  public final void onClick(View paramView)
  {
    int k = paramView.getId();
    int m = 2131364228;
    if (k == m)
    {
      b();
      return;
    }
    m = 2131362510;
    if (k == m)
    {
      paramView = getActivity();
      if (paramView != null) {
        paramView.finish();
      }
    }
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = TrueApp.y().a();
    Object localObject = paramBundle.aH();
    a = ((com.truecaller.androidactors.f)localObject);
    localObject = paramBundle.c();
    c = ((com.truecaller.analytics.b)localObject);
    localObject = paramBundle.m().a();
    b = ((i)localObject);
    paramBundle = paramBundle.aF();
    d = paramBundle;
    paramBundle = c;
    localObject = new com/truecaller/analytics/bc;
    ((bc)localObject).<init>("nameSuggestion");
    paramBundle.a((com.truecaller.analytics.e)localObject);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    return paramLayoutInflater.inflate(2131559229, paramViewGroup, false);
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    Object localObject = (TextView)paramView.findViewById(2131364893);
    e = ((TextView)localObject);
    localObject = (EditText)paramView.findViewById(2131363791);
    f = ((EditText)localObject);
    localObject = (RadioGroup)paramView.findViewById(2131364061);
    g = ((RadioGroup)localObject);
    localObject = (Button)paramView.findViewById(2131364228);
    h = ((Button)localObject);
    localObject = (android.support.v4.app.f)Objects.requireNonNull(getActivity());
    int k = 2131887242;
    ((android.support.v4.app.f)localObject).setTitle(k);
    localObject = (Contact)((Bundle)Objects.requireNonNull(getArguments())).getParcelable("contact");
    i = ((Contact)localObject);
    localObject = new com/truecaller/tag/a$a;
    int m = 0;
    ((a.a)localObject).<init>(this, (byte)0);
    int n = 1;
    Contact[] arrayOfContact = new Contact[n];
    Contact localContact = i;
    arrayOfContact[0] = localContact;
    com.truecaller.old.a.b.a((AsyncTask)localObject, arrayOfContact);
    localObject = i;
    boolean bool = ((Contact)localObject).N();
    if (bool)
    {
      localObject = e;
      m = 2131886285;
      ((TextView)localObject).setText(m);
      localObject = f;
      ((EditText)localObject).setHint(k);
    }
    localObject = f;
    -..Lambda.a.TrQ6T9Gnlkh8ZqgxMdCkG-JjHKo localTrQ6T9Gnlkh8ZqgxMdCkG-JjHKo = new com/truecaller/tag/-$$Lambda$a$TrQ6T9Gnlkh8ZqgxMdCkG-JjHKo;
    localTrQ6T9Gnlkh8ZqgxMdCkG-JjHKo.<init>(this);
    ((EditText)localObject).setOnEditorActionListener(localTrQ6T9Gnlkh8ZqgxMdCkG-JjHKo);
    h.setOnClickListener(this);
    paramView.findViewById(2131362510).setOnClickListener(this);
    super.onViewCreated(paramView, paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tag.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */