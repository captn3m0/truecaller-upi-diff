package com.truecaller.tag;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.data.entity.Contact;

final class d$b
  extends u
{
  private final Contact b;
  private final long c;
  private final long d;
  private final int e;
  private final int f;
  
  private d$b(e parame, Contact paramContact, long paramLong1, long paramLong2, int paramInt1, int paramInt2)
  {
    super(parame);
    b = paramContact;
    c = paramLong1;
    d = paramLong2;
    e = paramInt1;
    f = paramInt2;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".tagContact(");
    Object localObject = a(b, 1);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = Long.valueOf(c);
    int i = 2;
    localObject = a(localObject, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(Long.valueOf(d), i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(Integer.valueOf(e), i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(Integer.valueOf(f), i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tag.d.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */