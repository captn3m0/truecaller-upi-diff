package com.truecaller.tag;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;

final class j$3
  extends AnimatorListenerAdapter
{
  j$3(j paramj) {}
  
  public final void onAnimationEnd(Animator paramAnimator)
  {
    j.d(a).setVisibility(8);
    j.d(a).setTranslationY(0.0F);
    j.d(a).setAlpha(1.0F);
    j.e(a);
  }
  
  public final void onAnimationStart(Animator paramAnimator)
  {
    j.a(a, true);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tag.j.3
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */