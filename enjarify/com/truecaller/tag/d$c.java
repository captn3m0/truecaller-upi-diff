package com.truecaller.tag;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.data.entity.Contact;

final class d$c
  extends u
{
  private final Contact b;
  private final int c;
  
  private d$c(e parame, Contact paramContact, int paramInt)
  {
    super(parame);
    b = paramContact;
    c = paramInt;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".updateEntityTypeForContact(");
    Object localObject = b;
    int i = 2;
    localObject = a(localObject, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(Integer.valueOf(c), i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tag.d.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */