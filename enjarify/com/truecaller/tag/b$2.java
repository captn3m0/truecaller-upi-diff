package com.truecaller.tag;

import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnPreDrawListener;

final class b$2
  implements ViewTreeObserver.OnPreDrawListener
{
  b$2(b paramb) {}
  
  public final boolean onPreDraw()
  {
    b.a(a).getViewTreeObserver().removeOnPreDrawListener(this);
    b.c(a);
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tag.b.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */