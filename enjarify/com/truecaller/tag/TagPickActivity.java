package com.truecaller.tag;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;
import com.truecaller.TrueApp;
import com.truecaller.analytics.e;
import com.truecaller.analytics.e.a;
import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.i;
import com.truecaller.androidactors.k;
import com.truecaller.androidactors.w;
import com.truecaller.bp;
import com.truecaller.data.entity.Contact;
import com.truecaller.util.ce;

public class TagPickActivity
  extends b
  implements j.b
{
  private Contact a;
  private int b;
  private int c;
  private com.truecaller.androidactors.a d;
  private f i;
  private i j;
  
  public static Intent a(Context paramContext, Contact paramContact, int paramInt1, int paramInt2)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, TagPickActivity.class);
    localIntent.putExtra("contact", paramContact);
    localIntent.putExtra("tag_context", paramInt1);
    localIntent.putExtra("search_type", paramInt2);
    return localIntent;
  }
  
  public static Intent a(Context paramContext, Long paramLong, int paramInt)
  {
    Intent localIntent = new android/content/Intent;
    Class localClass = TagPickActivity.class;
    localIntent.<init>(paramContext, localClass);
    paramContext = "initial_tag";
    long l;
    if (paramLong != null) {
      l = paramLong.longValue();
    } else {
      l = Long.MIN_VALUE;
    }
    localIntent.putExtra(paramContext, l);
    localIntent.putExtra("tag_context", paramInt);
    return localIntent;
  }
  
  private void a(com.truecaller.common.tag.c paramc, Contact paramContact)
  {
    d = null;
    b(paramc);
    Intent localIntent = new android/content/Intent;
    localIntent.<init>();
    if (paramc != null)
    {
      String str = "tag_id";
      long l = a;
      localIntent.putExtra(str, l);
    }
    localIntent.putExtra("contact", paramContact);
    setResult(-1, localIntent);
    finish();
  }
  
  private static void b(com.truecaller.common.tag.c paramc)
  {
    Object localObject = new com/truecaller/analytics/e$a;
    String str = "TAGVIEW_Tagged";
    ((e.a)localObject).<init>(str);
    if (paramc != null)
    {
      long l = a;
      paramc = String.valueOf(l);
    }
    else
    {
      paramc = "NONE";
    }
    ((e.a)localObject).a("Tag_Id", paramc);
    paramc = TrueApp.y().a().c();
    localObject = ((e.a)localObject).a();
    paramc.a((e)localObject);
  }
  
  public final void a(com.truecaller.common.tag.c paramc)
  {
    int k = 1;
    Object localObject1 = new String[k];
    Object localObject2 = String.valueOf(paramc);
    Object localObject3 = "Tag changed to ".concat((String)localObject2);
    localObject2 = null;
    localObject1[0] = localObject3;
    localObject1 = a;
    if (localObject1 != null)
    {
      localObject1 = d;
      if (localObject1 != null) {
        ((com.truecaller.androidactors.a)localObject1).a();
      }
      long l1 = -1;
      long l3;
      if (paramc != null)
      {
        long l2 = c;
        l3 = l2;
      }
      else
      {
        l3 = l1;
      }
      if (paramc != null) {
        l1 = a;
      }
      localObject1 = i.a();
      Object localObject4 = localObject1;
      localObject4 = (c)localObject1;
      Contact localContact = a;
      int m = c;
      int n = b;
      localObject1 = ((c)localObject4).a(localContact, l3, l1, m, n);
      localObject3 = j;
      localObject2 = new com/truecaller/tag/-$$Lambda$TagPickActivity$5PfQhuNkBjldPYdFWN2xS9SI4kE;
      ((-..Lambda.TagPickActivity.5PfQhuNkBjldPYdFWN2xS9SI4kE)localObject2).<init>(this, paramc);
      localObject1 = ((w)localObject1).a((i)localObject3, (ac)localObject2);
      d = ((com.truecaller.androidactors.a)localObject1);
      if (paramc != null)
      {
        int i1 = 2131887275;
        paramc = Toast.makeText(this, i1, k);
        paramc.show();
      }
      return;
    }
    a(paramc, null);
  }
  
  protected final b.a d()
  {
    Object localObject = getIntent();
    Long localLong1 = null;
    if (localObject == null)
    {
      finish();
      return null;
    }
    int k = ((Intent)localObject).getIntExtra("search_type", 999);
    b = k;
    k = ((Intent)localObject).getIntExtra("tag_context", 0);
    c = k;
    long l1 = Long.MIN_VALUE;
    long l2 = ((Intent)localObject).getLongExtra("initial_tag", l1);
    Long localLong2 = Long.valueOf(l2);
    String str = "contact";
    localObject = (Contact)((Intent)localObject).getParcelableExtra(str);
    a = ((Contact)localObject);
    localObject = a;
    if (localObject != null)
    {
      localObject = ce.a(((Contact)localObject).J());
      if (localObject != null)
      {
        long l3 = a;
        localLong1 = Long.valueOf(l3);
      }
      localLong2 = localLong1;
    }
    int m = c;
    return j.a(localLong2, m);
  }
  
  public final void e()
  {
    setResult(0);
    finish();
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    com.truecaller.utils.extensions.a.a(this);
    paramBundle = TrueApp.y().a();
    f localf = paramBundle.aH();
    i = localf;
    paramBundle = paramBundle.m().a();
    j = paramBundle;
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    com.truecaller.androidactors.a locala = d;
    if (locala != null)
    {
      locala.a();
      locala = null;
      d = null;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tag.TagPickActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */