package com.truecaller.tag;

import android.animation.Animator.AnimatorListener;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.w;
import android.support.v4.app.w.a;
import android.support.v4.view.g;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewPropertyAnimator;
import android.widget.TextView;
import com.truecaller.TrueApp;
import com.truecaller.analytics.b;
import com.truecaller.analytics.bc;
import com.truecaller.bp;
import com.truecaller.common.tag.TagView;
import com.truecaller.common.tag.d;
import java.util.Iterator;
import java.util.List;

public final class j
  extends b.a
  implements w.a, View.OnClickListener
{
  ViewGroup a;
  ViewGroup b;
  TagView c;
  private View d;
  private View e;
  private TextView f;
  private TagView g;
  private com.truecaller.common.tag.c h;
  private TagView i;
  private boolean j = false;
  private float k;
  private final Animator.AnimatorListener l;
  private final Animator.AnimatorListener m;
  private final Animator.AnimatorListener n;
  private final Animator.AnimatorListener o;
  
  public j()
  {
    Object localObject = new com/truecaller/tag/j$1;
    ((j.1)localObject).<init>(this);
    l = ((Animator.AnimatorListener)localObject);
    localObject = new com/truecaller/tag/j$2;
    ((j.2)localObject).<init>(this);
    m = ((Animator.AnimatorListener)localObject);
    localObject = new com/truecaller/tag/j$3;
    ((j.3)localObject).<init>(this);
    n = ((Animator.AnimatorListener)localObject);
    localObject = new com/truecaller/tag/j$4;
    ((j.4)localObject).<init>(this);
    o = ((Animator.AnimatorListener)localObject);
  }
  
  private TagView a(ViewGroup paramViewGroup, List paramList)
  {
    paramViewGroup.removeAllViews();
    Resources localResources = getResources();
    int i1 = localResources.getDimensionPixelSize(2131166204);
    paramList = paramList.iterator();
    Object localObject1 = null;
    for (;;)
    {
      boolean bool1 = paramList.hasNext();
      if (!bool1) {
        break;
      }
      Object localObject2 = (com.truecaller.common.tag.c)paramList.next();
      TagView localTagView = new com/truecaller/common/tag/TagView;
      Object localObject3 = getContext();
      long l1 = c;
      long l2 = 0L;
      boolean bool2 = true;
      boolean bool3 = l1 < l2;
      boolean bool4;
      if (!bool3) {
        bool4 = true;
      } else {
        bool4 = false;
      }
      localTagView.<init>((Context)localObject3, false, bool4);
      localTagView.setTag((com.truecaller.common.tag.c)localObject2);
      localTagView.setOnClickListener(this);
      localObject3 = h;
      if (localObject3 != null)
      {
        long l3 = a;
        com.truecaller.common.tag.c localc = h;
        long l4 = a;
        boolean bool5 = l3 < l4;
        if (bool5)
        {
          l3 = a;
          localObject2 = h;
          l4 = c;
          bool1 = l3 < l4;
          if (bool1) {}
        }
        else
        {
          localTagView.a(bool2, false);
          localObject1 = localTagView;
        }
      }
      localObject2 = new android/view/ViewGroup$MarginLayoutParams;
      int i2 = -2;
      ((ViewGroup.MarginLayoutParams)localObject2).<init>(i2, i2);
      g.a((ViewGroup.MarginLayoutParams)localObject2, i1);
      bottomMargin = i1;
      paramViewGroup.addView(localTagView, (ViewGroup.LayoutParams)localObject2);
    }
    return (TagView)localObject1;
  }
  
  static j a(Long paramLong, int paramInt)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    String str = "initial_tag";
    long l1;
    if (paramLong != null) {
      l1 = paramLong.longValue();
    } else {
      l1 = Long.MIN_VALUE;
    }
    localBundle.putLong(str, l1);
    localBundle.putInt("tag_context", paramInt);
    paramLong = new com/truecaller/tag/j;
    paramLong.<init>();
    paramLong.setArguments(localBundle);
    return paramLong;
  }
  
  private void a(long paramLong)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putLong("root_tag", paramLong);
    getLoaderManager().a(2131364983, localBundle, this);
  }
  
  private void b()
  {
    Object localObject1 = getActivity();
    boolean bool1 = localObject1 instanceof j.b;
    if (bool1)
    {
      localObject1 = (j.b)localObject1;
      Object localObject2 = c;
      if (localObject2 != null)
      {
        localObject2 = ((TagView)localObject2).getAvailableTag();
      }
      else
      {
        localObject2 = i;
        if (localObject2 != null)
        {
          localObject2 = ((TagView)localObject2).getAvailableTag();
        }
        else
        {
          bool1 = false;
          localObject2 = null;
        }
      }
      if (localObject2 != null)
      {
        localc = h;
        bool2 = ((com.truecaller.common.tag.c)localObject2).equals(localc);
        if (!bool2) {}
      }
      else
      {
        localc = h;
        if (localc == null) {
          break label110;
        }
        bool2 = localc.equals(localObject2);
        if (bool2) {
          break label110;
        }
      }
      boolean bool2 = true;
      break label116;
      label110:
      bool2 = false;
      com.truecaller.common.tag.c localc = null;
      label116:
      if (bool2)
      {
        ((j.b)localObject1).a((com.truecaller.common.tag.c)localObject2);
        return;
      }
      ((j.b)localObject1).e();
    }
  }
  
  public final android.support.v4.content.c a(int paramInt, Bundle paramBundle)
  {
    long l1 = 0L;
    if (paramBundle != null)
    {
      localObject = "root_tag";
      l1 = paramBundle.getLong((String)localObject, l1);
    }
    Object localObject = new com/truecaller/tag/j$a;
    paramBundle = getContext();
    ((j.a)localObject).<init>(paramBundle, l1);
    return (android.support.v4.content.c)localObject;
  }
  
  final void a()
  {
    b();
  }
  
  public final void onClick(View paramView)
  {
    int i1 = paramView.getId();
    long l1 = 200L;
    int i2 = 2131364658;
    float f1 = 1.834916E38F;
    float f2;
    Object localObject1;
    Object localObject2;
    if (i1 == i2)
    {
      paramView = e.animate();
      f2 = -k;
      paramView = paramView.translationYBy(f2).alpha(0.0F).setDuration(l1);
      localObject1 = n;
      paramView.setListener((Animator.AnimatorListener)localObject1).start();
      paramView = d;
      f1 = k;
      paramView.setTranslationY(f1);
      d.setAlpha(0.0F);
      paramView = d.animate().translationY(0.0F).alpha(1.0F).setDuration(l1).setStartDelay(l1);
      localObject2 = m;
      paramView.setListener((Animator.AnimatorListener)localObject2).start();
      return;
    }
    i2 = 2131362510;
    f1 = 1.8344803E38F;
    if (i1 == i2)
    {
      b();
      return;
    }
    boolean bool1 = paramView instanceof TagView;
    if (bool1)
    {
      bool1 = j;
      if (!bool1)
      {
        paramView = (TagView)paramView;
        long l2 = paramView.getParentTagId();
        long l3 = 0L;
        bool1 = false;
        f2 = 0.0F;
        localObject2 = null;
        boolean bool2 = false;
        boolean bool3 = true;
        boolean bool4 = l2 < l3;
        if (!bool4)
        {
          TagView localTagView = i;
          if (paramView == localTagView)
          {
            paramView.a(false, bool3);
            i = null;
          }
          else
          {
            if (localTagView != null) {
              localTagView.a(false, bool3);
            }
            long l4 = paramView.getTagId();
            i = paramView;
            paramView.a(bool3, bool3);
            paramView = f;
            int i3 = 2131887273;
            paramView.setText(i3);
            a(l4);
          }
          paramView = new String[bool3];
          localObject2 = new java/lang/StringBuilder;
          ((StringBuilder)localObject2).<init>("Root = ");
          localTagView = g;
          ((StringBuilder)localObject2).append(localTagView);
          ((StringBuilder)localObject2).append(", Parent = ");
          localTagView = i;
          ((StringBuilder)localObject2).append(localTagView);
          ((StringBuilder)localObject2).append(", Child = ");
          localTagView = c;
          ((StringBuilder)localObject2).append(localTagView);
          localObject2 = ((StringBuilder)localObject2).toString();
          paramView[0] = localObject2;
          return;
        }
        localObject1 = c;
        long l5 = -1;
        long l6;
        if (localObject1 == paramView)
        {
          c = null;
          l6 = l5;
        }
        else
        {
          if (localObject1 != null) {
            ((TagView)localObject1).a(false, bool3);
          }
          c = paramView;
          l6 = paramView.getTagId();
        }
        bool1 = l6 < l5;
        if (bool1)
        {
          bool1 = true;
          f2 = Float.MIN_VALUE;
        }
        else
        {
          bool1 = false;
          f2 = 0.0F;
          localObject2 = null;
        }
        j = bool1;
        localObject2 = c;
        if (localObject2 == paramView) {
          bool2 = true;
        }
        paramView.a(bool2, bool3);
        boolean bool5 = j;
        if (bool5)
        {
          int i4 = 2;
          paramView = new float[i4];
          View tmp516_515 = paramView;
          tmp516_515[0] = 1.0F;
          tmp516_515[1] = 0.35F;
          paramView = ValueAnimator.ofFloat(paramView);
          paramView.setDuration(l1);
          localObject2 = new com/truecaller/tag/j$5;
          ((j.5)localObject2).<init>(this);
          paramView.addListener((Animator.AnimatorListener)localObject2);
          paramView.start();
        }
      }
    }
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = TrueApp.y().a().c();
    bc localbc = new com/truecaller/analytics/bc;
    localbc.<init>("tagPicker");
    paramBundle.a(localbc);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    return paramLayoutInflater.inflate(2131559230, paramViewGroup, false);
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    Object localObject1 = (ViewGroup)paramView.findViewById(2131364654);
    a = ((ViewGroup)localObject1);
    localObject1 = (ViewGroup)paramView.findViewById(2131364655);
    b = ((ViewGroup)localObject1);
    localObject1 = paramView.findViewById(2131364668);
    d = ((View)localObject1);
    localObject1 = paramView.findViewById(2131364669);
    e = ((View)localObject1);
    localObject1 = (TextView)paramView.findViewById(2131364657);
    f = ((TextView)localObject1);
    localObject1 = (TagView)paramView.findViewById(2131364658);
    g = ((TagView)localObject1);
    int i1 = getResources().getDimensionPixelSize(2131166220);
    float f1 = i1;
    k = f1;
    localObject1 = g;
    int i2 = 0;
    ((TagView)localObject1).a(true, false);
    localObject1 = getArguments();
    long l1 = Long.MIN_VALUE;
    long l2 = ((Bundle)localObject1).getLong("initial_tag", l1);
    View localView = null;
    boolean bool1 = l2 < l1;
    if (bool1)
    {
      localObject2 = d.a(l2);
    }
    else
    {
      i3 = 0;
      localObject2 = null;
    }
    h = ((com.truecaller.common.tag.c)localObject2);
    Object localObject2 = "tag_context";
    i1 = ((Bundle)localObject1).getInt((String)localObject2, 0);
    int i3 = 3;
    int i4 = 4;
    if ((i1 == i3) || (i1 == i4))
    {
      i1 = 2131364884;
      f1 = 1.8349618E38F;
      localObject1 = (TextView)paramView.findViewById(i1);
      localObject2 = "";
      ((TextView)localObject1).setText((CharSequence)localObject2);
    }
    localObject1 = h;
    if (localObject1 != null)
    {
      l2 = c;
      long l3 = 0L;
      boolean bool2 = l2 < l3;
      if (bool2)
      {
        l2 = c;
        localObject1 = d.a(l2);
      }
    }
    localObject2 = getLoaderManager();
    int i5 = 2131364984;
    ((w)localObject2).a(i5, null, this);
    if (localObject1 != null)
    {
      d.setVisibility(i4);
      localView = e;
      localView.setVisibility(0);
      long l4 = a;
      a(l4);
      localObject1 = f;
      i2 = 2131887274;
      ((TextView)localObject1).setText(i2);
    }
    else
    {
      localObject1 = f;
      i2 = 2131887273;
      ((TextView)localObject1).setText(i2);
    }
    g.setOnClickListener(this);
    paramView.findViewById(2131362510).setOnClickListener(this);
    super.onViewCreated(paramView, paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tag.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */