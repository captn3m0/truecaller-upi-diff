package com.truecaller.tag;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.truecaller.data.entity.Contact;
import com.truecaller.log.AssertionUtil.OnlyInDebug;

public class NameSuggestionActivity
  extends b
{
  public static Intent a(Context paramContext, Contact paramContact, String paramString)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, NameSuggestionActivity.class);
    localIntent.putExtra("contact", paramContact);
    localIntent.putExtra("source", paramString);
    return localIntent;
  }
  
  protected final b.a d()
  {
    Object localObject = getIntent();
    Contact localContact = (Contact)((Intent)localObject).getParcelableExtra("contact");
    String[] arrayOfString = null;
    boolean bool;
    if (localContact != null) {
      bool = true;
    } else {
      bool = false;
    }
    arrayOfString = new String[0];
    AssertionUtil.OnlyInDebug.isTrue(bool, arrayOfString);
    if (localContact == null)
    {
      finish();
      return null;
    }
    localObject = ((Intent)localObject).getStringExtra("source");
    return a.a(localContact, (String)localObject);
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    com.truecaller.utils.extensions.a.a(this);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tag.NameSuggestionActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */