package com.truecaller.tag;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;

public abstract class b$a
  extends Fragment
{
  abstract void a();
  
  public LayoutInflater onGetLayoutInflater(Bundle paramBundle)
  {
    paramBundle = super.onGetLayoutInflater(paramBundle);
    ContextThemeWrapper localContextThemeWrapper = new android/view/ContextThemeWrapper;
    f localf = getActivity();
    int i = aresId;
    localContextThemeWrapper.<init>(localf, i);
    return paramBundle.cloneInContext(localContextThemeWrapper);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tag.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */