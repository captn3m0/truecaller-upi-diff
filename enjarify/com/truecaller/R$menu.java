package com.truecaller;

public final class R$menu
{
  public static final int action_mode_call_log = 2131623936;
  public static final int action_mode_call_recording = 2131623937;
  public static final int action_mode_select_all = 2131623938;
  public static final int call_recording_item_menu = 2131623939;
  public static final int call_recording_list_menu = 2131623940;
  public static final int calls_list_menu = 2131623941;
  public static final int contacts_list_menu = 2131623942;
  public static final int conversation_action_mode = 2131623943;
  public static final int conversation_list_action_mode = 2131623944;
  public static final int conversation_list_menu = 2131623945;
  public static final int conversation_menu = 2131623946;
  public static final int details_call_log_menu = 2131623947;
  public static final int details_menu = 2131623948;
  public static final int drawer = 2131623949;
  public static final int edit_me_form_menu = 2131623950;
  public static final int edit_me_menu = 2131623951;
  public static final int features_panel = 2131623952;
  public static final int feedback_form_menu = 2131623953;
  public static final int flash_menu_top = 2131623954;
  public static final int history_menu_action_mode = 2131623955;
  public static final int im_group_info = 2131623956;
  public static final int im_group_participant = 2131623957;
  public static final int main_menu = 2131623958;
  public static final int menu_credit = 2131623959;
  public static final int menu_dashboard = 2131623960;
  public static final int menu_frag_banks = 2131623961;
  public static final int menu_frag_search_banks = 2131623962;
  public static final int menu_history = 2131623963;
  public static final int menu_incoming_header = 2131623964;
  public static final int menu_initial_offer = 2131623965;
  public static final int menu_operator_selection = 2131623966;
  public static final int menu_payments = 2131623967;
  public static final int menu_pending_collect = 2131623968;
  public static final int menu_settings = 2131623969;
  public static final int new_conversation = 2131623970;
  public static final int new_mms_group = 2131623971;
  public static final int notifications_menu = 2131623972;
  public static final int options_menu_all = 2131623973;
  public static final int options_menu_primary = 2131623974;
  public static final int options_menu_set_expiry = 2131623975;
  public static final int suggested_context_menu = 2131623976;
  public static final int suggested_premium_menu = 2131623977;
  public static final int theme_selector_menu = 2131623978;
  public static final int who_viewed_me_action_mode = 2131623979;
}

/* Location:
 * Qualified Name:     com.truecaller.R.menu
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */