package com.truecaller.aftercall;

import com.truecaller.common.b.e;

public enum PromotionType
{
  public final PromotionCategory category;
  private final String isDisabledFeatureFlagKey;
  public final String settingKey;
  
  static
  {
    Object localObject = new com/truecaller/aftercall/PromotionType;
    ((PromotionType)localObject).<init>("SIGN_UP", 0);
    SIGN_UP = (PromotionType)localObject;
    localObject = new com/truecaller/aftercall/PromotionType;
    PromotionCategory localPromotionCategory1 = PromotionCategory.PERMISSION;
    int i = 1;
    ((PromotionType)localObject).<init>("PHONE_PERMISSION", i, localPromotionCategory1);
    PHONE_PERMISSION = (PromotionType)localObject;
    localObject = new com/truecaller/aftercall/PromotionType;
    localPromotionCategory1 = PromotionCategory.PERMISSION;
    int j = 2;
    ((PromotionType)localObject).<init>("CONTACT_PERMISSION", j, localPromotionCategory1);
    CONTACT_PERMISSION = (PromotionType)localObject;
    localObject = new com/truecaller/aftercall/PromotionType;
    PromotionCategory localPromotionCategory2 = PromotionCategory.DIALER;
    ((PromotionType)localObject).<init>("DIALER_OUTGOING_OUTSIDE", 3, localPromotionCategory2, "outgoingOutside", "featureDisableOutgoingOutside");
    DIALER_OUTGOING_OUTSIDE = (PromotionType)localObject;
    localObject = new PromotionType[4];
    PromotionType localPromotionType = SIGN_UP;
    localObject[0] = localPromotionType;
    localPromotionType = PHONE_PERMISSION;
    localObject[i] = localPromotionType;
    localPromotionType = CONTACT_PERMISSION;
    localObject[j] = localPromotionType;
    localPromotionType = DIALER_OUTGOING_OUTSIDE;
    localObject[3] = localPromotionType;
    $VALUES = (PromotionType[])localObject;
  }
  
  private PromotionType()
  {
    this(localPromotionCategory);
  }
  
  private PromotionType(PromotionCategory paramPromotionCategory)
  {
    this(paramPromotionCategory, null, null);
  }
  
  private PromotionType(PromotionCategory paramPromotionCategory, String paramString2, String paramString3)
  {
    category = paramPromotionCategory;
    settingKey = paramString2;
    isDisabledFeatureFlagKey = paramString3;
  }
  
  public final boolean isEnabled()
  {
    String str = isDisabledFeatureFlagKey;
    if (str != null)
    {
      boolean bool = e.a(str, false);
      if (bool) {
        return false;
      }
    }
    return true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.aftercall.PromotionType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */