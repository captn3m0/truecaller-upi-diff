package com.truecaller.aftercall;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.b;
import android.support.v4.graphics.drawable.a;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import com.truecaller.utils.extensions.t;

public final class PromoBadgeView
  extends FrameLayout
{
  public PromoBadgeView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    paramContext = getContext();
    paramAttributeSet = this;
    paramAttributeSet = (ViewGroup)this;
    View.inflate(paramContext, 2131558428, paramAttributeSet);
  }
  
  public final void a(boolean paramBoolean)
  {
    boolean bool = t.d(this);
    if (bool == paramBoolean) {
      return;
    }
    t.a(this, paramBoolean);
    paramBoolean = t.d(this);
    if (paramBoolean)
    {
      Object localObject = getContext();
      int i = 2130771980;
      localObject = AnimationUtils.loadAnimation((Context)localObject, i);
      startAnimation((Animation)localObject);
    }
  }
  
  public final void setBorder(int paramInt)
  {
    Object localObject = getContext();
    int i = 2131230818;
    localObject = b.a((Context)localObject, i);
    if (localObject != null) {
      a.a((Drawable)localObject, paramInt);
    } else {
      localObject = null;
    }
    setBackground((Drawable)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.aftercall.PromoBadgeView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */