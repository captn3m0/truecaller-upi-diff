package com.truecaller.aftercall;

public enum PromotionCategory
{
  static
  {
    Object localObject = new com/truecaller/aftercall/PromotionCategory;
    ((PromotionCategory)localObject).<init>("NONE", 0);
    NONE = (PromotionCategory)localObject;
    localObject = new com/truecaller/aftercall/PromotionCategory;
    int i = 1;
    ((PromotionCategory)localObject).<init>("PERMISSION", i);
    PERMISSION = (PromotionCategory)localObject;
    localObject = new com/truecaller/aftercall/PromotionCategory;
    int j = 2;
    ((PromotionCategory)localObject).<init>("DIALER", j);
    DIALER = (PromotionCategory)localObject;
    localObject = new PromotionCategory[3];
    PromotionCategory localPromotionCategory = NONE;
    localObject[0] = localPromotionCategory;
    localPromotionCategory = PERMISSION;
    localObject[i] = localPromotionCategory;
    localPromotionCategory = DIALER;
    localObject[j] = localPromotionCategory;
    $VALUES = (PromotionCategory[])localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.aftercall.PromotionCategory
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */