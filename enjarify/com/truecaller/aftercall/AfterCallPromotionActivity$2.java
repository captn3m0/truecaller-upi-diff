package com.truecaller.aftercall;

import android.animation.ValueAnimator;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnPreDrawListener;

final class AfterCallPromotionActivity$2
  implements ViewTreeObserver.OnPreDrawListener
{
  AfterCallPromotionActivity$2(AfterCallPromotionActivity paramAfterCallPromotionActivity) {}
  
  public final boolean onPreDraw()
  {
    float f = -AfterCallPromotionActivity.a(a).getHeight();
    ValueAnimator localValueAnimator = AfterCallPromotionActivity.b(a);
    int i = 2;
    float[] arrayOfFloat1 = new float[i];
    arrayOfFloat1[0] = f;
    int j = 1;
    arrayOfFloat1[j] = 0.0F;
    localValueAnimator.setFloatValues(arrayOfFloat1);
    localValueAnimator = AfterCallPromotionActivity.c(a);
    float[] arrayOfFloat2 = new float[i];
    arrayOfFloat2[0] = 0.0F;
    arrayOfFloat2[j] = f;
    localValueAnimator.setFloatValues(arrayOfFloat2);
    AfterCallPromotionActivity.a(a).getViewTreeObserver().removeOnPreDrawListener(this);
    AfterCallPromotionActivity.d(a);
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.aftercall.AfterCallPromotionActivity.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */