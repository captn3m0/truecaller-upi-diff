package com.truecaller.aftercall;

import android.animation.Animator.AnimatorListener;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.widget.ImageView;
import android.widget.TextView;
import com.truecaller.TrueApp;
import com.truecaller.analytics.bc;
import com.truecaller.analytics.e.a;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.common.h.ac;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.notifications.RegistrationNudgeTask.TaskState;
import com.truecaller.notifications.aa;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;
import com.truecaller.ui.TruecallerInit;
import com.truecaller.ui.components.AvatarView;
import com.truecaller.ui.n;
import com.truecaller.util.at;
import com.truecaller.wizard.utils.i;

public class AfterCallPromotionActivity
  extends n
{
  private View a;
  private ValueAnimator b;
  private ValueAnimator c;
  private long d;
  private long i;
  private long j;
  private long k;
  private HistoryEvent l;
  private PendingIntent m;
  private BroadcastReceiver n;
  private com.truecaller.notificationchannels.e o;
  private com.truecaller.analytics.b p;
  
  public static void a(Context paramContext, com.truecaller.i.c paramc, PromotionType paramPromotionType, HistoryEvent paramHistoryEvent)
  {
    Object localObject1 = AfterCallPromotionActivity.5.a;
    int i1 = paramPromotionType.ordinal();
    int i2 = localObject1[i1];
    long l1;
    switch (i2)
    {
    default: 
      localObject1 = category;
      Object localObject2 = PromotionCategory.DIALER;
      if (localObject1 == localObject2)
      {
        l1 = System.currentTimeMillis();
        paramc.b("lastDialerPromotionTime", l1);
        paramc = new com/truecaller/analytics/e$a;
        paramc.<init>("DIALER_PROMO_showed");
        localObject2 = settingKey;
        paramc = paramc.a("DIALER_PROMO_name", (String)localObject2).a();
        localObject1 = TrueApp.y().a().c();
        ((com.truecaller.analytics.b)localObject1).a(paramc);
      }
      break;
    case 3: 
      localObject1 = "afterCallPromoteContactsPermissionTimestamp";
      l1 = System.currentTimeMillis();
      paramc.b((String)localObject1, l1);
      break;
    case 2: 
      localObject1 = "afterCallPromotePhonePermissionTimestamp";
      l1 = System.currentTimeMillis();
      paramc.b((String)localObject1, l1);
      break;
    case 1: 
      long l2 = System.currentTimeMillis();
      paramc.b("afterCallPromoteTcTimestamp", l2);
      paramc = TrueApp.y().a().aL();
      localObject1 = RegistrationNudgeTask.TaskState.DONE.toString();
      paramc.a(paramContext, 2131886641, 2131886640, (String)localObject1);
      return;
    }
    paramc = new android/content/Intent;
    paramc.<init>(paramContext, AfterCallPromotionActivity.class);
    paramc = paramc.addFlags(268533760).putExtra("promotionType", paramPromotionType).putExtra("historyEvent", paramHistoryEvent);
    paramContext.startActivity(paramc);
  }
  
  private void a(PromotionType paramPromotionType)
  {
    e.a locala = new com/truecaller/analytics/e$a;
    locala.<init>("DIALER_PROMO_dismissed");
    paramPromotionType = settingKey;
    paramPromotionType = locala.a("DIALER_PROMO_name", paramPromotionType).a();
    p.a(paramPromotionType);
  }
  
  public static boolean a(Context paramContext, PromotionType paramPromotionType)
  {
    Object localObject = ((bk)paramContext.getApplicationContext()).a();
    a locala = ((bp)localObject).Y();
    boolean bool = locala.a(paramPromotionType, null);
    if (bool)
    {
      localObject = ((bp)localObject).D();
      a(paramContext, (com.truecaller.i.c)localObject, paramPromotionType, null);
    }
    return bool;
  }
  
  private Intent b(PromotionType paramPromotionType)
  {
    Intent localIntent = TruecallerInit.a(this, "calls", "afterCall");
    paramPromotionType = settingKey;
    localIntent.putExtra("promotion_setting_key", paramPromotionType);
    return localIntent;
  }
  
  private void d()
  {
    ((NotificationManager)getApplicationContext().getSystemService("notification")).cancel(2131362836);
  }
  
  private void e()
  {
    ValueAnimator localValueAnimator1 = c;
    boolean bool1 = localValueAnimator1.isRunning();
    if (bool1) {
      return;
    }
    long l1 = i;
    long l2 = -1;
    boolean bool2 = l1 < l2;
    if (bool2)
    {
      localValueAnimator1 = b;
      bool1 = localValueAnimator1.isRunning();
      if (bool1)
      {
        localValueAnimator1 = c;
        l1 = localValueAnimator1.getDuration();
        ValueAnimator localValueAnimator2 = b;
        l2 = localValueAnimator2.getCurrentPlayTime();
        l1 -= l2;
        i = l1;
      }
      c.start();
      localValueAnimator1 = c;
      long l3 = i;
      localValueAnimator1.setCurrentPlayTime(l3);
      return;
    }
    finish();
  }
  
  public final boolean a()
  {
    return false;
  }
  
  public final boolean b()
  {
    e();
    return true;
  }
  
  public void onCreate(Bundle paramBundle)
  {
    AfterCallPromotionActivity localAfterCallPromotionActivity = this;
    Object localObject1 = paramBundle;
    super.onCreate(paramBundle);
    com.truecaller.utils.extensions.a.a(this);
    Object localObject2 = getTheme();
    int i1 = aresId;
    int i2 = 0;
    ((Resources.Theme)localObject2).applyStyle(i1, false);
    int i3 = 2131558434;
    setContentView(i3);
    localObject2 = ((bk)getApplicationContext()).a();
    Object localObject3 = ((bp)localObject2).aC();
    o = ((com.truecaller.notificationchannels.e)localObject3);
    localObject3 = ((bp)localObject2).c();
    p = ((com.truecaller.analytics.b)localObject3);
    localObject3 = (PromotionType)getIntent().getSerializableExtra("promotionType");
    Object localObject4 = new com/truecaller/aftercall/AfterCallPromotionActivity$1;
    ((AfterCallPromotionActivity.1)localObject4).<init>(this, (PromotionType)localObject3);
    n = ((BroadcastReceiver)localObject4);
    localObject4 = n;
    Object localObject5 = new android/content/IntentFilter;
    ((IntentFilter)localObject5).<init>("com.truecaller.promotion.DISMISS");
    registerReceiver((BroadcastReceiver)localObject4, (IntentFilter)localObject5);
    localObject4 = new android/content/Intent;
    ((Intent)localObject4).<init>("com.truecaller.promotion.DISMISS");
    localObject4 = PendingIntent.getBroadcast(this, 2131364122, (Intent)localObject4, 134217728);
    m = ((PendingIntent)localObject4);
    localObject4 = findViewById(2131362009);
    a = ((View)localObject4);
    int i5 = 2;
    localObject5 = new float[i5];
    Object tmp213_211 = localObject5;
    tmp213_211[0] = 0.0F;
    tmp213_211[1] = 1.0F;
    localObject5 = ValueAnimator.ofFloat((float[])localObject5);
    b = ((ValueAnimator)localObject5);
    localObject4 = new float[i5];
    Object tmp242_240 = localObject4;
    tmp242_240[0] = 0.0F;
    tmp242_240[1] = 1.0F;
    localObject4 = ValueAnimator.ofFloat((float[])localObject4);
    c = ((ValueAnimator)localObject4);
    localObject4 = getResources();
    int i6 = 17694720;
    i5 = ((Resources)localObject4).getInteger(i6);
    Object localObject6 = b;
    long l1 = i5;
    ((ValueAnimator)localObject6).setDuration(l1);
    c.setDuration(l1);
    localObject4 = b;
    localObject6 = new com/truecaller/aftercall/-$$Lambda$AfterCallPromotionActivity$A4fyzD7PsJjrpPhh0n1tqNk1mnU;
    ((-..Lambda.AfterCallPromotionActivity.A4fyzD7PsJjrpPhh0n1tqNk1mnU)localObject6).<init>(this);
    ((ValueAnimator)localObject4).addUpdateListener((ValueAnimator.AnimatorUpdateListener)localObject6);
    localObject4 = b;
    localObject6 = new com/truecaller/aftercall/AfterCallPromotionActivity$3;
    ((AfterCallPromotionActivity.3)localObject6).<init>(this);
    ((ValueAnimator)localObject4).addListener((Animator.AnimatorListener)localObject6);
    localObject4 = c;
    localObject6 = new com/truecaller/aftercall/-$$Lambda$AfterCallPromotionActivity$hRs0xhQGZQ5sdyloH4O0cjuXs80;
    ((-..Lambda.AfterCallPromotionActivity.hRs0xhQGZQ5sdyloH4O0cjuXs80)localObject6).<init>(this);
    ((ValueAnimator)localObject4).addUpdateListener((ValueAnimator.AnimatorUpdateListener)localObject6);
    localObject4 = c;
    localObject6 = new com/truecaller/aftercall/AfterCallPromotionActivity$4;
    ((AfterCallPromotionActivity.4)localObject6).<init>(this);
    ((ValueAnimator)localObject4).addListener((Animator.AnimatorListener)localObject6);
    localObject4 = a.getViewTreeObserver();
    localObject6 = new com/truecaller/aftercall/AfterCallPromotionActivity$2;
    ((AfterCallPromotionActivity.2)localObject6).<init>(this);
    ((ViewTreeObserver)localObject4).addOnPreDrawListener((ViewTreeObserver.OnPreDrawListener)localObject6);
    localObject4 = findViewById(16908290);
    localObject6 = new com/truecaller/aftercall/-$$Lambda$AfterCallPromotionActivity$_h58QA5iqzy6GeShD4s4Xo2FAo0;
    ((-..Lambda.AfterCallPromotionActivity._h58QA5iqzy6GeShD4s4Xo2FAo0)localObject6).<init>(this);
    ((View)localObject4).setOnClickListener((View.OnClickListener)localObject6);
    localObject4 = (ImageView)findViewById(2131364033);
    int i7 = com.truecaller.utils.ui.b.a(this, 2130969592);
    com.truecaller.utils.ui.b.a((ImageView)localObject4, i7);
    localObject6 = new com/truecaller/aftercall/-$$Lambda$AfterCallPromotionActivity$xIpkw-HCnPhAmk3wd4NlV7b4M9Y;
    ((-..Lambda.AfterCallPromotionActivity.xIpkw-HCnPhAmk3wd4NlV7b4M9Y)localObject6).<init>(this, (PromotionType)localObject3);
    ((ImageView)localObject4).setOnClickListener((View.OnClickListener)localObject6);
    i5 = 2131364031;
    localObject4 = findViewById(i5);
    localObject6 = new com/truecaller/aftercall/-$$Lambda$AfterCallPromotionActivity$qtC3joTV8QKPrkGvxq6pj6PRIng;
    ((-..Lambda.AfterCallPromotionActivity.qtC3joTV8QKPrkGvxq6pj6PRIng)localObject6).<init>(this, (PromotionType)localObject3);
    ((View)localObject4).setOnClickListener((View.OnClickListener)localObject6);
    long l2;
    if (paramBundle != null)
    {
      l2 = 0L;
      l1 = paramBundle.getLong("showPlayTime", l2);
      j = l1;
      d = l1;
      localObject4 = "hidePlayTime";
      long l3 = paramBundle.getLong((String)localObject4, l2);
      k = l3;
      i = l3;
    }
    else
    {
      localObject1 = getResources();
      i8 = ((Resources)localObject1).getInteger(i6);
      localObject4 = b;
      l2 = i8;
      ((ValueAnimator)localObject4).setStartDelay(l2);
    }
    localObject1 = localAfterCallPromotionActivity.findViewById(2131364035);
    Object localObject7 = localObject1;
    localObject7 = (ImageView)localObject1;
    localObject1 = localAfterCallPromotionActivity.findViewById(2131364036);
    Object localObject8 = localObject1;
    localObject8 = (TextView)localObject1;
    localObject1 = localAfterCallPromotionActivity.findViewById(2131364037);
    Object localObject9 = localObject1;
    localObject9 = (TextView)localObject1;
    localObject1 = localAfterCallPromotionActivity.findViewById(2131364032);
    Object localObject10 = localObject1;
    localObject10 = (TextView)localObject1;
    int i8 = 2131886990;
    float f = 1.9408574E38F;
    localObject1 = localAfterCallPromotionActivity.getString(i8);
    localObject4 = "";
    i6 = 2131886989;
    localObject5 = localAfterCallPromotionActivity.getString(i6);
    localObject6 = getIntent();
    Object localObject11 = "historyEvent";
    localObject6 = (HistoryEvent)((Intent)localObject6).getParcelableExtra((String)localObject11);
    l = ((HistoryEvent)localObject6);
    localObject6 = AfterCallPromotionActivity.5.a;
    int i10 = ((PromotionType)localObject3).ordinal();
    i7 = localObject6[i10];
    i10 = 2131689472;
    int i11 = 1;
    Object localObject12;
    Object localObject13;
    Object localObject14;
    switch (i7)
    {
    default: 
      localObject2 = "unknown";
      localObject12 = localObject1;
      localObject13 = localObject2;
      localObject14 = localObject4;
      localObject11 = localObject5;
      i7 = 0;
      localObject6 = null;
      break;
    case 4: 
      i3 = 2131886988;
      localObject4 = new Object[i11];
      localObject6 = l;
      if (localObject6 != null)
      {
        localObject11 = f;
        if (localObject11 != null)
        {
          localObject6 = f.t();
          break label909;
        }
      }
      localObject6 = "";
      localObject4[0] = localObject6;
      localObject2 = localAfterCallPromotionActivity.getString(i3, (Object[])localObject4);
      localObject4 = "dialFromTc";
      localObject12 = localObject1;
      localObject14 = localObject2;
      localObject13 = localObject4;
      localObject11 = localObject5;
      i7 = 0;
      localObject6 = null;
      break;
    case 2: 
    case 3: 
      localObject1 = PromotionType.CONTACT_PERMISSION;
      if (localObject3 == localObject1)
      {
        i8 = 2131689473;
        f = 1.9007962E38F;
      }
      else
      {
        i8 = 2131689472;
        f = 1.900796E38F;
      }
      i5 = 2131886815;
      localObject4 = localAfterCallPromotionActivity.getString(i5);
      localObject2 = ((bp)localObject2).aI();
      boolean bool1 = ((com.truecaller.common.h.c)localObject2).c();
      if (bool1)
      {
        localObject2 = ((com.truecaller.common.b.a)getApplicationContext()).u().n();
        bool1 = ((ac)localObject2).a();
        if (!bool1)
        {
          bool1 = true;
          break label1066;
        }
      }
      bool1 = false;
      localObject2 = null;
      if (bool1) {
        i4 = 2131886812;
      } else {
        i4 = 2131886813;
      }
      localObject2 = localAfterCallPromotionActivity.getString(i4);
      i6 = 2131886814;
      localObject5 = localAfterCallPromotionActivity.getString(i6);
      localObject6 = PromotionType.CONTACT_PERMISSION;
      if (localObject3 == localObject6) {
        localObject6 = "contactPermission";
      } else {
        localObject6 = "phonePermission";
      }
      localObject14 = localObject2;
      localObject12 = localObject4;
      localObject11 = localObject5;
      localObject13 = localObject6;
      i7 = i8;
      break;
    case 1: 
      label909:
      label1066:
      i8 = 2131886345;
      f = 1.9407266E38F;
      localObject1 = localAfterCallPromotionActivity.getString(i8);
      i4 = 2131886346;
      localObject2 = localAfterCallPromotionActivity.getString(i4);
      i5 = 2131888374;
      localObject4 = localAfterCallPromotionActivity.getString(i5);
      localObject5 = "signIn";
      localObject12 = localObject1;
      localObject14 = localObject2;
      localObject11 = localObject4;
      localObject13 = localObject5;
      i7 = 2131689472;
    }
    localObject1 = AfterCallPromotionActivity.5.a;
    int i4 = ((PromotionType)localObject3).ordinal();
    i8 = localObject1[i4];
    i6 = 4;
    Object localObject15;
    if (i8 != i6)
    {
      i8 = 0;
      f = 0.0F;
      localObject1 = null;
      localObject15 = null;
    }
    else
    {
      localObject1 = localAfterCallPromotionActivity.b((PromotionType)localObject3);
      localObject15 = localObject1;
    }
    Object localObject17;
    int i12;
    Object localObject18;
    if (localObject15 != null)
    {
      localObject4 = new java/lang/Thread;
      localObject1 = new com/truecaller/aftercall/-$$Lambda$AfterCallPromotionActivity$abLtIlrayR7Xd3Z7zxbwL1Kv72c;
      localObject2 = this;
      localObject3 = localObject12;
      Object localObject16 = localObject4;
      localObject4 = localObject14;
      localObject17 = localObject10;
      localObject5 = localObject15;
      i12 = i7;
      localObject6 = localObject11;
      localObject18 = localObject11;
      localObject11 = localObject13;
      ((-..Lambda.AfterCallPromotionActivity.abLtIlrayR7Xd3Z7zxbwL1Kv72c)localObject1).<init>(this, (String)localObject12, (String)localObject14, (Intent)localObject15, (String)localObject6, (String)localObject13);
      ((Thread)localObject16).<init>((Runnable)localObject1);
      i8 = 1;
      f = Float.MIN_VALUE;
      ((Thread)localObject16).setPriority(i8);
      ((Thread)localObject16).start();
    }
    else
    {
      localObject18 = localObject11;
      localObject17 = localObject10;
      i12 = i7;
    }
    if ((i12 != 0) && (localObject7 != null))
    {
      ((ImageView)localObject7).setImageResource(i12);
    }
    else
    {
      localObject1 = l;
      if (localObject1 != null)
      {
        localObject1 = f;
        if (localObject1 != null)
        {
          localObject1 = l.f;
          i4 = 2131362075;
          localObject2 = (AvatarView)localAfterCallPromotionActivity.findViewById(i4);
          localObject3 = new com/truecaller/ui/components/b;
          i5 = 1;
          localObject4 = ((Contact)localObject1).a(i5);
          i6 = 0;
          localObject5 = null;
          localObject6 = ((Contact)localObject1).a(false);
          boolean bool3 = ((Contact)localObject1).a(32);
          i2 = 4;
          boolean bool2 = ((Contact)localObject1).a(i2);
          ((com.truecaller.ui.components.b)localObject3).<init>((Uri)localObject4, (Uri)localObject6, bool3, bool2);
          ((AvatarView)localObject2).a((com.truecaller.ui.components.b)localObject3);
          ((AvatarView)localObject2).setVisibility(0);
          int i9 = 2131362084;
          f = 1.8343939E38F;
          localObject1 = localAfterCallPromotionActivity.findViewById(i9);
          ((View)localObject1).setVisibility(0);
        }
      }
    }
    at.b((TextView)localObject8, (CharSequence)localObject12);
    at.b((TextView)localObject9, (CharSequence)localObject14);
    localObject1 = localObject17;
    localObject5 = localObject18;
    at.b((TextView)localObject17, (CharSequence)localObject18);
    localObject1 = p;
    localObject2 = new com/truecaller/analytics/bc;
    ((bc)localObject2).<init>("afterCallPromotion");
    ((com.truecaller.analytics.b)localObject1).a((com.truecaller.analytics.e)localObject2);
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    Object localObject = n;
    if (localObject != null) {
      unregisterReceiver((BroadcastReceiver)localObject);
    }
    localObject = m;
    if (localObject != null)
    {
      localObject = (AlarmManager)getSystemService("alarm");
      PendingIntent localPendingIntent = m;
      ((AlarmManager)localObject).cancel(localPendingIntent);
    }
  }
  
  public void onPause()
  {
    super.onPause();
    overridePendingTransition(0, 0);
  }
  
  public void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    i.a(paramArrayOfString, paramArrayOfInt);
  }
  
  public void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    long l1 = j;
    paramBundle.putLong("showPlayTime", l1);
    l1 = k;
    paramBundle.putLong("hidePlayTime", l1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.aftercall.AfterCallPromotionActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */