package com.truecaller.aftercall;

import android.text.TextUtils;
import com.google.c.a.k.d;
import com.truecaller.callerid.i;
import com.truecaller.common.h.ab;
import com.truecaller.common.h.an;
import com.truecaller.common.h.j;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.data.entity.Number;
import com.truecaller.i.c;
import com.truecaller.util.al;
import com.truecaller.util.cn;
import java.util.Calendar;

public final class b
  implements a
{
  private final cn a;
  private final c b;
  private final al c;
  private final an d;
  
  public b(cn paramcn, c paramc, al paramal, com.truecaller.utils.a parama)
  {
    a = paramcn;
    b = paramc;
    c = paramal;
    paramcn = new com/truecaller/common/h/an;
    paramcn.<init>(parama);
    d = paramcn;
  }
  
  private boolean a(PromotionCategory paramPromotionCategory)
  {
    PromotionCategory localPromotionCategory = PromotionCategory.DIALER;
    if (paramPromotionCategory == localPromotionCategory)
    {
      paramPromotionCategory = b;
      boolean bool = paramPromotionCategory.b("hasNativeDialerCallerId");
      localPromotionCategory = null;
      if (bool) {
        return false;
      }
      paramPromotionCategory = b;
      String str = "lastCallMadeWithTcTime";
      long l1 = 0L;
      long l2 = paramPromotionCategory.a(str, l1);
      long l3 = 604800000L;
      bool = j.a(l2, l3);
      if (!bool) {
        return false;
      }
      paramPromotionCategory = b;
      str = "lastDialerPromotionTime";
      long l4 = paramPromotionCategory.a(str, l1);
      long l5 = 86400000L;
      bool = j.a(l4, l5);
      if (!bool) {
        return false;
      }
    }
    return true;
  }
  
  public final PromotionType a(i parami, HistoryEvent paramHistoryEvent)
  {
    Object localObject = PromotionCategory.DIALER;
    boolean bool1 = a((PromotionCategory)localObject);
    if (!bool1) {
      return null;
    }
    if (paramHistoryEvent != null)
    {
      localObject = f;
      if (localObject != null)
      {
        localObject = f.t();
        bool1 = TextUtils.isEmpty((CharSequence)localObject);
        if (!bool1)
        {
          localObject = a.m();
          int i = ab.a((k.d)localObject);
          int j = 2;
          if (i != j) {
            return null;
          }
          boolean bool2 = e;
          if (!bool2)
          {
            bool3 = f;
            if (!bool3)
            {
              parami = PromotionType.DIALER_OUTGOING_OUTSIDE;
              break label110;
            }
          }
        }
      }
    }
    boolean bool3 = false;
    parami = null;
    label110:
    if (parami != null)
    {
      boolean bool4 = a(parami, paramHistoryEvent);
      if (bool4) {
        return parami;
      }
    }
    return null;
  }
  
  public final boolean a(PromotionType paramPromotionType, HistoryEvent paramHistoryEvent)
  {
    Object localObject = a;
    boolean bool1 = ((cn)localObject).a();
    if (!bool1)
    {
      bool1 = paramPromotionType.isEnabled();
      if (bool1)
      {
        localObject = PromotionType.SIGN_UP;
        boolean bool2 = true;
        long l1 = 0L;
        if (paramPromotionType == localObject)
        {
          paramPromotionType = b;
          paramHistoryEvent = "afterCallPromoteTcTimestamp";
          long l2 = paramPromotionType.a(paramHistoryEvent, l1);
          localObject = d;
          al localal = c;
          l1 = localal.f();
          long l3 = 864000000L;
          bool1 = ((an)localObject).a(l1, l3);
          if (bool1)
          {
            localObject = d;
            l1 = 604800000L;
            boolean bool3 = ((an)localObject).a(l2, l1);
            if (bool3) {
              return bool2;
            }
          }
          return false;
        }
        localObject = category;
        PromotionCategory localPromotionCategory = PromotionCategory.PERMISSION;
        if (localObject == localPromotionCategory)
        {
          paramHistoryEvent = b;
          localObject = "afterCallPromotePhonePermissionTimestamp";
          long l4 = paramHistoryEvent.a((String)localObject, l1);
          paramHistoryEvent = b;
          String str = "afterCallPromoteContactsPermissionTimestamp";
          long l5 = paramHistoryEvent.a(str, l1);
          paramHistoryEvent = PromotionType.PHONE_PERMISSION;
          if (paramPromotionType != paramHistoryEvent) {
            l4 = l5;
          }
          return j.a(l4, 86400000L);
        }
        paramPromotionType = category;
        localObject = PromotionCategory.DIALER;
        if (paramPromotionType == localObject)
        {
          paramPromotionType = Calendar.getInstance();
          int i = 7;
          int j = paramPromotionType.get(i);
          if ((j == i) && (paramHistoryEvent != null))
          {
            paramPromotionType = f;
            if (paramPromotionType != null)
            {
              paramPromotionType = PromotionCategory.DIALER;
              boolean bool4 = a(paramPromotionType);
              if (bool4) {
                return bool2;
              }
            }
          }
          return false;
        }
        return false;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.aftercall.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */