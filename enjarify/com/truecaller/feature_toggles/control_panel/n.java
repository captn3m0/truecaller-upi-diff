package com.truecaller.feature_toggles.control_panel;

import dagger.a.d;
import javax.inject.Provider;

public final class n
  implements d
{
  private final i a;
  private final Provider b;
  
  private n(i parami, Provider paramProvider)
  {
    a = parami;
    b = paramProvider;
  }
  
  public static n a(i parami, Provider paramProvider)
  {
    n localn = new com/truecaller/feature_toggles/control_panel/n;
    localn.<init>(parami, paramProvider);
    return localn;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.feature_toggles.control_panel.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */