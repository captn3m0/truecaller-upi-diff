package com.truecaller.feature_toggles.control_panel;

import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.truecaller.b;

public final class u
  extends RecyclerView.Adapter
{
  private final b a;
  
  public u(b paramb)
  {
    a = paramb;
  }
  
  private static View a(ViewGroup paramViewGroup, int paramInt)
  {
    return LayoutInflater.from(paramViewGroup.getContext()).inflate(paramInt, paramViewGroup, false);
  }
  
  public final int getItemCount()
  {
    return a.a();
  }
  
  public final long getItemId(int paramInt)
  {
    return a.b(paramInt);
  }
  
  public final int getItemViewType(int paramInt)
  {
    return a.a(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.feature_toggles.control_panel.u
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */