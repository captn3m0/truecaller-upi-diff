package com.truecaller.feature_toggles.control_panel;

import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import c.g.a.b;

final class y$a
  implements CompoundButton.OnCheckedChangeListener
{
  y$a(b paramb) {}
  
  public final void onCheckedChanged(CompoundButton paramCompoundButton, boolean paramBoolean)
  {
    paramCompoundButton = a;
    Boolean localBoolean = Boolean.valueOf(paramBoolean);
    paramCompoundButton.invoke(localBoolean);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.feature_toggles.control_panel.y.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */