package com.truecaller.feature_toggles.control_panel;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.widget.EditText;
import android.widget.Toast;
import c.g.b.k;
import c.u;

final class FeaturesControlPanelActivity$a
  implements DialogInterface.OnClickListener
{
  FeaturesControlPanelActivity$a(FeaturesControlPanelActivity paramFeaturesControlPanelActivity, String paramString) {}
  
  public final void onClick(DialogInterface paramDialogInterface, int paramInt)
  {
    if (paramDialogInterface != null)
    {
      paramDialogInterface = (Dialog)paramDialogInterface;
      paramInt = 2131363087;
      paramDialogInterface = paramDialogInterface.findViewById(paramInt);
      k.a(paramDialogInterface, "(dialogInterface as Dial…firebase_dialog_edittext)");
      paramDialogInterface = ((EditText)paramDialogInterface).getText().toString();
      Object localObject = a.a;
      if (localObject == null)
      {
        str = "presenter";
        k.a(str);
      }
      String str = b;
      ((r)localObject).b(str, paramDialogInterface);
      paramDialogInterface = (Context)a;
      localObject = (CharSequence)"String changed";
      Toast.makeText(paramDialogInterface, (CharSequence)localObject, 1).show();
      return;
    }
    paramDialogInterface = new c/u;
    paramDialogInterface.<init>("null cannot be cast to non-null type android.app.Dialog");
    throw paramDialogInterface;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.feature_toggles.control_panel.FeaturesControlPanelActivity.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */