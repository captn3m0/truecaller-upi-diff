package com.truecaller.feature_toggles.control_panel;

import dagger.a.d;
import javax.inject.Provider;

public final class k
  implements d
{
  private final i a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  
  private k(i parami, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = parami;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
  }
  
  public static k a(i parami, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    k localk = new com/truecaller/feature_toggles/control_panel/k;
    localk.<init>(parami, paramProvider1, paramProvider2, paramProvider3);
    return localk;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.feature_toggles.control_panel.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */