package com.truecaller.feature_toggles.control_panel;

import dagger.a.d;
import javax.inject.Provider;

public final class m
  implements d
{
  private final i a;
  private final Provider b;
  
  private m(i parami, Provider paramProvider)
  {
    a = parami;
    b = paramProvider;
  }
  
  public static m a(i parami, Provider paramProvider)
  {
    m localm = new com/truecaller/feature_toggles/control_panel/m;
    localm.<init>(parami, paramProvider);
    return localm;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.feature_toggles.control_panel.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */