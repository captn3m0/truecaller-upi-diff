package com.truecaller.feature_toggles.control_panel;

import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import c.g.b.k;
import c.u;
import com.truecaller.TrueApp;
import com.truecaller.b;
import com.truecaller.bp;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;
import com.truecaller.ui.TruecallerInit;

public final class FeaturesControlPanelActivity
  extends AppCompatActivity
  implements r.a
{
  public r a;
  public f b;
  
  public final void a()
  {
    onBackPressed();
  }
  
  public final void a(String paramString1, String paramString2)
  {
    k.b(paramString1, "taskKey");
    k.b(paramString2, "firebaseString");
    Object localObject1 = this;
    localObject1 = (Context)this;
    View localView = View.inflate((Context)localObject1, 2131558619, null);
    k.a(localView, "View.inflate(this, R.layout.firebase_dialog, null)");
    Object localObject2 = localView.findViewById(2131363087);
    k.a(localObject2, "view.findViewById<EditTe…firebase_dialog_edittext)");
    localObject2 = (EditText)localObject2;
    paramString2 = (CharSequence)paramString2;
    ((EditText)localObject2).setHint(paramString2);
    paramString2 = new android/support/v7/app/AlertDialog$Builder;
    paramString2.<init>((Context)localObject1);
    localObject1 = (CharSequence)"Enter new value";
    paramString2 = paramString2.setTitle((CharSequence)localObject1);
    localObject1 = new com/truecaller/feature_toggles/control_panel/FeaturesControlPanelActivity$a;
    ((FeaturesControlPanelActivity.a)localObject1).<init>(this, paramString1);
    localObject1 = (DialogInterface.OnClickListener)localObject1;
    paramString1 = paramString2.setPositiveButton(2131887217, (DialogInterface.OnClickListener)localObject1);
    paramString2 = new com/truecaller/feature_toggles/control_panel/FeaturesControlPanelActivity$b;
    paramString2.<init>(this);
    paramString2 = (DialogInterface.OnClickListener)paramString2;
    paramString1.setNegativeButton(2131887197, paramString2).setView(localView).show();
  }
  
  public final void b()
  {
    Intent localIntent = new android/content/Intent;
    Object localObject = this;
    localObject = (Context)this;
    localIntent.<init>((Context)localObject, TruecallerInit.class);
    localIntent = localIntent.addFlags(335577088);
    startActivity(localIntent);
    System.exit(0);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    Object localObject1 = ThemeManager.a();
    int i = resId;
    setTheme(i);
    super.onCreate(paramBundle);
    paramBundle = getApplication();
    if (paramBundle != null)
    {
      paramBundle = ((TrueApp)paramBundle).a();
      localObject1 = new com/truecaller/feature_toggles/control_panel/i;
      ((i)localObject1).<init>();
      paramBundle.a((i)localObject1).a(this);
      int j = 2131558448;
      setContentView(j);
      paramBundle = a;
      if (paramBundle == null)
      {
        localObject1 = "presenter";
        k.a((String)localObject1);
      }
      localObject1 = new com/truecaller/feature_toggles/control_panel/w;
      Object localObject2 = a;
      if (localObject2 == null)
      {
        localObject3 = "presenter";
        k.a((String)localObject3);
      }
      localObject2 = (v.a)localObject2;
      int k = 16908290;
      Object localObject3 = findViewById(k);
      k.a(localObject3, "getContainerView()");
      Object localObject4 = b;
      if (localObject4 == null)
      {
        String str = "adapterPresenter";
        k.a(str);
      }
      localObject4 = (b)localObject4;
      ((w)localObject1).<init>((v.a)localObject2, (View)localObject3, (b)localObject4);
      paramBundle.a(localObject1);
      paramBundle = a;
      if (paramBundle == null)
      {
        localObject1 = "presenter";
        k.a((String)localObject1);
      }
      localObject1 = this;
      localObject1 = (r.a)this;
      paramBundle.a((r.a)localObject1);
      return;
    }
    paramBundle = new c/u;
    paramBundle.<init>("null cannot be cast to non-null type com.truecaller.TrueApp");
    throw paramBundle;
  }
  
  public final void onDestroy()
  {
    r localr = a;
    if (localr == null)
    {
      String str = "presenter";
      k.a(str);
    }
    localr.y_();
    super.onDestroy();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.feature_toggles.control_panel.FeaturesControlPanelActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */