package com.truecaller.feature_toggles.control_panel;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import c.g.a.b;
import c.g.b.k;

public final class y
  extends a
  implements x
{
  private final TextView a;
  private final CompoundButton b;
  private final View c;
  
  public y(View paramView)
  {
    super(paramView);
    Object localObject = paramView.findViewById(2131363085);
    k.a(localObject, "view.findViewById(R.id.f…feature_item_description)");
    localObject = (TextView)localObject;
    a = ((TextView)localObject);
    localObject = paramView.findViewById(2131363086);
    k.a(localObject, "view.findViewById(R.id.f…lean_feature_item_toggle)");
    localObject = (CompoundButton)localObject;
    b = ((CompoundButton)localObject);
    paramView = paramView.findViewById(2131363084);
    k.a(paramView, "view.findViewById(R.id.f…oolean_feature_container)");
    c = paramView;
    paramView = c;
    localObject = new com/truecaller/feature_toggles/control_panel/y$1;
    ((y.1)localObject).<init>(this);
    localObject = (View.OnClickListener)localObject;
    paramView.setOnClickListener((View.OnClickListener)localObject);
  }
  
  public final void a(b paramb)
  {
    k.b(paramb, "listener");
    CompoundButton localCompoundButton = b;
    Object localObject = new com/truecaller/feature_toggles/control_panel/y$a;
    ((y.a)localObject).<init>(paramb);
    localObject = (CompoundButton.OnCheckedChangeListener)localObject;
    localCompoundButton.setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)localObject);
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "text");
    CompoundButton localCompoundButton = b;
    paramString = (CharSequence)paramString;
    localCompoundButton.setText(paramString);
  }
  
  public final void b(boolean paramBoolean)
  {
    b.setChecked(paramBoolean);
  }
  
  public final void c()
  {
    super.c();
    b.setOnCheckedChangeListener(null);
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "text");
    TextView localTextView = a;
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.feature_toggles.control_panel.y
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */