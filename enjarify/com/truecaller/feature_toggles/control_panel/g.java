package com.truecaller.feature_toggles.control_panel;

import c.a.y;
import c.g.b.k;
import dagger.a;
import java.util.List;

public final class g
  implements f
{
  final a a;
  private List b;
  
  public g(a parama)
  {
    a = parama;
    parama = (List)y.a;
    b = parama;
  }
  
  public final int a()
  {
    return b.size();
  }
  
  public final int a(int paramInt)
  {
    List localList = b;
    d locald = (d)localList.get(paramInt);
    boolean bool = locald instanceof d.d;
    if (bool) {
      return 2131559085;
    }
    bool = locald instanceof d.c;
    if (bool) {
      return 2131558620;
    }
    paramInt = locald instanceof d.b;
    if (paramInt != 0) {
      return 2131558618;
    }
    return 2131558617;
  }
  
  public final void a(List paramList)
  {
    k.b(paramList, "<set-?>");
    b = paramList;
  }
  
  public final long b(int paramInt)
  {
    return paramInt;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.feature_toggles.control_panel.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */