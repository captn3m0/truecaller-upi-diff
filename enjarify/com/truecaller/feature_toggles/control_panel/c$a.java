package com.truecaller.feature_toggles.control_panel;

import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import c.g.a.b;

final class c$a
  implements CompoundButton.OnCheckedChangeListener
{
  c$a(b paramb) {}
  
  public final void onCheckedChanged(CompoundButton paramCompoundButton, boolean paramBoolean)
  {
    paramCompoundButton = a;
    Boolean localBoolean = Boolean.valueOf(paramBoolean);
    paramCompoundButton.invoke(localBoolean);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.feature_toggles.control_panel.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */