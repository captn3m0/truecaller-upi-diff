package com.truecaller.feature_toggles.control_panel;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import c.g.b.k;

public final class aa
  extends a
  implements z
{
  private final TextView a;
  private final TextView b;
  private final TextView c;
  private final TextView d;
  
  public aa(View paramView)
  {
    super(paramView);
    Object localObject = paramView.findViewById(2131363091);
    k.a(localObject, "view.findViewById(R.id.f…feature_item_description)");
    localObject = (TextView)localObject;
    a = ((TextView)localObject);
    localObject = paramView.findViewById(2131363092);
    k.a(localObject, "view.findViewById(R.id.f…string_feature_item_info)");
    localObject = (TextView)localObject;
    b = ((TextView)localObject);
    localObject = paramView.findViewById(2131363090);
    k.a(localObject, "view.findViewById(R.id.f…base_string_feature_item)");
    localObject = (TextView)localObject;
    c = ((TextView)localObject);
    paramView = paramView.findViewById(2131363088);
    k.a(paramView, "view.findViewById(R.id.f…se_string_feature_button)");
    paramView = (TextView)paramView;
    d = paramView;
  }
  
  public final void a(c.g.a.a parama)
  {
    k.b(parama, "listener");
    TextView localTextView = d;
    Object localObject = new com/truecaller/feature_toggles/control_panel/aa$a;
    ((aa.a)localObject).<init>(parama);
    localObject = (View.OnClickListener)localObject;
    localTextView.setOnClickListener((View.OnClickListener)localObject);
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "text");
    TextView localTextView = c;
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "text");
    TextView localTextView = a;
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void d(String paramString)
  {
    k.b(paramString, "text");
    TextView localTextView = b;
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.feature_toggles.control_panel.aa
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */