package com.truecaller.feature_toggles.control_panel;

import dagger.a.d;
import javax.inject.Provider;

public final class j
  implements d
{
  private final i a;
  private final Provider b;
  
  private j(i parami, Provider paramProvider)
  {
    a = parami;
    b = paramProvider;
  }
  
  public static j a(i parami, Provider paramProvider)
  {
    j localj = new com/truecaller/feature_toggles/control_panel/j;
    localj.<init>(parami, paramProvider);
    return localj;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.feature_toggles.control_panel.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */