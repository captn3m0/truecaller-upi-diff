package com.truecaller.feature_toggles.control_panel;

import dagger.a.d;
import javax.inject.Provider;

public final class o
  implements d
{
  private final i a;
  private final Provider b;
  
  private o(i parami, Provider paramProvider)
  {
    a = parami;
    b = paramProvider;
  }
  
  public static o a(i parami, Provider paramProvider)
  {
    o localo = new com/truecaller/feature_toggles/control_panel/o;
    localo.<init>(parami, paramProvider);
    return localo;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.feature_toggles.control_panel.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */