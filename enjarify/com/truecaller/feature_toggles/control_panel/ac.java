package com.truecaller.feature_toggles.control_panel;

import android.view.View;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import c.g.a.b;
import c.g.b.k;

public final class ac
  extends a
  implements ab
{
  private final TextView a;
  private final CompoundButton b;
  private final CompoundButton c;
  private final TextView d;
  
  public ac(View paramView)
  {
    super(paramView);
    Object localObject = paramView.findViewById(2131363049);
    k.a(localObject, "view.findViewById(R.id.feature_item_description)");
    localObject = (TextView)localObject;
    a = ((TextView)localObject);
    localObject = paramView.findViewById(2131363052);
    k.a(localObject, "view.findViewById(R.id.feature_item_toggle_local)");
    localObject = (CompoundButton)localObject;
    b = ((CompoundButton)localObject);
    localObject = paramView.findViewById(2131363053);
    k.a(localObject, "view.findViewById(R.id.feature_item_toggle_remote)");
    localObject = (CompoundButton)localObject;
    c = ((CompoundButton)localObject);
    paramView = paramView.findViewById(2131363050);
    k.a(paramView, "view.findViewById(R.id.feature_item_task_id)");
    paramView = (TextView)paramView;
    d = paramView;
  }
  
  public final void a(b paramb)
  {
    k.b(paramb, "listener");
    CompoundButton localCompoundButton = b;
    Object localObject = new com/truecaller/feature_toggles/control_panel/ac$a;
    ((ac.a)localObject).<init>(paramb);
    localObject = (CompoundButton.OnCheckedChangeListener)localObject;
    localCompoundButton.setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)localObject);
  }
  
  public final void b(b paramb)
  {
    k.b(paramb, "listener");
    CompoundButton localCompoundButton = c;
    Object localObject = new com/truecaller/feature_toggles/control_panel/ac$b;
    ((ac.b)localObject).<init>(paramb);
    localObject = (CompoundButton.OnCheckedChangeListener)localObject;
    localCompoundButton.setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)localObject);
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "text");
    TextView localTextView = d;
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void b(boolean paramBoolean)
  {
    b.setChecked(paramBoolean);
  }
  
  public final void c()
  {
    super.c();
    b.setOnCheckedChangeListener(null);
    c.setOnCheckedChangeListener(null);
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "text");
    TextView localTextView = a;
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void c(boolean paramBoolean)
  {
    c.setChecked(paramBoolean);
  }
  
  public final void d(boolean paramBoolean)
  {
    c.setEnabled(paramBoolean);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.feature_toggles.control_panel.ac
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */