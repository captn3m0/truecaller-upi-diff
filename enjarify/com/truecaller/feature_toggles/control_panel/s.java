package com.truecaller.feature_toggles.control_panel;

import c.u;
import com.truecaller.bb;
import com.truecaller.featuretoggles.FirebaseFlavor;
import com.truecaller.featuretoggles.b;
import com.truecaller.featuretoggles.c;
import com.truecaller.featuretoggles.h;
import com.truecaller.featuretoggles.j;
import com.truecaller.featuretoggles.l;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public final class s
  extends bb
  implements r
{
  private r.a a;
  private final f c;
  private final j d;
  private final com.truecaller.feature_toggles.control_panel.a.d e;
  
  public s(f paramf, j paramj, com.truecaller.feature_toggles.control_panel.a.d paramd)
  {
    c = paramf;
    d = paramj;
    e = paramd;
  }
  
  private final void b(String paramString)
  {
    s locals = this;
    Object localObject1 = c;
    Object localObject2 = (Iterable)d.a();
    Object localObject3 = new java/util/ArrayList;
    ((ArrayList)localObject3).<init>();
    localObject3 = (Collection)localObject3;
    localObject2 = ((Iterable)localObject2).iterator();
    int j;
    Object localObject7;
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject2).hasNext();
      j = 1;
      if (!bool1) {
        break;
      }
      localObject4 = ((Iterator)localObject2).next();
      localObject5 = localObject4;
      localObject5 = (b)localObject4;
      if (paramString != null)
      {
        Object localObject6 = ((b)localObject5).c();
        localObject5 = Locale.ENGLISH;
        localObject7 = "Locale.ENGLISH";
        c.g.b.k.a(localObject5, (String)localObject7);
        if (localObject6 != null)
        {
          localObject6 = ((String)localObject6).toLowerCase((Locale)localObject5);
          c.g.b.k.a(localObject6, "(this as java.lang.String).toLowerCase(locale)");
          localObject6 = (CharSequence)localObject6;
          localObject5 = paramString;
          localObject5 = (CharSequence)paramString;
          j = c.n.m.b((CharSequence)localObject6, (CharSequence)localObject5);
        }
        else
        {
          localObject1 = new c/u;
          ((u)localObject1).<init>("null cannot be cast to non-null type java.lang.String");
          throw ((Throwable)localObject1);
        }
      }
      if (j != 0) {
        ((Collection)localObject3).add(localObject4);
      }
    }
    localObject3 = (Iterable)localObject3;
    localObject2 = new com/truecaller/feature_toggles/control_panel/s$a;
    ((s.a)localObject2).<init>();
    localObject2 = (Comparator)localObject2;
    localObject2 = (Iterable)c.a.m.a((Iterable)localObject3, (Comparator)localObject2);
    localObject3 = new com/truecaller/featuretoggles/c;
    ((c)localObject3).<init>();
    localObject3 = (Comparator)localObject3;
    localObject2 = (Iterable)c.a.m.a((Iterable)localObject2, (Comparator)localObject3);
    localObject3 = new java/util/ArrayList;
    int i = c.a.m.a((Iterable)localObject2, 10);
    ((ArrayList)localObject3).<init>(i);
    localObject3 = (Collection)localObject3;
    localObject2 = ((Iterable)localObject2).iterator();
    i = 0;
    Object localObject4 = null;
    boolean bool2 = false;
    Object localObject5 = null;
    for (;;)
    {
      boolean bool3 = ((Iterator)localObject2).hasNext();
      if (!bool3) {
        break;
      }
      localObject7 = ((Iterator)localObject2).next();
      int k = bool2 + true;
      if (bool2) {
        c.a.m.a();
      }
      localObject7 = (b)localObject7;
      boolean bool4 = localObject7 instanceof l;
      Object localObject8;
      long l1;
      String str1;
      String str2;
      label453:
      Object localObject9;
      Object localObject10;
      if (bool4)
      {
        localObject8 = new com/truecaller/feature_toggles/control_panel/d$d;
        l1 = bool2;
        str1 = ((b)localObject7).b();
        str2 = ((b)localObject7).c();
        localObject7 = (l)localObject7;
        boolean bool5 = ((l)localObject7).f();
        bool2 = ((l)localObject7).e();
        if (!bool2)
        {
          bool2 = ((l)localObject7).g();
          if (!bool2)
          {
            bool6 = false;
            break label453;
          }
        }
        boolean bool6 = true;
        localObject9 = ((l)localObject7).d();
        bool2 = ((l)localObject7).g();
        boolean bool7 = bool2 ^ true;
        localObject10 = localObject8;
        ((d.d)localObject8).<init>(l1, str1, str2, (String)localObject9, bool5, bool6, bool7);
        localObject8 = (d)localObject8;
      }
      else
      {
        bool4 = localObject7 instanceof com.truecaller.featuretoggles.f;
        if (bool4)
        {
          localObject8 = localObject7;
          localObject8 = (com.truecaller.featuretoggles.f)localObject7;
          localObject10 = ((com.truecaller.featuretoggles.f)localObject8).f();
          Object localObject11 = t.a;
          int m = ((FirebaseFlavor)localObject10).ordinal();
          m = localObject11[m];
          if (m != j)
          {
            long l2 = bool2;
            str2 = ((b)localObject7).b();
            localObject9 = ((b)localObject7).c();
            localObject5 = ((com.truecaller.featuretoggles.f)localObject8).e();
            localObject7 = "";
            bool2 = c.g.b.k.a(localObject5, localObject7);
            Object localObject12;
            if (bool2)
            {
              localObject5 = "(Empty)";
              localObject12 = localObject5;
            }
            else
            {
              localObject5 = ((com.truecaller.featuretoggles.f)localObject8).e();
              localObject12 = localObject5;
            }
            localObject5 = ((com.truecaller.featuretoggles.f)localObject8).f().toString();
            if (localObject5 != null)
            {
              localObject5 = ((String)localObject5).toLowerCase();
              c.g.b.k.a(localObject5, "(this as java.lang.String).toLowerCase()");
              localObject7 = new com/truecaller/feature_toggles/control_panel/d$c;
              localObject11 = localObject7;
              ((d.c)localObject7).<init>(l2, str2, (String)localObject9, (String)localObject12, (String)localObject5);
              localObject8 = localObject7;
              localObject8 = (d)localObject7;
            }
            else
            {
              localObject1 = new c/u;
              ((u)localObject1).<init>("null cannot be cast to non-null type java.lang.String");
              throw ((Throwable)localObject1);
            }
          }
          else
          {
            localObject9 = new com/truecaller/feature_toggles/control_panel/d$b;
            long l3 = bool2;
            String str3 = ((b)localObject7).b();
            str1 = ((b)localObject7).c();
            boolean bool8 = ((b)localObject7).a();
            localObject8 = localObject9;
            ((d.b)localObject9).<init>(l3, str3, str1, bool8);
            localObject8 = (d)localObject9;
          }
        }
        else
        {
          localObject8 = new com/truecaller/feature_toggles/control_panel/d$a;
          l1 = bool2;
          str1 = ((b)localObject7).b();
          str2 = ((b)localObject7).c();
          boolean bool9 = ((b)localObject7).a();
          localObject10 = localObject8;
          ((d.a)localObject8).<init>(l1, str1, str2, bool9);
          localObject8 = (d)localObject8;
        }
      }
      ((Collection)localObject3).add(localObject8);
      bool2 = k;
    }
    localObject3 = (List)localObject3;
    ((f)localObject1).a((List)localObject3);
    localObject1 = (v)b;
    if (localObject1 != null)
    {
      ((v)localObject1).a();
      return;
    }
  }
  
  public final void a()
  {
    r.a locala = a;
    if (locala != null)
    {
      locala.b();
      return;
    }
  }
  
  public final void a(r.a parama)
  {
    c.g.b.k.b(parama, "router");
    a = parama;
  }
  
  public final void a(String paramString)
  {
    b(paramString);
  }
  
  public final void a(String paramString1, String paramString2)
  {
    c.g.b.k.b(paramString1, "taskKey");
    c.g.b.k.b(paramString2, "firebaseString");
    r.a locala = a;
    if (locala != null)
    {
      locala.a(paramString1, paramString2);
      return;
    }
  }
  
  public final void a(String paramString, boolean paramBoolean)
  {
    c.g.b.k.b(paramString, "taskKey");
    Object localObject = d;
    paramString = ((j)localObject).a(paramString);
    if (paramString == null) {
      return;
    }
    if (paramString != null)
    {
      localObject = paramString;
      ((h)paramString).a(paramBoolean);
      e.a(paramString);
      return;
    }
    paramString = new c/u;
    paramString.<init>("null cannot be cast to non-null type com.truecaller.featuretoggles.MutableFeature");
    throw paramString;
  }
  
  public final void b()
  {
    r.a locala = a;
    if (locala != null)
    {
      locala.a();
      return;
    }
  }
  
  public final void b(String paramString1, String paramString2)
  {
    c.g.b.k.b(paramString1, "taskKey");
    c.g.b.k.b(paramString2, "newFirebaseString");
    j localj = d;
    paramString1 = localj.a(paramString1);
    if (paramString1 == null) {
      return;
    }
    if (paramString1 != null)
    {
      ((com.truecaller.featuretoggles.k)paramString1).a(paramString2);
      b(null);
      return;
    }
    paramString1 = new c/u;
    paramString1.<init>("null cannot be cast to non-null type com.truecaller.featuretoggles.MutableFirebaseFeature");
    throw paramString1;
  }
  
  public final void b(String paramString, boolean paramBoolean)
  {
    c.g.b.k.b(paramString, "taskKey");
    d.a(paramString, paramBoolean);
  }
  
  public final void c()
  {
    Object localObject1 = (Iterable)d.a();
    Object localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>();
    localObject2 = (Collection)localObject2;
    localObject1 = ((Iterable)localObject1).iterator();
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      Object localObject3 = ((Iterator)localObject1).next();
      boolean bool2 = localObject3 instanceof h;
      if (bool2) {
        ((Collection)localObject2).add(localObject3);
      }
    }
    localObject2 = (Iterable)localObject2;
    localObject1 = ((Iterable)localObject2).iterator();
    for (;;)
    {
      boolean bool3 = ((Iterator)localObject1).hasNext();
      if (!bool3) {
        break;
      }
      localObject2 = (h)((Iterator)localObject1).next();
      ((h)localObject2).h();
    }
    b(null);
  }
  
  public final void y_()
  {
    super.y_();
    a = null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.feature_toggles.control_panel.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */