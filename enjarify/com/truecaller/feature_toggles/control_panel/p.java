package com.truecaller.feature_toggles.control_panel;

import dagger.a.d;
import javax.inject.Provider;

public final class p
  implements d
{
  private final i a;
  private final Provider b;
  
  private p(i parami, Provider paramProvider)
  {
    a = parami;
    b = paramProvider;
  }
  
  public static p a(i parami, Provider paramProvider)
  {
    p localp = new com/truecaller/feature_toggles/control_panel/p;
    localp.<init>(parami, paramProvider);
    return localp;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.feature_toggles.control_panel.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */