package com.truecaller.feature_toggles.control_panel;

import dagger.a.d;
import javax.inject.Provider;

public final class q
  implements d
{
  private final i a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  private final Provider f;
  
  private q(i parami, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5)
  {
    a = parami;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
  }
  
  public static q a(i parami, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5)
  {
    q localq = new com/truecaller/feature_toggles/control_panel/q;
    localq.<init>(parami, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5);
    return localq;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.feature_toggles.control_panel.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */