package com.truecaller.feature_toggles.control_panel;

import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import c.g.a.b;

final class ac$b
  implements CompoundButton.OnCheckedChangeListener
{
  ac$b(b paramb) {}
  
  public final void onCheckedChanged(CompoundButton paramCompoundButton, boolean paramBoolean)
  {
    paramCompoundButton = a;
    Boolean localBoolean = Boolean.valueOf(paramBoolean);
    paramCompoundButton.invoke(localBoolean);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.feature_toggles.control_panel.ac.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */