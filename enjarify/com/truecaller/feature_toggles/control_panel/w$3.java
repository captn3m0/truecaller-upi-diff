package com.truecaller.feature_toggles.control_panel;

import android.view.MenuItem;
import android.view.MenuItem.OnActionExpandListener;
import c.g.b.k;

public final class w$3
  implements MenuItem.OnActionExpandListener
{
  w$3(MenuItem paramMenuItem1, MenuItem paramMenuItem2) {}
  
  public final boolean onMenuItemActionCollapse(MenuItem paramMenuItem)
  {
    paramMenuItem = a;
    k.a(paramMenuItem, "menuResetValue");
    boolean bool = true;
    paramMenuItem.setVisible(bool);
    paramMenuItem = b;
    k.a(paramMenuItem, "menuSaveRestart");
    paramMenuItem.setVisible(bool);
    return bool;
  }
  
  public final boolean onMenuItemActionExpand(MenuItem paramMenuItem)
  {
    paramMenuItem = a;
    k.a(paramMenuItem, "menuResetValue");
    paramMenuItem.setVisible(false);
    paramMenuItem = b;
    k.a(paramMenuItem, "menuSaveRestart");
    paramMenuItem.setVisible(false);
    return true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.feature_toggles.control_panel.w.3
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */