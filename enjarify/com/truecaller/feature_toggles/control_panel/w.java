package com.truecaller.feature_toggles.control_panel;

import android.content.Context;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.Toolbar.OnMenuItemClickListener;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnActionExpandListener;
import android.view.View;
import android.view.View.OnClickListener;
import c.g.b.k;
import com.truecaller.ui.q;

public final class w
  implements v
{
  final v.a a;
  private final Toolbar b;
  private final RecyclerView c;
  private final u d;
  
  public w(v.a parama, View paramView, com.truecaller.b paramb)
  {
    a = parama;
    parama = paramView.findViewById(2131364907);
    k.a(parama, "view.findViewById(R.id.toolbar)");
    parama = (Toolbar)parama;
    b = parama;
    int i = 2131363055;
    parama = paramView.findViewById(i);
    k.a(parama, "view.findViewById(R.id.features_recycler)");
    parama = (RecyclerView)parama;
    c = parama;
    parama = new com/truecaller/feature_toggles/control_panel/u;
    parama.<init>(paramb);
    d = parama;
    d.setHasStableIds(true);
    parama = c;
    paramb = (RecyclerView.Adapter)d;
    parama.setAdapter(paramb);
    parama = c;
    paramb = new android/support/v7/widget/LinearLayoutManager;
    Object localObject = paramView.getContext();
    paramb.<init>((Context)localObject);
    paramb = (RecyclerView.LayoutManager)paramb;
    parama.setLayoutManager(paramb);
    parama = new com/truecaller/ui/q;
    paramView = paramView.getContext();
    int j = 2131558616;
    int k = 0;
    localObject = null;
    parama.<init>(paramView, j, 0);
    parama.b();
    parama.a();
    paramView = c;
    parama = (RecyclerView.ItemDecoration)parama;
    paramView.addItemDecoration(parama);
    parama = b;
    int m = 2131623952;
    parama.inflateMenu(m);
    parama = b.getNavigationIcon();
    if (parama != null)
    {
      paramView = parama.mutate();
      if (paramView != null)
      {
        paramb = b.getContext();
        k = 2130969592;
        j = com.truecaller.utils.ui.b.a(paramb, k);
        localObject = PorterDuff.Mode.SRC_IN;
        paramView.setColorFilter(j, (PorterDuff.Mode)localObject);
      }
    }
    b.setNavigationIcon(parama);
    parama = b;
    paramView = new com/truecaller/feature_toggles/control_panel/w$1;
    paramView.<init>(this);
    paramView = (View.OnClickListener)paramView;
    parama.setNavigationOnClickListener(paramView);
    parama = b;
    paramView = new com/truecaller/feature_toggles/control_panel/w$2;
    paramView.<init>(this);
    paramView = (Toolbar.OnMenuItemClickListener)paramView;
    parama.setOnMenuItemClickListener(paramView);
    parama = b.getMenu();
    m = 2131363749;
    parama = parama.findItem(m);
    paramView = b.getMenu();
    j = 2131363750;
    paramView = paramView.findItem(j);
    paramb = b.getMenu();
    k = 2131364245;
    paramb = paramb.findItem(k);
    localObject = new com/truecaller/feature_toggles/control_panel/w$3;
    ((w.3)localObject).<init>(parama, paramView);
    localObject = (MenuItem.OnActionExpandListener)localObject;
    paramb.setOnActionExpandListener((MenuItem.OnActionExpandListener)localObject);
    k.a(paramb, "menuSearch");
    parama = paramb.getActionView();
    if (parama != null)
    {
      parama = (SearchView)parama;
      paramView = new com/truecaller/feature_toggles/control_panel/w$4;
      paramView.<init>(this);
      paramView = (SearchView.OnQueryTextListener)paramView;
      parama.setOnQueryTextListener(paramView);
      return;
    }
    parama = new c/u;
    parama.<init>("null cannot be cast to non-null type android.support.v7.widget.SearchView");
    throw parama;
  }
  
  public final void a()
  {
    d.notifyDataSetChanged();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.feature_toggles.control_panel.w
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */