package com.truecaller.feature_toggles.control_panel;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;

public abstract class a
  extends RecyclerView.ViewHolder
  implements e
{
  private boolean a;
  private String b;
  
  public a(View paramView)
  {
    super(paramView);
  }
  
  public final String a()
  {
    return b;
  }
  
  public final boolean b()
  {
    return a;
  }
  
  public void c()
  {
    a = false;
    b = null;
  }
  
  public final void c_(String paramString)
  {
    b = paramString;
  }
  
  public final void c_(boolean paramBoolean)
  {
    a = paramBoolean;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.feature_toggles.control_panel.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */