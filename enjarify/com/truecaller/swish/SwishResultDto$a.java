package com.truecaller.swish;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class SwishResultDto$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    SwishResultDto localSwishResultDto = new com/truecaller/swish/SwishResultDto;
    String str1 = paramParcel.readString();
    String str2 = paramParcel.readString();
    int i = paramParcel.readInt();
    if (i != 0)
    {
      double d = paramParcel.readDouble();
      paramParcel = Double.valueOf(d);
    }
    else
    {
      paramParcel = null;
    }
    localSwishResultDto.<init>(str1, str2, paramParcel);
    return localSwishResultDto;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new SwishResultDto[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.swish.SwishResultDto.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */