package com.truecaller.swish;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class SwishStringDto$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    SwishStringDto localSwishStringDto = new com/truecaller/swish/SwishStringDto;
    String str = paramParcel.readString();
    int i = paramParcel.readInt();
    if (i != 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      paramParcel = null;
    }
    localSwishStringDto.<init>(str, i);
    return localSwishStringDto;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new SwishStringDto[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.swish.SwishStringDto.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */