package com.truecaller.swish;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class SwishDto$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    SwishDto localSwishDto = new com/truecaller/swish/SwishDto;
    SwishStringDto localSwishStringDto = (SwishStringDto)SwishStringDto.CREATOR.createFromParcel(paramParcel);
    SwishNumberDto localSwishNumberDto = (SwishNumberDto)SwishNumberDto.CREATOR.createFromParcel(paramParcel);
    paramParcel = (SwishStringDto)SwishStringDto.CREATOR.createFromParcel(paramParcel);
    localSwishDto.<init>(localSwishStringDto, localSwishNumberDto, paramParcel);
    return localSwishDto;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new SwishDto[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.swish.SwishDto.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */