package com.truecaller.swish;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.telephony.PhoneNumberUtils;
import c.u;
import com.google.c.a.k.c;
import com.google.c.a.m.a;
import com.truecaller.TrueApp;
import com.truecaller.common.h.aa;
import com.truecaller.log.AssertionUtil;

public final class e
  implements d
{
  private final Context a;
  
  public e(Context paramContext)
  {
    a = paramContext;
  }
  
  public final b a(String paramString)
  {
    c.g.b.k.b(paramString, "number");
    try
    {
      boolean bool = PhoneNumberUtils.isEmergencyNumber(paramString);
      if (bool) {
        return null;
      }
      Object localObject1 = a;
      localObject1 = ((Context)localObject1).getApplicationContext();
      if (localObject1 != null)
      {
        localObject1 = (TrueApp)localObject1;
        localObject1 = ((TrueApp)localObject1).H();
        Object localObject2 = "(context.applicationCont…pp).profileCountryIsoCode";
        c.g.b.k.a(localObject1, (String)localObject2);
        localObject2 = com.google.c.a.k.a();
        paramString = (CharSequence)paramString;
        localObject1 = aa.c((String)localObject1);
        paramString = ((com.google.c.a.k)localObject2).a(paramString, (String)localObject1);
        localObject1 = k.c.a;
        localObject1 = ((com.google.c.a.k)localObject2).a(paramString, (k.c)localObject1);
        localObject2 = new com/truecaller/swish/b;
        String str1 = "parsedNumber";
        c.g.b.k.a(paramString, str1);
        int i = b;
        long l = d;
        paramString = String.valueOf(l);
        String str2 = "normalizedNumber";
        c.g.b.k.a(localObject1, str2);
        ((b)localObject2).<init>(i, paramString, (String)localObject1);
        return (b)localObject2;
      }
      paramString = new c/u;
      localObject1 = "null cannot be cast to non-null type com.truecaller.TrueApp";
      paramString.<init>((String)localObject1);
      throw paramString;
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
    AssertionUtil.report(new String[] { "Cannot parse normalized number" });
    return null;
  }
  
  public final boolean a()
  {
    boolean bool = false;
    try
    {
      Object localObject = a;
      localObject = ((Context)localObject).getPackageManager();
      String str = "se.bankgirot.swish";
      localObject = ((PackageManager)localObject).getApplicationInfo(str, 0);
      if (localObject != null) {
        bool = enabled;
      }
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      for (;;) {}
    }
    return bool;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.swish.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */