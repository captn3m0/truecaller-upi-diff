package com.truecaller.swish;

import c.a.f;
import c.o.b;
import c.x;
import com.truecaller.common.h.aa;
import com.truecaller.data.entity.Contact;
import com.truecaller.flashsdk.core.b;
import com.truecaller.log.AssertionUtil;
import com.truecaller.utils.n;
import java.text.NumberFormat;
import java.util.List;
import kotlinx.coroutines.ag;

final class r$a
  extends c.d.b.a.k
  implements c.g.a.m
{
  int a;
  private ag d;
  
  r$a(r paramr, SwishResultDto paramSwishResultDto, c.d.c paramc)
  {
    super(2, paramc);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/swish/r$a;
    r localr = b;
    SwishResultDto localSwishResultDto = c;
    locala.<init>(localr, localSwishResultDto, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        paramObject = b.g;
        localObject1 = "flash_enabled";
        boolean bool2 = ((com.truecaller.common.g.a)paramObject).b((String)localObject1);
        if (bool2)
        {
          paramObject = b.c;
          bool2 = ((k)paramObject).b();
          if (bool2)
          {
            paramObject = c.getPayee();
            if (paramObject == null) {
              return x.a;
            }
            localObject1 = c.getAmount();
            if (localObject1 != null)
            {
              double d1 = ((Double)localObject1).doubleValue();
              try
              {
                Object localObject2 = b;
                localObject2 = c;
                boolean bool3 = ((k)localObject2).c((String)paramObject);
                if (bool3)
                {
                  localObject2 = "SE";
                  localObject2 = aa.a((String)paramObject, (String)localObject2);
                }
                else
                {
                  localObject2 = "+";
                  bool3 = c.n.m.b((String)paramObject, (String)localObject2, false);
                  if (!bool3)
                  {
                    localObject2 = "+";
                    localObject3 = String.valueOf(paramObject);
                    localObject2 = ((String)localObject2).concat((String)localObject3);
                  }
                  else
                  {
                    localObject2 = paramObject;
                  }
                }
                Object localObject3 = b;
                localObject3 = f;
                localObject2 = ((com.truecaller.data.access.c)localObject3).b((String)localObject2);
                if (localObject2 != null)
                {
                  bool3 = ((Contact)localObject2).L();
                  if (bool3)
                  {
                    long l = Long.parseLong((String)paramObject);
                    paramObject = b;
                    paramObject = a;
                    paramObject = ((NumberFormat)paramObject).format(d1);
                    localObject1 = b;
                    localObject1 = e;
                    Object localObject4 = b;
                    localObject4 = d;
                    int j = 2130903064;
                    localObject4 = ((n)localObject4).a(j);
                    localObject2 = "resourceProvider.getStri…rray.swish_flash_buttons)";
                    c.g.b.k.a(localObject4, (String)localObject2);
                    localObject4 = f.f((Object[])localObject4);
                    localObject2 = b;
                    localObject2 = d;
                    int k = 2131888862;
                    int m = 1;
                    Object[] arrayOfObject = new Object[m];
                    arrayOfObject[0] = paramObject;
                    paramObject = ((n)localObject2).a(k, arrayOfObject);
                    localObject2 = "resourceProvider.getStri…message, formattedAmount)";
                    c.g.b.k.a(paramObject, (String)localObject2);
                    ((b)localObject1).a(l, (List)localObject4, (String)paramObject);
                    break label399;
                  }
                }
                return x.a;
              }
              catch (NumberFormatException localNumberFormatException)
              {
                paramObject = new String[] { "Cannot parse Swish payment result number" };
                AssertionUtil.report((String[])paramObject);
              }
              label399:
              return x.a;
            }
            return x.a;
          }
        }
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.swish.r.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */