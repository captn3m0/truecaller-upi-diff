package com.truecaller.swish;

import c.l.g;
import com.google.gson.f;
import com.truecaller.common.account.r;
import com.truecaller.featuretoggles.e;
import com.truecaller.featuretoggles.e.a;
import com.truecaller.log.AssertionUtil;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLDecoder;
import java.net.URLEncoder;

public final class l
  implements k
{
  private final String a;
  private final c.n.k b;
  private final c.n.k c;
  private final e d;
  private final d e;
  private final r f;
  private final f g;
  private final com.truecaller.common.g.a h;
  
  public l(e parame, d paramd, r paramr, f paramf, com.truecaller.common.g.a parama)
  {
    d = parame;
    e = paramd;
    f = paramr;
    g = paramf;
    h = parama;
    a = "UTF-8";
    parame = new c/n/k;
    parame.<init>("07\\d{8}|7\\d{8}");
    b = parame;
    parame = new c/n/k;
    parame.<init>("123\\d{7}|90\\d{5}");
    c = parame;
  }
  
  public final String a(String paramString)
  {
    c.g.b.k.b(paramString, "number");
    Object localObject1 = e.a(paramString);
    int i = 1;
    StringBuilder localStringBuilder;
    if (localObject1 == null)
    {
      localObject1 = new String[i];
      localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localStringBuilder.append(paramString);
      localStringBuilder.append(" cannot be parsed");
      paramString = localStringBuilder.toString();
      localObject1[0] = paramString;
      return null;
    }
    Object localObject2 = b;
    int j = a;
    int k = 46;
    if (j != k)
    {
      localObject1 = new String[i];
      localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localStringBuilder.append(paramString);
      localStringBuilder.append(" does not have Sweden country code 46");
      paramString = localStringBuilder.toString();
      localObject1[0] = paramString;
      return null;
    }
    boolean bool = c((String)localObject2);
    if (bool) {
      return (String)localObject2;
    }
    localObject2 = (CharSequence)localObject2;
    paramString = b;
    bool = paramString.a((CharSequence)localObject2);
    if (bool) {
      return c;
    }
    return null;
  }
  
  public final String a(String paramString1, double paramDouble, String paramString2)
  {
    c.g.b.k.b(paramString1, "payee");
    c.g.b.k.b(paramString2, "message");
    SwishDto localSwishDto = new com/truecaller/swish/SwishDto;
    SwishStringDto localSwishStringDto = new com/truecaller/swish/SwishStringDto;
    localSwishStringDto.<init>(paramString1, false);
    paramString1 = new com/truecaller/swish/SwishNumberDto;
    paramString1.<init>(paramDouble, false);
    Object localObject1 = new com/truecaller/swish/SwishStringDto;
    ((SwishStringDto)localObject1).<init>(paramString2, false);
    localSwishDto.<init>(localSwishStringDto, paramString1, (SwishStringDto)localObject1);
    paramString1 = "truecaller://swish";
    try
    {
      localObject1 = a;
      paramString1 = URLEncoder.encode(paramString1, (String)localObject1);
      localObject1 = g;
      localObject1 = ((f)localObject1).b(localSwishDto);
      Object localObject2 = a;
      localObject1 = URLEncoder.encode((String)localObject1, (String)localObject2);
      localObject2 = new java/lang/StringBuilder;
      paramString2 = "swish://payment?data=";
      ((StringBuilder)localObject2).<init>(paramString2);
      ((StringBuilder)localObject2).append((String)localObject1);
      localObject1 = "&callbackurl=";
      ((StringBuilder)localObject2).append((String)localObject1);
      ((StringBuilder)localObject2).append(paramString1);
      paramString1 = "&callbackresultparameter=result";
      ((StringBuilder)localObject2).append(paramString1);
      paramString1 = ((StringBuilder)localObject2).toString();
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localUnsupportedEncodingException);
      paramString1 = null;
    }
    return paramString1;
  }
  
  public final void a(boolean paramBoolean)
  {
    h.b("swish_flash_enabled", paramBoolean);
  }
  
  public final boolean a()
  {
    Object localObject1 = e;
    boolean bool = ((d)localObject1).a();
    if (bool)
    {
      localObject1 = d;
      e.a locala = j;
      Object localObject2 = e.a;
      int i = 27;
      localObject2 = localObject2[i];
      localObject1 = locala.a((e)localObject1, (g)localObject2);
      bool = ((com.truecaller.featuretoggles.b)localObject1).a();
      if (bool)
      {
        localObject1 = f;
        bool = ((r)localObject1).c();
        if (bool) {
          return true;
        }
      }
    }
    return false;
  }
  
  public final SwishResultDto b(String paramString)
  {
    Object localObject1 = "rawResult";
    c.g.b.k.b(paramString, (String)localObject1);
    try
    {
      localObject1 = a;
      paramString = URLDecoder.decode(paramString, (String)localObject1);
      localObject1 = g;
      Object localObject2 = "decodedResult";
      c.g.b.k.a(paramString, (String)localObject2);
      localObject2 = new com/truecaller/swish/l$a;
      ((l.a)localObject2).<init>();
      localObject2 = b;
      String str = "object : TypeToken<T>() {}.type";
      c.g.b.k.a(localObject2, str);
      paramString = ((f)localObject1).a(paramString, (Type)localObject2);
      localObject1 = "this.fromJson(json, typeToken<T>())";
      c.g.b.k.a(paramString, (String)localObject1);
      paramString = (SwishResultDto)paramString;
    }
    catch (Exception localException)
    {
      AssertionUtil.report(new String[] { "Cannot parse Swish payment result" });
      paramString = null;
    }
    return paramString;
  }
  
  public final boolean b()
  {
    return h.a("swish_flash_enabled", true);
  }
  
  public final boolean c(String paramString)
  {
    c.g.b.k.b(paramString, "number");
    paramString = (CharSequence)paramString;
    return c.a(paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.swish.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */