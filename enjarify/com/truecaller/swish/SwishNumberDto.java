package com.truecaller.swish;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class SwishNumberDto
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  private final boolean editable;
  private final double value;
  
  static
  {
    SwishNumberDto.a locala = new com/truecaller/swish/SwishNumberDto$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public SwishNumberDto(double paramDouble, boolean paramBoolean)
  {
    value = paramDouble;
    editable = paramBoolean;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean getEditable()
  {
    return editable;
  }
  
  public final double getValue()
  {
    return value;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    double d = value;
    paramParcel.writeDouble(d);
    paramInt = editable;
    paramParcel.writeInt(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.swish.SwishNumberDto
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */