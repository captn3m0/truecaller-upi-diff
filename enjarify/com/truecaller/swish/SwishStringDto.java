package com.truecaller.swish;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class SwishStringDto
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  private final boolean editable;
  private final String value;
  
  static
  {
    SwishStringDto.a locala = new com/truecaller/swish/SwishStringDto$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public SwishStringDto(String paramString, boolean paramBoolean)
  {
    value = paramString;
    editable = paramBoolean;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean getEditable()
  {
    return editable;
  }
  
  public final String getValue()
  {
    return value;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    String str = value;
    paramParcel.writeString(str);
    paramInt = editable;
    paramParcel.writeInt(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.swish.SwishStringDto
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */