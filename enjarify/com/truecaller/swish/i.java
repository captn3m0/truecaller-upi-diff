package com.truecaller.swish;

import c.a.ag;
import c.n.m;
import c.t;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e;
import com.truecaller.analytics.e.a;
import com.truecaller.androidactors.f;
import com.truecaller.bb;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.log.AssertionUtil;
import com.truecaller.tracking.events.ao;
import com.truecaller.tracking.events.ao.a;
import java.util.Iterator;

public final class i
  extends bb
  implements h.a
{
  private String a;
  private final c.n.k c;
  private final k d;
  private final d e;
  private final b f;
  private final f g;
  
  public i(k paramk, d paramd, b paramb, f paramf)
  {
    d = paramk;
    e = paramd;
    f = paramb;
    g = paramf;
    paramk = new c/n/k;
    paramk.<init>("\\d{0,6}([.,]\\d{0,2})?");
    c = paramk;
  }
  
  private static Double a(String paramString)
  {
    Object localObject = paramString;
    localObject = (CharSequence)paramString;
    int i = ((CharSequence)localObject).length();
    if (i == 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject = null;
    }
    if (i != 0) {
      return Double.valueOf(0.0D);
    }
    localObject = ",";
    String str = ".";
    try
    {
      paramString = m.a(paramString, (String)localObject, str);
      double d1 = Double.parseDouble(paramString);
      paramString = Double.valueOf(d1);
    }
    catch (NumberFormatException localNumberFormatException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localNumberFormatException);
      paramString = null;
    }
    return paramString;
  }
  
  public final String a(CharSequence paramCharSequence1, int paramInt1, int paramInt2, CharSequence paramCharSequence2, int paramInt3, int paramInt4)
  {
    c.g.b.k.b(paramCharSequence1, "source");
    c.g.b.k.b(paramCharSequence2, "dest");
    paramCharSequence1 = paramCharSequence1.subSequence(paramInt1, paramInt2).toString();
    Object localObject1 = paramCharSequence2.subSequence(paramInt3, paramInt4).toString();
    paramInt2 = 0;
    CharSequence localCharSequence1 = null;
    Object localObject2 = paramCharSequence2.subSequence(0, paramInt3).toString();
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append((String)localObject2);
    localStringBuilder.append(paramCharSequence1);
    localObject2 = localStringBuilder.toString();
    localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append((String)localObject2);
    paramInt3 = paramCharSequence2.length();
    paramCharSequence2 = paramCharSequence2.subSequence(paramInt4, paramInt3).toString();
    localStringBuilder.append(paramCharSequence2);
    paramCharSequence2 = localStringBuilder.toString();
    localObject2 = c;
    CharSequence localCharSequence2 = paramCharSequence2;
    localCharSequence2 = (CharSequence)paramCharSequence2;
    paramInt3 = ((c.n.k)localObject2).a(localCharSequence2);
    if (paramInt3 != 0)
    {
      paramCharSequence2 = a(paramCharSequence2);
      if (paramCharSequence2 != null)
      {
        double d1 = paramCharSequence2.doubleValue();
        double d2 = 0.0D;
        paramInt3 = d1 < d2;
        if (paramInt3 >= 0)
        {
          d1 = paramCharSequence2.doubleValue();
          d2 = 150000.0D;
          paramInt3 = d1 < d2;
          if (paramInt3 <= 0)
          {
            localObject1 = (h.b)b;
            paramInt3 = 1;
            if (localObject1 != null)
            {
              d1 = paramCharSequence2.doubleValue();
              d2 = 1.0D;
              boolean bool = d1 < d2;
              if (!bool)
              {
                bool = true;
              }
              else
              {
                bool = false;
                paramCharSequence2 = null;
              }
              ((h.b)localObject1).a(bool);
            }
            localObject1 = (h.b)b;
            if (localObject1 != null)
            {
              int i = localCharSequence2.length();
              if (i > 0) {
                paramInt2 = 1;
              }
              ((h.b)localObject1).b(paramInt2);
            }
            localObject1 = paramCharSequence1;
            localObject1 = (CharSequence)paramCharSequence1;
            localCharSequence1 = (CharSequence)".";
            paramInt1 = m.b((CharSequence)localObject1, localCharSequence1);
            if (paramInt1 != 0) {
              return m.a(paramCharSequence1, ".", ",");
            }
            return null;
          }
        }
      }
      return (String)localObject1;
    }
    return (String)localObject1;
  }
  
  public final void a(String paramString, Contact paramContact)
  {
    if (paramString == null)
    {
      new String[1][0] = "Payee number is not supplied";
      paramString = (h.b)b;
      if (paramString != null)
      {
        paramString.finish();
        return;
      }
      return;
    }
    Object localObject1 = d.a(paramString);
    if (localObject1 == null)
    {
      int i = 1;
      paramContact = new String[i];
      localObject1 = null;
      localObject2 = "Unable to parse normalized number for Swish ";
      paramString = String.valueOf(paramString);
      paramString = ((String)localObject2).concat(paramString);
      paramContact[0] = paramString;
      paramString = (h.b)b;
      if (paramString != null)
      {
        paramString.finish();
        return;
      }
      return;
    }
    a = ((String)localObject1);
    if (paramContact != null)
    {
      localObject1 = paramContact.t();
      if (localObject1 != null) {}
    }
    else
    {
      localObject1 = paramString;
    }
    Object localObject2 = "contact?.displayName ?: normalizedNumber";
    c.g.b.k.a(localObject1, (String)localObject2);
    if (paramContact != null)
    {
      boolean bool1 = paramContact.W();
      if (!bool1)
      {
        localObject2 = paramContact.A();
        Object localObject3 = "contact.numbers";
        c.g.b.k.a(localObject2, (String)localObject3);
        localObject2 = ((Iterable)localObject2).iterator();
        boolean bool3;
        do
        {
          bool2 = ((Iterator)localObject2).hasNext();
          if (!bool2) {
            break;
          }
          localObject3 = ((Iterator)localObject2).next();
          Object localObject4 = localObject3;
          localObject4 = (Number)localObject3;
          String str = "it";
          c.g.b.k.a(localObject4, str);
          localObject4 = ((Number)localObject4).a();
          bool3 = c.g.b.k.a(localObject4, paramString);
        } while (!bool3);
        break label246;
        boolean bool2 = false;
        localObject3 = null;
        label246:
        localObject3 = (Number)localObject3;
        if (localObject3 != null)
        {
          paramString = ((Number)localObject3).n();
          if (paramString != null) {
            break label271;
          }
        }
      }
    }
    paramString = "";
    label271:
    c.g.b.k.a(paramString, "if (contact?.isUnknown =…\n            \"\"\n        }");
    localObject2 = (h.b)b;
    if (localObject2 != null) {
      ((h.b)localObject2).a((String)localObject1);
    }
    localObject1 = (h.b)b;
    if (localObject1 != null) {
      ((h.b)localObject1).b(paramString);
    }
    if (paramContact != null)
    {
      paramString = (h.b)b;
      if (paramString != null)
      {
        paramString.a(paramContact);
        return;
      }
    }
  }
  
  public final void a(String paramString1, String paramString2)
  {
    c.g.b.k.b(paramString1, "amount");
    Object localObject1 = "message";
    c.g.b.k.b(paramString2, (String)localObject1);
    paramString1 = a(paramString1);
    if (paramString1 != null)
    {
      double d1 = paramString1.doubleValue();
      paramString1 = d;
      boolean bool = paramString1.a();
      if (bool)
      {
        paramString1 = d;
        localObject2 = a;
        if (localObject2 == null)
        {
          str1 = "payeeNumber";
          c.g.b.k.a(str1);
        }
        paramString1 = paramString1.a((String)localObject2, d1, paramString2);
        if (paramString1 != null)
        {
          localObject2 = (h.b)b;
          if (localObject2 != null) {
            ((h.b)localObject2).c(paramString1);
          }
        }
      }
      else
      {
        paramString1 = e;
        bool = paramString1.a();
        if (!bool)
        {
          paramString1 = (h.b)b;
          if (paramString1 != null) {
            paramString1.b();
          }
        }
      }
      paramString2 = (CharSequence)paramString2;
      bool = m.a(paramString2) ^ true;
      if (bool) {
        paramString1 = "Yes";
      } else {
        paramString1 = "No";
      }
      paramString2 = f;
      Object localObject2 = new com/truecaller/analytics/e$a;
      ((e.a)localObject2).<init>("Swish");
      localObject2 = ((e.a)localObject2).a("Context", "swishInput");
      String str2 = "Clicked";
      localObject2 = ((e.a)localObject2).a("Status", str2).a("HasMessage", paramString1).a();
      String str1 = "AnalyticsEvent.Builder(A…\n                .build()";
      c.g.b.k.a(localObject2, str1);
      paramString2.a((e)localObject2);
      paramString1 = ag.a(t.a("HasMessage", paramString1));
      localObject1 = Double.valueOf(d1);
      paramString2 = ag.a(t.a("Amount", localObject1));
      localObject1 = ao.b();
      Object localObject3 = c.a;
      localObject3 = (CharSequence)c.a();
      localObject1 = ((ao.a)localObject1).b((CharSequence)localObject3);
      localObject3 = (CharSequence)"Swish_Payment_Sent";
      localObject1 = ((ao.a)localObject1).a((CharSequence)localObject3);
      paramString1 = ((ao.a)localObject1).a(paramString1).b(paramString2).a();
      paramString2 = (ae)g.a();
      paramString1 = (org.apache.a.d.d)paramString1;
      paramString2.a(paramString1);
      paramString1 = (h.b)b;
      if (paramString1 != null)
      {
        paramString1.finish();
        return;
      }
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.swish.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */