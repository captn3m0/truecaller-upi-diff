package com.truecaller.swish;

import c.g.b.k;
import java.util.UUID;

public final class c
{
  public static final c a;
  private static String b = "";
  
  static
  {
    c localc = new com/truecaller/swish/c;
    localc.<init>();
    a = localc;
  }
  
  public static String a()
  {
    return b;
  }
  
  public static String b()
  {
    String str = UUID.randomUUID().toString();
    k.a(str, "UUID.randomUUID().toString()");
    b = str;
    return str;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.swish.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */