package com.truecaller.swish;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import c.g.b.k;
import c.u;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.calling.ah;
import com.truecaller.calling.dialer.ax;
import com.truecaller.data.entity.Number;
import com.truecaller.utils.n;
import java.util.List;

public final class p
  extends BaseAdapter
{
  private final n a;
  private final ax b;
  private final Context c;
  private final List d;
  
  public p(Context paramContext, List paramList)
  {
    c = paramContext;
    d = paramList;
    paramContext = c.getApplicationContext();
    if (paramContext != null)
    {
      paramContext = ((bk)paramContext).a().r();
      paramList = "(context.applicationCont…sGraph.resourceProvider()";
      k.a(paramContext, paramList);
      a = paramContext;
      paramContext = c.getApplicationContext();
      if (paramContext != null)
      {
        paramContext = ((bk)paramContext).a().s();
        k.a(paramContext, "(context.applicationCont…numberTypeLabelProvider()");
        b = paramContext;
        return;
      }
      paramContext = new c/u;
      paramContext.<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
      throw paramContext;
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
    throw paramContext;
  }
  
  private Number a(int paramInt)
  {
    return (Number)d.get(paramInt);
  }
  
  public final int getCount()
  {
    return d.size();
  }
  
  public final long getItemId(int paramInt)
  {
    return 0L;
  }
  
  public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    Object localObject1 = "parent";
    k.b(paramViewGroup, (String)localObject1);
    if (paramView == null)
    {
      paramView = LayoutInflater.from(c);
      int i = 2131558926;
      localObject2 = null;
      paramView = paramView.inflate(i, paramViewGroup, false);
      k.a(paramView, "inflater.inflate(R.layou…er_dialog, parent, false)");
      paramViewGroup = new com/truecaller/swish/a;
      paramViewGroup.<init>(paramView);
      paramView.setTag(paramViewGroup);
    }
    else
    {
      paramViewGroup = paramView.getTag();
      if (paramViewGroup == null) {
        break label147;
      }
      paramViewGroup = (a)paramViewGroup;
    }
    localObject1 = a;
    Object localObject2 = (CharSequence)a(paramInt).n();
    ((TextView)localObject1).setText((CharSequence)localObject2);
    paramViewGroup = b;
    Object localObject3 = a(paramInt);
    localObject1 = a;
    localObject2 = b;
    localObject3 = (CharSequence)ah.a((Number)localObject3, (n)localObject1, (ax)localObject2);
    paramViewGroup.setText((CharSequence)localObject3);
    return paramView;
    label147:
    localObject3 = new c/u;
    ((u)localObject3).<init>("null cannot be cast to non-null type com.truecaller.swish.NumberViewHolder");
    throw ((Throwable)localObject3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.swish.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */