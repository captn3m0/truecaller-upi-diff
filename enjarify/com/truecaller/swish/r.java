package com.truecaller.swish;

import c.g.a.m;
import c.t;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.e.a;
import com.truecaller.bb;
import com.truecaller.common.g.a;
import com.truecaller.tracking.events.ao;
import com.truecaller.tracking.events.ao.a;
import com.truecaller.utils.n;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Map;
import kotlinx.coroutines.bg;
import org.apache.a.d.d;

public final class r
  extends bb
  implements q.a
{
  final NumberFormat a;
  final k c;
  final n d;
  final com.truecaller.flashsdk.core.b e;
  final com.truecaller.data.access.c f;
  final a g;
  private final c.d.f h;
  private final com.truecaller.analytics.b i;
  private final com.truecaller.androidactors.f j;
  
  public r(c.d.f paramf, k paramk, n paramn, com.truecaller.flashsdk.core.b paramb, com.truecaller.data.access.c paramc, a parama, com.truecaller.analytics.b paramb1, com.truecaller.androidactors.f paramf1)
  {
    h = paramf;
    c = paramk;
    d = paramn;
    e = paramb;
    f = paramc;
    g = parama;
    i = paramb1;
    j = paramf1;
    paramf = new java/util/Locale;
    paramf.<init>("sv", "SE");
    paramf = NumberFormat.getNumberInstance(paramf);
    a = paramf;
  }
  
  private final void a(SwishResultDto paramSwishResultDto)
  {
    if (paramSwishResultDto == null)
    {
      localObject1 = "SwishResultNotProvided";
    }
    else
    {
      localObject1 = (CharSequence)paramSwishResultDto.getResult();
      int k = 0;
      localObject2 = null;
      if (localObject1 != null)
      {
        m = ((CharSequence)localObject1).length();
        if (m != 0)
        {
          m = 0;
          localObject1 = null;
          break label54;
        }
      }
      int m = 1;
      label54:
      if (m != 0)
      {
        localObject1 = "ResultNotProvided";
      }
      else
      {
        localObject1 = paramSwishResultDto.getAmount();
        if (localObject1 == null)
        {
          localObject1 = "AmountNotProvided";
        }
        else
        {
          localObject1 = (CharSequence)paramSwishResultDto.getPayee();
          if (localObject1 != null)
          {
            m = ((CharSequence)localObject1).length();
            if (m != 0) {}
          }
          else
          {
            k = 1;
          }
          if (k != 0)
          {
            localObject1 = "PayeeNotProvided";
          }
          else
          {
            localObject1 = paramSwishResultDto.getResult();
            if (localObject1 == null) {
              localObject1 = "";
            }
          }
        }
      }
    }
    Object localObject2 = i;
    Object localObject3 = new com/truecaller/analytics/e$a;
    ((e.a)localObject3).<init>("Swish_Result");
    String str = "Status";
    Object localObject1 = ((e.a)localObject3).a(str, (String)localObject1).a();
    localObject3 = "AnalyticsEvent.Builder(A…\n                .build()";
    c.g.b.k.a(localObject1, (String)localObject3);
    ((com.truecaller.analytics.b)localObject2).a((com.truecaller.analytics.e)localObject1);
    if (paramSwishResultDto != null)
    {
      localObject1 = paramSwishResultDto.getResult();
      if (localObject1 != null)
      {
        localObject1 = paramSwishResultDto.getAmount();
        if (localObject1 != null)
        {
          ((Double)localObject1).doubleValue();
          localObject1 = ao.b();
          localObject2 = c.a;
          localObject2 = (CharSequence)c.a();
          localObject1 = ((ao.a)localObject1).b((CharSequence)localObject2);
          localObject2 = (CharSequence)"Swish_Result";
          localObject1 = ((ao.a)localObject1).a((CharSequence)localObject2);
          localObject3 = paramSwishResultDto.getResult();
          localObject2 = c.a.ag.a(t.a("Status", localObject3));
          localObject1 = ((ao.a)localObject1).a((Map)localObject2);
          paramSwishResultDto = paramSwishResultDto.getAmount();
          paramSwishResultDto = c.a.ag.a(t.a("Amount", paramSwishResultDto));
          paramSwishResultDto = ((ao.a)localObject1).b(paramSwishResultDto).a();
          localObject1 = (ae)j.a();
          paramSwishResultDto = (d)paramSwishResultDto;
          ((ae)localObject1).a(paramSwishResultDto);
          return;
        }
        return;
      }
    }
  }
  
  public final void a(String paramString)
  {
    localb = null;
    if (paramString == null) {}
    try
    {
      a(null);
      return;
    }
    finally
    {
      Object localObject1;
      int k;
      int m;
      Object localObject2;
      boolean bool;
      Object localObject3;
      int n;
      localb = (q.b)b;
      if (localb == null) {
        break label351;
      }
      localb.finish();
    }
    localObject1 = c;
    paramString = ((k)localObject1).b(paramString);
    if (paramString == null)
    {
      paramString = (q.b)b;
      if (paramString != null) {
        paramString.finish();
      }
      return;
    }
    localObject1 = paramString.getResult();
    if (localObject1 != null)
    {
      k = ((String)localObject1).hashCode();
      m = -284840886;
      if (k != m)
      {
        m = 3433164;
        if (k == m)
        {
          localObject2 = "paid";
          bool = ((String)localObject1).equals(localObject2);
          if (bool)
          {
            localObject1 = d;
            k = 2131888867;
            localObject3 = new Object[0];
            localObject1 = ((n)localObject1).a(k, (Object[])localObject3);
            break label206;
          }
        }
      }
      else
      {
        localObject2 = "unknown";
        bool = ((String)localObject1).equals(localObject2);
        if (bool)
        {
          localObject1 = d;
          k = 2131886538;
          localObject3 = new Object[0];
          localObject1 = ((n)localObject1).a(k, (Object[])localObject3);
          break label206;
        }
      }
    }
    bool = false;
    localObject1 = null;
    label206:
    if (localObject1 != null)
    {
      localObject2 = b;
      localObject2 = (q.b)localObject2;
      if (localObject2 != null) {
        ((q.b)localObject2).a((String)localObject1);
      }
    }
    a(paramString);
    localObject1 = paramString.getResult();
    localObject2 = "paid";
    bool = c.g.b.k.a(localObject1, localObject2);
    if (bool)
    {
      localObject1 = bg.a;
      localObject1 = (kotlinx.coroutines.ag)localObject1;
      localObject2 = h;
      localObject3 = new com/truecaller/swish/r$a;
      ((r.a)localObject3).<init>(this, paramString, null);
      localObject3 = (m)localObject3;
      n = 2;
      kotlinx.coroutines.e.b((kotlinx.coroutines.ag)localObject1, (c.d.f)localObject2, (m)localObject3, n);
    }
    paramString = (q.b)b;
    if (paramString != null)
    {
      paramString.finish();
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.swish.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */