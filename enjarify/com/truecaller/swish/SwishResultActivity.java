package com.truecaller.swish;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;
import c.g.b.k;
import c.u;
import com.truecaller.TrueApp;
import com.truecaller.bp;

public final class SwishResultActivity
  extends Activity
  implements q.b
{
  public q.a a;
  
  public final void a(String paramString)
  {
    k.b(paramString, "text");
    Object localObject = this;
    localObject = (Context)this;
    paramString = (CharSequence)paramString;
    Toast.makeText((Context)localObject, paramString, 1).show();
  }
  
  protected final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getApplicationContext();
    if (paramBundle != null)
    {
      ((TrueApp)paramBundle).a().aU().a().a(this);
      paramBundle = a;
      if (paramBundle == null)
      {
        localObject = "presenter";
        k.a((String)localObject);
      }
      paramBundle.a(this);
      paramBundle = getIntent();
      if (paramBundle != null)
      {
        paramBundle = paramBundle.getExtras();
        if (paramBundle != null)
        {
          localObject = "result";
          paramBundle = paramBundle.getString((String)localObject);
          break label92;
        }
      }
      paramBundle = null;
      label92:
      Object localObject = a;
      if (localObject == null)
      {
        String str = "presenter";
        k.a(str);
      }
      ((q.a)localObject).a(paramBundle);
      return;
    }
    paramBundle = new c/u;
    paramBundle.<init>("null cannot be cast to non-null type com.truecaller.TrueApp");
    throw paramBundle;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.swish.SwishResultActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */