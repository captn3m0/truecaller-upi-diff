package com.truecaller.swish;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.f;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import c.g.a.b;
import c.g.b.k;
import c.u;
import com.truecaller.R.id;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.common.h.o;
import com.truecaller.data.entity.Contact;
import com.truecaller.flashsdk.assist.t;
import com.truecaller.ui.view.ContactPhoto;
import java.util.HashMap;

public final class SwishInputActivity
  extends AppCompatActivity
  implements h.b
{
  public h.a a;
  private HashMap b;
  
  public final View a(int paramInt)
  {
    Object localObject1 = b;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      b = ((HashMap)localObject1);
    }
    localObject1 = b;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = findViewById(paramInt);
      localObject2 = b;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final h.a a()
  {
    h.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return locala;
  }
  
  public final void a(Contact paramContact)
  {
    k.b(paramContact, "contact");
    int i = R.id.contactPhoto;
    ((ContactPhoto)a(i)).a(paramContact, null);
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "name");
    int i = R.id.nameTextView;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "nameTextView");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void a(boolean paramBoolean)
  {
    int i = R.id.sendButton;
    Button localButton = (Button)a(i);
    k.a(localButton, "sendButton");
    localButton.setEnabled(paramBoolean);
  }
  
  public final void b()
  {
    try
    {
      localIntent = new android/content/Intent;
      str = "android.intent.action.VIEW";
      localObject = "market://details?id=se.bankgirot.swish";
      localObject = Uri.parse((String)localObject);
      localIntent.<init>(str, (Uri)localObject);
      startActivity(localIntent);
      return;
    }
    catch (ActivityNotFoundException localActivityNotFoundException1)
    {
      try
      {
        Intent localIntent = new android/content/Intent;
        String str = "android.intent.action.VIEW";
        Object localObject = "https://play.google.com/store/apps/details?id=se.bankgirot.swish";
        localObject = Uri.parse((String)localObject);
        localIntent.<init>(str, (Uri)localObject);
        startActivity(localIntent);
        return;
      }
      catch (ActivityNotFoundException localActivityNotFoundException2) {}
    }
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "number");
    int i = R.id.numberTextView;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "numberTextView");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void b(boolean paramBoolean)
  {
    int i = R.id.currencyTextView;
    TextView localTextView = (TextView)a(i);
    String str = "currencyTextView";
    k.a(localTextView, str);
    if (paramBoolean) {
      paramBoolean = false;
    } else {
      paramBoolean = true;
    }
    localTextView.setVisibility(paramBoolean);
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "uri");
    paramString = Uri.parse(paramString);
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("android.intent.action.VIEW", paramString);
    localIntent.setPackage("se.bankgirot.swish");
    paramString = this;
    o.a((Context)this, localIntent);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    int i = 2131952150;
    setTheme(i);
    super.onCreate(paramBundle);
    paramBundle = getApplication();
    if (paramBundle != null)
    {
      ((TrueApp)paramBundle).a().aU().a().a(this);
      setContentView(2131558473);
      int j = R.id.toolbar;
      paramBundle = (Toolbar)a(j);
      setSupportActionBar(paramBundle);
      paramBundle = getSupportActionBar();
      i = 1;
      if (paramBundle != null) {
        paramBundle.setDisplayHomeAsUpEnabled(i);
      }
      paramBundle = getSupportActionBar();
      if (paramBundle != null)
      {
        int k = 2131233794;
        paramBundle.setHomeAsUpIndicator(k);
      }
      paramBundle = a;
      if (paramBundle == null)
      {
        localObject1 = "presenter";
        k.a((String)localObject1);
      }
      paramBundle.a(this);
      paramBundle = getIntent().getStringExtra("payee_number");
      Object localObject1 = (Contact)getIntent().getParcelableExtra("payee_contact");
      Object localObject2 = a;
      if (localObject2 == null)
      {
        String str = "presenter";
        k.a(str);
      }
      ((h.a)localObject2).a(paramBundle, (Contact)localObject1);
      j = R.id.amountEditText;
      paramBundle = (EditText)a(j);
      k.a(paramBundle, "amountEditText");
      localObject1 = new com/truecaller/swish/SwishInputActivity$a;
      ((SwishInputActivity.a)localObject1).<init>(this);
      localObject1 = (b)localObject1;
      t.a(paramBundle, (b)localObject1);
      j = R.id.amountEditText;
      paramBundle = (EditText)a(j);
      k.a(paramBundle, "amountEditText");
      Object localObject3 = new InputFilter[i];
      localObject2 = new com/truecaller/swish/SwishInputActivity$b;
      ((SwishInputActivity.b)localObject2).<init>(this);
      localObject2 = (InputFilter)localObject2;
      localObject3[0] = localObject2;
      paramBundle.setFilters((InputFilter[])localObject3);
      j = R.id.sendButton;
      paramBundle = (Button)a(j);
      localObject3 = new com/truecaller/swish/SwishInputActivity$c;
      ((SwishInputActivity.c)localObject3).<init>(this);
      localObject3 = (View.OnClickListener)localObject3;
      paramBundle.setOnClickListener((View.OnClickListener)localObject3);
      j = R.id.amountEditText;
      ((EditText)a(j)).requestFocus();
      return;
    }
    paramBundle = new c/u;
    paramBundle.<init>("null cannot be cast to non-null type com.truecaller.TrueApp");
    throw paramBundle;
  }
  
  public final boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    String str = "item";
    k.b(paramMenuItem, str);
    int i = paramMenuItem.getItemId();
    int j = 16908332;
    if (i != j) {
      return super.onOptionsItemSelected(paramMenuItem);
    }
    onBackPressed();
    return true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.swish.SwishInputActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */