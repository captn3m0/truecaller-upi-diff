package com.truecaller.swish;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class SwishResultDto
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  private final Double amount;
  private final String payee;
  private final String result;
  
  static
  {
    SwishResultDto.a locala = new com/truecaller/swish/SwishResultDto$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public SwishResultDto(String paramString1, String paramString2, Double paramDouble)
  {
    result = paramString1;
    payee = paramString2;
    amount = paramDouble;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final Double getAmount()
  {
    return amount;
  }
  
  public final String getPayee()
  {
    return payee;
  }
  
  public final String getResult()
  {
    return result;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    Object localObject = result;
    paramParcel.writeString((String)localObject);
    localObject = payee;
    paramParcel.writeString((String)localObject);
    localObject = amount;
    if (localObject != null)
    {
      paramParcel.writeInt(1);
      double d = ((Double)localObject).doubleValue();
      paramParcel.writeDouble(d);
      return;
    }
    paramParcel.writeInt(0);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.swish.SwishResultDto
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */