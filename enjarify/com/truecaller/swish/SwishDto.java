package com.truecaller.swish;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class SwishDto
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  private final SwishNumberDto amount;
  private final SwishStringDto message;
  private final SwishStringDto payee;
  private final int version;
  
  static
  {
    SwishDto.a locala = new com/truecaller/swish/SwishDto$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public SwishDto(SwishStringDto paramSwishStringDto1, SwishNumberDto paramSwishNumberDto, SwishStringDto paramSwishStringDto2)
  {
    payee = paramSwishStringDto1;
    amount = paramSwishNumberDto;
    message = paramSwishStringDto2;
    version = 1;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final SwishNumberDto getAmount()
  {
    return amount;
  }
  
  public final SwishStringDto getMessage()
  {
    return message;
  }
  
  public final SwishStringDto getPayee()
  {
    return payee;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    payee.writeToParcel(paramParcel, 0);
    amount.writeToParcel(paramParcel, 0);
    message.writeToParcel(paramParcel, 0);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.swish.SwishDto
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */