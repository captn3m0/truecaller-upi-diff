package com.truecaller.swish;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class SwishNumberDto$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    SwishNumberDto localSwishNumberDto = new com/truecaller/swish/SwishNumberDto;
    double d = paramParcel.readDouble();
    int i = paramParcel.readInt();
    if (i != 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      paramParcel = null;
    }
    localSwishNumberDto.<init>(d, i);
    return localSwishNumberDto;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new SwishNumberDto[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.swish.SwishNumberDto.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */