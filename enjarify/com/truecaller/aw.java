package com.truecaller;

import dagger.a.d;
import javax.inject.Provider;

public final class aw
  implements d
{
  private final c a;
  private final Provider b;
  
  private aw(c paramc, Provider paramProvider)
  {
    a = paramc;
    b = paramProvider;
  }
  
  public static aw a(c paramc, Provider paramProvider)
  {
    aw localaw = new com/truecaller/aw;
    localaw.<init>(paramc, paramProvider);
    return localaw;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.aw
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */