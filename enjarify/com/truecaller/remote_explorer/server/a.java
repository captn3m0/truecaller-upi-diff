package com.truecaller.remote_explorer.server;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.AssetManager;
import android.net.Uri;
import android.support.v4.content.d;
import b.a.a.a.l;
import b.a.a.a.m;
import b.a.a.a.n;
import b.a.a.a.n.b;
import b.a.a.a.n.c;
import com.google.gson.f;
import com.google.gson.g;
import com.truecaller.common.h.k;
import com.truecaller.remote_explorer.a.c;
import com.truecaller.remote_explorer.activities.AllowClientActivity;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class a
  extends b.a.a.a
{
  private com.truecaller.remote_explorer.preferences.a d;
  private c e;
  private Context f;
  private int g;
  private String h;
  private BroadcastReceiver i;
  
  a(Context paramContext, com.truecaller.remote_explorer.preferences.a parama, c paramc)
  {
    a.1 local1 = new com/truecaller/remote_explorer/server/a$1;
    local1.<init>(this);
    i = local1;
    f = paramContext;
    d = parama;
    e = paramc;
    g = 3;
  }
  
  private a.n a(List paramList)
  {
    Object localObject1 = new com/google/gson/g;
    ((g)localObject1).<init>();
    Object localObject2 = org.a.a.b.class;
    Object localObject3 = new com/truecaller/remote_explorer/server/a$a;
    ((a.a)localObject3).<init>();
    localObject1 = ((g)localObject1).a((Type)localObject2, localObject3).a();
    int j = paramList.size();
    int n = 2;
    if (j == n)
    {
      paramList = c.a(f);
      return b.a.a.a.b(((f)localObject1).b(paramList));
    }
    j = paramList.size();
    int i2 = 3;
    int i1;
    Object localObject4;
    if (j == i2)
    {
      paramList = (String)paramList.get(n);
      localObject2 = f;
      paramList = c.a(paramList, (Context)localObject2);
      localObject2 = new com/truecaller/remote_explorer/a/b;
      localObject3 = f;
      ((com.truecaller.remote_explorer.a.b)localObject2).<init>((Context)localObject3, paramList);
      paramList = ((com.truecaller.remote_explorer.a.b)localObject2).a("SELECT name FROM sqlite_master WHERE type='table'");
      localObject2 = new java/util/ArrayList;
      ((ArrayList)localObject2).<init>();
      paramList = paramList.iterator();
      for (;;)
      {
        i1 = paramList.hasNext();
        if (i1 == 0) {
          break;
        }
        localObject3 = (String[])paramList.next();
        i2 = 0;
        localObject4 = null;
        localObject3 = localObject3[0];
        ((List)localObject2).add(localObject3);
      }
      return b.a.a.a.b(((f)localObject1).b(localObject2));
    }
    j = paramList.size();
    int i3 = 4;
    Object localObject5;
    if (j == i3)
    {
      localObject2 = (String)paramList.get(i2);
      localObject5 = "views";
      boolean bool1 = ((String)localObject2).equals(localObject5);
      if (bool1)
      {
        paramList = (String)paramList.get(i1);
        localObject2 = f;
        paramList = c.a(paramList, (Context)localObject2);
        localObject2 = new com/truecaller/remote_explorer/a/b;
        localObject3 = f;
        ((com.truecaller.remote_explorer.a.b)localObject2).<init>((Context)localObject3, paramList);
        paramList = ((com.truecaller.remote_explorer.a.b)localObject2).a();
        return b.a.a.a.b(((f)localObject1).b(paramList));
      }
    }
    int k = paramList.size();
    int i4 = 5;
    if (k == i4)
    {
      localObject2 = (String)paramList.get(i2);
      String str = "query";
      boolean bool2 = ((String)localObject2).equals(str);
      if (bool2)
      {
        localObject2 = (String)paramList.get(i1);
        localObject3 = f;
        localObject2 = c.a((String)localObject2, (Context)localObject3);
        localObject3 = new com/truecaller/remote_explorer/a/b;
        localObject4 = f;
        ((com.truecaller.remote_explorer.a.b)localObject3).<init>((Context)localObject4, (com.truecaller.remote_explorer.a.a)localObject2);
        paramList = (String)paramList.get(i3);
        paramList = ((com.truecaller.remote_explorer.a.b)localObject3).a(paramList);
        return b.a.a.a.b(((f)localObject1).b(paramList));
      }
    }
    int m = paramList.size();
    if (m == i4)
    {
      localObject2 = (String)paramList.get(i1);
      localObject3 = f;
      localObject2 = c.a((String)localObject2, (Context)localObject3);
      localObject3 = new com/truecaller/remote_explorer/a/b;
      localObject5 = f;
      ((com.truecaller.remote_explorer.a.b)localObject3).<init>((Context)localObject5, (com.truecaller.remote_explorer.a.a)localObject2);
      localObject2 = (String)paramList.get(i3);
      localObject5 = "pages";
      boolean bool3 = ((String)localObject2).equals(localObject5);
      if (bool3)
      {
        paramList = (String)paramList.get(i2);
        double d1 = ((com.truecaller.remote_explorer.a.b)localObject3).b(paramList);
        Double.isNaN(d1);
        paramList = Integer.valueOf((int)Math.ceil(d1 / 100.0D));
        return b.a.a.a.b(((f)localObject1).b(paramList));
      }
      localObject2 = (String)paramList.get(i2);
      int i5 = Integer.parseInt((String)paramList.get(i3));
      localObject4 = new java/lang/StringBuilder;
      ((StringBuilder)localObject4).<init>("SELECT * FROM ");
      ((StringBuilder)localObject4).append((String)localObject2);
      ((StringBuilder)localObject4).append(" LIMIT 100 OFFSET ");
      i5 *= 100;
      ((StringBuilder)localObject4).append(i5);
      paramList = ((StringBuilder)localObject4).toString();
      paramList = ((com.truecaller.remote_explorer.a.b)localObject3).a(paramList);
      return b.a.a.a.b(((f)localObject1).b(paramList));
    }
    return b.a.a.a.a(a.n.c.p, "text/plain", "Error: Data not found");
  }
  
  public final a.n a(a.l paraml)
  {
    int j = 3;
    try
    {
      Object localObject1 = paraml.e();
      localObject1 = Uri.parse((String)localObject1);
      localObject1 = ((Uri)localObject1).getPathSegments();
      Object localObject2 = a;
      int k = 0;
      Object localObject3 = null;
      int m = 1;
      if (localObject2 != null)
      {
        localObject2 = b;
        if (localObject2 != null)
        {
          bool1 = true;
          break label62;
        }
      }
      boolean bool1 = false;
      localObject2 = null;
      label62:
      if (bool1)
      {
        localObject2 = a;
        bool1 = ((ServerSocket)localObject2).isClosed();
        if (!bool1)
        {
          localObject2 = b;
          bool1 = ((Thread)localObject2).isAlive();
          if (bool1)
          {
            bool1 = true;
            break label115;
          }
        }
      }
      bool1 = false;
      localObject2 = null;
      label115:
      if (bool1)
      {
        localObject2 = a.2.a;
        Object localObject4 = paraml.b();
        int i3 = ((a.m)localObject4).ordinal();
        int n = localObject2[i3];
        i3 = -1;
        int i4 = 2;
        long l1;
        int i1;
        String str1;
        int i6;
        boolean bool5;
        switch (n)
        {
        default: 
          paraml = "Error: Unknown HTTP verb";
          break;
        case 2: 
          n = ((List)localObject1).size();
          Object localObject5;
          if (n >= i4)
          {
            localObject2 = ((List)localObject1).get(0);
            localObject2 = (String)localObject2;
            localObject5 = "api";
            boolean bool2 = ((String)localObject2).equals(localObject5);
            if (bool2)
            {
              localObject2 = ((List)localObject1).get(m);
              localObject2 = (String)localObject2;
              localObject5 = "permission";
              bool2 = ((String)localObject2).equals(localObject5);
              if (bool2)
              {
                g = j;
                localObject1 = f;
                localObject1 = d.a((Context)localObject1);
                localObject2 = i;
                localObject3 = new android/content/IntentFilter;
                localObject4 = AllowClientActivity.b;
                ((IntentFilter)localObject3).<init>((String)localObject4);
                ((d)localObject1).a((BroadcastReceiver)localObject2, (IntentFilter)localObject3);
                localObject1 = f;
                localObject2 = new android/content/Intent;
                localObject3 = f;
                localObject4 = AllowClientActivity.class;
                ((Intent)localObject2).<init>((Context)localObject3, (Class)localObject4);
                ((Context)localObject1).startActivity((Intent)localObject2);
                for (;;)
                {
                  i5 = g;
                  if (i5 != j) {
                    break;
                  }
                  l1 = 100;
                  Thread.sleep(l1);
                }
                i5 = g;
                if (i5 == m)
                {
                  paraml = paraml.f();
                  h = paraml;
                  paraml = "ok";
                  return b.a.a.a.b(paraml);
                }
                paraml = a.n.c.o;
                localObject1 = "text/plain";
                localObject2 = "Error: Access refused";
                return b.a.a.a.a(paraml, (String)localObject1, (String)localObject2);
              }
              localObject2 = paraml.f();
              localObject5 = h;
              bool2 = ((String)localObject2).equals(localObject5);
              if (bool2)
              {
                localObject2 = ((List)localObject1).get(m);
                localObject2 = (String)localObject2;
                localObject5 = "preferences";
                bool2 = ((String)localObject2).equals(localObject5);
                if (bool2)
                {
                  paraml = new com/google/gson/g;
                  paraml.<init>();
                  localObject1 = org.a.a.b.class;
                  localObject2 = new com/truecaller/remote_explorer/server/a$a;
                  ((a.a)localObject2).<init>();
                  paraml = paraml.a((Type)localObject1, localObject2);
                  paraml = paraml.a();
                  localObject1 = f;
                  localObject1 = com.truecaller.remote_explorer.preferences.a.a((Context)localObject1);
                  paraml = paraml.b(localObject1);
                  return b.a.a.a.b(paraml);
                }
                localObject2 = ((List)localObject1).get(m);
                localObject2 = (String)localObject2;
                localObject5 = "databases";
                bool2 = ((String)localObject2).equals(localObject5);
                if (bool2) {
                  return a((List)localObject1);
                }
                localObject2 = ((List)localObject1).get(m);
                localObject2 = (String)localObject2;
                localObject5 = "download";
                bool2 = ((String)localObject2).equals(localObject5);
                if (bool2)
                {
                  i1 = ((List)localObject1).size();
                  if (i1 == j)
                  {
                    paraml = new java/io/FileInputStream;
                    localObject2 = ((List)localObject1).get(i4);
                    localObject2 = (String)localObject2;
                    localObject3 = f;
                    localObject2 = c.a((String)localObject2, (Context)localObject3);
                    localObject2 = b;
                    paraml.<init>((String)localObject2);
                    i1 = paraml.available();
                    localObject3 = a.n.c.b;
                    str1 = "application/octet-stream";
                    long l2 = i1;
                    paraml = b.a.a.a.a((a.n.b)localObject3, str1, paraml, l2);
                    localObject2 = "Content-Disposition";
                    localObject3 = new java/lang/StringBuilder;
                    str1 = "attachment; filename=\"";
                    ((StringBuilder)localObject3).<init>(str1);
                    localObject1 = ((List)localObject1).get(i4);
                    localObject1 = (String)localObject1;
                    ((StringBuilder)localObject3).append((String)localObject1);
                    localObject1 = "\"";
                    ((StringBuilder)localObject3).append((String)localObject1);
                    localObject1 = ((StringBuilder)localObject3).toString();
                    paraml.a((String)localObject2, (String)localObject1);
                    return paraml;
                  }
                }
              }
              else
              {
                paraml = "Error: You are not allowed to access data";
                return b.a.a.a.b(paraml);
              }
            }
          }
          int i5 = ((List)localObject1).size();
          if (i5 > 0)
          {
            localObject1 = paraml.e();
            localObject2 = "\\.";
            localObject1 = ((String)localObject1).split((String)localObject2);
            i1 = localObject1.length - m;
            localObject1 = localObject1[i1];
            localObject2 = f;
            localObject2 = ((Context)localObject2).getAssets();
            localObject5 = new java/lang/StringBuilder;
            String str2 = "web_prefs";
            ((StringBuilder)localObject5).<init>(str2);
            paraml = paraml.e();
            ((StringBuilder)localObject5).append(paraml);
            paraml = ((StringBuilder)localObject5).toString();
            paraml = ((AssetManager)localObject2).open(paraml);
            i1 = paraml.available();
            localObject2 = new byte[i1];
            i6 = paraml.read((byte[])localObject2);
            paraml.close();
            if (i6 != i3)
            {
              int i7 = ((String)localObject1).hashCode();
              i6 = 3401;
              if (i7 != i6)
              {
                i4 = 98819;
                if (i7 != i4)
                {
                  m = 114276;
                  if (i7 != m)
                  {
                    m = 3213227;
                    if (i7 == m)
                    {
                      paraml = "html";
                      bool5 = ((String)localObject1).equals(paraml);
                      if (bool5) {
                        break label1073;
                      }
                    }
                  }
                  else
                  {
                    paraml = "svg";
                    bool5 = ((String)localObject1).equals(paraml);
                    if (bool5)
                    {
                      k = 3;
                      break label1073;
                    }
                  }
                }
                else
                {
                  paraml = "css";
                  bool5 = ((String)localObject1).equals(paraml);
                  if (bool5)
                  {
                    k = 1;
                    break label1073;
                  }
                }
              }
              else
              {
                paraml = "js";
                bool5 = ((String)localObject1).equals(paraml);
                if (bool5)
                {
                  k = 2;
                  break label1073;
                }
              }
              k = -1;
              switch (k)
              {
              default: 
                paraml = a.n.c.b;
                break;
              case 3: 
                paraml = a.n.c.b;
                localObject1 = "image/svg+xml";
                localObject3 = new java/lang/String;
                ((String)localObject3).<init>((byte[])localObject2);
                return b.a.a.a.a(paraml, (String)localObject1, (String)localObject3);
              case 2: 
                paraml = a.n.c.b;
                localObject1 = "application/javascript";
                localObject3 = new java/lang/String;
                ((String)localObject3).<init>((byte[])localObject2);
                return b.a.a.a.a(paraml, (String)localObject1, (String)localObject3);
              case 1: 
                paraml = a.n.c.b;
                localObject1 = "text/css";
                localObject3 = new java/lang/String;
                ((String)localObject3).<init>((byte[])localObject2);
                return b.a.a.a.a(paraml, (String)localObject1, (String)localObject3);
              case 0: 
                paraml = a.n.c.b;
                localObject1 = "text/html";
                localObject3 = new java/lang/String;
                ((String)localObject3).<init>((byte[])localObject2);
                return b.a.a.a.a(paraml, (String)localObject1, (String)localObject3);
              }
              localObject1 = "text/plain";
              localObject3 = new java/lang/String;
              ((String)localObject3).<init>((byte[])localObject2);
              return b.a.a.a.a(paraml, (String)localObject1, (String)localObject3);
            }
            paraml = new java/io/IOException;
            paraml.<init>();
            throw paraml;
          }
          paraml = f;
          paraml = paraml.getAssets();
          localObject1 = "web_prefs/index.html";
          paraml = paraml.open((String)localObject1);
          i5 = paraml.available();
          localObject1 = new byte[i5];
          i1 = paraml.read((byte[])localObject1);
          paraml.close();
          if (i1 != i3)
          {
            paraml = a.n.c.b;
            localObject2 = "text/html";
            localObject3 = new java/lang/String;
            ((String)localObject3).<init>((byte[])localObject1);
            return b.a.a.a.a(paraml, (String)localObject2, (String)localObject3);
          }
          paraml = new java/io/IOException;
          paraml.<init>();
          throw paraml;
        case 1: 
          label1073:
          paraml = paraml.f();
          localObject2 = h;
          bool5 = paraml.equals(localObject2);
          if (bool5)
          {
            int i8 = ((List)localObject1).size();
            if (i8 > m)
            {
              paraml = ((List)localObject1).get(0);
              paraml = (String)paraml;
              localObject2 = "api";
              boolean bool6 = paraml.equals(localObject2);
              if (bool6)
              {
                paraml = ((List)localObject1).get(m);
                paraml = (String)paraml;
                localObject2 = "preferences";
                bool6 = paraml.equals(localObject2);
                if (bool6)
                {
                  paraml = f;
                  localObject2 = ((List)localObject1).get(i4);
                  localObject2 = (String)localObject2;
                  paraml = paraml.getSharedPreferences((String)localObject2, 0);
                  i1 = 4;
                  localObject2 = ((List)localObject1).get(i1);
                  localObject2 = (String)localObject2;
                  i6 = ((String)localObject2).hashCode();
                  int i9 = -1034364087;
                  boolean bool3;
                  if (i6 != i9)
                  {
                    m = -891985903;
                    if (i6 != m)
                    {
                      k = 64711720;
                      if (i6 == k)
                      {
                        localObject3 = "boolean";
                        bool3 = ((String)localObject2).equals(localObject3);
                        if (bool3)
                        {
                          k = 2;
                          break label1618;
                        }
                      }
                    }
                    else
                    {
                      str1 = "string";
                      bool3 = ((String)localObject2).equals(str1);
                      if (bool3) {
                        break label1618;
                      }
                    }
                  }
                  else
                  {
                    localObject3 = "number";
                    bool3 = ((String)localObject2).equals(localObject3);
                    if (bool3)
                    {
                      k = 1;
                      break label1618;
                    }
                  }
                  k = -1;
                  label1618:
                  int i2 = 5;
                  switch (k)
                  {
                  default: 
                    break;
                  case 2: 
                    paraml = paraml.edit();
                    localObject3 = ((List)localObject1).get(j);
                    localObject3 = (String)localObject3;
                    localObject1 = ((List)localObject1).get(i2);
                    localObject1 = (String)localObject1;
                    boolean bool4 = Boolean.parseBoolean((String)localObject1);
                    paraml = paraml.putBoolean((String)localObject3, bool4);
                    paraml.apply();
                    break;
                  case 1: 
                    paraml = paraml.edit();
                    localObject3 = ((List)localObject1).get(j);
                    localObject3 = (String)localObject3;
                    localObject1 = ((List)localObject1).get(i2);
                    localObject1 = (String)localObject1;
                    l1 = Long.parseLong((String)localObject1);
                    paraml = paraml.putLong((String)localObject3, l1);
                    paraml.apply();
                    break;
                  case 0: 
                    paraml = paraml.edit();
                    localObject3 = ((List)localObject1).get(j);
                    localObject3 = (String)localObject3;
                    localObject1 = ((List)localObject1).get(i2);
                    localObject1 = (String)localObject1;
                    paraml = paraml.putString((String)localObject3, (String)localObject1);
                    paraml.apply();
                  }
                  paraml = "ok";
                  return b.a.a.a.b(paraml);
                }
                paraml = a.n.c.p;
                localObject1 = "text/plain";
                localObject2 = "Error: Data not found";
                return b.a.a.a.a(paraml, (String)localObject1, (String)localObject2);
              }
            }
          }
          paraml = "Error: You are not allowed to edit fields";
          return b.a.a.a.b(paraml);
        }
        return b.a.a.a.b(paraml);
      }
      paraml = "Error: No server alive";
      return b.a.a.a.b(paraml);
    }
    catch (Exception paraml)
    {
      g = j;
      paraml.printStackTrace();
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>("Error: ");
      paraml = paraml.getMessage();
      localStringBuilder.append(paraml);
      return b.a.a.a.b(localStringBuilder.toString());
    }
  }
  
  public final void a()
  {
    Context localContext = f;
    boolean bool = k.a(localContext);
    if (bool) {
      super.a();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.remote_explorer.server.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */