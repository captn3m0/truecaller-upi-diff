package com.truecaller.remote_explorer.server;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.IBinder;
import android.support.v4.app.z.d;
import android.widget.Toast;
import com.truecaller.common.h.k;
import com.truecaller.remote_explorer.R.drawable;
import com.truecaller.remote_explorer.R.id;
import com.truecaller.remote_explorer.R.string;
import com.truecaller.remote_explorer.a.c;
import java.io.IOException;

public class HttpServerService
  extends Service
{
  private a b;
  
  public static void a(Context paramContext)
  {
    Intent localIntent = new android/content/Intent;
    Class localClass = HttpServerService.class;
    localIntent.<init>(paramContext, localClass);
    localIntent.addFlags(32);
    boolean bool = k.a(paramContext);
    if (bool)
    {
      int i = Build.VERSION.SDK_INT;
      int j = 26;
      if (i >= j)
      {
        paramContext.startForegroundService(localIntent);
        return;
      }
      try
      {
        paramContext.startService(localIntent);
        return;
      }
      catch (SecurityException paramContext)
      {
        paramContext.printStackTrace();
      }
    }
  }
  
  public IBinder onBind(Intent paramIntent)
  {
    return null;
  }
  
  public void onDestroy()
  {
    Context localContext = getBaseContext();
    int i = R.string.serverStopped;
    String str = getString(i);
    Toast.makeText(localContext, str, 1).show();
    b.b();
  }
  
  public int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
  {
    paramInt1 = 1;
    if (paramIntent != null)
    {
      try
      {
        i = R.string.serverStarting;
        paramIntent = getString(i);
        paramIntent = Toast.makeText(this, paramIntent, paramInt1);
        paramIntent.show();
        paramIntent = new com/truecaller/remote_explorer/server/a;
        localObject1 = new com/truecaller/remote_explorer/preferences/a;
        ((com.truecaller.remote_explorer.preferences.a)localObject1).<init>();
        localObject2 = new com/truecaller/remote_explorer/a/c;
        ((c)localObject2).<init>();
        paramIntent.<init>(this, (com.truecaller.remote_explorer.preferences.a)localObject1, (c)localObject2);
        b = paramIntent;
        paramIntent = b;
        paramIntent.a();
      }
      catch (IOException paramIntent)
      {
        paramIntent.printStackTrace();
      }
      int i = R.id.http_server_service_foreground_notification;
      paramInt2 = Build.VERSION.SDK_INT;
      int j = 26;
      if (paramInt2 >= j)
      {
        localObject1 = new android/app/NotificationChannel;
        localObject3 = "httpchannel";
        k = 3;
        ((NotificationChannel)localObject1).<init>("httpserver_notification_channel_id", (CharSequence)localObject3, k);
        localObject2 = (NotificationManager)getSystemService(NotificationManager.class);
        boolean bool = a;
        if ((!bool) && (localObject2 == null))
        {
          paramIntent = new java/lang/AssertionError;
          paramIntent.<init>();
          throw paramIntent;
        }
        ((NotificationManager)localObject2).createNotificationChannel((NotificationChannel)localObject1);
      }
      Object localObject1 = new android/content/Intent;
      Object localObject3 = Uri.parse("truecaller://home/qamenu");
      ((Intent)localObject1).<init>("android.intent.action.VIEW", (Uri)localObject3);
      j = 0;
      Object localObject2 = null;
      localObject1 = PendingIntent.getActivity(this, 0, (Intent)localObject1, 0);
      localObject3 = new android/support/v4/app/z$d;
      ((z.d)localObject3).<init>(this, "httpserver_notification_channel_id");
      int k = R.drawable.ic_stat_name;
      localObject3 = ((z.d)localObject3).a(k);
      k = R.string.serverRunningNotifTitle;
      String str = getString(k);
      localObject3 = ((z.d)localObject3).a(str);
      k = R.string.serverRunningNotifMessage;
      str = getString(k);
      localObject3 = ((z.d)localObject3).b(str);
      l = 0;
      f = ((PendingIntent)localObject1);
      localObject1 = ((z.d)localObject3).h();
      startForeground(i, (Notification)localObject1);
    }
    return paramInt1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.remote_explorer.server.HttpServerService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */