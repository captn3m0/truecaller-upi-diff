package com.truecaller.remote_explorer.preferences;

public enum PreferenceItem$Type
{
  static
  {
    Object localObject = new com/truecaller/remote_explorer/preferences/PreferenceItem$Type;
    ((Type)localObject).<init>("Integer", 0);
    Integer = (Type)localObject;
    localObject = new com/truecaller/remote_explorer/preferences/PreferenceItem$Type;
    int i = 1;
    ((Type)localObject).<init>("Long", i);
    Long = (Type)localObject;
    localObject = new com/truecaller/remote_explorer/preferences/PreferenceItem$Type;
    int j = 2;
    ((Type)localObject).<init>("Boolean", j);
    Boolean = (Type)localObject;
    localObject = new com/truecaller/remote_explorer/preferences/PreferenceItem$Type;
    int k = 3;
    ((Type)localObject).<init>("String", k);
    String = (Type)localObject;
    localObject = new com/truecaller/remote_explorer/preferences/PreferenceItem$Type;
    int m = 4;
    ((Type)localObject).<init>("StringSet", m);
    StringSet = (Type)localObject;
    localObject = new com/truecaller/remote_explorer/preferences/PreferenceItem$Type;
    int n = 5;
    ((Type)localObject).<init>("Float", n);
    Float = (Type)localObject;
    localObject = new Type[6];
    Type localType = Integer;
    localObject[0] = localType;
    localType = Long;
    localObject[i] = localType;
    localType = Boolean;
    localObject[j] = localType;
    localType = String;
    localObject[k] = localType;
    localType = StringSet;
    localObject[m] = localType;
    localType = Float;
    localObject[n] = localType;
    $VALUES = (Type[])localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.remote_explorer.preferences.PreferenceItem.Type
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */