package com.truecaller.remote_explorer.preferences;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.io.Serializable;
import java.util.List;
import org.a.a.b;

public class PreferenceFile
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  public String a;
  public String b;
  public b c;
  public List d;
  private String e;
  private String f;
  
  static
  {
    PreferenceFile.1 local1 = new com/truecaller/remote_explorer/preferences/PreferenceFile$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  private PreferenceFile(Parcel paramParcel)
  {
    Object localObject = paramParcel.readString();
    e = ((String)localObject);
    localObject = paramParcel.readString();
    a = ((String)localObject);
    localObject = paramParcel.readString();
    b = ((String)localObject);
    localObject = paramParcel.readString();
    f = ((String)localObject);
    localObject = (b)paramParcel.readSerializable();
    c = ((b)localObject);
    localObject = PreferenceItem.CREATOR;
    paramParcel = paramParcel.createTypedArrayList((Parcelable.Creator)localObject);
    d = paramParcel;
  }
  
  private PreferenceFile(PreferenceFile.a parama)
  {
    Object localObject = a;
    e = ((String)localObject);
    localObject = b;
    a = ((String)localObject);
    localObject = c;
    b = ((String)localObject);
    localObject = d;
    f = ((String)localObject);
    localObject = e;
    c = ((b)localObject);
    parama = f;
    d = parama;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public String toString()
  {
    StringBuffer localStringBuffer = new java/lang/StringBuffer;
    localStringBuffer.<init>("PreferenceFile{");
    localStringBuffer.append("rawName='");
    Object localObject = e;
    localStringBuffer.append((String)localObject);
    char c1 = '\'';
    localStringBuffer.append(c1);
    localStringBuffer.append(", packageName='");
    String str = a;
    localStringBuffer.append(str);
    localStringBuffer.append(c1);
    localStringBuffer.append(", fileName='");
    str = b;
    localStringBuffer.append(str);
    localStringBuffer.append(c1);
    localStringBuffer.append(", absolutePath='");
    str = f;
    localStringBuffer.append(str);
    localStringBuffer.append(c1);
    localStringBuffer.append(", lastModified=");
    localObject = c;
    localStringBuffer.append(localObject);
    localStringBuffer.append(", items=");
    localObject = d;
    localStringBuffer.append(localObject);
    localStringBuffer.append('}');
    return localStringBuffer.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    Object localObject = e;
    paramParcel.writeString((String)localObject);
    localObject = a;
    paramParcel.writeString((String)localObject);
    localObject = b;
    paramParcel.writeString((String)localObject);
    localObject = f;
    paramParcel.writeString((String)localObject);
    localObject = c;
    paramParcel.writeSerializable((Serializable)localObject);
    localObject = d;
    paramParcel.writeTypedList((List)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.remote_explorer.preferences.PreferenceFile
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */