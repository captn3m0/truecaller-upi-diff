package com.truecaller.remote_explorer.preferences;

final class a$a
{
  final String a;
  final String b;
  
  a$a(String paramString1, String paramString2)
  {
    a = paramString1;
    b = paramString2;
  }
  
  public final String toString()
  {
    StringBuffer localStringBuffer = new java/lang/StringBuffer;
    localStringBuffer.<init>("PackageFilePair{");
    localStringBuffer.append("packageName='");
    String str1 = a;
    localStringBuffer.append(str1);
    char c = '\'';
    localStringBuffer.append(c);
    localStringBuffer.append(", fileName='");
    String str2 = b;
    localStringBuffer.append(str2);
    localStringBuffer.append(c);
    localStringBuffer.append('}');
    return localStringBuffer.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.remote_explorer.preferences.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */