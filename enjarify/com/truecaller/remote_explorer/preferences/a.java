package com.truecaller.remote_explorer.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import org.a.a.b;

public class a
{
  public static final String a = "a";
  
  private static a.a a(String paramString)
  {
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    boolean bool = TextUtils.isEmpty(paramString);
    if (!bool)
    {
      String str1 = "\\.";
      paramString = paramString.split(str1);
      int i = paramString.length + -2;
      if (i >= 0)
      {
        str2 = paramString[i];
        int j = 0;
        while (j < i)
        {
          String str3 = paramString[j];
          ((StringBuilder)localObject).append(str3);
          str3 = ".";
          ((StringBuilder)localObject).append(str3);
          j += 1;
        }
        int k = ((StringBuilder)localObject).length();
        if (k <= 0) {
          break label115;
        }
        k = ((StringBuilder)localObject).length() + -1;
        ((StringBuilder)localObject).deleteCharAt(k);
        break label115;
      }
    }
    String str2 = null;
    label115:
    paramString = new com/truecaller/remote_explorer/preferences/a$a;
    localObject = ((StringBuilder)localObject).toString();
    paramString.<init>((String)localObject, str2);
    return paramString;
  }
  
  public static List a(Context paramContext)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Object localObject1 = paramContext.getPackageManager();
    int i = 128;
    localObject1 = ((PackageManager)localObject1).getInstalledApplications(i).iterator();
    do
    {
      bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      localApplicationInfo = (ApplicationInfo)((Iterator)localObject1).next();
      str1 = packageName;
      str2 = "com.truecaller";
      bool2 = str1.equals(str2);
      if (bool2) {
        break label111;
      }
      str1 = packageName;
      str2 = "com.truecaller.debug";
      bool2 = str1.equals(str2);
    } while (!bool2);
    break label111;
    boolean bool1 = false;
    ApplicationInfo localApplicationInfo = null;
    label111:
    if (localApplicationInfo == null) {
      return localArrayList;
    }
    localObject1 = new java/io/File;
    String str1 = dataDir;
    String str2 = "shared_prefs";
    ((File)localObject1).<init>(str1, str2);
    boolean bool2 = ((File)localObject1).exists();
    if (bool2)
    {
      bool2 = ((File)localObject1).isDirectory();
      if (bool2)
      {
        localObject1 = ((File)localObject1).listFiles();
        if (localObject1 == null) {
          return localArrayList;
        }
        int j = localObject1.length;
        str2 = null;
        int k = 0;
        while (k < j)
        {
          Object localObject2 = localObject1[k];
          PreferenceFile.a locala = new com/truecaller/remote_explorer/preferences/PreferenceFile$a;
          locala.<init>();
          Object localObject3 = ((File)localObject2).getName();
          a.a locala1 = a((String)localObject3);
          Object localObject4 = packageName;
          localObject4 = paramContext.createPackageContext((String)localObject4, 0);
          Object localObject5 = packageName;
          g = ((String)localObject5);
          a = ((String)localObject3);
          long l = ((File)localObject2).lastModified();
          localObject3 = new org/a/a/b;
          ((b)localObject3).<init>();
          e = ((b)localObject3);
          e.a_(l);
          localObject3 = a;
          b = ((String)localObject3);
          localObject2 = ((File)localObject2).getAbsolutePath();
          d = ((String)localObject2);
          localObject2 = Locale.ENGLISH;
          int m = 2;
          localObject5 = new Object[m];
          String str3 = a;
          localObject5[0] = str3;
          str3 = b;
          int n = 1;
          localObject5[n] = str3;
          localObject2 = String.format((Locale)localObject2, "%s.%s", (Object[])localObject5);
          localObject2 = a(((Context)localObject4).getSharedPreferences((String)localObject2, 0));
          f = ((List)localObject2);
          localObject2 = b;
          c = ((String)localObject2);
          localObject2 = a;
          localObject3 = paramContext.getPackageName();
          boolean bool3 = ((String)localObject2).contains((CharSequence)localObject3);
          if (bool3)
          {
            localObject2 = locala.a();
            localArrayList.add(0, localObject2);
          }
          else
          {
            localObject2 = locala.a();
            localArrayList.add(localObject2);
          }
          k += 1;
        }
      }
    }
    paramContext = -..Lambda.a.Z7s58cNh1HAa1HvrlAClHXNeaJU.INSTANCE;
    Collections.sort(localArrayList, paramContext);
    return localArrayList;
  }
  
  private static List a(SharedPreferences paramSharedPreferences)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    paramSharedPreferences = paramSharedPreferences.getAll();
    Iterator localIterator = paramSharedPreferences.keySet().iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      String str = (String)localIterator.next();
      Object localObject1 = paramSharedPreferences.get(str);
      if (localObject1 != null)
      {
        localObject1 = new com/truecaller/remote_explorer/preferences/PreferenceItem;
        Object localObject2 = paramSharedPreferences.get(str);
        ((PreferenceItem)localObject1).<init>(str, localObject2);
        localArrayList.add(localObject1);
      }
    }
    return localArrayList;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.remote_explorer.preferences.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */