package com.truecaller.remote_explorer.preferences;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.io.Serializable;
import java.util.Set;

public class PreferenceItem
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  public final String a;
  public Object b;
  
  static
  {
    PreferenceItem.1 local1 = new com/truecaller/remote_explorer/preferences/PreferenceItem$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  private PreferenceItem(Parcel paramParcel)
  {
    Object localObject = paramParcel.readString();
    a = ((String)localObject);
    localObject = (PreferenceItem.Type)paramParcel.readSerializable();
    paramParcel = paramParcel.readString();
    int[] arrayOfInt = PreferenceItem.2.a;
    int i = ((PreferenceItem.Type)localObject).ordinal();
    i = arrayOfInt[i];
    arrayOfInt = null;
    switch (i)
    {
    default: 
      paramParcel = null;
      break;
    case 6: 
      paramParcel = Float.valueOf(paramParcel);
      break;
    case 5: 
      paramParcel = null;
      break;
    case 3: 
      paramParcel = Boolean.valueOf(paramParcel);
      break;
    case 2: 
      paramParcel = Long.valueOf(paramParcel);
      break;
    case 1: 
      paramParcel = Integer.valueOf(paramParcel);
    }
    b = paramParcel;
  }
  
  PreferenceItem(String paramString, Object paramObject)
  {
    a = paramString;
    b = paramObject;
  }
  
  public final PreferenceItem.Type a()
  {
    Object localObject = b;
    boolean bool1 = localObject instanceof Integer;
    if (bool1) {
      return PreferenceItem.Type.Integer;
    }
    bool1 = localObject instanceof Long;
    if (bool1) {
      return PreferenceItem.Type.Long;
    }
    bool1 = localObject instanceof Boolean;
    if (bool1) {
      return PreferenceItem.Type.Boolean;
    }
    bool1 = localObject instanceof String;
    if (bool1) {
      return PreferenceItem.Type.String;
    }
    bool1 = localObject instanceof Set;
    if (bool1) {
      return PreferenceItem.Type.StringSet;
    }
    boolean bool2 = localObject instanceof Float;
    if (bool2) {
      return PreferenceItem.Type.Float;
    }
    localObject = new java/lang/IllegalArgumentException;
    ((IllegalArgumentException)localObject).<init>();
    throw ((Throwable)localObject);
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    Object localObject = a;
    paramParcel.writeString((String)localObject);
    localObject = a();
    paramParcel.writeSerializable((Serializable)localObject);
    localObject = String.valueOf(b);
    paramParcel.writeString((String)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.remote_explorer.preferences.PreferenceItem
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */