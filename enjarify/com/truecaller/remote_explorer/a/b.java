package com.truecaller.remote_explorer.a;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class b
  extends SQLiteOpenHelper
{
  public b(Context paramContext, a parama)
  {
    super(paramContext, str, null, i);
  }
  
  public final List a()
  {
    Object localObject = a("SELECT name FROM sqlite_master WHERE type='view'");
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    localObject = ((List)localObject).iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject).hasNext();
      if (!bool) {
        break;
      }
      String str = ((String[])localObject.next())[0];
      localArrayList.add(str);
    }
    return localArrayList;
  }
  
  /* Error */
  public final List a(String paramString)
  {
    // Byte code:
    //   0: new 25	java/util/ArrayList
    //   3: astore_2
    //   4: aload_2
    //   5: invokespecial 28	java/util/ArrayList:<init>	()V
    //   8: aload_0
    //   9: invokevirtual 54	com/truecaller/remote_explorer/a/b:getWritableDatabase	()Landroid/database/sqlite/SQLiteDatabase;
    //   12: astore_3
    //   13: aload_3
    //   14: aload_1
    //   15: aconst_null
    //   16: invokevirtual 60	android/database/sqlite/SQLiteDatabase:rawQuery	(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    //   19: astore_1
    //   20: aload_1
    //   21: ifnull +172 -> 193
    //   24: aload_1
    //   25: invokeinterface 65 1 0
    //   30: istore 4
    //   32: iload 4
    //   34: ifeq +159 -> 193
    //   37: aload_1
    //   38: invokeinterface 69 1 0
    //   43: astore 5
    //   45: aload_2
    //   46: aload 5
    //   48: invokeinterface 50 2 0
    //   53: pop
    //   54: aload_1
    //   55: invokeinterface 73 1 0
    //   60: istore 4
    //   62: iload 4
    //   64: anewarray 75	java/lang/String
    //   67: astore 5
    //   69: iconst_0
    //   70: istore 6
    //   72: aconst_null
    //   73: astore 7
    //   75: aload_1
    //   76: invokeinterface 73 1 0
    //   81: istore 8
    //   83: iload 6
    //   85: iload 8
    //   87: if_icmpge +29 -> 116
    //   90: aload_1
    //   91: iload 6
    //   93: invokeinterface 79 2 0
    //   98: astore 9
    //   100: aload 5
    //   102: iload 6
    //   104: aload 9
    //   106: aastore
    //   107: iload 6
    //   109: iconst_1
    //   110: iadd
    //   111: istore 6
    //   113: goto -38 -> 75
    //   116: aload_2
    //   117: aload 5
    //   119: invokeinterface 50 2 0
    //   124: pop
    //   125: aload_1
    //   126: invokeinterface 82 1 0
    //   131: istore 4
    //   133: iload 4
    //   135: ifne -81 -> 54
    //   138: goto +55 -> 193
    //   141: astore_2
    //   142: goto +35 -> 177
    //   145: astore 5
    //   147: iconst_1
    //   148: istore 6
    //   150: iload 6
    //   152: anewarray 75	java/lang/String
    //   155: astore 7
    //   157: aload 5
    //   159: invokevirtual 89	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   162: astore 5
    //   164: aload 7
    //   166: iconst_0
    //   167: aload 5
    //   169: aastore
    //   170: aload_1
    //   171: ifnull +32 -> 203
    //   174: goto +23 -> 197
    //   177: aload_1
    //   178: ifnull +9 -> 187
    //   181: aload_1
    //   182: invokeinterface 92 1 0
    //   187: aload_3
    //   188: invokevirtual 93	android/database/sqlite/SQLiteDatabase:close	()V
    //   191: aload_2
    //   192: athrow
    //   193: aload_1
    //   194: ifnull +9 -> 203
    //   197: aload_1
    //   198: invokeinterface 92 1 0
    //   203: aload_3
    //   204: invokevirtual 93	android/database/sqlite/SQLiteDatabase:close	()V
    //   207: aload_2
    //   208: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	209	0	this	b
    //   0	209	1	paramString	String
    //   3	114	2	localArrayList	ArrayList
    //   141	67	2	localList	List
    //   12	192	3	localSQLiteDatabase	SQLiteDatabase
    //   30	3	4	bool1	boolean
    //   60	3	4	i	int
    //   131	3	4	bool2	boolean
    //   43	75	5	arrayOfString1	String[]
    //   145	13	5	localException	Exception
    //   162	6	5	str1	String
    //   70	81	6	j	int
    //   73	92	7	arrayOfString2	String[]
    //   81	7	8	k	int
    //   98	7	9	str2	String
    // Exception table:
    //   from	to	target	type
    //   24	30	141	finally
    //   37	43	141	finally
    //   46	54	141	finally
    //   54	60	141	finally
    //   62	67	141	finally
    //   75	81	141	finally
    //   91	98	141	finally
    //   104	107	141	finally
    //   117	125	141	finally
    //   125	131	141	finally
    //   150	155	141	finally
    //   157	162	141	finally
    //   167	170	141	finally
    //   24	30	145	java/lang/Exception
    //   37	43	145	java/lang/Exception
    //   46	54	145	java/lang/Exception
    //   54	60	145	java/lang/Exception
    //   62	67	145	java/lang/Exception
    //   75	81	145	java/lang/Exception
    //   91	98	145	java/lang/Exception
    //   104	107	145	java/lang/Exception
    //   117	125	145	java/lang/Exception
    //   125	131	145	java/lang/Exception
  }
  
  /* Error */
  public final int b(String paramString)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 96	com/truecaller/remote_explorer/a/b:getReadableDatabase	()Landroid/database/sqlite/SQLiteDatabase;
    //   4: astore_2
    //   5: aload_1
    //   6: invokestatic 102	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   9: astore_1
    //   10: ldc 98
    //   12: aload_1
    //   13: invokevirtual 106	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   16: astore_1
    //   17: aload_2
    //   18: aload_1
    //   19: aconst_null
    //   20: invokevirtual 60	android/database/sqlite/SQLiteDatabase:rawQuery	(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    //   23: astore_1
    //   24: iconst_0
    //   25: istore_3
    //   26: aconst_null
    //   27: astore 4
    //   29: aload_1
    //   30: ifnull +81 -> 111
    //   33: aload_1
    //   34: invokeinterface 65 1 0
    //   39: istore 5
    //   41: iload 5
    //   43: ifeq +68 -> 111
    //   46: aload_1
    //   47: iconst_0
    //   48: invokeinterface 110 2 0
    //   53: istore_3
    //   54: goto +57 -> 111
    //   57: astore 4
    //   59: goto +35 -> 94
    //   62: astore 6
    //   64: iconst_1
    //   65: istore 7
    //   67: iload 7
    //   69: anewarray 75	java/lang/String
    //   72: astore 8
    //   74: aload 6
    //   76: invokevirtual 89	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   79: astore 6
    //   81: aload 8
    //   83: iconst_0
    //   84: aload 6
    //   86: aastore
    //   87: aload_1
    //   88: ifnull +33 -> 121
    //   91: goto +24 -> 115
    //   94: aload_1
    //   95: ifnull +9 -> 104
    //   98: aload_1
    //   99: invokeinterface 92 1 0
    //   104: aload_2
    //   105: invokevirtual 93	android/database/sqlite/SQLiteDatabase:close	()V
    //   108: aload 4
    //   110: athrow
    //   111: aload_1
    //   112: ifnull +9 -> 121
    //   115: aload_1
    //   116: invokeinterface 92 1 0
    //   121: aload_2
    //   122: invokevirtual 93	android/database/sqlite/SQLiteDatabase:close	()V
    //   125: iload_3
    //   126: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	127	0	this	b
    //   0	127	1	paramString	String
    //   4	118	2	localSQLiteDatabase	SQLiteDatabase
    //   25	101	3	i	int
    //   27	1	4	localObject1	Object
    //   57	52	4	localObject2	Object
    //   39	3	5	bool	boolean
    //   62	13	6	localException	Exception
    //   79	6	6	str	String
    //   65	3	7	j	int
    //   72	10	8	arrayOfString	String[]
    // Exception table:
    //   from	to	target	type
    //   33	39	57	finally
    //   47	53	57	finally
    //   67	72	57	finally
    //   74	79	57	finally
    //   84	87	57	finally
    //   33	39	62	java/lang/Exception
    //   47	53	62	java/lang/Exception
  }
  
  public final void onCreate(SQLiteDatabase paramSQLiteDatabase) {}
  
  public final void onUpgrade(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2) {}
}

/* Location:
 * Qualified Name:     com.truecaller.remote_explorer.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */