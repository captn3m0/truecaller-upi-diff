package com.truecaller.remote_explorer.activities;

import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.truecaller.common.h.k;
import com.truecaller.remote_explorer.R.id;
import com.truecaller.remote_explorer.R.layout;
import com.truecaller.remote_explorer.preferences.PreferenceFile;
import com.truecaller.remote_explorer.preferences.PreferenceItem;
import com.truecaller.remote_explorer.preferences.PreferenceItem.Type;
import java.util.List;
import org.a.a.d.b;

public class PreferenceDetailActivity
  extends AppCompatActivity
  implements a.a
{
  private PreferenceFile a;
  private RecyclerView b;
  private List c;
  private boolean d = false;
  
  public static Intent a(Context paramContext, PreferenceFile paramPreferenceFile)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, PreferenceDetailActivity.class);
    localIntent.putExtra("PREFERENCE_ITEMS", paramPreferenceFile);
    return localIntent;
  }
  
  public final void a(int paramInt)
  {
    PreferenceItem localPreferenceItem = (PreferenceItem)c.get(paramInt);
    Object localObject1 = localPreferenceItem.a();
    Object localObject2 = PreferenceItem.Type.Boolean;
    if (localObject1 == localObject2) {
      return;
    }
    localObject1 = new android/support/v7/app/AlertDialog$Builder;
    ((AlertDialog.Builder)localObject1).<init>(this);
    localObject2 = LayoutInflater.from(this);
    int i = R.layout.item_edit_preference_other;
    int j = 0;
    Object localObject3 = null;
    localObject2 = ((LayoutInflater)localObject2).inflate(i, null, false);
    i = R.id.value;
    EditText localEditText = (EditText)((View)localObject2).findViewById(i);
    Object localObject4 = String.valueOf(b);
    localEditText.setText((CharSequence)localObject4);
    ((AlertDialog.Builder)localObject1).setView((View)localObject2);
    int k = R.id.dateTime;
    localObject2 = (TextView)((View)localObject2).findViewById(k);
    localObject4 = localPreferenceItem.a();
    PreferenceItem.Type localType = PreferenceItem.Type.Long;
    if (localObject4 != localType) {
      j = 8;
    }
    ((TextView)localObject2).setVisibility(j);
    localObject4 = localPreferenceItem.a();
    localObject3 = PreferenceItem.Type.Long;
    if (localObject4 == localObject3)
    {
      localObject4 = org.a.a.d.a.a("yyyy-MM-dd hh:mm aa");
      localObject3 = (Long)b;
      long l = ((Long)localObject3).longValue();
      localObject4 = ((b)localObject4).a(l);
      ((TextView)localObject2).setText((CharSequence)localObject4);
    }
    localObject2 = a;
    ((AlertDialog.Builder)localObject1).setTitle((CharSequence)localObject2);
    localObject4 = new com/truecaller/remote_explorer/activities/-$$Lambda$PreferenceDetailActivity$SYvjoLA2gF6c5mb4wNm8DeYATxk;
    ((-..Lambda.PreferenceDetailActivity.SYvjoLA2gF6c5mb4wNm8DeYATxk)localObject4).<init>(this, localEditText, localPreferenceItem);
    ((AlertDialog.Builder)localObject1).setPositiveButton("Save", (DialogInterface.OnClickListener)localObject4);
    ((AlertDialog.Builder)localObject1).show();
  }
  
  public final void a(PreferenceItem paramPreferenceItem, boolean paramBoolean)
  {
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>();
    Object localObject2 = a.a;
    ((StringBuilder)localObject1).append((String)localObject2);
    ((StringBuilder)localObject1).append(".");
    localObject2 = a.b;
    ((StringBuilder)localObject1).append((String)localObject2);
    localObject1 = ((StringBuilder)localObject1).toString();
    localObject1 = getSharedPreferences((String)localObject1, 0);
    localObject2 = Boolean.valueOf(paramBoolean);
    b = localObject2;
    d = true;
    localObject1 = ((SharedPreferences)localObject1).edit();
    paramPreferenceItem = a;
    ((SharedPreferences.Editor)localObject1).putBoolean(paramPreferenceItem, paramBoolean).apply();
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    boolean bool = k.a(this);
    if (!bool)
    {
      finish();
      return;
    }
    int i = R.layout.activity_preference_detail;
    setContentView(i);
    paramBundle = (PreferenceFile)getIntent().getParcelableExtra("PREFERENCE_ITEMS");
    a = paramBundle;
    paramBundle = a.d;
    c = paramBundle;
    i = R.id.search;
    paramBundle = (EditText)findViewById(i);
    Object localObject = new com/truecaller/remote_explorer/activities/PreferenceDetailActivity$1;
    ((PreferenceDetailActivity.1)localObject).<init>(this);
    paramBundle.addTextChangedListener((TextWatcher)localObject);
    i = R.id.recyclerView;
    paramBundle = (RecyclerView)findViewById(i);
    b = paramBundle;
    paramBundle = b;
    localObject = new android/support/v7/widget/LinearLayoutManager;
    ((LinearLayoutManager)localObject).<init>(this, 1, false);
    paramBundle.setLayoutManager((RecyclerView.LayoutManager)localObject);
    paramBundle = a.b;
    setTitle(paramBundle);
    paramBundle = b;
    localObject = new com/truecaller/remote_explorer/activities/a;
    List localList = c;
    ((a)localObject).<init>(localList, this);
    paramBundle.setAdapter((RecyclerView.Adapter)localObject);
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    boolean bool = d;
    if (!bool) {
      return;
    }
    Intent localIntent = new android/content/Intent;
    localIntent.<init>();
    localIntent.setAction("android.intent.action.MAIN");
    localIntent.setFlags(335577088);
    startActivity(localIntent);
    System.exit(0);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.remote_explorer.activities.PreferenceDetailActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */