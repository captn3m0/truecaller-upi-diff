package com.truecaller.remote_explorer.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.LayoutManager;
import com.truecaller.common.h.k;
import com.truecaller.remote_explorer.R.id;
import com.truecaller.remote_explorer.R.layout;
import com.truecaller.remote_explorer.preferences.PreferenceFile;
import com.truecaller.remote_explorer.preferences.a;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class PreferenceClientActivity
  extends AppCompatActivity
  implements b.a
{
  private final List a;
  
  public PreferenceClientActivity()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    a = localArrayList;
  }
  
  public static Intent a(Context paramContext)
  {
    Intent localIntent = new android/content/Intent;
    Class localClass = PreferenceClientActivity.class;
    localIntent.<init>(paramContext, localClass);
    boolean bool = paramContext instanceof Activity;
    if (!bool)
    {
      int i = 335544320;
      localIntent.setFlags(i);
    }
    return localIntent;
  }
  
  public final void a(int paramInt)
  {
    Object localObject = (PreferenceFile)a.get(paramInt);
    localObject = PreferenceDetailActivity.a(this, (PreferenceFile)localObject);
    startActivity((Intent)localObject);
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    boolean bool = k.a(this);
    if (bool)
    {
      int i = R.layout.activity_preference_client;
      setContentView(i);
      i = R.id.recyclerView;
      paramBundle = (RecyclerView)findViewById(i);
      Object localObject1 = new com/truecaller/remote_explorer/preferences/a;
      ((a)localObject1).<init>();
      try
      {
        localObject1 = a;
        ((List)localObject1).clear();
        localObject1 = a;
        localObject2 = a.a(this);
        ((List)localObject1).addAll((Collection)localObject2);
      }
      catch (PackageManager.NameNotFoundException localNameNotFoundException)
      {
        localNameNotFoundException.printStackTrace();
      }
      b localb = new com/truecaller/remote_explorer/activities/b;
      Object localObject2 = a;
      localb.<init>((List)localObject2, this);
      localObject2 = new android/support/v7/widget/LinearLayoutManager;
      ((LinearLayoutManager)localObject2).<init>(this, 1, false);
      paramBundle.setLayoutManager((RecyclerView.LayoutManager)localObject2);
      paramBundle.setAdapter(localb);
      return;
    }
    finish();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.remote_explorer.activities.PreferenceClientActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */