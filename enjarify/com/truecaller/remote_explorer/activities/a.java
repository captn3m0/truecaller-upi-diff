package com.truecaller.remote_explorer.activities;

import android.support.v7.widget.RecyclerView.Adapter;
import com.truecaller.remote_explorer.preferences.PreferenceItem;
import com.truecaller.remote_explorer.preferences.PreferenceItem.Type;
import java.util.List;

final class a
  extends RecyclerView.Adapter
{
  private final List a;
  private final a.a b;
  
  a(List paramList, a.a parama)
  {
    a = paramList;
    b = parama;
  }
  
  public final int getItemCount()
  {
    return a.size();
  }
  
  public final int getItemViewType(int paramInt)
  {
    PreferenceItem.Type localType = ((PreferenceItem)a.get(paramInt)).a();
    int[] arrayOfInt = a.1.a;
    paramInt = localType.ordinal();
    paramInt = arrayOfInt[paramInt];
    switch (paramInt)
    {
    default: 
      return -1;
    case 6: 
      return 1;
    }
    return 2;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.remote_explorer.activities.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */