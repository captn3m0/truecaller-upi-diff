package com.truecaller.remote_explorer.activities;

import android.content.Context;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.TextView;
import com.truecaller.remote_explorer.R.id;

final class b$b
  extends RecyclerView.ViewHolder
{
  TextView a;
  TextView b;
  TextView c;
  TextView d;
  final Context e;
  private final b.a f;
  
  public b$b(View paramView, b.a parama, Context paramContext)
  {
    super(paramView);
    int i = R.id.tvFileName;
    TextView localTextView = (TextView)paramView.findViewById(i);
    a = localTextView;
    i = R.id.tvPackage;
    localTextView = (TextView)paramView.findViewById(i);
    b = localTextView;
    i = R.id.tvUpdatedTime;
    localTextView = (TextView)paramView.findViewById(i);
    c = localTextView;
    i = R.id.tvItemsCount;
    localTextView = (TextView)paramView.findViewById(i);
    d = localTextView;
    f = parama;
    e = paramContext;
    parama = new com/truecaller/remote_explorer/activities/-$$Lambda$b$b$fhJTGp9lPdxW2_fSqszx6tQD7w0;
    parama.<init>(this);
    paramView.setOnClickListener(parama);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.remote_explorer.activities.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */