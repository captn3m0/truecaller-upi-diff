package com.truecaller.remote_explorer.activities;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import com.truecaller.remote_explorer.preferences.PreferenceFile;
import com.truecaller.remote_explorer.preferences.PreferenceItem;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

final class PreferenceDetailActivity$1
  implements TextWatcher
{
  PreferenceDetailActivity$1(PreferenceDetailActivity paramPreferenceDetailActivity) {}
  
  public final void afterTextChanged(Editable paramEditable)
  {
    paramEditable = paramEditable.toString();
    boolean bool1 = TextUtils.isEmpty(paramEditable);
    if (bool1)
    {
      paramEditable = a;
      localObject1 = ad;
      PreferenceDetailActivity.a(paramEditable, (List)localObject1);
      paramEditable = PreferenceDetailActivity.c(a);
      localObject1 = new com/truecaller/remote_explorer/activities/a;
      localObject2 = PreferenceDetailActivity.b(a);
      localObject3 = a;
      ((a)localObject1).<init>((List)localObject2, (a.a)localObject3);
      paramEditable.setAdapter((RecyclerView.Adapter)localObject1);
      return;
    }
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    Object localObject2 = aa).d.iterator();
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject2).hasNext();
      if (!bool2) {
        break;
      }
      localObject3 = (PreferenceItem)((Iterator)localObject2).next();
      String str = a.toLowerCase();
      boolean bool3 = str.contains(paramEditable);
      if (bool3) {
        ((List)localObject1).add(localObject3);
      }
    }
    PreferenceDetailActivity.a(a, (List)localObject1);
    paramEditable = PreferenceDetailActivity.c(a);
    localObject1 = new com/truecaller/remote_explorer/activities/a;
    localObject2 = PreferenceDetailActivity.b(a);
    Object localObject3 = a;
    ((a)localObject1).<init>((List)localObject2, (a.a)localObject3);
    paramEditable.setAdapter((RecyclerView.Adapter)localObject1);
  }
  
  public final void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
  
  public final void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
}

/* Location:
 * Qualified Name:     com.truecaller.remote_explorer.activities.PreferenceDetailActivity.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */