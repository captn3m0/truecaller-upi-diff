package com.truecaller.remote_explorer.activities;

import android.os.Bundle;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import com.truecaller.common.h.k;
import com.truecaller.remote_explorer.R.string;

public class AllowClientActivity
  extends AppCompatActivity
{
  public static String a = "state";
  public static String b = "HTTP_CLIENT_ALLOWED";
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    boolean bool = k.a(this);
    if (!bool)
    {
      finish();
      new String[1][0] = "onCreate:: Not qa device finishing";
      return;
    }
    paramBundle = new android/support/v7/app/AlertDialog$Builder;
    paramBundle.<init>(this);
    int i = R.string.connectClientPromptTitle;
    paramBundle.setTitle(i);
    i = R.string.connectClientPromptMessage;
    paramBundle.setMessage(i);
    i = R.string.connectClientPromptPositiveButton;
    -..Lambda.AllowClientActivity.9WYH-ct4La9mKImr-jnSkMcVDiY local9WYH-ct4La9mKImr-jnSkMcVDiY = new com/truecaller/remote_explorer/activities/-$$Lambda$AllowClientActivity$9WYH-ct4La9mKImr-jnSkMcVDiY;
    local9WYH-ct4La9mKImr-jnSkMcVDiY.<init>(this);
    paramBundle.setPositiveButton(i, local9WYH-ct4La9mKImr-jnSkMcVDiY);
    i = R.string.connectClientPromptNegativeButton;
    paramBundle.setNegativeButton(i, null);
    -..Lambda.AllowClientActivity.evcKcxgEIfM9vABb9lc2KiTQJlA localevcKcxgEIfM9vABb9lc2KiTQJlA = new com/truecaller/remote_explorer/activities/-$$Lambda$AllowClientActivity$evcKcxgEIfM9vABb9lc2KiTQJlA;
    localevcKcxgEIfM9vABb9lc2KiTQJlA.<init>(this);
    paramBundle.setOnDismissListener(localevcKcxgEIfM9vABb9lc2KiTQJlA);
    paramBundle.show();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.remote_explorer.activities.AllowClientActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */