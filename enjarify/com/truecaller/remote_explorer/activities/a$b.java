package com.truecaller.remote_explorer.activities;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Switch;
import android.widget.TextView;
import com.truecaller.remote_explorer.R.id;
import com.truecaller.remote_explorer.preferences.PreferenceItem;
import com.truecaller.remote_explorer.preferences.PreferenceItem.Type;
import org.a.a.d.a;
import org.a.a.d.b;

final class a$b
  extends RecyclerView.ViewHolder
{
  private TextView a;
  private Switch b;
  private TextView c;
  private TextView d;
  private final a.a e;
  
  public a$b(View paramView, a.a parama)
  {
    super(paramView);
    int i = R.id.key;
    Object localObject = (TextView)paramView.findViewById(i);
    a = ((TextView)localObject);
    i = R.id.valueSwitch;
    localObject = (Switch)paramView.findViewById(i);
    b = ((Switch)localObject);
    i = R.id.value;
    localObject = (TextView)paramView.findViewById(i);
    c = ((TextView)localObject);
    i = R.id.valueAlternative;
    paramView = (TextView)paramView.findViewById(i);
    d = paramView;
    e = parama;
  }
  
  public final void a(PreferenceItem paramPreferenceItem)
  {
    Object localObject1 = a;
    a.setText((CharSequence)localObject1);
    localObject1 = b;
    Object localObject2 = paramPreferenceItem.a();
    Object localObject3 = PreferenceItem.Type.Boolean;
    if (localObject2 == localObject3)
    {
      localObject2 = b;
      boolean bool1 = ((Boolean)localObject1).booleanValue();
      ((Switch)localObject2).setChecked(bool1);
      localObject1 = b;
      localObject2 = new com/truecaller/remote_explorer/activities/-$$Lambda$a$b$iZtq0wB4svilrZEM4M6xCzABuY0;
      ((-..Lambda.a.b.iZtq0wB4svilrZEM4M6xCzABuY0)localObject2).<init>(this, paramPreferenceItem);
      ((Switch)localObject1).setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)localObject2);
    }
    else
    {
      paramPreferenceItem = String.valueOf(localObject1);
      localObject3 = c;
      boolean bool2 = TextUtils.isEmpty(paramPreferenceItem);
      if (bool2) {
        paramPreferenceItem = "<not-set>";
      }
      ((TextView)localObject3).setText(paramPreferenceItem);
      paramPreferenceItem = PreferenceItem.Type.Long;
      if (localObject2 == paramPreferenceItem)
      {
        paramPreferenceItem = a.a("yyyy-MM-dd hh:mm aa");
        long l = ((Long)localObject1).longValue();
        paramPreferenceItem = paramPreferenceItem.a(l);
        d.setVisibility(0);
        localObject1 = d;
        localObject2 = "Time? : ";
        paramPreferenceItem = String.valueOf(paramPreferenceItem);
        paramPreferenceItem = ((String)localObject2).concat(paramPreferenceItem);
        ((TextView)localObject1).setText(paramPreferenceItem);
      }
      else
      {
        paramPreferenceItem = d;
        int i = 8;
        paramPreferenceItem.setVisibility(i);
      }
    }
    paramPreferenceItem = itemView;
    localObject1 = new com/truecaller/remote_explorer/activities/-$$Lambda$a$b$XtHy565Y4xfrOdYPlweCtd2HoNM;
    ((-..Lambda.a.b.XtHy565Y4xfrOdYPlweCtd2HoNM)localObject1).<init>(this);
    paramPreferenceItem.setOnClickListener((View.OnClickListener)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.remote_explorer.activities.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */