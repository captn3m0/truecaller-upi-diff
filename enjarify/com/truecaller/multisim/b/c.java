package com.truecaller.multisim.b;

import android.text.TextUtils;

public final class c
{
  private static boolean a;
  
  public static void a()
  {
    boolean bool = a;
    if (!bool) {
      return;
    }
    b();
  }
  
  public static void a(String... paramVarArgs)
  {
    boolean bool = a;
    if (!bool) {
      return;
    }
    b();
    TextUtils.join("\n", paramVarArgs);
  }
  
  private static String b()
  {
    Object localObject = Thread.currentThread().getStackTrace();
    int i = 4;
    for (;;)
    {
      int j = localObject.length;
      if (i <= j) {
        break;
      }
      i += -1;
    }
    localObject = localObject[i];
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    String str = ((StackTraceElement)localObject).getClassName();
    localStringBuilder.append(str);
    localStringBuilder.append(": ");
    int k = ((StackTraceElement)localObject).getLineNumber();
    localStringBuilder.append(k);
    return localStringBuilder.toString();
  }
  
  public static void b(String... paramVarArgs)
  {
    boolean bool = a;
    if (!bool) {
      return;
    }
    b();
    TextUtils.join("\n", paramVarArgs);
  }
  
  public static void c(String... paramVarArgs)
  {
    boolean bool = a;
    if (!bool) {
      return;
    }
    b();
    TextUtils.join("\n", paramVarArgs);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.multisim.b.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */