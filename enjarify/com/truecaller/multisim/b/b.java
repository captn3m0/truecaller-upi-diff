package com.truecaller.multisim.b;

import android.os.Build.VERSION;
import android.telephony.TelephonyManager;
import com.truecaller.multisim.SimInfo;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.List;

public final class b
{
  private final TelephonyManager a;
  
  public b(TelephonyManager paramTelephonyManager)
  {
    a = paramTelephonyManager;
  }
  
  public static boolean c(List paramList)
  {
    int i = paramList.size();
    return i == 0;
  }
  
  private int d(List paramList)
  {
    paramList = paramList.iterator();
    int i = 0;
    for (;;)
    {
      boolean bool1 = paramList.hasNext();
      if (!bool1) {
        break;
      }
      SimInfo localSimInfo = (SimInfo)paramList.next();
      int j = a;
      boolean bool2 = a(j);
      if (bool2) {
        i += 1;
      }
    }
    return i;
  }
  
  public final boolean a(int paramInt)
  {
    int i = Build.VERSION.SDK_INT;
    int j = 5;
    boolean bool = true;
    int k = 26;
    if (i >= k)
    {
      localObject1 = a;
      paramInt = ((TelephonyManager)localObject1).getSimState(paramInt);
      if (paramInt == j) {
        return bool;
      }
      return false;
    }
    Object localObject1 = TelephonyManager.class;
    Object localObject2 = "getSimState";
    try
    {
      Object localObject3 = new Class[bool];
      Class localClass = Integer.TYPE;
      localObject3[0] = localClass;
      localObject1 = ((Class)localObject1).getMethod((String)localObject2, (Class[])localObject3);
      localObject2 = a;
      localObject3 = new Object[bool];
      localObject4 = Integer.valueOf(paramInt);
      localObject3[0] = localObject4;
      localObject4 = ((Method)localObject1).invoke(localObject2, (Object[])localObject3);
      localObject4 = localObject4.toString();
      paramInt = Integer.parseInt((String)localObject4);
      if (paramInt == j) {
        return bool;
      }
      return false;
    }
    catch (Exception localException)
    {
      c.a();
      Object localObject4 = a;
      paramInt = ((TelephonyManager)localObject4).getSimState();
      if (paramInt == j) {
        return bool;
      }
    }
    return false;
  }
  
  public final boolean a(List paramList)
  {
    int i = d(paramList);
    int j = 1;
    if (i > j) {
      return j;
    }
    return false;
  }
  
  public final boolean b(List paramList)
  {
    int i = d(paramList);
    return i == 0;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.multisim.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */