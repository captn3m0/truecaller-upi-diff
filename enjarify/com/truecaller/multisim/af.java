package com.truecaller.multisim;

import android.os.SystemClock;
import java.util.HashMap;
import java.util.Map;

public final class af
  implements ae
{
  private long a;
  private final h b;
  private Map c;
  
  public af(h paramh)
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    c = localHashMap;
    b = paramh;
  }
  
  public final SimInfo a(String paramString)
  {
    long l1 = a + 3000L;
    long l2 = SystemClock.elapsedRealtime();
    boolean bool1 = l1 < l2;
    if (bool1)
    {
      bool2 = true;
    }
    else
    {
      bool2 = false;
      localObject = null;
    }
    if (bool2)
    {
      localObject = c;
      ((Map)localObject).clear();
    }
    l1 = SystemClock.elapsedRealtime();
    a = l1;
    Object localObject = c;
    boolean bool2 = ((Map)localObject).containsKey(paramString);
    if (bool2) {
      return (SimInfo)c.get(paramString);
    }
    localObject = b.b(paramString);
    c.put(paramString, localObject);
    return (SimInfo)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.multisim.af
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */