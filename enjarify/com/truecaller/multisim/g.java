package com.truecaller.multisim;

import android.database.Cursor;

final class g
  extends e
{
  private final h a;
  
  g(Cursor paramCursor, h paramh)
  {
    super(paramCursor, str);
    a = paramh;
  }
  
  public final String a(String paramString)
  {
    int i;
    try
    {
      i = Integer.parseInt(paramString);
    }
    catch (NumberFormatException localNumberFormatException)
    {
      i = -1;
    }
    if (i != 0)
    {
      int j = 1;
      if (i != j) {
        return "-1";
      }
    }
    h localh = a;
    paramString = localh.a(i);
    if (paramString != null) {
      return b;
    }
    return "-1";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.multisim.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */