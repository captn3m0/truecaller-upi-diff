package com.truecaller.multisim;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class SimInfo
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  public final int a;
  public final String b;
  public final String c;
  public final String d;
  public final String e;
  public final String f;
  public final String g;
  public final String h;
  public final String i;
  public final boolean j;
  
  static
  {
    SimInfo.1 local1 = new com/truecaller/multisim/SimInfo$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  public SimInfo(int paramInt, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, boolean paramBoolean)
  {
    a = paramInt;
    b = paramString1;
    c = paramString2;
    d = paramString3;
    e = paramString4;
    f = paramString5;
    g = paramString6;
    h = paramString7;
    i = paramString8;
    j = paramBoolean;
  }
  
  private SimInfo(Parcel paramParcel)
  {
    int k = paramParcel.readInt();
    a = k;
    String str = paramParcel.readString();
    b = str;
    str = paramParcel.readString();
    c = str;
    str = paramParcel.readString();
    d = str;
    str = paramParcel.readString();
    e = str;
    str = paramParcel.readString();
    f = str;
    str = paramParcel.readString();
    g = str;
    str = paramParcel.readString();
    h = str;
    str = paramParcel.readString();
    i = str;
    int m = paramParcel.readInt();
    k = 1;
    if (m != k)
    {
      k = 0;
      str = null;
    }
    j = k;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramInt = a;
    paramParcel.writeInt(paramInt);
    String str = b;
    paramParcel.writeString(str);
    str = c;
    paramParcel.writeString(str);
    str = d;
    paramParcel.writeString(str);
    str = e;
    paramParcel.writeString(str);
    str = f;
    paramParcel.writeString(str);
    str = g;
    paramParcel.writeString(str);
    str = h;
    paramParcel.writeString(str);
    str = i;
    paramParcel.writeString(str);
    paramInt = j;
    paramParcel.writeInt(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.multisim.SimInfo
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */