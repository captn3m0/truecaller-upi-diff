package com.truecaller.multisim;

import android.content.Context;
import android.content.Intent;
import com.mediatek.telephony.SmsManagerEx;
import com.mediatek.telephony.TelephonyManagerEx;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class x
  extends z
{
  static final j d = -..Lambda.x.QSkEXmvNKVUxW9sKarWZCrE64uU.INSTANCE;
  private final String e;
  private final String f;
  private final Method g;
  private final Method h;
  private final Field i;
  
  private x(Context paramContext, TelephonyManagerEx paramTelephonyManagerEx, SmsManagerEx paramSmsManagerEx)
  {
    super(paramContext, paramTelephonyManagerEx, paramSmsManagerEx);
    paramContext = Class.forName("android.provider.Telephony$Sms");
    paramContext = (String)paramContext.getField("SIM_ID").get(paramContext);
    e = paramContext;
    paramContext = Class.forName("android.provider.Telephony$Mms");
    paramContext = (String)paramContext.getField("SIM_ID").get(paramContext);
    f = paramContext;
    paramContext = Class.forName("com.mediatek.telephony.SimInfoManager");
    int j = 1;
    Class[] arrayOfClass = new Class[j];
    arrayOfClass[0] = Context.class;
    paramTelephonyManagerEx = paramContext.getMethod("getInsertedSimCount", arrayOfClass);
    g = paramTelephonyManagerEx;
    paramSmsManagerEx = new Class[j];
    paramSmsManagerEx[0] = Context.class;
    paramContext = paramContext.getMethod("getInsertedSimInfoList", paramSmsManagerEx);
    h = paramContext;
    paramContext = Class.forName("com.mediatek.telephony.SimInfoManager$SimInfoRecord").getField("mSimSlotId");
    i = paramContext;
  }
  
  private String c(Intent paramIntent)
  {
    String str = "simId";
    int j = -1;
    int k = paramIntent.getIntExtra(str, j);
    if (j == k) {
      return "-1";
    }
    return b(k);
  }
  
  public final String a()
  {
    return "Mediatek1";
  }
  
  public final String a(Intent paramIntent)
  {
    return c(paramIntent);
  }
  
  public final void a(Intent paramIntent, String paramString) {}
  
  public final String b(Intent paramIntent)
  {
    return c(paramIntent);
  }
  
  public final boolean e()
  {
    return false;
  }
  
  public final String f()
  {
    List localList = h();
    boolean bool = localList.isEmpty();
    if (!bool) {
      return get0b;
    }
    return "-1";
  }
  
  public final List h()
  {
    localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    try
    {
      Object localObject1 = h;
      boolean bool = false;
      Object localObject2 = null;
      int k = 1;
      Object localObject3 = new Object[k];
      Context localContext = a;
      localObject3[0] = localContext;
      localObject1 = ((Method)localObject1).invoke(null, (Object[])localObject3);
      localObject1 = (List)localObject1;
      if (localObject1 != null)
      {
        localObject1 = ((List)localObject1).iterator();
        for (;;)
        {
          bool = ((Iterator)localObject1).hasNext();
          if (!bool) {
            break;
          }
          localObject2 = ((Iterator)localObject1).next();
          localObject3 = i;
          localObject2 = ((Field)localObject3).get(localObject2);
          localObject2 = (Integer)localObject2;
          int j = ((Integer)localObject2).intValue();
          localObject2 = a(j);
          if (localObject2 != null) {
            localArrayList.add(localObject2);
          }
        }
      }
      return localArrayList;
    }
    catch (Exception localException) {}
  }
  
  public final boolean j()
  {
    try
    {
      Object localObject = g;
      int j = 1;
      Object[] arrayOfObject = new Object[j];
      Context localContext = a;
      arrayOfObject[0] = localContext;
      localObject = ((Method)localObject).invoke(null, arrayOfObject);
      localObject = (Integer)localObject;
      int k = ((Integer)localObject).intValue();
      if (k > j) {
        return j;
      }
      return false;
    }
    catch (Exception localException) {}
    return false;
  }
  
  public final boolean k()
  {
    return j();
  }
  
  public final boolean l()
  {
    return true;
  }
  
  public final String m()
  {
    return e;
  }
  
  public final String n()
  {
    return f;
  }
  
  protected final String o()
  {
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.multisim.x
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */