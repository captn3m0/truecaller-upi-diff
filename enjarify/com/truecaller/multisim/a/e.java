package com.truecaller.multisim.a;

import android.content.Context;
import android.telephony.TelephonyManager;

public final class e
  extends a
{
  private final TelephonyManager a;
  
  public e(Context paramContext)
  {
    paramContext = (TelephonyManager)paramContext.getSystemService("phone");
    a = paramContext;
  }
  
  public final int b(String paramString)
  {
    int i = a(paramString);
    int k = 1;
    int m = 2;
    int n = -1;
    int j;
    if (i == n)
    {
      paramString = a;
      j = paramString.isDataEnabled();
      if (j != 0) {
        return k;
      }
      return m;
    }
    TelephonyManager localTelephonyManager = a;
    paramString = localTelephonyManager.createForSubscriptionId(j);
    boolean bool = paramString.isDataEnabled();
    if (bool) {
      return k;
    }
    return m;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.multisim.a.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */