package com.truecaller.multisim.a;

import android.content.Context;
import android.net.ConnectivityManager;
import java.lang.reflect.Method;

public final class c
  extends a
{
  private final ConnectivityManager a;
  
  public c(Context paramContext)
  {
    paramContext = (ConnectivityManager)paramContext.getSystemService("connectivity");
    a = paramContext;
  }
  
  public final int b(String paramString)
  {
    boolean bool1 = false;
    try
    {
      Object localObject1 = a;
      localObject1 = localObject1.getClass();
      String str = "getMobileDataEnabled";
      Object localObject2 = new Class[0];
      localObject1 = ((Class)localObject1).getDeclaredMethod(str, (Class[])localObject2);
      if (localObject1 != null)
      {
        int i = 1;
        ((Method)localObject1).setAccessible(i);
        localObject2 = a;
        Object[] arrayOfObject = new Object[0];
        localObject1 = ((Method)localObject1).invoke(localObject2, arrayOfObject);
        boolean bool2 = localObject1 instanceof Boolean;
        if (bool2)
        {
          localObject1 = (Boolean)localObject1;
          bool1 = ((Boolean)localObject1).booleanValue();
          if (bool1) {
            return i;
          }
          return 2;
        }
        return 0;
      }
    }
    catch (Exception localException)
    {
      com.truecaller.multisim.b.c.a();
    }
    return 0;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.multisim.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */