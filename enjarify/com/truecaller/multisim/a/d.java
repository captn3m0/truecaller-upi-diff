package com.truecaller.multisim.a;

import android.content.Context;
import android.telephony.TelephonyManager;
import com.truecaller.multisim.b.c;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public final class d
  extends a
{
  private final TelephonyManager a;
  
  public d(Context paramContext)
  {
    paramContext = (TelephonyManager)paramContext.getSystemService("phone");
    a = paramContext;
  }
  
  private int a()
  {
    boolean bool1 = false;
    try
    {
      Object localObject1 = a;
      localObject1 = localObject1.getClass();
      String str = "getDataEnabled";
      Object localObject2 = new Class[0];
      localObject1 = ((Class)localObject1).getDeclaredMethod(str, (Class[])localObject2);
      if (localObject1 != null)
      {
        int i = 1;
        ((Method)localObject1).setAccessible(i);
        localObject2 = a;
        Object[] arrayOfObject = new Object[0];
        localObject1 = ((Method)localObject1).invoke(localObject2, arrayOfObject);
        boolean bool2 = localObject1 instanceof Boolean;
        if (bool2)
        {
          localObject1 = (Boolean)localObject1;
          bool1 = ((Boolean)localObject1).booleanValue();
          if (bool1) {
            return i;
          }
          return 2;
        }
      }
    }
    catch (NoSuchMethodException|SecurityException|IllegalAccessException|IllegalArgumentException|InvocationTargetException localNoSuchMethodException)
    {
      c.a();
    }
    return 0;
  }
  
  private int a(int paramInt)
  {
    try
    {
      Object localObject1 = a;
      localObject1 = localObject1.getClass();
      Object localObject2 = "getDataEnabled";
      int i = 1;
      Object localObject3 = new Class[i];
      Class localClass = Integer.class;
      localObject3[0] = localClass;
      localObject1 = ((Class)localObject1).getDeclaredMethod((String)localObject2, (Class[])localObject3);
      if (localObject1 != null)
      {
        ((Method)localObject1).setAccessible(i);
        localObject2 = a;
        localObject3 = new Object[i];
        Object localObject4 = Integer.valueOf(paramInt);
        localObject3[0] = localObject4;
        localObject4 = ((Method)localObject1).invoke(localObject2, (Object[])localObject3);
        boolean bool = localObject4 instanceof Boolean;
        if (bool)
        {
          localObject4 = (Boolean)localObject4;
          paramInt = ((Boolean)localObject4).booleanValue();
          if (paramInt != 0) {
            return i;
          }
          return 2;
        }
      }
    }
    catch (NoSuchMethodException|SecurityException|IllegalAccessException|IllegalArgumentException|InvocationTargetException localNoSuchMethodException)
    {
      c.a();
    }
    return 0;
  }
  
  public final int b(String paramString)
  {
    int i = a(paramString);
    int j = -1;
    if (i == j) {
      return a();
    }
    return a(i);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.multisim.a.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */