package com.truecaller.multisim;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;
import android.telephony.CarrierConfigManager;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import com.truecaller.multisim.b.c;
import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.List;
import org.c.a.a.a.b.a;

final class s
  extends q
{
  static final j h = -..Lambda.s.ZT0d0dl6KwY1QmnYrJdjnnWKtyo.INSTANCE;
  private SubscriptionManager i;
  
  private s(Context paramContext, SubscriptionManager paramSubscriptionManager, TelephonyManager paramTelephonyManager, TelecomManager paramTelecomManager, CarrierConfigManager paramCarrierConfigManager)
  {
    super(paramContext, paramSubscriptionManager, paramTelephonyManager, paramTelecomManager, paramCarrierConfigManager);
    i = paramSubscriptionManager;
  }
  
  private int p()
  {
    try
    {
      Object localObject1 = i;
      localObject1 = localObject1.getClass();
      Object localObject2 = "ACTIVE";
      localObject1 = ((Class)localObject1).getDeclaredField((String)localObject2);
      localObject2 = i;
      localObject1 = ((Field)localObject1).get(localObject2);
      localObject1 = (Integer)localObject1;
      return ((Integer)localObject1).intValue();
    }
    catch (Exception localException)
    {
      c.b(new String[] { "Cannot get sub ACTIVE constants from subscriptionManager." });
    }
    return 1;
  }
  
  public final d a(Cursor paramCursor)
  {
    g localg = new com/truecaller/multisim/g;
    localg.<init>(paramCursor, this);
    return localg;
  }
  
  public final String a()
  {
    return "MarshmallowHuawei";
  }
  
  public final void a(Intent paramIntent, String paramString)
  {
    PhoneAccountHandle localPhoneAccountHandle = h(paramString);
    if (localPhoneAccountHandle != null)
    {
      String str = "subscription";
      int j = a.a(paramString);
      paramIntent.putExtra(str, j);
      paramString = "android.telecom.extra.PHONE_ACCOUNT_HANDLE";
      paramIntent.putExtra(paramString, localPhoneAccountHandle);
    }
  }
  
  public final boolean k()
  {
    boolean bool1 = j();
    if (bool1)
    {
      int j = p();
      Object localObject1 = i.getActiveSubscriptionInfoList();
      int k = 1;
      if (localObject1 == null) {
        return k;
      }
      localObject1 = ((List)localObject1).iterator();
      int m = 0;
      for (;;)
      {
        boolean bool2 = ((Iterator)localObject1).hasNext();
        if (!bool2) {
          break;
        }
        Object localObject2 = (SubscriptionInfo)((Iterator)localObject1).next();
        try
        {
          Object localObject3 = localObject2.getClass();
          String str = "mStatus";
          localObject3 = ((Class)localObject3).getDeclaredField(str);
          localObject2 = ((Field)localObject3).get(localObject2);
          localObject2 = (Integer)localObject2;
          int n = ((Integer)localObject2).intValue();
          if (n == j) {
            m += 1;
          }
        }
        catch (Exception localException)
        {
          localObject2 = new String[] { "Cannot get mStatus from subscriptionInfo" };
          c.b((String[])localObject2);
          m += 1;
        }
      }
      if (m > k) {
        return k;
      }
      return false;
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.multisim.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */