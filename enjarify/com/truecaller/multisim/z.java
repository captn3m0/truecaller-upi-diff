package com.truecaller.multisim;

import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import com.mediatek.telephony.SmsManagerEx;
import com.mediatek.telephony.TelephonyManagerEx;
import java.util.ArrayList;

public abstract class z
  extends i
{
  private final TelephonyManagerEx d;
  private final SmsManagerEx e;
  
  z(Context paramContext, TelephonyManagerEx paramTelephonyManagerEx, SmsManagerEx paramSmsManagerEx)
  {
    super(paramContext);
    d = paramTelephonyManagerEx;
    e = paramSmsManagerEx;
  }
  
  public SimInfo a(int paramInt)
  {
    String str1 = b(paramInt);
    Object localObject = "-1";
    boolean bool1 = ((String)localObject).equals(str1);
    if (bool1) {
      return null;
    }
    SimInfo localSimInfo = new com/truecaller/multisim/SimInfo;
    String str2 = d(paramInt);
    String str3 = c(paramInt);
    String str4 = e(paramInt);
    String str5 = f(paramInt);
    String str6 = g(paramInt);
    String str7 = h(paramInt);
    boolean bool2 = i(paramInt);
    localObject = localSimInfo;
    localSimInfo.<init>(paramInt, str1, str2, str3, str4, str5, str6, str7, null, bool2);
    return localSimInfo;
  }
  
  public final boolean a(String paramString1, String paramString2, String paramString3, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2, String paramString4)
  {
    int i = h(paramString4);
    e.sendTextMessage(paramString1, paramString2, paramString3, paramPendingIntent1, paramPendingIntent2, i);
    return true;
  }
  
  public final boolean a(String paramString1, String paramString2, ArrayList paramArrayList1, ArrayList paramArrayList2, ArrayList paramArrayList3, String paramString3)
  {
    int i = h(paramString3);
    e.sendMultipartTextMessage(paramString1, paramString2, paramArrayList1, paramArrayList2, paramArrayList3, i);
    return true;
  }
  
  public SimInfo b(String paramString)
  {
    Object localObject = "-1";
    int i = ((String)localObject).equals(paramString);
    if (i != 0) {
      return null;
    }
    int j = h(paramString);
    if (j != 0)
    {
      i = 1;
      if (i != j) {
        return null;
      }
    }
    localObject = new com/truecaller/multisim/SimInfo;
    String str1 = d(j);
    String str2 = c(j);
    String str3 = e(j);
    String str4 = f(j);
    String str5 = g(j);
    String str6 = h(j);
    boolean bool = i(j);
    ((SimInfo)localObject).<init>(j, paramString, str1, str2, str3, str4, str5, str6, null, bool);
    return (SimInfo)localObject;
  }
  
  final String b(int paramInt)
  {
    TelephonyManagerEx localTelephonyManagerEx = d;
    String str = localTelephonyManagerEx.getSubscriberId(paramInt);
    if (str == null) {
      str = "-1";
    }
    return str;
  }
  
  public final a c(String paramString)
  {
    paramString = new com/truecaller/multisim/b;
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    paramString.<init>(localBundle);
    return paramString;
  }
  
  final String c(int paramInt)
  {
    return d.getSimOperatorName(paramInt);
  }
  
  final String d(int paramInt)
  {
    return d.getLine1Number(paramInt);
  }
  
  public final String d(String paramString)
  {
    int i = h(paramString);
    int j = -1;
    if (i != j) {
      return d.getNetworkCountryIso(i);
    }
    return null;
  }
  
  final String e(int paramInt)
  {
    return d.getSimOperator(paramInt);
  }
  
  public final String e(String paramString)
  {
    int i = h(paramString);
    int j = -1;
    if (i != j) {
      return f(i);
    }
    return null;
  }
  
  final String f(int paramInt)
  {
    return d.getSimCountryIso(paramInt);
  }
  
  final String g(int paramInt)
  {
    return d.getDeviceId(paramInt);
  }
  
  public final int h(String paramString)
  {
    Object localObject = d;
    int i = 0;
    localObject = ((TelephonyManagerEx)localObject).getSubscriberId(0);
    boolean bool1 = paramString.equals(localObject);
    if (bool1) {
      return 0;
    }
    localObject = d;
    i = 1;
    localObject = ((TelephonyManagerEx)localObject).getSubscriberId(i);
    boolean bool2 = paramString.equals(localObject);
    if (bool2) {
      return i;
    }
    return -1;
  }
  
  final String h(int paramInt)
  {
    return d.getSimSerialNumber(paramInt);
  }
  
  final boolean i(int paramInt)
  {
    return d.isNetworkRoaming(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.multisim.z
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */