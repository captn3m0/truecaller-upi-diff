package com.truecaller.multisim;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.CallLog.Calls;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public final class ab
  extends i
{
  static final j d = -..Lambda.ab.yHqUbsjIgc4BXDFZQN3lM1Yq5fU.INSTANCE;
  private final Method e;
  private final SmsManager f;
  private final SmsManager g;
  private final Method h;
  private final TelephonyManager i;
  private final TelephonyManager j;
  private final String k;
  
  private ab(Context paramContext)
  {
    super(paramContext);
    paramContext = Class.forName("android.telephony.MultiSimTelephonyManager");
    int m = 1;
    Object localObject1 = new Class[m];
    Object localObject2 = Integer.TYPE;
    localObject1[0] = localObject2;
    Object localObject3 = paramContext.getMethod("getDefault", (Class[])localObject1);
    localObject1 = new Object[m];
    localObject2 = Integer.valueOf(0);
    localObject1[0] = localObject2;
    localObject1 = (TelephonyManager)((Method)localObject3).invoke(null, (Object[])localObject1);
    i = ((TelephonyManager)localObject1);
    localObject1 = new Object[m];
    Object localObject4 = Integer.valueOf(m);
    localObject1[0] = localObject4;
    localObject3 = (TelephonyManager)((Method)localObject3).invoke(null, (Object[])localObject1);
    j = ((TelephonyManager)localObject3);
    localObject1 = new Class[0];
    paramContext = paramContext.getMethod("getDefault", (Class[])localObject1);
    e = paramContext;
    paramContext = Class.forName("com.android.internal.telephony.MultiSimManager");
    localObject1 = new Class[0];
    paramContext = paramContext.getMethod("isMultiSimSlot", (Class[])localObject1);
    h = paramContext;
    paramContext = Class.forName("android.telephony.MultiSimSmsManager");
    localObject1 = new Class[m];
    localObject4 = Integer.TYPE;
    localObject1[0] = localObject4;
    paramContext = paramContext.getMethod("getDefault", (Class[])localObject1);
    localObject3 = new Object[m];
    localObject1 = Integer.valueOf(0);
    localObject3[0] = localObject1;
    localObject3 = (SmsManager)paramContext.invoke(null, (Object[])localObject3);
    f = ((SmsManager)localObject3);
    localObject3 = new Object[m];
    Integer localInteger = Integer.valueOf(m);
    localObject3[0] = localInteger;
    paramContext = (SmsManager)paramContext.invoke(null, (Object[])localObject3);
    g = paramContext;
    paramContext = (String)CallLog.Calls.class.getField("SIM_ID").get(null);
    k = paramContext;
  }
  
  private String c(Intent paramIntent)
  {
    String str = "simSlot";
    paramIntent = paramIntent.getStringExtra(str);
    int m;
    if (paramIntent == null)
    {
      m = -1;
    }
    else
    {
      paramIntent = Integer.valueOf(paramIntent);
      m = paramIntent.intValue();
    }
    int n = 1;
    if (m == n) {
      paramIntent = j;
    } else {
      paramIntent = i;
    }
    paramIntent = paramIntent.getSubscriberId();
    if (paramIntent != null) {
      return paramIntent;
    }
    return "-1";
  }
  
  private SmsManager h(String paramString)
  {
    String str = i.getSubscriberId();
    boolean bool1 = paramString.equals(str);
    if (bool1)
    {
      paramString = f;
    }
    else
    {
      str = j.getSubscriberId();
      boolean bool2 = paramString.equals(str);
      if (bool2)
      {
        paramString = g;
      }
      else
      {
        bool2 = false;
        paramString = null;
      }
    }
    return paramString;
  }
  
  public final SimInfo a(int paramInt)
  {
    int m = 1;
    TelephonyManager localTelephonyManager;
    if (m == paramInt) {
      localTelephonyManager = j;
    } else {
      localTelephonyManager = i;
    }
    String str1 = localTelephonyManager.getSubscriberId();
    if (str1 == null) {
      return null;
    }
    SimInfo localSimInfo = new com/truecaller/multisim/SimInfo;
    String str2 = localTelephonyManager.getLine1Number();
    String str3 = localTelephonyManager.getSimOperatorName();
    String str4 = localTelephonyManager.getSimOperator();
    String str5 = localTelephonyManager.getSimCountryIso();
    String str6 = localTelephonyManager.getDeviceId();
    String str7 = localTelephonyManager.getSimSerialNumber();
    boolean bool = localTelephonyManager.isNetworkRoaming();
    localSimInfo.<init>(paramInt, str1, str2, str3, str4, str5, str6, str7, null, bool);
    return localSimInfo;
  }
  
  public final d a(Cursor paramCursor)
  {
    g localg = new com/truecaller/multisim/g;
    localg.<init>(paramCursor, this);
    return localg;
  }
  
  public final String a()
  {
    return "Samsung";
  }
  
  public final String a(Intent paramIntent)
  {
    return c(paramIntent);
  }
  
  public final void a(Intent paramIntent, String paramString)
  {
    paramString = b(paramString);
    if (paramString != null)
    {
      int m = a;
      paramIntent.putExtra("simSlot", m);
      String str = "subscriber_slot_id";
      int n = a;
      paramIntent.putExtra(str, n);
    }
  }
  
  public final boolean a(String paramString1, String paramString2, String paramString3, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2, String paramString4)
  {
    SmsManager localSmsManager = h(paramString4);
    if (localSmsManager != null)
    {
      localSmsManager.sendTextMessage(paramString1, paramString2, paramString3, paramPendingIntent1, paramPendingIntent2);
      return true;
    }
    return false;
  }
  
  public final boolean a(String paramString1, String paramString2, ArrayList paramArrayList1, ArrayList paramArrayList2, ArrayList paramArrayList3, String paramString3)
  {
    SmsManager localSmsManager = h(paramString3);
    if (localSmsManager != null)
    {
      localSmsManager.sendMultipartTextMessage(paramString1, paramString2, paramArrayList1, paramArrayList2, paramArrayList3);
      return true;
    }
    return false;
  }
  
  public final SimInfo b(String paramString)
  {
    Object localObject = i.getSubscriberId();
    boolean bool1 = paramString.equals(localObject);
    if (bool1)
    {
      paramString = i;
    }
    else
    {
      localObject = j.getSubscriberId();
      boolean bool2 = paramString.equals(localObject);
      if (!bool2) {
        break label153;
      }
      paramString = j;
    }
    localObject = j;
    int m;
    if (paramString == localObject)
    {
      bool1 = true;
      m = 1;
    }
    else
    {
      bool1 = false;
      localObject = null;
      m = 0;
    }
    localObject = new com/truecaller/multisim/SimInfo;
    String str1 = paramString.getSubscriberId();
    String str2 = paramString.getLine1Number();
    String str3 = paramString.getSimOperatorName();
    String str4 = paramString.getSimOperator();
    String str5 = paramString.getSimCountryIso();
    String str6 = paramString.getDeviceId();
    String str7 = paramString.getSimSerialNumber();
    boolean bool3 = paramString.isNetworkRoaming();
    ((SimInfo)localObject).<init>(m, str1, str2, str3, str4, str5, str6, str7, null, bool3);
    return (SimInfo)localObject;
    label153:
    return null;
  }
  
  public final String b(Intent paramIntent)
  {
    return c(paramIntent);
  }
  
  public final a c(String paramString)
  {
    paramString = new com/truecaller/multisim/b;
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    paramString.<init>(localBundle);
    return paramString;
  }
  
  public final String d(String paramString)
  {
    String str = i.getSubscriberId();
    boolean bool1 = paramString.equals(str);
    if (bool1)
    {
      paramString = i;
    }
    else
    {
      str = j.getSubscriberId();
      boolean bool2 = paramString.equals(str);
      if (bool2)
      {
        paramString = j;
      }
      else
      {
        bool2 = false;
        paramString = null;
      }
    }
    if (paramString != null) {
      return paramString.getNetworkCountryIso();
    }
    return null;
  }
  
  public final String e(String paramString)
  {
    String str = i.getSubscriberId();
    boolean bool1 = paramString.equals(str);
    if (bool1)
    {
      paramString = i;
    }
    else
    {
      str = j.getSubscriberId();
      boolean bool2 = paramString.equals(str);
      if (bool2)
      {
        paramString = j;
      }
      else
      {
        bool2 = false;
        paramString = null;
      }
    }
    if (paramString != null) {
      return paramString.getSimCountryIso();
    }
    return null;
  }
  
  public final boolean e()
  {
    return true;
  }
  
  public final String f()
  {
    String str = "-1";
    try
    {
      Object localObject = e;
      Object[] arrayOfObject = null;
      arrayOfObject = new Object[0];
      localObject = ((Method)localObject).invoke(null, arrayOfObject);
      localObject = (TelephonyManager)localObject;
      str = ((TelephonyManager)localObject).getSubscriberId();
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
    if (str != null) {
      return str;
    }
    return "-1";
  }
  
  public final List h()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    int m = 0;
    SimInfo localSimInfo = a(0);
    if (localSimInfo != null) {
      localArrayList.add(localSimInfo);
    }
    m = 1;
    localSimInfo = a(m);
    if (localSimInfo != null) {
      localArrayList.add(localSimInfo);
    }
    return localArrayList;
  }
  
  public final boolean j()
  {
    try
    {
      Object localObject = h;
      Object[] arrayOfObject = new Object[0];
      localObject = ((Method)localObject).invoke(null, arrayOfObject);
      localObject = (Boolean)localObject;
      return ((Boolean)localObject).booleanValue();
    }
    catch (Exception localException) {}
    return false;
  }
  
  public final boolean k()
  {
    return j();
  }
  
  public final boolean l()
  {
    return true;
  }
  
  public final String m()
  {
    return "sim_imsi";
  }
  
  public final String n()
  {
    return "sim_imsi";
  }
  
  protected final String o()
  {
    return k;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.multisim.ab
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */