package com.truecaller.multisim;

import android.content.Context;
import android.telecom.TelecomManager;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import java.lang.reflect.Field;

final class p
  extends o
{
  static final j g = -..Lambda.p.Skbfi5RWc2F7WrqkNuuNUtZyHBc.INSTANCE;
  private final String h;
  private final String i;
  private final String j;
  
  private p(Context paramContext, SubscriptionManager paramSubscriptionManager, TelephonyManager paramTelephonyManager, TelecomManager paramTelecomManager)
  {
    super(paramContext, paramSubscriptionManager, paramTelephonyManager, paramTelecomManager);
    paramContext = (String)Class.forName("miui.provider.ExtraTelephony$Sms").getField("SIM_ID").get(null);
    h = paramContext;
    paramContext = (String)Class.forName("miui.provider.ExtraTelephony$Mms").getField("SIM_ID").get(null);
    i = paramContext;
    paramContext = (String)Class.forName("miui.provider.ExtraContacts$Calls").getField("SIM_ID").get(null);
    j = paramContext;
  }
  
  public final String a()
  {
    return "LollipopMr1Xiaomi";
  }
  
  public final String m()
  {
    return h;
  }
  
  public final String n()
  {
    return i;
  }
  
  protected final String o()
  {
    return j;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.multisim.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */