package com.truecaller.multisim;

import android.content.Context;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import com.truecaller.multisim.b.b;
import com.truecaller.multisim.b.c;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

abstract class n
  extends i
{
  private final Method d;
  private final Method e;
  protected final TelephonyManager f;
  private final Method g;
  private final Method h;
  private final Method i;
  private final Method j;
  private final Method k;
  private final Method l;
  private final Method m;
  private final Method n;
  private final Method o;
  private final Method p;
  private final Method q;
  
  n(Context paramContext, TelephonyManager paramTelephonyManager)
  {
    super(paramContext);
    f = paramTelephonyManager;
    Class[] arrayOfClass = new Class[0];
    paramContext = SubscriptionManager.class.getMethod("getActiveSubInfoCount", arrayOfClass);
    d = paramContext;
    arrayOfClass = new Class[0];
    paramContext = SubscriptionManager.class.getMethod("getDefaultSubId", arrayOfClass);
    e = paramContext;
    int i1 = 1;
    Object localObject = new Class[i1];
    Class localClass = Integer.TYPE;
    localObject[0] = localClass;
    paramContext = SubscriptionManager.class.getMethod("getSubId", (Class[])localObject);
    g = paramContext;
    localObject = new Class[0];
    paramContext = SubscriptionManager.class.getMethod("getActiveSubIdList", (Class[])localObject);
    h = paramContext;
    localObject = new Class[i1];
    localClass = Long.TYPE;
    localObject[0] = localClass;
    paramContext = TelephonyManager.class.getMethod("getSimOperatorName", (Class[])localObject);
    i = paramContext;
    localObject = new Class[i1];
    localClass = Long.TYPE;
    localObject[0] = localClass;
    paramContext = TelephonyManager.class.getMethod("getLine1NumberForSubscriber", (Class[])localObject);
    j = paramContext;
    localObject = new Class[i1];
    localClass = Long.TYPE;
    localObject[0] = localClass;
    paramContext = TelephonyManager.class.getMethod("getSimOperator", (Class[])localObject);
    k = paramContext;
    localObject = new Class[i1];
    localClass = Long.TYPE;
    localObject[0] = localClass;
    paramContext = TelephonyManager.class.getMethod("getSimCountryIso", (Class[])localObject);
    l = paramContext;
    localObject = new Class[i1];
    localClass = Integer.TYPE;
    localObject[0] = localClass;
    paramContext = TelephonyManager.class.getMethod("getImei", (Class[])localObject);
    m = paramContext;
    localObject = new Class[i1];
    localClass = Long.TYPE;
    localObject[0] = localClass;
    paramContext = TelephonyManager.class.getMethod("getSimSerialNumber", (Class[])localObject);
    n = paramContext;
    localObject = new Class[i1];
    localClass = Long.TYPE;
    localObject[0] = localClass;
    paramContext = TelephonyManager.class.getMethod("isNetworkRoaming", (Class[])localObject);
    o = paramContext;
    localObject = new Class[i1];
    localClass = Long.TYPE;
    localObject[0] = localClass;
    paramContext = TelephonyManager.class.getMethod("getNetworkCountryIso", (Class[])localObject);
    p = paramContext;
    arrayOfClass = new Class[i1];
    localObject = Long.TYPE;
    arrayOfClass[0] = localObject;
    paramContext = TelephonyManager.class.getMethod("getSubscriberId", arrayOfClass);
    q = paramContext;
  }
  
  private String b(int paramInt)
  {
    try
    {
      Method localMethod = m;
      TelephonyManager localTelephonyManager = f;
      int i1 = 1;
      Object[] arrayOfObject = new Object[i1];
      Object localObject = Integer.valueOf(paramInt);
      arrayOfObject[0] = localObject;
      localObject = localMethod.invoke(localTelephonyManager, arrayOfObject);
      return (String)localObject;
    }
    catch (Exception localException) {}
    return null;
  }
  
  private String h(String paramString)
  {
    try
    {
      Method localMethod = i;
      TelephonyManager localTelephonyManager = f;
      int i1 = 1;
      Object[] arrayOfObject = new Object[i1];
      paramString = Long.valueOf(paramString);
      arrayOfObject[0] = paramString;
      paramString = localMethod.invoke(localTelephonyManager, arrayOfObject);
      return (String)paramString;
    }
    catch (Exception localException) {}
    return null;
  }
  
  private String i(String paramString)
  {
    try
    {
      Method localMethod = j;
      TelephonyManager localTelephonyManager = f;
      int i1 = 1;
      Object[] arrayOfObject = new Object[i1];
      paramString = Long.valueOf(paramString);
      arrayOfObject[0] = paramString;
      paramString = localMethod.invoke(localTelephonyManager, arrayOfObject);
      return (String)paramString;
    }
    catch (Exception localException) {}
    return null;
  }
  
  private String j(String paramString)
  {
    try
    {
      Method localMethod = k;
      TelephonyManager localTelephonyManager = f;
      int i1 = 1;
      Object[] arrayOfObject = new Object[i1];
      paramString = Long.valueOf(paramString);
      arrayOfObject[0] = paramString;
      paramString = localMethod.invoke(localTelephonyManager, arrayOfObject);
      return (String)paramString;
    }
    catch (Exception localException) {}
    return "";
  }
  
  private String k(String paramString)
  {
    try
    {
      Method localMethod = n;
      TelephonyManager localTelephonyManager = f;
      int i1 = 1;
      Object[] arrayOfObject = new Object[i1];
      paramString = Long.valueOf(paramString);
      arrayOfObject[0] = paramString;
      paramString = localMethod.invoke(localTelephonyManager, arrayOfObject);
      return (String)paramString;
    }
    catch (Exception localException) {}
    return null;
  }
  
  private String l(String paramString)
  {
    try
    {
      Method localMethod = q;
      TelephonyManager localTelephonyManager = f;
      int i1 = 1;
      Object[] arrayOfObject = new Object[i1];
      paramString = Long.valueOf(paramString);
      arrayOfObject[0] = paramString;
      paramString = localMethod.invoke(localTelephonyManager, arrayOfObject);
      return (String)paramString;
    }
    catch (Exception localException) {}
    return null;
  }
  
  private boolean m(String paramString)
  {
    try
    {
      Method localMethod = o;
      TelephonyManager localTelephonyManager = f;
      int i1 = 1;
      Object[] arrayOfObject = new Object[i1];
      paramString = Long.valueOf(paramString);
      arrayOfObject[0] = paramString;
      paramString = localMethod.invoke(localTelephonyManager, arrayOfObject);
      paramString = (Boolean)paramString;
      return paramString.booleanValue();
    }
    catch (Exception localException) {}
    return false;
  }
  
  public SimInfo a(int paramInt)
  {
    Object localObject1 = "-1";
    Object localObject2;
    int i1;
    Object localObject3;
    String str1;
    try
    {
      localObject2 = g;
      i1 = 1;
      Object[] arrayOfObject = new Object[i1];
      localObject3 = Integer.valueOf(paramInt);
      str1 = null;
      arrayOfObject[0] = localObject3;
      localObject2 = ((Method)localObject2).invoke(null, arrayOfObject);
      localObject2 = (long[])localObject2;
      localObject2 = (long[])localObject2;
      long l1 = localObject2[0];
      localObject1 = String.valueOf(l1);
      localObject3 = localObject1;
    }
    catch (Exception localException)
    {
      localObject3 = localObject1;
    }
    localObject1 = "-1";
    boolean bool1 = ((String)localObject1).equals(localObject3);
    if (!bool1)
    {
      localObject1 = "-1000";
      bool1 = ((String)localObject1).equals(localObject3);
      if (!bool1)
      {
        localObject1 = new com/truecaller/multisim/SimInfo;
        str1 = i((String)localObject3);
        String str2 = h((String)localObject3);
        String str3 = j((String)localObject3);
        String str4 = e((String)localObject3);
        String str5 = b(paramInt);
        String str6 = k((String)localObject3);
        String str7 = l((String)localObject3);
        boolean bool2 = m((String)localObject3);
        localObject2 = localObject1;
        i1 = paramInt;
        ((SimInfo)localObject1).<init>(paramInt, (String)localObject3, str1, str2, str3, str4, str5, str6, str7, bool2);
        return (SimInfo)localObject1;
      }
    }
    return null;
  }
  
  public SimInfo b(String paramString)
  {
    SimInfo localSimInfo = null;
    try
    {
      Object localObject1 = g;
      int i1 = 1;
      Object localObject2 = new Object[i1];
      int i2 = 0;
      Object localObject3 = Integer.valueOf(0);
      localObject2[0] = localObject3;
      localObject1 = ((Method)localObject1).invoke(null, (Object[])localObject2);
      localObject1 = (long[])localObject1;
      localObject1 = (long[])localObject1;
      long l1 = localObject1[0];
      localObject1 = String.valueOf(l1);
      boolean bool1 = ((String)localObject1).equals(paramString);
      if (!bool1)
      {
        localObject1 = g;
        localObject2 = new Object[i1];
        localObject3 = Integer.valueOf(i1);
        localObject2[0] = localObject3;
        localObject1 = ((Method)localObject1).invoke(null, (Object[])localObject2);
        localObject1 = (long[])localObject1;
        localObject1 = (long[])localObject1;
        long l2 = localObject1[0];
        localObject1 = String.valueOf(l2);
        bool1 = ((String)localObject1).equals(paramString);
        if (bool1) {
          i2 = 1;
        }
      }
      else
      {
        localSimInfo = new com/truecaller/multisim/SimInfo;
        String str1 = i(paramString);
        String str2 = h(paramString);
        String str3 = j(paramString);
        String str4 = e(paramString);
        String str5 = b(i2);
        String str6 = k(paramString);
        String str7 = l(paramString);
        boolean bool2 = m(paramString);
        localObject2 = localSimInfo;
        localObject3 = paramString;
        localSimInfo.<init>(i2, paramString, str1, str2, str3, str4, str5, str6, str7, bool2);
        return localSimInfo;
      }
      return null;
    }
    catch (Exception localException) {}
    return null;
  }
  
  public String d(String paramString)
  {
    try
    {
      Method localMethod = p;
      TelephonyManager localTelephonyManager = f;
      int i1 = 1;
      Object[] arrayOfObject = new Object[i1];
      paramString = Long.valueOf(paramString);
      arrayOfObject[0] = paramString;
      paramString = localMethod.invoke(localTelephonyManager, arrayOfObject);
      return (String)paramString;
    }
    catch (Exception localException)
    {
      c.a();
    }
    return null;
  }
  
  public String e(String paramString)
  {
    try
    {
      Method localMethod = l;
      TelephonyManager localTelephonyManager = f;
      int i1 = 1;
      Object[] arrayOfObject = new Object[i1];
      paramString = Long.valueOf(paramString);
      arrayOfObject[0] = paramString;
      paramString = localMethod.invoke(localTelephonyManager, arrayOfObject);
      return (String)paramString;
    }
    catch (Exception localException)
    {
      c.a();
    }
    return null;
  }
  
  public String f()
  {
    try
    {
      Object localObject = e;
      Object[] arrayOfObject = null;
      arrayOfObject = new Object[0];
      localObject = ((Method)localObject).invoke(null, arrayOfObject);
      return String.valueOf(localObject);
    }
    catch (Exception localException) {}
    return "-1";
  }
  
  public List h()
  {
    localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    try
    {
      Object localObject1 = h;
      int i1 = 0;
      int i2 = 0;
      Object localObject2 = new Object[0];
      localObject1 = ((Method)localObject1).invoke(null, (Object[])localObject2);
      localObject1 = (long[])localObject1;
      localObject1 = (long[])localObject1;
      i1 = localObject1.length;
      while (i2 < i1)
      {
        long l1 = localObject1[i2];
        localObject2 = String.valueOf(l1);
        localObject2 = b((String)localObject2);
        if (localObject2 != null) {
          localArrayList.add(localObject2);
        }
        i2 += 1;
      }
      return localArrayList;
    }
    catch (IllegalAccessException|InvocationTargetException localIllegalAccessException)
    {
      c.a();
    }
  }
  
  public boolean j()
  {
    try
    {
      Object localObject = d;
      int i1 = 0;
      Object[] arrayOfObject = new Object[0];
      localObject = ((Method)localObject).invoke(null, arrayOfObject);
      localObject = (Integer)localObject;
      int i2 = ((Integer)localObject).intValue();
      i1 = 1;
      if (i2 > i1) {
        return i1;
      }
      return false;
    }
    catch (Exception localException) {}
    return false;
  }
  
  public boolean k()
  {
    boolean bool = j();
    if (bool)
    {
      b localb = new com/truecaller/multisim/b/b;
      Object localObject = f;
      localb.<init>((TelephonyManager)localObject);
      localObject = h();
      return localb.a((List)localObject);
    }
    return false;
  }
  
  protected String o()
  {
    return "subscription_id";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.multisim.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */