package com.truecaller.multisim;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.telecom.TelecomManager;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import com.truecaller.multisim.b.c;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class l
  extends n
{
  static final j d = -..Lambda.l.uQlRrZYwtt0Z3Pxea0sxR-FqmL0.INSTANCE;
  private final TelecomManager e;
  private final Method g;
  private final String h;
  private final Method i;
  private final Method j;
  
  l(Context paramContext, TelephonyManager paramTelephonyManager, TelecomManager paramTelecomManager)
  {
    super(paramContext, paramTelephonyManager);
    e = paramTelecomManager;
    paramContext = Class.forName("android.telephony.SmsManager");
    paramTelecomManager = new Class[1];
    Class localClass = Long.TYPE;
    paramTelecomManager[0] = localClass;
    paramContext = paramContext.getMethod("getSmsManagerForSubscriber", paramTelecomManager);
    g = paramContext;
    paramContext = (String)TelecomManager.class.getField("EXTRA_PHONE_ACCOUNT_HANDLE").get(null);
    h = paramContext;
    paramTelecomManager = new Class[0];
    paramContext = TelecomManager.class.getMethod("getCallCapablePhoneAccounts", paramTelecomManager);
    i = paramContext;
    paramContext = Class.forName("android.telecom.PhoneAccountHandle");
    paramTelecomManager = new Class[0];
    paramContext = paramContext.getMethod("getId", paramTelecomManager);
    j = paramContext;
  }
  
  private SmsManager h(String paramString)
  {
    try
    {
      Method localMethod = g;
      int k = 1;
      Object[] arrayOfObject = new Object[k];
      paramString = Long.valueOf(paramString);
      arrayOfObject[0] = paramString;
      paramString = localMethod.invoke(null, arrayOfObject);
      return (SmsManager)paramString;
    }
    catch (Exception localException) {}
    return SmsManager.getDefault();
  }
  
  public String a()
  {
    return "Lollipop1";
  }
  
  public final String a(Intent paramIntent)
  {
    return String.valueOf(paramIntent.getLongExtra("subscription", -1));
  }
  
  public final void a(Intent paramIntent, String paramString)
  {
    Object localObject1 = b;
    Object localObject2 = { "android.permission.READ_PHONE_STATE" };
    boolean bool1 = ((com.truecaller.multisim.b.a)localObject1).a((String[])localObject2);
    if (bool1) {
      try
      {
        localObject1 = i;
        localObject2 = e;
        Object localObject3 = new Object[0];
        localObject1 = ((Method)localObject1).invoke(localObject2, (Object[])localObject3);
        localObject1 = (List)localObject1;
        localObject1 = ((List)localObject1).iterator();
        for (;;)
        {
          boolean bool2 = ((Iterator)localObject1).hasNext();
          if (!bool2) {
            break;
          }
          localObject2 = ((Iterator)localObject1).next();
          localObject3 = j;
          Object[] arrayOfObject = new Object[0];
          localObject3 = ((Method)localObject3).invoke(localObject2, arrayOfObject);
          boolean bool3 = paramString.equals(localObject3);
          if (bool3)
          {
            localObject3 = h;
            localObject2 = (Parcelable)localObject2;
            paramIntent.putExtra((String)localObject3, (Parcelable)localObject2);
          }
        }
        return;
      }
      catch (IllegalAccessException|InvocationTargetException localIllegalAccessException)
      {
        c.a();
      }
    }
  }
  
  public final boolean a(String paramString1, String paramString2, String paramString3, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2, String paramString4)
  {
    String str = "-1";
    boolean bool = str.equals(paramString4);
    if (bool) {
      return false;
    }
    h(paramString4).sendTextMessage(paramString1, paramString2, paramString3, paramPendingIntent1, paramPendingIntent2);
    return true;
  }
  
  public final boolean a(String paramString1, String paramString2, ArrayList paramArrayList1, ArrayList paramArrayList2, ArrayList paramArrayList3, String paramString3)
  {
    String str = "-1";
    boolean bool = str.equals(paramString3);
    if (bool) {
      return false;
    }
    h(paramString3).sendMultipartTextMessage(paramString1, paramString2, paramArrayList1, paramArrayList2, paramArrayList3);
    return true;
  }
  
  public final String b(Intent paramIntent)
  {
    return String.valueOf(paramIntent.getLongExtra("subscription", -1));
  }
  
  public final a c(String paramString)
  {
    paramString = f(paramString).getCarrierConfigValues();
    b localb = new com/truecaller/multisim/b;
    if (paramString == null)
    {
      paramString = new android/os/Bundle;
      paramString.<init>();
    }
    localb.<init>(paramString);
    return localb;
  }
  
  public final boolean e()
  {
    return true;
  }
  
  public final SmsManager f(String paramString)
  {
    String str = "-1";
    boolean bool = str.equals(paramString);
    if (bool) {
      paramString = SmsManager.getDefault();
    } else {
      paramString = h(paramString);
    }
    return paramString;
  }
  
  public final boolean l()
  {
    return true;
  }
  
  protected String m()
  {
    return "sub_id";
  }
  
  protected String n()
  {
    return "sub_id";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.multisim.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */