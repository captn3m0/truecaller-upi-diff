package com.truecaller.multisim;

import android.content.Context;
import android.database.Cursor;
import android.telecom.TelecomManager;
import android.telephony.CarrierConfigManager;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import java.lang.reflect.Method;

final class u
  extends q
{
  static final j h = -..Lambda.u.jw7urZLtjFAAMFY1UZ7qA7SUomc.INSTANCE;
  private final Method i;
  
  private u(Context paramContext, SubscriptionManager paramSubscriptionManager, TelephonyManager paramTelephonyManager, TelecomManager paramTelecomManager, CarrierConfigManager paramCarrierConfigManager)
  {
    super(paramContext, paramSubscriptionManager, paramTelephonyManager, paramTelecomManager, paramCarrierConfigManager);
    paramContext = null;
    paramSubscriptionManager = "com.samsung.android.telephony.MultiSimManager";
    try
    {
      paramSubscriptionManager = Class.forName(paramSubscriptionManager);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      paramSubscriptionManager = null;
    }
    if (paramSubscriptionManager != null)
    {
      paramTelephonyManager = new Class[1];
      paramTelephonyManager[0] = Context.class;
      paramContext = paramSubscriptionManager.getMethod("getEnabledSimCount", paramTelephonyManager);
      i = paramContext;
      return;
    }
    i = null;
  }
  
  public final d a(Cursor paramCursor)
  {
    g localg = new com/truecaller/multisim/g;
    localg.<init>(paramCursor, this);
    return localg;
  }
  
  public final String a()
  {
    return "MarshmallowSamsung";
  }
  
  public final boolean k()
  {
    Method localMethod = i;
    if (localMethod == null) {
      return super.k();
    }
    int j = j();
    if (j != 0)
    {
      j = 1;
      try
      {
        Object localObject = i;
        Object[] arrayOfObject = new Object[j];
        Context localContext = a;
        arrayOfObject[0] = localContext;
        localObject = ((Method)localObject).invoke(null, arrayOfObject);
        localObject = (Integer)localObject;
        int k = ((Integer)localObject).intValue();
        if (k > j) {
          return j;
        }
        return false;
      }
      catch (Exception localException)
      {
        return j;
      }
    }
    return false;
  }
  
  protected final String o()
  {
    return "sim_id";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.multisim.u
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */