package com.truecaller.multisim;

import android.content.Context;
import android.database.Cursor;
import android.telecom.TelecomManager;
import android.telephony.CarrierConfigManager;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import java.util.Iterator;
import java.util.List;

final class w
  extends q
{
  static final j h = -..Lambda.w.IeOvNLQh__MGDHkAuyptKx5ylLw.INSTANCE;
  
  private w(Context paramContext, SubscriptionManager paramSubscriptionManager, TelephonyManager paramTelephonyManager, TelecomManager paramTelecomManager, CarrierConfigManager paramCarrierConfigManager)
  {
    super(paramContext, paramSubscriptionManager, paramTelephonyManager, paramTelecomManager, paramCarrierConfigManager);
  }
  
  public final d a(Cursor paramCursor)
  {
    f localf = new com/truecaller/multisim/f;
    localf.<init>(paramCursor, this);
    return localf;
  }
  
  public final String a()
  {
    return "MarshmallowYu";
  }
  
  public final SimInfo b(String paramString)
  {
    Iterator localIterator = h().iterator();
    SimInfo localSimInfo;
    boolean bool2;
    do
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break label75;
      }
      localSimInfo = (SimInfo)localIterator.next();
      String str = h;
      bool2 = paramString.equalsIgnoreCase(str);
      if (bool2) {
        break;
      }
      str = b;
      bool2 = paramString.equalsIgnoreCase(str);
    } while (!bool2);
    return localSimInfo;
    label75:
    return null;
  }
  
  public final String g()
  {
    String str1 = super.g();
    SimInfo localSimInfo = b(str1);
    if (localSimInfo != null)
    {
      String str2 = h;
      if (str2 != null) {
        return h;
      }
    }
    return str1;
  }
  
  protected final String o()
  {
    return "iccid";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.multisim.w
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */