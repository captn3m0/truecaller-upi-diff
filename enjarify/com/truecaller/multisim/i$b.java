package com.truecaller.multisim;

import android.database.Cursor;
import android.database.CursorWrapper;

final class i$b
  extends CursorWrapper
  implements d
{
  private final int b;
  
  i$b(i parami, Cursor paramCursor)
  {
    super(paramCursor);
    parami = parami.d();
    int i;
    if (parami != null) {
      i = getColumnIndex(parami);
    } else {
      i = -1;
    }
    b = i;
  }
  
  public final String e()
  {
    int i = b;
    if (i >= 0)
    {
      String str = getString(i);
      if (str == null) {
        str = "-1";
      }
      return str;
    }
    return "-1";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.multisim.i.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */