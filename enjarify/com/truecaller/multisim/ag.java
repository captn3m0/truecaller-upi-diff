package com.truecaller.multisim;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

final class ag
  extends i
{
  private final TelephonyManager d;
  
  ag(Context paramContext, TelephonyManager paramTelephonyManager)
  {
    super(paramContext);
    d = paramTelephonyManager;
  }
  
  private SimInfo p()
  {
    SimInfo localSimInfo = new com/truecaller/multisim/SimInfo;
    String str1 = d.getLine1Number();
    String str2 = d.getSimOperatorName();
    String str3 = d.getSimOperator();
    String str4 = d.getSimCountryIso();
    String str5 = d.getDeviceId();
    String str6 = d.getSimSerialNumber();
    String str7 = d.getSubscriberId();
    boolean bool = d.isNetworkRoaming();
    localSimInfo.<init>(0, "-1", str1, str2, str3, str4, str5, str6, str7, bool);
    return localSimInfo;
  }
  
  public final SimInfo a(int paramInt)
  {
    if (paramInt > 0) {
      return null;
    }
    return p();
  }
  
  public final String a()
  {
    return "Single";
  }
  
  public final String a(Intent paramIntent)
  {
    return "-1";
  }
  
  public final void a(Intent paramIntent, String paramString) {}
  
  public final boolean a(String paramString1, String paramString2, String paramString3, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2, String paramString4)
  {
    SmsManager.getDefault().sendTextMessage(paramString1, paramString2, paramString3, paramPendingIntent1, paramPendingIntent2);
    return true;
  }
  
  public final boolean a(String paramString1, String paramString2, ArrayList paramArrayList1, ArrayList paramArrayList2, ArrayList paramArrayList3, String paramString3)
  {
    SmsManager.getDefault().sendMultipartTextMessage(paramString1, paramString2, paramArrayList1, paramArrayList2, paramArrayList3);
    return true;
  }
  
  public final SimInfo b(String paramString)
  {
    return p();
  }
  
  public final String b(Intent paramIntent)
  {
    return "-1";
  }
  
  public final a c(String paramString)
  {
    int i = Build.VERSION.SDK_INT;
    int j = 21;
    if (i >= j)
    {
      paramString = SmsManager.getDefault().getCarrierConfigValues();
    }
    else
    {
      i = 0;
      paramString = null;
    }
    b localb = new com/truecaller/multisim/b;
    if (paramString == null)
    {
      paramString = new android/os/Bundle;
      paramString.<init>();
    }
    localb.<init>(paramString);
    return localb;
  }
  
  public final String d(String paramString)
  {
    return d.getNetworkCountryIso();
  }
  
  public final String e(String paramString)
  {
    return d.getSimCountryIso();
  }
  
  public final boolean e()
  {
    return false;
  }
  
  public final String f()
  {
    return "-1";
  }
  
  public final List h()
  {
    return Collections.singletonList(p());
  }
  
  public final boolean j()
  {
    return false;
  }
  
  public final boolean k()
  {
    return false;
  }
  
  public final boolean l()
  {
    return true;
  }
  
  public final String m()
  {
    return null;
  }
  
  public final String n()
  {
    return null;
  }
  
  protected final String o()
  {
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.multisim.ag
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */