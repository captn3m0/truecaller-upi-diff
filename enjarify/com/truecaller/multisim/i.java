package com.truecaller.multisim;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION;
import android.provider.Telephony.Mms;
import android.provider.Telephony.Sms;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.truecaller.callhistory.k;
import com.truecaller.multisim.a.b;
import com.truecaller.multisim.a.e;
import com.truecaller.multisim.b.a;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class i
  implements h
{
  final Context a;
  final a b;
  final b c;
  private final k d;
  private String e;
  private String f;
  private String g;
  private volatile boolean h = false;
  private volatile boolean i = false;
  private volatile boolean j = false;
  
  i(Context paramContext)
  {
    Object localObject = paramContext.getApplicationContext();
    a = ((Context)localObject);
    localObject = k.a(paramContext);
    d = ((k)localObject);
    localObject = new com/truecaller/multisim/b/a;
    Context localContext = a;
    ((a)localObject).<init>(localContext);
    b = ((a)localObject);
    int k = Build.VERSION.SDK_INT;
    int m = 26;
    if (k >= m)
    {
      localObject = new com/truecaller/multisim/a/e;
      ((e)localObject).<init>(paramContext);
    }
    else
    {
      m = 23;
      if (k >= m)
      {
        localObject = new com/truecaller/multisim/a/d;
        ((com.truecaller.multisim.a.d)localObject).<init>(paramContext);
      }
      else
      {
        localObject = new com/truecaller/multisim/a/c;
        ((com.truecaller.multisim.a.c)localObject).<init>(paramContext);
      }
    }
    c = ((b)localObject);
  }
  
  public static h a(Context paramContext, TelephonyManager paramTelephonyManager)
  {
    Object localObject1 = Build.MANUFACTURER.toLowerCase();
    i.a[] arrayOfa = i.a.values();
    int k = arrayOfa.length;
    int m = 0;
    while (m < k)
    {
      Object localObject2 = arrayOfa[m];
      int n = Build.VERSION.SDK_INT;
      int i1 = s;
      if (n >= i1)
      {
        String str = t;
        if (str != null)
        {
          str = t;
          boolean bool = ((String)localObject1).contains(str);
          if (!bool) {}
        }
        else
        {
          localObject2 = r.create(paramContext, paramTelephonyManager);
          if (localObject2 != null)
          {
            paramContext = new String[1];
            paramTelephonyManager = new java/lang/StringBuilder;
            paramTelephonyManager.<init>("Creating MultiSimManager ");
            localObject1 = localObject2.getClass().getSimpleName();
            paramTelephonyManager.append((String)localObject1);
            paramTelephonyManager = paramTelephonyManager.toString();
            paramContext[0] = paramTelephonyManager;
            com.truecaller.multisim.b.c.a(paramContext);
            return (h)localObject2;
          }
        }
      }
      m += 1;
    }
    com.truecaller.multisim.b.c.a(new String[] { "Creating MultiSimManager SingleSimManager" });
    localObject1 = new com/truecaller/multisim/ag;
    ((ag)localObject1).<init>(paramContext, paramTelephonyManager);
    return (h)localObject1;
  }
  
  private boolean a(Uri paramUri, String paramString)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    if (!bool) {
      try
      {
        Context localContext = a;
        ContentResolver localContentResolver = localContext.getContentResolver();
        bool = true;
        String[] arrayOfString = new String[bool];
        arrayOfString[0] = paramString;
        String str = "_id ASC LIMIT 1";
        paramUri = localContentResolver.query(paramUri, arrayOfString, null, null, str);
        if (paramUri != null) {
          paramUri.close();
        }
        return bool;
      }
      finally
      {
        com.truecaller.multisim.b.c.a();
      }
    }
    return false;
  }
  
  public d a(Cursor paramCursor)
  {
    i.b localb = new com/truecaller/multisim/i$b;
    localb.<init>(this, paramCursor);
    return localb;
  }
  
  public void a(String paramString) {}
  
  public final String b()
  {
    boolean bool1 = h;
    if (bool1) {
      return e;
    }
    try
    {
      bool1 = h;
      if (bool1)
      {
        localObject1 = e;
        return (String)localObject1;
      }
      Object localObject1 = b;
      Object localObject3 = "android.permission.READ_SMS";
      localObject3 = new String[] { localObject3 };
      bool1 = ((a)localObject1).a((String[])localObject3);
      if (!bool1)
      {
        bool1 = false;
        localObject1 = null;
        return null;
      }
      localObject1 = m();
      localObject3 = Telephony.Sms.CONTENT_URI;
      boolean bool2 = a((Uri)localObject3, (String)localObject1);
      if (bool2) {
        e = ((String)localObject1);
      }
      bool1 = true;
      h = bool1;
      return e;
    }
    finally {}
  }
  
  public final String c()
  {
    boolean bool1 = i;
    if (bool1) {
      return f;
    }
    try
    {
      bool1 = i;
      if (bool1)
      {
        localObject1 = f;
        return (String)localObject1;
      }
      Object localObject1 = b;
      Object localObject3 = "android.permission.READ_SMS";
      localObject3 = new String[] { localObject3 };
      bool1 = ((a)localObject1).a((String[])localObject3);
      if (!bool1)
      {
        bool1 = false;
        localObject1 = null;
        return null;
      }
      localObject1 = n();
      localObject3 = Telephony.Mms.CONTENT_URI;
      boolean bool2 = a((Uri)localObject3, (String)localObject1);
      if (bool2) {
        f = ((String)localObject1);
      }
      bool1 = true;
      i = bool1;
      return f;
    }
    finally {}
  }
  
  public final String d()
  {
    boolean bool1 = j;
    if (bool1) {
      return g;
    }
    try
    {
      bool1 = j;
      if (bool1)
      {
        localObject1 = g;
        return (String)localObject1;
      }
      Object localObject1 = b;
      Object localObject3 = "android.permission.READ_CALL_LOG";
      localObject3 = new String[] { localObject3 };
      bool1 = ((a)localObject1).a((String[])localObject3);
      if (!bool1)
      {
        bool1 = false;
        localObject1 = null;
        return null;
      }
      localObject1 = o();
      localObject3 = d;
      localObject3 = ((k)localObject3).a();
      boolean bool2 = a((Uri)localObject3, (String)localObject1);
      if (bool2) {
        g = ((String)localObject1);
      }
      bool1 = true;
      j = bool1;
      return g;
    }
    finally {}
  }
  
  public SmsManager f(String paramString)
  {
    return SmsManager.getDefault();
  }
  
  public final int g(String paramString)
  {
    return c.b(paramString);
  }
  
  public String g()
  {
    return "-1";
  }
  
  public final List i()
  {
    Object localObject1 = h();
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    localObject1 = ((List)localObject1).iterator();
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      Object localObject2 = (SimInfo)((Iterator)localObject1).next();
      String str = h;
      boolean bool2 = TextUtils.isEmpty(str);
      if (!bool2)
      {
        localObject2 = h;
        localArrayList.add(localObject2);
      }
      else
      {
        localObject2 = "";
        localArrayList.add(localObject2);
      }
    }
    return localArrayList;
  }
  
  public boolean l()
  {
    return false;
  }
  
  protected abstract String m();
  
  protected abstract String n();
  
  protected abstract String o();
}

/* Location:
 * Qualified Name:     com.truecaller.multisim.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */