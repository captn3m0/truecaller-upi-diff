package com.truecaller.multisim;

import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION;
import android.telephony.SubscriptionManager;
import com.mediatek.telephony.SmsManagerEx;
import com.mediatek.telephony.TelephonyManagerEx;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public final class y
  extends z
{
  static final j d = -..Lambda.y.CTUdvdc09E-bf0ufJNpCAFWrp60.INSTANCE;
  private final String e;
  private final String f;
  private final Method g;
  private final Method h;
  private final Method i;
  private final Method j;
  
  private y(Context paramContext, TelephonyManagerEx paramTelephonyManagerEx, SmsManagerEx paramSmsManagerEx)
  {
    super(paramContext, paramTelephonyManagerEx, paramSmsManagerEx);
    paramContext = Class.forName("android.provider.Telephony$Sms");
    paramContext = (String)paramContext.getField("SUB_ID").get(paramContext);
    e = paramContext;
    paramContext = Class.forName("android.provider.Telephony$Mms");
    paramContext = (String)paramContext.getField("SUB_ID").get(paramContext);
    f = paramContext;
    Class[] arrayOfClass = new Class[0];
    paramContext = SubscriptionManager.class.getMethod("getActiveSubInfoCount", arrayOfClass);
    g = paramContext;
    arrayOfClass = new Class[0];
    paramContext = SubscriptionManager.class.getMethod("getDefaultSubId", arrayOfClass);
    h = paramContext;
    arrayOfClass = new Class[1];
    Class localClass = Integer.TYPE;
    arrayOfClass[0] = localClass;
    paramContext = SubscriptionManager.class.getMethod("getSubId", arrayOfClass);
    i = paramContext;
    paramSmsManagerEx = new Class[0];
    paramContext = SubscriptionManager.class.getMethod("getActiveSubIdList", paramSmsManagerEx);
    j = paramContext;
  }
  
  public final SimInfo a(int paramInt)
  {
    Object localObject1 = "-1";
    Object localObject2;
    int k;
    Object localObject3;
    String str1;
    try
    {
      localObject2 = i;
      k = 1;
      Object[] arrayOfObject = new Object[k];
      localObject3 = Integer.valueOf(paramInt);
      str1 = null;
      arrayOfObject[0] = localObject3;
      localObject2 = ((Method)localObject2).invoke(null, arrayOfObject);
      localObject2 = (long[])localObject2;
      localObject2 = (long[])localObject2;
      long l = localObject2[0];
      localObject1 = String.valueOf(l);
      localObject3 = localObject1;
    }
    catch (Exception localException)
    {
      localObject3 = localObject1;
    }
    localObject1 = "-1";
    boolean bool1 = ((String)localObject1).equals(localObject3);
    if (!bool1)
    {
      localObject1 = "-1000";
      bool1 = ((String)localObject1).equals(localObject3);
      if (!bool1)
      {
        localObject1 = new com/truecaller/multisim/SimInfo;
        str1 = d(paramInt);
        String str2 = c(paramInt);
        String str3 = e(paramInt);
        String str4 = f(paramInt);
        String str5 = g(paramInt);
        String str6 = h(paramInt);
        boolean bool2 = i(paramInt);
        localObject2 = localObject1;
        k = paramInt;
        ((SimInfo)localObject1).<init>(paramInt, (String)localObject3, str1, str2, str3, str4, str5, str6, null, bool2);
        return (SimInfo)localObject1;
      }
    }
    return null;
  }
  
  public final String a()
  {
    return "Mediatek2";
  }
  
  public final String a(Intent paramIntent)
  {
    return String.valueOf(paramIntent.getLongExtra("subscription", -1));
  }
  
  public final void a(Intent paramIntent, String paramString) {}
  
  public final SimInfo b(String paramString)
  {
    SimInfo localSimInfo = null;
    try
    {
      Object localObject1 = i;
      int k = 1;
      Object localObject2 = new Object[k];
      int m = 0;
      Object localObject3 = Integer.valueOf(0);
      localObject2[0] = localObject3;
      localObject1 = ((Method)localObject1).invoke(null, (Object[])localObject2);
      localObject1 = (long[])localObject1;
      localObject1 = (long[])localObject1;
      long l1 = localObject1[0];
      localObject1 = String.valueOf(l1);
      boolean bool1 = ((String)localObject1).equals(paramString);
      if (!bool1)
      {
        localObject1 = i;
        localObject2 = new Object[k];
        localObject3 = Integer.valueOf(k);
        localObject2[0] = localObject3;
        localObject1 = ((Method)localObject1).invoke(null, (Object[])localObject2);
        localObject1 = (long[])localObject1;
        localObject1 = (long[])localObject1;
        long l2 = localObject1[0];
        localObject1 = String.valueOf(l2);
        bool1 = ((String)localObject1).equals(paramString);
        if (bool1) {
          m = 1;
        }
      }
      else
      {
        localSimInfo = new com/truecaller/multisim/SimInfo;
        String str1 = d(m);
        String str2 = c(m);
        String str3 = e(m);
        String str4 = f(m);
        String str5 = g(m);
        String str6 = h(m);
        boolean bool2 = i(m);
        localObject2 = localSimInfo;
        localObject3 = paramString;
        localSimInfo.<init>(m, paramString, str1, str2, str3, str4, str5, str6, null, bool2);
        return localSimInfo;
      }
      return null;
    }
    catch (Exception localException) {}
    return null;
  }
  
  public final String b(Intent paramIntent)
  {
    return String.valueOf(paramIntent.getLongExtra("subscription", -1));
  }
  
  public final boolean e()
  {
    return false;
  }
  
  public final String f()
  {
    try
    {
      Object localObject = h;
      Object[] arrayOfObject = null;
      arrayOfObject = new Object[0];
      localObject = ((Method)localObject).invoke(null, arrayOfObject);
      return String.valueOf(localObject);
    }
    catch (Exception localException) {}
    return "-1";
  }
  
  public final List h()
  {
    localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    try
    {
      Object localObject1 = j;
      int k = 0;
      int m = 0;
      Object localObject2 = new Object[0];
      localObject1 = ((Method)localObject1).invoke(null, (Object[])localObject2);
      localObject1 = (long[])localObject1;
      localObject1 = (long[])localObject1;
      if (localObject1 != null)
      {
        k = localObject1.length;
        while (m < k)
        {
          long l = localObject1[m];
          localObject2 = String.valueOf(l);
          localObject2 = b((String)localObject2);
          if (localObject2 != null) {
            localArrayList.add(localObject2);
          }
          m += 1;
        }
      }
      return localArrayList;
    }
    catch (Exception localException) {}
  }
  
  public final boolean j()
  {
    try
    {
      Object localObject = g;
      int k = 0;
      Object[] arrayOfObject = new Object[0];
      localObject = ((Method)localObject).invoke(null, arrayOfObject);
      localObject = (Integer)localObject;
      int m = ((Integer)localObject).intValue();
      k = 1;
      if (m > k) {
        return k;
      }
      return false;
    }
    catch (Exception localException) {}
    return false;
  }
  
  public final boolean k()
  {
    return j();
  }
  
  public final boolean l()
  {
    int k = Build.VERSION.SDK_INT;
    int m = 21;
    return k >= m;
  }
  
  public final String m()
  {
    return e;
  }
  
  public final String n()
  {
    return f;
  }
  
  protected final String o()
  {
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.multisim.y
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */