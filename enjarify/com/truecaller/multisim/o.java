package com.truecaller.multisim;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.telecom.TelecomManager;
import android.telephony.SmsManager;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import com.truecaller.multisim.b.c;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class o
  extends i
{
  static final j f = -..Lambda.o.fKSzJaMKweSjkGB-SBmnRAIvZwY.INSTANCE;
  final TelephonyManager d;
  final TelecomManager e;
  private final SubscriptionManager g;
  private final String h;
  private final Method i;
  private final Method j;
  private final Method k;
  private final Method l;
  private final Method m;
  private final Method n;
  
  o(Context paramContext, SubscriptionManager paramSubscriptionManager, TelephonyManager paramTelephonyManager, TelecomManager paramTelecomManager)
  {
    super(paramContext);
    g = paramSubscriptionManager;
    d = paramTelephonyManager;
    e = paramTelecomManager;
    paramContext = Class.forName("android.telecom.PhoneAccountHandle");
    paramSubscriptionManager = (String)TelecomManager.class.getField("EXTRA_PHONE_ACCOUNT_HANDLE").get(null);
    h = paramSubscriptionManager;
    Class[] arrayOfClass1 = new Class[0];
    paramSubscriptionManager = TelecomManager.class.getMethod("getCallCapablePhoneAccounts", arrayOfClass1);
    i = paramSubscriptionManager;
    paramTelephonyManager = new Class[0];
    paramSubscriptionManager = paramContext.getMethod("getId", paramTelephonyManager);
    j = paramSubscriptionManager;
    int i1 = 1;
    Class[] arrayOfClass2 = new Class[i1];
    Class localClass = Integer.TYPE;
    arrayOfClass2[0] = localClass;
    paramSubscriptionManager = TelephonyManager.class.getMethod("getDeviceId", arrayOfClass2);
    k = paramSubscriptionManager;
    arrayOfClass2 = new Class[i1];
    localClass = Integer.TYPE;
    arrayOfClass2[0] = localClass;
    paramSubscriptionManager = TelephonyManager.class.getMethod("getSubscriberId", arrayOfClass2);
    l = paramSubscriptionManager;
    arrayOfClass2 = new Class[0];
    paramSubscriptionManager = TelecomManager.class.getMethod("getUserSelectedOutgoingPhoneAccount", arrayOfClass2);
    m = paramSubscriptionManager;
    arrayOfClass1 = new Class[i1];
    arrayOfClass1[0] = paramContext;
    paramContext = TelecomManager.class.getMethod("setUserSelectedOutgoingPhoneAccount", arrayOfClass1);
    n = paramContext;
  }
  
  private SimInfo a(SubscriptionInfo paramSubscriptionInfo)
  {
    int i1 = 0;
    String str1 = null;
    if (paramSubscriptionInfo == null) {
      return null;
    }
    Object localObject1 = paramSubscriptionInfo.getCarrierName();
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>();
    String str2 = String.valueOf(paramSubscriptionInfo.getMcc());
    ((StringBuilder)localObject2).append(str2);
    str2 = String.valueOf(paramSubscriptionInfo.getMnc());
    ((StringBuilder)localObject2).append(str2);
    String str3 = ((StringBuilder)localObject2).toString();
    localObject2 = new com/truecaller/multisim/SimInfo;
    int i2 = paramSubscriptionInfo.getSimSlotIndex();
    int i3 = paramSubscriptionInfo.getSubscriptionId();
    String str4 = String.valueOf(i3);
    String str5 = paramSubscriptionInfo.getNumber();
    String str6;
    if (localObject1 == null)
    {
      str6 = null;
    }
    else
    {
      str1 = ((CharSequence)localObject1).toString();
      str6 = str1;
    }
    String str7 = paramSubscriptionInfo.getCountryIso();
    i1 = paramSubscriptionInfo.getSimSlotIndex();
    localObject1 = this;
    String str8 = b(i1);
    String str9 = paramSubscriptionInfo.getIccId();
    String str10 = b(paramSubscriptionInfo);
    i1 = paramSubscriptionInfo.getDataRoaming();
    i3 = 1;
    boolean bool;
    if (i1 == i3)
    {
      bool = true;
    }
    else
    {
      i1 = 0;
      str1 = null;
      bool = false;
    }
    ((SimInfo)localObject2).<init>(i2, str4, str5, str6, str3, str7, str8, str9, str10, bool);
    return (SimInfo)localObject2;
  }
  
  private String b(SubscriptionInfo paramSubscriptionInfo)
  {
    try
    {
      Method localMethod = l;
      TelephonyManager localTelephonyManager = d;
      int i1 = 1;
      Object[] arrayOfObject = new Object[i1];
      int i2 = paramSubscriptionInfo.getSubscriptionId();
      paramSubscriptionInfo = Integer.valueOf(i2);
      arrayOfObject[0] = paramSubscriptionInfo;
      paramSubscriptionInfo = localMethod.invoke(localTelephonyManager, arrayOfObject);
      return (String)paramSubscriptionInfo;
    }
    catch (IllegalAccessException|InvocationTargetException localIllegalAccessException) {}
    return null;
  }
  
  private static int h(String paramString)
  {
    try
    {
      return Integer.parseInt(paramString);
    }
    catch (NumberFormatException localNumberFormatException) {}
    return -1;
  }
  
  private Object i(String paramString)
  {
    try
    {
      Object localObject1 = i;
      Object localObject2 = e;
      Object localObject3 = new Object[0];
      localObject1 = ((Method)localObject1).invoke(localObject2, (Object[])localObject3);
      localObject1 = (List)localObject1;
      localObject1 = ((List)localObject1).iterator();
      boolean bool2;
      do
      {
        boolean bool1 = ((Iterator)localObject1).hasNext();
        if (!bool1) {
          break;
        }
        localObject2 = ((Iterator)localObject1).next();
        localObject3 = j;
        Object[] arrayOfObject = new Object[0];
        localObject3 = ((Method)localObject3).invoke(localObject2, arrayOfObject);
        bool2 = paramString.equals(localObject3);
      } while (!bool2);
      return localObject2;
    }
    catch (IllegalAccessException|InvocationTargetException localIllegalAccessException)
    {
      c.a();
    }
    return null;
  }
  
  public final SimInfo a(int paramInt)
  {
    com.truecaller.multisim.b.a locala = b;
    String[] arrayOfString = { "android.permission.READ_PHONE_STATE" };
    boolean bool = locala.a(arrayOfString);
    if (!bool) {
      return null;
    }
    SubscriptionInfo localSubscriptionInfo = g.getActiveSubscriptionInfoForSimSlotIndex(paramInt);
    return a(localSubscriptionInfo);
  }
  
  public String a()
  {
    return "LollipopMr1";
  }
  
  public final String a(Intent paramIntent)
  {
    return String.valueOf(paramIntent.getIntExtra("subscription", -1));
  }
  
  public void a(Intent paramIntent, String paramString)
  {
    Object localObject = b;
    String[] arrayOfString = { "android.permission.READ_PHONE_STATE" };
    boolean bool = ((com.truecaller.multisim.b.a)localObject).a(arrayOfString);
    if (bool)
    {
      paramString = i(paramString);
      if (paramString != null)
      {
        localObject = h;
        paramString = (Parcelable)paramString;
        paramIntent.putExtra((String)localObject, paramString);
      }
    }
  }
  
  public final void a(String paramString)
  {
    Object localObject1 = b;
    Object localObject2 = "android.permission.READ_PHONE_STATE";
    Object localObject3 = { "android.permission.MODIFY_PHONE_STATE", localObject2 };
    boolean bool = ((com.truecaller.multisim.b.a)localObject1).a((String[])localObject3);
    if (!bool)
    {
      super.a(paramString);
      return;
    }
    paramString = i(paramString);
    try
    {
      localObject1 = n;
      localObject3 = e;
      int i1 = 1;
      localObject2 = new Object[i1];
      localObject2[0] = paramString;
      ((Method)localObject1).invoke(localObject3, (Object[])localObject2);
      return;
    }
    catch (IllegalAccessException|InvocationTargetException localIllegalAccessException)
    {
      c.a();
    }
  }
  
  public final boolean a(String paramString1, String paramString2, String paramString3, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2, String paramString4)
  {
    String str = "-1";
    boolean bool = str.equals(paramString4);
    if (bool) {
      return false;
    }
    SmsManager.getSmsManagerForSubscriptionId(h(paramString4)).sendTextMessage(paramString1, paramString2, paramString3, paramPendingIntent1, paramPendingIntent2);
    return true;
  }
  
  public final boolean a(String paramString1, String paramString2, ArrayList paramArrayList1, ArrayList paramArrayList2, ArrayList paramArrayList3, String paramString3)
  {
    String str = "-1";
    boolean bool = str.equals(paramString3);
    if (bool) {
      return false;
    }
    SmsManager.getSmsManagerForSubscriptionId(h(paramString3)).sendMultipartTextMessage(paramString1, paramString2, paramArrayList1, paramArrayList2, paramArrayList3);
    return true;
  }
  
  public SimInfo b(String paramString)
  {
    Object localObject = b;
    String[] arrayOfString = { "android.permission.READ_PHONE_STATE" };
    boolean bool = ((com.truecaller.multisim.b.a)localObject).a(arrayOfString);
    if (!bool) {
      return null;
    }
    localObject = g;
    int i1 = h(paramString);
    paramString = ((SubscriptionManager)localObject).getActiveSubscriptionInfo(i1);
    return a(paramString);
  }
  
  String b(int paramInt)
  {
    try
    {
      Method localMethod = k;
      TelephonyManager localTelephonyManager = d;
      int i1 = 1;
      Object[] arrayOfObject = new Object[i1];
      Object localObject = Integer.valueOf(paramInt);
      arrayOfObject[0] = localObject;
      localObject = localMethod.invoke(localTelephonyManager, arrayOfObject);
      return (String)localObject;
    }
    catch (IllegalAccessException|InvocationTargetException localIllegalAccessException) {}
    return null;
  }
  
  public final String b(Intent paramIntent)
  {
    return String.valueOf(paramIntent.getIntExtra("subscription", -1));
  }
  
  public a c(String paramString)
  {
    Object localObject = "-1";
    boolean bool = ((String)localObject).equals(paramString);
    if (bool)
    {
      paramString = SmsManager.getDefault();
    }
    else
    {
      int i1 = h(paramString);
      paramString = SmsManager.getSmsManagerForSubscriptionId(i1);
    }
    paramString = paramString.getCarrierConfigValues();
    localObject = new com/truecaller/multisim/b;
    if (paramString == null)
    {
      paramString = new android/os/Bundle;
      paramString.<init>();
    }
    ((b)localObject).<init>(paramString);
    return (a)localObject;
  }
  
  public final String d(String paramString)
  {
    return d.getNetworkCountryIso();
  }
  
  public final String e(String paramString)
  {
    Object localObject1 = b;
    Object localObject2 = { "android.permission.READ_PHONE_STATE" };
    boolean bool = ((com.truecaller.multisim.b.a)localObject1).a((String[])localObject2);
    localObject2 = null;
    if (bool)
    {
      localObject1 = "-1";
      bool = ((String)localObject1).equals(paramString);
      if (!bool)
      {
        localObject1 = g;
        int i1 = h(paramString);
        paramString = ((SubscriptionManager)localObject1).getActiveSubscriptionInfo(i1);
        if (paramString != null)
        {
          paramString = paramString.getCountryIso();
          localObject2 = paramString;
        }
      }
    }
    return (String)localObject2;
  }
  
  public final boolean e()
  {
    return true;
  }
  
  public final SmsManager f(String paramString)
  {
    String str = "-1";
    boolean bool = str.equals(paramString);
    if (bool)
    {
      paramString = SmsManager.getDefault();
    }
    else
    {
      int i1 = h(paramString);
      paramString = SmsManager.getSmsManagerForSubscriptionId(i1);
    }
    return paramString;
  }
  
  public final String f()
  {
    return String.valueOf(SmsManager.getDefaultSmsSubscriptionId());
  }
  
  public String g()
  {
    Object localObject1 = b;
    Object localObject2 = "android.permission.READ_PHONE_STATE";
    Object localObject3 = { "android.permission.MODIFY_PHONE_STATE", localObject2 };
    boolean bool = ((com.truecaller.multisim.b.a)localObject1).a((String[])localObject3);
    if (!bool) {
      return super.g();
    }
    try
    {
      localObject1 = m;
      localObject3 = e;
      localObject2 = null;
      Object[] arrayOfObject = new Object[0];
      localObject1 = ((Method)localObject1).invoke(localObject3, arrayOfObject);
      if (localObject1 != null)
      {
        localObject3 = j;
        localObject2 = new Object[0];
        localObject1 = ((Method)localObject3).invoke(localObject1, (Object[])localObject2);
        localObject1 = (String)localObject1;
        if (localObject1 != null) {
          return (String)localObject1;
        }
      }
    }
    catch (IllegalAccessException|InvocationTargetException localIllegalAccessException)
    {
      c.a();
    }
    return "-1";
  }
  
  public final List h()
  {
    Object localObject1 = b;
    Object localObject2 = { "android.permission.READ_PHONE_STATE" };
    boolean bool1 = ((com.truecaller.multisim.b.a)localObject1).a((String[])localObject2);
    if (!bool1) {
      return Collections.emptyList();
    }
    localObject1 = g.getActiveSubscriptionInfoList();
    if (localObject1 == null) {
      return Collections.emptyList();
    }
    localObject2 = new java/util/ArrayList;
    int i1 = ((List)localObject1).size();
    ((ArrayList)localObject2).<init>(i1);
    localObject1 = ((List)localObject1).iterator();
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject1).hasNext();
      if (!bool2) {
        break;
      }
      Object localObject3 = (SubscriptionInfo)((Iterator)localObject1).next();
      localObject3 = a((SubscriptionInfo)localObject3);
      ((List)localObject2).add(localObject3);
    }
    return (List)localObject2;
  }
  
  public final boolean j()
  {
    Object localObject = b;
    String[] arrayOfString = { "android.permission.READ_PHONE_STATE" };
    boolean bool = ((com.truecaller.multisim.b.a)localObject).a(arrayOfString);
    if (bool)
    {
      localObject = g;
      int i1 = ((SubscriptionManager)localObject).getActiveSubscriptionInfoCount();
      int i2 = 1;
      if (i1 > i2) {
        return i2;
      }
    }
    return false;
  }
  
  public boolean k()
  {
    boolean bool = j();
    if (bool)
    {
      com.truecaller.multisim.b.b localb = new com/truecaller/multisim/b/b;
      Object localObject = d;
      localb.<init>((TelephonyManager)localObject);
      localObject = h();
      return localb.a((List)localObject);
    }
    return false;
  }
  
  public final boolean l()
  {
    return true;
  }
  
  public String m()
  {
    return "sub_id";
  }
  
  public String n()
  {
    return "sub_id";
  }
  
  protected String o()
  {
    return "subscription_id";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.multisim.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */