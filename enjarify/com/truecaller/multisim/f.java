package com.truecaller.multisim;

import android.database.Cursor;
import android.text.TextUtils;
import java.util.Iterator;
import java.util.List;

final class f
  extends e
{
  private final h a;
  
  f(Cursor paramCursor, h paramh)
  {
    super(paramCursor, str);
    a = paramh;
  }
  
  public final String a(String paramString)
  {
    Iterator localIterator = a.h().iterator();
    SimInfo localSimInfo;
    boolean bool2;
    do
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      localSimInfo = (SimInfo)localIterator.next();
      String str = h;
      bool2 = TextUtils.equals(paramString, str);
    } while (!bool2);
    return b;
    return "-1";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.multisim.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */