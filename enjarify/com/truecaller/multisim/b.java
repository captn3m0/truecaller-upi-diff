package com.truecaller.multisim;

import android.os.Bundle;
import android.text.TextUtils;
import java.util.Map;

final class b
  implements a
{
  private final Bundle a;
  
  b(Bundle paramBundle)
  {
    a = paramBundle;
  }
  
  public final boolean a()
  {
    return a.getBoolean("sendMultipartSmsAsSeparateMessages");
  }
  
  public final boolean b()
  {
    return a.getBoolean("enableSMSDeliveryReports", true);
  }
  
  public final boolean c()
  {
    return a.getBoolean("supportMmsContentDisposition", true);
  }
  
  public final boolean d()
  {
    return a.getBoolean("enabledNotifyWapMMSC", true);
  }
  
  public final String e()
  {
    return a.getString("uaProfTagName", "x-wap-profile");
  }
  
  public final Map f()
  {
    Object localObject1 = a;
    String str1 = "";
    localObject1 = ((Bundle)localObject1).getString("httpParams", str1);
    Object localObject2 = "|";
    localObject1 = TextUtils.split((String)localObject1, (String)localObject2);
    if (localObject1 != null)
    {
      int i = localObject1.length;
      if (i != 0)
      {
        localObject2 = new android/support/v4/f/a;
        int j = localObject1.length;
        ((android.support.v4.f.a)localObject2).<init>(j);
        j = localObject1.length;
        int k = 0;
        while (k < j)
        {
          Object localObject3 = localObject1[k];
          String str2 = ":";
          localObject3 = TextUtils.split((String)localObject3, str2);
          if (localObject3 != null)
          {
            int m = localObject3.length;
            int n = 2;
            if (m == n)
            {
              str2 = localObject3[0];
              n = 1;
              localObject3 = localObject3[n];
              ((Map)localObject2).put(str2, localObject3);
            }
          }
          k += 1;
        }
        return (Map)localObject2;
      }
    }
    return null;
  }
  
  public final boolean g()
  {
    return false;
  }
  
  public final int h()
  {
    Bundle localBundle = a;
    String str = "maxImageWidth";
    int i = localBundle.getInt(str);
    if (i <= 0) {
      i = 640;
    }
    return i;
  }
  
  public final int i()
  {
    Bundle localBundle = a;
    String str = "maxImageHeight";
    int i = localBundle.getInt(str);
    if (i <= 0) {
      i = 480;
    }
    return i;
  }
  
  public final int j()
  {
    Bundle localBundle = a;
    String str = "maxMessageSize";
    int i = localBundle.getInt(str);
    if (i <= 0) {
      i = 307200;
    }
    return i;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.multisim.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */