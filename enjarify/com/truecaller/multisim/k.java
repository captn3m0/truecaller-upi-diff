package com.truecaller.multisim;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.Telephony.Mms;
import android.provider.Telephony.Sms;
import android.telephony.TelephonyManager;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;

final class k
  extends n
{
  static final j d = -..Lambda.k.5huJLkiGRh0jlEFgrJB4vTD01tM.INSTANCE;
  private final String e;
  private final String g;
  private final Object h;
  private final Method i;
  private final Method j;
  
  private k(Context paramContext, TelephonyManager paramTelephonyManager)
  {
    super(paramContext, paramTelephonyManager);
    paramContext = (String)Telephony.Sms.class.getField("SUB_ID").get(null);
    e = paramContext;
    paramContext = (String)Telephony.Mms.class.getField("SUB_ID").get(null);
    g = paramContext;
    paramContext = Class.forName("android.telephony.MSimSmsManager");
    Class[] arrayOfClass = new Class[0];
    paramContext = paramContext.getMethod("getDefault", arrayOfClass);
    Object localObject = new Object[0];
    paramContext = paramContext.invoke(null, (Object[])localObject);
    h = paramContext;
    paramContext = h.getClass();
    int k = 6;
    arrayOfClass = new Class[k];
    arrayOfClass[0] = String.class;
    int m = 1;
    arrayOfClass[m] = String.class;
    int n = 2;
    arrayOfClass[n] = String.class;
    int i1 = 3;
    arrayOfClass[i1] = PendingIntent.class;
    int i2 = 4;
    arrayOfClass[i2] = PendingIntent.class;
    Class localClass1 = Long.TYPE;
    int i3 = 5;
    arrayOfClass[i3] = localClass1;
    paramContext = paramContext.getMethod("sendTextMessage", arrayOfClass);
    i = paramContext;
    paramContext = h.getClass();
    localObject = new Class[k];
    localObject[0] = String.class;
    localObject[m] = String.class;
    localObject[n] = ArrayList.class;
    localObject[i1] = ArrayList.class;
    localObject[i2] = ArrayList.class;
    Class localClass2 = Long.TYPE;
    localObject[i3] = localClass2;
    paramContext = paramContext.getMethod("sendMultipartTextMessage", (Class[])localObject);
    j = paramContext;
  }
  
  private static String c(Intent paramIntent)
  {
    return String.valueOf(paramIntent.getLongExtra("subscription", -1));
  }
  
  public final d a(Cursor paramCursor)
  {
    f localf = new com/truecaller/multisim/f;
    localf.<init>(paramCursor, this);
    return localf;
  }
  
  public final String a()
  {
    return "Lg";
  }
  
  public final String a(Intent paramIntent)
  {
    return c(paramIntent);
  }
  
  public final void a(Intent paramIntent, String paramString) {}
  
  public final boolean a(String paramString1, String paramString2, String paramString3, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2, String paramString4)
  {
    try
    {
      Method localMethod = i;
      Object localObject = h;
      int k = 6;
      Object[] arrayOfObject = new Object[k];
      arrayOfObject[0] = paramString1;
      boolean bool = true;
      arrayOfObject[bool] = paramString2;
      int m = 2;
      arrayOfObject[m] = paramString3;
      m = 3;
      arrayOfObject[m] = paramPendingIntent1;
      m = 4;
      arrayOfObject[m] = paramPendingIntent2;
      m = 5;
      paramString3 = Long.valueOf(paramString4);
      arrayOfObject[m] = paramString3;
      localMethod.invoke(localObject, arrayOfObject);
      return bool;
    }
    catch (Exception localException) {}
    return false;
  }
  
  public final boolean a(String paramString1, String paramString2, ArrayList paramArrayList1, ArrayList paramArrayList2, ArrayList paramArrayList3, String paramString3)
  {
    try
    {
      Method localMethod = j;
      Object localObject = h;
      int k = 6;
      Object[] arrayOfObject = new Object[k];
      arrayOfObject[0] = paramString1;
      boolean bool = true;
      arrayOfObject[bool] = paramString2;
      int m = 2;
      arrayOfObject[m] = paramArrayList1;
      m = 3;
      arrayOfObject[m] = paramArrayList2;
      m = 4;
      arrayOfObject[m] = paramArrayList3;
      m = 5;
      paramArrayList1 = Long.valueOf(paramString3);
      arrayOfObject[m] = paramArrayList1;
      localMethod.invoke(localObject, arrayOfObject);
      return bool;
    }
    catch (Exception localException) {}
    return false;
  }
  
  public final String b(Intent paramIntent)
  {
    return c(paramIntent);
  }
  
  public final a c(String paramString)
  {
    paramString = new com/truecaller/multisim/b;
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    paramString.<init>(localBundle);
    return paramString;
  }
  
  public final boolean e()
  {
    return false;
  }
  
  public final String m()
  {
    return e;
  }
  
  public final String n()
  {
    return g;
  }
  
  protected final String o()
  {
    return "iccid";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.multisim.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */