package com.truecaller.multisim;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

final class aa
  extends i
{
  static final j d = -..Lambda.aa.Rn9OIqr-XMjKkl5VQP2vEoOjnI0.INSTANCE;
  private final Object e;
  private final Method f;
  private final Method g;
  private final Method h;
  private final Method i;
  private final Method j;
  private final Method k;
  private final Method l;
  private final Method m;
  private final Method n;
  private final Method o;
  private final Method p;
  private final Object q;
  private final Method r;
  private final Method s;
  private final String t;
  private final String u;
  
  private aa(Context paramContext)
  {
    super(paramContext);
    paramContext = Class.forName("android.telephony.MSimTelephonyManager");
    Object localObject1 = new Class[0];
    Object localObject2 = paramContext.getMethod("getDefault", (Class[])localObject1);
    localObject1 = new Object[0];
    localObject2 = ((Method)localObject2).invoke(null, (Object[])localObject1);
    e = localObject2;
    int i1 = 1;
    Object localObject3 = new Class[i1];
    Object localObject4 = Integer.TYPE;
    localObject3[0] = localObject4;
    localObject2 = paramContext.getMethod("getNetworkOperatorName", (Class[])localObject3);
    f = ((Method)localObject2);
    localObject3 = new Class[i1];
    localObject4 = Integer.TYPE;
    localObject3[0] = localObject4;
    localObject2 = paramContext.getMethod("getLine1Number", (Class[])localObject3);
    g = ((Method)localObject2);
    localObject3 = new Class[i1];
    localObject4 = Integer.TYPE;
    localObject3[0] = localObject4;
    localObject2 = paramContext.getMethod("getSimOperator", (Class[])localObject3);
    h = ((Method)localObject2);
    localObject3 = new Class[i1];
    localObject4 = Integer.TYPE;
    localObject3[0] = localObject4;
    localObject2 = paramContext.getMethod("getSimCountryIso", (Class[])localObject3);
    i = ((Method)localObject2);
    localObject3 = new Class[i1];
    localObject4 = Integer.TYPE;
    localObject3[0] = localObject4;
    localObject2 = paramContext.getMethod("getDeviceId", (Class[])localObject3);
    j = ((Method)localObject2);
    localObject3 = new Class[i1];
    localObject4 = Integer.TYPE;
    localObject3[0] = localObject4;
    localObject2 = paramContext.getMethod("getSimSerialNumber", (Class[])localObject3);
    k = ((Method)localObject2);
    localObject3 = new Class[i1];
    localObject4 = Integer.TYPE;
    localObject3[0] = localObject4;
    localObject2 = paramContext.getMethod("isNetworkRoaming", (Class[])localObject3);
    l = ((Method)localObject2);
    localObject3 = new Class[i1];
    localObject4 = Integer.TYPE;
    localObject3[0] = localObject4;
    localObject2 = paramContext.getMethod("getNetworkCountryIso", (Class[])localObject3);
    m = ((Method)localObject2);
    localObject3 = new Class[i1];
    localObject4 = Integer.TYPE;
    localObject3[0] = localObject4;
    localObject2 = paramContext.getMethod("getSubscriberId", (Class[])localObject3);
    n = ((Method)localObject2);
    localObject3 = new Class[0];
    localObject2 = paramContext.getMethod("isMultiSimEnabled", (Class[])localObject3);
    o = ((Method)localObject2);
    localObject3 = new Class[0];
    paramContext = paramContext.getMethod("getDefaultSubscription", (Class[])localObject3);
    p = paramContext;
    paramContext = Class.forName("android.telephony.MSimSmsManager");
    localObject3 = new Class[0];
    localObject2 = paramContext.getMethod("getDefault", (Class[])localObject3);
    localObject3 = new Object[0];
    localObject2 = ((Method)localObject2).invoke(paramContext, (Object[])localObject3);
    q = localObject2;
    int i2 = 6;
    localObject4 = new Class[i2];
    localObject4[0] = String.class;
    localObject4[i1] = String.class;
    int i3 = 2;
    localObject4[i3] = String.class;
    int i4 = 3;
    localObject4[i4] = PendingIntent.class;
    int i5 = 4;
    localObject4[i5] = PendingIntent.class;
    Class localClass1 = Integer.TYPE;
    int i6 = 5;
    localObject4[i6] = localClass1;
    localObject2 = paramContext.getMethod("sendTextMessage", (Class[])localObject4);
    r = ((Method)localObject2);
    localObject3 = new Class[i2];
    localObject3[0] = String.class;
    localObject3[i1] = String.class;
    localObject3[i3] = ArrayList.class;
    localObject3[i4] = ArrayList.class;
    localObject3[i5] = ArrayList.class;
    Class localClass2 = Integer.TYPE;
    localObject3[i6] = localClass2;
    paramContext = paramContext.getMethod("sendMultipartTextMessage", (Class[])localObject3);
    s = paramContext;
    paramContext = Class.forName("android.provider.Telephony$Sms");
    paramContext = (String)paramContext.getField("SUB_ID").get(paramContext);
    t = paramContext;
    paramContext = Class.forName("android.provider.Telephony$Mms");
    paramContext = (String)paramContext.getField("SUB_ID").get(paramContext);
    u = paramContext;
  }
  
  private String b(int paramInt)
  {
    try
    {
      Method localMethod = f;
      Object localObject1 = e;
      int i1 = 1;
      Object[] arrayOfObject = new Object[i1];
      Object localObject2 = Integer.valueOf(paramInt);
      arrayOfObject[0] = localObject2;
      localObject2 = localMethod.invoke(localObject1, arrayOfObject);
      return (String)localObject2;
    }
    catch (Exception localException) {}
    return null;
  }
  
  private String c(int paramInt)
  {
    try
    {
      Method localMethod = g;
      Object localObject1 = e;
      int i1 = 1;
      Object[] arrayOfObject = new Object[i1];
      Object localObject2 = Integer.valueOf(paramInt);
      arrayOfObject[0] = localObject2;
      localObject2 = localMethod.invoke(localObject1, arrayOfObject);
      return (String)localObject2;
    }
    catch (Exception localException) {}
    return null;
  }
  
  private String c(Intent paramIntent)
  {
    int i1 = paramIntent.getIntExtra("subscription", -1);
    return j(i1);
  }
  
  private String d(int paramInt)
  {
    try
    {
      Method localMethod = h;
      Object localObject1 = e;
      int i1 = 1;
      Object[] arrayOfObject = new Object[i1];
      Object localObject2 = Integer.valueOf(paramInt);
      arrayOfObject[0] = localObject2;
      localObject2 = localMethod.invoke(localObject1, arrayOfObject);
      return (String)localObject2;
    }
    catch (Exception localException) {}
    return "";
  }
  
  private String e(int paramInt)
  {
    try
    {
      Method localMethod = i;
      Object localObject1 = e;
      int i1 = 1;
      Object[] arrayOfObject = new Object[i1];
      Object localObject2 = Integer.valueOf(paramInt);
      arrayOfObject[0] = localObject2;
      localObject2 = localMethod.invoke(localObject1, arrayOfObject);
      return (String)localObject2;
    }
    catch (Exception localException) {}
    return null;
  }
  
  private String f(int paramInt)
  {
    try
    {
      Method localMethod = j;
      Object localObject1 = e;
      int i1 = 1;
      Object[] arrayOfObject = new Object[i1];
      Object localObject2 = Integer.valueOf(paramInt);
      arrayOfObject[0] = localObject2;
      localObject2 = localMethod.invoke(localObject1, arrayOfObject);
      return (String)localObject2;
    }
    catch (Exception localException) {}
    return null;
  }
  
  private String g(int paramInt)
  {
    try
    {
      Method localMethod = k;
      Object localObject1 = e;
      int i1 = 1;
      Object[] arrayOfObject = new Object[i1];
      Object localObject2 = Integer.valueOf(paramInt);
      arrayOfObject[0] = localObject2;
      localObject2 = localMethod.invoke(localObject1, arrayOfObject);
      return (String)localObject2;
    }
    catch (Exception localException) {}
    return null;
  }
  
  private int h(String paramString)
  {
    try
    {
      Object localObject1 = n;
      Object localObject2 = e;
      int i1 = 1;
      Object[] arrayOfObject = new Object[i1];
      Integer localInteger = Integer.valueOf(0);
      arrayOfObject[0] = localInteger;
      localObject1 = ((Method)localObject1).invoke(localObject2, arrayOfObject);
      boolean bool1 = paramString.equals(localObject1);
      if (bool1) {
        return 0;
      }
      localObject1 = n;
      localObject2 = e;
      arrayOfObject = new Object[i1];
      localInteger = Integer.valueOf(i1);
      arrayOfObject[0] = localInteger;
      localObject1 = ((Method)localObject1).invoke(localObject2, arrayOfObject);
      boolean bool2 = paramString.equals(localObject1);
      if (bool2) {
        return i1;
      }
    }
    catch (Exception localException) {}
    return -1;
  }
  
  private boolean h(int paramInt)
  {
    try
    {
      Method localMethod = l;
      Object localObject1 = e;
      int i1 = 1;
      Object[] arrayOfObject = new Object[i1];
      Object localObject2 = Integer.valueOf(paramInt);
      arrayOfObject[0] = localObject2;
      localObject2 = localMethod.invoke(localObject1, arrayOfObject);
      localObject2 = (Boolean)localObject2;
      return ((Boolean)localObject2).booleanValue();
    }
    catch (Exception localException) {}
    return false;
  }
  
  private String i(int paramInt)
  {
    try
    {
      Method localMethod = m;
      Object localObject1 = e;
      int i1 = 1;
      Object[] arrayOfObject = new Object[i1];
      Object localObject2 = Integer.valueOf(paramInt);
      arrayOfObject[0] = localObject2;
      localObject2 = localMethod.invoke(localObject1, arrayOfObject);
      return (String)localObject2;
    }
    catch (Exception localException) {}
    return null;
  }
  
  private String j(int paramInt)
  {
    try
    {
      Method localMethod = n;
      Object localObject1 = e;
      int i1 = 1;
      Object[] arrayOfObject = new Object[i1];
      Object localObject2 = Integer.valueOf(paramInt);
      arrayOfObject[0] = localObject2;
      localObject2 = localMethod.invoke(localObject1, arrayOfObject);
      localObject2 = (String)localObject2;
      if (localObject2 == null) {
        localObject2 = "-1";
      }
      return (String)localObject2;
    }
    catch (Exception localException) {}
    return "-1";
  }
  
  public final SimInfo a(int paramInt)
  {
    String str1 = j(paramInt);
    Object localObject = "-1";
    boolean bool1 = ((String)localObject).equals(str1);
    if (bool1) {
      return null;
    }
    SimInfo localSimInfo = new com/truecaller/multisim/SimInfo;
    String str2 = c(paramInt);
    String str3 = b(paramInt);
    String str4 = d(paramInt);
    String str5 = e(paramInt);
    String str6 = f(paramInt);
    String str7 = g(paramInt);
    boolean bool2 = h(paramInt);
    localObject = localSimInfo;
    localSimInfo.<init>(paramInt, str1, str2, str3, str4, str5, str6, str7, null, bool2);
    return localSimInfo;
  }
  
  public final String a()
  {
    return "Motorola";
  }
  
  public final String a(Intent paramIntent)
  {
    return c(paramIntent);
  }
  
  public final void a(Intent paramIntent, String paramString) {}
  
  public final boolean a(String paramString1, String paramString2, String paramString3, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2, String paramString4)
  {
    boolean bool1 = j();
    boolean bool2 = true;
    if (bool1)
    {
      Object localObject1 = paramString4;
      int i1 = h(paramString4);
      try
      {
        Method localMethod = r;
        Object localObject2 = q;
        int i2 = 6;
        Object[] arrayOfObject = new Object[i2];
        arrayOfObject[0] = paramString1;
        arrayOfObject[bool2] = paramString2;
        int i3 = 2;
        arrayOfObject[i3] = paramString3;
        i3 = 3;
        arrayOfObject[i3] = paramPendingIntent1;
        i3 = 4;
        arrayOfObject[i3] = paramPendingIntent2;
        i3 = 5;
        localObject1 = Integer.valueOf(i1);
        arrayOfObject[i3] = localObject1;
        localMethod.invoke(localObject2, arrayOfObject);
        return bool2;
      }
      catch (Exception localException)
      {
        return false;
      }
    }
    SmsManager.getDefault().sendTextMessage(paramString1, paramString2, paramString3, paramPendingIntent1, paramPendingIntent2);
    return bool2;
  }
  
  public final boolean a(String paramString1, String paramString2, ArrayList paramArrayList1, ArrayList paramArrayList2, ArrayList paramArrayList3, String paramString3)
  {
    boolean bool1 = j();
    boolean bool2 = true;
    if (bool1)
    {
      Object localObject1 = paramString3;
      int i1 = h(paramString3);
      try
      {
        Method localMethod = s;
        Object localObject2 = q;
        int i2 = 6;
        Object[] arrayOfObject = new Object[i2];
        arrayOfObject[0] = paramString1;
        arrayOfObject[bool2] = paramString2;
        int i3 = 2;
        arrayOfObject[i3] = paramArrayList1;
        i3 = 3;
        arrayOfObject[i3] = paramArrayList2;
        i3 = 4;
        arrayOfObject[i3] = paramArrayList3;
        i3 = 5;
        localObject1 = Integer.valueOf(i1);
        arrayOfObject[i3] = localObject1;
        localMethod.invoke(localObject2, arrayOfObject);
        return bool2;
      }
      catch (Exception localException)
      {
        return false;
      }
    }
    SmsManager.getDefault().sendMultipartTextMessage(paramString1, paramString2, paramArrayList1, paramArrayList2, paramArrayList3);
    return bool2;
  }
  
  public final SimInfo b(String paramString)
  {
    int i1 = h(paramString);
    int i2 = -1;
    if (i1 == i2) {
      return null;
    }
    SimInfo localSimInfo = new com/truecaller/multisim/SimInfo;
    String str1 = c(i1);
    String str2 = b(i1);
    String str3 = d(i1);
    String str4 = e(i1);
    String str5 = f(i1);
    String str6 = g(i1);
    boolean bool = h(i1);
    localSimInfo.<init>(i1, paramString, str1, str2, str3, str4, str5, str6, null, bool);
    return localSimInfo;
  }
  
  public final String b(Intent paramIntent)
  {
    return c(paramIntent);
  }
  
  public final a c(String paramString)
  {
    paramString = new com/truecaller/multisim/b;
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    paramString.<init>(localBundle);
    return paramString;
  }
  
  public final String d(String paramString)
  {
    int i1 = h(paramString);
    int i2 = -1;
    if (i1 != i2) {
      return i(i1);
    }
    return null;
  }
  
  public final String e(String paramString)
  {
    int i1 = h(paramString);
    int i2 = -1;
    if (i1 != i2) {
      return e(i1);
    }
    return null;
  }
  
  public final boolean e()
  {
    return false;
  }
  
  public final String f()
  {
    try
    {
      Object localObject1 = p;
      Object localObject2 = e;
      Object[] arrayOfObject = null;
      arrayOfObject = new Object[0];
      localObject1 = ((Method)localObject1).invoke(localObject2, arrayOfObject);
      localObject1 = (Integer)localObject1;
      int i1 = ((Integer)localObject1).intValue();
      return j(i1);
    }
    catch (Exception localException) {}
    return "-1";
  }
  
  public final List h()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    int i1 = 0;
    for (;;)
    {
      int i2 = 2;
      if (i1 >= i2) {
        break;
      }
      SimInfo localSimInfo = a(i1);
      if (localSimInfo != null) {
        localArrayList.add(localSimInfo);
      }
      i1 += 1;
    }
    return localArrayList;
  }
  
  public final boolean j()
  {
    try
    {
      Object localObject1 = o;
      Object localObject2 = e;
      Object[] arrayOfObject = new Object[0];
      localObject1 = ((Method)localObject1).invoke(localObject2, arrayOfObject);
      localObject1 = (Boolean)localObject1;
      return ((Boolean)localObject1).booleanValue();
    }
    catch (Exception localException) {}
    return false;
  }
  
  public final boolean k()
  {
    return j();
  }
  
  public final String m()
  {
    return t;
  }
  
  public final String n()
  {
    return u;
  }
  
  protected final String o()
  {
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.multisim.aa
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */