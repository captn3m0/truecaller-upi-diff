package com.truecaller.multisim;

import android.content.Context;
import android.telecom.TelecomManager;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import com.truecaller.multisim.b.a;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

final class ad
  extends ac
{
  static final j e = -..Lambda.ad._xU773MBE8u1-sOEpgJMRqsRQuA.INSTANCE;
  private final SubscriptionManager f;
  
  private ad(Context paramContext, TelecomManager paramTelecomManager, SubscriptionManager paramSubscriptionManager)
  {
    super(paramContext, paramTelecomManager);
    f = paramSubscriptionManager;
  }
  
  public final String a()
  {
    return "SamsungLollipopMr1";
  }
  
  public final List h()
  {
    Object localObject1 = b;
    Object localObject2 = { "android.permission.READ_PHONE_STATE" };
    boolean bool1 = ((a)localObject1).a((String[])localObject2);
    if (!bool1) {
      return Collections.emptyList();
    }
    localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    localObject2 = f.getActiveSubscriptionInfoList();
    if (localObject2 != null)
    {
      localObject2 = ((List)localObject2).iterator();
      for (;;)
      {
        boolean bool2 = ((Iterator)localObject2).hasNext();
        if (!bool2) {
          break;
        }
        int i = ((SubscriptionInfo)((Iterator)localObject2).next()).getSubscriptionId();
        Object localObject3 = String.valueOf(i);
        localObject3 = b((String)localObject3);
        if (localObject3 != null) {
          ((List)localObject1).add(localObject3);
        }
      }
    }
    return (List)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.multisim.ad
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */