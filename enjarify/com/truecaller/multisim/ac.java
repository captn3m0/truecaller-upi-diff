package com.truecaller.multisim;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Parcelable;
import android.telecom.TelecomManager;
import android.telephony.SmsManager;
import android.text.TextUtils;
import com.truecaller.multisim.b.c;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ac
  extends i
{
  static final j d = -..Lambda.ac.yJOpyeu4UIx4dyBLNAuZgZeOLsY.INSTANCE;
  private final TelecomManager e;
  private final Method f;
  private final Method g;
  private final Method h;
  private final Method i;
  private final Method j;
  private final Method k;
  private final Method l;
  private final Method m;
  private final Method n;
  private final Method o;
  private final Method p;
  private final Method q;
  private final Method r;
  private final Method s;
  private final Field t;
  private final String u;
  private final Method v;
  private final Method w;
  
  ac(Context paramContext, TelecomManager paramTelecomManager)
  {
    super(paramContext);
    e = paramTelecomManager;
    paramContext = Class.forName("com.samsung.android.telephony.MultiSimManager");
    int i1 = 1;
    Class[] arrayOfClass1 = new Class[i1];
    arrayOfClass1[0] = Context.class;
    paramTelecomManager = paramContext.getMethod("getEnabledSimCount", arrayOfClass1);
    f = paramTelecomManager;
    arrayOfClass1 = new Class[0];
    paramTelecomManager = paramContext.getMethod("getActiveSubInfoCount", arrayOfClass1);
    g = paramTelecomManager;
    arrayOfClass1 = new Class[i1];
    Class localClass = Integer.TYPE;
    arrayOfClass1[0] = localClass;
    paramTelecomManager = paramContext.getMethod("getSimOperatorName", arrayOfClass1);
    h = paramTelecomManager;
    arrayOfClass1 = new Class[i1];
    localClass = Integer.TYPE;
    arrayOfClass1[0] = localClass;
    paramTelecomManager = paramContext.getMethod("getLine1Number", arrayOfClass1);
    i = paramTelecomManager;
    arrayOfClass1 = new Class[i1];
    localClass = Integer.TYPE;
    arrayOfClass1[0] = localClass;
    paramTelecomManager = paramContext.getMethod("getSubId", arrayOfClass1);
    j = paramTelecomManager;
    arrayOfClass1 = new Class[i1];
    localClass = Integer.TYPE;
    arrayOfClass1[0] = localClass;
    paramTelecomManager = paramContext.getMethod("getSimOperator", arrayOfClass1);
    k = paramTelecomManager;
    arrayOfClass1 = new Class[i1];
    localClass = Integer.TYPE;
    arrayOfClass1[0] = localClass;
    paramTelecomManager = paramContext.getMethod("getSimCountryIso", arrayOfClass1);
    l = paramTelecomManager;
    arrayOfClass1 = new Class[i1];
    localClass = Integer.TYPE;
    arrayOfClass1[0] = localClass;
    paramTelecomManager = paramContext.getMethod("getImei", arrayOfClass1);
    m = paramTelecomManager;
    arrayOfClass1 = new Class[i1];
    localClass = Integer.TYPE;
    arrayOfClass1[0] = localClass;
    paramTelecomManager = paramContext.getMethod("getSimSerialNumber", arrayOfClass1);
    n = paramTelecomManager;
    arrayOfClass1 = new Class[i1];
    localClass = Integer.TYPE;
    arrayOfClass1[0] = localClass;
    paramTelecomManager = paramContext.getMethod("isNetworkRoaming", arrayOfClass1);
    o = paramTelecomManager;
    arrayOfClass1 = new Class[i1];
    localClass = Integer.TYPE;
    arrayOfClass1[0] = localClass;
    paramTelecomManager = paramContext.getMethod("getNetworkCountryIso", arrayOfClass1);
    p = paramTelecomManager;
    arrayOfClass1 = new Class[i1];
    localClass = Integer.TYPE;
    arrayOfClass1[0] = localClass;
    paramTelecomManager = paramContext.getMethod("getDefaultSubId", arrayOfClass1);
    q = paramTelecomManager;
    arrayOfClass1 = new Class[2];
    localClass = Integer.TYPE;
    arrayOfClass1[0] = localClass;
    localClass = Long.TYPE;
    arrayOfClass1[i1] = localClass;
    paramTelecomManager = paramContext.getMethod("setDefaultSubId", arrayOfClass1);
    r = paramTelecomManager;
    Class[] arrayOfClass2 = new Class[0];
    paramContext = paramContext.getMethod("getActiveSubInfoList", arrayOfClass2);
    s = paramContext;
    paramContext = Class.forName("android.telephony.SubInfoRecord").getField("subId");
    t = paramContext;
    paramContext = (String)TelecomManager.class.getField("EXTRA_PHONE_ACCOUNT_HANDLE").get(null);
    u = paramContext;
    arrayOfClass2 = new Class[0];
    paramContext = TelecomManager.class.getMethod("getCallCapablePhoneAccounts", arrayOfClass2);
    v = paramContext;
    paramContext = Class.forName("android.telecom.PhoneAccountHandle");
    arrayOfClass2 = new Class[0];
    paramContext = paramContext.getMethod("getId", arrayOfClass2);
    w = paramContext;
  }
  
  private String b(int paramInt)
  {
    try
    {
      Method localMethod = h;
      int i1 = 1;
      Object[] arrayOfObject = new Object[i1];
      Object localObject = Integer.valueOf(paramInt);
      arrayOfObject[0] = localObject;
      localObject = localMethod.invoke(null, arrayOfObject);
      return (String)localObject;
    }
    finally {}
    return null;
  }
  
  private String c(int paramInt)
  {
    try
    {
      Method localMethod = i;
      int i1 = 1;
      Object[] arrayOfObject = new Object[i1];
      Object localObject = Integer.valueOf(paramInt);
      arrayOfObject[0] = localObject;
      localObject = localMethod.invoke(null, arrayOfObject);
      return (String)localObject;
    }
    catch (Exception localException) {}
    return null;
  }
  
  private String c(Intent paramIntent)
  {
    Object localObject = paramIntent.getStringExtra("simSlot");
    if (localObject != null) {
      try
      {
        Method localMethod = j;
        int i1 = 1;
        Object[] arrayOfObject = new Object[i1];
        localObject = Integer.valueOf((String)localObject);
        arrayOfObject[0] = localObject;
        localObject = localMethod.invoke(null, arrayOfObject);
        localObject = (long[])localObject;
        localObject = (long[])localObject;
        if (localObject != null)
        {
          int i2 = localObject.length;
          if (i2 > 0)
          {
            long l1 = localObject[0];
            return String.valueOf(l1);
          }
        }
      }
      catch (Exception localException) {}
    }
    localObject = paramIntent.getStringExtra("subId");
    if (localObject != null) {
      return (String)localObject;
    }
    localObject = paramIntent.getStringExtra("subscription");
    if (localObject != null) {
      return (String)localObject;
    }
    return String.valueOf(paramIntent.getIntExtra("subscription", -1));
  }
  
  private String d(int paramInt)
  {
    try
    {
      Method localMethod = k;
      int i1 = 1;
      Object[] arrayOfObject = new Object[i1];
      Object localObject = Integer.valueOf(paramInt);
      arrayOfObject[0] = localObject;
      localObject = localMethod.invoke(null, arrayOfObject);
      return (String)localObject;
    }
    catch (Exception localException) {}
    return "";
  }
  
  private String e(int paramInt)
  {
    try
    {
      Method localMethod = l;
      int i1 = 1;
      Object[] arrayOfObject = new Object[i1];
      Object localObject = Integer.valueOf(paramInt);
      arrayOfObject[0] = localObject;
      localObject = localMethod.invoke(null, arrayOfObject);
      return (String)localObject;
    }
    catch (Exception localException) {}
    return null;
  }
  
  private String f(int paramInt)
  {
    try
    {
      Method localMethod = m;
      int i1 = 1;
      Object[] arrayOfObject = new Object[i1];
      Object localObject = Integer.valueOf(paramInt);
      arrayOfObject[0] = localObject;
      localObject = localMethod.invoke(null, arrayOfObject);
      return (String)localObject;
    }
    catch (Exception localException) {}
    return null;
  }
  
  private String g(int paramInt)
  {
    try
    {
      Method localMethod = n;
      int i1 = 1;
      Object[] arrayOfObject = new Object[i1];
      Object localObject = Integer.valueOf(paramInt);
      arrayOfObject[0] = localObject;
      localObject = localMethod.invoke(null, arrayOfObject);
      return (String)localObject;
    }
    catch (Exception localException) {}
    return null;
  }
  
  private static SmsManager h(String paramString)
  {
    Object localObject = "android.telephony.SmsManager";
    try
    {
      localObject = Class.forName((String)localObject);
      String str = "getSmsManagerForSubscriber";
      int i1 = 1;
      Class[] arrayOfClass = new Class[i1];
      Class localClass = Long.TYPE;
      arrayOfClass[0] = localClass;
      localObject = ((Class)localObject).getMethod(str, arrayOfClass);
      str = null;
      Object[] arrayOfObject = new Object[i1];
      paramString = Long.valueOf(paramString);
      arrayOfObject[0] = paramString;
      paramString = ((Method)localObject).invoke(null, arrayOfObject);
      return (SmsManager)paramString;
    }
    catch (Exception localException) {}
    return SmsManager.getDefault();
  }
  
  private boolean h(int paramInt)
  {
    try
    {
      Method localMethod = o;
      int i1 = 1;
      Object[] arrayOfObject = new Object[i1];
      Object localObject = Integer.valueOf(paramInt);
      arrayOfObject[0] = localObject;
      localObject = localMethod.invoke(null, arrayOfObject);
      localObject = (Boolean)localObject;
      return ((Boolean)localObject).booleanValue();
    }
    catch (Exception localException) {}
    return false;
  }
  
  private int i(String paramString)
  {
    try
    {
      Object localObject = j;
      int i1 = 1;
      Object[] arrayOfObject = new Object[i1];
      Integer localInteger1 = Integer.valueOf(0);
      arrayOfObject[0] = localInteger1;
      localInteger1 = null;
      localObject = ((Method)localObject).invoke(null, arrayOfObject);
      localObject = (long[])localObject;
      localObject = (long[])localObject;
      int i2;
      if (localObject != null)
      {
        i2 = localObject.length;
        if (i2 > 0)
        {
          long l1 = localObject[0];
          localObject = String.valueOf(l1);
          boolean bool1 = TextUtils.equals((CharSequence)localObject, paramString);
          if (bool1) {
            return 0;
          }
        }
      }
      localObject = j;
      arrayOfObject = new Object[i1];
      Integer localInteger2 = Integer.valueOf(i1);
      arrayOfObject[0] = localInteger2;
      localObject = ((Method)localObject).invoke(null, arrayOfObject);
      localObject = (long[])localObject;
      localObject = (long[])localObject;
      if (localObject != null)
      {
        i2 = localObject.length;
        if (i2 > 0)
        {
          long l2 = localObject[0];
          localObject = String.valueOf(l2);
          boolean bool2 = TextUtils.equals((CharSequence)localObject, paramString);
          if (bool2) {
            return i1;
          }
        }
      }
    }
    catch (Exception localException)
    {
      c.a();
    }
    return -1;
  }
  
  private String i(int paramInt)
  {
    try
    {
      Method localMethod = p;
      int i1 = 1;
      Object[] arrayOfObject = new Object[i1];
      Object localObject = Integer.valueOf(paramInt);
      arrayOfObject[0] = localObject;
      localObject = localMethod.invoke(null, arrayOfObject);
      return (String)localObject;
    }
    catch (Exception localException) {}
    return null;
  }
  
  public final SimInfo a(int paramInt)
  {
    Object localObject1 = "-1";
    Object localObject3;
    try
    {
      localObject2 = j;
      i1 = 1;
      Object[] arrayOfObject = new Object[i1];
      localObject3 = Integer.valueOf(paramInt);
      str1 = null;
      arrayOfObject[0] = localObject3;
      localObject2 = ((Method)localObject2).invoke(null, arrayOfObject);
      localObject2 = (long[])localObject2;
      localObject2 = (long[])localObject2;
      if (localObject2 != null)
      {
        i1 = localObject2.length;
        if (i1 > 0)
        {
          long l1 = localObject2[0];
          localObject1 = String.valueOf(l1);
        }
      }
      localObject3 = localObject1;
    }
    catch (Exception localException)
    {
      localObject3 = localObject1;
    }
    localObject1 = "-1";
    boolean bool1 = ((String)localObject1).equals(localObject3);
    if (bool1) {
      return null;
    }
    localObject1 = new com/truecaller/multisim/SimInfo;
    String str1 = c(paramInt);
    String str2 = b(paramInt);
    String str3 = d(paramInt);
    String str4 = e(paramInt);
    String str5 = f(paramInt);
    String str6 = g(paramInt);
    boolean bool2 = h(paramInt);
    Object localObject2 = localObject1;
    int i1 = paramInt;
    ((SimInfo)localObject1).<init>(paramInt, (String)localObject3, str1, str2, str3, str4, str5, str6, null, bool2);
    return (SimInfo)localObject1;
  }
  
  public final d a(Cursor paramCursor)
  {
    g localg = new com/truecaller/multisim/g;
    localg.<init>(paramCursor, this);
    return localg;
  }
  
  public String a()
  {
    return "SamsungLollipop";
  }
  
  public final String a(Intent paramIntent)
  {
    return c(paramIntent);
  }
  
  public final void a(Intent paramIntent, String paramString)
  {
    Object localObject1 = b;
    Object localObject2 = { "android.permission.READ_PHONE_STATE" };
    boolean bool1 = ((com.truecaller.multisim.b.a)localObject1).a((String[])localObject2);
    if (bool1) {
      try
      {
        localObject1 = v;
        localObject2 = e;
        Object localObject3 = new Object[0];
        localObject1 = ((Method)localObject1).invoke(localObject2, (Object[])localObject3);
        localObject1 = (List)localObject1;
        localObject1 = ((List)localObject1).iterator();
        for (;;)
        {
          boolean bool2 = ((Iterator)localObject1).hasNext();
          if (!bool2) {
            break;
          }
          localObject2 = ((Iterator)localObject1).next();
          localObject3 = w;
          Object[] arrayOfObject = new Object[0];
          localObject3 = ((Method)localObject3).invoke(localObject2, arrayOfObject);
          boolean bool3 = paramString.equals(localObject3);
          if (bool3)
          {
            localObject3 = u;
            localObject2 = (Parcelable)localObject2;
            paramIntent.putExtra((String)localObject3, (Parcelable)localObject2);
          }
        }
        return;
      }
      catch (IllegalAccessException|InvocationTargetException localIllegalAccessException)
      {
        c.a();
      }
    }
  }
  
  public final boolean a(String paramString1, String paramString2, String paramString3, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2, String paramString4)
  {
    try
    {
      Object localObject1 = q;
      boolean bool = true;
      Object[] arrayOfObject = new Object[bool];
      int i1 = 2;
      Integer localInteger = Integer.valueOf(i1);
      arrayOfObject[0] = localInteger;
      localInteger = null;
      localObject1 = ((Method)localObject1).invoke(null, arrayOfObject);
      localObject1 = (Long)localObject1;
      long l1 = ((Long)localObject1).longValue();
      localObject1 = r;
      arrayOfObject = new Object[i1];
      Object localObject2 = Integer.valueOf(i1);
      arrayOfObject[0] = localObject2;
      localObject2 = Long.valueOf(paramString4);
      arrayOfObject[bool] = localObject2;
      ((Method)localObject1).invoke(null, arrayOfObject);
      SmsManager localSmsManager = h(paramString4);
      localSmsManager.sendTextMessage(paramString1, paramString2, paramString3, paramPendingIntent1, paramPendingIntent2);
      localObject1 = r;
      arrayOfObject = new Object[i1];
      Object localObject3 = Integer.valueOf(i1);
      arrayOfObject[0] = localObject3;
      localObject3 = Long.valueOf(l1);
      arrayOfObject[bool] = localObject3;
      ((Method)localObject1).invoke(null, arrayOfObject);
      return bool;
    }
    catch (Exception localException) {}
    return false;
  }
  
  public final boolean a(String paramString1, String paramString2, ArrayList paramArrayList1, ArrayList paramArrayList2, ArrayList paramArrayList3, String paramString3)
  {
    try
    {
      Object localObject1 = q;
      boolean bool = true;
      Object[] arrayOfObject = new Object[bool];
      int i1 = 2;
      Integer localInteger = Integer.valueOf(i1);
      arrayOfObject[0] = localInteger;
      localInteger = null;
      localObject1 = ((Method)localObject1).invoke(null, arrayOfObject);
      localObject1 = (Long)localObject1;
      long l1 = ((Long)localObject1).longValue();
      localObject1 = r;
      arrayOfObject = new Object[i1];
      Object localObject2 = Integer.valueOf(i1);
      arrayOfObject[0] = localObject2;
      localObject2 = Long.valueOf(paramString3);
      arrayOfObject[bool] = localObject2;
      ((Method)localObject1).invoke(null, arrayOfObject);
      SmsManager localSmsManager = h(paramString3);
      localSmsManager.sendMultipartTextMessage(paramString1, paramString2, paramArrayList1, paramArrayList2, paramArrayList3);
      localObject1 = r;
      arrayOfObject = new Object[i1];
      Object localObject3 = Integer.valueOf(i1);
      arrayOfObject[0] = localObject3;
      localObject3 = Long.valueOf(l1);
      arrayOfObject[bool] = localObject3;
      ((Method)localObject1).invoke(null, arrayOfObject);
      return bool;
    }
    catch (Exception localException) {}
    return false;
  }
  
  public final SimInfo b(String paramString)
  {
    int i1 = i(paramString);
    int i2 = -1;
    if (i1 == i2) {
      return null;
    }
    SimInfo localSimInfo = new com/truecaller/multisim/SimInfo;
    String str1 = c(i1);
    String str2 = b(i1);
    String str3 = d(i1);
    String str4 = e(i1);
    String str5 = f(i1);
    String str6 = g(i1);
    boolean bool = h(i1);
    localSimInfo.<init>(i1, paramString, str1, str2, str3, str4, str5, str6, null, bool);
    return localSimInfo;
  }
  
  public final String b(Intent paramIntent)
  {
    return c(paramIntent);
  }
  
  public final a c(String paramString)
  {
    paramString = new com/truecaller/multisim/b;
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    paramString.<init>(localBundle);
    return paramString;
  }
  
  public final String d(String paramString)
  {
    int i1 = i(paramString);
    int i2 = -1;
    if (i1 != i2) {
      return i(i1);
    }
    return null;
  }
  
  public final String e(String paramString)
  {
    int i1 = i(paramString);
    int i2 = -1;
    if (i1 != i2) {
      return e(i1);
    }
    return null;
  }
  
  public final boolean e()
  {
    return true;
  }
  
  public final SmsManager f(String paramString)
  {
    return h(paramString);
  }
  
  public final String f()
  {
    try
    {
      Object localObject = q;
      int i1 = 1;
      Object[] arrayOfObject = new Object[i1];
      int i2 = 2;
      Integer localInteger = Integer.valueOf(i2);
      arrayOfObject[0] = localInteger;
      localObject = ((Method)localObject).invoke(null, arrayOfObject);
      localObject = (Long)localObject;
      long l1 = ((Long)localObject).longValue();
      return String.valueOf(l1);
    }
    catch (Exception localException) {}
    return "-1";
  }
  
  public List h()
  {
    localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    try
    {
      Object localObject1 = s;
      boolean bool = false;
      Object localObject2 = null;
      Object localObject3 = null;
      localObject3 = new Object[0];
      localObject1 = ((Method)localObject1).invoke(null, (Object[])localObject3);
      localObject1 = (List)localObject1;
      if (localObject1 != null)
      {
        localObject1 = ((List)localObject1).iterator();
        for (;;)
        {
          bool = ((Iterator)localObject1).hasNext();
          if (!bool) {
            break;
          }
          localObject2 = ((Iterator)localObject1).next();
          localObject3 = t;
          long l1 = ((Field)localObject3).getLong(localObject2);
          localObject2 = String.valueOf(l1);
          localObject2 = b((String)localObject2);
          if (localObject2 != null) {
            localArrayList.add(localObject2);
          }
        }
      }
      return localArrayList;
    }
    catch (IllegalAccessException|InvocationTargetException localIllegalAccessException) {}
  }
  
  public final boolean j()
  {
    try
    {
      Object localObject = g;
      int i1 = 0;
      Object[] arrayOfObject = new Object[0];
      localObject = ((Method)localObject).invoke(null, arrayOfObject);
      localObject = (Integer)localObject;
      int i2 = ((Integer)localObject).intValue();
      i1 = 1;
      if (i2 > i1) {
        return i1;
      }
      return false;
    }
    catch (Exception localException) {}
    return false;
  }
  
  public final boolean k()
  {
    int i1 = j();
    if (i1 != 0)
    {
      i1 = 1;
      try
      {
        Object localObject = f;
        Object[] arrayOfObject = new Object[i1];
        Context localContext = a;
        arrayOfObject[0] = localContext;
        localObject = ((Method)localObject).invoke(null, arrayOfObject);
        localObject = (Integer)localObject;
        int i2 = ((Integer)localObject).intValue();
        if (i2 > i1) {
          return i1;
        }
        return false;
      }
      catch (Exception localException)
      {
        return i1;
      }
    }
    return false;
  }
  
  public final String m()
  {
    return "sim_imsi";
  }
  
  public final String n()
  {
    return "sim_imsi";
  }
  
  protected final String o()
  {
    return "sim_id";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.multisim.ac
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */