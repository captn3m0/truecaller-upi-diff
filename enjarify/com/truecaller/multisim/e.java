package com.truecaller.multisim;

import android.database.Cursor;
import android.database.CursorWrapper;
import android.text.TextUtils;
import java.util.HashMap;
import java.util.Map;

abstract class e
  extends CursorWrapper
  implements d
{
  private final Map a;
  private final int b;
  
  e(Cursor paramCursor, String paramString)
  {
    super(paramCursor);
    paramCursor = new java/util/HashMap;
    paramCursor.<init>();
    a = paramCursor;
    int i;
    if (paramString != null) {
      i = getColumnIndex(paramString);
    } else {
      i = -1;
    }
    b = i;
  }
  
  public abstract String a(String paramString);
  
  public final String e()
  {
    int i = b;
    if (i < 0) {
      return "-1";
    }
    String str1 = getString(i);
    boolean bool = TextUtils.isEmpty(str1);
    if (bool) {
      return "-1";
    }
    String str2 = (String)a.get(str1);
    if (str2 != null) {
      return str2;
    }
    str2 = a(str1);
    a.put(str1, str2);
    return str2;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.multisim.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */