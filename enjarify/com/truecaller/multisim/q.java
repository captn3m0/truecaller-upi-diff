package com.truecaller.multisim;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.PersistableBundle;
import android.telecom.PhoneAccount;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;
import android.telephony.CarrierConfigManager;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.List;

class q
  extends o
{
  static final j g = -..Lambda.q.uisoG3GDypRJYvY4JluW45lT-8g.INSTANCE;
  private final CarrierConfigManager h;
  private final Method i;
  
  q(Context paramContext, SubscriptionManager paramSubscriptionManager, TelephonyManager paramTelephonyManager, TelecomManager paramTelecomManager, CarrierConfigManager paramCarrierConfigManager)
  {
    super(paramContext, paramSubscriptionManager, paramTelephonyManager, paramTelecomManager);
    h = paramCarrierConfigManager;
    paramTelephonyManager = new Class[1];
    paramTelephonyManager[0] = PhoneAccount.class;
    paramContext = TelephonyManager.class.getMethod("getSubIdForPhoneAccount", paramTelephonyManager);
    i = paramContext;
  }
  
  private String i(String paramString)
  {
    Object localObject1 = b;
    Object localObject2 = { "android.permission.READ_PHONE_STATE" };
    boolean bool1 = ((com.truecaller.multisim.b.a)localObject1).a((String[])localObject2);
    boolean bool2;
    if (bool1) {
      try
      {
        localObject1 = e;
        localObject1 = ((TelecomManager)localObject1).getCallCapablePhoneAccounts();
        localObject1 = ((List)localObject1).iterator();
        do
        {
          bool2 = ((Iterator)localObject1).hasNext();
          if (!bool2) {
            break;
          }
          localObject2 = ((Iterator)localObject1).next();
          localObject2 = (PhoneAccountHandle)localObject2;
          localObject3 = ((PhoneAccountHandle)localObject2).getId();
          bool3 = paramString.equals(localObject3);
        } while (!bool3);
        paramString = e;
        paramString = paramString.getPhoneAccount((PhoneAccountHandle)localObject2);
        localObject1 = i;
        localObject2 = d;
        boolean bool3 = true;
        Object localObject3 = new Object[bool3];
        localObject3[0] = paramString;
        paramString = ((Method)localObject1).invoke(localObject2, (Object[])localObject3);
        paramString = (Integer)paramString;
        int j = paramString.intValue();
        return String.valueOf(j);
      }
      catch (IllegalAccessException|InvocationTargetException localIllegalAccessException)
      {
        return "-1";
      }
    }
    localObject1 = h().iterator();
    do
    {
      bool2 = ((Iterator)localObject1).hasNext();
      if (!bool2) {
        break;
      }
      localObject2 = nextb;
      bool2 = ((String)localObject2).equals(paramString);
    } while (!bool2);
    return paramString;
    return "-1";
  }
  
  public d a(Cursor paramCursor)
  {
    q.a locala = new com/truecaller/multisim/q$a;
    String str = d();
    locala.<init>(this, paramCursor, str);
    return locala;
  }
  
  public String a()
  {
    return "Marshmallow";
  }
  
  public void a(Intent paramIntent, String paramString)
  {
    paramString = h(paramString);
    if (paramString != null)
    {
      String str = "android.telecom.extra.PHONE_ACCOUNT_HANDLE";
      paramIntent.putExtra(str, paramString);
    }
  }
  
  final String b(int paramInt)
  {
    try
    {
      TelephonyManager localTelephonyManager = d;
      return localTelephonyManager.getDeviceId(paramInt);
    }
    catch (SecurityException localSecurityException)
    {
      com.truecaller.multisim.b.c.a();
    }
    return null;
  }
  
  public final a c(String paramString)
  {
    Object localObject = "-1";
    boolean bool = ((String)localObject).equals(paramString);
    if (bool)
    {
      paramString = h.getConfig();
    }
    else
    {
      localObject = h;
      int j = Integer.valueOf(paramString).intValue();
      paramString = ((CarrierConfigManager)localObject).getConfigForSubId(j);
    }
    if (paramString == null)
    {
      paramString = new android/os/PersistableBundle;
      paramString.<init>();
    }
    localObject = new com/truecaller/multisim/c;
    ((c)localObject).<init>(paramString);
    return (a)localObject;
  }
  
  final PhoneAccountHandle h(String paramString)
  {
    Object localObject1 = b;
    Object localObject2 = { "android.permission.READ_PHONE_STATE" };
    boolean bool1 = ((com.truecaller.multisim.b.a)localObject1).a((String[])localObject2);
    if (bool1) {
      try
      {
        localObject1 = e;
        localObject1 = ((TelecomManager)localObject1).getCallCapablePhoneAccounts();
        localObject1 = ((List)localObject1).iterator();
        boolean bool3;
        do
        {
          boolean bool2 = ((Iterator)localObject1).hasNext();
          if (!bool2) {
            break;
          }
          localObject2 = ((Iterator)localObject1).next();
          localObject2 = (PhoneAccountHandle)localObject2;
          Object localObject3 = e;
          localObject3 = ((TelecomManager)localObject3).getPhoneAccount((PhoneAccountHandle)localObject2);
          Method localMethod = i;
          TelephonyManager localTelephonyManager = d;
          int j = 1;
          Object[] arrayOfObject = new Object[j];
          arrayOfObject[0] = localObject3;
          localObject3 = localMethod.invoke(localTelephonyManager, arrayOfObject);
          localObject3 = (Integer)localObject3;
          int k = ((Integer)localObject3).intValue();
          localObject3 = String.valueOf(k);
          bool3 = paramString.equals(localObject3);
        } while (!bool3);
        return (PhoneAccountHandle)localObject2;
      }
      catch (IllegalAccessException|InvocationTargetException localIllegalAccessException) {}
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.multisim.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */