package com.truecaller.multisim;

import android.content.Context;
import android.provider.Telephony.Mms;
import android.provider.Telephony.Sms;
import android.telecom.TelecomManager;
import android.telephony.TelephonyManager;
import java.lang.reflect.Field;

final class m
  extends l
{
  static final j e = -..Lambda.m.n0O09giK_XytEZ-KeNKXp9V9p_0.INSTANCE;
  private final String g;
  private final String h;
  
  private m(Context paramContext, TelephonyManager paramTelephonyManager, TelecomManager paramTelecomManager)
  {
    super(paramContext, paramTelephonyManager, paramTelecomManager);
    paramContext = (String)Telephony.Sms.class.getField("PHONE_ID").get(null);
    g = paramContext;
    paramContext = (String)Telephony.Mms.class.getField("PHONE_ID").get(null);
    h = paramContext;
  }
  
  public final String a()
  {
    return "Lollipop2";
  }
  
  public final String m()
  {
    return g;
  }
  
  public final String n()
  {
    return h;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.multisim.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */