package com.truecaller.social;

import android.content.Intent;
import android.os.Bundle;

public abstract interface b
{
  public abstract SocialNetworkType a();
  
  public abstract void a(Bundle paramBundle);
  
  public abstract void a(c paramc);
  
  public abstract boolean a(int paramInt1, int paramInt2, Intent paramIntent);
  
  public abstract void b(Bundle paramBundle);
  
  public abstract void b(c paramc);
  
  public abstract String[] b();
  
  public abstract void c();
  
  public abstract void d();
  
  public abstract void m();
  
  public abstract void n();
}

/* Location:
 * Qualified Name:     com.truecaller.social.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */