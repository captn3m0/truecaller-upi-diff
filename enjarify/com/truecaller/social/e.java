package com.truecaller.social;

import android.content.Context;
import android.support.v4.app.Fragment;
import com.truecaller.common.h.a;
import com.truecaller.log.AssertionUtil;
import java.util.EnumMap;
import java.util.Iterator;

public class e
{
  private static e b;
  public final EnumMap a;
  
  private e(Context paramContext)
  {
    EnumMap localEnumMap = new java/util/EnumMap;
    localEnumMap.<init>(SocialNetworkType.class);
    a = localEnumMap;
    b(paramContext);
  }
  
  public static e a(Context paramContext)
  {
    ??? = b;
    if (??? == null) {
      synchronized (e.class)
      {
        e locale = b;
        if (locale == null)
        {
          locale = new com/truecaller/social/e;
          paramContext = paramContext.getApplicationContext();
          locale.<init>(paramContext);
          b = locale;
        }
      }
    }
    return b;
  }
  
  private void b(Context paramContext)
  {
    Object localObject1 = SocialNetworkProvider.class.getClassLoader();
    Iterator localIterator = a.a(paramContext, SocialNetworkProvider.class, (ClassLoader)localObject1).iterator();
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      localObject1 = (SocialNetworkProvider)localIterator.next();
      boolean bool2 = ((SocialNetworkProvider)localObject1).isSupported(paramContext);
      int i = 1;
      Object localObject2;
      Object localObject4;
      if (bool2)
      {
        localObject2 = a;
        Object localObject3 = ((SocialNetworkProvider)localObject1).type();
        localObject2 = (SocialNetworkProvider)((EnumMap)localObject2).put((Enum)localObject3, localObject1);
        if (localObject2 == null)
        {
          bool2 = true;
        }
        else
        {
          bool2 = false;
          localObject2 = null;
        }
        localObject4 = new String[i];
        localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>();
        localObject1 = ((SocialNetworkProvider)localObject1).type();
        ((StringBuilder)localObject3).append(localObject1);
        ((StringBuilder)localObject3).append(" was redefined");
        localObject1 = ((StringBuilder)localObject3).toString();
        localObject4[0] = localObject1;
        AssertionUtil.isTrue(bool2, (String[])localObject4);
      }
      else
      {
        localObject2 = new String[i];
        localObject4 = new java/lang/StringBuilder;
        ((StringBuilder)localObject4).<init>();
        localObject1 = localObject1.getClass().getName();
        ((StringBuilder)localObject4).append((String)localObject1);
        ((StringBuilder)localObject4).append(" is not supported");
        localObject1 = ((StringBuilder)localObject4).toString();
        localObject2[0] = localObject1;
      }
    }
  }
  
  public final b a(SocialNetworkType paramSocialNetworkType, Fragment paramFragment)
  {
    Object localObject = (SocialNetworkProvider)a.get(paramSocialNetworkType);
    if (localObject != null)
    {
      paramSocialNetworkType = paramFragment.getActivity();
      return ((SocialNetworkProvider)localObject).network(paramSocialNetworkType, paramFragment);
    }
    paramFragment = new com/truecaller/social/d$c;
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    paramSocialNetworkType = String.valueOf(paramSocialNetworkType);
    ((StringBuilder)localObject).append(paramSocialNetworkType);
    ((StringBuilder)localObject).append(" is not supported");
    paramSocialNetworkType = ((StringBuilder)localObject).toString();
    paramFragment.<init>(paramSocialNetworkType);
    throw paramFragment;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.social.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */