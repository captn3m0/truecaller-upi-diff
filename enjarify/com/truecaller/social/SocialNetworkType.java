package com.truecaller.social;

public enum SocialNetworkType
{
  private final int mBackground;
  private final int mColor;
  private final int mIcon;
  private final int mName;
  
  static
  {
    SocialNetworkType localSocialNetworkType1 = new com/truecaller/social/SocialNetworkType;
    int i = R.string.SocialNetworkFacebook;
    int j = R.color.social_network_facebook;
    int k = R.drawable.ic_facebook;
    int m = R.drawable.social_network_facebook;
    Object localObject = localSocialNetworkType1;
    localSocialNetworkType1.<init>("FACEBOOK", 0, i, j, k, m);
    FACEBOOK = localSocialNetworkType1;
    localObject = new com/truecaller/social/SocialNetworkType;
    int n = R.string.SocialNetworkGoogle;
    int i1 = R.color.social_network_google;
    int i2 = R.drawable.ic_google;
    int i3 = R.drawable.social_network_google;
    ((SocialNetworkType)localObject).<init>("GOOGLE", 1, n, i1, i2, i3);
    GOOGLE = (SocialNetworkType)localObject;
    localObject = new SocialNetworkType[2];
    SocialNetworkType localSocialNetworkType2 = FACEBOOK;
    localObject[0] = localSocialNetworkType2;
    localSocialNetworkType2 = GOOGLE;
    localObject[1] = localSocialNetworkType2;
    $VALUES = (SocialNetworkType[])localObject;
  }
  
  private SocialNetworkType(int paramInt2, int paramInt3, int paramInt4, int paramInt5)
  {
    mName = paramInt2;
    mColor = paramInt3;
    mIcon = paramInt4;
    mBackground = paramInt5;
  }
  
  public final int getBackground()
  {
    return mBackground;
  }
  
  public final int getColor()
  {
    return mColor;
  }
  
  public final int getIcon()
  {
    return mIcon;
  }
  
  public final int getName()
  {
    return mName;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.social.SocialNetworkType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */