package com.truecaller.social.facebook;

import android.app.Activity;
import android.support.v4.app.Fragment;
import com.truecaller.social.SocialNetworkProvider;
import com.truecaller.social.SocialNetworkType;
import com.truecaller.social.b;

public class FacebookSocialNetworkProvider
  extends SocialNetworkProvider
{
  public b network(Activity paramActivity, Fragment paramFragment)
  {
    a locala = new com/truecaller/social/facebook/a;
    locala.<init>(paramActivity, paramFragment);
    return locala;
  }
  
  public SocialNetworkType type()
  {
    return SocialNetworkType.FACEBOOK;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.social.facebook.FacebookSocialNetworkProvider
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */