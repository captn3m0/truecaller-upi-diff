package com.truecaller.social.facebook;

public final class R$raw
{
  public static final int joda_africa_abidjan = 2131820545;
  public static final int joda_africa_accra = 2131820546;
  public static final int joda_africa_addis_ababa = 2131820547;
  public static final int joda_africa_algiers = 2131820548;
  public static final int joda_africa_asmara = 2131820549;
  public static final int joda_africa_asmera = 2131820550;
  public static final int joda_africa_bamako = 2131820551;
  public static final int joda_africa_bangui = 2131820552;
  public static final int joda_africa_banjul = 2131820553;
  public static final int joda_africa_bissau = 2131820554;
  public static final int joda_africa_blantyre = 2131820555;
  public static final int joda_africa_brazzaville = 2131820556;
  public static final int joda_africa_bujumbura = 2131820557;
  public static final int joda_africa_cairo = 2131820558;
  public static final int joda_africa_casablanca = 2131820559;
  public static final int joda_africa_ceuta = 2131820560;
  public static final int joda_africa_conakry = 2131820561;
  public static final int joda_africa_dakar = 2131820562;
  public static final int joda_africa_dar_es_salaam = 2131820563;
  public static final int joda_africa_djibouti = 2131820564;
  public static final int joda_africa_douala = 2131820565;
  public static final int joda_africa_el_aaiun = 2131820566;
  public static final int joda_africa_freetown = 2131820567;
  public static final int joda_africa_gaborone = 2131820568;
  public static final int joda_africa_harare = 2131820569;
  public static final int joda_africa_johannesburg = 2131820570;
  public static final int joda_africa_juba = 2131820571;
  public static final int joda_africa_kampala = 2131820572;
  public static final int joda_africa_khartoum = 2131820573;
  public static final int joda_africa_kigali = 2131820574;
  public static final int joda_africa_kinshasa = 2131820575;
  public static final int joda_africa_lagos = 2131820576;
  public static final int joda_africa_libreville = 2131820577;
  public static final int joda_africa_lome = 2131820578;
  public static final int joda_africa_luanda = 2131820579;
  public static final int joda_africa_lubumbashi = 2131820580;
  public static final int joda_africa_lusaka = 2131820581;
  public static final int joda_africa_malabo = 2131820582;
  public static final int joda_africa_maputo = 2131820583;
  public static final int joda_africa_maseru = 2131820584;
  public static final int joda_africa_mbabane = 2131820585;
  public static final int joda_africa_mogadishu = 2131820586;
  public static final int joda_africa_monrovia = 2131820587;
  public static final int joda_africa_nairobi = 2131820588;
  public static final int joda_africa_ndjamena = 2131820589;
  public static final int joda_africa_niamey = 2131820590;
  public static final int joda_africa_nouakchott = 2131820591;
  public static final int joda_africa_ouagadougou = 2131820592;
  public static final int joda_africa_porto_novo = 2131820593;
  public static final int joda_africa_sao_tome = 2131820594;
  public static final int joda_africa_timbuktu = 2131820595;
  public static final int joda_africa_tripoli = 2131820596;
  public static final int joda_africa_tunis = 2131820597;
  public static final int joda_africa_windhoek = 2131820598;
  public static final int joda_america_adak = 2131820599;
  public static final int joda_america_anchorage = 2131820600;
  public static final int joda_america_anguilla = 2131820601;
  public static final int joda_america_antigua = 2131820602;
  public static final int joda_america_araguaina = 2131820603;
  public static final int joda_america_argentina_buenos_aires = 2131820604;
  public static final int joda_america_argentina_catamarca = 2131820605;
  public static final int joda_america_argentina_comodrivadavia = 2131820606;
  public static final int joda_america_argentina_cordoba = 2131820607;
  public static final int joda_america_argentina_jujuy = 2131820608;
  public static final int joda_america_argentina_la_rioja = 2131820609;
  public static final int joda_america_argentina_mendoza = 2131820610;
  public static final int joda_america_argentina_rio_gallegos = 2131820611;
  public static final int joda_america_argentina_salta = 2131820612;
  public static final int joda_america_argentina_san_juan = 2131820613;
  public static final int joda_america_argentina_san_luis = 2131820614;
  public static final int joda_america_argentina_tucuman = 2131820615;
  public static final int joda_america_argentina_ushuaia = 2131820616;
  public static final int joda_america_aruba = 2131820617;
  public static final int joda_america_asuncion = 2131820618;
  public static final int joda_america_atikokan = 2131820619;
  public static final int joda_america_bahia = 2131820620;
  public static final int joda_america_bahia_banderas = 2131820621;
  public static final int joda_america_barbados = 2131820622;
  public static final int joda_america_belem = 2131820623;
  public static final int joda_america_belize = 2131820624;
  public static final int joda_america_blanc_sablon = 2131820625;
  public static final int joda_america_boa_vista = 2131820626;
  public static final int joda_america_bogota = 2131820627;
  public static final int joda_america_boise = 2131820628;
  public static final int joda_america_cambridge_bay = 2131820629;
  public static final int joda_america_campo_grande = 2131820630;
  public static final int joda_america_cancun = 2131820631;
  public static final int joda_america_caracas = 2131820632;
  public static final int joda_america_cayenne = 2131820633;
  public static final int joda_america_cayman = 2131820634;
  public static final int joda_america_chicago = 2131820635;
  public static final int joda_america_chihuahua = 2131820636;
  public static final int joda_america_coral_harbour = 2131820637;
  public static final int joda_america_costa_rica = 2131820638;
  public static final int joda_america_creston = 2131820639;
  public static final int joda_america_cuiaba = 2131820640;
  public static final int joda_america_curacao = 2131820641;
  public static final int joda_america_danmarkshavn = 2131820642;
  public static final int joda_america_dawson = 2131820643;
  public static final int joda_america_dawson_creek = 2131820644;
  public static final int joda_america_denver = 2131820645;
  public static final int joda_america_detroit = 2131820646;
  public static final int joda_america_dominica = 2131820647;
  public static final int joda_america_edmonton = 2131820648;
  public static final int joda_america_eirunepe = 2131820649;
  public static final int joda_america_el_salvador = 2131820650;
  public static final int joda_america_ensenada = 2131820651;
  public static final int joda_america_fort_nelson = 2131820652;
  public static final int joda_america_fortaleza = 2131820653;
  public static final int joda_america_glace_bay = 2131820654;
  public static final int joda_america_godthab = 2131820655;
  public static final int joda_america_goose_bay = 2131820656;
  public static final int joda_america_grand_turk = 2131820657;
  public static final int joda_america_grenada = 2131820658;
  public static final int joda_america_guadeloupe = 2131820659;
  public static final int joda_america_guatemala = 2131820660;
  public static final int joda_america_guayaquil = 2131820661;
  public static final int joda_america_guyana = 2131820662;
  public static final int joda_america_halifax = 2131820663;
  public static final int joda_america_havana = 2131820664;
  public static final int joda_america_hermosillo = 2131820665;
  public static final int joda_america_indiana_indianapolis = 2131820666;
  public static final int joda_america_indiana_knox = 2131820667;
  public static final int joda_america_indiana_marengo = 2131820668;
  public static final int joda_america_indiana_petersburg = 2131820669;
  public static final int joda_america_indiana_tell_city = 2131820670;
  public static final int joda_america_indiana_vevay = 2131820671;
  public static final int joda_america_indiana_vincennes = 2131820672;
  public static final int joda_america_indiana_winamac = 2131820673;
  public static final int joda_america_inuvik = 2131820674;
  public static final int joda_america_iqaluit = 2131820675;
  public static final int joda_america_jamaica = 2131820676;
  public static final int joda_america_juneau = 2131820677;
  public static final int joda_america_kentucky_louisville = 2131820678;
  public static final int joda_america_kentucky_monticello = 2131820679;
  public static final int joda_america_kralendijk = 2131820680;
  public static final int joda_america_la_paz = 2131820681;
  public static final int joda_america_lima = 2131820682;
  public static final int joda_america_los_angeles = 2131820683;
  public static final int joda_america_lower_princes = 2131820684;
  public static final int joda_america_maceio = 2131820685;
  public static final int joda_america_managua = 2131820686;
  public static final int joda_america_manaus = 2131820687;
  public static final int joda_america_marigot = 2131820688;
  public static final int joda_america_martinique = 2131820689;
  public static final int joda_america_matamoros = 2131820690;
  public static final int joda_america_mazatlan = 2131820691;
  public static final int joda_america_menominee = 2131820692;
  public static final int joda_america_merida = 2131820693;
  public static final int joda_america_metlakatla = 2131820694;
  public static final int joda_america_mexico_city = 2131820695;
  public static final int joda_america_miquelon = 2131820696;
  public static final int joda_america_moncton = 2131820697;
  public static final int joda_america_monterrey = 2131820698;
  public static final int joda_america_montevideo = 2131820699;
  public static final int joda_america_montreal = 2131820700;
  public static final int joda_america_montserrat = 2131820701;
  public static final int joda_america_nassau = 2131820702;
  public static final int joda_america_new_york = 2131820703;
  public static final int joda_america_nipigon = 2131820704;
  public static final int joda_america_nome = 2131820705;
  public static final int joda_america_noronha = 2131820706;
  public static final int joda_america_north_dakota_beulah = 2131820707;
  public static final int joda_america_north_dakota_center = 2131820708;
  public static final int joda_america_north_dakota_new_salem = 2131820709;
  public static final int joda_america_ojinaga = 2131820710;
  public static final int joda_america_panama = 2131820711;
  public static final int joda_america_pangnirtung = 2131820712;
  public static final int joda_america_paramaribo = 2131820713;
  public static final int joda_america_phoenix = 2131820714;
  public static final int joda_america_port_au_prince = 2131820715;
  public static final int joda_america_port_of_spain = 2131820716;
  public static final int joda_america_porto_velho = 2131820717;
  public static final int joda_america_puerto_rico = 2131820718;
  public static final int joda_america_punta_arenas = 2131820719;
  public static final int joda_america_rainy_river = 2131820720;
  public static final int joda_america_rankin_inlet = 2131820721;
  public static final int joda_america_recife = 2131820722;
  public static final int joda_america_regina = 2131820723;
  public static final int joda_america_resolute = 2131820724;
  public static final int joda_america_rio_branco = 2131820725;
  public static final int joda_america_rosario = 2131820726;
  public static final int joda_america_santarem = 2131820727;
  public static final int joda_america_santiago = 2131820728;
  public static final int joda_america_santo_domingo = 2131820729;
  public static final int joda_america_sao_paulo = 2131820730;
  public static final int joda_america_scoresbysund = 2131820731;
  public static final int joda_america_sitka = 2131820732;
  public static final int joda_america_st_barthelemy = 2131820733;
  public static final int joda_america_st_johns = 2131820734;
  public static final int joda_america_st_kitts = 2131820735;
  public static final int joda_america_st_lucia = 2131820736;
  public static final int joda_america_st_thomas = 2131820737;
  public static final int joda_america_st_vincent = 2131820738;
  public static final int joda_america_swift_current = 2131820739;
  public static final int joda_america_tegucigalpa = 2131820740;
  public static final int joda_america_thule = 2131820741;
  public static final int joda_america_thunder_bay = 2131820742;
  public static final int joda_america_tijuana = 2131820743;
  public static final int joda_america_toronto = 2131820744;
  public static final int joda_america_tortola = 2131820745;
  public static final int joda_america_vancouver = 2131820746;
  public static final int joda_america_whitehorse = 2131820747;
  public static final int joda_america_winnipeg = 2131820748;
  public static final int joda_america_yakutat = 2131820749;
  public static final int joda_america_yellowknife = 2131820750;
  public static final int joda_antarctica_casey = 2131820751;
  public static final int joda_antarctica_davis = 2131820752;
  public static final int joda_antarctica_dumontdurville = 2131820753;
  public static final int joda_antarctica_macquarie = 2131820754;
  public static final int joda_antarctica_mawson = 2131820755;
  public static final int joda_antarctica_mcmurdo = 2131820756;
  public static final int joda_antarctica_palmer = 2131820757;
  public static final int joda_antarctica_rothera = 2131820758;
  public static final int joda_antarctica_south_pole = 2131820759;
  public static final int joda_antarctica_syowa = 2131820760;
  public static final int joda_antarctica_troll = 2131820761;
  public static final int joda_antarctica_vostok = 2131820762;
  public static final int joda_arctic_longyearbyen = 2131820763;
  public static final int joda_asia_aden = 2131820764;
  public static final int joda_asia_almaty = 2131820765;
  public static final int joda_asia_amman = 2131820766;
  public static final int joda_asia_anadyr = 2131820767;
  public static final int joda_asia_aqtau = 2131820768;
  public static final int joda_asia_aqtobe = 2131820769;
  public static final int joda_asia_ashgabat = 2131820770;
  public static final int joda_asia_atyrau = 2131820771;
  public static final int joda_asia_baghdad = 2131820772;
  public static final int joda_asia_bahrain = 2131820773;
  public static final int joda_asia_baku = 2131820774;
  public static final int joda_asia_bangkok = 2131820775;
  public static final int joda_asia_barnaul = 2131820776;
  public static final int joda_asia_beirut = 2131820777;
  public static final int joda_asia_bishkek = 2131820778;
  public static final int joda_asia_brunei = 2131820779;
  public static final int joda_asia_chita = 2131820780;
  public static final int joda_asia_choibalsan = 2131820781;
  public static final int joda_asia_chongqing = 2131820782;
  public static final int joda_asia_chungking = 2131820783;
  public static final int joda_asia_colombo = 2131820784;
  public static final int joda_asia_damascus = 2131820785;
  public static final int joda_asia_dhaka = 2131820786;
  public static final int joda_asia_dili = 2131820787;
  public static final int joda_asia_dubai = 2131820788;
  public static final int joda_asia_dushanbe = 2131820789;
  public static final int joda_asia_famagusta = 2131820790;
  public static final int joda_asia_gaza = 2131820791;
  public static final int joda_asia_hanoi = 2131820792;
  public static final int joda_asia_harbin = 2131820793;
  public static final int joda_asia_hebron = 2131820794;
  public static final int joda_asia_ho_chi_minh = 2131820795;
  public static final int joda_asia_hong_kong = 2131820796;
  public static final int joda_asia_hovd = 2131820797;
  public static final int joda_asia_irkutsk = 2131820798;
  public static final int joda_asia_istanbul = 2131820799;
  public static final int joda_asia_jakarta = 2131820800;
  public static final int joda_asia_jayapura = 2131820801;
  public static final int joda_asia_jerusalem = 2131820802;
  public static final int joda_asia_kabul = 2131820803;
  public static final int joda_asia_kamchatka = 2131820804;
  public static final int joda_asia_karachi = 2131820805;
  public static final int joda_asia_kashgar = 2131820806;
  public static final int joda_asia_kathmandu = 2131820807;
  public static final int joda_asia_khandyga = 2131820808;
  public static final int joda_asia_kolkata = 2131820809;
  public static final int joda_asia_krasnoyarsk = 2131820810;
  public static final int joda_asia_kuala_lumpur = 2131820811;
  public static final int joda_asia_kuching = 2131820812;
  public static final int joda_asia_kuwait = 2131820813;
  public static final int joda_asia_macau = 2131820814;
  public static final int joda_asia_magadan = 2131820815;
  public static final int joda_asia_makassar = 2131820816;
  public static final int joda_asia_manila = 2131820817;
  public static final int joda_asia_muscat = 2131820818;
  public static final int joda_asia_nicosia = 2131820819;
  public static final int joda_asia_novokuznetsk = 2131820820;
  public static final int joda_asia_novosibirsk = 2131820821;
  public static final int joda_asia_omsk = 2131820822;
  public static final int joda_asia_oral = 2131820823;
  public static final int joda_asia_phnom_penh = 2131820824;
  public static final int joda_asia_pontianak = 2131820825;
  public static final int joda_asia_pyongyang = 2131820826;
  public static final int joda_asia_qatar = 2131820827;
  public static final int joda_asia_qyzylorda = 2131820828;
  public static final int joda_asia_riyadh = 2131820829;
  public static final int joda_asia_sakhalin = 2131820830;
  public static final int joda_asia_samarkand = 2131820831;
  public static final int joda_asia_seoul = 2131820832;
  public static final int joda_asia_shanghai = 2131820833;
  public static final int joda_asia_singapore = 2131820834;
  public static final int joda_asia_srednekolymsk = 2131820835;
  public static final int joda_asia_taipei = 2131820836;
  public static final int joda_asia_tashkent = 2131820837;
  public static final int joda_asia_tbilisi = 2131820838;
  public static final int joda_asia_tehran = 2131820839;
  public static final int joda_asia_tel_aviv = 2131820840;
  public static final int joda_asia_thimphu = 2131820841;
  public static final int joda_asia_tokyo = 2131820842;
  public static final int joda_asia_tomsk = 2131820843;
  public static final int joda_asia_ulaanbaatar = 2131820844;
  public static final int joda_asia_urumqi = 2131820845;
  public static final int joda_asia_ust_nera = 2131820846;
  public static final int joda_asia_vientiane = 2131820847;
  public static final int joda_asia_vladivostok = 2131820848;
  public static final int joda_asia_yakutsk = 2131820849;
  public static final int joda_asia_yangon = 2131820850;
  public static final int joda_asia_yekaterinburg = 2131820851;
  public static final int joda_asia_yerevan = 2131820852;
  public static final int joda_atlantic_azores = 2131820853;
  public static final int joda_atlantic_bermuda = 2131820854;
  public static final int joda_atlantic_canary = 2131820855;
  public static final int joda_atlantic_cape_verde = 2131820856;
  public static final int joda_atlantic_faroe = 2131820857;
  public static final int joda_atlantic_jan_mayen = 2131820858;
  public static final int joda_atlantic_madeira = 2131820859;
  public static final int joda_atlantic_reykjavik = 2131820860;
  public static final int joda_atlantic_south_georgia = 2131820861;
  public static final int joda_atlantic_st_helena = 2131820862;
  public static final int joda_atlantic_stanley = 2131820863;
  public static final int joda_australia_adelaide = 2131820864;
  public static final int joda_australia_brisbane = 2131820865;
  public static final int joda_australia_broken_hill = 2131820866;
  public static final int joda_australia_currie = 2131820867;
  public static final int joda_australia_darwin = 2131820868;
  public static final int joda_australia_eucla = 2131820869;
  public static final int joda_australia_hobart = 2131820870;
  public static final int joda_australia_lindeman = 2131820871;
  public static final int joda_australia_lord_howe = 2131820872;
  public static final int joda_australia_melbourne = 2131820873;
  public static final int joda_australia_perth = 2131820874;
  public static final int joda_australia_sydney = 2131820875;
  public static final int joda_cet = 2131820876;
  public static final int joda_cst6cdt = 2131820877;
  public static final int joda_eet = 2131820878;
  public static final int joda_est = 2131820879;
  public static final int joda_est5edt = 2131820880;
  public static final int joda_etc_gmt = 2131820881;
  public static final int joda_etc_gmt_1 = 2131820882;
  public static final int joda_etc_gmt_10 = 2131820883;
  public static final int joda_etc_gmt_11 = 2131820884;
  public static final int joda_etc_gmt_12 = 2131820885;
  public static final int joda_etc_gmt_13 = 2131820886;
  public static final int joda_etc_gmt_14 = 2131820887;
  public static final int joda_etc_gmt_2 = 2131820888;
  public static final int joda_etc_gmt_3 = 2131820889;
  public static final int joda_etc_gmt_4 = 2131820890;
  public static final int joda_etc_gmt_5 = 2131820891;
  public static final int joda_etc_gmt_6 = 2131820892;
  public static final int joda_etc_gmt_7 = 2131820893;
  public static final int joda_etc_gmt_8 = 2131820894;
  public static final int joda_etc_gmt_9 = 2131820895;
  public static final int joda_etc_gmtplus1 = 2131820896;
  public static final int joda_etc_gmtplus10 = 2131820897;
  public static final int joda_etc_gmtplus11 = 2131820898;
  public static final int joda_etc_gmtplus12 = 2131820899;
  public static final int joda_etc_gmtplus2 = 2131820900;
  public static final int joda_etc_gmtplus3 = 2131820901;
  public static final int joda_etc_gmtplus4 = 2131820902;
  public static final int joda_etc_gmtplus5 = 2131820903;
  public static final int joda_etc_gmtplus6 = 2131820904;
  public static final int joda_etc_gmtplus7 = 2131820905;
  public static final int joda_etc_gmtplus8 = 2131820906;
  public static final int joda_etc_gmtplus9 = 2131820907;
  public static final int joda_etc_uct = 2131820908;
  public static final int joda_etc_utc = 2131820909;
  public static final int joda_europe_amsterdam = 2131820910;
  public static final int joda_europe_andorra = 2131820911;
  public static final int joda_europe_astrakhan = 2131820912;
  public static final int joda_europe_athens = 2131820913;
  public static final int joda_europe_belfast = 2131820914;
  public static final int joda_europe_belgrade = 2131820915;
  public static final int joda_europe_berlin = 2131820916;
  public static final int joda_europe_bratislava = 2131820917;
  public static final int joda_europe_brussels = 2131820918;
  public static final int joda_europe_bucharest = 2131820919;
  public static final int joda_europe_budapest = 2131820920;
  public static final int joda_europe_busingen = 2131820921;
  public static final int joda_europe_chisinau = 2131820922;
  public static final int joda_europe_copenhagen = 2131820923;
  public static final int joda_europe_dublin = 2131820924;
  public static final int joda_europe_gibraltar = 2131820925;
  public static final int joda_europe_guernsey = 2131820926;
  public static final int joda_europe_helsinki = 2131820927;
  public static final int joda_europe_isle_of_man = 2131820928;
  public static final int joda_europe_istanbul = 2131820929;
  public static final int joda_europe_jersey = 2131820930;
  public static final int joda_europe_kaliningrad = 2131820931;
  public static final int joda_europe_kiev = 2131820932;
  public static final int joda_europe_kirov = 2131820933;
  public static final int joda_europe_lisbon = 2131820934;
  public static final int joda_europe_ljubljana = 2131820935;
  public static final int joda_europe_london = 2131820936;
  public static final int joda_europe_luxembourg = 2131820937;
  public static final int joda_europe_madrid = 2131820938;
  public static final int joda_europe_malta = 2131820939;
  public static final int joda_europe_mariehamn = 2131820940;
  public static final int joda_europe_minsk = 2131820941;
  public static final int joda_europe_monaco = 2131820942;
  public static final int joda_europe_moscow = 2131820943;
  public static final int joda_europe_nicosia = 2131820944;
  public static final int joda_europe_oslo = 2131820945;
  public static final int joda_europe_paris = 2131820946;
  public static final int joda_europe_podgorica = 2131820947;
  public static final int joda_europe_prague = 2131820948;
  public static final int joda_europe_riga = 2131820949;
  public static final int joda_europe_rome = 2131820950;
  public static final int joda_europe_samara = 2131820951;
  public static final int joda_europe_san_marino = 2131820952;
  public static final int joda_europe_sarajevo = 2131820953;
  public static final int joda_europe_saratov = 2131820954;
  public static final int joda_europe_simferopol = 2131820955;
  public static final int joda_europe_skopje = 2131820956;
  public static final int joda_europe_sofia = 2131820957;
  public static final int joda_europe_stockholm = 2131820958;
  public static final int joda_europe_tallinn = 2131820959;
  public static final int joda_europe_tirane = 2131820960;
  public static final int joda_europe_tiraspol = 2131820961;
  public static final int joda_europe_ulyanovsk = 2131820962;
  public static final int joda_europe_uzhgorod = 2131820963;
  public static final int joda_europe_vaduz = 2131820964;
  public static final int joda_europe_vatican = 2131820965;
  public static final int joda_europe_vienna = 2131820966;
  public static final int joda_europe_vilnius = 2131820967;
  public static final int joda_europe_volgograd = 2131820968;
  public static final int joda_europe_warsaw = 2131820969;
  public static final int joda_europe_zagreb = 2131820970;
  public static final int joda_europe_zaporozhye = 2131820971;
  public static final int joda_europe_zurich = 2131820972;
  public static final int joda_hst = 2131820973;
  public static final int joda_indian_antananarivo = 2131820974;
  public static final int joda_indian_chagos = 2131820975;
  public static final int joda_indian_christmas = 2131820976;
  public static final int joda_indian_cocos = 2131820977;
  public static final int joda_indian_comoro = 2131820978;
  public static final int joda_indian_kerguelen = 2131820979;
  public static final int joda_indian_mahe = 2131820980;
  public static final int joda_indian_maldives = 2131820981;
  public static final int joda_indian_mauritius = 2131820982;
  public static final int joda_indian_mayotte = 2131820983;
  public static final int joda_indian_reunion = 2131820984;
  public static final int joda_keep = 2131820985;
  public static final int joda_met = 2131820986;
  public static final int joda_mst = 2131820987;
  public static final int joda_mst7mdt = 2131820988;
  public static final int joda_pacific_apia = 2131820989;
  public static final int joda_pacific_auckland = 2131820990;
  public static final int joda_pacific_bougainville = 2131820991;
  public static final int joda_pacific_chatham = 2131820992;
  public static final int joda_pacific_chuuk = 2131820993;
  public static final int joda_pacific_easter = 2131820994;
  public static final int joda_pacific_efate = 2131820995;
  public static final int joda_pacific_enderbury = 2131820996;
  public static final int joda_pacific_fakaofo = 2131820997;
  public static final int joda_pacific_fiji = 2131820998;
  public static final int joda_pacific_funafuti = 2131820999;
  public static final int joda_pacific_galapagos = 2131821000;
  public static final int joda_pacific_gambier = 2131821001;
  public static final int joda_pacific_guadalcanal = 2131821002;
  public static final int joda_pacific_guam = 2131821003;
  public static final int joda_pacific_honolulu = 2131821004;
  public static final int joda_pacific_johnston = 2131821005;
  public static final int joda_pacific_kiritimati = 2131821006;
  public static final int joda_pacific_kosrae = 2131821007;
  public static final int joda_pacific_kwajalein = 2131821008;
  public static final int joda_pacific_majuro = 2131821009;
  public static final int joda_pacific_marquesas = 2131821010;
  public static final int joda_pacific_midway = 2131821011;
  public static final int joda_pacific_nauru = 2131821012;
  public static final int joda_pacific_niue = 2131821013;
  public static final int joda_pacific_norfolk = 2131821014;
  public static final int joda_pacific_noumea = 2131821015;
  public static final int joda_pacific_pago_pago = 2131821016;
  public static final int joda_pacific_palau = 2131821017;
  public static final int joda_pacific_pitcairn = 2131821018;
  public static final int joda_pacific_pohnpei = 2131821019;
  public static final int joda_pacific_port_moresby = 2131821020;
  public static final int joda_pacific_rarotonga = 2131821021;
  public static final int joda_pacific_saipan = 2131821022;
  public static final int joda_pacific_tahiti = 2131821023;
  public static final int joda_pacific_tarawa = 2131821024;
  public static final int joda_pacific_tongatapu = 2131821025;
  public static final int joda_pacific_wake = 2131821026;
  public static final int joda_pacific_wallis = 2131821027;
  public static final int joda_pst8pdt = 2131821028;
  public static final int joda_wet = 2131821029;
  public static final int joda_zoneinfomap = 2131821030;
}

/* Location:
 * Qualified Name:     com.truecaller.social.facebook.R.raw
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */