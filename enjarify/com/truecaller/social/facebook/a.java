package com.truecaller.social.facebook;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphRequest.b;
import com.facebook.h;
import com.facebook.login.LoginClient.Request;
import com.facebook.login.f.a;
import com.facebook.q;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import com.truecaller.social.SocialNetworkType;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

final class a
  extends com.truecaller.social.a
{
  private static final List c = Collections.singletonList("email");
  private final Activity d;
  private final Fragment e;
  private final com.facebook.d f;
  
  a(Activity paramActivity, Fragment paramFragment)
  {
    super((SocialNetworkType)localObject);
    d = paramActivity;
    e = paramFragment;
    paramActivity = new com/facebook/internal/d;
    paramActivity.<init>();
    f = paramActivity;
    paramActivity = com.facebook.login.f.a();
    paramFragment = f;
    localObject = new com/truecaller/social/facebook/a$1;
    ((a.1)localObject).<init>(this);
    paramActivity.a(paramFragment, (com.facebook.f)localObject);
  }
  
  private static boolean o()
  {
    AccessToken localAccessToken = AccessToken.a();
    if (localAccessToken != null)
    {
      boolean bool = localAccessToken.d();
      if (!bool) {
        return true;
      }
    }
    return false;
  }
  
  public final boolean a(int paramInt1, int paramInt2, Intent paramIntent)
  {
    String[] arrayOfString = new String[1];
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("onActivityResult(");
    ((StringBuilder)localObject).append(paramInt1);
    ((StringBuilder)localObject).append(",");
    ((StringBuilder)localObject).append(paramInt2);
    ((StringBuilder)localObject).append(",");
    ((StringBuilder)localObject).append(paramIntent);
    ((StringBuilder)localObject).append(")");
    localObject = ((StringBuilder)localObject).toString();
    arrayOfString[0] = localObject;
    return f.a(paramInt1, paramInt2, paramIntent);
  }
  
  public final void h()
  {
    try
    {
      Object localObject1 = e;
      if (localObject1 == null)
      {
        localObject1 = com.facebook.login.f.a();
        localObject2 = d;
        localObject3 = c;
        com.facebook.login.f.a((Collection)localObject3);
        localObject3 = ((com.facebook.login.f)localObject1).b((Collection)localObject3);
        f.a locala = new com/facebook/login/f$a;
        locala.<init>((Activity)localObject2);
        ((com.facebook.login.f)localObject1).a(locala, (LoginClient.Request)localObject3);
        return;
      }
      localObject1 = com.facebook.login.f.a();
      Object localObject2 = e;
      Object localObject3 = c;
      ((com.facebook.login.f)localObject1).a((Fragment)localObject2, (Collection)localObject3);
      return;
    }
    catch (h localh)
    {
      a(5, localh);
    }
  }
  
  public final void i()
  {
    com.facebook.login.f.a().b();
    boolean bool = o() ^ true;
    String[] arrayOfString = new String[0];
    AssertionUtil.OnlyInDebug.isTrue(bool, arrayOfString);
  }
  
  public final void j()
  {
    boolean bool = o();
    Object localObject1 = new String[0];
    AssertionUtil.OnlyInDebug.isTrue(bool, (String[])localObject1);
    Object localObject2 = new android/os/Bundle;
    ((Bundle)localObject2).<init>();
    ((Bundle)localObject2).putString("fields", "id,first_name,last_name,email,location,hometown,gender,picture.width(500).height(500),link");
    localObject1 = new com/facebook/GraphRequest;
    AccessToken localAccessToken = AccessToken.a();
    q localq = q.a;
    ((GraphRequest)localObject1).<init>(localAccessToken, "me", (Bundle)localObject2, localq);
    localObject2 = new com/truecaller/social/facebook/a$2;
    ((a.2)localObject2).<init>(this);
    ((GraphRequest)localObject1).a((GraphRequest.b)localObject2);
    ((GraphRequest)localObject1).a();
  }
  
  public final void m()
  {
    boolean bool = o();
    int i = 9;
    if (bool)
    {
      a(i);
      return;
    }
    a(4, i);
    a(5);
  }
  
  public final void n()
  {
    boolean bool = o();
    int i = 5;
    if (bool)
    {
      a(12, i);
      a(7);
      return;
    }
    a(i);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.social.facebook.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */