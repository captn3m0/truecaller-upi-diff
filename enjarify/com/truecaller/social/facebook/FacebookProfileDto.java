package com.truecaller.social.facebook;

import android.text.TextUtils;
import com.truecaller.social.f;
import java.util.HashMap;
import java.util.Map;

class FacebookProfileDto
{
  public String email;
  public String firstName;
  public String gender;
  public String id;
  public String lastName;
  public FacebookProfileDto.a location;
  public FacebookProfileDto.b picture;
  
  private static void putIfNotNull(Map paramMap, String paramString1, String paramString2)
  {
    boolean bool = TextUtils.isEmpty(paramString2);
    if (!bool) {
      paramMap.put(paramString1, paramString2);
    }
  }
  
  f toSocialNetworkProfile()
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    String str1 = id;
    putIfNotNull(localHashMap, "profileFacebook", str1);
    str1 = email;
    putIfNotNull(localHashMap, "profileEmail", str1);
    str1 = firstName;
    putIfNotNull(localHashMap, "profileFirstName", str1);
    str1 = lastName;
    putIfNotNull(localHashMap, "profileLastName", str1);
    Object localObject = gender;
    boolean bool;
    if (localObject != null)
    {
      int i = -1;
      int j = ((String)localObject).hashCode();
      int k = -1278174388;
      String str2;
      if (j != k)
      {
        k = 3343885;
        if (j == k)
        {
          str2 = "male";
          bool = ((String)localObject).equals(str2);
          if (bool)
          {
            i = 0;
            str1 = null;
          }
        }
      }
      else
      {
        str2 = "female";
        bool = ((String)localObject).equals(str2);
        if (bool) {
          i = 1;
        }
      }
      switch (i)
      {
      default: 
        localObject = "profileGender";
        str1 = "N";
        localHashMap.put(localObject, str1);
        break;
      case 1: 
        localObject = "profileGender";
        str1 = "F";
        localHashMap.put(localObject, str1);
        break;
      case 0: 
        localObject = "profileGender";
        str1 = "M";
        localHashMap.put(localObject, str1);
      }
    }
    localObject = location;
    if (localObject != null)
    {
      localObject = a;
      bool = TextUtils.isEmpty((CharSequence)localObject);
      if (!bool)
      {
        localObject = "profileCity";
        str1 = location.a;
        localHashMap.put(localObject, str1);
      }
    }
    localObject = picture;
    if (localObject != null)
    {
      localObject = a;
      if (localObject != null)
      {
        localObject = picture.a;
        bool = b;
        if (!bool)
        {
          localObject = "profileAvatar";
          str1 = picture.a.a;
          putIfNotNull(localHashMap, (String)localObject, str1);
        }
      }
    }
    localObject = new com/truecaller/social/f;
    ((f)localObject).<init>(localHashMap);
    return (f)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.social.facebook.FacebookProfileDto
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */