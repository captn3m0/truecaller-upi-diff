package com.truecaller.social;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Looper;
import android.os.Message;
import android.util.SparseArray;
import com.truecaller.log.AssertionUtil;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;
import org.c.a.a.a.h;

public abstract class a
  implements Handler.Callback, b
{
  protected c a;
  protected c b;
  private final SparseArray c;
  private final SocialNetworkType d;
  private final Handler e;
  
  protected a(SocialNetworkType paramSocialNetworkType)
  {
    Object localObject = new android/util/SparseArray;
    ((SparseArray)localObject).<init>();
    c = ((SparseArray)localObject);
    d = paramSocialNetworkType;
    paramSocialNetworkType = new com/truecaller/social/a$b;
    localObject = Looper.getMainLooper();
    paramSocialNetworkType.<init>((Looper)localObject, this);
    e = paramSocialNetworkType;
  }
  
  private static String a(Collection paramCollection)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("[");
    paramCollection = paramCollection.iterator();
    int i = 0;
    for (;;)
    {
      boolean bool = paramCollection.hasNext();
      if (!bool) {
        break;
      }
      Object localObject = (Message)paramCollection.next();
      if (i > 0)
      {
        String str = ", ";
        localStringBuilder.append(str);
      }
      int j = what;
      localObject = b(j);
      localStringBuilder.append((String)localObject);
      i += 1;
    }
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
  
  private void a(Message paramMessage)
  {
    SparseArray localSparseArray = c;
    int i = localSparseArray.size();
    int j = 0;
    while (j < i)
    {
      Object localObject1 = (Set)c.valueAt(j);
      boolean bool = ((Set)localObject1).remove(paramMessage);
      if (bool)
      {
        bool = true;
        localObject1 = new String[bool];
        Object localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>("--> dropped ");
        String str = b(what);
        ((StringBuilder)localObject2).append(str);
        ((StringBuilder)localObject2).append(" waiting for ");
        int k = c.keyAt(j);
        str = b(k);
        ((StringBuilder)localObject2).append(str);
        localObject2 = ((StringBuilder)localObject2).toString();
        localObject1[0] = localObject2;
      }
      j += 1;
    }
  }
  
  private void a(boolean paramBoolean)
  {
    c localc = b;
    if (localc != null)
    {
      Boolean localBoolean = Boolean.valueOf(paramBoolean);
      localc.a(this, localBoolean);
    }
  }
  
  private static String b(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>("MSG_(");
      localStringBuilder.append(paramInt);
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    case 16: 
      return "MSG_DO_ERROR";
    case 15: 
      return "MSG_DO_RESOLVE";
    case 14: 
      return "MSG_ON_DESTROYED";
    case 13: 
      return "MSG_DO_DESTROY";
    case 12: 
      return "MSG_ON_DISCONNECTED";
    case 11: 
      return "MSG_DO_DISCONNECTING";
    case 10: 
      return "MSG_ON_GOT_PROFILE";
    case 9: 
      return "MSG_DO_GET_PROFILE";
    case 8: 
      return "MSG_ON_SIGNED_OUT";
    case 7: 
      return "MSG_DO_SIGN_OUT";
    case 6: 
      return "MSG_ON_SIGNED_IN";
    case 5: 
      return "MSG_DO_SIGN_IN";
    case 4: 
      return "MSG_ON_CONNECTED";
    case 3: 
      return "MSG_DO_CONNECTING";
    case 2: 
      return "MSG_ON_INITIALIZED";
    }
    return "MSG_DO_INITIALIZE";
  }
  
  public final SocialNetworkType a()
  {
    return d;
  }
  
  public final void a(int paramInt)
  {
    e.sendEmptyMessage(paramInt);
  }
  
  protected final void a(int paramInt1, int paramInt2)
  {
    Object localObject1 = (Set)c.get(paramInt1);
    if (localObject1 == null)
    {
      localObject1 = new java/util/TreeSet;
      Object localObject2 = a.a.a;
      ((TreeSet)localObject1).<init>((Comparator)localObject2);
      localObject2 = c;
      ((SparseArray)localObject2).append(paramInt1, localObject1);
    }
    Message localMessage = e.obtainMessage(paramInt2, 0, 0, null);
    ((Set)localObject1).add(localMessage);
  }
  
  public final void a(int paramInt1, int paramInt2, Object paramObject)
  {
    Handler localHandler = e;
    Message localMessage = localHandler.obtainMessage(paramInt1, paramInt2, 0, paramObject);
    localHandler.sendMessage(localMessage);
  }
  
  protected void a(int paramInt, Object paramObject)
  {
    d locald = new com/truecaller/social/d;
    paramObject = h.a(paramObject, null);
    locald.<init>((String)paramObject);
    a(paramInt, locald);
  }
  
  public final void a(int paramInt, Throwable paramThrowable)
  {
    a(16, paramInt, paramThrowable);
  }
  
  public void a(Bundle paramBundle) {}
  
  public final void a(c paramc)
  {
    b = paramc;
  }
  
  public boolean a(int paramInt1, int paramInt2, Intent paramIntent)
  {
    return false;
  }
  
  public void b(Bundle paramBundle) {}
  
  public final void b(c paramc)
  {
    a = paramc;
  }
  
  public final String[] b()
  {
    return new String[0];
  }
  
  public void c() {}
  
  public void d() {}
  
  protected void e() {}
  
  protected void f()
  {
    a(4);
  }
  
  protected void g() {}
  
  protected void h()
  {
    a(6);
  }
  
  public boolean handleMessage(Message paramMessage)
  {
    boolean bool1 = true;
    String[] arrayOfString = null;
    try
    {
      Object localObject1 = new String[bool1];
      Object localObject2 = new java/lang/StringBuilder;
      Object localObject3 = "handleMessage(";
      ((StringBuilder)localObject2).<init>((String)localObject3);
      int i = what;
      localObject3 = b(i);
      ((StringBuilder)localObject2).append((String)localObject3);
      localObject3 = ",";
      ((StringBuilder)localObject2).append((String)localObject3);
      i = arg1;
      ((StringBuilder)localObject2).append(i);
      localObject3 = ",";
      ((StringBuilder)localObject2).append((String)localObject3);
      i = arg2;
      ((StringBuilder)localObject2).append(i);
      localObject3 = ",";
      ((StringBuilder)localObject2).append((String)localObject3);
      localObject3 = obj;
      ((StringBuilder)localObject2).append(localObject3);
      localObject3 = ")";
      ((StringBuilder)localObject2).append((String)localObject3);
      localObject2 = ((StringBuilder)localObject2).toString();
      localObject1[0] = localObject2;
      a(paramMessage);
      int j = what;
      int k = 11;
      String str;
      int i1;
      label424:
      int m;
      switch (j)
      {
      default: 
        localObject1 = new String[bool1];
        break;
      case 16: 
        localObject1 = new String[bool1];
        localObject3 = new java/lang/StringBuilder;
        str = "--> doError(";
        ((StringBuilder)localObject3).<init>(str);
        i1 = arg1;
        str = b(i1);
        ((StringBuilder)localObject3).append(str);
        str = "..)";
        ((StringBuilder)localObject3).append(str);
        localObject3 = ((StringBuilder)localObject3).toString();
        localObject1[0] = localObject3;
        j = arg1;
        localObject3 = obj;
        localObject3 = (Throwable)localObject3;
        switch (j)
        {
        default: 
          localObject1 = b;
          break;
        case 9: 
        case 10: 
          localObject1 = a;
          if (localObject1 == null) {
            break label424;
          }
          localObject1 = a;
          ((c)localObject1).a(this, (Throwable)localObject3);
          break;
        }
        if (localObject1 != null)
        {
          localObject1 = b;
          ((c)localObject1).a(this, (Throwable)localObject3);
        }
        a(k);
        break;
      case 15: 
        localObject1 = new String[bool1];
        localObject2 = new java/lang/StringBuilder;
        localObject3 = "--> doResolve(";
        ((StringBuilder)localObject2).<init>((String)localObject3);
        i = arg1;
        localObject3 = b(i);
        ((StringBuilder)localObject2).append((String)localObject3);
        localObject3 = "..)";
        ((StringBuilder)localObject2).append((String)localObject3);
        localObject2 = ((StringBuilder)localObject2).toString();
        localObject1[0] = localObject2;
        j = arg1;
        localObject2 = obj;
        a(j, localObject2);
        break;
      case 14: 
        localObject1 = obj;
        break;
      case 13: 
        localObject1 = obj;
        j = 14;
        a(j);
        break;
      case 12: 
        localObject1 = obj;
        l();
        break;
      case 11: 
        localObject1 = obj;
        k();
        break;
      case 10: 
        localObject1 = obj;
        localObject2 = a;
        if (localObject2 == null) {
          break label916;
        }
        m = localObject1 instanceof f;
        if (m != 0)
        {
          localObject2 = a;
          localObject1 = (f)localObject1;
          ((c)localObject2).a(this, localObject1);
        }
        else
        {
          m = localObject1 instanceof Throwable;
          if (m != 0)
          {
            localObject2 = a;
            localObject1 = (Throwable)localObject1;
            ((c)localObject2).a(this, (Throwable)localObject1);
          }
          else
          {
            localObject2 = a;
            localObject3 = new com/truecaller/social/d$b;
            str = "No profile";
            localObject1 = h.a(localObject1, str);
            ((d.b)localObject3).<init>((String)localObject1);
            ((c)localObject2).a(this, (Throwable)localObject3);
          }
        }
        break;
      case 9: 
        localObject1 = obj;
        j();
        break;
      case 8: 
        localObject1 = obj;
        a(false);
        a(m);
        break;
      case 7: 
        localObject1 = obj;
        i();
        break;
      case 6: 
        localObject1 = obj;
        a(bool1);
        j = 3;
        a(j);
        break;
      case 5: 
        localObject1 = obj;
        h();
        break;
      case 4: 
        localObject1 = obj;
        g();
        break;
      case 3: 
        localObject1 = obj;
        f();
        break;
      case 2: 
        localObject1 = obj;
        e();
        break;
      case 1: 
        localObject1 = obj;
        j = 2;
        a(j);
        break;
      }
      localObject2 = new java/lang/StringBuilder;
      localObject3 = "MSG_(";
      ((StringBuilder)localObject2).<init>((String)localObject3);
      i = what;
      ((StringBuilder)localObject2).append(i);
      localObject3 = ")";
      ((StringBuilder)localObject2).append((String)localObject3);
      localObject2 = ((StringBuilder)localObject2).toString();
      localObject1[0] = localObject2;
      AssertionUtil.isTrue(false, (String[])localObject1);
      label916:
      localObject1 = c;
      int n = what;
      localObject1 = ((SparseArray)localObject1).get(n);
      localObject1 = (Set)localObject1;
      if (localObject1 != null)
      {
        boolean bool3 = ((Set)localObject1).isEmpty();
        if (!bool3)
        {
          localObject2 = new String[bool1];
          localObject3 = new java/lang/StringBuilder;
          ((StringBuilder)localObject3).<init>();
          i1 = what;
          str = b(i1);
          ((StringBuilder)localObject3).append(str);
          str = " --> ";
          ((StringBuilder)localObject3).append(str);
          str = a((Collection)localObject1);
          ((StringBuilder)localObject3).append(str);
          localObject3 = ((StringBuilder)localObject3).toString();
          localObject2[0] = localObject3;
          localObject2 = ((Set)localObject1).iterator();
          for (;;)
          {
            boolean bool2 = ((Iterator)localObject2).hasNext();
            if (!bool2) {
              break;
            }
            localObject3 = ((Iterator)localObject2).next();
            localObject3 = (Message)localObject3;
            ((Message)localObject3).sendToTarget();
          }
          ((Set)localObject1).clear();
        }
      }
    }
    catch (RuntimeException localRuntimeException)
    {
      arrayOfString = new String[0];
      AssertionUtil.shouldNeverHappen(localRuntimeException, arrayOfString);
      int i2 = what;
      a(i2, localRuntimeException);
    }
    return bool1;
  }
  
  protected void i()
  {
    a(8);
  }
  
  protected void j()
  {
    a(9, null);
  }
  
  protected void k()
  {
    a(12);
  }
  
  protected void l() {}
}

/* Location:
 * Qualified Name:     com.truecaller.social.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */