package com.truecaller.social;

import android.os.Bundle;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.c.a.a.a.h;

public final class f
{
  public final Map a;
  public final Bundle b;
  
  public f(Map paramMap)
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>(paramMap);
    paramMap = Collections.unmodifiableMap(localHashMap);
    a = paramMap;
    b = null;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (paramObject == this) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof f;
    if (bool2)
    {
      Map localMap = a;
      paramObject = a;
      boolean bool3 = h.b(localMap, paramObject);
      if (bool3) {
        return bool1;
      }
    }
    return false;
  }
  
  public final int hashCode()
  {
    return a.hashCode();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.social.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */