package com.truecaller.social;

import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Looper;
import android.os.Message;
import java.lang.ref.WeakReference;

final class a$b
  extends Handler
{
  private final WeakReference a;
  
  a$b(Looper paramLooper, Handler.Callback paramCallback)
  {
    super(paramLooper);
    paramLooper = new java/lang/ref/WeakReference;
    paramLooper.<init>(paramCallback);
    a = paramLooper;
  }
  
  public final void handleMessage(Message paramMessage)
  {
    Handler.Callback localCallback = (Handler.Callback)a.get();
    if (localCallback != null) {
      localCallback.handleMessage(paramMessage);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.social.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */