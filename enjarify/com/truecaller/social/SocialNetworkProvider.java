package com.truecaller.social;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;

public abstract class SocialNetworkProvider
{
  protected boolean isSupported(Context paramContext)
  {
    return true;
  }
  
  protected abstract b network(Activity paramActivity, Fragment paramFragment);
  
  protected abstract SocialNetworkType type();
}

/* Location:
 * Qualified Name:     com.truecaller.social.SocialNetworkProvider
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */