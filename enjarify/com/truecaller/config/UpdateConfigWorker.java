package com.truecaller.config;

import android.content.Context;
import androidx.work.ListenableWorker.a;
import androidx.work.WorkerParameters;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.analytics.b;
import com.truecaller.androidactors.w;
import com.truecaller.bp;
import com.truecaller.common.account.r;
import com.truecaller.common.background.TrackedWorker;

public final class UpdateConfigWorker
  extends TrackedWorker
{
  public static final UpdateConfigWorker.a e;
  public b b;
  public a c;
  public r d;
  
  static
  {
    UpdateConfigWorker.a locala = new com/truecaller/config/UpdateConfigWorker$a;
    locala.<init>((byte)0);
    e = locala;
  }
  
  public UpdateConfigWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    paramContext = TrueApp.y();
    k.a(paramContext, "TrueApp.getApp()");
    paramContext.a().a(this);
  }
  
  public final b b()
  {
    b localb = b;
    if (localb == null)
    {
      String str = "analytics";
      k.a(str);
    }
    return localb;
  }
  
  public final boolean c()
  {
    r localr = d;
    if (localr == null)
    {
      String str = "accountManager";
      k.a(str);
    }
    return localr.c();
  }
  
  public final ListenableWorker.a d()
  {
    Object localObject1 = c;
    if (localObject1 == null)
    {
      localObject2 = "configManager";
      k.a((String)localObject2);
    }
    localObject1 = (Boolean)((a)localObject1).a().d();
    Object localObject2 = Boolean.TRUE;
    boolean bool = k.a(localObject1, localObject2);
    if (bool)
    {
      localObject1 = ListenableWorker.a.a();
      k.a(localObject1, "Result.success()");
      return (ListenableWorker.a)localObject1;
    }
    localObject1 = ListenableWorker.a.b();
    k.a(localObject1, "Result.retry()");
    return (ListenableWorker.a)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.config.UpdateConfigWorker
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */