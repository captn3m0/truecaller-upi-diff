package com.truecaller.config;

import dagger.a.d;
import javax.inject.Provider;

public final class g
  implements d
{
  private final e a;
  private final Provider b;
  private final Provider c;
  
  private g(e parame, Provider paramProvider1, Provider paramProvider2)
  {
    a = parame;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static g a(e parame, Provider paramProvider1, Provider paramProvider2)
  {
    g localg = new com/truecaller/config/g;
    localg.<init>(parame, paramProvider1, paramProvider2);
    return localg;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.config.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */