package com.truecaller.config;

import android.content.Context;
import androidx.work.ListenableWorker.a;
import androidx.work.WorkerParameters;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.analytics.b;
import com.truecaller.androidactors.w;
import com.truecaller.bp;
import com.truecaller.common.background.TrackedWorker;

public final class UpdateInstallationWorker
  extends TrackedWorker
{
  public static final UpdateInstallationWorker.a d;
  public b b;
  public a c;
  
  static
  {
    UpdateInstallationWorker.a locala = new com/truecaller/config/UpdateInstallationWorker$a;
    locala.<init>((byte)0);
    d = locala;
  }
  
  public UpdateInstallationWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    paramContext = TrueApp.y();
    k.a(paramContext, "TrueApp.getApp()");
    paramContext.a().a(this);
  }
  
  public final b b()
  {
    b localb = b;
    if (localb == null)
    {
      String str = "analytics";
      k.a(str);
    }
    return localb;
  }
  
  public final boolean c()
  {
    return TrueApp.y().p();
  }
  
  public final ListenableWorker.a d()
  {
    Object localObject1 = c;
    if (localObject1 == null)
    {
      localObject2 = "configManager";
      k.a((String)localObject2);
    }
    localObject1 = (Boolean)((a)localObject1).b().d();
    Object localObject2 = Boolean.TRUE;
    boolean bool = k.a(localObject1, localObject2);
    if (bool)
    {
      localObject1 = ListenableWorker.a.a();
      k.a(localObject1, "Result.success()");
      return (ListenableWorker.a)localObject1;
    }
    localObject1 = ListenableWorker.a.b();
    k.a(localObject1, "Result.retry()");
    return (ListenableWorker.a)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.config.UpdateInstallationWorker
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */