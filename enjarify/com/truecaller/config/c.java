package com.truecaller.config;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import c.a.ag;
import c.g.b.k;
import c.u;
import c.x;
import com.truecaller.analytics.ae;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.w;
import com.truecaller.common.network.account.InstallationDetailsDto;
import com.truecaller.common.network.b.a.b;
import com.truecaller.common.network.b.a.c;
import com.truecaller.filters.p;
import com.truecaller.log.AssertionUtil;
import com.truecaller.messaging.h;
import com.truecaller.old.data.access.i;
import com.truecaller.referral.am;
import com.truecaller.tracking.events.ao;
import com.truecaller.tracking.events.ao.a;
import com.truecaller.truepay.Truepay;
import com.truecaller.update.ForcedUpdate;
import com.truecaller.update.ForcedUpdate.UpdateType;
import e.r;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public final class c
  implements a
{
  private final int a;
  private final Context b;
  private final com.truecaller.common.g.a c;
  private final com.truecaller.i.a d;
  private final com.truecaller.i.e e;
  private final am f;
  private final com.truecaller.analytics.storage.a g;
  private final p h;
  private final com.truecaller.featuretoggles.e i;
  private final com.truecaller.util.b j;
  private final h k;
  private final com.truecaller.common.network.account.b l;
  private final f m;
  
  public c(int paramInt, Context paramContext, com.truecaller.common.g.a parama, com.truecaller.i.a parama1, com.truecaller.i.e parame, am paramam, com.truecaller.analytics.storage.a parama2, p paramp, com.truecaller.featuretoggles.e parame1, com.truecaller.util.b paramb, h paramh, com.truecaller.common.network.account.b paramb1, f paramf)
  {
    a = paramInt;
    b = paramContext;
    c = parama;
    d = parama1;
    e = parame;
    f = paramam;
    g = parama2;
    h = paramp;
    i = parame1;
    j = paramb;
    k = paramh;
    l = paramb1;
    m = paramf;
  }
  
  private static int a(String paramString, int paramInt)
  {
    if (paramString != null)
    {
      paramString = c.n.m.b(paramString);
      if (paramString != null) {
        return paramString.intValue();
      }
    }
    return paramInt;
  }
  
  private static long a(String paramString, long paramLong)
  {
    if (paramString != null)
    {
      paramString = c.n.m.d(paramString);
      if (paramString != null) {
        return paramString.longValue();
      }
    }
    return paramLong;
  }
  
  private static Long a(String paramString)
  {
    String str = null;
    Object localObject = paramString;
    try
    {
      localObject = (CharSequence)paramString;
      boolean bool1 = TextUtils.isEmpty((CharSequence)localObject);
      long l1;
      if (bool1)
      {
        paramString = null;
      }
      else
      {
        if (paramString == null) {
          k.a();
        }
        l1 = Long.parseLong(paramString);
        paramString = Long.valueOf(l1);
      }
      if (paramString != null)
      {
        l1 = 0L;
        try
        {
          long l2 = paramString.longValue();
          boolean bool2 = l1 < l2;
          if (!bool2) {
            str = paramString;
          }
        }
        catch (NumberFormatException localNumberFormatException1)
        {
          str = paramString;
        }
      }
    }
    catch (NumberFormatException localNumberFormatException2)
    {
      for (;;) {}
    }
    return str;
  }
  
  private final void a(a.b paramb)
  {
    Object localObject1 = g;
    a("featureSmsSearch", (String)localObject1, "0");
    localObject1 = f;
    a("featureOutgoingSearch", (String)localObject1, "0");
    localObject1 = h;
    a("featureStatsSearch", (String)localObject1, "1");
    Object localObject2 = a;
    boolean bool1 = b((String)localObject2);
    boolean bool2 = true;
    int i2 = 0;
    Object localObject3 = null;
    if (bool1)
    {
      localObject2 = "featureEmailSource";
      bool1 = com.truecaller.common.b.e.a((String)localObject2, false);
      if (!bool1)
      {
        localObject2 = "featureEmailSource";
        com.truecaller.common.b.e.b((String)localObject2, bool2);
      }
    }
    localObject2 = c;
    bool1 = b((String)localObject2);
    if (bool1)
    {
      localObject2 = new com/truecaller/old/data/access/i;
      Context localContext = b;
      ((i)localObject2).<init>(localContext);
      ((i)localObject2).b();
    }
    long l1 = 0L;
    try
    {
      localObject2 = l;
      localObject2 = (CharSequence)localObject2;
      bool1 = TextUtils.isEmpty((CharSequence)localObject2);
      if (bool1)
      {
        l2 = l1;
      }
      else
      {
        localObject2 = l;
        if (localObject2 == null) {
          k.a();
        }
        localObject4 = "features.featureTagUpload!!";
        k.a(localObject2, (String)localObject4);
        l2 = Long.parseLong((String)localObject2);
      }
    }
    catch (NumberFormatException localNumberFormatException)
    {
      l2 = l1;
    }
    com.truecaller.common.b.e.b("tagsKeywordsFeatureCurrentVersion", l2);
    localObject2 = "featureAutoTagging";
    boolean bool4 = l2 < l1;
    if (bool4)
    {
      bool5 = true;
    }
    else
    {
      bool5 = false;
      localObject4 = null;
    }
    com.truecaller.common.b.e.b((String)localObject2, bool5);
    localObject2 = c;
    boolean bool6 = b(m);
    ((com.truecaller.common.g.a)localObject2).b("featureAvailability", bool6);
    bool1 = b(n);
    com.truecaller.common.b.e.b("featureLoggingEnabled", bool1);
    localObject2 = c;
    bool6 = b(q);
    ((com.truecaller.common.g.a)localObject2).b("featureCacheAdAfterCall", bool6);
    localObject2 = c;
    bool6 = b(ac);
    ((com.truecaller.common.g.a)localObject2).b("featureAdCtpRotation", bool6);
    localObject2 = d;
    long l3 = a(av, l1);
    ((com.truecaller.i.a)localObject2).b("adsFeatureHouseAdsTimeout", l3);
    localObject2 = d;
    bool6 = b(ax);
    ((com.truecaller.i.a)localObject2).b("adsFeatureUnifiedAdsBlock", bool6);
    localObject2 = d;
    bool6 = b(aw);
    ((com.truecaller.i.a)localObject2).b("adsFeatureUnifiedAdsDetails", bool6);
    localObject2 = d;
    l3 = a(ay, l1);
    ((com.truecaller.i.a)localObject2).b("adFeatureRetentionTime", l3);
    localObject2 = c;
    bool6 = b(S);
    ((com.truecaller.common.g.a)localObject2).b("featureOTPNotificationEnabled", bool6);
    localObject2 = c;
    String str1 = T;
    ((com.truecaller.common.g.a)localObject2).a("otpParserRegex", str1);
    localObject2 = c;
    bool6 = b(O);
    ((com.truecaller.common.g.a)localObject2).b("featureUgcDisabled", bool6);
    localObject2 = c;
    bool6 = b(M);
    ((com.truecaller.common.g.a)localObject2).b("featureUgcContactsWithoutIdentity", bool6);
    localObject2 = c;
    bool6 = b(t);
    ((com.truecaller.common.g.a)localObject2).b("featureFlash", bool6);
    localObject2 = c;
    bool6 = b(ab);
    ((com.truecaller.common.g.a)localObject2).b("featureCleverTap", bool6);
    localObject2 = c;
    bool6 = b(L);
    ((com.truecaller.common.g.a)localObject2).b("featureOfflineDirectory", bool6);
    localObject2 = c;
    bool6 = b(az);
    ((com.truecaller.common.g.a)localObject2).b("featureSmartNotifications", bool6);
    localObject2 = c;
    bool6 = b(aB);
    ((com.truecaller.common.g.a)localObject2).b("featureShareImageInFlash", bool6);
    localObject2 = c;
    str1 = ad;
    bool6 = b(str1);
    bool2 ^= bool6;
    ((com.truecaller.common.g.a)localObject2).b("featureRegion1", bool2);
    localObject2 = c;
    long l2 = System.currentTimeMillis();
    ((com.truecaller.common.g.a)localObject2).b("key_region_1_timestamp", l2);
    bool2 = b(r);
    com.truecaller.common.b.e.b("featureDisableOutgoingOutside", bool2);
    bool2 = b(s);
    com.truecaller.common.b.e.b("featureHideDialpad", bool2);
    bool2 = b(N);
    com.truecaller.common.b.e.b("featureNumberScanner", bool2);
    int n = a(G, 0);
    com.truecaller.common.b.e.b("featurePromoSpamOffCount", n);
    localObject2 = c;
    boolean bool5 = b(L);
    ((com.truecaller.common.g.a)localObject2).b("featureOfflineDirectory", bool5);
    n = a(H, 0);
    com.truecaller.common.b.e.b("featurePromoIncomingMsgCount", n);
    localObject2 = p;
    com.truecaller.common.b.e.b("featureOperatorCustomization", (String)localObject2);
    localObject2 = c;
    Object localObject4 = ah;
    l3 = com.truecaller.presence.t.b();
    l2 = a((String)localObject4, l3);
    ((com.truecaller.common.g.a)localObject2).b("presence_interval", l2);
    localObject2 = c;
    localObject4 = ai;
    l3 = com.truecaller.presence.t.a();
    l2 = a((String)localObject4, l3);
    ((com.truecaller.common.g.a)localObject2).b("presence_initial_delay", l2);
    localObject2 = c;
    localObject4 = aj;
    l3 = com.truecaller.presence.t.d();
    l2 = a((String)localObject4, l3);
    ((com.truecaller.common.g.a)localObject2).b("presence_stop_time", l2);
    localObject2 = c;
    localObject4 = ak;
    l3 = com.truecaller.presence.t.e();
    l2 = a((String)localObject4, l3);
    ((com.truecaller.common.g.a)localObject2).b("presence_recheck_time", l2);
    localObject2 = e;
    localObject4 = a(P);
    ((com.truecaller.i.e)localObject2).a("feature_global_unimportant_promo_period_days", (Long)localObject4);
    localObject2 = e;
    localObject4 = a(Q);
    ((com.truecaller.i.e)localObject2).a("feature_unimportant_promo_period_days", (Long)localObject4);
    localObject2 = e;
    localObject4 = a(R);
    ((com.truecaller.i.e)localObject2).a("feature_unimportant_promo_dismissed_delay_days", (Long)localObject4);
    localObject2 = g;
    localObject4 = aF;
    int i4 = 100;
    int i3 = a((String)localObject4, i4);
    ((com.truecaller.analytics.storage.a)localObject2).b("uploadEventsMaxBatchSize", i3);
    localObject2 = g;
    i3 = a(aG, i4);
    ((com.truecaller.analytics.storage.a)localObject2).b("uploadEventsMinBatchSize", i3);
    localObject2 = g;
    localObject4 = aH;
    l3 = 10000L;
    l2 = a((String)localObject4, l3);
    ((com.truecaller.analytics.storage.a)localObject2).b("uploadEventsRetryJitter", l2);
    localObject2 = c;
    localObject1 = "featureBusinessSuggestionMaxCount";
    localObject4 = a(aq);
    if (localObject4 != null) {
      l2 = ((Long)localObject4).longValue();
    } else {
      l2 = l1;
    }
    ((com.truecaller.common.g.a)localObject2).b((String)localObject1, l2);
    localObject2 = k;
    l1 = a(aA, l1);
    ((h)localObject2).d(l1);
    localObject2 = k;
    n = a(aC, 0);
    ((h)localObject2).i(n);
    localObject2 = k;
    localObject1 = aD;
    long l4 = 104857600L;
    long l5 = a((String)localObject1, l4);
    ((h)localObject2).e(l5);
    localObject2 = k;
    boolean bool3 = b(aE);
    ((h)localObject2).g(bool3);
    localObject2 = k;
    localObject1 = aU;
    ((h)localObject2).e((String)localObject1);
    localObject2 = k;
    int i1 = a(aK, 30);
    ((h)localObject2).k(i1);
    localObject2 = k;
    i1 = a(bu, 7);
    ((h)localObject2).m(i1);
    localObject2 = k;
    i1 = a(bG, 200);
    ((h)localObject2).n(i1);
    localObject2 = k;
    i1 = a(bH, 20);
    ((h)localObject2).o(i1);
    localObject2 = k;
    i1 = a(bK, 59);
    ((h)localObject2).p(i1);
    localObject2 = e;
    localObject1 = "feature_im_promo_after_call_period_days";
    localObject3 = aJ;
    int i5 = 5;
    i2 = a((String)localObject3, i5);
    ((com.truecaller.i.e)localObject2).b((String)localObject1, i2);
    localObject2 = a(bc);
    long l6;
    String str2;
    if (localObject2 != null)
    {
      localObject2 = (Number)localObject2;
      l6 = ((Number)localObject2).longValue();
      localObject3 = c;
      str2 = "searchHitTtl";
      ((com.truecaller.common.g.a)localObject3).b(str2, l6);
    }
    localObject2 = a(bd);
    if (localObject2 != null)
    {
      localObject2 = (Number)localObject2;
      l6 = ((Number)localObject2).longValue();
      localObject3 = c;
      str2 = "searchMissTtl";
      ((com.truecaller.common.g.a)localObject3).b(str2, l6);
    }
    localObject2 = e;
    i2 = a(br, i5);
    ((com.truecaller.i.e)localObject2).b("feature_voip_promo_after_call_period_days", i2);
    localObject2 = e;
    localObject3 = bJ;
    ((com.truecaller.i.e)localObject2).a("httpAnalyitcsHosts", (String)localObject3);
    localObject2 = h;
    localObject1 = bM;
    i2 = 0;
    localObject3 = null;
    if (localObject1 != null)
    {
      localObject1 = c.n.m.b((String)localObject1);
    }
    else
    {
      i1 = 0;
      localObject1 = null;
    }
    ((p)localObject2).a((Integer)localObject1);
    localObject2 = h;
    paramb = bN;
    if (paramb != null) {
      localObject3 = c.n.m.b(paramb);
    }
    ((p)localObject2).b((Integer)localObject3);
  }
  
  private final void a(a.c paramc)
  {
    ForcedUpdate.a(paramc);
    Object localObject1 = j;
    boolean bool = ((com.truecaller.util.b)localObject1).a();
    Object localObject2;
    if (bool)
    {
      localObject1 = ForcedUpdate.a();
      localObject2 = ForcedUpdate.UpdateType.OPTIONAL;
      if (localObject1 != localObject2)
      {
        localObject1 = b;
        localObject2 = null;
        ForcedUpdate.a((Context)localObject1, false);
      }
    }
    if (paramc != null)
    {
      paramc = a;
      if (paramc != null)
      {
        localObject1 = ao.b();
        localObject2 = (CharSequence)"UpgradePathReceived";
        localObject1 = ((ao.a)localObject1).a((CharSequence)localObject2);
        paramc = ag.a(c.t.a("UpgradePath", paramc));
        paramc = ((ao.a)localObject1).a(paramc).a();
        localObject1 = (ae)m.a();
        paramc = (org.apache.a.d.d)paramc;
        ((ae)localObject1).a(paramc);
        return;
      }
    }
  }
  
  private static void a(String paramString1, String paramString2, String paramString3)
  {
    boolean bool = k.a(paramString3, paramString2);
    if (bool)
    {
      paramString2 = TimeUnit.DAYS;
      long l1 = 1L;
      long l2 = paramString2.toMillis(l1);
      bool = com.truecaller.common.b.e.c(paramString1, l2);
      if (bool) {
        com.truecaller.common.b.e.d(paramString1);
      }
    }
  }
  
  private final void b(a.b paramb)
  {
    am localam = f;
    boolean bool1 = b(C);
    localam.a("featureSearchBarIcon", bool1);
    localam = f;
    bool1 = b(u);
    localam.a("featureAftercall", bool1);
    localam = f;
    bool1 = b(v);
    localam.a("featureAftercallSaveContact", bool1);
    localam = f;
    bool1 = b(B);
    localam.a("featureContactDetail", bool1);
    localam = f;
    bool1 = b(x);
    localam.a("featureContacts", bool1);
    localam = f;
    bool1 = b(z);
    localam.a("featureInboxOverflow", bool1);
    localam = f;
    bool1 = b(y);
    localam.a("featureReferralDeeplink", bool1);
    localam = f;
    bool1 = b(D);
    localam.a("featureReferralNavigationDrawer", bool1);
    localam = f;
    bool1 = b(w);
    localam.a("featureUserBusyPrompt", bool1);
    localam = f;
    bool1 = b(F);
    localam.a("featureGoPro", bool1);
    localam = f;
    String str1 = K;
    bool1 = b(str1);
    localam.a("featureReferralAfterCallPromo", bool1);
    localam = f;
    String str2 = "featureReferralShareApps";
    paramb = E;
    if (paramb == null) {
      paramb = "App Chooser";
    }
    localam.a(str2, paramb);
    paramb = f;
    boolean bool2 = true;
    paramb.a("featurePushNotification", bool2);
    f.a("featureLaunchReferralFromDeeplink", bool2);
    f.a("featureSearchScreenPromo", bool2);
  }
  
  private static boolean b(String paramString)
  {
    String str = "1";
    boolean bool1 = k.a(str, paramString);
    if (!bool1) {
      if (paramString != null)
      {
        boolean bool2 = Boolean.parseBoolean(paramString);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  private final void c(a.b paramb)
  {
    paramb = af;
    Object localObject1 = null;
    if (paramb != null)
    {
      paramb = (CharSequence)paramb;
      localObject2 = new String[] { "," };
      str = null;
      int n = 6;
      paramb = c.n.m.c(paramb, (String[])localObject2, false, n);
    }
    else
    {
      paramb = null;
    }
    Object localObject2 = c;
    String str = "callRecordingLicense";
    if (paramb != null) {
      localObject1 = (String)c.a.m.e(paramb);
    }
    ((com.truecaller.common.g.a)localObject2).a(str, (String)localObject1);
    localObject1 = c;
    localObject2 = "callRecordingSerial";
    if (paramb != null)
    {
      paramb = (String)c.a.m.g(paramb);
      if (paramb != null)
      {
        l1 = Long.parseLong(paramb);
        break label116;
      }
    }
    long l1 = 0L;
    label116:
    ((com.truecaller.common.g.a)localObject1).b((String)localObject2, l1);
  }
  
  private final boolean c()
  {
    try
    {
      localObject1 = l;
      localObject1 = ((com.truecaller.common.network.account.b)localObject1).a();
      Object localObject2 = com.truecaller.common.network.account.a.a;
      localObject1 = com.truecaller.common.network.account.a.a((InstallationDetailsDto)localObject1);
      localObject1 = ((e.b)localObject1).c();
      localObject2 = "AccountRestAdapter.updat…nstallationDto).execute()";
      k.a(localObject1, (String)localObject2);
      boolean bool = ((r)localObject1).d();
      if (bool)
      {
        localObject1 = c;
        localObject2 = "lastUpdateInstallationVersion";
        int n = a;
        ((com.truecaller.common.g.a)localObject1).b((String)localObject2, n);
        return true;
      }
    }
    catch (IOException localIOException)
    {
      Object localObject1 = (Throwable)localIOException;
      com.truecaller.log.d.a((Throwable)localObject1);
    }
    return false;
  }
  
  private final void d(a.b paramb)
  {
    Object localObject = c;
    String str1 = "featureWhoViewdMeNewViewIntervalInDays";
    String str2 = am;
    long l1 = 5;
    long l2;
    if (str2 != null) {
      l2 = Long.parseLong(str2);
    } else {
      l2 = l1;
    }
    ((com.truecaller.common.g.a)localObject).b(str1, l2);
    localObject = c;
    str1 = "featureWhoViewdMeShowNotificationAfterXLookups";
    str2 = ao;
    if (str2 != null) {
      l2 = Long.parseLong(str2);
    } else {
      l2 = l1;
    }
    ((com.truecaller.common.g.a)localObject).b(str1, l2);
    localObject = c;
    str1 = "featureWhoViewdMeShowNotificationAfterXDays";
    str2 = an;
    if (str2 != null) {
      l1 = Long.parseLong(str2);
    }
    ((com.truecaller.common.g.a)localObject).b(str1, l1);
    localObject = c;
    str2 = ap;
    ((com.truecaller.common.g.a)localObject).a("whoViewedMeIncognitoBucket", str2);
    localObject = c;
    boolean bool1 = b(ar);
    ((com.truecaller.common.g.a)localObject).b("whoViewedMePBContactEnabled", bool1);
    localObject = c;
    bool1 = b(as);
    ((com.truecaller.common.g.a)localObject).b("whoViewedMeACSEnabled", bool1);
    localObject = i;
    boolean bool2 = b(al);
    ((com.truecaller.featuretoggles.e)localObject).a("featureWhoViewedMe", bool2);
  }
  
  private final boolean d()
  {
    com.truecaller.common.network.b.a locala = e();
    if (locala != null)
    {
      Object localObject = a;
      if (localObject != null)
      {
        String str = "features";
        k.a(localObject, str);
        a((a.b)localObject);
        e((a.b)localObject);
        b((a.b)localObject);
        c((a.b)localObject);
        d((a.b)localObject);
        if (localObject != null) {}
      }
      else
      {
        AssertionUtil.reportWeirdnessButNeverCrash("features object not present");
        localObject = x.a;
      }
      localObject = b;
      a((a.c)localObject);
    }
    return locala != null;
  }
  
  private static com.truecaller.common.network.b.a e()
  {
    try
    {
      localObject1 = com.truecaller.common.network.b.b.a;
      localObject1 = com.truecaller.common.network.b.b.a();
      localObject1 = ((e.b)localObject1).c();
      Object localObject2 = "ConfigRestAdapter.getConfig().execute()";
      k.a(localObject1, (String)localObject2);
      boolean bool = ((r)localObject1).d();
      if (bool)
      {
        localObject2 = ((r)localObject1).e();
        if (localObject2 != null)
        {
          localObject1 = ((r)localObject1).e();
          return (com.truecaller.common.network.b.a)localObject1;
        }
      }
    }
    catch (IOException localIOException)
    {
      Object localObject1 = (Throwable)localIOException;
      com.truecaller.log.d.a((Throwable)localObject1);
    }
    return null;
  }
  
  private final void e(a.b paramb)
  {
    Object localObject = i;
    boolean bool1 = b(U);
    ((com.truecaller.featuretoggles.e)localObject).a("featureIm", bool1);
    localObject = i;
    bool1 = b(I);
    ((com.truecaller.featuretoggles.e)localObject).a("featureBackup", bool1);
    localObject = i;
    bool1 = b(o);
    ((com.truecaller.featuretoggles.e)localObject).a("featureTcPayOnboarding", bool1);
    localObject = i;
    bool1 = b(J);
    ((com.truecaller.featuretoggles.e)localObject).a("featureSwish", bool1);
    localObject = i;
    bool1 = b(ae);
    ((com.truecaller.featuretoggles.e)localObject).a("featureCallRecording", bool1);
    localObject = i;
    bool1 = b(W);
    ((com.truecaller.featuretoggles.e)localObject).a("featureEnableUtilities", bool1);
    localObject = i;
    bool1 = b(at);
    ((com.truecaller.featuretoggles.e)localObject).a("featureSmsCategorizer", bool1);
    localObject = i;
    bool1 = b(au);
    ((com.truecaller.featuretoggles.e)localObject).a("featureBlockByCountry", bool1);
    localObject = i;
    bool1 = b(X);
    ((com.truecaller.featuretoggles.e)localObject).a("featureMultiplePsp", bool1);
    localObject = i;
    bool1 = b(Y);
    ((com.truecaller.featuretoggles.e)localObject).a("featureUseBankSmsData", bool1);
    localObject = i;
    bool1 = b(Z);
    ((com.truecaller.featuretoggles.e)localObject).a("featureWhatsAppCalls", bool1);
    localObject = i;
    bool1 = b(aa);
    ((com.truecaller.featuretoggles.e)localObject).a("featureTcCredit", bool1);
    localObject = i;
    bool1 = b(aI);
    ((com.truecaller.featuretoggles.e)localObject).a("featureEnableGoldCallerIdForContacts", bool1);
    localObject = i;
    bool1 = b(aL);
    ((com.truecaller.featuretoggles.e)localObject).a("featureBusinessProfiles", bool1);
    localObject = i;
    bool1 = b(aM);
    ((com.truecaller.featuretoggles.e)localObject).a("featureCreateBusinessProfiles", bool1);
    localObject = i;
    bool1 = b(aN);
    ((com.truecaller.featuretoggles.e)localObject).a("featureNormalizeShortCodes", bool1);
    localObject = i;
    bool1 = b(aO);
    ((com.truecaller.featuretoggles.e)localObject).a("featureShowUserJoinedImNotification", bool1);
    localObject = i;
    bool1 = b(aP);
    ((com.truecaller.featuretoggles.e)localObject).a("featureSmartNotificationPayAction", bool1);
    localObject = i;
    bool1 = b(aQ);
    ((com.truecaller.featuretoggles.e)localObject).a("featureImEmojiPoke", bool1);
    localObject = i;
    bool1 = b(aR);
    ((com.truecaller.featuretoggles.e)localObject).a("featureSdkScanner", bool1);
    localObject = i;
    bool1 = b(aS);
    ((com.truecaller.featuretoggles.e)localObject).a("featureTcPaySmsBindingDeliveryCheck", bool1);
    localObject = i;
    bool1 = b(aT);
    ((com.truecaller.featuretoggles.e)localObject).a("featureReactions", bool1);
    localObject = i;
    bool1 = b(aV);
    ((com.truecaller.featuretoggles.e)localObject).a("featureContactsListV2", bool1);
    localObject = i;
    bool1 = b(aW);
    ((com.truecaller.featuretoggles.e)localObject).a("featurePayHomeCarousel", bool1);
    localObject = i;
    bool1 = b(aX);
    ((com.truecaller.featuretoggles.e)localObject).a("featurePayRewards", bool1);
    localObject = i;
    bool1 = b(ba);
    ((com.truecaller.featuretoggles.e)localObject).a("featurePayInstantReward", bool1);
    localObject = i;
    bool1 = b(bb);
    ((com.truecaller.featuretoggles.e)localObject).a("featurePayAppUpdatePopUp", bool1);
    localObject = i;
    bool1 = b(be);
    ((com.truecaller.featuretoggles.e)localObject).a("featureDisableAftercallIfLandscape", bool1);
    localObject = i;
    bool1 = b(bf);
    ((com.truecaller.featuretoggles.e)localObject).a("featureBlockHiddenNumbersAsPremium", bool1);
    localObject = i;
    bool1 = b(bg);
    ((com.truecaller.featuretoggles.e)localObject).a("featureBlockTopSpammersAsPremium", bool1);
    localObject = i;
    bool1 = b(bh);
    ((com.truecaller.featuretoggles.e)localObject).a("featureBlockNonPhonebook", bool1);
    localObject = i;
    bool1 = b(bi);
    ((com.truecaller.featuretoggles.e)localObject).a("featureBlockNonPhonebookAsPremium", bool1);
    localObject = i;
    bool1 = b(bj);
    ((com.truecaller.featuretoggles.e)localObject).a("featureBlockForeignNumbers", bool1);
    localObject = i;
    bool1 = b(bk);
    ((com.truecaller.featuretoggles.e)localObject).a("featureBlockForeignNumbersAsPremium", bool1);
    localObject = i;
    bool1 = b(bl);
    ((com.truecaller.featuretoggles.e)localObject).a("featureBlockNeighbourSpoofing", bool1);
    localObject = i;
    bool1 = b(bm);
    ((com.truecaller.featuretoggles.e)localObject).a("featureBlockNeighbourSpoofingAsPremium", bool1);
    localObject = i;
    bool1 = b(bn);
    ((com.truecaller.featuretoggles.e)localObject).a("featureBlockRegisteredTelemarketersAsPremium", bool1);
    localObject = i;
    bool1 = b(bo);
    ((com.truecaller.featuretoggles.e)localObject).a("featureIMReply", bool1);
    localObject = i;
    bool1 = b(bI);
    ((com.truecaller.featuretoggles.e)localObject).a("featureVoiceClip", bool1);
    localObject = i;
    bool1 = b(bp);
    ((com.truecaller.featuretoggles.e)localObject).a("featureConvertBusinessProfileToPrivate", bool1);
    localObject = i;
    bool1 = b(bq);
    ((com.truecaller.featuretoggles.e)localObject).a("featureVoIP", bool1);
    localObject = i;
    bool1 = b(bs);
    ((com.truecaller.featuretoggles.e)localObject).a("featureTruecallerX", bool1);
    localObject = i;
    bool1 = b(bt);
    ((com.truecaller.featuretoggles.e)localObject).a("featureLargeDetailsViewAd", bool1);
    localObject = i;
    bool1 = b(bv);
    ((com.truecaller.featuretoggles.e)localObject).a("featureVisiblePushCallerId", bool1);
    localObject = i;
    bool1 = b(bw);
    ((com.truecaller.featuretoggles.e)localObject).a("featureContactFieldsPremiumForUgc", bool1);
    localObject = i;
    bool1 = b(bx);
    ((com.truecaller.featuretoggles.e)localObject).a("featureContactFieldsPremiumForProfile", bool1);
    localObject = i;
    bool1 = b(by);
    ((com.truecaller.featuretoggles.e)localObject).a("featureContactEmailAsPremium", bool1);
    localObject = i;
    bool1 = b(bz);
    ((com.truecaller.featuretoggles.e)localObject).a("featureContactAddressAsPremium", bool1);
    localObject = i;
    bool1 = b(bA);
    ((com.truecaller.featuretoggles.e)localObject).a("featureContactJobAsPremium", bool1);
    localObject = i;
    bool1 = b(bB);
    ((com.truecaller.featuretoggles.e)localObject).a("featureContactWebsiteAsPremium", bool1);
    localObject = i;
    bool1 = b(bC);
    ((com.truecaller.featuretoggles.e)localObject).a("featureContactSocialAsPremium", bool1);
    localObject = i;
    bool1 = b(bD);
    ((com.truecaller.featuretoggles.e)localObject).a("featureContactAboutAsPremium", bool1);
    localObject = i;
    bool1 = b(bL);
    ((com.truecaller.featuretoggles.e)localObject).a("featureFiveBottomTabsWithBlockingPremium", bool1);
    localObject = i;
    bool1 = b(bE);
    ((com.truecaller.featuretoggles.e)localObject).a("featureInsightsSMSParsing", bool1);
    localObject = i;
    bool1 = b(bO);
    ((com.truecaller.featuretoggles.e)localObject).a("featureSdkScannerIgnoreFilter", bool1);
    localObject = i;
    bool1 = b(bP);
    ((com.truecaller.featuretoggles.e)localObject).a("featurePayShortcutIcon", bool1);
    localObject = i;
    bool1 = b(bQ);
    ((com.truecaller.featuretoggles.e)localObject).a("featurePremiumTab", bool1);
    localObject = i;
    bool1 = b(bR);
    ((com.truecaller.featuretoggles.e)localObject).a("featurePayHomeRevamp", bool1);
    localObject = i;
    bool1 = b(bS);
    ((com.truecaller.featuretoggles.e)localObject).a("featureInCallUI", bool1);
    localObject = i;
    bool1 = b(bT);
    ((com.truecaller.featuretoggles.e)localObject).a("featureCrossDcSearch", bool1);
    localObject = i;
    bool1 = b(bU);
    ((com.truecaller.featuretoggles.e)localObject).a("featurePayRegistrationV2", bool1);
    localObject = i;
    bool1 = b(bF);
    ((com.truecaller.featuretoggles.e)localObject).a("featureInsightsSMSPersistence", bool1);
    localObject = i;
    bool1 = b(bV);
    ((com.truecaller.featuretoggles.e)localObject).a("featureEngagementRewards", bool1);
    localObject = i;
    bool1 = b(bW);
    ((com.truecaller.featuretoggles.e)localObject).a("featurePayAppIxigo", bool1);
    localObject = i;
    bool1 = b(aY);
    ((com.truecaller.featuretoggles.e)localObject).a("featurePayGooglePlayRecharge", bool1);
    localObject = i;
    bool1 = b(bX);
    ((com.truecaller.featuretoggles.e)localObject).a("featureAppsInstalledHeartbeat", bool1);
    localObject = i;
    bool1 = b(aZ);
    ((com.truecaller.featuretoggles.e)localObject).a("featurePayBbpsReminders", bool1);
    paramb = V;
    boolean bool2 = b(paramb);
    localObject = i.n();
    boolean bool3 = ((com.truecaller.featuretoggles.b)localObject).a();
    com.truecaller.featuretoggles.e locale = i;
    String str = "featureTcPay";
    locale.a(str, bool2);
    if ((bool2 != bool3) && (bool2))
    {
      paramb = b.getApplicationContext();
      if (paramb != null)
      {
        paramb = (Application)paramb;
        Truepay.initialize(paramb);
      }
      else
      {
        paramb = new c/u;
        paramb.<init>("null cannot be cast to non-null type android.app.Application");
        throw paramb;
      }
    }
  }
  
  public final w a()
  {
    Object localObject = c;
    String str = "lastUpdateInstallationVersion";
    int n = ((com.truecaller.common.g.a)localObject).a(str, 0);
    int i1 = a;
    if (n != i1)
    {
      boolean bool = c();
      if (!bool)
      {
        localObject = w.b(Boolean.FALSE);
        k.a(localObject, "Promise.wrap(false)");
        return (w)localObject;
      }
    }
    localObject = w.b(Boolean.valueOf(d()));
    k.a(localObject, "Promise.wrap(updateConfigInternal())");
    return (w)localObject;
  }
  
  public final w b()
  {
    boolean bool = c();
    if (bool)
    {
      d();
      localw = w.b(Boolean.TRUE);
      k.a(localw, "Promise.wrap(true)");
      return localw;
    }
    w localw = w.b(Boolean.FALSE);
    k.a(localw, "Promise.wrap(false)");
    return localw;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.config.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */