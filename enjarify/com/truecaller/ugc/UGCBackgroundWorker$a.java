package com.truecaller.ugc;

import androidx.work.a;
import androidx.work.j;
import c.g.b.k;
import c.g.b.w;
import c.l.b;
import com.truecaller.common.background.g;

public final class UGCBackgroundWorker$a
  implements com.truecaller.common.background.h
{
  public final g a()
  {
    g localg = new com/truecaller/common/background/g;
    Object localObject = w.a(UGCBackgroundWorker.class);
    org.a.a.h localh = org.a.a.h.b(12);
    localg.<init>((b)localObject, localh);
    localObject = org.a.a.h.b(6);
    k.a(localObject, "Duration.standardHours(6)");
    localg = localg.a((org.a.a.h)localObject);
    localObject = a.a;
    localh = org.a.a.h.b(2);
    k.a(localh, "Duration.standardHours(2)");
    localg = localg.a((a)localObject, localh);
    localObject = j.b;
    return localg.a((j)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ugc.UGCBackgroundWorker.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */