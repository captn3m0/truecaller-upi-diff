package com.truecaller.ugc;

import android.content.Context;
import androidx.work.ListenableWorker.a;
import androidx.work.WorkerParameters;
import c.g.b.k;
import c.u;
import com.truecaller.common.account.r;
import com.truecaller.common.background.TrackedWorker;

public final class UGCBackgroundWorker
  extends TrackedWorker
{
  public static final UGCBackgroundWorker.a f;
  public com.truecaller.analytics.b b;
  public r c;
  public b d;
  public com.truecaller.featuretoggles.e e;
  
  static
  {
    UGCBackgroundWorker.a locala = new com/truecaller/ugc/UGCBackgroundWorker$a;
    locala.<init>((byte)0);
    f = locala;
  }
  
  public UGCBackgroundWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    k.b(paramContext, "receiver$0");
    paramWorkerParameters = a.a();
    Object localObject = paramContext.getApplicationContext();
    if (localObject != null)
    {
      localObject = ((com.truecaller.common.b.a)localObject).u();
      paramWorkerParameters = paramWorkerParameters.a((com.truecaller.common.a)localObject);
      paramContext = paramContext.getApplicationContext();
      if (paramContext != null)
      {
        paramContext = ((com.truecaller.common.b.a)paramContext).v();
        paramContext = paramWorkerParameters.a(paramContext).a();
        k.a(paramContext, "DaggerUGCComponent.build…mponent)\n        .build()");
        paramContext.a(this);
        return;
      }
      paramContext = new c/u;
      paramContext.<init>("null cannot be cast to non-null type com.truecaller.common.app.ApplicationBase");
      throw paramContext;
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type com.truecaller.common.app.ApplicationBase");
    throw paramContext;
  }
  
  public final com.truecaller.analytics.b b()
  {
    com.truecaller.analytics.b localb = b;
    if (localb == null)
    {
      String str = "analytics";
      k.a(str);
    }
    return localb;
  }
  
  public final boolean c()
  {
    Object localObject = c;
    String str;
    if (localObject == null)
    {
      str = "accountManager";
      k.a(str);
    }
    boolean bool = ((r)localObject).c();
    if (bool)
    {
      localObject = e;
      if (localObject == null)
      {
        str = "featuresRegistry";
        k.a(str);
      }
      localObject = ((com.truecaller.featuretoggles.e)localObject).T();
      bool = ((com.truecaller.featuretoggles.b)localObject).a();
      if (bool) {
        return true;
      }
    }
    return false;
  }
  
  public final ListenableWorker.a d()
  {
    Object localObject = d;
    if (localObject == null)
    {
      localObject = "ugcBackgroundManager";
      k.a((String)localObject);
    }
    localObject = ListenableWorker.a.a();
    k.a(localObject, "Result.success()");
    return (ListenableWorker.a)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ugc.UGCBackgroundWorker
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */