package com.truecaller.fcm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import c.g.b.k;
import c.u;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.truecaller.TrueApp;
import com.truecaller.b.c;
import com.truecaller.bp;
import com.truecaller.clevertap.j;
import com.truecaller.messaging.transport.im.ap;
import com.truecaller.network.notification.c.a.a;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;

public final class FcmMessageListenerService
  extends FirebaseMessagingService
{
  public static final FcmMessageListenerService.a i;
  public com.truecaller.flashsdk.b.a b;
  public j c;
  public ap d;
  public c e;
  public com.truecaller.featuretoggles.e f;
  public com.truecaller.push.e g;
  public com.truecaller.push.b h;
  
  static
  {
    FcmMessageListenerService.a locala = new com/truecaller/fcm/FcmMessageListenerService$a;
    locala.<init>((byte)0);
    i = locala;
  }
  
  private final void a(Map paramMap, long paramLong)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    paramMap = paramMap.entrySet().iterator();
    Object localObject;
    for (;;)
    {
      boolean bool = paramMap.hasNext();
      if (!bool) {
        break;
      }
      localObject = (Map.Entry)paramMap.next();
      String str = (String)((Map.Entry)localObject).getKey();
      localObject = (String)((Map.Entry)localObject).getValue();
      localBundle.putString(str, (String)localObject);
    }
    paramMap = h;
    if (paramMap == null)
    {
      localObject = "pushHandler";
      k.a((String)localObject);
    }
    paramMap.a(localBundle, paramLong);
  }
  
  public final void a(RemoteMessage paramRemoteMessage)
  {
    Object localObject1 = paramRemoteMessage.a();
    if (localObject1 == null) {
      return;
    }
    localObject1 = paramRemoteMessage.a();
    Object localObject2 = "_type";
    localObject1 = (String)((Map)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = paramRemoteMessage.a();
      localObject2 = "wzrk_pn";
      localObject1 = (String)((Map)localObject1).get(localObject2);
    }
    localObject2 = null;
    String str1;
    if (localObject1 == null)
    {
      localObject1 = paramRemoteMessage.a();
      str1 = "c";
      localObject1 = ((Map)localObject1).get(str1);
      if (localObject1 == null)
      {
        localObject1 = paramRemoteMessage.a();
        str1 = "c.d";
        localObject1 = ((Map)localObject1).get(str1);
        if (localObject1 == null)
        {
          localObject1 = paramRemoteMessage.a();
          str1 = "c.o";
          localObject1 = ((Map)localObject1).get(str1);
          if (localObject1 == null)
          {
            j = 0;
            localObject1 = null;
            break label138;
          }
        }
      }
      localObject1 = "delayed_notification";
    }
    label138:
    if (localObject1 == null)
    {
      localObject1 = paramRemoteMessage.a();
      str1 = "a";
      localObject1 = ((Map)localObject1).get(str1);
      if (localObject1 == null)
      {
        localObject1 = paramRemoteMessage.a();
        str1 = "e";
        localObject1 = ((Map)localObject1).get(str1);
        if (localObject1 == null) {}
      }
      else
      {
        localObject2 = "notification";
      }
    }
    else
    {
      localObject2 = localObject1;
    }
    if (localObject2 == null) {
      return;
    }
    int j = ((String)localObject2).hashCode();
    boolean bool1;
    long l1;
    boolean bool2;
    switch (j)
    {
    default: 
      break;
    case 595233003: 
      localObject1 = "notification";
      bool1 = ((String)localObject2).equals(localObject1);
      if (bool1)
      {
        localObject1 = paramRemoteMessage.a();
        localObject2 = "remoteMessage.data";
        k.a(localObject1, (String)localObject2);
        l1 = paramRemoteMessage.c();
        a((Map)localObject1, l1);
      }
      break;
    case 261640360: 
      localObject1 = "delayed_notification";
      bool1 = ((String)localObject2).equals(localObject1);
      if (bool1)
      {
        localObject1 = paramRemoteMessage.a();
        localObject2 = "remoteMessage.data";
        k.a(localObject1, (String)localObject2);
        l1 = paramRemoteMessage.c();
        paramRemoteMessage = new android/content/Intent;
        paramRemoteMessage.<init>("com.truecaller.fcm.delayed_push");
        Object localObject3 = getBaseContext();
        Object localObject4 = DelayedPushReceiver.class;
        paramRemoteMessage.setClass((Context)localObject3, (Class)localObject4);
        localObject3 = ((Map)localObject1).entrySet().iterator();
        for (;;)
        {
          boolean bool3 = ((Iterator)localObject3).hasNext();
          if (!bool3) {
            break;
          }
          localObject4 = (Map.Entry)((Iterator)localObject3).next();
          String str2 = (String)((Map.Entry)localObject4).getKey();
          localObject4 = (String)((Map.Entry)localObject4).getValue();
          paramRemoteMessage.putExtra(str2, (String)localObject4);
        }
        localObject3 = "com.truecaller.fcm.delayed_push.EXTRA_SENT_TIME";
        paramRemoteMessage.putExtra((String)localObject3, l1);
        localObject2 = h;
        if (localObject2 == null)
        {
          str1 = "pushHandler";
          k.a(str1);
        }
        localObject1 = ((com.truecaller.push.b)localObject2).a((Map)localObject1);
        if (localObject1 != null)
        {
          localObject2 = getBaseContext();
          int m = 2131364145;
          localObject3 = null;
          paramRemoteMessage = PendingIntent.getBroadcast((Context)localObject2, m, paramRemoteMessage, 0);
          localObject2 = getBaseContext();
          str1 = "alarm";
          localObject2 = ((Context)localObject2).getSystemService(str1);
          if (localObject2 != null)
          {
            localObject2 = (AlarmManager)localObject2;
            long l2 = System.currentTimeMillis();
            long l3 = 1000L;
            m = b;
            Random localRandom = new java/util/Random;
            localRandom.<init>();
            int k = a;
            k = localRandom.nextInt(k);
            m += k;
            long l4 = m * l3;
            long l5 = Math.max(0L, l4);
            l2 += l5;
            android.support.v4.app.b.a((AlarmManager)localObject2, 0, l2, paramRemoteMessage);
          }
          else
          {
            paramRemoteMessage = new c/u;
            paramRemoteMessage.<init>("null cannot be cast to non-null type android.app.AlarmManager");
            throw paramRemoteMessage;
          }
        }
        return;
      }
      break;
    case 97513456: 
      localObject1 = "flash";
      bool2 = ((String)localObject2).equals(localObject1);
      if (bool2)
      {
        localObject1 = b;
        if (localObject1 == null)
        {
          localObject2 = "flashNotificationManager";
          k.a((String)localObject2);
        }
        ((com.truecaller.flashsdk.b.a)localObject1).a(paramRemoteMessage);
        return;
      }
      break;
    case 3569038: 
      localObject1 = "true";
      bool2 = ((String)localObject2).equals(localObject1);
      if (bool2)
      {
        localObject1 = c;
        if (localObject1 == null)
        {
          localObject2 = "cleverTapNotificationManager";
          k.a((String)localObject2);
        }
        ((j)localObject1).a(paramRemoteMessage);
        return;
      }
      break;
    case 3075901: 
      paramRemoteMessage = "dapp";
      boolean bool4 = ((String)localObject2).equals(paramRemoteMessage);
      if (bool4)
      {
        paramRemoteMessage = f;
        if (paramRemoteMessage == null)
        {
          localObject1 = "featureRegistry";
          k.a((String)localObject1);
        }
        paramRemoteMessage = paramRemoteMessage.A();
        bool4 = paramRemoteMessage.a();
        if (bool4)
        {
          paramRemoteMessage = e;
          if (paramRemoteMessage == null)
          {
            paramRemoteMessage = "dappSearchRouter";
            k.a(paramRemoteMessage);
          }
          return;
        }
      }
      break;
    case 3364: 
      localObject1 = "im";
      bool2 = ((String)localObject2).equals(localObject1);
      if (bool2)
      {
        localObject1 = d;
        if (localObject1 == null)
        {
          localObject2 = "imNotificationManager";
          k.a((String)localObject2);
        }
        ((ap)localObject1).a(paramRemoteMessage);
        return;
      }
      break;
    }
  }
  
  public final void a(String paramString)
  {
    super.a(paramString);
    com.truecaller.push.e locale = g;
    if (locale == null)
    {
      String str = "pushIdManager";
      k.a(str);
    }
    locale.b(paramString);
  }
  
  public final void onCreate()
  {
    super.onCreate();
    Object localObject = getApplication();
    if (localObject != null)
    {
      localObject = ((TrueApp)localObject).a();
      b localb = new com/truecaller/fcm/b;
      localb.<init>();
      ((bp)localObject).a(localb).a(this);
      return;
    }
    localObject = new c/u;
    ((u)localObject).<init>("null cannot be cast to non-null type com.truecaller.TrueApp");
    throw ((Throwable)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.fcm.FcmMessageListenerService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */