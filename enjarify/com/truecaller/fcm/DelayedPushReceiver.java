package com.truecaller.fcm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.push.b;

public final class DelayedPushReceiver
  extends BroadcastReceiver
{
  public static final DelayedPushReceiver.a b;
  public b a;
  
  static
  {
    DelayedPushReceiver.a locala = new com/truecaller/fcm/DelayedPushReceiver$a;
    locala.<init>((byte)0);
    b = locala;
  }
  
  public DelayedPushReceiver()
  {
    TrueApp localTrueApp = TrueApp.y();
    k.a(localTrueApp, "TrueApp.getApp()");
    localTrueApp.a().a(this);
  }
  
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    if (paramIntent != null)
    {
      paramContext = paramIntent.getAction();
    }
    else
    {
      bool = false;
      paramContext = null;
    }
    Object localObject = "com.truecaller.fcm.delayed_push";
    boolean bool = k.a(paramContext, localObject);
    if (bool)
    {
      paramContext = a;
      if (paramContext == null)
      {
        localObject = "pushHandler";
        k.a((String)localObject);
      }
      localObject = paramIntent.getExtras();
      String str = "com.truecaller.fcm.delayed_push.EXTRA_SENT_TIME";
      long l1 = 0L;
      long l2 = paramIntent.getLongExtra(str, l1);
      paramContext.a((Bundle)localObject, l2);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.fcm.DelayedPushReceiver
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */