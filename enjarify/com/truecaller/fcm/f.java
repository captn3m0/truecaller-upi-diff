package com.truecaller.fcm;

import dagger.a.d;
import javax.inject.Provider;

public final class f
  implements d
{
  private final b a;
  private final Provider b;
  
  private f(b paramb, Provider paramProvider)
  {
    a = paramb;
    b = paramProvider;
  }
  
  public static f a(b paramb, Provider paramProvider)
  {
    f localf = new com/truecaller/fcm/f;
    localf.<init>(paramb, paramProvider);
    return localf;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.fcm.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */