package com.truecaller.fcm;

import dagger.a.d;
import javax.inject.Provider;

public final class g
  implements d
{
  private final b a;
  private final Provider b;
  
  private g(b paramb, Provider paramProvider)
  {
    a = paramb;
    b = paramProvider;
  }
  
  public static g a(b paramb, Provider paramProvider)
  {
    g localg = new com/truecaller/fcm/g;
    localg.<init>(paramb, paramProvider);
    return localg;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.fcm.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */