package com.truecaller.fcm;

import dagger.a.d;
import javax.inject.Provider;

public final class e
  implements d
{
  private final b a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  
  private e(b paramb, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramb;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
  }
  
  public static e a(b paramb, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    e locale = new com/truecaller/fcm/e;
    locale.<init>(paramb, paramProvider1, paramProvider2, paramProvider3);
    return locale;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.fcm.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */