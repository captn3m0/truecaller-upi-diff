package com.truecaller.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.support.v4.app.z.d;
import android.support.v4.content.b;
import android.widget.RemoteViews;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.notifications.NotificationHandlerService;
import com.truecaller.utils.l;
import java.util.Iterator;
import java.util.Set;

 enum AlarmReceiver$AlarmType$8
{
  AlarmReceiver$AlarmType$8(String paramString2)
  {
    super(paramString, 7, 259200000L, 2131364129, paramString1, paramString2, null);
  }
  
  public final Notification getNotification(Context paramContext)
  {
    Object localObject1 = TrueApp.y().a().bw();
    boolean bool1 = ((l)localObject1).d();
    PendingIntent localPendingIntent = null;
    if (bool1) {
      return null;
    }
    localObject1 = paramContext.getPackageManager();
    Object localObject2 = NotificationHandlerService.a.iterator();
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject2).hasNext();
      int j = 0;
      Object localObject3 = null;
      String str1;
      if (bool2) {
        str1 = (String)((Iterator)localObject2).next();
      }
      try
      {
        ((PackageManager)localObject1).getPackageInfo(str1, 0);
        j = 1;
        if (j == 0) {
          return null;
        }
        localObject1 = new android/content/Intent;
        ((Intent)localObject1).<init>("com.truecaller.intent.action.PROMO_CLICKED", null, paramContext, AlarmReceiver.class);
        int i = 268435456;
        localObject1 = PendingIntent.getBroadcast(paramContext, 2131364132, (Intent)localObject1, i);
        localObject2 = new android/support/v4/app/z$d;
        localObject3 = AlarmReceiver.AlarmType.access$400(paramContext);
        ((z.d)localObject2).<init>(paramContext, (String)localObject3);
        ((z.d)localObject2).a(2131234787);
        j = b.c(paramContext, 2131100594);
        C = j;
        localObject3 = new android/widget/RemoteViews;
        String str2 = paramContext.getPackageName();
        ((RemoteViews)localObject3).<init>(str2, 2131559047);
        N.contentView = ((RemoteViews)localObject3);
        f = ((PendingIntent)localObject1);
        ((z.d)localObject2).d(16);
        localObject1 = ((z.d)localObject2).h();
        localObject2 = new android/content/Intent;
        ((Intent)localObject2).<init>("com.truecaller.intent.action.PROMO_DISMISSED", null, paramContext, AlarmReceiver.class);
        localPendingIntent = PendingIntent.getBroadcast(paramContext, 2131364130, (Intent)localObject2, i);
        localObject2 = new android/widget/RemoteViews;
        paramContext = paramContext.getPackageName();
        ((RemoteViews)localObject2).<init>(paramContext, 2131559048);
        ((RemoteViews)localObject2).setOnClickPendingIntent(2131362507, localPendingIntent);
        bigContentView = ((RemoteViews)localObject2);
        return (Notification)localObject1;
      }
      catch (PackageManager.NameNotFoundException localNameNotFoundException) {}
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.AlarmReceiver.AlarmType.8
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */