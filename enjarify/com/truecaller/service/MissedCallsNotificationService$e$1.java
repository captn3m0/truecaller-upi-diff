package com.truecaller.service;

import android.app.Notification;
import android.content.Context;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.g.b.v.c;
import c.o.b;
import c.x;
import com.truecaller.notifications.af;
import com.truecaller.util.f;
import com.truecaller.utils.h;
import kotlinx.coroutines.ag;

final class MissedCallsNotificationService$e$1
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag e;
  
  MissedCallsNotificationService$e$1(MissedCallsNotificationService paramMissedCallsNotificationService, v.c paramc, int paramInt, c paramc1)
  {
    super(2, paramc1);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    1 local1 = new com/truecaller/service/MissedCallsNotificationService$e$1;
    MissedCallsNotificationService localMissedCallsNotificationService = b;
    v.c localc = c;
    int i = d;
    local1.<init>(localMissedCallsNotificationService, localc, i, paramc);
    paramObject = (ag)paramObject;
    e = ((ag)paramObject);
    return local1;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        boolean bool2 = h.a();
        if (bool2)
        {
          paramObject = new com/truecaller/notifications/af;
          localObject = (Context)b;
          ((af)paramObject).<init>((Context)localObject);
          paramObject = (Context)b;
          localObject = (Notification)c.a;
          String str = "missedCall";
          int k = d;
          af.a((Context)paramObject, (Notification)localObject, str, k);
        }
        else
        {
          paramObject = (Context)b;
          int j = d;
          f.a((Context)paramObject, j);
        }
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (1)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((1)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.MissedCallsNotificationService.e.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */