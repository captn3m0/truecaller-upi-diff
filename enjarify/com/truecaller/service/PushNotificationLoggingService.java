package com.truecaller.service;

import android.app.IntentService;
import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.content.Context;
import android.content.Intent;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.log.d;
import com.truecaller.push.b;

public class PushNotificationLoggingService
  extends IntentService
{
  public PushNotificationLoggingService()
  {
    super("PushNotificationLoggingService");
  }
  
  public static PendingIntent a(Context paramContext)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, PushNotificationLoggingService.class);
    localIntent.setAction("com.truecaller.ACTION_NOTIFICATION_DISMISSED");
    return PendingIntent.getService(paramContext, 2131364163, localIntent, 134217728);
  }
  
  public static PendingIntent a(Context paramContext, PendingIntent paramPendingIntent)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, PushNotificationLoggingService.class);
    localIntent.setAction("com.truecaller.ACTION_NOTIFICATION_OPENED");
    localIntent.putExtra("pendingIntent", paramPendingIntent);
    return PendingIntent.getService(paramContext, 2131364164, localIntent, 134217728);
  }
  
  protected void onHandleIntent(Intent paramIntent)
  {
    if (paramIntent != null)
    {
      Object localObject = paramIntent.getAction();
      if (localObject != null)
      {
        localObject = TrueApp.y().a().ca();
        String str1 = paramIntent.getAction();
        int i = -1;
        int j = str1.hashCode();
        int k = -1391836266;
        String str2;
        boolean bool1;
        if (j != k)
        {
          k = 61993084;
          if (j == k)
          {
            str2 = "com.truecaller.ACTION_NOTIFICATION_OPENED";
            bool1 = str1.equals(str2);
            if (bool1) {
              i = 0;
            }
          }
        }
        else
        {
          str2 = "com.truecaller.ACTION_NOTIFICATION_DISMISSED";
          bool1 = str1.equals(str2);
          if (bool1) {
            i = 1;
          }
        }
        switch (i)
        {
        default: 
          break;
        case 1: 
          ((b)localObject).c();
          break;
        case 0: 
          ((b)localObject).a();
          ((b)localObject).c();
          localObject = "pendingIntent";
          boolean bool2 = paramIntent.hasExtra((String)localObject);
          if (bool2)
          {
            localObject = "pendingIntent";
            paramIntent = (PendingIntent)paramIntent.getParcelableExtra((String)localObject);
            if (paramIntent != null) {
              try
              {
                paramIntent.send();
                return;
              }
              catch (PendingIntent.CanceledException paramIntent)
              {
                d.a(paramIntent);
              }
            }
          }
          return;
        }
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.PushNotificationLoggingService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */