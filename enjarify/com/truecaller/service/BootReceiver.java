package com.truecaller.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.truecaller.androidactors.f;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.debug.log.a;
import com.truecaller.messaging.data.t;
import com.truecaller.utils.l;

public class BootReceiver
  extends BroadcastReceiver
{
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    Object localObject = "android.intent.action.BOOT_COMPLETED";
    paramIntent = paramIntent.getAction();
    boolean bool1 = ((String)localObject).equals(paramIntent);
    if (bool1)
    {
      bool1 = true;
      localObject = new Object[bool1];
      String str = "Device boot";
      localObject[0] = str;
      a.a((Object[])localObject);
      new String[1][0] = "DEVICE WAS BOOTED! All alarms was cleared, check for re-scheduling them";
      AlarmReceiver.a(paramContext, bool1);
      CallStateService.a(paramContext);
      paramContext = (bk)paramContext.getApplicationContext();
      localObject = paramContext.a().p();
      paramContext = paramContext.a().bw();
      String[] arrayOfString = { "android.permission.READ_SMS" };
      boolean bool2 = paramContext.a(arrayOfString);
      if (bool2)
      {
        paramContext = (t)((f)localObject).a();
        paramContext.b(bool1);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.BootReceiver
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */