package com.truecaller.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.calling.ak;
import com.truecaller.calling.am;
import com.truecaller.common.b.a;
import com.truecaller.common.background.b;
import com.truecaller.notificationchannels.p;
import com.truecaller.old.data.access.Settings;
import com.truecaller.old.data.access.d.a;
import com.truecaller.old.data.access.g;
import com.truecaller.old.data.entity.e;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Receiver
  extends BroadcastReceiver
{
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    Object localObject1 = paramIntent.getAction();
    if (localObject1 != null)
    {
      int i = -1;
      int j = ((String)localObject1).hashCode();
      int k = -1326089125;
      int n = 3;
      Object localObject2;
      boolean bool4;
      ArrayList localArrayList;
      if (j != k)
      {
        k = -1304749796;
        if (j != k)
        {
          k = -19011148;
          if (j != k)
          {
            k = 1901012141;
            if (j == k)
            {
              localObject2 = "android.intent.action.NEW_OUTGOING_CALL";
              bool4 = ((String)localObject1).equals(localObject2);
              if (bool4) {
                i = 1;
              }
            }
          }
          else
          {
            localObject2 = "android.intent.action.LOCALE_CHANGED";
            bool4 = ((String)localObject1).equals(localObject2);
            if (bool4) {
              i = 3;
            }
          }
        }
        else
        {
          localObject2 = "com.truecaller.intent.action.PHONE_NOTIFICATION_CANCELLED";
          bool4 = ((String)localObject1).equals(localObject2);
          if (bool4) {
            i = 2;
          }
        }
      }
      else
      {
        localObject2 = "android.intent.action.PHONE_STATE";
        bool4 = ((String)localObject1).equals(localObject2);
        if (bool4)
        {
          i = 0;
          localArrayList = null;
        }
      }
      switch (i)
      {
      default: 
        break;
      case 3: 
        Settings.c(paramContext);
        paramContext = (TrueApp)paramContext.getApplicationContext();
        paramContext.a().aB().b();
        paramIntent = c;
        localObject1 = new com/truecaller/old/data/access/d$a;
        ((d.a)localObject1).<init>(paramContext);
        paramIntent.a((Runnable)localObject1);
        break;
      case 2: 
        localObject1 = "notificationType";
        int i1 = paramIntent.getIntExtra((String)localObject1, n);
        if (i1 == n)
        {
          paramIntent = new com/truecaller/old/data/access/g;
          paramIntent.<init>(paramContext);
          paramIntent.b();
          return;
        }
        localObject1 = new com/truecaller/old/data/access/g;
        ((g)localObject1).<init>(paramContext);
        paramContext = ((g)localObject1).a();
        if (paramContext != null)
        {
          localArrayList = new java/util/ArrayList;
          localArrayList.<init>();
          paramContext = paramContext.iterator();
          for (;;)
          {
            boolean bool1 = paramContext.hasNext();
            if (!bool1) {
              break;
            }
            localObject2 = (e)paramContext.next();
            k = i1 & 0x1;
            if (k != 0)
            {
              boolean bool2 = d;
              if (bool2) {}
            }
            else
            {
              int m = i1 & 0x2;
              if (m == 0) {
                continue;
              }
              boolean bool3 = d;
              if (bool3) {
                continue;
              }
            }
            localArrayList.add(localObject2);
          }
          ((g)localObject1).a(localArrayList);
        }
        return;
      case 1: 
        am.a(paramContext).b(paramContext, paramIntent);
        return;
      case 0: 
        am.a(paramContext).a(paramContext, paramIntent);
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.Receiver
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */