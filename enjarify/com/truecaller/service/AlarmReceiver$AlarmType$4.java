package com.truecaller.service;

import android.app.Notification;
import android.content.Context;

 enum AlarmReceiver$AlarmType$4
{
  AlarmReceiver$AlarmType$4(String paramString2)
  {
    super(paramString, 3, 172800000L, 2131364126, paramString1, paramString2, null);
  }
  
  public final Notification getNotification(Context paramContext)
  {
    long l1 = System.currentTimeMillis();
    long l2 = AlarmReceiver.b(paramContext);
    l1 -= l2;
    l2 = 15552000000L;
    boolean bool = l1 < l2;
    if (bool) {
      return AlarmReceiver.AlarmType.TYPE_20DAYS.getNotification(paramContext);
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.AlarmReceiver.AlarmType.4
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */