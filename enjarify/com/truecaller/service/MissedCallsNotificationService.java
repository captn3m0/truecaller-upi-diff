package com.truecaller.service;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.af;
import android.support.v4.app.z.d;
import android.text.TextUtils;
import c.g.a.m;
import c.g.b.k;
import c.u;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.featuretoggles.e;
import com.truecaller.i.c;
import com.truecaller.network.search.j;
import com.truecaller.network.search.n;
import com.truecaller.notificationchannels.b;
import com.truecaller.utils.l;
import java.io.IOException;
import java.util.UUID;

public final class MissedCallsNotificationService
  extends af
{
  public static final MissedCallsNotificationService.a q;
  public c.d.f j;
  public b k;
  public com.truecaller.androidactors.f l;
  public e m;
  public com.truecaller.notifications.a n;
  public c o;
  public l p;
  
  static
  {
    MissedCallsNotificationService.a locala = new com/truecaller/service/MissedCallsNotificationService$a;
    locala.<init>((byte)0);
    q = locala;
  }
  
  private final Contact a(Context paramContext, String paramString)
  {
    Object localObject1 = getApplication();
    if (localObject1 != null)
    {
      localObject1 = (com.truecaller.common.b.a)localObject1;
      boolean bool = ((com.truecaller.common.b.a)localObject1).p();
      if (!bool) {
        return null;
      }
      localObject1 = paramString;
      localObject1 = (CharSequence)paramString;
      bool = TextUtils.isEmpty((CharSequence)localObject1);
      if (bool) {
        return null;
      }
      try
      {
        localObject1 = new com/truecaller/network/search/j;
        localObject2 = UUID.randomUUID();
        String str = "notification";
        ((j)localObject1).<init>(paramContext, (UUID)localObject2, str);
        i = 6;
        paramContext = ((j)localObject1).a(i);
        paramContext = paramContext.a(paramString);
        paramContext = paramContext.a();
        paramContext = paramContext.f();
        if (paramContext != null) {
          return paramContext.a();
        }
      }
      catch (IOException localIOException)
      {
        int i = 2;
        paramContext = new String[i];
        localObject1 = null;
        Object localObject2 = "Unable to search for %s";
        paramContext[0] = localObject2;
        bool = true;
        paramContext[bool] = paramString;
      }
      return null;
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type com.truecaller.common.app.ApplicationBase");
    throw paramContext;
  }
  
  public static final void a(Context paramContext)
  {
    MissedCallsNotificationService.a.a(paramContext);
  }
  
  private static boolean a(HistoryEvent paramHistoryEvent)
  {
    int i = paramHistoryEvent.r();
    int i1 = 3;
    return i == i1;
  }
  
  public final void a(Intent paramIntent)
  {
    k.b(paramIntent, "intent");
    paramIntent = new com/truecaller/service/MissedCallsNotificationService$e;
    paramIntent.<init>(this, null);
    kotlinx.coroutines.f.a((m)paramIntent);
  }
  
  public final c.d.f c()
  {
    c.d.f localf = j;
    if (localf == null)
    {
      String str = "uiCoroutineContext";
      k.a(str);
    }
    return localf;
  }
  
  public final com.truecaller.androidactors.f d()
  {
    com.truecaller.androidactors.f localf = l;
    if (localf == null)
    {
      String str = "historyManager";
      k.a(str);
    }
    return localf;
  }
  
  public final c e()
  {
    c localc = o;
    if (localc == null)
    {
      String str = "callingSettings";
      k.a(str);
    }
    return localc;
  }
  
  final z.d f()
  {
    z.d locald = new android/support/v4/app/z$d;
    Object localObject1 = this;
    localObject1 = (Context)this;
    Object localObject2 = k;
    if (localObject2 == null)
    {
      String str = "callingNotificationChannelProvider";
      k.a(str);
    }
    localObject2 = ((b)localObject2).X_();
    locald.<init>((Context)localObject1, (String)localObject2);
    return locald;
  }
  
  public final void onCreate()
  {
    super.onCreate();
    Object localObject = getApplication();
    if (localObject != null)
    {
      ((bk)localObject).a().a(this);
      return;
    }
    localObject = new c/u;
    ((u)localObject).<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
    throw ((Throwable)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.MissedCallsNotificationService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */