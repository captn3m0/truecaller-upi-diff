package com.truecaller.service;

import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ac;
import android.support.v4.app.z.c;
import android.support.v4.app.z.d;
import android.support.v4.app.z.g;
import android.text.TextUtils;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.calling.initiate_call.b.a.a;
import com.truecaller.common.h.am;
import com.truecaller.flashsdk.core.c;
import com.truecaller.notifications.a;
import com.truecaller.old.data.access.Settings;
import com.truecaller.util.at;

public class CallMeBackNotificationService
  extends IntentService
{
  public CallMeBackNotificationService()
  {
    super("CallMeBackNotificationService");
  }
  
  public static void a(Context paramContext, com.truecaller.old.data.entity.Notification paramNotification)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("com.truecaller.intent.action.CALL_ME_BACK_NOTIFICATION_RECEIVED", null, paramContext, CallMeBackNotificationService.class);
    String str = paramNotification.a(paramContext);
    localIntent.putExtra("title", str);
    str = paramNotification.b(paramContext);
    localIntent.putExtra("text", str);
    str = paramNotification.a("i");
    localIntent.putExtra("photo", str);
    str = paramNotification.a("f");
    localIntent.putExtra("name", str);
    paramNotification = paramNotification.a("n");
    localIntent.putExtra("number", paramNotification);
    paramContext.startService(localIntent);
  }
  
  protected void onHandleIntent(Intent paramIntent)
  {
    if (paramIntent == null) {
      return;
    }
    Object localObject1 = paramIntent.getAction();
    boolean bool1 = TextUtils.isEmpty((CharSequence)localObject1);
    if (bool1) {
      return;
    }
    int i = ((String)localObject1).hashCode();
    int j = -852225780;
    int k = -1;
    Object localObject2;
    boolean bool3;
    if (i != j)
    {
      j = -75112457;
      if (i != j)
      {
        j = 251302897;
        if (i == j)
        {
          localObject2 = "com.truecaller.intent.action.CALL_ME_BACK_NOTIFICATION_RECEIVED";
          bool3 = ((String)localObject1).equals(localObject2);
          if (bool3)
          {
            bool3 = false;
            localObject1 = null;
            break label135;
          }
        }
      }
      else
      {
        localObject2 = "com.truecaller.intent.action.CALL_ME_BACK_NOTIFICATION_CLICKED";
        bool3 = ((String)localObject1).equals(localObject2);
        if (bool3)
        {
          bool3 = true;
          break label135;
        }
      }
    }
    else
    {
      localObject2 = "com.truecaller.intent.action.CALL_ME_BACK_FLASH";
      bool3 = ((String)localObject1).equals(localObject2);
      if (bool3)
      {
        n = 2;
        break label135;
      }
    }
    int n = -1;
    label135:
    i = 2131362379;
    j = 0;
    Object localObject3 = null;
    String str1;
    switch (n)
    {
    default: 
      break;
    case 2: 
      ac.a(this).a(null, i);
      paramIntent = paramIntent.getExtras();
      localObject1 = paramIntent.getString("number");
      localObject2 = "name";
      str1 = paramIntent.getString((String)localObject2, (String)localObject1);
      boolean bool4 = am.b((CharSequence)localObject1);
      if (!bool4)
      {
        paramIntent = "+";
        localObject2 = "";
      }
      break;
    case 1: 
    case 0: 
      Object localObject4;
      Object localObject5;
      try
      {
        paramIntent = ((String)localObject1).replace(paramIntent, (CharSequence)localObject2);
        long l = Long.parseLong(paramIntent);
        localObject3 = c.a();
        localObject4 = "callMeBackNotification";
        localObject5 = this;
        ((com.truecaller.flashsdk.core.b)localObject3).a(this, l, str1, (String)localObject4);
        return;
      }
      catch (NumberFormatException localNumberFormatException) {}
      ac.a(this).a(null, i);
      localObject1 = ((bk)getApplicationContext()).a().bM();
      localObject2 = new com/truecaller/calling/initiate_call/b$a$a;
      paramIntent = paramIntent.getStringExtra("number");
      ((b.a.a)localObject2).<init>(paramIntent, "callMeBackNotification");
      paramIntent = ((b.a.a)localObject2).a();
      ((com.truecaller.calling.initiate_call.b)localObject1).a(paramIntent);
      return;
      localObject1 = paramIntent.getStringExtra("title");
      Object localObject6 = paramIntent.getStringExtra("text");
      Object localObject7 = paramIntent.getStringExtra("photo");
      str1 = paramIntent.getStringExtra("number");
      boolean bool5 = TextUtils.isEmpty(str1);
      if (!bool5)
      {
        bool5 = TextUtils.isEmpty((CharSequence)localObject1);
        if (!bool5)
        {
          bool5 = TextUtils.isEmpty((CharSequence)localObject7);
          if (!bool5) {
            localObject7 = Uri.parse((String)localObject7);
          } else {
            localObject7 = null;
          }
          if (localObject7 != null) {
            localObject7 = ((Uri)localObject7).toString();
          } else {
            localObject7 = null;
          }
          localObject7 = at.a(this, (String)localObject7);
          if (localObject7 == null)
          {
            localObject7 = getResources();
            int i2 = 2131689472;
            localObject7 = BitmapFactory.decodeResource((Resources)localObject7, i2);
          }
          localObject4 = new android/content/Intent;
          ((Intent)localObject4).<init>("com.truecaller.intent.action.CALL_ME_BACK_NOTIFICATION_CLICKED", null, this, CallMeBackNotificationService.class);
          String str2 = "number";
          localObject4 = ((Intent)localObject4).putExtra(str2, str1);
          int i3 = 134217728;
          localObject4 = PendingIntent.getService(this, i, (Intent)localObject4, i3);
          Object localObject8 = new android/support/v4/app/z$d;
          String str3 = ((bk)getApplicationContext()).a().aC().a();
          ((z.d)localObject8).<init>(this, str3);
          localObject1 = ((z.d)localObject8).a((CharSequence)localObject1).b((CharSequence)localObject6);
          localObject8 = new android/support/v4/app/z$c;
          ((z.c)localObject8).<init>();
          localObject6 = ((z.c)localObject8).b((CharSequence)localObject6);
          localObject1 = ((z.d)localObject1).a((z.g)localObject6);
          int i4 = android.support.v4.content.b.c(this, 2131100594);
          C = i4;
          localObject1 = ((z.d)localObject1).c(k).a(2131234787).a((Bitmap)localObject7);
          f = ((PendingIntent)localObject4);
          i4 = 2131888370;
          localObject6 = getString(i4);
          localObject1 = ((z.d)localObject1).a(2131234319, (CharSequence)localObject6, (PendingIntent)localObject4);
          ((z.d)localObject1).d(16);
          boolean bool2 = Settings.g();
          if (bool2)
          {
            localObject5 = ((bk)getApplicationContext()).a().aV();
            localObject4 = "";
            localObject6 = str1.replace("+", (CharSequence)localObject4);
            localObject5 = ((com.truecaller.flashsdk.core.b)localObject5).h((String)localObject6);
            bool2 = c;
            if (bool2)
            {
              paramIntent = paramIntent.getStringExtra("name");
              localObject5 = new android/content/Intent;
              localObject6 = "com.truecaller.intent.action.CALL_ME_BACK_FLASH";
              localObject4 = CallMeBackNotificationService.class;
              ((Intent)localObject5).<init>((String)localObject6, null, this, (Class)localObject4);
              ((Intent)localObject5).putExtra("number", str1);
              localObject3 = "name";
              ((Intent)localObject5).putExtra((String)localObject3, paramIntent);
              int i1 = 2131363114;
              paramIntent = PendingIntent.getService(this, i1, (Intent)localObject5, i3);
              j = 2131234137;
              int m = 2131888371;
              localObject5 = getString(m);
              ((z.d)localObject1).a(j, (CharSequence)localObject5, paramIntent);
            }
          }
          paramIntent = ((bk)getApplicationContext()).a().W();
          localObject1 = ((z.d)localObject1).h();
          localObject3 = "notificationCallMeBack";
          paramIntent.a(i, (android.app.Notification)localObject1, (String)localObject3);
          if (localObject7 == null) {
            break;
          }
          ((Bitmap)localObject7).recycle();
          return;
        }
      }
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.CallMeBackNotificationService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */