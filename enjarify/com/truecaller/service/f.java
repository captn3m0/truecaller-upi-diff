package com.truecaller.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.google.gson.c.a;
import com.truecaller.old.data.access.Settings;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public final class f
  implements e
{
  private Context a;
  private final Map b;
  
  public f(Context arg1)
  {
    ??? = ???.getApplicationContext();
    a = ???;
    ??? = c();
    if (??? == null)
    {
      ??? = new java/util/HashMap;
      ???.<init>();
    }
    ??? = Collections.synchronizedMap(???);
    b = ???;
    synchronized (b)
    {
      Object localObject1 = b;
      localObject1 = ((Map)localObject1).values();
      localObject1 = ((Collection)localObject1).iterator();
      for (;;)
      {
        boolean bool = ((Iterator)localObject1).hasNext();
        if (!bool) {
          break;
        }
        Object localObject3 = ((Iterator)localObject1).next();
        localObject3 = (MissedCallReminder)localObject3;
        a((MissedCallReminder)localObject3);
      }
      return;
    }
  }
  
  private PendingIntent a(MissedCallReminder paramMissedCallReminder, int paramInt)
  {
    Intent localIntent = MissedCallReminderService.a(a, paramMissedCallReminder);
    Context localContext = a;
    int i = notificationId;
    return PendingIntent.getService(localContext, i, localIntent, paramInt);
  }
  
  private void a(MissedCallReminder paramMissedCallReminder)
  {
    PendingIntent localPendingIntent = a(paramMissedCallReminder, 134217728);
    long l1 = System.currentTimeMillis();
    long l2 = timestamp;
    l1 -= l2;
    l2 = 3600000L;
    l1 /= l2;
    long l3 = timestamp;
    l1 = (l1 + 1L) * l2;
    l3 += l1;
    paramMissedCallReminder = a.getSystemService("alarm");
    Object localObject = paramMissedCallReminder;
    localObject = (AlarmManager)paramMissedCallReminder;
    long l4 = 3600000L;
    ((AlarmManager)localObject).setRepeating(1, l3, l4, localPendingIntent);
  }
  
  private void b()
  {
    ??? = b;
    boolean bool = ???.isEmpty();
    if (!bool) {
      synchronized (b)
      {
        Object localObject1 = new com/google/gson/f;
        ((com.google.gson.f)localObject1).<init>();
        Map localMap2 = b;
        localObject1 = ((com.google.gson.f)localObject1).b(localMap2);
        Settings.b("missedCallReminders", (String)localObject1);
        return;
      }
    }
    Settings.n("missedCallReminders");
  }
  
  private void b(MissedCallReminder paramMissedCallReminder)
  {
    int i = 536870912;
    paramMissedCallReminder = a(paramMissedCallReminder, i);
    if (paramMissedCallReminder != null)
    {
      Object localObject = a;
      String str = "alarm";
      localObject = (AlarmManager)((Context)localObject).getSystemService(str);
      ((AlarmManager)localObject).cancel(paramMissedCallReminder);
    }
  }
  
  private Map c()
  {
    Object localObject1 = Settings.b("missedCallReminders");
    boolean bool = TextUtils.isEmpty((CharSequence)localObject1);
    if (!bool) {
      try
      {
        com.google.gson.f localf = new com/google/gson/f;
        localf.<init>();
        Object localObject2 = new com/truecaller/service/f$1;
        ((f.1)localObject2).<init>(this);
        localObject2 = b;
        localObject1 = localf.a((String)localObject1, (Type)localObject2);
        return (Map)localObject1;
      }
      catch (Exception localException) {}
    }
    return null;
  }
  
  public final void a()
  {
    synchronized (b)
    {
      Object localObject1 = b;
      localObject1 = ((Map)localObject1).values();
      localObject1 = ((Collection)localObject1).iterator();
      for (;;)
      {
        boolean bool = ((Iterator)localObject1).hasNext();
        if (!bool) {
          break;
        }
        Object localObject3 = ((Iterator)localObject1).next();
        localObject3 = (MissedCallReminder)localObject3;
        b((MissedCallReminder)localObject3);
      }
      b.clear();
      b();
      return;
    }
  }
  
  /* Error */
  public final void a(com.truecaller.data.entity.HistoryEvent paramHistoryEvent)
  {
    // Byte code:
    //   0: ldc -85
    //   2: astore_2
    //   3: aload_2
    //   4: invokestatic 175	com/truecaller/old/data/access/Settings:e	(Ljava/lang/String;)Z
    //   7: istore_3
    //   8: iconst_1
    //   9: istore 4
    //   11: aconst_null
    //   12: astore 5
    //   14: iload_3
    //   15: ifeq +25 -> 40
    //   18: aload_0
    //   19: getfield 22	com/truecaller/service/f:a	Landroid/content/Context;
    //   22: checkcast 177	com/truecaller/TrueApp
    //   25: astore_2
    //   26: aload_2
    //   27: invokevirtual 180	com/truecaller/TrueApp:p	()Z
    //   30: istore_3
    //   31: iload_3
    //   32: ifeq +8 -> 40
    //   35: iconst_1
    //   36: istore_3
    //   37: goto +7 -> 44
    //   40: iconst_0
    //   41: istore_3
    //   42: aconst_null
    //   43: astore_2
    //   44: iload_3
    //   45: ifeq +274 -> 319
    //   48: aload_1
    //   49: getfield 185	com/truecaller/data/entity/HistoryEvent:b	Ljava/lang/String;
    //   52: astore_2
    //   53: aload_0
    //   54: getfield 22	com/truecaller/service/f:a	Landroid/content/Context;
    //   57: astore 6
    //   59: aload 6
    //   61: aload_2
    //   62: invokestatic 190	com/truecaller/search/f:a	(Landroid/content/Context;Ljava/lang/String;)Z
    //   65: istore 7
    //   67: iload 7
    //   69: ifeq +250 -> 319
    //   72: aload_0
    //   73: getfield 37	com/truecaller/service/f:b	Ljava/util/Map;
    //   76: astore 6
    //   78: aload 6
    //   80: aload_2
    //   81: invokeinterface 194 2 0
    //   86: istore 7
    //   88: iload 7
    //   90: ifne +229 -> 319
    //   93: iconst_0
    //   94: istore 7
    //   96: aconst_null
    //   97: astore 6
    //   99: aload_0
    //   100: getfield 22	com/truecaller/service/f:a	Landroid/content/Context;
    //   103: astore 8
    //   105: aload 8
    //   107: invokevirtual 198	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   110: astore 9
    //   112: invokestatic 203	com/truecaller/content/TruecallerContract$n:a	()Landroid/net/Uri;
    //   115: astore 10
    //   117: ldc -52
    //   119: astore 8
    //   121: iconst_1
    //   122: anewarray 206	java/lang/String
    //   125: dup
    //   126: iconst_0
    //   127: aload 8
    //   129: aastore
    //   130: astore 11
    //   132: ldc -48
    //   134: astore 12
    //   136: iconst_2
    //   137: istore 13
    //   139: iload 13
    //   141: anewarray 206	java/lang/String
    //   144: astore 14
    //   146: aload_1
    //   147: getfield 212	com/truecaller/data/entity/HistoryEvent:h	J
    //   150: lstore 15
    //   152: lload 15
    //   154: invokestatic 216	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   157: astore 8
    //   159: aload 14
    //   161: iconst_0
    //   162: aload 8
    //   164: aastore
    //   165: aload 14
    //   167: iload 4
    //   169: aload_2
    //   170: aastore
    //   171: aload 9
    //   173: aload 10
    //   175: aload 11
    //   177: aload 12
    //   179: aload 14
    //   181: aconst_null
    //   182: invokevirtual 222	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   185: astore 6
    //   187: aload 6
    //   189: ifnull +30 -> 219
    //   192: aload 6
    //   194: invokeinterface 228 1 0
    //   199: istore 4
    //   201: iload 4
    //   203: ifle +16 -> 219
    //   206: aload 6
    //   208: ifnull +10 -> 218
    //   211: aload 6
    //   213: invokeinterface 231 1 0
    //   218: return
    //   219: aload 6
    //   221: ifnull +29 -> 250
    //   224: goto +19 -> 243
    //   227: astore_1
    //   228: goto +77 -> 305
    //   231: astore 17
    //   233: aload 17
    //   235: invokestatic 236	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   238: aload 6
    //   240: ifnull +10 -> 250
    //   243: aload 6
    //   245: invokeinterface 231 1 0
    //   250: new 61	com/truecaller/service/MissedCallReminder
    //   253: astore 17
    //   255: aload_1
    //   256: getfield 238	com/truecaller/data/entity/HistoryEvent:c	Ljava/lang/String;
    //   259: astore 5
    //   261: aload_1
    //   262: getfield 212	com/truecaller/data/entity/HistoryEvent:h	J
    //   265: lstore 18
    //   267: aload 17
    //   269: aload 5
    //   271: aload_2
    //   272: lload 18
    //   274: invokespecial 241	com/truecaller/service/MissedCallReminder:<init>	(Ljava/lang/String;Ljava/lang/String;J)V
    //   277: aload_0
    //   278: aload 17
    //   280: invokespecial 64	com/truecaller/service/f:a	(Lcom/truecaller/service/MissedCallReminder;)V
    //   283: aload_0
    //   284: getfield 37	com/truecaller/service/f:b	Ljava/util/Map;
    //   287: astore_1
    //   288: aload_1
    //   289: aload_2
    //   290: aload 17
    //   292: invokeinterface 245 3 0
    //   297: pop
    //   298: aload_0
    //   299: invokespecial 169	com/truecaller/service/f:b	()V
    //   302: goto +17 -> 319
    //   305: aload 6
    //   307: ifnull +10 -> 317
    //   310: aload 6
    //   312: invokeinterface 231 1 0
    //   317: aload_1
    //   318: athrow
    //   319: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	320	0	this	f
    //   0	320	1	paramHistoryEvent	com.truecaller.data.entity.HistoryEvent
    //   2	288	2	localObject1	Object
    //   7	38	3	bool1	boolean
    //   9	193	4	i	int
    //   12	258	5	str1	String
    //   57	254	6	localObject2	Object
    //   65	30	7	bool2	boolean
    //   103	60	8	localObject3	Object
    //   110	62	9	localContentResolver	android.content.ContentResolver
    //   115	59	10	localUri	android.net.Uri
    //   130	46	11	arrayOfString1	String[]
    //   134	44	12	str2	String
    //   137	3	13	j	int
    //   144	36	14	arrayOfString2	String[]
    //   150	3	15	l1	long
    //   231	3	17	localException	Exception
    //   253	38	17	localMissedCallReminder	MissedCallReminder
    //   265	8	18	l2	long
    // Exception table:
    //   from	to	target	type
    //   99	103	227	finally
    //   105	110	227	finally
    //   112	115	227	finally
    //   121	130	227	finally
    //   139	144	227	finally
    //   146	150	227	finally
    //   152	157	227	finally
    //   162	165	227	finally
    //   169	171	227	finally
    //   181	185	227	finally
    //   192	199	227	finally
    //   233	238	227	finally
    //   99	103	231	java/lang/Exception
    //   105	110	231	java/lang/Exception
    //   112	115	231	java/lang/Exception
    //   121	130	231	java/lang/Exception
    //   139	144	231	java/lang/Exception
    //   146	150	231	java/lang/Exception
    //   152	157	231	java/lang/Exception
    //   162	165	231	java/lang/Exception
    //   169	171	231	java/lang/Exception
    //   181	185	231	java/lang/Exception
    //   192	199	231	java/lang/Exception
  }
  
  public final void a(String paramString)
  {
    Object localObject = (MissedCallReminder)b.get(paramString);
    if (localObject != null)
    {
      b((MissedCallReminder)localObject);
      localObject = b;
      ((Map)localObject).remove(paramString);
      b();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */