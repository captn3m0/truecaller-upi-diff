package com.truecaller.service;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Process;
import android.support.v4.app.af;
import com.google.gson.l;
import com.google.gson.o;
import com.google.gson.q;
import com.truecaller.log.d;
import com.truecaller.old.data.entity.Notification;
import com.truecaller.util.bl;

public class NotificationsService
  extends af
{
  public static void a(Context paramContext, long paramLong, Notification paramNotification)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putInt("EXTRA_TYPE", 0);
    paramNotification = paramNotification.s().toString();
    localBundle.putString("EXTRA_NOTIFICATION", paramNotification);
    com.truecaller.common.background.a.a(paramContext, NotificationsService.class, paramLong, localBundle, 2131364135);
  }
  
  public static void a(Context paramContext, long paramLong, String paramString1, String paramString2, Intent paramIntent, String paramString3)
  {
    new String[1][0] = "NotificationsService - Intent handling scheduled";
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putInt("EXTRA_TYPE", 1);
    localBundle.putString("EXTRA_TITLE", paramString1);
    localBundle.putString("EXTRA_TEXT", paramString2);
    localBundle.putParcelable("EXTRA_INTENT", paramIntent);
    localBundle.putString("EXTRA_ANALYTICS_SUBTYPE", paramString3);
    com.truecaller.common.background.a.a(paramContext, NotificationsService.class, paramLong, localBundle, 2131364134);
  }
  
  public final void a(Intent paramIntent)
  {
    new String[1][0] = "NotificationsService - Intent handling started";
    Process.setThreadPriority(10);
    Object localObject1 = (com.truecaller.common.b.a)getApplication();
    boolean bool = ((com.truecaller.common.b.a)localObject1).p();
    if (!bool)
    {
      new String[1][0] = "NotificationService - Wizard not done: skipping notification!";
      return;
    }
    localObject1 = "EXTRA_TYPE";
    int j = -1;
    try
    {
      int i = paramIntent.getIntExtra((String)localObject1, j);
      Object localObject3;
      switch (i)
      {
      default: 
        break;
      case 1: 
        localObject1 = "EXTRA_TITLE";
        String str1 = paramIntent.getStringExtra((String)localObject1);
        localObject1 = "EXTRA_TEXT";
        String str2 = paramIntent.getStringExtra((String)localObject1);
        localObject1 = "EXTRA_INTENT";
        localObject1 = paramIntent.getParcelableExtra((String)localObject1);
        Object localObject2 = localObject1;
        localObject2 = (Intent)localObject1;
        localObject1 = "EXTRA_ANALYTICS_SUBTYPE";
        String str3 = paramIntent.getStringExtra((String)localObject1);
        localObject3 = getApplicationContext();
        bl.a((Context)localObject3, str1, str2, (Intent)localObject2, false, str3);
        return;
      case 0: 
        localObject1 = "EXTRA_NOTIFICATION";
        paramIntent = paramIntent.getStringExtra((String)localObject1);
        paramIntent = q.a(paramIntent);
        paramIntent = paramIntent.i();
        localObject1 = getApplicationContext();
        localObject3 = new com/truecaller/old/data/entity/Notification;
        ((Notification)localObject3).<init>(paramIntent);
        bl.a((Context)localObject1, (Notification)localObject3);
      }
      return;
    }
    catch (Exception localException)
    {
      d.a(localException, "BGServ - Exception");
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.NotificationsService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */