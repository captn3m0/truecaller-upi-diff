package com.truecaller.service;

import c.g.a.m;
import c.o.b;
import c.u;
import c.x;
import com.truecaller.utils.l;
import kotlinx.coroutines.ag;

final class MissedCallsNotificationService$d
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  MissedCallsNotificationService$d(MissedCallsNotificationService paramMissedCallsNotificationService, c.d.c paramc)
  {
    super(2, paramc);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    d locald = new com/truecaller/service/MissedCallsNotificationService$d;
    MissedCallsNotificationService localMissedCallsNotificationService = b;
    locald.<init>(localMissedCallsNotificationService, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return locald;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = c.d.a.a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        paramObject = b.getApplication();
        if (paramObject != null)
        {
          paramObject = (com.truecaller.common.b.a)paramObject;
          bool1 = ((com.truecaller.common.b.a)paramObject).o();
          if (!bool1) {
            return MissedCallsNotificationService.b.a;
          }
          boolean bool2 = ((com.truecaller.common.b.a)paramObject).p();
          if (bool2)
          {
            paramObject = b.e();
            bool2 = ((com.truecaller.i.c)paramObject).b("showMissedCallsNotifications");
            localObject = b.p;
            if (localObject == null)
            {
              String str = "permissionUtil";
              c.g.b.k.a(str);
            }
            bool1 = ((l)localObject).d();
            if ((bool2) && (bool1)) {
              return MissedCallsNotificationService.b.a;
            }
            if (bool2)
            {
              paramObject = b.e();
              localObject = "showMissedCallsNotificationPromo";
              boolean bool3 = true;
              bool2 = ((com.truecaller.i.c)paramObject).a((String)localObject, bool3);
              if (bool2)
              {
                b.e().b("showMissedCallsNotificationPromo", false);
                return MissedCallsNotificationService.b.b;
              }
            }
            return MissedCallsNotificationService.b.c;
          }
          return MissedCallsNotificationService.b.c;
        }
        paramObject = new c/u;
        ((u)paramObject).<init>("null cannot be cast to non-null type com.truecaller.common.app.ApplicationBase");
        throw ((Throwable)paramObject);
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (d)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((d)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.MissedCallsNotificationService.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */