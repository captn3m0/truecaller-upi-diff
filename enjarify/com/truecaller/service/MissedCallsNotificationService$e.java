package com.truecaller.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.aj;
import android.support.v4.app.z.d;
import android.support.v4.content.b;
import c.d.c;
import c.g.a.m;
import c.g.b.v.c;
import c.o.b;
import c.x;
import com.truecaller.androidactors.w;
import com.truecaller.callhistory.z;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.notifications.MissedCallsNotificationActionReceiver;
import com.truecaller.ui.NotificationAccessActivity;
import com.truecaller.ui.NotificationAccessActivity.a;
import com.truecaller.ui.TruecallerInit;
import com.truecaller.util.bs;
import com.truecaller.util.q;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ar;

final class MissedCallsNotificationService$e
  extends c.d.b.a.k
  implements m
{
  Object a;
  Object b;
  Object c;
  Object d;
  boolean e;
  int f;
  int g;
  int h;
  private ag j;
  
  MissedCallsNotificationService$e(MissedCallsNotificationService paramMissedCallsNotificationService, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    e locale = new com/truecaller/service/MissedCallsNotificationService$e;
    MissedCallsNotificationService localMissedCallsNotificationService = i;
    locale.<init>(localMissedCallsNotificationService, paramc);
    paramObject = (ag)paramObject;
    j = ((ag)paramObject);
    return locale;
  }
  
  public final Object a(Object paramObject)
  {
    e locale = this;
    Object localObject1 = paramObject;
    Object localObject14 = c.d.a.a.a;
    int k = h;
    int i3 = 4;
    int i4 = 1;
    boolean bool1;
    Object localObject16;
    Object localObject17;
    int i6;
    Object localObject18;
    int i9;
    Object localObject19;
    boolean bool5;
    Object localObject20;
    boolean bool6;
    switch (k)
    {
    default: 
      localObject1 = new java/lang/IllegalStateException;
      ((IllegalStateException)localObject1).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)localObject1);
    case 18: 
      localObject14 = (Throwable)d;
      bool1 = paramObject instanceof o.b;
      if (!bool1) {
        break label3816;
      }
      throw a;
    case 17: 
      localObject15 = (Throwable)d;
      localObject16 = (v.c)c;
      localObject17 = (z)b;
      i6 = e;
      localObject18 = (MissedCallsNotificationService)a;
      i9 = paramObject instanceof o.b;
      if (i9 == 0) {
        break label3700;
      }
      throw a;
    case 16: 
      boolean bool4 = paramObject instanceof o.b;
      if (bool4) {
        throw a;
      }
      break;
    case 15: 
      localObject15 = (v.c)c;
      localObject16 = (z)b;
      i4 = e;
      localObject19 = (MissedCallsNotificationService)a;
      bool5 = paramObject instanceof o.b;
      if (bool5) {
        throw a;
      }
      break;
    case 14: 
      localObject15 = (v.c)d;
      localObject16 = (v.c)c;
      localObject17 = (z)b;
      i6 = e;
      localObject18 = (MissedCallsNotificationService)a;
      i9 = paramObject instanceof o.b;
      if (i9 == 0)
      {
        i9 = i6;
        localObject20 = localObject18;
        localObject18 = localObject17;
      }
      break;
    case 13: 
    case 12: 
    case 11: 
    case 10: 
    case 9: 
    case 8: 
    case 7: 
    case 6: 
    case 5: 
      try
      {
        localObject1 = (o.b)paramObject;
        localObject1 = a;
        throw ((Throwable)localObject1);
      }
      finally
      {
        int m;
        boolean bool7;
        break label3577;
      }
      localObject15 = (v.c)d;
      localObject16 = (v.c)c;
      localObject17 = (z)b;
      i6 = e;
      localObject18 = (MissedCallsNotificationService)a;
      i9 = localObject1 instanceof o.b;
      if (i9 == 0)
      {
        i9 = i6;
        localObject20 = localObject18;
        localObject18 = localObject17;
      }
      else
      {
        localObject1 = (o.b)localObject1;
        localObject1 = a;
        throw ((Throwable)localObject1);
        localObject15 = (v.c)c;
        localObject16 = (z)b;
        i4 = e;
        localObject19 = (MissedCallsNotificationService)a;
        bool5 = localObject1 instanceof o.b;
        if (!bool5) {
          break label3356;
        }
        try
        {
          localObject1 = (o.b)localObject1;
          localObject1 = a;
          throw ((Throwable)localObject1);
        }
        finally
        {
          localObject18 = localObject19;
          i6 = i4;
          localObject17 = localObject16;
          localObject16 = localObject15;
          break label3577;
        }
        localObject14 = (x)d;
        bool1 = paramObject instanceof o.b;
        if (bool1)
        {
          throw a;
          localObject15 = (x)d;
          i3 = f;
          localObject17 = (v.c)c;
          localObject19 = (z)b;
          bool5 = e;
          localObject21 = (MissedCallsNotificationService)a;
          bool6 = paramObject instanceof o.b;
          if (!bool6)
          {
            localObject22 = localObject17;
            localObject17 = localObject15;
            localObject15 = localObject22;
          }
          else
          {
            throw a;
            m = f;
            localObject19 = (v.c)c;
            localObject18 = (z)b;
            i9 = e;
            localObject20 = (MissedCallsNotificationService)a;
            bool7 = paramObject instanceof o.b;
            if (!bool7)
            {
              localObject22 = localObject19;
              i6 = m;
              localObject15 = localObject19;
            }
            else
            {
              try
              {
                localObject3 = (o.b)paramObject;
                localObject3 = a;
                throw ((Throwable)localObject3);
              }
              finally
              {
                Object localObject3;
                localObject16 = localObject19;
              }
              m = f;
              localObject19 = (v.c)c;
              localObject18 = (z)b;
              i9 = e;
              localObject20 = (MissedCallsNotificationService)a;
              bool7 = localObject3 instanceof o.b;
              if (bool7)
              {
                localObject3 = (o.b)localObject3;
                localObject3 = a;
                throw ((Throwable)localObject3);
              }
            }
          }
        }
      }
      break;
    }
    label1645:
    label2062:
    int i8;
    label3215:
    label3334:
    label3356:
    int i16;
    for (;;)
    {
      localObject17 = localObject18;
      i6 = i9;
      localObject18 = localObject20;
      break;
      localObject14 = (x)d;
      boolean bool2 = paramObject instanceof o.b;
      Object localObject7;
      int i11;
      int i5;
      int i2;
      if (bool2)
      {
        throw a;
        localObject15 = (x)d;
        i3 = f;
        localObject17 = (v.c)c;
        localObject19 = (z)b;
        bool5 = e;
        localObject21 = (MissedCallsNotificationService)a;
        bool6 = paramObject instanceof o.b;
        if (bool6)
        {
          throw a;
          int n = f;
          localObject16 = (v.c)c;
          localObject17 = (z)b;
          i6 = e;
          localObject18 = (MissedCallsNotificationService)a;
          i9 = paramObject instanceof o.b;
          if (i9 == 0)
          {
            localObject21 = localObject18;
            bool5 = i6;
            localObject19 = localObject17;
            localObject17 = localObject16;
            i3 = n;
          }
          else
          {
            Object localObject5 = (o.b)paramObject;
            localObject5 = a;
            throw ((Throwable)localObject5);
            localObject14 = (x)d;
            int i1 = paramObject instanceof o.b;
            if (i1 != 0)
            {
              throw a;
              localObject15 = (x)d;
              localObject17 = (v.c)c;
              localObject19 = (z)b;
              bool5 = e;
              localObject21 = (MissedCallsNotificationService)a;
              bool6 = paramObject instanceof o.b;
              if (bool6)
              {
                throw a;
                i1 = e;
                localObject19 = (MissedCallsNotificationService)a;
                bool5 = paramObject instanceof o.b;
                if (!bool5)
                {
                  i9 = i1;
                  localObject20 = localObject19;
                }
                else
                {
                  throw a;
                  localObject15 = (MissedCallsNotificationService)a;
                  i6 = paramObject instanceof o.b;
                  if (i6 != 0)
                  {
                    throw a;
                    i1 = paramObject instanceof o.b;
                    if (i1 != 0) {
                      break label3823;
                    }
                    localObject15 = i;
                    localObject7 = ((MissedCallsNotificationService)localObject15).c();
                    localObject19 = new com/truecaller/service/MissedCallsNotificationService$e$a;
                    ((MissedCallsNotificationService.e.a)localObject19).<init>((MissedCallsNotificationService)localObject15, null);
                    localObject19 = (m)localObject19;
                    a = localObject15;
                    h = i4;
                    localObject7 = kotlinx.coroutines.g.a((c.d.f)localObject7, (m)localObject19, this);
                    if (localObject7 == localObject14) {
                      return localObject14;
                    }
                  }
                  localObject7 = (Boolean)localObject7;
                  int i14 = ((Boolean)localObject7).booleanValue();
                  if (i14 != 0) {
                    return x.a;
                  }
                  localObject19 = i.d().a();
                  c.g.b.k.a(localObject19, "historyManager.tell()");
                  localObject19 = ((com.truecaller.callhistory.a)localObject19).d();
                  localObject18 = "historyManager.tell().newMissedCalls";
                  c.g.b.k.a(localObject19, (String)localObject18);
                  a = localObject15;
                  e = i14;
                  i11 = 2;
                  h = i11;
                  localObject19 = bs.a((w)localObject19, locale);
                  if (localObject19 == localObject14) {
                    return localObject14;
                  }
                  i9 = i14;
                  localObject20 = localObject15;
                  localObject7 = localObject19;
                }
                localObject18 = localObject7;
                localObject18 = (z)localObject7;
                localObject19 = new c/g/b/v$c;
                ((v.c)localObject19).<init>();
                a = null;
                if (localObject18 != null) {
                  break label1645;
                }
                localObject7 = x.a;
                localObject15 = localObject18;
                q.a((Cursor)localObject18);
                localObject15 = i.d().a();
                c.g.b.k.a(localObject15, "historyManager.tell()");
                localObject15 = ((com.truecaller.callhistory.a)localObject15).e();
                localObject17 = "historyManager.tell().missedCallsCount";
                c.g.b.k.a(localObject15, (String)localObject17);
                a = localObject20;
                e = i9;
                b = localObject18;
                c = localObject19;
                d = localObject7;
                i5 = 3;
                h = i5;
                localObject15 = bs.a((w)localObject15, locale);
                if (localObject15 == localObject14) {
                  return localObject14;
                }
                localObject17 = localObject19;
                localObject19 = localObject18;
                i11 = i9;
                localObject21 = localObject20;
                localObject22 = localObject15;
                localObject15 = localObject7;
                localObject7 = localObject22;
              }
              localObject7 = (Integer)localObject7;
              if (localObject7 != null)
              {
                i15 = ((Integer)localObject7).intValue();
                localObject20 = i.c();
                localObject23 = new com/truecaller/service/MissedCallsNotificationService$e$1;
                ((MissedCallsNotificationService.e.1)localObject23).<init>((MissedCallsNotificationService)localObject21, (v.c)localObject17, i15, null);
                localObject23 = (m)localObject23;
                a = localObject21;
                e = i11;
                b = localObject19;
                c = localObject17;
                d = localObject15;
                f = i15;
                h = i3;
                localObject7 = kotlinx.coroutines.g.a((c.d.f)localObject20, (m)localObject23, locale);
                if (localObject7 == localObject14) {
                  return localObject14;
                }
                localObject14 = localObject15;
              }
            }
            else
            {
              return localObject14;
            }
            return x.a;
            i2 = ((z)localObject18).getCount();
            if (i2 != 0) {
              break label2062;
            }
            localObject7 = i;
            a = localObject20;
            e = i9;
            b = localObject18;
            c = localObject19;
            f = i2;
            i3 = 5;
            h = i3;
            localObject16 = j;
            if (localObject16 == null)
            {
              localObject17 = "uiCoroutineContext";
              c.g.b.k.a((String)localObject17);
            }
            localObject17 = new com/truecaller/service/MissedCallsNotificationService$c;
            ((MissedCallsNotificationService.c)localObject17).<init>((MissedCallsNotificationService)localObject7, null);
            localObject17 = (m)localObject17;
            localObject7 = kotlinx.coroutines.g.a((c.d.f)localObject16, (m)localObject17, locale);
            if (localObject7 == localObject14) {
              return localObject14;
            }
            i3 = i2;
            localObject17 = localObject19;
            localObject19 = localObject18;
            i11 = i9;
            localObject21 = localObject20;
          }
        }
      }
      try
      {
        localObject7 = x.a;
        localObject15 = localObject19;
        q.a((Cursor)localObject19);
        localObject15 = i.d().a();
        c.g.b.k.a(localObject15, "historyManager.tell()");
        localObject15 = ((com.truecaller.callhistory.a)localObject15).e();
        localObject20 = "historyManager.tell().missedCallsCount";
        c.g.b.k.a(localObject15, (String)localObject20);
        a = localObject21;
        e = i11;
        b = localObject19;
        c = localObject17;
        f = i3;
        d = localObject7;
        int i12 = 6;
        h = i12;
        localObject15 = bs.a((w)localObject15, locale);
        if (localObject15 == localObject14) {
          return localObject14;
        }
        localObject22 = localObject15;
        localObject15 = localObject7;
        localObject7 = localObject22;
        localObject7 = (Integer)localObject7;
        if (localObject7 != null)
        {
          i15 = ((Integer)localObject7).intValue();
          localObject20 = i.c();
          localObject23 = new com/truecaller/service/MissedCallsNotificationService$e$1;
          ((MissedCallsNotificationService.e.1)localObject23).<init>((MissedCallsNotificationService)localObject21, (v.c)localObject17, i15, null);
          localObject23 = (m)localObject23;
          a = localObject21;
          e = i11;
          b = localObject19;
          c = localObject17;
          f = i3;
          d = localObject15;
          g = i15;
          i15 = 7;
          h = i15;
          localObject7 = kotlinx.coroutines.g.a((c.d.f)localObject20, (m)localObject23, locale);
          if (localObject7 == localObject14) {
            return localObject14;
          }
          localObject14 = localObject15;
          return localObject14;
        }
        return x.a;
      }
      finally
      {
        localObject16 = localObject17;
        localObject17 = localObject19;
        i6 = i11;
        localObject18 = localObject21;
        break;
      }
      long l1 = 300L;
      a = localObject20;
      e = i9;
      b = localObject18;
      c = localObject19;
      f = i2;
      int i15 = 8;
      h = i15;
      Object localObject9 = ar.a(l1, locale);
      if (localObject9 == localObject14) {
        return localObject14;
      }
      localObject9 = i;
      a = localObject20;
      e = i9;
      b = localObject18;
      c = localObject19;
      f = i2;
      int i13 = 9;
      h = i13;
      Object localObject23 = j;
      if (localObject23 == null)
      {
        localObject24 = "uiCoroutineContext";
        c.g.b.k.a((String)localObject24);
      }
      Object localObject24 = new com/truecaller/service/MissedCallsNotificationService$d;
      ((MissedCallsNotificationService.d)localObject24).<init>((MissedCallsNotificationService)localObject9, null);
      localObject24 = (m)localObject24;
      localObject9 = kotlinx.coroutines.g.a((c.d.f)localObject23, (m)localObject24, locale);
      if (localObject9 == localObject14) {
        return localObject14;
      }
      localObject22 = localObject19;
      i6 = i2;
      localObject15 = localObject19;
      try
      {
        localObject9 = (MissedCallsNotificationService.b)localObject9;
        localObject23 = g.a;
        i15 = ((MissedCallsNotificationService.b)localObject9).ordinal();
        i15 = localObject23[i15];
        switch (i15)
        {
        default: 
          if (i6 <= i5) {
            break label3215;
          }
          localObject9 = i;
          break;
        case 2: 
          localObject9 = i;
          localObject23 = i;
          localObject23 = ((MissedCallsNotificationService)localObject23).getApplicationContext();
          localObject24 = "applicationContext";
          c.g.b.k.a(localObject23, (String)localObject24);
          a = localObject20;
          e = i9;
          b = localObject18;
          c = localObject15;
          f = i6;
          int i7 = 12;
          h = i7;
          long l2 = 0L;
          for (;;)
          {
            boolean bool3 = ((z)localObject18).moveToNext();
            if (!bool3) {
              break;
            }
            localObject19 = ((z)localObject18).d();
            if (localObject19 != null)
            {
              localObject25 = "missedCalls.historyEvent ?: continue";
              c.g.b.k.a(localObject19, (String)localObject25);
              long l3 = ((HistoryEvent)localObject19).j();
              l2 = Math.max(l2, l3);
            }
          }
          localObject19 = ((MissedCallsNotificationService)localObject9).f();
          localObject16 = ((z.d)localObject19).c(i3);
          i8 = 2131100594;
          i8 = b.c((Context)localObject23, i8);
          localObject16 = ((z.d)localObject16).f(i8);
          i8 = 2131234322;
          localObject16 = ((z.d)localObject16).a(i8);
          localObject16 = ((z.d)localObject16).e();
          ((z.d)localObject16).a(l2);
          localObject19 = new android/content/Intent;
          Object localObject25 = MissedCallsNotificationActionReceiver.class;
          ((Intent)localObject19).<init>((Context)localObject23, (Class)localObject25);
          localObject25 = "com.truecaller.CLEAR_ALTERNATIVE_MISSED_CALLS";
          localObject19 = ((Intent)localObject19).setAction((String)localObject25);
          localObject25 = "lastTimestamp";
          localObject19 = ((Intent)localObject19).putExtra((String)localObject25, l2);
          int i17 = 2131364147;
          int i18 = 268435456;
          localObject19 = PendingIntent.getBroadcast((Context)localObject23, i17, (Intent)localObject19, i18);
          ((z.d)localObject16).b((PendingIntent)localObject19);
          localObject19 = "builder";
          c.g.b.k.a(localObject16, (String)localObject19);
          ((z.d)localObject16).e(i5);
          i5 = 2131888369;
          localObject17 = ((MissedCallsNotificationService)localObject9).getString(i5);
          localObject17 = (CharSequence)localObject17;
          ((z.d)localObject16).a((CharSequence)localObject17);
          i5 = 2131888368;
          localObject17 = ((MissedCallsNotificationService)localObject9).getString(i5);
          localObject17 = (CharSequence)localObject17;
          ((z.d)localObject16).b((CharSequence)localObject17);
          localObject17 = "calls";
          localObject19 = "notification";
          localObject17 = TruecallerInit.a((Context)localObject23, (String)localObject17, (String)localObject19);
          localObject19 = NotificationAccessActivity.a;
          localObject17 = NotificationAccessActivity.a.a((Context)localObject23, (Intent)localObject17);
          localObject19 = aj.a((Context)localObject23);
          localObject17 = ((aj)localObject19).a((Intent)localObject17);
          localObject19 = "TaskStackBuilder.create(…tionAccessActivityIntent)";
          c.g.b.k.a(localObject17, (String)localObject19);
          i8 = 2131364150;
          localObject17 = ((aj)localObject17).a(i8, i18);
          i8 = 0;
          localObject19 = null;
          i13 = 2131888374;
          localObject23 = ((MissedCallsNotificationService)localObject9).getString(i13);
          localObject23 = (CharSequence)localObject23;
          ((z.d)localObject16).a(0, (CharSequence)localObject23, (PendingIntent)localObject17);
          ((z.d)localObject16).a((PendingIntent)localObject17);
          localObject17 = ((MissedCallsNotificationService)localObject9).getResources();
          i8 = 2131689472;
          localObject17 = BitmapFactory.decodeResource((Resources)localObject17, i8);
          ((z.d)localObject16).a((Bitmap)localObject17);
          localObject16 = ((z.d)localObject16).h();
          localObject17 = "builder.build()";
          c.g.b.k.a(localObject16, (String)localObject17);
          localObject17 = "notificationMissedCallPromo";
          localObject9 = ((MissedCallsNotificationService)localObject9).a((Notification)localObject16, (String)localObject17, locale);
          if (localObject9 == localObject14) {
            return localObject14;
          }
          localObject16 = localObject18;
          i5 = i9;
          localObject19 = localObject20;
          break;
        case 1: 
          localObject9 = x.a;
          localObject16 = localObject18;
          q.a((Cursor)localObject18);
          localObject16 = i.d().a();
          c.g.b.k.a(localObject16, "historyManager.tell()");
          localObject16 = ((com.truecaller.callhistory.a)localObject16).e();
          localObject17 = "historyManager.tell().missedCallsCount";
          c.g.b.k.a(localObject16, (String)localObject17);
          a = localObject20;
          e = i9;
          b = localObject18;
          c = localObject15;
          f = i8;
          d = localObject9;
          i5 = 10;
          h = i5;
          localObject16 = bs.a((w)localObject16, locale);
          if (localObject16 == localObject14) {
            return localObject14;
          }
          localObject17 = localObject9;
          localObject9 = localObject16;
          i3 = i8;
          localObject19 = localObject18;
          i11 = i9;
          localObject21 = localObject20;
          localObject9 = (Integer)localObject9;
          if (localObject9 != null)
          {
            i15 = ((Integer)localObject9).intValue();
            localObject20 = i.c();
            localObject23 = new com/truecaller/service/MissedCallsNotificationService$e$1;
            ((MissedCallsNotificationService.e.1)localObject23).<init>((MissedCallsNotificationService)localObject21, (v.c)localObject15, i15, null);
            localObject23 = (m)localObject23;
            a = localObject21;
            e = i11;
            b = localObject19;
            c = localObject15;
            f = i3;
            d = localObject17;
            g = i15;
            i15 = 11;
            h = i15;
            localObject9 = kotlinx.coroutines.g.a((c.d.f)localObject20, (m)localObject23, locale);
            if (localObject9 == localObject14) {
              return localObject14;
            }
            return localObject14;
          }
          return x.a;
        }
        a = localObject20;
        e = i9;
        b = localObject18;
        c = localObject15;
        f = i8;
        d = localObject15;
        i3 = 13;
        h = i3;
        localObject9 = ((MissedCallsNotificationService)localObject9).a((z)localObject18, locale);
        if (localObject9 == localObject14) {
          return localObject14;
        }
        localObject16 = localObject15;
        try
        {
          localObject9 = (Notification)localObject9;
          break label3334;
        }
        finally {}
        continue;
        if (i8 == i5)
        {
          bool8 = ((z)localObject18).moveToFirst();
          if (bool8)
          {
            localObject11 = i;
            localObject16 = ((z)localObject18).d();
            a = localObject20;
            e = i9;
            b = localObject18;
            c = localObject15;
            f = i8;
            d = localObject15;
            i5 = 14;
            h = i5;
            localObject11 = ((MissedCallsNotificationService)localObject11).a((HistoryEvent)localObject16, locale);
            if (localObject11 == localObject14) {
              return localObject14;
            }
            localObject16 = localObject15;
            localObject11 = (Notification)localObject11;
            break label3334;
          }
        }
        localObject16 = localObject15;
        boolean bool8 = false;
        Object localObject11 = null;
        a = localObject11;
        localObject15 = localObject16;
        localObject16 = localObject18;
        i5 = i9;
        localObject19 = localObject20;
        localObject11 = localObject16;
        q.a((Cursor)localObject16);
        localObject11 = i.d().a();
        c.g.b.k.a(localObject11, "historyManager.tell()");
        localObject11 = ((com.truecaller.callhistory.a)localObject11).e();
        localObject18 = "historyManager.tell().missedCallsCount";
        c.g.b.k.a(localObject11, (String)localObject18);
        a = localObject19;
        e = i5;
        b = localObject16;
        c = localObject15;
        i11 = 15;
        h = i11;
        localObject11 = bs.a((w)localObject11, locale);
        if (localObject11 == localObject14) {
          return localObject14;
        }
        localObject11 = (Integer)localObject11;
        if (localObject11 != null)
        {
          i16 = ((Integer)localObject11).intValue();
          localObject18 = i.c();
          localObject21 = new com/truecaller/service/MissedCallsNotificationService$e$1;
          ((MissedCallsNotificationService.e.1)localObject21).<init>((MissedCallsNotificationService)localObject19, (v.c)localObject15, i16, null);
          localObject21 = (m)localObject21;
          a = localObject19;
          e = i5;
          b = localObject16;
          c = localObject15;
          f = i16;
          i16 = 16;
          h = i16;
          localObject11 = kotlinx.coroutines.g.a((c.d.f)localObject18, (m)localObject21, locale);
          if (localObject11 == localObject14) {
            return localObject14;
          }
          return x.a;
        }
        return x.a;
      }
      finally
      {
        localObject16 = localObject15;
      }
    }
    label3577:
    Object localObject15 = localObject17;
    q.a((Cursor)localObject17);
    localObject15 = i.d().a();
    c.g.b.k.a(localObject15, "historyManager.tell()");
    localObject15 = ((com.truecaller.callhistory.a)localObject15).e();
    Object localObject21 = "historyManager.tell().missedCallsCount";
    c.g.b.k.a(localObject15, (String)localObject21);
    a = localObject18;
    e = i8;
    b = localObject17;
    c = localObject16;
    d = localObject12;
    int i10 = 17;
    h = i10;
    localObject15 = bs.a((w)localObject15, locale);
    if (localObject15 == localObject14) {
      return localObject14;
    }
    Object localObject22 = localObject15;
    localObject15 = localObject12;
    Object localObject13 = localObject22;
    label3700:
    localObject13 = (Integer)localObject13;
    if (localObject13 != null)
    {
      i16 = ((Integer)localObject13).intValue();
      localObject21 = i.c();
      localObject20 = new com/truecaller/service/MissedCallsNotificationService$e$1;
      ((MissedCallsNotificationService.e.1)localObject20).<init>((MissedCallsNotificationService)localObject18, (v.c)localObject16, i16, null);
      localObject20 = (m)localObject20;
      a = localObject18;
      e = i8;
      b = localObject17;
      c = localObject16;
      d = localObject15;
      f = i16;
      i16 = 18;
      h = i16;
      localObject13 = kotlinx.coroutines.g.a((c.d.f)localObject21, (m)localObject20, locale);
      if (localObject13 == localObject14) {
        return localObject14;
      }
      localObject14 = localObject15;
      label3816:
      throw ((Throwable)localObject14);
    }
    return x.a;
    label3823:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (e)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((e)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.MissedCallsNotificationService.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */