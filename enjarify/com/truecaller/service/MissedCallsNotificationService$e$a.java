package com.truecaller.service;

import android.content.Context;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.old.data.access.Settings;
import com.truecaller.util.f;
import kotlinx.coroutines.ag;

final class MissedCallsNotificationService$e$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  MissedCallsNotificationService$e$a(MissedCallsNotificationService paramMissedCallsNotificationService, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/service/MissedCallsNotificationService$e$a;
    MissedCallsNotificationService localMissedCallsNotificationService = b;
    locala.<init>(localMissedCallsNotificationService, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        boolean bool2 = Settings.e();
        bool1 = false;
        if (bool2)
        {
          paramObject = (Context)b;
          f.a((Context)paramObject, 0);
          bool1 = true;
        }
        return Boolean.valueOf(bool1);
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.MissedCallsNotificationService.e.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */