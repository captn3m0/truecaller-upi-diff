package com.truecaller.service;

import android.content.Intent;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import com.truecaller.calling.ak;
import com.truecaller.log.AssertionUtil;

final class CallStateService$a
  extends PhoneStateListener
{
  private CallStateService$a(CallStateService paramCallStateService) {}
  
  public final void onCallStateChanged(int paramInt, String paramString)
  {
    super.onCallStateChanged(paramInt, paramString);
    boolean bool = com.truecaller.common.h.am.d(paramString);
    if (bool) {
      return;
    }
    Object localObject1 = a;
    bool = CallStateService.a((CallStateService)localObject1);
    String str1 = null;
    if (!bool)
    {
      localObject1 = a;
      bool = CallStateService.b((CallStateService)localObject1);
      if (bool)
      {
        CallStateService.c(a);
        CallStateService.a(a, 0);
        localObject1 = a;
        int i = 32;
        CallStateService.a((CallStateService)localObject1, i);
      }
    }
    bool = true;
    localObject1 = new String[bool];
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("CallStateService received call state change to ");
    ((StringBuilder)localObject2).append(paramInt);
    ((StringBuilder)localObject2).append(" for ");
    ((StringBuilder)localObject2).append(paramString);
    String str2 = " at ";
    ((StringBuilder)localObject2).append(str2);
    long l = System.currentTimeMillis();
    ((StringBuilder)localObject2).append(l);
    localObject2 = ((StringBuilder)localObject2).toString();
    localObject1[0] = localObject2;
    localObject1 = new android/content/Intent;
    str1 = "android.intent.action.PHONE_STATE";
    ((Intent)localObject1).<init>(str1);
    switch (paramInt)
    {
    default: 
      localObject3 = String.valueOf(paramInt);
      AssertionUtil.reportWeirdnessButNeverCrash("Unknown phone state: ".concat((String)localObject3));
      return;
    case 2: 
      localObject3 = TelephonyManager.EXTRA_STATE_OFFHOOK;
      break;
    case 1: 
      localObject3 = TelephonyManager.EXTRA_STATE_RINGING;
      break;
    case 0: 
      localObject3 = TelephonyManager.EXTRA_STATE_IDLE;
    }
    ((Intent)localObject1).putExtra("state", (String)localObject3);
    ((Intent)localObject1).putExtra("incoming_number", paramString);
    Object localObject3 = com.truecaller.calling.am.a(a);
    paramString = a;
    ((ak)localObject3).a(paramString, (Intent)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.CallStateService.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */