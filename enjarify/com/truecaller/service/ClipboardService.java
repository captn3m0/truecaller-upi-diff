package com.truecaller.service;

import android.app.Service;
import android.content.ClipboardManager;
import android.content.ClipboardManager.OnPrimaryClipChangedListener;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.IBinder;
import android.os.Message;
import android.util.DisplayMetrics;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.bo;
import com.truecaller.bo.a;
import com.truecaller.bp;
import com.truecaller.data.entity.Contact;
import com.truecaller.filters.g;
import com.truecaller.ui.components.FloatingWindow;
import com.truecaller.ui.components.e;
import com.truecaller.util.at;
import java.util.concurrent.CancellationException;
import java.util.concurrent.atomic.AtomicInteger;
import kotlinx.coroutines.bn;

public class ClipboardService
  extends Service
  implements bo.a
{
  public bo a;
  e b;
  private final AtomicInteger c;
  private Configuration d;
  private Handler e;
  
  public ClipboardService()
  {
    AtomicInteger localAtomicInteger = new java/util/concurrent/atomic/AtomicInteger;
    localAtomicInteger.<init>(0);
    c = localAtomicInteger;
  }
  
  final e a()
  {
    e locale = b;
    if (locale == null)
    {
      locale = new com/truecaller/ui/components/e;
      locale.<init>(this);
      b = locale;
    }
    return b;
  }
  
  public final Object a(String paramString)
  {
    Integer localInteger = Integer.valueOf(c.incrementAndGet());
    Message localMessage = e.obtainMessage(3, 0, 0, localInteger);
    localMessage.getData().putString("number", paramString);
    e.sendMessageDelayed(localMessage, 100);
    return localInteger;
  }
  
  public final void a(Object paramObject)
  {
    b(paramObject);
  }
  
  public final void a(String paramString, Contact paramContact, g paramg)
  {
    Object localObject = e;
    int i = 1;
    ((Handler)localObject).removeMessages(i);
    e.sendEmptyMessage(0);
    localObject = e.obtainMessage(i);
    ClipboardService.a.a locala = new com/truecaller/service/ClipboardService$a$a;
    locala.<init>(paramString, paramContact, paramg);
    obj = locala;
    e.sendMessage((Message)localObject);
  }
  
  final void b(Object paramObject)
  {
    e.removeCallbacksAndMessages(paramObject);
    Handler localHandler = e;
    paramObject = localHandler.obtainMessage(4, 0, 0, paramObject);
    localHandler.sendMessage((Message)paramObject);
  }
  
  public IBinder onBind(Intent paramIntent)
  {
    return null;
  }
  
  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
    Object localObject = d;
    int i = ((Configuration)localObject).updateFrom(paramConfiguration);
    boolean bool = Configuration.needNewResources(i, 4);
    int j;
    if (bool)
    {
      paramConfiguration = e;
      j = 2;
      paramConfiguration.removeMessages(j);
      e.sendEmptyMessage(j);
      return;
    }
    i &= 0x80;
    if (i != 0)
    {
      paramConfiguration = b;
      if (paramConfiguration != null)
      {
        localObject = d.getResources().getDisplayMetrics();
        int k = widthPixels;
        h = k;
        j = heightPixels;
        Resources localResources = d.getResources();
        k = at.a(localResources);
        j -= k;
        i = j;
      }
    }
  }
  
  public void onCreate()
  {
    super.onCreate();
    Object localObject1 = new android/content/res/Configuration;
    Object localObject2 = getResources().getConfiguration();
    ((Configuration)localObject1).<init>((Configuration)localObject2);
    d = ((Configuration)localObject1);
    localObject1 = new android/os/Handler;
    localObject2 = new com/truecaller/service/ClipboardService$a;
    ((ClipboardService.a)localObject2).<init>(this);
    ((Handler)localObject1).<init>((Handler.Callback)localObject2);
    e = ((Handler)localObject1);
    TrueApp.y().a().cB().a(this);
    localObject1 = a;
    k.b(this, "searchListener");
    a = this;
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    Object localObject1 = a;
    if (localObject1 != null)
    {
      Object localObject2 = b;
      if (localObject2 != null)
      {
        Object localObject3 = new java/util/concurrent/CancellationException;
        String str = "SearchOnCopyHelper destroyed";
        ((CancellationException)localObject3).<init>(str);
        localObject3 = (Throwable)localObject3;
        ((bn)localObject2).c((Throwable)localObject3);
      }
      localObject2 = c;
      localObject1 = (ClipboardManager.OnPrimaryClipChangedListener)localObject1;
      ((ClipboardManager)localObject2).removePrimaryClipChangedListener((ClipboardManager.OnPrimaryClipChangedListener)localObject1);
      localObject1 = null;
      a = null;
    }
  }
  
  public int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
  {
    return 1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.ClipboardService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */