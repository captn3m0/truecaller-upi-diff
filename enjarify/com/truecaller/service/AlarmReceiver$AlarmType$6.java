package com.truecaller.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.common.f.c;
import com.truecaller.filters.blockedevents.BlockedEventsActivity;
import com.truecaller.filters.p;

 enum AlarmReceiver$AlarmType$6
{
  AlarmReceiver$AlarmType$6(String paramString2)
  {
    super(paramString, 5, 1209600000L, 1209600000L, 2131364139, paramString1, paramString2, null);
  }
  
  public final Notification getNotification(Context paramContext)
  {
    Object localObject1 = TrueApp.y();
    boolean bool = ((TrueApp)localObject1).p();
    if (bool)
    {
      Object localObject2 = ((TrueApp)localObject1).a().ai();
      bool = ((c)localObject2).d();
      if (!bool)
      {
        ((TrueApp)localObject1).a().R().h(true);
        localObject1 = new android/content/Intent;
        ((Intent)localObject1).<init>(paramContext, BlockedEventsActivity.class);
        localObject1 = PendingIntent.getActivity(paramContext, 2131364137, (Intent)localObject1, 134217728);
        localObject2 = paramContext.getString(2131886117);
        String str = paramContext.getString(2131886642);
        return AlarmReceiver.AlarmType.access$100(paramContext, (String)localObject2, str, (PendingIntent)localObject1);
      }
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.AlarmReceiver.AlarmType.6
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */