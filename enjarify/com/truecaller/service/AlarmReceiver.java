package com.truecaller.service;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.support.v4.app.ac;
import com.truecaller.TrueApp;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e.a;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.common.h.ai;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import com.truecaller.log.d;
import com.truecaller.old.data.access.Settings;
import com.truecaller.ui.NotificationAccessActivity;
import com.truecaller.ui.TruecallerInit;
import com.truecaller.util.NotificationUtil;
import java.util.Calendar;

public class AlarmReceiver
  extends BroadcastReceiver
{
  private static final AlarmReceiver.AlarmType[] a;
  private static final AlarmReceiver.AlarmType[] b;
  private static final AlarmReceiver.AlarmType[] c;
  private static final AlarmReceiver.AlarmType[] d;
  private static PackageInfo e;
  
  static
  {
    AlarmReceiver.AlarmType[] arrayOfAlarmType = new AlarmReceiver.AlarmType[6];
    AlarmReceiver.AlarmType localAlarmType = AlarmReceiver.AlarmType.TYPE_15DAYS;
    arrayOfAlarmType[0] = localAlarmType;
    localAlarmType = AlarmReceiver.AlarmType.TYPE_20DAYS;
    int i = 1;
    arrayOfAlarmType[i] = localAlarmType;
    localAlarmType = AlarmReceiver.AlarmType.TYPE_RESCHEDULE;
    int j = 2;
    arrayOfAlarmType[j] = localAlarmType;
    localAlarmType = AlarmReceiver.AlarmType.TYPE_UPDATE_SPAM;
    int k = 3;
    arrayOfAlarmType[k] = localAlarmType;
    localAlarmType = AlarmReceiver.AlarmType.TYPE_NOTIFICATION_ACCESS;
    int m = 4;
    arrayOfAlarmType[m] = localAlarmType;
    localAlarmType = AlarmReceiver.AlarmType.TYPE_DISMISS_NOTIFICATION;
    int n = 5;
    arrayOfAlarmType[n] = localAlarmType;
    a = arrayOfAlarmType;
    arrayOfAlarmType = new AlarmReceiver.AlarmType[j];
    localAlarmType = AlarmReceiver.AlarmType.TYPE_15DAYS;
    arrayOfAlarmType[0] = localAlarmType;
    localAlarmType = AlarmReceiver.AlarmType.TYPE_RESCHEDULE;
    arrayOfAlarmType[i] = localAlarmType;
    b = arrayOfAlarmType;
    arrayOfAlarmType = new AlarmReceiver.AlarmType[n];
    localAlarmType = AlarmReceiver.AlarmType.TYPE_2DAYS_UPGRADED;
    arrayOfAlarmType[0] = localAlarmType;
    localAlarmType = AlarmReceiver.AlarmType.TYPE_5DAYS;
    arrayOfAlarmType[i] = localAlarmType;
    localAlarmType = AlarmReceiver.AlarmType.TYPE_UPDATE_SPAM;
    arrayOfAlarmType[j] = localAlarmType;
    localAlarmType = AlarmReceiver.AlarmType.TYPE_NOTIFICATION_ACCESS;
    arrayOfAlarmType[k] = localAlarmType;
    localAlarmType = AlarmReceiver.AlarmType.TYPE_DISMISS_NOTIFICATION;
    arrayOfAlarmType[m] = localAlarmType;
    c = arrayOfAlarmType;
    arrayOfAlarmType = new AlarmReceiver.AlarmType[i];
    localAlarmType = AlarmReceiver.AlarmType.TYPE_DO_NOT_DISTURB_ACCESS;
    arrayOfAlarmType[0] = localAlarmType;
    d = arrayOfAlarmType;
  }
  
  public static void a(Context paramContext)
  {
    AlarmReceiver.AlarmType[] arrayOfAlarmType = b;
    int i = arrayOfAlarmType.length;
    int j = 0;
    while (j < i)
    {
      String str = arrayOfAlarmType[j].name();
      long l = 0L;
      Settings.d(str, l);
      j += 1;
    }
    a(paramContext, false);
  }
  
  private static void a(Context paramContext, AlarmReceiver.AlarmType paramAlarmType)
  {
    String str1 = paramAlarmType.name();
    long l1 = Settings.l(str1);
    long l2 = 0L;
    boolean bool1 = l1 < l2;
    if (!bool1)
    {
      l1 = System.currentTimeMillis();
      String str2 = paramAlarmType.name();
      bool1 = Settings.k(str2);
      if (bool1)
      {
        long l3 = paramAlarmType.getRecurringPeriod();
        boolean bool2 = l3 < l2;
        if (bool2)
        {
          l2 = paramAlarmType.getRecurringPeriod();
          l1 += l2;
          break label89;
        }
      }
      l2 = paramAlarmType.getFirstDelay();
      l1 += l2;
    }
    label89:
    Object localObject = (AlarmManager)paramContext.getSystemService("alarm");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, AlarmReceiver.class);
    String str3 = paramAlarmType.name();
    localIntent = localIntent.putExtra("notification_type", str3);
    int i = paramAlarmType.getNotificationId();
    paramContext = PendingIntent.getBroadcast(paramContext, i, localIntent, 0);
    ((AlarmManager)localObject).set(0, l1, paramContext);
    Settings.d(paramAlarmType.name(), l1);
    paramContext = new String[1];
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("Scheduled alarm ");
    paramAlarmType = paramAlarmType.name();
    ((StringBuilder)localObject).append(paramAlarmType);
    ((StringBuilder)localObject).append(" for ");
    long l4 = System.currentTimeMillis();
    l1 = (l1 - l4) / 1000L;
    ((StringBuilder)localObject).append(l1);
    ((StringBuilder)localObject).append(" seconds from now");
    paramAlarmType = ((StringBuilder)localObject).toString();
    paramContext[0] = paramAlarmType;
  }
  
  public static void a(Context paramContext, boolean paramBoolean)
  {
    Object localObject1 = (com.truecaller.common.b.a)paramContext.getApplicationContext();
    boolean bool1 = ((com.truecaller.common.b.a)localObject1).p();
    if (!bool1) {
      return;
    }
    bool1 = true;
    Object localObject2 = new String[bool1];
    String str1 = String.valueOf(paramBoolean);
    Object localObject3 = "wasBooted = ".concat(str1);
    str1 = null;
    localObject2[0] = localObject3;
    d(paramContext);
    localObject2 = e;
    long l1 = firstInstallTime;
    PackageInfo localPackageInfo = e;
    long l2 = lastUpdateTime;
    boolean bool2 = l1 < l2;
    int i;
    if (bool2)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject2 = null;
    }
    if (i != 0)
    {
      localObject2 = c;
      localObject3 = a;
      int j = localObject3.length;
      int k = 0;
      while (k < j)
      {
        String str2 = localObject3[k];
        Object localObject4 = str2.name();
        long l3 = Settings.l((String)localObject4);
        long l4 = 0L;
        boolean bool3 = l3 < l4;
        if (bool3)
        {
          Settings.d(str2.name(), l4);
          localObject4 = (AlarmManager)paramContext.getSystemService("alarm");
          int m = str2.getNotificationId();
          Object localObject5 = new android/content/Intent;
          Class localClass = AlarmReceiver.class;
          ((Intent)localObject5).<init>(paramContext, localClass);
          Object localObject6 = PendingIntent.getBroadcast(paramContext, m, (Intent)localObject5, 0);
          ((AlarmManager)localObject4).cancel((PendingIntent)localObject6);
          localObject4 = new String[bool1];
          localObject6 = new java/lang/StringBuilder;
          localObject5 = "Canceled alarm ";
          ((StringBuilder)localObject6).<init>((String)localObject5);
          str2 = str2.name();
          ((StringBuilder)localObject6).append(str2);
          str2 = ((StringBuilder)localObject6).toString();
          localObject4[0] = str2;
        }
        k += 1;
      }
    }
    localObject2 = a;
    if (paramBoolean)
    {
      localObject1 = d;
      localObject1 = org.c.a.a.a.a.a((Object[])localObject2, (Object[])localObject1);
      localObject2 = localObject1;
      localObject2 = (AlarmReceiver.AlarmType[])localObject1;
    }
    a(paramContext, paramBoolean, (AlarmReceiver.AlarmType[])localObject2);
  }
  
  private static void a(Context paramContext, boolean paramBoolean, AlarmReceiver.AlarmType[] paramArrayOfAlarmType)
  {
    int i = paramArrayOfAlarmType.length;
    int j = 0;
    while (j < i)
    {
      AlarmReceiver.AlarmType localAlarmType = paramArrayOfAlarmType[j];
      String str1 = localAlarmType.name();
      long l1 = Settings.l(str1);
      String str2 = localAlarmType.name();
      boolean bool = Settings.k(str2);
      long l2 = 0L;
      if (bool)
      {
        long l3 = localAlarmType.getRecurringPeriod();
        bool = l3 < l2;
        if (!bool) {}
      }
      else
      {
        if (paramBoolean)
        {
          bool = l1 < l2;
          if (bool) {}
        }
        else
        {
          bool = l1 < l2;
          if (bool) {
            break label107;
          }
        }
        a(paramContext, localAlarmType);
      }
      label107:
      j += 1;
    }
  }
  
  public static void a(Intent paramIntent)
  {
    String str = "notification_type";
    boolean bool = paramIntent.hasExtra(str);
    if (bool)
    {
      str = "notification_type";
      paramIntent.removeExtra(str);
    }
  }
  
  private static long c(Context paramContext)
  {
    d(paramContext);
    return efirstInstallTime;
  }
  
  private static void d(Context paramContext)
  {
    Object localObject = e;
    if (localObject == null) {
      try
      {
        localObject = paramContext.getPackageManager();
        paramContext = paramContext.getPackageName();
        paramContext = ((PackageManager)localObject).getPackageInfo(paramContext, 0);
        e = paramContext;
        return;
      }
      catch (PackageManager.NameNotFoundException localNameNotFoundException) {}
    }
  }
  
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    boolean bool1 = true;
    Object localObject1 = new String[bool1];
    String str1 = String.valueOf(paramIntent);
    Object localObject2 = "AlarmReceiver received intent ".concat(str1);
    str1 = null;
    localObject1[0] = localObject2;
    localObject1 = paramIntent.getAction();
    int i;
    int j;
    int k;
    Object localObject3;
    label174:
    Object localObject4;
    if (localObject1 != null)
    {
      localObject1 = paramIntent.getAction();
      i = -1;
      j = ((String)localObject1).hashCode();
      k = 170717771;
      boolean bool2;
      if (j != k)
      {
        k = 1370363729;
        if (j != k)
        {
          k = 1763328585;
          if (j == k)
          {
            localObject3 = "com.truecaller.intent.action.PROMO_CLICKED";
            bool2 = ((String)localObject1).equals(localObject3);
            if (bool2)
            {
              bool2 = true;
              break label174;
            }
          }
        }
        else
        {
          localObject3 = "com.truecaller.intent.action.SHARE";
          bool2 = ((String)localObject1).equals(localObject3);
          if (bool2)
          {
            bool2 = false;
            localObject1 = null;
            break label174;
          }
        }
      }
      else
      {
        localObject3 = "com.truecaller.intent.action.PROMO_DISMISSED";
        bool2 = ((String)localObject1).equals(localObject3);
        if (bool2)
        {
          m = 2;
          break label174;
        }
      }
      int m = -1;
      switch (m)
      {
      default: 
        break;
      case 2: 
        paramContext = ac.a(paramContext);
        int i1 = AlarmReceiver.AlarmType.TYPE_NOTIFICATION_ACCESS.getNotificationId();
        paramContext.a(null, i1);
        return;
      case 1: 
        paramIntent.setAction("android.intent.action.MAIN");
        paramIntent = TruecallerInit.a(paramContext, "calls", "notification");
        paramIntent = NotificationAccessActivity.a(paramContext, paramIntent);
        paramContext.startActivity(paramIntent);
        return;
      case 0: 
        paramIntent = paramContext.getString(2131886653);
        localObject4 = paramContext.getString(2131887144);
        localObject1 = paramContext.getString(2131887143);
        paramIntent = ai.a(paramIntent, (String)localObject4, (CharSequence)localObject1).addFlags(268435456);
        paramContext.startActivity(paramIntent);
        paramContext = TrueApp.y().a().c();
        paramIntent = new com/truecaller/analytics/e$a;
        paramIntent.<init>("ViewAction");
        paramIntent = paramIntent.a("Context", "notification").a("Action", "share").a();
        paramContext.a(paramIntent);
        return;
      }
    }
    localObject1 = "notification_type";
    boolean bool3 = paramIntent.hasExtra((String)localObject1);
    if (bool3)
    {
      localObject1 = paramIntent.getStringExtra("notification_type");
      try
      {
        localObject1 = AlarmReceiver.AlarmType.valueOf((String)localObject1);
        localObject4 = new String[bool1];
        localObject2 = new java/lang/StringBuilder;
        localObject3 = "Alarm type: ";
        ((StringBuilder)localObject2).<init>((String)localObject3);
        localObject3 = ((AlarmReceiver.AlarmType)localObject1).name();
        ((StringBuilder)localObject2).append((String)localObject3);
        localObject2 = ((StringBuilder)localObject2).toString();
        localObject4[0] = localObject2;
        bool1 = NotificationUtil.a();
        if (!bool1)
        {
          localObject4 = paramContext.getApplicationContext();
          int n = ((AlarmReceiver.AlarmType)localObject1).getNotificationId();
          i = 134217728;
          paramIntent = PendingIntent.getBroadcast((Context)localObject4, n, paramIntent, i);
          localObject4 = "alarm";
          paramContext = paramContext.getSystemService((String)localObject4);
          paramContext = (AlarmManager)paramContext;
          localObject4 = Calendar.getInstance();
          n = 11;
          i = ((Calendar)localObject4).get(n);
          j = 9;
          if (i >= j)
          {
            j = 21;
            if (i <= j)
            {
              l1 = ((Calendar)localObject4).getTimeInMillis();
              break label565;
            }
          }
          i = 12;
          ((Calendar)localObject4).add(n, i);
          long l1 = ((Calendar)localObject4).getTimeInMillis();
          label565:
          paramContext.set(0, l1, paramIntent);
          return;
        }
        paramIntent = ((AlarmReceiver.AlarmType)localObject1).name();
        long l2 = 0L;
        Settings.d(paramIntent, l2);
        paramIntent = ((AlarmReceiver.AlarmType)localObject1).name();
        Settings.m(paramIntent);
        Notification localNotification = ((AlarmReceiver.AlarmType)localObject1).getNotification(paramContext);
        if (localNotification != null)
        {
          boolean bool4 = ((AlarmReceiver.AlarmType)localObject1).shouldShow(paramContext);
          if (bool4)
          {
            paramIntent = ((AlarmReceiver.AlarmType)localObject1).getAnalyticsSubtype();
            if (paramIntent == null)
            {
              paramContext = "Notification must specify analytics subtype";
              paramContext = new String[] { paramContext };
              AssertionUtil.OnlyInDebug.fail(paramContext);
              return;
            }
            Bundle localBundle = new android/os/Bundle;
            localBundle.<init>();
            localObject4 = "Subtype";
            localBundle.putString((String)localObject4, paramIntent);
            paramIntent = paramContext.getApplicationContext();
            paramIntent = (bk)paramIntent;
            paramIntent = paramIntent.a();
            localObject3 = paramIntent.W();
            int i2 = ((AlarmReceiver.AlarmType)localObject1).getNotificationId();
            ((com.truecaller.notifications.a)localObject3).a(i2);
            k = 0;
            int i3 = ((AlarmReceiver.AlarmType)localObject1).getNotificationId();
            String str2 = "notificationPeriodicPromo";
            ((com.truecaller.notifications.a)localObject3).a(null, i3, localNotification, str2, localBundle);
          }
        }
        long l3 = ((AlarmReceiver.AlarmType)localObject1).getRecurringPeriod();
        boolean bool5 = l3 < l2;
        if (bool5) {
          a(paramContext, (AlarmReceiver.AlarmType)localObject1);
        }
        return;
      }
      catch (IllegalArgumentException paramContext)
      {
        paramIntent = "Unsupported alarm type";
        d.a(paramContext, paramIntent);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.AlarmReceiver
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */