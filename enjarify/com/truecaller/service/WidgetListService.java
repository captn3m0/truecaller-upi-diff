package com.truecaller.service;

import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViewsService;
import android.widget.RemoteViewsService.RemoteViewsFactory;

public class WidgetListService
  extends RemoteViewsService
{
  public RemoteViewsService.RemoteViewsFactory onGetViewFactory(Intent paramIntent)
  {
    WidgetListService.b localb = new com/truecaller/service/WidgetListService$b;
    Context localContext = getApplicationContext();
    localb.<init>(localContext, paramIntent);
    return localb;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.WidgetListService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */