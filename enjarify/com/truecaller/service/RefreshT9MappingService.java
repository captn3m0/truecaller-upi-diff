package com.truecaller.service;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.af;
import android.support.v4.app.v;
import c.a.m;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.common.e.c;
import com.truecaller.common.e.d;
import com.truecaller.common.e.f;
import com.truecaller.data.access.s;
import com.truecaller.i.e;
import com.truecaller.i.g;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public final class RefreshT9MappingService
  extends af
{
  public static final RefreshT9MappingService.a j;
  
  static
  {
    RefreshT9MappingService.a locala = new com/truecaller/service/RefreshT9MappingService$a;
    locala.<init>((byte)0);
    j = locala;
  }
  
  public static final void a(Context paramContext)
  {
    k.b(paramContext, "context");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, RefreshT9MappingService.class);
    localIntent = localIntent.setAction("RefreshT9MappingService.action.sync").putExtra("RefreshT9MappingService.extra.rebuild_all", true);
    v.a(paramContext.getApplicationContext(), RefreshT9MappingService.class, 2131364101, localIntent);
  }
  
  public static final void b(Context paramContext)
  {
    RefreshT9MappingService.a.a(paramContext);
  }
  
  public final void a(Intent paramIntent)
  {
    k.b(paramIntent, "intent");
    Object localObject1 = TrueApp.y();
    k.a(localObject1, "TrueApp.getApp()");
    localObject1 = aHa;
    Object localObject2 = "t9_lang";
    localObject1 = ((e)localObject1).a((String)localObject2);
    Object localObject3;
    String str;
    boolean bool2;
    boolean bool3;
    if (localObject1 != null)
    {
      localObject2 = d.a;
      localObject2 = (Iterable)d.f();
      localObject3 = new java/util/ArrayList;
      int i = m.a((Iterable)localObject2, 10);
      ((ArrayList)localObject3).<init>(i);
      localObject3 = (Collection)localObject3;
      localObject2 = ((Iterable)localObject2).iterator();
      for (;;)
      {
        boolean bool1 = ((Iterator)localObject2).hasNext();
        if (!bool1) {
          break;
        }
        str = nextb;
        ((Collection)localObject3).add(str);
      }
      localObject3 = (List)localObject3;
      bool2 = ((List)localObject3).contains(localObject1);
      if (!bool2)
      {
        bool3 = false;
        localObject1 = null;
      }
      if (localObject1 != null) {}
    }
    else
    {
      localObject1 = f.a();
      k.a(localObject1, "LocaleManager.getAppLocale()");
      localObject1 = ((Locale)localObject1).getLanguage();
      localObject2 = "LocaleManager.getAppLocale().language";
      k.a(localObject1, (String)localObject2);
    }
    localObject2 = paramIntent.getAction();
    if (localObject2 == null) {
      return;
    }
    int m = ((String)localObject2).hashCode();
    int k = 1164272748;
    if (m != k)
    {
      k = 1517992106;
      if (m == k)
      {
        localObject3 = "RefreshT9MappingService.action.rebuild";
        bool2 = ((String)localObject2).equals(localObject3);
        if (bool2)
        {
          localObject2 = "RefreshT9MappingService.extra.scopes";
          paramIntent = paramIntent.getLongArrayExtra((String)localObject2);
          if (paramIntent != null)
          {
            localObject2 = new com/truecaller/data/access/s;
            localObject3 = getContentResolver();
            str = "contentResolver";
            k.a(localObject3, str);
            ((s)localObject2).<init>((ContentResolver)localObject3);
            ((s)localObject2).a(paramIntent, (String)localObject1);
          }
        }
      }
    }
    else
    {
      localObject1 = "RefreshT9MappingService.action.sync";
      bool3 = ((String)localObject2).equals(localObject1);
      if (bool3)
      {
        localObject1 = new com/truecaller/data/access/s;
        localObject2 = getContentResolver();
        k.a(localObject2, "contentResolver");
        ((s)localObject1).<init>((ContentResolver)localObject2);
        localObject2 = getApplicationContext();
        k.a(localObject2, "applicationContext");
        localObject3 = "RefreshT9MappingService.extra.rebuild_all";
        k = 0;
        str = null;
        boolean bool4 = paramIntent.getBooleanExtra((String)localObject3, false);
        ((s)localObject1).a((Context)localObject2, bool4);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.RefreshT9MappingService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */