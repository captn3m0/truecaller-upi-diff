package com.truecaller.service;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.aj;
import android.widget.RemoteViews;
import com.truecaller.analytics.bb;
import com.truecaller.calling.after_call.AfterCallActivity;
import com.truecaller.search.global.n;
import com.truecaller.ui.TruecallerInit;
import com.truecaller.ui.u;
import com.truecaller.util.b.at;
import com.truecaller.util.b.j;
import com.truecaller.util.b.j.b;

public class WidgetListProvider
  extends AppWidgetProvider
{
  private static PendingIntent a(Context paramContext, Intent paramIntent, int paramInt)
  {
    paramIntent.putExtra("widgetClick", true);
    bb.a(paramIntent, "widget", "openApp");
    return PendingIntent.getActivity(paramContext, paramInt, paramIntent, 0);
  }
  
  public static void a(Context paramContext)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("com.truecaller.widget.UPDATE_DATA");
    ComponentName localComponentName = new android/content/ComponentName;
    localComponentName.<init>(paramContext, WidgetListProvider.class);
    localIntent.setComponent(localComponentName);
    paramContext.sendBroadcast(localIntent);
  }
  
  public static void b(Context paramContext)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("com.truecaller.widget.UPDATE_HEADER");
    paramContext.sendBroadcast(localIntent);
  }
  
  public void onAppWidgetOptionsChanged(Context paramContext, AppWidgetManager paramAppWidgetManager, int paramInt, Bundle paramBundle)
  {
    Object localObject = paramAppWidgetManager.getAppWidgetOptions(paramInt);
    String str1 = "appWidgetMinWidth";
    int i = (((Bundle)localObject).getInt(str1) + 30) / 70;
    int j = 1;
    int k = 0;
    int m = 3;
    if (i < m)
    {
      i = 1;
    }
    else
    {
      i = 0;
      str1 = null;
    }
    String str2 = "widgetSizeKey";
    m = ((Bundle)localObject).getInt(str2);
    if (m != i)
    {
      ((Bundle)localObject).putInt("widgetSizeKey", i);
      paramAppWidgetManager.updateAppWidgetOptions(paramInt, (Bundle)localObject);
      localObject = new android/widget/RemoteViews;
      str2 = paramContext.getPackageName();
      int n = 2131559239;
      ((RemoteViews)localObject).<init>(str2, n);
      i ^= j;
      j = 2131365546;
      if (i == 0) {
        k = 8;
      }
      ((RemoteViews)localObject).setViewVisibility(j, k);
      paramAppWidgetManager.partiallyUpdateAppWidget(paramInt, (RemoteViews)localObject);
      int i1 = 2131365544;
      paramAppWidgetManager.notifyAppWidgetViewDataChanged(paramInt, i1);
    }
    super.onAppWidgetOptionsChanged(paramContext, paramAppWidgetManager, paramInt, paramBundle);
  }
  
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    Object localObject1 = paramIntent.getAction();
    Object localObject2 = "com.truecaller.widget.UPDATE_DATA";
    boolean bool1 = ((String)localObject2).equals(localObject1);
    if (bool1)
    {
      paramIntent = AppWidgetManager.getInstance(paramContext);
      localObject1 = new android/content/ComponentName;
      localObject2 = getClass();
      ((ComponentName)localObject1).<init>(paramContext, (Class)localObject2);
      paramContext = paramIntent.getAppWidgetIds((ComponentName)localObject1);
      paramIntent.notifyAppWidgetViewDataChanged(paramContext, 2131365544);
      return;
    }
    localObject2 = "com.truecaller.widget.UPDATE_HEADER";
    bool1 = ((String)localObject2).equals(localObject1);
    if (bool1)
    {
      paramIntent = AppWidgetManager.getInstance(paramContext);
      localObject1 = new android/content/ComponentName;
      localObject2 = getClass();
      ((ComponentName)localObject1).<init>(paramContext, (Class)localObject2);
      localObject1 = paramIntent.getAppWidgetIds((ComponentName)localObject1);
      onUpdate(paramContext, paramIntent, (int[])localObject1);
      return;
    }
    localObject2 = "com.sec.android.widgetapp.APPWIDGET_RESIZE";
    boolean bool2 = ((String)localObject2).equals(localObject1);
    if (bool2)
    {
      localObject1 = AppWidgetManager.getInstance(paramContext);
      localObject2 = "widgetId";
      Bundle localBundle = null;
      int i = paramIntent.getIntExtra((String)localObject2, 0);
      String str1 = "widgetspanx";
      int j = paramIntent.getIntExtra(str1, 0);
      String str2 = "widgetspany";
      int k = paramIntent.getIntExtra(str2, 0);
      if ((i > 0) && (j > 0) && (k > 0))
      {
        localBundle = new android/os/Bundle;
        localBundle.<init>();
        str2 = "appWidgetMinHeight";
        k *= 74;
        localBundle.putInt(str2, k);
        paramIntent = "appWidgetMinWidth";
        j *= 74;
        localBundle.putInt(paramIntent, j);
        onAppWidgetOptionsChanged(paramContext, (AppWidgetManager)localObject1, i, localBundle);
      }
      return;
    }
    super.onReceive(paramContext, paramIntent);
  }
  
  public void onUpdate(Context paramContext, AppWidgetManager paramAppWidgetManager, int[] paramArrayOfInt)
  {
    super.onUpdate(paramContext, paramAppWidgetManager, paramArrayOfInt);
    int i = paramArrayOfInt.length;
    int j = 0;
    while (j < i)
    {
      int k = paramArrayOfInt[j];
      RemoteViews localRemoteViews = new android/widget/RemoteViews;
      Object localObject1 = paramContext.getPackageName();
      int m = 2131559239;
      localRemoteViews.<init>((String)localObject1, m);
      int i1 = 2131365542;
      int i3 = 2131365544;
      localRemoteViews.setEmptyView(i3, i1);
      localObject1 = new com/truecaller/service/WidgetListService$a;
      ((WidgetListService.a)localObject1).<init>(paramContext, k);
      Object localObject2 = a;
      Object localObject3 = "extra_widget_layout";
      ((Intent)localObject2).putExtra((String)localObject3, m);
      Object localObject4 = a;
      localObject2 = "extra_list_item_layout";
      ((Intent)localObject4).putExtra((String)localObject2, 2131559240);
      localObject1 = a;
      localRemoteViews.setRemoteAdapter(i3, (Intent)localObject1);
      localObject1 = at.a(paramContext);
      localObject4 = ((j)localObject1).a(paramContext);
      int i4 = d;
      localRemoteViews.setImageViewResource(2131365545, i4);
      boolean bool3 = ((j.b)localObject4).a();
      i4 = 8;
      if (bool3)
      {
        bool3 = false;
        localObject2 = null;
      }
      else
      {
        i5 = 8;
      }
      int i6 = 2131365548;
      localRemoteViews.setViewVisibility(i6, i5);
      int i5 = f;
      localRemoteViews.setTextColor(i6, i5);
      i5 = 2131365543;
      Object localObject5 = "setBackgroundColor";
      m = e;
      localRemoteViews.setInt(i5, (String)localObject5, m);
      boolean bool1 = ((j)localObject1).c();
      if (bool1) {
        n = 2131234688;
      } else {
        n = 2131234687;
      }
      localObject2 = "setImageResource";
      i6 = 2131365550;
      localRemoteViews.setInt(i6, (String)localObject2, n);
      boolean bool2 = ((j)localObject1).c();
      int i2;
      if (bool2) {
        i2 = 2131234686;
      } else {
        i2 = 2131234685;
      }
      i5 = 2131365547;
      localRemoteViews.setInt(i5, "setImageResource", i2);
      localObject4 = "widget";
      localObject1 = TruecallerInit.a(paramContext, "calls", (String)localObject4);
      localObject1 = a(paramContext, (Intent)localObject1, 2131364162);
      int n = 2131365546;
      localRemoteViews.setOnClickPendingIntent(n, (PendingIntent)localObject1);
      localObject1 = n.a(paramContext, "widget");
      int i7 = 2131364160;
      localObject1 = a(paramContext, (Intent)localObject1, i7);
      localRemoteViews.setOnClickPendingIntent(i6, (PendingIntent)localObject1);
      localObject1 = u.a(paramContext);
      i6 = 2131364159;
      localObject1 = a(paramContext, (Intent)localObject1, i6);
      localRemoteViews.setOnClickPendingIntent(i5, (PendingIntent)localObject1);
      localObject1 = AppWidgetManager.getInstance(paramContext);
      localObject2 = ((AppWidgetManager)localObject1).getAppWidgetOptions(k);
      localObject5 = "widgetSizeKey";
      i5 = ((Bundle)localObject2).getInt((String)localObject5);
      if (i5 == 0)
      {
        i5 = 1;
      }
      else
      {
        i5 = 0;
        localObject2 = null;
      }
      if (i5 != 0)
      {
        i4 = 0;
        localObject3 = null;
      }
      localRemoteViews.setViewVisibility(n, i4);
      localObject4 = aj.a(paramContext);
      localObject3 = new android/content/ComponentName;
      localObject5 = b;
      ((ComponentName)localObject3).<init>((Context)localObject5, AfterCallActivity.class);
      ((aj)localObject4).a((ComponentName)localObject3);
      localObject2 = new android/content/Intent;
      localObject3 = AfterCallActivity.class;
      ((Intent)localObject2).<init>(paramContext, (Class)localObject3);
      ((aj)localObject4).a((Intent)localObject2);
      i5 = 2131364161;
      i4 = 134217728;
      localObject4 = ((aj)localObject4).a(i5, i4);
      localRemoteViews.setPendingIntentTemplate(i3, (PendingIntent)localObject4);
      ((AppWidgetManager)localObject1).updateAppWidget(k, localRemoteViews);
      j += 1;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.WidgetListProvider
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */