package com.truecaller.service;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.af;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.content.TruecallerContract.ah;
import com.truecaller.log.d;
import com.truecaller.util.bn;

public class SyncPhoneBookService
  extends af
{
  public static void a(Context paramContext)
  {
    a(paramContext, false);
  }
  
  public static void a(Context paramContext, boolean paramBoolean)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, SyncPhoneBookService.class);
    localIntent.putExtra("clear_existing", paramBoolean);
    a(paramContext.getApplicationContext(), SyncPhoneBookService.class, 2131364630, localIntent);
  }
  
  public final void a(Intent paramIntent)
  {
    Object localObject = "clear_existing";
    String str = null;
    try
    {
      boolean bool = paramIntent.getBooleanExtra((String)localObject, false);
      if (bool)
      {
        paramIntent = getContentResolver();
        localObject = TruecallerContract.ah.a();
        str = "contact_source=2";
        paramIntent.delete((Uri)localObject, str, null);
      }
      paramIntent = getApplicationContext();
      paramIntent = (TrueApp)paramIntent;
      paramIntent = paramIntent.a();
      paramIntent = paramIntent.bb();
      paramIntent.a();
      return;
    }
    catch (RuntimeException localRuntimeException)
    {
      d.a(localRuntimeException, "Error performing phone book sync");
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.SyncPhoneBookService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */