package com.truecaller.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

 enum AlarmReceiver$AlarmType$3
{
  AlarmReceiver$AlarmType$3(String paramString2)
  {
    super(paramString, 2, 1728000000L, 2131364125, paramString1, paramString2, null);
  }
  
  public final Notification getNotification(Context paramContext)
  {
    Object localObject = new android/content/Intent;
    ((Intent)localObject).<init>(paramContext, AlarmReceiver.class);
    localObject = ((Intent)localObject).setAction("com.truecaller.intent.action.SHARE");
    localObject = PendingIntent.getBroadcast(paramContext, 2131364136, (Intent)localObject, 134217728);
    String str1 = paramContext.getString(2131886117);
    String str2 = paramContext.getString(2131886631);
    return AlarmReceiver.AlarmType.access$100(paramContext, str1, str2, (PendingIntent)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.AlarmReceiver.AlarmType.3
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */