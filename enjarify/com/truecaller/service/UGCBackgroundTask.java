package com.truecaller.service;

import android.content.Context;
import android.os.Bundle;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.common.background.PersistentBackgroundTask;
import com.truecaller.common.background.PersistentBackgroundTask.RunResult;
import com.truecaller.common.h.ac;
import com.truecaller.old.data.access.i;
import com.truecaller.util.t;
import com.truecaller.utils.l;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class UGCBackgroundTask
  extends PersistentBackgroundTask
{
  public com.truecaller.featuretoggles.e a;
  public com.truecaller.common.h.c b;
  
  public UGCBackgroundTask()
  {
    TrueApp.y().a().a(this);
  }
  
  /* Error */
  private static int a(Context paramContext, ArrayList paramArrayList)
  {
    // Byte code:
    //   0: iconst_1
    //   1: istore_2
    //   2: iload_2
    //   3: anewarray 29	java/lang/String
    //   6: astore_3
    //   7: new 31	java/lang/StringBuilder
    //   10: astore 4
    //   12: aload 4
    //   14: ldc 33
    //   16: invokespecial 36	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   19: aload_1
    //   20: invokevirtual 42	java/util/ArrayList:size	()I
    //   23: istore 5
    //   25: aload 4
    //   27: iload 5
    //   29: invokevirtual 46	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   32: pop
    //   33: aload 4
    //   35: invokevirtual 50	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   38: astore 4
    //   40: iconst_0
    //   41: istore 5
    //   43: aload_3
    //   44: iconst_0
    //   45: aload 4
    //   47: aastore
    //   48: iconst_m1
    //   49: i2l
    //   50: lstore 6
    //   52: new 52	com/google/gson/i
    //   55: astore 8
    //   57: aload 8
    //   59: invokespecial 53	com/google/gson/i:<init>	()V
    //   62: aload_1
    //   63: invokevirtual 57	java/util/ArrayList:iterator	()Ljava/util/Iterator;
    //   66: astore 9
    //   68: aload 9
    //   70: invokeinterface 63 1 0
    //   75: istore 10
    //   77: iload 10
    //   79: ifeq +254 -> 333
    //   82: aload 9
    //   84: invokeinterface 67 1 0
    //   89: astore 11
    //   91: aload 11
    //   93: checkcast 69	com/truecaller/old/data/a/e
    //   96: astore 11
    //   98: new 71	com/google/gson/o
    //   101: astore 12
    //   103: aload 12
    //   105: invokespecial 72	com/google/gson/o:<init>	()V
    //   108: ldc 74
    //   110: astore 13
    //   112: aload 11
    //   114: getfield 78	com/truecaller/old/data/a/e:c	Ljava/lang/String;
    //   117: astore 14
    //   119: aload 12
    //   121: aload 13
    //   123: aload 14
    //   125: invokestatic 81	com/truecaller/old/data/a/e:a	(Lcom/google/gson/o;Ljava/lang/String;Ljava/lang/String;)V
    //   128: ldc 83
    //   130: astore 13
    //   132: aload 11
    //   134: getfield 86	com/truecaller/old/data/a/e:d	Ljava/lang/String;
    //   137: astore 14
    //   139: aload 12
    //   141: aload 13
    //   143: aload 14
    //   145: invokestatic 81	com/truecaller/old/data/a/e:a	(Lcom/google/gson/o;Ljava/lang/String;Ljava/lang/String;)V
    //   148: ldc 88
    //   150: astore 13
    //   152: aload 11
    //   154: getfield 91	com/truecaller/old/data/a/e:e	Ljava/lang/String;
    //   157: astore 14
    //   159: aload 12
    //   161: aload 13
    //   163: aload 14
    //   165: invokestatic 81	com/truecaller/old/data/a/e:a	(Lcom/google/gson/o;Ljava/lang/String;Ljava/lang/String;)V
    //   168: ldc 93
    //   170: astore 13
    //   172: new 38	java/util/ArrayList
    //   175: astore 14
    //   177: aload 11
    //   179: getfield 97	com/truecaller/old/data/a/e:j	Ljava/util/Set;
    //   182: astore 15
    //   184: aload 14
    //   186: aload 15
    //   188: invokespecial 100	java/util/ArrayList:<init>	(Ljava/util/Collection;)V
    //   191: aload 12
    //   193: aload 13
    //   195: aload 14
    //   197: invokestatic 103	com/truecaller/old/data/a/e:a	(Lcom/google/gson/o;Ljava/lang/String;Ljava/util/ArrayList;)V
    //   200: ldc 105
    //   202: astore 13
    //   204: new 38	java/util/ArrayList
    //   207: astore 14
    //   209: aload 11
    //   211: getfield 108	com/truecaller/old/data/a/e:i	Ljava/util/Set;
    //   214: astore 15
    //   216: aload 14
    //   218: aload 15
    //   220: invokespecial 100	java/util/ArrayList:<init>	(Ljava/util/Collection;)V
    //   223: aload 12
    //   225: aload 13
    //   227: aload 14
    //   229: invokestatic 103	com/truecaller/old/data/a/e:a	(Lcom/google/gson/o;Ljava/lang/String;Ljava/util/ArrayList;)V
    //   232: ldc 110
    //   234: astore 13
    //   236: new 38	java/util/ArrayList
    //   239: astore 14
    //   241: aload 11
    //   243: getfield 113	com/truecaller/old/data/a/e:h	Ljava/util/Set;
    //   246: astore 15
    //   248: aload 14
    //   250: aload 15
    //   252: invokespecial 100	java/util/ArrayList:<init>	(Ljava/util/Collection;)V
    //   255: aload 12
    //   257: aload 13
    //   259: aload 14
    //   261: invokestatic 103	com/truecaller/old/data/a/e:a	(Lcom/google/gson/o;Ljava/lang/String;Ljava/util/ArrayList;)V
    //   264: ldc 115
    //   266: astore 13
    //   268: new 38	java/util/ArrayList
    //   271: astore 14
    //   273: aload 11
    //   275: getfield 118	com/truecaller/old/data/a/e:k	Ljava/util/Set;
    //   278: astore 15
    //   280: aload 14
    //   282: aload 15
    //   284: invokespecial 100	java/util/ArrayList:<init>	(Ljava/util/Collection;)V
    //   287: aload 12
    //   289: aload 13
    //   291: aload 14
    //   293: invokestatic 103	com/truecaller/old/data/a/e:a	(Lcom/google/gson/o;Ljava/lang/String;Ljava/util/ArrayList;)V
    //   296: aload 11
    //   298: aload 12
    //   300: invokevirtual 121	com/truecaller/old/data/a/e:a	(Lcom/google/gson/o;)V
    //   303: ldc 123
    //   305: astore 13
    //   307: aload 11
    //   309: getfield 126	com/truecaller/old/data/a/e:f	Ljava/lang/String;
    //   312: astore 11
    //   314: aload 12
    //   316: aload 13
    //   318: aload 11
    //   320: invokestatic 81	com/truecaller/old/data/a/e:a	(Lcom/google/gson/o;Ljava/lang/String;Ljava/lang/String;)V
    //   323: aload 8
    //   325: aload 12
    //   327: invokevirtual 129	com/google/gson/i:a	(Lcom/google/gson/l;)V
    //   330: goto -262 -> 68
    //   333: aload 8
    //   335: invokevirtual 130	com/google/gson/i:toString	()Ljava/lang/String;
    //   338: astore 9
    //   340: aload 8
    //   342: invokevirtual 132	com/google/gson/i:a	()I
    //   345: istore 10
    //   347: iload 10
    //   349: ifeq +9 -> 358
    //   352: iconst_1
    //   353: istore 10
    //   355: goto +9 -> 364
    //   358: iconst_0
    //   359: istore 10
    //   361: aconst_null
    //   362: astore 11
    //   364: ldc -122
    //   366: astore 12
    //   368: iconst_1
    //   369: anewarray 29	java/lang/String
    //   372: dup
    //   373: iconst_0
    //   374: aload 12
    //   376: aastore
    //   377: astore 12
    //   379: iload 10
    //   381: aload 12
    //   383: invokestatic 140	com/truecaller/log/AssertionUtil$OnlyInDebug:isTrue	(Z[Ljava/lang/String;)V
    //   386: aload_1
    //   387: invokevirtual 143	java/util/ArrayList:isEmpty	()Z
    //   390: istore 10
    //   392: iload 10
    //   394: ifne +9 -> 403
    //   397: iconst_1
    //   398: istore 10
    //   400: goto +9 -> 409
    //   403: iconst_0
    //   404: istore 10
    //   406: aconst_null
    //   407: astore 11
    //   409: ldc -111
    //   411: astore 12
    //   413: iconst_1
    //   414: anewarray 29	java/lang/String
    //   417: dup
    //   418: iconst_0
    //   419: aload 12
    //   421: aastore
    //   422: astore 12
    //   424: iload 10
    //   426: aload 12
    //   428: invokestatic 140	com/truecaller/log/AssertionUtil$OnlyInDebug:isTrue	(Z[Ljava/lang/String;)V
    //   431: aload 9
    //   433: invokestatic 150	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   436: istore 10
    //   438: iload 10
    //   440: ifne +9 -> 449
    //   443: iconst_1
    //   444: istore 10
    //   446: goto +9 -> 455
    //   449: iconst_0
    //   450: istore 10
    //   452: aconst_null
    //   453: astore 11
    //   455: ldc -104
    //   457: astore 12
    //   459: iconst_1
    //   460: anewarray 29	java/lang/String
    //   463: dup
    //   464: iconst_0
    //   465: aload 12
    //   467: aastore
    //   468: astore 12
    //   470: iload 10
    //   472: aload 12
    //   474: invokestatic 140	com/truecaller/log/AssertionUtil$OnlyInDebug:isTrue	(Z[Ljava/lang/String;)V
    //   477: aload 8
    //   479: invokevirtual 132	com/google/gson/i:a	()I
    //   482: istore 16
    //   484: iload 16
    //   486: ifeq +669 -> 1155
    //   489: aload_1
    //   490: invokevirtual 143	java/util/ArrayList:isEmpty	()Z
    //   493: istore 16
    //   495: iload 16
    //   497: ifne +658 -> 1155
    //   500: aload 9
    //   502: invokestatic 150	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   505: istore 16
    //   507: iload 16
    //   509: ifeq +6 -> 515
    //   512: goto +643 -> 1155
    //   515: invokestatic 158	java/lang/System:currentTimeMillis	()J
    //   518: lstore 6
    //   520: iconst_0
    //   521: istore 16
    //   523: aconst_null
    //   524: astore 8
    //   526: ldc -96
    //   528: astore 11
    //   530: aload_0
    //   531: aload 11
    //   533: invokestatic 165	android/support/v4/content/b:a	(Landroid/content/Context;Ljava/lang/String;)I
    //   536: istore 10
    //   538: iload 10
    //   540: ifne +9 -> 549
    //   543: aload_0
    //   544: invokestatic 170	com/truecaller/common/h/k:e	(Landroid/content/Context;)Ljava/lang/String;
    //   547: astore 8
    //   549: new 172	com/truecaller/common/network/util/a
    //   552: astore 11
    //   554: aload 11
    //   556: invokespecial 173	com/truecaller/common/network/util/a:<init>	()V
    //   559: getstatic 179	com/truecaller/common/network/util/KnownEndpoints:PHONEBOOK	Lcom/truecaller/common/network/util/KnownEndpoints;
    //   562: astore 12
    //   564: aload 11
    //   566: aload 12
    //   568: invokevirtual 182	com/truecaller/common/network/util/a:a	(Lcom/truecaller/common/network/util/KnownEndpoints;)Lcom/truecaller/common/network/util/a;
    //   571: astore 11
    //   573: new 184	com/truecaller/common/network/d/b
    //   576: astore 12
    //   578: aload 12
    //   580: invokespecial 185	com/truecaller/common/network/d/b:<init>	()V
    //   583: ldc -69
    //   585: astore 13
    //   587: aload 12
    //   589: aload 13
    //   591: invokestatic 192	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   594: aload 11
    //   596: getfield 195	com/truecaller/common/network/util/a:a	Ljava/util/List;
    //   599: astore 13
    //   601: aload 13
    //   603: ifnonnull +27 -> 630
    //   606: new 38	java/util/ArrayList
    //   609: astore 13
    //   611: aload 13
    //   613: invokespecial 196	java/util/ArrayList:<init>	()V
    //   616: aload 13
    //   618: checkcast 198	java/util/List
    //   621: astore 13
    //   623: aload 11
    //   625: aload 13
    //   627: putfield 195	com/truecaller/common/network/util/a:a	Ljava/util/List;
    //   630: aload 11
    //   632: getfield 195	com/truecaller/common/network/util/a:a	Ljava/util/List;
    //   635: astore 13
    //   637: aload 13
    //   639: ifnull +13 -> 652
    //   642: aload 13
    //   644: aload 12
    //   646: invokeinterface 202 2 0
    //   651: pop
    //   652: ldc -52
    //   654: astore 12
    //   656: aload 11
    //   658: aload 12
    //   660: invokevirtual 207	com/truecaller/common/network/util/a:b	(Ljava/lang/Class;)Ljava/lang/Object;
    //   663: astore 11
    //   665: aload 11
    //   667: checkcast 204	com/truecaller/network/f/b$a
    //   670: astore 11
    //   672: getstatic 212	com/truecaller/common/network/util/g:b	Lokhttp3/w;
    //   675: astore 12
    //   677: aload 12
    //   679: aload 9
    //   681: invokestatic 217	okhttp3/ac:a	(Lokhttp3/w;Ljava/lang/String;)Lokhttp3/ac;
    //   684: astore 9
    //   686: aload 11
    //   688: aload 8
    //   690: aload 9
    //   692: invokeinterface 220 3 0
    //   697: astore 8
    //   699: aload 8
    //   701: invokeinterface 225 1 0
    //   706: astore 8
    //   708: aload 8
    //   710: getfield 230	e/r:b	Ljava/lang/Object;
    //   713: astore 8
    //   715: aload 8
    //   717: checkcast 232	com/truecaller/network/f/a
    //   720: astore 8
    //   722: invokestatic 158	java/lang/System:currentTimeMillis	()J
    //   725: lstore 17
    //   727: lload 17
    //   729: lload 6
    //   731: lsub
    //   732: lstore 6
    //   734: aload 8
    //   736: ifnull +386 -> 1122
    //   739: aload 8
    //   741: getfield 235	com/truecaller/network/f/a:a	Lcom/truecaller/network/f/a$a;
    //   744: astore 9
    //   746: aload 9
    //   748: ifnull +374 -> 1122
    //   751: aload 8
    //   753: getfield 235	com/truecaller/network/f/a:a	Lcom/truecaller/network/f/a$a;
    //   756: astore 9
    //   758: aload 9
    //   760: getfield 238	com/truecaller/network/f/a$a:a	Ljava/util/List;
    //   763: astore 9
    //   765: aload 9
    //   767: ifnull +355 -> 1122
    //   770: aload 8
    //   772: getfield 235	com/truecaller/network/f/a:a	Lcom/truecaller/network/f/a$a;
    //   775: astore 9
    //   777: aload 9
    //   779: getfield 238	com/truecaller/network/f/a$a:a	Ljava/util/List;
    //   782: astore 9
    //   784: aload 9
    //   786: invokeinterface 239 1 0
    //   791: istore 19
    //   793: iload 19
    //   795: ifne +327 -> 1122
    //   798: new 241	java/util/HashMap
    //   801: astore 9
    //   803: aload 9
    //   805: invokespecial 242	java/util/HashMap:<init>	()V
    //   808: aload 8
    //   810: getfield 235	com/truecaller/network/f/a:a	Lcom/truecaller/network/f/a$a;
    //   813: astore 8
    //   815: aload 8
    //   817: getfield 238	com/truecaller/network/f/a$a:a	Ljava/util/List;
    //   820: astore 8
    //   822: aload 8
    //   824: invokeinterface 243 1 0
    //   829: astore 8
    //   831: aload 8
    //   833: invokeinterface 63 1 0
    //   838: istore 10
    //   840: iload 10
    //   842: ifeq +72 -> 914
    //   845: aload 8
    //   847: invokeinterface 67 1 0
    //   852: astore 11
    //   854: aload 11
    //   856: checkcast 245	com/truecaller/network/f/a$b
    //   859: astore 11
    //   861: aload 11
    //   863: getfield 248	com/truecaller/network/f/a$b:c	Z
    //   866: istore 20
    //   868: iload 20
    //   870: ifeq -39 -> 831
    //   873: aload 11
    //   875: getfield 250	com/truecaller/network/f/a$b:a	Ljava/lang/String;
    //   878: astore 12
    //   880: aload 12
    //   882: ifnull -51 -> 831
    //   885: aload 11
    //   887: getfield 250	com/truecaller/network/f/a$b:a	Ljava/lang/String;
    //   890: astore 12
    //   892: aload 11
    //   894: getfield 252	com/truecaller/network/f/a$b:b	Ljava/lang/String;
    //   897: astore 11
    //   899: aload 9
    //   901: aload 12
    //   903: aload 11
    //   905: invokeinterface 258 3 0
    //   910: pop
    //   911: goto -80 -> 831
    //   914: iload_2
    //   915: anewarray 29	java/lang/String
    //   918: astore 8
    //   920: new 31	java/lang/StringBuilder
    //   923: astore 11
    //   925: ldc_w 260
    //   928: astore 12
    //   930: aload 11
    //   932: aload 12
    //   934: invokespecial 36	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   937: aload 9
    //   939: invokeinterface 261 1 0
    //   944: istore 20
    //   946: aload 11
    //   948: iload 20
    //   950: invokevirtual 46	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   953: pop
    //   954: aload 11
    //   956: invokevirtual 50	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   959: astore 11
    //   961: aload 8
    //   963: iconst_0
    //   964: aload 11
    //   966: aastore
    //   967: new 263	com/truecaller/old/data/access/e
    //   970: astore 8
    //   972: aload 8
    //   974: aload_0
    //   975: invokespecial 266	com/truecaller/old/data/access/e:<init>	(Landroid/content/Context;)V
    //   978: new 268	com/truecaller/old/data/access/i
    //   981: astore 11
    //   983: aload 11
    //   985: aload_0
    //   986: invokespecial 269	com/truecaller/old/data/access/i:<init>	(Landroid/content/Context;)V
    //   989: aload_1
    //   990: invokevirtual 57	java/util/ArrayList:iterator	()Ljava/util/Iterator;
    //   993: astore_0
    //   994: iconst_0
    //   995: istore 20
    //   997: aconst_null
    //   998: astore 12
    //   1000: aload_0
    //   1001: invokeinterface 63 1 0
    //   1006: istore 21
    //   1008: iload 21
    //   1010: ifeq +97 -> 1107
    //   1013: aload_0
    //   1014: invokeinterface 67 1 0
    //   1019: astore 13
    //   1021: aload 13
    //   1023: checkcast 69	com/truecaller/old/data/a/e
    //   1026: astore 13
    //   1028: aload 13
    //   1030: getfield 126	com/truecaller/old/data/a/e:f	Ljava/lang/String;
    //   1033: astore 14
    //   1035: aload 9
    //   1037: aload 14
    //   1039: invokeinterface 273 2 0
    //   1044: astore 14
    //   1046: aload 14
    //   1048: checkcast 29	java/lang/String
    //   1051: astore 14
    //   1053: aload 14
    //   1055: ifnull -55 -> 1000
    //   1058: aload 13
    //   1060: getfield 277	com/truecaller/old/data/a/e:g	J
    //   1063: lstore 22
    //   1065: aload 13
    //   1067: getfield 280	com/truecaller/old/data/a/e:a	I
    //   1070: istore 24
    //   1072: aload 8
    //   1074: lload 22
    //   1076: aload 14
    //   1078: iload 24
    //   1080: invokevirtual 283	com/truecaller/old/data/access/e:a	(JLjava/lang/String;I)Lcom/truecaller/old/data/entity/c;
    //   1083: pop
    //   1084: aload 13
    //   1086: getfield 126	com/truecaller/old/data/a/e:f	Ljava/lang/String;
    //   1089: astore 13
    //   1091: aload 11
    //   1093: aload 13
    //   1095: invokevirtual 285	com/truecaller/old/data/access/i:b	(Ljava/lang/String;)V
    //   1098: iload 20
    //   1100: iconst_1
    //   1101: iadd
    //   1102: istore 20
    //   1104: goto -104 -> 1000
    //   1107: iload 20
    //   1109: istore 5
    //   1111: goto +16 -> 1127
    //   1114: astore_0
    //   1115: goto +78 -> 1193
    //   1118: astore_0
    //   1119: goto +74 -> 1193
    //   1122: iconst_0
    //   1123: istore_2
    //   1124: aconst_null
    //   1125: astore 25
    //   1127: aload_1
    //   1128: invokevirtual 288	java/util/ArrayList:clear	()V
    //   1131: lload 6
    //   1133: iload_2
    //   1134: invokestatic 291	com/truecaller/service/UGCBackgroundTask:a	(JZ)V
    //   1137: goto +81 -> 1218
    //   1140: astore_0
    //   1141: invokestatic 158	java/lang/System:currentTimeMillis	()J
    //   1144: lstore 26
    //   1146: lload 26
    //   1148: lload 6
    //   1150: lsub
    //   1151: lstore 6
    //   1153: aload_0
    //   1154: athrow
    //   1155: ldc_w 293
    //   1158: astore_0
    //   1159: iconst_1
    //   1160: anewarray 29	java/lang/String
    //   1163: iconst_0
    //   1164: aload_0
    //   1165: aastore
    //   1166: aload_1
    //   1167: invokevirtual 288	java/util/ArrayList:clear	()V
    //   1170: lload 6
    //   1172: iconst_0
    //   1173: invokestatic 291	com/truecaller/service/UGCBackgroundTask:a	(JZ)V
    //   1176: iconst_0
    //   1177: ireturn
    //   1178: astore_0
    //   1179: goto +42 -> 1221
    //   1182: astore_0
    //   1183: goto +4 -> 1187
    //   1186: astore_0
    //   1187: iconst_0
    //   1188: istore 20
    //   1190: aconst_null
    //   1191: astore 12
    //   1193: ldc_w 295
    //   1196: astore 25
    //   1198: aload_0
    //   1199: aload 25
    //   1201: invokestatic 300	com/truecaller/log/d:a	(Ljava/lang/Throwable;Ljava/lang/String;)V
    //   1204: aload_1
    //   1205: invokevirtual 288	java/util/ArrayList:clear	()V
    //   1208: lload 6
    //   1210: iconst_0
    //   1211: invokestatic 291	com/truecaller/service/UGCBackgroundTask:a	(JZ)V
    //   1214: iload 20
    //   1216: istore 5
    //   1218: iload 5
    //   1220: ireturn
    //   1221: aload_1
    //   1222: invokevirtual 288	java/util/ArrayList:clear	()V
    //   1225: lload 6
    //   1227: iconst_0
    //   1228: invokestatic 291	com/truecaller/service/UGCBackgroundTask:a	(JZ)V
    //   1231: aload_0
    //   1232: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1233	0	paramContext	Context
    //   0	1233	1	paramArrayList	ArrayList
    //   1	1133	2	bool1	boolean
    //   6	38	3	arrayOfString	String[]
    //   10	36	4	localObject1	Object
    //   23	1196	5	i	int
    //   50	1176	6	l1	long
    //   55	1018	8	localObject2	Object
    //   66	970	9	localObject3	Object
    //   75	3	10	bool2	boolean
    //   345	35	10	j	int
    //   390	81	10	bool3	boolean
    //   536	3	10	k	int
    //   838	3	10	bool4	boolean
    //   89	1003	11	localObject4	Object
    //   101	1091	12	localObject5	Object
    //   110	984	13	localObject6	Object
    //   117	960	14	localObject7	Object
    //   182	101	15	localSet	java.util.Set
    //   482	3	16	m	int
    //   493	29	16	bool5	boolean
    //   725	3	17	l2	long
    //   791	3	19	bool6	boolean
    //   866	3	20	bool7	boolean
    //   944	271	20	n	int
    //   1006	3	21	bool8	boolean
    //   1063	12	22	l3	long
    //   1070	9	24	i1	int
    //   1125	75	25	str	String
    //   1144	3	26	l4	long
    // Exception table:
    //   from	to	target	type
    //   1000	1006	1114	java/io/IOException
    //   1013	1019	1114	java/io/IOException
    //   1021	1026	1114	java/io/IOException
    //   1028	1033	1114	java/io/IOException
    //   1037	1044	1114	java/io/IOException
    //   1046	1051	1114	java/io/IOException
    //   1058	1063	1114	java/io/IOException
    //   1065	1070	1114	java/io/IOException
    //   1078	1084	1114	java/io/IOException
    //   1084	1089	1114	java/io/IOException
    //   1093	1098	1114	java/io/IOException
    //   1000	1006	1118	java/lang/RuntimeException
    //   1013	1019	1118	java/lang/RuntimeException
    //   1021	1026	1118	java/lang/RuntimeException
    //   1028	1033	1118	java/lang/RuntimeException
    //   1037	1044	1118	java/lang/RuntimeException
    //   1046	1051	1118	java/lang/RuntimeException
    //   1058	1063	1118	java/lang/RuntimeException
    //   1065	1070	1118	java/lang/RuntimeException
    //   1078	1084	1118	java/lang/RuntimeException
    //   1084	1089	1118	java/lang/RuntimeException
    //   1093	1098	1118	java/lang/RuntimeException
    //   531	536	1140	finally
    //   543	547	1140	finally
    //   549	552	1140	finally
    //   554	559	1140	finally
    //   559	562	1140	finally
    //   566	571	1140	finally
    //   573	576	1140	finally
    //   578	583	1140	finally
    //   589	594	1140	finally
    //   594	599	1140	finally
    //   606	609	1140	finally
    //   611	616	1140	finally
    //   616	621	1140	finally
    //   625	630	1140	finally
    //   630	635	1140	finally
    //   644	652	1140	finally
    //   658	663	1140	finally
    //   665	670	1140	finally
    //   672	675	1140	finally
    //   679	684	1140	finally
    //   690	697	1140	finally
    //   699	706	1140	finally
    //   708	713	1140	finally
    //   715	720	1140	finally
    //   52	55	1178	finally
    //   57	62	1178	finally
    //   62	66	1178	finally
    //   68	75	1178	finally
    //   82	89	1178	finally
    //   91	96	1178	finally
    //   98	101	1178	finally
    //   103	108	1178	finally
    //   112	117	1178	finally
    //   123	128	1178	finally
    //   132	137	1178	finally
    //   143	148	1178	finally
    //   152	157	1178	finally
    //   163	168	1178	finally
    //   172	175	1178	finally
    //   177	182	1178	finally
    //   186	191	1178	finally
    //   195	200	1178	finally
    //   204	207	1178	finally
    //   209	214	1178	finally
    //   218	223	1178	finally
    //   227	232	1178	finally
    //   236	239	1178	finally
    //   241	246	1178	finally
    //   250	255	1178	finally
    //   259	264	1178	finally
    //   268	271	1178	finally
    //   273	278	1178	finally
    //   282	287	1178	finally
    //   291	296	1178	finally
    //   298	303	1178	finally
    //   307	312	1178	finally
    //   318	323	1178	finally
    //   325	330	1178	finally
    //   333	338	1178	finally
    //   340	345	1178	finally
    //   368	377	1178	finally
    //   381	386	1178	finally
    //   386	390	1178	finally
    //   413	422	1178	finally
    //   426	431	1178	finally
    //   431	436	1178	finally
    //   459	468	1178	finally
    //   472	477	1178	finally
    //   477	482	1178	finally
    //   489	493	1178	finally
    //   500	505	1178	finally
    //   515	518	1178	finally
    //   722	725	1178	finally
    //   739	744	1178	finally
    //   751	756	1178	finally
    //   758	763	1178	finally
    //   770	775	1178	finally
    //   777	782	1178	finally
    //   784	791	1178	finally
    //   798	801	1178	finally
    //   803	808	1178	finally
    //   808	813	1178	finally
    //   815	820	1178	finally
    //   822	829	1178	finally
    //   831	838	1178	finally
    //   845	852	1178	finally
    //   854	859	1178	finally
    //   861	866	1178	finally
    //   873	878	1178	finally
    //   885	890	1178	finally
    //   892	897	1178	finally
    //   903	911	1178	finally
    //   914	918	1178	finally
    //   920	923	1178	finally
    //   932	937	1178	finally
    //   937	944	1178	finally
    //   948	954	1178	finally
    //   954	959	1178	finally
    //   964	967	1178	finally
    //   967	970	1178	finally
    //   974	978	1178	finally
    //   978	981	1178	finally
    //   985	989	1178	finally
    //   989	993	1178	finally
    //   1000	1006	1178	finally
    //   1013	1019	1178	finally
    //   1021	1026	1178	finally
    //   1028	1033	1178	finally
    //   1037	1044	1178	finally
    //   1046	1051	1178	finally
    //   1058	1063	1178	finally
    //   1065	1070	1178	finally
    //   1078	1084	1178	finally
    //   1084	1089	1178	finally
    //   1093	1098	1178	finally
    //   1141	1144	1178	finally
    //   1153	1155	1178	finally
    //   1159	1166	1178	finally
    //   1199	1204	1178	finally
    //   52	55	1182	java/io/IOException
    //   57	62	1182	java/io/IOException
    //   62	66	1182	java/io/IOException
    //   68	75	1182	java/io/IOException
    //   82	89	1182	java/io/IOException
    //   91	96	1182	java/io/IOException
    //   98	101	1182	java/io/IOException
    //   103	108	1182	java/io/IOException
    //   112	117	1182	java/io/IOException
    //   123	128	1182	java/io/IOException
    //   132	137	1182	java/io/IOException
    //   143	148	1182	java/io/IOException
    //   152	157	1182	java/io/IOException
    //   163	168	1182	java/io/IOException
    //   172	175	1182	java/io/IOException
    //   177	182	1182	java/io/IOException
    //   186	191	1182	java/io/IOException
    //   195	200	1182	java/io/IOException
    //   204	207	1182	java/io/IOException
    //   209	214	1182	java/io/IOException
    //   218	223	1182	java/io/IOException
    //   227	232	1182	java/io/IOException
    //   236	239	1182	java/io/IOException
    //   241	246	1182	java/io/IOException
    //   250	255	1182	java/io/IOException
    //   259	264	1182	java/io/IOException
    //   268	271	1182	java/io/IOException
    //   273	278	1182	java/io/IOException
    //   282	287	1182	java/io/IOException
    //   291	296	1182	java/io/IOException
    //   298	303	1182	java/io/IOException
    //   307	312	1182	java/io/IOException
    //   318	323	1182	java/io/IOException
    //   325	330	1182	java/io/IOException
    //   333	338	1182	java/io/IOException
    //   340	345	1182	java/io/IOException
    //   368	377	1182	java/io/IOException
    //   381	386	1182	java/io/IOException
    //   386	390	1182	java/io/IOException
    //   413	422	1182	java/io/IOException
    //   426	431	1182	java/io/IOException
    //   431	436	1182	java/io/IOException
    //   459	468	1182	java/io/IOException
    //   472	477	1182	java/io/IOException
    //   477	482	1182	java/io/IOException
    //   489	493	1182	java/io/IOException
    //   500	505	1182	java/io/IOException
    //   515	518	1182	java/io/IOException
    //   722	725	1182	java/io/IOException
    //   739	744	1182	java/io/IOException
    //   751	756	1182	java/io/IOException
    //   758	763	1182	java/io/IOException
    //   770	775	1182	java/io/IOException
    //   777	782	1182	java/io/IOException
    //   784	791	1182	java/io/IOException
    //   798	801	1182	java/io/IOException
    //   803	808	1182	java/io/IOException
    //   808	813	1182	java/io/IOException
    //   815	820	1182	java/io/IOException
    //   822	829	1182	java/io/IOException
    //   831	838	1182	java/io/IOException
    //   845	852	1182	java/io/IOException
    //   854	859	1182	java/io/IOException
    //   861	866	1182	java/io/IOException
    //   873	878	1182	java/io/IOException
    //   885	890	1182	java/io/IOException
    //   892	897	1182	java/io/IOException
    //   903	911	1182	java/io/IOException
    //   914	918	1182	java/io/IOException
    //   920	923	1182	java/io/IOException
    //   932	937	1182	java/io/IOException
    //   937	944	1182	java/io/IOException
    //   948	954	1182	java/io/IOException
    //   954	959	1182	java/io/IOException
    //   964	967	1182	java/io/IOException
    //   967	970	1182	java/io/IOException
    //   974	978	1182	java/io/IOException
    //   978	981	1182	java/io/IOException
    //   985	989	1182	java/io/IOException
    //   989	993	1182	java/io/IOException
    //   1141	1144	1182	java/io/IOException
    //   1153	1155	1182	java/io/IOException
    //   1159	1166	1182	java/io/IOException
    //   52	55	1186	java/lang/RuntimeException
    //   57	62	1186	java/lang/RuntimeException
    //   62	66	1186	java/lang/RuntimeException
    //   68	75	1186	java/lang/RuntimeException
    //   82	89	1186	java/lang/RuntimeException
    //   91	96	1186	java/lang/RuntimeException
    //   98	101	1186	java/lang/RuntimeException
    //   103	108	1186	java/lang/RuntimeException
    //   112	117	1186	java/lang/RuntimeException
    //   123	128	1186	java/lang/RuntimeException
    //   132	137	1186	java/lang/RuntimeException
    //   143	148	1186	java/lang/RuntimeException
    //   152	157	1186	java/lang/RuntimeException
    //   163	168	1186	java/lang/RuntimeException
    //   172	175	1186	java/lang/RuntimeException
    //   177	182	1186	java/lang/RuntimeException
    //   186	191	1186	java/lang/RuntimeException
    //   195	200	1186	java/lang/RuntimeException
    //   204	207	1186	java/lang/RuntimeException
    //   209	214	1186	java/lang/RuntimeException
    //   218	223	1186	java/lang/RuntimeException
    //   227	232	1186	java/lang/RuntimeException
    //   236	239	1186	java/lang/RuntimeException
    //   241	246	1186	java/lang/RuntimeException
    //   250	255	1186	java/lang/RuntimeException
    //   259	264	1186	java/lang/RuntimeException
    //   268	271	1186	java/lang/RuntimeException
    //   273	278	1186	java/lang/RuntimeException
    //   282	287	1186	java/lang/RuntimeException
    //   291	296	1186	java/lang/RuntimeException
    //   298	303	1186	java/lang/RuntimeException
    //   307	312	1186	java/lang/RuntimeException
    //   318	323	1186	java/lang/RuntimeException
    //   325	330	1186	java/lang/RuntimeException
    //   333	338	1186	java/lang/RuntimeException
    //   340	345	1186	java/lang/RuntimeException
    //   368	377	1186	java/lang/RuntimeException
    //   381	386	1186	java/lang/RuntimeException
    //   386	390	1186	java/lang/RuntimeException
    //   413	422	1186	java/lang/RuntimeException
    //   426	431	1186	java/lang/RuntimeException
    //   431	436	1186	java/lang/RuntimeException
    //   459	468	1186	java/lang/RuntimeException
    //   472	477	1186	java/lang/RuntimeException
    //   477	482	1186	java/lang/RuntimeException
    //   489	493	1186	java/lang/RuntimeException
    //   500	505	1186	java/lang/RuntimeException
    //   515	518	1186	java/lang/RuntimeException
    //   722	725	1186	java/lang/RuntimeException
    //   739	744	1186	java/lang/RuntimeException
    //   751	756	1186	java/lang/RuntimeException
    //   758	763	1186	java/lang/RuntimeException
    //   770	775	1186	java/lang/RuntimeException
    //   777	782	1186	java/lang/RuntimeException
    //   784	791	1186	java/lang/RuntimeException
    //   798	801	1186	java/lang/RuntimeException
    //   803	808	1186	java/lang/RuntimeException
    //   808	813	1186	java/lang/RuntimeException
    //   815	820	1186	java/lang/RuntimeException
    //   822	829	1186	java/lang/RuntimeException
    //   831	838	1186	java/lang/RuntimeException
    //   845	852	1186	java/lang/RuntimeException
    //   854	859	1186	java/lang/RuntimeException
    //   861	866	1186	java/lang/RuntimeException
    //   873	878	1186	java/lang/RuntimeException
    //   885	890	1186	java/lang/RuntimeException
    //   892	897	1186	java/lang/RuntimeException
    //   903	911	1186	java/lang/RuntimeException
    //   914	918	1186	java/lang/RuntimeException
    //   920	923	1186	java/lang/RuntimeException
    //   932	937	1186	java/lang/RuntimeException
    //   937	944	1186	java/lang/RuntimeException
    //   948	954	1186	java/lang/RuntimeException
    //   954	959	1186	java/lang/RuntimeException
    //   964	967	1186	java/lang/RuntimeException
    //   967	970	1186	java/lang/RuntimeException
    //   974	978	1186	java/lang/RuntimeException
    //   978	981	1186	java/lang/RuntimeException
    //   985	989	1186	java/lang/RuntimeException
    //   989	993	1186	java/lang/RuntimeException
    //   1141	1144	1186	java/lang/RuntimeException
    //   1153	1155	1186	java/lang/RuntimeException
    //   1159	1166	1186	java/lang/RuntimeException
  }
  
  private static void a(long paramLong, boolean paramBoolean)
  {
    com.truecaller.i.e locale = TrueApp.y().a().F();
    String str1 = "backupBatchSize";
    int i = locale.a(str1, 0);
    if (paramBoolean)
    {
      long l = 10000L;
      paramBoolean = paramLong < l;
      if (!paramBoolean)
      {
        l = 5000L;
        paramBoolean = paramLong < l;
        if (paramBoolean)
        {
          i = i * 133 / 100;
          str2 = "backupBatchSize";
          j = 200;
          if (i <= j) {
            j = i;
          }
          locale.b(str2, j);
        }
        return;
      }
    }
    i = i * 66 / 100;
    String str2 = "backupBatchSize";
    int j = 50;
    if (i >= j) {
      j = i;
    }
    locale.b(str2, j);
  }
  
  public final int a()
  {
    return 10015;
  }
  
  public final PersistentBackgroundTask.RunResult a(Context paramContext, Bundle paramBundle)
  {
    Object localObject1 = this;
    Object localObject2 = a.T();
    int i = ((com.truecaller.featuretoggles.b)localObject2).a();
    if (i != 0) {
      return PersistentBackgroundTask.RunResult.Success;
    }
    localObject2 = (TrueApp)paramContext.getApplicationContext();
    Object localObject3 = b.c();
    Object localObject4 = b.n();
    Object localObject5 = ((TrueApp)localObject2).a().bw();
    com.truecaller.analytics.e.a locala = new com/truecaller/analytics/e$a;
    locala.<init>("EnhancedSearch");
    Object localObject6 = PersistentBackgroundTask.RunResult.FailedSkip;
    boolean bool1 = ((TrueApp)localObject2).p();
    if (bool1)
    {
      Object localObject7 = { "android.permission.READ_CONTACTS" };
      boolean bool2 = ((l)localObject5).a((String[])localObject7);
      if (bool2)
      {
        boolean bool3 = ((ac)localObject4).a();
        if (!bool3)
        {
          localObject4 = "featureUgcDisabled";
          boolean bool4 = ((com.truecaller.common.g.a)localObject3).b((String)localObject4);
          if (bool4)
          {
            localObject2 = "Result";
            localObject3 = "Disabled";
            locala.a((String)localObject2, (String)localObject3);
            break label1491;
          }
          locala.a("Result", "Proceeded");
          localObject3 = TrueApp.y().a();
          localObject4 = ((bp)localObject3).F();
          localObject3 = ((bp)localObject3).I();
          localObject5 = new com/truecaller/old/data/access/i;
          ((i)localObject5).<init>((Context)localObject2);
          localObject6 = new com/truecaller/common/tag/b;
          ((com.truecaller.common.tag.b)localObject6).<init>((Context)localObject2);
          locala.a("Type", "Normal");
          localObject7 = "tagsPhonebookForcedUpload";
          int m = 0;
          Object localObject8 = null;
          bool1 = com.truecaller.common.b.e.a((String)localObject7, false);
          boolean bool6 = true;
          Object localObject9;
          if (!bool1)
          {
            bool1 = false;
            localObject7 = null;
          }
          else
          {
            localObject7 = new com/truecaller/old/data/access/e;
            ((com.truecaller.old.data.access.e)localObject7).<init>((Context)localObject2);
            localObject9 = ((com.truecaller.old.data.access.e)localObject7).a(com.truecaller.old.data.entity.c.class);
            localObject10 = ((List)localObject9).iterator();
            bool7 = false;
            localObject11 = null;
            for (;;)
            {
              bool8 = ((Iterator)localObject10).hasNext();
              if (!bool8) {
                break;
              }
              localObject12 = (com.truecaller.old.data.entity.c)((Iterator)localObject10).next();
              i2 = o;
              int i3 = -1;
              if (i2 != i3)
              {
                o = i3;
                bool7 = true;
              }
            }
            if (bool7)
            {
              ((com.truecaller.old.data.access.a)localObject7).a(false);
              ((com.truecaller.old.data.access.a)localObject7).a((List)localObject9);
            }
            localObject7 = "tagsPhonebookForcedUpload";
            com.truecaller.common.b.e.b((String)localObject7, false);
            bool1 = true;
          }
          if (bool1)
          {
            localObject7 = "Type";
            localObject9 = "TagReset";
            locala.a((String)localObject7, (String)localObject9);
          }
          int k = ((i)localObject5).c();
          if (k == 0)
          {
            localObject7 = "Type";
            localObject9 = "Initial";
            locala.a((String)localObject7, (String)localObject9);
          }
          localObject7 = t.a((Context)localObject2);
          int i6 = ((List)localObject7).size();
          Object localObject10 = new java/util/ArrayList;
          ((ArrayList)localObject10).<init>();
          Object localObject11 = new String[bool6];
          Object localObject12 = new java/lang/StringBuilder;
          ((StringBuilder)localObject12).<init>("UGC - enough time passed since last timestamp - processing ");
          ((StringBuilder)localObject12).append(i6);
          String str1 = " device contacts";
          ((StringBuilder)localObject12).append(str1);
          localObject12 = ((StringBuilder)localObject12).toString();
          localObject11[0] = localObject12;
          locala.a("Total", i6);
          localObject11 = b;
          boolean bool7 = ((com.truecaller.common.h.c)localObject11).c();
          if (bool7)
          {
            localObject11 = "backup";
            bool7 = ((com.truecaller.common.g.a)localObject3).b((String)localObject11);
            if (bool7)
            {
              bool7 = true;
              break label629;
            }
          }
          bool7 = false;
          localObject11 = null;
          label629:
          localObject12 = "featureEmailSource";
          boolean bool8 = com.truecaller.common.b.e.a((String)localObject12, false);
          str1 = b.g();
          String str2 = "Full";
          locala.a(str2, bool7);
          if (str1 == null)
          {
            str1 = "<null>";
          }
          else
          {
            str2 = "";
            i4 = str1.equals(str2);
            if (i4 != 0) {
              str1 = "<empty>";
            }
          }
          locala.a("InstallerPackage", str1);
          str2 = b.f();
          locala.a("BuildName", str2);
          localObject7 = ((List)localObject7).iterator();
          int i2 = 0;
          str1 = null;
          int i4 = 0;
          str2 = null;
          label955:
          int i5;
          for (;;)
          {
            boolean bool9 = ((Iterator)localObject7).hasNext();
            if (!bool9) {
              break;
            }
            Object localObject13 = ((Iterator)localObject7).next();
            localObject8 = localObject13;
            localObject8 = (com.truecaller.old.data.a.b)localObject13;
            Object localObject14 = d;
            if (localObject14 != null)
            {
              localObject14 = d;
              bool6 = ((List)localObject14).isEmpty();
              if (!bool6)
              {
                localObject14 = "featureUgcContactsWithoutIdentity";
                boolean bool10 = true;
                bool6 = ((com.truecaller.common.g.a)localObject3).a((String)localObject14, bool10);
                if (!bool6)
                {
                  localObject1 = e;
                  if (localObject1 != null)
                  {
                    localObject1 = e;
                    bool10 = ((List)localObject1).isEmpty();
                    if (!bool10)
                    {
                      bool10 = true;
                      break label955;
                    }
                  }
                  localObject1 = c;
                  if (localObject1 != null)
                  {
                    localObject1 = c.e;
                    if (localObject1 == null)
                    {
                      localObject1 = c.b;
                      if (localObject1 == null) {}
                    }
                    else
                    {
                      bool10 = true;
                      break label955;
                    }
                  }
                  localObject1 = b;
                  if ((localObject1 != null) && (bool7))
                  {
                    bool10 = true;
                  }
                  else
                  {
                    bool10 = false;
                    localObject1 = null;
                  }
                  if (!bool10)
                  {
                    bool6 = false;
                    localObject14 = null;
                    break label1203;
                  }
                }
                localObject1 = new com/truecaller/old/data/a/e;
                ((com.truecaller.old.data.a.e)localObject1).<init>((com.truecaller.old.data.a.b)localObject8, (com.truecaller.common.tag.b)localObject6, bool7, bool8);
                m = b;
                i1 = a;
                if (m != i1)
                {
                  m = 1;
                }
                else
                {
                  m = 0;
                  localObject8 = null;
                }
                if (m == 0)
                {
                  localObject8 = f;
                  boolean bool5 = ((i)localObject5).a((String)localObject8);
                  if (bool5)
                  {
                    bool10 = false;
                    localObject1 = null;
                  }
                }
                if (localObject1 != null)
                {
                  ((ArrayList)localObject10).add(localObject1);
                  int i7 = ((ArrayList)localObject10).size();
                  localObject8 = "backupBatchSize";
                  i1 = 0;
                  localObject14 = null;
                  n = ((com.truecaller.i.e)localObject4).a((String)localObject8, 0);
                  if (i7 == n)
                  {
                    localObject8 = new String[1];
                    localObject1 = new java/lang/StringBuilder;
                    ((StringBuilder)localObject1).<init>("While iterating all contacts, UGC size: ");
                    i1 = ((ArrayList)localObject10).size();
                    ((StringBuilder)localObject1).append(i1);
                    localObject1 = ((StringBuilder)localObject1).toString();
                    i1 = 0;
                    localObject14 = null;
                    localObject8[0] = localObject1;
                    i7 = ((ArrayList)localObject10).size();
                    i2 += i7;
                    int i8 = a((Context)localObject2, (ArrayList)localObject10);
                    i4 += i8;
                  }
                }
                else
                {
                  i1 = 0;
                  localObject14 = null;
                }
                localObject1 = this;
                n = 0;
                localObject8 = null;
                i1 = 1;
                continue;
              }
            }
            int i1 = 0;
            localObject14 = null;
            label1203:
            localObject1 = this;
            int n = 0;
            localObject8 = null;
            i1 = 1;
          }
          int i9 = ((ArrayList)localObject10).size();
          if (i9 > 0)
          {
            i9 = ((ArrayList)localObject10).size();
            i2 += i9;
            int i10 = a((Context)localObject2, (ArrayList)localObject10);
            i5 += i10;
          }
          else
          {
            localObject1 = "UGC - skip as no new UGCs to upload";
            new String[1][0] = localObject1;
          }
          locala.a("Count", i2);
          locala.a("Successful", i5);
          localObject1 = "CountRatio";
          if (i6 == 0)
          {
            localObject2 = "0-1";
          }
          else
          {
            i2 = i2 * 100 / i6;
            i = 1;
            if (i2 <= i)
            {
              localObject2 = "0-1";
            }
            else
            {
              int j = 5;
              if (i2 <= j)
              {
                localObject2 = "2-5";
              }
              else
              {
                j = 10;
                if (i2 <= j)
                {
                  localObject2 = "6-10";
                }
                else
                {
                  j = 20;
                  if (i2 <= j)
                  {
                    localObject2 = "11-20";
                  }
                  else
                  {
                    j = 50;
                    if (i2 <= j)
                    {
                      localObject2 = "21-50";
                    }
                    else
                    {
                      j = 75;
                      if (i2 <= j) {
                        localObject2 = "51-75";
                      } else {
                        localObject2 = "76-100";
                      }
                    }
                  }
                }
              }
            }
          }
          locala.a((String)localObject1, (String)localObject2);
          double d = i5;
          localObject1 = Double.valueOf(d);
          a = ((Double)localObject1);
          localObject6 = PersistentBackgroundTask.RunResult.Success;
          break label1491;
        }
      }
    }
    localObject1 = "Result";
    localObject2 = "Skipped";
    locala.a((String)localObject1, (String)localObject2);
    label1491:
    localObject1 = TrueApp.y().a().c();
    localObject2 = locala.a();
    ((com.truecaller.analytics.b)localObject1).a((com.truecaller.analytics.e)localObject2);
    return (PersistentBackgroundTask.RunResult)localObject6;
  }
  
  public final boolean a(Context paramContext)
  {
    return e(paramContext);
  }
  
  public final com.truecaller.common.background.e b()
  {
    com.truecaller.common.background.e.a locala = new com/truecaller/common/background/e$a;
    int i = 1;
    locala.<init>(i);
    TimeUnit localTimeUnit = TimeUnit.HOURS;
    locala = locala.a(12, localTimeUnit);
    localTimeUnit = TimeUnit.HOURS;
    locala = locala.b(6, localTimeUnit);
    localTimeUnit = TimeUnit.HOURS;
    locala = locala.c(2, localTimeUnit);
    b = i;
    c = false;
    return locala.b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.UGCBackgroundTask
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */