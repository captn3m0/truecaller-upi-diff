package com.truecaller.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import com.truecaller.common.h.am;

 enum AlarmReceiver$AlarmType$1
{
  AlarmReceiver$AlarmType$1(String paramString2)
  {
    super(paramString, 0, 432000000L, 2131364127, paramString1, paramString2, null);
  }
  
  public final Notification getNotification(Context paramContext)
  {
    Object localObject = (com.truecaller.common.b.a)paramContext.getApplicationContext();
    boolean bool1 = ((com.truecaller.common.b.a)localObject).p();
    String str1 = null;
    if (!bool1) {
      return null;
    }
    localObject = ((com.truecaller.common.b.a)localObject).u().c();
    String str2 = "profileAvatar";
    localObject = ((com.truecaller.common.g.a)localObject).a(str2);
    boolean bool2 = am.a((CharSequence)localObject);
    if (bool2) {
      return null;
    }
    localObject = com.truecaller.profile.a.b(paramContext).putExtra("ARG_SHOW_PHOTO_SELECTOR", true);
    str1 = getNotificationType();
    localObject = ((Intent)localObject).putExtra("notification_type", str1);
    localObject = PendingIntent.getActivity(paramContext, 2131364133, (Intent)localObject, 134217728);
    str2 = paramContext.getString(2131886117);
    str1 = paramContext.getString(2131886629);
    return AlarmReceiver.AlarmType.access$100(paramContext, str2, str1, (PendingIntent)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.AlarmReceiver.AlarmType.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */