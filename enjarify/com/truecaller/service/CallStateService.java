package com.truecaller.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION;
import android.os.IBinder;
import android.telephony.TelephonyManager;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.utils.l;

public class CallStateService
  extends Service
{
  private static boolean b = false;
  private final CallStateService.a a;
  private l c;
  private boolean d;
  
  public CallStateService()
  {
    CallStateService.a locala = new com/truecaller/service/CallStateService$a;
    locala.<init>(this, (byte)0);
    a = locala;
  }
  
  private void a(int paramInt)
  {
    TelephonyManager localTelephonyManager = b();
    CallStateService.a locala = a;
    localTelephonyManager.listen(locala, paramInt);
  }
  
  public static void a(Context paramContext)
  {
    int i = Build.VERSION.SDK_INT;
    int j = 26;
    if (i < j)
    {
      Intent localIntent = new android/content/Intent;
      Class localClass = CallStateService.class;
      localIntent.<init>(paramContext, localClass);
      paramContext.startService(localIntent);
    }
  }
  
  public static boolean a()
  {
    return b;
  }
  
  private TelephonyManager b()
  {
    return (TelephonyManager)getSystemService("phone");
  }
  
  private boolean c()
  {
    l locall = c;
    String[] arrayOfString = { "android.permission.READ_PHONE_STATE", "android.permission.READ_CALL_LOG" };
    return locall.a(arrayOfString);
  }
  
  public IBinder onBind(Intent paramIntent)
  {
    return null;
  }
  
  public void onCreate()
  {
    super.onCreate();
    l locall = ((TrueApp)getApplicationContext()).a().bw();
    c = locall;
    boolean bool = c();
    d = bool;
    a(32);
    b = true;
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    a(0);
    b = false;
  }
  
  public int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
  {
    return 1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.CallStateService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */