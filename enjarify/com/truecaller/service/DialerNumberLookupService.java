package com.truecaller.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import com.d.b.aa.a;
import com.d.b.ab;
import com.d.b.t;
import com.truecaller.androidactors.f;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.common.account.r;
import com.truecaller.common.h.am;
import com.truecaller.data.access.i;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.data.entity.Number;
import com.truecaller.data.entity.g;
import com.truecaller.log.d;
import com.truecaller.network.search.j;
import com.truecaller.old.data.access.Settings;
import com.truecaller.util.b.at;
import com.truecaller.util.b.j.b;
import com.truecaller.wizard.b.c;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.UUID;

public class DialerNumberLookupService
  extends Service
  implements Handler.Callback
{
  private Messenger a;
  private Handler b;
  private g c;
  private f d;
  private i e;
  private r f;
  
  private Contact a(Number paramNumber, int paramInt)
  {
    Number localNumber = null;
    try
    {
      localObject1 = new com/truecaller/network/search/j;
      localObject2 = UUID.randomUUID();
      String str = "callerId";
      ((j)localObject1).<init>(this, (UUID)localObject2, str);
      localObject2 = paramNumber.d();
      i = ((String)localObject2);
      localObject2 = paramNumber.l();
      localObject1 = ((j)localObject1).b((String)localObject2);
      h = paramInt;
      paramInt = 1;
      b = paramInt;
      d = paramInt;
      e = paramInt;
      c = paramInt;
      com.truecaller.network.search.n localn = ((j)localObject1).f();
      if (localn == null) {
        return localNumber;
      }
      paramNumber = localn.a();
      localNumber = paramNumber;
    }
    catch (RuntimeException localRuntimeException) {}catch (IOException localIOException) {}
    Object localObject1 = new java/lang/StringBuilder;
    Object localObject2 = "Search for ";
    ((StringBuilder)localObject1).<init>((String)localObject2);
    ((StringBuilder)localObject1).append(paramNumber);
    ((StringBuilder)localObject1).append(" failed");
    paramNumber = ((StringBuilder)localObject1).toString();
    d.a(localIOException, paramNumber);
    return localNumber;
  }
  
  private void a(Bundle paramBundle, Contact paramContact, String paramString, Number paramNumber)
  {
    if (paramContact == null) {
      return;
    }
    String str1 = paramNumber.n();
    paramBundle.putString("phoneNumber", str1);
    str1 = paramNumber.a();
    paramBundle.putString("normalizedNumber", str1);
    int i = paramNumber.i();
    paramBundle.putInt("phoneType", i);
    paramNumber = paramNumber.k();
    paramBundle.putString("phoneLabel", paramNumber);
    boolean bool1 = paramContact.U();
    boolean bool2 = false;
    String str2 = null;
    boolean bool3;
    if (bool1)
    {
      bool3 = true;
      Object localObject = new Object[bool3];
      String str3 = paramContact.t();
      localObject[0] = str3;
      str1 = getString(2131886145, (Object[])localObject);
      paramBundle.putString("displayName", str1);
      paramNumber = "spamString";
      i = 2131886146;
      Object[] arrayOfObject = new Object[bool3];
      int k = paramContact.I();
      localObject = Integer.valueOf(k);
      arrayOfObject[0] = localObject;
      str1 = getString(i, arrayOfObject);
      paramBundle.putString(paramNumber, str1);
    }
    else
    {
      paramNumber = "displayName";
      str1 = paramContact.t();
      paramBundle.putString(paramNumber, str1);
    }
    paramNumber = paramContact.a(false);
    if (paramNumber != null)
    {
      str2 = "imageUrl";
      paramNumber = paramNumber.toString();
      paramBundle.putString(str2, paramNumber);
    }
    bool2 = am.a(paramContact.B());
    paramBundle.putBoolean("isBusiness", bool2);
    bool2 = paramContact.U();
    paramBundle.putBoolean("isSpam", bool2);
    paramNumber = getResources();
    str2 = "spamLogo";
    i = 2131234840;
    str1 = paramNumber.getResourceEntryName(i);
    paramBundle.putString(str2, str1);
    boolean bool4 = paramContact.Z();
    if (!bool4)
    {
      paramContact = at.b(this);
      i = d;
      str1 = paramNumber.getResourceEntryName(i);
      paramBundle.putString("partnerLogo", str1);
      str2 = "identifiedByText";
      i = 2131886597;
      str1 = paramNumber.getString(i);
      paramBundle.putString(str2, str1);
      bool4 = paramContact.a();
      if (bool4)
      {
        str2 = null;
        paramBundle.putString("poweredByLogo", null);
        paramContact = "poweredByText";
        int j = 2131886816;
        paramNumber = paramNumber.getString(j);
        paramBundle.putString(paramContact, paramNumber);
      }
    }
    try
    {
      paramContact = d;
      paramContact = paramContact.a();
      paramContact = (com.truecaller.callhistory.a)paramContact;
      paramContact = paramContact.c(paramString);
      paramContact = paramContact.d();
      paramContact = (HistoryEvent)paramContact;
      if (paramContact != null)
      {
        long l1 = h;
        long l2 = 0L;
        bool3 = l1 < l2;
        if (bool3)
        {
          paramString = "lastCall";
          l2 = h;
          paramBundle.putLong(paramString, l2);
        }
      }
      return;
    }
    catch (InterruptedException localInterruptedException) {}
  }
  
  private void a(String paramString, Bundle paramBundle, int paramInt)
  {
    boolean bool = paramBundle.isEmpty();
    if (bool)
    {
      bool = a();
      if (bool)
      {
        Object localObject = c;
        int i = 1;
        String[] arrayOfString = new String[i];
        arrayOfString[0] = paramString;
        localObject = ((g)localObject).a(arrayOfString);
        if (localObject == null) {
          return;
        }
        Contact localContact = a((Number)localObject, paramInt);
        a(paramBundle, localContact, paramString, (Number)localObject);
        return;
      }
    }
  }
  
  private boolean a()
  {
    Object localObject = f;
    boolean bool1 = ((r)localObject).c();
    boolean bool2 = true;
    if (bool1) {
      return bool2;
    }
    localObject = (com.truecaller.common.b.a)getApplication();
    boolean bool3 = ((com.truecaller.common.b.a)localObject).q();
    if (bool3)
    {
      com.truecaller.wizard.utils.a locala = new com/truecaller/wizard/utils/a;
      locala.<init>((Context)localObject);
      localObject = locala.d();
      if (localObject != null)
      {
        c.a(bool2);
        return bool2;
      }
      return false;
    }
    return false;
  }
  
  public boolean handleMessage(Message paramMessage)
  {
    long l = Binder.clearCallingIdentity();
    Object localObject1 = new android/os/Bundle;
    ((Bundle)localObject1).<init>();
    Object localObject2 = Message.obtain();
    int i = 3003;
    what = i;
    Object localObject3 = paramMessage.getData();
    Object localObject4 = "phoneNumber";
    localObject3 = ((Bundle)localObject3).getString((String)localObject4);
    Object localObject5;
    if (localObject3 != null)
    {
      localObject4 = e.a((String)localObject3);
      localObject5 = (Contact)a;
      localObject4 = (Number)b;
      if ((localObject5 != null) && (localObject4 != null)) {
        a((Bundle)localObject1, (Contact)localObject5, (String)localObject3, (Number)localObject4);
      }
    }
    int k = 1001;
    int m = what;
    int n = 1;
    if (k == m)
    {
      k = 2;
      a((String)localObject3, (Bundle)localObject1, k);
    }
    else
    {
      k = 2002;
      m = what;
      if (k == m) {
        a((String)localObject3, (Bundle)localObject1, n);
      }
    }
    ((Message)localObject2).setData((Bundle)localObject1);
    try
    {
      localObject3 = replyTo;
      if (localObject3 != null)
      {
        localObject3 = replyTo;
        ((Messenger)localObject3).send((Message)localObject2);
      }
    }
    catch (RemoteException localRemoteException1)
    {
      boolean bool2;
      for (;;) {}
    }
    localObject2 = "imageUrl";
    localObject1 = ((Bundle)localObject1).getString((String)localObject2);
    bool2 = am.b((CharSequence)localObject1);
    if (!bool2)
    {
      localObject1 = Uri.parse((String)localObject1);
      bool2 = false;
      localObject2 = null;
    }
    try
    {
      localObject3 = com.d.b.w.a(this);
      localObject1 = ((com.d.b.w)localObject3).a((Uri)localObject1);
      i = 800;
      localObject1 = ((ab)localObject1).b(i, i);
      localObject1 = ((ab)localObject1).c();
      localObject3 = b;
      k = b;
      if (k == 0)
      {
        k = a;
        if (k == 0)
        {
          localObject1 = new java/lang/IllegalStateException;
          localObject3 = "onlyScaleDown can not be applied without resize";
          ((IllegalStateException)localObject1).<init>((String)localObject3);
          throw ((Throwable)localObject1);
        }
      }
      e = n;
      boolean bool1 = Settings.d();
      if (!bool1)
      {
        localObject3 = t.c;
        k = 0;
        localObject4 = null;
        localObject4 = new t[0];
        ((ab)localObject1).a((t)localObject3);
      }
      localObject2 = ((ab)localObject1).d();
    }
    finally
    {
      int j;
      int i1;
      for (;;) {}
    }
    localObject1 = b;
    if (localObject1 != null)
    {
      j = 4004;
      localObject1 = ((Handler)localObject1).obtainMessage(j);
      if (localObject2 != null)
      {
        localObject3 = new android/os/Bundle;
        ((Bundle)localObject3).<init>();
        localObject4 = new java/io/ByteArrayOutputStream;
        ((ByteArrayOutputStream)localObject4).<init>();
        localObject5 = Bitmap.CompressFormat.JPEG;
        i1 = 75;
        ((Bitmap)localObject2).compress((Bitmap.CompressFormat)localObject5, i1, (OutputStream)localObject4);
        localObject5 = "image_data";
        localObject4 = ((ByteArrayOutputStream)localObject4).toByteArray();
        ((Bundle)localObject3).putByteArray((String)localObject5, (byte[])localObject4);
        ((Message)localObject1).setData((Bundle)localObject3);
        ((Bitmap)localObject2).recycle();
      }
    }
    try
    {
      paramMessage = replyTo;
      paramMessage.send((Message)localObject1);
    }
    catch (RemoteException localRemoteException2)
    {
      for (;;) {}
    }
    Binder.restoreCallingIdentity(l);
    return n;
  }
  
  public IBinder onBind(Intent paramIntent)
  {
    return a.getBinder();
  }
  
  public void onCreate()
  {
    super.onCreate();
    Object localObject1 = new android/os/HandlerThread;
    ((HandlerThread)localObject1).<init>("DialerLookup");
    ((HandlerThread)localObject1).start();
    Object localObject2 = new android/os/Handler;
    localObject1 = ((HandlerThread)localObject1).getLooper();
    ((Handler)localObject2).<init>((Looper)localObject1, this);
    b = ((Handler)localObject2);
    localObject1 = new android/os/Messenger;
    localObject2 = b;
    ((Messenger)localObject1).<init>((Handler)localObject2);
    a = ((Messenger)localObject1);
    localObject1 = ((bk)getApplicationContext()).a();
    localObject2 = ((bp)localObject1).aa();
    c = ((g)localObject2);
    localObject2 = ((bp)localObject1).ad();
    d = ((f)localObject2);
    localObject2 = ((bp)localObject1).br();
    e = ((i)localObject2);
    localObject1 = ((bp)localObject1).T();
    f = ((r)localObject1);
  }
  
  public void onDestroy()
  {
    b.getLooper().quit();
    b = null;
    a = null;
    super.onDestroy();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.DialerNumberLookupService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */