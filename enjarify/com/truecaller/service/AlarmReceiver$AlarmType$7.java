package com.truecaller.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.i.c;
import com.truecaller.old.data.access.Settings;
import com.truecaller.utils.l;

 enum AlarmReceiver$AlarmType$7
{
  AlarmReceiver$AlarmType$7(String paramString2)
  {
    super(paramString, 6, 60000L, 2131364128, paramString1, paramString2, null);
  }
  
  public final Notification getNotification(Context paramContext)
  {
    Object localObject = new android/content/Intent;
    ((Intent)localObject).<init>("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
    localObject = PendingIntent.getActivity(paramContext, 2131364131, (Intent)localObject, 134217728);
    String str1 = paramContext.getString(2131886158);
    String str2 = paramContext.getString(2131886630);
    return AlarmReceiver.AlarmType.access$100(paramContext, str1, str2, (PendingIntent)localObject);
  }
  
  public final boolean shouldShow(Context paramContext)
  {
    paramContext = TrueApp.y().a();
    c localc = paramContext.D();
    paramContext = paramContext.bw();
    String str = "blockCallMethod";
    int i = localc.a(str, 0);
    boolean bool = paramContext.e();
    if (!bool)
    {
      bool = Settings.b(i);
      if (bool) {
        return true;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.AlarmReceiver.AlarmType.7
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */