package com.truecaller.service;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.app.af;
import c.g.b.k;
import c.m.c;
import c.m.i;
import com.truecaller.content.TruecallerContract;
import com.truecaller.content.TruecallerContract.a;
import com.truecaller.content.TruecallerContract.f;
import com.truecaller.data.access.f.a;
import com.truecaller.data.access.f.b;
import com.truecaller.data.access.f.c;
import com.truecaller.data.access.f.d;
import com.truecaller.data.access.g;
import com.truecaller.data.access.n;
import com.truecaller.util.z;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public final class RefreshContactIndexingService
  extends af
{
  public final void a(Intent paramIntent)
  {
    Object localObject1 = paramIntent;
    k.b(paramIntent, "intent");
    Object localObject2 = paramIntent.getAction();
    if (localObject2 == null) {
      return;
    }
    int i = ((String)localObject2).hashCode();
    int j = 759309553;
    Object localObject3;
    boolean bool1;
    Object localObject4;
    Object localObject5;
    if (i != j)
    {
      j = 1874572677;
      if (i == j)
      {
        localObject3 = "RefreshContactIndexingService.action.sync";
        bool1 = ((String)localObject2).equals(localObject3);
        if (bool1)
        {
          localObject2 = new com/truecaller/data/access/f;
          localObject3 = getContentResolver();
          k.a(localObject3, "contentResolver");
          localObject4 = new com/truecaller/service/RefreshContactIndexingService$a;
          localObject5 = this;
          localObject5 = (Context)this;
          ((RefreshContactIndexingService.a)localObject4).<init>((Context)localObject5);
          ((com.truecaller.data.access.f)localObject2).<init>((ContentResolver)localObject3, (RefreshContactIndexingService.a)localObject4);
          localObject3 = "RefreshContactIndexingService.extra.rebuild_all";
          boolean bool2 = paramIntent.getBooleanExtra((String)localObject3, false);
          ((com.truecaller.data.access.f)localObject2).a(bool2);
        }
      }
    }
    else
    {
      localObject3 = "RefreshContactIndexingService.action.rebuild";
      bool1 = ((String)localObject2).equals(localObject3);
      if (bool1)
      {
        localObject2 = "RefreshContactIndexingService.extra.scopes";
        localObject1 = paramIntent.getLongArrayExtra((String)localObject2);
        if (localObject1 != null)
        {
          localObject2 = new com/truecaller/data/access/f;
          localObject3 = getContentResolver();
          k.a(localObject3, "contentResolver");
          localObject4 = new com/truecaller/service/RefreshContactIndexingService$a;
          localObject5 = this;
          localObject5 = (Context)this;
          ((RefreshContactIndexingService.a)localObject4).<init>((Context)localObject5);
          ((com.truecaller.data.access.f)localObject2).<init>((ContentResolver)localObject3, (RefreshContactIndexingService.a)localObject4);
          k.b(localObject1, "aggregatedContactIds");
          localObject3 = new com/truecaller/data/access/g;
          j = 2;
          localObject4 = new n[j];
          localObject5 = (n)com.truecaller.data.access.l.a;
          localObject4[0] = localObject5;
          localObject5 = (n)com.truecaller.data.access.b.a;
          int k = 1;
          localObject4[k] = localObject5;
          ((g)localObject3).<init>((n[])localObject4);
          localObject4 = TruecallerContract.f.a();
          long l1 = System.currentTimeMillis();
          localObject5 = c.a.f.b((long[])localObject1);
          Object localObject6 = localObject5;
          localObject6 = (Iterable)localObject5;
          Object localObject7 = (CharSequence)"(";
          Object localObject8 = (CharSequence)")";
          localObject5 = c.a.m.a((Iterable)localObject6, null, (CharSequence)localObject7, (CharSequence)localObject8, 0, null, null, 57);
          Object localObject9 = String.valueOf(localObject5);
          localObject6 = "aggregated_contact_id IN ".concat((String)localObject9);
          localObject5 = String.valueOf(localObject5);
          localObject5 = "_id IN ".concat((String)localObject5);
          long l2 = System.currentTimeMillis();
          int m = a.delete((Uri)localObject4, (String)localObject6, null);
          long l3 = System.currentTimeMillis() - l2;
          localObject9 = new String[k];
          localObject7 = new java/lang/StringBuilder;
          ((StringBuilder)localObject7).<init>("delete from ");
          ((StringBuilder)localObject7).append(localObject4);
          ((StringBuilder)localObject7).append(' ');
          ((StringBuilder)localObject7).append(m);
          ((StringBuilder)localObject7).append(" items, took: ");
          ((StringBuilder)localObject7).append(l3);
          ((StringBuilder)localObject7).append("ms\ndeleteWhere = ");
          ((StringBuilder)localObject7).append((String)localObject6);
          localObject6 = ((StringBuilder)localObject7).toString();
          localObject9[0] = localObject6;
          long l4 = System.currentTimeMillis();
          localObject7 = a;
          localObject8 = TruecallerContract.a.b();
          Object localObject10 = localObject5;
          localObject7 = ((ContentResolver)localObject7).query((Uri)localObject8, null, (String)localObject5, null, null);
          localObject8 = (c.g.a.b)f.b.a;
          c.g.a.m localm = (c.g.a.m)f.c.a;
          localObject7 = z.a((Cursor)localObject7, (c.g.a.b)localObject8, localm);
          long l5 = System.currentTimeMillis() - l4;
          localObject6 = new String[k];
          localObject9 = new java/lang/StringBuilder;
          ((StringBuilder)localObject9).<init>("query for aggregated contacts returned ");
          localObject10 = localObject7;
          int n = ((Collection)localObject7).size();
          ((StringBuilder)localObject9).append(n);
          localObject10 = " items, took: ";
          ((StringBuilder)localObject9).append((String)localObject10);
          ((StringBuilder)localObject9).append(l5);
          localObject8 = "ms\nwhere = ";
          ((StringBuilder)localObject9).append((String)localObject8);
          ((StringBuilder)localObject9).append((String)localObject5);
          localObject5 = ((StringBuilder)localObject9).toString();
          localObject6[0] = localObject5;
          localObject5 = c.a.m.n((Iterable)localObject7);
          localObject6 = (c.g.a.b)f.d.a;
          k.b(localObject5, "receiver$0");
          k.b(localObject6, "selector");
          localObject9 = new c/m/c;
          ((c)localObject9).<init>((i)localObject5, (c.g.a.b)localObject6);
          localObject9 = (i)localObject9;
          localObject5 = new com/truecaller/data/access/f$a;
          ((f.a)localObject5).<init>((com.truecaller.data.access.f)localObject2, (long[])localObject1, (Uri)localObject4, (g)localObject3);
          localObject5 = (c.g.a.b)localObject5;
          localObject3 = c.m.l.d(c.m.l.c((i)localObject9, (c.g.a.b)localObject5));
          l4 = System.currentTimeMillis();
          localObject2 = a;
          localObject4 = TruecallerContract.a();
          localObject5 = new java/util/ArrayList;
          localObject7 = localObject3;
          localObject7 = (Collection)localObject3;
          ((ArrayList)localObject5).<init>((Collection)localObject7);
          ((ContentResolver)localObject2).applyBatch((String)localObject4, (ArrayList)localObject5);
          long l6 = System.currentTimeMillis() - l4;
          localObject2 = new String[k];
          localObject4 = new java/lang/StringBuilder;
          ((StringBuilder)localObject4).<init>("applying ");
          i = ((List)localObject3).size();
          ((StringBuilder)localObject4).append(i);
          ((StringBuilder)localObject4).append(" insertions into contact index, took: ");
          ((StringBuilder)localObject4).append(l6);
          ((StringBuilder)localObject4).append("ms");
          localObject3 = ((StringBuilder)localObject4).toString();
          localObject2[0] = localObject3;
          long l7 = System.currentTimeMillis() - l1;
          localObject4 = new String[k];
          localObject5 = new java/lang/StringBuilder;
          String str = "Indexing contacts for sorting for ids ";
          ((StringBuilder)localObject5).<init>(str);
          localObject1 = Arrays.toString((long[])localObject1);
          ((StringBuilder)localObject5).append((String)localObject1);
          ((StringBuilder)localObject5).append(",\ntook: ");
          ((StringBuilder)localObject5).append(l7);
          ((StringBuilder)localObject5).append("ms");
          localObject1 = ((StringBuilder)localObject5).toString();
          localObject4[0] = localObject1;
        }
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.RefreshContactIndexingService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */