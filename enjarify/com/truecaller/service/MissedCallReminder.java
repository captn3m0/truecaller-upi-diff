package com.truecaller.service;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

class MissedCallReminder
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  private static int sSequence;
  public String normalizedNumber;
  public int notificationId;
  public String rawNumber;
  public long timestamp;
  
  static
  {
    MissedCallReminder.1 local1 = new com/truecaller/service/MissedCallReminder$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  public MissedCallReminder() {}
  
  protected MissedCallReminder(Parcel paramParcel)
  {
    String str = paramParcel.readString();
    rawNumber = str;
    str = paramParcel.readString();
    normalizedNumber = str;
    long l = paramParcel.readLong();
    timestamp = l;
    int i = paramParcel.readInt();
    notificationId = i;
  }
  
  public MissedCallReminder(String paramString1, String paramString2, long paramLong)
  {
    rawNumber = paramString1;
    normalizedNumber = paramString2;
    timestamp = paramLong;
    int i = sSequence;
    sSequence = i + 1;
    notificationId = i;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    String str = rawNumber;
    paramParcel.writeString(str);
    str = normalizedNumber;
    paramParcel.writeString(str);
    long l = timestamp;
    paramParcel.writeLong(l);
    paramInt = notificationId;
    paramParcel.writeInt(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.MissedCallReminder
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */