package com.truecaller.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.support.v4.app.z.d;
import android.support.v4.content.b;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.Toast;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.common.h.aa;
import com.truecaller.data.entity.Contact;
import com.truecaller.filters.g;
import com.truecaller.log.d;
import com.truecaller.ui.TruecallerInit;
import com.truecaller.ui.components.FloatingWindow;
import com.truecaller.ui.components.FloatingWindow.DismissCause;
import java.lang.ref.WeakReference;

final class ClipboardService$a
  implements Handler.Callback
{
  private final WeakReference a;
  private Object b;
  private boolean c;
  private z.d d;
  
  ClipboardService$a(ClipboardService paramClipboardService)
  {
    Object localObject1 = new java/lang/ref/WeakReference;
    ((WeakReference)localObject1).<init>(paramClipboardService);
    a = ((WeakReference)localObject1);
    localObject1 = paramClipboardService.getApplicationContext();
    Object localObject2 = localObject1;
    localObject2 = ((bk)localObject1).a().aC().a();
    z.d locald = new android/support/v4/app/z$d;
    locald.<init>((Context)localObject1, (String)localObject2);
    int i = 2131234787;
    localObject2 = locald.a(i);
    int j = b.c((Context)localObject1, 2131100594);
    C = j;
    j = 0;
    localObject1 = null;
    localObject2 = ((z.d)localObject2).a(0, 0);
    int k = 1;
    l = k;
    int m = 2;
    ((z.d)localObject2).d(m);
    d = ((z.d)localObject2);
    localObject2 = paramClipboardService.getPackageManager();
    Object localObject3 = paramClipboardService.getPackageName();
    localObject2 = ((PackageManager)localObject2).getLaunchIntentForPackage((String)localObject3);
    if (localObject2 == null)
    {
      d.a("Could not get standard Intent for clipboard search service notification");
      localObject2 = TruecallerInit.a(paramClipboardService, "clipboard");
      m = 268435456;
      ((Intent)localObject2).setFlags(m);
    }
    try
    {
      localObject3 = d;
      int n = 2131364142;
      localObject1 = PendingIntent.getActivity(paramClipboardService, n, (Intent)localObject2, 0);
      f = ((PendingIntent)localObject1);
      return;
    }
    catch (RuntimeException localRuntimeException)
    {
      localObject1 = String.valueOf(localRuntimeException);
      d.a("Could not set PendingIntent for clipboard search service notification: ".concat((String)localObject1));
      a.clear();
      paramClipboardService.stopSelf();
      Toast.makeText(paramClipboardService, 2131887210, k).show();
    }
  }
  
  public final boolean handleMessage(Message paramMessage)
  {
    Object localObject1 = (ClipboardService)a.get();
    int i = 1;
    if (localObject1 != null)
    {
      Object localObject2 = ClipboardService.a((ClipboardService)localObject1);
      int j = what;
      boolean bool1 = false;
      Object localObject3 = null;
      Object localObject4 = null;
      Object localObject5;
      boolean bool3;
      switch (j)
      {
      default: 
        break;
      case 4: 
        boolean bool2 = c;
        if (bool2)
        {
          localObject2 = obj;
          if (localObject2 != null)
          {
            paramMessage = obj;
            localObject2 = b;
            if (paramMessage != localObject2) {
              break;
            }
          }
          else
          {
            b = null;
            c = false;
            ((ClipboardService)localObject1).stopForeground(i);
          }
        }
        break;
      case 3: 
        localObject2 = aa.e(paramMessage.getData().getString("number"));
        paramMessage = obj;
        b = paramMessage;
        paramMessage = d;
        localObject4 = new Object[i];
        localObject4[0] = localObject2;
        localObject5 = ((ClipboardService)localObject1).getString(2131886393, (Object[])localObject4);
        paramMessage.a((CharSequence)localObject5);
        paramMessage = d;
        j = 2131886392;
        localObject4 = new Object[i];
        localObject4[0] = localObject2;
        localObject2 = ((ClipboardService)localObject1).getString(j, (Object[])localObject4);
        paramMessage.d((CharSequence)localObject2);
        c = i;
        int k = 2131362506;
        localObject2 = d.h();
        ((ClipboardService)localObject1).startForeground(k, (Notification)localObject2);
        break;
      case 2: 
        paramMessage = b;
        if (paramMessage != null)
        {
          paramMessage = b;
          bool3 = j;
          localObject2 = b.a;
          localObject5 = b.b;
          localObject3 = b.c;
          com.truecaller.ui.components.e locale = b;
          Object localObject6 = f;
          if (localObject6 != null)
          {
            f.setOnTouchListener(null);
            localObject6 = e;
            FrameLayout localFrameLayout = f;
            ((WindowManager)localObject6).removeView(localFrameLayout);
          }
          localObject6 = g;
          if (localObject6 != null)
          {
            g.removeMessages(i);
            localObject6 = g;
            int m = 2;
            ((Handler)localObject6).removeMessages(m);
            g = null;
          }
          b = null;
          if ((localObject2 != null) && (localObject3 != null))
          {
            localObject4 = ((ClipboardService)localObject1).a();
            ((com.truecaller.ui.components.e)localObject4).a((String)localObject5, (Contact)localObject2, (g)localObject3);
          }
          if (bool3)
          {
            paramMessage = ((ClipboardService)localObject1).a();
            paramMessage.c();
          }
        }
        break;
      case 1: 
        paramMessage = (ClipboardService.a.a)obj;
        j = 3;
        ((Handler)localObject2).removeMessages(j);
        localObject2 = a;
        localObject5 = b;
        paramMessage = c;
        localObject3 = ((ClipboardService)localObject1).a();
        bool1 = j;
        if (!bool1)
        {
          localObject3 = ((ClipboardService)localObject1).a();
          ((com.truecaller.ui.components.e)localObject3).c();
        }
        ((ClipboardService)localObject1).b(null);
        localObject1 = ((ClipboardService)localObject1).a();
        ((com.truecaller.ui.components.e)localObject1).a((String)localObject2, (Contact)localObject5, paramMessage);
        break;
      case 0: 
        paramMessage = ((ClipboardService)localObject1).a();
        bool3 = j;
        if (bool3)
        {
          paramMessage = ((ClipboardService)localObject1).a();
          localObject1 = FloatingWindow.DismissCause.UNDEFINED;
          paramMessage.b((FloatingWindow.DismissCause)localObject1);
        }
        break;
      }
    }
    return i;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.ClipboardService.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */