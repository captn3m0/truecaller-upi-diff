package com.truecaller.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.support.v4.app.z.c;
import android.support.v4.app.z.d;
import android.support.v4.content.b;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.notificationchannels.e;

public enum AlarmReceiver$AlarmType
{
  private final String mAnalyticsSubtype;
  private final long mFirstDelay;
  private final int mNotificationId;
  private final String mNotificationType;
  private final long mRecurringPeriod;
  
  static
  {
    Object localObject = new com/truecaller/service/AlarmReceiver$AlarmType$1;
    ((AlarmReceiver.AlarmType.1)localObject).<init>("TYPE_5DAYS", "add_photo", "addPhoto");
    TYPE_5DAYS = (AlarmType)localObject;
    localObject = new com/truecaller/service/AlarmReceiver$AlarmType$2;
    String str1 = TYPE_5DAYS.getNotificationType();
    String str2 = TYPE_5DAYS.getAnalyticsSubtype();
    ((AlarmReceiver.AlarmType.2)localObject).<init>("TYPE_15DAYS", str1, str2);
    TYPE_15DAYS = (AlarmType)localObject;
    localObject = new com/truecaller/service/AlarmReceiver$AlarmType$3;
    ((AlarmReceiver.AlarmType.3)localObject).<init>("TYPE_20DAYS", "share", "shareTc");
    TYPE_20DAYS = (AlarmType)localObject;
    localObject = new com/truecaller/service/AlarmReceiver$AlarmType$4;
    str1 = TYPE_20DAYS.getNotificationType();
    str2 = TYPE_20DAYS.getAnalyticsSubtype();
    ((AlarmReceiver.AlarmType.4)localObject).<init>("TYPE_2DAYS_UPGRADED", str1, str2);
    TYPE_2DAYS_UPGRADED = (AlarmType)localObject;
    localObject = new com/truecaller/service/AlarmReceiver$AlarmType$5;
    ((AlarmReceiver.AlarmType.5)localObject).<init>("TYPE_RESCHEDULE");
    TYPE_RESCHEDULE = (AlarmType)localObject;
    localObject = new com/truecaller/service/AlarmReceiver$AlarmType$6;
    ((AlarmReceiver.AlarmType.6)localObject).<init>("TYPE_UPDATE_SPAM", "update_spam", "openBlock");
    TYPE_UPDATE_SPAM = (AlarmType)localObject;
    localObject = new com/truecaller/service/AlarmReceiver$AlarmType$7;
    ((AlarmReceiver.AlarmType.7)localObject).<init>("TYPE_DO_NOT_DISTURB_ACCESS", "do_not_disturb", "muteCalls");
    TYPE_DO_NOT_DISTURB_ACCESS = (AlarmType)localObject;
    localObject = new com/truecaller/service/AlarmReceiver$AlarmType$8;
    ((AlarmReceiver.AlarmType.8)localObject).<init>("TYPE_NOTIFICATION_ACCESS", "notification_access", "messagingApps");
    TYPE_NOTIFICATION_ACCESS = (AlarmType)localObject;
    localObject = new com/truecaller/service/AlarmReceiver$AlarmType$9;
    ((AlarmReceiver.AlarmType.9)localObject).<init>("TYPE_DISMISS_NOTIFICATION");
    TYPE_DISMISS_NOTIFICATION = (AlarmType)localObject;
    localObject = new AlarmType[9];
    AlarmType localAlarmType = TYPE_5DAYS;
    localObject[0] = localAlarmType;
    localAlarmType = TYPE_15DAYS;
    localObject[1] = localAlarmType;
    localAlarmType = TYPE_20DAYS;
    localObject[2] = localAlarmType;
    localAlarmType = TYPE_2DAYS_UPGRADED;
    localObject[3] = localAlarmType;
    localAlarmType = TYPE_RESCHEDULE;
    localObject[4] = localAlarmType;
    localAlarmType = TYPE_UPDATE_SPAM;
    localObject[5] = localAlarmType;
    localAlarmType = TYPE_DO_NOT_DISTURB_ACCESS;
    localObject[6] = localAlarmType;
    localAlarmType = TYPE_NOTIFICATION_ACCESS;
    localObject[7] = localAlarmType;
    localAlarmType = TYPE_DISMISS_NOTIFICATION;
    localObject[8] = localAlarmType;
    $VALUES = (AlarmType[])localObject;
  }
  
  private AlarmReceiver$AlarmType(long paramLong, int paramInt1, String paramString2, String paramString3)
  {
    this(paramLong, 0L, paramInt1, paramString2, paramString3);
  }
  
  private AlarmReceiver$AlarmType(long paramLong1, long paramLong2, int paramInt1, String paramString2, String paramString3)
  {
    mFirstDelay = paramLong1;
    mRecurringPeriod = paramLong2;
    mNotificationId = paramInt1;
    mNotificationType = paramString2;
    mAnalyticsSubtype = paramString3;
  }
  
  private static Notification createNotification(Context paramContext, String paramString1, String paramString2, PendingIntent paramPendingIntent)
  {
    z.d locald = new android/support/v4/app/z$d;
    String str = getNotificationChannelId(paramContext);
    locald.<init>(paramContext, str);
    locald = locald.a(2131234787);
    int i = b.c(paramContext, 2131100594);
    C = i;
    paramContext = paramContext.getString(2131886117);
    paramContext = locald.d(paramContext).a(paramString1);
    paramString1 = new android/support/v4/app/z$c;
    paramString1.<init>();
    paramString1 = paramString1.b(paramString2);
    paramContext = paramContext.a(paramString1).b(paramString2).c(1);
    f = paramPendingIntent;
    paramContext.d(16);
    return paramContext.h();
  }
  
  private static String getNotificationChannelId(Context paramContext)
  {
    return ((bk)paramContext.getApplicationContext()).a().aC().a();
  }
  
  public String getAnalyticsSubtype()
  {
    return mAnalyticsSubtype;
  }
  
  public long getFirstDelay()
  {
    return mFirstDelay;
  }
  
  public abstract Notification getNotification(Context paramContext);
  
  public int getNotificationId()
  {
    return mNotificationId;
  }
  
  public String getNotificationType()
  {
    return mNotificationType;
  }
  
  public long getRecurringPeriod()
  {
    return mRecurringPeriod;
  }
  
  public boolean shouldShow(Context paramContext)
  {
    return true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.AlarmReceiver.AlarmType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */