package com.truecaller.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import com.truecaller.search.local.model.g;

public class DataManagerService
  extends Service
{
  private boolean a;
  private g b;
  
  public IBinder onBind(Intent paramIntent)
  {
    new String[1][0] = "onBind";
    boolean bool = true;
    a = bool;
    b.a(bool);
    paramIntent = new android/os/Binder;
    paramIntent.<init>();
    return paramIntent;
  }
  
  public void onCreate()
  {
    super.onCreate();
    g localg = g.a(this);
    b = localg;
    new String[1][0] = "Service created";
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    b.a(false);
    b.a();
    new String[1][0] = "Service destroyed";
  }
  
  public void onRebind(Intent paramIntent)
  {
    new String[1][0] = "onRebind";
    boolean bool = true;
    a = bool;
    b.a(bool);
  }
  
  public int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
  {
    return 2;
  }
  
  public void onTrimMemory(int paramInt)
  {
    super.onTrimMemory(paramInt);
    String[] arrayOfString = new String[1];
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("Trim memory: level=");
    ((StringBuilder)localObject).append(paramInt);
    ((StringBuilder)localObject).append(", isBound=");
    boolean bool1 = a;
    ((StringBuilder)localObject).append(bool1);
    localObject = ((StringBuilder)localObject).toString();
    bool1 = false;
    arrayOfString[0] = localObject;
    boolean bool2 = a;
    if (bool2) {
      return;
    }
    int i = 60;
    if (paramInt >= i)
    {
      b.a();
      return;
    }
    i = 80;
    if (paramInt >= i) {
      stopSelf();
    }
  }
  
  public boolean onUnbind(Intent paramIntent)
  {
    new String[1][0] = "onUnbind";
    a = false;
    b.a(false);
    return true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.DataManagerService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */