package com.truecaller.service;

import android.app.Notification;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import kotlinx.coroutines.ag;

final class MissedCallsNotificationService$g
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag e;
  
  MissedCallsNotificationService$g(MissedCallsNotificationService paramMissedCallsNotificationService, Notification paramNotification, String paramString, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    g localg = new com/truecaller/service/MissedCallsNotificationService$g;
    MissedCallsNotificationService localMissedCallsNotificationService = b;
    Notification localNotification = c;
    String str = d;
    localg.<init>(localMissedCallsNotificationService, localNotification, str, paramc);
    paramObject = (ag)paramObject;
    e = ((ag)paramObject);
    return localg;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = c.d.a.a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = b.n;
        if (paramObject == null)
        {
          localObject = "notificationManager";
          c.g.b.k.a((String)localObject);
        }
        Notification localNotification = c;
        String str = d;
        ((com.truecaller.notifications.a)paramObject).a("missedCall", 12345, localNotification, str);
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (g)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((g)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.MissedCallsNotificationService.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */