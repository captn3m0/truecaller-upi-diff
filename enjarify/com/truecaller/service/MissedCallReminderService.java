package com.truecaller.service;

import android.app.IntentService;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.support.v4.app.ac;
import android.support.v4.app.aj;
import android.support.v4.app.z.d;
import android.text.TextUtils;
import com.truecaller.TrueApp;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.calling.initiate_call.b.a;
import com.truecaller.calling.initiate_call.b.a.a;
import com.truecaller.data.access.c;
import com.truecaller.data.entity.Contact;
import com.truecaller.notifications.a;
import com.truecaller.old.data.access.Settings;
import com.truecaller.ui.TruecallerInit;
import com.truecaller.ui.details.DetailsFragment;
import com.truecaller.ui.details.DetailsFragment.SourceType;
import com.truecaller.util.at;

public class MissedCallReminderService
  extends IntentService
{
  public MissedCallReminderService()
  {
    super("MissedCallReminderService");
  }
  
  public static Intent a(Context paramContext, MissedCallReminder paramMissedCallReminder)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putParcelable("reminder", paramMissedCallReminder);
    paramMissedCallReminder = new android/content/Intent;
    paramMissedCallReminder.<init>("com.truecaller.intent.action.MISSED_CALL_POST_REMINDER", null, paramContext, MissedCallReminderService.class);
    return paramMissedCallReminder.putExtra("reminderBundle", localBundle);
  }
  
  protected void onHandleIntent(Intent paramIntent)
  {
    MissedCallReminderService localMissedCallReminderService = this;
    Object localObject1 = paramIntent;
    if (paramIntent == null) {
      return;
    }
    Object localObject2 = TrueApp.y().a().Z();
    Object localObject3 = paramIntent.getAction();
    boolean bool1 = TextUtils.isEmpty((CharSequence)localObject3);
    if (!bool1)
    {
      Object localObject4 = "reminderBundle";
      bool1 = paramIntent.hasExtra((String)localObject4);
      int j = 0;
      String str1 = null;
      int k;
      if (bool1)
      {
        localObject4 = "reminderBundle";
        localObject1 = paramIntent.getBundleExtra((String)localObject4);
        if (localObject1 != null)
        {
          localObject4 = "reminder";
          localObject1 = (MissedCallReminder)((Bundle)localObject1).getParcelable((String)localObject4);
        }
        else
        {
          k = 0;
          localObject1 = null;
        }
      }
      else
      {
        localObject4 = "reminder";
        localObject1 = (MissedCallReminder)paramIntent.getParcelableExtra((String)localObject4);
      }
      if (localObject1 != null)
      {
        localObject4 = rawNumber;
        if (localObject4 != null)
        {
          long l1 = timestamp;
          long l2 = 0L;
          bool1 = l1 < l2;
          if (bool1)
          {
            int i = -1;
            int m = ((String)localObject3).hashCode();
            int n = 2;
            Object localObject5 = null;
            boolean bool2 = true;
            Object localObject6;
            boolean bool3;
            switch (m)
            {
            default: 
              break;
            case 1808750213: 
              localObject6 = "com.truecaller.intent.action.MISSED_CALL_REMINDER_CLICKED";
              bool3 = ((String)localObject3).equals(localObject6);
              if (bool3) {
                i = 3;
              }
              break;
            case 871229319: 
              localObject6 = "com.truecaller.intent.action.MISSED_CALL_REMINDER_DISMISSED";
              bool3 = ((String)localObject3).equals(localObject6);
              if (bool3) {
                i = 4;
              }
              break;
            case 186833215: 
              localObject6 = "com.truecaller.intent.action.MISSED_CALL_REMINDER_CALLED";
              bool3 = ((String)localObject3).equals(localObject6);
              if (bool3) {
                i = 2;
              }
              break;
            case -1107888740: 
              localObject6 = "com.truecaller.intent.action.MISSED_CALL_REMINDER_SNOOZED";
              bool3 = ((String)localObject3).equals(localObject6);
              if (bool3) {
                i = 1;
              }
              break;
            case -1301312570: 
              localObject6 = "com.truecaller.intent.action.MISSED_CALL_POST_REMINDER";
              bool3 = ((String)localObject3).equals(localObject6);
              if (bool3)
              {
                i = 0;
                localObject4 = null;
              }
              break;
            }
            int i2 = 268435456;
            int i1;
            Object localObject7;
            switch (i)
            {
            default: 
              break;
            case 4: 
              localObject3 = ac.a(this);
              i = notificationId;
              ((ac)localObject3).a(null, i);
              localObject1 = normalizedNumber;
              ((e)localObject2).a((String)localObject1);
              break;
            case 3: 
              localObject2 = ac.a(this);
              i1 = notificationId;
              ((ac)localObject2).a(null, i1);
              localObject4 = normalizedNumber;
              str1 = rawNumber;
              localObject7 = DetailsFragment.SourceType.MissedCallReminder;
              localObject1 = this;
              localObject1 = DetailsFragment.a(this, null, null, (String)localObject4, str1, null, (DetailsFragment.SourceType)localObject7, false, true, 10).addFlags(i2);
              aj.a(this).b((Intent)localObject1).a();
              return;
            case 2: 
              localObject3 = ac.a(this);
              i = notificationId;
              ((ac)localObject3).a(null, i);
              localObject3 = normalizedNumber;
              ((e)localObject2).a((String)localObject3);
              localObject2 = new android/content/Intent;
              ((Intent)localObject2).<init>("android.intent.action.CLOSE_SYSTEM_DIALOGS");
              localMissedCallReminderService.sendBroadcast((Intent)localObject2);
              localObject2 = TruecallerInit.a(localMissedCallReminderService, "notificationCalls").addFlags(335544320);
              localMissedCallReminderService.startActivity((Intent)localObject2);
              localObject2 = ((bk)getApplicationContext()).a().bM();
              localObject3 = new com/truecaller/calling/initiate_call/b$a$a;
              localObject1 = rawNumber;
              ((b.a.a)localObject3).<init>((String)localObject1, "notificationMissedCallReminder");
              localObject1 = ((b.a.a)localObject3).a();
              ((com.truecaller.calling.initiate_call.b)localObject2).a((b.a)localObject1);
              return;
            case 1: 
              localObject2 = new android/os/Handler;
              localObject3 = Looper.getMainLooper();
              ((Handler)localObject2).<init>((Looper)localObject3);
              localObject3 = new com/truecaller/service/-$$Lambda$MissedCallReminderService$esW7beA7PgV73SaF5TN7JT_ufRI;
              ((-..Lambda.MissedCallReminderService.esW7beA7PgV73SaF5TN7JT_ufRI)localObject3).<init>(localMissedCallReminderService);
              ((Handler)localObject2).post((Runnable)localObject3);
              localObject2 = ac.a(this);
              k = notificationId;
              ((ac)localObject2).a(null, k);
              localObject1 = new android/content/Intent;
              ((Intent)localObject1).<init>("android.intent.action.CLOSE_SYSTEM_DIALOGS");
              localMissedCallReminderService.sendBroadcast((Intent)localObject1);
              return;
            case 0: 
              localObject2 = "showMissedCallReminders";
              boolean bool4 = Settings.e((String)localObject2);
              if (!bool4) {
                break;
              }
              long l3 = System.currentTimeMillis();
              long l4 = timestamp;
              l3 -= l4;
              float f = (float)l3 / 3600000.0F;
              int i3 = Math.round(f);
              i1 = 12;
              if ((i3 <= i1) && (i3 > 0))
              {
                localObject4 = new com/truecaller/data/access/c;
                ((c)localObject4).<init>(localMissedCallReminderService);
                localObject6 = normalizedNumber;
                localObject4 = ((c)localObject4).b((String)localObject6);
                if (localObject4 != null) {
                  localObject6 = ((Contact)localObject4).s();
                } else {
                  localObject6 = rawNumber;
                }
                Object localObject8 = getResources();
                int i4 = 2131755023;
                localObject7 = new Object[n];
                localObject7[0] = localObject6;
                localObject6 = Integer.valueOf(i3);
                localObject7[bool2] = localObject6;
                localObject6 = ((Resources)localObject8).getQuantityString(i4, i3, (Object[])localObject7);
                if (localObject4 != null)
                {
                  localObject4 = ((Contact)localObject4).a(bool2);
                }
                else
                {
                  i = 0;
                  localObject4 = null;
                }
                if (localObject4 != null)
                {
                  localObject4 = ((Uri)localObject4).toString();
                }
                else
                {
                  i = 0;
                  localObject4 = null;
                }
                localObject4 = at.a(localMissedCallReminderService, (String)localObject4);
                if (localObject4 == null)
                {
                  localObject4 = getResources();
                  n = 2131689472;
                  localObject4 = BitmapFactory.decodeResource((Resources)localObject4, n);
                }
                n = android.support.v4.content.b.c(localMissedCallReminderService, 2131100594);
                localObject5 = new android/content/Intent;
                ((Intent)localObject5).<init>("com.truecaller.intent.action.MISSED_CALL_REMINDER_CLICKED", null, localMissedCallReminderService, MissedCallReminderService.class);
                localObject5 = ((Intent)localObject5).putExtra("reminder", (Parcelable)localObject1);
                int i5 = notificationId;
                localObject5 = PendingIntent.getService(localMissedCallReminderService, i5, (Intent)localObject5, i2);
                localObject8 = new android/content/Intent;
                ((Intent)localObject8).<init>("com.truecaller.intent.action.MISSED_CALL_REMINDER_SNOOZED", null, localMissedCallReminderService, MissedCallReminderService.class);
                localObject8 = ((Intent)localObject8).putExtra("reminder", (Parcelable)localObject1);
                i4 = notificationId;
                localObject8 = PendingIntent.getService(localMissedCallReminderService, i4, (Intent)localObject8, i2);
                Object localObject9 = new android/content/Intent;
                ((Intent)localObject9).<init>("com.truecaller.intent.action.MISSED_CALL_REMINDER_DISMISSED", null, localMissedCallReminderService, MissedCallReminderService.class);
                localObject9 = ((Intent)localObject9).putExtra("reminder", (Parcelable)localObject1);
                int i6 = notificationId;
                localObject9 = PendingIntent.getService(localMissedCallReminderService, i6, (Intent)localObject9, i2);
                Object localObject10 = new android/content/Intent;
                ((Intent)localObject10).<init>("com.truecaller.intent.action.MISSED_CALL_REMINDER_CALLED", null, localMissedCallReminderService, MissedCallReminderService.class);
                localObject10 = ((Intent)localObject10).putExtra("reminder", (Parcelable)localObject1);
                int i7 = notificationId;
                PendingIntent localPendingIntent = PendingIntent.getService(localMissedCallReminderService, i7, (Intent)localObject10, i2);
                localObject10 = ((bk)getApplicationContext()).a();
                String str2 = ((bp)localObject10).aD().d();
                z.d locald = new android/support/v4/app/z$d;
                locald.<init>(localMissedCallReminderService, str2);
                locald.a(2131234118);
                i7 = 2131886704;
                str2 = localMissedCallReminderService.getString(i7);
                locald.a(str2);
                locald.b((CharSequence)localObject6);
                locald.a((Bitmap)localObject4);
                m = bool2;
                long l5 = timestamp;
                locald.a(l5);
                locald.a(n, 500, 3000);
                C = n;
                f = ((PendingIntent)localObject5);
                locald.b((PendingIntent)localObject9);
                j = 2131234319;
                m = 2131886701;
                localObject6 = localMissedCallReminderService.getString(m);
                locald.a(j, (CharSequence)localObject6, localPendingIntent);
                if (i3 < i1)
                {
                  i1 = 2131886702;
                  localObject3 = localMissedCallReminderService.getString(i1);
                  locald.a(2131234339, (CharSequence)localObject3, (PendingIntent)localObject8);
                  i3 = 0;
                  f = 0.0F;
                  localObject2 = null;
                }
                else
                {
                  i3 = 0;
                  f = 0.0F;
                  localObject2 = null;
                }
                locald.a(null);
                locald.a(null);
                localObject2 = locald.h();
                localObject3 = ((bp)localObject10).W();
                k = notificationId;
                str1 = "notificationMissedCallReminder";
                ((a)localObject3).a(k, (Notification)localObject2, str1);
                if (localObject4 != null) {
                  ((Bitmap)localObject4).recycle();
                }
                return;
              }
              return;
            }
          }
        }
      }
      ((e)localObject2).a();
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.MissedCallReminderService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */