package com.truecaller.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.truecaller.debug.log.a;

public class AccountsChangedReceiver
  extends BroadcastReceiver
{
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    paramContext = "android.accounts.LOGIN_ACCOUNTS_CHANGED";
    paramIntent = paramIntent.getAction();
    boolean bool = paramContext.equals(paramIntent);
    if (bool)
    {
      bool = true;
      paramContext = new Object[bool];
      paramIntent = null;
      String str = "Account was changed (LOGIN_ACCOUNTS_CHANGED_ACTION).";
      paramContext[0] = str;
      a.a(paramContext);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.AccountsChangedReceiver
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */