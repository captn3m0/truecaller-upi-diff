package com.truecaller.service;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.os.Bundle;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService.RemoteViewsFactory;
import com.d.b.aa.a;
import com.truecaller.androidactors.f;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.callhistory.z;
import com.truecaller.calling.after_call.AfterCallActivity;
import com.truecaller.calling.e.e;
import com.truecaller.common.h.aa;
import com.truecaller.common.h.am;
import com.truecaller.common.h.aq.d;
import com.truecaller.common.h.j;
import com.truecaller.common.h.k;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.log.AssertionUtil;
import com.truecaller.voip.d;

final class WidgetListService$b
  implements RemoteViewsService.RemoteViewsFactory
{
  private final Context a;
  private final int b;
  private z c;
  private final AppWidgetManager d;
  private final android.support.v4.e.a e;
  private final d f;
  private final e g;
  private final int h;
  private final int i;
  private final f j;
  
  public WidgetListService$b(Context paramContext, Intent paramIntent)
  {
    a = paramContext;
    int k = paramIntent.getIntExtra("appWidgetId", 0);
    b = k;
    Object localObject = AppWidgetManager.getInstance(paramContext);
    d = ((AppWidgetManager)localObject);
    localObject = android.support.v4.e.a.a();
    e = ((android.support.v4.e.a)localObject);
    k = paramIntent.getIntExtra("extra_widget_layout", 2131559239);
    h = k;
    int m = paramIntent.getIntExtra("extra_list_item_layout", 2131559240);
    i = m;
    paramContext = (bk)paramContext.getApplicationContext();
    paramIntent = paramContext.a().ad();
    j = paramIntent;
    paramIntent = paramContext.a().cb();
    f = paramIntent;
    paramContext = paramContext.a().n();
    g = paramContext;
  }
  
  private Bitmap a(Contact paramContact)
  {
    if (paramContact == null) {
      return null;
    }
    try
    {
      Object localObject = a;
      localObject = com.d.b.w.a((Context)localObject);
      boolean bool = true;
      paramContact = paramContact.a(bool);
      paramContact = ((com.d.b.w)localObject).a(paramContact);
      int k = 2131166325;
      paramContact = paramContact.a(k, k);
      localObject = Bitmap.Config.RGB_565;
      aa.a locala = b;
      g = ((Bitmap.Config)localObject);
      paramContact = paramContact.d();
      if (paramContact != null) {
        return aq.d.b().a(paramContact);
      }
      return null;
    }
    catch (Exception localException) {}
    return null;
  }
  
  public final int getCount()
  {
    try
    {
      z localz = c;
      if (localz == null)
      {
        k = 0;
        localz = null;
        return 0;
      }
      localz = c;
      int k = localz.getCount();
      int m = 20;
      k = Math.min(k, m);
      return k;
    }
    finally {}
  }
  
  public final long getItemId(int paramInt)
  {
    try
    {
      z localz1 = c;
      if (localz1 != null)
      {
        localz1 = c;
        paramInt = localz1.moveToPosition(paramInt);
        if (paramInt != 0)
        {
          z localz2 = c;
          l = localz2.a();
          return l;
        }
      }
      long l = 0L;
      return l;
    }
    finally {}
  }
  
  public final RemoteViews getLoadingView()
  {
    RemoteViews localRemoteViews = new android/widget/RemoteViews;
    Object localObject = a.getPackageName();
    localRemoteViews.<init>((String)localObject, 2131559241);
    localObject = a.getText(2131887685);
    localRemoteViews.setTextViewText(2131363687, (CharSequence)localObject);
    return localRemoteViews;
  }
  
  public final RemoteViews getViewAt(int paramInt)
  {
    RemoteViews localRemoteViews = new android/widget/RemoteViews;
    Object localObject1 = a.getPackageName();
    int k = i;
    localRemoteViews.<init>((String)localObject1, k);
    try
    {
      localObject1 = c;
      k = 8;
      int m = 2131365549;
      if (localObject1 != null)
      {
        localObject1 = c;
        paramInt = ((z)localObject1).moveToPosition(paramInt);
        if (paramInt != 0)
        {
          Object localObject2 = c;
          localObject2 = ((z)localObject2).d();
          if (localObject2 != null)
          {
            localObject1 = b;
            boolean bool1 = com.truecaller.common.h.ab.d((String)localObject1);
            if (bool1)
            {
              bool1 = false;
              localObject1 = null;
              localRemoteViews.setViewVisibility(m, 0);
              Object localObject4 = f;
              Object localObject5 = d;
              int n = b;
              localObject5 = ((AppWidgetManager)localObject5).getAppWidgetOptions(n);
              String str1 = "widgetSizeKey";
              int i1 = ((Bundle)localObject5).getInt(str1);
              n = 1;
              if (i1 == 0)
              {
                i1 = 1;
              }
              else
              {
                i1 = 0;
                localObject5 = null;
              }
              int i4 = 2;
              int i7 = 2131364194;
              Bitmap localBitmap;
              if (i1 != 0)
              {
                localRemoteViews.setViewVisibility(i7, 0);
                localBitmap = a((Contact)localObject4);
                i1 = p;
                if (i1 == i4)
                {
                  k = 2131233883;
                  localRemoteViews.setImageViewResource(i7, k);
                }
                else
                {
                  i1 = p;
                  if (i1 == n)
                  {
                    k = 2131234967;
                    localRemoteViews.setImageViewResource(i7, k);
                  }
                  else if (localBitmap != null)
                  {
                    localRemoteViews.setImageViewBitmap(i7, localBitmap);
                  }
                  else
                  {
                    k = 2131233847;
                    localRemoteViews.setImageViewResource(i7, k);
                  }
                }
              }
              else
              {
                localRemoteViews.setViewVisibility(i7, k);
                k = 0;
                localBitmap = null;
                localRemoteViews.setImageViewBitmap(i7, null);
              }
              k = 2131364196;
              int i2 = p;
              i7 = 3;
              int i3;
              if (i2 == n)
              {
                i3 = 2131235009;
              }
              else
              {
                i3 = p;
                if (i3 == i7)
                {
                  i3 = 2131235013;
                }
                else
                {
                  i3 = o;
                  switch (i3)
                  {
                  default: 
                    i3 = 2131235011;
                    break;
                  case 3: 
                    i3 = 2131235012;
                    break;
                  case 2: 
                    i3 = 2131235014;
                    break;
                  case 1: 
                    i3 = 2131235010;
                  }
                }
              }
              localRemoteViews.setImageViewResource(k, i3);
              k = 2131364195;
              localObject5 = a;
              int i8 = p;
              int i9;
              if (i8 == n)
              {
                i9 = 2131887339;
                localObject4 = ((Context)localObject5).getString(i9);
              }
              else if (i8 == i7)
              {
                i9 = 2131886779;
                localObject4 = ((Context)localObject5).getString(i9);
              }
              else if (i8 == i4)
              {
                i9 = 2131887340;
                localObject4 = ((Context)localObject5).getString(i9);
              }
              else
              {
                if (localObject4 != null)
                {
                  bool2 = ((Contact)localObject4).Q();
                  if (bool2)
                  {
                    bool2 = ((Contact)localObject4).R();
                    if (!bool2) {}
                  }
                  else
                  {
                    localObject6 = ((Contact)localObject4).z();
                    bool2 = am.b((CharSequence)localObject6);
                    if (bool2)
                    {
                      localObject4 = c;
                      localObject6 = b;
                      localObject4 = (String)am.e((CharSequence)localObject4, (CharSequence)localObject6);
                      localObject5 = k.f((Context)localObject5);
                      localObject4 = aa.d((String)localObject4, (String)localObject5);
                      break label647;
                    }
                    localObject4 = ((Contact)localObject4).t();
                    break label647;
                  }
                }
                i9 = 2131886587;
                localObject4 = ((Context)localObject5).getString(i9);
              }
              label647:
              localRemoteViews.setTextViewText(k, (CharSequence)localObject4);
              k = 2131364193;
              localObject4 = a;
              localObject5 = new java/lang/StringBuilder;
              ((StringBuilder)localObject5).<init>();
              Object localObject6 = e;
              String str2 = c;
              String str3 = b;
              str2 = (String)am.e(str2, str3);
              str3 = k.f((Context)localObject4);
              str2 = aa.d(str2, str3);
              localObject6 = ((android.support.v4.e.a)localObject6).a(str2);
              ((StringBuilder)localObject5).append((String)localObject6);
              localObject6 = f;
              boolean bool2 = ((d)localObject6).a();
              if (bool2)
              {
                localObject6 = q;
                if (localObject6 != null)
                {
                  localObject6 = q;
                  str2 = "com.truecaller.voip.manager.VOIP";
                  bool2 = ((String)localObject6).equals(str2);
                  break label806;
                }
              }
              bool2 = false;
              localObject6 = null;
              label806:
              if (bool2)
              {
                int i5 = ((StringBuilder)localObject5).length();
                i7 = 2131889034;
                str2 = ((Context)localObject4).getString(i7);
                ((StringBuilder)localObject5).replace(0, i5, str2);
              }
              localObject6 = g;
              boolean bool3 = ((e)localObject6).b();
              if (bool3)
              {
                localObject6 = q;
                if (localObject6 != null)
                {
                  localObject6 = q;
                  str2 = "com.whatsapp";
                  bool3 = ((String)localObject6).equals(str2);
                  break label908;
                }
              }
              bool3 = false;
              localObject6 = null;
              label908:
              if (bool3)
              {
                int i6 = ((StringBuilder)localObject5).length();
                i7 = 2131886456;
                str2 = ((Context)localObject4).getString(i7);
                ((StringBuilder)localObject5).replace(0, i6, str2);
              }
              ((StringBuilder)localObject5).append(", ");
              long l1 = h;
              localObject1 = j.a((Context)localObject4, l1, n);
              ((StringBuilder)localObject5).append((CharSequence)localObject1);
              l1 = i;
              long l2 = 0L;
              bool1 = l1 < l2;
              if (bool1)
              {
                ((StringBuilder)localObject5).append(" (");
                localObject1 = j.c((Context)localObject4, l1);
                ((StringBuilder)localObject5).append((String)localObject1);
                localObject1 = ")";
                ((StringBuilder)localObject5).append((String)localObject1);
              }
              localObject1 = ((StringBuilder)localObject5).toString();
              localRemoteViews.setTextViewText(k, (CharSequence)localObject1);
              localObject2 = AfterCallActivity.a(a, (HistoryEvent)localObject2, n);
              ((Intent)localObject2).putExtra("widgetClick", n);
              localRemoteViews.setOnClickFillInIntent(m, (Intent)localObject2);
              return localRemoteViews;
            }
          }
          localRemoteViews.setViewVisibility(m, k);
          return localRemoteViews;
        }
      }
      localRemoteViews.setViewVisibility(m, k);
      return localRemoteViews;
    }
    finally {}
  }
  
  public final int getViewTypeCount()
  {
    return 1;
  }
  
  public final boolean hasStableIds()
  {
    return true;
  }
  
  public final void onCreate() {}
  
  public final void onDataSetChanged()
  {
    try
    {
      Object localObject1 = c;
      if (localObject1 != null)
      {
        localObject1 = c;
        ((z)localObject1).close();
        localObject1 = null;
        c = null;
      }
      try
      {
        localObject1 = j;
        localObject1 = ((f)localObject1).a();
        localObject1 = (com.truecaller.callhistory.a)localObject1;
        localObject1 = ((com.truecaller.callhistory.a)localObject1).b();
        localObject1 = ((com.truecaller.androidactors.w)localObject1).d();
        localObject1 = (z)localObject1;
        c = ((z)localObject1);
      }
      catch (InterruptedException localInterruptedException)
      {
        AssertionUtil.reportThrowableButNeverCrash(localInterruptedException);
      }
      RemoteViews localRemoteViews = new android/widget/RemoteViews;
      Object localObject3 = a.getPackageName();
      int k = h;
      localRemoteViews.<init>((String)localObject3, k);
      localObject3 = d;
      k = b;
      ((AppWidgetManager)localObject3).partiallyUpdateAppWidget(k, localRemoteViews);
      return;
    }
    finally {}
  }
  
  public final void onDestroy()
  {
    try
    {
      z localz = c;
      if (localz != null)
      {
        localz = c;
        boolean bool = localz.isClosed();
        if (!bool)
        {
          localz = c;
          localz.close();
          bool = false;
          localz = null;
          c = null;
        }
      }
      return;
    }
    finally {}
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.WidgetListService.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */