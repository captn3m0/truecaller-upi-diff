package com.truecaller.service;

import android.content.Context;
import c.d.f;
import c.g.a.m;
import c.g.b.k;
import c.o.b;
import com.truecaller.api.services.presence.v1.models.Availability;
import com.truecaller.api.services.presence.v1.models.Availability.Context;
import com.truecaller.data.entity.Contact;
import com.truecaller.ui.CallMeBackActivity;
import kotlinx.coroutines.g;
import org.a.a.x;

public final class b
  implements a
{
  final com.truecaller.data.access.c a;
  private final int b;
  private final f c;
  private final com.truecaller.search.local.model.c d;
  
  public b(f paramf, com.truecaller.data.access.c paramc, com.truecaller.search.local.model.c paramc1)
  {
    c = paramf;
    a = paramc;
    d = paramc1;
    b = 24;
  }
  
  public final Object a(Context paramContext, String paramString1, String paramString2, c.d.c paramc)
  {
    boolean bool1 = paramc instanceof b.a;
    if (bool1)
    {
      localObject1 = paramc;
      localObject1 = (b.a)paramc;
      int i = b;
      j = -1 << -1;
      i &= j;
      if (i != 0)
      {
        int k = b - j;
        b = k;
        break label83;
      }
    }
    Object localObject1 = new com/truecaller/service/b$a;
    ((b.a)localObject1).<init>(this, (c.d.c)paramc);
    label83:
    paramc = a;
    Object localObject2 = c.d.a.a.a;
    int j = b;
    int m = 1;
    switch (j)
    {
    default: 
      paramContext = new java/lang/IllegalStateException;
      paramContext.<init>("call to 'resume' before 'invoke' with coroutine");
      throw paramContext;
    case 1: 
      paramContext = f;
      paramString1 = paramContext;
      paramString1 = (String)paramContext;
      paramContext = (Context)e;
      paramString2 = (b)d;
      bool1 = paramc instanceof o.b;
      if (bool1) {
        throw a;
      }
      break;
    case 0: 
      boolean bool2 = paramc instanceof o.b;
      if (bool2) {
        break label504;
      }
      if (paramString2 == null) {
        return Boolean.FALSE;
      }
      paramc = c;
      localObject3 = new com/truecaller/service/b$b;
      ((b.b)localObject3).<init>(this, paramString2, null);
      localObject3 = (m)localObject3;
      d = this;
      e = paramContext;
      f = paramString1;
      g = paramString2;
      b = m;
      paramc = g.a(paramc, (m)localObject3, (c.d.c)localObject1);
      if (paramc == localObject2) {
        return localObject2;
      }
      paramString2 = this;
    }
    paramc = (Contact)paramc;
    if (paramc == null) {
      return Boolean.FALSE;
    }
    localObject1 = "withContext(asyncContext…Number) } ?: return false";
    k.a(paramc, (String)localObject1);
    bool1 = paramc.Z();
    if (!bool1) {
      return Boolean.FALSE;
    }
    localObject1 = d.a(paramc);
    if (localObject1 == null) {
      return Boolean.FALSE;
    }
    localObject2 = b;
    if (localObject2 == null) {
      return Boolean.FALSE;
    }
    Object localObject3 = d;
    if (localObject3 != null)
    {
      localObject2 = ((Availability)localObject2).b();
      localObject3 = Availability.Context.CALL;
      if (localObject2 == localObject3) {}
    }
    else
    {
      m = 0;
    }
    if (m != 0)
    {
      localObject2 = new org/a/a/h;
      localObject1 = (x)d;
      localObject3 = (x)org.a.a.b.ay_();
      ((org.a.a.h)localObject2).<init>((x)localObject1, (x)localObject3);
      long l1 = b / 3600000L;
      long l2 = b;
      boolean bool3 = l1 < l2;
      if (!bool3)
      {
        paramString1 = CallMeBackActivity.a(paramContext, paramc, paramString1, 0, "callMeBackPopupInApp");
        paramContext.startActivity(paramString1);
        return Boolean.TRUE;
      }
    }
    return Boolean.FALSE;
    label504:
    throw a;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */