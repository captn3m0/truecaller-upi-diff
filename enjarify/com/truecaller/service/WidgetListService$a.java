package com.truecaller.service;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

final class WidgetListService$a
{
  final Intent a;
  
  WidgetListService$a(Context paramContext, int paramInt)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, WidgetListService.class);
    a = localIntent;
    a.putExtra("appWidgetId", paramInt);
    paramContext = a;
    Uri localUri = Uri.parse(paramContext.toUri(1));
    paramContext.setData(localUri);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.WidgetListService.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */