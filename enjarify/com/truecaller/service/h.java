package com.truecaller.service;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Binder;
import android.os.IBinder;
import com.truecaller.log.AssertionUtil;

public final class h
  implements ServiceConnection
{
  private final Context a;
  private final h.a b;
  private final Intent c;
  private Binder d;
  
  private h(Context paramContext, Intent paramIntent)
  {
    a = paramContext;
    b = null;
    c = paramIntent;
  }
  
  public h(Context paramContext, Class paramClass)
  {
    this(paramContext, localIntent);
  }
  
  private boolean c()
  {
    Binder localBinder = d;
    return localBinder != null;
  }
  
  public final void a()
  {
    boolean bool = c();
    if (!bool) {
      try
      {
        Object localObject1 = a;
        Intent localIntent = c;
        localObject1 = ((Context)localObject1).startService(localIntent);
        int i = 1;
        Object localObject2;
        if (localObject1 != null)
        {
          localObject1 = a;
          localObject2 = c;
          bool = ((Context)localObject1).bindService((Intent)localObject2, this, i);
          if (bool) {}
        }
        else
        {
          localObject1 = new String[i];
          i = 0;
          localIntent = null;
          localObject2 = new java/lang/StringBuilder;
          Object localObject3 = "Could not start service ";
          ((StringBuilder)localObject2).<init>((String)localObject3);
          localObject3 = c;
          localObject3 = ((Intent)localObject3).getComponent();
          ((StringBuilder)localObject2).append(localObject3);
          localObject2 = ((StringBuilder)localObject2).toString();
          localObject1[0] = localObject2;
        }
        return;
      }
      catch (IllegalStateException localIllegalStateException)
      {
        AssertionUtil.reportThrowableButNeverCrash(localIllegalStateException);
      }
    }
  }
  
  public final void b()
  {
    boolean bool = c();
    if (bool)
    {
      bool = false;
      d = null;
      Context localContext = a;
      localContext.unbindService(this);
    }
  }
  
  public final void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
  {
    String[] arrayOfString = new String[1];
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Service ");
    localStringBuilder.append(paramComponentName);
    localStringBuilder.append(" connected");
    paramComponentName = localStringBuilder.toString();
    arrayOfString[0] = paramComponentName;
    paramIBinder = (Binder)paramIBinder;
    d = paramIBinder;
  }
  
  public final void onServiceDisconnected(ComponentName paramComponentName)
  {
    String[] arrayOfString = new String[1];
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Service ");
    localStringBuilder.append(paramComponentName);
    localStringBuilder.append(" disconnected");
    paramComponentName = localStringBuilder.toString();
    arrayOfString[0] = paramComponentName;
    d = null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */