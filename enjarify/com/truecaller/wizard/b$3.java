package com.truecaller.wizard;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.text.style.ClickableSpan;
import android.view.View;
import com.truecaller.common.d;
import com.truecaller.utils.i;
import org.c.a.a.a.a;

final class b$3
  extends ClickableSpan
{
  b$3(b paramb, String paramString) {}
  
  public final void onClick(View paramView)
  {
    Object localObject1 = b;
    boolean bool1 = ((b)localObject1).isAdded();
    if (!bool1) {
      return;
    }
    localObject1 = b.e(b);
    bool1 = ((i)localObject1).a();
    if (bool1)
    {
      paramView = b;
      localObject1 = new android/content/Intent;
      localObject2 = Uri.parse(a);
      ((Intent)localObject1).<init>("android.intent.action.VIEW", (Uri)localObject2);
      paramView.startActivity((Intent)localObject1);
      return;
    }
    localObject1 = d.b;
    String str1 = a;
    bool1 = a.a((Object[])localObject1, str1);
    str1 = null;
    if (bool1)
    {
      int i = R.string.Welcome_offlineToSTitle;
    }
    else
    {
      localObject1 = d.a;
      localObject2 = a;
      boolean bool2 = a.a((Object[])localObject1, localObject2);
      if (bool2)
      {
        j = R.string.Welcome_offlinePPTitle;
      }
      else
      {
        j = 0;
        localObject1 = null;
      }
    }
    Object localObject2 = b;
    AlertDialog.Builder localBuilder = new android/support/v7/app/AlertDialog$Builder;
    paramView = paramView.getContext();
    localBuilder.<init>(paramView);
    paramView = localBuilder.setTitle(j);
    localObject1 = b;
    int k = R.string.Welcome_offlineMessage;
    Object[] arrayOfObject = new Object[1];
    String str2 = a;
    arrayOfObject[0] = str2;
    localObject1 = ((b)localObject1).getString(k, arrayOfObject);
    paramView = paramView.setMessage((CharSequence)localObject1);
    int j = R.string.Close;
    paramView = paramView.setPositiveButton(j, null).create();
    b.a((b)localObject2, paramView);
    b.f(b).show();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.b.3
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */