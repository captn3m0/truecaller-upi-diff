package com.truecaller.wizard;

import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.w;
import android.support.v4.app.w.a;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.view.View;
import android.widget.TextView;
import c.g.a.m;
import com.google.c.a.g;
import com.truecaller.common.h.aa;
import com.truecaller.common.h.ac;
import com.truecaller.common.h.am;
import com.truecaller.common.h.h;
import com.truecaller.common.network.country.CountryListDto.a;
import com.truecaller.utils.extensions.p;
import com.truecaller.utils.l;
import com.truecaller.utils.t;
import com.truecaller.utils.t.a;
import com.truecaller.wizard.b.a.b;
import java.util.Locale;

public class b
  extends com.truecaller.wizard.b.i
  implements w.a
{
  private static final Locale e;
  protected final com.truecaller.analytics.a.d a;
  final com.truecaller.common.e.e b;
  protected CountryListDto.a c;
  public com.truecaller.wizard.utils.k d;
  private AlertDialog i;
  private AlertDialog j;
  private String k;
  
  static
  {
    Locale localLocale = new java/util/Locale;
    localLocale.<init>("en");
    e = localLocale;
  }
  
  public b()
  {
    Object localObject = new com/truecaller/analytics/a/d;
    ((com.truecaller.analytics.a.d)localObject).<init>();
    a = ((com.truecaller.analytics.a.d)localObject);
    localObject = new com/truecaller/common/e/e;
    ((com.truecaller.common.e.e)localObject).<init>();
    b = ((com.truecaller.common.e.e)localObject);
  }
  
  static String a(Context paramContext)
  {
    paramContext = com.truecaller.common.h.k.g(paramContext);
    boolean bool = am.b(paramContext);
    if (!bool) {
      return paramContext;
    }
    return "IN";
  }
  
  private boolean i()
  {
    Object localObject = c;
    if (localObject != null)
    {
      String str = c;
      if (str != null)
      {
        str = "br";
        localObject = c.toLowerCase();
        boolean bool = str.equals(localObject);
        if (bool) {
          return true;
        }
      }
      return false;
    }
    return false;
  }
  
  public final android.support.v4.content.c a(int paramInt, Bundle paramBundle)
  {
    int m = R.id.wizard_loader_suggestedCountry;
    Object localObject;
    if (paramInt == m)
    {
      localObject = new com/truecaller/wizard/b$a;
      paramBundle = getContext();
      ((b.a)localObject).<init>(paramBundle);
      return (android.support.v4.content.c)localObject;
    }
    m = R.id.wizard_loader_autologin;
    if (paramInt == m)
    {
      localObject = new com/truecaller/wizard/utils/a;
      paramBundle = getContext();
      ((com.truecaller.wizard.utils.a)localObject).<init>(paramBundle);
      return (android.support.v4.content.c)localObject;
    }
    return null;
  }
  
  final Locale a()
  {
    Object localObject = "languageAuto";
    boolean bool1 = true;
    boolean bool2 = com.truecaller.common.b.e.a((String)localObject, bool1);
    if (!bool2)
    {
      localObject = new java/util/Locale;
      String str = com.truecaller.common.b.e.a("language");
      ((Locale)localObject).<init>(str);
      return (Locale)localObject;
    }
    localObject = com.truecaller.common.e.e.a(getContext());
    if (localObject != null) {
      return (Locale)localObject;
    }
    return com.truecaller.common.e.e.a(e);
  }
  
  public final void a(android.support.v4.content.c paramc, Object paramObject)
  {
    int m = f;
    int i2 = R.id.wizard_loader_suggestedCountry;
    int n;
    if (m == i2)
    {
      n = paramObject instanceof CountryListDto.a;
      if (n != 0)
      {
        paramObject = (CountryListDto.a)paramObject;
        a((CountryListDto.a)paramObject);
      }
    }
    else
    {
      i2 = R.id.wizard_loader_autologin;
      if (n == i2)
      {
        n = paramObject instanceof Boolean;
        if (n != 0)
        {
          paramObject = (Boolean)paramObject;
          n = ((Boolean)paramObject).booleanValue();
          if (n != 0)
          {
            com.truecaller.utils.extensions.i.a(getContext(), "com.truecaller.action.ACTION_UPDATE_CONFIG");
            b();
            ((com.truecaller.wizard.b.c)getActivity()).a();
            paramc = d;
            paramObject = new com/truecaller/wizard/-$$Lambda$b$TAjZ32BK7qg7gecekuDxejhdZkM;
            ((-..Lambda.b.TAjZ32BK7qg7gecekuDxejhdZkM)paramObject).<init>(this);
            paramc.a((Runnable)paramObject);
            return;
          }
        }
        e();
        int i1 = R.string.NetworkError;
        a(i1);
      }
    }
  }
  
  protected final void a(TextView paramTextView)
  {
    int m = R.string.Welcome_terms_r;
    int n = 3;
    Object[] arrayOfObject = new Object[n];
    String str1 = "https://www.truecaller.com/terms-of-service#eu";
    boolean bool1 = false;
    arrayOfObject[0] = str1;
    boolean bool2 = i();
    if (bool2) {
      str1 = "https://www.truecaller.com/pt-BR/terms-of-service";
    } else {
      str1 = "https://www.truecaller.com/terms-of-service#row";
    }
    bool1 = true;
    arrayOfObject[bool1] = str1;
    int i1 = 2;
    boolean bool3 = i();
    String str2;
    if (bool3) {
      str2 = "https://www.truecaller.com/pt-BR/privacy-policy";
    } else {
      str2 = "https://privacy.truecaller.com/privacy-policy";
    }
    arrayOfObject[i1] = str2;
    p.a(paramTextView, m, arrayOfObject);
    paramTextView.setLinksClickable(bool1);
    Object localObject = LinkMovementMethod.getInstance();
    paramTextView.setMovementMethod((MovementMethod)localObject);
    localObject = new com/truecaller/wizard/-$$Lambda$b$6d9VGhBqMxtqCkAQC-BfjHTvJiQ;
    ((-..Lambda.b.6d9VGhBqMxtqCkAQC-BfjHTvJiQ)localObject).<init>(this);
    p.a(paramTextView, (m)localObject);
  }
  
  protected void a(CountryListDto.a parama)
  {
    c = parama;
  }
  
  protected void a(String paramString)
  {
    k = paramString;
  }
  
  protected final void c()
  {
    boolean bool = l();
    if (bool)
    {
      b();
      w localw = getLoaderManager();
      int m = R.id.wizard_loader_autologin;
      localw.a(m, null, this);
    }
  }
  
  protected final CountryListDto.a d()
  {
    return c;
  }
  
  protected String f()
  {
    return k;
  }
  
  protected final boolean g()
  {
    Object localObject = c;
    if (localObject != null)
    {
      localObject = f();
      boolean bool = TextUtils.isEmpty((CharSequence)localObject);
      if (!bool) {
        return true;
      }
    }
    return false;
  }
  
  protected final void h()
  {
    Object localObject1 = c;
    if (localObject1 != null)
    {
      localObject1 = d;
      if (localObject1 != null)
      {
        localObject1 = c.c;
        if (localObject1 != null)
        {
          localObject1 = f();
          boolean bool1 = TextUtils.isEmpty((CharSequence)localObject1);
          if (bool1) {
            return;
          }
          String str1 = am.e((String)localObject1);
          if (str1 == null) {
            return;
          }
          Object localObject2 = new java/lang/StringBuilder;
          ((StringBuilder)localObject2).<init>("00");
          String str2 = c.d;
          ((StringBuilder)localObject2).append(str2);
          localObject2 = ((StringBuilder)localObject2).toString();
          boolean bool2 = str1.startsWith((String)localObject2);
          int m;
          if (bool2)
          {
            localObject2 = c.d;
            m = ((String)localObject2).length() + 2;
            str1 = str1.substring(m);
          }
          com.truecaller.common.b.e.b("wizard_EnteredNumber", (String)localObject1);
          h.a("profileNumber", str1);
          localObject1 = h;
          localObject2 = c.c;
          ((com.truecaller.common.g.a)localObject1).a("profileCountryIso", (String)localObject2);
          localObject1 = com.truecaller.common.b.a.F().u().n();
          str1 = h.a("profileCountryIso");
          localObject2 = com.truecaller.common.b.e.a("wizard_EnteredNumber");
          str2 = null;
          try
          {
            localObject2 = aa.a((String)localObject2, str1);
          }
          catch (g localg)
          {
            m = 0;
            localObject2 = null;
          }
          boolean bool3;
          if (localObject2 != null) {
            bool3 = ((ac)localObject1).a((String)localObject2);
          } else {
            bool3 = ((ac)localObject1).b(str1);
          }
          if (bool3)
          {
            ((com.truecaller.wizard.b.c)getActivity()).a("Page_Privacy", null);
            return;
          }
          localObject1 = com.truecaller.wizard.d.e.a(h.a("profileNumber"), str1);
          ((com.truecaller.wizard.b.c)getActivity()).a("Page_Verification", (Bundle)localObject1);
          return;
        }
      }
    }
    new String[1][0] = "No/invalid country selected";
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = com.truecaller.wizard.b.a.a();
    Object localObject = com.truecaller.utils.c.a();
    Context localContext = requireContext();
    localObject = ((t.a)localObject).a(localContext).a();
    paramBundle = paramBundle.a((t)localObject);
    localObject = com.truecaller.common.b.a.F().u();
    paramBundle.a((com.truecaller.common.a)localObject).a().a(this);
  }
  
  public void onDestroyView()
  {
    super.onDestroyView();
    AlertDialog localAlertDialog = i;
    if (localAlertDialog != null) {
      localAlertDialog.dismiss();
    }
    localAlertDialog = j;
    if (localAlertDialog != null) {
      localAlertDialog.dismiss();
    }
  }
  
  public void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    com.truecaller.wizard.utils.i.a(paramArrayOfString, paramArrayOfInt);
    Object localObject = f;
    paramArrayOfString = new String[] { "android.permission.READ_PHONE_STATE" };
    paramInt = ((l)localObject).a(paramArrayOfString);
    if (paramInt != 0)
    {
      localObject = f();
      paramInt = TextUtils.isEmpty((CharSequence)localObject);
      if (paramInt != 0)
      {
        localObject = com.truecaller.common.h.k.c(getContext());
        a((String)localObject);
      }
    }
  }
  
  public void onStart()
  {
    super.onStart();
    com.truecaller.analytics.a.d locald = a;
    long l = SystemClock.elapsedRealtime();
    a = l;
  }
  
  public void onStop()
  {
    super.onStop();
    a.a();
  }
  
  public void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    paramView = h.b();
    if (paramView != null)
    {
      a(paramView);
    }
    else
    {
      paramView = getLoaderManager();
      int m = R.id.wizard_loader_suggestedCountry;
      bool1 = false;
      arrayOfString = null;
      paramView.a(m, null, this);
    }
    paramView = com.truecaller.common.b.e.a("wizard_EnteredNumber");
    paramBundle = com.truecaller.common.b.a.F().u().c();
    boolean bool1 = TextUtils.isEmpty(paramView);
    if (!bool1)
    {
      a(paramView);
      return;
    }
    paramView = f;
    String[] arrayOfString = { "android.permission.READ_PHONE_STATE" };
    boolean bool2 = paramView.a(arrayOfString);
    if (bool2)
    {
      paramView = "isUserChangingNumber";
      bool1 = false;
      arrayOfString = null;
      bool2 = paramBundle.a(paramView, false);
      if (!bool2)
      {
        paramView = com.truecaller.common.h.k.c(getContext());
        a(paramView);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */