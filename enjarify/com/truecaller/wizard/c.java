package com.truecaller.wizard;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.TextView;

public class c
  extends d
{
  protected final Animator a()
  {
    Object localObject1 = getResources();
    int i = R.integer.wizard_cyclic_animation_pause;
    long l1 = ((Resources)localObject1).getInteger(i);
    Resources localResources = getResources();
    int j = R.integer.wizard_animation_duration_medium;
    long l2 = localResources.getInteger(j);
    DisplayMetrics localDisplayMetrics = getResources().getDisplayMetrics();
    int k = 1;
    float f1 = TypedValue.applyDimension(k, 33.0F, localDisplayMetrics);
    Object localObject2 = getResources().getDisplayMetrics();
    float f2 = TypedValue.applyDimension(k, 25.0F, (DisplayMetrics)localObject2);
    c.1 local1 = new com/truecaller/wizard/c$1;
    local1.<init>(this, f1, f2);
    int m = 2;
    localObject2 = new float[m];
    Object tmp106_104 = localObject2;
    tmp106_104[0] = 0.0F;
    tmp106_104[1] = 1.0F;
    localObject2 = ValueAnimator.ofFloat((float[])localObject2);
    ((ValueAnimator)localObject2).setDuration(l2);
    ((ValueAnimator)localObject2).setStartDelay(l1);
    Object localObject3 = new android/view/animation/AccelerateDecelerateInterpolator;
    ((AccelerateDecelerateInterpolator)localObject3).<init>();
    ((ValueAnimator)localObject2).setInterpolator((TimeInterpolator)localObject3);
    ((ValueAnimator)localObject2).addUpdateListener(local1);
    localObject3 = new float[m];
    Object tmp167_165 = localObject3;
    tmp167_165[0] = 1.0F;
    tmp167_165[1] = 0.0F;
    localObject3 = ValueAnimator.ofFloat((float[])localObject3);
    ((ValueAnimator)localObject3).setDuration(l2);
    ((ValueAnimator)localObject3).setStartDelay(l1);
    localObject1 = new android/view/animation/AccelerateDecelerateInterpolator;
    ((AccelerateDecelerateInterpolator)localObject1).<init>();
    ((ValueAnimator)localObject3).setInterpolator((TimeInterpolator)localObject1);
    ((ValueAnimator)localObject3).addUpdateListener(local1);
    localObject1 = new android/animation/AnimatorSet;
    ((AnimatorSet)localObject1).<init>();
    Object localObject4 = new Animator[m];
    localObject4[0] = localObject2;
    localObject4[k] = localObject3;
    ((AnimatorSet)localObject1).playSequentially((Animator[])localObject4);
    localObject4 = new com/truecaller/wizard/c$2;
    ((c.2)localObject4).<init>(this);
    ((AnimatorSet)localObject1).addListener((Animator.AnimatorListener)localObject4);
    return (Animator)localObject1;
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    int i = R.layout.wizard_fragment_draw_details;
    return paramLayoutInflater.inflate(i, paramViewGroup, false);
  }
  
  public void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    a.setOnClickListener(null);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */