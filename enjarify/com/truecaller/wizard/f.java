package com.truecaller.wizard;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v4.app.o;
import android.support.v4.app.w.a;
import android.support.v7.app.AlertDialog.Builder;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import com.truecaller.social.SocialNetworkType;
import com.truecaller.social.d.c;
import com.truecaller.utils.t.a;
import com.truecaller.wizard.b.a.b;
import com.truecaller.wizard.internal.components.EditText;
import com.truecaller.wizard.internal.components.EditText.a;
import com.truecaller.wizard.utils.k;
import java.io.File;
import java.util.Collection;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import okhttp3.w;

public class f
  extends com.truecaller.wizard.b.i
  implements w.a, View.OnClickListener
{
  private static final int[] d;
  private com.truecaller.common.profile.e A;
  private boolean B;
  public com.truecaller.wizard.a.a a;
  public com.truecaller.common.g.a b;
  public k c;
  private ImageView e;
  private ImageView i;
  private EditText j;
  private EditText k;
  private EditText l;
  private View m;
  private Button n;
  private View o;
  private View p;
  private View q;
  private ViewGroup r;
  private String s;
  private Uri t;
  private boolean u;
  private View v;
  private boolean w;
  private com.truecaller.social.b x;
  private final com.truecaller.analytics.a.d y;
  private boolean z;
  
  static
  {
    int[] arrayOfInt = new int[2];
    int i1 = R.id.wizard_social1;
    arrayOfInt[0] = i1;
    i1 = R.id.wizard_social2;
    arrayOfInt[1] = i1;
    d = arrayOfInt;
  }
  
  public f()
  {
    com.truecaller.analytics.a.d locald = new com/truecaller/analytics/a/d;
    locald.<init>();
    y = locald;
    B = true;
  }
  
  public static Bundle a()
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putBoolean("playTransitionAnimation", true);
    return localBundle;
  }
  
  private Collection a(Collection paramCollection)
  {
    LinkedList localLinkedList = new java/util/LinkedList;
    localLinkedList.<init>(paramCollection);
    paramCollection = SocialNetworkType.GOOGLE;
    boolean bool = localLinkedList.remove(paramCollection);
    if (bool)
    {
      paramCollection = SocialNetworkType.GOOGLE;
      localLinkedList.addFirst(paramCollection);
    }
    try
    {
      paramCollection = getContext();
      paramCollection = paramCollection.getPackageManager();
      String str = "com.facebook.katana";
      paramCollection.getPackageInfo(str, 0);
      paramCollection = SocialNetworkType.FACEBOOK;
      bool = localLinkedList.remove(paramCollection);
      if (bool)
      {
        paramCollection = SocialNetworkType.FACEBOOK;
        localLinkedList.addFirst(paramCollection);
      }
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      for (;;) {}
    }
    return localLinkedList;
  }
  
  private void a(SocialNetworkType paramSocialNetworkType)
  {
    try
    {
      Object localObject1 = x;
      if (localObject1 != null)
      {
        localObject1 = x;
        localObject1 = ((com.truecaller.social.b)localObject1).a();
        if (localObject1 == paramSocialNetworkType) {}
      }
      else
      {
        localObject1 = x;
        if (localObject1 != null)
        {
          localObject1 = x;
          ((com.truecaller.social.b)localObject1).d();
          localObject1 = null;
          x = null;
        }
        localObject1 = getActivity();
        localObject1 = com.truecaller.social.e.a((Context)localObject1);
        localObject1 = ((com.truecaller.social.e)localObject1).a(paramSocialNetworkType, this);
        x = ((com.truecaller.social.b)localObject1);
        localObject1 = x;
        ((com.truecaller.social.b)localObject1).b();
        localObject1 = x;
        ((com.truecaller.social.b)localObject1).c();
      }
      localObject1 = x;
      localObject2 = new com/truecaller/wizard/f$3;
      ((f.3)localObject2).<init>(this);
      ((com.truecaller.social.b)localObject1).a((com.truecaller.social.c)localObject2);
      localObject1 = x;
      localObject2 = new com/truecaller/wizard/f$4;
      ((f.4)localObject2).<init>(this);
      ((com.truecaller.social.b)localObject1).b((com.truecaller.social.c)localObject2);
      b();
      localObject1 = x;
      ((com.truecaller.social.b)localObject1).n();
      return;
    }
    catch (d.c localc)
    {
      Object localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      ((StringBuilder)localObject2).append(paramSocialNetworkType);
      ((StringBuilder)localObject2).append(" is not supported");
      paramSocialNetworkType = ((StringBuilder)localObject2).toString();
      com.truecaller.log.d.a(localc, paramSocialNetworkType);
    }
  }
  
  private void a(Map paramMap)
  {
    boolean bool1 = h();
    Object localObject1;
    Object localObject3;
    if (bool1)
    {
      bool1 = u;
      if (bool1)
      {
        localObject1 = t;
        if (localObject1 != null)
        {
          localObject1 = com.truecaller.common.network.util.g.c;
          localObject2 = new java/io/File;
          String str = t.getPath();
          ((File)localObject2).<init>(str);
          localObject1 = okhttp3.ac.a((w)localObject1, (File)localObject2);
          localObject3 = localObject1;
          break label92;
        }
      }
      localObject1 = "avatar_url";
      localObject2 = s;
      paramMap.put(localObject1, localObject2);
    }
    else
    {
      bool1 = false;
      localObject1 = null;
      localObject3 = null;
    }
    label92:
    Object localObject2 = A;
    boolean bool2 = u;
    -..Lambda.f.BrNJHkQ7Ur5Qyz_Qu0-G0S4k_io localBrNJHkQ7Ur5Qyz_Qu0-G0S4k_io = new com/truecaller/wizard/-$$Lambda$f$BrNJHkQ7Ur5Qyz_Qu0-G0S4k_io;
    localBrNJHkQ7Ur5Qyz_Qu0-G0S4k_io.<init>(this);
    ((com.truecaller.common.profile.e)localObject2).a(bool2, (okhttp3.ac)localObject3, false, null, paramMap, false, localBrNJHkQ7Ur5Qyz_Qu0-G0S4k_io);
  }
  
  private void c()
  {
    b();
    ((com.truecaller.wizard.b.c)getActivity()).a();
    k localk = c;
    -..Lambda.f.D0_1PPIvsxGaJuADGsdsBd1ZFUs localD0_1PPIvsxGaJuADGsdsBd1ZFUs = new com/truecaller/wizard/-$$Lambda$f$D0_1PPIvsxGaJuADGsdsBd1ZFUs;
    localD0_1PPIvsxGaJuADGsdsBd1ZFUs.<init>(this);
    localk.a(localD0_1PPIvsxGaJuADGsdsBd1ZFUs);
  }
  
  private void d()
  {
    Object localObject1 = new com/truecaller/wizard/g;
    ((g)localObject1).<init>();
    Object localObject2 = getActivity().getSupportFragmentManager().a();
    int i1 = R.anim.fast_slide_in_up;
    int i2 = R.anim.fast_slide_out_down;
    int i3 = R.anim.fast_slide_in_up;
    int i4 = R.anim.fast_slide_out_down;
    localObject2 = ((o)localObject2).a(i1, i2, i3, i4);
    i1 = R.id.profileInputPlaceholder;
    localObject1 = ((o)localObject2).b(i1, (Fragment)localObject1);
    localObject2 = g.class.getName();
    ((o)localObject1).a((String)localObject2).d();
  }
  
  private void f()
  {
    String str1 = b.a("profileFirstName");
    String str2 = b.a("profileLastName");
    String str3 = b.a("profileEmail");
    j.setText(str1);
    k.setText(str2);
    l.setText(str3);
    str1 = b.a("profileAvatar");
    s = str1;
    i();
  }
  
  private void g()
  {
    Object localObject1 = l;
    boolean bool1 = ((EditText)localObject1).a();
    if (!bool1)
    {
      int i1 = R.string.Profile_InvalidEmail;
      a(i1);
      return;
    }
    localObject1 = getView();
    int i2 = 2;
    com.truecaller.utils.extensions.t.a((View)localObject1, false, i2);
    b();
    localObject1 = new java/util/HashMap;
    ((HashMap)localObject1).<init>();
    Object localObject2 = j.getText().toString();
    Object localObject3 = k.getText().toString();
    String str1 = l.getText().toString();
    String str2 = "first_name";
    ((Map)localObject1).put(str2, localObject2);
    ((Map)localObject1).put("last_name", localObject3);
    localObject2 = "email";
    ((Map)localObject1).put(localObject2, str1);
    boolean bool2 = z;
    if (bool2)
    {
      a((Map)localObject1);
      return;
    }
    localObject2 = A;
    localObject3 = new com/truecaller/wizard/-$$Lambda$f$wXrJH13Ib9mRmepDJfBb8UKrBL4;
    ((-..Lambda.f.wXrJH13Ib9mRmepDJfBb8UKrBL4)localObject3).<init>(this, (Map)localObject1);
    ((com.truecaller.common.profile.e)localObject2).a((com.truecaller.common.profile.a)localObject3);
  }
  
  private boolean h()
  {
    Object localObject = s;
    boolean bool = TextUtils.isEmpty((CharSequence)localObject);
    if (bool)
    {
      localObject = t;
      if (localObject == null) {
        return false;
      }
    }
    return true;
  }
  
  private void i()
  {
    ImageView localImageView = e;
    int i1 = R.drawable.wizard_ic_profile;
    localImageView.setImageResource(i1);
  }
  
  public final android.support.v4.content.c a(int paramInt, Bundle paramBundle)
  {
    int i1 = R.id.wizard_loader_downloader;
    if (paramInt == i1)
    {
      f.a locala = new com/truecaller/wizard/f$a;
      Context localContext = getContext();
      locala.<init>(localContext, paramBundle);
      return locala;
    }
    return null;
  }
  
  public final void a(android.support.v4.content.c paramc, Object paramObject)
  {
    e();
    int i1 = f;
    int i3 = R.id.wizard_loader_downloader;
    if (i1 == i3)
    {
      boolean bool = paramObject instanceof Uri;
      if (bool)
      {
        paramc = com.truecaller.common.h.n.d(getContext());
        t = paramc;
        paramc = null;
        s = null;
        u = true;
        i();
        bool = w;
        if (bool) {
          g();
        }
      }
      else
      {
        i2 = R.string.Profile_PhotoError;
        a(i2);
      }
      int i2 = 0;
      paramc = null;
      w = false;
    }
  }
  
  public final boolean a(int paramInt1, int paramInt2, Intent paramIntent)
  {
    com.truecaller.social.b localb = x;
    if (localb != null)
    {
      paramInt1 = localb.a(paramInt1, paramInt2, paramIntent);
      if (paramInt1 != 0) {
        return true;
      }
    }
    return false;
  }
  
  public void onActivityCreated(Bundle paramBundle)
  {
    super.onActivityCreated(paramBundle);
    getActivityb.b();
  }
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    boolean bool = a(paramInt1, paramInt2, paramIntent);
    if (bool) {
      return;
    }
    if (paramInt1 == 0)
    {
      paramInt1 = -1;
      if (paramInt2 == paramInt1) {
        c();
      }
    }
  }
  
  public void onClick(View paramView)
  {
    int i1 = paramView.getId();
    int i2 = R.id.wizard_later;
    if (i1 == i2)
    {
      paramView = new android/support/v7/app/AlertDialog$Builder;
      localObject1 = getContext();
      paramView.<init>((Context)localObject1);
      i1 = R.string.Profile_Later_Caption;
      paramView = paramView.setTitle(i1);
      i1 = R.string.Profile_Later_Text;
      paramView = paramView.setMessage(i1);
      i1 = R.string.Profile_Add_Name;
      Object localObject2 = new com/truecaller/wizard/-$$Lambda$f$vo45NOdhnZ7G-30LZFxyYEbxI94;
      ((-..Lambda.f.vo45NOdhnZ7G-30LZFxyYEbxI94)localObject2).<init>(this);
      paramView = paramView.setPositiveButton(i1, (DialogInterface.OnClickListener)localObject2);
      i1 = R.string.Profile_Skip;
      localObject2 = new com/truecaller/wizard/-$$Lambda$f$Y0XXNlHW4dsviaJxLdxEzuXkN9w;
      ((-..Lambda.f.Y0XXNlHW4dsviaJxLdxEzuXkN9w)localObject2).<init>(this);
      paramView.setNegativeButton(i1, (DialogInterface.OnClickListener)localObject2).show();
      return;
    }
    super.onClick(paramView);
    i2 = R.id.nextButton;
    if (i1 == i2)
    {
      g();
      return;
    }
    Object localObject1 = paramView.getTag();
    boolean bool = localObject1 instanceof SocialNetworkType;
    if (bool)
    {
      paramView = (SocialNetworkType)paramView.getTag();
      a(paramView);
    }
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Object localObject1 = com.truecaller.wizard.b.a.a();
    Object localObject2 = com.truecaller.utils.c.a();
    Context localContext = getContext();
    localObject2 = ((t.a)localObject2).a(localContext).a();
    localObject1 = ((a.b)localObject1).a((com.truecaller.utils.t)localObject2);
    localObject2 = com.truecaller.common.b.a.F().u();
    ((a.b)localObject1).a((com.truecaller.common.a)localObject2).a().a(this);
    localObject1 = b.a("profileCountryIso");
    localObject2 = com.truecaller.common.b.a.F().u().n();
    boolean bool1 = ((String)localObject1).isEmpty();
    if (!bool1)
    {
      bool2 = ((com.truecaller.common.h.ac)localObject2).b((String)localObject1);
      if (!bool2)
      {
        bool2 = false;
        localObject1 = null;
        break label124;
      }
    }
    boolean bool2 = true;
    label124:
    B = bool2;
    if (paramBundle != null)
    {
      localObject1 = "stateSocialNetworkType";
      bool2 = paramBundle.containsKey((String)localObject1);
      if (bool2)
      {
        localObject1 = SocialNetworkType.valueOf(paramBundle.getString("stateSocialNetworkType"));
        try
        {
          localObject2 = getActivity();
          localObject2 = com.truecaller.social.e.a((Context)localObject2);
          localObject1 = ((com.truecaller.social.e)localObject2).a((SocialNetworkType)localObject1, this);
          x = ((com.truecaller.social.b)localObject1);
        }
        catch (d.c localc) {}
      }
      localObject1 = x;
      if (localObject1 != null) {
        ((com.truecaller.social.b)localObject1).a(paramBundle);
      }
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    int i1 = R.layout.wizard_fragment_profile;
    paramLayoutInflater = paramLayoutInflater.inflate(i1, paramViewGroup, false);
    int i2 = R.id.photo;
    paramViewGroup = (ImageView)paramLayoutInflater.findViewById(i2);
    e = paramViewGroup;
    i2 = R.id.headerBackgroundImageView;
    paramViewGroup = (ImageView)paramLayoutInflater.findViewById(i2);
    i = paramViewGroup;
    i2 = R.id.firstName;
    paramViewGroup = (EditText)paramLayoutInflater.findViewById(i2);
    j = paramViewGroup;
    i2 = R.id.lastName;
    paramViewGroup = (EditText)paramLayoutInflater.findViewById(i2);
    k = paramViewGroup;
    i2 = R.id.email;
    paramViewGroup = (EditText)paramLayoutInflater.findViewById(i2);
    l = paramViewGroup;
    i2 = R.id.nextButton;
    paramViewGroup = paramLayoutInflater.findViewById(i2);
    m = paramViewGroup;
    i2 = R.id.animated;
    paramViewGroup = paramLayoutInflater.findViewById(i2);
    q = paramViewGroup;
    i2 = R.id.content;
    paramViewGroup = (ViewGroup)paramLayoutInflater.findViewById(i2);
    r = paramViewGroup;
    i2 = R.id.socialContent;
    paramViewGroup = paramLayoutInflater.findViewById(i2);
    v = paramViewGroup;
    i2 = R.id.manualInputButton;
    paramViewGroup = (Button)paramLayoutInflater.findViewById(i2);
    n = paramViewGroup;
    i2 = R.id.businessProfileContainer;
    paramViewGroup = paramLayoutInflater.findViewById(i2);
    o = paramViewGroup;
    i2 = R.id.businessProfileButton;
    paramViewGroup = paramLayoutInflater.findViewById(i2);
    p = paramViewGroup;
    return paramLayoutInflater;
  }
  
  public void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    com.truecaller.wizard.utils.i.a(paramArrayOfString, paramArrayOfInt);
    int i1 = 1;
    int i2 = 0;
    int i3 = 1;
    for (;;)
    {
      int i4 = paramArrayOfString.length;
      if (i2 >= i4) {
        break;
      }
      i4 = paramArrayOfInt[i2];
      if (i4 == 0) {
        i4 = 1;
      } else {
        i4 = 0;
      }
      i3 &= i4;
      i2 += 1;
    }
    paramArrayOfString = SocialNetworkType.values();
    String str = paramArrayOfString[paramInt];
    if (i3 != 0)
    {
      a(str);
      return;
    }
    paramArrayOfString = new String[i1];
    str = String.valueOf(str);
    str = "Not all required permissions were granted for ".concat(str);
    paramArrayOfString[0] = str;
  }
  
  public void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    com.truecaller.social.b localb = x;
    if (localb != null) {
      localb.b(paramBundle);
    }
  }
  
  public void onStart()
  {
    super.onStart();
    Object localObject = x;
    if (localObject != null) {
      ((com.truecaller.social.b)localObject).c();
    }
    localObject = y;
    long l1 = SystemClock.elapsedRealtime();
    a = l1;
  }
  
  public void onStop()
  {
    super.onStop();
    com.truecaller.social.b localb = x;
    if (localb != null) {
      localb.d();
    }
    y.a();
  }
  
  public void onViewCreated(View paramView, Bundle paramBundle)
  {
    f localf = this;
    Object localObject1 = paramView;
    super.onViewCreated(paramView, paramBundle);
    int i1 = R.id.nextButton;
    paramView.findViewById(i1).setOnClickListener(this);
    Object localObject2 = j;
    Object localObject3 = com.truecaller.wizard.utils.n.b;
    ((EditText)localObject2).setInputValidator((EditText.a)localObject3);
    localObject2 = k;
    localObject3 = com.truecaller.wizard.utils.n.b;
    ((EditText)localObject2).setInputValidator((EditText.a)localObject3);
    localObject2 = l;
    localObject3 = com.truecaller.wizard.utils.n.c;
    ((EditText)localObject2).setInputValidator((EditText.a)localObject3);
    f();
    b();
    localObject2 = com.truecaller.common.b.a.F().b();
    A = ((com.truecaller.common.profile.e)localObject2);
    localObject2 = A;
    localObject3 = new com/truecaller/wizard/-$$Lambda$f$AoN3YNUUm6vEinp9qP2WedWu8y8;
    ((-..Lambda.f.AoN3YNUUm6vEinp9qP2WedWu8y8)localObject3).<init>(this);
    ((com.truecaller.common.profile.e)localObject2).a((com.truecaller.common.profile.a)localObject3);
    localObject2 = getArguments();
    int i3 = 1;
    Object localObject4;
    int i4;
    Object localObject5;
    Object localObject6;
    int i6;
    Object localObject7;
    int i7;
    Object localObject8;
    int i8;
    if (localObject2 != null)
    {
      localObject4 = "playTransitionAnimation";
      boolean bool1 = ((Bundle)localObject2).getBoolean((String)localObject4);
      if (bool1)
      {
        localObject2 = e;
        i4 = 0;
        localObject4 = null;
        ((ImageView)localObject2).setAlpha(0.0F);
        localObject2 = i;
        ((ImageView)localObject2).setAlpha(0.0F);
        int i2 = 2;
        localObject5 = new float[i2];
        Object tmp201_199 = localObject5;
        tmp201_199[0] = 0.0F;
        tmp201_199[1] = 1.0F;
        localObject5 = ValueAnimator.ofFloat((float[])localObject5);
        localObject6 = getResources();
        i6 = R.integer.wizard_animation_duration_medium;
        long l1 = ((Resources)localObject6).getInteger(i6);
        ((ValueAnimator)localObject5).setDuration(l1);
        localObject6 = new android/view/animation/AccelerateDecelerateInterpolator;
        ((AccelerateDecelerateInterpolator)localObject6).<init>();
        ((ValueAnimator)localObject5).setInterpolator((TimeInterpolator)localObject6);
        localObject6 = new com/truecaller/wizard/-$$Lambda$f$zIDpk42IFuczbTfdtJjwDnIpnW8;
        ((-..Lambda.f.zIDpk42IFuczbTfdtJjwDnIpnW8)localObject6).<init>(this);
        ((ValueAnimator)localObject5).addUpdateListener((ValueAnimator.AnimatorUpdateListener)localObject6);
        localObject6 = new float[i2];
        Object tmp288_286 = localObject6;
        tmp288_286[0] = 0.0F;
        tmp288_286[1] = 1.0F;
        localObject6 = ValueAnimator.ofFloat((float[])localObject6);
        localObject7 = getResources();
        i7 = R.integer.wizard_animation_duration_short;
        i6 = ((Resources)localObject7).getInteger(i7);
        long l2 = i6;
        ((ValueAnimator)localObject6).setDuration(l2);
        localObject7 = new com/truecaller/wizard/-$$Lambda$f$Ph_wvn_rQnOE5TEHVTV1_3Vnofw;
        ((-..Lambda.f.Ph_wvn_rQnOE5TEHVTV1_3Vnofw)localObject7).<init>(this);
        ((ValueAnimator)localObject6).addUpdateListener((ValueAnimator.AnimatorUpdateListener)localObject7);
        localObject7 = new android/animation/AnimatorSet;
        ((AnimatorSet)localObject7).<init>();
        localObject8 = new Animator[i2];
        localObject8[0] = localObject5;
        localObject8[i3] = localObject6;
        ((AnimatorSet)localObject7).playSequentially((Animator[])localObject8);
        localObject5 = new Animator[i2];
        localObject6 = getResources();
        i7 = R.integer.wizard_animation_duration_medium;
        long l3 = ((Resources)localObject6).getInteger(i7);
        i8 = 1;
        for (;;)
        {
          int i9 = 3;
          if (i8 >= i9) {
            break;
          }
          View localView = r.getChildAt(i8);
          localView.setAlpha(0.0F);
          Object localObject9 = new float[i2];
          Object tmp457_455 = localObject9;
          tmp457_455[0] = 0.0F;
          tmp457_455[1] = 1.0F;
          localObject9 = ValueAnimator.ofFloat((float[])localObject9);
          ((ValueAnimator)localObject9).setDuration(l3);
          Object localObject10 = new android/view/animation/DecelerateInterpolator;
          ((DecelerateInterpolator)localObject10).<init>();
          ((ValueAnimator)localObject9).setInterpolator((TimeInterpolator)localObject10);
          localObject10 = new com/truecaller/wizard/-$$Lambda$f$BMgl6W-SaqCsuuteEwQ1Rp0UKNE;
          ((-..Lambda.f.BMgl6W-SaqCsuuteEwQ1Rp0UKNE)localObject10).<init>(localView);
          ((ValueAnimator)localObject9).addUpdateListener((ValueAnimator.AnimatorUpdateListener)localObject10);
          long l4 = i8 * l3;
          long l5 = 4;
          l4 /= l5;
          ((ValueAnimator)localObject9).setStartDelay(l4);
          int i10 = i8 + -1;
          localObject5[i10] = localObject9;
          i8 += 1;
        }
        localObject4 = new android/animation/AnimatorSet;
        ((AnimatorSet)localObject4).<init>();
        ((AnimatorSet)localObject4).playTogether((Animator[])localObject5);
        localObject5 = new android/animation/AnimatorSet;
        ((AnimatorSet)localObject5).<init>();
        localObject2 = new Animator[i2];
        localObject2[0] = localObject7;
        localObject2[i3] = localObject4;
        ((AnimatorSet)localObject5).playTogether((Animator[])localObject2);
        localObject2 = new com/truecaller/wizard/f$2;
        ((f.2)localObject2).<init>(localf);
        ((AnimatorSet)localObject5).addListener((Animator.AnimatorListener)localObject2);
        ((AnimatorSet)localObject5).start();
        break label662;
      }
    }
    localObject2 = localObject1;
    localObject2 = (ViewGroup)localObject1;
    ((ViewGroup)localObject2).removeViewAt(i3);
    label662:
    m();
    localObject2 = v;
    if (localObject2 != null)
    {
      localObject2 = com.truecaller.social.e.a(getActivity());
      localObject4 = a;
      i4 = ((EnumMap)localObject4).isEmpty();
      if (i4 != 0) {
        localObject2 = EnumSet.noneOf(SocialNetworkType.class);
      } else {
        localObject2 = EnumSet.copyOf(a.keySet());
      }
      i4 = ((EnumSet)localObject2).isEmpty();
      if (i4 == 0)
      {
        localObject4 = new com/truecaller/wizard/utils/g;
        localObject5 = new com/truecaller/wizard/f$1;
        ((f.1)localObject5).<init>(localf);
        ((com.truecaller.wizard.utils.g)localObject4).<init>((View)localObject1, (com.truecaller.wizard.utils.f)localObject5);
        localObject1 = localf.a((Collection)localObject2).iterator();
        i4 = 0;
        localObject4 = null;
        for (;;)
        {
          boolean bool3 = ((Iterator)localObject1).hasNext();
          if (!bool3) {
            break;
          }
          localObject5 = (SocialNetworkType)((Iterator)localObject1).next();
          localObject6 = d;
          i8 = localObject6.length;
          boolean bool2;
          if (i4 < i8)
          {
            bool2 = true;
          }
          else
          {
            bool2 = false;
            localObject6 = null;
          }
          localObject7 = new String[i3];
          localObject8 = new java/lang/StringBuilder;
          ((StringBuilder)localObject8).<init>("You've added more social networks than supported (");
          ((StringBuilder)localObject8).append(localObject2);
          String str = ")";
          ((StringBuilder)localObject8).append(str);
          localObject8 = ((StringBuilder)localObject8).toString();
          localObject7[0] = localObject8;
          AssertionUtil.AlwaysFatal.isTrue(bool2, (String[])localObject7);
          localObject6 = v;
          localObject7 = d;
          i6 = localObject7[i4];
          localObject6 = ((View)localObject6).findViewById(i6);
          if (localObject6 != null)
          {
            ((View)localObject6).setVisibility(0);
            i6 = R.id.wizard_socialButton;
            localObject7 = (Button)((View)localObject6).findViewById(i6);
            ((Button)localObject7).setTag(localObject5);
            ((Button)localObject7).setOnClickListener(localf);
            i7 = ((SocialNetworkType)localObject5).getBackground();
            ((Button)localObject7).setBackgroundResource(i7);
            i7 = ((SocialNetworkType)localObject5).getName();
            ((Button)localObject7).setText(i7);
            i7 = ((SocialNetworkType)localObject5).getIcon();
            ((Button)localObject7).setCompoundDrawablesRelativeWithIntrinsicBounds(i7, 0, 0, 0);
            int i11 = ((SocialNetworkType)localObject5).getName();
            localObject5 = localf.getString(i11);
            ((View)localObject6).setContentDescription((CharSequence)localObject5);
          }
          int i5;
          i4 += 1;
        }
        localObject1 = v;
        ((View)localObject1).setVisibility(0);
      }
    }
    localObject1 = n;
    localObject2 = new com/truecaller/wizard/-$$Lambda$f$CMVbI4Gd2S73IfzuS-vyUr7UBnw;
    ((-..Lambda.f.CMVbI4Gd2S73IfzuS-vyUr7UBnw)localObject2).<init>(localf);
    ((Button)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    localObject1 = com.truecaller.common.b.a.F().f().z();
    boolean bool4 = ((com.truecaller.featuretoggles.b)localObject1).a();
    if (bool4)
    {
      o.setVisibility(0);
      localObject1 = p;
      localObject2 = new com/truecaller/wizard/-$$Lambda$f$J5eIfBAdAwl1Gg6P87MSntvYo2c;
      ((-..Lambda.f.J5eIfBAdAwl1Gg6P87MSntvYo2c)localObject2).<init>(localf);
      ((View)localObject1).setOnClickListener((View.OnClickListener)localObject2);
      return;
    }
    o.setVisibility(8);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */