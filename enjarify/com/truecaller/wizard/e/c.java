package com.truecaller.wizard.e;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import c.g.a.m;
import c.g.b.k;
import com.truecaller.utils.extensions.p;
import com.truecaller.wizard.R.id;
import com.truecaller.wizard.R.layout;
import com.truecaller.wizard.R.string;
import com.truecaller.wizard.b.i;
import com.truecaller.wizard.d.e.a;
import java.util.HashMap;

public final class c
  extends i
  implements d.b
{
  public d.a a;
  private HashMap b;
  
  private final void a(TextView paramTextView)
  {
    Object localObject = LinkMovementMethod.getInstance();
    paramTextView.setMovementMethod((MovementMethod)localObject);
    localObject = new com/truecaller/wizard/e/c$a;
    ((c.a)localObject).<init>(this, paramTextView);
    localObject = (m)localObject;
    p.a(paramTextView, (m)localObject);
  }
  
  public final d.a a()
  {
    d.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return locala;
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "url");
    Context localContext = getContext();
    if (localContext == null) {
      return;
    }
    k.a(localContext, "context ?: return");
    Object localObject1 = new android/support/customtabs/c$a;
    ((android.support.customtabs.c.a)localObject1).<init>();
    localObject1 = ((android.support.customtabs.c.a)localObject1).a();
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("android-app://");
    Object localObject3 = localContext.getPackageName();
    ((StringBuilder)localObject2).append((String)localObject3);
    localObject2 = Uri.parse(((StringBuilder)localObject2).toString());
    localObject3 = a;
    localObject2 = (Parcelable)localObject2;
    ((Intent)localObject3).putExtra("android.intent.extra.REFERRER", (Parcelable)localObject2);
    paramString = Uri.parse(paramString);
    ((android.support.customtabs.c)localObject1).a(localContext, paramString);
  }
  
  public final void c()
  {
    Object localObject1 = h;
    Object localObject2 = "profileNumber";
    localObject1 = ((com.truecaller.common.g.a)localObject1).a((String)localObject2);
    if (localObject1 == null) {
      localObject1 = "";
    }
    k.a(localObject1, "mCoreSettings.getString(…ngs.PROFILE_NUMBER) ?: \"\"");
    localObject2 = h;
    Object localObject3 = "profileCountryIso";
    localObject2 = ((com.truecaller.common.g.a)localObject2).a((String)localObject3);
    if (localObject2 == null) {
      localObject2 = "";
    }
    k.a(localObject2, "mCoreSettings.getString(…ROFILE_COUNTRY_ISO) ?: \"\"");
    localObject3 = k();
    localObject1 = e.a.a((String)localObject1, (String)localObject2);
    ((com.truecaller.wizard.b.c)localObject3).a("Page_Verification", (Bundle)localObject1);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = a.a();
    Object localObject = com.truecaller.common.b.a.F();
    k.a(localObject, "ApplicationBase.getAppBase()");
    localObject = ((com.truecaller.common.b.a)localObject).u();
    paramBundle.a((com.truecaller.common.a)localObject).a().a(this);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    int i = R.layout.wizard_fragment_privacy;
    paramLayoutInflater = paramLayoutInflater.inflate(i, paramViewGroup, false);
    int j = R.id.privacyPolicyText;
    paramViewGroup = (TextView)paramLayoutInflater.findViewById(j);
    k.a(paramViewGroup, "privacyPolicyText");
    i = R.string.Privacy_text;
    int k = 2;
    Object[] arrayOfObject1 = new Object[k];
    arrayOfObject1[0] = "https://privacy.truecaller.com/privacy-policy-eu";
    int m = 1;
    arrayOfObject1[m] = "https://www.truecaller.com/terms-of-service#eu";
    paramViewGroup = p.a(paramViewGroup, i, arrayOfObject1);
    a(paramViewGroup);
    j = R.id.legalFooterText;
    paramViewGroup = (TextView)paramLayoutInflater.findViewById(j);
    k.a(paramViewGroup, "legalFooterText");
    i = R.string.Privacy_agree_text;
    Object[] arrayOfObject2 = new Object[k];
    arrayOfObject2[0] = "https://privacy.truecaller.com/privacy-policy-eu";
    arrayOfObject2[m] = "https://www.truecaller.com/terms-of-service#eu";
    paramViewGroup = p.a(paramViewGroup, i, arrayOfObject2);
    a(paramViewGroup);
    j = R.id.howWeUseYourDataText;
    paramViewGroup = (TextView)paramLayoutInflater.findViewById(j);
    k.a(paramViewGroup, "howWeUseYourDataText");
    i = R.string.Privacy_usage_text;
    arrayOfObject2 = new Object[m];
    arrayOfObject2[0] = "https://privacy.truecaller.com/privacy-policy-eu";
    paramViewGroup = p.a(paramViewGroup, i, arrayOfObject2);
    a(paramViewGroup);
    j = R.id.nextButton;
    paramViewGroup = (Button)paramLayoutInflater.findViewById(j);
    paramBundle = new com/truecaller/wizard/e/c$b;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramViewGroup.setOnClickListener(paramBundle);
    return paramLayoutInflater;
  }
  
  public final void onDestroyView()
  {
    super.onDestroyView();
    Object localObject = a;
    if (localObject == null)
    {
      String str = "presenter";
      k.a(str);
    }
    ((d.a)localObject).y_();
    localObject = b;
    if (localObject != null) {
      ((HashMap)localObject).clear();
    }
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    String str = "view";
    k.b(paramView, str);
    super.onViewCreated(paramView, paramBundle);
    paramView = a;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramView.a(this);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.e.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */