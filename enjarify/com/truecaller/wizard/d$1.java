package com.truecaller.wizard;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.drawable.GradientDrawable;
import android.widget.ImageView;

final class d$1
  implements ValueAnimator.AnimatorUpdateListener
{
  private final ArgbEvaluator e;
  
  d$1(d paramd, int paramInt1, int paramInt2, float paramFloat)
  {
    paramd = new android/animation/ArgbEvaluator;
    paramd.<init>();
    e = paramd;
  }
  
  public final void onAnimationUpdate(ValueAnimator paramValueAnimator)
  {
    float f1 = ((Float)paramValueAnimator.getAnimatedValue()).floatValue();
    ArgbEvaluator localArgbEvaluator = e;
    Object localObject = Integer.valueOf(a);
    Integer localInteger = Integer.valueOf(b);
    int i = ((Integer)localArgbEvaluator.evaluate(f1, localObject, localInteger)).intValue();
    localObject = d.b;
    float f2 = c;
    f1 *= f2;
    ((ImageView)localObject).setTranslationX(f1);
    ((GradientDrawable)d.b.getDrawable()).setColor(i);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.d.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */