package com.truecaller.wizard.d;

import c.n.i;
import com.truecaller.bb;
import com.truecaller.common.account.r;
import com.truecaller.common.h.d;
import com.truecaller.common.h.u;
import com.truecaller.common.network.account.InstallationDetailsDto;
import com.truecaller.common.network.account.SendTokenRequestDto;
import com.truecaller.common.network.account.TokenResponseDto;
import com.truecaller.common.network.account.VerifyTokenRequestDto;
import com.truecaller.common.network.account.b;
import com.truecaller.wizard.R.string;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.bn;
import kotlinx.coroutines.e;

public final class n
  extends bb
  implements m
{
  private final u A;
  private final b B;
  private final com.truecaller.common.network.c C;
  private final d D;
  private final com.truecaller.utils.a E;
  final p a;
  final c.d.f c;
  final com.truecaller.wizard.utils.j d;
  final com.truecaller.wizard.a.a e;
  private int f;
  private String g;
  private String h;
  private long i;
  private String j;
  private Long k;
  private String l;
  private List m;
  private String n;
  private String o;
  private bn p;
  private bn q;
  private final c.n.k r;
  private final c.n.k s;
  private final String t;
  private final String u;
  private final f v;
  private final c w;
  private final c.d.f x;
  private final com.truecaller.common.g.a y;
  private final r z;
  
  public n(String paramString1, String paramString2, p paramp, f paramf, c paramc, c.d.f paramf1, c.d.f paramf2, com.truecaller.common.g.a parama, r paramr, u paramu, b paramb, com.truecaller.common.network.c paramc1, com.truecaller.wizard.utils.j paramj, d paramd, com.truecaller.wizard.a.a parama1, com.truecaller.utils.a parama2)
  {
    t = paramString1;
    u = paramString2;
    a = paramp;
    v = paramf;
    w = paramc;
    c = paramf1;
    x = paramf2;
    y = parama;
    z = paramr;
    A = paramu;
    B = paramb;
    C = paramc1;
    d = paramj;
    D = paramd;
    localObject = parama1;
    e = parama1;
    E = parama2;
    localObject = new c/n/k;
    ((c.n.k)localObject).<init>("Truecaller code (\\d{6})");
    r = ((c.n.k)localObject);
    localObject = new c/n/k;
    ((c.n.k)localObject).<init>("\\D");
    s = ((c.n.k)localObject);
  }
  
  private final void a(int paramInt)
  {
    h();
    o localo = (o)b;
    if (localo != null)
    {
      localo.b(paramInt);
      return;
    }
  }
  
  private final void a(TokenResponseDto paramTokenResponseDto, boolean paramBoolean)
  {
    if (paramBoolean)
    {
      paramBoolean = f + 1;
      f = paramBoolean;
      localObject = y;
      String str = "verificationLastSequenceNumber";
      int i1 = f;
      long l1 = i1;
      ((com.truecaller.common.g.a)localObject).b(str, l1);
    }
    Object localObject = paramTokenResponseDto.getMethod();
    g = ((String)localObject);
    localObject = paramTokenResponseDto.getRequestId();
    h = ((String)localObject);
    long l2 = E.a();
    localObject = TimeUnit.SECONDS;
    Long localLong = paramTokenResponseDto.getTokenTtl();
    if (localLong != null) {
      l3 = localLong.longValue();
    } else {
      l3 = 0L;
    }
    long l3 = ((TimeUnit)localObject).toMillis(l3);
    l2 += l3;
    i = l2;
    localObject = paramTokenResponseDto.getPattern();
    j = ((String)localObject);
    localObject = paramTokenResponseDto.getParsedCountryCode();
    l = ((String)localObject);
    localObject = paramTokenResponseDto.getParsedPhoneNumber();
    k = ((Long)localObject);
    paramTokenResponseDto = paramTokenResponseDto.getClis();
    m = paramTokenResponseDto;
    f();
  }
  
  private final boolean d(String paramString)
  {
    Object localObject1 = m;
    int i1 = 1;
    if (localObject1 != null)
    {
      localObject1 = c.a.m.e((Iterable)localObject1);
      if (localObject1 != null)
      {
        Object localObject2 = localObject1;
        localObject2 = (Iterable)localObject1;
        boolean bool1 = localObject2 instanceof Collection;
        if (bool1)
        {
          localObject3 = localObject2;
          localObject3 = (Collection)localObject2;
          bool1 = ((Collection)localObject3).isEmpty();
          if (bool1) {}
        }
        else
        {
          localObject2 = ((Iterable)localObject2).iterator();
          int i2;
          label141:
          do
          {
            bool1 = ((Iterator)localObject2).hasNext();
            if (!bool1) {
              break;
            }
            localObject3 = (CharSequence)((Iterator)localObject2).next();
            if (localObject3 != null)
            {
              i2 = ((CharSequence)localObject3).length();
              if (i2 != 0)
              {
                i2 = 0;
                localObject3 = null;
                break label141;
              }
            }
            i2 = 1;
            i2 ^= i1;
          } while (i2 == 0);
          bool2 = true;
          break label164;
        }
        boolean bool2 = false;
        localObject2 = null;
        label164:
        int i3 = 0;
        Object localObject3 = null;
        if (!bool2) {
          localObject1 = null;
        }
        if (localObject1 != null)
        {
          paramString = (CharSequence)paramString;
          localObject2 = s;
          Object localObject4 = "";
          paramString = com.truecaller.utils.extensions.o.a(((c.n.k)localObject2).a(paramString, (String)localObject4));
          if (paramString == null) {
            return i1;
          }
          localObject1 = ((Iterable)localObject1).iterator();
          boolean bool3;
          do
          {
            bool2 = ((Iterator)localObject1).hasNext();
            if (!bool2) {
              break;
            }
            localObject2 = ((Iterator)localObject1).next();
            localObject4 = localObject2;
            localObject4 = (String)localObject2;
            bool3 = c.n.m.a((String)localObject4, paramString, i1);
          } while (!bool3);
          break label286;
          bool2 = false;
          localObject2 = null;
          label286:
          if (localObject2 != null) {
            return i1;
          }
          return false;
        }
      }
    }
    return i1;
  }
  
  private final void e(String paramString)
  {
    Object localObject1 = p;
    if (localObject1 != null) {
      ((bn)localObject1).n();
    }
    localObject1 = q;
    if (localObject1 != null) {
      ((bn)localObject1).n();
    }
    localObject1 = h;
    if (localObject1 != null)
    {
      Object localObject2 = (o)b;
      if (localObject2 != null)
      {
        ((o)localObject2).a(false);
        ((o)localObject2).b(false);
        boolean bool = true;
        ((o)localObject2).c(bool);
        localObject3 = "";
        ((o)localObject2).b((String)localObject3);
      }
      localObject2 = new com/truecaller/common/network/account/VerifyTokenRequestDto;
      Object localObject3 = t;
      String str = u;
      ((VerifyTokenRequestDto)localObject2).<init>((String)localObject1, (String)localObject3, str, paramString);
      paramString = (ag)bg.a;
      localObject1 = x;
      localObject3 = new com/truecaller/wizard/d/n$d;
      ((n.d)localObject3).<init>(this, (VerifyTokenRequestDto)localObject2, null);
      localObject3 = (c.g.a.m)localObject3;
      e.b(paramString, (c.d.f)localObject1, (c.g.a.m)localObject3, 2);
      return;
    }
    paramString = new java/lang/IllegalStateException;
    paramString.<init>();
    throw ((Throwable)paramString);
  }
  
  private final String f(String paramString)
  {
    Object localObject1 = j;
    if (localObject1 == null) {
      return null;
    }
    localObject1 = (CharSequence)localObject1;
    int i1 = 1;
    Object localObject2 = new char[i1];
    Object localObject3 = null;
    localObject2[0] = 44;
    localObject1 = (Iterable)c.n.m.a((CharSequence)localObject1, (char[])localObject2, 0, 6);
    localObject2 = new java/util/ArrayList;
    int i2 = c.a.m.a((Iterable)localObject1, 10);
    ((ArrayList)localObject2).<init>(i2);
    localObject2 = (Collection)localObject2;
    localObject1 = ((Iterable)localObject1).iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject1).hasNext();
      if (!bool) {
        break label183;
      }
      localObject4 = c.n.m.b((String)((Iterator)localObject1).next());
      if (localObject4 == null) {
        break label181;
      }
      int i3 = ((Integer)localObject4).intValue();
      localObject3 = paramString;
      localObject3 = (CharSequence)paramString;
      int i4 = paramString.length() - i3 - i1;
      localObject4 = c.n.m.a((CharSequence)localObject3, i4);
      if (localObject4 == null) {
        break;
      }
      i3 = ((Character)localObject4).charValue();
      localObject4 = Character.valueOf(i3);
      ((Collection)localObject2).add(localObject4);
    }
    return null;
    label181:
    return null;
    label183:
    localObject2 = (List)localObject2;
    Object localObject4 = localObject2;
    localObject4 = (Iterable)localObject2;
    localObject3 = (CharSequence)"";
    return c.a.m.a((Iterable)localObject4, (CharSequence)localObject3, null, null, 0, null, null, 62);
  }
  
  private final void f()
  {
    long l1 = i;
    Object localObject1 = E;
    long l2 = ((com.truecaller.utils.a)localObject1).a();
    l1 -= l2;
    l2 = 0L;
    boolean bool1 = l1 < l2;
    if (!bool1)
    {
      e();
      return;
    }
    localObject1 = g;
    if (localObject1 != null)
    {
      int i2 = ((String)localObject1).hashCode();
      int i1 = 114009;
      int i3 = 2;
      boolean bool3 = true;
      boolean bool4;
      boolean bool2;
      if (i2 != i1)
      {
        i1 = 3045982;
        if (i2 != i1) {
          break label479;
        }
        localObject2 = "call";
        bool4 = ((String)localObject1).equals(localObject2);
        if (!bool4) {
          break label479;
        }
        localObject1 = (o)b;
        if (localObject1 != null) {
          ((o)localObject1).a(bool3);
        }
        localObject1 = A;
        localObject2 = t;
        localObject3 = u;
        localObject1 = ((u)localObject1).a((String)localObject2, (String)localObject3);
        localObject2 = (o)b;
        if (localObject2 != null) {
          ((o)localObject2).a((String)localObject1);
        }
        localObject1 = (o)b;
        if (localObject1 != null) {
          ((o)localObject1).f();
        }
        localObject1 = n;
        if (localObject1 != null)
        {
          bool2 = d((String)localObject1);
          if (bool2)
          {
            localObject1 = f((String)localObject1);
            if (localObject1 != null)
            {
              n = null;
              e((String)localObject1);
            }
          }
        }
      }
      else
      {
        localObject2 = "sms";
        bool4 = ((String)localObject1).equals(localObject2);
        if (!bool4) {
          break label479;
        }
        localObject1 = (o)b;
        if (localObject1 != null) {
          ((o)localObject1).b(bool3);
        }
        localObject1 = (o)b;
        if (localObject1 != null) {
          ((o)localObject1).a(l1);
        }
        localObject1 = (o)b;
        if (localObject1 != null)
        {
          bool2 = false;
          localObject2 = null;
          ((o)localObject1).d(false);
        }
        localObject1 = v;
        bool4 = ((f)localObject1).b();
        if (!bool4)
        {
          localObject1 = (ag)bg.a;
          localObject2 = c;
          localObject3 = new com/truecaller/wizard/d/n$c;
          ((n.c)localObject3).<init>(this, null);
          localObject3 = (c.g.a.m)localObject3;
          localObject1 = e.b((ag)localObject1, (c.d.f)localObject2, (c.g.a.m)localObject3, i3);
          q = ((bn)localObject1);
        }
        localObject1 = o;
        if (localObject1 != null)
        {
          o = null;
          localObject4 = (o)b;
          if (localObject4 != null)
          {
            ((o)localObject4).b((String)localObject1);
            return;
          }
          return;
        }
      }
      localObject1 = (ag)bg.a;
      Object localObject2 = c;
      Object localObject3 = new com/truecaller/wizard/d/n$a;
      ((n.a)localObject3).<init>(this, l1, null);
      localObject3 = (c.g.a.m)localObject3;
      Object localObject4 = e.b((ag)localObject1, (c.d.f)localObject2, (c.g.a.m)localObject3, i3);
      p = ((bn)localObject4);
      return;
    }
    label479:
    e.a("NumberVerification", "Client fault. Unknown method");
    int i4 = R.string.VerificationError_general;
    a(i4);
  }
  
  private final void g()
  {
    Object localObject1 = (o)b;
    int i1 = 1;
    if (localObject1 != null) {
      ((o)localObject1).c(i1);
    }
    h();
    localObject1 = new com/truecaller/common/network/account/SendTokenRequestDto;
    Object localObject2 = t;
    Object localObject3 = u;
    int i2 = f + i1;
    Object localObject4 = B.a();
    ((SendTokenRequestDto)localObject1).<init>((String)localObject2, (String)localObject3, i2, (InstallationDetailsDto)localObject4);
    localObject4 = (ag)bg.a;
    localObject2 = x;
    localObject3 = new com/truecaller/wizard/d/n$b;
    ((n.b)localObject3).<init>(this, (SendTokenRequestDto)localObject1, null);
    localObject3 = (c.g.a.m)localObject3;
    e.b((ag)localObject4, (c.d.f)localObject2, (c.g.a.m)localObject3, 2);
  }
  
  private final void h()
  {
    bn localbn = p;
    if (localbn != null) {
      localbn.n();
    }
    localbn = q;
    if (localbn != null) {
      localbn.n();
    }
    g = null;
    h = null;
    i = 0L;
    j = null;
    n = null;
    o = null;
  }
  
  public final void a()
  {
    o localo = (o)b;
    if (localo != null)
    {
      localo.c();
      return;
    }
  }
  
  public final void a(String paramString)
  {
    if (paramString != null)
    {
      Object localObject = paramString;
      localObject = (CharSequence)paramString;
      boolean bool = c.n.m.a((CharSequence)localObject);
      if (!bool)
      {
        localObject = g;
        String str = "call";
        bool = c.g.b.k.a(localObject, str);
        if (bool)
        {
          bool = d(paramString);
          if (bool)
          {
            localObject = f(paramString);
            if (localObject == null) {
              break label83;
            }
            e((String)localObject);
          }
        }
        else
        {
          localObject = g;
          if (localObject != null) {
            return;
          }
        }
        label83:
        n = paramString;
        D.a();
        return;
      }
    }
  }
  
  public final void b(String paramString)
  {
    if (paramString == null) {
      return;
    }
    Object localObject1 = r;
    paramString = (CharSequence)paramString;
    c.g.b.k.b(paramString, "input");
    localObject1 = a.matcher(paramString);
    Object localObject2 = "nativePattern.matcher(input)";
    c.g.b.k.a(localObject1, (String)localObject2);
    boolean bool1 = ((Matcher)localObject1).matches();
    if (!bool1)
    {
      paramString = null;
    }
    else
    {
      localObject2 = new c/n/j;
      ((c.n.j)localObject2).<init>((Matcher)localObject1, paramString);
      paramString = (String)localObject2;
      paramString = (i)localObject2;
    }
    if (paramString != null)
    {
      paramString = paramString.a();
      if (paramString != null)
      {
        int i1 = 1;
        paramString = (String)c.a.m.a(paramString, i1);
        if (paramString != null)
        {
          localObject1 = g;
          localObject2 = "sms";
          boolean bool2 = c.g.b.k.a(localObject1, localObject2);
          if (bool2)
          {
            localObject1 = (o)b;
            if (localObject1 != null) {
              ((o)localObject1).b(paramString);
            }
          }
          else
          {
            localObject1 = g;
            if (localObject1 != null) {
              return;
            }
          }
          o = paramString;
          return;
        }
      }
    }
  }
  
  public final boolean b()
  {
    Object localObject = p;
    if (localObject != null)
    {
      boolean bool1 = ((bn)localObject).av_();
      boolean bool2 = true;
      if (bool1 == bool2)
      {
        h();
        localObject = (o)b;
        if (localObject != null) {
          ((o)localObject).c();
        }
        return bool2;
      }
    }
    return false;
  }
  
  public final void c()
  {
    o localo = (o)b;
    if (localo != null)
    {
      localo.g();
      return;
    }
  }
  
  public final void c(String paramString)
  {
    c.g.b.k.b(paramString, "token");
    String str1 = g;
    String str2 = "sms";
    boolean bool = c.g.b.k.a(str1, str2);
    if (bool) {
      e(paramString);
    }
  }
  
  final void e()
  {
    Object localObject = g;
    String str = "call";
    boolean bool = c.g.b.k.a(localObject, str);
    if (bool)
    {
      int i1 = f;
      int i2 = 1;
      if (i1 == i2)
      {
        localObject = (o)b;
        if (localObject != null)
        {
          i2 = 0;
          str = null;
          ((o)localObject).a(false);
        }
        g();
        return;
      }
    }
    localObject = (o)b;
    if (localObject != null)
    {
      ((o)localObject).c();
      return;
    }
  }
  
  public final void y_()
  {
    h();
    super.y_();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.d.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */