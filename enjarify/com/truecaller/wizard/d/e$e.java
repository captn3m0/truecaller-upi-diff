package com.truecaller.wizard.d;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.wizard.R.string;

public final class e$e
  extends AnimatorListenerAdapter
{
  e$e(e parame) {}
  
  public final void onAnimationStart(Animator paramAnimator)
  {
    k.b(paramAnimator, "animation");
    paramAnimator = e.b(a);
    int i = R.string.CallVerification_title2;
    paramAnimator.setText(i);
    paramAnimator = e.c(a);
    i = R.string.CallVerification_details;
    paramAnimator.setText(i);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.d.e.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */