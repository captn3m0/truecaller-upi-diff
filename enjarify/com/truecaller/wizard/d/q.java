package com.truecaller.wizard.d;

import c.g.b.k;
import com.google.gson.f;
import com.truecaller.common.network.account.SendTokenRequestDto;
import com.truecaller.common.network.account.VerifyTokenRequestDto;
import com.truecaller.common.network.account.a;
import com.truecaller.common.network.account.a.a;

public final class q
  implements p
{
  private final f a;
  
  public q()
  {
    f localf = new com/google/gson/f;
    localf.<init>();
    a = localf;
  }
  
  /* Error */
  private final b a(e.r paramr)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_2
    //   2: aload_1
    //   3: ifnull +14 -> 17
    //   6: aload_1
    //   7: invokevirtual 23	e/r:e	()Ljava/lang/Object;
    //   10: checkcast 25	com/truecaller/common/network/account/TokenResponseDto
    //   13: astore_3
    //   14: goto +5 -> 19
    //   17: aconst_null
    //   18: astore_3
    //   19: aload_1
    //   20: ifnull +88 -> 108
    //   23: aload_1
    //   24: invokevirtual 29	e/r:f	()Lokhttp3/ae;
    //   27: astore_1
    //   28: aload_1
    //   29: ifnull +79 -> 108
    //   32: aload_1
    //   33: invokevirtual 34	okhttp3/ae:f	()Ljava/io/Reader;
    //   36: astore_1
    //   37: aload_1
    //   38: ifnull +70 -> 108
    //   41: aload_1
    //   42: checkcast 36	java/io/Closeable
    //   45: astore_1
    //   46: aload_1
    //   47: astore 4
    //   49: aload_1
    //   50: checkcast 38	java/io/Reader
    //   53: astore 4
    //   55: aload_0
    //   56: getfield 17	com/truecaller/wizard/d/q:a	Lcom/google/gson/f;
    //   59: astore 5
    //   61: ldc 40
    //   63: astore 6
    //   65: aload 5
    //   67: aload 4
    //   69: aload 6
    //   71: invokevirtual 43	com/google/gson/f:a	(Ljava/io/Reader;Ljava/lang/Class;)Ljava/lang/Object;
    //   74: astore 4
    //   76: aload 4
    //   78: checkcast 40	com/truecaller/common/network/account/TokenErrorResponseDto
    //   81: astore 4
    //   83: aload_1
    //   84: aconst_null
    //   85: invokestatic 48	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   88: aload 4
    //   90: astore_2
    //   91: goto +17 -> 108
    //   94: astore_3
    //   95: goto +6 -> 101
    //   98: astore_2
    //   99: aload_2
    //   100: athrow
    //   101: aload_1
    //   102: aload_2
    //   103: invokestatic 48	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   106: aload_3
    //   107: athrow
    //   108: new 50	com/truecaller/wizard/d/b
    //   111: astore_1
    //   112: aload_1
    //   113: aload_3
    //   114: aload_2
    //   115: invokespecial 53	com/truecaller/wizard/d/b:<init>	(Lcom/truecaller/common/network/account/TokenResponseDto;Lcom/truecaller/common/network/account/TokenErrorResponseDto;)V
    //   118: aload_1
    //   119: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	120	0	this	q
    //   0	120	1	paramr	e.r
    //   1	90	2	localObject1	Object
    //   98	17	2	localThrowable	Throwable
    //   13	6	3	localTokenResponseDto1	com.truecaller.common.network.account.TokenResponseDto
    //   94	20	3	localTokenResponseDto2	com.truecaller.common.network.account.TokenResponseDto
    //   47	42	4	localObject2	Object
    //   59	7	5	localf	f
    //   63	7	6	localClass	Class
    // Exception table:
    //   from	to	target	type
    //   99	101	94	finally
    //   49	53	98	finally
    //   55	59	98	finally
    //   69	74	98	finally
    //   76	81	98	finally
  }
  
  public final b a(SendTokenRequestDto paramSendTokenRequestDto)
  {
    k.b(paramSendTokenRequestDto, "requestDto");
    k.b(paramSendTokenRequestDto, "requestDto");
    paramSendTokenRequestDto = a.d().a(paramSendTokenRequestDto).c();
    return a(paramSendTokenRequestDto);
  }
  
  public final b a(VerifyTokenRequestDto paramVerifyTokenRequestDto)
  {
    k.b(paramVerifyTokenRequestDto, "requestDto");
    k.b(paramVerifyTokenRequestDto, "requestDto");
    paramVerifyTokenRequestDto = a.d().a(paramVerifyTokenRequestDto).c();
    return a(paramVerifyTokenRequestDto);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.d.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */