package com.truecaller.wizard.d;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.view.View;
import c.g.b.k;
import c.u;

final class e$d
  implements ValueAnimator.AnimatorUpdateListener
{
  e$d(e parame) {}
  
  public final void onAnimationUpdate(ValueAnimator paramValueAnimator)
  {
    Object localObject = "animation";
    k.a(paramValueAnimator, (String)localObject);
    paramValueAnimator = paramValueAnimator.getAnimatedValue();
    if (paramValueAnimator != null)
    {
      float f1 = ((Float)paramValueAnimator).floatValue();
      localObject = e.a(a);
      float f2 = -e.a(a).getWidth() * f1;
      ((View)localObject).setTranslationX(f2);
      localObject = e.a(a);
      f2 = e.a(a).getHeight() * f1;
      ((View)localObject).setTranslationY(f2);
      return;
    }
    paramValueAnimator = new c/u;
    paramValueAnimator.<init>("null cannot be cast to non-null type kotlin.Float");
    throw paramValueAnimator;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.d.e.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */