package com.truecaller.wizard.d;

import c.g.b.k;
import com.truecaller.common.network.account.TokenErrorResponseDto;
import com.truecaller.common.network.account.TokenResponseDto;

public final class b
{
  final TokenResponseDto a;
  final TokenErrorResponseDto b;
  
  public b(TokenResponseDto paramTokenResponseDto, TokenErrorResponseDto paramTokenErrorResponseDto)
  {
    a = paramTokenResponseDto;
    b = paramTokenErrorResponseDto;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof b;
      if (bool1)
      {
        paramObject = (b)paramObject;
        Object localObject = a;
        TokenResponseDto localTokenResponseDto = a;
        bool1 = k.a(localObject, localTokenResponseDto);
        if (bool1)
        {
          localObject = b;
          paramObject = b;
          boolean bool2 = k.a(localObject, paramObject);
          if (bool2) {
            break label68;
          }
        }
      }
      return false;
    }
    label68:
    return true;
  }
  
  public final int hashCode()
  {
    TokenResponseDto localTokenResponseDto = a;
    int i = 0;
    int j;
    if (localTokenResponseDto != null)
    {
      j = localTokenResponseDto.hashCode();
    }
    else
    {
      j = 0;
      localTokenResponseDto = null;
    }
    j *= 31;
    TokenErrorResponseDto localTokenErrorResponseDto = b;
    if (localTokenErrorResponseDto != null) {
      i = localTokenErrorResponseDto.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("TokenResponse(success=");
    Object localObject = a;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", error=");
    localObject = b;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.d.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */