package com.truecaller.wizard.d;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import c.g.b.k;

public final class e$c
  extends AnimatorListenerAdapter
{
  public final void onAnimationEnd(Animator paramAnimator)
  {
    k.b(paramAnimator, "animation");
    paramAnimator.start();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.d.e.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */