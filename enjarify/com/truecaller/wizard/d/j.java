package com.truecaller.wizard.d;

import android.os.Handler;
import android.os.Looper;
import com.truecaller.common.account.r;
import com.truecaller.common.h.u;
import dagger.a.g;
import javax.inject.Provider;

public final class j
  implements dagger.a.d
{
  private final h a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  private final Provider f;
  private final Provider g;
  private final Provider h;
  private final Provider i;
  private final Provider j;
  private final Provider k;
  private final Provider l;
  private final Provider m;
  
  public static m a(h paramh, p paramp, f paramf, com.truecaller.common.g.a parama, r paramr, u paramu, com.truecaller.common.network.account.b paramb, com.truecaller.common.network.c paramc, c.d.f paramf1, com.truecaller.wizard.utils.j paramj, com.truecaller.common.h.d paramd, com.truecaller.wizard.a.a parama1, com.truecaller.utils.a parama2)
  {
    Object localObject1 = paramh;
    p localp = paramp;
    f localf = paramf;
    Object localObject2 = new com/truecaller/wizard/d/n;
    Object localObject3 = localObject2;
    Object localObject4 = a;
    String str = b;
    c localc = c.c();
    localObject1 = new kotlinx/coroutines/android/b;
    Object localObject5 = localObject2;
    localObject2 = localObject1;
    paramp = (p)localObject3;
    localObject3 = new android/os/Handler;
    paramf = (f)localObject4;
    localObject4 = Looper.getMainLooper();
    ((Handler)localObject3).<init>((Looper)localObject4);
    ((kotlinx.coroutines.android.b)localObject1).<init>((Handler)localObject3, "UI");
    localObject3 = localObject5;
    localObject4 = paramf;
    ((n)localObject5).<init>(paramf, str, localp, localf, localc, (c.d.f)localObject1, paramf1, parama, paramr, paramu, paramb, paramc, paramj, paramd, parama1, parama2);
    return (m)g.a(localObject5, "Cannot return null from a non-@Nullable @Provides method");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.d.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */