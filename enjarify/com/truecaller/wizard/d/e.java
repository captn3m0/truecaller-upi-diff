package com.truecaller.wizard.d;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface.OnDismissListener;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.f;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.CycleInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import c.g.b.k;
import c.u;
import com.truecaller.utils.t.a;
import com.truecaller.wizard.R.id;
import com.truecaller.wizard.R.integer;
import com.truecaller.wizard.R.layout;
import com.truecaller.wizard.R.plurals;
import com.truecaller.wizard.b.a.b;
import com.truecaller.wizard.b.c.b;
import com.truecaller.wizard.internal.components.VerificationEditText;
import com.truecaller.wizard.internal.components.VerificationEditText.a;
import java.util.HashMap;

public final class e
  extends com.truecaller.wizard.b.i
  implements c.b, o
{
  public static final e.a b;
  public m a;
  private View c;
  private TextView d;
  private TextView e;
  private View i;
  private View j;
  private View k;
  private TextView l;
  private ProgressBar m;
  private VerificationEditText n;
  private Button o;
  private final e.b p;
  private final e.j q;
  private HashMap r;
  
  static
  {
    e.a locala = new com/truecaller/wizard/d/e$a;
    locala.<init>((byte)0);
    b = locala;
  }
  
  public e()
  {
    Object localObject = new com/truecaller/wizard/d/e$b;
    ((e.b)localObject).<init>(this);
    p = ((e.b)localObject);
    localObject = new com/truecaller/wizard/d/e$j;
    ((e.j)localObject).<init>(this);
    q = ((e.j)localObject);
  }
  
  public static final Bundle a(String paramString1, String paramString2)
  {
    return e.a.a(paramString1, paramString2);
  }
  
  private final void c(String paramString)
  {
    Object localObject = getContext();
    if (localObject == null) {
      return;
    }
    k.a(localObject, "context ?: return");
    AlertDialog.Builder localBuilder = new android/support/v7/app/AlertDialog$Builder;
    localBuilder.<init>((Context)localObject);
    paramString = (CharSequence)paramString;
    paramString = localBuilder.setMessage(paramString).setPositiveButton(17039370, null).show();
    localObject = new com/truecaller/wizard/d/e$i;
    ((e.i)localObject).<init>(this);
    localObject = (DialogInterface.OnDismissListener)localObject;
    paramString.setOnDismissListener((DialogInterface.OnDismissListener)localObject);
  }
  
  public final m a()
  {
    m localm = a;
    if (localm == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return localm;
  }
  
  public final void a(int paramInt)
  {
    Toast.makeText(getContext(), paramInt, 1).show();
  }
  
  public final void a(long paramLong)
  {
    Object localObject1 = l;
    if (localObject1 == null)
    {
      localObject2 = "smsTimer";
      k.a((String)localObject2);
    }
    localObject1 = (View)localObject1;
    boolean bool = true;
    com.truecaller.utils.extensions.t.a((View)localObject1, bool);
    localObject1 = new com/truecaller/wizard/d/a;
    Object localObject2 = l;
    if (localObject2 == null)
    {
      String str = "smsTimer";
      k.a(str);
    }
    ((a)localObject1).<init>((TextView)localObject2, paramLong);
    ((a)localObject1).start();
  }
  
  public final void a(String paramString)
  {
    TextView localTextView = e;
    if (localTextView == null)
    {
      String str = "detailsView";
      k.a(str);
    }
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void a(boolean paramBoolean)
  {
    View localView = c;
    if (localView == null)
    {
      String str = "callContainer";
      k.a(str);
    }
    com.truecaller.utils.extensions.t.a(localView, paramBoolean);
  }
  
  public final void b(int paramInt)
  {
    String str = getString(paramInt);
    k.a(str, "getString(stringRes)");
    c(str);
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "token");
    VerificationEditText localVerificationEditText = n;
    if (localVerificationEditText == null)
    {
      String str = "verificationEditText";
      k.a(str);
    }
    paramString = (CharSequence)paramString;
    localVerificationEditText.setText(paramString);
  }
  
  public final void b(boolean paramBoolean)
  {
    View localView = k;
    if (localView == null)
    {
      String str = "smsContainer";
      k.a(str);
    }
    com.truecaller.utils.extensions.t.a(localView, paramBoolean);
  }
  
  public final void c()
  {
    k().a("Page_EnterNumber", null);
  }
  
  public final void c(int paramInt)
  {
    Resources localResources = getResources();
    int i1 = R.plurals.VerificationError_limitExceededHours;
    Object[] arrayOfObject = new Object[1];
    Integer localInteger = Integer.valueOf(paramInt);
    arrayOfObject[0] = localInteger;
    String str = localResources.getQuantityString(i1, paramInt, arrayOfObject);
    k.a(str, "resources.getQuantityStr…eededHours, hours, hours)");
    c(str);
  }
  
  public final void c(boolean paramBoolean)
  {
    ProgressBar localProgressBar = m;
    if (localProgressBar == null)
    {
      String str = "progressBar";
      k.a(str);
    }
    com.truecaller.utils.extensions.t.a((View)localProgressBar, paramBoolean);
  }
  
  public final void d()
  {
    k().a("Page_Success");
  }
  
  public final void d(boolean paramBoolean)
  {
    Button localButton = o;
    if (localButton == null)
    {
      String str = "smsLaterButton";
      k.a(str);
    }
    if (paramBoolean) {
      paramBoolean = false;
    } else {
      paramBoolean = true;
    }
    localButton.setVisibility(paramBoolean);
  }
  
  public final void f()
  {
    e locale = this;
    Object localObject1 = new com/truecaller/wizard/d/e$h;
    Object localObject2 = j;
    Object localObject3;
    if (localObject2 == null)
    {
      localObject3 = "robotView";
      k.a((String)localObject3);
    }
    ((e.h)localObject1).<init>(locale, (View)localObject2);
    localObject1 = getContext();
    if (localObject1 != null)
    {
      k.a(localObject1, "it");
      localObject2 = ((Context)localObject1).getResources();
      int i1 = R.integer.wizard_animation_duration_medium;
      int i2 = ((Resources)localObject2).getInteger(i1);
      localObject3 = ((Context)localObject1).getResources();
      int i3 = R.integer.wizard_cyclic_animation_pause;
      i1 = ((Resources)localObject3).getInteger(i3);
      long l1 = i1;
      Object localObject4 = ((Context)localObject1).getResources();
      String str = "context.resources";
      k.a(localObject4, str);
      localObject4 = ((Resources)localObject4).getDisplayMetrics();
      int i4 = 1;
      float f1 = TypedValue.applyDimension(i4, 5.0F, (DisplayMetrics)localObject4);
      localObject4 = i;
      if (localObject4 == null)
      {
        localObject5 = "handleView";
        k.a((String)localObject5);
      }
      int i5 = 2;
      Object localObject6 = new float[i5];
      float[] arrayOfFloat1 = null;
      localObject6[0] = 0.0F;
      f1 = -f1;
      localObject6[i4] = f1;
      localObject4 = ObjectAnimator.ofFloat(localObject4, "translationY", (float[])localObject6);
      long l2 = i2;
      localObject4 = ((ObjectAnimator)localObject4).setDuration(l2);
      k.a(localObject4, "handleUpAnimator");
      ((ObjectAnimator)localObject4).setStartDelay(l1);
      Object localObject5 = i;
      if (localObject5 == null)
      {
        localObject6 = "handleView";
        k.a((String)localObject6);
      }
      float[] arrayOfFloat2 = new float[i5];
      float[] tmp248_246 = arrayOfFloat2;
      tmp248_246[0] = 0.0F;
      tmp248_246[1] = 5.0F;
      localObject5 = ObjectAnimator.ofFloat(localObject5, "rotation", arrayOfFloat2);
      long l3 = i2 * 2;
      localObject5 = ((ObjectAnimator)localObject5).setDuration(l3);
      k.a(localObject5, "handleVibrationAnimator");
      localObject6 = new android/view/animation/CycleInterpolator;
      float f2 = 6.0F;
      ((CycleInterpolator)localObject6).<init>(f2);
      localObject6 = (TimeInterpolator)localObject6;
      ((ObjectAnimator)localObject5).setInterpolator((TimeInterpolator)localObject6);
      int i6 = 3;
      i2 /= i6;
      long l4 = l3;
      long l5 = i2 + l1;
      ((ObjectAnimator)localObject5).setStartDelay(l5);
      localObject2 = i;
      if (localObject2 == null)
      {
        localObject6 = "handleView";
        k.a((String)localObject6);
      }
      arrayOfFloat1 = new float[i5];
      arrayOfFloat1[0] = f1;
      arrayOfFloat1[i4] = 0.0F;
      localObject2 = ObjectAnimator.ofFloat(localObject2, "translationY", arrayOfFloat1).setDuration(l2);
      k.a(localObject2, "handleDownAnimator");
      l1 += l4;
      ((ObjectAnimator)localObject2).setStartDelay(l1);
      localObject3 = new android/animation/AnimatorSet;
      ((AnimatorSet)localObject3).<init>();
      Animator[] arrayOfAnimator = new Animator[3];
      localObject4 = (Animator)localObject4;
      arrayOfAnimator[0] = localObject4;
      localObject5 = (Animator)localObject5;
      arrayOfAnimator[i4] = localObject5;
      localObject2 = (Animator)localObject2;
      arrayOfAnimator[i5] = localObject2;
      ((AnimatorSet)localObject3).playTogether(arrayOfAnimator);
      localObject2 = new com/truecaller/wizard/d/e$c;
      ((e.c)localObject2).<init>();
      localObject2 = (Animator.AnimatorListener)localObject2;
      ((AnimatorSet)localObject3).addListener((Animator.AnimatorListener)localObject2);
      localObject3 = (Animator)localObject3;
      localObject2 = new float[i5];
      Object tmp518_517 = localObject2;
      tmp518_517[0] = 1.0F;
      tmp518_517[1] = 0.25F;
      localObject2 = ValueAnimator.ofFloat((float[])localObject2);
      localObject1 = ((Context)localObject1).getResources();
      i3 = R.integer.wizard_animation_duration_long;
      long l6 = ((Resources)localObject1).getInteger(i3);
      localObject1 = ((ValueAnimator)localObject2).setDuration(l6);
      k.a(localObject1, "animator");
      ((ValueAnimator)localObject1).setStartDelay(3000L);
      localObject2 = new android/view/animation/DecelerateInterpolator;
      ((DecelerateInterpolator)localObject2).<init>();
      localObject2 = (TimeInterpolator)localObject2;
      ((ValueAnimator)localObject1).setInterpolator((TimeInterpolator)localObject2);
      localObject2 = new com/truecaller/wizard/d/e$d;
      ((e.d)localObject2).<init>(locale);
      localObject2 = (ValueAnimator.AnimatorUpdateListener)localObject2;
      ((ValueAnimator)localObject1).addUpdateListener((ValueAnimator.AnimatorUpdateListener)localObject2);
      localObject2 = new com/truecaller/wizard/d/e$e;
      ((e.e)localObject2).<init>(locale);
      localObject2 = (Animator.AnimatorListener)localObject2;
      ((ValueAnimator)localObject1).addListener((Animator.AnimatorListener)localObject2);
      localObject1 = (Animator)localObject1;
      localObject2 = new android/animation/AnimatorSet;
      ((AnimatorSet)localObject2).<init>();
      arrayOfAnimator = new Animator[i5];
      arrayOfAnimator[0] = localObject3;
      arrayOfAnimator[i4] = localObject1;
      ((AnimatorSet)localObject2).playTogether(arrayOfAnimator);
      ((AnimatorSet)localObject2).start();
      return;
    }
  }
  
  public final void g()
  {
    Object localObject1 = getActivity();
    if (localObject1 == null) {
      return;
    }
    k.a(localObject1, "activity ?: return");
    Object localObject2 = ((f)localObject1).getApplication();
    if (localObject2 != null)
    {
      localObject2 = (com.truecaller.common.b.a)localObject2;
      localObject1 = (Activity)localObject1;
      ((com.truecaller.common.b.a)localObject2).a((Activity)localObject1);
      return;
    }
    localObject1 = new c/u;
    ((u)localObject1).<init>("null cannot be cast to non-null type com.truecaller.common.app.ApplicationBase");
    throw ((Throwable)localObject1);
  }
  
  public final boolean i()
  {
    m localm = a;
    if (localm == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return localm.b();
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getArguments();
    String str = null;
    if (paramBundle != null)
    {
      localObject1 = "phone_number";
      paramBundle = paramBundle.getString((String)localObject1);
    }
    else
    {
      paramBundle = null;
    }
    if (paramBundle == null) {
      paramBundle = "";
    }
    Object localObject1 = getArguments();
    if (localObject1 != null) {
      str = ((Bundle)localObject1).getString("country_code");
    }
    if (str == null) {
      str = "";
    }
    localObject1 = getContext();
    if (localObject1 == null) {
      return;
    }
    k.a(localObject1, "context ?: return");
    Object localObject2 = com.truecaller.wizard.b.a.a();
    Object localObject3 = ((Context)localObject1).getApplicationContext();
    if (localObject3 != null)
    {
      localObject3 = ((com.truecaller.common.b.a)localObject3).u();
      localObject2 = ((a.b)localObject2).a((com.truecaller.common.a)localObject3);
      localObject1 = com.truecaller.utils.c.a().a((Context)localObject1).a();
      localObject1 = ((a.b)localObject2).a((com.truecaller.utils.t)localObject1).a();
      localObject2 = new com/truecaller/wizard/d/h;
      localObject3 = k();
      ((h)localObject2).<init>(paramBundle, str, (com.truecaller.wizard.b.c)localObject3);
      ((com.truecaller.wizard.b.d)localObject1).a((h)localObject2).a(this);
      return;
    }
    paramBundle = new c/u;
    paramBundle.<init>("null cannot be cast to non-null type com.truecaller.common.app.ApplicationBase");
    throw paramBundle;
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    int i1 = R.layout.wizard_fragment_verification;
    return paramLayoutInflater.inflate(i1, paramViewGroup, false);
  }
  
  public final void onDestroyView()
  {
    Object localObject1 = getContext();
    if (localObject1 != null)
    {
      localObject1 = com.truecaller.utils.extensions.i.b((Context)localObject1);
      if (localObject1 != null)
      {
        localObject2 = (PhoneStateListener)p;
        ((TelephonyManager)localObject1).listen((PhoneStateListener)localObject2, 0);
      }
    }
    localObject1 = getContext();
    if (localObject1 != null)
    {
      localObject2 = (BroadcastReceiver)q;
      ((Context)localObject1).unregisterReceiver((BroadcastReceiver)localObject2);
    }
    localObject1 = k();
    Object localObject2 = this;
    localObject2 = (c.b)this;
    ((com.truecaller.wizard.b.c)localObject1).b((c.b)localObject2);
    localObject1 = a;
    if (localObject1 == null)
    {
      localObject2 = "presenter";
      k.a((String)localObject2);
    }
    ((m)localObject1).y_();
    super.onDestroyView();
    localObject1 = r;
    if (localObject1 != null) {
      ((HashMap)localObject1).clear();
    }
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    k.b(paramView, "view");
    super.onViewCreated(paramView, paramBundle);
    int i1 = R.id.call_container;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.call_container)");
    c = paramBundle;
    i1 = R.id.title;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.title)");
    paramBundle = (TextView)paramBundle;
    d = paramBundle;
    i1 = R.id.details;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.details)");
    paramBundle = (TextView)paramBundle;
    e = paramBundle;
    i1 = R.id.handle;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.handle)");
    i = paramBundle;
    i1 = R.id.robot;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.robot)");
    j = paramBundle;
    i1 = R.id.sms_container;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.sms_container)");
    k = paramBundle;
    i1 = R.id.timer;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.timer)");
    paramBundle = (TextView)paramBundle;
    l = paramBundle;
    i1 = R.id.progress_bar;
    paramBundle = paramView.findViewById(i1);
    Object localObject = "view.findViewById(R.id.progress_bar)";
    k.a(paramBundle, (String)localObject);
    paramBundle = (ProgressBar)paramBundle;
    m = paramBundle;
    i1 = R.id.input;
    paramView = paramView.findViewById(i1);
    paramBundle = "view.findViewById(R.id.input)";
    k.a(paramView, paramBundle);
    paramView = (VerificationEditText)paramView;
    n = paramView;
    paramView = k;
    if (paramView == null)
    {
      paramBundle = "smsContainer";
      k.a(paramBundle);
    }
    i1 = R.id.wizard_later;
    paramView = paramView.findViewById(i1);
    paramBundle = "smsContainer.findViewById(R.id.wizard_later)";
    k.a(paramView, paramBundle);
    paramView = (Button)paramView;
    o = paramView;
    paramView = a;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramView.a(this);
    paramView = k();
    paramBundle = this;
    paramBundle = (c.b)this;
    paramView.a(paramBundle);
    paramView = getContext();
    if (paramView != null)
    {
      paramView = com.truecaller.utils.extensions.i.b(paramView);
      if (paramView != null)
      {
        paramBundle = (PhoneStateListener)p;
        int i2 = 32;
        paramView.listen(paramBundle, i2);
      }
    }
    paramView = getContext();
    if (paramView != null)
    {
      paramBundle = (BroadcastReceiver)q;
      localObject = new android/content/IntentFilter;
      String str = "android.provider.Telephony.SMS_RECEIVED";
      ((IntentFilter)localObject).<init>(str);
      paramView.registerReceiver(paramBundle, (IntentFilter)localObject);
    }
    paramView = n;
    if (paramView == null)
    {
      paramBundle = "verificationEditText";
      k.a(paramBundle);
    }
    paramBundle = new com/truecaller/wizard/d/e$f;
    paramBundle.<init>(this);
    paramBundle = (VerificationEditText.a)paramBundle;
    paramView.setOnCodeEnteredListener(paramBundle);
    paramView = o;
    if (paramView == null)
    {
      paramBundle = "smsLaterButton";
      k.a(paramBundle);
    }
    paramBundle = new com/truecaller/wizard/d/e$g;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.d.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */