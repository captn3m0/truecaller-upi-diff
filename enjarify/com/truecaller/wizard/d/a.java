package com.truecaller.wizard.d;

import android.os.CountDownTimer;
import android.text.format.DateUtils;
import android.widget.TextView;
import java.lang.ref.WeakReference;

public final class a
  extends CountDownTimer
{
  private final WeakReference a;
  
  public a(TextView paramTextView, long paramLong)
  {
    super(paramLong, 1000L);
    WeakReference localWeakReference = new java/lang/ref/WeakReference;
    localWeakReference.<init>(paramTextView);
    a = localWeakReference;
  }
  
  public final void onFinish() {}
  
  public final void onTick(long paramLong)
  {
    TextView localTextView = (TextView)a.get();
    if (localTextView != null)
    {
      CharSequence localCharSequence = (CharSequence)DateUtils.formatElapsedTime(paramLong / 1000L);
      localTextView.setText(localCharSequence);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */