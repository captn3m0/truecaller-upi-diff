package com.truecaller.wizard.d;

import android.os.Bundle;
import c.g.b.k;

public final class e$a
{
  public static Bundle a(String paramString1, String paramString2)
  {
    k.b(paramString1, "phoneNumber");
    k.b(paramString2, "countryCode");
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putString("phone_number", paramString1);
    localBundle.putString("country_code", paramString2);
    return localBundle;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.d.e.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */