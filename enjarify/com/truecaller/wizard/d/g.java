package com.truecaller.wizard.d;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.b;
import android.support.v4.content.d;
import android.telephony.TelephonyManager;
import c.g.b.k;
import c.u;

public final class g
  implements f
{
  private final TelephonyManager a;
  private final com.truecaller.common.g.a b;
  private final Context c;
  
  public g(com.truecaller.common.g.a parama, Context paramContext)
  {
    b = parama;
    c = paramContext;
    parama = c;
    paramContext = "phone";
    parama = parama.getSystemService(paramContext);
    if (parama != null)
    {
      parama = (TelephonyManager)parama;
      a = parama;
      return;
    }
    parama = new c/u;
    parama.<init>("null cannot be cast to non-null type android.telephony.TelephonyManager");
    throw parama;
  }
  
  private String c()
  {
    Object localObject = c;
    int i = b.a((Context)localObject, "android.permission.READ_PHONE_STATE");
    String str = null;
    if (i != 0) {
      return null;
    }
    try
    {
      localObject = a;
      str = ((TelephonyManager)localObject).getSimSerialNumber();
    }
    catch (SecurityException localSecurityException)
    {
      for (;;) {}
    }
    return str;
  }
  
  public final void a()
  {
    com.truecaller.common.g.a locala = b;
    long l = System.currentTimeMillis();
    locala.b("profileVerificationDate", l);
    locala = b;
    String str = c();
    locala.a("profileSimNumber", str);
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "action");
    d locald = d.a(c);
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramString);
    locald.a(localIntent);
  }
  
  public final boolean b()
  {
    com.truecaller.common.b.a locala = com.truecaller.common.b.a.F();
    k.a(locala, "ApplicationBase.getAppBase()");
    return locala.o();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.d.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */