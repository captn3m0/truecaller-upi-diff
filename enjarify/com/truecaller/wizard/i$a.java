package com.truecaller.wizard;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.truecaller.multisim.SimInfo;
import java.util.List;

final class i$a
  extends BaseAdapter
{
  private final LayoutInflater a;
  private final List b;
  
  i$a(Context paramContext, List paramList)
  {
    paramContext = LayoutInflater.from(paramContext);
    a = paramContext;
    b = paramList;
  }
  
  public final int getCount()
  {
    return b.size() + 1;
  }
  
  public final long getItemId(int paramInt)
  {
    return 0L;
  }
  
  public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    if (paramView == null)
    {
      paramView = a;
      i = R.layout.wizard_subscription_info_item;
      paramView = paramView.inflate(i, paramViewGroup, false);
    }
    int j = R.id.wizard_subscription_name;
    paramViewGroup = (TextView)paramView.findViewById(j);
    int i = R.id.wizard_subscription_icon;
    ImageView localImageView = (ImageView)paramView.findViewById(i);
    Object localObject = b;
    int k = ((List)localObject).size();
    if (paramInt < k)
    {
      SimInfo localSimInfo = (SimInfo)b.get(paramInt);
      localObject = c;
      paramViewGroup.setText((CharSequence)localObject);
      localImageView.setVisibility(0);
      paramInt = a;
      switch (paramInt)
      {
      default: 
        paramInt = R.drawable.ic_sim_questionmark;
        localImageView.setImageResource(paramInt);
        break;
      case 1: 
        paramInt = R.drawable.ic_sim_card_2_small;
        localImageView.setImageResource(paramInt);
        break;
      case 0: 
        paramInt = R.drawable.ic_sim_card_1_small;
        localImageView.setImageResource(paramInt);
        break;
      }
    }
    else
    {
      paramInt = R.string.Welcome_enterManually;
      paramViewGroup.setText(paramInt);
      paramInt = 4;
      localImageView.setVisibility(paramInt);
    }
    return paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.i.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */