package com.truecaller.wizard;

import android.content.Context;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.view.View;
import com.truecaller.common.e.c;
import com.truecaller.common.e.e;
import java.util.Locale;

final class b$2
  extends ClickableSpan
{
  b$2(b paramb, URLSpan paramURLSpan, Context paramContext) {}
  
  public final void onClick(View paramView)
  {
    paramView = c;
    boolean bool = paramView.isAdded();
    if (!bool) {
      return;
    }
    paramView = a.getURL();
    String str = "language";
    bool = paramView.contains(str);
    if (bool)
    {
      paramView = c;
      b.c(paramView);
      str = b.b(b);
      Object localObject = b.b(c).getLanguage();
      str = ab;
      localObject = b;
      b.a(paramView, str, (Context)localObject);
      b.a(c);
      return;
    }
    paramView = a.getURL();
    str = "options";
    bool = paramView.contains(str);
    if (bool)
    {
      paramView = c;
      b.d(paramView);
    }
  }
  
  public final void updateDrawState(TextPaint paramTextPaint)
  {
    super.updateDrawState(paramTextPaint);
    paramTextPaint.setColor(-1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.b.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */