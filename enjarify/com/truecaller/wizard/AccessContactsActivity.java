package com.truecaller.wizard;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import com.truecaller.common.enhancedsearch.EnhancedSearchStateWorker;
import com.truecaller.utils.c;
import com.truecaller.utils.l;
import com.truecaller.utils.t;
import com.truecaller.utils.t.a;
import com.truecaller.wizard.utils.i;

public class AccessContactsActivity
  extends Activity
  implements View.OnClickListener
{
  private l a;
  
  private void a(boolean paramBoolean)
  {
    EnhancedSearchStateWorker.a(paramBoolean);
    finish();
  }
  
  public void onClick(View paramView)
  {
    int i = paramView.getId();
    int j = R.id.deny_button;
    if (i == j)
    {
      a(false);
      return;
    }
    j = R.id.allow_button;
    if (i == j)
    {
      paramView = a;
      String[] arrayOfString = { "android.permission.READ_CONTACTS" };
      boolean bool = paramView.a(arrayOfString);
      j = 1;
      if (bool)
      {
        a(j);
        return;
      }
      paramView = "android.permission.READ_CONTACTS";
      i.a(this, paramView, j);
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = c.a().a(this).a().b();
    a = paramBundle;
    int i = R.layout.wizard_view_access_contacts;
    setContentView(i);
    i = R.id.deny_button;
    findViewById(i).setOnClickListener(this);
    i = R.id.allow_button;
    findViewById(i).setOnClickListener(this);
  }
  
  public void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    i.a(paramArrayOfString, paramArrayOfInt);
    l locall = a;
    paramArrayOfString = new String[] { "android.permission.READ_CONTACTS" };
    paramInt = locall.a(paramArrayOfString);
    if (paramInt != 0)
    {
      paramInt = 1;
      a(paramInt);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.AccessContactsActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */