package com.truecaller.wizard;

import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.f;
import android.support.v4.app.j;
import android.support.v4.app.w.a;
import android.support.v7.app.AlertDialog.Builder;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import com.d.b.ab;
import com.d.b.ai;
import com.d.b.s;
import com.truecaller.common.h.aq.d;
import com.truecaller.common.h.o;
import com.truecaller.common.profile.e;
import com.truecaller.utils.t.a;
import com.truecaller.wizard.b.a.b;
import com.truecaller.wizard.internal.components.EditText;
import com.truecaller.wizard.utils.k;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class g
  extends com.truecaller.wizard.b.i
  implements w.a, TextWatcher, View.OnClickListener, TextView.OnEditorActionListener
{
  public com.truecaller.wizard.a.a a;
  public com.truecaller.common.g.a b;
  public k c;
  private ImageView d;
  private EditText e;
  private EditText i;
  private EditText j;
  private View k;
  private ImageButton l;
  private String m;
  private Uri n;
  private boolean o;
  private final com.truecaller.analytics.a.d p;
  private boolean q;
  private e r;
  private boolean s;
  private final DialogInterface.OnClickListener t;
  
  public g()
  {
    Object localObject = new com/truecaller/analytics/a/d;
    ((com.truecaller.analytics.a.d)localObject).<init>();
    p = ((com.truecaller.analytics.a.d)localObject);
    s = true;
    localObject = new com/truecaller/wizard/-$$Lambda$g$V7AeLvL7QmMjzKlFCpAt_LzHggw;
    ((-..Lambda.g.V7AeLvL7QmMjzKlFCpAt_LzHggw)localObject).<init>(this);
    t = ((DialogInterface.OnClickListener)localObject);
  }
  
  private void a()
  {
    b();
    ((com.truecaller.wizard.b.c)getActivity()).a();
    k localk = c;
    -..Lambda.g.9dMH7zHHGVESzPEEF1h736vPj9I local9dMH7zHHGVESzPEEF1h736vPj9I = new com/truecaller/wizard/-$$Lambda$g$9dMH7zHHGVESzPEEF1h736vPj9I;
    local9dMH7zHHGVESzPEEF1h736vPj9I.<init>(this);
    localk.a(local9dMH7zHHGVESzPEEF1h736vPj9I);
  }
  
  private void a(Map paramMap)
  {
    boolean bool1 = g();
    Object localObject1;
    Object localObject3;
    if (bool1)
    {
      bool1 = o;
      if (bool1)
      {
        localObject1 = n;
        if (localObject1 != null)
        {
          localObject1 = com.truecaller.common.network.util.g.c;
          localObject2 = new java/io/File;
          String str = n.getPath();
          ((File)localObject2).<init>(str);
          localObject1 = okhttp3.ac.a((okhttp3.w)localObject1, (File)localObject2);
          localObject3 = localObject1;
          break label91;
        }
      }
      localObject1 = "avatar_url";
      localObject2 = m;
      paramMap.put(localObject1, localObject2);
    }
    else
    {
      bool1 = false;
      localObject1 = null;
      localObject3 = null;
    }
    label91:
    Object localObject2 = r;
    boolean bool2 = o;
    -..Lambda.g.PIzl3hk3xrHiVraiGAZ-9p8-wyg localPIzl3hk3xrHiVraiGAZ-9p8-wyg = new com/truecaller/wizard/-$$Lambda$g$PIzl3hk3xrHiVraiGAZ-9p8-wyg;
    localPIzl3hk3xrHiVraiGAZ-9p8-wyg.<init>(this);
    ((e)localObject2).a(bool2, (okhttp3.ac)localObject3, false, null, paramMap, false, localPIzl3hk3xrHiVraiGAZ-9p8-wyg);
  }
  
  private void c()
  {
    String str1 = b.a("profileFirstName");
    String str2 = b.a("profileLastName");
    String str3 = b.a("profileEmail");
    e.setText(str1);
    i.setText(str2);
    j.setText(str3);
    str1 = b.a("profileAvatar");
    m = str1;
    i();
  }
  
  private void d()
  {
    Object localObject1 = j;
    boolean bool1 = ((EditText)localObject1).a();
    if (!bool1)
    {
      int i1 = R.string.Profile_InvalidEmail;
      a(i1);
      return;
    }
    localObject1 = getView();
    int i2 = 2;
    com.truecaller.utils.extensions.t.a((View)localObject1, false, i2);
    b();
    localObject1 = new java/util/HashMap;
    ((HashMap)localObject1).<init>();
    Object localObject2 = e.getText().toString();
    Object localObject3 = i.getText().toString();
    String str1 = j.getText().toString();
    String str2 = "first_name";
    ((Map)localObject1).put(str2, localObject2);
    ((Map)localObject1).put("last_name", localObject3);
    localObject2 = "email";
    ((Map)localObject1).put(localObject2, str1);
    boolean bool2 = q;
    if (bool2)
    {
      a((Map)localObject1);
      return;
    }
    localObject2 = r;
    localObject3 = new com/truecaller/wizard/-$$Lambda$g$FT0LtQ_miA5Dqb63nwQYkf8HCDI;
    ((-..Lambda.g.FT0LtQ_miA5Dqb63nwQYkf8HCDI)localObject3).<init>(this, (Map)localObject1);
    ((e)localObject2).a((com.truecaller.common.profile.a)localObject3);
  }
  
  private boolean f()
  {
    EditText localEditText = e;
    boolean bool = localEditText.a();
    if (bool)
    {
      localEditText = i;
      bool = localEditText.a();
      if (bool) {
        return true;
      }
    }
    return false;
  }
  
  private boolean g()
  {
    Object localObject = m;
    boolean bool = TextUtils.isEmpty((CharSequence)localObject);
    if (bool)
    {
      localObject = n;
      if (localObject == null) {
        return false;
      }
    }
    return true;
  }
  
  private void h()
  {
    Intent localIntent = com.truecaller.common.h.n.a(getContext());
    o.a(this, localIntent, 1);
  }
  
  private void i()
  {
    Object localObject1 = n;
    int i1 = 0;
    if (localObject1 == null)
    {
      localObject1 = m;
      boolean bool = TextUtils.isEmpty((CharSequence)localObject1);
      if (!bool)
      {
        localObject1 = Uri.parse(m);
      }
      else
      {
        bool = false;
        localObject1 = null;
      }
    }
    if (localObject1 != null)
    {
      localObject1 = com.d.b.w.a(getContext()).a((Uri)localObject1);
      c = true;
      Object localObject2 = aq.d.b();
      localObject1 = ((ab)localObject1).a((ai)localObject2);
      localObject2 = s.a;
      new s[0];
      localObject1 = ((ab)localObject1).a((s)localObject2);
      localObject2 = d;
      ((ab)localObject1).a((ImageView)localObject2, null);
      return;
    }
    localObject1 = d;
    i1 = R.drawable.wizard_btn_photo;
    ((ImageView)localObject1).setImageResource(i1);
  }
  
  public final android.support.v4.content.c a(int paramInt, Bundle paramBundle)
  {
    int i1 = R.id.wizard_loader_photo;
    if (paramInt == i1)
    {
      g.a locala = new com/truecaller/wizard/g$a;
      Context localContext = getContext();
      locala.<init>(localContext, paramBundle);
      return locala;
    }
    return null;
  }
  
  public final void a(android.support.v4.content.c paramc, Object paramObject)
  {
    e();
    int i1 = f;
    int i3 = R.id.wizard_loader_photo;
    if (i1 == i3)
    {
      boolean bool = paramObject instanceof Uri;
      if (bool)
      {
        paramc = getContext();
        paramObject = (Uri)paramObject;
        paramc = com.truecaller.common.h.n.a(paramc, (Uri)paramObject);
        o.a(this, paramc, 3);
        return;
      }
      int i2 = R.string.Profile_PhotoError;
      a(i2);
    }
  }
  
  public void afterTextChanged(Editable paramEditable)
  {
    paramEditable = k;
    boolean bool = f();
    paramEditable.setEnabled(bool);
  }
  
  public void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    boolean bool = a(paramInt1, paramInt2, paramIntent);
    if (bool) {
      return;
    }
    int i1 = -1;
    if (paramInt2 == i1)
    {
      paramInt2 = 1;
      i1 = 3;
      Object localObject;
      if (paramInt1 == i1)
      {
        localObject = com.truecaller.common.h.n.c(getContext());
        n = ((Uri)localObject);
        m = null;
        o = paramInt2;
        i();
        com.truecaller.common.h.n.e(getContext());
        return;
      }
      if (paramInt1 == paramInt2)
      {
        localObject = getContext();
        Uri localUri1 = com.truecaller.common.h.n.b(getContext());
        localObject = com.truecaller.common.h.n.a((Context)localObject, localUri1);
        o.a(this, (Intent)localObject, i1);
        return;
      }
      paramInt2 = 2;
      if ((paramInt1 == paramInt2) && (paramIntent != null))
      {
        b();
        localObject = getLoaderManager();
        paramInt2 = R.id.wizard_loader_photo;
        paramIntent = paramIntent.getData();
        Uri localUri2 = com.truecaller.common.h.n.b(getContext());
        Bundle localBundle = new android/os/Bundle;
        localBundle.<init>();
        String str = "source";
        localBundle.putParcelable(str, paramIntent);
        paramIntent = "destination";
        localBundle.putParcelable(paramIntent, localUri2);
        ((android.support.v4.app.w)localObject).a(paramInt2, localBundle, this);
      }
    }
  }
  
  public void onClick(View paramView)
  {
    int i1 = paramView.getId();
    int i3 = R.id.wizard_later;
    Object localObject1;
    Object localObject2;
    if (i1 == i3)
    {
      paramView = new android/support/v7/app/AlertDialog$Builder;
      localObject1 = getContext();
      paramView.<init>((Context)localObject1);
      i1 = R.string.Profile_Later_Caption;
      paramView = paramView.setTitle(i1);
      i1 = R.string.Profile_Later_Text;
      paramView = paramView.setMessage(i1);
      i1 = R.string.Profile_Add_Name;
      localObject2 = new com/truecaller/wizard/-$$Lambda$g$V4TWedoI8LbDHH-VQH-ZSK8_x3s;
      ((-..Lambda.g.V4TWedoI8LbDHH-VQH-ZSK8_x3s)localObject2).<init>(this);
      paramView = paramView.setPositiveButton(i1, (DialogInterface.OnClickListener)localObject2);
      i1 = R.string.Profile_Skip;
      localObject2 = new com/truecaller/wizard/-$$Lambda$g$eooW3chGyNMVtIaxkYIEbSP-_YA;
      ((-..Lambda.g.eooW3chGyNMVtIaxkYIEbSP-_YA)localObject2).<init>(this);
      paramView.setNegativeButton(i1, (DialogInterface.OnClickListener)localObject2).show();
      return;
    }
    super.onClick(paramView);
    int i4 = R.id.nextButton;
    if (i1 == i4)
    {
      d();
      return;
    }
    i4 = R.id.photo;
    int i2;
    if (i1 == i4)
    {
      paramView = new android/support/v7/app/AlertDialog$Builder;
      localObject1 = getContext();
      paramView.<init>((Context)localObject1);
      i1 = R.string.Profile_AddPhoto;
      paramView = paramView.setTitle(i1);
      boolean bool = g();
      if (bool) {
        i2 = R.array.Profile_PhotoMenuWithRemove;
      } else {
        i2 = R.array.Profile_PhotoMenu;
      }
      localObject2 = t;
      paramView.setItems(i2, (DialogInterface.OnClickListener)localObject2).show();
      return;
    }
    i4 = R.id.closeButton;
    if (i2 == i4)
    {
      paramView = l;
      i2 = 0;
      localObject1 = null;
      i3 = 2;
      com.truecaller.utils.extensions.t.a(paramView, false, i3);
      paramView = getActivity();
      if (paramView != null)
      {
        paramView = getActivity().getSupportFragmentManager();
        localObject1 = g.class.getName();
        paramView.b((String)localObject1);
      }
    }
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = com.truecaller.wizard.b.a.a();
    Object localObject = com.truecaller.utils.c.a();
    Context localContext = getContext();
    localObject = ((t.a)localObject).a(localContext).a();
    paramBundle = paramBundle.a((com.truecaller.utils.t)localObject);
    localObject = com.truecaller.common.b.a.F().u();
    paramBundle.a((com.truecaller.common.a)localObject).a().a(this);
    paramBundle = b.a("profileCountryIso");
    localObject = com.truecaller.common.b.a.F().u().n();
    boolean bool1 = paramBundle.isEmpty();
    if (!bool1)
    {
      bool2 = ((com.truecaller.common.h.ac)localObject).b(paramBundle);
      if (!bool2)
      {
        bool2 = false;
        paramBundle = null;
        break label122;
      }
    }
    boolean bool2 = true;
    label122:
    s = bool2;
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    int i1 = R.layout.wizard_fragment_profile_input;
    paramLayoutInflater = paramLayoutInflater.inflate(i1, paramViewGroup, false);
    int i2 = R.id.photo;
    paramViewGroup = (ImageView)paramLayoutInflater.findViewById(i2);
    d = paramViewGroup;
    i2 = R.id.firstName;
    paramViewGroup = (EditText)paramLayoutInflater.findViewById(i2);
    e = paramViewGroup;
    i2 = R.id.lastName;
    paramViewGroup = (EditText)paramLayoutInflater.findViewById(i2);
    i = paramViewGroup;
    i2 = R.id.email;
    paramViewGroup = (EditText)paramLayoutInflater.findViewById(i2);
    j = paramViewGroup;
    i2 = R.id.nextButton;
    paramViewGroup = paramLayoutInflater.findViewById(i2);
    k = paramViewGroup;
    i2 = R.id.closeButton;
    paramViewGroup = (ImageButton)paramLayoutInflater.findViewById(i2);
    l = paramViewGroup;
    return paramLayoutInflater;
  }
  
  public boolean onEditorAction(TextView paramTextView, int paramInt, KeyEvent paramKeyEvent)
  {
    int i1 = 6;
    if (paramInt == i1)
    {
      boolean bool = f();
      if (bool) {
        d();
      }
    }
    return false;
  }
  
  public void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    com.truecaller.wizard.utils.i.a(paramArrayOfString, paramArrayOfInt);
    int i1 = 201;
    if (paramInt == i1)
    {
      paramInt = paramArrayOfInt.length;
      if (paramInt > 0)
      {
        paramInt = paramArrayOfInt[0];
        if (paramInt == 0) {
          h();
        }
      }
    }
  }
  
  public void onStart()
  {
    super.onStart();
    com.truecaller.analytics.a.d locald = p;
    long l1 = SystemClock.elapsedRealtime();
    a = l1;
  }
  
  public void onStop()
  {
    super.onStop();
    p.a();
  }
  
  public void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
  
  public void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    int i1 = R.id.nextButton;
    paramView.findViewById(i1).setOnClickListener(this);
    d.setOnClickListener(this);
    e.addTextChangedListener(this);
    paramView = e;
    paramBundle = com.truecaller.wizard.utils.n.b;
    paramView.setInputValidator(paramBundle);
    i.addTextChangedListener(this);
    paramView = i;
    paramBundle = com.truecaller.wizard.utils.n.b;
    paramView.setInputValidator(paramBundle);
    j.addTextChangedListener(this);
    j.setOnEditorActionListener(this);
    paramView = j;
    paramBundle = com.truecaller.wizard.utils.n.c;
    paramView.setInputValidator(paramBundle);
    l.setOnClickListener(this);
    c();
    paramView = com.truecaller.common.b.a.F().b();
    r = paramView;
    paramView = r;
    paramBundle = new com/truecaller/wizard/-$$Lambda$g$w3dYqbyMlIw6iLe6092v0S2TkWI;
    paramBundle.<init>(this);
    paramView.a(paramBundle);
    m();
    com.truecaller.utils.extensions.t.a(e, true, 300L);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */