package com.truecaller.wizard;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import com.truecaller.log.AssertionUtil;
import d.n;
import d.t;
import d.u;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

final class g$a
  extends com.truecaller.wizard.utils.c
{
  private final Uri n;
  private final Uri o;
  
  g$a(Context paramContext, Bundle paramBundle)
  {
    super(paramContext);
    paramContext = "source";
    boolean bool = paramBundle.containsKey(paramContext);
    if (bool)
    {
      paramContext = "destination";
      bool = paramBundle.containsKey(paramContext);
      if (bool) {}
    }
    else
    {
      bool = false;
      paramContext = null;
      String[] arrayOfString = { "Source and destination Uris should be provided via Loader arguments" };
      AssertionUtil.shouldNeverHappen(null, arrayOfString);
    }
    paramContext = (Uri)paramBundle.getParcelable("source");
    n = paramContext;
    paramContext = (Uri)paramBundle.getParcelable("destination");
    o = paramContext;
  }
  
  private Uri p()
  {
    Object localObject1 = h.getContentResolver();
    try
    {
      Object localObject2 = n;
      localObject2 = ((ContentResolver)localObject1).openInputStream((Uri)localObject2);
      Object localObject3 = o;
      localObject1 = ((ContentResolver)localObject1).openOutputStream((Uri)localObject3);
      if ((localObject2 != null) && (localObject1 != null)) {
        try
        {
          localObject3 = n.a((OutputStream)localObject1);
          localObject3 = n.a((t)localObject3);
          u localu = n.a((InputStream)localObject2);
          ((d.d)localObject3).a(localu);
          ((d.d)localObject3).close();
          localObject3 = o;
          return (Uri)localObject3;
        }
        finally
        {
          if (localObject2 != null) {
            ((InputStream)localObject2).close();
          }
          if (localObject1 != null) {
            ((OutputStream)localObject1).close();
          }
        }
      }
      if (localObject2 != null) {
        ((InputStream)localObject2).close();
      }
      if (localObject1 != null) {
        ((OutputStream)localObject1).close();
      }
    }
    catch (IOException localIOException)
    {
      com.truecaller.log.d.a(localIOException);
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.g.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */