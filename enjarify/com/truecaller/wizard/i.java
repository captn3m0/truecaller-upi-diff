package com.truecaller.wizard;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.SpannedString;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.text.style.URLSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.TextView;
import c.l.g;
import com.truecaller.common.h.o;
import com.truecaller.multisim.SimInfo;
import com.truecaller.multisim.h;
import com.truecaller.tcpermissions.f.a;
import com.truecaller.utils.t;
import com.truecaller.utils.t.a;
import com.truecaller.wizard.b.c.b;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import org.c.a.a.a.k;

public class i
  extends b
  implements View.OnClickListener, c.b
{
  public static final String[] e = { "android.permission.READ_SMS", "android.permission.RECEIVE_SMS" };
  private AlertDialog i;
  private boolean j;
  private final List k;
  private List l;
  private boolean m;
  private int n;
  private int o;
  private boolean p;
  private com.truecaller.tcpermissions.l q;
  private com.truecaller.utils.d r;
  private com.truecaller.featuretoggles.e s;
  private com.truecaller.analytics.b t;
  
  public i()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    k = localArrayList;
    p = true;
  }
  
  private void a(List paramList)
  {
    com.truecaller.wizard.utils.e locale = new com/truecaller/wizard/utils/e;
    locale.<init>();
    -..Lambda.i.wPZi3khPB9OFd2sbM7_Y9opLNmY localwPZi3khPB9OFd2sbM7_Y9opLNmY = new com/truecaller/wizard/-$$Lambda$i$wPZi3khPB9OFd2sbM7_Y9opLNmY;
    localwPZi3khPB9OFd2sbM7_Y9opLNmY.<init>(this, paramList);
    a = localwPZi3khPB9OFd2sbM7_Y9opLNmY;
    paramList = getChildFragmentManager();
    locale.show(paramList, "explain_permission_dialog_tag");
    p = false;
  }
  
  private void b(String paramString)
  {
    com.truecaller.analytics.e.a locala = new com/truecaller/analytics/e$a;
    locala.<init>("StartupDialog");
    paramString = locala.a("Type", "DefaultDialer").a("Action", paramString).a();
    t.b(paramString);
  }
  
  private void b(List paramList)
  {
    String[] arrayOfString = new String[paramList.size()];
    paramList = (String[])paramList.toArray(arrayOfString);
    requestPermissions(paramList, 100);
  }
  
  private void j()
  {
    boolean bool = l();
    if (bool)
    {
      c();
      return;
    }
    n();
  }
  
  private void n()
  {
    boolean bool = q();
    if (bool) {
      return;
    }
    t();
    bool = j;
    if (bool)
    {
      o();
      return;
    }
    p();
  }
  
  private void o()
  {
    Object localObject1 = k;
    boolean bool = ((List)localObject1).isEmpty();
    if (bool)
    {
      a("");
      p();
      return;
    }
    localObject1 = new android/support/v7/app/AlertDialog$Builder;
    Object localObject2 = getContext();
    ((AlertDialog.Builder)localObject1).<init>((Context)localObject2);
    int i1 = R.string.Welcome_chooseNumber;
    localObject1 = ((AlertDialog.Builder)localObject1).setTitle(i1);
    localObject2 = new com/truecaller/wizard/i$a;
    Object localObject3 = getContext();
    List localList = k;
    ((i.a)localObject2).<init>((Context)localObject3, localList);
    localObject3 = new com/truecaller/wizard/-$$Lambda$i$t_FFW55p-eB4Dcf0QlPz1u-4DXM;
    ((-..Lambda.i.t_FFW55p-eB4Dcf0QlPz1u-4DXM)localObject3).<init>(this);
    localObject1 = ((AlertDialog.Builder)localObject1).setAdapter((ListAdapter)localObject2, (DialogInterface.OnClickListener)localObject3).create();
    i = ((AlertDialog)localObject1);
    i.show();
  }
  
  private void p()
  {
    boolean bool = j;
    if (bool)
    {
      List localList = k;
      bool = localList.isEmpty();
      if (bool) {}
    }
    else
    {
      bool = g();
      if (bool)
      {
        bool = com.truecaller.wizard.b.c.g();
        if (!bool)
        {
          h();
          return;
        }
      }
    }
    ((com.truecaller.wizard.b.c)getActivity()).a("Page_EnterNumber", null);
  }
  
  private boolean q()
  {
    Object localObject1 = q.c();
    ArrayList localArrayList = new java/util/ArrayList;
    int i1 = localObject1.length;
    int i2 = e.length;
    i1 += i2;
    localArrayList.<init>(i1);
    i1 = localObject1.length;
    i2 = 0;
    int i3 = 0;
    boolean bool1;
    String str;
    Object localObject2;
    String[] arrayOfString;
    boolean bool2;
    for (;;)
    {
      bool1 = true;
      if (i3 >= i1) {
        break;
      }
      str = localObject1[i3];
      localObject2 = f;
      arrayOfString = new String[bool1];
      arrayOfString[0] = str;
      bool2 = ((com.truecaller.utils.l)localObject2).a(arrayOfString);
      if (!bool2)
      {
        localObject2 = getActivity();
        bool2 = com.truecaller.wizard.utils.i.a((Activity)localObject2, str);
        if (bool2)
        {
          localObject1 = getContext();
          int i4 = R.string.Welcome_permissionDenied;
          com.truecaller.wizard.utils.i.a((Context)localObject1, str, i4);
          return bool1;
        }
        localArrayList.add(str);
      }
      i3 += 1;
    }
    boolean bool3 = localArrayList.isEmpty();
    if (bool3) {
      return false;
    }
    localObject1 = e;
    i1 = localObject1.length;
    i3 = 0;
    while (i3 < i1)
    {
      str = localObject1[i3];
      localObject2 = f;
      arrayOfString = new String[bool1];
      arrayOfString[0] = str;
      bool2 = ((com.truecaller.utils.l)localObject2).a(arrayOfString);
      if (!bool2)
      {
        localObject2 = getActivity();
        bool2 = com.truecaller.wizard.utils.i.a((Activity)localObject2, str);
        if (!bool2) {
          localArrayList.add(str);
        }
      }
      i3 += 1;
    }
    bool3 = s();
    if (bool3)
    {
      p = false;
      l = localArrayList;
      localObject1 = o.a(getActivity());
      o.a(this, (Intent)localObject1);
      localObject1 = "Shown";
      b((String)localObject1);
    }
    else
    {
      bool3 = p;
      if (bool3)
      {
        localObject1 = com.truecaller.common.b.a.F().e();
        bool3 = ((com.truecaller.common.h.c)localObject1).b();
        if (bool3) {
          i2 = 1;
        }
      }
      if (i2 != 0) {
        a(localArrayList);
      } else {
        b(localArrayList);
      }
    }
    return bool1;
  }
  
  private boolean r()
  {
    Object localObject1 = r;
    int i1 = ((com.truecaller.utils.d)localObject1).h();
    int i2 = 23;
    if (i1 >= i2)
    {
      localObject1 = r;
      boolean bool1 = ((com.truecaller.utils.d)localObject1).g();
      if (!bool1)
      {
        localObject1 = Build.MANUFACTURER.toLowerCase();
        Object localObject2 = s;
        Object localObject3 = N;
        Object localObject4 = com.truecaller.featuretoggles.e.a;
        int i3 = 110;
        localObject4 = localObject4[i3];
        localObject2 = ((com.truecaller.featuretoggles.f)((com.truecaller.featuretoggles.e.a)localObject3).a((com.truecaller.featuretoggles.e)localObject2, (g)localObject4)).e().toLowerCase();
        localObject3 = ",";
        localObject2 = ((String)localObject2).split((String)localObject3);
        int i4 = localObject2.length;
        int i5 = 0;
        localObject4 = null;
        for (;;)
        {
          i3 = 1;
          if (i5 >= i4) {
            break;
          }
          CharSequence localCharSequence = localObject2[i5];
          boolean bool2 = ((String)localObject1).contains(localCharSequence);
          if (bool2)
          {
            bool1 = true;
            break label167;
          }
          i5 += 1;
        }
        bool1 = false;
        localObject1 = null;
        label167:
        if (!bool1) {
          return i3;
        }
      }
    }
    return false;
  }
  
  private boolean s()
  {
    String str = s.an().e();
    Object localObject = "dialerPermission";
    boolean bool1 = ((String)localObject).equals(str);
    boolean bool2 = r();
    if (bool2)
    {
      localObject = r;
      bool2 = ((com.truecaller.utils.d)localObject).c();
      if ((!bool2) && (bool1)) {
        return true;
      }
    }
    return false;
  }
  
  private void t()
  {
    Object localObject1 = com.truecaller.common.b.a.F().u().g();
    boolean bool1 = ((h)localObject1).j();
    j = bool1;
    Object localObject2 = k;
    ((List)localObject2).clear();
    localObject1 = ((h)localObject1).h().iterator();
    for (;;)
    {
      bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      localObject2 = (SimInfo)((Iterator)localObject1).next();
      if (localObject2 != null)
      {
        Object localObject3 = c;
        boolean bool2 = k.b((CharSequence)localObject3);
        if (!bool2)
        {
          localObject3 = k;
          ((List)localObject3).add(localObject2);
        }
      }
    }
  }
  
  public final boolean i()
  {
    return false;
  }
  
  public void onClick(View paramView)
  {
    super.onClick(paramView);
    int i1 = paramView.getId();
    int i2 = R.id.nextButton;
    if (i1 == i2)
    {
      boolean bool = l();
      if (bool)
      {
        bool = q();
        if (!bool) {
          c();
        }
        return;
      }
      n();
    }
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    ((com.truecaller.wizard.b.c)getActivity()).a(this);
    paramBundle = (com.truecaller.common.b.a)getContext().getApplicationContext();
    Object localObject = com.truecaller.tcpermissions.f.a;
    localObject = f.a.a().c();
    q = ((com.truecaller.tcpermissions.l)localObject);
    localObject = com.truecaller.utils.c.a();
    Context localContext = getContext();
    localObject = ((t.a)localObject).a(localContext).a().c();
    r = ((com.truecaller.utils.d)localObject);
    localObject = paramBundle.v().c();
    t = ((com.truecaller.analytics.b)localObject);
    paramBundle = paramBundle.f();
    s = paramBundle;
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    int i1 = R.layout.wizard_fragment_welcome;
    return paramLayoutInflater.inflate(i1, paramViewGroup, false);
  }
  
  public void onDestroyView()
  {
    super.onDestroyView();
    AlertDialog localAlertDialog = i;
    if (localAlertDialog != null) {
      localAlertDialog.dismiss();
    }
    ((com.truecaller.wizard.b.c)getActivity()).b(this);
  }
  
  public void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    com.truecaller.wizard.utils.i.a(paramArrayOfString, paramArrayOfInt);
    paramInt = paramArrayOfInt.length;
    if (paramInt > 0) {
      paramInt = 1;
    } else {
      paramInt = 0;
    }
    int i1 = 0;
    for (;;)
    {
      int i2 = paramArrayOfInt.length;
      if (i1 >= i2) {
        break;
      }
      i2 = paramArrayOfInt[i1];
      String str = paramArrayOfString[i1];
      int i3 = -1;
      if (i2 == i3)
      {
        String[] arrayOfString = q.c();
        boolean bool = org.c.a.a.a.a.a(arrayOfString, str);
        if (bool)
        {
          paramInt = 0;
          break;
        }
      }
      i1 += 1;
    }
    if (paramInt != 0) {
      j();
    }
  }
  
  public void onResume()
  {
    super.onResume();
    Object localObject1 = l;
    int i1;
    if (localObject1 != null)
    {
      i1 = 1;
    }
    else
    {
      i1 = 0;
      localObject1 = null;
    }
    if (i1 != 0)
    {
      localObject1 = l;
      Object localObject2 = new String[((List)localObject1).size()];
      localObject1 = (String[])((List)localObject1).toArray((Object[])localObject2);
      l = null;
      localObject2 = r;
      boolean bool = ((com.truecaller.utils.d)localObject2).c();
      if (bool)
      {
        b("Enabled");
        j();
        return;
      }
      localObject2 = "Disabled";
      b((String)localObject2);
      int i2 = 100;
      requestPermissions((String[])localObject1, i2);
    }
  }
  
  public void onStart()
  {
    super.onStart();
    int i1 = Build.VERSION.SDK_INT;
    int i2 = 21;
    if (i1 >= i2)
    {
      Object localObject = getActivity().getWindow();
      m = true;
      i2 = ((Window)localObject).getDecorView().getSystemUiVisibility();
      o = i2;
      i2 = ((Window)localObject).getStatusBarColor();
      n = i2;
      View localView = ((Window)localObject).getDecorView();
      int i3 = 1280;
      localView.setSystemUiVisibility(i3);
      i2 = 0;
      localView = null;
      ((Window)localObject).setStatusBarColor(0);
      localObject = ((Window)localObject).getDecorView();
      ((View)localObject).requestApplyInsets();
    }
  }
  
  public void onStop()
  {
    super.onStop();
    int i1 = Build.VERSION.SDK_INT;
    int i2 = 21;
    if (i1 >= i2)
    {
      boolean bool = m;
      if (bool)
      {
        Object localObject = getActivity().getWindow();
        View localView = ((Window)localObject).getDecorView();
        int i3 = o;
        localView.setSystemUiVisibility(i3);
        i2 = n;
        ((Window)localObject).setStatusBarColor(i2);
        localObject = ((Window)localObject).getDecorView();
        ((View)localObject).requestApplyInsets();
      }
    }
  }
  
  public void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    int i1 = R.id.title;
    paramBundle = (TextView)paramView.findViewById(i1);
    Object localObject1 = Typeface.createFromAsset(getContext().getAssets(), "Montserrat-Bold.otf");
    paramBundle.setTypeface((Typeface)localObject1);
    i1 = R.id.nextButton;
    ((Button)paramView.findViewById(i1)).setOnClickListener(this);
    i1 = R.id.terms;
    paramBundle = (TextView)paramView.findViewById(i1);
    a(paramBundle);
    i1 = R.id.language;
    paramBundle = (TextView)paramView.findViewById(i1);
    boolean bool1 = true;
    paramBundle.setLinksClickable(bool1);
    Object localObject2 = LinkMovementMethod.getInstance();
    paramBundle.setMovementMethod((MovementMethod)localObject2);
    localObject2 = getContext();
    int i3 = R.string.Welcome_language;
    localObject1 = new Object[bool1];
    Object localObject3 = b.a((Context)localObject2);
    String str1 = super.a().getLanguage();
    localObject3 = aa;
    int i4 = 0;
    str1 = null;
    localObject1[0] = localObject3;
    int i5 = 0;
    localObject3 = null;
    String str2;
    while (i5 <= 0)
    {
      str2 = localObject1[0];
      boolean bool2 = str2 instanceof String;
      if (bool2) {
        str2 = TextUtils.htmlEncode((String)localObject1[0]);
      } else {
        str2 = localObject1[0];
      }
      localObject1[0] = str2;
      i5 += 1;
    }
    localObject3 = new android/text/SpannedString;
    Object localObject4 = ((Context)localObject2).getText(i3);
    ((SpannedString)localObject3).<init>((CharSequence)localObject4);
    localObject1 = Html.fromHtml(String.format(Html.toHtml((Spanned)localObject3), (Object[])localObject1));
    localObject4 = new android/text/SpannableStringBuilder;
    ((SpannableStringBuilder)localObject4).<init>((CharSequence)localObject1);
    int i2 = ((SpannableStringBuilder)localObject4).length();
    localObject3 = URLSpan.class;
    localObject1 = (URLSpan[])((SpannableStringBuilder)localObject4).getSpans(0, i2, (Class)localObject3);
    i5 = localObject1.length;
    while (i4 < i5)
    {
      str2 = localObject1[i4];
      b.2 local2 = new com/truecaller/wizard/b$2;
      local2.<init>(this, str2, (Context)localObject2);
      int i6 = ((SpannableStringBuilder)localObject4).getSpanStart(str2);
      int i7 = ((SpannableStringBuilder)localObject4).getSpanEnd(str2);
      int i8 = ((SpannableStringBuilder)localObject4).getSpanFlags(str2);
      ((SpannableStringBuilder)localObject4).removeSpan(str2);
      ((SpannableStringBuilder)localObject4).setSpan(local2, i6, i7, i8);
      i4 += 1;
    }
    paramBundle.setText((CharSequence)localObject4);
    i1 = R.id.wizardLogo;
    paramView = paramView.findViewById(i1);
    paramBundle = new com/truecaller/wizard/-$$Lambda$i$pinfU6eOMD1eEhhRY5_12Z5zd-M;
    paramBundle.<init>(this);
    paramView.setOnLongClickListener(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */