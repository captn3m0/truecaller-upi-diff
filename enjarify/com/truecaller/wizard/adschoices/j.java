package com.truecaller.wizard.adschoices;

import c.d.f;
import c.g.a.m;
import c.g.b.k;
import com.truecaller.bb;
import com.truecaller.common.h.ac;
import com.truecaller.common.network.optout.a;
import java.util.concurrent.atomic.AtomicInteger;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.e;

public abstract class j
  extends bb
  implements h.b
{
  final AtomicInteger a;
  final a c;
  final f d;
  private boolean e;
  private final Boolean[] f;
  private final f g;
  private final ac h;
  
  public j(a parama, f paramf1, f paramf2, ac paramac)
  {
    c = parama;
    d = paramf1;
    g = paramf2;
    h = paramac;
    parama = new java/util/concurrent/atomic/AtomicInteger;
    int i = 0;
    paramf1 = null;
    parama.<init>(0);
    a = parama;
    parama = AdsChoice.values();
    int j = parama.length;
    paramf2 = new Boolean[j];
    while (i < j)
    {
      paramac = null;
      paramf2[i] = null;
      i += 1;
    }
    f = paramf2;
  }
  
  private final void b(AdsChoice paramAdsChoice, boolean paramBoolean)
  {
    Object localObject1 = (h.c)b;
    if (localObject1 != null)
    {
      boolean bool = true;
      ((h.c)localObject1).b(bool);
    }
    a.incrementAndGet();
    localObject1 = (ag)bg.a;
    f localf = g;
    Object localObject2 = new com/truecaller/wizard/adschoices/j$b;
    ((j.b)localObject2).<init>(this, paramBoolean, paramAdsChoice, null);
    localObject2 = (m)localObject2;
    e.b((ag)localObject1, localf, (m)localObject2, 2);
  }
  
  private final boolean j()
  {
    Boolean[] arrayOfBoolean = f;
    int i = arrayOfBoolean.length;
    int j = 0;
    boolean bool;
    for (;;)
    {
      bool = true;
      if (j >= i) {
        break;
      }
      Boolean localBoolean = arrayOfBoolean[j];
      if (localBoolean == null) {
        bool = false;
      }
      if (!bool) {
        return false;
      }
      j += 1;
    }
    return bool;
  }
  
  public final void a(AdsChoice paramAdsChoice, boolean paramBoolean)
  {
    k.b(paramAdsChoice, "choice");
    Object localObject = f;
    int i = paramAdsChoice.ordinal();
    localObject = localObject[i];
    Boolean localBoolean = Boolean.valueOf(paramBoolean);
    boolean bool = k.a(localObject, localBoolean);
    if (bool) {
      return;
    }
    localObject = AdsChoice.PERSONALIZED_ADS;
    if (paramAdsChoice == localObject)
    {
      bool = e;
      if ((!bool) && (!paramBoolean))
      {
        paramAdsChoice = (h.c)b;
        if (paramAdsChoice != null) {
          paramAdsChoice.b();
        }
        return;
      }
    }
    b(paramAdsChoice, paramBoolean);
  }
  
  protected void a(AdsChoice paramAdsChoice, boolean paramBoolean1, boolean paramBoolean2)
  {
    k.b(paramAdsChoice, "choice");
    Object localObject = (h.c)b;
    if (localObject != null) {
      ((h.c)localObject).a(paramAdsChoice, paramBoolean1);
    }
    localObject = f;
    int i = paramAdsChoice.ordinal();
    Boolean localBoolean = Boolean.valueOf(paramBoolean1);
    localObject[i] = localBoolean;
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "url");
    h.c localc = (h.c)b;
    if (localc != null)
    {
      localc.a(paramString);
      return;
    }
  }
  
  public abstract boolean a();
  
  public abstract void b();
  
  public final void c()
  {
    boolean bool = j();
    if (bool)
    {
      bool = g();
      if (bool)
      {
        h.c localc = (h.c)b;
        if (localc != null)
        {
          localc.a();
          return;
        }
      }
    }
  }
  
  public final void e()
  {
    e = true;
    AdsChoice localAdsChoice = AdsChoice.PERSONALIZED_ADS;
    b(localAdsChoice, false);
  }
  
  public final void f()
  {
    boolean bool = g();
    if (bool)
    {
      h.c localc = (h.c)b;
      if (localc != null)
      {
        localc.a();
        return;
      }
    }
  }
  
  final boolean g()
  {
    AtomicInteger localAtomicInteger = a;
    int i = localAtomicInteger.get();
    return i == 0;
  }
  
  final void h()
  {
    h.c localc = (h.c)b;
    if (localc != null)
    {
      boolean bool = j();
      if (bool)
      {
        bool = g();
        if (bool)
        {
          bool = true;
          break label37;
        }
      }
      bool = false;
      label37:
      localc.a(bool);
      return;
    }
  }
  
  protected void i() {}
  
  public void y_()
  {
    super.y_();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.adschoices.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */