package com.truecaller.wizard.adschoices;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import c.g.b.k;
import c.u;
import com.truecaller.utils.l;
import com.truecaller.utils.t;
import com.truecaller.utils.t.a;
import com.truecaller.wizard.b.a.b;
import com.truecaller.wizard.b.i;
import java.util.HashMap;

public final class e
  extends i
  implements h.a
{
  public h.b a;
  public h.c b;
  private l c;
  private HashMap d;
  
  public final void E_()
  {
    Object localObject = getContext();
    boolean bool = com.truecaller.wizard.a.a((Context)localObject);
    if (bool)
    {
      k().a("Page_AccessContacts");
      return;
    }
    localObject = k();
    String str = "Page_DrawPermission";
    bool = ((com.truecaller.wizard.b.c)localObject).c(str);
    if (bool)
    {
      localObject = c;
      if (localObject == null)
      {
        str = "permissionUtils";
        k.a(str);
      }
      bool = ((l)localObject).a();
      if (!bool)
      {
        k().a("Page_DrawPermission");
        return;
      }
    }
    k().b();
  }
  
  public final void b()
  {
    super.b();
  }
  
  public final void e()
  {
    super.e();
  }
  
  public final void onAttach(Context paramContext)
  {
    k.b(paramContext, "context");
    super.onAttach(paramContext);
    paramContext = com.truecaller.utils.c.a().a(paramContext).a().b();
    c = paramContext;
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getContext();
    if (paramBundle == null) {
      return;
    }
    k.a(paramBundle, "context ?: return");
    Object localObject1 = com.truecaller.wizard.b.a.a();
    Object localObject2 = paramBundle.getApplicationContext();
    if (localObject2 != null)
    {
      localObject2 = ((com.truecaller.common.b.a)localObject2).u();
      localObject1 = ((a.b)localObject1).a((com.truecaller.common.a)localObject2);
      paramBundle = com.truecaller.utils.c.a().a(paramBundle).a();
      paramBundle = ((a.b)localObject1).a(paramBundle).a();
      localObject1 = new com/truecaller/wizard/adschoices/f;
      localObject2 = this;
      localObject2 = (h.a)this;
      ((f)localObject1).<init>((h.a)localObject2);
      paramBundle.a((f)localObject1).a(this);
      return;
    }
    paramBundle = new c/u;
    paramBundle.<init>("null cannot be cast to non-null type com.truecaller.common.app.ApplicationBase");
    throw paramBundle;
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    paramBundle = b;
    if (paramBundle == null)
    {
      String str = "view";
      k.a(str);
    }
    return paramBundle.d().a(paramLayoutInflater, paramViewGroup, true);
  }
  
  public final void onDestroyView()
  {
    super.onDestroyView();
    Object localObject = b;
    if (localObject == null)
    {
      String str = "view";
      k.a(str);
    }
    ((h.c)localObject).d().f();
    localObject = d;
    if (localObject != null) {
      ((HashMap)localObject).clear();
    }
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    String str = "v";
    k.b(paramView, str);
    super.onViewCreated(paramView, paramBundle);
    paramView = b;
    if (paramView == null)
    {
      paramBundle = "view";
      k.a(paramBundle);
    }
    paramView.d().e();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.adschoices.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */