package com.truecaller.wizard.adschoices;

public enum AdsChoiceOptOutStatus
{
  static
  {
    AdsChoiceOptOutStatus[] arrayOfAdsChoiceOptOutStatus = new AdsChoiceOptOutStatus[3];
    AdsChoiceOptOutStatus localAdsChoiceOptOutStatus = new com/truecaller/wizard/adschoices/AdsChoiceOptOutStatus;
    localAdsChoiceOptOutStatus.<init>("OPTED_OUT", 0);
    OPTED_OUT = localAdsChoiceOptOutStatus;
    arrayOfAdsChoiceOptOutStatus[0] = localAdsChoiceOptOutStatus;
    localAdsChoiceOptOutStatus = new com/truecaller/wizard/adschoices/AdsChoiceOptOutStatus;
    int i = 1;
    localAdsChoiceOptOutStatus.<init>("OPTED_IN", i);
    OPTED_IN = localAdsChoiceOptOutStatus;
    arrayOfAdsChoiceOptOutStatus[i] = localAdsChoiceOptOutStatus;
    localAdsChoiceOptOutStatus = new com/truecaller/wizard/adschoices/AdsChoiceOptOutStatus;
    i = 2;
    localAdsChoiceOptOutStatus.<init>("UNKNOWN", i);
    UNKNOWN = localAdsChoiceOptOutStatus;
    arrayOfAdsChoiceOptOutStatus[i] = localAdsChoiceOptOutStatus;
    $VALUES = arrayOfAdsChoiceOptOutStatus;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.adschoices.AdsChoiceOptOutStatus
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */