package com.truecaller.wizard.adschoices;

import com.truecaller.common.network.optout.OptOutRestAdapter.OptOutsDto;
import java.util.List;

public final class c
{
  private static final AdsChoiceOptOutStatus a(OptOutRestAdapter.OptOutsDto paramOptOutsDto, String paramString)
  {
    List localList = paramOptOutsDto.getOptOuts();
    boolean bool1 = localList.contains(paramString);
    if (bool1) {
      return AdsChoiceOptOutStatus.OPTED_OUT;
    }
    paramOptOutsDto = paramOptOutsDto.getOptIns();
    boolean bool2 = paramOptOutsDto.contains(paramString);
    if (bool2) {
      return AdsChoiceOptOutStatus.OPTED_IN;
    }
    return AdsChoiceOptOutStatus.UNKNOWN;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.adschoices.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */