package com.truecaller.wizard.adschoices;

import com.truecaller.wizard.R.drawable;
import com.truecaller.wizard.R.id;
import com.truecaller.wizard.R.string;

public enum AdsChoice
{
  private final c.g.a.b accessDto;
  private final int icon;
  private final int id;
  private final String moreInfoUrl;
  private final c.g.a.b optIn;
  private final c.g.a.b optOut;
  private final int text;
  private final int title;
  
  static
  {
    AdsChoice[] arrayOfAdsChoice = new AdsChoice[2];
    AdsChoice localAdsChoice = new com/truecaller/wizard/adschoices/AdsChoice;
    int i = R.id.adsCard;
    int j = R.drawable.ic_show_ads;
    int k = R.string.AdsChoices_Ads_Title;
    int m = R.string.AdsChoices_Ads_Text;
    Object localObject1 = a.a;
    Object localObject2 = localObject1;
    localObject2 = (c.g.a.b)localObject1;
    localObject1 = AdsChoice.1.a;
    Object localObject3 = localObject1;
    localObject3 = (c.g.a.b)localObject1;
    localObject1 = AdsChoice.2.a;
    Object localObject4 = localObject1;
    localObject4 = (c.g.a.b)localObject1;
    localObject1 = localAdsChoice;
    localAdsChoice.<init>("PERSONALIZED_ADS", 0, i, j, k, m, "https://privacy.truecaller.com/ads", (c.g.a.b)localObject2, (c.g.a.b)localObject3, (c.g.a.b)localObject4);
    PERSONALIZED_ADS = localAdsChoice;
    arrayOfAdsChoice[0] = localAdsChoice;
    localObject1 = new com/truecaller/wizard/adschoices/AdsChoice;
    int n = R.id.dmCard;
    int i1 = R.drawable.ic_deals_and_promo;
    int i2 = R.string.AdsChoices_dm_title;
    int i3 = R.string.AdsChoices_dm_text;
    Object localObject5 = b.a;
    Object localObject6 = localObject5;
    localObject6 = (c.g.a.b)localObject5;
    localObject5 = AdsChoice.3.a;
    Object localObject7 = localObject5;
    localObject7 = (c.g.a.b)localObject5;
    localObject5 = AdsChoice.4.a;
    Object localObject8 = localObject5;
    localObject8 = (c.g.a.b)localObject5;
    ((AdsChoice)localObject1).<init>("DIRECT_MARKETING", 1, n, i1, i2, i3, null, (c.g.a.b)localObject6, (c.g.a.b)localObject7, (c.g.a.b)localObject8);
    DIRECT_MARKETING = (AdsChoice)localObject1;
    arrayOfAdsChoice[1] = localObject1;
    $VALUES = arrayOfAdsChoice;
  }
  
  private AdsChoice(int paramInt2, int paramInt3, int paramInt4, int paramInt5, String paramString1, c.g.a.b paramb1, c.g.a.b paramb2, c.g.a.b paramb3)
  {
    id = paramInt2;
    icon = paramInt3;
    title = paramInt4;
    text = paramInt5;
    moreInfoUrl = paramString1;
    accessDto = paramb1;
    optOut = paramb2;
    optIn = paramb3;
  }
  
  public final c.g.a.b getAccessDto()
  {
    return accessDto;
  }
  
  public final int getIcon()
  {
    return icon;
  }
  
  public final int getId()
  {
    return id;
  }
  
  public final String getMoreInfoUrl()
  {
    return moreInfoUrl;
  }
  
  public final c.g.a.b getOptIn()
  {
    return optIn;
  }
  
  public final c.g.a.b getOptOut()
  {
    return optOut;
  }
  
  public final int getText()
  {
    return text;
  }
  
  public final int getTitle()
  {
    return title;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.adschoices.AdsChoice
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */