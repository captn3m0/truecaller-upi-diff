package com.truecaller.wizard.adschoices;

import c.d.a.a;
import c.d.c;
import c.d.f;
import c.g.a.b;
import c.g.a.m;
import c.l;
import c.o.b;
import c.x;
import java.util.concurrent.atomic.AtomicInteger;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

final class j$b
  extends c.d.b.a.k
  implements m
{
  boolean a;
  int b;
  private ag f;
  
  j$b(j paramj, boolean paramBoolean, AdsChoice paramAdsChoice, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/wizard/adschoices/j$b;
    j localj = c;
    boolean bool = d;
    AdsChoice localAdsChoice = e;
    localb.<init>(localj, bool, localAdsChoice, paramc);
    paramObject = (ag)paramObject;
    f = ((ag)paramObject);
    return localb;
  }
  
  public final Object a(Object paramObject)
  {
    a locala = a.a;
    int i = b;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        throw a;
      }
      break;
    case 0: 
      int j = paramObject instanceof o.b;
      if (j != 0) {
        break label249;
      }
      boolean bool2 = d;
      j = 1;
      if (bool2 == j)
      {
        paramObject = e.getOptIn();
        localObject1 = c.c;
        paramObject = (Boolean)((b)paramObject).invoke(localObject1);
        bool2 = ((Boolean)paramObject).booleanValue();
      }
      else
      {
        if (bool2) {
          break label239;
        }
        paramObject = e.getOptOut();
        localObject1 = c.c;
        paramObject = (Boolean)((b)paramObject).invoke(localObject1);
        bool2 = ((Boolean)paramObject).booleanValue();
      }
      c.a.decrementAndGet();
      Object localObject1 = c.d;
      Object localObject2 = new com/truecaller/wizard/adschoices/j$b$1;
      ((j.b.1)localObject2).<init>(this, bool2, null);
      localObject2 = (m)localObject2;
      a = bool2;
      b = j;
      paramObject = g.a((f)localObject1, (m)localObject2, this);
      if (paramObject == locala) {
        return locala;
      }
      break;
    }
    return x.a;
    label239:
    paramObject = new c/l;
    ((l)paramObject).<init>();
    throw ((Throwable)paramObject);
    label249:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.adschoices.j.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */