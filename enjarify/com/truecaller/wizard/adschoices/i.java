package com.truecaller.wizard.adschoices;

import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Resources;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import c.g.a.m;
import c.g.b.k;
import com.truecaller.common.h.o;
import com.truecaller.utils.extensions.p;
import com.truecaller.utils.extensions.t;
import com.truecaller.wizard.R.color;
import com.truecaller.wizard.R.id;
import com.truecaller.wizard.R.layout;
import com.truecaller.wizard.R.string;
import java.util.HashMap;
import java.util.Map;

public final class i
  implements h.c, h.d
{
  final h.b a;
  private final Map b;
  private View c;
  private View d;
  private final h.d e;
  private final h.a f;
  
  public i(h.a parama, h.b paramb)
  {
    f = parama;
    a = paramb;
    parama = new java/util/HashMap;
    parama.<init>();
    parama = (Map)parama;
    b = parama;
    parama = this;
    parama = (h.d)this;
    e = parama;
  }
  
  private static void a(View paramView, boolean paramBoolean)
  {
    int i = R.id.check;
    ImageView localImageView = (ImageView)paramView.findViewById(i);
    if (localImageView != null)
    {
      int j;
      if (paramBoolean) {
        j = 0;
      } else {
        j = 4;
      }
      localImageView.setVisibility(j);
    }
    i = R.id.text;
    paramView = (TextView)paramView.findViewById(i);
    if (paramView != null)
    {
      paramView.setEnabled(paramBoolean);
      return;
    }
  }
  
  private final Context g()
  {
    View localView = c;
    if (localView != null) {
      return localView.getContext();
    }
    return null;
  }
  
  public final View a(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, boolean paramBoolean)
  {
    i locali = this;
    LayoutInflater localLayoutInflater = paramLayoutInflater;
    boolean bool = paramBoolean;
    k.b(paramLayoutInflater, "inflater");
    int i = R.layout.wizard_fragment_ads_choices;
    Object localObject1 = null;
    Object localObject2 = paramViewGroup;
    View localView1 = paramLayoutInflater.inflate(i, paramViewGroup, false);
    c = localView1;
    i = R.id.nextButton;
    Object localObject3 = (Button)localView1.findViewById(i);
    localObject2 = new com/truecaller/wizard/adschoices/i$b;
    ((i.b)localObject2).<init>(this, paramLayoutInflater, paramBoolean);
    localObject2 = (View.OnClickListener)localObject2;
    ((Button)localObject3).setOnClickListener((View.OnClickListener)localObject2);
    i = R.id.backButton;
    localObject3 = (ImageView)localView1.findViewById(i);
    localObject2 = new com/truecaller/wizard/adschoices/i$c;
    ((i.c)localObject2).<init>(this, paramLayoutInflater, paramBoolean);
    localObject2 = (View.OnClickListener)localObject2;
    ((ImageView)localObject3).setOnClickListener((View.OnClickListener)localObject2);
    AdsChoice[] arrayOfAdsChoice = AdsChoice.values();
    int j = arrayOfAdsChoice.length;
    int k = 0;
    int n;
    Object localObject4;
    while (k < j)
    {
      AdsChoice localAdsChoice = arrayOfAdsChoice[k];
      i = R.layout.wizard_card_ad_choices;
      localObject2 = c;
      int m;
      if (localObject2 != null)
      {
        m = R.id.content;
        localObject2 = (LinearLayout)((View)localObject2).findViewById(m);
      }
      else
      {
        n = 0;
        localObject2 = null;
      }
      localObject2 = (ViewGroup)localObject2;
      View localView2 = localLayoutInflater.inflate(i, (ViewGroup)localObject2, false);
      i = localAdsChoice.getId();
      localView2.setId(i);
      i = R.id.cardImage;
      localObject3 = (ImageView)localView2.findViewById(i);
      n = localAdsChoice.getIcon();
      ((ImageView)localObject3).setImageResource(n);
      i = R.id.cardTitle;
      localObject3 = (TextView)localView2.findViewById(i);
      n = localAdsChoice.getTitle();
      ((TextView)localObject3).setText(n);
      i = R.id.cardText;
      localObject3 = (TextView)localView2.findViewById(i);
      n = localAdsChoice.getText();
      ((TextView)localObject3).setText(n);
      localObject3 = localAdsChoice.getMoreInfoUrl();
      if (localObject3 != null)
      {
        i = R.id.cardReadMore;
        localObject3 = (TextView)localView2.findViewById(i);
        k.a(localObject3, "cardReadMore");
        t.a((View)localObject3);
        i = R.id.cardReadMore;
        localObject3 = (TextView)localView2.findViewById(i);
        k.a(localObject3, "cardReadMore");
        n = R.string.LinkTemplate;
        m = 2;
        localObject4 = new Object[m];
        localObject5 = localAdsChoice.getMoreInfoUrl();
        localObject4[0] = localObject5;
        localObject5 = localView2.getContext();
        int i1 = R.string.ReadMore;
        localObject5 = ((Context)localObject5).getString(i1);
        localObject6 = "context.getString(R.string.ReadMore)";
        k.a(localObject5, (String)localObject6);
        i1 = 1;
        localObject4[i1] = localObject5;
        localObject3 = p.a((TextView)localObject3, n, (Object[])localObject4);
        localObject2 = LinkMovementMethod.getInstance();
        ((TextView)localObject3).setMovementMethod((MovementMethod)localObject2);
        localObject2 = new com/truecaller/wizard/adschoices/i$a;
        ((i.a)localObject2).<init>(locali, (TextView)localObject3);
        localObject2 = (m)localObject2;
        p.a((TextView)localObject3, (m)localObject2);
      }
      else
      {
        i = R.id.cardReadMore;
        localObject3 = (TextView)localView2.findViewById(i);
        localObject2 = "cardReadMore";
        k.a(localObject3, (String)localObject2);
        localObject3 = (View)localObject3;
        t.a((View)localObject3, false);
      }
      i = R.id.selectionNo;
      localObject3 = localView2.findViewById(i);
      Object localObject7 = localObject3;
      localObject7 = (LinearLayout)localObject3;
      Object localObject8 = new com/truecaller/wizard/adschoices/i$d;
      localObject3 = localObject8;
      localObject2 = localAdsChoice;
      localObject4 = localView1;
      Object localObject5 = this;
      Object localObject6 = paramLayoutInflater;
      localObject1 = localObject7;
      ((i.d)localObject8).<init>(localAdsChoice, localView1, this, paramLayoutInflater, paramBoolean);
      localObject3 = (View.OnClickListener)localObject8;
      ((LinearLayout)localObject7).setOnClickListener((View.OnClickListener)localObject3);
      i = R.id.selectionYes;
      localObject3 = localView2.findViewById(i);
      localObject1 = localObject3;
      localObject1 = (LinearLayout)localObject3;
      localObject8 = new com/truecaller/wizard/adschoices/i$e;
      localObject3 = localObject8;
      ((i.e)localObject8).<init>(localAdsChoice, localView1, this, paramLayoutInflater, paramBoolean);
      localObject3 = (View.OnClickListener)localObject8;
      ((LinearLayout)localObject1).setOnClickListener((View.OnClickListener)localObject3);
      i = R.id.content;
      localObject3 = (LinearLayout)localView1.findViewById(i);
      k.a(localObject3, "content");
      localObject3 = (View)localObject3;
      n = R.id.container;
      ((LinearLayout)((View)localObject3).findViewById(n)).addView(localView2);
      localObject3 = b;
      localObject2 = "view";
      k.a(localView2, (String)localObject2);
      ((Map)localObject3).put(localAdsChoice, localView2);
      k += 1;
      localObject1 = null;
    }
    i = 8;
    if (bool)
    {
      n = R.id.backButton;
      localObject2 = (ImageView)localView1.findViewById(n);
      localObject4 = "backButton";
      k.a(localObject2, (String)localObject4);
      ((ImageView)localObject2).setVisibility(i);
      i = R.id.nextButton;
      localObject3 = (View)localView1.findViewById(i);
      d = ((View)localObject3);
    }
    else
    {
      n = R.id.nextButton;
      localObject2 = (Button)localView1.findViewById(n);
      localObject4 = "nextButton";
      k.a(localObject2, (String)localObject4);
      ((Button)localObject2).setVisibility(i);
    }
    k.a(localView1, "inflater.inflate(R.layou…E\n            }\n        }");
    return localView1;
  }
  
  public final void a()
  {
    f.E_();
  }
  
  public final void a(AdsChoice paramAdsChoice, boolean paramBoolean)
  {
    k.b(paramAdsChoice, "choice");
    Object localObject = b;
    paramAdsChoice = (View)((Map)localObject).get(paramAdsChoice);
    if (paramAdsChoice == null) {
      return;
    }
    int i = R.id.selectionYes;
    localObject = (LinearLayout)paramAdsChoice.findViewById(i);
    k.a(localObject, "view.selectionYes");
    a((View)localObject, paramBoolean);
    i = R.id.selectionNo;
    paramAdsChoice = (LinearLayout)paramAdsChoice.findViewById(i);
    k.a(paramAdsChoice, "view.selectionNo");
    paramAdsChoice = (View)paramAdsChoice;
    paramBoolean ^= true;
    a(paramAdsChoice, paramBoolean);
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "link");
    Context localContext = g();
    if (localContext == null) {
      return;
    }
    o.d(localContext, paramString);
  }
  
  public final void a(boolean paramBoolean)
  {
    View localView = d;
    if (localView != null)
    {
      localView.setEnabled(paramBoolean);
      return;
    }
  }
  
  public final void b()
  {
    Object localObject1 = g();
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = new android/support/v7/app/AlertDialog$Builder;
    ((AlertDialog.Builder)localObject2).<init>((Context)localObject1);
    int i = R.string.AdsChoices_Ads_WarningTitle;
    localObject2 = ((AlertDialog.Builder)localObject2).setTitle(i);
    i = R.string.AdsChoices_Ads_WarningText;
    localObject2 = ((AlertDialog.Builder)localObject2).setMessage(i);
    i = R.string.Cancel;
    localObject2 = ((AlertDialog.Builder)localObject2).setPositiveButton(i, null);
    i = R.string.AdsChoices_Ads_TurnOff;
    Object localObject3 = new com/truecaller/wizard/adschoices/i$f;
    ((i.f)localObject3).<init>(this);
    localObject3 = (DialogInterface.OnClickListener)localObject3;
    localObject2 = ((AlertDialog.Builder)localObject2).setNegativeButton(i, (DialogInterface.OnClickListener)localObject3).show();
    localObject1 = ((Context)localObject1).getResources();
    Button localButton = ((AlertDialog)localObject2).getButton(-1);
    int j = R.color.wizard_blue;
    j = ((Resources)localObject1).getColor(j);
    localButton.setTextColor(j);
    localObject2 = ((AlertDialog)localObject2).getButton(-2);
    i = R.color.wizard_gray_medium;
    int k = ((Resources)localObject1).getColor(i);
    ((Button)localObject2).setTextColor(k);
  }
  
  public final void b(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      f.b();
      return;
    }
    f.e();
  }
  
  public final void c()
  {
    Context localContext = g();
    if (localContext == null) {
      return;
    }
    int i = R.string.NetworkError;
    Toast.makeText(localContext, i, 1).show();
  }
  
  public final h.d d()
  {
    return e;
  }
  
  public final void e()
  {
    a.a(this);
  }
  
  public final void f()
  {
    a.y_();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.adschoices.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */