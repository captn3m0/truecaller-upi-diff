package com.truecaller.wizard.b;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;
import com.truecaller.common.account.r;
import com.truecaller.common.b.e;
import com.truecaller.utils.l;
import com.truecaller.utils.t;
import com.truecaller.utils.t.a;
import com.truecaller.wizard.R.id;
import com.truecaller.wizard.R.string;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.c.a.a.a.g;

public abstract class i
  extends Fragment
  implements View.OnClickListener, c.a
{
  private ProgressDialog a;
  private View b;
  private boolean c;
  protected l f;
  public com.truecaller.utils.i g;
  protected com.truecaller.common.g.a h;
  
  private boolean a()
  {
    android.support.v4.app.f localf = getActivity();
    if (localf == null) {
      return false;
    }
    return ((com.truecaller.common.b.a)localf.getApplication()).o();
  }
  
  protected void a(int paramInt)
  {
    Context localContext = getContext();
    if (localContext != null)
    {
      Toast localToast = Toast.makeText(localContext, paramInt, 0);
      localToast.show();
    }
  }
  
  public boolean a(int paramInt1, int paramInt2, Intent paramIntent)
  {
    return false;
  }
  
  protected void b()
  {
    Object localObject1 = getActivity();
    if (localObject1 != null) {
      localObject1 = getActivity();
    } else {
      localObject1 = getContext();
    }
    if (localObject1 != null)
    {
      Object localObject2 = a;
      if (localObject2 == null)
      {
        localObject2 = new android/app/ProgressDialog;
        ((ProgressDialog)localObject2).<init>((Context)localObject1);
        a = ((ProgressDialog)localObject2);
        a.setCancelable(false);
        a.setCanceledOnTouchOutside(false);
        localObject1 = a;
        int i = R.string.Loading;
        localObject2 = getString(i);
        ((ProgressDialog)localObject1).setMessage((CharSequence)localObject2);
      }
      localObject1 = a;
      ((ProgressDialog)localObject1).show();
    }
  }
  
  public void e()
  {
    ProgressDialog localProgressDialog = a;
    if (localProgressDialog != null)
    {
      localProgressDialog.dismiss();
      localProgressDialog = null;
      a = null;
    }
  }
  
  protected final c k()
  {
    return (c)getActivity();
  }
  
  protected final boolean l()
  {
    android.support.v4.app.f localf = getActivity();
    if (localf == null) {
      return false;
    }
    return ((com.truecaller.common.b.a)localf.getApplication()).u().k().c();
  }
  
  protected final void m()
  {
    View localView = b;
    if (localView == null) {
      return;
    }
    localView.setVisibility(0);
  }
  
  public void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    paramContext = com.truecaller.utils.c.a().a(paramContext).a();
    l locall = paramContext.b();
    f = locall;
    paramContext = paramContext.f();
    g = paramContext;
    paramContext = com.truecaller.common.b.a.F().u().c();
    h = paramContext;
  }
  
  public void onClick(View paramView)
  {
    int i = paramView.getId();
    int j = R.id.wizard_later;
    if (i == j)
    {
      paramView = getActivity();
      com.truecaller.common.b.a locala = (com.truecaller.common.b.a)paramView.getApplication();
      locala.a(paramView);
    }
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = "languageAuto";
    int i = 1;
    boolean bool = e.a(paramBundle, i);
    if (!bool)
    {
      paramBundle = getContext();
      localObject = g.a(e.a("language"));
      com.truecaller.common.e.f.a(paramBundle, (Locale)localObject);
    }
    paramBundle = (c)getActivity();
    Object localObject = a;
    if (localObject == null)
    {
      localObject = new java/util/ArrayList;
      ((ArrayList)localObject).<init>(i);
      a = ((List)localObject);
    }
    a.add(this);
  }
  
  public void onDestroyView()
  {
    super.onDestroyView();
    e();
    Object localObject = (c)getActivity();
    List localList = a;
    if (localList != null)
    {
      localObject = a;
      ((List)localObject).remove(this);
    }
  }
  
  public void onResume()
  {
    super.onResume();
    boolean bool = c;
    if (bool) {
      m();
    }
  }
  
  public void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    boolean bool = a();
    if (!bool)
    {
      int i = R.id.wizard_later;
      paramView = paramView.findViewById(i);
      b = paramView;
      paramView = b;
      if (paramView != null) {
        paramView.setOnClickListener(this);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.b.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */