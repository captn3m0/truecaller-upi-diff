package com.truecaller.wizard.b;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import java.lang.ref.WeakReference;

final class c$c
  extends Handler
{
  final WeakReference a;
  
  c$c(c paramc)
  {
    super((Looper)localObject);
    localObject = new java/lang/ref/WeakReference;
    ((WeakReference)localObject).<init>(paramc);
    a = ((WeakReference)localObject);
  }
  
  final void a(String paramString, Bundle paramBundle)
  {
    Message localMessage = Message.obtain();
    obj = paramString;
    if (paramBundle != null) {
      localMessage.setData(paramBundle);
    }
    sendMessage(localMessage);
  }
  
  public final void handleMessage(Message paramMessage)
  {
    String str = (String)obj;
    paramMessage = paramMessage.peekData();
    c localc = (c)a.get();
    boolean bool = TextUtils.isEmpty(str);
    if ((!bool) && (localc != null)) {
      c.a(localc, str, paramMessage);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.b.c.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */