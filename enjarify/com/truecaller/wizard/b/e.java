package com.truecaller.wizard.b;

import android.content.Context;
import c.g.b.k;
import com.truecaller.common.f.c;

public abstract class e
{
  public static final e.a a;
  
  static
  {
    e.a locala = new com/truecaller/wizard/b/e$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  public static final com.truecaller.analytics.b a(Context paramContext)
  {
    k.b(paramContext, "context");
    paramContext = ((com.truecaller.analytics.a)paramContext).z();
    k.a(paramContext, "(context as AnalyticEngineHolder).analyticsTracker");
    return paramContext;
  }
  
  public static final com.truecaller.common.f.b b(Context paramContext)
  {
    k.b(paramContext, "context");
    paramContext = ((com.truecaller.common.b.a)paramContext).d();
    k.a(paramContext, "(context as ApplicationBase).premiumDataPrefetcher");
    return paramContext;
  }
  
  public static final c c(Context paramContext)
  {
    k.b(paramContext, "context");
    paramContext = ((com.truecaller.common.b.a)paramContext).c();
    k.a(paramContext, "(context as ApplicationBase).premiumRepository");
    return paramContext;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.b.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */