package com.truecaller.wizard.b;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v4.app.o;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import com.truecaller.common.b.e;
import com.truecaller.utils.t;
import com.truecaller.utils.t.a;
import com.truecaller.wizard.R.anim;
import com.truecaller.wizard.R.id;
import com.truecaller.wizard.R.layout;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public abstract class c
  extends AppCompatActivity
{
  private static volatile boolean c;
  List a;
  public com.truecaller.wizard.a.d b;
  private final c.c d;
  private List e;
  private boolean f;
  private final Map g;
  
  public c()
  {
    Object localObject = new com/truecaller/wizard/b/c$c;
    ((c.c)localObject).<init>(this);
    d = ((c.c)localObject);
    localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    g = ((Map)localObject);
  }
  
  private o a(b paramb, Bundle paramBundle)
  {
    paramb = a;
    paramb = Fragment.instantiate(this, paramb, paramBundle);
    paramBundle = getSupportFragmentManager().a();
    int i = R.anim.wizard_fragment_enter;
    int j = R.anim.wizard_fragment_exit;
    paramBundle = paramBundle.a(i, j);
    i = R.id.wizardPage;
    return paramBundle.b(i, paramb);
  }
  
  public static void a(Context paramContext, Class paramClass)
  {
    a(paramContext, paramClass, null, true);
  }
  
  public static void a(Context paramContext, Class paramClass, Bundle paramBundle, boolean paramBoolean)
  {
    int i = 2;
    Object localObject = new Object[i];
    localObject[0] = "Wizard start. Class ";
    String str = paramClass.getSimpleName();
    int j = 1;
    localObject[j] = str;
    com.truecaller.debug.log.a.a((Object[])localObject);
    localObject = new android/content/Intent;
    ((Intent)localObject).<init>(paramContext, paramClass);
    if (paramBundle != null) {
      ((Intent)localObject).putExtras(paramBundle);
    }
    ((Intent)localObject).addFlags(65536);
    int k = 268435456;
    ((Intent)localObject).addFlags(k);
    if (paramBoolean)
    {
      k = 32768;
      ((Intent)localObject).addFlags(k);
    }
    paramContext.startActivity((Intent)localObject);
  }
  
  public static void a(Context paramContext, Class paramClass, String paramString)
  {
    String str1 = "wizard_FullyCompleted";
    boolean bool1 = e.a(str1, false);
    int i = 4;
    Object[] arrayOfObject = new Object[i];
    arrayOfObject[0] = "Wizard start. ResetAndStart ";
    String str2 = paramClass.getSimpleName();
    boolean bool2 = true;
    arrayOfObject[bool2] = str2;
    arrayOfObject[2] = ", isCompleted: ";
    str2 = String.valueOf(bool1);
    int j = 3;
    arrayOfObject[j] = str2;
    com.truecaller.debug.log.a.a(arrayOfObject);
    if (bool1) {
      a(false);
    }
    e.b("signUpOrigin", paramString);
    a(paramContext, paramClass, null, bool2);
  }
  
  public static void a(boolean paramBoolean)
  {
    e.b("wizard_RequiredStepsCompleted", paramBoolean);
    e.b("wizard_FullyCompleted", paramBoolean);
    e.b("wizard_StartPage");
  }
  
  public static void b(Context paramContext, Class paramClass)
  {
    com.truecaller.common.g.a locala = com.truecaller.common.b.a.F().u().c();
    boolean bool = true;
    locala.b("isUserChangingNumber", bool);
    a(paramContext, paramClass, null, bool);
  }
  
  public static void b(String paramString)
  {
    e.b("wizard_StartPage", paramString);
  }
  
  public static boolean e()
  {
    return e.a("wizard_RequiredStepsCompleted", false);
  }
  
  public static boolean f()
  {
    return e.a("wizard_FullyCompleted", false);
  }
  
  public static boolean g()
  {
    return e.a("wizard_OEMMode", false);
  }
  
  public static void h()
  {
    e.b("wizard_OEMMode", false);
  }
  
  public static boolean i()
  {
    return c;
  }
  
  public void a()
  {
    e.b("wizard_RequiredStepsCompleted", true);
  }
  
  public final void a(c.b paramb)
  {
    Object localObject = e;
    if (localObject == null)
    {
      localObject = new java/util/ArrayList;
      int i = 1;
      ((ArrayList)localObject).<init>(i);
      e = ((List)localObject);
    }
    e.add(paramb);
  }
  
  public final void a(String paramString)
  {
    a(paramString, null);
  }
  
  public final void a(String paramString, Bundle paramBundle)
  {
    b.a(paramString);
    d.a(paramString, paramBundle);
  }
  
  protected abstract void a(Map paramMap);
  
  public void b()
  {
    b.c();
    String str = "wizard_RequiredStepsCompleted";
    boolean bool = e.a(str, false);
    if (!bool) {
      a();
    }
    e.b("wizard_FullyCompleted", true);
    e.b("wizard_StartPage");
    e.b("signUpOrigin");
    com.truecaller.common.b.a.F().u().c().d("isUserChangingNumber");
    finish();
  }
  
  public final void b(c.b paramb)
  {
    List localList = e;
    if (localList != null) {
      localList.remove(paramb);
    }
  }
  
  public abstract com.truecaller.wizard.d.c c();
  
  public final boolean c(String paramString)
  {
    return g.containsKey(paramString);
  }
  
  protected abstract String d();
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    Object localObject = a;
    if (localObject != null)
    {
      localObject = ((List)localObject).iterator();
      boolean bool;
      do
      {
        bool = ((Iterator)localObject).hasNext();
        if (!bool) {
          break;
        }
        c.a locala = (c.a)((Iterator)localObject).next();
        bool = locala.a(paramInt1, paramInt2, paramIntent);
      } while (!bool);
      return;
    }
  }
  
  public void onBackPressed()
  {
    Object localObject = e;
    if (localObject != null)
    {
      int i = ((List)localObject).size() + -1;
      while (i >= 0)
      {
        localb = (c.b)e.get(i);
        bool2 = localb.i();
        if (bool2) {
          return;
        }
        i += -1;
      }
    }
    localObject = "wizard_OEMMode";
    boolean bool2 = false;
    c.b localb = null;
    boolean bool1 = e.a((String)localObject, false);
    if (bool1) {
      return;
    }
    super.onBackPressed();
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Object localObject1 = a.a();
    Object localObject2 = com.truecaller.utils.c.a().a(this).a();
    localObject1 = ((a.b)localObject1).a((t)localObject2);
    localObject2 = com.truecaller.common.b.a.F().u();
    ((a.b)localObject1).a((com.truecaller.common.a)localObject2).a().a(this);
    int i = R.layout.wizard_base;
    setContentView(i);
    i = 1;
    f = i;
    localObject1 = g;
    a((Map)localObject1);
    if (paramBundle == null)
    {
      paramBundle = e.a("wizard_StartPage");
      boolean bool = TextUtils.isEmpty(paramBundle);
      if (bool) {
        paramBundle = d();
      }
      b.a();
      bool = false;
      localObject1 = null;
      a(paramBundle, null);
    }
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    d.removeCallbacksAndMessages(null);
  }
  
  public void onPostResume()
  {
    super.onPostResume();
    f = true;
  }
  
  public void onSaveInstanceState(Bundle paramBundle)
  {
    f = false;
    super.onSaveInstanceState(paramBundle);
  }
  
  public void onStart()
  {
    super.onStart();
    c = true;
  }
  
  public void onStop()
  {
    super.onStop();
    c = false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.b.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */