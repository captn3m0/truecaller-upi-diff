package com.truecaller.wizard.c;

import android.content.Context;
import android.os.Handler;
import com.truecaller.utils.c;
import com.truecaller.utils.t.a;
import com.truecaller.wizard.b.a.b;
import com.truecaller.wizard.b.d;

public abstract class a
  extends android.support.v4.content.a
{
  private final Handler n;
  private final Runnable o;
  private final int p;
  private final long q;
  private int r;
  private com.truecaller.wizard.a.a s;
  
  public a(Context paramContext)
  {
    this(paramContext, (byte)0);
    Object localObject = com.truecaller.wizard.b.a.a();
    paramContext = c.a().a(paramContext).a();
    paramContext = ((a.b)localObject).a(paramContext);
    localObject = com.truecaller.common.b.a.F().u();
    paramContext = paramContext.a((com.truecaller.common.a)localObject).a().b();
    s = paramContext;
  }
  
  private a(Context paramContext, byte paramByte)
  {
    super(paramContext);
    paramContext = new android/os/Handler;
    paramContext.<init>();
    n = paramContext;
    paramContext = new com/truecaller/wizard/c/-$$Lambda$a$96VqH14n5_PlbZFlmR8atCgUsxo;
    paramContext.<init>(this);
    o = paramContext;
    p = 3;
    q = 500L;
  }
  
  /* Error */
  public final Object d()
  {
    // Byte code:
    //   0: iconst_1
    //   1: istore_1
    //   2: aload_0
    //   3: invokevirtual 129	com/truecaller/wizard/c/a:q	()Ljava/lang/Object;
    //   6: astore_2
    //   7: aload_0
    //   8: getfield 131	com/truecaller/wizard/c/a:r	I
    //   11: iload_1
    //   12: iadd
    //   13: istore_3
    //   14: aload_0
    //   15: iload_3
    //   16: putfield 131	com/truecaller/wizard/c/a:r	I
    //   19: aload_2
    //   20: areturn
    //   21: astore_2
    //   22: goto +210 -> 232
    //   25: astore_2
    //   26: aload_0
    //   27: getfield 67	com/truecaller/wizard/c/a:s	Lcom/truecaller/wizard/a/a;
    //   30: astore 4
    //   32: aload_0
    //   33: invokevirtual 133	com/truecaller/wizard/c/a:p	()Ljava/lang/String;
    //   36: astore 5
    //   38: aload_2
    //   39: invokevirtual 111	java/lang/Object:getClass	()Ljava/lang/Class;
    //   42: astore_2
    //   43: aload_2
    //   44: invokevirtual 117	java/lang/Class:getSimpleName	()Ljava/lang/String;
    //   47: astore_2
    //   48: aload 4
    //   50: aload 5
    //   52: aload_2
    //   53: invokeinterface 138 3 0
    //   58: aload_0
    //   59: getfield 131	com/truecaller/wizard/c/a:r	I
    //   62: iload_1
    //   63: iadd
    //   64: istore 6
    //   66: aload_0
    //   67: iload 6
    //   69: putfield 131	com/truecaller/wizard/c/a:r	I
    //   72: goto +61 -> 133
    //   75: astore_2
    //   76: aload_0
    //   77: getfield 67	com/truecaller/wizard/c/a:s	Lcom/truecaller/wizard/a/a;
    //   80: astore 4
    //   82: aload_0
    //   83: invokevirtual 133	com/truecaller/wizard/c/a:p	()Ljava/lang/String;
    //   86: astore 5
    //   88: aload_2
    //   89: invokevirtual 111	java/lang/Object:getClass	()Ljava/lang/Class;
    //   92: astore_2
    //   93: aload_2
    //   94: invokevirtual 117	java/lang/Class:getSimpleName	()Ljava/lang/String;
    //   97: astore_2
    //   98: aload 4
    //   100: aload 5
    //   102: aload_2
    //   103: invokeinterface 138 3 0
    //   108: ldc -116
    //   110: astore_2
    //   111: iconst_1
    //   112: anewarray 98	java/lang/String
    //   115: iconst_0
    //   116: aload_2
    //   117: aastore
    //   118: aload_0
    //   119: getfield 87	com/truecaller/wizard/c/a:p	I
    //   122: istore 6
    //   124: aload_0
    //   125: iload 6
    //   127: putfield 131	com/truecaller/wizard/c/a:r	I
    //   130: goto -72 -> 58
    //   133: aload_0
    //   134: getfield 131	com/truecaller/wizard/c/a:r	I
    //   137: istore 6
    //   139: aload_0
    //   140: getfield 87	com/truecaller/wizard/c/a:p	I
    //   143: istore_3
    //   144: iload 6
    //   146: iload_3
    //   147: if_icmpge +83 -> 230
    //   150: aload_0
    //   151: getfield 146	android/support/v4/content/c:j	Z
    //   154: istore 6
    //   156: iload 6
    //   158: ifne +72 -> 230
    //   161: aload_0
    //   162: getfield 149	android/support/v4/content/a:b	Landroid/support/v4/content/a$a;
    //   165: astore_2
    //   166: aload_2
    //   167: ifnull +6 -> 173
    //   170: goto +8 -> 178
    //   173: iconst_0
    //   174: istore_1
    //   175: aconst_null
    //   176: astore 7
    //   178: iload_1
    //   179: ifne +51 -> 230
    //   182: aload_0
    //   183: invokevirtual 153	com/truecaller/wizard/c/a:g	()Z
    //   186: pop
    //   187: aload_0
    //   188: getfield 77	com/truecaller/wizard/c/a:n	Landroid/os/Handler;
    //   191: astore 7
    //   193: aload_0
    //   194: getfield 84	com/truecaller/wizard/c/a:o	Ljava/lang/Runnable;
    //   197: astore_2
    //   198: aload 7
    //   200: aload_2
    //   201: invokevirtual 157	android/os/Handler:removeCallbacks	(Ljava/lang/Runnable;)V
    //   204: aload_0
    //   205: getfield 77	com/truecaller/wizard/c/a:n	Landroid/os/Handler;
    //   208: astore 7
    //   210: aload_0
    //   211: getfield 84	com/truecaller/wizard/c/a:o	Ljava/lang/Runnable;
    //   214: astore_2
    //   215: aload_0
    //   216: getfield 93	com/truecaller/wizard/c/a:q	J
    //   219: lstore 8
    //   221: aload 7
    //   223: aload_2
    //   224: lload 8
    //   226: invokevirtual 161	android/os/Handler:postDelayed	(Ljava/lang/Runnable;J)Z
    //   229: pop
    //   230: aconst_null
    //   231: areturn
    //   232: aload_0
    //   233: getfield 131	com/truecaller/wizard/c/a:r	I
    //   236: iload_1
    //   237: iadd
    //   238: istore_3
    //   239: aload_0
    //   240: iload_3
    //   241: putfield 131	com/truecaller/wizard/c/a:r	I
    //   244: aload_2
    //   245: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	246	0	this	a
    //   1	237	1	i	int
    //   6	14	2	localObject1	Object
    //   21	1	2	localObject2	Object
    //   25	14	2	localException	Exception
    //   42	11	2	localObject3	Object
    //   75	14	2	locala	a.a
    //   92	153	2	localObject4	Object
    //   13	228	3	j	int
    //   30	69	4	locala1	com.truecaller.wizard.a.a
    //   36	65	5	str	String
    //   64	84	6	k	int
    //   154	3	6	bool	boolean
    //   176	46	7	localHandler	Handler
    //   219	6	8	l	long
    // Exception table:
    //   from	to	target	type
    //   2	6	21	finally
    //   26	30	21	finally
    //   32	36	21	finally
    //   38	42	21	finally
    //   43	47	21	finally
    //   52	58	21	finally
    //   76	80	21	finally
    //   82	86	21	finally
    //   88	92	21	finally
    //   93	97	21	finally
    //   102	108	21	finally
    //   111	118	21	finally
    //   118	122	21	finally
    //   125	130	21	finally
    //   2	6	25	java/lang/Exception
    //   2	6	75	com/truecaller/wizard/c/a$a
  }
  
  public final void f()
  {
    h();
  }
  
  public final boolean g()
  {
    boolean bool = super.g();
    Handler localHandler = n;
    Runnable localRunnable = o;
    localHandler.removeCallbacks(localRunnable);
    return bool;
  }
  
  public final void j()
  {
    Handler localHandler = n;
    Runnable localRunnable = o;
    localHandler.removeCallbacks(localRunnable);
  }
  
  public final void l()
  {
    super.n();
    Handler localHandler = n;
    Runnable localRunnable = o;
    localHandler.removeCallbacks(localRunnable);
  }
  
  public final void n()
  {
    super.n();
    Handler localHandler = n;
    Runnable localRunnable = o;
    localHandler.removeCallbacks(localRunnable);
  }
  
  protected abstract String p();
  
  protected abstract Object q();
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */