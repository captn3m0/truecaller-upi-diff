package com.truecaller.wizard;

import android.animation.Animator.AnimatorListener;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.v4.content.b;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.truecaller.wizard.b.c;
import com.truecaller.wizard.b.i;

public class h
  extends i
{
  private ImageView a;
  private ValueAnimator b;
  
  public void onActivityCreated(Bundle paramBundle)
  {
    super.onActivityCreated(paramBundle);
    c.b("Page_Profile");
    paramBundle = new int[2];
    Bundle tmp15_14 = paramBundle;
    tmp15_14[0] = 0;
    tmp15_14[1] = '✐';
    paramBundle = ValueAnimator.ofInt(paramBundle);
    b = paramBundle;
    paramBundle = b;
    Object localObject = getResources();
    int i = R.integer.wizard_animation_duration_medium;
    long l = ((Resources)localObject).getInteger(i);
    paramBundle.setDuration(l);
    b.setStartDelay(1000L);
    paramBundle = b;
    localObject = new com/truecaller/wizard/h$1;
    ((h.1)localObject).<init>(this);
    paramBundle.addUpdateListener((ValueAnimator.AnimatorUpdateListener)localObject);
    paramBundle = b;
    localObject = new com/truecaller/wizard/h$2;
    ((h.2)localObject).<init>(this);
    paramBundle.addListener((Animator.AnimatorListener)localObject);
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    int i = R.layout.wizard_fragment_success;
    paramLayoutInflater = paramLayoutInflater.inflate(i, paramViewGroup, false);
    int j = R.id.image;
    paramViewGroup = (ImageView)paramLayoutInflater.findViewById(j);
    a = paramViewGroup;
    paramViewGroup = a;
    paramBundle = new android/graphics/drawable/LayerDrawable;
    Drawable[] arrayOfDrawable = new Drawable[2];
    Object localObject = getContext();
    int k = R.drawable.wizard_anim_circular_background;
    localObject = b.a((Context)localObject, k);
    arrayOfDrawable[0] = localObject;
    ClipDrawable localClipDrawable = new android/graphics/drawable/ClipDrawable;
    localObject = getContext();
    k = R.drawable.wizard_anim_check_center;
    localObject = b.a((Context)localObject, k);
    k = 1;
    localClipDrawable.<init>((Drawable)localObject, 8388611, k);
    arrayOfDrawable[k] = localClipDrawable;
    paramBundle.<init>(arrayOfDrawable);
    paramViewGroup.setImageDrawable(paramBundle);
    return paramLayoutInflater;
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    b.removeAllListeners();
  }
  
  public void onPause()
  {
    super.onPause();
    b.cancel();
  }
  
  public void onResume()
  {
    super.onResume();
    b.start();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */