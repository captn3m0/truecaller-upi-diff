package com.truecaller.wizard.internal.components;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputFilter.LengthFilter;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Property;
import android.util.TypedValue;
import android.view.ActionMode.Callback;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import com.truecaller.common.h.am;
import com.truecaller.wizard.R.id;
import com.truecaller.wizard.R.integer;
import com.truecaller.wizard.R.layout;

public class VerificationEditText
  extends FrameLayout
  implements TextWatcher
{
  private final ViewGroup a;
  private final View b;
  private final EditText c;
  private final int d;
  private Animator e;
  private VerificationEditText.a f;
  private final Animator.AnimatorListener g;
  
  public VerificationEditText(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private VerificationEditText(Context paramContext, AttributeSet paramAttributeSet, byte paramByte)
  {
    super(paramContext, paramAttributeSet, 0);
    paramAttributeSet = new com/truecaller/wizard/internal/components/VerificationEditText$1;
    paramAttributeSet.<init>(this);
    g = paramAttributeSet;
    paramAttributeSet = getResources();
    int i = R.integer.wizard_animation_duration_short;
    int j = paramAttributeSet.getInteger(i);
    d = j;
    paramContext = LayoutInflater.from(paramContext);
    j = R.layout.wizard_view_verification_edittext;
    paramAttributeSet = paramContext.inflate(j, this);
    i = R.id.digitsContainer;
    Object localObject1 = (ViewGroup)paramAttributeSet.findViewById(i);
    a = ((ViewGroup)localObject1);
    i = R.id.cursor;
    localObject1 = paramAttributeSet.findViewById(i);
    b = ((View)localObject1);
    i = R.id.editText;
    paramAttributeSet = (EditText)paramAttributeSet.findViewById(i);
    c = paramAttributeSet;
    c.setSaveEnabled(false);
    paramAttributeSet = c;
    i = 1;
    Object localObject2 = new InputFilter[i];
    Object localObject3 = new android/text/InputFilter$LengthFilter;
    int k = 6;
    ((InputFilter.LengthFilter)localObject3).<init>(k);
    localObject2[0] = localObject3;
    paramAttributeSet.setFilters((InputFilter[])localObject2);
    c.addTextChangedListener(this);
    paramAttributeSet = c;
    localObject2 = new com/truecaller/wizard/internal/components/VerificationEditText$2;
    ((VerificationEditText.2)localObject2).<init>(this);
    paramAttributeSet.setCustomSelectionActionModeCallback((ActionMode.Callback)localObject2);
    j = 0;
    paramAttributeSet = null;
    while (j < k)
    {
      int m = R.layout.wizard_view_verification_digit;
      localObject3 = a;
      localObject2 = paramContext.inflate(m, (ViewGroup)localObject3, false);
      int n = 2;
      if (j == n)
      {
        localObject3 = (LinearLayout.LayoutParams)((View)localObject2).getLayoutParams();
        DisplayMetrics localDisplayMetrics = getResources().getDisplayMetrics();
        float f1 = TypedValue.applyDimension(i, 32.0F, localDisplayMetrics);
        int i1 = (int)f1;
        int i2 = leftMargin;
        int i3 = topMargin;
        int i4 = bottomMargin;
        ((LinearLayout.LayoutParams)localObject3).setMargins(i2, i3, i1, i4);
      }
      localObject3 = a;
      ((ViewGroup)localObject3).addView((View)localObject2);
      j += 1;
    }
  }
  
  private float a(View paramView)
  {
    int i = paramView.getLeft();
    int j = paramView.getWidth();
    int k = b.getWidth();
    j = (j - k) / 2;
    return i + j;
  }
  
  public void afterTextChanged(Editable paramEditable)
  {
    int i = 0;
    for (;;)
    {
      int j = 6;
      if (i >= j) {
        break;
      }
      Object localObject1 = (ViewGroup)a.getChildAt(i);
      Object localObject2 = (TextView)((ViewGroup)localObject1).getChildAt(0);
      CharSequence localCharSequence = ((TextView)localObject2).getText();
      boolean bool1 = am.b(localCharSequence);
      int k;
      if (bool1)
      {
        bool1 = false;
        localCharSequence = null;
      }
      else
      {
        localCharSequence = ((TextView)localObject2).getText();
        k = localCharSequence.charAt(0);
      }
      int m = paramEditable.length();
      float f1;
      Object localObject3;
      if (i >= m)
      {
        m = 0;
        f1 = 0.0F;
        localObject3 = null;
      }
      else
      {
        m = paramEditable.charAt(i);
      }
      if (k != m)
      {
        boolean bool2 = true;
        long l;
        int i1;
        if (m != 0)
        {
          int n;
          if (k == 0)
          {
            l = d;
            ((ViewGroup)localObject1).setActivated(bool2);
            localObject1 = (TextView)((ViewGroup)localObject1).getChildAt(0);
            ViewPropertyAnimator localViewPropertyAnimator = ((TextView)localObject1).animate();
            localViewPropertyAnimator.cancel();
            ((TextView)localObject1).setAlpha(0.0F);
            localObject3 = String.valueOf(m);
            ((TextView)localObject1).setText((CharSequence)localObject3);
            localObject1 = ((TextView)localObject1).animate();
            n = 1065353216;
            f1 = 1.0F;
            localObject1 = ((ViewPropertyAnimator)localObject1).alpha(f1).setDuration(l);
            i1 = 0;
            localObject2 = null;
            localObject1 = ((ViewPropertyAnimator)localObject1).setListener(null);
            ((ViewPropertyAnimator)localObject1).start();
          }
          else
          {
            localObject1 = String.valueOf(n);
            ((TextView)localObject2).setText((CharSequence)localObject1);
          }
        }
        else
        {
          i1 = d;
          l = i1;
          ((ViewGroup)localObject1).setActivated(false);
          localObject1 = (TextView)((ViewGroup)localObject1).getChildAt(0);
          ((TextView)localObject1).animate().cancel();
          int i2 = 2;
          float[] arrayOfFloat = new float[i2];
          float f2 = ((TextView)localObject1).getAlpha();
          arrayOfFloat[0] = f2;
          arrayOfFloat[bool2] = 0.0F;
          localObject3 = ObjectAnimator.ofFloat(localObject1, "alpha", arrayOfFloat);
          ((ObjectAnimator)localObject3).setDuration(l);
          localObject2 = new com/truecaller/wizard/internal/components/VerificationEditText$3;
          ((VerificationEditText.3)localObject2).<init>(this, (TextView)localObject1);
          ((ObjectAnimator)localObject3).addListener((Animator.AnimatorListener)localObject2);
          ((ObjectAnimator)localObject3).start();
        }
      }
      i += 1;
    }
  }
  
  public void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
  
  protected void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    b.animate().cancel();
    ViewGroup localViewGroup = a;
    int i = localViewGroup.getChildCount();
    int j = 0;
    while (j < i)
    {
      Object localObject = (ViewGroup)a.getChildAt(j);
      ViewPropertyAnimator localViewPropertyAnimator = ((ViewGroup)localObject).animate();
      localViewPropertyAnimator.cancel();
      localObject = ((ViewGroup)localObject).getChildAt(0).animate();
      ((ViewPropertyAnimator)localObject).cancel();
      j += 1;
    }
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
    View localView = b;
    float f1 = localView.getTranslationX();
    paramInt1 = 0;
    float f2 = 0.0F;
    Object localObject = null;
    paramBoolean = f1 < 0.0F;
    if (!paramBoolean)
    {
      localView = b;
      localObject = a;
      paramInt2 = 0;
      localObject = ((ViewGroup)localObject).getChildAt(0);
      f2 = a((View)localObject);
      localView.setTranslationX(f2);
    }
  }
  
  public void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
  {
    paramInt2 += paramInt1;
    paramInt1 += paramInt3;
    if (paramInt2 != paramInt1)
    {
      int i = d;
      long l = i;
      paramCharSequence = e;
      if (paramCharSequence != null)
      {
        boolean bool = paramCharSequence.isRunning();
        if (bool)
        {
          e.removeAllListeners();
          paramCharSequence = e;
          paramCharSequence.cancel();
        }
      }
      paramCharSequence = new android/animation/AnimatorSet;
      paramCharSequence.<init>();
      Object localObject1 = a.getChildAt(5);
      int j = 6;
      int k = 2;
      int m = 1;
      int n;
      float f1;
      float f2;
      if (paramInt2 >= j)
      {
        localObject2 = new float[k];
        localObject3 = b;
        n = ((View)localObject3).getWidth();
        int i1 = ((View)localObject1).getRight();
        n += i1;
        f1 = n;
        localObject2[0] = f1;
        f2 = a((View)localObject1);
        localObject2[m] = f2;
      }
      else if (paramInt1 >= j)
      {
        localObject2 = new float[k];
        f1 = a((View)localObject1);
        localObject2[0] = f1;
        localObject3 = b;
        n = ((View)localObject3).getWidth();
        paramInt3 = ((View)localObject1).getRight();
        n += paramInt3;
        f2 = n;
        localObject2[m] = f2;
      }
      else
      {
        localObject2 = a.getChildAt(paramInt2);
        localObject1 = a.getChildAt(paramInt1);
        localObject3 = new float[k];
        float f3 = a((View)localObject2);
        localObject3[0] = f3;
        f3 = a((View)localObject1);
        localObject3[m] = f3;
        localObject2 = localObject3;
      }
      localObject1 = new Animator[k];
      Object localObject4 = b;
      Object localObject3 = View.TRANSLATION_X;
      Object localObject2 = ObjectAnimator.ofFloat(localObject4, (Property)localObject3, (float[])localObject2).setDuration(l);
      localObject1[0] = localObject2;
      localObject2 = b;
      localObject4 = "alpha";
      localObject3 = new float[m];
      float f4;
      if (paramInt1 >= j)
      {
        paramInt1 = 0;
        f4 = 0.0F;
        localObject5 = null;
      }
      else
      {
        paramInt1 = 1065353216;
        f4 = 1.0F;
      }
      localObject3[0] = f4;
      Object localObject5 = ObjectAnimator.ofFloat(localObject2, (String)localObject4, (float[])localObject3).setDuration(l);
      localObject1[m] = localObject5;
      paramCharSequence.playTogether((Animator[])localObject1);
      localObject5 = g;
      paramCharSequence.addListener((Animator.AnimatorListener)localObject5);
      paramCharSequence.start();
      e = paramCharSequence;
    }
  }
  
  public void setOnCodeEnteredListener(VerificationEditText.a parama)
  {
    f = parama;
  }
  
  public void setText(CharSequence paramCharSequence)
  {
    c.setText(paramCharSequence);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.internal.components.VerificationEditText
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */