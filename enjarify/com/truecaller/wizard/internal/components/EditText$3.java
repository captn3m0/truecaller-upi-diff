package com.truecaller.wizard.internal.components;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.drawable.Drawable;

final class EditText$3
  implements ValueAnimator.AnimatorUpdateListener
{
  EditText$3(EditText paramEditText, Drawable paramDrawable) {}
  
  public final void onAnimationUpdate(ValueAnimator paramValueAnimator)
  {
    Drawable localDrawable = a;
    int i = ((Integer)paramValueAnimator.getAnimatedValue()).intValue();
    localDrawable.setLevel(i);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.internal.components.EditText.3
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */