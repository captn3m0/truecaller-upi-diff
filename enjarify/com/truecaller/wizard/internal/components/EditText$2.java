package com.truecaller.wizard.internal.components;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.graphics.drawable.Drawable;
import android.support.v4.widget.m;

final class EditText$2
  extends AnimatorListenerAdapter
{
  EditText$2(EditText paramEditText, Drawable paramDrawable) {}
  
  public final void onAnimationEnd(Animator paramAnimator)
  {
    paramAnimator = b;
    Drawable localDrawable = a;
    m.a(paramAnimator, null, localDrawable);
    paramAnimator = b;
    localDrawable = a;
    EditText.a(paramAnimator, localDrawable).start();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.internal.components.EditText.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */