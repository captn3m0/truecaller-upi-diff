package com.truecaller.wizard.internal.components;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.support.v4.view.r;
import android.widget.EditText;

final class VerificationEditText$1
  extends AnimatorListenerAdapter
{
  VerificationEditText$1(VerificationEditText paramVerificationEditText) {}
  
  public final void onAnimationEnd(Animator paramAnimator)
  {
    paramAnimator = a;
    boolean bool = r.H(paramAnimator);
    if (bool)
    {
      paramAnimator = VerificationEditText.a(a);
      if (paramAnimator != null)
      {
        paramAnimator = VerificationEditText.b(a).getText();
        if (paramAnimator != null)
        {
          int i = paramAnimator.length();
          int j = 6;
          if (i == j)
          {
            VerificationEditText.a locala = VerificationEditText.a(a);
            locala.a(paramAnimator);
          }
        }
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.internal.components.VerificationEditText.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */