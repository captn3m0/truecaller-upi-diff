package com.truecaller.wizard.internal.components;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v4.content.b;
import android.support.v4.widget.m;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import com.truecaller.wizard.R.dimen;
import com.truecaller.wizard.R.drawable;
import com.truecaller.wizard.R.integer;

public class EditText
  extends AppCompatEditText
{
  private final Drawable a;
  private final Drawable b;
  private EditText.a c;
  private Drawable d;
  
  public EditText(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    paramContext = getContext();
    int i = R.drawable.wizard_ic_edittext_clear_clipped;
    paramContext = b.a(paramContext, i);
    a = paramContext;
    paramContext = getContext();
    i = R.drawable.wizard_ic_check_clipped;
    paramContext = b.a(paramContext, i);
    b = paramContext;
    paramContext = getResources();
    i = R.dimen.space;
    int j = paramContext.getDimensionPixelSize(i);
    setCompoundDrawablePadding(j);
  }
  
  private Animator a(Drawable paramDrawable, boolean paramBoolean)
  {
    Object localObject = getResources();
    int i = R.integer.wizard_animation_duration_short;
    int j = ((Resources)localObject).getInteger(i);
    long l = j;
    int k = 2;
    int[] arrayOfInt = new int[k];
    int m = 10000;
    if (paramBoolean) {
      n = 0;
    } else {
      n = 10000;
    }
    arrayOfInt[0] = n;
    int n = 1;
    if (!paramBoolean) {
      m = 0;
    }
    arrayOfInt[n] = m;
    ValueAnimator localValueAnimator = ValueAnimator.ofInt(arrayOfInt);
    localValueAnimator.setDuration(l);
    localObject = new com/truecaller/wizard/internal/components/EditText$3;
    ((EditText.3)localObject).<init>(this, paramDrawable);
    localValueAnimator.addUpdateListener((ValueAnimator.AnimatorUpdateListener)localObject);
    return localValueAnimator;
  }
  
  private void b()
  {
    Object localObject = getCurrentIcon();
    EditText.1 local1 = null;
    d = null;
    if (localObject != null)
    {
      localObject = a((Drawable)localObject, false);
      local1 = new com/truecaller/wizard/internal/components/EditText$1;
      local1.<init>(this);
      ((Animator)localObject).addListener(local1);
      ((Animator)localObject).start();
    }
  }
  
  private boolean c()
  {
    Configuration localConfiguration = getResources().getConfiguration();
    int i = localConfiguration.getLayoutDirection();
    int j = 1;
    if (i == j) {
      return j;
    }
    return false;
  }
  
  private Drawable getCurrentIcon()
  {
    return d;
  }
  
  private void setIcon(Drawable paramDrawable)
  {
    Object localObject = getCurrentIcon();
    d = paramDrawable;
    if (localObject != null)
    {
      localObject = a((Drawable)localObject, false);
      EditText.2 local2 = new com/truecaller/wizard/internal/components/EditText$2;
      local2.<init>(this, paramDrawable);
      ((Animator)localObject).addListener(local2);
      ((Animator)localObject).start();
      return;
    }
    m.a(this, null, paramDrawable);
    a(paramDrawable, true).start();
  }
  
  public final boolean a()
  {
    EditText.a locala = c;
    if (locala != null)
    {
      Editable localEditable = getText();
      boolean bool = locala.a(localEditable);
      if (bool) {
        return true;
      }
    }
    return false;
  }
  
  protected void onFocusChanged(boolean paramBoolean, int paramInt, Rect paramRect)
  {
    super.onFocusChanged(paramBoolean, paramInt, paramRect);
    Object localObject;
    if (paramBoolean)
    {
      localObject = getText();
      paramBoolean = ((Editable)localObject).length();
      if (paramBoolean)
      {
        localObject = a;
        setIcon((Drawable)localObject);
      }
    }
    else
    {
      localObject = getText();
      paramBoolean = ((Editable)localObject).length();
      if (paramBoolean)
      {
        paramBoolean = a();
        if (paramBoolean)
        {
          localObject = b;
          setIcon((Drawable)localObject);
          return;
        }
      }
      b();
    }
  }
  
  protected void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
  {
    int i = paramCharSequence.length();
    if (i == 0)
    {
      b();
      return;
    }
    boolean bool = a();
    Drawable localDrawable;
    if (bool)
    {
      paramCharSequence = getCurrentIcon();
      localDrawable = b;
      if (paramCharSequence != localDrawable) {
        setIcon(localDrawable);
      }
    }
    else
    {
      paramCharSequence = getCurrentIcon();
      localDrawable = a;
      if (paramCharSequence != localDrawable) {
        setIcon(localDrawable);
      }
    }
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    Object localObject = getCurrentIcon();
    Drawable localDrawable1 = a;
    if (localObject == localDrawable1)
    {
      int i = paramMotionEvent.getAction();
      int k = 1;
      float f1 = Float.MIN_VALUE;
      if (i == k)
      {
        boolean bool1 = c();
        float f2;
        if (bool1)
        {
          bool1 = false;
          f2 = 0.0F;
          localObject = null;
        }
        else
        {
          int j = getWidth();
          k = getPaddingRight();
          j -= k;
          localDrawable1 = a;
          k = localDrawable1.getIntrinsicWidth();
          j -= k;
          k = getCompoundDrawablePadding();
          j -= k;
          f2 = j;
        }
        boolean bool3 = c();
        int m;
        if (bool3)
        {
          m = getPaddingLeft();
          Drawable localDrawable2 = a;
          int n = localDrawable2.getIntrinsicWidth();
          m += n;
          n = getCompoundDrawablePadding();
          m += n;
        }
        else
        {
          m = getWidth();
        }
        f1 = m;
        float f3 = paramMotionEvent.getX();
        boolean bool2 = f3 < f2;
        if (!bool2)
        {
          bool2 = f3 < f1;
          if (!bool2)
          {
            localObject = "";
            setText((CharSequence)localObject);
          }
        }
      }
    }
    return super.onTouchEvent(paramMotionEvent);
  }
  
  public void setInputValidator(EditText.a parama)
  {
    c = parama;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.internal.components.EditText
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */