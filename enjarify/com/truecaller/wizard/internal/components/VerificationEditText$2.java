package com.truecaller.wizard.internal.components;

import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.Menu;
import android.view.MenuItem;

final class VerificationEditText$2
  implements ActionMode.Callback
{
  VerificationEditText$2(VerificationEditText paramVerificationEditText) {}
  
  public final boolean onActionItemClicked(ActionMode paramActionMode, MenuItem paramMenuItem)
  {
    return false;
  }
  
  public final boolean onCreateActionMode(ActionMode paramActionMode, Menu paramMenu)
  {
    return false;
  }
  
  public final void onDestroyActionMode(ActionMode paramActionMode) {}
  
  public final boolean onPrepareActionMode(ActionMode paramActionMode, Menu paramMenu)
  {
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.internal.components.VerificationEditText.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */