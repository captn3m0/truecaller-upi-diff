package com.truecaller.wizard;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.widget.ImageView;

final class c$1
  implements ValueAnimator.AnimatorUpdateListener
{
  c$1(c paramc, float paramFloat1, float paramFloat2) {}
  
  public final void onAnimationUpdate(ValueAnimator paramValueAnimator)
  {
    float f1 = ((Float)paramValueAnimator.getAnimatedValue()).floatValue();
    ImageView localImageView = c.b;
    float f2 = a * f1;
    localImageView.setTranslationX(f2);
    localImageView = c.b;
    f2 = b * f1;
    localImageView.setTranslationY(f2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.c.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */