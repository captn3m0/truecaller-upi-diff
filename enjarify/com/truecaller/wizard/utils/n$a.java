package com.truecaller.wizard.utils;

import android.util.Patterns;
import com.truecaller.wizard.internal.components.EditText.a;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

final class n$a
  implements EditText.a
{
  public final boolean a(CharSequence paramCharSequence)
  {
    int i = paramCharSequence.length();
    if (i != 0)
    {
      Pattern localPattern = Patterns.EMAIL_ADDRESS;
      paramCharSequence = localPattern.matcher(paramCharSequence);
      boolean bool = paramCharSequence.matches();
      if (!bool) {
        return false;
      }
    }
    return true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.utils.n.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */