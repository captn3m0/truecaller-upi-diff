package com.truecaller.wizard.utils;

import android.content.Context;
import android.support.v4.content.a;

public abstract class c
  extends a
{
  private Object n;
  
  protected c(Context paramContext)
  {
    super(paramContext);
  }
  
  public final void a(Object paramObject)
  {
    super.a(paramObject);
    n = null;
  }
  
  public final void b(Object paramObject)
  {
    boolean bool = k;
    if (bool)
    {
      n = null;
      return;
    }
    n = paramObject;
    bool = i;
    if (bool) {
      super.b(paramObject);
    }
  }
  
  public final void f()
  {
    Object localObject = n;
    if (localObject != null) {
      b(localObject);
    }
    boolean bool1 = l;
    l = false;
    boolean bool2 = m | bool1;
    m = bool2;
    if (!bool1)
    {
      localObject = n;
      if (localObject != null) {}
    }
    else
    {
      h();
    }
  }
  
  public final void j()
  {
    g();
  }
  
  public final void n()
  {
    super.n();
    g();
    n = null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.utils.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */