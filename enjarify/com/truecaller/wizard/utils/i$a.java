package com.truecaller.wizard.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import java.util.Iterator;
import java.util.List;

public abstract class i$a
  extends BroadcastReceiver
{
  protected abstract void a(Context paramContext, String paramString);
  
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    String str = "granted";
    paramIntent = paramIntent.getCharSequenceArrayListExtra(str);
    if (paramIntent != null)
    {
      paramIntent = paramIntent.iterator();
      for (;;)
      {
        boolean bool = paramIntent.hasNext();
        if (!bool) {
          break;
        }
        str = String.valueOf((CharSequence)paramIntent.next());
        a(paramContext, str);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.utils.i.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */