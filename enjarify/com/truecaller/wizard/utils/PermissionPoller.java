package com.truecaller.wizard.utils;

import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.PowerManager;
import android.provider.Settings.System;
import com.truecaller.utils.c;
import com.truecaller.utils.l;
import com.truecaller.utils.t;
import com.truecaller.utils.t.a;
import java.util.concurrent.TimeUnit;

public final class PermissionPoller
  implements Runnable
{
  private static final long a = TimeUnit.MINUTES.toMillis(2);
  private final Context b;
  private final Handler c;
  private final Intent d;
  private int e;
  private PermissionPoller.Permission f;
  private l g;
  
  public PermissionPoller(Context paramContext, Handler paramHandler, Intent paramIntent)
  {
    b = paramContext;
    c = paramHandler;
    d = paramIntent;
    paramContext = c.a().a(paramContext).a().b();
    g = paramContext;
    d.addFlags(603979776);
  }
  
  public final void a()
  {
    c.removeCallbacks(this);
  }
  
  public final void a(PermissionPoller.Permission paramPermission)
  {
    c.removeCallbacks(this);
    e = 0;
    f = paramPermission;
    c.postDelayed(this, 500L);
  }
  
  public final void run()
  {
    long l1 = e;
    long l2 = 500L;
    int i = (int)(l1 + l2);
    e = i;
    int j = e;
    l1 = j;
    long l3 = a;
    boolean bool3 = l1 < l3;
    if (bool3)
    {
      a();
      return;
    }
    Object localObject1 = PermissionPoller.1.a;
    Object localObject2 = f;
    i = ((PermissionPoller.Permission)localObject2).ordinal();
    j = localObject1[i];
    i = 1;
    int k = 23;
    boolean bool1;
    switch (j)
    {
    default: 
      a();
      return;
    case 4: 
      j = Build.VERSION.SDK_INT;
      if (j >= k)
      {
        localObject1 = (PowerManager)b.getSystemService("power");
        localObject2 = b.getPackageName();
        bool1 = ((PowerManager)localObject1).isIgnoringBatteryOptimizations((String)localObject2);
      }
      break;
    case 3: 
      j = Build.VERSION.SDK_INT;
      if (j >= k)
      {
        localObject1 = b;
        boolean bool2 = Settings.System.canWrite((Context)localObject1);
        if (!bool2)
        {
          bool1 = false;
          localObject2 = null;
        }
      }
      break;
    case 2: 
      localObject1 = g;
      bool1 = ((l)localObject1).d();
      break;
    case 1: 
      localObject1 = g;
      bool1 = ((l)localObject1).a();
    }
    if (bool1)
    {
      a();
      localObject1 = b;
      localObject2 = d;
      ((Context)localObject1).startActivity((Intent)localObject2);
      return;
    }
    c.postDelayed(this, l2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.utils.PermissionPoller
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */