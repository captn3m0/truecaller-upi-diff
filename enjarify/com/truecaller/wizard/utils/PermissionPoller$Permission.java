package com.truecaller.wizard.utils;

public enum PermissionPoller$Permission
{
  static
  {
    Object localObject = new com/truecaller/wizard/utils/PermissionPoller$Permission;
    ((Permission)localObject).<init>("DRAW_OVERLAY", 0);
    DRAW_OVERLAY = (Permission)localObject;
    localObject = new com/truecaller/wizard/utils/PermissionPoller$Permission;
    int i = 1;
    ((Permission)localObject).<init>("NOTIFICATION_ACCESS", i);
    NOTIFICATION_ACCESS = (Permission)localObject;
    localObject = new com/truecaller/wizard/utils/PermissionPoller$Permission;
    int j = 2;
    ((Permission)localObject).<init>("SYSTEM_SETTINGS", j);
    SYSTEM_SETTINGS = (Permission)localObject;
    localObject = new com/truecaller/wizard/utils/PermissionPoller$Permission;
    int k = 3;
    ((Permission)localObject).<init>("BATTERY_OPTIMISATIONS", k);
    BATTERY_OPTIMISATIONS = (Permission)localObject;
    localObject = new Permission[4];
    Permission localPermission = DRAW_OVERLAY;
    localObject[0] = localPermission;
    localPermission = NOTIFICATION_ACCESS;
    localObject[i] = localPermission;
    localPermission = SYSTEM_SETTINGS;
    localObject[j] = localPermission;
    localPermission = BATTERY_OPTIMISATIONS;
    localObject[k] = localPermission;
    $VALUES = (Permission[])localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.utils.PermissionPoller.Permission
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */