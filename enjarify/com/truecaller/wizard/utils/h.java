package com.truecaller.wizard.utils;

import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnPreDrawListener;

public abstract class h
  implements ViewTreeObserver.OnPreDrawListener
{
  private final View a;
  
  public h(View paramView)
  {
    a = paramView;
    a.getViewTreeObserver().addOnPreDrawListener(this);
  }
  
  protected abstract void a(View paramView);
  
  public final boolean onPreDraw()
  {
    View localView = a;
    a(localView);
    a.getViewTreeObserver().removeOnPreDrawListener(this);
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.utils.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */