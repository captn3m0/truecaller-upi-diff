package com.truecaller.wizard.utils;

import android.view.View;
import android.view.View.OnAttachStateChangeListener;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import c.g.b.k;

public final class g
  implements View.OnAttachStateChangeListener, ViewTreeObserver.OnGlobalLayoutListener
{
  private boolean a;
  private final View b;
  private final f c;
  
  public g(View paramView, f paramf)
  {
    b = paramView;
    c = paramf;
    paramView = b.getViewTreeObserver();
    paramf = this;
    paramf = (ViewTreeObserver.OnGlobalLayoutListener)this;
    paramView.addOnGlobalLayoutListener(paramf);
    paramView = b;
    paramf = this;
    paramf = (View.OnAttachStateChangeListener)this;
    paramView.addOnAttachStateChangeListener(paramf);
  }
  
  public final void onGlobalLayout()
  {
    Object localObject = b.getRootView();
    k.a(localObject, "view.rootView");
    float f1 = ((View)localObject).getHeight();
    View localView = b;
    int i = localView.getHeight();
    float f2 = i;
    f2 = (f1 - f2) / f1;
    f1 = 0.2F;
    boolean bool = f2 < f1;
    if (bool)
    {
      bool = a;
      if (!bool)
      {
        localObject = c;
        ((f)localObject).i();
      }
      a = true;
      return;
    }
    bool = a;
    if (bool)
    {
      c.j();
      bool = false;
      f1 = 0.0F;
      localObject = null;
      a = false;
    }
  }
  
  public final void onViewAttachedToWindow(View paramView)
  {
    k.b(paramView, "v");
  }
  
  public final void onViewDetachedFromWindow(View paramView)
  {
    k.b(paramView, "v");
    paramView = b.getViewTreeObserver();
    Object localObject = this;
    localObject = (ViewTreeObserver.OnGlobalLayoutListener)this;
    paramView.removeOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener)localObject);
    paramView = b;
    localObject = this;
    localObject = (View.OnAttachStateChangeListener)this;
    paramView.removeOnAttachStateChangeListener((View.OnAttachStateChangeListener)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.utils.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */