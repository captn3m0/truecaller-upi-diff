package com.truecaller.wizard.utils;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import c.g.b.k;
import com.truecaller.wizard.R.layout;
import com.truecaller.wizard.R.string;
import java.util.HashMap;

public final class e
  extends AppCompatDialogFragment
{
  public d a;
  private HashMap b;
  
  public final Dialog onCreateDialog(Bundle paramBundle)
  {
    paramBundle = new android/support/v7/app/AlertDialog$Builder;
    Object localObject1 = getActivity();
    if (localObject1 == null) {
      k.a();
    }
    localObject1 = (Context)localObject1;
    paramBundle.<init>((Context)localObject1);
    localObject1 = LayoutInflater.from(getContext());
    int i = R.layout.dialog_explain_permission;
    localObject1 = ((LayoutInflater)localObject1).inflate(i, null);
    localObject1 = paramBundle.setView((View)localObject1).setCancelable(false);
    i = R.string.Welcome_permission_request_explain_continue;
    Object localObject2 = new com/truecaller/wizard/utils/e$a;
    ((e.a)localObject2).<init>(this);
    localObject2 = (DialogInterface.OnClickListener)localObject2;
    ((AlertDialog.Builder)localObject1).setPositiveButton(i, (DialogInterface.OnClickListener)localObject2);
    paramBundle = paramBundle.create();
    k.a(paramBundle, "builder.create()");
    return (Dialog)paramBundle;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.utils.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */