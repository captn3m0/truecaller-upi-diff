package com.truecaller.wizard.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

final class n$c
  extends n.d
{
  private static final Pattern a = Pattern.compile("[\\x{1F300}-\\x{1F5FF}\\x{1F900}-\\x{1F9FF}\\x{1F600}-\\x{1F64F}\\x{1F680}-\\x{1F6FF}\\x{2600}-\\x{26FF}\\x{2700}-\\x{27BF}\\x{1F100}-\\x{1F1FF}\\x{1F000}-\\x{1F02F}\\x{2B00}-\\x{2BFF}\\x{2300}-\\x{23FF}\\x{1F0A0}-\\x{1F0FF}]");
  
  public final boolean a(CharSequence paramCharSequence)
  {
    boolean bool1 = super.a(paramCharSequence);
    if (bool1)
    {
      Pattern localPattern = a;
      paramCharSequence = localPattern.matcher(paramCharSequence);
      boolean bool2 = paramCharSequence.find();
      if (!bool2) {
        return true;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.utils.n.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */