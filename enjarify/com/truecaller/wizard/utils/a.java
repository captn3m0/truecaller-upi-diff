package com.truecaller.wizard.utils;

import android.content.Context;
import c.g.a.m;
import c.g.b.k;
import com.truecaller.analytics.d;
import com.truecaller.analytics.e;
import com.truecaller.analytics.e.a;
import com.truecaller.common.network.account.CheckCredentialsDeviceDto;
import com.truecaller.common.network.account.CheckCredentialsRequestDto;
import com.truecaller.common.network.account.CheckCredentialsResponseDto;
import com.truecaller.common.network.c;
import com.truecaller.log.AssertionUtil;
import com.truecaller.wizard.d.g;
import java.util.concurrent.TimeUnit;
import kotlinx.coroutines.f;

public class a
  extends com.truecaller.wizard.c.a
{
  public a(Context paramContext)
  {
    super(paramContext);
  }
  
  private static Boolean r()
  {
    Object localObject1 = com.truecaller.common.b.a.F();
    k.a(localObject1, "ApplicationBase.getAppBase()");
    Object localObject2 = ((com.truecaller.common.b.a)localObject1).u().c();
    Object localObject3 = ((com.truecaller.common.b.a)localObject1).v().c();
    Object localObject4 = ((com.truecaller.common.g.a)localObject2).a("accountRestorationSource");
    TimeUnit localTimeUnit = null;
    if (localObject4 != null)
    {
      int i = ((String)localObject4).hashCode();
      int j = -1040995011;
      if (i != j)
      {
        j = 406778383;
        if (i != j)
        {
          j = 1810319240;
          if (i == j)
          {
            localObject5 = "restored_from_file";
            bool3 = ((String)localObject4).equals(localObject5);
            if (bool3)
            {
              localObject4 = "restored_from_file";
              break label170;
            }
          }
        }
        else
        {
          localObject5 = "restored_from_account_manager";
          bool3 = ((String)localObject4).equals(localObject5);
          if (bool3)
          {
            localObject4 = "restored_from_account_manager";
            break label170;
          }
        }
      }
      else
      {
        localObject5 = "restored_from_autobackup";
        bool3 = ((String)localObject4).equals(localObject5);
        if (bool3)
        {
          localObject4 = "restored_from_autobackup";
          break label170;
        }
      }
    }
    AssertionUtil.reportWeirdnessButNeverCrash("Account was restored, but restoration source is unknown!");
    boolean bool3 = false;
    localObject4 = null;
    label170:
    if (localObject4 == null) {
      return Boolean.FALSE;
    }
    Object localObject5 = "restored_from_autobackup";
    boolean bool1 = k.a(localObject4, localObject5);
    Object localObject6;
    if (bool1)
    {
      localObject5 = ((com.truecaller.common.b.a)localObject1).u().j().b();
      localObject6 = localObject5;
    }
    else
    {
      localObject6 = null;
    }
    localObject5 = com.truecaller.common.network.account.a.a;
    CheckCredentialsRequestDto localCheckCredentialsRequestDto = new com/truecaller/common/network/account/CheckCredentialsRequestDto;
    String str = null;
    int k = 2;
    localObject5 = localCheckCredentialsRequestDto;
    Object localObject7 = localObject4;
    localCheckCredentialsRequestDto.<init>((String)localObject4, null, (CheckCredentialsDeviceDto)localObject6, k, null);
    localObject5 = com.truecaller.common.network.account.a.a(localCheckCredentialsRequestDto).c();
    localObject7 = "response";
    k.a(localObject5, (String)localObject7);
    boolean bool2 = ((e.r)localObject5).d();
    if (bool2)
    {
      localObject7 = new com/truecaller/analytics/e$a;
      ((e.a)localObject7).<init>("AccountRestored");
      str = "RestorationSource";
      localObject4 = ((e.a)localObject7).a(str, (String)localObject4).a();
      localObject7 = "AnalyticsEvent.Builder(A…estorationSource).build()";
      k.a(localObject4, (String)localObject7);
      ((com.truecaller.analytics.b)localObject3).b((e)localObject4);
      ((com.truecaller.common.g.a)localObject2).d("accountRestorationSource");
      localObject3 = new com/truecaller/wizard/utils/a$a;
      ((a.a)localObject3).<init>((com.truecaller.common.b.a)localObject1, null);
      localObject3 = (Boolean)f.a((m)localObject3);
      boolean bool4 = ((Boolean)localObject3).booleanValue();
      if (bool4)
      {
        localObject3 = new com/truecaller/wizard/d/g;
        localObject4 = localObject1;
        localObject4 = (Context)localObject1;
        ((g)localObject3).<init>((com.truecaller.common.g.a)localObject2, (Context)localObject4);
        ((g)localObject3).a();
        localObject2 = "com.truecaller.wizard.verification.action.PHONE_VERIFIED";
        ((g)localObject3).a((String)localObject2);
      }
      localObject2 = (CheckCredentialsResponseDto)((e.r)localObject5).e();
      if (localObject2 != null)
      {
        localObject3 = ((CheckCredentialsResponseDto)localObject2).getInstallationId();
        if (localObject3 != null)
        {
          localObject4 = ((com.truecaller.common.b.a)localObject1).u().k();
          localTimeUnit = TimeUnit.SECONDS;
          localObject5 = ((CheckCredentialsResponseDto)localObject2).getTtl();
          long l1;
          if (localObject5 != null) {
            l1 = ((Long)localObject5).longValue();
          } else {
            l1 = 0L;
          }
          long l2 = localTimeUnit.toMillis(l1);
          ((com.truecaller.common.account.r)localObject4).a((String)localObject3, l2);
        }
        localObject1 = ((com.truecaller.common.b.a)localObject1).u().m();
        localObject2 = ((CheckCredentialsResponseDto)localObject2).getDomain();
        ((c)localObject1).a((String)localObject2);
      }
      return Boolean.TRUE;
    }
    int n = ((e.r)localObject5).b();
    int m = 401;
    if (n == m)
    {
      localObject2 = ((com.truecaller.common.b.a)localObject1).u().k().e();
      m = 1;
      localObject4 = "AutoLogin";
      try
      {
        ((com.truecaller.common.b.a)localObject1).a((String)localObject2, m, (String)localObject4);
      }
      catch (SecurityException localSecurityException)
      {
        localObject1 = (Throwable)localSecurityException;
        AssertionUtil.reportThrowableButNeverCrash((Throwable)localObject1);
      }
    }
    return Boolean.FALSE;
  }
  
  public final String p()
  {
    return "AutoLogin";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.utils.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */