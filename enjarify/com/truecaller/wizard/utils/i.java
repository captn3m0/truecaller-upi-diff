package com.truecaller.wizard.utils;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.PermissionGroupInfo;
import android.content.pm.PermissionInfo;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v7.app.AlertDialog.Builder;
import android.text.TextUtils;
import com.truecaller.common.b.e;
import com.truecaller.common.h.k;
import com.truecaller.log.AssertionUtil;
import com.truecaller.wizard.AccessContactsActivity;
import com.truecaller.wizard.R.attr;
import com.truecaller.wizard.R.string;
import java.util.ArrayList;

public final class i
{
  public static void a(Activity paramActivity)
  {
    try
    {
      Intent localIntent = new android/content/Intent;
      Object localObject = "android.settings.action.MANAGE_OVERLAY_PERMISSION";
      localIntent.<init>((String)localObject);
      localObject = new java/lang/StringBuilder;
      String str = "package:";
      ((StringBuilder)localObject).<init>(str);
      str = paramActivity.getPackageName();
      ((StringBuilder)localObject).append(str);
      localObject = ((StringBuilder)localObject).toString();
      localObject = Uri.parse((String)localObject);
      localIntent = localIntent.setData((Uri)localObject);
      paramActivity.startActivity(localIntent);
      return;
    }
    catch (ActivityNotFoundException localActivityNotFoundException)
    {
      com.truecaller.log.d.a(localActivityNotFoundException;
    }
  }
  
  public static void a(Context paramContext)
  {
    try
    {
      Intent localIntent = c(paramContext);
      paramContext.startActivity(localIntent);
      return;
    }
    catch (ActivityNotFoundException localActivityNotFoundException)
    {
      com.truecaller.log.d.a(localActivityNotFoundException, "App settings page couldn't be opened");
    }
  }
  
  public static void a(Context paramContext, String paramString, int paramInt)
  {
    paramString = b(paramContext, paramString);
    if (paramString != null)
    {
      int i = 1;
      localObject1 = new int[i];
      j = R.attr.colorAccent;
      localObject1[0] = j;
      localObject1 = paramContext.obtainStyledAttributes((int[])localObject1);
      localObject2 = ((TypedArray)localObject1).getColorStateList(0);
      android.support.v4.graphics.drawable.a.a(paramString, (ColorStateList)localObject2);
      ((TypedArray)localObject1).recycle();
    }
    Object localObject1 = new android/support/v7/app/AlertDialog$Builder;
    ((AlertDialog.Builder)localObject1).<init>(paramContext);
    paramString = ((AlertDialog.Builder)localObject1).setIcon(paramString);
    int j = R.string.PermissionDenied;
    paramString = paramString.setTitle(j).setMessage(paramInt);
    paramInt = R.string.Draw_GoToSettings;
    Object localObject2 = new com/truecaller/wizard/utils/-$$Lambda$i$K3IFaQKVDigCZ4SGIG8vi5_BELE;
    ((-..Lambda.i.K3IFaQKVDigCZ4SGIG8vi5_BELE)localObject2).<init>(paramContext);
    paramString.setPositiveButton(paramInt, (DialogInterface.OnClickListener)localObject2);
    ((AlertDialog.Builder)localObject1).show();
  }
  
  public static void a(String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    int i = paramArrayOfInt.length;
    localArrayList.<init>(i);
    i = 0;
    int j = 0;
    for (;;)
    {
      int k = paramArrayOfString.length;
      if (j >= k) {
        break;
      }
      String str = paramArrayOfString[j];
      boolean bool1 = e.a(str, false);
      if (!bool1)
      {
        int m = paramArrayOfInt[j];
        if (m == 0)
        {
          str = paramArrayOfString[j];
          localArrayList.add(str);
        }
      }
      str = paramArrayOfString[j];
      int n = paramArrayOfInt[j];
      if (n != 0) {
        n = 1;
      } else {
        n = 0;
      }
      e.b(str, n);
      j += 1;
    }
    boolean bool2 = localArrayList.isEmpty();
    if (!bool2)
    {
      paramArrayOfString = new android/content/Intent;
      paramArrayOfString.<init>("com.truecaller.ACTION_PERMISSIONS_CHANGED");
      paramArrayOfString.putCharSequenceArrayListExtra("granted", localArrayList);
      paramArrayOfInt = android.support.v4.content.d.a(com.truecaller.common.b.a.F());
      paramArrayOfInt.a(paramArrayOfString);
    }
  }
  
  public static boolean a(Activity paramActivity, String paramString)
  {
    boolean bool1 = k.f();
    if (!bool1) {
      return false;
    }
    bool1 = e.a(paramString, false);
    if (bool1)
    {
      boolean bool2 = android.support.v4.app.a.a(paramActivity, paramString);
      if (!bool2) {
        return true;
      }
    }
    return false;
  }
  
  public static boolean a(Activity paramActivity, String paramString, int paramInt)
  {
    boolean bool = paramActivity instanceof AccessContactsActivity;
    if (!bool)
    {
      bool = a(paramActivity, paramString);
      if (bool) {
        return false;
      }
    }
    bool = a(paramActivity, paramString);
    if (bool)
    {
      a(paramActivity);
      return false;
    }
    bool = true;
    String[] arrayOfString = new String[bool];
    arrayOfString[0] = paramString;
    android.support.v4.app.a.a(paramActivity, arrayOfString, paramInt);
    return bool;
  }
  
  public static boolean a(Context paramContext, String paramString)
  {
    boolean bool1 = com.truecaller.wizard.a.a(paramContext);
    if (!bool1) {
      return false;
    }
    String str = "android.permission.READ_CONTACTS";
    boolean bool2 = paramString.equals(str);
    if (bool2)
    {
      paramString = new android/content/Intent;
      paramString.<init>(paramContext, AccessContactsActivity.class);
      paramContext.startActivity(paramString);
      return true;
    }
    return false;
  }
  
  public static boolean a(Fragment paramFragment, String paramString, int paramInt)
  {
    return a(paramFragment, paramString, paramInt, true);
  }
  
  public static boolean a(Fragment paramFragment, String paramString, int paramInt, boolean paramBoolean)
  {
    if (paramBoolean)
    {
      localf = paramFragment.getActivity();
      paramBoolean = a(localf, paramString);
      if (paramBoolean) {
        return false;
      }
    }
    f localf = paramFragment.getActivity();
    paramBoolean = a(localf, paramString);
    if (paramBoolean)
    {
      a(paramFragment.getContext());
      return false;
    }
    paramBoolean = true;
    String[] arrayOfString = new String[paramBoolean];
    arrayOfString[0] = paramString;
    paramFragment.requestPermissions(arrayOfString, paramInt);
    return paramBoolean;
  }
  
  public static boolean a(Fragment paramFragment, String[] paramArrayOfString, int paramInt)
  {
    return b(paramFragment, paramArrayOfString, paramInt);
  }
  
  private static Drawable b(Context paramContext, String paramString)
  {
    try
    {
      paramContext = paramContext.getPackageManager();
      Object localObject = null;
      paramString = paramContext.getPermissionInfo(paramString, 0);
      String str = group;
      boolean bool = TextUtils.isEmpty(str);
      if (!bool)
      {
        str = group;
        localObject = paramContext.getPermissionGroupInfo(str, 0);
        localObject = ((PermissionGroupInfo)localObject).loadIcon(paramContext);
        if (localObject != null) {
          return (Drawable)localObject;
        }
      }
      paramContext = paramString.loadIcon(paramContext);
      if (paramContext != null) {
        return paramContext;
      }
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException) {}
    return null;
  }
  
  public static void b(Activity paramActivity)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>();
    localIntent.setAction("android.settings.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS");
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("package:");
    String str = paramActivity.getPackageName();
    ((StringBuilder)localObject).append(str);
    localObject = Uri.parse(((StringBuilder)localObject).toString());
    localIntent.setData((Uri)localObject);
    try
    {
      paramActivity.startActivity(localIntent);
      return;
    }
    catch (ActivityNotFoundException localActivityNotFoundException)
    {
      com.truecaller.log.d.a(localActivityNotFoundException;
    }
  }
  
  public static void b(Context paramContext)
  {
    try
    {
      Intent localIntent = new android/content/Intent;
      Object localObject = "android.settings.action.MANAGE_OVERLAY_PERMISSION";
      localIntent.<init>((String)localObject);
      localObject = new java/lang/StringBuilder;
      String str = "package:";
      ((StringBuilder)localObject).<init>(str);
      str = paramContext.getPackageName();
      ((StringBuilder)localObject).append(str);
      localObject = ((StringBuilder)localObject).toString();
      localObject = Uri.parse((String)localObject);
      localIntent = localIntent.setData((Uri)localObject);
      int i = 268435456;
      localIntent.addFlags(i);
      paramContext.startActivity(localIntent);
      return;
    }
    catch (ActivityNotFoundException localActivityNotFoundException)
    {
      com.truecaller.log.d.a(localActivityNotFoundException;
    }
  }
  
  public static boolean b(Fragment paramFragment, String[] paramArrayOfString, int paramInt)
  {
    int i = paramArrayOfString.length;
    int j = 0;
    String str;
    f localf;
    boolean bool;
    while (j < i)
    {
      str = paramArrayOfString[j];
      localf = paramFragment.getActivity();
      bool = a(localf, str);
      if (bool) {
        return false;
      }
      j += 1;
    }
    i = paramArrayOfString.length;
    j = 0;
    while (j < i)
    {
      str = paramArrayOfString[j];
      localf = paramFragment.getActivity();
      bool = a(localf, str);
      if (bool)
      {
        a(paramFragment.getActivity());
        return false;
      }
      j += 1;
    }
    paramFragment.requestPermissions(paramArrayOfString, paramInt);
    return true;
  }
  
  private static Intent c(Context paramContext)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("android.settings.APPLICATION_DETAILS_SETTINGS");
    paramContext = paramContext.getPackageName();
    paramContext = Uri.fromParts("package", paramContext, null);
    return localIntent.setData(paramContext);
  }
  
  public static boolean c(Activity paramActivity)
  {
    Intent localIntent = new android/content/Intent;
    String str1 = "miui.intent.action.APP_PERM_EDITOR";
    localIntent.<init>(str1);
    boolean bool = true;
    String str2 = "com.miui.securitycenter";
    String str3 = "com.miui.permcenter.permissions.PermissionsEditorActivity";
    try
    {
      localIntent.setClassName(str2, str3);
      str2 = "extra_pkgname";
      str3 = paramActivity.getPackageName();
      localIntent.putExtra(str2, str3);
      paramActivity.startActivity(localIntent);
      return bool;
    }
    catch (RuntimeException localRuntimeException1)
    {
      str2 = "com.miui.securitycenter";
      str3 = "com.miui.permcenter.permissions.AppPermissionsEditorActivity";
      try
      {
        localIntent.setClassName(str2, str3);
        paramActivity.startActivity(localIntent);
        return bool;
      }
      catch (RuntimeException localRuntimeException2)
      {
        try
        {
          localIntent = c(paramActivity);
          paramActivity.startActivity(localIntent);
          return bool;
        }
        catch (RuntimeException localRuntimeException3)
        {
          AssertionUtil.reportThrowableButNeverCrash(localRuntimeException3;
        }
      }
    }
    return false;
  }
  
  public static boolean d(Activity paramActivity)
  {
    try
    {
      Intent localIntent = new android/content/Intent;
      localIntent.<init>();
      String str1 = "com.coloros.safecenter";
      String str2 = "com.coloros.safecenter.permission.PermissionManagerActivity";
      localIntent.setClassName(str1, str2);
      paramActivity.startActivity(localIntent);
      return true;
    }
    catch (RuntimeException localRuntimeException) {}
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.utils.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */