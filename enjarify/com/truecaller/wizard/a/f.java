package com.truecaller.wizard.a;

import dagger.a.d;
import javax.inject.Provider;

public final class f
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  
  private f(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
  }
  
  public static f a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    f localf = new com/truecaller/wizard/a/f;
    localf.<init>(paramProvider1, paramProvider2, paramProvider3);
    return localf;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.a.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */