package com.truecaller.wizard.a;

import c.d.f;
import c.g.b.k;
import c.n;
import c.t;
import com.truecaller.analytics.b;
import com.truecaller.utils.a;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import kotlinx.coroutines.bg;

public final class e
  implements d
{
  final Queue a;
  String b;
  final f c;
  private final LinkedHashSet d;
  private long e;
  private boolean f;
  private final List g;
  private final Map h;
  private final List i;
  private final b j;
  private final a k;
  
  public e(b paramb, a parama, f paramf)
  {
    j = paramb;
    k = parama;
    c = paramf;
    paramb = new java/util/LinkedList;
    paramb.<init>();
    paramb = (Queue)paramb;
    a = paramb;
    paramb = new java/util/LinkedHashSet;
    paramb.<init>();
    d = paramb;
    boolean bool = true;
    f = bool;
    String[] tmp82_79 = new String[10];
    String[] tmp83_82 = tmp82_79;
    String[] tmp83_82 = tmp82_79;
    tmp83_82[0] = "WizardStarted";
    tmp83_82[1] = "EnterNumber";
    String[] tmp92_83 = tmp83_82;
    String[] tmp92_83 = tmp83_82;
    tmp92_83[2] = "Privacy";
    tmp92_83[3] = "Verification";
    String[] tmp101_92 = tmp92_83;
    String[] tmp101_92 = tmp92_83;
    tmp101_92[4] = "Profile";
    tmp101_92[5] = "AdsChoices";
    String[] tmp110_101 = tmp101_92;
    String[] tmp110_101 = tmp101_92;
    tmp110_101[6] = "EnhancedSearch";
    tmp110_101[7] = "DrawPermission";
    tmp110_101[8] = "DrawPermissionDetails";
    String[] tmp126_110 = tmp110_101;
    tmp126_110[9] = "WizardDone";
    parama = c.a.m.b(tmp126_110);
    g = parama;
    parama = new n[10];
    paramf = t.a("Page_Welcome", "WizardStarted");
    parama[0] = paramf;
    paramf = t.a("Page_EnterNumber", "EnterNumber");
    parama[bool] = paramf;
    paramb = t.a("Page_Privacy", "Privacy");
    parama[2] = paramb;
    paramb = t.a("Page_Verification", "Verification");
    parama[3] = paramb;
    paramb = t.a("Page_Success", "Verification");
    parama[4] = paramb;
    paramb = t.a("Page_Profile", "Profile");
    parama[5] = paramb;
    paramb = t.a("Page_AdsChoices", "AdsChoices");
    parama[6] = paramb;
    paramb = t.a("Page_AccessContacts", "EnhancedSearch");
    parama[7] = paramb;
    paramb = t.a("Page_DrawPermission", "DrawPermission");
    parama[8] = paramb;
    paramb = t.a("Page_DrawPermissionDetails", "DrawPermissionDetails");
    parama[9] = paramb;
    paramb = c.a.ag.a(parama);
    h = paramb;
    String[] tmp286_283 = new String[5];
    String[] tmp287_286 = tmp286_283;
    String[] tmp287_286 = tmp286_283;
    tmp287_286[0] = "AdsChoices";
    tmp287_286[1] = "EnhancedSearch";
    String[] tmp296_287 = tmp287_286;
    String[] tmp296_287 = tmp287_286;
    tmp296_287[2] = "DrawPermission";
    tmp296_287[3] = "DrawPermissionDetails";
    tmp296_287[4] = "WizardDone";
    paramb = c.a.m.b(tmp296_287);
    i = paramb;
  }
  
  private final void c(String paramString)
  {
    int m;
    int n;
    Object localObject2;
    Object localObject3;
    for (;;)
    {
      localObject1 = g;
      m = ((List)localObject1).indexOf(paramString);
      n = -1;
      if (m == n) {
        return;
      }
      localObject2 = (String)c.a.m.d((Iterable)d);
      if (localObject2 != null)
      {
        localObject3 = g;
        n = ((List)localObject3).indexOf(localObject2);
      }
      else
      {
        n = 0;
        localObject2 = null;
      }
      if (m >= n) {
        break;
      }
      a();
    }
    paramString = g;
    m += 1;
    paramString = (Iterable)paramString.subList(n, m);
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    localObject1 = (Collection)localObject1;
    paramString = paramString.iterator();
    boolean bool2;
    for (;;)
    {
      bool2 = paramString.hasNext();
      if (!bool2) {
        break;
      }
      localObject2 = paramString.next();
      localObject3 = localObject2;
      localObject3 = (String)localObject2;
      LinkedHashSet localLinkedHashSet = d;
      boolean bool3 = localLinkedHashSet.contains(localObject3);
      if (!bool3) {
        ((Collection)localObject1).add(localObject2);
      }
    }
    localObject1 = (Iterable)localObject1;
    paramString = ((Iterable)localObject1).iterator();
    for (;;)
    {
      boolean bool1 = paramString.hasNext();
      if (!bool1) {
        break;
      }
      localObject1 = (String)paramString.next();
      localObject2 = a;
      bool2 = ((Queue)localObject2).isEmpty();
      if (bool2)
      {
        localObject2 = k;
        long l1 = ((a)localObject2).b();
        long l2 = e;
        l1 -= l2;
        l2 = 1000L;
        boolean bool4 = l1 < l2;
        if (bool4)
        {
          b((String)localObject1);
          break label302;
        }
      }
      d((String)localObject1);
      label302:
      localObject2 = d;
      ((LinkedHashSet)localObject2).add(localObject1);
    }
  }
  
  private final void d(String paramString)
  {
    Object localObject1 = a;
    boolean bool = ((Queue)localObject1).isEmpty();
    if (bool)
    {
      a.add(paramString);
      paramString = (kotlinx.coroutines.ag)bg.a;
      localObject1 = c;
      Object localObject2 = new com/truecaller/wizard/a/e$a;
      ((e.a)localObject2).<init>(this, null);
      localObject2 = (c.g.a.m)localObject2;
      kotlinx.coroutines.e.b(paramString, (f)localObject1, (c.g.a.m)localObject2, 2);
      return;
    }
    a.add(paramString);
  }
  
  public final void a()
  {
    d.clear();
    a.clear();
    c("WizardStarted");
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "page");
    Map localMap = h;
    paramString = (String)localMap.get(paramString);
    b = paramString;
    paramString = b;
    if (paramString != null)
    {
      c(paramString);
      return;
    }
  }
  
  public final void b()
  {
    f = false;
  }
  
  final void b(String paramString)
  {
    Object localObject1 = new com/truecaller/analytics/e$a;
    ((com.truecaller.analytics.e.a)localObject1).<init>("WizardAction");
    localObject1 = ((com.truecaller.analytics.e.a)localObject1).a("Action", paramString);
    Object localObject2 = i;
    boolean bool1 = ((List)localObject2).contains(paramString);
    if (bool1)
    {
      paramString = "ProfilePreference";
      boolean bool2 = f;
      if (bool2) {
        localObject2 = "NameOnTop";
      } else {
        localObject2 = "SocialOnTop";
      }
      ((com.truecaller.analytics.e.a)localObject1).a(paramString, (String)localObject2);
    }
    paramString = j;
    localObject1 = ((com.truecaller.analytics.e.a)localObject1).a();
    k.a(localObject1, "eventBuilder.build()");
    paramString.b((com.truecaller.analytics.e)localObject1);
    long l = k.b();
    e = l;
  }
  
  public final void c()
  {
    c("WizardDone");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.a.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */