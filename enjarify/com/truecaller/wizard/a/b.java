package com.truecaller.wizard.a;

import c.g.b.k;
import com.truecaller.analytics.e.a;

public final class b
  implements a
{
  private String a;
  private String b;
  private final com.truecaller.analytics.b c;
  private final e d;
  
  public b(com.truecaller.analytics.b paramb, e parame)
  {
    c = paramb;
    d = parame;
  }
  
  public final void a(String paramString1, String paramString2)
  {
    k.b(paramString1, "requestName");
    k.b(paramString2, "cause");
    Object localObject = b;
    boolean bool = k.a(localObject, paramString2);
    if (bool)
    {
      localObject = a;
      bool = k.a(localObject, paramString1);
      if (bool) {
        return;
      }
    }
    b = paramString2;
    a = paramString1;
    localObject = new com/truecaller/analytics/e$a;
    ((e.a)localObject).<init>("WizardError");
    String str1 = "WizardStep";
    String str2 = d.b;
    if (str2 == null) {
      str2 = "Unknown step";
    }
    paramString1 = ((e.a)localObject).a(str1, str2).a("FailedRequest", paramString1).a("Cause", paramString2).a();
    paramString2 = c;
    k.a(paramString1, "event");
    paramString2.a(paramString1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */