package com.truecaller.wizard;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import com.truecaller.common.h.h;
import com.truecaller.common.network.country.CountryListDto.a;
import com.truecaller.common.ui.a.b.a;
import com.truecaller.utils.extensions.t;
import com.truecaller.wizard.b.c;
import com.truecaller.wizard.internal.components.EditText;
import com.truecaller.wizard.internal.components.EditText.a;
import com.truecaller.wizard.utils.f;
import com.truecaller.wizard.utils.g;
import com.truecaller.wizard.utils.n;
import com.truecaller.wizard.utils.n.b;

public class e
  extends b
  implements TextWatcher, TextView.OnEditorActionListener, f
{
  private ImageView e;
  private View i;
  private View j;
  private EditText k;
  private EditText l;
  private View m;
  private com.truecaller.common.ui.a.b n;
  
  private void n()
  {
    Object localObject = d();
    if (localObject == null)
    {
      i1 = R.string.EnterCountry;
      a(i1);
      return;
    }
    localObject = l.getText();
    int i1 = ((Editable)localObject).length();
    if (i1 == 0)
    {
      i1 = R.string.EnterNumber;
      a(i1);
      return;
    }
    localObject = l;
    boolean bool = ((EditText)localObject).a();
    if (!bool)
    {
      int i2 = R.string.EnterNumberError_InvalidNumber;
      a(i2);
      return;
    }
    t.a(l, false, 2);
    h();
  }
  
  protected final void a(CountryListDto.a parama)
  {
    super.a(parama);
    EditText localEditText = k;
    Object[] arrayOfObject = new Object[2];
    String str = b;
    arrayOfObject[0] = str;
    parama = d;
    arrayOfObject[1] = parama;
    parama = String.format("%s (+%s)", arrayOfObject);
    localEditText.setText(parama);
  }
  
  protected final void a(String paramString)
  {
    super.a(paramString);
    EditText localEditText = l;
    if (localEditText != null) {
      localEditText.setText(paramString);
    }
  }
  
  public void afterTextChanged(Editable paramEditable)
  {
    paramEditable = h.c(paramEditable.toString());
    if (paramEditable != null) {
      a(paramEditable);
    }
  }
  
  public void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
  
  protected final String f()
  {
    EditText localEditText = l;
    if (localEditText == null) {
      return super.f();
    }
    return localEditText.getText().toString();
  }
  
  public final void i()
  {
    j.setVisibility(8);
    LinearLayout.LayoutParams localLayoutParams = (LinearLayout.LayoutParams)i.getLayoutParams();
    height = -2;
    weight = 0.0F;
  }
  
  public final void j()
  {
    LinearLayout.LayoutParams localLayoutParams = (LinearLayout.LayoutParams)i.getLayoutParams();
    height = 0;
    weight = 1.0F;
    j.setVisibility(0);
  }
  
  public void onClick(View paramView)
  {
    super.onClick(paramView);
    int i1 = paramView.getId();
    int i2 = R.id.nextButton;
    if (i1 == i2)
    {
      n();
      return;
    }
    i2 = R.id.countrySpinner;
    if (i1 == i2)
    {
      paramView = new com/truecaller/common/ui/a/b$a;
      Object localObject = getActivity();
      paramView.<init>((Context)localObject);
      i2 = R.string.EnterCountry;
      b = i2;
      i2 = R.string.SearchCountryTip;
      d = i2;
      i2 = R.drawable.wizard_ic_country_search;
      c = i2;
      localObject = new com/truecaller/wizard/-$$Lambda$e$Yo7p9GjHKnmoL-Q_oZXYB7wcLyM;
      ((-..Lambda.e.Yo7p9GjHKnmoL-Q_oZXYB7wcLyM)localObject).<init>(this);
      e = ((AdapterView.OnItemClickListener)localObject);
      localObject = new com/truecaller/common/ui/a/b;
      Context localContext = a;
      int i3 = b;
      int i4 = d;
      int i5 = c;
      AdapterView.OnItemClickListener localOnItemClickListener = e;
      ((com.truecaller.common.ui.a.b)localObject).<init>(localContext, i3, i4, i5, localOnItemClickListener, (byte)0);
      n = ((com.truecaller.common.ui.a.b)localObject);
      paramView = n;
      paramView.show();
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    boolean bool1 = c.g();
    int i1;
    if (bool1) {
      i1 = R.layout.wizard_fragment_enter_number_preload;
    } else {
      i1 = R.layout.wizard_fragment_enter_number;
    }
    int i2 = 0;
    Object localObject1 = null;
    paramLayoutInflater = paramLayoutInflater.inflate(i1, paramViewGroup, false);
    int i3 = R.id.image;
    paramViewGroup = (ImageView)paramLayoutInflater.findViewById(i3);
    e = paramViewGroup;
    i3 = R.id.bottomSection;
    paramViewGroup = paramLayoutInflater.findViewById(i3);
    i = paramViewGroup;
    i3 = R.id.spacer;
    paramViewGroup = paramLayoutInflater.findViewById(i3);
    j = paramViewGroup;
    i3 = R.id.countrySpinner;
    paramViewGroup = (EditText)paramLayoutInflater.findViewById(i3);
    k = paramViewGroup;
    i3 = R.id.nextButton;
    paramViewGroup = paramLayoutInflater.findViewById(i3);
    m = paramViewGroup;
    i3 = R.id.numberField;
    paramViewGroup = (EditText)paramLayoutInflater.findViewById(i3);
    l = paramViewGroup;
    boolean bool2 = c.g();
    if (!bool2)
    {
      paramViewGroup = e;
      paramBundle = new android/graphics/drawable/LayerDrawable;
      Object localObject2 = new Drawable[3];
      Object localObject3 = getContext();
      int i5 = R.drawable.wizard_anim_circular_background;
      localObject3 = android.support.v4.content.b.a((Context)localObject3, i5);
      localObject2[0] = localObject3;
      localObject1 = getContext();
      int i6 = R.drawable.wizard_anim_phone_handle_center;
      localObject1 = android.support.v4.content.b.a((Context)localObject1, i6);
      i6 = 1;
      localObject2[i6] = localObject1;
      Object localObject4 = getContext();
      int i7 = R.drawable.wizard_anim_phone_body_center;
      localObject4 = android.support.v4.content.b.a((Context)localObject4, i7);
      localObject2[2] = localObject4;
      paramBundle.<init>((Drawable[])localObject2);
      i2 = 1;
      i5 = 0;
      localObject4 = null;
      i7 = 0;
      DisplayMetrics localDisplayMetrics = getResources().getDisplayMetrics();
      float f = TypedValue.applyDimension(i6, 50.0F, localDisplayMetrics);
      int i8 = (int)f;
      localObject2 = paramBundle;
      i6 = i2;
      paramBundle.setLayerInset(i2, 0, 0, 0, i8);
      paramViewGroup.setImageDrawable(paramBundle);
    }
    else
    {
      int i4 = R.id.terms;
      paramViewGroup = (TextView)paramLayoutInflater.findViewById(i4);
      a(paramViewGroup);
    }
    return paramLayoutInflater;
  }
  
  public void onDestroyView()
  {
    super.onDestroyView();
    com.truecaller.common.ui.a.b localb = n;
    if (localb != null) {
      localb.dismiss();
    }
  }
  
  public boolean onEditorAction(TextView paramTextView, int paramInt, KeyEvent paramKeyEvent)
  {
    int i1 = 6;
    if ((paramInt == i1) || (paramInt == 0)) {
      n();
    }
    return true;
  }
  
  public void onStart()
  {
    super.onStart();
    c();
  }
  
  public void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
  
  public void onViewCreated(View paramView, Bundle paramBundle)
  {
    Object localObject1 = k;
    Object localObject2 = n.a;
    ((EditText)localObject1).setInputValidator((EditText.a)localObject2);
    localObject1 = l;
    localObject2 = new com/truecaller/wizard/utils/n$b;
    ((n.b)localObject2).<init>(3);
    ((EditText)localObject1).setInputValidator((EditText.a)localObject2);
    l.addTextChangedListener(this);
    super.onViewCreated(paramView, paramBundle);
    m.setOnClickListener(this);
    k.setOnClickListener(this);
    l.setOnEditorActionListener(this);
    paramBundle = new com/truecaller/wizard/e$1;
    localObject1 = e;
    paramBundle.<init>(this, (View)localObject1);
    new g(paramView, this);
    m();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */