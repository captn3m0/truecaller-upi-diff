package com.truecaller.wizard;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.widget.ImageView;

final class h$1
  implements ValueAnimator.AnimatorUpdateListener
{
  h$1(h paramh) {}
  
  public final void onAnimationUpdate(ValueAnimator paramValueAnimator)
  {
    ImageView localImageView = h.a(a);
    int i = ((Integer)paramValueAnimator.getAnimatedValue()).intValue();
    localImageView.setImageLevel(i);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.h.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */