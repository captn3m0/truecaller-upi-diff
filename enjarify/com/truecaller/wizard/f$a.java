package com.truecaller.wizard;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import com.google.firebase.perf.network.FirebasePerfOkHttpClient;
import com.truecaller.common.h.am;
import com.truecaller.log.AssertionUtil;
import d.d;
import d.n;
import d.t;
import d.u;
import java.io.IOException;
import java.io.OutputStream;
import okhttp3.aa;
import okhttp3.ab;
import okhttp3.ab.a;
import okhttp3.ad;
import okhttp3.ae;
import okhttp3.e;
import okhttp3.w;
import okhttp3.y;

final class f$a
  extends com.truecaller.wizard.utils.c
{
  private final String n;
  private final Uri o;
  
  f$a(Context paramContext, Bundle paramBundle)
  {
    super(paramContext);
    paramContext = "url";
    boolean bool = paramBundle.containsKey(paramContext);
    String[] arrayOfString = null;
    if (bool)
    {
      paramContext = "destination";
      bool = paramBundle.containsKey(paramContext);
      if (bool)
      {
        bool = true;
        break label43;
      }
    }
    bool = false;
    paramContext = null;
    label43:
    arrayOfString = new String[0];
    AssertionUtil.isTrue(bool, arrayOfString);
    paramContext = paramBundle.getString("url");
    n = paramContext;
    paramContext = (Uri)paramBundle.getParcelable("destination");
    o = paramContext;
  }
  
  private Uri p()
  {
    Uri localUri = null;
    try
    {
      Object localObject1 = new okhttp3/y;
      ((y)localObject1).<init>();
      Object localObject3 = new okhttp3/ab$a;
      ((ab.a)localObject3).<init>();
      Object localObject4 = n;
      localObject3 = ((ab.a)localObject3).a((String)localObject4);
      localObject3 = ((ab.a)localObject3).a();
      localObject4 = null;
      localObject1 = aa.a((y)localObject1, (ab)localObject3, false);
      localObject1 = FirebasePerfOkHttpClient.execute((e)localObject1);
      boolean bool1 = ((ad)localObject1).c();
      if (bool1)
      {
        localObject1 = g;
        if (localObject1 != null)
        {
          localObject3 = ((ae)localObject1).a();
          if (localObject3 != null)
          {
            str1 = "image";
            String str2 = a;
            boolean bool2 = am.b(str1, str2);
            if (bool2)
            {
              localObject3 = h;
              localObject3 = ((Context)localObject3).getContentResolver();
              localObject4 = o;
              localObject3 = ((ContentResolver)localObject3).openOutputStream((Uri)localObject4);
              if (localObject3 == null) {
                break label213;
              }
              try
              {
                localObject4 = n.a((OutputStream)localObject3);
                localObject4 = n.a((t)localObject4);
                localObject1 = ((ae)localObject1).c();
                ((d)localObject4).a((u)localObject1);
                ((d)localObject4).close();
                localUri = o;
              }
              finally
              {
                ((OutputStream)localObject3).close();
              }
            }
          }
          int i = 1;
          String[] arrayOfString = new String[i];
          String str1 = "Invalid Content-Type, ";
          localObject3 = String.valueOf(localObject3);
          localObject3 = str1.concat((String)localObject3);
          arrayOfString[0] = localObject3;
        }
      }
    }
    catch (IOException|IllegalArgumentException localIOException)
    {
      label213:
      for (;;) {}
    }
    return localUri;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.f.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */