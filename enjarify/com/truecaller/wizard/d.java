package com.truecaller.wizard;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.b;
import android.text.method.MovementMethod;
import android.text.method.ScrollingMovementMethod;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.truecaller.utils.l;
import com.truecaller.wizard.b.c;
import com.truecaller.wizard.utils.PermissionPoller;
import com.truecaller.wizard.utils.PermissionPoller.Permission;

public class d
  extends com.truecaller.wizard.b.i
  implements View.OnClickListener
{
  protected TextView a;
  protected ImageView b;
  private Animator c;
  private final Handler d;
  private PermissionPoller e;
  
  public d()
  {
    Handler localHandler = new android/os/Handler;
    Looper localLooper = Looper.getMainLooper();
    localHandler.<init>(localLooper);
    d = localHandler;
  }
  
  private void c()
  {
    ((c)getActivity()).b();
  }
  
  protected Animator a()
  {
    Object localObject1 = getResources().getDisplayMetrics();
    int i = 1;
    float f = TypedValue.applyDimension(i, 9.0F, (DisplayMetrics)localObject1);
    Object localObject2 = getContext();
    int j = R.color.wizard_gray_light;
    int k = b.c((Context)localObject2, j);
    Object localObject3 = getContext();
    int m = R.color.wizard_blue;
    j = b.c((Context)localObject3, m);
    Object localObject4 = getResources();
    int n = R.integer.wizard_animation_duration_medium;
    long l1 = ((Resources)localObject4).getInteger(n);
    Resources localResources = getResources();
    int i1 = R.integer.wizard_cyclic_animation_pause;
    long l2 = localResources.getInteger(i1);
    d.1 local1 = new com/truecaller/wizard/d$1;
    local1.<init>(this, k, j, f);
    int i2 = 2;
    localObject2 = new float[i2];
    Object tmp127_125 = localObject2;
    tmp127_125[0] = 0.0F;
    tmp127_125[1] = 1.0F;
    localObject2 = ValueAnimator.ofFloat((float[])localObject2);
    ((ValueAnimator)localObject2).setDuration(l1);
    ((ValueAnimator)localObject2).setStartDelay(l2);
    ((ValueAnimator)localObject2).addUpdateListener(tmp127_125);
    localObject3 = new float[i2];
    Object tmp172_170 = localObject3;
    tmp172_170[0] = 1.0F;
    tmp172_170[1] = 0.0F;
    localObject3 = ValueAnimator.ofFloat((float[])localObject3);
    ((ValueAnimator)localObject3).setDuration(l1);
    ((ValueAnimator)localObject3).setStartDelay(l2);
    ((ValueAnimator)localObject3).addUpdateListener(tmp127_125);
    localObject4 = new android/animation/AnimatorSet;
    ((AnimatorSet)localObject4).<init>();
    localObject1 = new Animator[tmp172_170];
    localObject1[0] = localObject2;
    localObject1[i] = localObject3;
    ((AnimatorSet)localObject4).playSequentially((Animator[])localObject1);
    localObject1 = new com/truecaller/wizard/d$2;
    ((d.2)localObject1).<init>(this);
    ((AnimatorSet)localObject4).addListener((Animator.AnimatorListener)localObject1);
    return (Animator)localObject4;
  }
  
  public void onClick(View paramView)
  {
    super.onClick(paramView);
    int i = paramView.getId();
    int j = R.id.nextButton;
    Object localObject;
    Handler localHandler;
    if (i == j)
    {
      paramView = getActivity();
      if (paramView != null) {
        com.truecaller.wizard.utils.i.a(paramView);
      }
      paramView = e;
      if (paramView == null)
      {
        paramView = new com/truecaller/wizard/utils/PermissionPoller;
        localObject = requireContext();
        localHandler = d;
        Intent localIntent = new android/content/Intent;
        Context localContext = getContext();
        Class localClass = getActivity().getClass();
        localIntent.<init>(localContext, localClass);
        paramView.<init>((Context)localObject, localHandler, localIntent);
        e = paramView;
      }
      paramView = e;
      localObject = PermissionPoller.Permission.DRAW_OVERLAY;
      paramView.a((PermissionPoller.Permission)localObject);
      return;
    }
    i = paramView.getId();
    j = R.id.later;
    if (i == j)
    {
      c();
      return;
    }
    int k = paramView.getId();
    i = R.id.details;
    if (k == i)
    {
      paramView = (c)getActivity();
      localObject = "Page_DrawPermissionDetails";
      j = 0;
      localHandler = null;
      paramView.a((String)localObject, null);
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    int i = R.layout.wizard_fragment_draw;
    return paramLayoutInflater.inflate(i, paramViewGroup, false);
  }
  
  public void onDestroy()
  {
    PermissionPoller localPermissionPoller = e;
    if (localPermissionPoller != null) {
      localPermissionPoller.a();
    }
    super.onDestroy();
    c.removeAllListeners();
  }
  
  public void onPause()
  {
    super.onPause();
    c.end();
  }
  
  public void onResume()
  {
    super.onResume();
    Object localObject = e;
    if (localObject != null) {
      ((PermissionPoller)localObject).a();
    }
    localObject = f;
    boolean bool = ((l)localObject).a();
    if (bool)
    {
      c();
      return;
    }
    c.start();
  }
  
  public void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    int i = R.id.nextButton;
    paramView.findViewById(i).setOnClickListener(this);
    i = R.id.later;
    paramView.findViewById(i).setOnClickListener(this);
    i = R.id.details;
    paramBundle = (TextView)paramView.findViewById(i);
    a = paramBundle;
    a.setOnClickListener(this);
    paramBundle = a;
    MovementMethod localMovementMethod = ScrollingMovementMethod.getInstance();
    paramBundle.setMovementMethod(localMovementMethod);
    i = R.id.animated;
    paramView = (ImageView)paramView.findViewById(i);
    b = paramView;
    paramView = a();
    c = paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */