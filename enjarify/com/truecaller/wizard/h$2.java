package com.truecaller.wizard;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.widget.ImageView;

final class h$2
  extends AnimatorListenerAdapter
{
  private boolean b;
  
  h$2(h paramh) {}
  
  public final void onAnimationCancel(Animator paramAnimator)
  {
    b = true;
  }
  
  public final void onAnimationEnd(Animator paramAnimator)
  {
    boolean bool = b;
    if (!bool)
    {
      paramAnimator = h.a(a);
      h.2.1 local1 = new com/truecaller/wizard/h$2$1;
      local1.<init>(this);
      long l = 1500L;
      paramAnimator.postDelayed(local1, l);
    }
  }
  
  public final void onAnimationStart(Animator paramAnimator)
  {
    b = false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.h.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */