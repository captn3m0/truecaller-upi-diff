package com.truecaller.wizard;

import android.content.Context;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import com.truecaller.common.enhancedsearch.EnhancedSearchStateWorker;
import com.truecaller.common.h.ac;
import com.truecaller.utils.l;

public class a
  extends com.truecaller.wizard.b.i
  implements View.OnClickListener
{
  private void a()
  {
    l locall = f;
    boolean bool = locall.a();
    if (!bool)
    {
      ((com.truecaller.wizard.b.c)getActivity()).a("Page_DrawPermission", null);
      return;
    }
    ((com.truecaller.wizard.b.c)getActivity()).b();
  }
  
  private void a(boolean paramBoolean)
  {
    EnhancedSearchStateWorker.a(paramBoolean);
    a();
  }
  
  public static boolean a(Context paramContext)
  {
    if (paramContext == null) {
      return false;
    }
    paramContext = (com.truecaller.common.b.a)paramContext.getApplicationContext();
    Object localObject = paramContext.u();
    paramContext = paramContext.e();
    boolean bool = paramContext.c();
    if (bool)
    {
      paramContext = ((com.truecaller.common.a)localObject).n();
      bool = paramContext.a();
      if (!bool)
      {
        paramContext = ((com.truecaller.common.a)localObject).c();
        localObject = "backup";
        bool = paramContext.a((String)localObject, false);
        if (!bool) {
          return true;
        }
      }
    }
    return false;
  }
  
  public void onClick(View paramView)
  {
    super.onClick(paramView);
    int i = paramView.getId();
    int j = R.id.deny_button;
    if (i == j)
    {
      a(false);
      return;
    }
    j = R.id.allow_button;
    if (i == j)
    {
      paramView = f;
      String[] arrayOfString = { "android.permission.READ_CONTACTS" };
      boolean bool = paramView.a(arrayOfString);
      j = 1;
      if (bool)
      {
        a(j);
        return;
      }
      paramView = "android.permission.READ_CONTACTS";
      com.truecaller.wizard.utils.i.a(this, paramView, j, false);
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    int i = R.layout.wizard_view_access_contacts;
    return paramLayoutInflater.inflate(i, paramViewGroup, false);
  }
  
  public void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    com.truecaller.wizard.utils.i.a(paramArrayOfString, paramArrayOfInt);
    l locall = f;
    paramArrayOfString = new String[] { "android.permission.READ_CONTACTS" };
    paramInt = locall.a(paramArrayOfString);
    if (paramInt != 0)
    {
      paramInt = 1;
      a(paramInt);
    }
  }
  
  public void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    int i = R.id.deny_button;
    paramView.findViewById(i).setOnClickListener(this);
    i = R.id.allow_button;
    paramView.findViewById(i).setOnClickListener(this);
    i = R.id.details;
    paramView = (TextView)paramView.findViewById(i);
    paramBundle = LinkMovementMethod.getInstance();
    paramView.setMovementMethod(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */