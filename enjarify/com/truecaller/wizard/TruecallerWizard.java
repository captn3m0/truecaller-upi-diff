package com.truecaller.wizard;

import android.app.NotificationManager;
import android.os.Bundle;
import android.text.TextUtils;
import com.truecaller.utils.l;
import com.truecaller.utils.t;
import com.truecaller.utils.t.a;
import com.truecaller.wizard.b.b;
import java.util.Map;

public abstract class TruecallerWizard
  extends com.truecaller.wizard.b.c
{
  private l c;
  
  public final void a(Map paramMap)
  {
    b localb = new com/truecaller/wizard/b/b;
    localb.<init>(i.class, false);
    paramMap.put("Page_Welcome", localb);
    localb = new com/truecaller/wizard/b/b;
    boolean bool = true;
    localb.<init>(e.class, bool);
    paramMap.put("Page_EnterNumber", localb);
    localb = new com/truecaller/wizard/b/b;
    localb.<init>(com.truecaller.wizard.e.c.class, bool);
    paramMap.put("Page_Privacy", localb);
    localb = new com/truecaller/wizard/b/b;
    localb.<init>(com.truecaller.wizard.d.e.class, false);
    paramMap.put("Page_Verification", localb);
    localb = new com/truecaller/wizard/b/b;
    localb.<init>(h.class, false);
    paramMap.put("Page_Success", localb);
    localb = new com/truecaller/wizard/b/b;
    localb.<init>(f.class, bool);
    paramMap.put("Page_Profile", localb);
    localb = new com/truecaller/wizard/b/b;
    localb.<init>(com.truecaller.wizard.adschoices.e.class, bool);
    paramMap.put("Page_AdsChoices", localb);
    localb = new com/truecaller/wizard/b/b;
    localb.<init>(a.class, bool);
    paramMap.put("Page_AccessContacts", localb);
    localb = new com/truecaller/wizard/b/b;
    localb.<init>(d.class, bool);
    paramMap.put("Page_DrawPermission", localb);
    localb = new com/truecaller/wizard/b/b;
    localb.<init>(c.class, false);
    paramMap.put("Page_DrawPermissionDetails", localb);
  }
  
  public final String d()
  {
    Object localObject = com.truecaller.common.b.a.F().u().c();
    String str = "isUserChangingNumber";
    boolean bool = ((com.truecaller.common.g.a)localObject).a(str, false);
    if (!bool)
    {
      localObject = "wizard_OEMMode";
      bool = com.truecaller.common.b.e.a((String)localObject, false);
      if (!bool) {
        return "Page_Welcome";
      }
    }
    return "Page_EnterNumber";
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    int i = 1;
    setRequestedOrientation(i);
    Object localObject = com.truecaller.utils.c.a().a(this).a().b();
    c = ((l)localObject);
    localObject = "wizard_HasSentFirstStartEvent";
    boolean bool = com.truecaller.common.b.e.a((String)localObject, false);
    if (!bool)
    {
      localObject = "wizard_HasSentFirstStartEvent";
      com.truecaller.common.b.e.b((String)localObject, i);
    }
  }
  
  public void onResume()
  {
    super.onResume();
    Object localObject = (NotificationManager)getSystemService("notification");
    int i;
    if (localObject != null)
    {
      i = R.id.wizard_notification;
      ((NotificationManager)localObject).cancel(i);
    }
    localObject = com.truecaller.common.b.e.a("wizard_StartPage");
    boolean bool = TextUtils.isEmpty((CharSequence)localObject);
    if (!bool)
    {
      localObject = c;
      String[] arrayOfString = { "android.permission.READ_PHONE_STATE" };
      bool = ((l)localObject).a(arrayOfString);
      if (!bool)
      {
        localObject = d();
        i = 0;
        arrayOfString = null;
        a((String)localObject, null);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.TruecallerWizard
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */