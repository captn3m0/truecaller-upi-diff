package com.truecaller.h;

import android.content.Context;
import android.media.AudioManager;
import android.telecom.TelecomManager;
import c.g.b.k;
import c.u;
import com.truecaller.h.a.b;
import com.truecaller.utils.d;

public final class a
{
  private final dagger.a a;
  private final Context b;
  private final com.truecaller.i.c c;
  private final d d;
  
  public a(dagger.a parama, Context paramContext, com.truecaller.i.c paramc, d paramd)
  {
    a = parama;
    b = paramContext;
    c = paramc;
    d = paramd;
  }
  
  public final b a()
  {
    Object localObject1 = d;
    int i = ((d)localObject1).h();
    int j = 23;
    if (i >= j)
    {
      localObject1 = c;
      localObject2 = "hasNativeDialerCallerId";
      boolean bool = ((com.truecaller.i.c)localObject1).b((String)localObject2);
      if (bool)
      {
        localObject1 = b;
        localObject2 = "telecom";
        localObject1 = ((Context)localObject1).getSystemService((String)localObject2);
        if (localObject1 != null)
        {
          localObject1 = (TelecomManager)localObject1;
          localObject2 = new com/truecaller/h/a/c;
          ((com.truecaller.h.a.c)localObject2).<init>((TelecomManager)localObject1);
          return (b)localObject2;
        }
        localObject1 = new c/u;
        ((u)localObject1).<init>("null cannot be cast to non-null type android.telecom.TelecomManager");
        throw ((Throwable)localObject1);
      }
    }
    localObject1 = new com/truecaller/h/a/a;
    Object localObject2 = a.get();
    k.a(localObject2, "audioManager.get()");
    localObject2 = (AudioManager)localObject2;
    ((com.truecaller.h.a.a)localObject1).<init>((AudioManager)localObject2);
    return (b)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.h.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */