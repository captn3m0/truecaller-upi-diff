package com.truecaller.h;

import dagger.a.d;
import javax.inject.Provider;

public final class h
  implements d
{
  private final Provider a;
  private final Provider b;
  
  private h(Provider paramProvider1, Provider paramProvider2)
  {
    a = paramProvider1;
    b = paramProvider2;
  }
  
  public static h a(Provider paramProvider1, Provider paramProvider2)
  {
    h localh = new com/truecaller/h/h;
    localh.<init>(paramProvider1, paramProvider2);
    return localh;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.h.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */