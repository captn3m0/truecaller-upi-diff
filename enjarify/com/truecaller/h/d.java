package com.truecaller.h;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.v;
import com.truecaller.androidactors.w;

public final class d
  implements c
{
  private final v a;
  
  public d(v paramv)
  {
    a = paramv;
  }
  
  public static boolean a(Class paramClass)
  {
    return c.class.equals(paramClass);
  }
  
  public final w a()
  {
    v localv = a;
    d.a locala = new com/truecaller/h/d$a;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locala.<init>(locale, (byte)0);
    return w.a(localv, locala);
  }
  
  public final w b()
  {
    v localv = a;
    d.b localb = new com/truecaller/h/d$b;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localb.<init>(locale, (byte)0);
    return w.a(localv, localb);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.h.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */