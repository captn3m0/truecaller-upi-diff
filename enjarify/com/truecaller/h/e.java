package com.truecaller.h;

import c.g.b.k;
import com.truecaller.androidactors.w;
import com.truecaller.h.a.b;
import com.truecaller.utils.l;

public final class e
  implements c
{
  private b a;
  private Boolean b;
  private final a c;
  private final l d;
  
  public e(a parama, l paraml)
  {
    c = parama;
    d = paraml;
  }
  
  private final boolean a(boolean paramBoolean)
  {
    Boolean localBoolean1 = b;
    Boolean localBoolean2 = Boolean.valueOf(paramBoolean);
    return k.a(localBoolean1, localBoolean2) ^ true;
  }
  
  private b c()
  {
    Object localObject = d;
    boolean bool1 = ((l)localObject).e();
    b localb = a;
    if (localb != null)
    {
      boolean bool2 = a(bool1);
      if (!bool2) {
        return localb;
      }
    }
    localb = c.a();
    a = localb;
    localObject = Boolean.valueOf(bool1);
    b = ((Boolean)localObject);
    return localb;
  }
  
  public final w a()
  {
    new String[1][0] = "Trying to mute";
    Object localObject = c();
    boolean bool = ((b)localObject).a();
    if (bool)
    {
      new String[1][0] = "Ringer is already mute";
      localObject = w.b(Boolean.FALSE);
      k.a(localObject, "Promise.wrap(false)");
      return (w)localObject;
    }
    ((b)localObject).b();
    localObject = w.b(Boolean.TRUE);
    k.a(localObject, "Promise.wrap(true)");
    return (w)localObject;
  }
  
  public final w b()
  {
    new String[1][0] = "Trying to unmute";
    Object localObject = c();
    boolean bool = ((b)localObject).a();
    if (!bool)
    {
      new String[1][0] = "Ringer is already unmute";
      localObject = w.b(Boolean.FALSE);
      k.a(localObject, "Promise.wrap(false)");
      return (w)localObject;
    }
    ((b)localObject).c();
    localObject = w.b(Boolean.TRUE);
    k.a(localObject, "Promise.wrap(true)");
    return (w)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.h.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */