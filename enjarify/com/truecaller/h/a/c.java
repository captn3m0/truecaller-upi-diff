package com.truecaller.h.a;

import android.telecom.TelecomManager;
import com.truecaller.log.AssertionUtil;

public final class c
  implements b
{
  private final TelecomManager a;
  
  public c(TelecomManager paramTelecomManager)
  {
    a = paramTelecomManager;
  }
  
  public final boolean a()
  {
    return false;
  }
  
  public final void b()
  {
    Object localObject = "Silencing ringer";
    try
    {
      new String[1][0] = localObject;
      localObject = a;
      ((TelecomManager)localObject).silenceRinger();
      return;
    }
    catch (SecurityException localSecurityException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localSecurityException);
    }
  }
  
  public final void c() {}
}

/* Location:
 * Qualified Name:     com.truecaller.h.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */