package com.truecaller.h.a;

import android.media.AudioManager;

public final class a
  implements b
{
  private Integer a;
  private Integer b;
  private final AudioManager c;
  
  public a(AudioManager paramAudioManager)
  {
    c = paramAudioManager;
  }
  
  private static String a(Integer paramInteger)
  {
    int i;
    if (paramInteger != null)
    {
      i = paramInteger.intValue();
      if (i == 0) {
        return "RINGER_MODE_SILENT";
      }
    }
    if (paramInteger != null)
    {
      i = paramInteger.intValue();
      int j = 1;
      if (i == j) {
        return "RINGER_MODE_VIBRATE";
      }
    }
    if (paramInteger != null)
    {
      int k = paramInteger.intValue();
      i = 2;
      if (k == i) {
        return "RINGER_MODE_NORMAL";
      }
    }
    return null;
  }
  
  public final boolean a()
  {
    AudioManager localAudioManager = c;
    int i = localAudioManager.getStreamVolume(2);
    int j = 1;
    if (i == 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localAudioManager = null;
    }
    if (i != 0)
    {
      localAudioManager = c;
      i = localAudioManager.getRingerMode();
      if (i == j)
      {
        i = 1;
      }
      else
      {
        i = 0;
        localAudioManager = null;
      }
      if (i == 0) {
        return j;
      }
    }
    return false;
  }
  
  public final void b()
  {
    Object localObject1 = a;
    boolean bool = true;
    int i;
    Object localObject2;
    if (localObject1 == null)
    {
      localObject1 = c;
      i = ((AudioManager)localObject1).getRingerMode();
      if (i == 0)
      {
        i = 1;
      }
      else
      {
        i = 0;
        localObject1 = null;
      }
      if (i == 0)
      {
        i = c.getRingerMode();
        localObject1 = Integer.valueOf(i);
        a = ((Integer)localObject1);
        try
        {
          localObject1 = c;
          ((AudioManager)localObject1).setRingerMode(0);
          localObject1 = new String[bool];
          localObject2 = new java/lang/StringBuilder;
          Object localObject3 = "Changed ringer mode to RINGER_MODE_SILENT from ";
          ((StringBuilder)localObject2).<init>((String)localObject3);
          localObject3 = a;
          localObject3 = a((Integer)localObject3);
          ((StringBuilder)localObject2).append((String)localObject3);
          localObject2 = ((StringBuilder)localObject2).toString();
          localObject1[0] = localObject2;
        }
        catch (SecurityException localSecurityException1) {}
      }
    }
    localObject1 = b;
    if (localObject1 == null)
    {
      localObject1 = c;
      int j = 2;
      i = ((AudioManager)localObject1).getStreamVolume(j);
      if (i != 0)
      {
        i = c.getStreamVolume(j);
        localObject1 = Integer.valueOf(i);
        b = ((Integer)localObject1);
        try
        {
          localObject1 = c;
          ((AudioManager)localObject1).setStreamMute(j, bool);
          localObject1 = new String[bool];
          Object localObject4 = new java/lang/StringBuilder;
          localObject2 = "Muted STREAM_RING from Volume ";
          ((StringBuilder)localObject4).<init>((String)localObject2);
          localObject2 = b;
          ((StringBuilder)localObject4).append(localObject2);
          localObject4 = ((StringBuilder)localObject4).toString();
          localObject1[0] = localObject4;
          return;
        }
        catch (SecurityException localSecurityException2) {}
      }
    }
  }
  
  public final void c()
  {
    Object localObject1 = a;
    Object localObject2 = b;
    int i = 1;
    int j;
    if (localObject1 != null) {
      try
      {
        Object localObject3 = c;
        j = ((Integer)localObject1).intValue();
        ((AudioManager)localObject3).setRingerMode(j);
        localObject3 = new String[i];
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        String str1 = "Changed ringer mode back to ";
        localStringBuilder.<init>(str1);
        localObject1 = a((Integer)localObject1);
        localStringBuilder.append((String)localObject1);
        localObject1 = localStringBuilder.toString();
        localObject3[0] = localObject1;
        a = null;
      }
      catch (SecurityException localSecurityException1) {}
    }
    if (localObject2 != null) {
      try
      {
        localObject1 = c;
        int k = 2;
        ((AudioManager)localObject1).setStreamMute(k, false);
        localObject1 = c;
        j = ((Integer)localObject2).intValue();
        ((AudioManager)localObject1).setStreamVolume(k, j, 0);
        localObject1 = new String[i];
        String str2 = "Changed STREAM_RING back to Volume ";
        localObject2 = String.valueOf(localObject2);
        localObject2 = str2.concat((String)localObject2);
        localObject1[0] = localObject2;
        b = null;
        return;
      }
      catch (SecurityException localSecurityException2) {}
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.h.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */