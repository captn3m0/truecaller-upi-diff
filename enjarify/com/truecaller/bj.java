package com.truecaller;

import android.content.Context;
import android.net.Uri;
import com.bumptech.glide.b.h;
import com.bumptech.glide.b.m;
import com.bumptech.glide.e;
import com.bumptech.glide.e.f;
import com.bumptech.glide.l;

public final class bj
  extends l
{
  public bj(e parame, h paramh, m paramm, Context paramContext)
  {
    super(parame, paramh, paramm, paramContext);
  }
  
  public final void a(f paramf)
  {
    boolean bool = paramf instanceof bh;
    if (bool)
    {
      super.a((f)paramf);
      return;
    }
    bh localbh = new com/truecaller/bh;
    localbh.<init>();
    paramf = localbh.a(paramf);
    super.a(paramf);
  }
  
  public final bi b(Uri paramUri)
  {
    return (bi)super.a(paramUri);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.bj
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */