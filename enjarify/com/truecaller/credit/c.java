package com.truecaller.credit;

import android.app.Application;
import android.content.Context;
import c.u;
import com.truecaller.featuretoggles.d.b;
import com.truecaller.truepay.Truepay;

public final class c
  implements d.b
{
  private final String a;
  private final Context b;
  private final e c;
  
  public c(Context paramContext, e parame)
  {
    b = paramContext;
    c = parame;
    a = "featureTcCredit";
  }
  
  public final String a()
  {
    return a;
  }
  
  public final void b()
  {
    Object localObject1 = c;
    Object localObject2 = b.getApplicationContext();
    if (localObject2 != null)
    {
      localObject2 = (Application)localObject2;
      ((e)localObject1).a((Application)localObject2);
      localObject1 = Truepay.getInstance();
      localObject2 = (com.truecaller.truepay.c)c;
      ((Truepay)localObject1).setCreditHelper((com.truecaller.truepay.c)localObject2);
      return;
    }
    localObject1 = new c/u;
    ((u)localObject1).<init>("null cannot be cast to non-null type android.app.Application");
    throw ((Throwable)localObject1);
  }
  
  public final void c() {}
}

/* Location:
 * Qualified Name:     com.truecaller.credit.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */