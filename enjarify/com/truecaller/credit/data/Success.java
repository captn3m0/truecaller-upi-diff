package com.truecaller.credit.data;

import c.g.b.k;

public final class Success
  extends Result
{
  private final Object data;
  
  public Success(Object paramObject)
  {
    super(null);
    data = paramObject;
  }
  
  public final Object component1()
  {
    return data;
  }
  
  public final Success copy(Object paramObject)
  {
    k.b(paramObject, "data");
    Success localSuccess = new com/truecaller/credit/data/Success;
    localSuccess.<init>(paramObject);
    return localSuccess;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof Success;
      if (bool1)
      {
        paramObject = (Success)paramObject;
        Object localObject = data;
        paramObject = data;
        boolean bool2 = k.a(localObject, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final Object getData()
  {
    return data;
  }
  
  public final int hashCode()
  {
    Object localObject = data;
    if (localObject != null) {
      return localObject.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Success(data=");
    Object localObject = data;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.Success
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */