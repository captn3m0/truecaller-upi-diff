package com.truecaller.credit.data.models;

import c.g.b.k;
import c.n.m;
import com.truecaller.credit.domain.interactors.onboarding.models.b;

public final class VerifyPanResponseHolder
  extends BaseApiResponse
  implements Mappable
{
  private final VerifyPanResponseData data;
  
  public VerifyPanResponseHolder(VerifyPanResponseData paramVerifyPanResponseData)
  {
    data = paramVerifyPanResponseData;
  }
  
  private final VerifyPanResponseData component1()
  {
    return data;
  }
  
  public final VerifyPanResponseHolder copy(VerifyPanResponseData paramVerifyPanResponseData)
  {
    k.b(paramVerifyPanResponseData, "data");
    VerifyPanResponseHolder localVerifyPanResponseHolder = new com/truecaller/credit/data/models/VerifyPanResponseHolder;
    localVerifyPanResponseHolder.<init>(paramVerifyPanResponseData);
    return localVerifyPanResponseHolder;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof VerifyPanResponseHolder;
      if (bool1)
      {
        paramObject = (VerifyPanResponseHolder)paramObject;
        VerifyPanResponseData localVerifyPanResponseData = data;
        paramObject = data;
        boolean bool2 = k.a(localVerifyPanResponseData, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String errorMessage()
  {
    BaseApiResponse.Meta localMeta = getMeta();
    if (localMeta != null) {
      return localMeta.getMessage();
    }
    return null;
  }
  
  public final int hashCode()
  {
    VerifyPanResponseData localVerifyPanResponseData = data;
    if (localVerifyPanResponseData != null) {
      return localVerifyPanResponseData.hashCode();
    }
    return 0;
  }
  
  public final boolean isValid()
  {
    return m.a(getStatus(), "success", true);
  }
  
  public final b mapToData()
  {
    b localb = new com/truecaller/credit/domain/interactors/onboarding/models/b;
    String str1 = data.getPan().getNumber();
    String str2 = data.getPan().getName();
    String str3 = data.getPan().getGender();
    localb.<init>(str1, str2, str3);
    return localb;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("VerifyPanResponseHolder(data=");
    VerifyPanResponseData localVerifyPanResponseData = data;
    localStringBuilder.append(localVerifyPanResponseData);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.VerifyPanResponseHolder
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */