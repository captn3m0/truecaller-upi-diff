package com.truecaller.credit.data.models;

import c.g.b.k;

public final class RequestFinalOfferOtpResult
{
  private final String msisdn;
  
  public RequestFinalOfferOtpResult(String paramString)
  {
    msisdn = paramString;
  }
  
  public final String component1()
  {
    return msisdn;
  }
  
  public final RequestFinalOfferOtpResult copy(String paramString)
  {
    RequestFinalOfferOtpResult localRequestFinalOfferOtpResult = new com/truecaller/credit/data/models/RequestFinalOfferOtpResult;
    localRequestFinalOfferOtpResult.<init>(paramString);
    return localRequestFinalOfferOtpResult;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof RequestFinalOfferOtpResult;
      if (bool1)
      {
        paramObject = (RequestFinalOfferOtpResult)paramObject;
        String str = msisdn;
        paramObject = msisdn;
        boolean bool2 = k.a(str, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String getMsisdn()
  {
    return msisdn;
  }
  
  public final int hashCode()
  {
    String str = msisdn;
    if (str != null) {
      return str.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("RequestFinalOfferOtpResult(msisdn=");
    String str = msisdn;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.RequestFinalOfferOtpResult
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */