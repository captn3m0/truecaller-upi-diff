package com.truecaller.credit.data.models;

import c.n.m;

public final class BankDetailsResponse
  extends BaseApiResponse
  implements Mappable
{
  public final String errorMessage()
  {
    BaseApiResponse.Meta localMeta = getMeta();
    if (localMeta != null) {
      return localMeta.getMessage();
    }
    return null;
  }
  
  public final boolean isValid()
  {
    return m.a(getStatus(), "success", true);
  }
  
  public final BankDetailsResult mapToData()
  {
    BankDetailsResult localBankDetailsResult = new com/truecaller/credit/data/models/BankDetailsResult;
    Object localObject = getMeta();
    if (localObject != null)
    {
      localObject = ((BaseApiResponse.Meta)localObject).getMessage();
      if (localObject != null) {}
    }
    else
    {
      localObject = "";
    }
    localBankDetailsResult.<init>((String)localObject);
    return localBankDetailsResult;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.BankDetailsResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */