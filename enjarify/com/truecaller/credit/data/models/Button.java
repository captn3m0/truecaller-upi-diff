package com.truecaller.credit.data.models;

import c.g.b.k;

public final class Button
{
  private final String action;
  private final String text;
  
  public Button(String paramString1, String paramString2)
  {
    action = paramString1;
    text = paramString2;
  }
  
  public final String component1()
  {
    return action;
  }
  
  public final String component2()
  {
    return text;
  }
  
  public final Button copy(String paramString1, String paramString2)
  {
    Button localButton = new com/truecaller/credit/data/models/Button;
    localButton.<init>(paramString1, paramString2);
    return localButton;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof Button;
      if (bool1)
      {
        paramObject = (Button)paramObject;
        String str1 = action;
        String str2 = action;
        bool1 = k.a(str1, str2);
        if (bool1)
        {
          str1 = text;
          paramObject = text;
          boolean bool2 = k.a(str1, paramObject);
          if (bool2) {
            break label68;
          }
        }
      }
      return false;
    }
    label68:
    return true;
  }
  
  public final String getAction()
  {
    return action;
  }
  
  public final String getText()
  {
    return text;
  }
  
  public final int hashCode()
  {
    String str1 = action;
    int i = 0;
    int j;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = text;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Button(action=");
    String str = action;
    localStringBuilder.append(str);
    localStringBuilder.append(", text=");
    str = text;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.Button
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */