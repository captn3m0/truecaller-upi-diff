package com.truecaller.credit.data.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class APIStatusMessage
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  private final String actionButtonText;
  private final String message;
  private final boolean shouldRedirectToHomeScreen;
  private final int status;
  private final String title;
  
  static
  {
    APIStatusMessage.Creator localCreator = new com/truecaller/credit/data/models/APIStatusMessage$Creator;
    localCreator.<init>();
    CREATOR = localCreator;
  }
  
  public APIStatusMessage(int paramInt, String paramString1, String paramString2, boolean paramBoolean, String paramString3)
  {
    status = paramInt;
    title = paramString1;
    message = paramString2;
    shouldRedirectToHomeScreen = paramBoolean;
    actionButtonText = paramString3;
  }
  
  public final int component1()
  {
    return status;
  }
  
  public final String component2()
  {
    return title;
  }
  
  public final String component3()
  {
    return message;
  }
  
  public final boolean component4()
  {
    return shouldRedirectToHomeScreen;
  }
  
  public final String component5()
  {
    return actionButtonText;
  }
  
  public final APIStatusMessage copy(int paramInt, String paramString1, String paramString2, boolean paramBoolean, String paramString3)
  {
    k.b(paramString1, "title");
    k.b(paramString2, "message");
    k.b(paramString3, "actionButtonText");
    APIStatusMessage localAPIStatusMessage = new com/truecaller/credit/data/models/APIStatusMessage;
    localAPIStatusMessage.<init>(paramInt, paramString1, paramString2, paramBoolean, paramString3);
    return localAPIStatusMessage;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof APIStatusMessage;
      if (bool2)
      {
        paramObject = (APIStatusMessage)paramObject;
        int i = status;
        int j = status;
        String str1;
        if (i == j)
        {
          i = 1;
        }
        else
        {
          i = 0;
          str1 = null;
        }
        if (i != 0)
        {
          str1 = title;
          String str2 = title;
          boolean bool3 = k.a(str1, str2);
          if (bool3)
          {
            str1 = message;
            str2 = message;
            bool3 = k.a(str1, str2);
            if (bool3)
            {
              bool3 = shouldRedirectToHomeScreen;
              boolean bool4 = shouldRedirectToHomeScreen;
              if (bool3 == bool4)
              {
                bool3 = true;
              }
              else
              {
                bool3 = false;
                str1 = null;
              }
              if (bool3)
              {
                str1 = actionButtonText;
                paramObject = actionButtonText;
                boolean bool5 = k.a(str1, paramObject);
                if (bool5) {
                  return bool1;
                }
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final String getActionButtonText()
  {
    return actionButtonText;
  }
  
  public final String getMessage()
  {
    return message;
  }
  
  public final boolean getShouldRedirectToHomeScreen()
  {
    return shouldRedirectToHomeScreen;
  }
  
  public final int getStatus()
  {
    return status;
  }
  
  public final String getTitle()
  {
    return title;
  }
  
  public final int hashCode()
  {
    int i = status * 31;
    String str = title;
    int j = 0;
    int k;
    if (str != null)
    {
      k = str.hashCode();
    }
    else
    {
      k = 0;
      str = null;
    }
    i = (i + k) * 31;
    str = message;
    if (str != null)
    {
      k = str.hashCode();
    }
    else
    {
      k = 0;
      str = null;
    }
    i = (i + k) * 31;
    int m = shouldRedirectToHomeScreen;
    if (m != 0) {
      m = 1;
    }
    i = (i + m) * 31;
    str = actionButtonText;
    if (str != null) {
      j = str.hashCode();
    }
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("APIStatusMessage(status=");
    int i = status;
    localStringBuilder.append(i);
    localStringBuilder.append(", title=");
    String str = title;
    localStringBuilder.append(str);
    localStringBuilder.append(", message=");
    str = message;
    localStringBuilder.append(str);
    localStringBuilder.append(", shouldRedirectToHomeScreen=");
    boolean bool = shouldRedirectToHomeScreen;
    localStringBuilder.append(bool);
    localStringBuilder.append(", actionButtonText=");
    str = actionButtonText;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    paramInt = status;
    paramParcel.writeInt(paramInt);
    String str = title;
    paramParcel.writeString(str);
    str = message;
    paramParcel.writeString(str);
    paramInt = shouldRedirectToHomeScreen;
    paramParcel.writeInt(paramInt);
    str = actionButtonText;
    paramParcel.writeString(str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.APIStatusMessage
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */