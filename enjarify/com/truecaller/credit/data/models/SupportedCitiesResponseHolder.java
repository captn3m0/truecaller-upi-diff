package com.truecaller.credit.data.models;

import c.g.b.k;
import c.n.m;
import com.truecaller.credit.domain.interactors.onboarding.models.d;
import java.util.List;

public final class SupportedCitiesResponseHolder
  extends BaseApiResponse
  implements Mappable
{
  private final SupportedCitiesResponse data;
  
  public SupportedCitiesResponseHolder(SupportedCitiesResponse paramSupportedCitiesResponse)
  {
    data = paramSupportedCitiesResponse;
  }
  
  public final SupportedCitiesResponse component1()
  {
    return data;
  }
  
  public final SupportedCitiesResponseHolder copy(SupportedCitiesResponse paramSupportedCitiesResponse)
  {
    k.b(paramSupportedCitiesResponse, "data");
    SupportedCitiesResponseHolder localSupportedCitiesResponseHolder = new com/truecaller/credit/data/models/SupportedCitiesResponseHolder;
    localSupportedCitiesResponseHolder.<init>(paramSupportedCitiesResponse);
    return localSupportedCitiesResponseHolder;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof SupportedCitiesResponseHolder;
      if (bool1)
      {
        paramObject = (SupportedCitiesResponseHolder)paramObject;
        SupportedCitiesResponse localSupportedCitiesResponse = data;
        paramObject = data;
        boolean bool2 = k.a(localSupportedCitiesResponse, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String errorMessage()
  {
    BaseApiResponse.Meta localMeta = getMeta();
    if (localMeta != null) {
      return localMeta.getMessage();
    }
    return null;
  }
  
  public final SupportedCitiesResponse getData()
  {
    return data;
  }
  
  public final int hashCode()
  {
    SupportedCitiesResponse localSupportedCitiesResponse = data;
    if (localSupportedCitiesResponse != null) {
      return localSupportedCitiesResponse.hashCode();
    }
    return 0;
  }
  
  public final boolean isValid()
  {
    return m.a(getStatus(), "success", true);
  }
  
  public final d mapToData()
  {
    d locald = new com/truecaller/credit/domain/interactors/onboarding/models/d;
    List localList = data.getLocations();
    locald.<init>(localList);
    return locald;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("SupportedCitiesResponseHolder(data=");
    SupportedCitiesResponse localSupportedCitiesResponse = data;
    localStringBuilder.append(localSupportedCitiesResponse);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.SupportedCitiesResponseHolder
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */