package com.truecaller.credit.data.models;

import c.g.b.k;

public final class BankDetailsRequest
{
  private final String account_number;
  private final String ifsc;
  
  public BankDetailsRequest(String paramString1, String paramString2)
  {
    account_number = paramString1;
    ifsc = paramString2;
  }
  
  public final String component1()
  {
    return account_number;
  }
  
  public final String component2()
  {
    return ifsc;
  }
  
  public final BankDetailsRequest copy(String paramString1, String paramString2)
  {
    k.b(paramString1, "account_number");
    k.b(paramString2, "ifsc");
    BankDetailsRequest localBankDetailsRequest = new com/truecaller/credit/data/models/BankDetailsRequest;
    localBankDetailsRequest.<init>(paramString1, paramString2);
    return localBankDetailsRequest;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof BankDetailsRequest;
      if (bool1)
      {
        paramObject = (BankDetailsRequest)paramObject;
        String str1 = account_number;
        String str2 = account_number;
        bool1 = k.a(str1, str2);
        if (bool1)
        {
          str1 = ifsc;
          paramObject = ifsc;
          boolean bool2 = k.a(str1, paramObject);
          if (bool2) {
            break label68;
          }
        }
      }
      return false;
    }
    label68:
    return true;
  }
  
  public final String getAccount_number()
  {
    return account_number;
  }
  
  public final String getIfsc()
  {
    return ifsc;
  }
  
  public final int hashCode()
  {
    String str1 = account_number;
    int i = 0;
    int j;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = ifsc;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("BankDetailsRequest(account_number=");
    String str = account_number;
    localStringBuilder.append(str);
    localStringBuilder.append(", ifsc=");
    str = ifsc;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.BankDetailsRequest
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */