package com.truecaller.credit.data.models;

import c.g.b.k;
import java.util.List;

public final class ScoreDataRules
{
  private String blacklist_regex;
  private String min_app_version;
  private final List rules;
  private String version;
  
  public ScoreDataRules(String paramString1, String paramString2, List paramList, String paramString3)
  {
    blacklist_regex = paramString1;
    min_app_version = paramString2;
    rules = paramList;
    version = paramString3;
  }
  
  public final String component1()
  {
    return blacklist_regex;
  }
  
  public final String component2()
  {
    return min_app_version;
  }
  
  public final List component3()
  {
    return rules;
  }
  
  public final String component4()
  {
    return version;
  }
  
  public final ScoreDataRules copy(String paramString1, String paramString2, List paramList, String paramString3)
  {
    k.b(paramString1, "blacklist_regex");
    k.b(paramString2, "min_app_version");
    k.b(paramList, "rules");
    k.b(paramString3, "version");
    ScoreDataRules localScoreDataRules = new com/truecaller/credit/data/models/ScoreDataRules;
    localScoreDataRules.<init>(paramString1, paramString2, paramList, paramString3);
    return localScoreDataRules;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof ScoreDataRules;
      if (bool1)
      {
        paramObject = (ScoreDataRules)paramObject;
        Object localObject1 = blacklist_regex;
        Object localObject2 = blacklist_regex;
        bool1 = k.a(localObject1, localObject2);
        if (bool1)
        {
          localObject1 = min_app_version;
          localObject2 = min_app_version;
          bool1 = k.a(localObject1, localObject2);
          if (bool1)
          {
            localObject1 = rules;
            localObject2 = rules;
            bool1 = k.a(localObject1, localObject2);
            if (bool1)
            {
              localObject1 = version;
              paramObject = version;
              boolean bool2 = k.a(localObject1, paramObject);
              if (bool2) {
                break label112;
              }
            }
          }
        }
      }
      return false;
    }
    label112:
    return true;
  }
  
  public final String getBlacklist_regex()
  {
    return blacklist_regex;
  }
  
  public final String getMin_app_version()
  {
    return min_app_version;
  }
  
  public final List getRules()
  {
    return rules;
  }
  
  public final String getVersion()
  {
    return version;
  }
  
  public final int hashCode()
  {
    String str = blacklist_regex;
    int i = 0;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    j *= 31;
    Object localObject = min_app_version;
    int k;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    int j = (j + k) * 31;
    localObject = rules;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = version;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final void setBlacklist_regex(String paramString)
  {
    k.b(paramString, "<set-?>");
    blacklist_regex = paramString;
  }
  
  public final void setMin_app_version(String paramString)
  {
    k.b(paramString, "<set-?>");
    min_app_version = paramString;
  }
  
  public final void setVersion(String paramString)
  {
    k.b(paramString, "<set-?>");
    version = paramString;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("ScoreDataRules(blacklist_regex=");
    Object localObject = blacklist_regex;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", min_app_version=");
    localObject = min_app_version;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", rules=");
    localObject = rules;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", version=");
    localObject = version;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.ScoreDataRules
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */