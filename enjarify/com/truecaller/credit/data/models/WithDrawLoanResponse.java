package com.truecaller.credit.data.models;

import c.g.b.k;
import c.n.m;
import com.truecaller.credit.domain.interactors.withdrawloan.models.LoanCategory;
import com.truecaller.credit.domain.interactors.withdrawloan.models.LoanDetails;
import com.truecaller.credit.domain.interactors.withdrawloan.models.LoanDisplayData;

public final class WithDrawLoanResponse
  extends BaseApiResponse
  implements Mappable
{
  private final LoanDetailsData data;
  
  public WithDrawLoanResponse(LoanDetailsData paramLoanDetailsData)
  {
    data = paramLoanDetailsData;
  }
  
  public final LoanDetailsData component1()
  {
    return data;
  }
  
  public final WithDrawLoanResponse copy(LoanDetailsData paramLoanDetailsData)
  {
    k.b(paramLoanDetailsData, "data");
    WithDrawLoanResponse localWithDrawLoanResponse = new com/truecaller/credit/data/models/WithDrawLoanResponse;
    localWithDrawLoanResponse.<init>(paramLoanDetailsData);
    return localWithDrawLoanResponse;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof WithDrawLoanResponse;
      if (bool1)
      {
        paramObject = (WithDrawLoanResponse)paramObject;
        LoanDetailsData localLoanDetailsData = data;
        paramObject = data;
        boolean bool2 = k.a(localLoanDetailsData, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String errorMessage()
  {
    BaseApiResponse.Meta localMeta = getMeta();
    if (localMeta != null) {
      return localMeta.getMessage();
    }
    return null;
  }
  
  public final LoanDetailsData getData()
  {
    return data;
  }
  
  public final int hashCode()
  {
    LoanDetailsData localLoanDetailsData = data;
    if (localLoanDetailsData != null) {
      return localLoanDetailsData.hashCode();
    }
    return 0;
  }
  
  public final boolean isValid()
  {
    return m.a(getStatus(), "success", true);
  }
  
  public final LoanDetails mapToData()
  {
    LoanDetails localLoanDetails = new com/truecaller/credit/domain/interactors/withdrawloan/models/LoanDetails;
    String str1 = data.getLoan().getId();
    String str2 = data.getLoan().getName();
    String str3 = data.getLoan().getStatus();
    String str4 = data.getLoan().getTenure();
    String str5 = data.getLoan().getAmount();
    LoanCategory localLoanCategory = data.getLoan().getCategory();
    String str6 = data.getLoan().getTenure_type();
    String str7 = data.getLoan().getVendor_uuid();
    LoanDisplayData localLoanDisplayData = data.getLoan().getDisplay_configuration();
    localLoanDetails.<init>(str1, str2, str3, str4, str5, localLoanCategory, str6, str7, localLoanDisplayData);
    return localLoanDetails;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("WithDrawLoanResponse(data=");
    LoanDetailsData localLoanDetailsData = data;
    localStringBuilder.append(localLoanDetailsData);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.WithDrawLoanResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */