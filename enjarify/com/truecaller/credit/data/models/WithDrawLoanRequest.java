package com.truecaller.credit.data.models;

import c.g.b.k;

public final class WithDrawLoanRequest
{
  private final String custom_category_name;
  private final String drawdown_name;
  private final String loan_amount;
  private final String loan_category_id;
  private final String tenure;
  
  public WithDrawLoanRequest(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    drawdown_name = paramString1;
    tenure = paramString2;
    loan_amount = paramString3;
    loan_category_id = paramString4;
    custom_category_name = paramString5;
  }
  
  public final String component1()
  {
    return drawdown_name;
  }
  
  public final String component2()
  {
    return tenure;
  }
  
  public final String component3()
  {
    return loan_amount;
  }
  
  public final String component4()
  {
    return loan_category_id;
  }
  
  public final String component5()
  {
    return custom_category_name;
  }
  
  public final WithDrawLoanRequest copy(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    k.b(paramString1, "drawdown_name");
    k.b(paramString2, "tenure");
    k.b(paramString3, "loan_amount");
    k.b(paramString4, "loan_category_id");
    WithDrawLoanRequest localWithDrawLoanRequest = new com/truecaller/credit/data/models/WithDrawLoanRequest;
    localWithDrawLoanRequest.<init>(paramString1, paramString2, paramString3, paramString4, paramString5);
    return localWithDrawLoanRequest;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof WithDrawLoanRequest;
      if (bool1)
      {
        paramObject = (WithDrawLoanRequest)paramObject;
        String str1 = drawdown_name;
        String str2 = drawdown_name;
        bool1 = k.a(str1, str2);
        if (bool1)
        {
          str1 = tenure;
          str2 = tenure;
          bool1 = k.a(str1, str2);
          if (bool1)
          {
            str1 = loan_amount;
            str2 = loan_amount;
            bool1 = k.a(str1, str2);
            if (bool1)
            {
              str1 = loan_category_id;
              str2 = loan_category_id;
              bool1 = k.a(str1, str2);
              if (bool1)
              {
                str1 = custom_category_name;
                paramObject = custom_category_name;
                boolean bool2 = k.a(str1, paramObject);
                if (bool2) {
                  break label134;
                }
              }
            }
          }
        }
      }
      return false;
    }
    label134:
    return true;
  }
  
  public final String getCustom_category_name()
  {
    return custom_category_name;
  }
  
  public final String getDrawdown_name()
  {
    return drawdown_name;
  }
  
  public final String getLoan_amount()
  {
    return loan_amount;
  }
  
  public final String getLoan_category_id()
  {
    return loan_category_id;
  }
  
  public final String getTenure()
  {
    return tenure;
  }
  
  public final int hashCode()
  {
    String str1 = drawdown_name;
    int i = 0;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = tenure;
    int k;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    int j = (j + k) * 31;
    str2 = loan_amount;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    j = (j + k) * 31;
    str2 = loan_category_id;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    j = (j + k) * 31;
    str2 = custom_category_name;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("WithDrawLoanRequest(drawdown_name=");
    String str = drawdown_name;
    localStringBuilder.append(str);
    localStringBuilder.append(", tenure=");
    str = tenure;
    localStringBuilder.append(str);
    localStringBuilder.append(", loan_amount=");
    str = loan_amount;
    localStringBuilder.append(str);
    localStringBuilder.append(", loan_category_id=");
    str = loan_category_id;
    localStringBuilder.append(str);
    localStringBuilder.append(", custom_category_name=");
    str = custom_category_name;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.WithDrawLoanRequest
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */