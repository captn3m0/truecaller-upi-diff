package com.truecaller.credit.data.models;

import c.g.b.k;

public final class FetchSlotRequest
{
  private final String pincode;
  
  public FetchSlotRequest(String paramString)
  {
    pincode = paramString;
  }
  
  public final String component1()
  {
    return pincode;
  }
  
  public final FetchSlotRequest copy(String paramString)
  {
    k.b(paramString, "pincode");
    FetchSlotRequest localFetchSlotRequest = new com/truecaller/credit/data/models/FetchSlotRequest;
    localFetchSlotRequest.<init>(paramString);
    return localFetchSlotRequest;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof FetchSlotRequest;
      if (bool1)
      {
        paramObject = (FetchSlotRequest)paramObject;
        String str = pincode;
        paramObject = pincode;
        boolean bool2 = k.a(str, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String getPincode()
  {
    return pincode;
  }
  
  public final int hashCode()
  {
    String str = pincode;
    if (str != null) {
      return str.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("FetchSlotRequest(pincode=");
    String str = pincode;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.FetchSlotRequest
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */