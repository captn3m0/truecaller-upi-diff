package com.truecaller.credit.data.models;

import c.g.b.k;
import c.n.m;

public final class BookSlotResponse
  extends BaseApiResponse
  implements Mappable
{
  private final BookSlotResponse.GetBookSlotResponseData data;
  
  public BookSlotResponse(BookSlotResponse.GetBookSlotResponseData paramGetBookSlotResponseData)
  {
    data = paramGetBookSlotResponseData;
  }
  
  public final BookSlotResponse.GetBookSlotResponseData component1()
  {
    return data;
  }
  
  public final BookSlotResponse copy(BookSlotResponse.GetBookSlotResponseData paramGetBookSlotResponseData)
  {
    k.b(paramGetBookSlotResponseData, "data");
    BookSlotResponse localBookSlotResponse = new com/truecaller/credit/data/models/BookSlotResponse;
    localBookSlotResponse.<init>(paramGetBookSlotResponseData);
    return localBookSlotResponse;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof BookSlotResponse;
      if (bool1)
      {
        paramObject = (BookSlotResponse)paramObject;
        BookSlotResponse.GetBookSlotResponseData localGetBookSlotResponseData = data;
        paramObject = data;
        boolean bool2 = k.a(localGetBookSlotResponseData, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String errorMessage()
  {
    BaseApiResponse.Meta localMeta = getMeta();
    if (localMeta != null) {
      return localMeta.getMessage();
    }
    return null;
  }
  
  public final BookSlotResponse.GetBookSlotResponseData getData()
  {
    return data;
  }
  
  public final int hashCode()
  {
    BookSlotResponse.GetBookSlotResponseData localGetBookSlotResponseData = data;
    if (localGetBookSlotResponseData != null) {
      return localGetBookSlotResponseData.hashCode();
    }
    return 0;
  }
  
  public final boolean isValid()
  {
    return m.a(getStatus(), "success", true);
  }
  
  public final String mapToData()
  {
    String str = data.getMessage();
    if (str == null) {
      str = "";
    }
    return str;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("BookSlotResponse(data=");
    BookSlotResponse.GetBookSlotResponseData localGetBookSlotResponseData = data;
    localStringBuilder.append(localGetBookSlotResponseData);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.BookSlotResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */