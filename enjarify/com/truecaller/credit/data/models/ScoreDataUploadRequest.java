package com.truecaller.credit.data.models;

import c.g.b.k;
import com.google.gson.o;

public final class ScoreDataUploadRequest
{
  private final o user_data;
  
  public ScoreDataUploadRequest(o paramo)
  {
    user_data = paramo;
  }
  
  public final o component1()
  {
    return user_data;
  }
  
  public final ScoreDataUploadRequest copy(o paramo)
  {
    k.b(paramo, "user_data");
    ScoreDataUploadRequest localScoreDataUploadRequest = new com/truecaller/credit/data/models/ScoreDataUploadRequest;
    localScoreDataUploadRequest.<init>(paramo);
    return localScoreDataUploadRequest;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof ScoreDataUploadRequest;
      if (bool1)
      {
        paramObject = (ScoreDataUploadRequest)paramObject;
        o localo = user_data;
        paramObject = user_data;
        boolean bool2 = k.a(localo, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final o getUser_data()
  {
    return user_data;
  }
  
  public final int hashCode()
  {
    o localo = user_data;
    if (localo != null) {
      return localo.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("ScoreDataUploadRequest(user_data=");
    o localo = user_data;
    localStringBuilder.append(localo);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.ScoreDataUploadRequest
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */