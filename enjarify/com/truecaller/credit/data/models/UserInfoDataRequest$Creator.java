package com.truecaller.credit.data.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;
import java.util.ArrayList;

public final class UserInfoDataRequest$Creator
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    UserInfoDataRequest localUserInfoDataRequest = new com/truecaller/credit/data/models/UserInfoDataRequest;
    String str1 = paramParcel.readString();
    String str2 = paramParcel.readString();
    String str3 = paramParcel.readString();
    String str4 = paramParcel.readString();
    int i = paramParcel.readInt();
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>(i);
    while (i != 0)
    {
      Address localAddress = (Address)Address.CREATOR.createFromParcel(paramParcel);
      localArrayList.add(localAddress);
      i += -1;
    }
    localUserInfoDataRequest.<init>(str1, str2, str3, str4, localArrayList);
    return localUserInfoDataRequest;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new UserInfoDataRequest[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.UserInfoDataRequest.Creator
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */