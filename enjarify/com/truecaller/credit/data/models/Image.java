package com.truecaller.credit.data.models;

import c.g.b.k;

public final class Image
{
  private final Integer percentage;
  private final String type;
  private final String url;
  
  public Image(Integer paramInteger, String paramString1, String paramString2)
  {
    percentage = paramInteger;
    type = paramString1;
    url = paramString2;
  }
  
  public final Integer component1()
  {
    return percentage;
  }
  
  public final String component2()
  {
    return type;
  }
  
  public final String component3()
  {
    return url;
  }
  
  public final Image copy(Integer paramInteger, String paramString1, String paramString2)
  {
    Image localImage = new com/truecaller/credit/data/models/Image;
    localImage.<init>(paramInteger, paramString1, paramString2);
    return localImage;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof Image;
      if (bool1)
      {
        paramObject = (Image)paramObject;
        Object localObject1 = percentage;
        Object localObject2 = percentage;
        bool1 = k.a(localObject1, localObject2);
        if (bool1)
        {
          localObject1 = type;
          localObject2 = type;
          bool1 = k.a(localObject1, localObject2);
          if (bool1)
          {
            localObject1 = url;
            paramObject = url;
            boolean bool2 = k.a(localObject1, paramObject);
            if (bool2) {
              break label90;
            }
          }
        }
      }
      return false;
    }
    label90:
    return true;
  }
  
  public final Integer getPercentage()
  {
    return percentage;
  }
  
  public final String getType()
  {
    return type;
  }
  
  public final String getUrl()
  {
    return url;
  }
  
  public final int hashCode()
  {
    Integer localInteger = percentage;
    int i = 0;
    if (localInteger != null)
    {
      j = localInteger.hashCode();
    }
    else
    {
      j = 0;
      localInteger = null;
    }
    j *= 31;
    String str = type;
    int k;
    if (str != null)
    {
      k = str.hashCode();
    }
    else
    {
      k = 0;
      str = null;
    }
    int j = (j + k) * 31;
    str = url;
    if (str != null) {
      i = str.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Image(percentage=");
    Object localObject = percentage;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", type=");
    localObject = type;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", url=");
    localObject = url;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.Image
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */