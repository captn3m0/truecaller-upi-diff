package com.truecaller.credit.data.models;

import c.g.b.k;

public final class FetchAddressRequest
{
  private final String type;
  
  public FetchAddressRequest(String paramString)
  {
    type = paramString;
  }
  
  public final String component1()
  {
    return type;
  }
  
  public final FetchAddressRequest copy(String paramString)
  {
    k.b(paramString, "type");
    FetchAddressRequest localFetchAddressRequest = new com/truecaller/credit/data/models/FetchAddressRequest;
    localFetchAddressRequest.<init>(paramString);
    return localFetchAddressRequest;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof FetchAddressRequest;
      if (bool1)
      {
        paramObject = (FetchAddressRequest)paramObject;
        String str = type;
        paramObject = type;
        boolean bool2 = k.a(str, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String getType()
  {
    return type;
  }
  
  public final int hashCode()
  {
    String str = type;
    if (str != null) {
      return str.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("FetchAddressRequest(type=");
    String str = type;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.FetchAddressRequest
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */