package com.truecaller.credit.data.models;

import c.g.b.k;
import java.util.List;

public final class FetchSlotResponse$GetSlotResponseData
{
  private final List appointments;
  
  public FetchSlotResponse$GetSlotResponseData(List paramList)
  {
    appointments = paramList;
  }
  
  public final List component1()
  {
    return appointments;
  }
  
  public final GetSlotResponseData copy(List paramList)
  {
    k.b(paramList, "appointments");
    GetSlotResponseData localGetSlotResponseData = new com/truecaller/credit/data/models/FetchSlotResponse$GetSlotResponseData;
    localGetSlotResponseData.<init>(paramList);
    return localGetSlotResponseData;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof GetSlotResponseData;
      if (bool1)
      {
        paramObject = (GetSlotResponseData)paramObject;
        List localList = appointments;
        paramObject = appointments;
        boolean bool2 = k.a(localList, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final List getAppointments()
  {
    return appointments;
  }
  
  public final int hashCode()
  {
    List localList = appointments;
    if (localList != null) {
      return localList.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("GetSlotResponseData(appointments=");
    List localList = appointments;
    localStringBuilder.append(localList);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.FetchSlotResponse.GetSlotResponseData
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */