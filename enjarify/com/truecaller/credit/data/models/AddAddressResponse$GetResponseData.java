package com.truecaller.credit.data.models;

import c.g.b.k;

public final class AddAddressResponse$GetResponseData
{
  private final Address address;
  
  public AddAddressResponse$GetResponseData(Address paramAddress)
  {
    address = paramAddress;
  }
  
  public final Address component1()
  {
    return address;
  }
  
  public final GetResponseData copy(Address paramAddress)
  {
    k.b(paramAddress, "address");
    GetResponseData localGetResponseData = new com/truecaller/credit/data/models/AddAddressResponse$GetResponseData;
    localGetResponseData.<init>(paramAddress);
    return localGetResponseData;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof GetResponseData;
      if (bool1)
      {
        paramObject = (GetResponseData)paramObject;
        Address localAddress = address;
        paramObject = address;
        boolean bool2 = k.a(localAddress, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final Address getAddress()
  {
    return address;
  }
  
  public final int hashCode()
  {
    Address localAddress = address;
    if (localAddress != null) {
      return localAddress.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("GetResponseData(address=");
    Address localAddress = address;
    localStringBuilder.append(localAddress);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.AddAddressResponse.GetResponseData
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */