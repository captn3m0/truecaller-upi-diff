package com.truecaller.credit.data.models;

import c.g.b.k;
import c.n.m;
import com.truecaller.credit.domain.interactors.withdrawloan.models.CreditLineDetails;

public final class CreditLineResponse
  extends BaseApiResponse
  implements Mappable
{
  private final CreditLineDetails data;
  
  public CreditLineResponse(CreditLineDetails paramCreditLineDetails)
  {
    data = paramCreditLineDetails;
  }
  
  public final CreditLineDetails component1()
  {
    return data;
  }
  
  public final CreditLineResponse copy(CreditLineDetails paramCreditLineDetails)
  {
    k.b(paramCreditLineDetails, "data");
    CreditLineResponse localCreditLineResponse = new com/truecaller/credit/data/models/CreditLineResponse;
    localCreditLineResponse.<init>(paramCreditLineDetails);
    return localCreditLineResponse;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof CreditLineResponse;
      if (bool1)
      {
        paramObject = (CreditLineResponse)paramObject;
        CreditLineDetails localCreditLineDetails = data;
        paramObject = data;
        boolean bool2 = k.a(localCreditLineDetails, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String errorMessage()
  {
    BaseApiResponse.Meta localMeta = getMeta();
    if (localMeta != null) {
      return localMeta.getMessage();
    }
    return null;
  }
  
  public final CreditLineDetails getData()
  {
    return data;
  }
  
  public final int hashCode()
  {
    CreditLineDetails localCreditLineDetails = data;
    if (localCreditLineDetails != null) {
      return localCreditLineDetails.hashCode();
    }
    return 0;
  }
  
  public final boolean isValid()
  {
    return m.a(getStatus(), "success", true);
  }
  
  public final CreditLineDetails mapToData()
  {
    CreditLineDetails localCreditLineDetails = new com/truecaller/credit/domain/interactors/withdrawloan/models/CreditLineDetails;
    String str1 = data.getTotal_amount();
    String str2 = data.getAvailable_amount();
    String str3 = data.getMinimum_amount();
    String str4 = data.getTitle();
    localCreditLineDetails.<init>(str1, str2, str3, str4);
    return localCreditLineDetails;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("CreditLineResponse(data=");
    CreditLineDetails localCreditLineDetails = data;
    localStringBuilder.append(localCreditLineDetails);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.CreditLineResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */