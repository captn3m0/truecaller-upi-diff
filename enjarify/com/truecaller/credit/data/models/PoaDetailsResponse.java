package com.truecaller.credit.data.models;

import c.g.b.k;
import c.n.m;
import com.truecaller.credit.domain.interactors.infocollection.models.PoaDetails;

public final class PoaDetailsResponse
  extends BaseApiResponse
  implements Mappable
{
  private final PoaDetailsData data;
  
  public PoaDetailsResponse(PoaDetailsData paramPoaDetailsData)
  {
    data = paramPoaDetailsData;
  }
  
  public final PoaDetailsData component1()
  {
    return data;
  }
  
  public final PoaDetailsResponse copy(PoaDetailsData paramPoaDetailsData)
  {
    k.b(paramPoaDetailsData, "data");
    PoaDetailsResponse localPoaDetailsResponse = new com/truecaller/credit/data/models/PoaDetailsResponse;
    localPoaDetailsResponse.<init>(paramPoaDetailsData);
    return localPoaDetailsResponse;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof PoaDetailsResponse;
      if (bool1)
      {
        paramObject = (PoaDetailsResponse)paramObject;
        PoaDetailsData localPoaDetailsData = data;
        paramObject = data;
        boolean bool2 = k.a(localPoaDetailsData, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String errorMessage()
  {
    BaseApiResponse.Meta localMeta = getMeta();
    if (localMeta != null) {
      return localMeta.getMessage();
    }
    return null;
  }
  
  public final PoaDetailsData getData()
  {
    return data;
  }
  
  public final int hashCode()
  {
    PoaDetailsData localPoaDetailsData = data;
    if (localPoaDetailsData != null) {
      return localPoaDetailsData.hashCode();
    }
    return 0;
  }
  
  public final boolean isValid()
  {
    return m.a(getStatus(), "success", true);
  }
  
  public final PoaDetails mapToData()
  {
    PoaDetails localPoaDetails = new com/truecaller/credit/domain/interactors/infocollection/models/PoaDetails;
    String str1 = data.getUser().getName();
    String str2 = data.getProof_of_address();
    String str3 = data.getProof_of_address_text();
    localPoaDetails.<init>(str1, str2, str3);
    return localPoaDetails;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("PoaDetailsResponse(data=");
    PoaDetailsData localPoaDetailsData = data;
    localStringBuilder.append(localPoaDetailsData);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.PoaDetailsResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */