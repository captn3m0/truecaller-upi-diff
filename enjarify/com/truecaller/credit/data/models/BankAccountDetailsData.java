package com.truecaller.credit.data.models;

import c.g.b.k;

public final class BankAccountDetailsData
{
  private final BankAccountDetails account;
  
  public BankAccountDetailsData(BankAccountDetails paramBankAccountDetails)
  {
    account = paramBankAccountDetails;
  }
  
  public final BankAccountDetails component1()
  {
    return account;
  }
  
  public final BankAccountDetailsData copy(BankAccountDetails paramBankAccountDetails)
  {
    k.b(paramBankAccountDetails, "account");
    BankAccountDetailsData localBankAccountDetailsData = new com/truecaller/credit/data/models/BankAccountDetailsData;
    localBankAccountDetailsData.<init>(paramBankAccountDetails);
    return localBankAccountDetailsData;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof BankAccountDetailsData;
      if (bool1)
      {
        paramObject = (BankAccountDetailsData)paramObject;
        BankAccountDetails localBankAccountDetails = account;
        paramObject = account;
        boolean bool2 = k.a(localBankAccountDetails, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final BankAccountDetails getAccount()
  {
    return account;
  }
  
  public final int hashCode()
  {
    BankAccountDetails localBankAccountDetails = account;
    if (localBankAccountDetails != null) {
      return localBankAccountDetails.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("BankAccountDetailsData(account=");
    BankAccountDetails localBankAccountDetails = account;
    localStringBuilder.append(localBankAccountDetails);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.BankAccountDetailsData
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */