package com.truecaller.credit.data.models;

import c.g.b.k;

public final class VerifyPanResponse
{
  private final String gender;
  private final String name;
  private final String number;
  
  public VerifyPanResponse(String paramString1, String paramString2, String paramString3)
  {
    number = paramString1;
    name = paramString2;
    gender = paramString3;
  }
  
  public final String component1()
  {
    return number;
  }
  
  public final String component2()
  {
    return name;
  }
  
  public final String component3()
  {
    return gender;
  }
  
  public final VerifyPanResponse copy(String paramString1, String paramString2, String paramString3)
  {
    k.b(paramString1, "number");
    k.b(paramString2, "name");
    k.b(paramString3, "gender");
    VerifyPanResponse localVerifyPanResponse = new com/truecaller/credit/data/models/VerifyPanResponse;
    localVerifyPanResponse.<init>(paramString1, paramString2, paramString3);
    return localVerifyPanResponse;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof VerifyPanResponse;
      if (bool1)
      {
        paramObject = (VerifyPanResponse)paramObject;
        String str1 = number;
        String str2 = number;
        bool1 = k.a(str1, str2);
        if (bool1)
        {
          str1 = name;
          str2 = name;
          bool1 = k.a(str1, str2);
          if (bool1)
          {
            str1 = gender;
            paramObject = gender;
            boolean bool2 = k.a(str1, paramObject);
            if (bool2) {
              break label90;
            }
          }
        }
      }
      return false;
    }
    label90:
    return true;
  }
  
  public final String getGender()
  {
    return gender;
  }
  
  public final String getName()
  {
    return name;
  }
  
  public final String getNumber()
  {
    return number;
  }
  
  public final int hashCode()
  {
    String str1 = number;
    int i = 0;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = name;
    int k;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    int j = (j + k) * 31;
    str2 = gender;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("VerifyPanResponse(number=");
    String str = number;
    localStringBuilder.append(str);
    localStringBuilder.append(", name=");
    str = name;
    localStringBuilder.append(str);
    localStringBuilder.append(", gender=");
    str = gender;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.VerifyPanResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */