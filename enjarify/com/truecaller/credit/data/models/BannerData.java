package com.truecaller.credit.data.models;

import c.g.b.k;

public final class BannerData
{
  private final String app_state;
  private final Banner banner;
  
  public BannerData(String paramString, Banner paramBanner)
  {
    app_state = paramString;
    banner = paramBanner;
  }
  
  public final String component1()
  {
    return app_state;
  }
  
  public final Banner component2()
  {
    return banner;
  }
  
  public final BannerData copy(String paramString, Banner paramBanner)
  {
    BannerData localBannerData = new com/truecaller/credit/data/models/BannerData;
    localBannerData.<init>(paramString, paramBanner);
    return localBannerData;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof BannerData;
      if (bool1)
      {
        paramObject = (BannerData)paramObject;
        Object localObject = app_state;
        String str = app_state;
        bool1 = k.a(localObject, str);
        if (bool1)
        {
          localObject = banner;
          paramObject = banner;
          boolean bool2 = k.a(localObject, paramObject);
          if (bool2) {
            break label68;
          }
        }
      }
      return false;
    }
    label68:
    return true;
  }
  
  public final String getApp_state()
  {
    return app_state;
  }
  
  public final Banner getBanner()
  {
    return banner;
  }
  
  public final int hashCode()
  {
    String str = app_state;
    int i = 0;
    int j;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    j *= 31;
    Banner localBanner = banner;
    if (localBanner != null) {
      i = localBanner.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("BannerData(app_state=");
    Object localObject = app_state;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", banner=");
    localObject = banner;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.BannerData
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */