package com.truecaller.credit.data.models;

import c.g.b.k;
import c.n.m;
import com.truecaller.credit.domain.interactors.infocollection.models.IFSCList;
import java.util.List;

public final class IFSCSearchResponse
  extends BaseApiResponse
  implements Mappable
{
  private final IFSCSearchsData data;
  
  public IFSCSearchResponse(IFSCSearchsData paramIFSCSearchsData)
  {
    data = paramIFSCSearchsData;
  }
  
  public final IFSCSearchsData component1()
  {
    return data;
  }
  
  public final IFSCSearchResponse copy(IFSCSearchsData paramIFSCSearchsData)
  {
    k.b(paramIFSCSearchsData, "data");
    IFSCSearchResponse localIFSCSearchResponse = new com/truecaller/credit/data/models/IFSCSearchResponse;
    localIFSCSearchResponse.<init>(paramIFSCSearchsData);
    return localIFSCSearchResponse;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof IFSCSearchResponse;
      if (bool1)
      {
        paramObject = (IFSCSearchResponse)paramObject;
        IFSCSearchsData localIFSCSearchsData = data;
        paramObject = data;
        boolean bool2 = k.a(localIFSCSearchsData, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String errorMessage()
  {
    BaseApiResponse.Meta localMeta = getMeta();
    if (localMeta != null) {
      return localMeta.getMessage();
    }
    return null;
  }
  
  public final IFSCSearchsData getData()
  {
    return data;
  }
  
  public final int hashCode()
  {
    IFSCSearchsData localIFSCSearchsData = data;
    if (localIFSCSearchsData != null) {
      return localIFSCSearchsData.hashCode();
    }
    return 0;
  }
  
  public final boolean isValid()
  {
    return m.a(getStatus(), "success", true);
  }
  
  public final IFSCList mapToData()
  {
    IFSCList localIFSCList = new com/truecaller/credit/domain/interactors/infocollection/models/IFSCList;
    List localList = data.getResults();
    localIFSCList.<init>(localList);
    return localIFSCList;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("IFSCSearchResponse(data=");
    IFSCSearchsData localIFSCSearchsData = data;
    localStringBuilder.append(localIFSCSearchsData);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.IFSCSearchResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */