package com.truecaller.credit.data.models;

import c.g.b.k;

public final class BankDetailsResult
{
  private final String message;
  
  public BankDetailsResult(String paramString)
  {
    message = paramString;
  }
  
  public final String component1()
  {
    return message;
  }
  
  public final BankDetailsResult copy(String paramString)
  {
    k.b(paramString, "message");
    BankDetailsResult localBankDetailsResult = new com/truecaller/credit/data/models/BankDetailsResult;
    localBankDetailsResult.<init>(paramString);
    return localBankDetailsResult;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof BankDetailsResult;
      if (bool1)
      {
        paramObject = (BankDetailsResult)paramObject;
        String str = message;
        paramObject = message;
        boolean bool2 = k.a(str, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String getMessage()
  {
    return message;
  }
  
  public final int hashCode()
  {
    String str = message;
    if (str != null) {
      return str.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("BankDetailsResult(message=");
    String str = message;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.BankDetailsResult
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */