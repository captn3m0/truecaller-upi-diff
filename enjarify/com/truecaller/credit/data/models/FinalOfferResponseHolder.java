package com.truecaller.credit.data.models;

import c.g.b.k;
import c.n.m;
import com.truecaller.credit.domain.interactors.onboarding.models.a;

public final class FinalOfferResponseHolder
  extends BaseApiResponse
  implements Mappable
{
  private final FinalOfferResponse data;
  
  public FinalOfferResponseHolder(FinalOfferResponse paramFinalOfferResponse)
  {
    data = paramFinalOfferResponse;
  }
  
  public final FinalOfferResponse component1()
  {
    return data;
  }
  
  public final FinalOfferResponseHolder copy(FinalOfferResponse paramFinalOfferResponse)
  {
    k.b(paramFinalOfferResponse, "data");
    FinalOfferResponseHolder localFinalOfferResponseHolder = new com/truecaller/credit/data/models/FinalOfferResponseHolder;
    localFinalOfferResponseHolder.<init>(paramFinalOfferResponse);
    return localFinalOfferResponseHolder;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof FinalOfferResponseHolder;
      if (bool1)
      {
        paramObject = (FinalOfferResponseHolder)paramObject;
        FinalOfferResponse localFinalOfferResponse = data;
        paramObject = data;
        boolean bool2 = k.a(localFinalOfferResponse, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String errorMessage()
  {
    BaseApiResponse.Meta localMeta = getMeta();
    if (localMeta != null) {
      return localMeta.getMessage();
    }
    return null;
  }
  
  public final FinalOfferResponse getData()
  {
    return data;
  }
  
  public final int hashCode()
  {
    FinalOfferResponse localFinalOfferResponse = data;
    if (localFinalOfferResponse != null) {
      return localFinalOfferResponse.hashCode();
    }
    return 0;
  }
  
  public final boolean isValid()
  {
    Object localObject = getStatus();
    String str = "success";
    int i = 1;
    boolean bool = m.a((String)localObject, str, i);
    if (bool)
    {
      localObject = data;
      int j = ((FinalOfferResponse)localObject).getFinal_offer();
      if (j > i) {
        return i;
      }
    }
    return false;
  }
  
  public final a mapToData()
  {
    a locala = new com/truecaller/credit/domain/interactors/onboarding/models/a;
    int i = data.getFinal_offer();
    String str1 = data.getDescription().getText1();
    String str2 = data.getDescription().getText2();
    String str3 = data.getDescription().getTerms();
    locala.<init>(i, str1, str2, str3);
    return locala;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("FinalOfferResponseHolder(data=");
    FinalOfferResponse localFinalOfferResponse = data;
    localStringBuilder.append(localFinalOfferResponse);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.FinalOfferResponseHolder
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */