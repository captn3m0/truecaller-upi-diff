package com.truecaller.credit.data.models;

import c.g.b.k;
import c.n.m;
import com.truecaller.credit.domain.interactors.loanhistory.models.EmiHistory;
import java.util.List;

public final class EmiHistoryResponse
  extends BaseApiResponse
  implements Mappable
{
  private final EmiHistory data;
  
  public EmiHistoryResponse(EmiHistory paramEmiHistory)
  {
    data = paramEmiHistory;
  }
  
  public final EmiHistory component1()
  {
    return data;
  }
  
  public final EmiHistoryResponse copy(EmiHistory paramEmiHistory)
  {
    k.b(paramEmiHistory, "data");
    EmiHistoryResponse localEmiHistoryResponse = new com/truecaller/credit/data/models/EmiHistoryResponse;
    localEmiHistoryResponse.<init>(paramEmiHistory);
    return localEmiHistoryResponse;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof EmiHistoryResponse;
      if (bool1)
      {
        paramObject = (EmiHistoryResponse)paramObject;
        EmiHistory localEmiHistory = data;
        paramObject = data;
        boolean bool2 = k.a(localEmiHistory, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String errorMessage()
  {
    BaseApiResponse.Meta localMeta = getMeta();
    if (localMeta != null) {
      return localMeta.getMessage();
    }
    return null;
  }
  
  public final EmiHistory getData()
  {
    return data;
  }
  
  public final int hashCode()
  {
    EmiHistory localEmiHistory = data;
    if (localEmiHistory != null) {
      return localEmiHistory.hashCode();
    }
    return 0;
  }
  
  public final boolean isValid()
  {
    return m.a(getStatus(), "success", true);
  }
  
  public final EmiHistory mapToData()
  {
    EmiHistory localEmiHistory = new com/truecaller/credit/domain/interactors/loanhistory/models/EmiHistory;
    List localList = data.getEmis();
    localEmiHistory.<init>(localList);
    return localEmiHistory;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("EmiHistoryResponse(data=");
    EmiHistory localEmiHistory = data;
    localStringBuilder.append(localEmiHistory);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.EmiHistoryResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */