package com.truecaller.credit.data.models;

import c.g.b.k;
import c.n.m;

public final class ScheduleSlotResponse
  extends BaseApiResponse
  implements Mappable
{
  private final ScheduleSlotResponse.GetSlotResponseData data;
  
  public ScheduleSlotResponse(ScheduleSlotResponse.GetSlotResponseData paramGetSlotResponseData)
  {
    data = paramGetSlotResponseData;
  }
  
  public final ScheduleSlotResponse.GetSlotResponseData component1()
  {
    return data;
  }
  
  public final ScheduleSlotResponse copy(ScheduleSlotResponse.GetSlotResponseData paramGetSlotResponseData)
  {
    k.b(paramGetSlotResponseData, "data");
    ScheduleSlotResponse localScheduleSlotResponse = new com/truecaller/credit/data/models/ScheduleSlotResponse;
    localScheduleSlotResponse.<init>(paramGetSlotResponseData);
    return localScheduleSlotResponse;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof ScheduleSlotResponse;
      if (bool1)
      {
        paramObject = (ScheduleSlotResponse)paramObject;
        ScheduleSlotResponse.GetSlotResponseData localGetSlotResponseData = data;
        paramObject = data;
        boolean bool2 = k.a(localGetSlotResponseData, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String errorMessage()
  {
    BaseApiResponse.Meta localMeta = getMeta();
    if (localMeta != null) {
      return localMeta.getMessage();
    }
    return null;
  }
  
  public final ScheduleSlotResponse.GetSlotResponseData getData()
  {
    return data;
  }
  
  public final int hashCode()
  {
    ScheduleSlotResponse.GetSlotResponseData localGetSlotResponseData = data;
    if (localGetSlotResponseData != null) {
      return localGetSlotResponseData.hashCode();
    }
    return 0;
  }
  
  public final boolean isValid()
  {
    return m.a(getStatus(), "success", true);
  }
  
  public final String mapToData()
  {
    return data.getMessage();
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("ScheduleSlotResponse(data=");
    ScheduleSlotResponse.GetSlotResponseData localGetSlotResponseData = data;
    localStringBuilder.append(localGetSlotResponseData);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.ScheduleSlotResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */