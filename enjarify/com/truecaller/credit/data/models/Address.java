package com.truecaller.credit.data.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class Address
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  private String address_line_1;
  private String address_line_2;
  private String address_line_3;
  private String address_type;
  private String city;
  private String pincode;
  private String state;
  
  static
  {
    Address.Creator localCreator = new com/truecaller/credit/data/models/Address$Creator;
    localCreator.<init>();
    CREATOR = localCreator;
  }
  
  public Address()
  {
    this(null, null, null, null, null, null, null, 127, null);
  }
  
  public Address(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7)
  {
    address_type = paramString1;
    address_line_1 = paramString2;
    address_line_2 = paramString3;
    address_line_3 = paramString4;
    city = paramString5;
    pincode = paramString6;
    state = paramString7;
  }
  
  public final String component1()
  {
    return address_type;
  }
  
  public final String component2()
  {
    return address_line_1;
  }
  
  public final String component3()
  {
    return address_line_2;
  }
  
  public final String component4()
  {
    return address_line_3;
  }
  
  public final String component5()
  {
    return city;
  }
  
  public final String component6()
  {
    return pincode;
  }
  
  public final String component7()
  {
    return state;
  }
  
  public final Address copy(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7)
  {
    k.b(paramString1, "address_type");
    k.b(paramString2, "address_line_1");
    k.b(paramString3, "address_line_2");
    k.b(paramString4, "address_line_3");
    k.b(paramString5, "city");
    k.b(paramString6, "pincode");
    k.b(paramString7, "state");
    Address localAddress = new com/truecaller/credit/data/models/Address;
    localAddress.<init>(paramString1, paramString2, paramString3, paramString4, paramString5, paramString6, paramString7);
    return localAddress;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof Address;
      if (bool1)
      {
        paramObject = (Address)paramObject;
        String str1 = address_type;
        String str2 = address_type;
        bool1 = k.a(str1, str2);
        if (bool1)
        {
          str1 = address_line_1;
          str2 = address_line_1;
          bool1 = k.a(str1, str2);
          if (bool1)
          {
            str1 = address_line_2;
            str2 = address_line_2;
            bool1 = k.a(str1, str2);
            if (bool1)
            {
              str1 = address_line_3;
              str2 = address_line_3;
              bool1 = k.a(str1, str2);
              if (bool1)
              {
                str1 = city;
                str2 = city;
                bool1 = k.a(str1, str2);
                if (bool1)
                {
                  str1 = pincode;
                  str2 = pincode;
                  bool1 = k.a(str1, str2);
                  if (bool1)
                  {
                    str1 = state;
                    paramObject = state;
                    boolean bool2 = k.a(str1, paramObject);
                    if (bool2) {
                      break label178;
                    }
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    label178:
    return true;
  }
  
  public final String getAddress_line_1()
  {
    return address_line_1;
  }
  
  public final String getAddress_line_2()
  {
    return address_line_2;
  }
  
  public final String getAddress_line_3()
  {
    return address_line_3;
  }
  
  public final String getAddress_type()
  {
    return address_type;
  }
  
  public final String getCity()
  {
    return city;
  }
  
  public final String getPincode()
  {
    return pincode;
  }
  
  public final String getState()
  {
    return state;
  }
  
  public final int hashCode()
  {
    String str1 = address_type;
    int i = 0;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = address_line_1;
    int k;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    int j = (j + k) * 31;
    str2 = address_line_2;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    j = (j + k) * 31;
    str2 = address_line_3;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    j = (j + k) * 31;
    str2 = city;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    j = (j + k) * 31;
    str2 = pincode;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    j = (j + k) * 31;
    str2 = state;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final void setAddress_line_1(String paramString)
  {
    k.b(paramString, "<set-?>");
    address_line_1 = paramString;
  }
  
  public final void setAddress_line_2(String paramString)
  {
    k.b(paramString, "<set-?>");
    address_line_2 = paramString;
  }
  
  public final void setAddress_line_3(String paramString)
  {
    k.b(paramString, "<set-?>");
    address_line_3 = paramString;
  }
  
  public final void setAddress_type(String paramString)
  {
    k.b(paramString, "<set-?>");
    address_type = paramString;
  }
  
  public final void setCity(String paramString)
  {
    k.b(paramString, "<set-?>");
    city = paramString;
  }
  
  public final void setPincode(String paramString)
  {
    k.b(paramString, "<set-?>");
    pincode = paramString;
  }
  
  public final void setState(String paramString)
  {
    k.b(paramString, "<set-?>");
    state = paramString;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Address(address_type=");
    String str = address_type;
    localStringBuilder.append(str);
    localStringBuilder.append(", address_line_1=");
    str = address_line_1;
    localStringBuilder.append(str);
    localStringBuilder.append(", address_line_2=");
    str = address_line_2;
    localStringBuilder.append(str);
    localStringBuilder.append(", address_line_3=");
    str = address_line_3;
    localStringBuilder.append(str);
    localStringBuilder.append(", city=");
    str = city;
    localStringBuilder.append(str);
    localStringBuilder.append(", pincode=");
    str = pincode;
    localStringBuilder.append(str);
    localStringBuilder.append(", state=");
    str = state;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    String str = address_type;
    paramParcel.writeString(str);
    str = address_line_1;
    paramParcel.writeString(str);
    str = address_line_2;
    paramParcel.writeString(str);
    str = address_line_3;
    paramParcel.writeString(str);
    str = city;
    paramParcel.writeString(str);
    str = pincode;
    paramParcel.writeString(str);
    str = state;
    paramParcel.writeString(str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.Address
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */