package com.truecaller.credit.data.models;

import c.g.b.k;

public final class VerifyFinalOfferOtpRequest
{
  private final String otp;
  
  public VerifyFinalOfferOtpRequest(String paramString)
  {
    otp = paramString;
  }
  
  public final String component1()
  {
    return otp;
  }
  
  public final VerifyFinalOfferOtpRequest copy(String paramString)
  {
    k.b(paramString, "otp");
    VerifyFinalOfferOtpRequest localVerifyFinalOfferOtpRequest = new com/truecaller/credit/data/models/VerifyFinalOfferOtpRequest;
    localVerifyFinalOfferOtpRequest.<init>(paramString);
    return localVerifyFinalOfferOtpRequest;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof VerifyFinalOfferOtpRequest;
      if (bool1)
      {
        paramObject = (VerifyFinalOfferOtpRequest)paramObject;
        String str = otp;
        paramObject = otp;
        boolean bool2 = k.a(str, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String getOtp()
  {
    return otp;
  }
  
  public final int hashCode()
  {
    String str = otp;
    if (str != null) {
      return str.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("VerifyFinalOfferOtpRequest(otp=");
    String str = otp;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.VerifyFinalOfferOtpRequest
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */