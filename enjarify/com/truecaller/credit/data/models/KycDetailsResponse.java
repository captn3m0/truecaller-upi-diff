package com.truecaller.credit.data.models;

import c.g.b.k;
import c.n.m;
import com.truecaller.credit.domain.interactors.infocollection.models.KycDetails;

public final class KycDetailsResponse
  extends BaseApiResponse
  implements Mappable
{
  private final KycDetailsData data;
  
  public KycDetailsResponse(KycDetailsData paramKycDetailsData)
  {
    data = paramKycDetailsData;
  }
  
  public final KycDetailsData component1()
  {
    return data;
  }
  
  public final KycDetailsResponse copy(KycDetailsData paramKycDetailsData)
  {
    k.b(paramKycDetailsData, "data");
    KycDetailsResponse localKycDetailsResponse = new com/truecaller/credit/data/models/KycDetailsResponse;
    localKycDetailsResponse.<init>(paramKycDetailsData);
    return localKycDetailsResponse;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof KycDetailsResponse;
      if (bool1)
      {
        paramObject = (KycDetailsResponse)paramObject;
        KycDetailsData localKycDetailsData = data;
        paramObject = data;
        boolean bool2 = k.a(localKycDetailsData, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String errorMessage()
  {
    BaseApiResponse.Meta localMeta = getMeta();
    if (localMeta != null) {
      return localMeta.getMessage();
    }
    return null;
  }
  
  public final KycDetailsData getData()
  {
    return data;
  }
  
  public final int hashCode()
  {
    KycDetailsData localKycDetailsData = data;
    if (localKycDetailsData != null) {
      return localKycDetailsData.hashCode();
    }
    return 0;
  }
  
  public final boolean isValid()
  {
    return m.a(getStatus(), "success", true);
  }
  
  public final KycDetails mapToData()
  {
    KycDetails localKycDetails1 = new com/truecaller/credit/domain/interactors/infocollection/models/KycDetails;
    Object localObject = data.getUser();
    String str = null;
    if (localObject != null) {
      localObject = ((KycDetails)localObject).getId();
    } else {
      localObject = null;
    }
    KycDetails localKycDetails2 = data.getUser();
    if (localKycDetails2 != null) {
      str = localKycDetails2.getType();
    }
    localKycDetails1.<init>((String)localObject, str);
    return localKycDetails1;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("KycDetailsResponse(data=");
    KycDetailsData localKycDetailsData = data;
    localStringBuilder.append(localKycDetailsData);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.KycDetailsResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */