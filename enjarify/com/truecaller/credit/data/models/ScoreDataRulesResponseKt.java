package com.truecaller.credit.data.models;

import c.g.b.k;
import com.google.gson.f;

public final class ScoreDataRulesResponseKt
{
  public static final String toJson(ScoreDataRules paramScoreDataRules)
  {
    k.b(paramScoreDataRules, "receiver$0");
    f localf = new com/google/gson/f;
    localf.<init>();
    paramScoreDataRules = localf.b(paramScoreDataRules);
    k.a(paramScoreDataRules, "Gson().toJson(this)");
    return paramScoreDataRules;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.ScoreDataRulesResponseKt
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */