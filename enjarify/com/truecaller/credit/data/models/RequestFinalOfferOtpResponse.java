package com.truecaller.credit.data.models;

import c.g.b.k;

public final class RequestFinalOfferOtpResponse
{
  private final String mobile_number;
  
  public RequestFinalOfferOtpResponse(String paramString)
  {
    mobile_number = paramString;
  }
  
  public final String component1()
  {
    return mobile_number;
  }
  
  public final RequestFinalOfferOtpResponse copy(String paramString)
  {
    RequestFinalOfferOtpResponse localRequestFinalOfferOtpResponse = new com/truecaller/credit/data/models/RequestFinalOfferOtpResponse;
    localRequestFinalOfferOtpResponse.<init>(paramString);
    return localRequestFinalOfferOtpResponse;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof RequestFinalOfferOtpResponse;
      if (bool1)
      {
        paramObject = (RequestFinalOfferOtpResponse)paramObject;
        String str = mobile_number;
        paramObject = mobile_number;
        boolean bool2 = k.a(str, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String getMobile_number()
  {
    return mobile_number;
  }
  
  public final int hashCode()
  {
    String str = mobile_number;
    if (str != null) {
      return str.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("RequestFinalOfferOtpResponse(mobile_number=");
    String str = mobile_number;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.RequestFinalOfferOtpResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */