package com.truecaller.credit.data.models;

import c.g.b.k;

public final class Appointment
{
  private final String date;
  private final String id;
  private final String slot;
  
  public Appointment(String paramString1, String paramString2, String paramString3)
  {
    id = paramString1;
    date = paramString2;
    slot = paramString3;
  }
  
  public final String component1()
  {
    return id;
  }
  
  public final String component2()
  {
    return date;
  }
  
  public final String component3()
  {
    return slot;
  }
  
  public final Appointment copy(String paramString1, String paramString2, String paramString3)
  {
    k.b(paramString1, "id");
    k.b(paramString2, "date");
    k.b(paramString3, "slot");
    Appointment localAppointment = new com/truecaller/credit/data/models/Appointment;
    localAppointment.<init>(paramString1, paramString2, paramString3);
    return localAppointment;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof Appointment;
      if (bool1)
      {
        paramObject = (Appointment)paramObject;
        String str1 = id;
        String str2 = id;
        bool1 = k.a(str1, str2);
        if (bool1)
        {
          str1 = date;
          str2 = date;
          bool1 = k.a(str1, str2);
          if (bool1)
          {
            str1 = slot;
            paramObject = slot;
            boolean bool2 = k.a(str1, paramObject);
            if (bool2) {
              break label90;
            }
          }
        }
      }
      return false;
    }
    label90:
    return true;
  }
  
  public final String getDate()
  {
    return date;
  }
  
  public final String getId()
  {
    return id;
  }
  
  public final String getSlot()
  {
    return slot;
  }
  
  public final int hashCode()
  {
    String str1 = id;
    int i = 0;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = date;
    int k;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    int j = (j + k) * 31;
    str2 = slot;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Appointment(id=");
    String str = id;
    localStringBuilder.append(str);
    localStringBuilder.append(", date=");
    str = date;
    localStringBuilder.append(str);
    localStringBuilder.append(", slot=");
    str = slot;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.Appointment
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */