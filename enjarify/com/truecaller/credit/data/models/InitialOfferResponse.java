package com.truecaller.credit.data.models;

import c.g.b.k;
import c.n.m;

public final class InitialOfferResponse
  extends BaseApiResponse
  implements Mappable
{
  private final InitialOfferResponse.InitialOfferData data;
  
  public InitialOfferResponse(InitialOfferResponse.InitialOfferData paramInitialOfferData)
  {
    data = paramInitialOfferData;
  }
  
  public final InitialOfferResponse.InitialOfferData component1()
  {
    return data;
  }
  
  public final InitialOfferResponse copy(InitialOfferResponse.InitialOfferData paramInitialOfferData)
  {
    k.b(paramInitialOfferData, "data");
    InitialOfferResponse localInitialOfferResponse = new com/truecaller/credit/data/models/InitialOfferResponse;
    localInitialOfferResponse.<init>(paramInitialOfferData);
    return localInitialOfferResponse;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof InitialOfferResponse;
      if (bool1)
      {
        paramObject = (InitialOfferResponse)paramObject;
        InitialOfferResponse.InitialOfferData localInitialOfferData = data;
        paramObject = data;
        boolean bool2 = k.a(localInitialOfferData, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String errorMessage()
  {
    BaseApiResponse.Meta localMeta = getMeta();
    if (localMeta != null) {
      return localMeta.getMessage();
    }
    return null;
  }
  
  public final InitialOfferResponse.InitialOfferData getData()
  {
    return data;
  }
  
  public final int hashCode()
  {
    InitialOfferResponse.InitialOfferData localInitialOfferData = data;
    if (localInitialOfferData != null) {
      return localInitialOfferData.hashCode();
    }
    return 0;
  }
  
  public final boolean isValid()
  {
    return m.a(getStatus(), "success", true);
  }
  
  public final InitialOfferResponse.InitialOfferData mapToData()
  {
    InitialOfferResponse.InitialOfferData localInitialOfferData = new com/truecaller/credit/data/models/InitialOfferResponse$InitialOfferData;
    int i = data.getInitial_offer();
    localInitialOfferData.<init>(i);
    return localInitialOfferData;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("InitialOfferResponse(data=");
    InitialOfferResponse.InitialOfferData localInitialOfferData = data;
    localStringBuilder.append(localInitialOfferData);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.InitialOfferResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */