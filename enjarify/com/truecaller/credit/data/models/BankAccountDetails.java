package com.truecaller.credit.data.models;

public final class BankAccountDetails
{
  private final String last_4_digit;
  private final String logo;
  private final String name;
  
  public BankAccountDetails(String paramString1, String paramString2, String paramString3)
  {
    last_4_digit = paramString1;
    name = paramString2;
    logo = paramString3;
  }
  
  public final String getLast_4_digit()
  {
    return last_4_digit;
  }
  
  public final String getLogo()
  {
    return logo;
  }
  
  public final String getName()
  {
    return name;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.BankAccountDetails
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */