package com.truecaller.credit.data.models;

import c.g.b.k;

public final class FinalOfferResponse
{
  private final FinalOfferDescription description;
  private final int final_offer;
  
  public FinalOfferResponse(int paramInt, FinalOfferDescription paramFinalOfferDescription)
  {
    final_offer = paramInt;
    description = paramFinalOfferDescription;
  }
  
  public final int component1()
  {
    return final_offer;
  }
  
  public final FinalOfferDescription component2()
  {
    return description;
  }
  
  public final FinalOfferResponse copy(int paramInt, FinalOfferDescription paramFinalOfferDescription)
  {
    k.b(paramFinalOfferDescription, "description");
    FinalOfferResponse localFinalOfferResponse = new com/truecaller/credit/data/models/FinalOfferResponse;
    localFinalOfferResponse.<init>(paramInt, paramFinalOfferDescription);
    return localFinalOfferResponse;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof FinalOfferResponse;
      if (bool2)
      {
        paramObject = (FinalOfferResponse)paramObject;
        int i = final_offer;
        int j = final_offer;
        FinalOfferDescription localFinalOfferDescription;
        if (i == j)
        {
          i = 1;
        }
        else
        {
          i = 0;
          localFinalOfferDescription = null;
        }
        if (i != 0)
        {
          localFinalOfferDescription = description;
          paramObject = description;
          boolean bool3 = k.a(localFinalOfferDescription, paramObject);
          if (bool3) {
            return bool1;
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final FinalOfferDescription getDescription()
  {
    return description;
  }
  
  public final int getFinal_offer()
  {
    return final_offer;
  }
  
  public final int hashCode()
  {
    int i = final_offer * 31;
    FinalOfferDescription localFinalOfferDescription = description;
    int j;
    if (localFinalOfferDescription != null)
    {
      j = localFinalOfferDescription.hashCode();
    }
    else
    {
      j = 0;
      localFinalOfferDescription = null;
    }
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("FinalOfferResponse(final_offer=");
    int i = final_offer;
    localStringBuilder.append(i);
    localStringBuilder.append(", description=");
    FinalOfferDescription localFinalOfferDescription = description;
    localStringBuilder.append(localFinalOfferDescription);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.FinalOfferResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */