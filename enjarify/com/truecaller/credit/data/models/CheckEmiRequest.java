package com.truecaller.credit.data.models;

import c.g.b.k;

public final class CheckEmiRequest
{
  private final String loan_amount;
  
  public CheckEmiRequest(String paramString)
  {
    loan_amount = paramString;
  }
  
  public final String component1()
  {
    return loan_amount;
  }
  
  public final CheckEmiRequest copy(String paramString)
  {
    k.b(paramString, "loan_amount");
    CheckEmiRequest localCheckEmiRequest = new com/truecaller/credit/data/models/CheckEmiRequest;
    localCheckEmiRequest.<init>(paramString);
    return localCheckEmiRequest;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof CheckEmiRequest;
      if (bool1)
      {
        paramObject = (CheckEmiRequest)paramObject;
        String str = loan_amount;
        paramObject = loan_amount;
        boolean bool2 = k.a(str, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String getLoan_amount()
  {
    return loan_amount;
  }
  
  public final int hashCode()
  {
    String str = loan_amount;
    if (str != null) {
      return str.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("CheckEmiRequest(loan_amount=");
    String str = loan_amount;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.CheckEmiRequest
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */