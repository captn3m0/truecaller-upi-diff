package com.truecaller.credit.data.models;

import c.g.b.k;

public final class EmiHistoryRequest
{
  private final String loan_id;
  
  public EmiHistoryRequest(String paramString)
  {
    loan_id = paramString;
  }
  
  public final String component1()
  {
    return loan_id;
  }
  
  public final EmiHistoryRequest copy(String paramString)
  {
    k.b(paramString, "loan_id");
    EmiHistoryRequest localEmiHistoryRequest = new com/truecaller/credit/data/models/EmiHistoryRequest;
    localEmiHistoryRequest.<init>(paramString);
    return localEmiHistoryRequest;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof EmiHistoryRequest;
      if (bool1)
      {
        paramObject = (EmiHistoryRequest)paramObject;
        String str = loan_id;
        paramObject = loan_id;
        boolean bool2 = k.a(str, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String getLoan_id()
  {
    return loan_id;
  }
  
  public final int hashCode()
  {
    String str = loan_id;
    if (str != null) {
      return str.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("EmiHistoryRequest(loan_id=");
    String str = loan_id;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.EmiHistoryRequest
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */