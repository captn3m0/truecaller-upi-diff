package com.truecaller.credit.data.models;

import c.n.m;
import com.truecaller.credit.domain.interactors.onboarding.models.ScoreDataResult;

public final class ScoreDataUploadResponse
  extends BaseApiResponse
  implements Mappable
{
  public final String errorMessage()
  {
    BaseApiResponse.Meta localMeta = getMeta();
    if (localMeta != null) {
      return localMeta.getMessage();
    }
    return null;
  }
  
  public final boolean isValid()
  {
    return m.a(getStatus(), "success", true);
  }
  
  public final ScoreDataResult mapToData()
  {
    ScoreDataResult localScoreDataResult = new com/truecaller/credit/domain/interactors/onboarding/models/ScoreDataResult;
    String str = getStatus();
    localScoreDataResult.<init>(str);
    return localScoreDataResult;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.ScoreDataUploadResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */