package com.truecaller.credit.data.models;

import c.n.m;

public final class RequestFinalOfferOtpResponseHolder
  extends BaseApiResponse
  implements Mappable
{
  private final RequestFinalOfferOtpResponse data;
  
  public RequestFinalOfferOtpResponseHolder(RequestFinalOfferOtpResponse paramRequestFinalOfferOtpResponse)
  {
    data = paramRequestFinalOfferOtpResponse;
  }
  
  public final String errorMessage()
  {
    BaseApiResponse.Meta localMeta = getMeta();
    if (localMeta != null) {
      return localMeta.getMessage();
    }
    return null;
  }
  
  public final RequestFinalOfferOtpResponse getData()
  {
    return data;
  }
  
  public final boolean isValid()
  {
    return m.a(getStatus(), "success", true);
  }
  
  public final RequestFinalOfferOtpResult mapToData()
  {
    RequestFinalOfferOtpResult localRequestFinalOfferOtpResult = new com/truecaller/credit/data/models/RequestFinalOfferOtpResult;
    String str = data.getMobile_number();
    localRequestFinalOfferOtpResult.<init>(str);
    return localRequestFinalOfferOtpResult;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.RequestFinalOfferOtpResponseHolder
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */