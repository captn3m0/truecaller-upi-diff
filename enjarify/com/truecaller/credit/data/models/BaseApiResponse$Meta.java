package com.truecaller.credit.data.models;

import c.g.b.k;

public final class BaseApiResponse$Meta
{
  private final String message;
  private final String status_code;
  private final String type;
  
  public BaseApiResponse$Meta(String paramString1, String paramString2, String paramString3)
  {
    status_code = paramString1;
    type = paramString2;
    message = paramString3;
  }
  
  public final String component1()
  {
    return status_code;
  }
  
  public final String component2()
  {
    return type;
  }
  
  public final String component3()
  {
    return message;
  }
  
  public final Meta copy(String paramString1, String paramString2, String paramString3)
  {
    Meta localMeta = new com/truecaller/credit/data/models/BaseApiResponse$Meta;
    localMeta.<init>(paramString1, paramString2, paramString3);
    return localMeta;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof Meta;
      if (bool1)
      {
        paramObject = (Meta)paramObject;
        String str1 = status_code;
        String str2 = status_code;
        bool1 = k.a(str1, str2);
        if (bool1)
        {
          str1 = type;
          str2 = type;
          bool1 = k.a(str1, str2);
          if (bool1)
          {
            str1 = message;
            paramObject = message;
            boolean bool2 = k.a(str1, paramObject);
            if (bool2) {
              break label90;
            }
          }
        }
      }
      return false;
    }
    label90:
    return true;
  }
  
  public final String getMessage()
  {
    return message;
  }
  
  public final String getStatus_code()
  {
    return status_code;
  }
  
  public final String getType()
  {
    return type;
  }
  
  public final int hashCode()
  {
    String str1 = status_code;
    int i = 0;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = type;
    int k;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    int j = (j + k) * 31;
    str2 = message;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Meta(status_code=");
    String str = status_code;
    localStringBuilder.append(str);
    localStringBuilder.append(", type=");
    str = type;
    localStringBuilder.append(str);
    localStringBuilder.append(", message=");
    str = message;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.BaseApiResponse.Meta
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */