package com.truecaller.credit.data.models;

import c.g.b.k;

public final class VerifyPanRequest
{
  private final String pan;
  
  public VerifyPanRequest(String paramString)
  {
    pan = paramString;
  }
  
  public final String component1()
  {
    return pan;
  }
  
  public final VerifyPanRequest copy(String paramString)
  {
    k.b(paramString, "pan");
    VerifyPanRequest localVerifyPanRequest = new com/truecaller/credit/data/models/VerifyPanRequest;
    localVerifyPanRequest.<init>(paramString);
    return localVerifyPanRequest;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof VerifyPanRequest;
      if (bool1)
      {
        paramObject = (VerifyPanRequest)paramObject;
        String str = pan;
        paramObject = pan;
        boolean bool2 = k.a(str, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String getPan()
  {
    return pan;
  }
  
  public final int hashCode()
  {
    String str = pan;
    if (str != null) {
      return str.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("VerifyPanRequest(pan=");
    String str = pan;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.VerifyPanRequest
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */