package com.truecaller.credit.data.models;

import c.g.b.k;

public final class PoaTypeRequest
{
  private final String type;
  
  public PoaTypeRequest(String paramString)
  {
    type = paramString;
  }
  
  public final String component1()
  {
    return type;
  }
  
  public final PoaTypeRequest copy(String paramString)
  {
    k.b(paramString, "type");
    PoaTypeRequest localPoaTypeRequest = new com/truecaller/credit/data/models/PoaTypeRequest;
    localPoaTypeRequest.<init>(paramString);
    return localPoaTypeRequest;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof PoaTypeRequest;
      if (bool1)
      {
        paramObject = (PoaTypeRequest)paramObject;
        String str = type;
        paramObject = type;
        boolean bool2 = k.a(str, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String getType()
  {
    return type;
  }
  
  public final int hashCode()
  {
    String str = type;
    if (str != null) {
      return str.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("PoaTypeRequest(type=");
    String str = type;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.PoaTypeRequest
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */