package com.truecaller.credit.data.models;

import c.g.b.k;
import c.n.m;
import com.truecaller.credit.domain.interactors.loanhistory.models.LoanHistory;
import java.util.List;

public final class LoanHistoryResponse
  extends BaseApiResponse
  implements Mappable
{
  private final LoanHistory data;
  
  public LoanHistoryResponse(LoanHistory paramLoanHistory)
  {
    data = paramLoanHistory;
  }
  
  public final LoanHistory component1()
  {
    return data;
  }
  
  public final LoanHistoryResponse copy(LoanHistory paramLoanHistory)
  {
    k.b(paramLoanHistory, "data");
    LoanHistoryResponse localLoanHistoryResponse = new com/truecaller/credit/data/models/LoanHistoryResponse;
    localLoanHistoryResponse.<init>(paramLoanHistory);
    return localLoanHistoryResponse;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof LoanHistoryResponse;
      if (bool1)
      {
        paramObject = (LoanHistoryResponse)paramObject;
        LoanHistory localLoanHistory = data;
        paramObject = data;
        boolean bool2 = k.a(localLoanHistory, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String errorMessage()
  {
    BaseApiResponse.Meta localMeta = getMeta();
    if (localMeta != null) {
      return localMeta.getMessage();
    }
    return null;
  }
  
  public final LoanHistory getData()
  {
    return data;
  }
  
  public final int hashCode()
  {
    LoanHistory localLoanHistory = data;
    if (localLoanHistory != null) {
      return localLoanHistory.hashCode();
    }
    return 0;
  }
  
  public final boolean isValid()
  {
    return m.a(getStatus(), "success", true);
  }
  
  public final LoanHistory mapToData()
  {
    LoanHistory localLoanHistory = new com/truecaller/credit/domain/interactors/loanhistory/models/LoanHistory;
    List localList = data.getLoans();
    localLoanHistory.<init>(localList);
    return localLoanHistory;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("LoanHistoryResponse(data=");
    LoanHistory localLoanHistory = data;
    localStringBuilder.append(localLoanHistory);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.LoanHistoryResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */