package com.truecaller.credit.data.models;

public final class InitialOfferResponse$InitialOfferData
{
  private final int initial_offer;
  
  public InitialOfferResponse$InitialOfferData(int paramInt)
  {
    initial_offer = paramInt;
  }
  
  public final int component1()
  {
    return initial_offer;
  }
  
  public final InitialOfferData copy(int paramInt)
  {
    InitialOfferData localInitialOfferData = new com/truecaller/credit/data/models/InitialOfferResponse$InitialOfferData;
    localInitialOfferData.<init>(paramInt);
    return localInitialOfferData;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof InitialOfferData;
      if (bool2)
      {
        paramObject = (InitialOfferData)paramObject;
        int i = initial_offer;
        int j = initial_offer;
        if (i == j)
        {
          j = 1;
        }
        else
        {
          j = 0;
          paramObject = null;
        }
        if (j != 0) {}
      }
      else
      {
        return false;
      }
    }
    return bool1;
  }
  
  public final int getInitial_offer()
  {
    return initial_offer;
  }
  
  public final int hashCode()
  {
    return initial_offer;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("InitialOfferData(initial_offer=");
    int i = initial_offer;
    localStringBuilder.append(i);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.InitialOfferResponse.InitialOfferData
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */