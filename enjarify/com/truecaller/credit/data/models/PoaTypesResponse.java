package com.truecaller.credit.data.models;

import c.g.b.k;
import c.n.m;
import com.truecaller.credit.domain.interactors.infocollection.models.PoaTypes;
import java.util.List;

public final class PoaTypesResponse
  extends BaseApiResponse
  implements Mappable
{
  private final PoaTypesData data;
  
  public PoaTypesResponse(PoaTypesData paramPoaTypesData)
  {
    data = paramPoaTypesData;
  }
  
  public final PoaTypesData component1()
  {
    return data;
  }
  
  public final PoaTypesResponse copy(PoaTypesData paramPoaTypesData)
  {
    k.b(paramPoaTypesData, "data");
    PoaTypesResponse localPoaTypesResponse = new com/truecaller/credit/data/models/PoaTypesResponse;
    localPoaTypesResponse.<init>(paramPoaTypesData);
    return localPoaTypesResponse;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof PoaTypesResponse;
      if (bool1)
      {
        paramObject = (PoaTypesResponse)paramObject;
        PoaTypesData localPoaTypesData = data;
        paramObject = data;
        boolean bool2 = k.a(localPoaTypesData, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String errorMessage()
  {
    BaseApiResponse.Meta localMeta = getMeta();
    if (localMeta != null) {
      return localMeta.getMessage();
    }
    return null;
  }
  
  public final PoaTypesData getData()
  {
    return data;
  }
  
  public final int hashCode()
  {
    PoaTypesData localPoaTypesData = data;
    if (localPoaTypesData != null) {
      return localPoaTypesData.hashCode();
    }
    return 0;
  }
  
  public final boolean isValid()
  {
    return m.a(getStatus(), "success", true);
  }
  
  public final PoaTypes mapToData()
  {
    PoaTypes localPoaTypes = new com/truecaller/credit/domain/interactors/infocollection/models/PoaTypes;
    List localList = data.getKyc_types();
    localPoaTypes.<init>(localList);
    return localPoaTypes;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("PoaTypesResponse(data=");
    PoaTypesData localPoaTypesData = data;
    localStringBuilder.append(localPoaTypesData);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.PoaTypesResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */