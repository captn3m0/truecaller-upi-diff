package com.truecaller.credit.data.models;

import c.g.b.k;

public final class AddAddressRequest
{
  private final Address address;
  
  public AddAddressRequest(Address paramAddress)
  {
    address = paramAddress;
  }
  
  public final Address component1()
  {
    return address;
  }
  
  public final AddAddressRequest copy(Address paramAddress)
  {
    k.b(paramAddress, "address");
    AddAddressRequest localAddAddressRequest = new com/truecaller/credit/data/models/AddAddressRequest;
    localAddAddressRequest.<init>(paramAddress);
    return localAddAddressRequest;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof AddAddressRequest;
      if (bool1)
      {
        paramObject = (AddAddressRequest)paramObject;
        Address localAddress = address;
        paramObject = address;
        boolean bool2 = k.a(localAddress, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final Address getAddress()
  {
    return address;
  }
  
  public final int hashCode()
  {
    Address localAddress = address;
    if (localAddress != null) {
      return localAddress.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("AddAddressRequest(address=");
    Address localAddress = address;
    localStringBuilder.append(localAddress);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.AddAddressRequest
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */