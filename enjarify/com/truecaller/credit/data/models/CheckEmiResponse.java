package com.truecaller.credit.data.models;

import c.g.b.k;
import c.n.m;
import com.truecaller.credit.domain.interactors.withdrawloan.models.EmiTypes;
import java.util.List;

public final class CheckEmiResponse
  extends BaseApiResponse
  implements Mappable
{
  private final EmiTypes data;
  
  public CheckEmiResponse(EmiTypes paramEmiTypes)
  {
    data = paramEmiTypes;
  }
  
  public final EmiTypes component1()
  {
    return data;
  }
  
  public final CheckEmiResponse copy(EmiTypes paramEmiTypes)
  {
    k.b(paramEmiTypes, "data");
    CheckEmiResponse localCheckEmiResponse = new com/truecaller/credit/data/models/CheckEmiResponse;
    localCheckEmiResponse.<init>(paramEmiTypes);
    return localCheckEmiResponse;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof CheckEmiResponse;
      if (bool1)
      {
        paramObject = (CheckEmiResponse)paramObject;
        EmiTypes localEmiTypes = data;
        paramObject = data;
        boolean bool2 = k.a(localEmiTypes, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String errorMessage()
  {
    BaseApiResponse.Meta localMeta = getMeta();
    if (localMeta != null) {
      return localMeta.getMessage();
    }
    return null;
  }
  
  public final EmiTypes getData()
  {
    return data;
  }
  
  public final int hashCode()
  {
    EmiTypes localEmiTypes = data;
    if (localEmiTypes != null) {
      return localEmiTypes.hashCode();
    }
    return 0;
  }
  
  public final boolean isValid()
  {
    return m.a(getStatus(), "success", true);
  }
  
  public final EmiTypes mapToData()
  {
    EmiTypes localEmiTypes = new com/truecaller/credit/domain/interactors/withdrawloan/models/EmiTypes;
    List localList = data.getEmis();
    String str1 = data.getInterest_rate();
    String str2 = data.getDescription();
    localEmiTypes.<init>(localList, str1, str2);
    return localEmiTypes;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("CheckEmiResponse(data=");
    EmiTypes localEmiTypes = data;
    localStringBuilder.append(localEmiTypes);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.CheckEmiResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */