package com.truecaller.credit.data.models;

import c.g.b.k;

public final class ScheduleSlotRequest
{
  private final String date;
  private final String slot;
  
  public ScheduleSlotRequest(String paramString1, String paramString2)
  {
    date = paramString1;
    slot = paramString2;
  }
  
  public final String component1()
  {
    return date;
  }
  
  public final String component2()
  {
    return slot;
  }
  
  public final ScheduleSlotRequest copy(String paramString1, String paramString2)
  {
    k.b(paramString1, "date");
    k.b(paramString2, "slot");
    ScheduleSlotRequest localScheduleSlotRequest = new com/truecaller/credit/data/models/ScheduleSlotRequest;
    localScheduleSlotRequest.<init>(paramString1, paramString2);
    return localScheduleSlotRequest;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof ScheduleSlotRequest;
      if (bool1)
      {
        paramObject = (ScheduleSlotRequest)paramObject;
        String str1 = date;
        String str2 = date;
        bool1 = k.a(str1, str2);
        if (bool1)
        {
          str1 = slot;
          paramObject = slot;
          boolean bool2 = k.a(str1, paramObject);
          if (bool2) {
            break label68;
          }
        }
      }
      return false;
    }
    label68:
    return true;
  }
  
  public final String getDate()
  {
    return date;
  }
  
  public final String getSlot()
  {
    return slot;
  }
  
  public final int hashCode()
  {
    String str1 = date;
    int i = 0;
    int j;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = slot;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("ScheduleSlotRequest(date=");
    String str = date;
    localStringBuilder.append(str);
    localStringBuilder.append(", slot=");
    str = slot;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.ScheduleSlotRequest
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */