package com.truecaller.credit.data.models;

import c.g.b.k;

public final class PreScoreDataUploadResponse$PreScoreData
{
  private final String app_state;
  private final Integer initial_offer_score;
  
  public PreScoreDataUploadResponse$PreScoreData(Integer paramInteger, String paramString)
  {
    initial_offer_score = paramInteger;
    app_state = paramString;
  }
  
  public final Integer component1()
  {
    return initial_offer_score;
  }
  
  public final String component2()
  {
    return app_state;
  }
  
  public final PreScoreData copy(Integer paramInteger, String paramString)
  {
    PreScoreData localPreScoreData = new com/truecaller/credit/data/models/PreScoreDataUploadResponse$PreScoreData;
    localPreScoreData.<init>(paramInteger, paramString);
    return localPreScoreData;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof PreScoreData;
      if (bool1)
      {
        paramObject = (PreScoreData)paramObject;
        Object localObject = initial_offer_score;
        Integer localInteger = initial_offer_score;
        bool1 = k.a(localObject, localInteger);
        if (bool1)
        {
          localObject = app_state;
          paramObject = app_state;
          boolean bool2 = k.a(localObject, paramObject);
          if (bool2) {
            break label68;
          }
        }
      }
      return false;
    }
    label68:
    return true;
  }
  
  public final String getApp_state()
  {
    return app_state;
  }
  
  public final Integer getInitial_offer_score()
  {
    return initial_offer_score;
  }
  
  public final int hashCode()
  {
    Integer localInteger = initial_offer_score;
    int i = 0;
    int j;
    if (localInteger != null)
    {
      j = localInteger.hashCode();
    }
    else
    {
      j = 0;
      localInteger = null;
    }
    j *= 31;
    String str = app_state;
    if (str != null) {
      i = str.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("PreScoreData(initial_offer_score=");
    Object localObject = initial_offer_score;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", app_state=");
    localObject = app_state;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.PreScoreDataUploadResponse.PreScoreData
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */