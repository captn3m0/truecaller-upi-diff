package com.truecaller.credit.data.models;

import c.g.b.k;
import java.util.List;

public final class SupportedCitiesResponse
{
  private final List locations;
  
  public SupportedCitiesResponse(List paramList)
  {
    locations = paramList;
  }
  
  public final List component1()
  {
    return locations;
  }
  
  public final SupportedCitiesResponse copy(List paramList)
  {
    k.b(paramList, "locations");
    SupportedCitiesResponse localSupportedCitiesResponse = new com/truecaller/credit/data/models/SupportedCitiesResponse;
    localSupportedCitiesResponse.<init>(paramList);
    return localSupportedCitiesResponse;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof SupportedCitiesResponse;
      if (bool1)
      {
        paramObject = (SupportedCitiesResponse)paramObject;
        List localList = locations;
        paramObject = locations;
        boolean bool2 = k.a(localList, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final List getLocations()
  {
    return locations;
  }
  
  public final int hashCode()
  {
    List localList = locations;
    if (localList != null) {
      return localList.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("SupportedCitiesResponse(locations=");
    List localList = locations;
    localStringBuilder.append(localList);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.SupportedCitiesResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */