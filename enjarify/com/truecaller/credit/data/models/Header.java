package com.truecaller.credit.data.models;

import c.g.b.k;

public final class Header
{
  private final String left_text;
  private final String right_text;
  
  public Header(String paramString1, String paramString2)
  {
    left_text = paramString1;
    right_text = paramString2;
  }
  
  public final String component1()
  {
    return left_text;
  }
  
  public final String component2()
  {
    return right_text;
  }
  
  public final Header copy(String paramString1, String paramString2)
  {
    Header localHeader = new com/truecaller/credit/data/models/Header;
    localHeader.<init>(paramString1, paramString2);
    return localHeader;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof Header;
      if (bool1)
      {
        paramObject = (Header)paramObject;
        String str1 = left_text;
        String str2 = left_text;
        bool1 = k.a(str1, str2);
        if (bool1)
        {
          str1 = right_text;
          paramObject = right_text;
          boolean bool2 = k.a(str1, paramObject);
          if (bool2) {
            break label68;
          }
        }
      }
      return false;
    }
    label68:
    return true;
  }
  
  public final String getLeft_text()
  {
    return left_text;
  }
  
  public final String getRight_text()
  {
    return right_text;
  }
  
  public final int hashCode()
  {
    String str1 = left_text;
    int i = 0;
    int j;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = right_text;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Header(left_text=");
    String str = left_text;
    localStringBuilder.append(str);
    localStringBuilder.append(", right_text=");
    str = right_text;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.Header
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */