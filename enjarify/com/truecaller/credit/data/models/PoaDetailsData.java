package com.truecaller.credit.data.models;

import c.g.b.k;
import com.truecaller.credit.domain.interactors.infocollection.models.PoaDetails;

public final class PoaDetailsData
{
  private final String proof_of_address;
  private final String proof_of_address_text;
  private final PoaDetails user;
  
  public PoaDetailsData(PoaDetails paramPoaDetails, String paramString1, String paramString2)
  {
    user = paramPoaDetails;
    proof_of_address = paramString1;
    proof_of_address_text = paramString2;
  }
  
  public final PoaDetails component1()
  {
    return user;
  }
  
  public final String component2()
  {
    return proof_of_address;
  }
  
  public final String component3()
  {
    return proof_of_address_text;
  }
  
  public final PoaDetailsData copy(PoaDetails paramPoaDetails, String paramString1, String paramString2)
  {
    k.b(paramPoaDetails, "user");
    k.b(paramString1, "proof_of_address");
    k.b(paramString2, "proof_of_address_text");
    PoaDetailsData localPoaDetailsData = new com/truecaller/credit/data/models/PoaDetailsData;
    localPoaDetailsData.<init>(paramPoaDetails, paramString1, paramString2);
    return localPoaDetailsData;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof PoaDetailsData;
      if (bool1)
      {
        paramObject = (PoaDetailsData)paramObject;
        Object localObject1 = user;
        Object localObject2 = user;
        bool1 = k.a(localObject1, localObject2);
        if (bool1)
        {
          localObject1 = proof_of_address;
          localObject2 = proof_of_address;
          bool1 = k.a(localObject1, localObject2);
          if (bool1)
          {
            localObject1 = proof_of_address_text;
            paramObject = proof_of_address_text;
            boolean bool2 = k.a(localObject1, paramObject);
            if (bool2) {
              break label90;
            }
          }
        }
      }
      return false;
    }
    label90:
    return true;
  }
  
  public final String getProof_of_address()
  {
    return proof_of_address;
  }
  
  public final String getProof_of_address_text()
  {
    return proof_of_address_text;
  }
  
  public final PoaDetails getUser()
  {
    return user;
  }
  
  public final int hashCode()
  {
    PoaDetails localPoaDetails = user;
    int i = 0;
    if (localPoaDetails != null)
    {
      j = localPoaDetails.hashCode();
    }
    else
    {
      j = 0;
      localPoaDetails = null;
    }
    j *= 31;
    String str = proof_of_address;
    int k;
    if (str != null)
    {
      k = str.hashCode();
    }
    else
    {
      k = 0;
      str = null;
    }
    int j = (j + k) * 31;
    str = proof_of_address_text;
    if (str != null) {
      i = str.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("PoaDetailsData(user=");
    Object localObject = user;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", proof_of_address=");
    localObject = proof_of_address;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", proof_of_address_text=");
    localObject = proof_of_address_text;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.PoaDetailsData
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */