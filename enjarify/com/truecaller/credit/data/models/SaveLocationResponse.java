package com.truecaller.credit.data.models;

import c.n.m;

public final class SaveLocationResponse
  extends BaseApiResponse
  implements Mappable
{
  public final String errorMessage()
  {
    BaseApiResponse.Meta localMeta = getMeta();
    if (localMeta != null) {
      return localMeta.getMessage();
    }
    return null;
  }
  
  public final boolean isValid()
  {
    return m.a(getStatus(), "success", true);
  }
  
  public final SaveLocation mapToData()
  {
    SaveLocation localSaveLocation = new com/truecaller/credit/data/models/SaveLocation;
    Object localObject = getMeta();
    if (localObject != null)
    {
      localObject = ((BaseApiResponse.Meta)localObject).getMessage();
      if (localObject != null) {}
    }
    else
    {
      localObject = "Success";
    }
    localSaveLocation.<init>((String)localObject);
    return localSaveLocation;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.SaveLocationResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */