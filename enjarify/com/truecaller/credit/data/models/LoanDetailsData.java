package com.truecaller.credit.data.models;

import c.g.b.k;
import com.truecaller.credit.domain.interactors.withdrawloan.models.LoanDetails;

public final class LoanDetailsData
{
  private final LoanDetails loan;
  
  public LoanDetailsData(LoanDetails paramLoanDetails)
  {
    loan = paramLoanDetails;
  }
  
  public final LoanDetails component1()
  {
    return loan;
  }
  
  public final LoanDetailsData copy(LoanDetails paramLoanDetails)
  {
    k.b(paramLoanDetails, "loan");
    LoanDetailsData localLoanDetailsData = new com/truecaller/credit/data/models/LoanDetailsData;
    localLoanDetailsData.<init>(paramLoanDetails);
    return localLoanDetailsData;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof LoanDetailsData;
      if (bool1)
      {
        paramObject = (LoanDetailsData)paramObject;
        LoanDetails localLoanDetails = loan;
        paramObject = loan;
        boolean bool2 = k.a(localLoanDetails, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final LoanDetails getLoan()
  {
    return loan;
  }
  
  public final int hashCode()
  {
    LoanDetails localLoanDetails = loan;
    if (localLoanDetails != null) {
      return localLoanDetails.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("LoanDetailsData(loan=");
    LoanDetails localLoanDetails = loan;
    localStringBuilder.append(localLoanDetails);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.LoanDetailsData
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */