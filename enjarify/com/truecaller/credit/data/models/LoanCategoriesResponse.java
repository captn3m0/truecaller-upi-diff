package com.truecaller.credit.data.models;

import c.g.b.k;
import c.n.m;
import com.truecaller.credit.domain.interactors.withdrawloan.models.LoanCategories;
import java.util.List;

public final class LoanCategoriesResponse
  extends BaseApiResponse
  implements Mappable
{
  private final LoanCategories data;
  
  public LoanCategoriesResponse(LoanCategories paramLoanCategories)
  {
    data = paramLoanCategories;
  }
  
  public final LoanCategories component1()
  {
    return data;
  }
  
  public final LoanCategoriesResponse copy(LoanCategories paramLoanCategories)
  {
    k.b(paramLoanCategories, "data");
    LoanCategoriesResponse localLoanCategoriesResponse = new com/truecaller/credit/data/models/LoanCategoriesResponse;
    localLoanCategoriesResponse.<init>(paramLoanCategories);
    return localLoanCategoriesResponse;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof LoanCategoriesResponse;
      if (bool1)
      {
        paramObject = (LoanCategoriesResponse)paramObject;
        LoanCategories localLoanCategories = data;
        paramObject = data;
        boolean bool2 = k.a(localLoanCategories, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String errorMessage()
  {
    BaseApiResponse.Meta localMeta = getMeta();
    if (localMeta != null) {
      return localMeta.getMessage();
    }
    return null;
  }
  
  public final LoanCategories getData()
  {
    return data;
  }
  
  public final int hashCode()
  {
    LoanCategories localLoanCategories = data;
    if (localLoanCategories != null) {
      return localLoanCategories.hashCode();
    }
    return 0;
  }
  
  public final boolean isValid()
  {
    return m.a(getStatus(), "success", true);
  }
  
  public final LoanCategories mapToData()
  {
    LoanCategories localLoanCategories = new com/truecaller/credit/domain/interactors/withdrawloan/models/LoanCategories;
    List localList = data.getCategories();
    localLoanCategories.<init>(localList);
    return localLoanCategories;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("LoanCategoriesResponse(data=");
    LoanCategories localLoanCategories = data;
    localStringBuilder.append(localLoanCategories);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.LoanCategoriesResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */