package com.truecaller.credit.data.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class APIStatusMessage$Creator
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    APIStatusMessage localAPIStatusMessage = new com/truecaller/credit/data/models/APIStatusMessage;
    int i = paramParcel.readInt();
    String str1 = paramParcel.readString();
    String str2 = paramParcel.readString();
    int j = paramParcel.readInt();
    boolean bool;
    if (j != 0)
    {
      j = 1;
      bool = true;
    }
    else
    {
      j = 0;
      bool = false;
    }
    String str3 = paramParcel.readString();
    localAPIStatusMessage.<init>(i, str1, str2, bool, str3);
    return localAPIStatusMessage;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new APIStatusMessage[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.APIStatusMessage.Creator
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */