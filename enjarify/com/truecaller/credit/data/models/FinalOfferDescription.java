package com.truecaller.credit.data.models;

import c.g.b.k;

public final class FinalOfferDescription
{
  private final String terms;
  private final String text1;
  private final String text2;
  
  public FinalOfferDescription(String paramString1, String paramString2, String paramString3)
  {
    text1 = paramString1;
    text2 = paramString2;
    terms = paramString3;
  }
  
  public final String component1()
  {
    return text1;
  }
  
  public final String component2()
  {
    return text2;
  }
  
  public final String component3()
  {
    return terms;
  }
  
  public final FinalOfferDescription copy(String paramString1, String paramString2, String paramString3)
  {
    k.b(paramString1, "text1");
    k.b(paramString2, "text2");
    k.b(paramString3, "terms");
    FinalOfferDescription localFinalOfferDescription = new com/truecaller/credit/data/models/FinalOfferDescription;
    localFinalOfferDescription.<init>(paramString1, paramString2, paramString3);
    return localFinalOfferDescription;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof FinalOfferDescription;
      if (bool1)
      {
        paramObject = (FinalOfferDescription)paramObject;
        String str1 = text1;
        String str2 = text1;
        bool1 = k.a(str1, str2);
        if (bool1)
        {
          str1 = text2;
          str2 = text2;
          bool1 = k.a(str1, str2);
          if (bool1)
          {
            str1 = terms;
            paramObject = terms;
            boolean bool2 = k.a(str1, paramObject);
            if (bool2) {
              break label90;
            }
          }
        }
      }
      return false;
    }
    label90:
    return true;
  }
  
  public final String getTerms()
  {
    return terms;
  }
  
  public final String getText1()
  {
    return text1;
  }
  
  public final String getText2()
  {
    return text2;
  }
  
  public final int hashCode()
  {
    String str1 = text1;
    int i = 0;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = text2;
    int k;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    int j = (j + k) * 31;
    str2 = terms;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("FinalOfferDescription(text1=");
    String str = text1;
    localStringBuilder.append(str);
    localStringBuilder.append(", text2=");
    str = text2;
    localStringBuilder.append(str);
    localStringBuilder.append(", terms=");
    str = terms;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.FinalOfferDescription
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */