package com.truecaller.credit.data.models;

import c.g.b.k;

public final class FetchAppointmentResponse$GetAppointmentResponseData
{
  private final Appointment appointment;
  
  public FetchAppointmentResponse$GetAppointmentResponseData(Appointment paramAppointment)
  {
    appointment = paramAppointment;
  }
  
  public final Appointment component1()
  {
    return appointment;
  }
  
  public final GetAppointmentResponseData copy(Appointment paramAppointment)
  {
    k.b(paramAppointment, "appointment");
    GetAppointmentResponseData localGetAppointmentResponseData = new com/truecaller/credit/data/models/FetchAppointmentResponse$GetAppointmentResponseData;
    localGetAppointmentResponseData.<init>(paramAppointment);
    return localGetAppointmentResponseData;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof GetAppointmentResponseData;
      if (bool1)
      {
        paramObject = (GetAppointmentResponseData)paramObject;
        Appointment localAppointment = appointment;
        paramObject = appointment;
        boolean bool2 = k.a(localAppointment, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final Appointment getAppointment()
  {
    return appointment;
  }
  
  public final int hashCode()
  {
    Appointment localAppointment = appointment;
    if (localAppointment != null) {
      return localAppointment.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("GetAppointmentResponseData(appointment=");
    Appointment localAppointment = appointment;
    localStringBuilder.append(localAppointment);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.FetchAppointmentResponse.GetAppointmentResponseData
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */