package com.truecaller.credit.data.models;

import c.n.m;

public final class DocumentUploadResponse
  extends BaseApiResponse
  implements Mappable
{
  private final DocumentUploadResponse.DocumentUploadedData data;
  
  public DocumentUploadResponse(DocumentUploadResponse.DocumentUploadedData paramDocumentUploadedData)
  {
    data = paramDocumentUploadedData;
  }
  
  public final String errorMessage()
  {
    BaseApiResponse.Meta localMeta = getMeta();
    if (localMeta != null) {
      return localMeta.getMessage();
    }
    return null;
  }
  
  public final DocumentUploadResponse.DocumentUploadedData getData()
  {
    return data;
  }
  
  public final boolean isValid()
  {
    return m.a(getStatus(), "success", true);
  }
  
  public final DocumentUploadResponse.Document mapToData()
  {
    DocumentUploadResponse.Document localDocument = new com/truecaller/credit/data/models/DocumentUploadResponse$Document;
    Object localObject1 = data;
    String str = null;
    if (localObject1 != null)
    {
      localObject1 = ((DocumentUploadResponse.DocumentUploadedData)localObject1).getDocument();
      if (localObject1 != null)
      {
        localObject1 = ((DocumentUploadResponse.Document)localObject1).getId();
        break label34;
      }
    }
    localObject1 = null;
    label34:
    Object localObject2 = data;
    if (localObject2 != null)
    {
      localObject2 = ((DocumentUploadResponse.DocumentUploadedData)localObject2).getDocument();
      if (localObject2 != null)
      {
        localObject2 = ((DocumentUploadResponse.Document)localObject2).getType();
        break label70;
      }
    }
    localObject2 = null;
    label70:
    Object localObject3 = data;
    if (localObject3 != null)
    {
      localObject3 = ((DocumentUploadResponse.DocumentUploadedData)localObject3).getDocument();
      if (localObject3 != null) {
        str = ((DocumentUploadResponse.Document)localObject3).getData();
      }
    }
    localDocument.<init>((String)localObject1, (String)localObject2, str);
    return localDocument;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.DocumentUploadResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */