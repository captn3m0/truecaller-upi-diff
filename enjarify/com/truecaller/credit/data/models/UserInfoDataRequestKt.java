package com.truecaller.credit.data.models;

import c.g.b.k;

public final class UserInfoDataRequestKt
{
  public static final String ADDRESS_TYPE_CURRENT = "current";
  public static final String ADDRESS_TYPE_KYC = "kyc";
  public static final String ADDRESS_TYPE_PERMANENT = "permanent";
  
  public static final String flattenToString(Address paramAddress)
  {
    k.b(paramAddress, "receiver$0");
    Object localObject = (CharSequence)paramAddress.getAddress_line_3();
    int i = ((CharSequence)localObject).length();
    if (i > 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject = null;
    }
    if (i != 0)
    {
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      str = paramAddress.getAddress_line_1();
      ((StringBuilder)localObject).append(str);
      ((StringBuilder)localObject).append(", ");
      str = paramAddress.getAddress_line_2();
      ((StringBuilder)localObject).append(str);
      ((StringBuilder)localObject).append(", ");
      str = paramAddress.getAddress_line_3();
      ((StringBuilder)localObject).append(str);
      ((StringBuilder)localObject).append(", ");
      str = paramAddress.getCity();
      ((StringBuilder)localObject).append(str);
      ((StringBuilder)localObject).append(", ");
      str = paramAddress.getState();
      ((StringBuilder)localObject).append(str);
      ((StringBuilder)localObject).append(" - ");
      paramAddress = paramAddress.getPincode();
      ((StringBuilder)localObject).append(paramAddress);
      return ((StringBuilder)localObject).toString();
    }
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    String str = paramAddress.getAddress_line_1();
    ((StringBuilder)localObject).append(str);
    ((StringBuilder)localObject).append(", ");
    str = paramAddress.getAddress_line_2();
    ((StringBuilder)localObject).append(str);
    ((StringBuilder)localObject).append(", ");
    str = paramAddress.getCity();
    ((StringBuilder)localObject).append(str);
    ((StringBuilder)localObject).append(", ");
    str = paramAddress.getState();
    ((StringBuilder)localObject).append(str);
    ((StringBuilder)localObject).append(" - ");
    paramAddress = paramAddress.getPincode();
    ((StringBuilder)localObject).append(paramAddress);
    return ((StringBuilder)localObject).toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.UserInfoDataRequestKt
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */