package com.truecaller.credit.data.models;

import c.g.b.k;

public final class Banner
{
  private final Button button;
  private final Header header;
  private final Image image;
  private final String subtitle;
  private final String title;
  private final Integer type;
  
  public Banner(Button paramButton, Header paramHeader, Image paramImage, String paramString1, String paramString2, Integer paramInteger)
  {
    button = paramButton;
    header = paramHeader;
    image = paramImage;
    subtitle = paramString1;
    title = paramString2;
    type = paramInteger;
  }
  
  public final Button component1()
  {
    return button;
  }
  
  public final Header component2()
  {
    return header;
  }
  
  public final Image component3()
  {
    return image;
  }
  
  public final String component4()
  {
    return subtitle;
  }
  
  public final String component5()
  {
    return title;
  }
  
  public final Integer component6()
  {
    return type;
  }
  
  public final Banner copy(Button paramButton, Header paramHeader, Image paramImage, String paramString1, String paramString2, Integer paramInteger)
  {
    Banner localBanner = new com/truecaller/credit/data/models/Banner;
    localBanner.<init>(paramButton, paramHeader, paramImage, paramString1, paramString2, paramInteger);
    return localBanner;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof Banner;
      if (bool1)
      {
        paramObject = (Banner)paramObject;
        Object localObject1 = button;
        Object localObject2 = button;
        bool1 = k.a(localObject1, localObject2);
        if (bool1)
        {
          localObject1 = header;
          localObject2 = header;
          bool1 = k.a(localObject1, localObject2);
          if (bool1)
          {
            localObject1 = image;
            localObject2 = image;
            bool1 = k.a(localObject1, localObject2);
            if (bool1)
            {
              localObject1 = subtitle;
              localObject2 = subtitle;
              bool1 = k.a(localObject1, localObject2);
              if (bool1)
              {
                localObject1 = title;
                localObject2 = title;
                bool1 = k.a(localObject1, localObject2);
                if (bool1)
                {
                  localObject1 = type;
                  paramObject = type;
                  boolean bool2 = k.a(localObject1, paramObject);
                  if (bool2) {
                    break label156;
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    label156:
    return true;
  }
  
  public final Button getButton()
  {
    return button;
  }
  
  public final Header getHeader()
  {
    return header;
  }
  
  public final Image getImage()
  {
    return image;
  }
  
  public final String getSubtitle()
  {
    return subtitle;
  }
  
  public final String getTitle()
  {
    return title;
  }
  
  public final Integer getType()
  {
    return type;
  }
  
  public final int hashCode()
  {
    Button localButton = button;
    int i = 0;
    if (localButton != null)
    {
      j = localButton.hashCode();
    }
    else
    {
      j = 0;
      localButton = null;
    }
    j *= 31;
    Object localObject = header;
    int k;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    int j = (j + k) * 31;
    localObject = image;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = subtitle;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = title;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = type;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Banner(button=");
    Object localObject = button;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", header=");
    localObject = header;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", image=");
    localObject = image;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", subtitle=");
    localObject = subtitle;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", title=");
    localObject = title;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", type=");
    localObject = type;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.Banner
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */