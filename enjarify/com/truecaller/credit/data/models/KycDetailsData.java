package com.truecaller.credit.data.models;

import c.g.b.k;
import com.truecaller.credit.domain.interactors.infocollection.models.KycDetails;

public final class KycDetailsData
{
  private final KycDetails user;
  
  public KycDetailsData(KycDetails paramKycDetails)
  {
    user = paramKycDetails;
  }
  
  public final KycDetails component1()
  {
    return user;
  }
  
  public final KycDetailsData copy(KycDetails paramKycDetails)
  {
    KycDetailsData localKycDetailsData = new com/truecaller/credit/data/models/KycDetailsData;
    localKycDetailsData.<init>(paramKycDetails);
    return localKycDetailsData;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof KycDetailsData;
      if (bool1)
      {
        paramObject = (KycDetailsData)paramObject;
        KycDetails localKycDetails = user;
        paramObject = user;
        boolean bool2 = k.a(localKycDetails, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final KycDetails getUser()
  {
    return user;
  }
  
  public final int hashCode()
  {
    KycDetails localKycDetails = user;
    if (localKycDetails != null) {
      return localKycDetails.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("KycDetailsData(user=");
    KycDetails localKycDetails = user;
    localStringBuilder.append(localKycDetails);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.KycDetailsData
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */