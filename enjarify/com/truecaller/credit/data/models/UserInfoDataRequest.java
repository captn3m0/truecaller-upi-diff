package com.truecaller.credit.data.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class UserInfoDataRequest
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  private final List addresses;
  private String birth_date;
  private String full_name;
  private String identifier;
  private String type;
  
  static
  {
    UserInfoDataRequest.Creator localCreator = new com/truecaller/credit/data/models/UserInfoDataRequest$Creator;
    localCreator.<init>();
    CREATOR = localCreator;
  }
  
  public UserInfoDataRequest()
  {
    this(null, null, null, null, null, 31, null);
  }
  
  public UserInfoDataRequest(String paramString1, String paramString2, String paramString3, String paramString4, List paramList)
  {
    type = paramString1;
    full_name = paramString2;
    birth_date = paramString3;
    identifier = paramString4;
    addresses = paramList;
  }
  
  public final String component1()
  {
    return type;
  }
  
  public final String component2()
  {
    return full_name;
  }
  
  public final String component3()
  {
    return birth_date;
  }
  
  public final String component4()
  {
    return identifier;
  }
  
  public final List component5()
  {
    return addresses;
  }
  
  public final UserInfoDataRequest copy(String paramString1, String paramString2, String paramString3, String paramString4, List paramList)
  {
    k.b(paramString1, "type");
    k.b(paramString2, "full_name");
    k.b(paramString3, "birth_date");
    k.b(paramString4, "identifier");
    k.b(paramList, "addresses");
    UserInfoDataRequest localUserInfoDataRequest = new com/truecaller/credit/data/models/UserInfoDataRequest;
    localUserInfoDataRequest.<init>(paramString1, paramString2, paramString3, paramString4, paramList);
    return localUserInfoDataRequest;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof UserInfoDataRequest;
      if (bool1)
      {
        paramObject = (UserInfoDataRequest)paramObject;
        Object localObject = type;
        String str = type;
        bool1 = k.a(localObject, str);
        if (bool1)
        {
          localObject = full_name;
          str = full_name;
          bool1 = k.a(localObject, str);
          if (bool1)
          {
            localObject = birth_date;
            str = birth_date;
            bool1 = k.a(localObject, str);
            if (bool1)
            {
              localObject = identifier;
              str = identifier;
              bool1 = k.a(localObject, str);
              if (bool1)
              {
                localObject = addresses;
                paramObject = addresses;
                boolean bool2 = k.a(localObject, paramObject);
                if (bool2) {
                  break label134;
                }
              }
            }
          }
        }
      }
      return false;
    }
    label134:
    return true;
  }
  
  public final List getAddresses()
  {
    return addresses;
  }
  
  public final String getBirth_date()
  {
    return birth_date;
  }
  
  public final String getFull_name()
  {
    return full_name;
  }
  
  public final String getIdentifier()
  {
    return identifier;
  }
  
  public final String getType()
  {
    return type;
  }
  
  public final int hashCode()
  {
    String str = type;
    int i = 0;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    j *= 31;
    Object localObject = full_name;
    int k;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    int j = (j + k) * 31;
    localObject = birth_date;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = identifier;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = addresses;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final void setBirth_date(String paramString)
  {
    k.b(paramString, "<set-?>");
    birth_date = paramString;
  }
  
  public final void setFull_name(String paramString)
  {
    k.b(paramString, "<set-?>");
    full_name = paramString;
  }
  
  public final void setIdentifier(String paramString)
  {
    k.b(paramString, "<set-?>");
    identifier = paramString;
  }
  
  public final void setType(String paramString)
  {
    k.b(paramString, "<set-?>");
    type = paramString;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("UserInfoDataRequest(type=");
    Object localObject = type;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", full_name=");
    localObject = full_name;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", birth_date=");
    localObject = birth_date;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", identifier=");
    localObject = identifier;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", addresses=");
    localObject = addresses;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    Object localObject = type;
    paramParcel.writeString((String)localObject);
    localObject = full_name;
    paramParcel.writeString((String)localObject);
    localObject = birth_date;
    paramParcel.writeString((String)localObject);
    localObject = identifier;
    paramParcel.writeString((String)localObject);
    localObject = addresses;
    int i = ((Collection)localObject).size();
    paramParcel.writeInt(i);
    localObject = ((Collection)localObject).iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject).hasNext();
      if (!bool) {
        break;
      }
      Address localAddress = (Address)((Iterator)localObject).next();
      localAddress.writeToParcel(paramParcel, 0);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.UserInfoDataRequest
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */