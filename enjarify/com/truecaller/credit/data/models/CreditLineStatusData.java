package com.truecaller.credit.data.models;

import c.g.b.k;
import com.truecaller.credit.domain.interactors.onboarding.models.CreditLineStatus;

public final class CreditLineStatusData
{
  private final CreditLineStatus credit_line;
  
  public CreditLineStatusData(CreditLineStatus paramCreditLineStatus)
  {
    credit_line = paramCreditLineStatus;
  }
  
  public final CreditLineStatus component1()
  {
    return credit_line;
  }
  
  public final CreditLineStatusData copy(CreditLineStatus paramCreditLineStatus)
  {
    CreditLineStatusData localCreditLineStatusData = new com/truecaller/credit/data/models/CreditLineStatusData;
    localCreditLineStatusData.<init>(paramCreditLineStatus);
    return localCreditLineStatusData;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof CreditLineStatusData;
      if (bool1)
      {
        paramObject = (CreditLineStatusData)paramObject;
        CreditLineStatus localCreditLineStatus = credit_line;
        paramObject = credit_line;
        boolean bool2 = k.a(localCreditLineStatus, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final CreditLineStatus getCredit_line()
  {
    return credit_line;
  }
  
  public final int hashCode()
  {
    CreditLineStatus localCreditLineStatus = credit_line;
    if (localCreditLineStatus != null) {
      return localCreditLineStatus.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("CreditLineStatusData(credit_line=");
    CreditLineStatus localCreditLineStatus = credit_line;
    localStringBuilder.append(localCreditLineStatus);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.CreditLineStatusData
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */