package com.truecaller.credit.data.models;

import c.g.b.k;

public final class IFSCSearchRequest
{
  private final String search_term;
  
  public IFSCSearchRequest(String paramString)
  {
    search_term = paramString;
  }
  
  public final String component1()
  {
    return search_term;
  }
  
  public final IFSCSearchRequest copy(String paramString)
  {
    k.b(paramString, "search_term");
    IFSCSearchRequest localIFSCSearchRequest = new com/truecaller/credit/data/models/IFSCSearchRequest;
    localIFSCSearchRequest.<init>(paramString);
    return localIFSCSearchRequest;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof IFSCSearchRequest;
      if (bool1)
      {
        paramObject = (IFSCSearchRequest)paramObject;
        String str = search_term;
        paramObject = search_term;
        boolean bool2 = k.a(str, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String getSearch_term()
  {
    return search_term;
  }
  
  public final int hashCode()
  {
    String str = search_term;
    if (str != null) {
      return str.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("IFSCSearchRequest(search_term=");
    String str = search_term;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.IFSCSearchRequest
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */