package com.truecaller.credit.data.models;

import c.n.m;

public final class VerifyFinalOfferOtpResponse
  extends BaseApiResponse
  implements Mappable
{
  public final String errorMessage()
  {
    BaseApiResponse.Meta localMeta = getMeta();
    if (localMeta != null) {
      return localMeta.getMessage();
    }
    return null;
  }
  
  public final boolean isValid()
  {
    return m.a(getStatus(), "success", true);
  }
  
  public final VerifyFinalOfferOtpResult mapToData()
  {
    VerifyFinalOfferOtpResult localVerifyFinalOfferOtpResult = new com/truecaller/credit/data/models/VerifyFinalOfferOtpResult;
    Object localObject = getMeta();
    if (localObject != null)
    {
      localObject = ((BaseApiResponse.Meta)localObject).getMessage();
      if (localObject != null) {}
    }
    else
    {
      localObject = "success";
    }
    localVerifyFinalOfferOtpResult.<init>((String)localObject);
    return localVerifyFinalOfferOtpResult;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.VerifyFinalOfferOtpResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */