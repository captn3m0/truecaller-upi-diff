package com.truecaller.credit.data.models;

import c.g.b.k;
import c.n.m;

public final class FetchAddressResponse
  extends BaseApiResponse
  implements Mappable
{
  private final FetchAddressResponse.GetAddressResponseData data;
  
  public FetchAddressResponse(FetchAddressResponse.GetAddressResponseData paramGetAddressResponseData)
  {
    data = paramGetAddressResponseData;
  }
  
  public final FetchAddressResponse.GetAddressResponseData component1()
  {
    return data;
  }
  
  public final FetchAddressResponse copy(FetchAddressResponse.GetAddressResponseData paramGetAddressResponseData)
  {
    k.b(paramGetAddressResponseData, "data");
    FetchAddressResponse localFetchAddressResponse = new com/truecaller/credit/data/models/FetchAddressResponse;
    localFetchAddressResponse.<init>(paramGetAddressResponseData);
    return localFetchAddressResponse;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof FetchAddressResponse;
      if (bool1)
      {
        paramObject = (FetchAddressResponse)paramObject;
        FetchAddressResponse.GetAddressResponseData localGetAddressResponseData = data;
        paramObject = data;
        boolean bool2 = k.a(localGetAddressResponseData, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String errorMessage()
  {
    BaseApiResponse.Meta localMeta = getMeta();
    if (localMeta != null) {
      return localMeta.getMessage();
    }
    return null;
  }
  
  public final FetchAddressResponse.GetAddressResponseData getData()
  {
    return data;
  }
  
  public final int hashCode()
  {
    FetchAddressResponse.GetAddressResponseData localGetAddressResponseData = data;
    if (localGetAddressResponseData != null) {
      return localGetAddressResponseData.hashCode();
    }
    return 0;
  }
  
  public final boolean isValid()
  {
    return m.a(getStatus(), "success", true);
  }
  
  public final Address mapToData()
  {
    return data.getAddress();
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("FetchAddressResponse(data=");
    FetchAddressResponse.GetAddressResponseData localGetAddressResponseData = data;
    localStringBuilder.append(localGetAddressResponseData);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.FetchAddressResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */