package com.truecaller.credit.data.models;

import c.g.b.k;
import java.util.List;

public final class IFSCSearchsData
{
  private final List results;
  
  public IFSCSearchsData(List paramList)
  {
    results = paramList;
  }
  
  public final List component1()
  {
    return results;
  }
  
  public final IFSCSearchsData copy(List paramList)
  {
    k.b(paramList, "results");
    IFSCSearchsData localIFSCSearchsData = new com/truecaller/credit/data/models/IFSCSearchsData;
    localIFSCSearchsData.<init>(paramList);
    return localIFSCSearchsData;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof IFSCSearchsData;
      if (bool1)
      {
        paramObject = (IFSCSearchsData)paramObject;
        List localList = results;
        paramObject = results;
        boolean bool2 = k.a(localList, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final List getResults()
  {
    return results;
  }
  
  public final int hashCode()
  {
    List localList = results;
    if (localList != null) {
      return localList.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("IFSCSearchsData(results=");
    List localList = results;
    localStringBuilder.append(localList);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.IFSCSearchsData
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */