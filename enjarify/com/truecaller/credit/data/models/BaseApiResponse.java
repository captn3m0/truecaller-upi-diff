package com.truecaller.credit.data.models;

import c.g.b.k;

public abstract class BaseApiResponse
{
  private String message;
  private BaseApiResponse.Meta meta;
  private String status = "";
  
  public final String getMessage()
  {
    return message;
  }
  
  public final BaseApiResponse.Meta getMeta()
  {
    return meta;
  }
  
  public final String getStatus()
  {
    return status;
  }
  
  public final void setMessage(String paramString)
  {
    message = paramString;
  }
  
  public final void setMeta(BaseApiResponse.Meta paramMeta)
  {
    meta = paramMeta;
  }
  
  public final void setStatus(String paramString)
  {
    k.b(paramString, "<set-?>");
    status = paramString;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.BaseApiResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */