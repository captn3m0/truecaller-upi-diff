package com.truecaller.credit.data.models;

import c.g.b.k;
import c.n.m;
import com.truecaller.credit.domain.interactors.onboarding.models.CreditLineStatus;

public final class CreditLineStatusResponse
  extends BaseApiResponse
  implements Mappable
{
  private final CreditLineStatusData data;
  
  public CreditLineStatusResponse(CreditLineStatusData paramCreditLineStatusData)
  {
    data = paramCreditLineStatusData;
  }
  
  public final CreditLineStatusData component1()
  {
    return data;
  }
  
  public final CreditLineStatusResponse copy(CreditLineStatusData paramCreditLineStatusData)
  {
    CreditLineStatusResponse localCreditLineStatusResponse = new com/truecaller/credit/data/models/CreditLineStatusResponse;
    localCreditLineStatusResponse.<init>(paramCreditLineStatusData);
    return localCreditLineStatusResponse;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof CreditLineStatusResponse;
      if (bool1)
      {
        paramObject = (CreditLineStatusResponse)paramObject;
        CreditLineStatusData localCreditLineStatusData = data;
        paramObject = data;
        boolean bool2 = k.a(localCreditLineStatusData, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String errorMessage()
  {
    BaseApiResponse.Meta localMeta = getMeta();
    if (localMeta != null) {
      return localMeta.getMessage();
    }
    return null;
  }
  
  public final CreditLineStatusData getData()
  {
    return data;
  }
  
  public final int hashCode()
  {
    CreditLineStatusData localCreditLineStatusData = data;
    if (localCreditLineStatusData != null) {
      return localCreditLineStatusData.hashCode();
    }
    return 0;
  }
  
  public final boolean isValid()
  {
    return m.a(getStatus(), "success", true);
  }
  
  public final CreditLineStatus mapToData()
  {
    CreditLineStatus localCreditLineStatus = new com/truecaller/credit/domain/interactors/onboarding/models/CreditLineStatus;
    Object localObject1 = data;
    String str = null;
    if (localObject1 != null)
    {
      localObject1 = ((CreditLineStatusData)localObject1).getCredit_line();
      if (localObject1 != null)
      {
        localObject1 = ((CreditLineStatus)localObject1).getId();
        break label34;
      }
    }
    localObject1 = null;
    label34:
    Object localObject2 = data;
    if (localObject2 != null)
    {
      localObject2 = ((CreditLineStatusData)localObject2).getCredit_line();
      if (localObject2 != null)
      {
        localObject2 = ((CreditLineStatus)localObject2).getStatus();
        break label70;
      }
    }
    localObject2 = null;
    label70:
    Object localObject3 = data;
    if (localObject3 != null)
    {
      localObject3 = ((CreditLineStatusData)localObject3).getCredit_line();
      if (localObject3 != null)
      {
        localObject3 = ((CreditLineStatus)localObject3).getAmount();
        break label106;
      }
    }
    localObject3 = null;
    label106:
    Object localObject4 = data;
    if (localObject4 != null)
    {
      localObject4 = ((CreditLineStatusData)localObject4).getCredit_line();
      if (localObject4 != null) {
        str = ((CreditLineStatus)localObject4).getInterest_rate();
      }
    }
    localCreditLineStatus.<init>((String)localObject1, (String)localObject2, (String)localObject3, str);
    return localCreditLineStatus;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("CreditLineStatusResponse(data=");
    CreditLineStatusData localCreditLineStatusData = data;
    localStringBuilder.append(localCreditLineStatusData);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.CreditLineStatusResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */