package com.truecaller.credit.data.models;

import c.g.b.k;
import c.n.m;

public final class BankAccountDetailsResponse
  extends BaseApiResponse
  implements Mappable
{
  private final BankAccountDetailsData data;
  
  public BankAccountDetailsResponse(BankAccountDetailsData paramBankAccountDetailsData)
  {
    data = paramBankAccountDetailsData;
  }
  
  public final BankAccountDetailsData component1()
  {
    return data;
  }
  
  public final BankAccountDetailsResponse copy(BankAccountDetailsData paramBankAccountDetailsData)
  {
    k.b(paramBankAccountDetailsData, "data");
    BankAccountDetailsResponse localBankAccountDetailsResponse = new com/truecaller/credit/data/models/BankAccountDetailsResponse;
    localBankAccountDetailsResponse.<init>(paramBankAccountDetailsData);
    return localBankAccountDetailsResponse;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof BankAccountDetailsResponse;
      if (bool1)
      {
        paramObject = (BankAccountDetailsResponse)paramObject;
        BankAccountDetailsData localBankAccountDetailsData = data;
        paramObject = data;
        boolean bool2 = k.a(localBankAccountDetailsData, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String errorMessage()
  {
    BaseApiResponse.Meta localMeta = getMeta();
    if (localMeta != null) {
      return localMeta.getMessage();
    }
    return null;
  }
  
  public final BankAccountDetailsData getData()
  {
    return data;
  }
  
  public final int hashCode()
  {
    BankAccountDetailsData localBankAccountDetailsData = data;
    if (localBankAccountDetailsData != null) {
      return localBankAccountDetailsData.hashCode();
    }
    return 0;
  }
  
  public final boolean isValid()
  {
    return m.a(getStatus(), "success", true);
  }
  
  public final BankAccountDetails mapToData()
  {
    BankAccountDetails localBankAccountDetails = new com/truecaller/credit/data/models/BankAccountDetails;
    String str1 = data.getAccount().getLast_4_digit();
    String str2 = data.getAccount().getName();
    String str3 = data.getAccount().getLogo();
    localBankAccountDetails.<init>(str1, str2, str3);
    return localBankAccountDetails;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("BankAccountDetailsResponse(data=");
    BankAccountDetailsData localBankAccountDetailsData = data;
    localStringBuilder.append(localBankAccountDetailsData);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.BankAccountDetailsResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */