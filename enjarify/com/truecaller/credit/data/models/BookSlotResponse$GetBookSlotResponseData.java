package com.truecaller.credit.data.models;

import c.g.b.k;

public final class BookSlotResponse$GetBookSlotResponseData
{
  private final String message;
  
  public BookSlotResponse$GetBookSlotResponseData(String paramString)
  {
    message = paramString;
  }
  
  public final String component1()
  {
    return message;
  }
  
  public final GetBookSlotResponseData copy(String paramString)
  {
    GetBookSlotResponseData localGetBookSlotResponseData = new com/truecaller/credit/data/models/BookSlotResponse$GetBookSlotResponseData;
    localGetBookSlotResponseData.<init>(paramString);
    return localGetBookSlotResponseData;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof GetBookSlotResponseData;
      if (bool1)
      {
        paramObject = (GetBookSlotResponseData)paramObject;
        String str = message;
        paramObject = message;
        boolean bool2 = k.a(str, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String getMessage()
  {
    return message;
  }
  
  public final int hashCode()
  {
    String str = message;
    if (str != null) {
      return str.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("GetBookSlotResponseData(message=");
    String str = message;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.BookSlotResponse.GetBookSlotResponseData
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */