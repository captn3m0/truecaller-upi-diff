package com.truecaller.credit.data.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class Address$Creator
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    Address localAddress = new com/truecaller/credit/data/models/Address;
    String str1 = paramParcel.readString();
    String str2 = paramParcel.readString();
    String str3 = paramParcel.readString();
    String str4 = paramParcel.readString();
    String str5 = paramParcel.readString();
    String str6 = paramParcel.readString();
    String str7 = paramParcel.readString();
    localAddress.<init>(str1, str2, str3, str4, str5, str6, str7);
    return localAddress;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new Address[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.Address.Creator
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */