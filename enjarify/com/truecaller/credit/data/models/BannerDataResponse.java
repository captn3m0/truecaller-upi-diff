package com.truecaller.credit.data.models;

import c.g.b.k;
import c.n.m;
import com.truecaller.credit.domain.interactors.banner.CreditBanner;

public final class BannerDataResponse
  extends BaseApiResponse
  implements Mappable
{
  private final BannerData data;
  
  public BannerDataResponse(BannerData paramBannerData)
  {
    data = paramBannerData;
  }
  
  public final BannerData component1()
  {
    return data;
  }
  
  public final BannerDataResponse copy(BannerData paramBannerData)
  {
    BannerDataResponse localBannerDataResponse = new com/truecaller/credit/data/models/BannerDataResponse;
    localBannerDataResponse.<init>(paramBannerData);
    return localBannerDataResponse;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof BannerDataResponse;
      if (bool1)
      {
        paramObject = (BannerDataResponse)paramObject;
        BannerData localBannerData = data;
        paramObject = data;
        boolean bool2 = k.a(localBannerData, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String errorMessage()
  {
    BaseApiResponse.Meta localMeta = getMeta();
    if (localMeta != null) {
      return localMeta.getMessage();
    }
    return null;
  }
  
  public final BannerData getData()
  {
    return data;
  }
  
  public final int hashCode()
  {
    BannerData localBannerData = data;
    if (localBannerData != null) {
      return localBannerData.hashCode();
    }
    return 0;
  }
  
  public final boolean isValid()
  {
    return m.a(getStatus(), "success", true);
  }
  
  public final CreditBanner mapToData()
  {
    CreditBanner localCreditBanner = new com/truecaller/credit/domain/interactors/banner/CreditBanner;
    Object localObject1 = data;
    Object localObject2 = null;
    if (localObject1 != null)
    {
      localObject1 = ((BannerData)localObject1).getApp_state();
      localObject3 = localObject1;
    }
    else
    {
      localObject3 = null;
    }
    localObject1 = data;
    if (localObject1 != null)
    {
      localObject1 = ((BannerData)localObject1).getBanner();
      if (localObject1 != null)
      {
        localObject1 = ((Banner)localObject1).getType();
        localObject4 = localObject1;
        break label61;
      }
    }
    Object localObject4 = null;
    label61:
    localObject1 = data;
    if (localObject1 != null)
    {
      localObject1 = ((BannerData)localObject1).getBanner();
      if (localObject1 != null)
      {
        localObject1 = ((Banner)localObject1).getTitle();
        localObject5 = localObject1;
        break label93;
      }
    }
    Object localObject5 = null;
    label93:
    localObject1 = data;
    if (localObject1 != null)
    {
      localObject1 = ((BannerData)localObject1).getBanner();
      if (localObject1 != null)
      {
        localObject1 = ((Banner)localObject1).getSubtitle();
        localObject6 = localObject1;
        break label125;
      }
    }
    Object localObject6 = null;
    label125:
    localObject1 = data;
    if (localObject1 != null)
    {
      localObject1 = ((BannerData)localObject1).getBanner();
      if (localObject1 != null)
      {
        localObject1 = ((Banner)localObject1).getButton();
        if (localObject1 != null)
        {
          localObject1 = ((Button)localObject1).getAction();
          localObject7 = localObject1;
          break label166;
        }
      }
    }
    Object localObject7 = null;
    label166:
    localObject1 = data;
    if (localObject1 != null)
    {
      localObject1 = ((BannerData)localObject1).getBanner();
      if (localObject1 != null)
      {
        localObject1 = ((Banner)localObject1).getButton();
        if (localObject1 != null)
        {
          localObject1 = ((Button)localObject1).getText();
          localObject8 = localObject1;
          break label207;
        }
      }
    }
    Object localObject8 = null;
    label207:
    localObject1 = data;
    if (localObject1 != null)
    {
      localObject1 = ((BannerData)localObject1).getBanner();
      if (localObject1 != null)
      {
        localObject1 = ((Banner)localObject1).getHeader();
        if (localObject1 != null)
        {
          localObject1 = ((Header)localObject1).getLeft_text();
          localObject9 = localObject1;
          break label248;
        }
      }
    }
    Object localObject9 = null;
    label248:
    localObject1 = data;
    if (localObject1 != null)
    {
      localObject1 = ((BannerData)localObject1).getBanner();
      if (localObject1 != null)
      {
        localObject1 = ((Banner)localObject1).getHeader();
        if (localObject1 != null)
        {
          localObject1 = ((Header)localObject1).getRight_text();
          localObject10 = localObject1;
          break label289;
        }
      }
    }
    Object localObject10 = null;
    label289:
    localObject1 = data;
    if (localObject1 != null)
    {
      localObject1 = ((BannerData)localObject1).getBanner();
      if (localObject1 != null)
      {
        localObject1 = ((Banner)localObject1).getImage();
        if (localObject1 != null)
        {
          localObject1 = ((Image)localObject1).getType();
          localObject11 = localObject1;
          break label330;
        }
      }
    }
    Object localObject11 = null;
    label330:
    localObject1 = data;
    if (localObject1 != null)
    {
      localObject1 = ((BannerData)localObject1).getBanner();
      if (localObject1 != null)
      {
        localObject1 = ((Banner)localObject1).getImage();
        if (localObject1 != null)
        {
          localObject1 = ((Image)localObject1).getPercentage();
          localObject12 = localObject1;
          break label371;
        }
      }
    }
    Object localObject12 = null;
    label371:
    localObject1 = data;
    if (localObject1 != null)
    {
      localObject1 = ((BannerData)localObject1).getBanner();
      if (localObject1 != null)
      {
        localObject1 = ((Banner)localObject1).getImage();
        if (localObject1 != null)
        {
          localObject1 = ((Image)localObject1).getUrl();
          localObject13 = localObject1;
          break label412;
        }
      }
    }
    Object localObject13 = null;
    label412:
    localObject1 = localCreditBanner;
    localObject2 = localObject3;
    Object localObject3 = localObject4;
    localObject4 = localObject5;
    localObject5 = localObject6;
    localObject6 = localObject7;
    localObject7 = localObject8;
    localObject8 = localObject9;
    localObject9 = localObject10;
    localObject10 = localObject11;
    localObject11 = localObject12;
    localObject12 = localObject13;
    localCreditBanner.<init>((String)localObject2, (Integer)localObject3, (String)localObject4, (String)localObject5, (String)localObject6, (String)localObject7, (String)localObject8, (String)localObject9, (String)localObject10, (Integer)localObject11, (String)localObject13);
    return localCreditBanner;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("BannerDataResponse(data=");
    BannerData localBannerData = data;
    localStringBuilder.append(localBannerData);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.BannerDataResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */