package com.truecaller.credit.data.models;

import c.g.b.k;
import c.n.m;

public final class AddAddressResponse
  extends BaseApiResponse
  implements Mappable
{
  private final AddAddressResponse.GetResponseData data;
  
  public AddAddressResponse(AddAddressResponse.GetResponseData paramGetResponseData)
  {
    data = paramGetResponseData;
  }
  
  public final AddAddressResponse.GetResponseData component1()
  {
    return data;
  }
  
  public final AddAddressResponse copy(AddAddressResponse.GetResponseData paramGetResponseData)
  {
    k.b(paramGetResponseData, "data");
    AddAddressResponse localAddAddressResponse = new com/truecaller/credit/data/models/AddAddressResponse;
    localAddAddressResponse.<init>(paramGetResponseData);
    return localAddAddressResponse;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof AddAddressResponse;
      if (bool1)
      {
        paramObject = (AddAddressResponse)paramObject;
        AddAddressResponse.GetResponseData localGetResponseData = data;
        paramObject = data;
        boolean bool2 = k.a(localGetResponseData, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String errorMessage()
  {
    BaseApiResponse.Meta localMeta = getMeta();
    if (localMeta != null) {
      return localMeta.getMessage();
    }
    return null;
  }
  
  public final AddAddressResponse.GetResponseData getData()
  {
    return data;
  }
  
  public final int hashCode()
  {
    AddAddressResponse.GetResponseData localGetResponseData = data;
    if (localGetResponseData != null) {
      return localGetResponseData.hashCode();
    }
    return 0;
  }
  
  public final boolean isValid()
  {
    return m.a(getStatus(), "success", true);
  }
  
  public final Address mapToData()
  {
    return data.getAddress();
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("AddAddressResponse(data=");
    AddAddressResponse.GetResponseData localGetResponseData = data;
    localStringBuilder.append(localGetResponseData);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.AddAddressResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */