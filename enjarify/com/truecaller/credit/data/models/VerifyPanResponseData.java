package com.truecaller.credit.data.models;

import c.g.b.k;

public final class VerifyPanResponseData
{
  private final VerifyPanResponse pan;
  
  public VerifyPanResponseData(VerifyPanResponse paramVerifyPanResponse)
  {
    pan = paramVerifyPanResponse;
  }
  
  public final VerifyPanResponse component1()
  {
    return pan;
  }
  
  public final VerifyPanResponseData copy(VerifyPanResponse paramVerifyPanResponse)
  {
    k.b(paramVerifyPanResponse, "pan");
    VerifyPanResponseData localVerifyPanResponseData = new com/truecaller/credit/data/models/VerifyPanResponseData;
    localVerifyPanResponseData.<init>(paramVerifyPanResponse);
    return localVerifyPanResponseData;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof VerifyPanResponseData;
      if (bool1)
      {
        paramObject = (VerifyPanResponseData)paramObject;
        VerifyPanResponse localVerifyPanResponse = pan;
        paramObject = pan;
        boolean bool2 = k.a(localVerifyPanResponse, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final VerifyPanResponse getPan()
  {
    return pan;
  }
  
  public final int hashCode()
  {
    VerifyPanResponse localVerifyPanResponse = pan;
    if (localVerifyPanResponse != null) {
      return localVerifyPanResponse.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("VerifyPanResponseData(pan=");
    VerifyPanResponse localVerifyPanResponse = pan;
    localStringBuilder.append(localVerifyPanResponse);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.VerifyPanResponseData
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */