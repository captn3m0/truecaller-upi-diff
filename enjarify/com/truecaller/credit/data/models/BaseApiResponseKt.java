package com.truecaller.credit.data.models;

public final class BaseApiResponseKt
{
  public static final int min_offer = 1;
  public static final String success = "success";
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.BaseApiResponseKt
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */