package com.truecaller.credit.data.models;

import c.g.b.k;
import c.n.m;
import java.util.List;

public final class FetchSlotResponse
  extends BaseApiResponse
  implements Mappable
{
  private final FetchSlotResponse.GetSlotResponseData data;
  
  public FetchSlotResponse(FetchSlotResponse.GetSlotResponseData paramGetSlotResponseData)
  {
    data = paramGetSlotResponseData;
  }
  
  public final FetchSlotResponse.GetSlotResponseData component1()
  {
    return data;
  }
  
  public final FetchSlotResponse copy(FetchSlotResponse.GetSlotResponseData paramGetSlotResponseData)
  {
    k.b(paramGetSlotResponseData, "data");
    FetchSlotResponse localFetchSlotResponse = new com/truecaller/credit/data/models/FetchSlotResponse;
    localFetchSlotResponse.<init>(paramGetSlotResponseData);
    return localFetchSlotResponse;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof FetchSlotResponse;
      if (bool1)
      {
        paramObject = (FetchSlotResponse)paramObject;
        FetchSlotResponse.GetSlotResponseData localGetSlotResponseData = data;
        paramObject = data;
        boolean bool2 = k.a(localGetSlotResponseData, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String errorMessage()
  {
    BaseApiResponse.Meta localMeta = getMeta();
    if (localMeta != null) {
      return localMeta.getMessage();
    }
    return null;
  }
  
  public final FetchSlotResponse.GetSlotResponseData getData()
  {
    return data;
  }
  
  public final int hashCode()
  {
    FetchSlotResponse.GetSlotResponseData localGetSlotResponseData = data;
    if (localGetSlotResponseData != null) {
      return localGetSlotResponseData.hashCode();
    }
    return 0;
  }
  
  public final boolean isValid()
  {
    return m.a(getStatus(), "success", true);
  }
  
  public final List mapToData()
  {
    return data.getAppointments();
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("FetchSlotResponse(data=");
    FetchSlotResponse.GetSlotResponseData localGetSlotResponseData = data;
    localStringBuilder.append(localGetSlotResponseData);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.FetchSlotResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */