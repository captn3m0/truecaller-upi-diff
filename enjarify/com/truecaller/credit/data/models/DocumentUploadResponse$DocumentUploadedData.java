package com.truecaller.credit.data.models;

import c.g.b.k;

public final class DocumentUploadResponse$DocumentUploadedData
{
  private final DocumentUploadResponse.Document document;
  
  public DocumentUploadResponse$DocumentUploadedData(DocumentUploadResponse.Document paramDocument)
  {
    document = paramDocument;
  }
  
  public final DocumentUploadResponse.Document component1()
  {
    return document;
  }
  
  public final DocumentUploadedData copy(DocumentUploadResponse.Document paramDocument)
  {
    DocumentUploadedData localDocumentUploadedData = new com/truecaller/credit/data/models/DocumentUploadResponse$DocumentUploadedData;
    localDocumentUploadedData.<init>(paramDocument);
    return localDocumentUploadedData;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof DocumentUploadedData;
      if (bool1)
      {
        paramObject = (DocumentUploadedData)paramObject;
        DocumentUploadResponse.Document localDocument = document;
        paramObject = document;
        boolean bool2 = k.a(localDocument, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final DocumentUploadResponse.Document getDocument()
  {
    return document;
  }
  
  public final int hashCode()
  {
    DocumentUploadResponse.Document localDocument = document;
    if (localDocument != null) {
      return localDocument.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("DocumentUploadedData(document=");
    DocumentUploadResponse.Document localDocument = document;
    localStringBuilder.append(localDocument);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.DocumentUploadResponse.DocumentUploadedData
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */