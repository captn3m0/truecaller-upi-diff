package com.truecaller.credit.data.models;

import c.g.b.k;

public final class SaveLocationRequest
{
  private final String location;
  
  public SaveLocationRequest(String paramString)
  {
    location = paramString;
  }
  
  public final String component1()
  {
    return location;
  }
  
  public final SaveLocationRequest copy(String paramString)
  {
    k.b(paramString, "location");
    SaveLocationRequest localSaveLocationRequest = new com/truecaller/credit/data/models/SaveLocationRequest;
    localSaveLocationRequest.<init>(paramString);
    return localSaveLocationRequest;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof SaveLocationRequest;
      if (bool1)
      {
        paramObject = (SaveLocationRequest)paramObject;
        String str = location;
        paramObject = location;
        boolean bool2 = k.a(str, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String getLocation()
  {
    return location;
  }
  
  public final int hashCode()
  {
    String str = location;
    if (str != null) {
      return str.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("SaveLocationRequest(location=");
    String str = location;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.SaveLocationRequest
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */