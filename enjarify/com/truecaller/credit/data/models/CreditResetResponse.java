package com.truecaller.credit.data.models;

import c.g.b.k;
import c.n.m;

public final class CreditResetResponse
  extends BaseApiResponse
  implements Mappable
{
  private final CreditResetResponse data;
  
  public CreditResetResponse(CreditResetResponse paramCreditResetResponse)
  {
    data = paramCreditResetResponse;
  }
  
  public final CreditResetResponse component1()
  {
    return data;
  }
  
  public final CreditResetResponse copy(CreditResetResponse paramCreditResetResponse)
  {
    k.b(paramCreditResetResponse, "data");
    CreditResetResponse localCreditResetResponse = new com/truecaller/credit/data/models/CreditResetResponse;
    localCreditResetResponse.<init>(paramCreditResetResponse);
    return localCreditResetResponse;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof CreditResetResponse;
      if (bool1)
      {
        paramObject = (CreditResetResponse)paramObject;
        CreditResetResponse localCreditResetResponse = data;
        paramObject = data;
        boolean bool2 = k.a(localCreditResetResponse, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String errorMessage()
  {
    BaseApiResponse.Meta localMeta = getMeta();
    if (localMeta != null) {
      return localMeta.getMessage();
    }
    return null;
  }
  
  public final CreditResetResponse getData()
  {
    return data;
  }
  
  public final int hashCode()
  {
    CreditResetResponse localCreditResetResponse = data;
    if (localCreditResetResponse != null) {
      return localCreditResetResponse.hashCode();
    }
    return 0;
  }
  
  public final boolean isValid()
  {
    return m.a(getStatus(), "success", true);
  }
  
  public final CreditResetResponse mapToData()
  {
    return this;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("CreditResetResponse(data=");
    CreditResetResponse localCreditResetResponse = data;
    localStringBuilder.append(localCreditResetResponse);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.CreditResetResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */