package com.truecaller.credit.data.models;

public abstract interface Mappable
{
  public abstract String errorMessage();
  
  public abstract boolean isValid();
  
  public abstract Object mapToData();
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.Mappable
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */