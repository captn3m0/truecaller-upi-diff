package com.truecaller.credit.data.models;

import c.g.b.k;
import c.n.m;
import java.util.List;

public final class ScoreDataRulesResponse
  extends BaseApiResponse
  implements Mappable
{
  private final ScoreDataRules data;
  
  public ScoreDataRulesResponse(ScoreDataRules paramScoreDataRules)
  {
    data = paramScoreDataRules;
  }
  
  public final ScoreDataRules component1()
  {
    return data;
  }
  
  public final ScoreDataRulesResponse copy(ScoreDataRules paramScoreDataRules)
  {
    k.b(paramScoreDataRules, "data");
    ScoreDataRulesResponse localScoreDataRulesResponse = new com/truecaller/credit/data/models/ScoreDataRulesResponse;
    localScoreDataRulesResponse.<init>(paramScoreDataRules);
    return localScoreDataRulesResponse;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof ScoreDataRulesResponse;
      if (bool1)
      {
        paramObject = (ScoreDataRulesResponse)paramObject;
        ScoreDataRules localScoreDataRules = data;
        paramObject = data;
        boolean bool2 = k.a(localScoreDataRules, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String errorMessage()
  {
    BaseApiResponse.Meta localMeta = getMeta();
    if (localMeta != null) {
      return localMeta.getMessage();
    }
    return null;
  }
  
  public final ScoreDataRules getData()
  {
    return data;
  }
  
  public final int hashCode()
  {
    ScoreDataRules localScoreDataRules = data;
    if (localScoreDataRules != null) {
      return localScoreDataRules.hashCode();
    }
    return 0;
  }
  
  public final boolean isValid()
  {
    return m.a(getStatus(), "success", true);
  }
  
  public final ScoreDataRules mapToData()
  {
    ScoreDataRules localScoreDataRules = new com/truecaller/credit/data/models/ScoreDataRules;
    String str1 = data.getBlacklist_regex();
    String str2 = data.getMin_app_version();
    List localList = data.getRules();
    String str3 = data.getVersion();
    localScoreDataRules.<init>(str1, str2, localList, str3);
    return localScoreDataRules;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("ScoreDataRulesResponse(data=");
    ScoreDataRules localScoreDataRules = data;
    localStringBuilder.append(localScoreDataRules);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.ScoreDataRulesResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */