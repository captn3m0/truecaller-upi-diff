package com.truecaller.credit.data.models;

import c.g.b.k;
import c.n.m;

public final class FetchAppointmentResponse
  extends BaseApiResponse
  implements Mappable
{
  private final FetchAppointmentResponse.GetAppointmentResponseData data;
  
  public FetchAppointmentResponse(FetchAppointmentResponse.GetAppointmentResponseData paramGetAppointmentResponseData)
  {
    data = paramGetAppointmentResponseData;
  }
  
  public final FetchAppointmentResponse.GetAppointmentResponseData component1()
  {
    return data;
  }
  
  public final FetchAppointmentResponse copy(FetchAppointmentResponse.GetAppointmentResponseData paramGetAppointmentResponseData)
  {
    k.b(paramGetAppointmentResponseData, "data");
    FetchAppointmentResponse localFetchAppointmentResponse = new com/truecaller/credit/data/models/FetchAppointmentResponse;
    localFetchAppointmentResponse.<init>(paramGetAppointmentResponseData);
    return localFetchAppointmentResponse;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof FetchAppointmentResponse;
      if (bool1)
      {
        paramObject = (FetchAppointmentResponse)paramObject;
        FetchAppointmentResponse.GetAppointmentResponseData localGetAppointmentResponseData = data;
        paramObject = data;
        boolean bool2 = k.a(localGetAppointmentResponseData, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String errorMessage()
  {
    BaseApiResponse.Meta localMeta = getMeta();
    if (localMeta != null) {
      return localMeta.getMessage();
    }
    return null;
  }
  
  public final FetchAppointmentResponse.GetAppointmentResponseData getData()
  {
    return data;
  }
  
  public final int hashCode()
  {
    FetchAppointmentResponse.GetAppointmentResponseData localGetAppointmentResponseData = data;
    if (localGetAppointmentResponseData != null) {
      return localGetAppointmentResponseData.hashCode();
    }
    return 0;
  }
  
  public final boolean isValid()
  {
    return m.a(getStatus(), "success", true);
  }
  
  public final Appointment mapToData()
  {
    return data.getAppointment();
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("FetchAppointmentResponse(data=");
    FetchAppointmentResponse.GetAppointmentResponseData localGetAppointmentResponseData = data;
    localStringBuilder.append(localGetAppointmentResponseData);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.FetchAppointmentResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */