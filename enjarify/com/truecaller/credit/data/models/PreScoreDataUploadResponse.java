package com.truecaller.credit.data.models;

import c.g.b.k;
import c.n.m;
import com.truecaller.credit.domain.interactors.onboarding.models.c;

public final class PreScoreDataUploadResponse
  extends BaseApiResponse
  implements Mappable
{
  private final PreScoreDataUploadResponse.PreScoreData data;
  
  public PreScoreDataUploadResponse(PreScoreDataUploadResponse.PreScoreData paramPreScoreData)
  {
    data = paramPreScoreData;
  }
  
  public final PreScoreDataUploadResponse.PreScoreData component1()
  {
    return data;
  }
  
  public final PreScoreDataUploadResponse copy(PreScoreDataUploadResponse.PreScoreData paramPreScoreData)
  {
    PreScoreDataUploadResponse localPreScoreDataUploadResponse = new com/truecaller/credit/data/models/PreScoreDataUploadResponse;
    localPreScoreDataUploadResponse.<init>(paramPreScoreData);
    return localPreScoreDataUploadResponse;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof PreScoreDataUploadResponse;
      if (bool1)
      {
        paramObject = (PreScoreDataUploadResponse)paramObject;
        PreScoreDataUploadResponse.PreScoreData localPreScoreData = data;
        paramObject = data;
        boolean bool2 = k.a(localPreScoreData, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String errorMessage()
  {
    BaseApiResponse.Meta localMeta = getMeta();
    if (localMeta != null) {
      return localMeta.getMessage();
    }
    return null;
  }
  
  public final PreScoreDataUploadResponse.PreScoreData getData()
  {
    return data;
  }
  
  public final int hashCode()
  {
    PreScoreDataUploadResponse.PreScoreData localPreScoreData = data;
    if (localPreScoreData != null) {
      return localPreScoreData.hashCode();
    }
    return 0;
  }
  
  public final boolean isValid()
  {
    return m.a(getStatus(), "success", true);
  }
  
  public final c mapToData()
  {
    c localc = new com/truecaller/credit/domain/interactors/onboarding/models/c;
    Object localObject = data;
    if (localObject != null) {
      localObject = ((PreScoreDataUploadResponse.PreScoreData)localObject).getInitial_offer_score();
    } else {
      localObject = null;
    }
    localc.<init>((Integer)localObject);
    return localc;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("PreScoreDataUploadResponse(data=");
    PreScoreDataUploadResponse.PreScoreData localPreScoreData = data;
    localStringBuilder.append(localPreScoreData);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.PreScoreDataUploadResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */