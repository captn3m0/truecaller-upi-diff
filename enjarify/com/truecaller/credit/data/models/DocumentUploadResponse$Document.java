package com.truecaller.credit.data.models;

import c.g.b.k;

public final class DocumentUploadResponse$Document
{
  private final String data;
  private final String id;
  private final String type;
  
  public DocumentUploadResponse$Document(String paramString1, String paramString2, String paramString3)
  {
    id = paramString1;
    type = paramString2;
    data = paramString3;
  }
  
  public final String component1()
  {
    return id;
  }
  
  public final String component2()
  {
    return type;
  }
  
  public final String component3()
  {
    return data;
  }
  
  public final Document copy(String paramString1, String paramString2, String paramString3)
  {
    Document localDocument = new com/truecaller/credit/data/models/DocumentUploadResponse$Document;
    localDocument.<init>(paramString1, paramString2, paramString3);
    return localDocument;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof Document;
      if (bool1)
      {
        paramObject = (Document)paramObject;
        String str1 = id;
        String str2 = id;
        bool1 = k.a(str1, str2);
        if (bool1)
        {
          str1 = type;
          str2 = type;
          bool1 = k.a(str1, str2);
          if (bool1)
          {
            str1 = data;
            paramObject = data;
            boolean bool2 = k.a(str1, paramObject);
            if (bool2) {
              break label90;
            }
          }
        }
      }
      return false;
    }
    label90:
    return true;
  }
  
  public final String getData()
  {
    return data;
  }
  
  public final String getId()
  {
    return id;
  }
  
  public final String getType()
  {
    return type;
  }
  
  public final int hashCode()
  {
    String str1 = id;
    int i = 0;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = type;
    int k;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    int j = (j + k) * 31;
    str2 = data;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Document(id=");
    String str = id;
    localStringBuilder.append(str);
    localStringBuilder.append(", type=");
    str = type;
    localStringBuilder.append(str);
    localStringBuilder.append(", data=");
    str = data;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.models.DocumentUploadResponse.Document
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */