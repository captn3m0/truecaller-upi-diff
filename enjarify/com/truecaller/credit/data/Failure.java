package com.truecaller.credit.data;

import c.g.b.k;

public final class Failure
  extends Result
{
  private final Throwable error;
  private final String message;
  
  public Failure(Throwable paramThrowable, String paramString)
  {
    super(null);
    error = paramThrowable;
    message = paramString;
  }
  
  public final Throwable component1()
  {
    return error;
  }
  
  public final String component2()
  {
    return message;
  }
  
  public final Failure copy(Throwable paramThrowable, String paramString)
  {
    Failure localFailure = new com/truecaller/credit/data/Failure;
    localFailure.<init>(paramThrowable, paramString);
    return localFailure;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof Failure;
      if (bool1)
      {
        paramObject = (Failure)paramObject;
        Object localObject = error;
        Throwable localThrowable = error;
        bool1 = k.a(localObject, localThrowable);
        if (bool1)
        {
          localObject = message;
          paramObject = message;
          boolean bool2 = k.a(localObject, paramObject);
          if (bool2) {
            break label68;
          }
        }
      }
      return false;
    }
    label68:
    return true;
  }
  
  public final Throwable getError()
  {
    return error;
  }
  
  public final String getMessage()
  {
    return message;
  }
  
  public final int hashCode()
  {
    Throwable localThrowable = error;
    int i = 0;
    int j;
    if (localThrowable != null)
    {
      j = localThrowable.hashCode();
    }
    else
    {
      j = 0;
      localThrowable = null;
    }
    j *= 31;
    String str = message;
    if (str != null) {
      i = str.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Failure(error=");
    Object localObject = error;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", message=");
    localObject = message;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.Failure
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */