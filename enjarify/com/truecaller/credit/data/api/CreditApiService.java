package com.truecaller.credit.data.api;

import com.truecaller.credit.app.core.model.CreditFeatureRequest;
import com.truecaller.credit.data.models.AddAddressRequest;
import com.truecaller.credit.data.models.BankDetailsRequest;
import com.truecaller.credit.data.models.BookSlotRequest;
import com.truecaller.credit.data.models.CheckEmiRequest;
import com.truecaller.credit.data.models.EmiHistoryRequest;
import com.truecaller.credit.data.models.FetchAddressRequest;
import com.truecaller.credit.data.models.FetchSlotRequest;
import com.truecaller.credit.data.models.IFSCSearchRequest;
import com.truecaller.credit.data.models.PoaTypeRequest;
import com.truecaller.credit.data.models.SaveLocationRequest;
import com.truecaller.credit.data.models.ScoreDataUploadRequest;
import com.truecaller.credit.data.models.UserInfoDataRequest;
import com.truecaller.credit.data.models.VerifyFinalOfferOtpRequest;
import com.truecaller.credit.data.models.VerifyPanRequest;
import com.truecaller.credit.data.models.WithDrawLoanRequest;
import com.truecaller.credit.domain.interactors.onboarding.models.PreScoreDataRequest;
import e.b;
import okhttp3.ac;
import okhttp3.x.b;

public abstract interface CreditApiService
{
  public abstract b addAddress(AddAddressRequest paramAddAddressRequest);
  
  public abstract b addBankAccount(BankDetailsRequest paramBankDetailsRequest);
  
  public abstract b bookSlot(BookSlotRequest paramBookSlotRequest);
  
  public abstract b checkCreditLineStatus();
  
  public abstract b fetchAccountDetails();
  
  public abstract b fetchAddress(FetchAddressRequest paramFetchAddressRequest);
  
  public abstract b fetchAppointment();
  
  public abstract b fetchCreditLineDetails();
  
  public abstract b fetchEmiHistory(EmiHistoryRequest paramEmiHistoryRequest);
  
  public abstract b fetchEmiList(CheckEmiRequest paramCheckEmiRequest);
  
  public abstract b fetchFinalOfferDetails();
  
  public abstract b fetchIFSCSearchResults(IFSCSearchRequest paramIFSCSearchRequest);
  
  public abstract b fetchInitialOffer();
  
  public abstract b fetchLoanCategories();
  
  public abstract b fetchLoanHistory(String paramString);
  
  public abstract b fetchPoaDetails();
  
  public abstract b fetchPoaTypes(PoaTypeRequest paramPoaTypeRequest);
  
  public abstract b fetchScoreDataRules();
  
  public abstract b fetchSlots(FetchSlotRequest paramFetchSlotRequest);
  
  public abstract b fetchSupportedCities();
  
  public abstract b notifyUnsupportedLocation(SaveLocationRequest paramSaveLocationRequest);
  
  public abstract b requestFinalOfferOtp();
  
  public abstract b requestLoan(WithDrawLoanRequest paramWithDrawLoanRequest);
  
  public abstract b resetCredit(String paramString);
  
  public abstract b syncBanner(String paramString);
  
  public abstract b syncFeatures(CreditFeatureRequest paramCreditFeatureRequest, String paramString);
  
  public abstract b uploadDocument(ac paramac, x.b paramb);
  
  public abstract b uploadPreScoreData(PreScoreDataRequest paramPreScoreDataRequest, String paramString);
  
  public abstract b uploadScoreData(ScoreDataUploadRequest paramScoreDataUploadRequest);
  
  public abstract b uploadUserDetails(UserInfoDataRequest paramUserInfoDataRequest);
  
  public abstract b verifyFinalOfferOtp(VerifyFinalOfferOtpRequest paramVerifyFinalOfferOtpRequest);
  
  public abstract b verifyPan(VerifyPanRequest paramVerifyPanRequest);
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.api.CreditApiService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */