package com.truecaller.credit.data.api;

import com.truecaller.credit.j;
import dagger.a.d;
import javax.inject.Provider;

public final class CreditUserAgentInterceptor_Factory
  implements d
{
  private final Provider payHelperProvider;
  
  public CreditUserAgentInterceptor_Factory(Provider paramProvider)
  {
    payHelperProvider = paramProvider;
  }
  
  public static CreditUserAgentInterceptor_Factory create(Provider paramProvider)
  {
    CreditUserAgentInterceptor_Factory localCreditUserAgentInterceptor_Factory = new com/truecaller/credit/data/api/CreditUserAgentInterceptor_Factory;
    localCreditUserAgentInterceptor_Factory.<init>(paramProvider);
    return localCreditUserAgentInterceptor_Factory;
  }
  
  public static CreditUserAgentInterceptor newCreditUserAgentInterceptor(j paramj)
  {
    CreditUserAgentInterceptor localCreditUserAgentInterceptor = new com/truecaller/credit/data/api/CreditUserAgentInterceptor;
    localCreditUserAgentInterceptor.<init>(paramj);
    return localCreditUserAgentInterceptor;
  }
  
  public final CreditUserAgentInterceptor get()
  {
    CreditUserAgentInterceptor localCreditUserAgentInterceptor = new com/truecaller/credit/data/api/CreditUserAgentInterceptor;
    j localj = (j)payHelperProvider.get();
    localCreditUserAgentInterceptor.<init>(localj);
    return localCreditUserAgentInterceptor;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.api.CreditUserAgentInterceptor_Factory
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */