package com.truecaller.credit.data.api;

import c.d.c;
import c.o;
import com.truecaller.credit.data.Failure;
import com.truecaller.credit.data.Result;
import com.truecaller.credit.data.Success;
import com.truecaller.credit.data.models.Mappable;
import e.b;
import e.d;
import e.h;
import e.r;
import kotlinx.coroutines.j;

public final class RetrofitExtensionsKt$getResult$2$1
  implements d
{
  RetrofitExtensionsKt$getResult$2$1(j paramj) {}
  
  public final void onFailure(b paramb, Throwable paramThrowable)
  {
    paramb = $it;
    boolean bool = paramb.b();
    if (bool)
    {
      paramb = (c)$it;
      Failure localFailure = new com/truecaller/credit/data/Failure;
      String str;
      if (paramThrowable != null) {
        str = paramThrowable.getLocalizedMessage();
      } else {
        str = null;
      }
      localFailure.<init>(paramThrowable, str);
      paramThrowable = o.a;
      paramThrowable = o.d(localFailure);
      paramb.b(paramThrowable);
    }
  }
  
  public final void onResponse(b paramb, r paramr)
  {
    Object localObject1;
    Object localObject2;
    if (paramr != null)
    {
      paramb = (Mappable)paramr.e();
      if (paramb != null)
      {
        localObject1 = $it;
        boolean bool1 = ((j)localObject1).b();
        if (bool1)
        {
          localObject1 = (c)$it;
          boolean bool2 = paramb.isValid();
          if (bool2)
          {
            localObject2 = new com/truecaller/credit/data/Success;
            paramb = paramb.mapToData();
            ((Success)localObject2).<init>(paramb);
            localObject2 = (Result)localObject2;
          }
          else
          {
            localObject2 = new com/truecaller/credit/data/Failure;
            Throwable localThrowable = new java/lang/Throwable;
            String str = paramb.errorMessage();
            localThrowable.<init>(str);
            paramb = paramb.errorMessage();
            ((Failure)localObject2).<init>(localThrowable, paramb);
            localObject2 = (Result)localObject2;
          }
          paramb = o.a;
          paramb = o.d(localObject2);
          ((c)localObject1).b(paramb);
        }
      }
    }
    if (paramr != null)
    {
      paramb = paramr.f();
      if (paramb != null)
      {
        paramb = $it;
        boolean bool3 = paramb.b();
        if (bool3)
        {
          paramb = (c)$it;
          localObject1 = new com/truecaller/credit/data/Failure;
          localObject2 = new e/h;
          ((h)localObject2).<init>(paramr);
          localObject2 = (Throwable)localObject2;
          ((Failure)localObject1).<init>((Throwable)localObject2, null);
          paramr = o.a;
          paramr = o.d(localObject1);
          paramb.b(paramr);
        }
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.api.RetrofitExtensionsKt.getResult.2.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */