package com.truecaller.credit.data.api;

import c.g.b.k;
import com.truecaller.credit.R.string;
import com.truecaller.credit.app.ui.errors.a;
import com.truecaller.utils.i;
import com.truecaller.utils.n;
import java.io.IOException;
import java.net.SocketException;
import okhttp3.ab;
import okhttp3.ad;
import okhttp3.v;
import okhttp3.v.a;

public final class CreditNetworkInterceptor
  implements v
{
  private final a creditErrorHandler;
  private final i networkUtil;
  private final n resourceProvider;
  
  public CreditNetworkInterceptor(a parama, n paramn, i parami)
  {
    creditErrorHandler = parama;
    resourceProvider = paramn;
    networkUtil = parami;
  }
  
  public final ad intercept(v.a parama)
  {
    k.b(parama, "chain");
    Object localObject1 = parama.a();
    Object localObject2 = ((ab)localObject1).a("api_tag");
    Object localObject3 = null;
    int i;
    if (localObject2 != null)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject2 = null;
    }
    Object localObject4 = networkUtil;
    boolean bool = ((i)localObject4).a();
    if (!bool)
    {
      if (i == 0)
      {
        parama = creditErrorHandler;
        localObject1 = resourceProvider;
        i = R.string.error_no_internet_available;
        localObject4 = new Object[0];
        localObject1 = ((n)localObject1).a(i, (Object[])localObject4);
        parama.a("no_network_error", null, (String)localObject1);
        parama = new java/io/IOException;
        localObject1 = resourceProvider;
        i = R.string.error_no_internet_available;
        localObject3 = new Object[0];
        localObject1 = ((n)localObject1).a(i, (Object[])localObject3);
        parama.<init>((String)localObject1);
        throw ((Throwable)parama);
      }
    }
    else {
      try
      {
        localObject4 = parama.a((ab)localObject1);
        localObject5 = "chain.proceed(request)";
        k.a(localObject4, (String)localObject5);
        return (ad)localObject4;
      }
      catch (SocketException localSocketException)
      {
        Object localObject5;
        if (i == 0)
        {
          localObject2 = creditErrorHandler;
          localObject4 = "no_network_error";
          localObject5 = resourceProvider;
          int j = R.string.server_error_message;
          localObject3 = new Object[0];
          localObject3 = ((n)localObject5).a(j, (Object[])localObject3);
          ((a)localObject2).a((String)localObject4, null, (String)localObject3);
        }
      }
    }
    parama = parama.a((ab)localObject1);
    k.a(parama, "chain.proceed(request)");
    return parama;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.api.CreditNetworkInterceptor
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */