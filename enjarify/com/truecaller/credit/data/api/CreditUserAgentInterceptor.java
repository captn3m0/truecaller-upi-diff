package com.truecaller.credit.data.api;

import android.os.Build.VERSION;
import c.g.b.k;
import com.truecaller.credit.j;
import okhttp3.ab;
import okhttp3.ab.a;
import okhttp3.ad;
import okhttp3.v;
import okhttp3.v.a;

public final class CreditUserAgentInterceptor
  implements v
{
  private final String androidVersion;
  private final j payHelper;
  
  public CreditUserAgentInterceptor(j paramj)
  {
    payHelper = paramj;
    paramj = Build.VERSION.RELEASE;
    k.a(paramj, "Build.VERSION.RELEASE");
    paramj = CreditUserAgentInterceptorKt.access$stripNonAsciiChars(paramj);
    androidVersion = paramj;
  }
  
  public final ad intercept(v.a parama)
  {
    k.b(parama, "chain");
    Object localObject1 = parama.a().e().b("User-Agent");
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("android:");
    String str = androidVersion;
    ((StringBuilder)localObject2).append(str);
    ((StringBuilder)localObject2).append("/10.41.6 tc_pay_user:");
    boolean bool = payHelper.g();
    ((StringBuilder)localObject2).append(bool);
    localObject2 = ((StringBuilder)localObject2).toString();
    localObject1 = ((ab.a)localObject1).b("User-Agent", (String)localObject2).a();
    parama = parama.a((ab)localObject1);
    k.a(parama, "chain.proceed(userAgent)");
    return parama;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.api.CreditUserAgentInterceptor
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */