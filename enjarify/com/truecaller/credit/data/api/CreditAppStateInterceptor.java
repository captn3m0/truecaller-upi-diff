package com.truecaller.credit.data.api;

import c.g.b.k;
import com.google.gson.l;
import com.google.gson.o;
import com.google.gson.q;
import com.truecaller.credit.app.core.g;
import okhttp3.ab;
import okhttp3.ab.a;
import okhttp3.ad;
import okhttp3.ae;
import okhttp3.v;
import okhttp3.v.a;

public final class CreditAppStateInterceptor
  implements v
{
  private final com.truecaller.credit.app.ui.a.a creditBannerManager;
  private final g creditSettings;
  private final q jsonParser;
  
  public CreditAppStateInterceptor(q paramq, com.truecaller.credit.app.ui.a.a parama, g paramg)
  {
    jsonParser = paramq;
    creditBannerManager = parama;
    creditSettings = paramg;
  }
  
  public final ad intercept(v.a parama)
  {
    k.b(parama, "chain");
    Object localObject1 = parama.a();
    Object localObject2 = ((ab)localObject1).a("api_tag");
    String str;
    if (localObject2 != null)
    {
      localObject2 = ((ab)localObject1).e();
      str = "api_tag";
      localObject2 = ((ab.a)localObject2).b(str);
      ((ab.a)localObject2).a();
    }
    parama = parama.a((ab)localObject1);
    localObject1 = "response";
    k.a(parama, (String)localObject1);
    boolean bool1 = parama.c();
    if (bool1)
    {
      long l = 1048576L;
      localObject1 = parama.a(l);
      localObject2 = null;
      if (localObject1 != null)
      {
        localObject1 = ((ae)localObject1).g();
      }
      else
      {
        bool1 = false;
        localObject1 = null;
      }
      localObject1 = q.a((String)localObject1);
      k.a(localObject1, "jsonParser.parse(responseBody)");
      localObject1 = ((l)localObject1).i();
      str = "data";
      boolean bool2 = ((o)localObject1).a(str);
      if (bool2)
      {
        str = "data";
        localObject1 = ((o)localObject1).b(str);
        if (localObject1 != null)
        {
          localObject1 = ((l)localObject1).i();
          str = "app_state";
          localObject1 = ((o)localObject1).b(str);
          if (localObject1 != null)
          {
            localObject1 = com.truecaller.credit.app.ui.assist.a.a((l)localObject1);
            localObject2 = localObject1;
          }
        }
      }
      localObject1 = creditSettings;
      str = "credit_app_state";
      localObject1 = ((g)localObject1).a(str);
      bool1 = k.a(localObject2, localObject1) ^ true;
      if (bool1)
      {
        localObject1 = creditSettings;
        str = "credit_app_state";
        ((g)localObject1).a(str, (String)localObject2);
        localObject1 = creditBannerManager;
        ((com.truecaller.credit.app.ui.a.a)localObject1).b();
      }
    }
    return parama;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.api.CreditAppStateInterceptor
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */