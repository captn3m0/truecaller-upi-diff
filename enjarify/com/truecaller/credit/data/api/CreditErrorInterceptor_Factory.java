package com.truecaller.credit.data.api;

import com.truecaller.credit.app.ui.errors.a;
import com.truecaller.utils.n;
import dagger.a.d;
import javax.inject.Provider;

public final class CreditErrorInterceptor_Factory
  implements d
{
  private final Provider creditErrorHandlerProvider;
  private final Provider resourceProvider;
  
  public CreditErrorInterceptor_Factory(Provider paramProvider1, Provider paramProvider2)
  {
    creditErrorHandlerProvider = paramProvider1;
    resourceProvider = paramProvider2;
  }
  
  public static CreditErrorInterceptor_Factory create(Provider paramProvider1, Provider paramProvider2)
  {
    CreditErrorInterceptor_Factory localCreditErrorInterceptor_Factory = new com/truecaller/credit/data/api/CreditErrorInterceptor_Factory;
    localCreditErrorInterceptor_Factory.<init>(paramProvider1, paramProvider2);
    return localCreditErrorInterceptor_Factory;
  }
  
  public static CreditErrorInterceptor newCreditErrorInterceptor(a parama, n paramn)
  {
    CreditErrorInterceptor localCreditErrorInterceptor = new com/truecaller/credit/data/api/CreditErrorInterceptor;
    localCreditErrorInterceptor.<init>(parama, paramn);
    return localCreditErrorInterceptor;
  }
  
  public final CreditErrorInterceptor get()
  {
    CreditErrorInterceptor localCreditErrorInterceptor = new com/truecaller/credit/data/api/CreditErrorInterceptor;
    a locala = (a)creditErrorHandlerProvider.get();
    n localn = (n)resourceProvider.get();
    localCreditErrorInterceptor.<init>(locala, localn);
    return localCreditErrorInterceptor;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.api.CreditErrorInterceptor_Factory
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */