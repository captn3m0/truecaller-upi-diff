package com.truecaller.credit.data.api;

import c.g.b.k;
import com.google.gson.l;
import com.google.gson.o;
import com.google.gson.q;
import okhttp3.ab;
import okhttp3.ad;
import okhttp3.ad.a;
import okhttp3.ae;
import okhttp3.v;
import okhttp3.v.a;
import okhttp3.w;

public final class CreditCustomErrorInterceptor
  implements v
{
  private final com.truecaller.credit.app.ui.errors.a creditErrorHandler;
  private final q jsonParser;
  
  public CreditCustomErrorInterceptor(q paramq, com.truecaller.credit.app.ui.errors.a parama)
  {
    jsonParser = paramq;
    creditErrorHandler = parama;
  }
  
  public final ad intercept(v.a parama)
  {
    k.b(parama, "chain");
    Object localObject1 = parama.a();
    Object localObject2 = ((ab)localObject1).a("api_tag");
    boolean bool1 = true;
    int i;
    if (localObject2 != null)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject2 = null;
    }
    parama = parama.a((ab)localObject1);
    localObject1 = "response";
    k.a(parama, (String)localObject1);
    boolean bool2 = parama.c();
    if (bool2)
    {
      localObject1 = parama.d();
      if (localObject1 != null)
      {
        localObject1 = ((ae)localObject1).g();
      }
      else
      {
        bool2 = false;
        localObject1 = null;
      }
      Object localObject3 = q.a((String)localObject1);
      k.a(localObject3, "jsonParser.parse(responseBody)");
      localObject3 = ((l)localObject3).i();
      String str1 = "meta";
      boolean bool3 = ((o)localObject3).a(str1);
      if (bool3)
      {
        localObject1 = ((o)localObject3).b("meta");
        if (localObject1 != null)
        {
          localObject1 = ((l)localObject1).i();
          str1 = "type";
          localObject1 = ((o)localObject1).b(str1);
          if (localObject1 != null)
          {
            localObject1 = com.truecaller.credit.app.ui.assist.a.a((l)localObject1);
            break label170;
          }
        }
        bool2 = false;
        localObject1 = null;
        label170:
        str1 = "success";
        bool3 = k.a(localObject1, str1);
        bool1 ^= bool3;
        if (bool1)
        {
          str2 = "status";
          str1 = "Failure";
          ((o)localObject3).a(str2, str1);
        }
        String str2 = ((o)localObject3).toString();
        if (i == 0)
        {
          localObject2 = creditErrorHandler;
          localObject3 = ((o)localObject3).b("meta");
          str1 = "responseObject.get(CreditErrorConstants.KEY_META)";
          k.a(localObject3, str1);
          localObject3 = ((l)localObject3).i();
          ((com.truecaller.credit.app.ui.errors.a)localObject2).a((String)localObject1, (o)localObject3, null);
        }
        localObject1 = str2;
      }
      localObject2 = parama.e();
      parama = ae.a(w.b(parama.a("Content-Type")), (String)localObject1);
      parama = ((ad.a)localObject2).a(parama).a();
      k.a(parama, "response.newBuilder().bo…), responseBody)).build()");
      return parama;
    }
    return parama;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.api.CreditCustomErrorInterceptor
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */