package com.truecaller.credit.data.api;

public final class CreditResetStateInterceptorKt
{
  private static final String ACTION = "action";
  private static final String BANNER = "banner";
  private static final String BUTTON = "button";
  private static final String DATA = "data";
  private static final String NEXT_PAGE = "next_page";
  private static final String RESET_FLOW = "reset_flow";
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.api.CreditResetStateInterceptorKt
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */