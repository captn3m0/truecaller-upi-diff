package com.truecaller.credit.data.api;

import com.google.gson.q;
import com.truecaller.credit.app.ui.errors.a;
import dagger.a.d;
import javax.inject.Provider;

public final class CreditCustomErrorInterceptor_Factory
  implements d
{
  private final Provider creditErrorHandlerProvider;
  private final Provider jsonParserProvider;
  
  public CreditCustomErrorInterceptor_Factory(Provider paramProvider1, Provider paramProvider2)
  {
    jsonParserProvider = paramProvider1;
    creditErrorHandlerProvider = paramProvider2;
  }
  
  public static CreditCustomErrorInterceptor_Factory create(Provider paramProvider1, Provider paramProvider2)
  {
    CreditCustomErrorInterceptor_Factory localCreditCustomErrorInterceptor_Factory = new com/truecaller/credit/data/api/CreditCustomErrorInterceptor_Factory;
    localCreditCustomErrorInterceptor_Factory.<init>(paramProvider1, paramProvider2);
    return localCreditCustomErrorInterceptor_Factory;
  }
  
  public static CreditCustomErrorInterceptor newCreditCustomErrorInterceptor(q paramq, a parama)
  {
    CreditCustomErrorInterceptor localCreditCustomErrorInterceptor = new com/truecaller/credit/data/api/CreditCustomErrorInterceptor;
    localCreditCustomErrorInterceptor.<init>(paramq, parama);
    return localCreditCustomErrorInterceptor;
  }
  
  public final CreditCustomErrorInterceptor get()
  {
    CreditCustomErrorInterceptor localCreditCustomErrorInterceptor = new com/truecaller/credit/data/api/CreditCustomErrorInterceptor;
    q localq = (q)jsonParserProvider.get();
    a locala = (a)creditErrorHandlerProvider.get();
    localCreditCustomErrorInterceptor.<init>(localq, locala);
    return localCreditCustomErrorInterceptor;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.api.CreditCustomErrorInterceptor_Factory
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */