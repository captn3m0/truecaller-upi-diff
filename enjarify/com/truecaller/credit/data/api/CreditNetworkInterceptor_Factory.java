package com.truecaller.credit.data.api;

import com.truecaller.credit.app.ui.errors.a;
import com.truecaller.utils.i;
import com.truecaller.utils.n;
import dagger.a.d;
import javax.inject.Provider;

public final class CreditNetworkInterceptor_Factory
  implements d
{
  private final Provider creditErrorHandlerProvider;
  private final Provider networkUtilProvider;
  private final Provider resourceProvider;
  
  public CreditNetworkInterceptor_Factory(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    creditErrorHandlerProvider = paramProvider1;
    resourceProvider = paramProvider2;
    networkUtilProvider = paramProvider3;
  }
  
  public static CreditNetworkInterceptor_Factory create(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    CreditNetworkInterceptor_Factory localCreditNetworkInterceptor_Factory = new com/truecaller/credit/data/api/CreditNetworkInterceptor_Factory;
    localCreditNetworkInterceptor_Factory.<init>(paramProvider1, paramProvider2, paramProvider3);
    return localCreditNetworkInterceptor_Factory;
  }
  
  public static CreditNetworkInterceptor newCreditNetworkInterceptor(a parama, n paramn, i parami)
  {
    CreditNetworkInterceptor localCreditNetworkInterceptor = new com/truecaller/credit/data/api/CreditNetworkInterceptor;
    localCreditNetworkInterceptor.<init>(parama, paramn, parami);
    return localCreditNetworkInterceptor;
  }
  
  public final CreditNetworkInterceptor get()
  {
    CreditNetworkInterceptor localCreditNetworkInterceptor = new com/truecaller/credit/data/api/CreditNetworkInterceptor;
    a locala = (a)creditErrorHandlerProvider.get();
    n localn = (n)resourceProvider.get();
    i locali = (i)networkUtilProvider.get();
    localCreditNetworkInterceptor.<init>(locala, localn, locali);
    return localCreditNetworkInterceptor;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.api.CreditNetworkInterceptor_Factory
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */