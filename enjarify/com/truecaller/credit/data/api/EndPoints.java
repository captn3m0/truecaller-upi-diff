package com.truecaller.credit.data.api;

public final class EndPoints
{
  public static final String ACCEPT_OFFER = "accept-offer";
  public static final String ACCOUNT_VALIDATION_DETAILS = "account-validation-details";
  public static final String ADD_ADDRESS = "upload-address";
  public static final String ADD_BANK_ACCOUNT = "add-bank-account";
  public static final String BOOK_SLOT = "book-slot";
  public static final String CALCULATE_EMI = "calculate-emi";
  public static final String CALCULATE_INITIAL_OFFER = "calculate-initial-offer";
  public static final String CREDIT_LINE_DETAILS = "credit-line-details";
  public static final String CREDIT_LINE_STATUS = "credit-line-status";
  public static final EndPoints.Companion Companion;
  public static final String EMI_LIST = "emi-list";
  public static final String FEATURE_CONFIG = "feature-config";
  public static final String FETCH_APPOINTMENT = "get-appointment";
  public static final String FINAL_OFFER_DETAILS = "final-offer-details";
  public static final String GET_ADDRESS = "get-address";
  public static final String GET_INITIAL_OFFER = "get-initial-offer";
  public static final String GET_SLOTS_LIST = "list-slots";
  public static final String HOME_BANNER = "home-banner";
  public static final String LOAN_CATEGORIES = "loan-categories";
  public static final String LOAN_SUMMARY_LIST = "loan-summary-list";
  public static final String NOTIFY_UNSUPPORTED_LOCATION = "save-location";
  public static final String PARSE_RULES = "parser-rules";
  public static final String POA_DETAILS = "poa-details";
  public static final String POA_TYPES = "kyc-types";
  public static final String REQUEST_LOAN = "request-loan";
  public static final String REQUEST_OTP = "request-otp";
  public static final String RESET_CREDIT = "clean-slate";
  public static final String SEARCH_IFSC = "search-ifsc";
  public static final String SUBMIT_PERSONAL_DETAILS = "submit-personal-details";
  public static final String SUBMIT_USER_DATA = "submit-user-data";
  public static final String SUPPORTED_LOCATIONS = "supported-locations";
  public static final String UPLOAD_DOCUMENT = "upload-document";
  public static final String VALIDATE_PAN = "validate-pan";
  
  static
  {
    EndPoints.Companion localCompanion = new com/truecaller/credit/data/api/EndPoints$Companion;
    localCompanion.<init>(null);
    Companion = localCompanion;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.api.EndPoints
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */