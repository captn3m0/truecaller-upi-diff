package com.truecaller.credit.data.api;

import c.n.k;

public final class CreditUserAgentInterceptorKt
{
  private static final String stripNonAsciiChars(String paramString)
  {
    paramString = (CharSequence)paramString;
    k localk = new c/n/k;
    localk.<init>("[^\\x20-\\x7E]");
    return localk.a(paramString, "");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.api.CreditUserAgentInterceptorKt
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */