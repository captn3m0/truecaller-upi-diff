package com.truecaller.credit.data.api;

public final class CreditAppStateInterceptorKt
{
  private static final String APP_STATE = "app_state";
  private static final String DATA = "data";
  public static final long MB_1 = 1048576L;
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.api.CreditAppStateInterceptorKt
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */