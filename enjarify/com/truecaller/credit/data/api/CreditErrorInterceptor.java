package com.truecaller.credit.data.api;

import c.g.b.k;
import com.truecaller.credit.R.string;
import com.truecaller.credit.app.ui.errors.a;
import com.truecaller.utils.n;
import okhttp3.ab;
import okhttp3.ad;
import okhttp3.v;
import okhttp3.v.a;

public final class CreditErrorInterceptor
  implements v
{
  private final a creditErrorHandler;
  private final n resourceProvider;
  
  public CreditErrorInterceptor(a parama, n paramn)
  {
    creditErrorHandler = parama;
    resourceProvider = paramn;
  }
  
  public final ad intercept(v.a parama)
  {
    k.b(parama, "chain");
    Object localObject1 = parama.a();
    String str = ((ab)localObject1).a("api_tag");
    Object localObject2 = null;
    int i;
    if (str != null)
    {
      i = 1;
    }
    else
    {
      i = 0;
      str = null;
    }
    parama = parama.a((ab)localObject1);
    localObject1 = "response";
    k.a(parama, (String)localObject1);
    boolean bool = parama.c();
    if ((!bool) && (i == 0))
    {
      localObject1 = creditErrorHandler;
      str = "error";
      n localn = resourceProvider;
      int j = R.string.server_error_message;
      localObject2 = new Object[0];
      localObject2 = localn.a(j, (Object[])localObject2);
      ((a)localObject1).a(str, null, (String)localObject2);
    }
    return parama;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.api.CreditErrorInterceptor
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */