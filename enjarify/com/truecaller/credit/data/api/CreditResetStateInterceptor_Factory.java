package com.truecaller.credit.data.api;

import com.google.gson.q;
import com.truecaller.credit.app.core.g;
import dagger.a.d;
import javax.inject.Provider;

public final class CreditResetStateInterceptor_Factory
  implements d
{
  private final Provider creditSettingsProvider;
  private final Provider jsonParserProvider;
  
  public CreditResetStateInterceptor_Factory(Provider paramProvider1, Provider paramProvider2)
  {
    jsonParserProvider = paramProvider1;
    creditSettingsProvider = paramProvider2;
  }
  
  public static CreditResetStateInterceptor_Factory create(Provider paramProvider1, Provider paramProvider2)
  {
    CreditResetStateInterceptor_Factory localCreditResetStateInterceptor_Factory = new com/truecaller/credit/data/api/CreditResetStateInterceptor_Factory;
    localCreditResetStateInterceptor_Factory.<init>(paramProvider1, paramProvider2);
    return localCreditResetStateInterceptor_Factory;
  }
  
  public static CreditResetStateInterceptor newCreditResetStateInterceptor(q paramq, g paramg)
  {
    CreditResetStateInterceptor localCreditResetStateInterceptor = new com/truecaller/credit/data/api/CreditResetStateInterceptor;
    localCreditResetStateInterceptor.<init>(paramq, paramg);
    return localCreditResetStateInterceptor;
  }
  
  public final CreditResetStateInterceptor get()
  {
    CreditResetStateInterceptor localCreditResetStateInterceptor = new com/truecaller/credit/data/api/CreditResetStateInterceptor;
    q localq = (q)jsonParserProvider.get();
    g localg = (g)creditSettingsProvider.get();
    localCreditResetStateInterceptor.<init>(localq, localg);
    return localCreditResetStateInterceptor;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.api.CreditResetStateInterceptor_Factory
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */