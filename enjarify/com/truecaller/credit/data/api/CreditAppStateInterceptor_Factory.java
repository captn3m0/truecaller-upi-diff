package com.truecaller.credit.data.api;

import com.google.gson.q;
import com.truecaller.credit.app.core.g;
import com.truecaller.credit.app.ui.a.a;
import dagger.a.d;
import javax.inject.Provider;

public final class CreditAppStateInterceptor_Factory
  implements d
{
  private final Provider creditBannerManagerProvider;
  private final Provider creditSettingsProvider;
  private final Provider jsonParserProvider;
  
  public CreditAppStateInterceptor_Factory(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    jsonParserProvider = paramProvider1;
    creditBannerManagerProvider = paramProvider2;
    creditSettingsProvider = paramProvider3;
  }
  
  public static CreditAppStateInterceptor_Factory create(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    CreditAppStateInterceptor_Factory localCreditAppStateInterceptor_Factory = new com/truecaller/credit/data/api/CreditAppStateInterceptor_Factory;
    localCreditAppStateInterceptor_Factory.<init>(paramProvider1, paramProvider2, paramProvider3);
    return localCreditAppStateInterceptor_Factory;
  }
  
  public static CreditAppStateInterceptor newCreditAppStateInterceptor(q paramq, a parama, g paramg)
  {
    CreditAppStateInterceptor localCreditAppStateInterceptor = new com/truecaller/credit/data/api/CreditAppStateInterceptor;
    localCreditAppStateInterceptor.<init>(paramq, parama, paramg);
    return localCreditAppStateInterceptor;
  }
  
  public final CreditAppStateInterceptor get()
  {
    CreditAppStateInterceptor localCreditAppStateInterceptor = new com/truecaller/credit/data/api/CreditAppStateInterceptor;
    q localq = (q)jsonParserProvider.get();
    a locala = (a)creditBannerManagerProvider.get();
    g localg = (g)creditSettingsProvider.get();
    localCreditAppStateInterceptor.<init>(localq, locala, localg);
    return localCreditAppStateInterceptor;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.api.CreditAppStateInterceptor_Factory
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */