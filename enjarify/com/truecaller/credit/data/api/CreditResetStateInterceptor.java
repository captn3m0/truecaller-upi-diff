package com.truecaller.credit.data.api;

import c.g.b.k;
import com.google.gson.l;
import com.google.gson.o;
import com.google.gson.q;
import com.truecaller.credit.app.core.g;
import com.truecaller.credit.app.ui.assist.a;
import okhttp3.ab;
import okhttp3.ad;
import okhttp3.ae;
import okhttp3.v;
import okhttp3.v.a;

public final class CreditResetStateInterceptor
  implements v
{
  private final g creditSettings;
  private final q jsonParser;
  
  public CreditResetStateInterceptor(q paramq, g paramg)
  {
    jsonParser = paramq;
    creditSettings = paramg;
  }
  
  public final ad intercept(v.a parama)
  {
    k.b(parama, "chain");
    Object localObject1 = parama.a();
    parama = parama.a((ab)localObject1);
    Object localObject2 = "api_tag";
    localObject1 = ((ab)localObject1).a((String)localObject2);
    int i;
    if (localObject1 != null)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject1 = null;
    }
    localObject2 = "response";
    k.a(parama, (String)localObject2);
    boolean bool1 = parama.c();
    if (bool1)
    {
      long l = 1048576L;
      localObject2 = parama.a(l);
      String str1 = null;
      if (localObject2 != null)
      {
        localObject2 = ((ae)localObject2).g();
      }
      else
      {
        bool1 = false;
        localObject2 = null;
      }
      localObject2 = q.a((String)localObject2);
      k.a(localObject2, "jsonParser.parse(responseBody)");
      localObject2 = ((l)localObject2).i();
      String str2 = "meta";
      boolean bool2 = ((o)localObject2).a(str2);
      Object localObject3 = ((o)localObject2).b("data");
      String str3;
      if (localObject3 != null)
      {
        localObject4 = ((l)localObject3).i();
        str3 = "reset_flow";
        localObject4 = ((o)localObject4).b(str3);
        if (localObject4 != null)
        {
          bool3 = ((l)localObject4).h();
          localObject4 = Boolean.valueOf(bool3);
          break label185;
        }
      }
      boolean bool3 = false;
      Object localObject4 = null;
      label185:
      if (localObject4 != null)
      {
        bool3 = ((Boolean)localObject4).booleanValue();
        if (bool3)
        {
          localObject3 = ((l)localObject3).i();
          localObject4 = "banner";
          localObject3 = ((o)localObject3).b((String)localObject4);
          if (localObject3 != null)
          {
            localObject3 = ((l)localObject3).i();
            localObject4 = "button";
            localObject3 = ((o)localObject3).b((String)localObject4);
            if (localObject3 != null)
            {
              localObject3 = ((l)localObject3).i();
              localObject4 = "action";
              localObject3 = ((o)localObject3).b((String)localObject4);
              if (localObject3 != null)
              {
                localObject3 = a.a((l)localObject3);
                break label290;
              }
            }
          }
          localObject3 = null;
          label290:
          localObject4 = creditSettings;
          str3 = "credit_next_page";
          ((g)localObject4).a(str3, (String)localObject3);
        }
      }
      if ((bool2) && (i == 0))
      {
        localObject1 = ((o)localObject2).b("meta");
        if (localObject1 != null)
        {
          localObject1 = ((l)localObject1).i();
          localObject2 = "next_page";
          localObject1 = ((o)localObject1).b((String)localObject2);
          if (localObject1 != null) {
            str1 = a.a((l)localObject1);
          }
        }
        localObject1 = creditSettings;
        localObject2 = "credit_next_page";
        ((g)localObject1).a((String)localObject2, str1);
      }
    }
    return parama;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.api.CreditResetStateInterceptor
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */