package com.truecaller.credit.data.repository;

import c.d.c;
import com.truecaller.credit.app.core.model.CreditFeatureRequest;
import com.truecaller.credit.data.api.CreditApiService;
import com.truecaller.credit.data.api.RetrofitExtensionsKt;
import com.truecaller.credit.data.models.AddAddressRequest;
import com.truecaller.credit.data.models.BankDetailsRequest;
import com.truecaller.credit.data.models.BookSlotRequest;
import com.truecaller.credit.data.models.CheckEmiRequest;
import com.truecaller.credit.data.models.EmiHistoryRequest;
import com.truecaller.credit.data.models.FetchAddressRequest;
import com.truecaller.credit.data.models.FetchSlotRequest;
import com.truecaller.credit.data.models.IFSCSearchRequest;
import com.truecaller.credit.data.models.PoaTypeRequest;
import com.truecaller.credit.data.models.SaveLocationRequest;
import com.truecaller.credit.data.models.ScoreDataUploadRequest;
import com.truecaller.credit.data.models.UserInfoDataRequest;
import com.truecaller.credit.data.models.VerifyFinalOfferOtpRequest;
import com.truecaller.credit.data.models.VerifyPanRequest;
import com.truecaller.credit.data.models.WithDrawLoanRequest;
import com.truecaller.credit.domain.interactors.onboarding.models.PreScoreDataRequest;
import okhttp3.ac;
import okhttp3.x.b;

public final class CreditRepository$Network
  implements CreditRepository
{
  private final CreditApiService creditApiService;
  
  public CreditRepository$Network(CreditApiService paramCreditApiService)
  {
    creditApiService = paramCreditApiService;
  }
  
  public final Object addAddress(AddAddressRequest paramAddAddressRequest, c paramc)
  {
    return RetrofitExtensionsKt.getResult(creditApiService.addAddress(paramAddAddressRequest), paramc);
  }
  
  public final Object addBankAccount(BankDetailsRequest paramBankDetailsRequest, c paramc)
  {
    return RetrofitExtensionsKt.getResult(creditApiService.addBankAccount(paramBankDetailsRequest), paramc);
  }
  
  public final Object bookSlot(BookSlotRequest paramBookSlotRequest, c paramc)
  {
    return RetrofitExtensionsKt.getResult(creditApiService.bookSlot(paramBookSlotRequest), paramc);
  }
  
  public final Object checkCreditLineStatus(c paramc)
  {
    return RetrofitExtensionsKt.getResult(creditApiService.checkCreditLineStatus(), paramc);
  }
  
  public final Object fetchAccountDetails(c paramc)
  {
    return RetrofitExtensionsKt.getResult(creditApiService.fetchAccountDetails(), paramc);
  }
  
  public final Object fetchAddress(FetchAddressRequest paramFetchAddressRequest, c paramc)
  {
    return RetrofitExtensionsKt.getResult(creditApiService.fetchAddress(paramFetchAddressRequest), paramc);
  }
  
  public final Object fetchAppointment(c paramc)
  {
    return RetrofitExtensionsKt.getResult(creditApiService.fetchAppointment(), paramc);
  }
  
  public final Object fetchCreditLineDetails(c paramc)
  {
    return RetrofitExtensionsKt.getResult(creditApiService.fetchCreditLineDetails(), paramc);
  }
  
  public final Object fetchEmiHistory(EmiHistoryRequest paramEmiHistoryRequest, c paramc)
  {
    return RetrofitExtensionsKt.getResult(creditApiService.fetchEmiHistory(paramEmiHistoryRequest), paramc);
  }
  
  public final Object fetchEmiList(CheckEmiRequest paramCheckEmiRequest, c paramc)
  {
    return RetrofitExtensionsKt.getResult(creditApiService.fetchEmiList(paramCheckEmiRequest), paramc);
  }
  
  public final Object fetchFinalOfferDetails(c paramc)
  {
    return RetrofitExtensionsKt.getResult(creditApiService.fetchFinalOfferDetails(), paramc);
  }
  
  public final Object fetchIFSCSearchResults(IFSCSearchRequest paramIFSCSearchRequest, c paramc)
  {
    return RetrofitExtensionsKt.getResult(creditApiService.fetchIFSCSearchResults(paramIFSCSearchRequest), paramc);
  }
  
  public final Object fetchInitialOffer(c paramc)
  {
    return RetrofitExtensionsKt.getResult(creditApiService.fetchInitialOffer(), paramc);
  }
  
  public final Object fetchLoanCategories(c paramc)
  {
    return RetrofitExtensionsKt.getResult(creditApiService.fetchLoanCategories(), paramc);
  }
  
  public final Object fetchLoanHistory(c paramc)
  {
    return RetrofitExtensionsKt.getResult(creditApiService.fetchLoanHistory("background"), paramc);
  }
  
  public final Object fetchPoaDetails(c paramc)
  {
    return RetrofitExtensionsKt.getResult(creditApiService.fetchPoaDetails(), paramc);
  }
  
  public final Object fetchPoaTypes(PoaTypeRequest paramPoaTypeRequest, c paramc)
  {
    return RetrofitExtensionsKt.getResult(creditApiService.fetchPoaTypes(paramPoaTypeRequest), paramc);
  }
  
  public final Object fetchScoreDataRules(c paramc)
  {
    return RetrofitExtensionsKt.getResult(creditApiService.fetchScoreDataRules(), paramc);
  }
  
  public final Object fetchSlots(FetchSlotRequest paramFetchSlotRequest, c paramc)
  {
    return RetrofitExtensionsKt.getResult(creditApiService.fetchSlots(paramFetchSlotRequest), paramc);
  }
  
  public final Object fetchSupportedCities(c paramc)
  {
    return RetrofitExtensionsKt.getResult(creditApiService.fetchSupportedCities(), paramc);
  }
  
  public final Object requestFinalOfferOtp(c paramc)
  {
    return RetrofitExtensionsKt.getResult(creditApiService.requestFinalOfferOtp(), paramc);
  }
  
  public final Object requestLoan(WithDrawLoanRequest paramWithDrawLoanRequest, c paramc)
  {
    return RetrofitExtensionsKt.getResult(creditApiService.requestLoan(paramWithDrawLoanRequest), paramc);
  }
  
  public final Object resetCredit(c paramc)
  {
    return RetrofitExtensionsKt.getResult(creditApiService.resetCredit("background"), paramc);
  }
  
  public final Object saveLocation(SaveLocationRequest paramSaveLocationRequest, c paramc)
  {
    return RetrofitExtensionsKt.getResult(creditApiService.notifyUnsupportedLocation(paramSaveLocationRequest), paramc);
  }
  
  public final Object syncBanner(c paramc)
  {
    return RetrofitExtensionsKt.getResult(creditApiService.syncBanner("background"), paramc);
  }
  
  public final Object syncFeatures(CreditFeatureRequest paramCreditFeatureRequest, c paramc)
  {
    return RetrofitExtensionsKt.getResult(creditApiService.syncFeatures(paramCreditFeatureRequest, "background"), paramc);
  }
  
  public final Object uploadDocument(ac paramac, x.b paramb, c paramc)
  {
    return RetrofitExtensionsKt.getResult(creditApiService.uploadDocument(paramac, paramb), paramc);
  }
  
  public final Object uploadPreScoreData(PreScoreDataRequest paramPreScoreDataRequest, c paramc)
  {
    return RetrofitExtensionsKt.getResult(creditApiService.uploadPreScoreData(paramPreScoreDataRequest, "background"), paramc);
  }
  
  public final Object uploadScoreData(ScoreDataUploadRequest paramScoreDataUploadRequest, c paramc)
  {
    return RetrofitExtensionsKt.getResult(creditApiService.uploadScoreData(paramScoreDataUploadRequest), paramc);
  }
  
  public final Object uploadUserDetails(UserInfoDataRequest paramUserInfoDataRequest, c paramc)
  {
    return RetrofitExtensionsKt.getResult(creditApiService.uploadUserDetails(paramUserInfoDataRequest), paramc);
  }
  
  public final Object verifyFinalOfferOtp(VerifyFinalOfferOtpRequest paramVerifyFinalOfferOtpRequest, c paramc)
  {
    return RetrofitExtensionsKt.getResult(creditApiService.verifyFinalOfferOtp(paramVerifyFinalOfferOtpRequest), paramc);
  }
  
  public final Object verifyPan(VerifyPanRequest paramVerifyPanRequest, c paramc)
  {
    return RetrofitExtensionsKt.getResult(creditApiService.verifyPan(paramVerifyPanRequest), paramc);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.repository.CreditRepository.Network
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */