package com.truecaller.credit.data.repository;

import c.d.c;
import com.truecaller.credit.app.core.model.CreditFeatureRequest;
import com.truecaller.credit.data.models.AddAddressRequest;
import com.truecaller.credit.data.models.BankDetailsRequest;
import com.truecaller.credit.data.models.BookSlotRequest;
import com.truecaller.credit.data.models.CheckEmiRequest;
import com.truecaller.credit.data.models.EmiHistoryRequest;
import com.truecaller.credit.data.models.FetchAddressRequest;
import com.truecaller.credit.data.models.FetchSlotRequest;
import com.truecaller.credit.data.models.IFSCSearchRequest;
import com.truecaller.credit.data.models.PoaTypeRequest;
import com.truecaller.credit.data.models.SaveLocationRequest;
import com.truecaller.credit.data.models.ScoreDataUploadRequest;
import com.truecaller.credit.data.models.UserInfoDataRequest;
import com.truecaller.credit.data.models.VerifyFinalOfferOtpRequest;
import com.truecaller.credit.data.models.VerifyPanRequest;
import com.truecaller.credit.data.models.WithDrawLoanRequest;
import com.truecaller.credit.domain.interactors.onboarding.models.PreScoreDataRequest;
import okhttp3.ac;
import okhttp3.x.b;

public abstract interface CreditRepository
{
  public static final String API_TAG = "api_tag";
  public static final String API_TAG_BACKGROUND = "background";
  public static final CreditRepository.Companion Companion = CreditRepository.Companion.$$INSTANCE;
  
  public abstract Object addAddress(AddAddressRequest paramAddAddressRequest, c paramc);
  
  public abstract Object addBankAccount(BankDetailsRequest paramBankDetailsRequest, c paramc);
  
  public abstract Object bookSlot(BookSlotRequest paramBookSlotRequest, c paramc);
  
  public abstract Object checkCreditLineStatus(c paramc);
  
  public abstract Object fetchAccountDetails(c paramc);
  
  public abstract Object fetchAddress(FetchAddressRequest paramFetchAddressRequest, c paramc);
  
  public abstract Object fetchAppointment(c paramc);
  
  public abstract Object fetchCreditLineDetails(c paramc);
  
  public abstract Object fetchEmiHistory(EmiHistoryRequest paramEmiHistoryRequest, c paramc);
  
  public abstract Object fetchEmiList(CheckEmiRequest paramCheckEmiRequest, c paramc);
  
  public abstract Object fetchFinalOfferDetails(c paramc);
  
  public abstract Object fetchIFSCSearchResults(IFSCSearchRequest paramIFSCSearchRequest, c paramc);
  
  public abstract Object fetchInitialOffer(c paramc);
  
  public abstract Object fetchLoanCategories(c paramc);
  
  public abstract Object fetchLoanHistory(c paramc);
  
  public abstract Object fetchPoaDetails(c paramc);
  
  public abstract Object fetchPoaTypes(PoaTypeRequest paramPoaTypeRequest, c paramc);
  
  public abstract Object fetchScoreDataRules(c paramc);
  
  public abstract Object fetchSlots(FetchSlotRequest paramFetchSlotRequest, c paramc);
  
  public abstract Object fetchSupportedCities(c paramc);
  
  public abstract Object requestFinalOfferOtp(c paramc);
  
  public abstract Object requestLoan(WithDrawLoanRequest paramWithDrawLoanRequest, c paramc);
  
  public abstract Object resetCredit(c paramc);
  
  public abstract Object saveLocation(SaveLocationRequest paramSaveLocationRequest, c paramc);
  
  public abstract Object syncBanner(c paramc);
  
  public abstract Object syncFeatures(CreditFeatureRequest paramCreditFeatureRequest, c paramc);
  
  public abstract Object uploadDocument(ac paramac, x.b paramb, c paramc);
  
  public abstract Object uploadPreScoreData(PreScoreDataRequest paramPreScoreDataRequest, c paramc);
  
  public abstract Object uploadScoreData(ScoreDataUploadRequest paramScoreDataUploadRequest, c paramc);
  
  public abstract Object uploadUserDetails(UserInfoDataRequest paramUserInfoDataRequest, c paramc);
  
  public abstract Object verifyFinalOfferOtp(VerifyFinalOfferOtpRequest paramVerifyFinalOfferOtpRequest, c paramc);
  
  public abstract Object verifyPan(VerifyPanRequest paramVerifyPanRequest, c paramc);
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.repository.CreditRepository
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */