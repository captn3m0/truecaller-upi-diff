package com.truecaller.credit.data.repository;

import com.truecaller.credit.data.api.CreditApiService;
import dagger.a.d;
import javax.inject.Provider;

public final class CreditRepository_Network_Factory
  implements d
{
  private final Provider creditApiServiceProvider;
  
  public CreditRepository_Network_Factory(Provider paramProvider)
  {
    creditApiServiceProvider = paramProvider;
  }
  
  public static CreditRepository_Network_Factory create(Provider paramProvider)
  {
    CreditRepository_Network_Factory localCreditRepository_Network_Factory = new com/truecaller/credit/data/repository/CreditRepository_Network_Factory;
    localCreditRepository_Network_Factory.<init>(paramProvider);
    return localCreditRepository_Network_Factory;
  }
  
  public static CreditRepository.Network newNetwork(CreditApiService paramCreditApiService)
  {
    CreditRepository.Network localNetwork = new com/truecaller/credit/data/repository/CreditRepository$Network;
    localNetwork.<init>(paramCreditApiService);
    return localNetwork;
  }
  
  public final CreditRepository.Network get()
  {
    CreditRepository.Network localNetwork = new com/truecaller/credit/data/repository/CreditRepository$Network;
    CreditApiService localCreditApiService = (CreditApiService)creditApiServiceProvider.get();
    localNetwork.<init>(localCreditApiService);
    return localNetwork;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.repository.CreditRepository_Network_Factory
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */