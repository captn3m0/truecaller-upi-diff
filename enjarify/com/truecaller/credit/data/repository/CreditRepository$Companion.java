package com.truecaller.credit.data.repository;

public final class CreditRepository$Companion
{
  public static final String API_TAG = "api_tag";
  public static final String API_TAG_BACKGROUND = "background";
  
  static
  {
    Companion localCompanion = new com/truecaller/credit/data/repository/CreditRepository$Companion;
    localCompanion.<init>();
    $$INSTANCE = localCompanion;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.data.repository.CreditRepository.Companion
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */