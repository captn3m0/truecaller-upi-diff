package com.truecaller.credit;

import c.d.c;
import c.g.b.k;
import c.o;
import com.daamitt.prime.sdk.ScoreCallback;
import com.truecaller.credit.data.Failure;
import com.truecaller.credit.data.Success;
import kotlinx.coroutines.j;

public final class l$b
  implements ScoreCallback
{
  l$b(j paramj) {}
  
  public final void onError(String paramString)
  {
    k.b(paramString, "error");
    Object localObject = a;
    boolean bool = ((j)localObject).b();
    if (bool)
    {
      localObject = (c)a;
      Failure localFailure = new com/truecaller/credit/data/Failure;
      Throwable localThrowable = new java/lang/Throwable;
      localThrowable.<init>(paramString);
      localFailure.<init>(localThrowable, paramString);
      paramString = o.a;
      paramString = o.d(localFailure);
      ((c)localObject).b(paramString);
    }
  }
  
  public final void onScoreData(String paramString)
  {
    k.b(paramString, "score");
    Object localObject = a;
    boolean bool = ((j)localObject).b();
    if (bool)
    {
      localObject = (c)a;
      Success localSuccess = new com/truecaller/credit/data/Success;
      localSuccess.<init>(paramString);
      paramString = o.a;
      paramString = o.d(localSuccess);
      ((c)localObject).b(paramString);
    }
  }
  
  public final void onScoreDataProgress(int paramInt1, int paramInt2) {}
}

/* Location:
 * Qualified Name:     com.truecaller.credit.l.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */