package com.truecaller.credit;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.content.Context;
import com.google.gson.o;

public abstract interface h
{
  public abstract Object a();
  
  public abstract void a(Application paramApplication, j paramj);
  
  public abstract void a(Context paramContext);
  
  public abstract void a(o paramo);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract void a(boolean paramBoolean, String paramString1, String paramString2);
  
  public abstract Object b();
  
  public abstract void c();
  
  public abstract void d();
  
  public abstract void e();
  
  public abstract void f();
  
  public abstract LiveData g();
}

/* Location:
 * Qualified Name:     com.truecaller.credit.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */