package com.truecaller.credit;

import com.truecaller.credit.app.core.g;
import com.truecaller.utils.d;

public final class b
  implements a
{
  public static final b.a a;
  private final g b;
  private final d c;
  
  static
  {
    b.a locala = new com/truecaller/credit/b$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  public b(g paramg, d paramd)
  {
    b = paramg;
    c = paramd;
  }
  
  public final String a()
  {
    d locald = c;
    String str = "com.truecaller.pay.qa";
    boolean bool = locald.d(str);
    if (bool) {
      return "https://credits.staging.tcpay.in/";
    }
    return "https://credits.api.tcpay.in/";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */