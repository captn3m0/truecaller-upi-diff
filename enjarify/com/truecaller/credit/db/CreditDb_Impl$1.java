package com.truecaller.credit.db;

import android.arch.persistence.room.b.b.a;
import android.arch.persistence.room.f.b;
import android.arch.persistence.room.h.a;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

final class CreditDb_Impl$1
  extends h.a
{
  CreditDb_Impl$1(CreditDb_Impl paramCreditDb_Impl)
  {
    super(6);
  }
  
  public final void a(android.arch.persistence.db.b paramb)
  {
    paramb.c("DROP TABLE IF EXISTS `banners`");
    paramb.c("DROP TABLE IF EXISTS `loans`");
    paramb.c("DROP TABLE IF EXISTS `locations`");
  }
  
  public final void b(android.arch.persistence.db.b paramb)
  {
    paramb.c("CREATE TABLE IF NOT EXISTS `banners` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `app_state` TEXT, `banner_type` INTEGER, `title` TEXT, `sub_title` TEXT, `button_action` TEXT, `button_text` TEXT, `header_left` TEXT, `header_right` TEXT, `progress_type` TEXT, `progress_percent` INTEGER, `image_url` TEXT)");
    paramb.c("CREATE TABLE IF NOT EXISTS `loans` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `name` TEXT NOT NULL, `amount` TEXT NOT NULL, `disbursed_on` INTEGER NOT NULL, `emis_count` INTEGER, `remaining_emis_count` INTEGER, `next_emi_due_date` INTEGER, `emi_amount` TEXT, `status` TEXT NOT NULL, `status_text` TEXT NOT NULL, `loan_id` TEXT NOT NULL, `category_id` TEXT NOT NULL, `category_name` TEXT NOT NULL, `category_icon` TEXT NOT NULL, `repayment_link` TEXT, `repayment_message` TEXT, `disbursed_amount` TEXT, `processing_fee` TEXT)");
    paramb.c("CREATE TABLE IF NOT EXISTS `locations` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `locations` TEXT NOT NULL)");
    paramb.c("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
    paramb.c("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, \"a94ae3f891fe5f7b02882376568e659f\")");
  }
  
  public final void c(android.arch.persistence.db.b paramb)
  {
    CreditDb_Impl.a(b, paramb);
    CreditDb_Impl.b(b, paramb);
    List localList1 = CreditDb_Impl.d(b);
    if (localList1 != null)
    {
      int i = 0;
      localList1 = null;
      List localList2 = CreditDb_Impl.e(b);
      int j = localList2.size();
      while (i < j)
      {
        f.b localb = (f.b)CreditDb_Impl.f(b).get(i);
        localb.b(paramb);
        i += 1;
      }
    }
  }
  
  public final void d(android.arch.persistence.db.b paramb)
  {
    List localList1 = CreditDb_Impl.a(b);
    if (localList1 != null)
    {
      int i = 0;
      localList1 = null;
      List localList2 = CreditDb_Impl.b(b);
      int j = localList2.size();
      while (i < j)
      {
        f.b localb = (f.b)CreditDb_Impl.c(b).get(i);
        localb.a(paramb);
        i += 1;
      }
    }
  }
  
  public final void e(android.arch.persistence.db.b paramb)
  {
    Object localObject1 = new java/util/HashMap;
    ((HashMap)localObject1).<init>(12);
    Object localObject2 = new android/arch/persistence/room/b/b$a;
    int i = 1;
    ((b.a)localObject2).<init>("_id", "INTEGER", i, i);
    ((HashMap)localObject1).put("_id", localObject2);
    localObject2 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject2).<init>("app_state", "TEXT", false, 0);
    ((HashMap)localObject1).put("app_state", localObject2);
    localObject2 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject2).<init>("banner_type", "INTEGER", false, 0);
    ((HashMap)localObject1).put("banner_type", localObject2);
    localObject2 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject2).<init>("title", "TEXT", false, 0);
    ((HashMap)localObject1).put("title", localObject2);
    localObject2 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject2).<init>("sub_title", "TEXT", false, 0);
    ((HashMap)localObject1).put("sub_title", localObject2);
    localObject2 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject2).<init>("button_action", "TEXT", false, 0);
    ((HashMap)localObject1).put("button_action", localObject2);
    localObject2 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject2).<init>("button_text", "TEXT", false, 0);
    ((HashMap)localObject1).put("button_text", localObject2);
    localObject2 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject2).<init>("header_left", "TEXT", false, 0);
    ((HashMap)localObject1).put("header_left", localObject2);
    localObject2 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject2).<init>("header_right", "TEXT", false, 0);
    ((HashMap)localObject1).put("header_right", localObject2);
    localObject2 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject2).<init>("progress_type", "TEXT", false, 0);
    ((HashMap)localObject1).put("progress_type", localObject2);
    localObject2 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject2).<init>("progress_percent", "INTEGER", false, 0);
    ((HashMap)localObject1).put("progress_percent", localObject2);
    localObject2 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject2).<init>("image_url", "TEXT", false, 0);
    ((HashMap)localObject1).put("image_url", localObject2);
    Object localObject3 = new java/util/HashSet;
    ((HashSet)localObject3).<init>(0);
    localObject2 = new java/util/HashSet;
    ((HashSet)localObject2).<init>(0);
    android.arch.persistence.room.b.b localb = new android/arch/persistence/room/b/b;
    String str = "banners";
    localb.<init>(str, (Map)localObject1, (Set)localObject3, (Set)localObject2);
    localObject1 = android.arch.persistence.room.b.b.a(paramb, "banners");
    boolean bool1 = localb.equals(localObject1);
    if (bool1)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>(18);
      localObject2 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject2).<init>("_id", "INTEGER", i, i);
      ((HashMap)localObject1).put("_id", localObject2);
      localObject2 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject2).<init>("name", "TEXT", i, 0);
      ((HashMap)localObject1).put("name", localObject2);
      localObject2 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject2).<init>("amount", "TEXT", i, 0);
      ((HashMap)localObject1).put("amount", localObject2);
      localObject2 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject2).<init>("disbursed_on", "INTEGER", i, 0);
      ((HashMap)localObject1).put("disbursed_on", localObject2);
      localObject2 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject2).<init>("emis_count", "INTEGER", false, 0);
      ((HashMap)localObject1).put("emis_count", localObject2);
      localObject2 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject2).<init>("remaining_emis_count", "INTEGER", false, 0);
      ((HashMap)localObject1).put("remaining_emis_count", localObject2);
      localObject2 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject2).<init>("next_emi_due_date", "INTEGER", false, 0);
      ((HashMap)localObject1).put("next_emi_due_date", localObject2);
      localObject2 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject2).<init>("emi_amount", "TEXT", false, 0);
      ((HashMap)localObject1).put("emi_amount", localObject2);
      localObject2 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject2).<init>("status", "TEXT", i, 0);
      ((HashMap)localObject1).put("status", localObject2);
      localObject2 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject2).<init>("status_text", "TEXT", i, 0);
      ((HashMap)localObject1).put("status_text", localObject2);
      localObject2 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject2).<init>("loan_id", "TEXT", i, 0);
      ((HashMap)localObject1).put("loan_id", localObject2);
      localObject2 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject2).<init>("category_id", "TEXT", i, 0);
      ((HashMap)localObject1).put("category_id", localObject2);
      localObject2 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject2).<init>("category_name", "TEXT", i, 0);
      ((HashMap)localObject1).put("category_name", localObject2);
      localObject2 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject2).<init>("category_icon", "TEXT", i, 0);
      ((HashMap)localObject1).put("category_icon", localObject2);
      localObject2 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject2).<init>("repayment_link", "TEXT", false, 0);
      ((HashMap)localObject1).put("repayment_link", localObject2);
      localObject2 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject2).<init>("repayment_message", "TEXT", false, 0);
      ((HashMap)localObject1).put("repayment_message", localObject2);
      localObject2 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject2).<init>("disbursed_amount", "TEXT", false, 0);
      ((HashMap)localObject1).put("disbursed_amount", localObject2);
      localObject2 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject2).<init>("processing_fee", "TEXT", false, 0);
      ((HashMap)localObject1).put("processing_fee", localObject2);
      localObject3 = new java/util/HashSet;
      ((HashSet)localObject3).<init>(0);
      localObject2 = new java/util/HashSet;
      ((HashSet)localObject2).<init>(0);
      localb = new android/arch/persistence/room/b/b;
      str = "loans";
      localb.<init>(str, (Map)localObject1, (Set)localObject3, (Set)localObject2);
      localObject1 = android.arch.persistence.room.b.b.a(paramb, "loans");
      bool1 = localb.equals(localObject1);
      if (bool1)
      {
        localObject1 = new java/util/HashMap;
        int j = 2;
        ((HashMap)localObject1).<init>(j);
        localObject2 = new android/arch/persistence/room/b/b$a;
        ((b.a)localObject2).<init>("_id", "INTEGER", i, i);
        ((HashMap)localObject1).put("_id", localObject2);
        localObject2 = new android/arch/persistence/room/b/b$a;
        ((b.a)localObject2).<init>("locations", "TEXT", i, 0);
        ((HashMap)localObject1).put("locations", localObject2);
        localObject3 = new java/util/HashSet;
        ((HashSet)localObject3).<init>(0);
        localObject2 = new java/util/HashSet;
        ((HashSet)localObject2).<init>(0);
        localb = new android/arch/persistence/room/b/b;
        str = "locations";
        localb.<init>(str, (Map)localObject1, (Set)localObject3, (Set)localObject2);
        localObject1 = "locations";
        paramb = android.arch.persistence.room.b.b.a(paramb, (String)localObject1);
        boolean bool2 = localb.equals(paramb);
        if (bool2) {
          return;
        }
        localObject1 = new java/lang/IllegalStateException;
        localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>("Migration didn't properly handle locations(com.truecaller.credit.domain.interactors.onboarding.models.SupportedCities).\n Expected:\n");
        ((StringBuilder)localObject3).append(localb);
        ((StringBuilder)localObject3).append("\n Found:\n");
        ((StringBuilder)localObject3).append(paramb);
        paramb = ((StringBuilder)localObject3).toString();
        ((IllegalStateException)localObject1).<init>(paramb);
        throw ((Throwable)localObject1);
      }
      paramb = new java/lang/IllegalStateException;
      localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>("Migration didn't properly handle loans(com.truecaller.common.payments.credit.Loan).\n Expected:\n");
      ((StringBuilder)localObject3).append(localb);
      ((StringBuilder)localObject3).append("\n Found:\n");
      ((StringBuilder)localObject3).append(localObject1);
      localObject1 = ((StringBuilder)localObject3).toString();
      paramb.<init>((String)localObject1);
      throw paramb;
    }
    paramb = new java/lang/IllegalStateException;
    localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>("Migration didn't properly handle banners(com.truecaller.common.payments.credit.CreditBanner).\n Expected:\n");
    ((StringBuilder)localObject3).append(localb);
    ((StringBuilder)localObject3).append("\n Found:\n");
    ((StringBuilder)localObject3).append(localObject1);
    localObject1 = ((StringBuilder)localObject3).toString();
    paramb.<init>((String)localObject1);
    throw paramb;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.db.CreditDb_Impl.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */