package com.truecaller.credit.db;

import android.arch.persistence.db.c.a;
import android.arch.persistence.db.c.b;
import android.arch.persistence.db.c.b.a;
import android.arch.persistence.db.c.c;
import android.arch.persistence.room.d;
import android.arch.persistence.room.h;
import android.arch.persistence.room.h.a;

public class CreditDb_Impl
  extends CreditDb
{
  private volatile com.truecaller.credit.app.ui.a.a.a g;
  private volatile com.truecaller.credit.app.ui.loanhistory.a.a h;
  private volatile com.truecaller.credit.app.ui.onboarding.a.b i;
  
  public final d a()
  {
    d locald = new android/arch/persistence/room/d;
    String[] tmp8_5 = new String[3];
    String[] tmp9_8 = tmp8_5;
    String[] tmp9_8 = tmp8_5;
    tmp9_8[0] = "banners";
    tmp9_8[1] = "loans";
    tmp9_8[2] = "locations";
    String[] arrayOfString = tmp9_8;
    locald.<init>(this, arrayOfString);
    return locald;
  }
  
  public final android.arch.persistence.db.c b(android.arch.persistence.room.a parama)
  {
    Object localObject1 = new android/arch/persistence/room/h;
    Object localObject2 = new com/truecaller/credit/db/CreditDb_Impl$1;
    ((CreditDb_Impl.1)localObject2).<init>(this);
    ((h)localObject1).<init>(parama, (h.a)localObject2, "a94ae3f891fe5f7b02882376568e659f", "ff40ec40b7db761eca2e05137fdde09e");
    localObject2 = c.b.a(b);
    String str = c;
    b = str;
    c = ((c.a)localObject1);
    localObject1 = ((c.b.a)localObject2).a();
    return a.a((c.b)localObject1);
  }
  
  public final com.truecaller.credit.app.ui.a.a.a h()
  {
    Object localObject1 = g;
    if (localObject1 != null) {
      return g;
    }
    try
    {
      localObject1 = g;
      if (localObject1 == null)
      {
        localObject1 = new com/truecaller/credit/app/ui/a/a/b;
        ((com.truecaller.credit.app.ui.a.a.b)localObject1).<init>(this);
        g = ((com.truecaller.credit.app.ui.a.a.a)localObject1);
      }
      localObject1 = g;
      return (com.truecaller.credit.app.ui.a.a.a)localObject1;
    }
    finally {}
  }
  
  public final com.truecaller.credit.app.ui.loanhistory.a.a i()
  {
    Object localObject1 = h;
    if (localObject1 != null) {
      return h;
    }
    try
    {
      localObject1 = h;
      if (localObject1 == null)
      {
        localObject1 = new com/truecaller/credit/app/ui/loanhistory/a/b;
        ((com.truecaller.credit.app.ui.loanhistory.a.b)localObject1).<init>(this);
        h = ((com.truecaller.credit.app.ui.loanhistory.a.a)localObject1);
      }
      localObject1 = h;
      return (com.truecaller.credit.app.ui.loanhistory.a.a)localObject1;
    }
    finally {}
  }
  
  public final com.truecaller.credit.app.ui.onboarding.a.b j()
  {
    Object localObject1 = i;
    if (localObject1 != null) {
      return i;
    }
    try
    {
      localObject1 = i;
      if (localObject1 == null)
      {
        localObject1 = new com/truecaller/credit/app/ui/onboarding/a/c;
        ((com.truecaller.credit.app.ui.onboarding.a.c)localObject1).<init>(this);
        i = ((com.truecaller.credit.app.ui.onboarding.a.b)localObject1);
      }
      localObject1 = i;
      return (com.truecaller.credit.app.ui.onboarding.a.b)localObject1;
    }
    finally {}
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.db.CreditDb_Impl
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */