package com.truecaller.credit.app.a;

import c.g.b.k;
import c.x;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.e.a;
import com.truecaller.androidactors.f;
import com.truecaller.tracking.events.ao;
import com.truecaller.tracking.events.ao.a;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.apache.a.d.d;

public final class c
  implements b
{
  private final com.truecaller.analytics.b a;
  private final f b;
  private final com.truecaller.clevertap.e c;
  
  public c(com.truecaller.analytics.b paramb, f paramf, com.truecaller.clevertap.e parame)
  {
    a = paramb;
    b = paramf;
    c = parame;
  }
  
  private void b(a parama)
  {
    k.b(parama, "creditAnalytics");
    Object localObject1 = new java/util/LinkedHashMap;
    ((LinkedHashMap)localObject1).<init>();
    localObject1 = (Map)localObject1;
    Object localObject2 = c;
    Object localObject3 = new java/util/ArrayList;
    int i = ((Map)localObject2).size();
    ((ArrayList)localObject3).<init>(i);
    localObject3 = (Collection)localObject3;
    localObject2 = ((Map)localObject2).entrySet().iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject2).hasNext();
      if (!bool) {
        break;
      }
      Object localObject4 = (Map.Entry)((Iterator)localObject2).next();
      Object localObject5 = (CharSequence)((Map.Entry)localObject4).getKey();
      localObject4 = (CharSequence)((Map.Entry)localObject4).getValue();
      localObject5 = localObject5.toString();
      localObject4 = localObject4.toString();
      ((Map)localObject1).put(localObject5, localObject4);
      localObject4 = x.a;
      ((Collection)localObject3).add(localObject4);
    }
    localObject2 = c;
    parama = a;
    ((com.truecaller.clevertap.e)localObject2).a(parama, (Map)localObject1);
  }
  
  public final void a(a parama)
  {
    Object localObject1 = "creditAnalytics";
    k.b(parama, (String)localObject1);
    boolean bool1 = g;
    Object localObject2;
    if (bool1)
    {
      k.b(parama, "creditAnalytics");
      localObject1 = new com/truecaller/analytics/e$a;
      localObject2 = a;
      ((e.a)localObject1).<init>((String)localObject2);
      localObject2 = c;
      Object localObject3 = new java/util/ArrayList;
      int i = ((Map)localObject2).size();
      ((ArrayList)localObject3).<init>(i);
      localObject3 = (Collection)localObject3;
      localObject2 = ((Map)localObject2).entrySet().iterator();
      for (;;)
      {
        boolean bool2 = ((Iterator)localObject2).hasNext();
        if (!bool2) {
          break;
        }
        Object localObject4 = (Map.Entry)((Iterator)localObject2).next();
        Object localObject5 = (CharSequence)((Map.Entry)localObject4).getKey();
        localObject4 = (CharSequence)((Map.Entry)localObject4).getValue();
        localObject5 = localObject5.toString();
        localObject4 = localObject4.toString();
        localObject4 = ((e.a)localObject1).a((String)localObject5, (String)localObject4);
        ((Collection)localObject3).add(localObject4);
      }
      localObject2 = a;
      localObject1 = ((e.a)localObject1).a();
      localObject3 = "builder.build()";
      k.a(localObject1, (String)localObject3);
      ((com.truecaller.analytics.b)localObject2).b((com.truecaller.analytics.e)localObject1);
    }
    bool1 = f;
    if (bool1) {
      b(parama);
    }
    bool1 = e;
    if (bool1)
    {
      k.b(parama, "creditAnalytics");
      localObject1 = ao.b();
      localObject2 = (CharSequence)b;
      ((ao.a)localObject1).a((CharSequence)localObject2);
      parama = d;
      ((ao.a)localObject1).a(parama);
      parama = (ae)b.a();
      localObject1 = (d)((ao.a)localObject1).a();
      parama.a((d)localObject1);
    }
  }
  
  public final void a(e parame)
  {
    k.b(parame, "creditUserProperty");
    com.truecaller.clevertap.e locale = c;
    parame = a;
    locale.a(parame);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */