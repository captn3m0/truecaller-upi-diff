package com.truecaller.credit.app.a;

import c.g.b.k;
import c.n;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

public final class a$a
{
  public boolean a;
  public boolean b;
  private boolean c;
  private final String d;
  private final String e;
  private final Map f;
  private final Map g;
  
  public a$a(String paramString)
  {
    this(paramString, paramString);
  }
  
  private a$a(String paramString1, String paramString2, Map paramMap1, Map paramMap2)
  {
    d = paramString1;
    e = paramString2;
    f = paramMap1;
    g = paramMap2;
  }
  
  public final a a()
  {
    boolean bool = true;
    c = bool;
    b = bool;
    return this;
  }
  
  public final a a(n paramn)
  {
    k.b(paramn, "property");
    CharSequence localCharSequence = (CharSequence)a;
    paramn = (CharSequence)b;
    if (paramn != null)
    {
      f.put(localCharSequence, paramn);
      Map localMap = g;
      localMap.put(localCharSequence, paramn);
    }
    return this;
  }
  
  public final a a(n[] paramArrayOfn)
  {
    k.b(paramArrayOfn, "values");
    Object localObject1 = new java/util/ArrayList;
    int i = paramArrayOfn.length;
    ((ArrayList)localObject1).<init>(i);
    localObject1 = (Collection)localObject1;
    i = paramArrayOfn.length;
    int j = 0;
    while (j < i)
    {
      Object localObject2 = paramArrayOfn[j];
      localObject2 = a((n)localObject2);
      ((Collection)localObject1).add(localObject2);
      j += 1;
    }
    return this;
  }
  
  public final a b()
  {
    a locala = new com/truecaller/credit/app/a/a;
    String str1 = d;
    String str2 = e;
    Map localMap1 = g;
    Map localMap2 = f;
    boolean bool1 = a;
    boolean bool2 = b;
    boolean bool3 = c;
    locala.<init>(str1, str2, localMap1, localMap2, bool1, bool2, bool3);
    return locala;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.a.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */