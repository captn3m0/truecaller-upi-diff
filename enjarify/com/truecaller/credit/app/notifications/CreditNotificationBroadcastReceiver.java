package com.truecaller.credit.app.notifications;

import android.app.NotificationManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import c.g.b.k;
import c.n.m;
import com.truecaller.credit.R.id;
import com.truecaller.log.d;
import com.truecaller.utils.extensions.i;

public final class CreditNotificationBroadcastReceiver
  extends BroadcastReceiver
{
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    k.b(paramContext, "context");
    k.b(paramIntent, "intent");
    Object localObject1 = paramIntent.getAction();
    int i = R.id.credit_notification_id;
    int k = paramIntent.getIntExtra("notification_id", i);
    Object localObject2 = "com.truecaller.credit.DISMISS";
    i = 1;
    boolean bool = m.a((String)localObject1, (String)localObject2, i);
    if (bool)
    {
      i.f(paramContext).cancel(k);
      return;
    }
    localObject2 = new android/content/Intent;
    ((Intent)localObject2).<init>();
    String str = "android.intent.action.VIEW";
    ((Intent)localObject2).setAction(str);
    int j = 268435456;
    ((Intent)localObject2).setFlags(j);
    if (localObject1 == null) {
      localObject1 = "truecaller://home/tabs/banking";
    }
    localObject1 = Uri.parse((String)localObject1);
    ((Intent)localObject2).setData((Uri)localObject1);
    localObject1 = i.f(paramContext);
    ((NotificationManager)localObject1).cancel(k);
    try
    {
      paramContext.startActivity((Intent)localObject2);
      return;
    }
    catch (ActivityNotFoundException localActivityNotFoundException)
    {
      d.a((Throwable)localActivityNotFoundException);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.notifications.CreditNotificationBroadcastReceiver
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */