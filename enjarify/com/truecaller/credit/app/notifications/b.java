package com.truecaller.credit.app.notifications;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.z.a;
import android.support.v4.app.z.c;
import android.support.v4.app.z.d;
import android.support.v4.app.z.g;
import c.a.m;
import c.g.b.k;
import c.x;
import com.d.b.ab;
import com.d.b.ai;
import com.d.b.w;
import com.google.gson.l;
import com.google.gson.o;
import com.truecaller.common.h.aq.d;
import com.truecaller.credit.R.color;
import com.truecaller.credit.R.drawable;
import com.truecaller.credit.R.id;
import com.truecaller.credit.R.string;
import com.truecaller.credit.app.ui.assist.LoanConfirmationStatus;
import com.truecaller.credit.app.ui.loanhistory.a.c;
import com.truecaller.credit.app.ui.onboarding.services.ScoreDataUploadService;
import com.truecaller.credit.app.ui.onboarding.services.ScoreDataUploadService.a;
import com.truecaller.utils.n;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Random;

public final class b
  implements a
{
  private final Context a;
  private final n b;
  private final com.truecaller.credit.app.ui.a.a c;
  private final com.truecaller.notificationchannels.e d;
  private final c e;
  
  public b(Context paramContext, n paramn, com.truecaller.credit.app.ui.a.a parama, com.truecaller.notificationchannels.e parame, c paramc)
  {
    a = paramContext;
    b = paramn;
    c = parama;
    d = parame;
    e = paramc;
  }
  
  private final void a(String paramString1, PendingIntent paramPendingIntent, String paramString2, boolean paramBoolean, ArrayList paramArrayList, int paramInt, String paramString3)
  {
    Object localObject1 = d.j();
    Object localObject2;
    if (localObject1 == null)
    {
      localObject1 = new android/support/v4/app/z$d;
      localObject2 = a;
      ((z.d)localObject1).<init>((Context)localObject2);
    }
    else
    {
      localObject2 = new android/support/v4/app/z$d;
      Context localContext = a;
      ((z.d)localObject2).<init>(localContext, (String)localObject1);
      localObject1 = localObject2;
    }
    paramString1 = (CharSequence)paramString1;
    ((z.d)localObject1).a(paramString1);
    paramString2 = (CharSequence)paramString2;
    ((z.d)localObject1).b(paramString2);
    paramString1 = new android/support/v4/app/z$c;
    paramString1.<init>();
    paramString1 = (z.g)paramString1.b(paramString2);
    ((z.d)localObject1).a(paramString1);
    paramString1 = a;
    int i = R.color.colorPrimary;
    int j = android.support.v4.content.b.c(paramString1, i);
    ((z.d)localObject1).f(j);
    j = R.drawable.ic_stat_notification;
    ((z.d)localObject1).a(j);
    ((z.d)localObject1).a(paramPendingIntent);
    long l = System.currentTimeMillis();
    ((z.d)localObject1).a(l);
    ((z.d)localObject1).e();
    ((z.d)localObject1).a();
    paramString1 = b(paramString3);
    ((z.d)localObject1).a(paramString1);
    paramArrayList = (Iterable)paramArrayList;
    paramString1 = paramArrayList.iterator();
    for (;;)
    {
      boolean bool = paramString1.hasNext();
      if (!bool) {
        break;
      }
      paramPendingIntent = (z.a)paramString1.next();
      ((z.d)localObject1).a(paramPendingIntent);
    }
    if (paramBoolean)
    {
      ((z.d)localObject1).b();
      j = 100;
      ((z.d)localObject1).a(j, j);
    }
    paramString1 = com.truecaller.utils.extensions.i.f(a);
    paramPendingIntent = ((z.d)localObject1).h();
    paramString1.notify(paramInt, paramPendingIntent);
  }
  
  private final Bitmap b(String paramString)
  {
    Object localObject = paramString;
    localObject = (CharSequence)paramString;
    if (localObject != null)
    {
      i = ((CharSequence)localObject).length();
      if (i != 0)
      {
        i = 0;
        localObject = null;
        break label34;
      }
    }
    int i = 1;
    label34:
    if (i == 0) {
      try
      {
        paramString = Uri.parse(paramString);
        localObject = a;
        localObject = w.a((Context)localObject);
        paramString = ((w)localObject).a(paramString);
        localObject = aq.d.b();
        localObject = (ai)localObject;
        paramString = paramString.a((ai)localObject);
        paramString = paramString.d();
      }
      catch (IOException|SecurityException localIOException) {}
    } else {
      paramString = null;
    }
    return paramString;
  }
  
  public final void a()
  {
    Object localObject = new android/content/Intent;
    ((Intent)localObject).<init>();
    ((Intent)localObject).putExtra("extra_state", "state_start");
    Context localContext = a;
    int i = R.id.credit_notification_id;
    PendingIntent localPendingIntent = PendingIntent.getService(localContext, i, (Intent)localObject, 134217728);
    localObject = b;
    int j = R.string.score_upload_gathering_info;
    Object[] arrayOfObject = new Object[0];
    String str = ((n)localObject).a(j, arrayOfObject);
    k.a(str, "resourceProvider.getStri…re_upload_gathering_info)");
    a(this, str, localPendingIntent, null, false, null, 0, 124);
  }
  
  public final void a(o paramo)
  {
    k.b(paramo, "notification");
    Object localObject1 = new java/util/Random;
    ((Random)localObject1).<init>();
    int i = ((Random)localObject1).nextInt();
    localObject1 = new com/truecaller/credit/app/notifications/d;
    boolean bool1 = false;
    Object localObject2 = null;
    ((d)localObject1).<init>((byte)0);
    Object localObject3 = "receiver$0";
    k.b(localObject1, (String)localObject3);
    if (paramo != null)
    {
      localObject3 = "d";
      paramo = paramo.b((String)localObject3);
      if (paramo != null)
      {
        paramo = paramo.i();
        localObject3 = "a";
        paramo = paramo.b((String)localObject3);
        if (paramo != null)
        {
          paramo = paramo.i();
          localObject3 = e.c(paramo, "t");
          a = ((String)localObject3);
          localObject3 = e.c(paramo, "s");
          b = ((String)localObject3);
          localObject3 = e.c(paramo, "i");
          c = ((String)localObject3);
          localObject3 = e.c(paramo, "u");
          d = ((String)localObject3);
          localObject3 = e.a(paramo, "ac");
          e = ((com.google.gson.i)localObject3);
          localObject3 = "cu";
          paramo = e.b(paramo, (String)localObject3);
          f = paramo;
        }
      }
    }
    paramo = new android/content/Intent;
    paramo.<init>();
    paramo.setAction("android.intent.action.VIEW");
    localObject3 = d;
    if (localObject3 == null) {
      localObject3 = "truecaller://home/tabs/banking";
    }
    localObject3 = Uri.parse((String)localObject3);
    paramo.setData((Uri)localObject3);
    localObject3 = a;
    int j = 268435456;
    paramo = PendingIntent.getActivity((Context)localObject3, 0, paramo, j);
    String str1 = a;
    String str2 = b;
    String str3 = c;
    Object localObject4 = new java/util/ArrayList;
    ((ArrayList)localObject4).<init>();
    localObject3 = e;
    Object localObject5;
    boolean bool2;
    Object localObject6;
    if (localObject3 != null)
    {
      localObject3 = (Iterable)localObject3;
      localObject5 = new java/util/ArrayList;
      int k = m.a((Iterable)localObject3, 10);
      ((ArrayList)localObject5).<init>(k);
      localObject5 = (Collection)localObject5;
      localObject3 = ((Iterable)localObject3).iterator();
      for (;;)
      {
        bool2 = ((Iterator)localObject3).hasNext();
        if (!bool2) {
          break;
        }
        paramo = (l)((Iterator)localObject3).next();
        localObject6 = new android/content/Intent;
        localObject7 = a;
        ((Intent)localObject6).<init>((Context)localObject7, CreditNotificationBroadcastReceiver.class);
        k.a(paramo, "it");
        localObject7 = paramo.i().b("value");
        k.a(localObject7, "it.asJsonObject[ACTION_VALUE]");
        localObject7 = ((l)localObject7).c();
        ((Intent)localObject6).setAction((String)localObject7);
        ((Intent)localObject6).putExtra("notification_id", i);
        localObject6 = PendingIntent.getBroadcast(a, i, (Intent)localObject6, j);
        localObject7 = localObject4;
        localObject7 = (Collection)localObject4;
        localObject8 = new android/support/v4/app/z$a;
        paramo = paramo.i().b("name");
        localObject9 = "it.asJsonObject[ACTION_NAME]";
        k.a(paramo, (String)localObject9);
        paramo = (CharSequence)paramo.c();
        ((z.a)localObject8).<init>(0, paramo, (PendingIntent)localObject6);
        ((Collection)localObject7).add(localObject8);
        paramo = x.a;
        ((Collection)localObject5).add(paramo);
        paramo = (o)localObject6;
      }
    }
    Object localObject10 = paramo;
    paramo = f;
    Object localObject7 = null;
    if (paramo != null)
    {
      localObject2 = "silent";
      paramo = paramo.b((String)localObject2);
      if (paramo != null)
      {
        bool3 = paramo.h();
        paramo = Boolean.valueOf(bool3);
        break label580;
      }
    }
    boolean bool3 = false;
    paramo = null;
    label580:
    localObject2 = f;
    if (localObject2 != null)
    {
      localObject3 = "refresh_banner";
      localObject2 = ((o)localObject2).b((String)localObject3);
      if (localObject2 != null)
      {
        bool1 = ((l)localObject2).h();
        localObject2 = Boolean.valueOf(bool1);
        localObject8 = localObject2;
        break label634;
      }
    }
    Object localObject8 = null;
    label634:
    localObject2 = f;
    if (localObject2 != null)
    {
      localObject3 = "refresh_loan_list";
      localObject2 = ((o)localObject2).b((String)localObject3);
      if (localObject2 != null)
      {
        bool1 = ((l)localObject2).h();
        localObject2 = Boolean.valueOf(bool1);
        localObject9 = localObject2;
        break label688;
      }
    }
    Object localObject9 = null;
    label688:
    localObject1 = f;
    if (localObject1 != null)
    {
      localObject2 = "event";
      localObject1 = ((o)localObject1).b((String)localObject2);
      if (localObject1 != null)
      {
        localObject1 = ((l)localObject1).c();
        break label726;
      }
    }
    int n = 0;
    localObject1 = null;
    label726:
    localObject2 = Boolean.FALSE;
    bool3 = k.a(paramo, localObject2);
    if (bool3)
    {
      bool2 = false;
      localObject6 = null;
      localObject2 = this;
      localObject3 = str1;
      localObject5 = str2;
      a(str1, (PendingIntent)localObject10, str2, false, (ArrayList)localObject4, i, str3);
    }
    paramo = Boolean.TRUE;
    bool3 = k.a(localObject8, paramo);
    if (bool3)
    {
      paramo = c;
      paramo.b();
    }
    paramo = Boolean.TRUE;
    bool3 = k.a(localObject9, paramo);
    if (bool3)
    {
      paramo = e;
      paramo.a();
    }
    paramo = String.valueOf(localObject1);
    localObject3 = String.valueOf(str1);
    localObject10 = String.valueOf(str2);
    n = paramo.hashCode();
    switch (n)
    {
    default: 
      break;
    case 201288556: 
      localObject1 = "offer_generated";
      bool3 = paramo.equals(localObject1);
      if (!bool3) {
        break label1027;
      }
      localObject7 = "state_offer_generated";
      break;
    case 72116155: 
      localObject1 = "offer_revoked";
      bool3 = paramo.equals(localObject1);
      if (!bool3) {
        break label1027;
      }
      break;
    case -333340127: 
      localObject1 = "offer_generation_failed";
      bool3 = paramo.equals(localObject1);
      if (!bool3) {
        break label1027;
      }
      localObject7 = "state_offer_generation_failed";
      break;
    case -1263011554: 
      localObject1 = "loan_disbursal_failed";
      bool3 = paramo.equals(localObject1);
      if (!bool3) {
        break label1027;
      }
      localObject7 = "loan_disbursal_failure";
      break;
    }
    localObject1 = "loan_disbursal_success";
    bool3 = paramo.equals(localObject1);
    if (bool3) {
      localObject7 = "loan_disbursal_success";
    }
    label1027:
    if (localObject7 != null)
    {
      int m = ((String)localObject7).hashCode();
      n = -1279232190;
      boolean bool4;
      Object localObject11;
      if (m != n)
      {
        n = -745535373;
        if (m != n)
        {
          n = -498636599;
          if (m != n)
          {
            n = 836387774;
            if (m != n) {
              break label1360;
            }
            paramo = "state_offer_generated";
            bool4 = ((String)localObject7).equals(paramo);
            if (!bool4) {
              break label1360;
            }
          }
          else
          {
            paramo = "loan_disbursal_failure";
            bool4 = ((String)localObject7).equals(paramo);
            if (!bool4) {
              break label1360;
            }
            paramo = new android/os/Bundle;
            paramo.<init>();
            localObject4 = "loan_status";
            localObject11 = new com/truecaller/credit/app/ui/assist/LoanConfirmationStatus;
            localObject5 = "failure";
            bool2 = false;
            localObject6 = null;
            localObject1 = localObject11;
            ((LoanConfirmationStatus)localObject11).<init>("animations/loan_failed.json", (String)localObject3, (String)localObject10, (String)localObject5, 0);
            localObject11 = (Parcelable)localObject11;
            paramo.putParcelable((String)localObject4, (Parcelable)localObject11);
            localObject1 = a;
            localObject2 = "loan_status";
            com.truecaller.utils.extensions.i.a((Context)localObject1, (String)localObject2, paramo);
            break label1360;
          }
        }
        else
        {
          paramo = "state_offer_generation_failed";
          bool4 = ((String)localObject7).equals(paramo);
          if (!bool4) {
            break label1360;
          }
        }
        paramo = new android/os/Bundle;
        paramo.<init>();
        paramo.putString("extra_state", (String)localObject7);
        paramo.putString("source", "persistant_notification");
        com.truecaller.utils.extensions.i.a(a, "credit_line_offer", paramo);
        paramo = com.truecaller.utils.extensions.i.f(a);
        n = R.id.credit_notification_id;
        paramo.cancel(n);
      }
      else
      {
        paramo = "loan_disbursal_success";
        bool4 = ((String)localObject7).equals(paramo);
        if (bool4)
        {
          paramo = new android/os/Bundle;
          paramo.<init>();
          localObject11 = new com/truecaller/credit/app/ui/assist/LoanConfirmationStatus;
          localObject1 = localObject11;
          ((LoanConfirmationStatus)localObject11).<init>("animations/loan_success.json", (String)localObject3, (String)localObject10, "success", 0);
          localObject11 = (Parcelable)localObject11;
          paramo.putParcelable("loan_status", (Parcelable)localObject11);
          com.truecaller.utils.extensions.i.a(a, "loan_status", paramo);
          return;
        }
      }
      label1360:
      return;
    }
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "title");
    Object localObject1 = ScoreDataUploadService.b;
    localObject1 = ScoreDataUploadService.a.a(a);
    ((Intent)localObject1).putExtra("extra_state", "state_offer_generation_failed");
    Object localObject2 = a;
    int i = R.id.credit_notification_id;
    PendingIntent localPendingIntent = PendingIntent.getService((Context)localObject2, i, (Intent)localObject1, 134217728);
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    localObject1 = new android/content/Intent;
    localObject2 = a;
    ((Intent)localObject1).<init>((Context)localObject2, CreditNotificationBroadcastReceiver.class);
    ((Intent)localObject1).setAction("com.truecaller.credit.DISMISS");
    i = R.id.credit_notification_id;
    ((Intent)localObject1).putExtra("notification_id", i);
    localObject2 = a;
    i = R.id.credit_notification_id;
    localObject1 = PendingIntent.getBroadcast((Context)localObject2, i, (Intent)localObject1, 268435456);
    localObject2 = new android/support/v4/app/z$a;
    Object localObject3 = b;
    int j = R.string.retry;
    Object localObject4 = new Object[0];
    localObject3 = (CharSequence)((n)localObject3).a(j, (Object[])localObject4);
    ((z.a)localObject2).<init>(0, (CharSequence)localObject3, localPendingIntent);
    localArrayList.add(localObject2);
    localObject2 = new android/support/v4/app/z$a;
    localObject3 = b;
    j = R.string.dismiss;
    localObject4 = new Object[0];
    localObject3 = (CharSequence)((n)localObject3).a(j, (Object[])localObject4);
    ((z.a)localObject2).<init>(0, (CharSequence)localObject3, (PendingIntent)localObject1);
    localArrayList.add(localObject2);
    localObject1 = b;
    int k = R.string.score_upload_error;
    localObject3 = new Object[0];
    String str = ((n)localObject1).a(k, (Object[])localObject3);
    k.a(str, "resourceProvider.getStri…tring.score_upload_error)");
    localObject4 = paramString;
    a(this, paramString, localPendingIntent, str, false, localArrayList, 0, 96);
  }
  
  public final void b()
  {
    Object localObject = new android/content/Intent;
    ((Intent)localObject).<init>();
    ((Intent)localObject).putExtra("extra_state", "state_calculation");
    Context localContext = a;
    int i = R.id.credit_notification_id;
    PendingIntent localPendingIntent = PendingIntent.getService(localContext, i, (Intent)localObject, 134217728);
    localObject = b;
    int j = R.string.score_upload_calculation;
    Object[] arrayOfObject = new Object[0];
    String str = ((n)localObject).a(j, arrayOfObject);
    k.a(str, "resourceProvider.getStri…score_upload_calculation)");
    a(this, str, localPendingIntent, null, false, null, 0, 124);
  }
  
  public final void c()
  {
    NotificationManager localNotificationManager = com.truecaller.utils.extensions.i.f(a);
    int i = R.id.credit_notification_id;
    localNotificationManager.cancel(i);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.notifications.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */