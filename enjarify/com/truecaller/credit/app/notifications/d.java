package com.truecaller.credit.app.notifications;

import c.g.b.k;
import com.google.gson.i;
import com.google.gson.o;

public final class d
{
  String a = null;
  String b = null;
  String c = null;
  String d = null;
  i e = null;
  o f = null;
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof d;
      if (bool1)
      {
        paramObject = (d)paramObject;
        Object localObject1 = a;
        Object localObject2 = a;
        bool1 = k.a(localObject1, localObject2);
        if (bool1)
        {
          localObject1 = b;
          localObject2 = b;
          bool1 = k.a(localObject1, localObject2);
          if (bool1)
          {
            localObject1 = c;
            localObject2 = c;
            bool1 = k.a(localObject1, localObject2);
            if (bool1)
            {
              localObject1 = d;
              localObject2 = d;
              bool1 = k.a(localObject1, localObject2);
              if (bool1)
              {
                localObject1 = e;
                localObject2 = e;
                bool1 = k.a(localObject1, localObject2);
                if (bool1)
                {
                  localObject1 = f;
                  paramObject = f;
                  boolean bool2 = k.a(localObject1, paramObject);
                  if (bool2) {
                    break label156;
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    label156:
    return true;
  }
  
  public final int hashCode()
  {
    String str = a;
    int i = 0;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    j *= 31;
    Object localObject = b;
    int k;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    int j = (j + k) * 31;
    localObject = c;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = d;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = e;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = f;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("NotificationModel(title=");
    Object localObject = a;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", subtitle=");
    localObject = b;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", imageUrl=");
    localObject = c;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", uri=");
    localObject = d;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", action=");
    localObject = e;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", custom=");
    localObject = f;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.notifications.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */