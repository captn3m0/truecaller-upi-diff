package com.truecaller.credit.app.core;

import androidx.work.f;
import androidx.work.m;
import androidx.work.p;
import c.g.b.k;
import c.g.b.w;
import c.n;
import c.o.b;
import c.t;
import com.truecaller.credit.app.a.a.a;
import com.truecaller.credit.app.a.e;
import com.truecaller.credit.app.a.e.a;
import com.truecaller.credit.app.core.model.CreditFeatureData;
import com.truecaller.credit.app.core.model.CreditFeatureRequest;
import com.truecaller.credit.data.Failure;
import com.truecaller.credit.data.Result;
import com.truecaller.credit.data.Success;
import com.truecaller.credit.data.repository.CreditRepository;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import org.a.a.h;

public final class b
  implements a
{
  private final CreditRepository a;
  private final com.truecaller.credit.app.ui.onboarding.services.a b;
  private final com.truecaller.credit.app.ui.loanhistory.a.c c;
  private final g d;
  private final com.truecaller.credit.j e;
  private final com.truecaller.utils.l f;
  private final com.truecaller.credit.app.a.b g;
  
  public b(CreditRepository paramCreditRepository, com.truecaller.credit.app.ui.onboarding.services.a parama, com.truecaller.credit.app.ui.loanhistory.a.c paramc, g paramg, com.truecaller.credit.j paramj, com.truecaller.utils.l paraml, com.truecaller.credit.app.a.b paramb)
  {
    a = paramCreditRepository;
    b = parama;
    c = paramc;
    d = paramg;
    e = paramj;
    f = paraml;
    g = paramb;
  }
  
  public final Object a(c.d.c paramc)
  {
    boolean bool1 = paramc instanceof b.a;
    if (bool1)
    {
      localObject1 = paramc;
      localObject1 = (b.a)paramc;
      int i = b;
      j = -1 << -1;
      i &= j;
      if (i != 0)
      {
        int m = b - j;
        b = m;
        break label70;
      }
    }
    Object localObject1 = new com/truecaller/credit/app/core/b$a;
    ((b.a)localObject1).<init>(this, (c.d.c)paramc);
    label70:
    paramc = a;
    Object localObject2 = c.d.a.a.a;
    int j = b;
    int n = 2;
    int i1 = 1;
    int i2 = 0;
    boolean bool3;
    Object localObject3;
    Object localObject4;
    Object localObject5;
    Object localObject6;
    boolean bool6;
    switch (j)
    {
    default: 
      paramc = new java/lang/IllegalStateException;
      paramc.<init>("call to 'resume' before 'invoke' with coroutine");
      throw paramc;
    case 2: 
      localObject2 = (CreditFeatureData)f;
      localObject1 = (b)d;
      bool3 = paramc instanceof o.b;
      if (!bool3) {
        break label784;
      }
      throw a;
    case 1: 
      localObject3 = (b)d;
      bool5 = paramc instanceof o.b;
      if (bool5) {
        throw a;
      }
      break;
    case 0: 
      bool3 = paramc instanceof o.b;
      if (bool3) {
        break label1367;
      }
      paramc = a;
      localObject3 = new com/truecaller/credit/app/core/model/CreditFeatureRequest;
      localObject4 = e;
      bool5 = ((com.truecaller.credit.j)localObject4).g();
      localObject5 = f;
      localObject6 = new String[] { "android.permission.READ_SMS" };
      bool6 = ((com.truecaller.utils.l)localObject5).a((String[])localObject6);
      ((CreditFeatureRequest)localObject3).<init>(bool5, bool6);
      d = this;
      b = i1;
      paramc = paramc.syncFeatures((CreditFeatureRequest)localObject3, (c.d.c)localObject1);
      if (paramc == localObject2) {
        return localObject2;
      }
      localObject3 = this;
    }
    paramc = (Result)paramc;
    boolean bool5 = paramc instanceof Success;
    label784:
    boolean bool4;
    if (bool5)
    {
      localObject4 = paramc;
      localObject4 = (CreditFeatureData)((Success)paramc).getData();
      localObject5 = d;
      localObject6 = "background_sync_interval";
      long l1 = ((g)localObject5).a((String)localObject6, 86400000L);
      long l2 = ((CreditFeatureData)localObject4).getBackgroundSyncInterval();
      boolean bool7 = l1 < l2;
      if (bool7)
      {
        localObject5 = CreditFeatureSyncWorker.d;
        l1 = ((CreditFeatureData)localObject4).getBackgroundSyncInterval();
        localObject7 = p.a();
        k.a(localObject7, "WorkManager.getInstance()");
        Object localObject8 = new com/truecaller/common/background/g;
        Object localObject9 = w.a(CreditFeatureSyncWorker.class);
        Object localObject10 = h.e(l1);
        ((com.truecaller.common.background.g)localObject8).<init>((c.l.b)localObject9, (h)localObject10);
        localObject9 = h.e(l1);
        localObject10 = "Duration.millis(intervalInMillis)";
        k.a(localObject9, (String)localObject10);
        localObject8 = ((com.truecaller.common.background.g)localObject8).a((h)localObject9);
        localObject9 = androidx.work.a.a;
        long l3 = 2;
        l1 /= l3;
        localObject5 = h.e(l1);
        k.a(localObject5, "Duration.millis(intervalInMillis / 2)");
        localObject5 = ((com.truecaller.common.background.g)localObject8).a((androidx.work.a)localObject9, (h)localObject5);
        localObject6 = androidx.work.j.b;
        localObject5 = ((com.truecaller.common.background.g)localObject5).a((androidx.work.j)localObject6).a();
        localObject6 = "CreditFeatureSyncWorker";
        localObject8 = f.a;
        ((p)localObject7).a((String)localObject6, (f)localObject8, (m)localObject5);
      }
      localObject5 = d;
      boolean bool8 = ((CreditFeatureData)localObject4).getShowBanner();
      ((g)localObject5).b("credit_banner", bool8);
      bool8 = ((CreditFeatureData)localObject4).getCreditEnabled();
      ((g)localObject5).b("credit_feature_status", bool8);
      Object localObject7 = ((CreditFeatureData)localObject4).getAppState();
      ((g)localObject5).a("credit_app_state", (String)localObject7);
      localObject7 = ((CreditFeatureData)localObject4).getParserRulesVersion();
      ((g)localObject5).a("credit_parser_version", (String)localObject7);
      localObject7 = ((CreditFeatureData)localObject4).getCreditFaq();
      ((g)localObject5).a("credit_faq", (String)localObject7);
      localObject7 = ((CreditFeatureData)localObject4).getCreditTnC();
      ((g)localObject5).a("credit_tnc", (String)localObject7);
      l2 = ((CreditFeatureData)localObject4).getBackgroundSyncInterval();
      ((g)localObject5).b("background_sync_interval", l2);
      l2 = ((CreditFeatureData)localObject4).getForegroundSyncInterval();
      ((g)localObject5).b("foreground_sync_interval", l2);
      localObject6 = "last_sync_timestamp";
      l2 = System.currentTimeMillis();
      ((g)localObject5).b((String)localObject6, l2);
      bool6 = ((CreditFeatureData)localObject4).getCalculatePreScore();
      if (bool6)
      {
        localObject5 = b;
        d = localObject3;
        e = paramc;
        f = localObject4;
        b = n;
        paramc = ((com.truecaller.credit.app.ui.onboarding.services.a)localObject5).a((c.d.c)localObject1);
        if (paramc == localObject2) {
          return localObject2;
        }
      }
      localObject1 = localObject3;
      localObject2 = localObject4;
      bool4 = ((CreditFeatureData)localObject2).getCreditEnabled();
      int k = 3;
      if (bool4)
      {
        paramc = "CreditEnabled";
        localObject4 = new com/truecaller/credit/app/a/a$a;
        ((a.a)localObject4).<init>(paramc, paramc);
        paramc = new n[n];
        localObject5 = t.a("Status", "enabled");
        paramc[0] = localObject5;
        localObject6 = "pre_score_calculation";
        localObject5 = t.a("Context", localObject6);
        paramc[i1] = localObject5;
        ((a.a)localObject4).a(paramc);
        paramc = ((a.a)localObject4).a();
        a = false;
        b = false;
        localObject4 = g;
        paramc = paramc.b();
        ((com.truecaller.credit.app.a.b)localObject4).a(paramc);
        bool4 = ((CreditFeatureData)localObject2).getSyncLoanList();
        if (bool4)
        {
          paramc = c;
          paramc.a();
        }
      }
      else
      {
        paramc = "CreditEnabled";
        localObject4 = new com/truecaller/credit/app/a/a$a;
        ((a.a)localObject4).<init>(paramc, paramc);
        paramc = new n[k];
        localObject5 = t.a("Status", "disabled");
        paramc[0] = localObject5;
        localObject5 = t.a("Context", "pre_score_calculation");
        paramc[i1] = localObject5;
        localObject5 = "Custom";
        localObject6 = f;
        localObject7 = new String[] { "android.permission.READ_SMS" };
        boolean bool9 = ((com.truecaller.utils.l)localObject6).a((String[])localObject7);
        if (bool9) {
          localObject6 = "sms_permission_enabled";
        } else {
          localObject6 = "sms_permission_disabled";
        }
        localObject5 = t.a(localObject5, localObject6);
        paramc[n] = localObject5;
        ((a.a)localObject4).a(paramc);
        paramc = ((a.a)localObject4).a();
        a = false;
        b = false;
        localObject4 = g;
        paramc = paramc.b();
        ((com.truecaller.credit.app.a.b)localObject4).a(paramc);
      }
      bool4 = ((CreditFeatureData)localObject2).getCalculatePreScore();
      bool5 = ((CreditFeatureData)localObject2).getCreditEnabled();
      boolean bool2 = ((CreditFeatureData)localObject2).getShowBanner();
      localObject5 = new com/truecaller/credit/app/a/e$a;
      ((e.a)localObject5).<init>();
      localObject6 = new n[k];
      localObject7 = "CalculatePreScore";
      paramc = Boolean.valueOf(bool4);
      paramc = t.a(localObject7, paramc);
      localObject6[0] = paramc;
      localObject4 = Boolean.valueOf(bool5);
      paramc = t.a("CreditEnabled", localObject4);
      localObject6[i1] = paramc;
      localObject2 = Boolean.valueOf(bool2);
      paramc = t.a("ShowCreditBanner", localObject2);
      localObject6[n] = paramc;
      k.b(localObject6, "values");
      paramc = new java/util/ArrayList;
      paramc.<init>(k);
      paramc = (Collection)paramc;
      while (i2 < k)
      {
        localObject2 = localObject6[i2];
        k.b(localObject2, "property");
        String str = (String)a;
        localObject2 = b;
        if (localObject2 != null)
        {
          localObject4 = a;
          str = str.toString();
          ((Map)localObject4).put(str, localObject2);
        }
        paramc.add(localObject5);
        i2 += 1;
      }
      paramc = g;
      localObject1 = new com/truecaller/credit/app/a/e;
      localObject2 = a;
      ((e)localObject1).<init>((Map)localObject2);
      paramc.a((e)localObject1);
    }
    else
    {
      bool4 = paramc instanceof Failure;
      if (!bool4) {
        break label1357;
      }
      i1 = 0;
    }
    return Boolean.valueOf(i1);
    label1357:
    paramc = new c/l;
    paramc.<init>();
    throw paramc;
    label1367:
    throw a;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.core.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */