package com.truecaller.credit.app.core;

import androidx.work.a;
import androidx.work.j;
import c.g.b.k;
import c.g.b.w;
import c.l.b;
import com.truecaller.common.background.g;

public final class CreditFeatureSyncWorker$a
  implements com.truecaller.common.background.h
{
  public final g a()
  {
    g localg = new com/truecaller/common/background/g;
    Object localObject = w.a(CreditFeatureSyncWorker.class);
    long l = 1L;
    org.a.a.h localh1 = org.a.a.h.a(l);
    localg.<init>((b)localObject, localh1);
    localObject = org.a.a.h.a(l);
    k.a(localObject, "Duration.standardDays(1)");
    localg = localg.a((org.a.a.h)localObject);
    localObject = a.a;
    org.a.a.h localh2 = org.a.a.h.b(12);
    k.a(localh2, "Duration.standardHours(12)");
    localg = localg.a((a)localObject, localh2);
    localObject = j.b;
    return localg.a((j)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.core.CreditFeatureSyncWorker.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */