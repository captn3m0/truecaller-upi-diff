package com.truecaller.credit.app.core.model;

import c.g.b.k;

public final class CreditFeatureData
{
  private final String appState;
  private final long backgroundSyncInterval;
  private final boolean calculatePreScore;
  private final boolean creditEnabled;
  private final String creditFaq;
  private final String creditTnC;
  private final long foregroundSyncInterval;
  private final String parserRulesVersion;
  private final boolean showBanner;
  private final boolean syncLoanList;
  
  public CreditFeatureData(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, String paramString1, String paramString2, boolean paramBoolean4, String paramString3, String paramString4, long paramLong1, long paramLong2)
  {
    calculatePreScore = paramBoolean1;
    creditEnabled = paramBoolean2;
    showBanner = paramBoolean3;
    parserRulesVersion = paramString1;
    appState = paramString2;
    syncLoanList = paramBoolean4;
    creditFaq = paramString3;
    creditTnC = paramString4;
    backgroundSyncInterval = paramLong1;
    foregroundSyncInterval = paramLong2;
  }
  
  public final boolean component1()
  {
    return calculatePreScore;
  }
  
  public final long component10()
  {
    return foregroundSyncInterval;
  }
  
  public final boolean component2()
  {
    return creditEnabled;
  }
  
  public final boolean component3()
  {
    return showBanner;
  }
  
  public final String component4()
  {
    return parserRulesVersion;
  }
  
  public final String component5()
  {
    return appState;
  }
  
  public final boolean component6()
  {
    return syncLoanList;
  }
  
  public final String component7()
  {
    return creditFaq;
  }
  
  public final String component8()
  {
    return creditTnC;
  }
  
  public final long component9()
  {
    return backgroundSyncInterval;
  }
  
  public final CreditFeatureData copy(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, String paramString1, String paramString2, boolean paramBoolean4, String paramString3, String paramString4, long paramLong1, long paramLong2)
  {
    CreditFeatureData localCreditFeatureData = new com/truecaller/credit/app/core/model/CreditFeatureData;
    localCreditFeatureData.<init>(paramBoolean1, paramBoolean2, paramBoolean3, paramString1, paramString2, paramBoolean4, paramString3, paramString4, paramLong1, paramLong2);
    return localCreditFeatureData;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof CreditFeatureData;
      if (bool2)
      {
        paramObject = (CreditFeatureData)paramObject;
        bool2 = calculatePreScore;
        boolean bool3 = calculatePreScore;
        String str1;
        if (bool2 == bool3)
        {
          bool2 = true;
        }
        else
        {
          bool2 = false;
          str1 = null;
        }
        if (bool2)
        {
          bool2 = creditEnabled;
          bool3 = creditEnabled;
          if (bool2 == bool3)
          {
            bool2 = true;
          }
          else
          {
            bool2 = false;
            str1 = null;
          }
          if (bool2)
          {
            bool2 = showBanner;
            bool3 = showBanner;
            if (bool2 == bool3)
            {
              bool2 = true;
            }
            else
            {
              bool2 = false;
              str1 = null;
            }
            if (bool2)
            {
              str1 = parserRulesVersion;
              String str2 = parserRulesVersion;
              bool2 = k.a(str1, str2);
              if (bool2)
              {
                str1 = appState;
                str2 = appState;
                bool2 = k.a(str1, str2);
                if (bool2)
                {
                  bool2 = syncLoanList;
                  bool3 = syncLoanList;
                  if (bool2 == bool3)
                  {
                    bool2 = true;
                  }
                  else
                  {
                    bool2 = false;
                    str1 = null;
                  }
                  if (bool2)
                  {
                    str1 = creditFaq;
                    str2 = creditFaq;
                    bool2 = k.a(str1, str2);
                    if (bool2)
                    {
                      str1 = creditTnC;
                      str2 = creditTnC;
                      bool2 = k.a(str1, str2);
                      if (bool2)
                      {
                        long l1 = backgroundSyncInterval;
                        long l2 = backgroundSyncInterval;
                        bool2 = l1 < l2;
                        if (!bool2)
                        {
                          bool2 = true;
                        }
                        else
                        {
                          bool2 = false;
                          str1 = null;
                        }
                        if (bool2)
                        {
                          l1 = foregroundSyncInterval;
                          l2 = foregroundSyncInterval;
                          boolean bool4 = l1 < l2;
                          if (!bool4)
                          {
                            bool4 = true;
                          }
                          else
                          {
                            bool4 = false;
                            paramObject = null;
                          }
                          if (bool4) {
                            return bool1;
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final String getAppState()
  {
    return appState;
  }
  
  public final long getBackgroundSyncInterval()
  {
    return backgroundSyncInterval;
  }
  
  public final boolean getCalculatePreScore()
  {
    return calculatePreScore;
  }
  
  public final boolean getCreditEnabled()
  {
    return creditEnabled;
  }
  
  public final String getCreditFaq()
  {
    return creditFaq;
  }
  
  public final String getCreditTnC()
  {
    return creditTnC;
  }
  
  public final long getForegroundSyncInterval()
  {
    return foregroundSyncInterval;
  }
  
  public final String getParserRulesVersion()
  {
    return parserRulesVersion;
  }
  
  public final boolean getShowBanner()
  {
    return showBanner;
  }
  
  public final boolean getSyncLoanList()
  {
    return syncLoanList;
  }
  
  public final int hashCode()
  {
    boolean bool = calculatePreScore;
    int j = 1;
    if (bool) {
      bool = true;
    }
    bool *= true;
    int k = creditEnabled;
    if (k != 0) {
      k = 1;
    }
    int i = (i + k) * 31;
    int m = showBanner;
    if (m != 0) {
      m = 1;
    }
    i = (i + m) * 31;
    String str1 = parserRulesVersion;
    int i5 = 0;
    int n;
    if (str1 != null)
    {
      n = str1.hashCode();
    }
    else
    {
      n = 0;
      str1 = null;
    }
    i = (i + n) * 31;
    str1 = appState;
    int i1;
    if (str1 != null)
    {
      i1 = str1.hashCode();
    }
    else
    {
      i1 = 0;
      str1 = null;
    }
    i = (i + i1) * 31;
    int i2 = syncLoanList;
    if (i2 == 0) {
      j = i2;
    }
    i = (i + j) * 31;
    String str2 = creditFaq;
    if (str2 != null)
    {
      j = str2.hashCode();
    }
    else
    {
      j = 0;
      str2 = null;
    }
    i = (i + j) * 31;
    str2 = creditTnC;
    if (str2 != null) {
      i5 = str2.hashCode();
    }
    i = (i + i5) * 31;
    long l1 = backgroundSyncInterval;
    int i6 = 32;
    long l2 = l1 >>> i6;
    int i3 = (int)(l1 ^ l2);
    i = (i + i3) * 31;
    l1 = foregroundSyncInterval;
    long l3 = l1 >>> i6;
    int i4 = (int)(l1 ^ l3);
    return i + i4;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("CreditFeatureData(calculatePreScore=");
    boolean bool = calculatePreScore;
    localStringBuilder.append(bool);
    localStringBuilder.append(", creditEnabled=");
    bool = creditEnabled;
    localStringBuilder.append(bool);
    localStringBuilder.append(", showBanner=");
    bool = showBanner;
    localStringBuilder.append(bool);
    localStringBuilder.append(", parserRulesVersion=");
    String str = parserRulesVersion;
    localStringBuilder.append(str);
    localStringBuilder.append(", appState=");
    str = appState;
    localStringBuilder.append(str);
    localStringBuilder.append(", syncLoanList=");
    bool = syncLoanList;
    localStringBuilder.append(bool);
    localStringBuilder.append(", creditFaq=");
    str = creditFaq;
    localStringBuilder.append(str);
    localStringBuilder.append(", creditTnC=");
    str = creditTnC;
    localStringBuilder.append(str);
    localStringBuilder.append(", backgroundSyncInterval=");
    long l = backgroundSyncInterval;
    localStringBuilder.append(l);
    localStringBuilder.append(", foregroundSyncInterval=");
    l = foregroundSyncInterval;
    localStringBuilder.append(l);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.core.model.CreditFeatureData
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */