package com.truecaller.credit.app.core.model;

import c.g.b.k;
import c.n.m;
import com.truecaller.credit.data.models.BaseApiResponse;
import com.truecaller.credit.data.models.BaseApiResponse.Meta;
import com.truecaller.credit.data.models.Mappable;

public final class CreditFeatureResponse
  extends BaseApiResponse
  implements Mappable
{
  private final CreditFeatureData data;
  
  public CreditFeatureResponse(CreditFeatureData paramCreditFeatureData)
  {
    data = paramCreditFeatureData;
  }
  
  public final CreditFeatureData component1()
  {
    return data;
  }
  
  public final CreditFeatureResponse copy(CreditFeatureData paramCreditFeatureData)
  {
    k.b(paramCreditFeatureData, "data");
    CreditFeatureResponse localCreditFeatureResponse = new com/truecaller/credit/app/core/model/CreditFeatureResponse;
    localCreditFeatureResponse.<init>(paramCreditFeatureData);
    return localCreditFeatureResponse;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof CreditFeatureResponse;
      if (bool1)
      {
        paramObject = (CreditFeatureResponse)paramObject;
        CreditFeatureData localCreditFeatureData = data;
        paramObject = data;
        boolean bool2 = k.a(localCreditFeatureData, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String errorMessage()
  {
    BaseApiResponse.Meta localMeta = getMeta();
    if (localMeta != null) {
      return localMeta.getMessage();
    }
    return null;
  }
  
  public final CreditFeatureData getData()
  {
    return data;
  }
  
  public final int hashCode()
  {
    CreditFeatureData localCreditFeatureData = data;
    if (localCreditFeatureData != null) {
      return localCreditFeatureData.hashCode();
    }
    return 0;
  }
  
  public final boolean isValid()
  {
    return m.a(getStatus(), "success", true);
  }
  
  public final CreditFeatureData mapToData()
  {
    CreditFeatureData localCreditFeatureData = new com/truecaller/credit/app/core/model/CreditFeatureData;
    boolean bool1 = data.getCalculatePreScore();
    boolean bool2 = data.getCreditEnabled();
    boolean bool3 = data.getShowBanner();
    String str1 = data.getParserRulesVersion();
    String str2 = data.getAppState();
    boolean bool4 = data.getSyncLoanList();
    String str3 = data.getCreditFaq();
    String str4 = data.getCreditTnC();
    long l1 = data.getBackgroundSyncInterval();
    long l2 = data.getForegroundSyncInterval();
    localCreditFeatureData.<init>(bool1, bool2, bool3, str1, str2, bool4, str3, str4, l1, l2);
    return localCreditFeatureData;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("CreditFeatureResponse(data=");
    CreditFeatureData localCreditFeatureData = data;
    localStringBuilder.append(localCreditFeatureData);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.core.model.CreditFeatureResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */