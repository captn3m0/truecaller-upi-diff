package com.truecaller.credit.app.core.model;

public final class CreditFeatureRequest
{
  private final boolean readSmsEnabled;
  private final boolean tcPayUser;
  
  public CreditFeatureRequest(boolean paramBoolean1, boolean paramBoolean2)
  {
    tcPayUser = paramBoolean1;
    readSmsEnabled = paramBoolean2;
  }
  
  public final boolean component1()
  {
    return tcPayUser;
  }
  
  public final boolean component2()
  {
    return readSmsEnabled;
  }
  
  public final CreditFeatureRequest copy(boolean paramBoolean1, boolean paramBoolean2)
  {
    CreditFeatureRequest localCreditFeatureRequest = new com/truecaller/credit/app/core/model/CreditFeatureRequest;
    localCreditFeatureRequest.<init>(paramBoolean1, paramBoolean2);
    return localCreditFeatureRequest;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof CreditFeatureRequest;
      if (bool2)
      {
        paramObject = (CreditFeatureRequest)paramObject;
        bool2 = tcPayUser;
        boolean bool3 = tcPayUser;
        if (bool2 == bool3) {
          bool2 = true;
        } else {
          bool2 = false;
        }
        if (bool2)
        {
          bool2 = readSmsEnabled;
          boolean bool4 = readSmsEnabled;
          if (bool2 == bool4)
          {
            bool4 = true;
          }
          else
          {
            bool4 = false;
            paramObject = null;
          }
          if (bool4) {
            return bool1;
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final boolean getReadSmsEnabled()
  {
    return readSmsEnabled;
  }
  
  public final boolean getTcPayUser()
  {
    return tcPayUser;
  }
  
  public final int hashCode()
  {
    boolean bool = tcPayUser;
    int j = 1;
    if (bool) {
      bool = true;
    }
    int i;
    bool *= true;
    int k = readSmsEnabled;
    if (k == 0) {
      j = k;
    }
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("CreditFeatureRequest(tcPayUser=");
    boolean bool = tcPayUser;
    localStringBuilder.append(bool);
    localStringBuilder.append(", readSmsEnabled=");
    bool = readSmsEnabled;
    localStringBuilder.append(bool);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.core.model.CreditFeatureRequest
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */