package com.truecaller.credit.app.core;

import android.content.Context;
import android.content.SharedPreferences;
import c.g.b.k;
import com.truecaller.utils.a.a;

public final class h
  extends a
  implements g
{
  public h(SharedPreferences paramSharedPreferences)
  {
    super(paramSharedPreferences);
  }
  
  public final int a()
  {
    return 0;
  }
  
  public final void a(int paramInt, Context paramContext)
  {
    k.b(paramContext, "context");
  }
  
  public final String b()
  {
    return "credit-prefs";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.core.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */