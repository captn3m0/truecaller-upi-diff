package com.truecaller.credit.app.core;

import androidx.work.ListenableWorker.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.credit.i;
import com.truecaller.credit.i.a;
import kotlinx.coroutines.ag;

final class CreditFeatureSyncWorker$b
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  CreditFeatureSyncWorker$b(CreditFeatureSyncWorker paramCreditFeatureSyncWorker, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/credit/app/core/CreditFeatureSyncWorker$b;
    CreditFeatureSyncWorker localCreditFeatureSyncWorker = b;
    localb.<init>(localCreditFeatureSyncWorker, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return localb;
  }
  
  public final Object a(Object paramObject)
  {
    c.d.a.a locala = c.d.a.a.a;
    int i = a;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        throw a;
      }
      break;
    case 0: 
      int j = paramObject instanceof o.b;
      if (j != 0) {
        break label164;
      }
      paramObject = i.i;
      paramObject = i.a.a();
      Object localObject = b;
      ((com.truecaller.credit.app.c.a.a)paramObject).a((CreditFeatureSyncWorker)localObject);
      paramObject = b.c;
      if (paramObject == null)
      {
        localObject = "creditFeatureManager";
        c.g.b.k.a((String)localObject);
      }
      j = 1;
      a = j;
      paramObject = ((a)paramObject).a(this);
      if (paramObject == locala) {
        return locala;
      }
      break;
    }
    paramObject = (Boolean)paramObject;
    boolean bool2 = ((Boolean)paramObject).booleanValue();
    if (bool2) {
      return ListenableWorker.a.a();
    }
    return ListenableWorker.a.b();
    label164:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.core.CreditFeatureSyncWorker.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */