package com.truecaller.credit.app.core;

import android.content.Context;
import androidx.work.ListenableWorker.a;
import androidx.work.WorkerParameters;
import c.g.a.m;
import c.g.b.k;
import com.truecaller.common.background.TrackedWorker;
import com.truecaller.featuretoggles.e;
import kotlinx.coroutines.f;

public final class CreditFeatureSyncWorker
  extends TrackedWorker
{
  public static final CreditFeatureSyncWorker.a d;
  public com.truecaller.analytics.b b;
  public a c;
  
  static
  {
    CreditFeatureSyncWorker.a locala = new com/truecaller/credit/app/core/CreditFeatureSyncWorker$a;
    locala.<init>((byte)0);
    d = locala;
  }
  
  public CreditFeatureSyncWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
  }
  
  public final com.truecaller.analytics.b b()
  {
    com.truecaller.analytics.b localb = b;
    if (localb == null)
    {
      String str = "analytics";
      k.a(str);
    }
    return localb;
  }
  
  public final boolean c()
  {
    Object localObject = com.truecaller.common.b.a.F();
    boolean bool = ((com.truecaller.common.b.a)localObject).p();
    if (bool)
    {
      localObject = com.truecaller.common.b.a.F();
      String str = "ApplicationBase.getAppBase()";
      k.a(localObject, str);
      localObject = ((com.truecaller.common.b.a)localObject).u().v().n();
      bool = ((com.truecaller.featuretoggles.b)localObject).a();
      if (bool)
      {
        localObject = com.truecaller.common.b.a.F();
        str = "ApplicationBase.getAppBase()";
        k.a(localObject, str);
        localObject = ((com.truecaller.common.b.a)localObject).u().v().r();
        bool = ((com.truecaller.featuretoggles.b)localObject).a();
        if (bool)
        {
          localObject = com.truecaller.common.b.a.F();
          k.a(localObject, "ApplicationBase.getAppBase()");
          localObject = ((com.truecaller.common.b.a)localObject).h();
          str = "ApplicationBase.getAppBase().isTcPayRegistered";
          k.a(localObject, str);
          bool = ((Boolean)localObject).booleanValue();
          if (bool) {
            return true;
          }
        }
      }
    }
    return false;
  }
  
  public final ListenableWorker.a d()
  {
    Object localObject = new com/truecaller/credit/app/core/CreditFeatureSyncWorker$b;
    ((CreditFeatureSyncWorker.b)localObject).<init>(this, null);
    localObject = f.a((m)localObject);
    k.a(localObject, "runBlocking {\n        ge…t.retry()\n        }\n    }");
    return (ListenableWorker.a)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.core.CreditFeatureSyncWorker
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */