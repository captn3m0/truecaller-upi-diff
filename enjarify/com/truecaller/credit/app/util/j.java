package com.truecaller.credit.app.util;

import java.util.Calendar;

public abstract interface j
{
  public static final j.a a = j.a.a;
  
  public abstract long a();
  
  public abstract String a(long paramLong);
  
  public abstract String a(long paramLong, String paramString);
  
  public abstract String a(Calendar paramCalendar, String paramString);
  
  public abstract Calendar a(String paramString1, String paramString2);
  
  public abstract String b(long paramLong);
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.util.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */