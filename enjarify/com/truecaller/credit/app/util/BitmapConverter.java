package com.truecaller.credit.app.util;

import android.graphics.Bitmap;
import com.truecaller.credit.app.ui.assist.CreditDocumentType;
import java.lang.ref.WeakReference;

public abstract interface BitmapConverter
{
  public abstract Object a(WeakReference paramWeakReference);
  
  public abstract Object a(WeakReference paramWeakReference, CreditDocumentType paramCreditDocumentType);
  
  public abstract Object a(byte[] paramArrayOfByte);
  
  public abstract WeakReference a(Bitmap paramBitmap);
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.util.BitmapConverter
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */