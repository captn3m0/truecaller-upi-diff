package com.truecaller.credit.app.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Matrix;
import android.net.Uri;
import c.g.b.k;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;

public final class t
  implements s
{
  private final Context a;
  private final BitmapConverter b;
  private final m c;
  private final p d;
  
  public t(Context paramContext, BitmapConverter paramBitmapConverter, m paramm, p paramp)
  {
    a = paramContext;
    b = paramBitmapConverter;
    c = paramm;
    d = paramp;
  }
  
  public final Object a(Bitmap paramBitmap)
  {
    File localFile = c.b(".jpg");
    FileOutputStream localFileOutputStream = new java/io/FileOutputStream;
    localFileOutputStream.<init>(localFile);
    ByteArrayOutputStream localByteArrayOutputStream = new java/io/ByteArrayOutputStream;
    localByteArrayOutputStream.<init>();
    Bitmap.CompressFormat localCompressFormat = Bitmap.CompressFormat.JPEG;
    Object localObject = localByteArrayOutputStream;
    localObject = (OutputStream)localByteArrayOutputStream;
    paramBitmap.compress(localCompressFormat, 100, (OutputStream)localObject);
    paramBitmap = localByteArrayOutputStream.toByteArray();
    localFileOutputStream.write(paramBitmap);
    localFileOutputStream.close();
    return c.a(localFile);
  }
  
  public final Object a(Uri paramUri)
  {
    return d.a(paramUri);
  }
  
  public final Object a(WeakReference paramWeakReference)
  {
    return b.a(paramWeakReference);
  }
  
  public final Object a(WeakReference paramWeakReference, float paramFloat)
  {
    Bitmap localBitmap1 = (Bitmap)paramWeakReference.get();
    if (localBitmap1 != null)
    {
      Object localObject = "it";
      k.a(localBitmap1, (String)localObject);
      int i = localBitmap1.getWidth();
      int j = localBitmap1.getHeight();
      if (i > j)
      {
        Matrix localMatrix = new android/graphics/Matrix;
        localMatrix.<init>();
        localMatrix.postRotate(paramFloat);
        localObject = localBitmap1;
        Bitmap localBitmap2 = Bitmap.createBitmap(localBitmap1, 0, 0, i, j, localMatrix, false);
        localBitmap1.recycle();
        paramWeakReference.clear();
        paramWeakReference = b;
        k.a(localBitmap2, "bitmap");
        return paramWeakReference.a(localBitmap2);
      }
    }
    return paramWeakReference;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.util.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */