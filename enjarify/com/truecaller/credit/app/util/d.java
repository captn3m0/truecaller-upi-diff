package com.truecaller.credit.app.util;

import android.content.Context;
import c.g.b.k;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.CameraSource.Builder;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.google.android.gms.vision.barcode.BarcodeDetector.Builder;

public final class d
  implements c
{
  private final Context a;
  
  public d(Context paramContext)
  {
    a = paramContext;
  }
  
  public final CameraSource a(BarcodeDetector paramBarcodeDetector, int paramInt)
  {
    k.b(paramBarcodeDetector, "detector");
    CameraSource.Builder localBuilder = new com/google/android/gms/vision/CameraSource$Builder;
    Context localContext = a;
    paramBarcodeDetector = (Detector)paramBarcodeDetector;
    localBuilder.<init>(localContext, paramBarcodeDetector);
    paramBarcodeDetector = localBuilder.a(paramInt).a().c().b().d();
    k.a(paramBarcodeDetector, "CameraSource.Builder(con…ght)\n            .build()");
    return paramBarcodeDetector;
  }
  
  public final BarcodeDetector a()
  {
    Object localObject = new com/google/android/gms/vision/barcode/BarcodeDetector$Builder;
    Context localContext = a;
    ((BarcodeDetector.Builder)localObject).<init>(localContext);
    localObject = ((BarcodeDetector.Builder)localObject).a().b();
    k.a(localObject, "BarcodeDetector.Builder(…(Barcode.QR_CODE).build()");
    return (BarcodeDetector)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.util.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */