package com.truecaller.credit.app.util;

import dagger.a.d;
import javax.inject.Provider;

public final class e
  implements d
{
  private final Provider a;
  
  private e(Provider paramProvider)
  {
    a = paramProvider;
  }
  
  public static e a(Provider paramProvider)
  {
    e locale = new com/truecaller/credit/app/util/e;
    locale.<init>(paramProvider);
    return locale;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.util.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */