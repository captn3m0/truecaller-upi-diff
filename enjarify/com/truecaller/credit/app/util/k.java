package com.truecaller.credit.app.util;

import com.truecaller.utils.n;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import org.a.a.d.a;

public final class k
  implements j
{
  private final n b;
  
  public k(n paramn)
  {
    b = paramn;
  }
  
  public static String a(Calendar paramCalendar)
  {
    String str = "calendar";
    c.g.b.k.b(paramCalendar, str);
    int i = paramCalendar.get(5);
    int j = 10;
    if (i < j)
    {
      paramCalendar = String.valueOf(i);
      return "0".concat(paramCalendar);
    }
    return String.valueOf(i);
  }
  
  public static String b(Calendar paramCalendar)
  {
    c.g.b.k.b(paramCalendar, "calendar");
    int i = paramCalendar.get(2);
    return j.a.d()[i];
  }
  
  public static String c(Calendar paramCalendar)
  {
    c.g.b.k.b(paramCalendar, "calendar");
    int i = paramCalendar.get(7);
    String[] arrayOfString = j.a.c();
    i += -1;
    return arrayOfString[i];
  }
  
  public final long a()
  {
    Date localDate = new java/util/Date;
    localDate.<init>();
    Calendar localCalendar = Calendar.getInstance();
    c.g.b.k.a(localCalendar, "c");
    localCalendar.setTime(localDate);
    localCalendar.add(1, -18);
    localDate = localCalendar.getTime();
    c.g.b.k.a(localDate, "c.time");
    return localDate.getTime();
  }
  
  public final String a(long paramLong)
  {
    org.a.a.b localb = new org/a/a/b;
    paramLong *= 1000L;
    localb.<init>(paramLong);
    String str = a.a("MMM").a(localb);
    c.g.b.k.a(str, "DateTime(time * android.…String(DATE_FORMAT_MONTH)");
    return str;
  }
  
  public final String a(long paramLong, String paramString)
  {
    c.g.b.k.b(paramString, "format");
    paramString = a.a(paramString);
    paramLong *= 1000L;
    String str = paramString.a(paramLong);
    c.g.b.k.a(str, "DateTimeFormat.forPatter…teUtils.SECOND_IN_MILLIS)");
    return str;
  }
  
  public final String a(Calendar paramCalendar, String paramString)
  {
    c.g.b.k.b(paramCalendar, "calendar");
    c.g.b.k.b(paramString, "format");
    SimpleDateFormat localSimpleDateFormat = new java/text/SimpleDateFormat;
    Locale localLocale = Locale.getDefault();
    localSimpleDateFormat.<init>(paramString, localLocale);
    paramString = TimeZone.getTimeZone(j.a.a());
    localSimpleDateFormat.setTimeZone(paramString);
    paramCalendar = paramCalendar.getTime();
    paramCalendar = localSimpleDateFormat.format(paramCalendar);
    c.g.b.k.a(paramCalendar, "sdf.format(calendar.time)");
    return paramCalendar;
  }
  
  public final Calendar a(String paramString1, String paramString2)
  {
    c.g.b.k.b(paramString1, "date");
    c.g.b.k.b(paramString2, "format");
    SimpleDateFormat localSimpleDateFormat = new java/text/SimpleDateFormat;
    Locale localLocale = Locale.getDefault();
    localSimpleDateFormat.<init>(paramString2, localLocale);
    paramString2 = TimeZone.getTimeZone(j.a.a());
    localSimpleDateFormat.setTimeZone(paramString2);
    paramString2 = Calendar.getInstance();
    c.g.b.k.a(paramString2, "cal");
    paramString1 = localSimpleDateFormat.parse(paramString1);
    paramString2.setTime(paramString1);
    return paramString2;
  }
  
  public final String b(long paramLong)
  {
    org.a.a.b localb = new org/a/a/b;
    paramLong *= 1000L;
    localb.<init>(paramLong);
    return String.valueOf(localb.i());
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.util.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */