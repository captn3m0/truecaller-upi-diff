package com.truecaller.credit.app.util;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Xfermode;
import android.util.DisplayMetrics;
import c.g.b.k;
import c.h.b;
import com.truecaller.credit.app.ui.assist.CreditDocumentType;
import java.lang.ref.WeakReference;

public final class a
  implements BitmapConverter
{
  private final DisplayMetrics a;
  private final Context b;
  private final m c;
  
  public a(Context paramContext, m paramm)
  {
    b = paramContext;
    c = paramm;
    paramContext = b.getResources();
    k.a(paramContext, "context.resources");
    paramContext = paramContext.getDisplayMetrics();
    a = paramContext;
  }
  
  private static int a(BitmapFactory.Options paramOptions)
  {
    int i = outHeight;
    int j = outWidth;
    Runtime localRuntime = Runtime.getRuntime();
    long l1 = localRuntime.totalMemory();
    long l2 = localRuntime.freeMemory();
    l1 -= l2;
    l2 = 1048576L;
    l1 /= l2;
    long l3 = localRuntime.maxMemory() / l2 - l1;
    double d1 = Math.ceil(-b.a(Math.sqrt(l3))) + 1.0D;
    double d2 = 2.0D;
    d1 = Math.pow(d2, d1);
    int k = (int)d1;
    k = Math.max(1, k);
    int m = 700;
    if ((i > m) || (j > m))
    {
      i /= 2;
      j /= 2;
      for (;;)
      {
        int n = i / k;
        if (n <= m) {
          break;
        }
        n = j / k;
        if (n <= m) {
          break;
        }
        k *= 2;
      }
    }
    return k;
  }
  
  public final Object a(WeakReference paramWeakReference)
  {
    Object localObject1 = (Bitmap)paramWeakReference.get();
    int i = 0;
    float f1 = 0.0F;
    PorterDuff.Mode localMode = null;
    int j;
    if (localObject1 != null)
    {
      j = ((Bitmap)localObject1).getWidth();
    }
    else
    {
      j = 0;
      f2 = 0.0F;
      localObject1 = null;
    }
    Bitmap localBitmap = (Bitmap)paramWeakReference.get();
    int k;
    if (localBitmap != null)
    {
      k = localBitmap.getHeight();
    }
    else
    {
      k = 0;
      localBitmap = null;
    }
    Object localObject2 = Bitmap.Config.ARGB_8888;
    localObject2 = Bitmap.createBitmap(j, k, (Bitmap.Config)localObject2);
    k.a(localObject2, "Bitmap.createBitmap(\n   …g.ARGB_8888\n            )");
    localObject2 = a((Bitmap)localObject2);
    Object localObject3 = ((WeakReference)localObject2).get();
    boolean bool = true;
    int m;
    if (localObject3 != null)
    {
      m = 1;
    }
    else
    {
      m = 0;
      localObject3 = null;
    }
    if (m == 0) {
      localObject2 = null;
    }
    localObject3 = new android/graphics/Canvas;
    if (localObject2 != null) {
      localObject4 = (Bitmap)((WeakReference)localObject2).get();
    } else {
      localObject4 = null;
    }
    ((Canvas)localObject3).<init>((Bitmap)localObject4);
    Object localObject4 = new android/graphics/Paint;
    ((Paint)localObject4).<init>();
    Rect localRect = new android/graphics/Rect;
    localRect.<init>(0, 0, j, k);
    ((Paint)localObject4).setAntiAlias(bool);
    ((Canvas)localObject3).drawARGB(0, 0, 0, 0);
    i = -12434878;
    ((Paint)localObject4).setColor(i);
    j /= 2;
    float f2 = j;
    k /= 2;
    f1 = k;
    ((Canvas)localObject3).drawCircle(f2, f1, f2, (Paint)localObject4);
    localObject1 = new android/graphics/PorterDuffXfermode;
    localMode = PorterDuff.Mode.SRC_IN;
    ((PorterDuffXfermode)localObject1).<init>(localMode);
    localObject1 = (Xfermode)localObject1;
    ((Paint)localObject4).setXfermode((Xfermode)localObject1);
    paramWeakReference = (Bitmap)paramWeakReference.get();
    if (paramWeakReference != null)
    {
      ((Canvas)localObject3).drawBitmap(paramWeakReference, localRect, localRect, (Paint)localObject4);
      return localObject2;
    }
    return null;
  }
  
  public final Object a(WeakReference paramWeakReference, CreditDocumentType paramCreditDocumentType)
  {
    Bitmap localBitmap1 = (Bitmap)paramWeakReference.get();
    int i = 0;
    float f1 = 0.0F;
    Object localObject1 = null;
    int j;
    float f2;
    if (localBitmap1 != null)
    {
      j = localBitmap1.getWidth();
    }
    else
    {
      j = 0;
      f2 = 0.0F;
      localBitmap1 = null;
    }
    Bitmap localBitmap2 = (Bitmap)paramWeakReference.get();
    if (localBitmap2 != null) {
      i = localBitmap2.getHeight();
    }
    int k = d;
    int m = -1;
    if (k == m) {
      k = i;
    } else {
      k = d;
    }
    int n = e;
    int i1;
    if (n == m) {
      i1 = j;
    } else {
      i1 = e;
    }
    m = j + -16;
    k = k * m / i1;
    float f3 = i - k;
    f1 = 2.0F;
    f3 /= f1;
    i = k + -16;
    Object localObject2 = Bitmap.Config.ARGB_8888;
    localObject1 = Bitmap.createBitmap(m, i, (Bitmap.Config)localObject2);
    k.a(localObject1, "Bitmap.createBitmap((bit… Bitmap.Config.ARGB_8888)");
    localObject1 = a((Bitmap)localObject1);
    localObject2 = (Bitmap)((WeakReference)localObject1).get();
    if (localObject2 != null)
    {
      Canvas localCanvas = new android/graphics/Canvas;
      localCanvas.<init>((Bitmap)localObject2);
      paramWeakReference = (Bitmap)paramWeakReference.get();
      if (paramWeakReference != null)
      {
        localObject2 = new android/graphics/Rect;
        i1 = (int)f3 + 8;
        int i2 = i1 + k;
        ((Rect)localObject2).<init>(16, i1, m, i2);
        paramCreditDocumentType = new android/graphics/RectF;
        f2 = j;
        float f4 = k;
        paramCreditDocumentType.<init>(0.0F, 0.0F, f2, f4);
        localCanvas.drawBitmap(paramWeakReference, (Rect)localObject2, paramCreditDocumentType, null);
        return localObject1;
      }
    }
    return null;
  }
  
  public final Object a(byte[] paramArrayOfByte)
  {
    k.b(paramArrayOfByte, "data");
    BitmapFactory.Options localOptions = new android/graphics/BitmapFactory$Options;
    localOptions.<init>();
    inJustDecodeBounds = true;
    int i = paramArrayOfByte.length;
    BitmapFactory.decodeByteArray(paramArrayOfByte, 0, i, localOptions);
    i = a(localOptions);
    inSampleSize = i;
    inJustDecodeBounds = false;
    i = paramArrayOfByte.length;
    paramArrayOfByte = BitmapFactory.decodeByteArray(paramArrayOfByte, 0, i, localOptions);
    k.a(paramArrayOfByte, "BitmapFactory.decodeByte…a, 0, data.size, options)");
    return a(paramArrayOfByte).get();
  }
  
  public final WeakReference a(Bitmap paramBitmap)
  {
    k.b(paramBitmap, "bitmap");
    WeakReference localWeakReference = new java/lang/ref/WeakReference;
    localWeakReference.<init>(paramBitmap);
    return localWeakReference;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.util.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */