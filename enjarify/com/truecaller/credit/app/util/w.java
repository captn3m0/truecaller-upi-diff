package com.truecaller.credit.app.util;

import android.util.Xml;
import c.g.b.k;
import com.truecaller.credit.data.models.Address;
import com.truecaller.credit.data.models.UserInfoDataRequest;
import com.truecaller.log.AssertionUtil;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public final class w
  implements v
{
  public final UserInfoDataRequest a(String paramString)
  {
    Object localObject1 = paramString;
    localObject1 = (CharSequence)paramString;
    if (localObject1 != null)
    {
      i = ((CharSequence)localObject1).length();
      if (i != 0)
      {
        i = 0;
        localObject1 = null;
        break label34;
      }
    }
    int i = 1;
    label34:
    if (i != 0) {
      return null;
    }
    try
    {
      localObject1 = new com/truecaller/credit/data/models/UserInfoDataRequest;
      Object localObject2 = null;
      String str1 = null;
      String str2 = null;
      String str3 = null;
      String str4 = null;
      int j = 31;
      Object localObject3 = localObject1;
      ((UserInfoDataRequest)localObject1).<init>(null, null, null, null, null, j, null);
      localObject3 = "xml";
      k.b(paramString, (String)localObject3);
      localObject3 = Xml.newPullParser();
      localObject2 = "Xml.newPullParser()";
      k.a(localObject3, (String)localObject2);
      localObject2 = new java/io/StringReader;
      ((StringReader)localObject2).<init>(paramString);
      localObject2 = (Reader)localObject2;
      ((XmlPullParser)localObject3).setInput((Reader)localObject2);
      paramString = "PrintLetterBarcodeData";
      paramString = i.a((XmlPullParser)localObject3, paramString);
      Address localAddress = new com/truecaller/credit/data/models/Address;
      localObject2 = null;
      str1 = null;
      str2 = null;
      str3 = null;
      str4 = null;
      j = 0;
      StringBuilder localStringBuilder = null;
      int k = 127;
      localObject3 = localAddress;
      localAddress.<init>(null, null, null, null, null, null, null, k, null);
      if (paramString != null)
      {
        localObject3 = "uid";
        localObject3 = i.b(paramString, (String)localObject3);
        if (localObject3 == null) {
          localObject3 = "";
        }
        ((UserInfoDataRequest)localObject1).setIdentifier((String)localObject3);
        localObject3 = "name";
        localObject3 = i.b(paramString, (String)localObject3);
        if (localObject3 == null) {
          localObject3 = "";
        }
        ((UserInfoDataRequest)localObject1).setFull_name((String)localObject3);
        localObject3 = "dob";
        localObject3 = i.b(paramString, (String)localObject3);
        if (localObject3 == null) {
          localObject3 = "";
        }
        ((UserInfoDataRequest)localObject1).setBirth_date((String)localObject3);
        localObject3 = "house";
        localObject3 = i.b(paramString, (String)localObject3);
        if (localObject3 == null) {
          localObject3 = "";
        }
        localAddress.setAddress_line_1((String)localObject3);
        localObject3 = "street";
        localObject3 = i.b(paramString, (String)localObject3);
        if (localObject3 == null) {
          localObject3 = "";
        }
        localObject2 = "lm";
        localObject2 = i.b(paramString, (String)localObject2);
        if (localObject2 == null) {
          localObject2 = "";
        }
        str1 = "loc";
        str1 = i.b(paramString, str1);
        if (str1 == null) {
          str1 = "";
        }
        str2 = "vtc";
        str2 = i.b(paramString, str2);
        if (str2 == null) {
          str2 = "";
        }
        str3 = "po";
        str3 = i.b(paramString, str3);
        if (str3 == null) {
          str3 = "";
        }
        str4 = "subdist";
        str4 = i.b(paramString, str4);
        if (str4 == null) {
          str4 = "";
        }
        localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        localStringBuilder.append((String)localObject3);
        localObject3 = ", ";
        localStringBuilder.append((String)localObject3);
        localStringBuilder.append((String)localObject2);
        localObject3 = ", ";
        localStringBuilder.append((String)localObject3);
        localStringBuilder.append(str1);
        localObject3 = localStringBuilder.toString();
        localAddress.setAddress_line_2((String)localObject3);
        localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>();
        ((StringBuilder)localObject3).append(str2);
        localObject2 = ", ";
        ((StringBuilder)localObject3).append((String)localObject2);
        ((StringBuilder)localObject3).append(str3);
        localObject2 = ", ";
        ((StringBuilder)localObject3).append((String)localObject2);
        ((StringBuilder)localObject3).append(str4);
        localObject3 = ((StringBuilder)localObject3).toString();
        localAddress.setAddress_line_3((String)localObject3);
        localObject3 = "dist";
        localObject3 = i.b(paramString, (String)localObject3);
        if (localObject3 == null) {
          localObject3 = "";
        }
        localAddress.setCity((String)localObject3);
        localObject3 = "state";
        localObject3 = i.b(paramString, (String)localObject3);
        if (localObject3 == null) {
          localObject3 = "";
        }
        localAddress.setState((String)localObject3);
        localObject3 = "pc";
        paramString = i.b(paramString, (String)localObject3);
        if (paramString == null) {
          paramString = "";
        }
        localAddress.setPincode(paramString);
        paramString = ((UserInfoDataRequest)localObject1).getAddresses();
        paramString.add(localAddress);
      }
      return (UserInfoDataRequest)localObject1;
    }
    catch (IOException localIOException)
    {
      paramString = (Throwable)localIOException;
      AssertionUtil.reportThrowableButNeverCrash(paramString);
    }
    catch (XmlPullParserException localXmlPullParserException)
    {
      paramString = (Throwable)localXmlPullParserException;
      AssertionUtil.reportThrowableButNeverCrash(paramString);
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.util.w
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */