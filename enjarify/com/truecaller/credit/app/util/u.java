package com.truecaller.credit.app.util;

import dagger.a.d;
import javax.inject.Provider;

public final class u
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  
  private u(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
    d = paramProvider4;
  }
  
  public static u a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    u localu = new com/truecaller/credit/app/util/u;
    localu.<init>(paramProvider1, paramProvider2, paramProvider3, paramProvider4);
    return localu;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.util.u
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */