package com.truecaller.credit.app.util;

import android.graphics.Bitmap;
import android.net.Uri;
import java.lang.ref.WeakReference;

public abstract interface s
{
  public abstract Object a(Bitmap paramBitmap);
  
  public abstract Object a(Uri paramUri);
  
  public abstract Object a(WeakReference paramWeakReference);
  
  public abstract Object a(WeakReference paramWeakReference, float paramFloat);
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.util.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */