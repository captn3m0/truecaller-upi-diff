package com.truecaller.credit.app.util;

public enum BitmapConverter$Scheme
{
  private final String value;
  
  static
  {
    Scheme[] arrayOfScheme = new Scheme[2];
    Scheme localScheme = new com/truecaller/credit/app/util/BitmapConverter$Scheme;
    localScheme.<init>("FILE", 0, "file");
    FILE = localScheme;
    arrayOfScheme[0] = localScheme;
    localScheme = new com/truecaller/credit/app/util/BitmapConverter$Scheme;
    int i = 1;
    localScheme.<init>("CONTENT", i, "content");
    CONTENT = localScheme;
    arrayOfScheme[i] = localScheme;
    $VALUES = arrayOfScheme;
  }
  
  private BitmapConverter$Scheme(String paramString1)
  {
    value = paramString1;
  }
  
  public final String getValue()
  {
    return value;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.util.BitmapConverter.Scheme
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */