package com.truecaller.credit.app.util;

import android.content.Context;
import android.net.Uri;
import android.support.v4.content.FileProvider;
import c.g.b.k;
import java.io.File;

public final class n
  implements m
{
  private final Context a;
  
  public n(Context paramContext)
  {
    a = paramContext;
  }
  
  public final Uri a(File paramFile)
  {
    k.b(paramFile, "file");
    Context localContext = a;
    Object localObject = localContext.getApplicationContext();
    k.a(localObject, "context.applicationContext");
    localObject = ((Context)localObject).getPackageName();
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(".fileprovider");
    localObject = localStringBuilder.toString();
    paramFile = FileProvider.a(localContext, (String)localObject, paramFile);
    k.a(paramFile, "FileProvider.getUriForFi…roviderAuthority(), file)");
    return paramFile;
  }
  
  public final String a(String paramString)
  {
    k.b(paramString, "time");
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("IMG");
    localStringBuilder.append("_");
    localStringBuilder.append(paramString);
    paramString = localStringBuilder.toString();
    k.a(paramString, "StringBuilder(DEFAULT_IM…\n            }.toString()");
    return paramString;
  }
  
  public final File b(String paramString)
  {
    k.b(paramString, "extension");
    File localFile = a.getExternalFilesDir(null);
    String str = String.valueOf(System.currentTimeMillis());
    paramString = File.createTempFile(a(str), paramString, localFile);
    k.a(paramString, "File.createTempFile(uniq…FileName, extension, dir)");
    return paramString;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.util.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */