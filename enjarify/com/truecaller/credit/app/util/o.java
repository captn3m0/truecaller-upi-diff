package com.truecaller.credit.app.util;

import dagger.a.d;
import javax.inject.Provider;

public final class o
  implements d
{
  private final Provider a;
  
  private o(Provider paramProvider)
  {
    a = paramProvider;
  }
  
  public static o a(Provider paramProvider)
  {
    o localo = new com/truecaller/credit/app/util/o;
    localo.<init>(paramProvider);
    return localo;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.util.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */