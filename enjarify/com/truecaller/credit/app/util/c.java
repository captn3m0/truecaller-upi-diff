package com.truecaller.credit.app.util;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.barcode.BarcodeDetector;

public abstract interface c
{
  public abstract CameraSource a(BarcodeDetector paramBarcodeDetector, int paramInt);
  
  public abstract BarcodeDetector a();
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.util.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */