package com.truecaller.credit.app.util;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import c.g.b.k;
import com.truecaller.utils.extensions.m;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import okhttp3.ac;
import okhttp3.w;

public final class h
  extends ac
{
  private final String a;
  private final Context b;
  private final Uri c;
  
  public h(Context paramContext, Uri paramUri)
  {
    b = paramContext;
    c = paramUri;
    a = "image/jpg";
  }
  
  public final w a()
  {
    return w.b(a);
  }
  
  public final void a(d.d paramd)
  {
    if (paramd != null)
    {
      InputStream localInputStream = null;
      try
      {
        Object localObject = b;
        localObject = ((Context)localObject).getContentResolver();
        Uri localUri = c;
        localInputStream = ((ContentResolver)localObject).openInputStream(localUri);
        localObject = "input";
        k.a(localInputStream, (String)localObject);
        paramd = paramd.c();
        localObject = "sink.outputStream()";
        k.a(paramd, (String)localObject);
        m.a(localInputStream, paramd);
        return;
      }
      finally
      {
        com.truecaller.utils.extensions.d.a((Closeable)localInputStream);
      }
    }
    paramd = new java/io/IOException;
    paramd.<init>();
    throw ((Throwable)paramd);
  }
  
  /* Error */
  public final long b()
  {
    // Byte code:
    //   0: iconst_m1
    //   1: i2l
    //   2: lstore_1
    //   3: aconst_null
    //   4: astore_3
    //   5: aload_0
    //   6: getfield 25	com/truecaller/credit/app/util/h:b	Landroid/content/Context;
    //   9: astore 4
    //   11: aload 4
    //   13: invokevirtual 42	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   16: astore 4
    //   18: aload_0
    //   19: getfield 27	com/truecaller/credit/app/util/h:c	Landroid/net/Uri;
    //   22: astore 5
    //   24: aload 4
    //   26: aload 5
    //   28: invokevirtual 48	android/content/ContentResolver:openInputStream	(Landroid/net/Uri;)Ljava/io/InputStream;
    //   31: astore_3
    //   32: aload_3
    //   33: ifnull +29 -> 62
    //   36: aload_3
    //   37: invokevirtual 82	java/io/InputStream:available	()I
    //   40: istore 6
    //   42: iload 6
    //   44: i2l
    //   45: lstore_1
    //   46: goto +16 -> 62
    //   49: astore 7
    //   51: aload_3
    //   52: checkcast 66	java/io/Closeable
    //   55: invokestatic 71	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   58: aload 7
    //   60: athrow
    //   61: pop
    //   62: aload_3
    //   63: checkcast 66	java/io/Closeable
    //   66: invokestatic 71	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   69: lload_1
    //   70: lreturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	71	0	this	h
    //   2	68	1	l	long
    //   4	59	3	localInputStream	InputStream
    //   9	16	4	localObject1	Object
    //   22	5	5	localUri	Uri
    //   40	3	6	i	int
    //   49	10	7	localObject2	Object
    //   61	1	7	localIOException	IOException
    // Exception table:
    //   from	to	target	type
    //   5	9	49	finally
    //   11	16	49	finally
    //   18	22	49	finally
    //   26	31	49	finally
    //   36	40	49	finally
    //   5	9	61	java/io/IOException
    //   11	16	61	java/io/IOException
    //   18	22	61	java/io/IOException
    //   26	31	61	java/io/IOException
    //   36	40	61	java/io/IOException
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.util.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */