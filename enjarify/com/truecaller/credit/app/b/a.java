package com.truecaller.credit.app.b;

import com.airbnb.deeplinkdispatch.DeepLinkEntry;
import com.airbnb.deeplinkdispatch.DeepLinkEntry.Type;
import com.airbnb.deeplinkdispatch.Parser;
import com.truecaller.credit.app.ui.infocollection.views.activities.InfoCollectionActivity;
import com.truecaller.credit.app.ui.loanhistory.views.activities.LoanHistoryActivity;
import com.truecaller.credit.app.ui.onboarding.views.activities.InitialOfferActivity;
import com.truecaller.credit.app.ui.withdrawloan.views.activities.WithdrawLoanActivity;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public final class a
  implements Parser
{
  public static final List a;
  
  static
  {
    DeepLinkEntry[] arrayOfDeepLinkEntry = new DeepLinkEntry[10];
    DeepLinkEntry localDeepLinkEntry = new com/airbnb/deeplinkdispatch/DeepLinkEntry;
    DeepLinkEntry.Type localType = DeepLinkEntry.Type.CLASS;
    localDeepLinkEntry.<init>("truecaller://credit/add_bank_details", localType, InfoCollectionActivity.class, null);
    arrayOfDeepLinkEntry[0] = localDeepLinkEntry;
    localDeepLinkEntry = new com/airbnb/deeplinkdispatch/DeepLinkEntry;
    localType = DeepLinkEntry.Type.CLASS;
    localDeepLinkEntry.<init>("truecaller://credit/address_confirmation", localType, InfoCollectionActivity.class, null);
    arrayOfDeepLinkEntry[1] = localDeepLinkEntry;
    localDeepLinkEntry = new com/airbnb/deeplinkdispatch/DeepLinkEntry;
    localType = DeepLinkEntry.Type.CLASS;
    localDeepLinkEntry.<init>("truecaller://credit/address_verification", localType, InfoCollectionActivity.class, null);
    arrayOfDeepLinkEntry[2] = localDeepLinkEntry;
    localDeepLinkEntry = new com/airbnb/deeplinkdispatch/DeepLinkEntry;
    localType = DeepLinkEntry.Type.CLASS;
    localDeepLinkEntry.<init>("truecaller://credit/final_offer", localType, InitialOfferActivity.class, null);
    arrayOfDeepLinkEntry[3] = localDeepLinkEntry;
    localDeepLinkEntry = new com/airbnb/deeplinkdispatch/DeepLinkEntry;
    localType = DeepLinkEntry.Type.CLASS;
    localDeepLinkEntry.<init>("truecaller://credit/initial_offer", localType, InitialOfferActivity.class, null);
    arrayOfDeepLinkEntry[4] = localDeepLinkEntry;
    localDeepLinkEntry = new com/airbnb/deeplinkdispatch/DeepLinkEntry;
    localType = DeepLinkEntry.Type.CLASS;
    localDeepLinkEntry.<init>("truecaller://credit/loan_history", localType, LoanHistoryActivity.class, null);
    arrayOfDeepLinkEntry[5] = localDeepLinkEntry;
    localDeepLinkEntry = new com/airbnb/deeplinkdispatch/DeepLinkEntry;
    localType = DeepLinkEntry.Type.CLASS;
    localDeepLinkEntry.<init>("truecaller://credit/meeting_confirmed", localType, InfoCollectionActivity.class, null);
    arrayOfDeepLinkEntry[6] = localDeepLinkEntry;
    localDeepLinkEntry = new com/airbnb/deeplinkdispatch/DeepLinkEntry;
    localType = DeepLinkEntry.Type.CLASS;
    localDeepLinkEntry.<init>("truecaller://credit/schedule_meeting", localType, InfoCollectionActivity.class, null);
    arrayOfDeepLinkEntry[7] = localDeepLinkEntry;
    localDeepLinkEntry = new com/airbnb/deeplinkdispatch/DeepLinkEntry;
    localType = DeepLinkEntry.Type.CLASS;
    localDeepLinkEntry.<init>("truecaller://credit/user_info", localType, InfoCollectionActivity.class, null);
    arrayOfDeepLinkEntry[8] = localDeepLinkEntry;
    localDeepLinkEntry = new com/airbnb/deeplinkdispatch/DeepLinkEntry;
    localType = DeepLinkEntry.Type.CLASS;
    localDeepLinkEntry.<init>("truecaller://credit/withdraw_amount", localType, WithdrawLoanActivity.class, null);
    arrayOfDeepLinkEntry[9] = localDeepLinkEntry;
    a = Collections.unmodifiableList(Arrays.asList(arrayOfDeepLinkEntry));
  }
  
  public final DeepLinkEntry parseUri(String paramString)
  {
    Iterator localIterator = a.iterator();
    DeepLinkEntry localDeepLinkEntry;
    boolean bool2;
    do
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      localDeepLinkEntry = (DeepLinkEntry)localIterator.next();
      bool2 = localDeepLinkEntry.matches(paramString);
    } while (!bool2);
    return localDeepLinkEntry;
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */