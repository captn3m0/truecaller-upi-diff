package com.truecaller.credit.app.c.a;

import android.content.Context;
import com.d.b.w;
import com.google.gson.q;
import com.truecaller.credit.app.a.b;
import com.truecaller.credit.app.core.CreditFeatureSyncWorker;
import com.truecaller.credit.app.core.g;
import com.truecaller.credit.app.ui.faq.activities.CreditWebViewActivity;
import com.truecaller.credit.app.ui.loanhistory.a.c;
import com.truecaller.credit.app.ui.onboarding.a.d;
import com.truecaller.credit.app.util.j;
import com.truecaller.credit.app.util.p;
import com.truecaller.credit.data.repository.CreditRepository;
import com.truecaller.credit.i;
import com.truecaller.credit.k;
import com.truecaller.tcpermissions.o;
import com.truecaller.utils.l;
import com.truecaller.utils.n;

public abstract interface a
{
  public abstract Context a();
  
  public abstract void a(CreditFeatureSyncWorker paramCreditFeatureSyncWorker);
  
  public abstract void a(CreditWebViewActivity paramCreditWebViewActivity);
  
  public abstract void a(com.truecaller.credit.app.ui.infocollection.views.b.a parama);
  
  public abstract void a(i parami);
  
  public abstract c.d.f b();
  
  public abstract c.d.f c();
  
  public abstract c.d.f d();
  
  public abstract l e();
  
  public abstract n f();
  
  public abstract com.truecaller.credit.app.util.f g();
  
  public abstract CreditRepository h();
  
  public abstract j i();
  
  public abstract c j();
  
  public abstract com.truecaller.credit.app.ui.a.a k();
  
  public abstract k l();
  
  public abstract com.truecaller.credit.app.notifications.a m();
  
  public abstract q n();
  
  public abstract w o();
  
  public abstract com.truecaller.utils.a p();
  
  public abstract p q();
  
  public abstract b r();
  
  public abstract g s();
  
  public abstract d t();
  
  public abstract o u();
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.c.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */