package com.truecaller.credit.app.c.a;

import android.content.Context;
import com.google.gson.q;
import com.truecaller.bm;
import com.truecaller.common.h.ag;
import com.truecaller.credit.app.c.b.aa;
import com.truecaller.credit.app.c.b.ab;
import com.truecaller.credit.app.c.b.s;
import com.truecaller.credit.app.c.b.u;
import com.truecaller.credit.app.c.b.v;
import com.truecaller.credit.app.c.b.x;
import com.truecaller.credit.app.c.b.y;
import com.truecaller.credit.app.c.b.z;
import com.truecaller.credit.app.core.CreditFeatureSyncWorker;
import com.truecaller.credit.app.ui.faq.activities.CreditWebViewActivity;
import com.truecaller.credit.app.ui.infocollection.views.c.a.a;
import com.truecaller.credit.data.api.CreditAppStateInterceptor_Factory;
import com.truecaller.credit.data.api.CreditCustomErrorInterceptor_Factory;
import com.truecaller.credit.data.api.CreditErrorInterceptor_Factory;
import com.truecaller.credit.data.api.CreditNetworkInterceptor_Factory;
import com.truecaller.credit.data.api.CreditResetStateInterceptor_Factory;
import com.truecaller.credit.data.api.CreditUserAgentInterceptor_Factory;
import com.truecaller.credit.data.repository.CreditRepository;
import javax.inject.Provider;

public final class b
  implements a
{
  private Provider A;
  private Provider B;
  private Provider C;
  private Provider D;
  private Provider E;
  private Provider F;
  private Provider G;
  private Provider H;
  private Provider I;
  private Provider J;
  private Provider K;
  private Provider L;
  private Provider M;
  private Provider N;
  private Provider O;
  private Provider P;
  private Provider Q;
  private Provider R;
  private Provider S;
  private Provider T;
  private Provider U;
  private Provider V;
  private Provider W;
  private Provider X;
  private Provider Y;
  private Provider Z;
  private final com.truecaller.common.a a;
  private Provider aA;
  private Provider aB;
  private Provider aC;
  private Provider aa;
  private Provider ab;
  private Provider ac;
  private Provider ad;
  private Provider ae;
  private Provider af;
  private Provider ag;
  private Provider ah;
  private Provider ai;
  private Provider aj;
  private Provider ak;
  private Provider al;
  private Provider am;
  private Provider an;
  private Provider ao;
  private Provider ap;
  private Provider aq;
  private Provider ar;
  private Provider as;
  private Provider at;
  private Provider au;
  private Provider av;
  private Provider aw;
  private Provider ax;
  private Provider ay;
  private Provider az;
  private final com.truecaller.utils.t b;
  private final com.truecaller.analytics.d c;
  private final com.truecaller.tcpermissions.e d;
  private Provider e;
  private Provider f;
  private Provider g;
  private Provider h;
  private Provider i;
  private Provider j;
  private Provider k;
  private Provider l;
  private Provider m;
  private Provider n;
  private Provider o;
  private Provider p;
  private Provider q;
  private Provider r;
  private Provider s;
  private Provider t;
  private Provider u;
  private Provider v;
  private Provider w;
  private Provider x;
  private Provider y;
  private Provider z;
  
  private b(com.truecaller.credit.app.c.b.a parama, com.truecaller.credit.app.c.b.p paramp, y paramy, com.truecaller.clevertap.g paramg, com.truecaller.common.a parama1, com.truecaller.utils.t paramt, com.truecaller.analytics.d paramd, com.truecaller.tcpermissions.e parame)
  {
    a = parama1;
    b = paramt;
    c = paramd;
    Object localObject3 = parame;
    d = parame;
    localObject3 = dagger.a.c.a(com.truecaller.credit.app.c.b.d.a(parama));
    e = ((Provider)localObject3);
    localObject3 = dagger.a.c.a(u.a(paramp));
    f = ((Provider)localObject3);
    localObject3 = new com/truecaller/credit/app/c/a/b$d;
    ((b.d)localObject3).<init>(parama1);
    g = ((Provider)localObject3);
    localObject3 = g;
    localObject3 = dagger.a.c.a(z.a(paramy, (Provider)localObject3));
    h = ((Provider)localObject3);
    localObject3 = h;
    localObject3 = dagger.a.c.a(ab.a(paramy, (Provider)localObject3));
    i = ((Provider)localObject3);
    localObject3 = i;
    localObject2 = dagger.a.c.a(aa.a(paramy, (Provider)localObject3));
    j = ((Provider)localObject2);
    localObject2 = f;
    localObject3 = j;
    Provider localProvider1 = i;
    localObject2 = dagger.a.c.a(v.a(paramp, (Provider)localObject2, (Provider)localObject3, localProvider1));
    k = ((Provider)localObject2);
    localObject2 = new com/truecaller/credit/app/c/a/b$i;
    ((b.i)localObject2).<init>(parama1);
    l = ((Provider)localObject2);
    localObject2 = new com/truecaller/credit/app/c/a/b$h;
    ((b.h)localObject2).<init>(parama1);
    m = ((Provider)localObject2);
    localObject2 = e;
    localObject2 = dagger.a.c.a(com.truecaller.credit.app.c.b.j.a(parama, (Provider)localObject2));
    n = ((Provider)localObject2);
    localObject2 = ag.a(n);
    o = ((Provider)localObject2);
    localObject2 = l;
    localObject3 = m;
    localProvider1 = o;
    localObject2 = dagger.a.c.a(com.truecaller.credit.app.c.b.r.a(paramp, (Provider)localObject2, (Provider)localObject3, localProvider1));
    p = ((Provider)localObject2);
    localObject2 = new com/truecaller/credit/app/c/a/b$j;
    ((b.j)localObject2).<init>(parama1);
    q = ((Provider)localObject2);
    localObject2 = e;
    localObject2 = dagger.a.c.a(com.truecaller.credit.app.c.b.g.a(parama, (Provider)localObject2));
    r = ((Provider)localObject2);
    localObject2 = r;
    localObject2 = dagger.a.c.a(com.truecaller.credit.app.c.b.f.a(parama, (Provider)localObject2));
    s = ((Provider)localObject2);
    localObject2 = new com/truecaller/credit/app/c/a/b$f;
    ((b.f)localObject2).<init>(parama1);
    t = ((Provider)localObject2);
    localObject2 = e;
    localObject2 = dagger.a.c.a(com.truecaller.credit.app.c.b.o.a(parama, (Provider)localObject2));
    u = ((Provider)localObject2);
    localObject2 = com.truecaller.credit.app.core.i.a(u);
    v = ((Provider)localObject2);
    localObject2 = dagger.a.c.a(v);
    w = ((Provider)localObject2);
    localObject2 = s;
    localObject3 = t;
    localProvider1 = w;
    localObject2 = com.truecaller.credit.app.ui.a.c.a((Provider)localObject2, (Provider)localObject3, localProvider1);
    x = ((Provider)localObject2);
    localObject2 = dagger.a.c.a(x);
    y = ((Provider)localObject2);
    localObject2 = e;
    localObject3 = q;
    localProvider1 = y;
    localObject2 = com.truecaller.credit.app.ui.errors.c.a((Provider)localObject2, (Provider)localObject3, localProvider1);
    z = ((Provider)localObject2);
    localObject2 = dagger.a.c.a(z);
    A = ((Provider)localObject2);
    localObject2 = new com/truecaller/credit/app/c/a/b$n;
    ((b.n)localObject2).<init>(paramt);
    B = ((Provider)localObject2);
    localObject2 = new com/truecaller/credit/app/c/a/b$l;
    ((b.l)localObject2).<init>(paramt);
    C = ((Provider)localObject2);
    localObject2 = A;
    localObject3 = B;
    localProvider1 = C;
    localObject2 = CreditNetworkInterceptor_Factory.create((Provider)localObject2, (Provider)localObject3, localProvider1);
    D = ((Provider)localObject2);
    localObject2 = A;
    localObject3 = B;
    localObject2 = CreditErrorInterceptor_Factory.create((Provider)localObject2, (Provider)localObject3);
    E = ((Provider)localObject2);
    localObject2 = dagger.a.c.a(com.truecaller.credit.app.c.b.b.a(parama));
    F = ((Provider)localObject2);
    localObject2 = F;
    localObject3 = A;
    localObject2 = CreditCustomErrorInterceptor_Factory.create((Provider)localObject2, (Provider)localObject3);
    G = ((Provider)localObject2);
    localObject2 = F;
    localObject3 = w;
    localObject2 = CreditResetStateInterceptor_Factory.create((Provider)localObject2, (Provider)localObject3);
    H = ((Provider)localObject2);
    localObject2 = F;
    localObject3 = y;
    localProvider1 = w;
    localObject2 = CreditAppStateInterceptor_Factory.create((Provider)localObject2, (Provider)localObject3, localProvider1);
    I = ((Provider)localObject2);
    localObject2 = dagger.a.c.a(com.truecaller.credit.app.c.b.m.a(parama));
    J = ((Provider)localObject2);
    localObject2 = CreditUserAgentInterceptor_Factory.create(J);
    K = ((Provider)localObject2);
    localObject3 = k;
    localProvider1 = p;
    Provider localProvider2 = D;
    Provider localProvider3 = E;
    Provider localProvider4 = G;
    Provider localProvider5 = H;
    Provider localProvider6 = I;
    Provider localProvider7 = K;
    localObject2 = paramp;
    localObject2 = dagger.a.c.a(com.truecaller.credit.app.c.b.w.a(paramp, (Provider)localObject3, localProvider1, localProvider2, localProvider3, localProvider4, localProvider5, localProvider6, localProvider7));
    L = ((Provider)localObject2);
    localObject2 = new com/truecaller/credit/app/c/a/b$k;
    ((b.k)localObject2).<init>(paramt);
    M = ((Provider)localObject2);
    localObject2 = w;
    localObject3 = M;
    localObject2 = dagger.a.c.a(s.a(paramp, (Provider)localObject2, (Provider)localObject3));
    N = ((Provider)localObject2);
    localObject2 = L;
    localObject3 = N;
    localObject2 = dagger.a.c.a(x.a(paramp, (Provider)localObject2, (Provider)localObject3));
    O = ((Provider)localObject2);
    localObject2 = O;
    localObject2 = dagger.a.c.a(com.truecaller.credit.app.c.b.t.a(paramp, (Provider)localObject2));
    P = ((Provider)localObject2);
    localObject2 = g;
    localObject2 = dagger.a.c.a(com.truecaller.credit.app.c.b.e.a(parama, (Provider)localObject2));
    Q = ((Provider)localObject2);
    localObject2 = P;
    localObject2 = dagger.a.c.a(com.truecaller.credit.app.c.b.h.a(parama, (Provider)localObject2));
    R = ((Provider)localObject2);
    localObject2 = R;
    localObject2 = dagger.a.c.a(com.truecaller.credit.app.c.b.i.a(parama, (Provider)localObject2));
    S = ((Provider)localObject2);
    localObject2 = r;
    localObject2 = dagger.a.c.a(com.truecaller.credit.app.c.b.k.a(parama, (Provider)localObject2));
    T = ((Provider)localObject2);
    localObject2 = com.truecaller.credit.app.util.l.a(B);
    U = ((Provider)localObject2);
    localObject2 = dagger.a.c.a(U);
    V = ((Provider)localObject2);
    localObject2 = T;
    localObject3 = S;
    localProvider1 = t;
    localObject2 = com.truecaller.credit.app.ui.loanhistory.a.e.a((Provider)localObject2, (Provider)localObject3, localProvider1);
    W = ((Provider)localObject2);
    localObject2 = dagger.a.c.a(W);
    X = ((Provider)localObject2);
    localObject2 = com.truecaller.credit.m.a(e);
    Y = ((Provider)localObject2);
    localObject2 = dagger.a.c.a(Y);
    Z = ((Provider)localObject2);
    localObject2 = g;
    localObject2 = dagger.a.c.a(com.truecaller.credit.app.c.b.c.a(parama, (Provider)localObject2));
    aa = ((Provider)localObject2);
    localObject2 = e;
    localObject3 = B;
    localProvider1 = y;
    localProvider2 = aa;
    localProvider3 = X;
    localObject2 = com.truecaller.credit.app.notifications.c.a((Provider)localObject2, (Provider)localObject3, localProvider1, localProvider2, localProvider3);
    ab = ((Provider)localObject2);
    localObject2 = dagger.a.c.a(ab);
    ac = ((Provider)localObject2);
    localObject2 = e;
    localObject2 = dagger.a.c.a(com.truecaller.credit.app.c.b.n.a(parama, (Provider)localObject2));
    ad = ((Provider)localObject2);
    localObject2 = com.truecaller.credit.app.util.r.a(ad);
    ae = ((Provider)localObject2);
    localObject2 = dagger.a.c.a(ae);
    af = ((Provider)localObject2);
    localObject2 = new com/truecaller/credit/app/c/a/b$b;
    ((b.b)localObject2).<init>(paramd);
    ag = ((Provider)localObject2);
    localObject2 = new com/truecaller/credit/app/c/a/b$c;
    ((b.c)localObject2).<init>(paramd);
    ah = ((Provider)localObject2);
    localObject2 = new com/truecaller/credit/app/c/a/b$g;
    ((b.g)localObject2).<init>(parama1);
    ai = ((Provider)localObject2);
    localObject2 = new com/truecaller/credit/app/c/a/b$e;
    ((b.e)localObject2).<init>(parama1);
    aj = ((Provider)localObject2);
    localObject2 = g;
    localObject3 = ai;
    localProvider1 = aj;
    localObject2 = com.truecaller.clevertap.h.a(paramg, (Provider)localObject2, (Provider)localObject3, localProvider1);
    ak = ((Provider)localObject2);
    localObject2 = ak;
    localObject2 = com.truecaller.clevertap.i.a(paramg, (Provider)localObject2);
    al = ((Provider)localObject2);
    localObject2 = ag;
    localObject3 = ah;
    localProvider1 = al;
    localObject2 = com.truecaller.credit.app.a.d.a((Provider)localObject2, (Provider)localObject3, localProvider1);
    am = ((Provider)localObject2);
    localObject2 = dagger.a.c.a(am);
    an = ((Provider)localObject2);
    localObject2 = Z;
    localObject3 = S;
    localProvider1 = F;
    localProvider2 = an;
    localProvider3 = J;
    localObject2 = com.truecaller.credit.app.ui.onboarding.services.c.a((Provider)localObject2, (Provider)localObject3, localProvider1, localProvider2, localProvider3);
    ao = ((Provider)localObject2);
    localObject2 = dagger.a.c.a(ao);
    ap = ((Provider)localObject2);
    localObject2 = new com/truecaller/credit/app/c/a/b$m;
    ((b.m)localObject2).<init>(paramt);
    aq = ((Provider)localObject2);
    localObject2 = S;
    localObject3 = ap;
    localProvider1 = X;
    localProvider2 = w;
    localProvider3 = J;
    localProvider4 = aq;
    localProvider5 = an;
    paramp = (com.truecaller.credit.app.c.b.p)localObject2;
    paramy = (y)localObject3;
    paramg = localProvider1;
    parama1 = localProvider2;
    paramt = localProvider3;
    paramd = localProvider4;
    parame = localProvider5;
    localObject2 = com.truecaller.credit.app.core.c.a((Provider)localObject2, (Provider)localObject3, localProvider1, localProvider2, localProvider3, localProvider4, localProvider5);
    ar = ((Provider)localObject2);
    localObject2 = dagger.a.c.a(ar);
    as = ((Provider)localObject2);
    localObject2 = as;
    localObject3 = w;
    localProvider1 = X;
    localProvider2 = t;
    localObject2 = com.truecaller.credit.app.ui.a.f.a((Provider)localObject2, (Provider)localObject3, localProvider1, localProvider2);
    at = ((Provider)localObject2);
    localObject2 = dagger.a.c.a(at);
    au = ((Provider)localObject2);
    localObject2 = S;
    localObject3 = t;
    localObject2 = com.truecaller.credit.app.core.f.a((Provider)localObject2, (Provider)localObject3);
    av = ((Provider)localObject2);
    localObject2 = dagger.a.c.a(av);
    aw = ((Provider)localObject2);
    localObject2 = dagger.a.c.a(com.truecaller.credit.app.ui.faq.a.b.a());
    ax = ((Provider)localObject2);
    localObject2 = com.truecaller.credit.app.ui.infocollection.b.b.a(q);
    ay = ((Provider)localObject2);
    localObject2 = dagger.a.c.a(ay);
    az = ((Provider)localObject2);
    localObject2 = r;
    localObject1 = dagger.a.c.a(com.truecaller.credit.app.c.b.l.a(parama, (Provider)localObject2));
    aA = ((Provider)localObject1);
    localObject1 = aA;
    localObject2 = S;
    localObject3 = t;
    localProvider1 = w;
    localObject1 = com.truecaller.credit.app.ui.onboarding.a.f.a((Provider)localObject1, (Provider)localObject2, (Provider)localObject3, localProvider1);
    aB = ((Provider)localObject1);
    localObject1 = dagger.a.c.a(aB);
    aC = ((Provider)localObject1);
  }
  
  public static b.a v()
  {
    b.a locala = new com/truecaller/credit/app/c/a/b$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  public final Context a()
  {
    return (Context)e.get();
  }
  
  public final void a(CreditFeatureSyncWorker paramCreditFeatureSyncWorker)
  {
    Object localObject = (com.truecaller.analytics.b)dagger.a.g.a(c.c(), "Cannot return null from a non-@Nullable component method");
    b = ((com.truecaller.analytics.b)localObject);
    localObject = (com.truecaller.credit.app.core.a)as.get();
    c = ((com.truecaller.credit.app.core.a)localObject);
  }
  
  public final void a(CreditWebViewActivity paramCreditWebViewActivity)
  {
    bm localbm = (bm)ax.get();
    a_ = localbm;
  }
  
  public final void a(com.truecaller.credit.app.ui.infocollection.views.b.a parama)
  {
    a.a locala = (a.a)az.get();
    c = locala;
  }
  
  public final void a(com.truecaller.credit.i parami)
  {
    Object localObject = (com.truecaller.credit.app.ui.a.a)y.get();
    a = ((com.truecaller.credit.app.ui.a.a)localObject);
    localObject = (com.truecaller.credit.app.ui.loanhistory.a.c)X.get();
    b = ((com.truecaller.credit.app.ui.loanhistory.a.c)localObject);
    localObject = (com.truecaller.credit.app.notifications.a)ac.get();
    c = ((com.truecaller.credit.app.notifications.a)localObject);
    localObject = (com.truecaller.credit.app.ui.a.d)au.get();
    d = ((com.truecaller.credit.app.ui.a.d)localObject);
    localObject = (com.truecaller.credit.app.a.b)an.get();
    e = ((com.truecaller.credit.app.a.b)localObject);
    localObject = (com.truecaller.credit.app.core.g)w.get();
    f = ((com.truecaller.credit.app.core.g)localObject);
    localObject = (com.truecaller.credit.app.core.d)aw.get();
    g = ((com.truecaller.credit.app.core.d)localObject);
  }
  
  public final c.d.f b()
  {
    return (c.d.f)dagger.a.g.a(a.r(), "Cannot return null from a non-@Nullable component method");
  }
  
  public final c.d.f c()
  {
    return (c.d.f)dagger.a.g.a(a.t(), "Cannot return null from a non-@Nullable component method");
  }
  
  public final c.d.f d()
  {
    return (c.d.f)dagger.a.g.a(a.u(), "Cannot return null from a non-@Nullable component method");
  }
  
  public final com.truecaller.utils.l e()
  {
    return (com.truecaller.utils.l)dagger.a.g.a(b.b(), "Cannot return null from a non-@Nullable component method");
  }
  
  public final com.truecaller.utils.n f()
  {
    return (com.truecaller.utils.n)dagger.a.g.a(b.g(), "Cannot return null from a non-@Nullable component method");
  }
  
  public final com.truecaller.credit.app.util.f g()
  {
    return (com.truecaller.credit.app.util.f)Q.get();
  }
  
  public final CreditRepository h()
  {
    return (CreditRepository)S.get();
  }
  
  public final com.truecaller.credit.app.util.j i()
  {
    return (com.truecaller.credit.app.util.j)V.get();
  }
  
  public final com.truecaller.credit.app.ui.loanhistory.a.c j()
  {
    return (com.truecaller.credit.app.ui.loanhistory.a.c)X.get();
  }
  
  public final com.truecaller.credit.app.ui.a.a k()
  {
    return (com.truecaller.credit.app.ui.a.a)y.get();
  }
  
  public final com.truecaller.credit.k l()
  {
    return (com.truecaller.credit.k)Z.get();
  }
  
  public final com.truecaller.credit.app.notifications.a m()
  {
    return (com.truecaller.credit.app.notifications.a)ac.get();
  }
  
  public final q n()
  {
    return (q)F.get();
  }
  
  public final com.d.b.w o()
  {
    return (com.d.b.w)ad.get();
  }
  
  public final com.truecaller.utils.a p()
  {
    return (com.truecaller.utils.a)dagger.a.g.a(b.d(), "Cannot return null from a non-@Nullable component method");
  }
  
  public final com.truecaller.credit.app.util.p q()
  {
    return (com.truecaller.credit.app.util.p)af.get();
  }
  
  public final com.truecaller.credit.app.a.b r()
  {
    return (com.truecaller.credit.app.a.b)an.get();
  }
  
  public final com.truecaller.credit.app.core.g s()
  {
    return (com.truecaller.credit.app.core.g)w.get();
  }
  
  public final com.truecaller.credit.app.ui.onboarding.a.d t()
  {
    return (com.truecaller.credit.app.ui.onboarding.a.d)aC.get();
  }
  
  public final com.truecaller.tcpermissions.o u()
  {
    return (com.truecaller.tcpermissions.o)dagger.a.g.a(d.b(), "Cannot return null from a non-@Nullable component method");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.c.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */