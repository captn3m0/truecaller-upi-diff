package com.truecaller.credit.app.c.a;

import com.truecaller.analytics.d;
import com.truecaller.credit.app.c.b.p;
import com.truecaller.credit.app.c.b.y;
import com.truecaller.tcpermissions.e;
import com.truecaller.utils.t;

public final class b$a
{
  private com.truecaller.credit.app.c.b.a a;
  private p b;
  private y c;
  private com.truecaller.clevertap.g d;
  private com.truecaller.common.a e;
  private t f;
  private d g;
  private e h;
  
  public final a a()
  {
    Object localObject = a;
    Class localClass = com.truecaller.credit.app.c.b.a.class;
    dagger.a.g.a(localObject, localClass);
    localObject = b;
    if (localObject == null)
    {
      localObject = new com/truecaller/credit/app/c/b/p;
      ((p)localObject).<init>();
      b = ((p)localObject);
    }
    localObject = c;
    if (localObject == null)
    {
      localObject = new com/truecaller/credit/app/c/b/y;
      ((y)localObject).<init>();
      c = ((y)localObject);
    }
    localObject = d;
    if (localObject == null)
    {
      localObject = new com/truecaller/clevertap/g;
      ((com.truecaller.clevertap.g)localObject).<init>();
      d = ((com.truecaller.clevertap.g)localObject);
    }
    dagger.a.g.a(e, com.truecaller.common.a.class);
    dagger.a.g.a(f, t.class);
    dagger.a.g.a(g, d.class);
    dagger.a.g.a(h, e.class);
    localObject = new com/truecaller/credit/app/c/a/b;
    com.truecaller.credit.app.c.b.a locala = a;
    p localp = b;
    y localy = c;
    com.truecaller.clevertap.g localg = d;
    com.truecaller.common.a locala1 = e;
    t localt = f;
    d locald = g;
    e locale = h;
    ((b)localObject).<init>(locala, localp, localy, localg, locala1, localt, locald, locale, (byte)0);
    return (a)localObject;
  }
  
  public final a a(d paramd)
  {
    paramd = (d)dagger.a.g.a(paramd);
    g = paramd;
    return this;
  }
  
  public final a a(com.truecaller.common.a parama)
  {
    parama = (com.truecaller.common.a)dagger.a.g.a(parama);
    e = parama;
    return this;
  }
  
  public final a a(com.truecaller.credit.app.c.b.a parama)
  {
    parama = (com.truecaller.credit.app.c.b.a)dagger.a.g.a(parama);
    a = parama;
    return this;
  }
  
  public final a a(e parame)
  {
    parame = (e)dagger.a.g.a(parame);
    h = parame;
    return this;
  }
  
  public final a a(t paramt)
  {
    paramt = (t)dagger.a.g.a(paramt);
    f = paramt;
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.c.a.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */