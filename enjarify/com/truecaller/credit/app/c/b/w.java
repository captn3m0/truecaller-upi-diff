package com.truecaller.credit.app.c.b;

import dagger.a.d;
import javax.inject.Provider;

public final class w
  implements d
{
  private final p a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  private final Provider f;
  private final Provider g;
  private final Provider h;
  private final Provider i;
  
  private w(p paramp, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5, Provider paramProvider6, Provider paramProvider7, Provider paramProvider8)
  {
    a = paramp;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
    h = paramProvider7;
    i = paramProvider8;
  }
  
  public static w a(p paramp, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5, Provider paramProvider6, Provider paramProvider7, Provider paramProvider8)
  {
    w localw = new com/truecaller/credit/app/c/b/w;
    localw.<init>(paramp, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7, paramProvider8);
    return localw;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.c.b.w
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */