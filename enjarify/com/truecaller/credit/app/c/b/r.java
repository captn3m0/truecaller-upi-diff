package com.truecaller.credit.app.c.b;

import dagger.a.d;
import javax.inject.Provider;

public final class r
  implements d
{
  private final p a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  
  private r(p paramp, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramp;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
  }
  
  public static r a(p paramp, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    r localr = new com/truecaller/credit/app/c/b/r;
    localr.<init>(paramp, paramProvider1, paramProvider2, paramProvider3);
    return localr;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.c.b.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */