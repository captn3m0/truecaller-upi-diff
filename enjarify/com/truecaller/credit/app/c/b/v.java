package com.truecaller.credit.app.c.b;

import dagger.a.d;
import javax.inject.Provider;

public final class v
  implements d
{
  private final p a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  
  private v(p paramp, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramp;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
  }
  
  public static v a(p paramp, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    v localv = new com/truecaller/credit/app/c/b/v;
    localv.<init>(paramp, paramProvider1, paramProvider2, paramProvider3);
    return localv;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.c.b.v
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */