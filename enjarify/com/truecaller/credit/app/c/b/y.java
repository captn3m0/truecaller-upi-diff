package com.truecaller.credit.app.c.b;

import android.content.Context;
import android.content.res.Resources;
import c.g.b.k;
import com.truecaller.credit.R.raw;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public final class y
{
  public static KeyStore a(Context paramContext)
  {
    k.b(paramContext, "context");
    paramContext = paramContext.getResources();
    int i = R.raw.credit_trust_store;
    paramContext = paramContext.openRawResource(i);
    i = 0;
    Object localObject1 = null;
    Object localObject2 = "BKS";
    try
    {
      localObject2 = KeyStore.getInstance((String)localObject2);
      String str = "KeyStore.getInstance(\"BKS\")";
      k.a(localObject2, str);
      if (localObject2 == null) {
        localObject1 = "keyStore";
      }
      paramContext = "Could not load key store file";
    }
    catch (KeyStoreException localKeyStoreException1)
    {
      try
      {
        k.a((String)localObject1);
        if (localObject2 == null) {
          k.a();
        }
        localObject1 = "longwindnorth";
        localObject1 = ((String)localObject1).toCharArray();
        str = "(this as java.lang.String).toCharArray()";
        k.a(localObject1, str);
        ((KeyStore)localObject2).load(paramContext, (char[])localObject1);
        paramContext.close();
      }
      catch (KeyStoreException localKeyStoreException2)
      {
        for (;;) {}
      }
      catch (NoSuchAlgorithmException localNoSuchAlgorithmException2)
      {
        for (;;) {}
      }
      catch (CertificateException localCertificateException2)
      {
        for (;;) {}
      }
      catch (IOException localIOException2)
      {
        for (;;) {}
      }
      localKeyStoreException1;
      localObject2 = null;
      paramContext = "Could not load key store file";
      new String[1][0] = paramContext;
    }
    catch (NoSuchAlgorithmException localNoSuchAlgorithmException1)
    {
      localObject2 = null;
      paramContext = "Could not load key store file";
      new String[1][0] = paramContext;
    }
    catch (CertificateException localCertificateException1)
    {
      localObject2 = null;
      paramContext = "Could not load key store file";
      new String[1][0] = paramContext;
    }
    catch (IOException localIOException1)
    {
      localObject2 = null;
    }
    new String[1][0] = paramContext;
    if (localObject2 == null)
    {
      paramContext = "keyStore";
      k.a(paramContext);
    }
    return (KeyStore)localObject2;
  }
  
  public static SSLSocketFactory a(X509TrustManager paramX509TrustManager)
  {
    k.b(paramX509TrustManager, "trustManager");
    String str = null;
    Object localObject1 = "TLSv1.2";
    try
    {
      localObject1 = SSLContext.getInstance((String)localObject1);
      Object localObject2 = "SSLContext.getInstance(\"TLSv1.2\")";
      k.a(localObject1, (String)localObject2);
      if (localObject1 == null) {
        localObject2 = "sslContext";
      }
      int i;
      paramX509TrustManager = "SecurityModule";
    }
    catch (KeyManagementException localKeyManagementException1)
    {
      try
      {
        k.a((String)localObject2);
        if (localObject1 == null) {
          k.a();
        }
        i = 1;
        localObject2 = new TrustManager[i];
        paramX509TrustManager = (TrustManager)paramX509TrustManager;
        localObject2[0] = paramX509TrustManager;
        paramX509TrustManager = new java/security/SecureRandom;
        paramX509TrustManager.<init>();
        ((SSLContext)localObject1).init(null, (TrustManager[])localObject2, paramX509TrustManager);
      }
      catch (KeyManagementException localKeyManagementException2)
      {
        for (;;) {}
      }
      catch (NoSuchAlgorithmException localNoSuchAlgorithmException2)
      {
        for (;;) {}
      }
      localKeyManagementException1;
      localObject1 = null;
      paramX509TrustManager = "SecurityModule";
      str = "Could not initiate socket factory";
      { paramX509TrustManager }[1] = str;
    }
    catch (NoSuchAlgorithmException localNoSuchAlgorithmException1)
    {
      localObject1 = null;
    }
    str = "Could not initiate socket factory";
    { paramX509TrustManager }[1] = str;
    if (localObject1 == null)
    {
      paramX509TrustManager = "sslContext";
      k.a(paramX509TrustManager);
    }
    paramX509TrustManager = ((SSLContext)localObject1).getSocketFactory();
    k.a(paramX509TrustManager, "sslContext.socketFactory");
    return paramX509TrustManager;
  }
  
  /* Error */
  public static X509TrustManager a(KeyStore paramKeyStore)
  {
    // Byte code:
    //   0: aload_0
    //   1: ldc 49
    //   3: invokestatic 16	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: iconst_0
    //   7: istore_1
    //   8: aconst_null
    //   9: astore_2
    //   10: invokestatic 124	javax/net/ssl/TrustManagerFactory:getDefaultAlgorithm	()Ljava/lang/String;
    //   13: astore_3
    //   14: aload_3
    //   15: invokestatic 127	javax/net/ssl/TrustManagerFactory:getInstance	(Ljava/lang/String;)Ljavax/net/ssl/TrustManagerFactory;
    //   18: astore_3
    //   19: ldc -127
    //   21: astore 4
    //   23: aload_3
    //   24: aload 4
    //   26: invokestatic 47	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   29: aload_3
    //   30: ifnonnull +10 -> 40
    //   33: ldc -125
    //   35: astore_2
    //   36: aload_2
    //   37: invokestatic 52	c/g/b/k:a	(Ljava/lang/String;)V
    //   40: aload_3
    //   41: aload_0
    //   42: invokevirtual 134	javax/net/ssl/TrustManagerFactory:init	(Ljava/security/KeyStore;)V
    //   45: aload_3
    //   46: invokevirtual 138	javax/net/ssl/TrustManagerFactory:getTrustManagers	()[Ljavax/net/ssl/TrustManager;
    //   49: astore_0
    //   50: aload_0
    //   51: arraylength
    //   52: istore_1
    //   53: iconst_1
    //   54: istore 5
    //   56: iload_1
    //   57: iload 5
    //   59: if_icmpne +43 -> 102
    //   62: aload_0
    //   63: iconst_0
    //   64: aaload
    //   65: astore_2
    //   66: aload_2
    //   67: instanceof 140
    //   70: istore_1
    //   71: iload_1
    //   72: ifeq +30 -> 102
    //   75: aload_0
    //   76: iconst_0
    //   77: aaload
    //   78: astore_0
    //   79: aload_0
    //   80: ifnull +8 -> 88
    //   83: aload_0
    //   84: checkcast 140	javax/net/ssl/X509TrustManager
    //   87: areturn
    //   88: new 142	c/u
    //   91: astore_0
    //   92: ldc -112
    //   94: astore_2
    //   95: aload_0
    //   96: aload_2
    //   97: invokespecial 146	c/u:<init>	(Ljava/lang/String;)V
    //   100: aload_0
    //   101: athrow
    //   102: new 148	java/lang/IllegalStateException
    //   105: astore_2
    //   106: new 150	java/lang/StringBuilder
    //   109: astore 4
    //   111: ldc -104
    //   113: astore 6
    //   115: aload 4
    //   117: aload 6
    //   119: invokespecial 153	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   122: aload_0
    //   123: invokestatic 159	java/util/Arrays:toString	([Ljava/lang/Object;)Ljava/lang/String;
    //   126: astore_0
    //   127: aload 4
    //   129: aload_0
    //   130: invokevirtual 163	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   133: pop
    //   134: aload 4
    //   136: invokevirtual 165	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   139: astore_0
    //   140: aload_2
    //   141: aload_0
    //   142: invokespecial 166	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
    //   145: aload_2
    //   146: checkcast 168	java/lang/Throwable
    //   149: astore_2
    //   150: aload_2
    //   151: athrow
    //   152: aload_3
    //   153: astore_2
    //   154: goto +10 -> 164
    //   157: pop
    //   158: aload_3
    //   159: astore_2
    //   160: goto +25 -> 185
    //   163: pop
    //   164: ldc 108
    //   166: astore_0
    //   167: ldc -86
    //   169: astore_3
    //   170: iconst_2
    //   171: anewarray 58	java/lang/String
    //   174: dup
    //   175: iconst_0
    //   176: aload_0
    //   177: aastore
    //   178: iconst_1
    //   179: aload_3
    //   180: aastore
    //   181: goto +21 -> 202
    //   184: pop
    //   185: ldc 108
    //   187: astore_0
    //   188: ldc -86
    //   190: astore_3
    //   191: iconst_2
    //   192: anewarray 58	java/lang/String
    //   195: dup
    //   196: iconst_0
    //   197: aload_0
    //   198: aastore
    //   199: iconst_1
    //   200: aload_3
    //   201: aastore
    //   202: aload_2
    //   203: ifnonnull +10 -> 213
    //   206: ldc -125
    //   208: astore_0
    //   209: aload_0
    //   210: invokestatic 52	c/g/b/k:a	(Ljava/lang/String;)V
    //   213: aload_2
    //   214: ifnonnull +6 -> 220
    //   217: invokestatic 54	c/g/b/k:a	()V
    //   220: aload_2
    //   221: invokevirtual 138	javax/net/ssl/TrustManagerFactory:getTrustManagers	()[Ljavax/net/ssl/TrustManager;
    //   224: iconst_0
    //   225: aaload
    //   226: astore_0
    //   227: aload_0
    //   228: ifnull +8 -> 236
    //   231: aload_0
    //   232: checkcast 140	javax/net/ssl/X509TrustManager
    //   235: areturn
    //   236: new 142	c/u
    //   239: astore_0
    //   240: aload_0
    //   241: ldc -112
    //   243: invokespecial 146	c/u:<init>	(Ljava/lang/String;)V
    //   246: aload_0
    //   247: athrow
    //   248: pop
    //   249: goto -97 -> 152
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	252	0	paramKeyStore	KeyStore
    //   7	53	1	i	int
    //   70	2	1	bool	boolean
    //   9	212	2	localObject1	Object
    //   13	188	3	localObject2	Object
    //   21	114	4	localObject3	Object
    //   54	6	5	j	int
    //   113	5	6	str	String
    //   157	1	8	localNoSuchAlgorithmException1	NoSuchAlgorithmException
    //   163	1	9	localKeyStoreException1	KeyStoreException
    //   184	1	10	localNoSuchAlgorithmException2	NoSuchAlgorithmException
    //   248	1	11	localKeyStoreException2	KeyStoreException
    // Exception table:
    //   from	to	target	type
    //   36	40	157	java/security/NoSuchAlgorithmException
    //   41	45	157	java/security/NoSuchAlgorithmException
    //   45	49	157	java/security/NoSuchAlgorithmException
    //   50	52	157	java/security/NoSuchAlgorithmException
    //   63	65	157	java/security/NoSuchAlgorithmException
    //   76	78	157	java/security/NoSuchAlgorithmException
    //   83	87	157	java/security/NoSuchAlgorithmException
    //   88	91	157	java/security/NoSuchAlgorithmException
    //   96	100	157	java/security/NoSuchAlgorithmException
    //   100	102	157	java/security/NoSuchAlgorithmException
    //   102	105	157	java/security/NoSuchAlgorithmException
    //   106	109	157	java/security/NoSuchAlgorithmException
    //   117	122	157	java/security/NoSuchAlgorithmException
    //   122	126	157	java/security/NoSuchAlgorithmException
    //   129	134	157	java/security/NoSuchAlgorithmException
    //   134	139	157	java/security/NoSuchAlgorithmException
    //   141	145	157	java/security/NoSuchAlgorithmException
    //   145	149	157	java/security/NoSuchAlgorithmException
    //   150	152	157	java/security/NoSuchAlgorithmException
    //   10	13	163	java/security/KeyStoreException
    //   14	18	163	java/security/KeyStoreException
    //   24	29	163	java/security/KeyStoreException
    //   10	13	184	java/security/NoSuchAlgorithmException
    //   14	18	184	java/security/NoSuchAlgorithmException
    //   24	29	184	java/security/NoSuchAlgorithmException
    //   36	40	248	java/security/KeyStoreException
    //   41	45	248	java/security/KeyStoreException
    //   45	49	248	java/security/KeyStoreException
    //   50	52	248	java/security/KeyStoreException
    //   63	65	248	java/security/KeyStoreException
    //   76	78	248	java/security/KeyStoreException
    //   83	87	248	java/security/KeyStoreException
    //   88	91	248	java/security/KeyStoreException
    //   96	100	248	java/security/KeyStoreException
    //   100	102	248	java/security/KeyStoreException
    //   102	105	248	java/security/KeyStoreException
    //   106	109	248	java/security/KeyStoreException
    //   117	122	248	java/security/KeyStoreException
    //   122	126	248	java/security/KeyStoreException
    //   129	134	248	java/security/KeyStoreException
    //   134	139	248	java/security/KeyStoreException
    //   141	145	248	java/security/KeyStoreException
    //   145	149	248	java/security/KeyStoreException
    //   150	152	248	java/security/KeyStoreException
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.c.b.y
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */