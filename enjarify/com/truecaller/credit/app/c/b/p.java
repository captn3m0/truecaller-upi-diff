package com.truecaller.credit.app.c.b;

import android.os.Build.VERSION;
import c.g.b.z;
import c.u;
import com.c.b.a.a.a.a.b;
import com.truecaller.common.account.r;
import com.truecaller.common.h.af;
import com.truecaller.common.h.i;
import com.truecaller.credit.app.core.g;
import com.truecaller.credit.b;
import com.truecaller.credit.data.api.CreditApiService;
import com.truecaller.credit.data.api.CreditAppStateInterceptor;
import com.truecaller.credit.data.api.CreditCustomErrorInterceptor;
import com.truecaller.credit.data.api.CreditErrorInterceptor;
import com.truecaller.credit.data.api.CreditNetworkInterceptor;
import com.truecaller.credit.data.api.CreditResetStateInterceptor;
import com.truecaller.credit.data.api.CreditUserAgentInterceptor;
import e.c.a;
import e.f.a;
import e.s;
import e.s.a;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.X509TrustManager;
import javax.security.auth.x500.X500Principal;
import okhttp3.a.a.a;
import okhttp3.ag;
import okhttp3.k.a;
import okhttp3.v;
import okhttp3.y.a;
import org.apache.http.conn.ssl.AbstractVerifier;

public final class p
{
  public static com.truecaller.common.network.a a(r paramr, dagger.a parama, af paramaf)
  {
    c.g.b.k.b(paramr, "truecallerAccountManager");
    c.g.b.k.b(parama, "tempTokenManager");
    c.g.b.k.b(paramaf, "crossDomainSupport");
    com.truecaller.common.network.a locala = new com/truecaller/common/network/a;
    paramaf = (i)paramaf;
    locala.<init>(true, paramr, parama, paramaf);
    return locala;
  }
  
  public static com.truecaller.credit.a a(g paramg, com.truecaller.utils.d paramd)
  {
    c.g.b.k.b(paramg, "creditSettings");
    c.g.b.k.b(paramd, "deviceInfoUtil");
    b localb = new com/truecaller/credit/b;
    localb.<init>(paramg, paramd);
    return (com.truecaller.credit.a)localb;
  }
  
  public static CreditApiService a(s params)
  {
    c.g.b.k.b(params, "retrofit");
    params = params.a(CreditApiService.class);
    c.g.b.k.a(params, "retrofit.create(CreditApiService::class.java)");
    return (CreditApiService)params;
  }
  
  public static s a(okhttp3.y paramy, com.truecaller.credit.a parama)
  {
    c.g.b.k.b(paramy, "okHttpClient");
    c.g.b.k.b(parama, "creditEnvironment");
    Object localObject = new e/s$a;
    ((s.a)localObject).<init>();
    parama = parama.a();
    parama = ((s.a)localObject).a(parama);
    localObject = com.c.b.a.a.a.a.a;
    localObject = (c.a)a.b.a();
    parama = parama.a((c.a)localObject).a();
    localObject = (f.a)e.a.a.a.a();
    paramy = parama.a((f.a)localObject).a(paramy).b();
    c.g.b.k.a(paramy, "Retrofit.Builder()\n     …ent)\n            .build()");
    return paramy;
  }
  
  public static okhttp3.a.a a()
  {
    okhttp3.a.a locala = new okhttp3/a/a;
    locala.<init>();
    a.a locala1 = a.a.a;
    locala.a(locala1);
    return locala;
  }
  
  public static okhttp3.y a(y.a parama, com.truecaller.common.network.a parama1, CreditNetworkInterceptor paramCreditNetworkInterceptor, CreditErrorInterceptor paramCreditErrorInterceptor, CreditCustomErrorInterceptor paramCreditCustomErrorInterceptor, CreditResetStateInterceptor paramCreditResetStateInterceptor, CreditAppStateInterceptor paramCreditAppStateInterceptor, CreditUserAgentInterceptor paramCreditUserAgentInterceptor)
  {
    c.g.b.k.b(parama, "okHttpBuilder");
    c.g.b.k.b(parama1, "authRequestInterceptor");
    c.g.b.k.b(paramCreditNetworkInterceptor, "creditNetworkInterceptor");
    c.g.b.k.b(paramCreditErrorInterceptor, "creditErrorInterceptor");
    c.g.b.k.b(paramCreditCustomErrorInterceptor, "creditCustomErrorInterceptor");
    c.g.b.k.b(paramCreditResetStateInterceptor, "creditResetStateInterceptor");
    c.g.b.k.b(paramCreditAppStateInterceptor, "creditAppStateInterceptor");
    c.g.b.k.b(paramCreditUserAgentInterceptor, "creditUserAgentInterceptor");
    parama1 = (v)parama1;
    parama.a(parama1);
    paramCreditNetworkInterceptor = (v)paramCreditNetworkInterceptor;
    parama.a(paramCreditNetworkInterceptor);
    paramCreditErrorInterceptor = (v)paramCreditErrorInterceptor;
    parama.a(paramCreditErrorInterceptor);
    paramCreditCustomErrorInterceptor = (v)paramCreditCustomErrorInterceptor;
    parama.a(paramCreditCustomErrorInterceptor);
    paramCreditResetStateInterceptor = (v)paramCreditResetStateInterceptor;
    parama.a(paramCreditResetStateInterceptor);
    paramCreditAppStateInterceptor = (v)paramCreditAppStateInterceptor;
    parama.a(paramCreditAppStateInterceptor);
    paramCreditUserAgentInterceptor = (v)paramCreditUserAgentInterceptor;
    parama.a(paramCreditUserAgentInterceptor);
    parama = parama.d();
    c.g.b.k.a(parama, "okHttpBuilder.build()");
    return parama;
  }
  
  private static boolean b(String paramString, SSLSession paramSSLSession)
  {
    try
    {
      paramSSLSession = paramSSLSession.getPeerCertificates();
      paramSSLSession = paramSSLSession[0];
      if (paramSSLSession != null)
      {
        paramSSLSession = (X509Certificate)paramSSLSession;
        paramString = (CharSequence)paramString;
        int i = paramString.length();
        int k = 1;
        int m = i - k;
        i = 0;
        Object localObject1 = null;
        boolean bool3 = false;
        Object localObject2 = null;
        int i4;
        Object localObject3;
        while (i <= m)
        {
          if (!bool3) {
            i3 = i;
          } else {
            i3 = m;
          }
          int i3 = paramString.charAt(i3);
          i4 = 32;
          if (i3 <= i4)
          {
            i3 = 1;
          }
          else
          {
            i3 = 0;
            localObject3 = null;
          }
          if (!bool3)
          {
            if (i3 == 0) {
              bool3 = true;
            } else {
              i += 1;
            }
          }
          else
          {
            if (i3 == 0) {
              break;
            }
            m += -1;
          }
        }
        int n;
        m += k;
        paramString = paramString.subSequence(i, n);
        paramString = paramString.toString();
        localObject1 = Locale.ENGLISH;
        Object localObject4 = "Locale.ENGLISH";
        c.g.b.k.a(localObject1, (String)localObject4);
        if (paramString != null)
        {
          paramString = paramString.toLowerCase((Locale)localObject1);
          localObject1 = "(this as java.lang.String).toLowerCase(locale)";
          c.g.b.k.a(paramString, (String)localObject1);
          localObject1 = paramSSLSession.getSubjectX500Principal();
          localObject1 = ((X500Principal)localObject1).toString();
          localObject4 = "cert.subjectX500Principal.toString()";
          c.g.b.k.a(localObject1, (String)localObject4);
          localObject1 = (CharSequence)localObject1;
          localObject4 = ",";
          localObject2 = new c/n/k;
          ((c.n.k)localObject2).<init>((String)localObject4);
          localObject1 = ((c.n.k)localObject2).a((CharSequence)localObject1, 0);
          boolean bool2 = ((List)localObject1).isEmpty();
          int i1;
          int i2;
          if (!bool2)
          {
            i1 = ((List)localObject1).size();
            localObject4 = ((List)localObject1).listIterator(i1);
            do
            {
              bool3 = ((ListIterator)localObject4).hasPrevious();
              if (!bool3) {
                break;
              }
              localObject2 = ((ListIterator)localObject4).previous();
              localObject2 = (String)localObject2;
              localObject2 = (CharSequence)localObject2;
              i2 = ((CharSequence)localObject2).length();
              if (i2 == 0)
              {
                i2 = 1;
              }
              else
              {
                i2 = 0;
                localObject2 = null;
              }
            } while (i2 != 0);
            localObject1 = (Iterable)localObject1;
            i1 = ((ListIterator)localObject4).nextIndex() + k;
            localObject1 = c.a.m.d((Iterable)localObject1, i1);
          }
          else
          {
            localObject1 = c.a.y.a;
            localObject1 = (List)localObject1;
          }
          localObject1 = (Collection)localObject1;
          if (localObject1 != null)
          {
            localObject4 = new String[0];
            localObject1 = ((Collection)localObject1).toArray((Object[])localObject4);
            if (localObject1 != null)
            {
              localObject1 = (String[])localObject1;
              i1 = localObject1.length;
              i2 = 0;
              localObject2 = null;
              Object localObject5;
              String str;
              while (i2 < i1)
              {
                localObject3 = localObject1[i2];
                localObject5 = localObject3;
                localObject5 = (CharSequence)localObject3;
                str = "CN=";
                int i5 = 6;
                i4 = c.n.m.a((CharSequence)localObject5, str, 0, false, i5);
                if (i4 >= 0)
                {
                  i4 += 3;
                  if (localObject3 != null)
                  {
                    localObject1 = ((String)localObject3).substring(i4);
                    localObject4 = "(this as java.lang.String).substring(startIndex)";
                    c.g.b.k.a(localObject1, (String)localObject4);
                    break label567;
                  }
                  paramString = new c/u;
                  paramSSLSession = "null cannot be cast to non-null type java.lang.String";
                  paramString.<init>(paramSSLSession);
                  throw paramString;
                }
                i2 += 1;
              }
              i = 0;
              localObject1 = null;
              label567:
              localObject1 = (CharSequence)localObject1;
              boolean bool1 = Pattern.matches(paramString, (CharSequence)localObject1);
              if (bool1) {
                return k;
              }
              paramSSLSession = AbstractVerifier.getDNSSubjectAlts(paramSSLSession);
              int j = paramSSLSession.length;
              i1 = 0;
              localObject4 = null;
              while (i1 < j)
              {
                localObject2 = paramSSLSession[i1];
                localObject3 = z.a;
                localObject3 = ".*%s$";
                localObject5 = new Object[k];
                str = "cn";
                c.g.b.k.a(localObject2, str);
                int i6 = ((String)localObject2).length() + -8;
                if (localObject2 != null)
                {
                  localObject2 = ((String)localObject2).substring(i6);
                  str = "(this as java.lang.String).substring(startIndex)";
                  c.g.b.k.a(localObject2, str);
                  localObject5[0] = localObject2;
                  localObject2 = Arrays.copyOf((Object[])localObject5, k);
                  localObject2 = String.format((String)localObject3, (Object[])localObject2);
                  localObject3 = "java.lang.String.format(format, *args)";
                  c.g.b.k.a(localObject2, (String)localObject3);
                  localObject2 = Pattern.compile((String)localObject2);
                  localObject3 = paramString;
                  localObject3 = (CharSequence)paramString;
                  localObject2 = ((Pattern)localObject2).matcher((CharSequence)localObject3);
                  boolean bool4 = ((Matcher)localObject2).find();
                  if (bool4) {
                    return k;
                  }
                  i1 += 1;
                }
                else
                {
                  paramString = new c/u;
                  paramSSLSession = "null cannot be cast to non-null type java.lang.String";
                  paramString.<init>(paramSSLSession);
                  throw paramString;
                }
              }
              return false;
            }
            paramString = new c/u;
            paramSSLSession = "null cannot be cast to non-null type kotlin.Array<T>";
            paramString.<init>(paramSSLSession);
            throw paramString;
          }
          paramString = new c/u;
          paramSSLSession = "null cannot be cast to non-null type java.util.Collection<T>";
          paramString.<init>(paramSSLSession);
          throw paramString;
        }
        paramString = new c/u;
        paramSSLSession = "null cannot be cast to non-null type java.lang.String";
        paramString.<init>(paramSSLSession);
        throw paramString;
      }
      paramString = new c/u;
      paramSSLSession = "null cannot be cast to non-null type java.security.cert.X509Certificate";
      paramString.<init>(paramSSLSession);
      throw paramString;
    }
    catch (SSLException localSSLException)
    {
      for (;;) {}
    }
    return false;
  }
  
  public final y.a a(okhttp3.a.a parama, SSLSocketFactory paramSSLSocketFactory, X509TrustManager paramX509TrustManager)
  {
    c.g.b.k.b(parama, "httpLoggingInterceptor");
    c.g.b.k.b(paramSSLSocketFactory, "trustedSocketFactory");
    c.g.b.k.b(paramX509TrustManager, "trustManager");
    y.a locala = new okhttp3/y$a;
    locala.<init>();
    parama = (v)parama;
    locala.b(parama);
    parama = TimeUnit.MINUTES;
    long l = 1L;
    locala.a(l, parama);
    parama = TimeUnit.MINUTES;
    locala.a(parama);
    parama = TimeUnit.MINUTES;
    locala.b(l, parama);
    parama = new com/truecaller/credit/app/c/b/p$a;
    Object localObject = this;
    localObject = (p)this;
    parama.<init>((p)localObject);
    parama = (c.g.a.m)parama;
    localObject = new com/truecaller/credit/app/c/b/q;
    ((q)localObject).<init>(parama);
    localObject = (HostnameVerifier)localObject;
    locala.a((HostnameVerifier)localObject);
    int i = Build.VERSION.SDK_INT;
    int j = 22;
    if (i < j) {
      try
      {
        parama = new com/truecaller/utils/k;
        parama.<init>(paramSSLSocketFactory);
        parama = (SSLSocketFactory)parama;
        locala.a(parama, paramX509TrustManager);
        parama = new okhttp3/k$a;
        paramSSLSocketFactory = okhttp3.k.b;
        parama.<init>(paramSSLSocketFactory);
        int k = 1;
        paramSSLSocketFactory = new ag[k];
        paramX509TrustManager = null;
        localObject = ag.b;
        paramSSLSocketFactory[0] = localObject;
        parama = parama.a(paramSSLSocketFactory);
        parama = parama.b();
        paramSSLSocketFactory = new java/util/ArrayList;
        paramSSLSocketFactory.<init>();
        paramSSLSocketFactory.add(parama);
        parama = okhttp3.k.c;
        paramSSLSocketFactory.add(parama);
        parama = okhttp3.k.d;
        paramSSLSocketFactory.add(parama);
        paramSSLSocketFactory = (List)paramSSLSocketFactory;
        locala.b(paramSSLSocketFactory);
      }
      catch (Exception localException)
      {
        parama = (Throwable)localException;
        com.truecaller.log.d.a(parama);
      }
    } else {
      locala.a(paramSSLSocketFactory, paramX509TrustManager);
    }
    return locala;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.c.b.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */