package com.truecaller.credit.app.c.b;

import android.app.Application;
import android.arch.persistence.room.f.a;
import android.content.Context;
import android.content.SharedPreferences;
import c.g.b.k;
import c.u;
import com.d.b.w;
import com.google.gson.q;
import com.truecaller.credit.app.ui.onboarding.a.b;
import com.truecaller.credit.app.util.f;
import com.truecaller.credit.app.util.g;
import com.truecaller.credit.data.api.CreditApiService;
import com.truecaller.credit.data.repository.CreditRepository;
import com.truecaller.credit.data.repository.CreditRepository.Network;
import com.truecaller.credit.db.CreditDb;
import com.truecaller.credit.j;
import com.truecaller.notificationchannels.n;
import com.truecaller.notificationchannels.n.a;

public final class a
{
  final Application a;
  final j b;
  
  public a(Application paramApplication, j paramj)
  {
    a = paramApplication;
    b = paramj;
  }
  
  public static q a()
  {
    q localq = new com/google/gson/q;
    localq.<init>();
    return localq;
  }
  
  public static com.truecaller.credit.app.ui.a.a.a a(CreditDb paramCreditDb)
  {
    k.b(paramCreditDb, "creditDb");
    return paramCreditDb.h();
  }
  
  public static CreditRepository.Network a(CreditApiService paramCreditApiService)
  {
    k.b(paramCreditApiService, "creditApiService");
    CreditRepository.Network localNetwork = new com/truecaller/credit/data/repository/CreditRepository$Network;
    localNetwork.<init>(paramCreditApiService);
    return localNetwork;
  }
  
  public static CreditRepository a(CreditRepository.Network paramNetwork)
  {
    k.b(paramNetwork, "creditRepository");
    return (CreditRepository)paramNetwork;
  }
  
  public static com.truecaller.notificationchannels.e a(Context paramContext)
  {
    k.b(paramContext, "context");
    return n.a.a(paramContext).c();
  }
  
  public static com.truecaller.credit.app.ui.loanhistory.a.a b(CreditDb paramCreditDb)
  {
    k.b(paramCreditDb, "creditDb");
    return paramCreditDb.i();
  }
  
  public static com.truecaller.featuretoggles.e b(Context paramContext)
  {
    String str = "context";
    k.b(paramContext, str);
    paramContext = paramContext.getApplicationContext();
    if (paramContext != null)
    {
      paramContext = ((com.truecaller.common.b.a)paramContext).f();
      k.a(paramContext, "(context.applicationCont…tionBase).featureRegistry");
      return paramContext;
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type com.truecaller.common.app.ApplicationBase");
    throw paramContext;
  }
  
  public static b c(CreditDb paramCreditDb)
  {
    k.b(paramCreditDb, "creditDb");
    return paramCreditDb.j();
  }
  
  public static f c(Context paramContext)
  {
    k.b(paramContext, "context");
    g localg = new com/truecaller/credit/app/util/g;
    localg.<init>(paramContext);
    return (f)localg;
  }
  
  public static CreditDb d(Context paramContext)
  {
    k.b(paramContext, "context");
    paramContext = android.arch.persistence.room.e.a(paramContext, CreditDb.class, "tc_credit.db");
    android.arch.persistence.room.a.a[] arrayOfa = new android.arch.persistence.room.a.a[2];
    android.arch.persistence.room.a.a locala = com.truecaller.credit.db.a.a();
    arrayOfa[0] = locala;
    locala = com.truecaller.credit.db.a.b();
    arrayOfa[1] = locala;
    paramContext = paramContext.a(arrayOfa).b();
    k.a(paramContext, "Room.databaseBuilder<Cre…5_6)\n            .build()");
    return (CreditDb)paramContext;
  }
  
  public static SharedPreferences e(Context paramContext)
  {
    k.b(paramContext, "context");
    paramContext = paramContext.getSharedPreferences("tc_credit_preference", 0);
    k.a(paramContext, "context.getSharedPrefere…PREFERENCE, MODE_PRIVATE)");
    return paramContext;
  }
  
  public static w f(Context paramContext)
  {
    k.b(paramContext, "context");
    paramContext = w.a(paramContext);
    k.a(paramContext, "Picasso.with(context)");
    return paramContext;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.c.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */