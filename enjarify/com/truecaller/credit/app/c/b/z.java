package com.truecaller.credit.app.c.b;

import dagger.a.d;
import javax.inject.Provider;

public final class z
  implements d
{
  private final y a;
  private final Provider b;
  
  private z(y paramy, Provider paramProvider)
  {
    a = paramy;
    b = paramProvider;
  }
  
  public static z a(y paramy, Provider paramProvider)
  {
    z localz = new com/truecaller/credit/app/c/b/z;
    localz.<init>(paramy, paramProvider);
    return localz;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.c.b.z
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */