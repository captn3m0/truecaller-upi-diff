package com.truecaller.credit.app.c.b;

import dagger.a.d;
import javax.inject.Provider;

public final class t
  implements d
{
  private final p a;
  private final Provider b;
  
  private t(p paramp, Provider paramProvider)
  {
    a = paramp;
    b = paramProvider;
  }
  
  public static t a(p paramp, Provider paramProvider)
  {
    t localt = new com/truecaller/credit/app/c/b/t;
    localt.<init>(paramp, paramProvider);
    return localt;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.c.b.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */