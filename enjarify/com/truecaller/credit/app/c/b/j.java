package com.truecaller.credit.app.c.b;

import dagger.a.d;
import javax.inject.Provider;

public final class j
  implements d
{
  private final a a;
  private final Provider b;
  
  private j(a parama, Provider paramProvider)
  {
    a = parama;
    b = paramProvider;
  }
  
  public static j a(a parama, Provider paramProvider)
  {
    j localj = new com/truecaller/credit/app/c/b/j;
    localj.<init>(parama, paramProvider);
    return localj;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.c.b.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */