package com.truecaller.credit.app.c.b;

import dagger.a.d;
import javax.inject.Provider;

public final class ab
  implements d
{
  private final y a;
  private final Provider b;
  
  private ab(y paramy, Provider paramProvider)
  {
    a = paramy;
    b = paramProvider;
  }
  
  public static ab a(y paramy, Provider paramProvider)
  {
    ab localab = new com/truecaller/credit/app/c/b/ab;
    localab.<init>(paramy, paramProvider);
    return localab;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.c.b.ab
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */