package com.truecaller.credit.app.c.b;

import dagger.a.d;
import javax.inject.Provider;

public final class s
  implements d
{
  private final p a;
  private final Provider b;
  private final Provider c;
  
  private s(p paramp, Provider paramProvider1, Provider paramProvider2)
  {
    a = paramp;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static s a(p paramp, Provider paramProvider1, Provider paramProvider2)
  {
    s locals = new com/truecaller/credit/app/c/b/s;
    locals.<init>(paramp, paramProvider1, paramProvider2);
    return locals;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.c.b.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */