package com.truecaller.credit.app.ui.infocollection.views.c;

import com.truecaller.credit.data.models.APIStatusMessage;
import com.truecaller.credit.data.models.UserInfoDataRequest;

public abstract interface g$b
{
  public abstract void a(APIStatusMessage paramAPIStatusMessage);
  
  public abstract void a(String paramString);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract void b(String paramString);
  
  public abstract void c(String paramString);
  
  public abstract void d(String paramString);
  
  public abstract void e(String paramString);
  
  public abstract void f(String paramString);
  
  public abstract UserInfoDataRequest g();
  
  public abstract void h();
  
  public abstract String i();
  
  public abstract void j();
  
  public abstract void k();
  
  public abstract void l();
  
  public abstract void m();
  
  public abstract void n();
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.views.c.g.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */