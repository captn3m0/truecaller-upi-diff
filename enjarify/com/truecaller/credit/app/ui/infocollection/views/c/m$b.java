package com.truecaller.credit.app.ui.infocollection.views.c;

import android.graphics.drawable.Drawable;
import com.truecaller.credit.app.ui.assist.CreditDocumentType;
import com.truecaller.credit.data.models.UserInfoDataRequest;

public abstract interface m$b
{
  public static final m.b.a a = m.b.a.a;
  
  public abstract void a(int paramInt1, int paramInt2, int paramInt3, long paramLong);
  
  public abstract void a(CreditDocumentType paramCreditDocumentType, String paramString);
  
  public abstract void a(UserInfoDataRequest paramUserInfoDataRequest);
  
  public abstract void a(String paramString);
  
  public abstract void a(String paramString, Drawable paramDrawable, int paramInt);
  
  public abstract void b(String paramString);
  
  public abstract void c(String paramString);
  
  public abstract void d(String paramString);
  
  public abstract void e(String paramString);
  
  public abstract void f(String paramString);
  
  public abstract CreditDocumentType g();
  
  public abstract void g(String paramString);
  
  public abstract UserInfoDataRequest h();
  
  public abstract void h(String paramString);
  
  public abstract void i();
  
  public abstract void i(String paramString);
  
  public abstract void j();
  
  public abstract void j(String paramString);
  
  public abstract void k();
  
  public abstract void k(String paramString);
  
  public abstract void l();
  
  public abstract void l(String paramString);
  
  public abstract void m();
  
  public abstract void n();
  
  public abstract void o();
  
  public abstract void p();
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.views.c.m.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */