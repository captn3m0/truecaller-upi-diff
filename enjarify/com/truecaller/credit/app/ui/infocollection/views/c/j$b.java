package com.truecaller.credit.app.ui.infocollection.views.c;

import android.net.Uri;
import com.truecaller.credit.app.ui.assist.CreditDocumentType;

public abstract interface j$b
{
  public abstract void a(Uri paramUri, String paramString);
  
  public abstract void a(String paramString);
  
  public abstract Uri g();
  
  public abstract CreditDocumentType h();
  
  public abstract String i();
  
  public abstract void j();
  
  public abstract void k();
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.views.c.j.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */