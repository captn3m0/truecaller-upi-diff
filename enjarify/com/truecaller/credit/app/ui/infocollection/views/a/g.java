package com.truecaller.credit.app.ui.infocollection.views.a;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import c.g.b.k;
import com.truecaller.credit.R.id;
import com.truecaller.credit.app.ui.customview.CustomInfoCollectionRadioButton;
import com.truecaller.credit.domain.interactors.infocollection.models.IFSCDetails;
import java.util.HashMap;
import kotlinx.a.a.a;

public final class g
  extends RecyclerView.ViewHolder
  implements f, a
{
  private final View a;
  private HashMap b;
  
  public g(View paramView)
  {
    super(paramView);
    a = paramView;
  }
  
  private View b(int paramInt)
  {
    Object localObject1 = b;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      b = ((HashMap)localObject1);
    }
    localObject1 = b;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = a();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = b;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final View a()
  {
    return a;
  }
  
  public final void a(int paramInt)
  {
    int i = R.id.customRadioButton;
    CustomInfoCollectionRadioButton localCustomInfoCollectionRadioButton = (CustomInfoCollectionRadioButton)b(i);
    int j = getAdapterPosition();
    if (j == paramInt) {
      paramInt = 1;
    } else {
      paramInt = 0;
    }
    localCustomInfoCollectionRadioButton.setChecked(paramInt);
  }
  
  public final void a(IFSCDetails paramIFSCDetails)
  {
    k.b(paramIFSCDetails, "ifscDetails");
    int i = R.id.customRadioButton;
    CustomInfoCollectionRadioButton localCustomInfoCollectionRadioButton = (CustomInfoCollectionRadioButton)b(i);
    Object localObject = paramIFSCDetails.getBank();
    localCustomInfoCollectionRadioButton.setTitle((String)localObject);
    i = R.id.customRadioButton;
    localCustomInfoCollectionRadioButton = (CustomInfoCollectionRadioButton)b(i);
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    String str = paramIFSCDetails.getBranch();
    ((StringBuilder)localObject).append(str);
    ((StringBuilder)localObject).append(", IFSC: ");
    paramIFSCDetails = paramIFSCDetails.getIfsc();
    ((StringBuilder)localObject).append(paramIFSCDetails);
    paramIFSCDetails = ((StringBuilder)localObject).toString();
    localCustomInfoCollectionRadioButton.a(paramIFSCDetails, false);
    int j = R.id.customRadioButton;
    ((CustomInfoCollectionRadioButton)b(j)).setContainerBackground(-1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.views.a.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */