package com.truecaller.credit.app.ui.infocollection.views.a;

import android.net.Uri;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.ImageView;
import com.d.b.ab;
import com.d.b.w;
import com.truecaller.credit.R.id;
import com.truecaller.credit.app.ui.customview.CustomInfoCollectionRadioButton;
import com.truecaller.credit.domain.interactors.infocollection.models.PoaData;
import com.truecaller.credit.domain.interactors.infocollection.models.PoaImage;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;
import java.util.List;
import kotlinx.a.a.a;

public final class l
  extends RecyclerView.ViewHolder
  implements k, a
{
  private w a;
  private final View b;
  private HashMap c;
  
  public l(View paramView)
  {
    super(paramView);
    b = paramView;
  }
  
  private View b(int paramInt)
  {
    Object localObject1 = c;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      c = ((HashMap)localObject1);
    }
    localObject1 = c;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = a();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = c;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final View a()
  {
    return b;
  }
  
  public final void a(int paramInt)
  {
    int i = R.id.textPoaType;
    CustomInfoCollectionRadioButton localCustomInfoCollectionRadioButton = (CustomInfoCollectionRadioButton)b(i);
    int j = getAdapterPosition();
    if (j == paramInt) {
      paramInt = 1;
    } else {
      paramInt = 0;
    }
    localCustomInfoCollectionRadioButton.setChecked(paramInt);
  }
  
  public final void a(Uri paramUri, String paramString, boolean paramBoolean)
  {
    c.g.b.k.b(paramUri, "uri");
    c.g.b.k.b(paramString, "message");
    int i = R.id.textPoaType;
    CustomInfoCollectionRadioButton localCustomInfoCollectionRadioButton = (CustomInfoCollectionRadioButton)b(i);
    localCustomInfoCollectionRadioButton.b();
    Object localObject = a;
    if (localObject == null)
    {
      String str = "picasso";
      c.g.b.k.a(str);
    }
    paramUri = ((w)localObject).a(paramUri);
    localObject = localCustomInfoCollectionRadioButton.getFrontImage();
    paramUri.a((ImageView)localObject, null);
    localCustomInfoCollectionRadioButton.a(paramString, paramBoolean);
    localCustomInfoCollectionRadioButton.setChecked(true);
  }
  
  public final void a(w paramw)
  {
    c.g.b.k.b(paramw, "picasso");
    a = paramw;
  }
  
  public final void a(PoaData paramPoaData)
  {
    c.g.b.k.b(paramPoaData, "poaData");
    int i = R.id.textPoaType;
    CustomInfoCollectionRadioButton localCustomInfoCollectionRadioButton = (CustomInfoCollectionRadioButton)b(i);
    paramPoaData = paramPoaData.getValue();
    localCustomInfoCollectionRadioButton.setTitle(paramPoaData);
  }
  
  public final void a(List paramList, String paramString, boolean paramBoolean)
  {
    c.g.b.k.b(paramList, "images");
    c.g.b.k.b(paramString, "message");
    int i = R.id.textPoaType;
    CustomInfoCollectionRadioButton localCustomInfoCollectionRadioButton = (CustomInfoCollectionRadioButton)b(i);
    localCustomInfoCollectionRadioButton.b();
    int j = R.id.imageTwo;
    Object localObject1 = (ImageView)localCustomInfoCollectionRadioButton.a(j);
    Object localObject2 = "imageTwo";
    c.g.b.k.a(localObject1, (String)localObject2);
    t.a((View)localObject1);
    localObject1 = a;
    if (localObject1 == null)
    {
      localObject2 = "picasso";
      c.g.b.k.a((String)localObject2);
    }
    int k = 0;
    localObject2 = ((PoaImage)paramList.get(0)).getUri();
    localObject1 = ((w)localObject1).a((Uri)localObject2);
    localObject2 = localCustomInfoCollectionRadioButton.getFrontImage();
    ((ab)localObject1).a((ImageView)localObject2, null);
    localObject1 = a;
    if (localObject1 == null)
    {
      localObject2 = "picasso";
      c.g.b.k.a((String)localObject2);
    }
    k = 1;
    paramList = ((PoaImage)paramList.get(k)).getUri();
    paramList = ((w)localObject1).a(paramList);
    localObject1 = localCustomInfoCollectionRadioButton.getBackImage();
    paramList.a((ImageView)localObject1, null);
    localCustomInfoCollectionRadioButton.a(paramString, paramBoolean);
    localCustomInfoCollectionRadioButton.setChecked(k);
  }
  
  public final void b()
  {
    int i = R.id.textPoaType;
    CustomInfoCollectionRadioButton localCustomInfoCollectionRadioButton = (CustomInfoCollectionRadioButton)b(i);
    localCustomInfoCollectionRadioButton.setImageOne(null);
    localCustomInfoCollectionRadioButton.setImageTwo(null);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.views.a.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */