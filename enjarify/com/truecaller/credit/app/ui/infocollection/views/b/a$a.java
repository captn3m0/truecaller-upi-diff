package com.truecaller.credit.app.ui.infocollection.views.b;

import android.os.Bundle;
import android.os.Parcelable;
import c.g.b.k;
import com.truecaller.credit.data.models.APIStatusMessage;

public final class a$a
{
  public static a a(APIStatusMessage paramAPIStatusMessage)
  {
    k.b(paramAPIStatusMessage, "item");
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    paramAPIStatusMessage = (Parcelable)paramAPIStatusMessage;
    localBundle.putParcelable("api_status_message", paramAPIStatusMessage);
    paramAPIStatusMessage = new com/truecaller/credit/app/ui/infocollection/views/b/a;
    paramAPIStatusMessage.<init>();
    paramAPIStatusMessage.setArguments(localBundle);
    return paramAPIStatusMessage;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.views.b.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */