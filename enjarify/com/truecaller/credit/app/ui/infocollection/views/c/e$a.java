package com.truecaller.credit.app.ui.infocollection.views.c;

import android.net.Uri;
import com.truecaller.bm;
import com.truecaller.credit.domain.interactors.infocollection.models.PoaData;

public abstract interface e$a
  extends bm
{
  public abstract void a();
  
  public abstract void a(int paramInt1, int paramInt2, Uri paramUri);
  
  public abstract void a(PoaData paramPoaData);
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.views.c.e.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */