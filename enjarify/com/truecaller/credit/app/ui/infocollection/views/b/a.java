package com.truecaller.credit.app.ui.infocollection.views.b;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.constraint.Group;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.credit.R.id;
import com.truecaller.credit.R.layout;
import com.truecaller.credit.app.ui.infocollection.views.c.a.b;
import com.truecaller.credit.app.ui.infocollection.views.c.b;
import com.truecaller.credit.data.models.APIStatusMessage;
import com.truecaller.credit.i.a;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;

public final class a
  extends com.truecaller.credit.app.ui.b.a
  implements View.OnClickListener, a.b
{
  public static final a.a d;
  public b b;
  public com.truecaller.credit.app.ui.infocollection.views.c.a.a c;
  private HashMap e;
  
  static
  {
    a.a locala = new com/truecaller/credit/app/ui/infocollection/views/b/a$a;
    locala.<init>((byte)0);
    d = locala;
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = e;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      e = ((HashMap)localObject1);
    }
    localObject1 = e;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = e;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a()
  {
    i.a.a().a(this);
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "title");
    int i = R.id.tvLoading;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "tvLoading");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final int b()
  {
    return R.layout.fragment_status;
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "title");
    int i = R.id.tvSuccess;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "tvSuccess");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void c()
  {
    HashMap localHashMap = e;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "title");
    int i = R.id.tvErrorTitle;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "tvErrorTitle");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void d()
  {
    b localb = b;
    if (localb != null) {
      localb.G_();
    }
    dismiss();
  }
  
  public final void d(String paramString)
  {
    k.b(paramString, "description");
    int i = R.id.tvErrorDescription;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "tvErrorDescription");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void e()
  {
    com.truecaller.credit.app.ui.infocollection.views.c.a.a locala = c;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    locala.a();
  }
  
  public final void e(String paramString)
  {
    k.b(paramString, "continueButtonText");
    int i = R.id.btnContinue;
    Button localButton = (Button)a(i);
    k.a(localButton, "btnContinue");
    paramString = (CharSequence)paramString;
    localButton.setText(paramString);
  }
  
  public final void f()
  {
    int i = R.id.groupLoading;
    Group localGroup = (Group)a(i);
    k.a(localGroup, "groupLoading");
    t.a((View)localGroup);
  }
  
  public final void g()
  {
    int i = R.id.groupLoading;
    Group localGroup = (Group)a(i);
    k.a(localGroup, "groupLoading");
    t.b((View)localGroup);
  }
  
  public final void h()
  {
    int i = R.id.groupSuccess;
    Group localGroup = (Group)a(i);
    k.a(localGroup, "groupSuccess");
    t.a((View)localGroup);
  }
  
  public final void i()
  {
    int i = R.id.groupSuccess;
    Group localGroup = (Group)a(i);
    k.a(localGroup, "groupSuccess");
    t.b((View)localGroup);
  }
  
  public final void j()
  {
    int i = R.id.groupFailure;
    Group localGroup = (Group)a(i);
    k.a(localGroup, "groupFailure");
    t.a((View)localGroup);
  }
  
  public final void k()
  {
    int i = R.id.groupFailure;
    Group localGroup = (Group)a(i);
    k.a(localGroup, "groupFailure");
    t.b((View)localGroup);
  }
  
  public final void l()
  {
    int i = R.id.btnBackToHome;
    Button localButton = (Button)a(i);
    k.a(localButton, "btnBackToHome");
    t.a((View)localButton);
  }
  
  public final void m()
  {
    int i = R.id.btnBackToHome;
    Button localButton = (Button)a(i);
    k.a(localButton, "btnBackToHome");
    t.b((View)localButton);
  }
  
  public final void n()
  {
    setCancelable(false);
  }
  
  public final void o()
  {
    setCancelable(true);
  }
  
  public final void onCancel(DialogInterface paramDialogInterface)
  {
    super.onCancel(paramDialogInterface);
    paramDialogInterface = c;
    if (paramDialogInterface == null)
    {
      String str = "presenter";
      k.a(str);
    }
    paramDialogInterface.a();
  }
  
  public final void onClick(View paramView)
  {
    int i = R.id.btnContinue;
    Button localButton = (Button)a(i);
    boolean bool1 = k.a(paramView, localButton);
    if (bool1)
    {
      paramView = b;
      if (paramView != null) {
        paramView.H_();
      }
      return;
    }
    int j = R.id.btnBackToHome;
    localButton = (Button)a(j);
    boolean bool2 = k.a(paramView, localButton);
    if (bool2)
    {
      paramView = b;
      if (paramView != null)
      {
        paramView.I_();
        return;
      }
    }
  }
  
  public final Dialog onCreateDialog(Bundle paramBundle)
  {
    paramBundle = new android/support/design/widget/a;
    Context localContext = requireContext();
    int i = getTheme();
    paramBundle.<init>(localContext, i);
    return (Dialog)paramBundle;
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    b = null;
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    String str = "view";
    k.b(paramView, str);
    super.onViewCreated(paramView, paramBundle);
    paramView = c;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramView.a(this);
    paramView = c;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramBundle = getArguments();
    if (paramBundle != null)
    {
      str = "api_status_message";
      paramBundle = (APIStatusMessage)paramBundle.getParcelable(str);
    }
    else
    {
      paramBundle = null;
    }
    paramView.a(paramBundle);
    int i = R.id.btnContinue;
    paramView = (Button)a(i);
    paramBundle = this;
    paramBundle = (View.OnClickListener)this;
    paramView.setOnClickListener(paramBundle);
    i = R.id.btnBackToHome;
    ((Button)a(i)).setOnClickListener(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.views.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */