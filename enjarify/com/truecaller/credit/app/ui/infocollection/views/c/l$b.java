package com.truecaller.credit.app.ui.infocollection.views.c;

import android.graphics.drawable.Drawable;

public abstract interface l$b
{
  public static final l.b.a a = l.b.a.a;
  
  public abstract void a(int paramInt, float paramFloat);
  
  public abstract void a(Drawable paramDrawable);
  
  public abstract void a(String paramString);
  
  public abstract void b(String paramString);
  
  public abstract void d(String paramString);
  
  public abstract void i();
  
  public abstract void j();
  
  public abstract void k();
  
  public abstract void l();
  
  public abstract String m();
  
  public abstract void r();
  
  public abstract void s();
  
  public abstract void t();
  
  public abstract void u();
  
  public abstract void v();
  
  public abstract void w();
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.views.c.l.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */