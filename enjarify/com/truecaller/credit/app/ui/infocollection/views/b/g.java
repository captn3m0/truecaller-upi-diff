package com.truecaller.credit.app.ui.infocollection.views.b;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import c.g.b.k;
import c.u;
import com.d.b.ab;
import com.d.b.w;
import com.google.android.gms.vision.CameraSource;
import com.truecaller.credit.R.id;
import com.truecaller.credit.R.layout;
import com.truecaller.credit.app.ui.assist.CreditDocumentType;
import com.truecaller.credit.app.ui.b.c;
import com.truecaller.credit.app.ui.customview.CameraScannerView;
import com.truecaller.credit.app.ui.infocollection.a.a.a.a;
import com.truecaller.credit.app.ui.infocollection.a.a.b;
import com.truecaller.credit.app.ui.infocollection.views.c.h.a;
import com.truecaller.credit.app.ui.infocollection.views.c.h.b;
import com.truecaller.credit.i;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;

public final class g
  extends c
  implements View.OnClickListener, h.b
{
  public w c;
  private com.truecaller.credit.app.ui.infocollection.views.c.i.a d;
  private View e;
  private HashMap f;
  
  public final View a(int paramInt)
  {
    Object localObject1 = f;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      f = ((HashMap)localObject1);
    }
    localObject1 = f;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = f;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a(Uri paramUri)
  {
    Object localObject = c;
    if (localObject == null)
    {
      String str = "picasso";
      k.a(str);
    }
    paramUri = ((w)localObject).a(paramUri);
    int i = R.id.imagePreview;
    localObject = (ImageView)a(i);
    paramUri.a((ImageView)localObject, null);
  }
  
  public final void a(Uri paramUri, String paramString)
  {
    com.truecaller.credit.app.ui.infocollection.views.c.i.a locala = d;
    if (locala == null)
    {
      String str = "captureListener";
      k.a(str);
    }
    locala.a(paramUri, paramString);
  }
  
  public final void a(CameraSource paramCameraSource)
  {
    k.b(paramCameraSource, "camera");
    int i = R.id.cameraScanner;
    CameraScannerView localCameraScannerView = (CameraScannerView)a(i);
    Object localObject = "cameraSource";
    k.b(paramCameraSource, (String)localObject);
    int j = localCameraScannerView.getChildCount();
    if (j == 0)
    {
      localObject = (View)a;
      localCameraScannerView.addView((View)localObject);
    }
    c = paramCameraSource;
    b = true;
    localCameraScannerView.a();
  }
  
  public final void a(CreditDocumentType paramCreditDocumentType, int paramInt)
  {
    k.b(paramCreditDocumentType, "type");
    Object localObject1 = getActivity();
    if (localObject1 != null)
    {
      Object localObject2 = new com/truecaller/credit/app/ui/customview/a;
      String str = "it";
      k.a(localObject1, str);
      localObject1 = (Context)localObject1;
      ((com.truecaller.credit.app.ui.customview.a)localObject2).<init>((Context)localObject1, paramCreditDocumentType);
      localObject2 = (View)localObject2;
      e = ((View)localObject2);
      paramCreditDocumentType = e;
      if (paramCreditDocumentType != null) {
        paramCreditDocumentType.setBackgroundColor(paramInt);
      }
      int i = R.id.overlay;
      paramCreditDocumentType = (FrameLayout)a(i);
      View localView = e;
      paramCreditDocumentType.addView(localView);
      return;
    }
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "description");
    int i = R.id.textCameraDescription;
    TextView localTextView = (TextView)a(i);
    t.a((View)localTextView);
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void a(boolean paramBoolean)
  {
    int i = R.id.overlay;
    FrameLayout localFrameLayout = (FrameLayout)a(i);
    k.a(localFrameLayout, "overlay");
    t.a((View)localFrameLayout, paramBoolean);
  }
  
  public final void b()
  {
    a.a locala = com.truecaller.credit.app.ui.infocollection.a.a.a.a();
    Object localObject = i.i;
    localObject = com.truecaller.credit.i.a.a();
    locala.a((com.truecaller.credit.app.c.a.a)localObject).a().a(this);
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "message");
    Context localContext = getContext();
    paramString = (CharSequence)paramString;
    Toast.makeText(localContext, paramString, 0).show();
  }
  
  public final void b(boolean paramBoolean)
  {
    int i = R.id.imagePreview;
    ImageView localImageView = (ImageView)a(i);
    k.a(localImageView, "imagePreview");
    t.a((View)localImageView, paramBoolean);
  }
  
  public final int c()
  {
    return R.layout.fragment_document_capture;
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "text");
    com.truecaller.credit.app.ui.infocollection.views.c.i.a locala = d;
    if (locala == null)
    {
      String str = "captureListener";
      k.a(str);
    }
    locala.b(paramString);
  }
  
  public final void c(boolean paramBoolean)
  {
    int i = R.id.buttonCapture;
    ImageView localImageView = (ImageView)a(i);
    k.a(localImageView, "buttonCapture");
    t.a((View)localImageView, paramBoolean);
  }
  
  public final void d(boolean paramBoolean)
  {
    int i = R.id.pbCapture;
    ProgressBar localProgressBar = (ProgressBar)a(i);
    k.a(localProgressBar, "pbCapture");
    t.a((View)localProgressBar, paramBoolean);
  }
  
  public final void e(boolean paramBoolean)
  {
    int i = R.id.buttonCapture;
    ImageView localImageView = (ImageView)a(i);
    k.a(localImageView, "buttonCapture");
    t.a((View)localImageView, paramBoolean, 1.0F);
  }
  
  public final void f()
  {
    HashMap localHashMap = f;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final void f(boolean paramBoolean)
  {
    int i = R.id.buttonRetake;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "buttonRetake");
    t.a((View)localTextView, paramBoolean);
  }
  
  public final String g()
  {
    Object localObject = getArguments();
    if (localObject != null)
    {
      String str = "camera_type";
      localObject = ((Bundle)localObject).getString(str);
      if (localObject != null) {}
    }
    else
    {
      localObject = "back";
    }
    return (String)localObject;
  }
  
  public final void g(boolean paramBoolean)
  {
    int i = R.id.buttonDone;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "buttonDone");
    t.a((View)localTextView, paramBoolean);
  }
  
  public final CreditDocumentType h()
  {
    Object localObject = getArguments();
    if (localObject != null)
    {
      String str = "document_type";
      localObject = (CreditDocumentType)((Bundle)localObject).getParcelable(str);
    }
    else
    {
      localObject = null;
    }
    if (localObject != null) {
      return (CreditDocumentType)localObject;
    }
    localObject = new c/u;
    ((u)localObject).<init>("null cannot be cast to non-null type com.truecaller.credit.app.ui.assist.CreditDocumentType");
    throw ((Throwable)localObject);
  }
  
  public final void h(boolean paramBoolean)
  {
    int i = R.id.cameraScanner;
    CameraScannerView localCameraScannerView = (CameraScannerView)a(i);
    k.a(localCameraScannerView, "cameraScanner");
    t.a((View)localCameraScannerView, paramBoolean);
  }
  
  public final boolean i()
  {
    Bundle localBundle = getArguments();
    if (localBundle != null) {
      return localBundle.getBoolean("qr_reader");
    }
    return false;
  }
  
  public final void j()
  {
    Object localObject1 = getActivity();
    if (localObject1 != null)
    {
      localObject1 = (AppCompatActivity)localObject1;
      Object localObject2 = localObject1;
      localObject2 = (f)localObject1;
      int i = R.id.toolbar;
      localObject2 = (Toolbar)((f)localObject2).findViewById(i);
      ((AppCompatActivity)localObject1).setSupportActionBar((Toolbar)localObject2);
      localObject2 = ((AppCompatActivity)localObject1).getSupportActionBar();
      if (localObject2 != null)
      {
        i = 1;
        ((ActionBar)localObject2).setDisplayHomeAsUpEnabled(i);
      }
      localObject2 = (CharSequence)"";
      ((AppCompatActivity)localObject1).setTitle((CharSequence)localObject2);
      localObject1 = ((AppCompatActivity)localObject1).getSupportActionBar();
      if (localObject1 != null)
      {
        ((ActionBar)localObject1).setElevation(0.0F);
        return;
      }
      return;
    }
    localObject1 = new c/u;
    ((u)localObject1).<init>("null cannot be cast to non-null type android.support.v7.app.AppCompatActivity");
    throw ((Throwable)localObject1);
  }
  
  public final void k()
  {
    int i = R.id.textCameraDescription;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "textCameraDescription");
    t.b((View)localTextView);
  }
  
  public final void l()
  {
    f localf = getActivity();
    if (localf != null)
    {
      localf.finish();
      return;
    }
  }
  
  public final void onAttach(Context paramContext)
  {
    Object localObject = "context";
    k.b(paramContext, (String)localObject);
    super.onAttach(paramContext);
    boolean bool = paramContext instanceof com.truecaller.credit.app.ui.infocollection.views.c.i.a;
    if (bool)
    {
      paramContext = (com.truecaller.credit.app.ui.infocollection.views.c.i.a)paramContext;
      d = paramContext;
      return;
    }
    localObject = new java/lang/RuntimeException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    paramContext = paramContext.toString();
    localStringBuilder.append(paramContext);
    localStringBuilder.append(" must implement DocumentCaptureMvp.CaptureListener");
    paramContext = localStringBuilder.toString();
    ((RuntimeException)localObject).<init>(paramContext);
    throw ((Throwable)localObject);
  }
  
  public final void onClick(View paramView)
  {
    int i = R.id.buttonCapture;
    Object localObject = (ImageView)a(i);
    boolean bool1 = k.a(paramView, localObject);
    if (bool1)
    {
      ((h.a)a()).g();
      return;
    }
    int j = R.id.buttonRetake;
    localObject = (TextView)a(j);
    boolean bool2 = k.a(paramView, localObject);
    if (bool2)
    {
      ((h.a)a()).e();
      return;
    }
    int k = R.id.buttonDone;
    localObject = (TextView)a(k);
    boolean bool3 = k.a(paramView, localObject);
    if (bool3)
    {
      paramView = (h.a)a();
      paramView.h();
    }
  }
  
  public final void onDestroyView()
  {
    super.onDestroyView();
    ((h.a)a()).f();
    f();
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    k.b(paramView, "view");
    super.onViewCreated(paramView, paramBundle);
    ((h.a)a()).e();
    int i = R.id.buttonCapture;
    paramView = (ImageView)a(i);
    paramBundle = this;
    paramBundle = (View.OnClickListener)this;
    paramView.setOnClickListener(paramBundle);
    i = R.id.buttonDone;
    ((TextView)a(i)).setOnClickListener(paramBundle);
    i = R.id.buttonRetake;
    ((TextView)a(i)).setOnClickListener(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.views.b.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */