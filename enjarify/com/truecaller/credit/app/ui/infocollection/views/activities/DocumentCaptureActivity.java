package com.truecaller.credit.app.ui.infocollection.views.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v4.app.j;
import android.support.v4.app.o;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;
import c.g.b.k;
import com.truecaller.credit.R.id;
import com.truecaller.credit.R.layout;
import com.truecaller.credit.app.ui.assist.CreditDocumentType;
import com.truecaller.credit.app.ui.infocollection.a.a.a.a;
import com.truecaller.credit.app.ui.infocollection.views.b.g;
import com.truecaller.credit.app.ui.infocollection.views.b.h;
import com.truecaller.credit.app.ui.infocollection.views.c.i.b;
import com.truecaller.credit.app.ui.infocollection.views.c.i.c;
import com.truecaller.credit.app.ui.infocollection.views.c.i.d;
import com.truecaller.credit.i;
import java.util.HashMap;

public final class DocumentCaptureActivity
  extends com.truecaller.credit.app.ui.b.b
  implements com.truecaller.credit.app.ui.infocollection.views.c.i.a, i.c, i.d
{
  public static final DocumentCaptureActivity.a b;
  private HashMap c;
  
  static
  {
    DocumentCaptureActivity.a locala = new com/truecaller/credit/app/ui/infocollection/views/activities/DocumentCaptureActivity$a;
    locala.<init>((byte)0);
    b = locala;
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = c;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      c = ((HashMap)localObject1);
    }
    localObject1 = c;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = findViewById(paramInt);
      localObject2 = c;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a(Uri paramUri, CreditDocumentType paramCreditDocumentType, String paramString)
  {
    k.b(paramUri, "uri");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>();
    if (paramString != null)
    {
      String str = "scanned_text";
      localIntent.putExtra(str, paramString);
    }
    paramUri = (Parcelable)paramUri;
    localIntent.putExtra("image", paramUri);
    paramCreditDocumentType = (Parcelable)paramCreditDocumentType;
    localIntent.putExtra("document_type", paramCreditDocumentType);
    setResult(-1, localIntent);
    finish();
  }
  
  public final void a(Uri paramUri, String paramString)
  {
    ((i.b)a()).a(paramUri, paramString);
  }
  
  public final void a(Uri paramUri, String paramString, CreditDocumentType paramCreditDocumentType)
  {
    k.b(paramUri, "uri");
    k.b(paramCreditDocumentType, "docType");
    Object localObject = new com/truecaller/credit/app/ui/infocollection/views/b/h;
    ((h)localObject).<init>();
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    if (paramString != null)
    {
      String str = "scanned_text";
      localBundle.putString(str, paramString);
    }
    paramUri = (Parcelable)paramUri;
    localBundle.putParcelable("uri", paramUri);
    paramCreditDocumentType = (Parcelable)paramCreditDocumentType;
    localBundle.putParcelable("type", paramCreditDocumentType);
    ((h)localObject).setArguments(localBundle);
    paramUri = getSupportFragmentManager().a();
    int i = R.id.container;
    localObject = (Fragment)localObject;
    paramUri.b(i, (Fragment)localObject).d();
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "message");
    Object localObject = this;
    localObject = (Context)this;
    paramString = (CharSequence)paramString;
    Toast.makeText((Context)localObject, paramString, 0).show();
  }
  
  public final void a(boolean paramBoolean, String paramString, CreditDocumentType paramCreditDocumentType)
  {
    k.b(paramString, "cameraType");
    k.b(paramCreditDocumentType, "docType");
    Object localObject = new com/truecaller/credit/app/ui/infocollection/views/b/g;
    ((g)localObject).<init>();
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putBoolean("qr_reader", paramBoolean);
    localBundle.putString("camera_type", paramString);
    paramCreditDocumentType = (Parcelable)paramCreditDocumentType;
    localBundle.putParcelable("document_type", paramCreditDocumentType);
    ((g)localObject).setArguments(localBundle);
    o localo = getSupportFragmentManager().a();
    int i = R.id.container;
    localObject = (Fragment)localObject;
    localo.a(i, (Fragment)localObject).d();
  }
  
  public final int b()
  {
    return R.layout.activity_document_capture;
  }
  
  public final void b(int paramInt)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>();
    localIntent.putExtra("state", paramInt);
    setResult(0, localIntent);
    finish();
  }
  
  public final void b(Uri paramUri, String paramString)
  {
    k.b(paramUri, "uri");
    ((i.b)a()).b(paramUri, paramString);
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "text");
    ((i.b)a()).a(paramString);
  }
  
  public final void c()
  {
    a.a locala = com.truecaller.credit.app.ui.infocollection.a.a.a.a();
    Object localObject = i.i;
    localObject = com.truecaller.credit.i.a.a();
    locala.a((com.truecaller.credit.app.c.a.a)localObject).a().a(this);
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "text");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>();
    localIntent.putExtra("scanned_text", paramString);
    setResult(-1, localIntent);
    finish();
  }
  
  public final void d()
  {
    Object localObject = getIntent();
    boolean bool = ((Intent)localObject).getBooleanExtra("qr_reader", false);
    CreditDocumentType localCreditDocumentType = (CreditDocumentType)((Intent)localObject).getParcelableExtra("document_type");
    localObject = ((Intent)localObject).getStringExtra("camera_type");
    i.b localb = (i.b)a();
    k.a(localCreditDocumentType, "docType");
    k.a(localObject, "cameraType");
    localb.a(bool, localCreditDocumentType, (String)localObject);
  }
  
  public final void e()
  {
    Object localObject = this;
    localObject = (AppCompatActivity)this;
    int i = R.id.toolbarKycClickSelfie;
    Toolbar localToolbar = (Toolbar)a(i);
    ((AppCompatActivity)localObject).setSupportActionBar(localToolbar);
    localObject = ((AppCompatActivity)localObject).getSupportActionBar();
    if (localObject != null)
    {
      ((ActionBar)localObject).setElevation(0.0F);
      return;
    }
  }
  
  public final void f()
  {
    Object localObject = this;
    localObject = (Activity)this;
    String[] tmp11_8 = new String[3];
    String[] tmp12_11 = tmp11_8;
    String[] tmp12_11 = tmp11_8;
    tmp12_11[0] = "android.permission.READ_EXTERNAL_STORAGE";
    tmp12_11[1] = "android.permission.WRITE_EXTERNAL_STORAGE";
    tmp12_11[2] = "android.permission.CAMERA";
    String[] arrayOfString = tmp12_11;
    android.support.v4.app.a.a((Activity)localObject, arrayOfString, 11);
  }
  
  public final void g()
  {
    ((i.b)a()).a();
  }
  
  public final void onBackPressed()
  {
    ((i.b)a()).b();
  }
  
  public final void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    k.b(paramArrayOfString, "permissions");
    k.b(paramArrayOfInt, "grantResults");
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    ((i.b)a()).a(paramInt, paramArrayOfString, paramArrayOfInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.views.activities.DocumentCaptureActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */