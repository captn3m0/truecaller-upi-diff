package com.truecaller.credit.app.ui.infocollection.views.b;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import c.g.a.m;
import c.g.b.k;
import c.u;
import com.truecaller.credit.R.layout;
import com.truecaller.credit.app.ui.infocollection.a.a.a.a;
import com.truecaller.credit.app.ui.infocollection.b.e.a;
import com.truecaller.credit.app.ui.infocollection.views.c.d.b;
import com.truecaller.credit.data.models.Address;
import com.truecaller.credit.i;
import com.truecaller.credit.i.a;
import com.truecaller.log.d;
import com.truecaller.utils.extensions.p;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;

public final class c
  extends com.truecaller.credit.app.ui.b.a
  implements DialogInterface.OnShowListener, View.OnClickListener, d.b
{
  public static final c.a c;
  public com.truecaller.credit.app.ui.infocollection.b.e b;
  private c.b d;
  private final c.g.a.b e;
  private HashMap f;
  
  static
  {
    c.a locala = new com/truecaller/credit/app/ui/infocollection/views/b/c$a;
    locala.<init>((byte)0);
    c = locala;
  }
  
  public c()
  {
    Object localObject = new com/truecaller/credit/app/ui/infocollection/views/b/c$c;
    ((c.c)localObject).<init>(this);
    localObject = (c.g.a.b)localObject;
    e = ((c.g.a.b)localObject);
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = f;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      f = ((HashMap)localObject1);
    }
    localObject1 = f;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = f;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a()
  {
    a.a locala = com.truecaller.credit.app.ui.infocollection.a.a.a.a();
    Object localObject = i.i;
    localObject = i.a.a();
    locala.a((com.truecaller.credit.app.c.a.a)localObject).a().a(this);
  }
  
  public final void a(Address paramAddress)
  {
    k.b(paramAddress, "address");
    int i = com.truecaller.credit.R.id.textAddressLine1;
    TextInputEditText localTextInputEditText = (TextInputEditText)a(i);
    CharSequence localCharSequence = (CharSequence)paramAddress.getAddress_line_1();
    localTextInputEditText.setText(localCharSequence);
    i = com.truecaller.credit.R.id.textAddressLine2;
    localTextInputEditText = (TextInputEditText)a(i);
    localCharSequence = (CharSequence)paramAddress.getAddress_line_2();
    localTextInputEditText.setText(localCharSequence);
    i = com.truecaller.credit.R.id.textAddressLine3;
    localTextInputEditText = (TextInputEditText)a(i);
    localCharSequence = (CharSequence)paramAddress.getAddress_line_3();
    localTextInputEditText.setText(localCharSequence);
    i = com.truecaller.credit.R.id.textPinCode;
    localTextInputEditText = (TextInputEditText)a(i);
    localCharSequence = (CharSequence)paramAddress.getPincode();
    localTextInputEditText.setText(localCharSequence);
    i = com.truecaller.credit.R.id.textCity;
    localTextInputEditText = (TextInputEditText)a(i);
    localCharSequence = (CharSequence)paramAddress.getCity();
    localTextInputEditText.setText(localCharSequence);
    i = com.truecaller.credit.R.id.textState;
    localTextInputEditText = (TextInputEditText)a(i);
    paramAddress = (CharSequence)paramAddress.getState();
    localTextInputEditText.setText(paramAddress);
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "continueButtonText");
    int i = com.truecaller.credit.R.id.textCity;
    Object localObject1 = (TextInputEditText)a(i);
    k.a(localObject1, "textCity");
    localObject1 = (TextView)localObject1;
    Object localObject2 = e;
    p.a((TextView)localObject1, (c.g.a.b)localObject2);
    i = com.truecaller.credit.R.id.textPinCode;
    localObject1 = (TextInputEditText)a(i);
    k.a(localObject1, "textPinCode");
    localObject1 = (TextView)localObject1;
    localObject2 = e;
    p.a((TextView)localObject1, (c.g.a.b)localObject2);
    i = com.truecaller.credit.R.id.textAddressLine1;
    localObject1 = (TextInputEditText)a(i);
    k.a(localObject1, "textAddressLine1");
    localObject1 = (TextView)localObject1;
    localObject2 = e;
    p.a((TextView)localObject1, (c.g.a.b)localObject2);
    i = com.truecaller.credit.R.id.textAddressLine2;
    localObject1 = (TextInputEditText)a(i);
    k.a(localObject1, "textAddressLine2");
    localObject1 = (TextView)localObject1;
    localObject2 = e;
    p.a((TextView)localObject1, (c.g.a.b)localObject2);
    i = com.truecaller.credit.R.id.textAddressLine3;
    localObject1 = (TextInputEditText)a(i);
    k.a(localObject1, "textAddressLine3");
    localObject1 = (TextView)localObject1;
    localObject2 = e;
    p.a((TextView)localObject1, (c.g.a.b)localObject2);
    i = com.truecaller.credit.R.id.textState;
    localObject1 = (TextInputEditText)a(i);
    k.a(localObject1, "textState");
    localObject1 = (TextView)localObject1;
    localObject2 = e;
    p.a((TextView)localObject1, (c.g.a.b)localObject2);
    i = com.truecaller.credit.R.id.btnContinue;
    localObject1 = (Button)a(i);
    localObject2 = this;
    localObject2 = (View.OnClickListener)this;
    ((Button)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    paramString = (CharSequence)paramString;
    ((Button)localObject1).setText(paramString);
  }
  
  public final int b()
  {
    return R.layout.fragment_add_address;
  }
  
  public final void b(Address paramAddress)
  {
    k.b(paramAddress, "address");
    c.b localb = d;
    if (localb != null) {
      localb.a(paramAddress);
    }
    getDialog().dismiss();
  }
  
  public final void c()
  {
    HashMap localHashMap = f;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final com.truecaller.credit.app.ui.infocollection.b.e d()
  {
    com.truecaller.credit.app.ui.infocollection.b.e locale = b;
    if (locale == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return locale;
  }
  
  public final String e()
  {
    Bundle localBundle = getArguments();
    if (localBundle != null) {
      return localBundle.getString("address_type");
    }
    return null;
  }
  
  public final Address f()
  {
    Bundle localBundle = getArguments();
    if (localBundle != null) {
      return (Address)localBundle.getParcelable("address");
    }
    return null;
  }
  
  public final void g()
  {
    int i = com.truecaller.credit.R.id.btnContinue;
    Button localButton = (Button)a(i);
    k.a(localButton, "btnContinue");
    t.a((View)localButton, true, 0.5F);
  }
  
  public final void h()
  {
    int i = com.truecaller.credit.R.id.btnContinue;
    Button localButton = (Button)a(i);
    k.a(localButton, "btnContinue");
    t.a((View)localButton, false, 0.5F);
  }
  
  public final void i()
  {
    int i = com.truecaller.credit.R.id.progressOverlay;
    LinearLayout localLinearLayout = (LinearLayout)a(i);
    k.a(localLinearLayout, "progressOverlay");
    t.a((View)localLinearLayout);
  }
  
  public final void j()
  {
    int i = com.truecaller.credit.R.id.progressOverlay;
    LinearLayout localLinearLayout = (LinearLayout)a(i);
    k.a(localLinearLayout, "progressOverlay");
    t.b((View)localLinearLayout);
  }
  
  public final void onClick(View paramView)
  {
    com.truecaller.credit.app.ui.infocollection.b.e locale = b;
    if (locale == null)
    {
      str1 = "presenter";
      k.a(str1);
    }
    String str1 = null;
    if (paramView != null)
    {
      i = paramView.getId();
      paramView = Integer.valueOf(i);
    }
    else
    {
      i = 0;
      paramView = null;
    }
    int j = com.truecaller.credit.R.id.btnContinue;
    if (paramView == null) {
      return;
    }
    int i = paramView.intValue();
    if (i == j)
    {
      paramView = (d.b)b;
      if (paramView != null)
      {
        paramView.i();
        paramView.h();
        paramView = paramView.e();
        if (paramView != null)
        {
          Address localAddress = c;
          if (localAddress == null)
          {
            String str2 = "address";
            k.a(str2);
          }
          localAddress.setAddress_type(paramView);
          paramView = new com/truecaller/credit/app/ui/infocollection/b/e$a;
          paramView.<init>(null, locale);
          paramView = (m)paramView;
          kotlinx.coroutines.e.b(locale, null, paramView, 3);
          return;
        }
        return;
      }
    }
  }
  
  public final Dialog onCreateDialog(Bundle paramBundle)
  {
    paramBundle = new android/support/design/widget/a;
    Object localObject = requireContext();
    int i = getTheme();
    paramBundle.<init>((Context)localObject, i);
    localObject = paramBundle.getWindow();
    if (localObject != null)
    {
      i = 16;
      ((Window)localObject).setSoftInputMode(i);
    }
    localObject = this;
    localObject = (DialogInterface.OnShowListener)this;
    paramBundle.setOnShowListener((DialogInterface.OnShowListener)localObject);
    return (Dialog)paramBundle;
  }
  
  public final void onShow(DialogInterface paramDialogInterface)
  {
    if (paramDialogInterface != null) {}
    try
    {
      paramDialogInterface = (android.support.design.widget.a)paramDialogInterface;
      int i = android.support.design.R.id.design_bottom_sheet;
      paramDialogInterface = paramDialogInterface.findViewById(i);
      paramDialogInterface = (FrameLayout)paramDialogInterface;
      localObject = paramDialogInterface;
      localObject = (View)paramDialogInterface;
      localObject = BottomSheetBehavior.a((View)localObject);
      if (paramDialogInterface != null)
      {
        j = paramDialogInterface.getHeight();
      }
      else
      {
        j = 0;
        paramDialogInterface = null;
      }
      ((BottomSheetBehavior)localObject).a(j);
      int j = 3;
      ((BottomSheetBehavior)localObject).b(j);
      j = 1;
      ((BottomSheetBehavior)localObject).a(j);
      return;
    }
    catch (Exception localException)
    {
      Object localObject;
      for (;;) {}
    }
    paramDialogInterface = new c/u;
    localObject = "null cannot be cast to non-null type android.support.design.widget.BottomSheetDialog";
    paramDialogInterface.<init>((String)localObject);
    throw paramDialogInterface;
    paramDialogInterface = new java/lang/AssertionError;
    paramDialogInterface.<init>("Bottom sheet unavailable");
    d.a((Throwable)paramDialogInterface);
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    String str = "view";
    k.b(paramView, str);
    super.onViewCreated(paramView, paramBundle);
    paramView = b;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramBundle = this;
    paramBundle = (d.b)this;
    paramView.a(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.views.b.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */