package com.truecaller.credit.app.ui.infocollection.views.b;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.view.View;
import android.view.View.OnClickListener;
import c.g.b.k;
import com.truecaller.credit.R.id;
import com.truecaller.credit.R.layout;
import com.truecaller.credit.app.ui.assist.CreditDocumentType;
import com.truecaller.credit.app.ui.b.c;
import com.truecaller.credit.app.ui.customview.CustomInfoCollectionRadioButton;
import com.truecaller.credit.app.ui.infocollection.a.a.a.a;
import com.truecaller.credit.app.ui.infocollection.views.activities.DocumentCaptureActivity.a;
import com.truecaller.credit.app.ui.infocollection.views.c.n;
import com.truecaller.credit.app.ui.infocollection.views.c.p.a;
import com.truecaller.credit.app.ui.infocollection.views.c.p.b;
import com.truecaller.credit.data.models.UserInfoDataRequest;
import com.truecaller.credit.i;
import com.truecaller.credit.i.a;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;

public final class l
  extends c
  implements View.OnClickListener, p.b
{
  private n c;
  private HashMap d;
  
  public final View a(int paramInt)
  {
    Object localObject1 = d;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      d = ((HashMap)localObject1);
    }
    localObject1 = d;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = d;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a(Bitmap paramBitmap, String paramString)
  {
    k.b(paramString, "message");
    int i = R.id.rbAadhaar;
    ((CustomInfoCollectionRadioButton)a(i)).setImageOne(paramBitmap);
    int j = R.id.rbAadhaar;
    ((CustomInfoCollectionRadioButton)a(j)).a(paramString, false);
    j = R.id.rbPanAddress;
    paramBitmap = (CustomInfoCollectionRadioButton)a(j);
    k.a(paramBitmap, "rbPanAddress");
    t.b((View)paramBitmap);
  }
  
  public final void a(CreditDocumentType paramCreditDocumentType, UserInfoDataRequest paramUserInfoDataRequest, String paramString)
  {
    k.b(paramCreditDocumentType, "entryType");
    k.b(paramString, "context");
    n localn = c;
    if (localn == null)
    {
      localObject = "infoUIUpdateListener";
      k.a((String)localObject);
    }
    localn.c(paramString);
    Object localObject = j.c;
    paramCreditDocumentType = j.a.a(paramCreditDocumentType, paramUserInfoDataRequest);
    localn.a(50, paramCreditDocumentType);
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "string");
    int i = R.id.rbAadhaar;
    ((CustomInfoCollectionRadioButton)a(i)).a(paramString, true);
  }
  
  public final void a(boolean paramBoolean, String paramString, CreditDocumentType paramCreditDocumentType)
  {
    k.b(paramString, "cameraType");
    k.b(paramCreditDocumentType, "type");
    Context localContext = getContext();
    if (localContext != null)
    {
      k.a(localContext, "it");
      Intent localIntent = DocumentCaptureActivity.a.a(localContext, paramBoolean, paramCreditDocumentType, paramString);
      startActivityForResult(localIntent, 13);
      return;
    }
  }
  
  public final void b()
  {
    a.a locala = com.truecaller.credit.app.ui.infocollection.a.a.a.a();
    Object localObject = i.i;
    localObject = i.a.a();
    locala.a((com.truecaller.credit.app.c.a.a)localObject).a().a(this);
  }
  
  public final void b(Bitmap paramBitmap, String paramString)
  {
    k.b(paramString, "message");
    int i = R.id.rbAadhaar;
    ((CustomInfoCollectionRadioButton)a(i)).setImageTwo(paramBitmap);
    int j = R.id.rbAadhaar;
    ((CustomInfoCollectionRadioButton)a(j)).a(paramString, false);
    j = R.id.rbPanAddress;
    paramBitmap = (CustomInfoCollectionRadioButton)a(j);
    k.a(paramBitmap, "rbPanAddress");
    t.b((View)paramBitmap);
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "message");
    int i = R.id.rbPanAddress;
    ((CustomInfoCollectionRadioButton)a(i)).a(paramString, true);
  }
  
  public final int c()
  {
    return R.layout.fragment_info_collection_proof_selection;
  }
  
  public final void c(Bitmap paramBitmap, String paramString)
  {
    k.b(paramString, "message");
    int i = R.id.rbAadhaar;
    ((CustomInfoCollectionRadioButton)a(i)).setImageOne(paramBitmap);
    int j = R.id.rbAadhaar;
    ((CustomInfoCollectionRadioButton)a(j)).a(paramString, false);
    j = R.id.rbPanAddress;
    paramBitmap = (CustomInfoCollectionRadioButton)a(j);
    k.a(paramBitmap, "rbPanAddress");
    t.b((View)paramBitmap);
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "text");
    n localn = c;
    if (localn == null)
    {
      String str = "infoUIUpdateListener";
      k.a(str);
    }
    localn.e(paramString);
  }
  
  public final void d(Bitmap paramBitmap, String paramString)
  {
    k.b(paramString, "message");
    int i = R.id.rbPanAddress;
    ((CustomInfoCollectionRadioButton)a(i)).setImageOne(paramBitmap);
    int j = R.id.rbPanAddress;
    ((CustomInfoCollectionRadioButton)a(j)).a(paramString, false);
    j = R.id.rbAadhaar;
    paramBitmap = (CustomInfoCollectionRadioButton)a(j);
    k.a(paramBitmap, "rbAadhaar");
    t.b((View)paramBitmap);
  }
  
  public final void f()
  {
    HashMap localHashMap = d;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final String g()
  {
    Object localObject = getActivity();
    if (localObject != null)
    {
      localObject = ((f)localObject).getIntent();
      if (localObject != null) {
        return ((Intent)localObject).getStringExtra("analytics_context");
      }
    }
    return null;
  }
  
  public final void h()
  {
    n localn = c;
    if (localn == null)
    {
      String str = "infoUIUpdateListener";
      k.a(str);
    }
    localn.a(50, null);
  }
  
  public final void i()
  {
    n localn = c;
    if (localn == null)
    {
      String str = "infoUIUpdateListener";
      k.a(str);
    }
    Object localObject = new com/truecaller/credit/app/ui/infocollection/views/b/b;
    ((b)localObject).<init>();
    localObject = (Fragment)localObject;
    localn.a(25, (Fragment)localObject);
  }
  
  public final void j()
  {
    n localn = c;
    if (localn == null)
    {
      String str = "infoUIUpdateListener";
      k.a(str);
    }
    localn.a(40, null);
  }
  
  public final void k()
  {
    Object localObject = new com/truecaller/credit/app/ui/infocollection/views/b/d;
    ((d)localObject).<init>();
    n localn = c;
    if (localn == null)
    {
      String str = "infoUIUpdateListener";
      k.a(str);
    }
    localObject = (Fragment)localObject;
    localn.a(40, (Fragment)localObject);
  }
  
  public final void l()
  {
    int i = R.id.rbPanAddress;
    CustomInfoCollectionRadioButton localCustomInfoCollectionRadioButton = (CustomInfoCollectionRadioButton)a(i);
    int j = R.id.rbAadhaar;
    boolean bool = ((CustomInfoCollectionRadioButton)a(j)).a() ^ true;
    localCustomInfoCollectionRadioButton.setChecked(bool);
  }
  
  public final void m()
  {
    int i = R.id.rbAadhaar;
    CustomInfoCollectionRadioButton localCustomInfoCollectionRadioButton = (CustomInfoCollectionRadioButton)a(i);
    int j = R.id.rbPanAddress;
    boolean bool = ((CustomInfoCollectionRadioButton)a(j)).a() ^ true;
    localCustomInfoCollectionRadioButton.setChecked(bool);
  }
  
  public final void n()
  {
    n localn = c;
    if (localn == null)
    {
      String str = "infoUIUpdateListener";
      k.a(str);
    }
    localn.n();
  }
  
  public final void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    ((p.a)a()).a(paramInt1, paramInt2, paramIntent);
  }
  
  public final void onAttach(Context paramContext)
  {
    Object localObject = "context";
    k.b(paramContext, (String)localObject);
    super.onAttach(paramContext);
    boolean bool = paramContext instanceof n;
    if (bool)
    {
      paramContext = (n)paramContext;
      c = paramContext;
      return;
    }
    localObject = new java/lang/RuntimeException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    paramContext = paramContext.toString();
    localStringBuilder.append(paramContext);
    localStringBuilder.append(" must implement InfoUIUpdateListener");
    paramContext = localStringBuilder.toString();
    ((RuntimeException)localObject).<init>(paramContext);
    throw ((Throwable)localObject);
  }
  
  public final void onClick(View paramView)
  {
    int i;
    if (paramView != null)
    {
      i = paramView.getId();
      paramView = Integer.valueOf(i);
    }
    else
    {
      i = 0;
      paramView = null;
    }
    int k = R.id.rbAadhaar;
    CustomInfoCollectionRadioButton localCustomInfoCollectionRadioButton = (CustomInfoCollectionRadioButton)a(k);
    String str = "rbAadhaar";
    k.a(localCustomInfoCollectionRadioButton, str);
    k = localCustomInfoCollectionRadioButton.getId();
    if (paramView != null)
    {
      int m = paramView.intValue();
      if (m == k)
      {
        i = R.id.rbAadhaar;
        paramView = (CustomInfoCollectionRadioButton)a(i);
        boolean bool1 = a;
        if (bool1) {
          return;
        }
        ((p.a)a()).b(0);
        return;
      }
    }
    k = R.id.rbPanAddress;
    localCustomInfoCollectionRadioButton = (CustomInfoCollectionRadioButton)a(k);
    str = "rbPanAddress";
    k.a(localCustomInfoCollectionRadioButton, str);
    k = localCustomInfoCollectionRadioButton.getId();
    if (paramView == null) {
      return;
    }
    int j = paramView.intValue();
    if (j == k)
    {
      j = R.id.rbPanAddress;
      paramView = (CustomInfoCollectionRadioButton)a(j);
      boolean bool2 = a;
      if (!bool2)
      {
        paramView = (p.a)a();
        k = 1;
        paramView.b(k);
      }
    }
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    String str = "view";
    k.b(paramView, str);
    super.onViewCreated(paramView, paramBundle);
    paramView = c;
    if (paramView == null)
    {
      paramBundle = "infoUIUpdateListener";
      k.a(paramBundle);
    }
    paramView.q();
    paramView = c;
    if (paramView == null)
    {
      paramBundle = "infoUIUpdateListener";
      k.a(paramBundle);
    }
    paramView.o();
    int i = R.id.rbAadhaar;
    paramView = (CustomInfoCollectionRadioButton)a(i);
    paramBundle = this;
    paramBundle = (View.OnClickListener)this;
    paramView.setOnClickListener(paramBundle);
    i = R.id.rbPanAddress;
    ((CustomInfoCollectionRadioButton)a(i)).setOnClickListener(paramBundle);
    paramView = getArguments();
    if (paramView != null)
    {
      paramBundle = (p.a)a();
      i = paramView.getInt("aadhaar_type", -1);
      paramBundle.a(i);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.views.b.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */