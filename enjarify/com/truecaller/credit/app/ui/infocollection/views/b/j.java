package com.truecaller.credit.app.ui.infocollection.views.b;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import c.g.b.k;
import com.truecaller.credit.R.id;
import com.truecaller.credit.R.layout;
import com.truecaller.credit.R.string;
import com.truecaller.credit.R.style;
import com.truecaller.credit.app.ui.assist.CreditDocumentType;
import com.truecaller.credit.app.ui.b.c;
import com.truecaller.credit.app.ui.infocollection.a.a.a.a;
import com.truecaller.credit.app.ui.infocollection.a.a.b;
import com.truecaller.credit.app.ui.infocollection.views.activities.DocumentCaptureActivity.a;
import com.truecaller.credit.app.ui.infocollection.views.c.m.a;
import com.truecaller.credit.app.ui.infocollection.views.c.m.b;
import com.truecaller.credit.app.ui.infocollection.views.c.n;
import com.truecaller.credit.data.models.UserInfoDataRequest;
import com.truecaller.credit.i;
import com.truecaller.credit.i.a;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;

public final class j
  extends c
  implements DatePickerDialog.OnDateSetListener, TextWatcher, View.OnClickListener, m.b
{
  public static final j.a c;
  private n d;
  private DatePickerDialog e;
  private HashMap f;
  
  static
  {
    j.a locala = new com/truecaller/credit/app/ui/infocollection/views/b/j$a;
    locala.<init>((byte)0);
    c = locala;
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = f;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      f = ((HashMap)localObject1);
    }
    localObject1 = f;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = f;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a(int paramInt1, int paramInt2, int paramInt3, long paramLong)
  {
    android.support.v4.app.f localf = getActivity();
    if (localf != null)
    {
      DatePickerDialog localDatePickerDialog = new android/app/DatePickerDialog;
      Object localObject1 = localf;
      localObject1 = (Context)localf;
      int i = R.style.DatePickerDialog;
      Object localObject2 = this;
      localObject2 = (DatePickerDialog.OnDateSetListener)this;
      localDatePickerDialog.<init>((Context)localObject1, i, (DatePickerDialog.OnDateSetListener)localObject2, paramInt1, paramInt2, paramInt3);
      DatePicker localDatePicker = localDatePickerDialog.getDatePicker();
      k.a(localDatePicker, "datePicker");
      localDatePicker.setMaxDate(paramLong);
      localDatePickerDialog.show();
      e = localDatePickerDialog;
      return;
    }
  }
  
  public final void a(CreditDocumentType paramCreditDocumentType, String paramString)
  {
    k.b(paramCreditDocumentType, "creditDocType");
    k.b(paramString, "cameraType");
    Context localContext = getContext();
    if (localContext != null)
    {
      k.a(localContext, "it");
      paramCreditDocumentType = DocumentCaptureActivity.a.a(localContext, true, paramCreditDocumentType, paramString);
      startActivityForResult(paramCreditDocumentType, 13);
      return;
    }
  }
  
  public final void a(UserInfoDataRequest paramUserInfoDataRequest)
  {
    k.b(paramUserInfoDataRequest, "userData");
    Object localObject = new com/truecaller/credit/app/ui/infocollection/views/b/f;
    ((f)localObject).<init>();
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    String str = "extras_user_detail";
    paramUserInfoDataRequest = (Parcelable)paramUserInfoDataRequest;
    localBundle.putParcelable(str, paramUserInfoDataRequest);
    ((f)localObject).setArguments(localBundle);
    paramUserInfoDataRequest = d;
    if (paramUserInfoDataRequest != null)
    {
      localObject = (Fragment)localObject;
      paramUserInfoDataRequest.a(75, (Fragment)localObject);
      return;
    }
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "text");
    int i = R.id.textCity;
    TextInputEditText localTextInputEditText = (TextInputEditText)a(i);
    paramString = (CharSequence)paramString;
    localTextInputEditText.setText(paramString);
  }
  
  public final void a(String paramString, Drawable paramDrawable, int paramInt)
  {
    k.b(paramString, "name");
    k.b(paramDrawable, "drawable");
    int i = R.id.containerName;
    ((FrameLayout)a(i)).setBackgroundColor(paramInt);
    i = R.id.textName;
    ((TextInputEditText)a(i)).setBackgroundColor(paramInt);
    paramInt = R.id.textName;
    TextInputEditText localTextInputEditText = (TextInputEditText)a(paramInt);
    paramString = (CharSequence)paramString;
    localTextInputEditText.setText(paramString);
    int j = R.id.progressBarName;
    paramString = (ProgressBar)a(j);
    k.a(paramString, "progressBarName");
    paramString.setBackground(paramDrawable);
  }
  
  public final void afterTextChanged(Editable paramEditable)
  {
    if (paramEditable != null)
    {
      paramEditable = paramEditable.toString();
      if (paramEditable != null)
      {
        int i = R.id.textName;
        Object localObject1 = (TextInputEditText)a(i);
        Object localObject2 = "textName";
        k.a(localObject1, (String)localObject2);
        localObject1 = String.valueOf(((TextInputEditText)localObject1).getText());
        boolean bool1 = k.a(paramEditable, localObject1);
        int i4;
        int i5;
        Object localObject3;
        int i6;
        Object localObject4;
        int i7;
        Object localObject5;
        int i8;
        Object localObject6;
        int i9;
        Object localObject7;
        int i10;
        Object localObject8;
        int i11;
        Object localObject9;
        Object localObject10;
        if (bool1)
        {
          localObject1 = (m.a)a();
          i4 = R.id.textName;
          localObject2 = (TextInputEditText)a(i4);
          k.a(localObject2, "textName");
          localObject2 = Integer.valueOf(((TextInputEditText)localObject2).getId());
          i5 = R.id.textCity;
          localObject3 = (TextInputEditText)a(i5);
          k.a(localObject3, "textCity");
          localObject3 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject3);
          i6 = R.id.textAddressLine1;
          localObject4 = (TextInputEditText)a(i6);
          k.a(localObject4, "textAddressLine1");
          localObject4 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject4);
          i7 = R.id.textAddressLine2;
          localObject5 = (TextInputEditText)a(i7);
          k.a(localObject5, "textAddressLine2");
          localObject5 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject5);
          i8 = R.id.textAddressLine3;
          localObject6 = (TextInputEditText)a(i8);
          k.a(localObject6, "textAddressLine3");
          localObject6 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject6);
          i9 = R.id.textPinCode;
          localObject7 = (TextInputEditText)a(i9);
          k.a(localObject7, "textPinCode");
          localObject7 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject7);
          i10 = R.id.textState;
          localObject8 = (TextInputEditText)a(i10);
          k.a(localObject8, "textState");
          localObject8 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject8);
          i11 = R.id.textAadhaarNumber;
          localObject9 = (TextInputEditText)a(i11);
          k.a(localObject9, "textAadhaarNumber");
          localObject9 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject9);
          localObject10 = paramEditable;
          ((m.a)localObject1).a((Integer)localObject2, (String)localObject3, (String)localObject4, (String)localObject5, (String)localObject6, (String)localObject7, (String)localObject8, (String)localObject9, paramEditable);
          return;
        }
        int j = R.id.textCity;
        localObject1 = ((TextInputEditText)a(j)).toString();
        boolean bool2 = k.a(paramEditable, localObject1);
        if (bool2)
        {
          localObject1 = (m.a)a();
          i4 = R.id.textCity;
          localObject2 = (TextInputEditText)a(i4);
          k.a(localObject2, "textCity");
          localObject2 = Integer.valueOf(((TextInputEditText)localObject2).getId());
          i5 = R.id.textAddressLine1;
          localObject3 = (TextInputEditText)a(i5);
          k.a(localObject3, "textAddressLine1");
          localObject4 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject3);
          i5 = R.id.textAddressLine2;
          localObject3 = (TextInputEditText)a(i5);
          k.a(localObject3, "textAddressLine2");
          localObject5 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject3);
          i5 = R.id.textAddressLine3;
          localObject3 = (TextInputEditText)a(i5);
          k.a(localObject3, "textAddressLine3");
          localObject6 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject3);
          i5 = R.id.textPinCode;
          localObject3 = (TextInputEditText)a(i5);
          k.a(localObject3, "textPinCode");
          localObject7 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject3);
          i5 = R.id.textState;
          localObject3 = (TextInputEditText)a(i5);
          k.a(localObject3, "textState");
          localObject8 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject3);
          i5 = R.id.textAadhaarNumber;
          localObject3 = (TextInputEditText)a(i5);
          k.a(localObject3, "textAadhaarNumber");
          localObject9 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject3);
          i5 = R.id.textName;
          localObject3 = (TextInputEditText)a(i5);
          k.a(localObject3, "textName");
          localObject10 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject3);
          localObject3 = paramEditable;
          ((m.a)localObject1).a((Integer)localObject2, paramEditable, (String)localObject4, (String)localObject5, (String)localObject6, (String)localObject7, (String)localObject8, (String)localObject9, (String)localObject10);
          return;
        }
        int k = R.id.textPinCode;
        localObject1 = (TextInputEditText)a(k);
        localObject2 = "textPinCode";
        k.a(localObject1, (String)localObject2);
        localObject1 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject1);
        boolean bool3 = k.a(paramEditable, localObject1);
        if (bool3)
        {
          localObject1 = (m.a)a();
          i4 = R.id.textPinCode;
          localObject2 = (TextInputEditText)a(i4);
          k.a(localObject2, "textPinCode");
          localObject2 = Integer.valueOf(((TextInputEditText)localObject2).getId());
          i5 = R.id.textCity;
          localObject3 = (TextInputEditText)a(i5);
          k.a(localObject3, "textCity");
          localObject3 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject3);
          i6 = R.id.textAddressLine1;
          localObject4 = (TextInputEditText)a(i6);
          k.a(localObject4, "textAddressLine1");
          localObject4 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject4);
          i7 = R.id.textAddressLine2;
          localObject5 = (TextInputEditText)a(i7);
          k.a(localObject5, "textAddressLine2");
          localObject5 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject5);
          i8 = R.id.textAddressLine3;
          localObject6 = (TextInputEditText)a(i8);
          k.a(localObject6, "textAddressLine3");
          localObject6 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject6);
          i9 = R.id.textState;
          localObject7 = (TextInputEditText)a(i9);
          k.a(localObject7, "textState");
          localObject8 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject7);
          i9 = R.id.textAadhaarNumber;
          localObject7 = (TextInputEditText)a(i9);
          k.a(localObject7, "textAadhaarNumber");
          localObject9 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject7);
          i9 = R.id.textName;
          localObject7 = (TextInputEditText)a(i9);
          k.a(localObject7, "textName");
          localObject10 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject7);
          localObject7 = paramEditable;
          ((m.a)localObject1).a((Integer)localObject2, (String)localObject3, (String)localObject4, (String)localObject5, (String)localObject6, paramEditable, (String)localObject8, (String)localObject9, (String)localObject10);
          return;
        }
        int m = R.id.textAddressLine1;
        localObject1 = (TextInputEditText)a(m);
        localObject2 = "textAddressLine1";
        k.a(localObject1, (String)localObject2);
        localObject1 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject1);
        boolean bool4 = k.a(paramEditable, localObject1);
        if (bool4)
        {
          localObject1 = (m.a)a();
          i4 = R.id.textAddressLine1;
          localObject2 = (TextInputEditText)a(i4);
          k.a(localObject2, "textAddressLine1");
          localObject2 = Integer.valueOf(((TextInputEditText)localObject2).getId());
          i5 = R.id.textCity;
          localObject3 = (TextInputEditText)a(i5);
          k.a(localObject3, "textCity");
          localObject3 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject3);
          i6 = R.id.textAddressLine2;
          localObject4 = (TextInputEditText)a(i6);
          k.a(localObject4, "textAddressLine2");
          localObject5 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject4);
          i6 = R.id.textAddressLine3;
          localObject4 = (TextInputEditText)a(i6);
          k.a(localObject4, "textAddressLine3");
          localObject6 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject4);
          i6 = R.id.textPinCode;
          localObject4 = (TextInputEditText)a(i6);
          k.a(localObject4, "textPinCode");
          localObject7 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject4);
          i6 = R.id.textState;
          localObject4 = (TextInputEditText)a(i6);
          k.a(localObject4, "textState");
          localObject8 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject4);
          i6 = R.id.textAadhaarNumber;
          localObject4 = (TextInputEditText)a(i6);
          k.a(localObject4, "textAadhaarNumber");
          localObject9 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject4);
          i6 = R.id.textName;
          localObject4 = (TextInputEditText)a(i6);
          k.a(localObject4, "textName");
          localObject10 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject4);
          localObject4 = paramEditable;
          ((m.a)localObject1).a((Integer)localObject2, (String)localObject3, paramEditable, (String)localObject5, (String)localObject6, (String)localObject7, (String)localObject8, (String)localObject9, (String)localObject10);
          return;
        }
        int n = R.id.textAddressLine2;
        localObject1 = (TextInputEditText)a(n);
        localObject2 = "textAddressLine2";
        k.a(localObject1, (String)localObject2);
        localObject1 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject1);
        boolean bool5 = k.a(paramEditable, localObject1);
        if (bool5)
        {
          localObject1 = (m.a)a();
          i4 = R.id.textAddressLine2;
          localObject2 = (TextInputEditText)a(i4);
          k.a(localObject2, "textAddressLine2");
          localObject2 = Integer.valueOf(((TextInputEditText)localObject2).getId());
          i5 = R.id.textCity;
          localObject3 = (TextInputEditText)a(i5);
          k.a(localObject3, "textCity");
          localObject3 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject3);
          i6 = R.id.textAddressLine1;
          localObject4 = (TextInputEditText)a(i6);
          k.a(localObject4, "textAddressLine1");
          localObject4 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject4);
          i7 = R.id.textAddressLine3;
          localObject5 = (TextInputEditText)a(i7);
          k.a(localObject5, "textAddressLine3");
          localObject6 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject5);
          i7 = R.id.textPinCode;
          localObject5 = (TextInputEditText)a(i7);
          k.a(localObject5, "textPinCode");
          localObject7 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject5);
          i7 = R.id.textState;
          localObject5 = (TextInputEditText)a(i7);
          k.a(localObject5, "textState");
          localObject8 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject5);
          i7 = R.id.textAadhaarNumber;
          localObject5 = (TextInputEditText)a(i7);
          k.a(localObject5, "textAadhaarNumber");
          localObject9 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject5);
          i7 = R.id.textName;
          localObject5 = (TextInputEditText)a(i7);
          k.a(localObject5, "textName");
          localObject10 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject5);
          localObject5 = paramEditable;
          ((m.a)localObject1).a((Integer)localObject2, (String)localObject3, (String)localObject4, paramEditable, (String)localObject6, (String)localObject7, (String)localObject8, (String)localObject9, (String)localObject10);
          return;
        }
        int i1 = R.id.textAddressLine3;
        localObject1 = (TextInputEditText)a(i1);
        localObject2 = "textAddressLine3";
        k.a(localObject1, (String)localObject2);
        localObject1 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject1);
        boolean bool6 = k.a(paramEditable, localObject1);
        if (bool6)
        {
          localObject1 = (m.a)a();
          i4 = R.id.textAddressLine3;
          localObject2 = (TextInputEditText)a(i4);
          k.a(localObject2, "textAddressLine3");
          localObject2 = Integer.valueOf(((TextInputEditText)localObject2).getId());
          i5 = R.id.textCity;
          localObject3 = (TextInputEditText)a(i5);
          k.a(localObject3, "textCity");
          localObject3 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject3);
          i6 = R.id.textAddressLine1;
          localObject4 = (TextInputEditText)a(i6);
          k.a(localObject4, "textAddressLine1");
          localObject4 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject4);
          i7 = R.id.textAddressLine2;
          localObject5 = (TextInputEditText)a(i7);
          k.a(localObject5, "textAddressLine2");
          localObject5 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject5);
          i8 = R.id.textPinCode;
          localObject6 = (TextInputEditText)a(i8);
          k.a(localObject6, "textPinCode");
          localObject7 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject6);
          i8 = R.id.textState;
          localObject6 = (TextInputEditText)a(i8);
          k.a(localObject6, "textState");
          localObject8 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject6);
          i8 = R.id.textAadhaarNumber;
          localObject6 = (TextInputEditText)a(i8);
          k.a(localObject6, "textAadhaarNumber");
          localObject9 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject6);
          i8 = R.id.textName;
          localObject6 = (TextInputEditText)a(i8);
          k.a(localObject6, "textName");
          localObject10 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject6);
          localObject6 = paramEditable;
          ((m.a)localObject1).a((Integer)localObject2, (String)localObject3, (String)localObject4, (String)localObject5, paramEditable, (String)localObject7, (String)localObject8, (String)localObject9, (String)localObject10);
          return;
        }
        int i2 = R.id.textState;
        localObject1 = (TextInputEditText)a(i2);
        localObject2 = "textState";
        k.a(localObject1, (String)localObject2);
        localObject1 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject1);
        boolean bool7 = k.a(paramEditable, localObject1);
        if (bool7)
        {
          localObject1 = (m.a)a();
          i4 = R.id.textState;
          localObject2 = (TextInputEditText)a(i4);
          k.a(localObject2, "textState");
          localObject2 = Integer.valueOf(((TextInputEditText)localObject2).getId());
          i5 = R.id.textCity;
          localObject3 = (TextInputEditText)a(i5);
          k.a(localObject3, "textCity");
          localObject3 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject3);
          i6 = R.id.textAddressLine1;
          localObject4 = (TextInputEditText)a(i6);
          k.a(localObject4, "textAddressLine1");
          localObject4 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject4);
          i7 = R.id.textAddressLine2;
          localObject5 = (TextInputEditText)a(i7);
          k.a(localObject5, "textAddressLine2");
          localObject5 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject5);
          i8 = R.id.textAddressLine3;
          localObject6 = (TextInputEditText)a(i8);
          k.a(localObject6, "textAddressLine3");
          localObject6 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject6);
          i9 = R.id.textPinCode;
          localObject7 = (TextInputEditText)a(i9);
          k.a(localObject7, "textPinCode");
          localObject7 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject7);
          i10 = R.id.textAadhaarNumber;
          localObject8 = (TextInputEditText)a(i10);
          k.a(localObject8, "textAadhaarNumber");
          localObject9 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject8);
          i10 = R.id.textName;
          localObject8 = (TextInputEditText)a(i10);
          k.a(localObject8, "textName");
          localObject10 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject8);
          localObject8 = paramEditable;
          ((m.a)localObject1).a((Integer)localObject2, (String)localObject3, (String)localObject4, (String)localObject5, (String)localObject6, (String)localObject7, paramEditable, (String)localObject9, (String)localObject10);
          return;
        }
        int i3 = R.id.textAadhaarNumber;
        localObject1 = (TextInputEditText)a(i3);
        localObject2 = "textAadhaarNumber";
        k.a(localObject1, (String)localObject2);
        localObject1 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject1);
        boolean bool8 = k.a(paramEditable, localObject1);
        if (bool8)
        {
          localObject1 = (m.a)a();
          i4 = R.id.textAadhaarNumber;
          localObject2 = (TextInputEditText)a(i4);
          k.a(localObject2, "textAadhaarNumber");
          i4 = ((TextInputEditText)localObject2).getId();
          localObject2 = Integer.valueOf(i4);
          i5 = R.id.textCity;
          localObject3 = (TextInputEditText)a(i5);
          k.a(localObject3, "textCity");
          localObject3 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject3);
          i6 = R.id.textAddressLine1;
          localObject4 = (TextInputEditText)a(i6);
          k.a(localObject4, "textAddressLine1");
          localObject4 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject4);
          i7 = R.id.textAddressLine2;
          localObject5 = (TextInputEditText)a(i7);
          k.a(localObject5, "textAddressLine2");
          localObject5 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject5);
          i8 = R.id.textAddressLine3;
          localObject6 = (TextInputEditText)a(i8);
          k.a(localObject6, "textAddressLine3");
          localObject6 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject6);
          i9 = R.id.textPinCode;
          localObject7 = (TextInputEditText)a(i9);
          k.a(localObject7, "textPinCode");
          localObject7 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject7);
          i10 = R.id.textState;
          localObject8 = (TextInputEditText)a(i10);
          k.a(localObject8, "textState");
          localObject8 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject8);
          i11 = R.id.textName;
          localObject9 = (TextInputEditText)a(i11);
          k.a(localObject9, "textName");
          localObject10 = com.truecaller.credit.app.ui.assist.a.a((EditText)localObject9);
          localObject9 = paramEditable;
          ((m.a)localObject1).a((Integer)localObject2, (String)localObject3, (String)localObject4, (String)localObject5, (String)localObject6, (String)localObject7, (String)localObject8, paramEditable, (String)localObject10);
        }
        return;
      }
    }
  }
  
  public final void b()
  {
    a.a locala = com.truecaller.credit.app.ui.infocollection.a.a.a.a();
    Object localObject = i.i;
    localObject = i.a.a();
    locala.a((com.truecaller.credit.app.c.a.a)localObject).a().a(this);
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "text");
    int i = R.id.textPinCode;
    TextInputEditText localTextInputEditText = (TextInputEditText)a(i);
    paramString = (CharSequence)paramString;
    localTextInputEditText.setText(paramString);
  }
  
  public final void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
  
  public final int c()
  {
    return R.layout.fragment_info_manual_entry;
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "text");
    int i = R.id.textAddressLine1;
    TextInputEditText localTextInputEditText = (TextInputEditText)a(i);
    paramString = (CharSequence)paramString;
    localTextInputEditText.setText(paramString);
  }
  
  public final void d(String paramString)
  {
    k.b(paramString, "text");
    int i = R.id.textAddressLine2;
    TextInputEditText localTextInputEditText = (TextInputEditText)a(i);
    paramString = (CharSequence)paramString;
    localTextInputEditText.setText(paramString);
  }
  
  public final String e()
  {
    int i = R.string.credit_title_personal_info;
    String str = getString(i);
    k.a(str, "getString(R.string.credit_title_personal_info)");
    return str;
  }
  
  public final void e(String paramString)
  {
    k.b(paramString, "text");
    int i = R.id.textAddressLine3;
    TextInputEditText localTextInputEditText = (TextInputEditText)a(i);
    paramString = (CharSequence)paramString;
    localTextInputEditText.setText(paramString);
  }
  
  public final void f()
  {
    HashMap localHashMap = f;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final void f(String paramString)
  {
    k.b(paramString, "text");
    int i = R.id.textAadhaarNumber;
    TextInputEditText localTextInputEditText = (TextInputEditText)a(i);
    paramString = (CharSequence)paramString;
    localTextInputEditText.setText(paramString);
  }
  
  public final CreditDocumentType g()
  {
    Bundle localBundle = getArguments();
    if (localBundle != null) {
      return (CreditDocumentType)localBundle.getParcelable("document_type");
    }
    return null;
  }
  
  public final void g(String paramString)
  {
    k.b(paramString, "text");
    int i = R.id.textState;
    TextInputEditText localTextInputEditText = (TextInputEditText)a(i);
    paramString = (CharSequence)paramString;
    localTextInputEditText.setText(paramString);
  }
  
  public final UserInfoDataRequest h()
  {
    Bundle localBundle = getArguments();
    if (localBundle != null) {
      return (UserInfoDataRequest)localBundle.getParcelable("extras_user_detail");
    }
    return null;
  }
  
  public final void h(String paramString)
  {
    k.b(paramString, "message");
    Context localContext = getContext();
    paramString = (CharSequence)paramString;
    Toast.makeText(localContext, paramString, 1).show();
  }
  
  public final void i()
  {
    n localn = d;
    if (localn != null)
    {
      localn.g();
      return;
    }
  }
  
  public final void i(String paramString)
  {
    k.b(paramString, "buttonText");
    n localn = d;
    if (localn != null)
    {
      localn.e(paramString);
      localn.q();
      localn.o();
      return;
    }
  }
  
  public final void j()
  {
    int i = R.id.textCity;
    TextInputEditText localTextInputEditText = (TextInputEditText)a(i);
    Object localObject = this;
    localObject = (TextWatcher)this;
    localTextInputEditText.addTextChangedListener((TextWatcher)localObject);
    i = R.id.textPinCode;
    ((TextInputEditText)a(i)).addTextChangedListener((TextWatcher)localObject);
    i = R.id.textAddressLine1;
    ((TextInputEditText)a(i)).addTextChangedListener((TextWatcher)localObject);
    i = R.id.textAddressLine2;
    ((TextInputEditText)a(i)).addTextChangedListener((TextWatcher)localObject);
    i = R.id.textAddressLine3;
    ((TextInputEditText)a(i)).addTextChangedListener((TextWatcher)localObject);
    i = R.id.textAadhaarNumber;
    ((TextInputEditText)a(i)).addTextChangedListener((TextWatcher)localObject);
    i = R.id.textDob;
    ((TextInputEditText)a(i)).addTextChangedListener((TextWatcher)localObject);
    i = R.id.textName;
    ((TextInputEditText)a(i)).addTextChangedListener((TextWatcher)localObject);
    i = R.id.textState;
    ((TextInputEditText)a(i)).addTextChangedListener((TextWatcher)localObject);
    i = R.id.textDob;
    localTextInputEditText = (TextInputEditText)a(i);
    localObject = this;
    localObject = (View.OnClickListener)this;
    localTextInputEditText.setOnClickListener((View.OnClickListener)localObject);
    i = R.id.btnScanQR;
    ((TextView)a(i)).setOnClickListener((View.OnClickListener)localObject);
  }
  
  public final void j(String paramString)
  {
    k.b(paramString, "hintText");
    int i = R.id.containerAadhaar;
    TextInputLayout localTextInputLayout = (TextInputLayout)a(i);
    paramString = (CharSequence)paramString;
    localTextInputLayout.setHint(paramString);
    t.a((View)localTextInputLayout, false, 2);
  }
  
  public final void k()
  {
    int i = R.id.textAadhaarNumber;
    TextInputEditText localTextInputEditText = (TextInputEditText)a(i);
    k.a(localTextInputEditText, "textAadhaarNumber");
    localTextInputEditText.setInputType(2);
  }
  
  public final void k(String paramString)
  {
    k.b(paramString, "date");
    int i = R.id.textDob;
    TextInputEditText localTextInputEditText = (TextInputEditText)a(i);
    Object localObject = paramString;
    localObject = (CharSequence)paramString;
    localTextInputEditText.setText((CharSequence)localObject);
    int j = paramString.length();
    localTextInputEditText.setSelection(j);
  }
  
  public final void l()
  {
    int i = R.id.btnScanQR;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "btnScanQR");
    t.b((View)localTextView);
  }
  
  public final void l(String paramString)
  {
    k.b(paramString, "errorMessage");
    int i = R.id.containerAadhaar;
    TextInputLayout localTextInputLayout = (TextInputLayout)a(i);
    k.a(localTextInputLayout, "containerAadhaar");
    paramString = (CharSequence)paramString;
    localTextInputLayout.setError(paramString);
    int j = R.id.containerAadhaar;
    paramString = (TextInputLayout)a(j);
    k.a(paramString, "containerAadhaar");
    paramString.setErrorEnabled(true);
  }
  
  public final void m()
  {
    int i = R.id.textName;
    Object localObject = (TextInputEditText)a(i);
    ((TextInputEditText)localObject).setEnabled(true);
    ((TextInputEditText)localObject).setInputType(96);
    i = R.id.progressBarName;
    localObject = (ProgressBar)a(i);
    k.a(localObject, "progressBarName");
    t.b((View)localObject);
  }
  
  public final void n()
  {
    n localn = d;
    if (localn != null)
    {
      localn.n();
      return;
    }
  }
  
  public final void o()
  {
    n localn = d;
    if (localn != null)
    {
      localn.o();
      return;
    }
  }
  
  public final void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    m.a locala = (m.a)a();
    if (paramIntent != null)
    {
      String str = "scanned_text";
      paramIntent = paramIntent.getStringExtra(str);
    }
    else
    {
      paramIntent = null;
    }
    locala.a(paramInt1, paramInt2, paramIntent);
  }
  
  public final void onAttach(Context paramContext)
  {
    Object localObject = "context";
    k.b(paramContext, (String)localObject);
    super.onAttach(paramContext);
    boolean bool = paramContext instanceof n;
    if (bool)
    {
      paramContext = (n)paramContext;
      d = paramContext;
      return;
    }
    localObject = new java/lang/RuntimeException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(paramContext);
    localStringBuilder.append(" must implement InfoUIUpdateListener");
    paramContext = localStringBuilder.toString();
    ((RuntimeException)localObject).<init>(paramContext);
    throw ((Throwable)localObject);
  }
  
  public final void onClick(View paramView)
  {
    if (paramView != null)
    {
      i = paramView.getId();
      paramView = Integer.valueOf(i);
    }
    else
    {
      i = 0;
      paramView = null;
    }
    int j = R.id.textDob;
    if (paramView != null)
    {
      int k = paramView.intValue();
      if (k == j)
      {
        ((m.a)a()).f();
        return;
      }
    }
    j = R.id.btnScanQR;
    if (paramView == null) {
      return;
    }
    int i = paramView.intValue();
    if (i == j)
    {
      paramView = (m.a)a();
      paramView.a();
    }
  }
  
  public final void onDateSet(DatePicker paramDatePicker, int paramInt1, int paramInt2, int paramInt3)
  {
    paramDatePicker = e;
    if (paramDatePicker != null) {
      paramDatePicker.dismiss();
    }
    ((m.a)a()).a(paramInt1, paramInt2, paramInt3);
  }
  
  public final void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
  
  public final void p()
  {
    int i = R.id.containerAadhaar;
    TextInputLayout localTextInputLayout = (TextInputLayout)a(i);
    k.a(localTextInputLayout, "containerAadhaar");
    localTextInputLayout.setErrorEnabled(false);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.views.b.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */