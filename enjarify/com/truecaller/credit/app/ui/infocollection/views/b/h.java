package com.truecaller.credit.app.ui.infocollection.views.b;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.common.ui.PausingLottieAnimationView;
import com.truecaller.credit.R.id;
import com.truecaller.credit.R.layout;
import com.truecaller.credit.app.ui.assist.CreditDocumentType;
import com.truecaller.credit.app.ui.b.c;
import com.truecaller.credit.app.ui.infocollection.a.a.a.a;
import com.truecaller.credit.app.ui.infocollection.a.a.b;
import com.truecaller.credit.app.ui.infocollection.views.c.i.c;
import com.truecaller.credit.app.ui.infocollection.views.c.j.b;
import com.truecaller.credit.i;
import com.truecaller.credit.i.a;
import java.util.HashMap;

public final class h
  extends c
  implements j.b
{
  private i.c c;
  private HashMap d;
  
  public final View a(int paramInt)
  {
    Object localObject1 = d;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      d = ((HashMap)localObject1);
    }
    localObject1 = d;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = d;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a(Uri paramUri, String paramString)
  {
    k.b(paramUri, "uri");
    i.c localc = c;
    if (localc == null)
    {
      String str = "uploaderListener";
      k.a(str);
    }
    localc.b(paramUri, paramString);
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "text");
    int i = R.id.textUpload;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "textUpload");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void b()
  {
    a.a locala = com.truecaller.credit.app.ui.infocollection.a.a.a.a();
    Object localObject = i.i;
    localObject = i.a.a();
    locala.a((com.truecaller.credit.app.c.a.a)localObject).a().a(this);
  }
  
  public final int c()
  {
    return R.layout.fragment_document_uploader;
  }
  
  public final void f()
  {
    HashMap localHashMap = d;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final Uri g()
  {
    Bundle localBundle = getArguments();
    if (localBundle != null) {
      return (Uri)localBundle.getParcelable("uri");
    }
    return null;
  }
  
  public final CreditDocumentType h()
  {
    Bundle localBundle = getArguments();
    if (localBundle != null) {
      return (CreditDocumentType)localBundle.getParcelable("type");
    }
    return null;
  }
  
  public final String i()
  {
    Bundle localBundle = getArguments();
    if (localBundle != null) {
      return localBundle.getString("scanned_text");
    }
    return null;
  }
  
  public final void j()
  {
    int i = R.id.lottieUpload;
    PausingLottieAnimationView localPausingLottieAnimationView = (PausingLottieAnimationView)a(i);
    localPausingLottieAnimationView.setAnimation("animations/lottie_uploading_image.json");
    localPausingLottieAnimationView.a(true);
    localPausingLottieAnimationView.a();
  }
  
  public final void k()
  {
    i.c localc = c;
    if (localc == null)
    {
      String str = "uploaderListener";
      k.a(str);
    }
    localc.g();
  }
  
  public final void onAttach(Context paramContext)
  {
    Object localObject = "context";
    k.b(paramContext, (String)localObject);
    super.onAttach(paramContext);
    boolean bool = paramContext instanceof i.c;
    if (bool)
    {
      paramContext = (i.c)paramContext;
      c = paramContext;
      return;
    }
    localObject = new java/lang/RuntimeException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    paramContext = paramContext.toString();
    localStringBuilder.append(paramContext);
    localStringBuilder.append(" must implement  DocumentCaptureMvp.UploaderListener");
    paramContext = localStringBuilder.toString();
    ((RuntimeException)localObject).<init>(paramContext);
    throw ((Throwable)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.views.b.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */