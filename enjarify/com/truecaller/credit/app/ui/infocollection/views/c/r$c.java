package com.truecaller.credit.app.ui.infocollection.views.c;

import com.truecaller.credit.data.models.APIStatusMessage;
import com.truecaller.credit.data.models.Address;
import java.util.List;

public abstract interface r$c
{
  public abstract void a(APIStatusMessage paramAPIStatusMessage);
  
  public abstract void a(String paramString);
  
  public abstract void a(String paramString, Address paramAddress);
  
  public abstract void a(String paramString1, String paramString2, String paramString3);
  
  public abstract void a(List paramList);
  
  public abstract void b(String paramString);
  
  public abstract void c(String paramString);
  
  public abstract String g();
  
  public abstract String h();
  
  public abstract void i();
  
  public abstract void j();
  
  public abstract void k();
  
  public abstract void l();
  
  public abstract void m();
  
  public abstract void n();
  
  public abstract void o();
  
  public abstract void p();
  
  public abstract void q();
  
  public abstract void r();
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.views.c.r.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */