package com.truecaller.credit.app.ui.infocollection.views.a;

import android.content.Context;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import com.truecaller.credit.R.layout;
import com.truecaller.credit.domain.interactors.infocollection.models.PoaData;
import java.util.List;

public final class a
  extends RecyclerView.Adapter
{
  private final LayoutInflater a;
  private List b;
  private final h c;
  private final a.a d;
  
  public a(Context paramContext, List paramList, h paramh, a.a parama)
  {
    b = paramList;
    c = paramh;
    d = parama;
    paramContext = LayoutInflater.from(paramContext);
    c.g.b.k.a(paramContext, "LayoutInflater.from(context)");
    a = paramContext;
  }
  
  public final int getItemCount()
  {
    List localList = b;
    if (localList != null) {
      return localList.size();
    }
    return 0;
  }
  
  public final long getItemId(int paramInt)
  {
    return paramInt;
  }
  
  public final void onBindViewHolder(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    c.g.b.k.b(paramViewHolder, "holder");
    Object localObject1 = b;
    if (localObject1 != null)
    {
      localObject1 = (PoaData)((List)localObject1).get(paramInt);
      if (localObject1 != null)
      {
        Object localObject2 = c;
        Object localObject3 = paramViewHolder;
        localObject3 = (k)paramViewHolder;
        ((h)localObject2).a((k)localObject3, (PoaData)localObject1);
        paramViewHolder = itemView;
        localObject2 = new com/truecaller/credit/app/ui/infocollection/views/a/a$b;
        ((a.b)localObject2).<init>(this, paramInt, (PoaData)localObject1);
        localObject2 = (View.OnClickListener)localObject2;
        paramViewHolder.setOnClickListener((View.OnClickListener)localObject2);
        return;
      }
    }
  }
  
  public final RecyclerView.ViewHolder onCreateViewHolder(ViewGroup paramViewGroup, int paramInt)
  {
    c.g.b.k.b(paramViewGroup, "parent");
    l locall = new com/truecaller/credit/app/ui/infocollection/views/a/l;
    LayoutInflater localLayoutInflater = a;
    int i = R.layout.item_poa_type;
    paramViewGroup = localLayoutInflater.inflate(i, paramViewGroup, false);
    c.g.b.k.a(paramViewGroup, "inflater.inflate(R.layou…_poa_type, parent, false)");
    locall.<init>(paramViewGroup);
    return (RecyclerView.ViewHolder)locall;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.views.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */