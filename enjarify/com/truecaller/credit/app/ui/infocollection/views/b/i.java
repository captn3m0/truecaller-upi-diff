package com.truecaller.credit.app.ui.infocollection.views.b;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.credit.R.drawable;
import com.truecaller.credit.R.id;
import com.truecaller.credit.R.layout;
import com.truecaller.credit.app.ui.infocollection.a.a.a.a;
import com.truecaller.credit.app.ui.infocollection.views.a.b.a;
import com.truecaller.credit.app.ui.infocollection.views.c.k.a;
import com.truecaller.credit.app.ui.infocollection.views.c.k.b;
import com.truecaller.credit.app.ui.infocollection.views.c.n;
import com.truecaller.credit.domain.interactors.infocollection.models.IFSCDetails;
import com.truecaller.utils.extensions.p;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;
import java.util.List;

public final class i
  extends com.truecaller.credit.app.ui.b.c
  implements b.a, k.b
{
  public static final i.a e;
  public com.truecaller.credit.app.ui.infocollection.views.a.c c;
  public com.truecaller.credit.app.ui.infocollection.views.a.b d;
  private n f;
  private final c.g.a.b g;
  private HashMap h;
  
  static
  {
    i.a locala = new com/truecaller/credit/app/ui/infocollection/views/b/i$a;
    locala.<init>((byte)0);
    e = locala;
  }
  
  public i()
  {
    Object localObject = new com/truecaller/credit/app/ui/infocollection/views/b/i$b;
    ((i.b)localObject).<init>(this);
    localObject = (c.g.a.b)localObject;
    g = ((c.g.a.b)localObject);
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = h;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      h = ((HashMap)localObject1);
    }
    localObject1 = h;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = h;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a(IFSCDetails paramIFSCDetails)
  {
    k.b(paramIFSCDetails, "ifscDetails");
    com.truecaller.credit.app.ui.infocollection.views.a.b localb = d;
    if (localb == null)
    {
      String str = "ifscAdapter";
      k.a(str);
    }
    localb.notifyDataSetChanged();
    ((k.a)a()).a(paramIFSCDetails);
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "continueButtonText");
    n localn = f;
    if (localn != null)
    {
      localn.e(paramString);
      return;
    }
  }
  
  public final void a(List paramList)
  {
    k.b(paramList, "ifscSearchList");
    com.truecaller.credit.app.ui.infocollection.views.a.b localb = d;
    if (localb == null)
    {
      String str = "ifscAdapter";
      k.a(str);
    }
    k.b(paramList, "ifscSearchList");
    a = paramList;
    localb.notifyDataSetChanged();
  }
  
  public final void b()
  {
    a.a locala = com.truecaller.credit.app.ui.infocollection.a.a.a.a();
    Object localObject = com.truecaller.credit.i.i;
    localObject = com.truecaller.credit.i.a.a();
    locala.a((com.truecaller.credit.app.c.a.a)localObject).a().a(this);
  }
  
  public final void b(IFSCDetails paramIFSCDetails)
  {
    k.b(paramIFSCDetails, "selectedIFSC");
    Fragment localFragment = getTargetFragment();
    if (localFragment != null)
    {
      int i = getTargetRequestCode();
      int j = -1;
      Intent localIntent = new android/content/Intent;
      localIntent.<init>();
      String str = "extra_ifsc_value";
      paramIFSCDetails = (Parcelable)paramIFSCDetails;
      paramIFSCDetails = localIntent.putExtra(str, paramIFSCDetails);
      localFragment.onActivityResult(i, j, paramIFSCDetails);
    }
    requireActivity().onBackPressed();
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "bankName");
    int i = R.id.etBankName;
    EditText localEditText = (EditText)a(i);
    paramString = (CharSequence)paramString;
    localEditText.setText(paramString);
  }
  
  public final int c()
  {
    return R.layout.fragment_ifsc_search;
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "errorMessage");
    int i = R.id.tvError;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "tvError");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final boolean d()
  {
    return true;
  }
  
  public final void f()
  {
    HashMap localHashMap = h;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final void g()
  {
    n localn = f;
    if (localn != null)
    {
      localn.g();
      return;
    }
  }
  
  public final void h()
  {
    Object localObject1 = new com/truecaller/credit/app/ui/infocollection/views/a/b;
    Object localObject2 = requireActivity();
    k.a(localObject2, "requireActivity()");
    localObject2 = (Context)localObject2;
    Object localObject3 = c;
    if (localObject3 == null)
    {
      localObject4 = "ifscItemPresenter";
      k.a((String)localObject4);
    }
    Object localObject4 = this;
    localObject4 = (b.a)this;
    ((com.truecaller.credit.app.ui.infocollection.views.a.b)localObject1).<init>((Context)localObject2, (com.truecaller.credit.app.ui.infocollection.views.a.c)localObject3, (b.a)localObject4);
    d = ((com.truecaller.credit.app.ui.infocollection.views.a.b)localObject1);
    int i = R.id.rvAvailableIFSC;
    localObject1 = (RecyclerView)a(i);
    k.a(localObject1, "rvAvailableIFSC");
    localObject2 = d;
    if (localObject2 == null)
    {
      localObject3 = "ifscAdapter";
      k.a((String)localObject3);
    }
    localObject2 = (RecyclerView.Adapter)localObject2;
    ((RecyclerView)localObject1).setAdapter((RecyclerView.Adapter)localObject2);
    i = R.id.rvAvailableIFSC;
    localObject1 = (RecyclerView)a(i);
    k.a(localObject1, "rvAvailableIFSC");
    localObject2 = new android/support/v7/widget/LinearLayoutManager;
    localObject3 = (Context)requireActivity();
    ((LinearLayoutManager)localObject2).<init>((Context)localObject3);
    localObject2 = (RecyclerView.LayoutManager)localObject2;
    ((RecyclerView)localObject1).setLayoutManager((RecyclerView.LayoutManager)localObject2);
    i = R.id.rvAvailableIFSC;
    localObject1 = (RecyclerView)a(i);
    k.a(localObject1, "rvAvailableIFSC");
    int j = 0;
    ((RecyclerView)localObject1).setNestedScrollingEnabled(false);
    localObject1 = new com/truecaller/credit/app/ui/customview/b;
    localObject2 = (Context)requireActivity();
    int k = R.drawable.divider_grey;
    localObject2 = android.support.v4.content.b.a((Context)localObject2, k);
    if (localObject2 == null) {
      k.a();
    }
    k.a(localObject2, "ContextCompat.getDrawabl….drawable.divider_grey)!!");
    ((com.truecaller.credit.app.ui.customview.b)localObject1).<init>((Drawable)localObject2);
    j = R.id.rvAvailableIFSC;
    localObject2 = (RecyclerView)a(j);
    localObject1 = (RecyclerView.ItemDecoration)localObject1;
    ((RecyclerView)localObject2).addItemDecoration((RecyclerView.ItemDecoration)localObject1);
    i = R.id.etBranchName;
    localObject1 = (EditText)a(i);
    k.a(localObject1, "etBranchName");
    localObject1 = (TextView)localObject1;
    localObject2 = g;
    p.a((TextView)localObject1, (c.g.a.b)localObject2);
    i = R.id.etCityDistrict;
    localObject1 = (EditText)a(i);
    k.a(localObject1, "etCityDistrict");
    localObject1 = (TextView)localObject1;
    localObject2 = g;
    p.a((TextView)localObject1, (c.g.a.b)localObject2);
  }
  
  public final void i()
  {
    Object localObject1 = getArguments();
    String str = null;
    Object localObject2;
    if (localObject1 != null)
    {
      localObject2 = "bank_name";
      localObject1 = ((Bundle)localObject1).getString((String)localObject2);
    }
    else
    {
      localObject1 = null;
    }
    if (localObject1 != null)
    {
      localObject1 = (k.a)a();
      localObject2 = getArguments();
      if (localObject2 != null) {
        str = ((Bundle)localObject2).getString("bank_name");
      }
      if (str == null) {
        k.a();
      }
      ((k.a)localObject1).a(str);
      return;
    }
  }
  
  public final void j()
  {
    n localn = f;
    if (localn != null)
    {
      localn.n();
      return;
    }
  }
  
  public final void k()
  {
    n localn = f;
    if (localn != null)
    {
      localn.o();
      return;
    }
  }
  
  public final void l()
  {
    n localn = f;
    if (localn != null)
    {
      localn.q();
      return;
    }
  }
  
  public final void m()
  {
    int i = R.id.tvError;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "tvError");
    t.b((View)localTextView);
  }
  
  public final void n()
  {
    int i = R.id.tvError;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "tvError");
    t.a((View)localTextView);
  }
  
  public final void o()
  {
    int i = R.id.rvAvailableIFSC;
    RecyclerView localRecyclerView = (RecyclerView)a(i);
    k.a(localRecyclerView, "rvAvailableIFSC");
    t.a((View)localRecyclerView);
  }
  
  public final void onAttach(Context paramContext)
  {
    Object localObject = "context";
    k.b(paramContext, (String)localObject);
    super.onAttach(paramContext);
    boolean bool = paramContext instanceof n;
    if (bool)
    {
      paramContext = (n)paramContext;
      f = paramContext;
      return;
    }
    localObject = new java/lang/RuntimeException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    paramContext = paramContext.toString();
    localStringBuilder.append(paramContext);
    localStringBuilder.append(" must implement InfoUIUpdateListener");
    paramContext = localStringBuilder.toString();
    ((RuntimeException)localObject).<init>(paramContext);
    throw ((Throwable)localObject);
  }
  
  public final void p()
  {
    int i = R.id.rvAvailableIFSC;
    RecyclerView localRecyclerView = (RecyclerView)a(i);
    k.a(localRecyclerView, "rvAvailableIFSC");
    t.b((View)localRecyclerView);
  }
  
  public final void q()
  {
    int i = R.id.tvSearchResults;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "tvSearchResults");
    t.b((View)localTextView);
  }
  
  public final void r()
  {
    int i = R.id.tvSearchResults;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "tvSearchResults");
    t.a((View)localTextView);
  }
  
  public final void s()
  {
    int i = R.id.pbLoading;
    ProgressBar localProgressBar = (ProgressBar)a(i);
    k.a(localProgressBar, "pbLoading");
    t.c((View)localProgressBar);
  }
  
  public final void t()
  {
    int i = R.id.pbLoading;
    ProgressBar localProgressBar = (ProgressBar)a(i);
    k.a(localProgressBar, "pbLoading");
    t.a((View)localProgressBar);
  }
  
  public final void u()
  {
    int i = R.id.etBranchName;
    EditText localEditText = (EditText)a(i);
    k.a(localEditText, "etBranchName");
    t.a((View)localEditText, false, 5);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.views.b.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */