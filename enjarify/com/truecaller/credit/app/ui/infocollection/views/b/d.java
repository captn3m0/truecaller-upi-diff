package com.truecaller.credit.app.ui.infocollection.views.b;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.view.View;
import c.g.b.k;
import com.truecaller.credit.R.id;
import com.truecaller.credit.R.layout;
import com.truecaller.credit.app.ui.assist.CreditDocumentType;
import com.truecaller.credit.app.ui.b.c;
import com.truecaller.credit.app.ui.infocollection.a.a.b;
import com.truecaller.credit.app.ui.infocollection.views.a.h;
import com.truecaller.credit.app.ui.infocollection.views.activities.DocumentCaptureActivity.a;
import com.truecaller.credit.app.ui.infocollection.views.c.e.a;
import com.truecaller.credit.app.ui.infocollection.views.c.e.b;
import com.truecaller.credit.app.ui.infocollection.views.c.n;
import com.truecaller.credit.domain.interactors.infocollection.models.PoaData;
import com.truecaller.credit.i;
import com.truecaller.credit.i.a;
import java.util.HashMap;
import java.util.List;

public final class d
  extends c
  implements com.truecaller.credit.app.ui.infocollection.views.a.a.a, e.b
{
  public h c;
  private n d;
  private com.truecaller.credit.app.ui.infocollection.views.a.a e;
  private HashMap f;
  
  public final View a(int paramInt)
  {
    Object localObject1 = f;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      f = ((HashMap)localObject1);
    }
    localObject1 = f;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = f;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a(CreditDocumentType paramCreditDocumentType)
  {
    k.b(paramCreditDocumentType, "entryType");
    n localn = d;
    if (localn != null)
    {
      paramCreditDocumentType = j.a.a(paramCreditDocumentType, null);
      localn.a(50, paramCreditDocumentType);
      return;
    }
  }
  
  public final void a(PoaData paramPoaData)
  {
    k.b(paramPoaData, "poaData");
    e.a locala = (e.a)a();
    locala.a(paramPoaData);
    paramPoaData = e;
    if (paramPoaData != null)
    {
      paramPoaData.notifyDataSetChanged();
      return;
    }
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "buttonText");
    n localn = d;
    if (localn != null)
    {
      localn.e(paramString);
      localn.q();
      localn.o();
      return;
    }
  }
  
  public final void a(String paramString, CreditDocumentType paramCreditDocumentType)
  {
    k.b(paramString, "cameraType");
    k.b(paramCreditDocumentType, "poaType");
    Context localContext = getContext();
    if (localContext != null)
    {
      k.a(localContext, "it");
      paramString = DocumentCaptureActivity.a.a(localContext, false, paramCreditDocumentType, paramString);
      startActivityForResult(paramString, 13);
      return;
    }
  }
  
  public final void a(List paramList)
  {
    k.b(paramList, "types");
    Object localObject1 = getContext();
    if (localObject1 != null)
    {
      com.truecaller.credit.app.ui.infocollection.views.a.a locala = new com/truecaller/credit/app/ui/infocollection/views/a/a;
      k.a(localObject1, "it");
      h localh = c;
      if (localh == null)
      {
        localObject2 = "poaItemPresenter";
        k.a((String)localObject2);
      }
      Object localObject2 = this;
      localObject2 = (com.truecaller.credit.app.ui.infocollection.views.a.a.a)this;
      locala.<init>((Context)localObject1, paramList, localh, (com.truecaller.credit.app.ui.infocollection.views.a.a.a)localObject2);
      e = locala;
      paramList = new android/support/v7/widget/LinearLayoutManager;
      localObject1 = getContext();
      paramList.<init>((Context)localObject1);
      int i = R.id.listPoaTypes;
      localObject1 = (RecyclerView)a(i);
      paramList = (RecyclerView.LayoutManager)paramList;
      ((RecyclerView)localObject1).setLayoutManager(paramList);
      ((RecyclerView)localObject1).setHasFixedSize(true);
      paramList = (RecyclerView.Adapter)e;
      ((RecyclerView)localObject1).setAdapter(paramList);
      return;
    }
  }
  
  public final void b()
  {
    com.truecaller.credit.app.ui.infocollection.a.a.a.a locala = com.truecaller.credit.app.ui.infocollection.a.a.a.a();
    Object localObject = i.i;
    localObject = i.a.a();
    locala.a((com.truecaller.credit.app.c.a.a)localObject).a().a(this);
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "text");
    n localn = d;
    if (localn != null)
    {
      localn.e(paramString);
      return;
    }
  }
  
  public final int c()
  {
    return R.layout.fragment_poa_type_selection;
  }
  
  public final void f()
  {
    HashMap localHashMap = f;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final void g()
  {
    n localn = d;
    if (localn != null)
    {
      localn.a(40, null);
      return;
    }
  }
  
  public final void h()
  {
    n localn = d;
    if (localn != null)
    {
      localn.n();
      return;
    }
  }
  
  public final void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    e.a locala = (e.a)a();
    if (paramIntent != null)
    {
      String str = "image";
      paramIntent = (Uri)paramIntent.getParcelableExtra(str);
    }
    else
    {
      paramIntent = null;
    }
    locala.a(paramInt1, paramInt2, paramIntent);
  }
  
  public final void onAttach(Context paramContext)
  {
    Object localObject = "context";
    k.b(paramContext, (String)localObject);
    super.onAttach(paramContext);
    boolean bool = paramContext instanceof n;
    if (bool)
    {
      paramContext = (n)paramContext;
      d = paramContext;
      return;
    }
    localObject = new java/lang/RuntimeException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(paramContext);
    localStringBuilder.append(" must implement InfoUIUpdateListener");
    paramContext = localStringBuilder.toString();
    ((RuntimeException)localObject).<init>(paramContext);
    throw ((Throwable)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.views.b.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */