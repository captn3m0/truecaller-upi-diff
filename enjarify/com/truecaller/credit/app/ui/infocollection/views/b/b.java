package com.truecaller.credit.app.ui.infocollection.views.b;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.View.OnClickListener;
import c.g.b.k;
import com.truecaller.credit.R.id;
import com.truecaller.credit.R.layout;
import com.truecaller.credit.app.ui.b.c;
import com.truecaller.credit.app.ui.infocollection.a.a.a.a;
import com.truecaller.credit.app.ui.infocollection.views.c.c.a;
import com.truecaller.credit.app.ui.infocollection.views.c.c.b;
import com.truecaller.credit.app.ui.infocollection.views.c.n;
import com.truecaller.credit.i;
import com.truecaller.credit.i.a;
import java.util.HashMap;

public final class b
  extends c
  implements View.OnClickListener, c.b
{
  private n c;
  private HashMap d;
  
  public final View a(int paramInt)
  {
    Object localObject1 = d;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      d = ((HashMap)localObject1);
    }
    localObject1 = d;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = d;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void b()
  {
    a.a locala = com.truecaller.credit.app.ui.infocollection.a.a.a.a();
    Object localObject = i.i;
    localObject = i.a.a();
    locala.a((com.truecaller.credit.app.c.a.a)localObject).a().a(this);
  }
  
  public final void b(int paramInt)
  {
    int i = R.id.typeWalletCard;
    ((CardView)a(i)).setCardBackgroundColor(paramInt);
  }
  
  public final int c()
  {
    return R.layout.fragment_aadhaar_type_selection;
  }
  
  public final void c(int paramInt)
  {
    int i = R.id.typeLongCard;
    ((CardView)a(i)).setCardBackgroundColor(paramInt);
  }
  
  public final void d(int paramInt)
  {
    Object localObject = new com/truecaller/credit/app/ui/infocollection/views/b/l;
    ((l)localObject).<init>();
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    String str = "aadhaar_type";
    localBundle.putInt(str, paramInt);
    ((l)localObject).setArguments(localBundle);
    n localn = c;
    if (localn != null)
    {
      localObject = (Fragment)localObject;
      localn.a(25, (Fragment)localObject);
      return;
    }
  }
  
  public final void f()
  {
    HashMap localHashMap = d;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final void onAttach(Context paramContext)
  {
    Object localObject = "context";
    k.b(paramContext, (String)localObject);
    super.onAttach(paramContext);
    boolean bool = paramContext instanceof n;
    if (bool)
    {
      paramContext = (n)paramContext;
      c = paramContext;
      return;
    }
    localObject = new java/lang/RuntimeException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    paramContext = paramContext.toString();
    localStringBuilder.append(paramContext);
    localStringBuilder.append(" must implement InfoUIUpdateListener");
    paramContext = localStringBuilder.toString();
    ((RuntimeException)localObject).<init>(paramContext);
    throw ((Throwable)localObject);
  }
  
  public final void onClick(View paramView)
  {
    c.a locala = (c.a)a();
    int i;
    if (paramView != null)
    {
      i = paramView.getId();
      paramView = Integer.valueOf(i);
    }
    else
    {
      i = 0;
      paramView = null;
    }
    locala.a(paramView);
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    String str = "view";
    k.b(paramView, str);
    super.onViewCreated(paramView, paramBundle);
    paramView = c;
    if (paramView != null)
    {
      paramView.p();
      paramView.f();
    }
    int i = R.id.typeWalletCard;
    paramView = (CardView)a(i);
    paramBundle = this;
    paramBundle = (View.OnClickListener)this;
    paramView.setOnClickListener(paramBundle);
    i = R.id.typeLongCard;
    ((CardView)a(i)).setOnClickListener(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.views.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */