package com.truecaller.credit.app.ui.infocollection.views.b;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import c.g.b.k;
import com.truecaller.credit.R.layout;
import com.truecaller.credit.R.string;
import com.truecaller.credit.app.ui.b.c;
import com.truecaller.credit.app.ui.infocollection.a.a.a.a;
import com.truecaller.credit.app.ui.infocollection.a.a.b;
import com.truecaller.credit.app.ui.infocollection.views.c.q.c;
import com.truecaller.credit.i;
import com.truecaller.credit.i.a;
import java.util.HashMap;

public final class n
  extends c
  implements q.c
{
  private com.truecaller.credit.app.ui.infocollection.views.c.n c;
  private HashMap d;
  
  public final View a(int paramInt)
  {
    Object localObject1 = d;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      d = ((HashMap)localObject1);
    }
    localObject1 = d;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = d;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "text");
    com.truecaller.credit.app.ui.infocollection.views.c.n localn = c;
    if (localn != null)
    {
      localn.e(paramString);
      return;
    }
  }
  
  public final void b()
  {
    a.a locala = com.truecaller.credit.app.ui.infocollection.a.a.a.a();
    Object localObject = i.i;
    localObject = i.a.a();
    locala.a((com.truecaller.credit.app.c.a.a)localObject).a().a(this);
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "type");
    Object localObject = new com/truecaller/credit/app/ui/infocollection/views/b/m;
    ((m)localObject).<init>();
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    String str = "address_type";
    localBundle.putString(str, paramString);
    ((m)localObject).setArguments(localBundle);
    paramString = c;
    if (paramString != null)
    {
      localObject = (Fragment)localObject;
      paramString.a(200, (Fragment)localObject);
      return;
    }
  }
  
  public final int c()
  {
    return R.layout.fragment_info_schedule_visit;
  }
  
  public final String e()
  {
    int i = R.string.credit_title_schedule_meeting;
    String str = getString(i);
    k.a(str, "getString(R.string.credit_title_schedule_meeting)");
    return str;
  }
  
  public final void f()
  {
    HashMap localHashMap = d;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final String g()
  {
    com.truecaller.credit.app.ui.infocollection.views.c.n localn = c;
    if (localn != null) {
      return localn.m();
    }
    return null;
  }
  
  public final String h()
  {
    Bundle localBundle = getArguments();
    if (localBundle != null) {
      return localBundle.getString("address_type");
    }
    return null;
  }
  
  public final void onAttach(Context paramContext)
  {
    Object localObject = "context";
    k.b(paramContext, (String)localObject);
    super.onAttach(paramContext);
    boolean bool = paramContext instanceof com.truecaller.credit.app.ui.infocollection.views.c.n;
    if (bool)
    {
      paramContext = (com.truecaller.credit.app.ui.infocollection.views.c.n)paramContext;
      c = paramContext;
      return;
    }
    localObject = new java/lang/RuntimeException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(paramContext);
    localStringBuilder.append(" must implement InfoUIUpdateListener");
    paramContext = localStringBuilder.toString();
    ((RuntimeException)localObject).<init>(paramContext);
    throw ((Throwable)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.views.b.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */