package com.truecaller.credit.app.ui.infocollection.views.c;

import android.net.Uri;
import com.truecaller.credit.app.ui.assist.CreditDocumentType;

public abstract interface i$d
{
  public abstract void a(Uri paramUri, CreditDocumentType paramCreditDocumentType, String paramString);
  
  public abstract void a(Uri paramUri, String paramString, CreditDocumentType paramCreditDocumentType);
  
  public abstract void a(String paramString);
  
  public abstract void a(boolean paramBoolean, String paramString, CreditDocumentType paramCreditDocumentType);
  
  public abstract void b(int paramInt);
  
  public abstract void c(String paramString);
  
  public abstract void e();
  
  public abstract void f();
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.views.c.i.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */