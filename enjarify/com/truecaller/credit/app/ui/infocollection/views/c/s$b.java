package com.truecaller.credit.app.ui.infocollection.views.c;

import android.graphics.Bitmap;
import com.truecaller.credit.app.ui.assist.CreditDocumentType;

public abstract interface s$b
{
  public abstract void a(Bitmap paramBitmap, String paramString);
  
  public abstract void a(CreditDocumentType paramCreditDocumentType, String paramString);
  
  public abstract void a(String paramString);
  
  public abstract void b(String paramString);
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.views.c.s.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */