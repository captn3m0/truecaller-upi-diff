package com.truecaller.credit.app.ui.infocollection.views.b;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.view.View;
import c.g.b.k;
import com.truecaller.credit.R.id;
import com.truecaller.credit.R.layout;
import com.truecaller.credit.app.ui.assist.CreditDocumentType;
import com.truecaller.credit.app.ui.b.c;
import com.truecaller.credit.app.ui.customview.CustomInfoCollectionRadioButton;
import com.truecaller.credit.app.ui.infocollection.a.a.a.a;
import com.truecaller.credit.app.ui.infocollection.a.a.b;
import com.truecaller.credit.app.ui.infocollection.views.activities.DocumentCaptureActivity.a;
import com.truecaller.credit.app.ui.infocollection.views.c.n;
import com.truecaller.credit.app.ui.infocollection.views.c.s.a;
import com.truecaller.credit.app.ui.infocollection.views.c.s.b;
import com.truecaller.credit.i;
import com.truecaller.credit.i.a;
import java.util.HashMap;

public final class o
  extends c
  implements s.b
{
  public static final o.a c;
  private n d;
  private HashMap e;
  
  static
  {
    o.a locala = new com/truecaller/credit/app/ui/infocollection/views/b/o$a;
    locala.<init>((byte)0);
    c = locala;
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = e;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      e = ((HashMap)localObject1);
    }
    localObject1 = e;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = e;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a(Bitmap paramBitmap, String paramString)
  {
    k.b(paramString, "text");
    int i = R.id.takeSelfie;
    ((CustomInfoCollectionRadioButton)a(i)).a(paramString, false);
    int j = R.id.takeSelfie;
    ((CustomInfoCollectionRadioButton)a(j)).setImageOne(paramBitmap);
  }
  
  public final void a(CreditDocumentType paramCreditDocumentType, String paramString)
  {
    k.b(paramCreditDocumentType, "creditDocType");
    k.b(paramString, "cameraType");
    Context localContext = getContext();
    if (localContext != null)
    {
      k.a(localContext, "it");
      paramCreditDocumentType = DocumentCaptureActivity.a.a(localContext, false, paramCreditDocumentType, paramString);
      startActivityForResult(paramCreditDocumentType, 13);
      return;
    }
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "text");
    int i = R.id.takeSelfie;
    ((CustomInfoCollectionRadioButton)a(i)).a(paramString, true);
  }
  
  public final void b()
  {
    a.a locala = com.truecaller.credit.app.ui.infocollection.a.a.a.a();
    Object localObject = i.i;
    localObject = i.a.a();
    locala.a((com.truecaller.credit.app.c.a.a)localObject).a().a(this);
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "analyticsContext");
    n localn = d;
    if (localn != null)
    {
      Object localObject = new com/truecaller/credit/app/ui/infocollection/views/b/l;
      ((l)localObject).<init>();
      localObject = (Fragment)localObject;
      localn.a(25, (Fragment)localObject);
      localn.c(paramString);
      return;
    }
  }
  
  public final int c()
  {
    return R.layout.fragment_info_collection_selfie;
  }
  
  public final void f()
  {
    HashMap localHashMap = e;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    int i = 13;
    if (paramInt1 == i)
    {
      paramInt1 = -1;
      Object localObject1;
      Object localObject2;
      if (paramInt2 == paramInt1)
      {
        if (paramIntent != null)
        {
          localObject1 = (Uri)paramIntent.getParcelableExtra("image");
          if (localObject1 != null)
          {
            localObject2 = (s.a)a();
            ((s.a)localObject2).a((Uri)localObject1);
          }
        }
        return;
      }
      if ((paramInt2 == 0) && (paramIntent != null))
      {
        localObject1 = "state";
        paramInt1 = paramIntent.hasExtra((String)localObject1);
        if (paramInt1 != 0)
        {
          localObject1 = (s.a)a();
          localObject2 = "state";
          i = 0;
          paramInt2 = paramIntent.getIntExtra((String)localObject2, 0);
          ((s.a)localObject1).a(paramInt2);
        }
      }
    }
  }
  
  public final void onAttach(Context paramContext)
  {
    Object localObject = "context";
    k.b(paramContext, (String)localObject);
    super.onAttach(paramContext);
    boolean bool = paramContext instanceof n;
    if (bool)
    {
      paramContext = (n)paramContext;
      d = paramContext;
      return;
    }
    localObject = new java/lang/RuntimeException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(paramContext);
    localStringBuilder.append(" must implement InfoUIUpdateListener");
    paramContext = localStringBuilder.toString();
    ((RuntimeException)localObject).<init>(paramContext);
    throw ((Throwable)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.views.b.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */