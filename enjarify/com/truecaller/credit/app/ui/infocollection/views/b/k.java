package com.truecaller.credit.app.ui.infocollection.views.b;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v4.app.j;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import com.truecaller.credit.R.id;
import com.truecaller.credit.R.layout;
import com.truecaller.credit.app.ui.b.c;
import com.truecaller.credit.app.ui.infocollection.a.a.b;
import com.truecaller.credit.app.ui.infocollection.views.c.o.b;
import com.truecaller.credit.data.models.APIStatusMessage;
import com.truecaller.credit.i;
import com.truecaller.credit.i.a;
import java.util.HashMap;

public final class k
  extends c
  implements o.b
{
  private com.truecaller.credit.app.ui.infocollection.views.c.n c;
  private HashMap d;
  
  public final View a(int paramInt)
  {
    Object localObject1 = d;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      d = ((HashMap)localObject1);
    }
    localObject1 = d;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = d;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a(APIStatusMessage paramAPIStatusMessage)
  {
    c.g.b.k.b(paramAPIStatusMessage, "apiStatusMessage");
    Object localObject = getActivity();
    if (localObject != null)
    {
      localObject = a.d;
      paramAPIStatusMessage = a.a.a(paramAPIStatusMessage);
      localObject = getFragmentManager();
      String str = "APIStatusFragment";
      paramAPIStatusMessage.show((j)localObject, str);
    }
  }
  
  public final void a(String paramString)
  {
    c.g.b.k.b(paramString, "text");
    com.truecaller.credit.app.ui.infocollection.views.c.n localn = c;
    if (localn != null)
    {
      localn.e(paramString);
      return;
    }
  }
  
  public final void b()
  {
    com.truecaller.credit.app.ui.infocollection.a.a.a.a locala = com.truecaller.credit.app.ui.infocollection.a.a.a.a();
    Object localObject = i.i;
    localObject = i.a.a();
    locala.a((com.truecaller.credit.app.c.a.a)localObject).a().a(this);
  }
  
  public final void b(String paramString)
  {
    c.g.b.k.b(paramString, "slot");
    int i = R.id.textDateTimeSlot;
    TextView localTextView = (TextView)a(i);
    c.g.b.k.a(localTextView, "textDateTimeSlot");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final int c()
  {
    return R.layout.fragment_meeting_scheduled;
  }
  
  public final void c(String paramString)
  {
    c.g.b.k.b(paramString, "address");
    int i = R.id.textAddress;
    TextView localTextView = (TextView)a(i);
    c.g.b.k.a(localTextView, "textAddress");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void d(String paramString)
  {
    c.g.b.k.b(paramString, "addressType");
    Object localObject = new com/truecaller/credit/app/ui/infocollection/views/b/n;
    ((n)localObject).<init>();
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    String str = "address_type";
    localBundle.putString(str, paramString);
    ((n)localObject).setArguments(localBundle);
    paramString = c;
    if (paramString != null)
    {
      localObject = (Fragment)localObject;
      paramString.a(200, (Fragment)localObject);
      return;
    }
  }
  
  public final void f()
  {
    HashMap localHashMap = d;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final void g()
  {
    com.truecaller.credit.app.ui.infocollection.views.c.n localn = c;
    if (localn != null)
    {
      localn.e();
      return;
    }
  }
  
  public final String h()
  {
    Bundle localBundle = getArguments();
    if (localBundle != null) {
      return localBundle.getString("date", null);
    }
    return null;
  }
  
  public final String i()
  {
    Bundle localBundle = getArguments();
    if (localBundle != null) {
      return localBundle.getString("slot", null);
    }
    return null;
  }
  
  public final String j()
  {
    Bundle localBundle = getArguments();
    if (localBundle != null) {
      return localBundle.getString("address", null);
    }
    return null;
  }
  
  public final void k()
  {
    f localf = getActivity();
    if (localf != null)
    {
      localf.finish();
      return;
    }
  }
  
  public final void l()
  {
    Object localObject = getActivity();
    if (localObject != null)
    {
      localObject = getFragmentManager();
      if (localObject != null)
      {
        String str = "APIStatusFragment";
        localObject = ((j)localObject).a(str);
      }
      else
      {
        localObject = null;
      }
      if (localObject != null)
      {
        localObject = (a)localObject;
        ((a)localObject).e();
      }
    }
  }
  
  public final void onAttach(Context paramContext)
  {
    Object localObject = "context";
    c.g.b.k.b(paramContext, (String)localObject);
    super.onAttach(paramContext);
    boolean bool = paramContext instanceof com.truecaller.credit.app.ui.infocollection.views.c.n;
    if (bool)
    {
      paramContext = (com.truecaller.credit.app.ui.infocollection.views.c.n)paramContext;
      c = paramContext;
      return;
    }
    localObject = new java/lang/RuntimeException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(paramContext);
    localStringBuilder.append(" must implement InfoUIUpdateListener");
    paramContext = localStringBuilder.toString();
    ((RuntimeException)localObject).<init>(paramContext);
    throw ((Throwable)localObject);
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    c.g.b.k.b(paramView, "view");
    super.onViewCreated(paramView, paramBundle);
    int i = R.id.btnReschedule;
    paramView = (TextView)a(i);
    paramBundle = new com/truecaller/credit/app/ui/infocollection/views/b/k$a;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.views.b.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */