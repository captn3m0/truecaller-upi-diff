package com.truecaller.credit.app.ui.infocollection.views.a;

import android.net.Uri;
import com.d.b.w;
import com.truecaller.credit.domain.interactors.infocollection.models.PoaData;
import java.util.List;

public abstract interface k
{
  public abstract void a(int paramInt);
  
  public abstract void a(Uri paramUri, String paramString, boolean paramBoolean);
  
  public abstract void a(w paramw);
  
  public abstract void a(PoaData paramPoaData);
  
  public abstract void a(List paramList, String paramString, boolean paramBoolean);
  
  public abstract void b();
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.views.a.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */