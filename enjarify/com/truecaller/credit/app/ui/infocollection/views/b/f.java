package com.truecaller.credit.app.ui.infocollection.views.b;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.credit.R.id;
import com.truecaller.credit.R.layout;
import com.truecaller.credit.R.string;
import com.truecaller.credit.app.ui.b.c;
import com.truecaller.credit.app.ui.infocollection.a.a.b;
import com.truecaller.credit.app.ui.infocollection.views.c.g.a;
import com.truecaller.credit.app.ui.infocollection.views.c.g.b;
import com.truecaller.credit.app.ui.infocollection.views.c.n;
import com.truecaller.credit.data.models.APIStatusMessage;
import com.truecaller.credit.data.models.UserInfoDataRequest;
import com.truecaller.credit.i;
import com.truecaller.credit.i.a;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;

public final class f
  extends c
  implements TextWatcher, View.OnClickListener, g.b
{
  private n c;
  private HashMap d;
  
  public final View a(int paramInt)
  {
    Object localObject1 = d;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      d = ((HashMap)localObject1);
    }
    localObject1 = d;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = d;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a(APIStatusMessage paramAPIStatusMessage)
  {
    k.b(paramAPIStatusMessage, "apiStatusMessage");
    Object localObject = getActivity();
    if (localObject != null)
    {
      localObject = a.d;
      paramAPIStatusMessage = a.a.a(paramAPIStatusMessage);
      localObject = getFragmentManager();
      String str = "APIStatusFragment";
      paramAPIStatusMessage.show((j)localObject, str);
    }
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "buttonText");
    n localn = c;
    if (localn != null)
    {
      localn.e(paramString);
      localn.q();
      localn.n();
      return;
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    int i = R.id.addressFormContainer;
    Object localObject = (LinearLayout)a(i);
    k.a(localObject, "addressFormContainer");
    t.a((View)localObject, paramBoolean);
    i = R.id.textAddressLine1;
    localObject = (TextInputEditText)a(i);
    k.a(localObject, "textAddressLine1");
    t.a((View)localObject, paramBoolean, 10);
  }
  
  public final void afterTextChanged(Editable paramEditable)
  {
    if (paramEditable != null)
    {
      Object localObject1 = paramEditable.toString();
      if (localObject1 != null)
      {
        int i = R.id.textCity;
        paramEditable = (TextInputEditText)a(i);
        Object localObject2 = "textCity";
        k.a(paramEditable, (String)localObject2);
        paramEditable = String.valueOf(paramEditable.getText());
        boolean bool1 = k.a(localObject1, paramEditable);
        Object localObject3;
        Object localObject4;
        Object localObject5;
        Object localObject6;
        Object localObject7;
        if (bool1)
        {
          paramEditable = a();
          localObject2 = paramEditable;
          localObject2 = (g.a)paramEditable;
          j = R.id.textAddressLine1;
          paramEditable = (TextInputEditText)a(j);
          k.a(paramEditable, "textAddressLine1");
          localObject3 = com.truecaller.credit.app.ui.assist.a.a((EditText)paramEditable);
          j = R.id.textAddressLine2;
          paramEditable = (TextInputEditText)a(j);
          k.a(paramEditable, "textAddressLine2");
          localObject4 = com.truecaller.credit.app.ui.assist.a.a((EditText)paramEditable);
          j = R.id.textAddressLine3;
          paramEditable = (TextInputEditText)a(j);
          k.a(paramEditable, "textAddressLine3");
          localObject5 = com.truecaller.credit.app.ui.assist.a.a((EditText)paramEditable);
          j = R.id.textPinCode;
          paramEditable = (TextInputEditText)a(j);
          k.a(paramEditable, "textPinCode");
          localObject6 = com.truecaller.credit.app.ui.assist.a.a((EditText)paramEditable);
          j = R.id.textState;
          paramEditable = (TextInputEditText)a(j);
          k.a(paramEditable, "textState");
          paramEditable = com.truecaller.credit.app.ui.assist.a.a((EditText)paramEditable);
          localObject7 = localObject1;
          localObject1 = paramEditable;
          ((g.a)localObject2).a((String)localObject7, (String)localObject3, (String)localObject4, (String)localObject5, (String)localObject6, paramEditable);
          return;
        }
        int j = R.id.textPinCode;
        paramEditable = (TextInputEditText)a(j);
        localObject2 = "textPinCode";
        k.a(paramEditable, (String)localObject2);
        paramEditable = com.truecaller.credit.app.ui.assist.a.a((EditText)paramEditable);
        boolean bool2 = k.a(localObject1, paramEditable);
        if (bool2)
        {
          paramEditable = a();
          localObject2 = paramEditable;
          localObject2 = (g.a)paramEditable;
          k = R.id.textCity;
          paramEditable = (TextInputEditText)a(k);
          k.a(paramEditable, "textCity");
          localObject7 = com.truecaller.credit.app.ui.assist.a.a((EditText)paramEditable);
          k = R.id.textAddressLine1;
          paramEditable = (TextInputEditText)a(k);
          k.a(paramEditable, "textAddressLine1");
          localObject3 = com.truecaller.credit.app.ui.assist.a.a((EditText)paramEditable);
          k = R.id.textAddressLine2;
          paramEditable = (TextInputEditText)a(k);
          k.a(paramEditable, "textAddressLine2");
          localObject4 = com.truecaller.credit.app.ui.assist.a.a((EditText)paramEditable);
          k = R.id.textAddressLine3;
          paramEditable = (TextInputEditText)a(k);
          k.a(paramEditable, "textAddressLine3");
          localObject5 = com.truecaller.credit.app.ui.assist.a.a((EditText)paramEditable);
          k = R.id.textState;
          paramEditable = (TextInputEditText)a(k);
          k.a(paramEditable, "textState");
          paramEditable = com.truecaller.credit.app.ui.assist.a.a((EditText)paramEditable);
          localObject6 = localObject1;
          localObject1 = paramEditable;
          ((g.a)localObject2).a((String)localObject7, (String)localObject3, (String)localObject4, (String)localObject5, (String)localObject6, paramEditable);
          return;
        }
        int k = R.id.textAddressLine1;
        paramEditable = (TextInputEditText)a(k);
        localObject2 = "textAddressLine1";
        k.a(paramEditable, (String)localObject2);
        paramEditable = com.truecaller.credit.app.ui.assist.a.a((EditText)paramEditable);
        boolean bool3 = k.a(localObject1, paramEditable);
        if (bool3)
        {
          paramEditable = a();
          localObject2 = paramEditable;
          localObject2 = (g.a)paramEditable;
          m = R.id.textCity;
          paramEditable = (TextInputEditText)a(m);
          k.a(paramEditable, "textCity");
          localObject7 = com.truecaller.credit.app.ui.assist.a.a((EditText)paramEditable);
          m = R.id.textAddressLine2;
          paramEditable = (TextInputEditText)a(m);
          k.a(paramEditable, "textAddressLine2");
          localObject4 = com.truecaller.credit.app.ui.assist.a.a((EditText)paramEditable);
          m = R.id.textAddressLine3;
          paramEditable = (TextInputEditText)a(m);
          k.a(paramEditable, "textAddressLine3");
          localObject5 = com.truecaller.credit.app.ui.assist.a.a((EditText)paramEditable);
          m = R.id.textPinCode;
          paramEditable = (TextInputEditText)a(m);
          k.a(paramEditable, "textPinCode");
          localObject6 = com.truecaller.credit.app.ui.assist.a.a((EditText)paramEditable);
          m = R.id.textState;
          paramEditable = (TextInputEditText)a(m);
          k.a(paramEditable, "textState");
          paramEditable = com.truecaller.credit.app.ui.assist.a.a((EditText)paramEditable);
          localObject3 = localObject1;
          localObject1 = paramEditable;
          ((g.a)localObject2).a((String)localObject7, (String)localObject3, (String)localObject4, (String)localObject5, (String)localObject6, paramEditable);
          return;
        }
        int m = R.id.textAddressLine2;
        paramEditable = (TextInputEditText)a(m);
        localObject2 = "textAddressLine2";
        k.a(paramEditable, (String)localObject2);
        paramEditable = com.truecaller.credit.app.ui.assist.a.a((EditText)paramEditable);
        boolean bool4 = k.a(localObject1, paramEditable);
        if (bool4)
        {
          paramEditable = a();
          localObject2 = paramEditable;
          localObject2 = (g.a)paramEditable;
          n = R.id.textCity;
          paramEditable = (TextInputEditText)a(n);
          k.a(paramEditable, "textCity");
          localObject7 = com.truecaller.credit.app.ui.assist.a.a((EditText)paramEditable);
          n = R.id.textAddressLine1;
          paramEditable = (TextInputEditText)a(n);
          k.a(paramEditable, "textAddressLine1");
          localObject3 = com.truecaller.credit.app.ui.assist.a.a((EditText)paramEditable);
          n = R.id.textAddressLine3;
          paramEditable = (TextInputEditText)a(n);
          k.a(paramEditable, "textAddressLine3");
          localObject5 = com.truecaller.credit.app.ui.assist.a.a((EditText)paramEditable);
          n = R.id.textPinCode;
          paramEditable = (TextInputEditText)a(n);
          k.a(paramEditable, "textPinCode");
          localObject6 = com.truecaller.credit.app.ui.assist.a.a((EditText)paramEditable);
          n = R.id.textState;
          paramEditable = (TextInputEditText)a(n);
          k.a(paramEditable, "textState");
          paramEditable = com.truecaller.credit.app.ui.assist.a.a((EditText)paramEditable);
          localObject4 = localObject1;
          localObject1 = paramEditable;
          ((g.a)localObject2).a((String)localObject7, (String)localObject3, (String)localObject4, (String)localObject5, (String)localObject6, paramEditable);
          return;
        }
        int n = R.id.textAddressLine3;
        paramEditable = (TextInputEditText)a(n);
        localObject2 = "textAddressLine3";
        k.a(paramEditable, (String)localObject2);
        paramEditable = com.truecaller.credit.app.ui.assist.a.a((EditText)paramEditable);
        boolean bool5 = k.a(localObject1, paramEditable);
        if (bool5)
        {
          paramEditable = a();
          localObject2 = paramEditable;
          localObject2 = (g.a)paramEditable;
          i1 = R.id.textCity;
          paramEditable = (TextInputEditText)a(i1);
          k.a(paramEditable, "textCity");
          localObject7 = com.truecaller.credit.app.ui.assist.a.a((EditText)paramEditable);
          i1 = R.id.textAddressLine1;
          paramEditable = (TextInputEditText)a(i1);
          k.a(paramEditable, "textAddressLine1");
          localObject3 = com.truecaller.credit.app.ui.assist.a.a((EditText)paramEditable);
          i1 = R.id.textAddressLine2;
          paramEditable = (TextInputEditText)a(i1);
          k.a(paramEditable, "textAddressLine2");
          localObject4 = com.truecaller.credit.app.ui.assist.a.a((EditText)paramEditable);
          i1 = R.id.textPinCode;
          paramEditable = (TextInputEditText)a(i1);
          k.a(paramEditable, "textPinCode");
          localObject6 = com.truecaller.credit.app.ui.assist.a.a((EditText)paramEditable);
          i1 = R.id.textState;
          paramEditable = (TextInputEditText)a(i1);
          k.a(paramEditable, "textState");
          paramEditable = com.truecaller.credit.app.ui.assist.a.a((EditText)paramEditable);
          localObject5 = localObject1;
          localObject1 = paramEditable;
          ((g.a)localObject2).a((String)localObject7, (String)localObject3, (String)localObject4, (String)localObject5, (String)localObject6, paramEditable);
          return;
        }
        int i1 = R.id.textState;
        paramEditable = (TextInputEditText)a(i1);
        localObject2 = "textState";
        k.a(paramEditable, (String)localObject2);
        paramEditable = com.truecaller.credit.app.ui.assist.a.a((EditText)paramEditable);
        boolean bool6 = k.a(localObject1, paramEditable);
        if (bool6)
        {
          paramEditable = a();
          localObject2 = paramEditable;
          localObject2 = (g.a)paramEditable;
          int i2 = R.id.textCity;
          paramEditable = (TextInputEditText)a(i2);
          k.a(paramEditable, "textCity");
          localObject7 = com.truecaller.credit.app.ui.assist.a.a((EditText)paramEditable);
          i2 = R.id.textAddressLine1;
          paramEditable = (TextInputEditText)a(i2);
          k.a(paramEditable, "textAddressLine1");
          localObject3 = com.truecaller.credit.app.ui.assist.a.a((EditText)paramEditable);
          i2 = R.id.textAddressLine2;
          paramEditable = (TextInputEditText)a(i2);
          k.a(paramEditable, "textAddressLine2");
          localObject4 = com.truecaller.credit.app.ui.assist.a.a((EditText)paramEditable);
          i2 = R.id.textAddressLine3;
          paramEditable = (TextInputEditText)a(i2);
          k.a(paramEditable, "textAddressLine3");
          localObject5 = com.truecaller.credit.app.ui.assist.a.a((EditText)paramEditable);
          i2 = R.id.textPinCode;
          paramEditable = (TextInputEditText)a(i2);
          k.a(paramEditable, "textPinCode");
          paramEditable = (EditText)paramEditable;
          localObject6 = com.truecaller.credit.app.ui.assist.a.a(paramEditable);
          ((g.a)localObject2).a((String)localObject7, (String)localObject3, (String)localObject4, (String)localObject5, (String)localObject6, (String)localObject1);
        }
        return;
      }
    }
  }
  
  public final void b()
  {
    com.truecaller.credit.app.ui.infocollection.a.a.a.a locala = com.truecaller.credit.app.ui.infocollection.a.a.a.a();
    Object localObject = i.i;
    localObject = i.a.a();
    locala.a((com.truecaller.credit.app.c.a.a)localObject).a().a(this);
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "context");
    n localn = c;
    if (localn != null)
    {
      localn.c(paramString);
      return;
    }
  }
  
  public final void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
  
  public final int c()
  {
    return R.layout.fragment_confirm_user_details;
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "name");
    int i = R.id.textName;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "textName");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void d(String paramString)
  {
    k.b(paramString, "title");
    int i = R.id.sectionHeaderMain;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "sectionHeaderMain");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final boolean d()
  {
    return true;
  }
  
  public final String e()
  {
    int i = R.string.credit_title_personal_info;
    String str = getString(i);
    k.a(str, "getString(R.string.credit_title_personal_info)");
    return str;
  }
  
  public final void e(String paramString)
  {
    k.b(paramString, "address");
    int i = R.id.textAddressPermanent;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "textAddressPermanent");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
    i = R.id.textAddressSelectedCurrent;
    localTextView = (TextView)a(i);
    k.a(localTextView, "textAddressSelectedCurrent");
    localTextView.setText(paramString);
  }
  
  public final void f()
  {
    HashMap localHashMap = d;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final void f(String paramString)
  {
    k.b(paramString, "date");
    int i = R.id.textDob;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "textDob");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
    int j = R.id.textDob;
    paramString = (TextView)a(j);
    k.a(paramString, "textDob");
    t.a((View)paramString);
  }
  
  public final UserInfoDataRequest g()
  {
    Bundle localBundle = getArguments();
    if (localBundle != null) {
      return (UserInfoDataRequest)localBundle.getParcelable("extras_user_detail");
    }
    return null;
  }
  
  public final void h()
  {
    int i = R.id.textCity;
    Object localObject1 = (TextInputEditText)a(i);
    Object localObject2 = this;
    localObject2 = (TextWatcher)this;
    ((TextInputEditText)localObject1).addTextChangedListener((TextWatcher)localObject2);
    i = R.id.textPinCode;
    ((TextInputEditText)a(i)).addTextChangedListener((TextWatcher)localObject2);
    i = R.id.textAddressLine1;
    ((TextInputEditText)a(i)).addTextChangedListener((TextWatcher)localObject2);
    i = R.id.textAddressLine2;
    ((TextInputEditText)a(i)).addTextChangedListener((TextWatcher)localObject2);
    i = R.id.textAddressLine3;
    ((TextInputEditText)a(i)).addTextChangedListener((TextWatcher)localObject2);
    i = R.id.textDob;
    ((TextView)a(i)).addTextChangedListener((TextWatcher)localObject2);
    i = R.id.textName;
    ((TextView)a(i)).addTextChangedListener((TextWatcher)localObject2);
    i = R.id.textState;
    ((TextInputEditText)a(i)).addTextChangedListener((TextWatcher)localObject2);
    i = R.id.checkBoxAddress;
    localObject1 = (CheckBox)a(i);
    localObject2 = this;
    localObject2 = (View.OnClickListener)this;
    ((CheckBox)localObject1).setOnClickListener((View.OnClickListener)localObject2);
  }
  
  public final String i()
  {
    n localn = c;
    if (localn != null) {
      return localn.m();
    }
    return null;
  }
  
  public final void j()
  {
    n localn = c;
    if (localn != null)
    {
      localn.n();
      return;
    }
  }
  
  public final void k()
  {
    n localn = c;
    if (localn != null)
    {
      localn.o();
      return;
    }
  }
  
  public final void l()
  {
    n localn = c;
    if (localn != null)
    {
      localn.g();
      return;
    }
  }
  
  public final void m()
  {
    n localn = c;
    if (localn != null)
    {
      Object localObject = e.d;
      localObject = new com/truecaller/credit/app/ui/infocollection/views/b/e;
      ((e)localObject).<init>();
      localObject = (Fragment)localObject;
      localn.a(100, (Fragment)localObject);
      return;
    }
  }
  
  public final void n()
  {
    Object localObject = getActivity();
    if (localObject != null)
    {
      localObject = getFragmentManager();
      if (localObject != null)
      {
        String str = "APIStatusFragment";
        localObject = ((j)localObject).a(str);
      }
      else
      {
        localObject = null;
      }
      if (localObject != null)
      {
        localObject = (a)localObject;
        ((a)localObject).e();
      }
    }
  }
  
  public final void onAttach(Context paramContext)
  {
    Object localObject = "context";
    k.b(paramContext, (String)localObject);
    super.onAttach(paramContext);
    boolean bool = paramContext instanceof n;
    if (bool)
    {
      paramContext = (n)paramContext;
      c = paramContext;
      return;
    }
    localObject = new java/lang/RuntimeException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(paramContext);
    localStringBuilder.append(" must implement InfoUIUpdateListener");
    paramContext = localStringBuilder.toString();
    ((RuntimeException)localObject).<init>(paramContext);
    throw ((Throwable)localObject);
  }
  
  public final void onClick(View paramView)
  {
    if (paramView != null)
    {
      i = paramView.getId();
      paramView = Integer.valueOf(i);
    }
    else
    {
      i = 0;
      paramView = null;
    }
    int j = R.id.checkBoxAddress;
    if (paramView == null) {
      return;
    }
    int i = paramView.intValue();
    if (i == j)
    {
      paramView = (g.a)a();
      j = R.id.checkBoxAddress;
      CheckBox localCheckBox = (CheckBox)a(j);
      String str = "checkBoxAddress";
      k.a(localCheckBox, str);
      boolean bool = localCheckBox.isChecked();
      paramView.a(bool);
    }
  }
  
  public final void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.views.b.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */