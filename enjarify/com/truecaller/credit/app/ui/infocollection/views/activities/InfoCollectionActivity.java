package com.truecaller.credit.app.ui.infocollection.views.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.AppBarLayout.c;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.transition.g;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import c.u;
import com.truecaller.credit.R.drawable;
import com.truecaller.credit.R.id;
import com.truecaller.credit.R.layout;
import com.truecaller.credit.R.menu;
import com.truecaller.credit.app.ui.customview.ThreeStepProgressView;
import com.truecaller.credit.app.ui.faq.activities.CreditWebViewActivity;
import com.truecaller.credit.app.ui.infocollection.a.a.a.a;
import com.truecaller.credit.app.ui.infocollection.views.b.e;
import com.truecaller.credit.app.ui.infocollection.views.b.m;
import com.truecaller.credit.app.ui.infocollection.views.c.e.a;
import com.truecaller.credit.app.ui.infocollection.views.c.f.a;
import com.truecaller.credit.app.ui.infocollection.views.c.g.a;
import com.truecaller.credit.app.ui.infocollection.views.c.k.a;
import com.truecaller.credit.app.ui.infocollection.views.c.l.a;
import com.truecaller.credit.app.ui.infocollection.views.c.l.b;
import com.truecaller.credit.app.ui.infocollection.views.c.m.a;
import com.truecaller.credit.app.ui.infocollection.views.c.o.a;
import com.truecaller.credit.app.ui.infocollection.views.c.p.a;
import com.truecaller.credit.app.ui.infocollection.views.c.q.b;
import com.truecaller.credit.app.ui.infocollection.views.c.r.b;
import com.truecaller.credit.app.ui.infocollection.views.c.s.a;
import com.truecaller.credit.i.a;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;

public final class InfoCollectionActivity
  extends com.truecaller.credit.app.ui.b.b
  implements AppBarLayout.c, View.OnClickListener, l.b, com.truecaller.credit.app.ui.infocollection.views.c.n
{
  private HashMap b;
  
  private final void a(Fragment paramFragment)
  {
    Object localObject = getSupportFragmentManager();
    int i = R.id.container;
    localObject = ((android.support.v4.app.j)localObject).a(i);
    if (localObject == null)
    {
      localObject = getSupportFragmentManager().a();
      i = R.id.container;
      String str = paramFragment.getClass().getSimpleName();
      ((android.support.v4.app.o)localObject).a(i, paramFragment, str).a(null).c();
      localObject = (l.a)a();
      paramFragment = paramFragment.getClass().getSimpleName();
      c.g.b.k.a(paramFragment, "it::class.java.simpleName");
      ((l.a)localObject).b(paramFragment);
      return;
    }
    b(paramFragment);
  }
  
  private void b(Fragment paramFragment)
  {
    if (paramFragment != null)
    {
      Object localObject1 = getSupportFragmentManager();
      int i = R.id.container;
      localObject1 = ((android.support.v4.app.j)localObject1).a(i);
      long l1 = 100;
      if (localObject1 != null)
      {
        g localg = new android/support/transition/g;
        localg.<init>();
        localg.a(l1);
        ((Fragment)localObject1).setExitTransition(localg);
      }
      localObject1 = new android/support/transition/g;
      ((g)localObject1).<init>();
      long l2 = 300L;
      ((g)localObject1).b(l2);
      ((g)localObject1).a(l1);
      paramFragment.setEnterTransition(localObject1);
      localObject1 = getSupportFragmentManager().a();
      i = R.id.container;
      String str = paramFragment.getClass().getSimpleName();
      localObject1 = ((android.support.v4.app.o)localObject1).b(i, paramFragment, str);
      Object localObject2 = "supportFragmentManager.b…t::class.java.simpleName)";
      c.g.b.k.a(localObject1, (String)localObject2);
      if (paramFragment != null)
      {
        localObject2 = paramFragment;
        localObject2 = (com.truecaller.credit.app.ui.b.d)paramFragment;
        boolean bool = ((com.truecaller.credit.app.ui.b.d)localObject2).d();
        if (bool)
        {
          bool = false;
          localObject2 = null;
          ((android.support.v4.app.o)localObject1).a(null);
        }
        ((android.support.v4.app.o)localObject1).d();
        localObject1 = (l.a)a();
        paramFragment = paramFragment.getClass().getSimpleName();
        c.g.b.k.a(paramFragment, "it::class.java.simpleName");
        ((l.a)localObject1).b(paramFragment);
        return;
      }
      paramFragment = new c/u;
      paramFragment.<init>("null cannot be cast to non-null type com.truecaller.credit.app.ui.base.FragmentPropertyProvider");
      throw paramFragment;
    }
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = b;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      b = ((HashMap)localObject1);
    }
    localObject1 = b;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = findViewById(paramInt);
      localObject2 = b;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a(int paramInt, float paramFloat)
  {
    int i = R.id.threeStepProgress;
    Object localObject1 = (ThreeStepProgressView)a(i);
    Object localObject2 = ((ThreeStepProgressView)localObject1).getLayoutParams();
    if (localObject2 != null)
    {
      localObject2 = (ViewGroup.MarginLayoutParams)localObject2;
      i = g;
      paramInt *= i;
      topMargin = paramInt;
      paramInt = R.id.appBarLayout;
      localObject3 = (AppBarLayout)a(paramInt);
      localObject1 = "appBarLayout";
      c.g.b.k.a(localObject3, (String)localObject1);
      localObject3 = ((AppBarLayout)localObject3).getLayoutParams();
      if (localObject3 != null)
      {
        localObject3 = (ViewGroup.MarginLayoutParams)localObject3;
        localObject1 = this;
        int j = com.truecaller.common.h.l.a((Context)this, paramFloat);
        topMargin = j;
        return;
      }
      localObject3 = new c/u;
      ((u)localObject3).<init>("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
      throw ((Throwable)localObject3);
    }
    Object localObject3 = new c/u;
    ((u)localObject3).<init>("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
    throw ((Throwable)localObject3);
  }
  
  public final void a(int paramInt, Fragment paramFragment)
  {
    String str = ((l.a)a()).h();
    if (str == null)
    {
      int i = R.id.threeStepProgress;
      ((ThreeStepProgressView)a(i)).setProgress(paramInt);
      b(paramFragment);
      return;
    }
    ((l.a)a()).g();
  }
  
  public final void a(Drawable paramDrawable)
  {
    c.g.b.k.b(paramDrawable, "drawable");
    int i = R.id.infoBanner;
    ((ImageView)a(i)).setImageDrawable(paramDrawable);
  }
  
  public final void a(AppBarLayout paramAppBarLayout, int paramInt)
  {
    c.g.b.k.b(paramAppBarLayout, "appBarLayout");
    l.a locala = (l.a)a();
    int i = paramAppBarLayout.getTotalScrollRange();
    locala.a(i, paramInt);
  }
  
  public final void a(String paramString)
  {
    c.g.b.k.b(paramString, "title");
    Object localObject = getSupportFragmentManager();
    paramString = ((android.support.v4.app.j)localObject).a(paramString);
    if (paramString != null)
    {
      localObject = getSupportActionBar();
      if (localObject != null)
      {
        if (paramString != null)
        {
          paramString = (CharSequence)((com.truecaller.credit.app.ui.b.d)paramString).e();
          ((ActionBar)localObject).setTitle(paramString);
          return;
        }
        paramString = new c/u;
        paramString.<init>("null cannot be cast to non-null type com.truecaller.credit.app.ui.base.FragmentPropertyProvider");
        throw paramString;
      }
      return;
    }
  }
  
  public final int b()
  {
    return R.layout.activity_info_collection;
  }
  
  public final void b(String paramString)
  {
    c.g.b.k.b(paramString, "url");
    Intent localIntent = new android/content/Intent;
    Object localObject = this;
    localObject = (Context)this;
    localIntent.<init>((Context)localObject, CreditWebViewActivity.class);
    localIntent.putExtra("url", paramString);
    startActivity(localIntent);
  }
  
  public final void c()
  {
    a.a locala = com.truecaller.credit.app.ui.infocollection.a.a.a.a();
    Object localObject = com.truecaller.credit.i.i;
    localObject = i.a.a();
    locala.a((com.truecaller.credit.app.c.a.a)localObject).a().a(this);
  }
  
  public final void c(String paramString)
  {
    c.g.b.k.b(paramString, "context");
    getIntent().putExtra("analytics_context", paramString);
  }
  
  public final void d()
  {
    int i = R.id.toolbarKycSelfieInfo;
    Object localObject1 = (Toolbar)a(i);
    setSupportActionBar((Toolbar)localObject1);
    localObject1 = getSupportActionBar();
    if (localObject1 != null)
    {
      ((ActionBar)localObject1).setDisplayHomeAsUpEnabled(true);
      localObject2 = null;
      ((ActionBar)localObject1).setElevation(0.0F);
      int k = R.drawable.ic_credit_close_white;
      ((ActionBar)localObject1).setHomeAsUpIndicator(k);
    }
    localObject1 = getIntent();
    Object localObject2 = "is_deep_link_flag";
    boolean bool = ((Intent)localObject1).getBooleanExtra((String)localObject2, false);
    if (bool)
    {
      localObject1 = getIntent();
      localObject2 = "intent";
      c.g.b.k.a(localObject1, (String)localObject2);
      localObject1 = ((Intent)localObject1).getData();
      if (localObject1 != null)
      {
        localObject1 = ((Uri)localObject1).getLastPathSegment();
        localObject2 = (l.a)a();
        ((l.a)localObject2).a((String)localObject1);
      }
    }
    else
    {
      localObject1 = com.truecaller.credit.app.ui.infocollection.views.b.o.c;
      localObject1 = new com/truecaller/credit/app/ui/infocollection/views/b/o;
      ((com.truecaller.credit.app.ui.infocollection.views.b.o)localObject1).<init>();
      localObject2 = new android/os/Bundle;
      ((Bundle)localObject2).<init>();
      ((com.truecaller.credit.app.ui.infocollection.views.b.o)localObject1).setArguments((Bundle)localObject2);
      localObject1 = (Fragment)localObject1;
      a((Fragment)localObject1);
    }
    int j = R.id.btnContinue;
    localObject1 = (Button)a(j);
    localObject2 = this;
    localObject2 = (View.OnClickListener)this;
    ((Button)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    j = R.id.appBarLayout;
    localObject1 = (AppBarLayout)a(j);
    localObject2 = this;
    localObject2 = (AppBarLayout.c)this;
    ((AppBarLayout)localObject1).a((AppBarLayout.c)localObject2);
    ((l.a)a()).f();
  }
  
  public final void d(String paramString)
  {
    c.g.b.k.b(paramString, "context");
    c(paramString);
  }
  
  public final void e()
  {
    int i = R.id.appBarLayout;
    ((AppBarLayout)a(i)).setExpanded(true);
  }
  
  public final void e(String paramString)
  {
    c.g.b.k.b(paramString, "buttonText");
    int i = R.id.btnContinue;
    Button localButton = (Button)a(i);
    c.g.b.k.a(localButton, "btnContinue");
    paramString = (CharSequence)paramString;
    localButton.setText(paramString);
  }
  
  public final void f()
  {
    int i = R.id.appBarLayout;
    ((AppBarLayout)a(i)).setExpanded(false);
  }
  
  public final void g()
  {
    ((l.a)a()).c();
  }
  
  public final void h()
  {
    ((l.a)a()).e();
  }
  
  public final void i()
  {
    int i = R.id.collapsingToolbarLayout;
    CollapsingToolbarLayout localCollapsingToolbarLayout = (CollapsingToolbarLayout)a(i);
    c.g.b.k.a(localCollapsingToolbarLayout, "collapsingToolbarLayout");
    t.a((View)localCollapsingToolbarLayout);
    e();
  }
  
  public final void j()
  {
    int i = R.id.collapsingToolbarLayout;
    CollapsingToolbarLayout localCollapsingToolbarLayout = (CollapsingToolbarLayout)a(i);
    c.g.b.k.a(localCollapsingToolbarLayout, "collapsingToolbarLayout");
    t.b((View)localCollapsingToolbarLayout);
  }
  
  public final void k()
  {
    ActionBar localActionBar = getSupportActionBar();
    if (localActionBar != null)
    {
      CharSequence localCharSequence = (CharSequence)"";
      localActionBar.setTitle(localCharSequence);
      return;
    }
  }
  
  public final void l()
  {
    Object localObject1 = getSupportFragmentManager();
    int i = R.id.container;
    localObject1 = ((android.support.v4.app.j)localObject1).a(i);
    boolean bool = localObject1 instanceof com.truecaller.credit.app.ui.infocollection.views.b.o;
    if (bool)
    {
      ((s.a)((com.truecaller.credit.app.ui.infocollection.views.b.o)localObject1).a()).a();
      return;
    }
    bool = localObject1 instanceof com.truecaller.credit.app.ui.infocollection.views.b.l;
    if (bool)
    {
      ((p.a)((com.truecaller.credit.app.ui.infocollection.views.b.l)localObject1).a()).a();
      return;
    }
    bool = localObject1 instanceof com.truecaller.credit.app.ui.infocollection.views.b.j;
    if (bool)
    {
      ((m.a)((com.truecaller.credit.app.ui.infocollection.views.b.j)localObject1).a()).e();
      return;
    }
    bool = localObject1 instanceof com.truecaller.credit.app.ui.infocollection.views.b.f;
    if (bool)
    {
      ((g.a)((com.truecaller.credit.app.ui.infocollection.views.b.f)localObject1).a()).a();
      return;
    }
    bool = localObject1 instanceof com.truecaller.credit.app.ui.infocollection.views.b.n;
    if (bool)
    {
      ((q.b)((com.truecaller.credit.app.ui.infocollection.views.b.n)localObject1).a()).a();
      return;
    }
    bool = localObject1 instanceof e;
    if (bool)
    {
      ((f.a)((e)localObject1).a()).h();
      return;
    }
    bool = localObject1 instanceof com.truecaller.credit.app.ui.infocollection.views.b.d;
    if (bool)
    {
      ((e.a)((com.truecaller.credit.app.ui.infocollection.views.b.d)localObject1).a()).a();
      return;
    }
    bool = localObject1 instanceof com.truecaller.credit.app.ui.infocollection.views.b.i;
    if (bool)
    {
      localObject1 = (com.truecaller.credit.app.ui.infocollection.views.b.i)localObject1;
      k.a locala = (k.a)((com.truecaller.credit.app.ui.infocollection.views.b.i)localObject1).a();
      int j = R.id.etBankName;
      Object localObject2 = (EditText)((com.truecaller.credit.app.ui.infocollection.views.b.i)localObject1).a(j);
      c.g.b.k.a(localObject2, "etBankName");
      localObject2 = ((EditText)localObject2).getText().toString();
      int k = R.id.etCityDistrict;
      Object localObject3 = (EditText)((com.truecaller.credit.app.ui.infocollection.views.b.i)localObject1).a(k);
      c.g.b.k.a(localObject3, "etCityDistrict");
      localObject3 = ((EditText)localObject3).getText().toString();
      int m = R.id.etBranchName;
      localObject1 = (EditText)((com.truecaller.credit.app.ui.infocollection.views.b.i)localObject1).a(m);
      c.g.b.k.a(localObject1, "etBranchName");
      localObject1 = ((EditText)localObject1).getText().toString();
      locala.a((String)localObject2, (String)localObject3, (String)localObject1);
      return;
    }
    bool = localObject1 instanceof m;
    if (bool)
    {
      ((r.b)((m)localObject1).a()).a();
      return;
    }
    bool = localObject1 instanceof com.truecaller.credit.app.ui.infocollection.views.b.k;
    if (bool)
    {
      localObject1 = (o.a)((com.truecaller.credit.app.ui.infocollection.views.b.k)localObject1).a();
      ((o.a)localObject1).a();
    }
  }
  
  public final String m()
  {
    Intent localIntent = getIntent();
    if (localIntent != null) {
      return localIntent.getStringExtra("analytics_context");
    }
    return null;
  }
  
  public final void n()
  {
    int i = R.id.btnContinue;
    Button localButton = (Button)a(i);
    c.g.b.k.a(localButton, "btnContinue");
    localButton.setEnabled(true);
  }
  
  public final void o()
  {
    int i = R.id.btnContinue;
    Button localButton = (Button)a(i);
    c.g.b.k.a(localButton, "btnContinue");
    localButton.setEnabled(false);
  }
  
  public final void onBackPressed()
  {
    Object localObject = getSupportFragmentManager();
    String str = "supportFragmentManager";
    c.g.b.k.a(localObject, str);
    int i = ((android.support.v4.app.j)localObject).e();
    int j = 1;
    if (i != j)
    {
      super.onBackPressed();
      return;
    }
    localObject = getSupportFragmentManager();
    j = R.id.container;
    localObject = ((android.support.v4.app.j)localObject).a(j);
    boolean bool2 = localObject instanceof com.truecaller.credit.app.ui.infocollection.views.b.i;
    if (!bool2)
    {
      boolean bool1 = localObject instanceof com.truecaller.credit.app.ui.infocollection.views.b.f;
      if (!bool1)
      {
        finish();
        return;
      }
    }
    getSupportFragmentManager().c();
  }
  
  public final void onClick(View paramView)
  {
    int i = R.id.btnContinue;
    Button localButton = (Button)a(i);
    boolean bool = c.g.b.k.a(paramView, localButton);
    if (bool)
    {
      paramView = (l.a)a();
      paramView.a();
    }
  }
  
  public final boolean onCreateOptionsMenu(Menu paramMenu)
  {
    super.onCreateOptionsMenu(paramMenu);
    MenuInflater localMenuInflater = getMenuInflater();
    int i = R.menu.menu_credit;
    localMenuInflater.inflate(i, paramMenu);
    return true;
  }
  
  public final boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    super.onOptionsItemSelected(paramMenuItem);
    int i;
    if (paramMenuItem != null)
    {
      i = paramMenuItem.getItemId();
      paramMenuItem = Integer.valueOf(i);
    }
    else
    {
      i = 0;
      paramMenuItem = null;
    }
    if (paramMenuItem != null)
    {
      j = paramMenuItem.intValue();
      int k = 16908332;
      if (j == k)
      {
        finish();
        break label94;
      }
    }
    int j = R.id.info;
    if (paramMenuItem != null)
    {
      i = paramMenuItem.intValue();
      if (i == j)
      {
        paramMenuItem = (l.a)a();
        paramMenuItem.b();
      }
    }
    label94:
    return true;
  }
  
  public final void p()
  {
    int i = R.id.btnContinue;
    Button localButton = (Button)a(i);
    c.g.b.k.a(localButton, "btnContinue");
    localButton.setVisibility(8);
  }
  
  public final void q()
  {
    int i = R.id.btnContinue;
    Button localButton = (Button)a(i);
    c.g.b.k.a(localButton, "btnContinue");
    localButton.setVisibility(0);
  }
  
  public final void r()
  {
    Object localObject = new com/truecaller/credit/app/ui/infocollection/views/b/o;
    ((com.truecaller.credit.app.ui.infocollection.views.b.o)localObject).<init>();
    localObject = (Fragment)localObject;
    a((Fragment)localObject);
  }
  
  public final void s()
  {
    Object localObject = new com/truecaller/credit/app/ui/infocollection/views/b/l;
    ((com.truecaller.credit.app.ui.infocollection.views.b.l)localObject).<init>();
    int i = R.id.threeStepProgress;
    ((ThreeStepProgressView)a(i)).setProgress(25);
    localObject = (Fragment)localObject;
    b((Fragment)localObject);
  }
  
  public final void t()
  {
    Object localObject = new com/truecaller/credit/app/ui/infocollection/views/b/j;
    ((com.truecaller.credit.app.ui.infocollection.views.b.j)localObject).<init>();
    int i = R.id.threeStepProgress;
    ((ThreeStepProgressView)a(i)).setProgress(50);
    localObject = (Fragment)localObject;
    b((Fragment)localObject);
  }
  
  public final void u()
  {
    Object localObject1 = new com/truecaller/credit/app/ui/infocollection/views/b/k;
    ((com.truecaller.credit.app.ui.infocollection.views.b.k)localObject1).<init>();
    int i = R.id.threeStepProgress;
    Object localObject2 = (ThreeStepProgressView)a(i);
    ((ThreeStepProgressView)localObject2).a();
    ((ThreeStepProgressView)localObject2).b();
    Object localObject3 = Integer.valueOf(c);
    ((ThreeStepProgressView)localObject2).setTag(localObject3);
    int j = R.id.pbStep3;
    localObject3 = (ProgressBar)((ThreeStepProgressView)localObject2).c(j);
    int k = R.drawable.ic_tick_active;
    Drawable localDrawable = ((ThreeStepProgressView)localObject2).b(k);
    ((ProgressBar)localObject3).setBackgroundDrawable(localDrawable);
    j = R.id.tvStepCount3;
    localObject2 = (TextView)((ThreeStepProgressView)localObject2).c(j);
    c.g.b.k.a(localObject2, "tvStepCount3");
    ((TextView)localObject2).setVisibility(8);
    localObject1 = (Fragment)localObject1;
    a((Fragment)localObject1);
  }
  
  public final void v()
  {
    Object localObject = new com/truecaller/credit/app/ui/infocollection/views/b/n;
    ((com.truecaller.credit.app.ui.infocollection.views.b.n)localObject).<init>();
    int i = R.id.threeStepProgress;
    ThreeStepProgressView localThreeStepProgressView = (ThreeStepProgressView)a(i);
    localThreeStepProgressView.a();
    localThreeStepProgressView.b();
    localObject = (Fragment)localObject;
    a((Fragment)localObject);
  }
  
  public final void w()
  {
    Object localObject = new com/truecaller/credit/app/ui/infocollection/views/b/e;
    ((e)localObject).<init>();
    int i = R.id.threeStepProgress;
    ((ThreeStepProgressView)a(i)).a();
    localObject = (Fragment)localObject;
    a((Fragment)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.views.activities.InfoCollectionActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */