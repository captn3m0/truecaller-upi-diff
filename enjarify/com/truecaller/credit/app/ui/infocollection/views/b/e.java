package com.truecaller.credit.app.ui.infocollection.views.b;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v4.app.j;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import c.g.b.k;
import com.d.b.ab;
import com.d.b.ai;
import com.d.b.w;
import com.truecaller.common.h.aq.d;
import com.truecaller.credit.R.id;
import com.truecaller.credit.R.layout;
import com.truecaller.credit.R.string;
import com.truecaller.credit.app.ui.b.c;
import com.truecaller.credit.app.ui.infocollection.views.c.f.a;
import com.truecaller.credit.app.ui.infocollection.views.c.f.b;
import com.truecaller.credit.app.util.q;
import com.truecaller.credit.data.models.APIStatusMessage;
import com.truecaller.credit.domain.interactors.infocollection.models.IFSCDetails;
import com.truecaller.credit.i.a;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;

public final class e
  extends c
  implements TextWatcher, com.truecaller.credit.app.ui.infocollection.views.c.b, f.b
{
  public static final e.a d;
  public q c;
  private com.truecaller.credit.app.ui.infocollection.views.c.n e;
  private String f;
  private HashMap g;
  
  static
  {
    e.a locala = new com/truecaller/credit/app/ui/infocollection/views/b/e$a;
    locala.<init>((byte)0);
    d = locala;
  }
  
  private final void a(APIStatusMessage paramAPIStatusMessage, com.truecaller.credit.app.ui.infocollection.views.c.b paramb)
  {
    paramAPIStatusMessage = a.a.a(paramAPIStatusMessage);
    b = paramb;
    paramb = getFragmentManager();
    paramAPIStatusMessage.show(paramb, "APIStatusFragment");
  }
  
  public final void G_()
  {
    ((f.a)a()).a();
  }
  
  public final void H_()
  {
    ((f.a)a()).g();
  }
  
  public final void I_()
  {
    requireActivity().finish();
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = g;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      g = ((HashMap)localObject1);
    }
    localObject1 = g;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = g;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "context");
    com.truecaller.credit.app.ui.infocollection.views.c.n localn = e;
    if (localn != null)
    {
      localn.c(paramString);
      return;
    }
  }
  
  public final void a(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    k.b(paramString1, "bankImage");
    k.b(paramString2, "bankName");
    k.b(paramString3, "lastFourDigits");
    k.b(paramString4, "maskedLastFourDigits");
    k.b(paramString5, "enterAccountNumberText");
    int i = R.id.viewBankDetails;
    Object localObject1 = (ConstraintLayout)a(i);
    k.a(localObject1, "viewBankDetails");
    t.a((View)localObject1);
    i = R.id.tvEnterAccountNumber;
    localObject1 = (TextView)a(i);
    Object localObject2 = "tvEnterAccountNumber";
    k.a(localObject1, (String)localObject2);
    t.a((View)localObject1);
    localObject1 = c;
    if (localObject1 == null)
    {
      localObject2 = "imageLoaderImp";
      k.a((String)localObject2);
    }
    int j = R.id.ivBank;
    localObject2 = (ImageView)a(j);
    k.a(localObject2, "ivBank");
    Object localObject3 = aq.d.b();
    k.a(localObject3, "Transformations.RoundedI…nsformation.getInstance()");
    localObject3 = (ai)localObject3;
    k.b(paramString1, "imageURL");
    k.b(localObject2, "imageView");
    k.b(localObject3, "transformations");
    a.a(paramString1).a((ai)localObject3).a((ImageView)localObject2, null);
    int k = R.id.tvBankName;
    paramString1 = (TextView)a(k);
    k.a(paramString1, "tvBankName");
    paramString2 = (CharSequence)paramString2;
    paramString1.setText(paramString2);
    k = R.id.tvAccountNumber;
    paramString1 = (TextView)a(k);
    k.a(paramString1, "tvAccountNumber");
    paramString4 = (CharSequence)paramString4;
    paramString1.setText(paramString4);
    k = R.id.tvEnterAccountNumber;
    paramString1 = (TextView)a(k);
    k.a(paramString1, "tvEnterAccountNumber");
    paramString5 = (CharSequence)paramString5;
    paramString1.setText(paramString5);
    f = paramString3;
  }
  
  public final void afterTextChanged(Editable paramEditable)
  {
    if (paramEditable != null)
    {
      paramEditable = paramEditable.toString();
      if (paramEditable != null)
      {
        int i = R.id.etBankAccountNumber;
        Object localObject1 = (EditText)a(i);
        Object localObject2 = "etBankAccountNumber";
        k.a(localObject1, (String)localObject2);
        localObject1 = ((EditText)localObject1).getText().toString();
        boolean bool1 = k.a(paramEditable, localObject1);
        if (!bool1)
        {
          int j = R.id.etReEnterBankAccountNumber;
          localObject1 = (EditText)a(j);
          localObject2 = "etReEnterBankAccountNumber";
          k.a(localObject1, (String)localObject2);
          localObject1 = ((EditText)localObject1).getText().toString();
          boolean bool2 = k.a(paramEditable, localObject1);
          if (!bool2)
          {
            k = R.id.etIFSCNumber;
            localObject1 = (EditText)a(k);
            localObject2 = "etIFSCNumber";
            k.a(localObject1, (String)localObject2);
            localObject1 = ((EditText)localObject1).getText().toString();
            boolean bool3 = k.a(paramEditable, localObject1);
            if (bool3)
            {
              paramEditable = (f.a)a();
              k = R.id.etBankAccountNumber;
              localObject1 = (EditText)a(k);
              k.a(localObject1, "etBankAccountNumber");
              localObject1 = ((EditText)localObject1).getText().toString();
              m = R.id.etReEnterBankAccountNumber;
              localObject2 = (EditText)a(m);
              k.a(localObject2, "etReEnterBankAccountNumber");
              localObject2 = ((EditText)localObject2).getText().toString();
              int n = R.id.etIFSCNumber;
              Object localObject3 = (EditText)a(n);
              String str = "etIFSCNumber";
              k.a(localObject3, str);
              localObject3 = ((EditText)localObject3).getText().toString();
              paramEditable.a((String)localObject1, (String)localObject2, (String)localObject3);
            }
            return;
          }
        }
        paramEditable = (f.a)a();
        int k = R.id.etBankAccountNumber;
        localObject1 = (EditText)a(k);
        k.a(localObject1, "etBankAccountNumber");
        localObject1 = ((EditText)localObject1).getText().toString();
        int m = R.id.etReEnterBankAccountNumber;
        localObject2 = (EditText)a(m);
        k.a(localObject2, "etReEnterBankAccountNumber");
        localObject2 = ((EditText)localObject2).getText().toString();
        paramEditable.a((String)localObject1, (String)localObject2);
        return;
      }
    }
  }
  
  public final void b()
  {
    com.truecaller.credit.app.ui.infocollection.a.a.a.a locala = com.truecaller.credit.app.ui.infocollection.a.a.a.a();
    Object localObject = com.truecaller.credit.i.i;
    localObject = i.a.a();
    locala.a((com.truecaller.credit.app.c.a.a)localObject).a().a(this);
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "continueButtonText");
    com.truecaller.credit.app.ui.infocollection.views.c.n localn = e;
    if (localn != null)
    {
      localn.e(paramString);
      return;
    }
  }
  
  public final void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
  
  public final int c()
  {
    return R.layout.fragment_bank_info;
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "errorMessage");
    int i = R.id.tilBankAccountNumber;
    TextInputLayout localTextInputLayout = (TextInputLayout)a(i);
    k.a(localTextInputLayout, "tilBankAccountNumber");
    localTextInputLayout.setErrorEnabled(true);
    i = R.id.tilBankAccountNumber;
    localTextInputLayout = (TextInputLayout)a(i);
    k.a(localTextInputLayout, "tilBankAccountNumber");
    paramString = (CharSequence)paramString;
    localTextInputLayout.setError(paramString);
  }
  
  public final void d(String paramString)
  {
    k.b(paramString, "errorMessage");
    int i = R.id.tilReEnterBankAccountNumber;
    TextInputLayout localTextInputLayout = (TextInputLayout)a(i);
    k.a(localTextInputLayout, "tilReEnterBankAccountNumber");
    localTextInputLayout.setErrorEnabled(true);
    i = R.id.tilReEnterBankAccountNumber;
    localTextInputLayout = (TextInputLayout)a(i);
    k.a(localTextInputLayout, "tilReEnterBankAccountNumber");
    paramString = (CharSequence)paramString;
    localTextInputLayout.setError(paramString);
  }
  
  public final String e()
  {
    int i = R.string.credit_title_bank_details;
    String str = getString(i);
    k.a(str, "getString(R.string.credit_title_bank_details)");
    return str;
  }
  
  public final void e(String paramString)
  {
    k.b(paramString, "errorMessage");
    int i = R.id.tilIFSCNumber;
    TextInputLayout localTextInputLayout = (TextInputLayout)a(i);
    k.a(localTextInputLayout, "tilIFSCNumber");
    localTextInputLayout.setErrorEnabled(true);
    i = R.id.tilIFSCNumber;
    localTextInputLayout = (TextInputLayout)a(i);
    k.a(localTextInputLayout, "tilIFSCNumber");
    paramString = (CharSequence)paramString;
    localTextInputLayout.setError(paramString);
  }
  
  public final void f()
  {
    HashMap localHashMap = g;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final void f(String paramString)
  {
    k.b(paramString, "loadingTitle");
    APIStatusMessage localAPIStatusMessage = new com/truecaller/credit/data/models/APIStatusMessage;
    localAPIStatusMessage.<init>(1, paramString, null, false, null, 28, null);
    paramString = this;
    paramString = (com.truecaller.credit.app.ui.infocollection.views.c.b)this;
    a(localAPIStatusMessage, paramString);
  }
  
  public final void g()
  {
    com.truecaller.credit.app.ui.infocollection.views.c.n localn = e;
    if (localn != null)
    {
      localn.g();
      return;
    }
  }
  
  public final void g(String paramString)
  {
    k.b(paramString, "successTitle");
    APIStatusMessage localAPIStatusMessage = new com/truecaller/credit/data/models/APIStatusMessage;
    localAPIStatusMessage.<init>(2, paramString, null, false, null, 28, null);
    paramString = this;
    paramString = (com.truecaller.credit.app.ui.infocollection.views.c.b)this;
    a(localAPIStatusMessage, paramString);
  }
  
  public final void h()
  {
    int i = R.id.tvIFSCSearch;
    TextView localTextView = (TextView)a(i);
    Object localObject = new com/truecaller/credit/app/ui/infocollection/views/b/e$b;
    ((e.b)localObject).<init>(this);
    localObject = (View.OnClickListener)localObject;
    localTextView.setOnClickListener((View.OnClickListener)localObject);
  }
  
  public final void h(String paramString)
  {
    k.b(paramString, "ifscBankAndBranchName");
    int i = R.id.tvIfscBankName;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "tvIfscBankName");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void i()
  {
    int i = R.id.tilBankAccountNumber;
    TextInputLayout localTextInputLayout = (TextInputLayout)a(i);
    k.a(localTextInputLayout, "tilBankAccountNumber");
    localTextInputLayout.setErrorEnabled(false);
  }
  
  public final void i(String paramString)
  {
    k.b(paramString, "bankName");
    Object localObject = i.e;
    k.b(paramString, "bankName");
    localObject = new android/os/Bundle;
    ((Bundle)localObject).<init>();
    String str = "bank_name";
    ((Bundle)localObject).putString(str, paramString);
    paramString = new com/truecaller/credit/app/ui/infocollection/views/b/i;
    paramString.<init>();
    paramString.setArguments((Bundle)localObject);
    localObject = this;
    localObject = (Fragment)this;
    int i = 1;
    paramString.setTargetFragment((Fragment)localObject, i);
    localObject = e;
    if (localObject != null)
    {
      paramString = (Fragment)paramString;
      ((com.truecaller.credit.app.ui.infocollection.views.c.n)localObject).a(100, paramString);
      return;
    }
  }
  
  public final void j()
  {
    int i = R.id.tilReEnterBankAccountNumber;
    TextInputLayout localTextInputLayout = (TextInputLayout)a(i);
    k.a(localTextInputLayout, "tilReEnterBankAccountNumber");
    localTextInputLayout.setErrorEnabled(false);
  }
  
  public final void j(String paramString)
  {
    k.b(paramString, "ifsc");
    int i = R.id.etIFSCNumber;
    EditText localEditText = (EditText)a(i);
    paramString = (CharSequence)paramString;
    localEditText.setText(paramString);
  }
  
  public final void k()
  {
    int i = R.id.tilIFSCNumber;
    TextInputLayout localTextInputLayout = (TextInputLayout)a(i);
    k.a(localTextInputLayout, "tilIFSCNumber");
    localTextInputLayout.setErrorEnabled(false);
  }
  
  public final void k(String paramString)
  {
    k.b(paramString, "message");
    Context localContext = (Context)requireActivity();
    paramString = (CharSequence)paramString;
    Toast.makeText(localContext, paramString, 0).show();
  }
  
  public final void l()
  {
    int i = R.id.tvIfscBankName;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "tvIfscBankName");
    t.a((View)localTextView);
  }
  
  public final void m()
  {
    int i = R.id.tvIfscBankName;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "tvIfscBankName");
    t.c((View)localTextView);
  }
  
  public final void n()
  {
    com.truecaller.credit.app.ui.infocollection.views.c.n localn = e;
    if (localn != null)
    {
      localn.n();
      return;
    }
  }
  
  public final void o()
  {
    com.truecaller.credit.app.ui.infocollection.views.c.n localn = e;
    if (localn != null)
    {
      localn.o();
      return;
    }
  }
  
  public final void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    int i = 1;
    if (paramInt1 == i)
    {
      paramInt1 = -1;
      if (paramInt2 == paramInt1)
      {
        if (paramIntent == null) {
          k.a();
        }
        IFSCDetails localIFSCDetails = (IFSCDetails)paramIntent.getParcelableExtra("extra_ifsc_value");
        f.a locala = (f.a)a();
        paramIntent = "ifscDetails";
        k.a(localIFSCDetails, paramIntent);
        locala.a(localIFSCDetails);
      }
    }
  }
  
  public final void onAttach(Context paramContext)
  {
    Object localObject = "context";
    k.b(paramContext, (String)localObject);
    super.onAttach(paramContext);
    boolean bool = paramContext instanceof com.truecaller.credit.app.ui.infocollection.views.c.n;
    if (bool)
    {
      paramContext = (com.truecaller.credit.app.ui.infocollection.views.c.n)paramContext;
      e = paramContext;
      return;
    }
    localObject = new java/lang/RuntimeException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    paramContext = paramContext.toString();
    localStringBuilder.append(paramContext);
    localStringBuilder.append(" must implement InfoUIUpdateListener");
    paramContext = localStringBuilder.toString();
    ((RuntimeException)localObject).<init>(paramContext);
    throw ((Throwable)localObject);
  }
  
  public final void onResume()
  {
    super.onResume();
    int i = R.id.etBankAccountNumber;
    EditText localEditText = (EditText)a(i);
    Object localObject = this;
    localObject = (TextWatcher)this;
    localEditText.addTextChangedListener((TextWatcher)localObject);
    i = R.id.etReEnterBankAccountNumber;
    ((EditText)a(i)).addTextChangedListener((TextWatcher)localObject);
    i = R.id.etIFSCNumber;
    ((EditText)a(i)).addTextChangedListener((TextWatcher)localObject);
    ((f.a)a()).f();
  }
  
  public final void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
  
  public final void p()
  {
    com.truecaller.credit.app.ui.infocollection.views.c.n localn = e;
    if (localn != null)
    {
      Object localObject = new com/truecaller/credit/app/ui/infocollection/views/b/n;
      ((n)localObject).<init>();
      localObject = (Fragment)localObject;
      localn.a(200, (Fragment)localObject);
      localn.h();
      return;
    }
  }
  
  public final void q()
  {
    Object localObject = getFragmentManager();
    if (localObject != null)
    {
      String str = "APIStatusFragment";
      localObject = ((j)localObject).a(str);
    }
    else
    {
      localObject = null;
    }
    if (localObject != null)
    {
      localObject = (a)localObject;
      ((a)localObject).e();
    }
  }
  
  public final void r()
  {
    f.a locala = (f.a)a();
    int i = R.id.etBankAccountNumber;
    Object localObject1 = (EditText)a(i);
    k.a(localObject1, "etBankAccountNumber");
    localObject1 = ((EditText)localObject1).getText().toString();
    int j = R.id.etIFSCNumber;
    Object localObject2 = (EditText)a(j);
    k.a(localObject2, "etIFSCNumber");
    localObject2 = ((EditText)localObject2).getText().toString();
    locala.b((String)localObject1, (String)localObject2);
  }
  
  public final void s()
  {
    int i = R.id.pbIFSCSearch;
    ProgressBar localProgressBar = (ProgressBar)a(i);
    k.a(localProgressBar, "pbIFSCSearch");
    t.a((View)localProgressBar);
  }
  
  public final void t()
  {
    int i = R.id.pbIFSCSearch;
    ProgressBar localProgressBar = (ProgressBar)a(i);
    k.a(localProgressBar, "pbIFSCSearch");
    t.b((View)localProgressBar);
  }
  
  public final void u()
  {
    int i = R.id.tvIFSCSearch;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "tvIFSCSearch");
    t.a((View)localTextView);
  }
  
  public final void v()
  {
    int i = R.id.tvIFSCSearch;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "tvIFSCSearch");
    t.b((View)localTextView);
  }
  
  public final void w()
  {
    int i = R.id.viewDetails;
    ConstraintLayout localConstraintLayout = (ConstraintLayout)a(i);
    k.a(localConstraintLayout, "viewDetails");
    t.b((View)localConstraintLayout);
  }
  
  public final void x()
  {
    int i = R.id.viewDetails;
    ConstraintLayout localConstraintLayout = (ConstraintLayout)a(i);
    k.a(localConstraintLayout, "viewDetails");
    t.a((View)localConstraintLayout);
  }
  
  public final void y()
  {
    int i = R.id.etIFSCNumber;
    EditText localEditText = (EditText)a(i);
    k.a(localEditText, "etIFSCNumber");
    t.a((View)localEditText, false, 5);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.views.b.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */