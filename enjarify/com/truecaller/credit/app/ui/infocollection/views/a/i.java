package com.truecaller.credit.app.ui.infocollection.views.a;

import android.net.Uri;
import com.d.b.w;
import com.truecaller.credit.R.string;
import com.truecaller.credit.domain.interactors.infocollection.models.PoaData;
import com.truecaller.credit.domain.interactors.infocollection.models.PoaImage;
import com.truecaller.utils.n;
import java.util.List;

public final class i
  implements h
{
  private int a;
  private final n b;
  private final w c;
  
  public i(n paramn, w paramw)
  {
    b = paramn;
    c = paramw;
    a = -1;
  }
  
  public final void a(int paramInt)
  {
    a = paramInt;
  }
  
  public final void a(k paramk, PoaData paramPoaData)
  {
    c.g.b.k.b(paramk, "poaItemView");
    c.g.b.k.b(paramPoaData, "poaData");
    Object localObject1 = c;
    paramk.a((w)localObject1);
    paramk.a(paramPoaData);
    int i = a;
    int j = -1;
    if (i != j) {
      paramk.a(i);
    }
    paramPoaData = paramPoaData.getImages();
    if (paramPoaData != null)
    {
      i = paramPoaData.size();
      j = 1;
      Object localObject2 = null;
      boolean bool1;
      switch (i)
      {
      default: 
        break;
      case 2: 
        localObject1 = (PoaImage)paramPoaData.get(j);
        boolean bool2 = ((PoaImage)localObject1).getStatus();
        int m;
        if (bool2)
        {
          localObject3 = b;
          m = R.string.upload_success;
        }
        else
        {
          localObject3 = b;
          m = R.string.upload_failed;
        }
        localObject2 = new Object[0];
        localObject2 = ((n)localObject3).a(m, (Object[])localObject2);
        Object localObject3 = "if(poaImage.status) reso…g(R.string.upload_failed)";
        c.g.b.k.a(localObject2, (String)localObject3);
        bool1 = ((PoaImage)localObject1).getStatus() ^ j;
        paramk.a(paramPoaData, (String)localObject2, bool1);
        break;
      case 1: 
        paramPoaData = (PoaImage)paramPoaData.get(0);
        bool1 = paramPoaData.getStatus();
        int k;
        if (bool1)
        {
          localObject1 = b;
          k = R.string.credit_poa_front_upload_success;
        }
        else
        {
          localObject1 = b;
          k = R.string.upload_failed;
        }
        localObject2 = new Object[0];
        localObject1 = ((n)localObject1).a(k, (Object[])localObject2);
        c.g.b.k.a(localObject1, "if(poaImage.status) reso…g(R.string.upload_failed)");
        localObject2 = paramPoaData.getUri();
        boolean bool3 = paramPoaData.getStatus() ^ j;
        paramk.a((Uri)localObject2, (String)localObject1, bool3);
        return;
      }
      return;
    }
    paramk.b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.views.a.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */