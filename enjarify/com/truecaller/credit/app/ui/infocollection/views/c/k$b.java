package com.truecaller.credit.app.ui.infocollection.views.c;

import com.truecaller.credit.domain.interactors.infocollection.models.IFSCDetails;
import java.util.List;

public abstract interface k$b
{
  public static final k.b.a a = k.b.a.a;
  
  public abstract void a(String paramString);
  
  public abstract void a(List paramList);
  
  public abstract void b(IFSCDetails paramIFSCDetails);
  
  public abstract void b(String paramString);
  
  public abstract void c(String paramString);
  
  public abstract void g();
  
  public abstract void h();
  
  public abstract void i();
  
  public abstract void j();
  
  public abstract void k();
  
  public abstract void l();
  
  public abstract void m();
  
  public abstract void n();
  
  public abstract void o();
  
  public abstract void p();
  
  public abstract void q();
  
  public abstract void r();
  
  public abstract void s();
  
  public abstract void t();
  
  public abstract void u();
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.views.c.k.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */