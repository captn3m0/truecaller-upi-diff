package com.truecaller.credit.app.ui.infocollection.views.b;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v4.app.j;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.truecaller.credit.R.id;
import com.truecaller.credit.R.layout;
import com.truecaller.credit.R.string;
import com.truecaller.credit.app.ui.customview.ScheduleSlotView;
import com.truecaller.credit.app.ui.customview.a.d;
import com.truecaller.credit.app.ui.infocollection.a.a.b;
import com.truecaller.credit.app.ui.infocollection.views.c.n;
import com.truecaller.credit.app.ui.infocollection.views.c.r.b;
import com.truecaller.credit.app.ui.infocollection.views.c.r.c;
import com.truecaller.credit.data.models.APIStatusMessage;
import com.truecaller.credit.data.models.Address;
import com.truecaller.credit.domain.interactors.infocollection.models.AppointmentData;
import com.truecaller.credit.i;
import com.truecaller.credit.i.a;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;
import java.util.List;

public final class m
  extends com.truecaller.credit.app.ui.b.c
  implements View.OnClickListener, com.truecaller.credit.app.ui.customview.c, c.b, r.c
{
  private n c;
  private HashMap d;
  
  public final View a(int paramInt)
  {
    Object localObject1 = d;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      d = ((HashMap)localObject1);
    }
    localObject1 = d;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = d;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a(APIStatusMessage paramAPIStatusMessage)
  {
    c.g.b.k.b(paramAPIStatusMessage, "apiStatusMessage");
    Object localObject = getActivity();
    if (localObject != null)
    {
      localObject = a.d;
      paramAPIStatusMessage = a.a.a(paramAPIStatusMessage);
      localObject = getFragmentManager();
      String str = "APIStatusFragment";
      paramAPIStatusMessage.show((j)localObject, str);
    }
  }
  
  public final void a(Address paramAddress)
  {
    ((r.b)a()).a(paramAddress, true);
  }
  
  public final void a(String paramString)
  {
    c.g.b.k.b(paramString, "text");
    n localn = c;
    if (localn != null)
    {
      localn.e(paramString);
      return;
    }
  }
  
  public final void a(String paramString, Address paramAddress)
  {
    c.g.b.k.b(paramString, "addressType");
    Object localObject1 = getActivity();
    if (localObject1 != null)
    {
      c.g.b.k.a(localObject1, "it");
      localObject1 = ((f)localObject1).getSupportFragmentManager();
      String str1 = c.class.getCanonicalName();
      Object localObject2 = ((j)localObject1).a(str1);
      if (localObject2 == null)
      {
        localObject2 = c.c;
        localObject2 = this;
        localObject2 = (c.b)this;
        c.g.b.k.b(paramString, "addressType");
        Object localObject3 = new com/truecaller/credit/app/ui/infocollection/views/b/c;
        ((c)localObject3).<init>();
        Bundle localBundle = new android/os/Bundle;
        localBundle.<init>();
        String str2 = "address_type";
        localBundle.putString(str2, paramString);
        paramString = "address";
        paramAddress = (Parcelable)paramAddress;
        localBundle.putParcelable(paramString, paramAddress);
        ((c)localObject3).setArguments(localBundle);
        c.a((c)localObject3, (c.b)localObject2);
        localObject3 = (android.support.v4.app.e)(android.support.v4.app.e)localObject3;
        ((android.support.v4.app.e)localObject3).show((j)localObject1, str1);
      }
      ((j)localObject1).b();
      return;
    }
  }
  
  public final void a(String paramString1, String paramString2)
  {
    ((r.b)a()).a(paramString1, paramString2);
  }
  
  public final void a(String paramString1, String paramString2, String paramString3)
  {
    c.g.b.k.b(paramString1, "date");
    c.g.b.k.b(paramString2, "slot");
    c.g.b.k.b(paramString3, "address");
    Object localObject1 = new com/truecaller/credit/app/ui/infocollection/views/b/k;
    ((k)localObject1).<init>();
    Object localObject2 = new android/os/Bundle;
    ((Bundle)localObject2).<init>();
    ((k)localObject1).setArguments((Bundle)localObject2);
    localObject2 = ((k)localObject1).getArguments();
    if (localObject2 != null)
    {
      String str = "date";
      ((Bundle)localObject2).putString(str, paramString1);
    }
    paramString1 = ((k)localObject1).getArguments();
    if (paramString1 != null)
    {
      localObject2 = "slot";
      paramString1.putString((String)localObject2, paramString2);
    }
    paramString1 = ((k)localObject1).getArguments();
    if (paramString1 != null)
    {
      paramString2 = "address";
      paramString1.putString(paramString2, paramString3);
    }
    paramString1 = c;
    if (paramString1 != null)
    {
      localObject1 = (Fragment)localObject1;
      paramString1.a(300, (Fragment)localObject1);
      return;
    }
  }
  
  public final void a(List paramList)
  {
    c.g.b.k.b(paramList, "appointments");
    int i = R.id.scheduleSlotView;
    Object localObject1 = (ScheduleSlotView)a(i);
    Object localObject2 = this;
    localObject2 = (com.truecaller.credit.app.ui.customview.c)this;
    c.g.b.k.b(paramList, "appointments");
    c = ((com.truecaller.credit.app.ui.customview.c)localObject2);
    f = paramList;
    d = 0;
    e = -1;
    int j = d;
    ((AppointmentData)paramList.get(j)).setSelected(true);
    Object localObject3 = new com/truecaller/credit/app/ui/customview/a/c;
    Object localObject4 = localObject1;
    localObject4 = (com.truecaller.credit.app.ui.customview.a.a)localObject1;
    Context localContext = ((ScheduleSlotView)localObject1).getContext();
    c.g.b.k.a(localContext, "context");
    ((com.truecaller.credit.app.ui.customview.a.c)localObject3).<init>(paramList, (com.truecaller.credit.app.ui.customview.a.a)localObject4, localContext);
    a = ((com.truecaller.credit.app.ui.customview.a.c)localObject3);
    j = R.id.recyclerDatePicker;
    localObject3 = (RecyclerView)((ScheduleSlotView)localObject1).c(j);
    c.g.b.k.a(localObject3, "recyclerDatePicker");
    localObject4 = (RecyclerView.Adapter)a;
    ((RecyclerView)localObject3).setAdapter((RecyclerView.Adapter)localObject4);
    localObject3 = new com/truecaller/credit/app/ui/customview/a/d;
    paramList = ((AppointmentData)paramList.get(0)).getSlots();
    localObject2 = localObject1;
    localObject2 = (com.truecaller.credit.app.ui.customview.a.e)localObject1;
    ((d)localObject3).<init>(paramList, (com.truecaller.credit.app.ui.customview.a.e)localObject2);
    b = ((d)localObject3);
    paramList = new android/support/v7/widget/GridLayoutManager;
    localObject2 = ((ScheduleSlotView)localObject1).getContext();
    paramList.<init>((Context)localObject2, 2);
    int k = R.id.recyclerTimePicker;
    localObject2 = (RecyclerView)((ScheduleSlotView)localObject1).c(k);
    c.g.b.k.a(localObject2, "recyclerTimePicker");
    paramList = (RecyclerView.LayoutManager)paramList;
    ((RecyclerView)localObject2).setLayoutManager(paramList);
    int m = R.id.recyclerTimePicker;
    paramList = (RecyclerView)((ScheduleSlotView)localObject1).c(m);
    c.g.b.k.a(paramList, "recyclerTimePicker");
    localObject1 = (RecyclerView.Adapter)b;
    paramList.setAdapter((RecyclerView.Adapter)localObject1);
  }
  
  public final void b()
  {
    com.truecaller.credit.app.ui.infocollection.a.a.a.a locala = com.truecaller.credit.app.ui.infocollection.a.a.a.a();
    Object localObject = i.i;
    localObject = i.a.a();
    locala.a((com.truecaller.credit.app.c.a.a)localObject).a().a(this);
  }
  
  public final void b(String paramString)
  {
    c.g.b.k.b(paramString, "text");
    int i = R.id.btnAddressChange;
    TextView localTextView = (TextView)a(i);
    c.g.b.k.a(localTextView, "btnAddressChange");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final int c()
  {
    return R.layout.fragment_schedule_visit;
  }
  
  public final void c(String paramString)
  {
    c.g.b.k.b(paramString, "address");
    int i = R.id.tvAddressForScheduleVisit;
    TextView localTextView = (TextView)a(i);
    c.g.b.k.a(localTextView, "tvAddressForScheduleVisit");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final String e()
  {
    int i = R.string.credit_title_schedule_meeting;
    String str = getString(i);
    c.g.b.k.a(str, "getString(R.string.credit_title_schedule_meeting)");
    return str;
  }
  
  public final void f()
  {
    HashMap localHashMap = d;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final String g()
  {
    n localn = c;
    if (localn != null) {
      return localn.m();
    }
    return null;
  }
  
  public final String h()
  {
    Bundle localBundle = getArguments();
    if (localBundle != null) {
      return localBundle.getString("address_type");
    }
    return null;
  }
  
  public final void i()
  {
    n localn = c;
    if (localn != null)
    {
      localn.n();
      return;
    }
  }
  
  public final void j()
  {
    n localn = c;
    if (localn != null)
    {
      localn.o();
      return;
    }
  }
  
  public final void k()
  {
    n localn = c;
    if (localn != null)
    {
      localn.g();
      return;
    }
  }
  
  public final void l()
  {
    int i = R.id.progressScheduleSlot;
    ProgressBar localProgressBar = (ProgressBar)a(i);
    c.g.b.k.a(localProgressBar, "progressScheduleSlot");
    t.b((View)localProgressBar);
  }
  
  public final void m()
  {
    int i = R.id.progressScheduleSlot;
    ProgressBar localProgressBar = (ProgressBar)a(i);
    c.g.b.k.a(localProgressBar, "progressScheduleSlot");
    t.a((View)localProgressBar);
  }
  
  public final void n()
  {
    int i = R.id.pbScheduleVisit;
    ProgressBar localProgressBar = (ProgressBar)a(i);
    c.g.b.k.a(localProgressBar, "pbScheduleVisit");
    t.b((View)localProgressBar);
  }
  
  public final void o()
  {
    int i = R.id.pbScheduleVisit;
    ProgressBar localProgressBar = (ProgressBar)a(i);
    c.g.b.k.a(localProgressBar, "pbScheduleVisit");
    t.a((View)localProgressBar);
  }
  
  public final void onAttach(Context paramContext)
  {
    Object localObject = "context";
    c.g.b.k.b(paramContext, (String)localObject);
    super.onAttach(paramContext);
    boolean bool = paramContext instanceof n;
    if (bool)
    {
      paramContext = (n)paramContext;
      c = paramContext;
      return;
    }
    localObject = new java/lang/RuntimeException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(paramContext);
    localStringBuilder.append(" must implement InfoUIUpdateListener");
    paramContext = localStringBuilder.toString();
    ((RuntimeException)localObject).<init>(paramContext);
    throw ((Throwable)localObject);
  }
  
  public final void onClick(View paramView)
  {
    r.b localb = (r.b)a();
    int i;
    if (paramView != null)
    {
      i = paramView.getId();
      paramView = Integer.valueOf(i);
    }
    else
    {
      i = 0;
      paramView = null;
    }
    localb.a(paramView);
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    c.g.b.k.b(paramView, "view");
    super.onViewCreated(paramView, paramBundle);
    int i = R.id.btnAddressChange;
    paramView = (TextView)a(i);
    paramBundle = this;
    paramBundle = (View.OnClickListener)this;
    paramView.setOnClickListener(paramBundle);
  }
  
  public final void p()
  {
    int i = R.id.scheduleSlotView;
    ScheduleSlotView localScheduleSlotView = (ScheduleSlotView)a(i);
    c.g.b.k.a(localScheduleSlotView, "scheduleSlotView");
    t.c((View)localScheduleSlotView);
  }
  
  public final void q()
  {
    int i = R.id.scheduleSlotView;
    ScheduleSlotView localScheduleSlotView = (ScheduleSlotView)a(i);
    c.g.b.k.a(localScheduleSlotView, "scheduleSlotView");
    t.a((View)localScheduleSlotView);
  }
  
  public final void r()
  {
    Object localObject = getActivity();
    if (localObject != null)
    {
      localObject = getFragmentManager();
      if (localObject != null)
      {
        String str = "APIStatusFragment";
        localObject = ((j)localObject).a(str);
      }
      else
      {
        localObject = null;
      }
      if (localObject != null)
      {
        localObject = (a)localObject;
        ((a)localObject).e();
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.views.b.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */