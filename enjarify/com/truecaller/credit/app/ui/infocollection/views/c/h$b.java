package com.truecaller.credit.app.ui.infocollection.views.c;

import android.net.Uri;
import com.google.android.gms.vision.CameraSource;
import com.truecaller.credit.app.ui.assist.CreditDocumentType;

public abstract interface h$b
{
  public static final h.b.a a = h.b.a.a;
  
  public abstract void a(Uri paramUri);
  
  public abstract void a(Uri paramUri, String paramString);
  
  public abstract void a(CameraSource paramCameraSource);
  
  public abstract void a(CreditDocumentType paramCreditDocumentType, int paramInt);
  
  public abstract void a(String paramString);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract void b(String paramString);
  
  public abstract void b(boolean paramBoolean);
  
  public abstract void c(String paramString);
  
  public abstract void c(boolean paramBoolean);
  
  public abstract void d(boolean paramBoolean);
  
  public abstract void e(boolean paramBoolean);
  
  public abstract void f(boolean paramBoolean);
  
  public abstract String g();
  
  public abstract void g(boolean paramBoolean);
  
  public abstract CreditDocumentType h();
  
  public abstract void h(boolean paramBoolean);
  
  public abstract boolean i();
  
  public abstract void j();
  
  public abstract void k();
  
  public abstract void l();
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.views.c.h.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */