package com.truecaller.credit.app.ui.infocollection.views.b;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import c.g.b.k;
import com.truecaller.credit.app.ui.assist.CreditDocumentType;
import com.truecaller.credit.data.models.UserInfoDataRequest;

public final class j$a
{
  public static Fragment a(CreditDocumentType paramCreditDocumentType, UserInfoDataRequest paramUserInfoDataRequest)
  {
    k.b(paramCreditDocumentType, "entryType");
    j localj = new com/truecaller/credit/app/ui/infocollection/views/b/j;
    localj.<init>();
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    paramCreditDocumentType = (Parcelable)paramCreditDocumentType;
    localBundle.putParcelable("document_type", paramCreditDocumentType);
    paramUserInfoDataRequest = (Parcelable)paramUserInfoDataRequest;
    localBundle.putParcelable("extras_user_detail", paramUserInfoDataRequest);
    localj.setArguments(localBundle);
    return (Fragment)localj;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.views.b.j.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */