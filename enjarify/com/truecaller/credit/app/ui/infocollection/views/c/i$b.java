package com.truecaller.credit.app.ui.infocollection.views.c;

import android.net.Uri;
import com.truecaller.bm;
import com.truecaller.credit.app.ui.assist.CreditDocumentType;

public abstract interface i$b
  extends bm
{
  public abstract void a();
  
  public abstract void a(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt);
  
  public abstract void a(Uri paramUri, String paramString);
  
  public abstract void a(String paramString);
  
  public abstract void a(boolean paramBoolean, CreditDocumentType paramCreditDocumentType, String paramString);
  
  public abstract void b();
  
  public abstract void b(Uri paramUri, String paramString);
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.views.c.i.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */