package com.truecaller.credit.app.ui.infocollection.views.a;

import android.content.Context;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import c.g.b.k;
import java.util.List;

public final class b
  extends RecyclerView.Adapter
{
  public List a;
  private final LayoutInflater b;
  private final c c;
  private final b.a d;
  
  public b(Context paramContext, c paramc, b.a parama)
  {
    c = paramc;
    d = parama;
    paramContext = LayoutInflater.from(paramContext);
    k.a(paramContext, "LayoutInflater.from(context)");
    b = paramContext;
  }
  
  public final int getItemCount()
  {
    List localList = a;
    if (localList != null) {
      return localList.size();
    }
    return 0;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.views.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */