package com.truecaller.credit.app.ui.infocollection.views.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import c.g.b.k;
import com.truecaller.credit.app.ui.assist.CreditDocumentType;

public final class DocumentCaptureActivity$a
{
  public static Intent a(Context paramContext, boolean paramBoolean, CreditDocumentType paramCreditDocumentType, String paramString)
  {
    k.b(paramContext, "context");
    k.b(paramCreditDocumentType, "creditDocType");
    k.b(paramString, "cameraType");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, DocumentCaptureActivity.class);
    localIntent.putExtra("qr_reader", paramBoolean);
    paramCreditDocumentType = (Parcelable)paramCreditDocumentType;
    localIntent.putExtra("document_type", paramCreditDocumentType);
    localIntent.putExtra("camera_type", paramString);
    return localIntent;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.views.activities.DocumentCaptureActivity.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */