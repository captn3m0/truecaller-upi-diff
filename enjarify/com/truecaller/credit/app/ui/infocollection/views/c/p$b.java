package com.truecaller.credit.app.ui.infocollection.views.c;

import android.graphics.Bitmap;
import com.truecaller.credit.app.ui.assist.CreditDocumentType;
import com.truecaller.credit.data.models.UserInfoDataRequest;

public abstract interface p$b
{
  public abstract void a(Bitmap paramBitmap, String paramString);
  
  public abstract void a(CreditDocumentType paramCreditDocumentType, UserInfoDataRequest paramUserInfoDataRequest, String paramString);
  
  public abstract void a(String paramString);
  
  public abstract void a(boolean paramBoolean, String paramString, CreditDocumentType paramCreditDocumentType);
  
  public abstract void b(Bitmap paramBitmap, String paramString);
  
  public abstract void b(String paramString);
  
  public abstract void c(Bitmap paramBitmap, String paramString);
  
  public abstract void c(String paramString);
  
  public abstract void d(Bitmap paramBitmap, String paramString);
  
  public abstract String g();
  
  public abstract void h();
  
  public abstract void i();
  
  public abstract void j();
  
  public abstract void k();
  
  public abstract void l();
  
  public abstract void m();
  
  public abstract void n();
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.views.c.p.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */