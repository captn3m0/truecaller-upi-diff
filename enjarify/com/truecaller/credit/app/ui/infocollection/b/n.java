package com.truecaller.credit.app.ui.infocollection.b;

import android.net.Uri;
import c.g.b.k;
import c.t;
import com.truecaller.bb;
import com.truecaller.credit.R.string;
import com.truecaller.credit.app.a.a.a;
import com.truecaller.credit.app.a.b;
import com.truecaller.credit.app.ui.assist.CreditDocumentType;
import com.truecaller.credit.app.ui.infocollection.views.c.i.b;
import com.truecaller.credit.app.ui.infocollection.views.c.i.d;
import com.truecaller.utils.l;
import java.util.Arrays;

public final class n
  extends bb
  implements i.b
{
  private boolean a;
  private CreditDocumentType c;
  private String d;
  private int e;
  private final String[] f;
  private final l g;
  private final com.truecaller.utils.n h;
  private final b i;
  
  public n(l paraml, com.truecaller.utils.n paramn, b paramb)
  {
    g = paraml;
    h = paramn;
    i = paramb;
    d = "back";
    String[] tmp47_44 = new String[3];
    String[] tmp48_47 = tmp47_44;
    String[] tmp48_47 = tmp47_44;
    tmp48_47[0] = "android.permission.READ_EXTERNAL_STORAGE";
    tmp48_47[1] = "android.permission.WRITE_EXTERNAL_STORAGE";
    tmp48_47[2] = "android.permission.CAMERA";
    paraml = tmp48_47;
    f = paraml;
  }
  
  private final void a(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    Object localObject = "CreditPersonalInfo";
    a.a locala = new com/truecaller/credit/app/a/a$a;
    locala.<init>((String)localObject, (String)localObject);
    localObject = new c.n[5];
    paramString1 = t.a("Status", paramString1);
    localObject[0] = paramString1;
    paramString1 = t.a("Action", paramString2);
    localObject[1] = paramString1;
    paramString1 = t.a("Custom", paramString4);
    localObject[2] = paramString1;
    paramString1 = t.a("Type", paramString3);
    localObject[3] = paramString1;
    paramString1 = t.a("Context", paramString5);
    localObject[4] = paramString1;
    locala.a((c.n[])localObject);
    paramString1 = locala.a();
    a = false;
    paramString2 = i;
    paramString1 = paramString1.b();
    paramString2.a(paramString1);
  }
  
  private final String b(String paramString)
  {
    boolean bool = a;
    if (bool)
    {
      paramString = (CharSequence)paramString;
      if (paramString != null)
      {
        j = paramString.length();
        if (j != 0)
        {
          j = 0;
          paramString = null;
          break label41;
        }
      }
      int j = 1;
      label41:
      if (j != 0) {
        return "qr_resolve_failed";
      }
      return "qr_resolve_success";
    }
    return null;
  }
  
  public final void a()
  {
    i.d locald = (i.d)b;
    if (locald != null)
    {
      int j = e;
      locald.b(j);
    }
    a("failed", null, "selfie", null, "photo_upload");
  }
  
  public final void a(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    k.b(paramArrayOfString, "permissions");
    Object localObject1 = "grantResults";
    k.b(paramArrayOfInt, (String)localObject1);
    int j = 11;
    if (paramInt == j)
    {
      Object localObject2 = g;
      localObject1 = f;
      int k = localObject1.length;
      localObject1 = (String[])Arrays.copyOf((Object[])localObject1, k);
      paramInt = ((l)localObject2).a(paramArrayOfString, paramArrayOfInt, (String[])localObject1);
      if (paramInt != 0)
      {
        localObject2 = c;
        if (localObject2 != null)
        {
          paramArrayOfString = (i.d)b;
          if (paramArrayOfString != null)
          {
            boolean bool = a;
            localObject1 = d;
            paramArrayOfString.a(bool, (String)localObject1, (CreditDocumentType)localObject2);
            return;
          }
        }
        return;
      }
      localObject2 = (i.d)b;
      if (localObject2 != null)
      {
        paramArrayOfString = h;
        int m = R.string.credit_camera_access_denied_error;
        localObject1 = new Object[0];
        paramArrayOfString = paramArrayOfString.a(m, (Object[])localObject1);
        k.a(paramArrayOfString, "resourceProvider.getStri…mera_access_denied_error)");
        ((i.d)localObject2).a(paramArrayOfString);
        int n = e;
        ((i.d)localObject2).b(n);
        return;
      }
    }
  }
  
  public final void a(Uri paramUri, String paramString)
  {
    if (paramUri != null)
    {
      int j = 1;
      e = j;
      CreditDocumentType localCreditDocumentType = c;
      if (localCreditDocumentType != null)
      {
        i.d locald = (i.d)b;
        if (locald != null) {
          locald.a(paramUri, paramString, localCreditDocumentType);
        }
        String str1 = "initiated";
        String str2 = "done";
        paramUri = c;
        if (paramUri != null) {
          paramUri = g;
        } else {
          paramUri = null;
        }
        String str3 = b(paramString);
        a(str1, str2, paramUri, str3, "photo_upload");
        return;
      }
      return;
    }
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "text");
    i.d locald = (i.d)b;
    if (locald != null)
    {
      locald.c(paramString);
      return;
    }
  }
  
  public final void a(boolean paramBoolean, CreditDocumentType paramCreditDocumentType, String paramString)
  {
    k.b(paramCreditDocumentType, "docType");
    k.b(paramString, "cameraType");
    a = paramBoolean;
    c = paramCreditDocumentType;
    d = paramString;
    i.d locald = (i.d)b;
    if (locald != null)
    {
      locald.e();
      l locall = g;
      Object localObject = f;
      int j = localObject.length;
      localObject = (String[])Arrays.copyOf((Object[])localObject, j);
      boolean bool = locall.a((String[])localObject);
      if (bool)
      {
        locald.a(paramBoolean, paramString, paramCreditDocumentType);
        String str = g;
        localObject = this;
        a("shown", null, str, null, "camera_launched");
        return;
      }
      locald.f();
      return;
    }
  }
  
  public final void b()
  {
    int j = e;
    if (j != 0)
    {
      locald = (i.d)b;
      if (locald != null)
      {
        Object localObject = h;
        int k = R.string.upload_in_progress_toast;
        Object[] arrayOfObject = new Object[0];
        localObject = ((com.truecaller.utils.n)localObject).a(k, arrayOfObject);
        k.a(localObject, "resourceProvider.getStri…upload_in_progress_toast)");
        locald.a((String)localObject);
        return;
      }
      return;
    }
    i.d locald = (i.d)b;
    if (locald != null)
    {
      int m = e;
      locald.b(m);
    }
  }
  
  public final void b(Uri paramUri, String paramString)
  {
    k.b(paramUri, "uri");
    int j = 2;
    e = j;
    i.d locald = (i.d)b;
    if (locald != null)
    {
      CreditDocumentType localCreditDocumentType = c;
      locald.a(paramUri, localCreditDocumentType, paramString);
    }
    String str1 = "success";
    paramUri = c;
    if (paramUri != null) {
      paramUri = g;
    } else {
      paramUri = null;
    }
    String str2 = b(paramString);
    a(str1, null, paramUri, str2, "photo_upload");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.b.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */