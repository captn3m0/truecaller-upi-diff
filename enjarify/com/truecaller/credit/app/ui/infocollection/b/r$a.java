package com.truecaller.credit.app.ui.infocollection.b;

import c.d.a.a;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.credit.R.string;
import com.truecaller.credit.app.ui.infocollection.views.c.k.b;
import com.truecaller.credit.data.Result;
import com.truecaller.credit.data.Success;
import com.truecaller.credit.domain.interactors.infocollection.models.IFSCList;
import com.truecaller.utils.n;
import java.util.List;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

final class r$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag f;
  
  r$a(r paramr, String paramString1, String paramString2, String paramString3, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/credit/app/ui/infocollection/b/r$a;
    r localr = b;
    String str1 = c;
    String str2 = d;
    String str3 = e;
    locala.<init>(localr, str1, str2, str3, paramc);
    paramObject = (ag)paramObject;
    f = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    boolean bool1;
    Object localObject2;
    int k;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label337;
      }
      paramObject = r.a(b);
      localObject2 = new com/truecaller/credit/app/ui/infocollection/b/r$a$a;
      ((r.a.a)localObject2).<init>(this, null);
      localObject2 = (m)localObject2;
      k = 1;
      a = k;
      paramObject = g.a((f)paramObject, (m)localObject2, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (Result)paramObject;
    boolean bool2 = paramObject instanceof Success;
    if (bool2)
    {
      paramObject = (Success)paramObject;
      localObject1 = ((IFSCList)((Success)paramObject).getData()).getResults();
      int j = ((List)localObject1).size();
      bool1 = false;
      localObject2 = null;
      if (j > 0)
      {
        localObject1 = r.c(b);
        if (localObject1 != null)
        {
          paramObject = ((IFSCList)((Success)paramObject).getData()).getResults();
          ((k.b)localObject1).a((List)paramObject);
          ((k.b)localObject1).m();
          ((k.b)localObject1).o();
          ((k.b)localObject1).k();
          paramObject = r.d(b);
          k = R.string.credit_button_continue;
          localObject2 = new Object[0];
          paramObject = ((n)paramObject).a(k, (Object[])localObject2);
          localObject2 = "resourceProvider.getStri…g.credit_button_continue)";
          c.g.b.k.a(paramObject, (String)localObject2);
          ((k.b)localObject1).a((String)paramObject);
          ((k.b)localObject1).r();
          ((k.b)localObject1).s();
        }
      }
      else
      {
        paramObject = b;
        localObject1 = r.d((r)paramObject);
        k = R.string.error_no_ifsc;
        localObject2 = new Object[0];
        localObject1 = ((n)localObject1).a(k, (Object[])localObject2);
        localObject2 = "resourceProvider.getString(R.string.error_no_ifsc)";
        c.g.b.k.a(localObject1, (String)localObject2);
        r.a((r)paramObject, (String)localObject1);
      }
    }
    return x.a;
    label337:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.b.r.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */