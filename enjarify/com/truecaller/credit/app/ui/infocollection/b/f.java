package com.truecaller.credit.app.ui.infocollection.b;

import android.net.Uri;
import c.g.b.k;
import com.truecaller.ba;
import com.truecaller.credit.R.color;
import com.truecaller.credit.R.string;
import com.truecaller.credit.app.ui.assist.CreditDocumentType;
import com.truecaller.credit.app.ui.assist.PoaType;
import com.truecaller.credit.app.ui.infocollection.views.c.e.a;
import com.truecaller.credit.app.ui.infocollection.views.c.e.b;
import com.truecaller.credit.data.repository.CreditRepository;
import com.truecaller.credit.domain.interactors.infocollection.models.PoaData;
import com.truecaller.credit.domain.interactors.infocollection.models.PoaImage;
import com.truecaller.utils.n;
import java.util.ArrayList;
import java.util.List;

public final class f
  extends ba
  implements e.a
{
  private PoaData c;
  private final List d;
  private final CreditRepository e;
  private final n f;
  private final c.d.f g;
  private final c.d.f h;
  
  public f(CreditRepository paramCreditRepository, n paramn, c.d.f paramf1, c.d.f paramf2)
  {
    super(paramf1);
    e = paramCreditRepository;
    f = paramn;
    g = paramf1;
    h = paramf2;
    paramCreditRepository = new java/util/ArrayList;
    paramCreditRepository.<init>();
    paramCreditRepository = (List)paramCreditRepository;
    d = paramCreditRepository;
  }
  
  public final void a()
  {
    Object localObject1 = c;
    if (localObject1 != null)
    {
      Object localObject2 = d;
      int i = ((List)localObject2).size();
      int j = 1;
      PoaType localPoaType;
      int k;
      int m;
      String str1;
      Object localObject3;
      int n;
      Object localObject4;
      Object localObject5;
      int i1;
      boolean bool;
      String str2;
      int i2;
      int i4;
      switch (i)
      {
      default: 
        localObject2 = (e.b)b;
        if (localObject2 != null)
        {
          localPoaType = new com/truecaller/credit/app/ui/assist/PoaType;
          k = R.string.credit_poa_camera_description;
          m = R.color.black_50;
          str1 = ((PoaData)localObject1).getType();
          localObject3 = f;
          n = R.string.credit_document_type_front;
          localObject4 = new Object[j];
          localObject1 = ((PoaData)localObject1).getType();
          localObject4[0] = localObject1;
          localObject5 = ((n)localObject3).a(n, (Object[])localObject4);
          k.a(localObject5, "resourceProvider.getStri…ment_type_front, it.type)");
          i1 = R.string.credit_upload_image_default;
          localObject3 = localPoaType;
          localPoaType.<init>(k, m, 3, 4, str1, null, (String)localObject5, i1, 32);
          localObject1 = localPoaType;
          localObject1 = (CreditDocumentType)localPoaType;
          ((e.b)localObject2).a("back", (CreditDocumentType)localObject1);
          return;
        }
        break;
      case 2: 
        localObject2 = (PoaImage)d.get(j);
        bool = ((PoaImage)localObject2).getStatus();
        if (bool)
        {
          localObject2 = (e.b)b;
          if (localObject2 != null)
          {
            localObject5 = new com/truecaller/credit/app/ui/assist/PoaType;
            str2 = null;
            localObject3 = null;
            k = 0;
            String str3 = ((PoaData)localObject1).getType();
            String str4 = ((PoaData)localObject1).getValue();
            i2 = 0;
            int i3 = R.string.credit_upload_image_default;
            i4 = 79;
            localObject4 = localObject5;
            ((PoaType)localObject5).<init>(0, 0, 0, 0, str3, str4, null, i3, i4);
            localObject5 = (CreditDocumentType)localObject5;
            ((e.b)localObject2).a((CreditDocumentType)localObject5);
          }
          return;
        }
        localObject2 = (e.b)b;
        if (localObject2 != null)
        {
          localPoaType = new com/truecaller/credit/app/ui/assist/PoaType;
          k = R.string.credit_poa_camera_description;
          m = R.color.black_50;
          str1 = ((PoaData)localObject1).getType();
          localObject3 = f;
          n = R.string.credit_document_type_back;
          localObject4 = new Object[j];
          localObject1 = ((PoaData)localObject1).getType();
          localObject4[0] = localObject1;
          localObject5 = ((n)localObject3).a(n, (Object[])localObject4);
          k.a(localObject5, "resourceProvider.getStri…_type_back, poaData.type)");
          i1 = R.string.credit_upload_image_default;
          localObject3 = localPoaType;
          localPoaType.<init>(k, m, 3, 4, str1, null, (String)localObject5, i1, 32);
          localObject1 = localPoaType;
          localObject1 = (CreditDocumentType)localPoaType;
          ((e.b)localObject2).a("back", (CreditDocumentType)localObject1);
          return;
        }
        return;
      case 1: 
        localObject2 = (PoaImage)d.get(0);
        bool = ((PoaImage)localObject2).getStatus();
        if (bool)
        {
          localObject2 = (e.b)b;
          if (localObject2 != null)
          {
            str2 = "back";
            localPoaType = new com/truecaller/credit/app/ui/assist/PoaType;
            k = R.string.credit_poa_camera_description;
            m = R.color.black_50;
            int i5 = 3;
            i2 = 4;
            str1 = ((PoaData)localObject1).getType();
            i4 = 0;
            localObject3 = f;
            n = R.string.credit_document_type_back;
            localObject4 = new Object[j];
            localObject1 = ((PoaData)localObject1).getType();
            localObject4[0] = localObject1;
            localObject5 = ((n)localObject3).a(n, (Object[])localObject4);
            k.a(localObject5, "resourceProvider.getStri…_type_back, poaData.type)");
            i1 = R.string.credit_upload_image_default;
            int i6 = 32;
            localObject3 = localPoaType;
            localPoaType.<init>(k, m, i5, i2, str1, null, (String)localObject5, i1, i6);
            localObject1 = localPoaType;
            localObject1 = (CreditDocumentType)localPoaType;
            ((e.b)localObject2).a(str2, (CreditDocumentType)localObject1);
          }
          return;
        }
        localObject2 = (e.b)b;
        if (localObject2 != null)
        {
          localPoaType = new com/truecaller/credit/app/ui/assist/PoaType;
          k = R.string.credit_poa_camera_description;
          m = R.color.black_50;
          str1 = ((PoaData)localObject1).getType();
          localObject3 = f;
          n = R.string.credit_document_type_front;
          localObject4 = new Object[j];
          localObject1 = ((PoaData)localObject1).getType();
          localObject4[0] = localObject1;
          localObject5 = ((n)localObject3).a(n, (Object[])localObject4);
          k.a(localObject5, "resourceProvider.getStri…type_front, poaData.type)");
          i1 = R.string.credit_upload_image_default;
          localObject3 = localPoaType;
          localPoaType.<init>(k, m, 3, 4, str1, null, (String)localObject5, i1, 32);
          localObject1 = localPoaType;
          localObject1 = (CreditDocumentType)localPoaType;
          ((e.b)localObject2).a("back", (CreditDocumentType)localObject1);
          return;
        }
        return;
      }
      return;
    }
  }
  
  public final void a(int paramInt1, int paramInt2, Uri paramUri)
  {
    int i = 13;
    if (paramInt1 == i)
    {
      paramInt1 = -1;
      i = 1;
      List localList1 = null;
      Object localObject3;
      if (paramInt2 == paramInt1)
      {
        if (paramUri != null)
        {
          localObject1 = d;
          localObject2 = new com/truecaller/credit/domain/interactors/infocollection/models/PoaImage;
          ((PoaImage)localObject2).<init>(paramUri, i);
          ((List)localObject1).add(localObject2);
          localObject1 = new java/util/ArrayList;
          ((ArrayList)localObject1).<init>();
          localObject1 = (List)localObject1;
          localObject2 = c;
          if (localObject2 != null)
          {
            paramUri = new com/truecaller/credit/domain/interactors/infocollection/models/PoaData;
            localObject3 = ((PoaData)localObject2).getType();
            localObject2 = ((PoaData)localObject2).getValue();
            List localList2 = d;
            paramUri.<init>((String)localObject3, (String)localObject2, localList2);
            ((List)localObject1).add(paramUri);
          }
          localObject2 = (e.b)b;
          if (localObject2 != null) {
            ((e.b)localObject2).a((List)localObject1);
          }
          localObject1 = d;
          paramInt1 = ((List)localObject1).size();
          switch (paramInt1)
          {
          default: 
            break;
          case 2: 
            localObject1 = (e.b)b;
            if (localObject1 != null)
            {
              localObject2 = f;
              j = R.string.credit_button_continue;
              localObject3 = new Object[0];
              localObject2 = ((n)localObject2).a(j, (Object[])localObject3);
              paramUri = "resourceProvider.getStri…g.credit_button_continue)";
              k.a(localObject2, paramUri);
              ((e.b)localObject1).b((String)localObject2);
            }
            return;
          case 1: 
            localObject1 = (e.b)b;
            if (localObject1 != null)
            {
              localObject2 = f;
              j = R.string.credit_back_capture_button;
              localObject3 = new Object[0];
              localObject2 = ((n)localObject2).a(j, (Object[])localObject3);
              k.a(localObject2, "resourceProvider.getStri…edit_back_capture_button)");
              ((e.b)localObject1).b((String)localObject2);
              return;
            }
            break;
          }
        }
        return;
      }
      Object localObject1 = d;
      Object localObject2 = new com/truecaller/credit/domain/interactors/infocollection/models/PoaImage;
      int j = 0;
      paramUri = null;
      ((PoaImage)localObject2).<init>(null, false, i, null);
      ((List)localObject1).add(localObject2);
      localObject1 = new java/util/ArrayList;
      ((ArrayList)localObject1).<init>();
      localObject1 = (List)localObject1;
      localObject2 = c;
      if (localObject2 != null)
      {
        paramUri = new com/truecaller/credit/domain/interactors/infocollection/models/PoaData;
        localObject3 = ((PoaData)localObject2).getType();
        localObject2 = ((PoaData)localObject2).getValue();
        localList1 = d;
        paramUri.<init>((String)localObject3, (String)localObject2, localList1);
        ((List)localObject1).add(paramUri);
        return;
      }
    }
  }
  
  public final void a(PoaData paramPoaData)
  {
    Object localObject = "poaData";
    k.b(paramPoaData, (String)localObject);
    c = paramPoaData;
    paramPoaData = (e.b)b;
    if (paramPoaData != null)
    {
      localObject = f;
      int i = R.string.credit_button_continue;
      Object[] arrayOfObject = new Object[0];
      localObject = ((n)localObject).a(i, arrayOfObject);
      k.a(localObject, "resourceProvider.getStri…g.credit_button_continue)");
      paramPoaData.b((String)localObject);
      paramPoaData.h();
      paramPoaData.g();
      return;
    }
  }
  
  public final void y_()
  {
    super.y_();
    d.clear();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.b.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */