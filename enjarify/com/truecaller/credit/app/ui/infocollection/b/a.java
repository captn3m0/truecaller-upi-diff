package com.truecaller.credit.app.ui.infocollection.b;

import c.d.f;
import c.g.a.m;
import com.truecaller.ba;
import com.truecaller.credit.app.ui.infocollection.views.c.a.b;
import com.truecaller.credit.data.models.APIStatusMessage;
import kotlinx.coroutines.e;

public final class a
  extends ba
  implements com.truecaller.credit.app.ui.infocollection.views.c.a.a
{
  private APIStatusMessage c;
  private final f d;
  
  public a(f paramf)
  {
    super(paramf);
    d = paramf;
  }
  
  public final void a()
  {
    a.b localb = (a.b)b;
    if (localb != null)
    {
      localb.d();
      return;
    }
  }
  
  public final void a(APIStatusMessage paramAPIStatusMessage)
  {
    if (paramAPIStatusMessage != null)
    {
      c = paramAPIStatusMessage;
      int i = paramAPIStatusMessage.getStatus();
      a.b localb;
      switch (i)
      {
      default: 
        break;
      case 3: 
        localb = (a.b)b;
        if (localb != null)
        {
          String str = paramAPIStatusMessage.getTitle();
          localb.c(str);
          str = paramAPIStatusMessage.getMessage();
          localb.d(str);
          str = paramAPIStatusMessage.getActionButtonText();
          localb.e(str);
          localb.j();
          localb.i();
          localb.g();
          localb.o();
          boolean bool = paramAPIStatusMessage.getShouldRedirectToHomeScreen();
          if (bool)
          {
            localb.l();
            return;
          }
          localb.m();
          return;
        }
        break;
      case 2: 
        localb = (a.b)b;
        if (localb != null)
        {
          paramAPIStatusMessage = paramAPIStatusMessage.getTitle();
          localb.b(paramAPIStatusMessage);
          localb.h();
          localb.g();
          localb.k();
          localb.m();
          localb.o();
        }
        paramAPIStatusMessage = new com/truecaller/credit/app/ui/infocollection/b/a$a;
        paramAPIStatusMessage.<init>(this, null);
        paramAPIStatusMessage = (m)paramAPIStatusMessage;
        e.b(this, null, paramAPIStatusMessage, 3);
        return;
      case 1: 
        localb = (a.b)b;
        if (localb != null)
        {
          paramAPIStatusMessage = paramAPIStatusMessage.getTitle();
          localb.a(paramAPIStatusMessage);
          localb.f();
          localb.i();
          localb.k();
          localb.m();
          localb.n();
        }
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */