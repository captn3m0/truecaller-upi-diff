package com.truecaller.credit.app.ui.infocollection.b;

import c.d.f;
import c.g.a.m;
import c.g.b.k;
import c.t;
import com.truecaller.ba;
import com.truecaller.credit.R.id;
import com.truecaller.credit.R.string;
import com.truecaller.credit.app.a.a.a;
import com.truecaller.credit.app.a.b;
import com.truecaller.credit.app.ui.infocollection.views.c.r.b;
import com.truecaller.credit.app.ui.infocollection.views.c.r.c;
import com.truecaller.credit.data.models.APIStatusMessage;
import com.truecaller.credit.data.models.Address;
import com.truecaller.credit.data.models.UserInfoDataRequestKt;
import com.truecaller.credit.data.repository.CreditRepository;
import kotlinx.coroutines.e;

public final class ad
  extends ba
  implements r.b
{
  private Address c;
  private String d;
  private String e;
  private boolean f;
  private final f g;
  private final f h;
  private final CreditRepository i;
  private final com.truecaller.utils.n j;
  private final b k;
  
  public ad(f paramf1, f paramf2, CreditRepository paramCreditRepository, com.truecaller.utils.n paramn, b paramb)
  {
    super(paramf1);
    g = paramf1;
    h = paramf2;
    i = paramCreditRepository;
    j = paramn;
    k = paramb;
  }
  
  private final void a(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    Object localObject = "CreditScheduleVisit";
    a.a locala = new com/truecaller/credit/app/a/a$a;
    locala.<init>((String)localObject, (String)localObject);
    localObject = new c.n[4];
    paramString1 = t.a("Status", paramString1);
    localObject[0] = paramString1;
    paramString1 = t.a("Action", paramString2);
    localObject[1] = paramString1;
    paramString1 = t.a("Custom", paramString3);
    localObject[2] = paramString1;
    paramString1 = t.a("Context", paramString4);
    localObject[3] = paramString1;
    locala.a((c.n[])localObject);
    paramString1 = locala.a();
    a = false;
    paramString2 = k;
    paramString1 = paramString1.b();
    paramString2.a(paramString1);
  }
  
  public final void a()
  {
    boolean bool = f;
    Object localObject1;
    if (bool) {
      localObject1 = "address_changed";
    } else {
      localObject1 = "same_address";
    }
    Object localObject2 = (CharSequence)d;
    int m = 1;
    Object[] arrayOfObject = null;
    if (localObject2 != null)
    {
      n = ((CharSequence)localObject2).length();
      if (n != 0)
      {
        n = 0;
        localObject2 = null;
        break label63;
      }
    }
    int n = 1;
    label63:
    if (n == 0)
    {
      localObject2 = (CharSequence)e;
      Object localObject3;
      if (localObject2 != null)
      {
        n = ((CharSequence)localObject2).length();
        if (n != 0)
        {
          m = 0;
          localObject3 = null;
        }
      }
      if (m == 0)
      {
        localObject2 = "initiated";
        localObject3 = "continue";
        Object localObject4 = (r.c)b;
        int i1;
        if (localObject4 != null)
        {
          localObject4 = ((r.c)localObject4).g();
        }
        else
        {
          i1 = 0;
          localObject4 = null;
        }
        a((String)localObject2, (String)localObject3, (String)localObject1, (String)localObject4);
        localObject1 = (r.c)b;
        if (localObject1 != null)
        {
          localObject2 = new com/truecaller/credit/data/models/APIStatusMessage;
          int i2 = 1;
          localObject3 = j;
          i1 = R.string.credit_scheduling_a_meeting;
          arrayOfObject = new Object[0];
          String str = ((com.truecaller.utils.n)localObject3).a(i1, arrayOfObject);
          localObject3 = "resourceProvider.getStri…dit_scheduling_a_meeting)";
          k.a(str, (String)localObject3);
          int i3 = 28;
          ((APIStatusMessage)localObject2).<init>(i2, str, null, false, null, i3, null);
          ((r.c)localObject1).a((APIStatusMessage)localObject2);
        }
        localObject1 = new com/truecaller/credit/app/ui/infocollection/b/ad$a;
        ((ad.a)localObject1).<init>(this, null);
        localObject1 = (m)localObject1;
        e.b(this, null, (m)localObject1, 3);
        return;
      }
    }
  }
  
  public final void a(Address paramAddress, boolean paramBoolean)
  {
    f = paramBoolean;
    if (paramAddress != null)
    {
      c = paramAddress;
      Object localObject1 = paramAddress.getPincode();
      Object localObject2 = null;
      d = null;
      e = null;
      Object localObject3 = (r.c)b;
      if (localObject3 != null)
      {
        ((r.c)localObject3).j();
        ((r.c)localObject3).m();
        ((r.c)localObject3).p();
      }
      localObject3 = new com/truecaller/credit/app/ui/infocollection/b/ad$c;
      ((ad.c)localObject3).<init>(this, (String)localObject1, null);
      localObject3 = (m)localObject3;
      paramBoolean = true;
      e.b(this, null, (m)localObject3, paramBoolean);
      localObject1 = (r.c)b;
      if (localObject1 != null)
      {
        localObject2 = UserInfoDataRequestKt.flattenToString(paramAddress);
        ((r.c)localObject1).c((String)localObject2);
        paramAddress = paramAddress.getAddress_type();
        boolean bool = "kyc".equals(paramAddress);
        localObject2 = null;
        int m;
        if (bool)
        {
          paramAddress = j;
          m = R.string.schedule_change_address;
          localObject2 = new Object[0];
          paramAddress = paramAddress.a(m, (Object[])localObject2);
        }
        else
        {
          paramAddress = j;
          m = R.string.schedule_add_office_address;
          localObject2 = new Object[0];
          paramAddress = paramAddress.a(m, (Object[])localObject2);
        }
        k.a(paramAddress, "if (ADDRESS_TYPE_KYC.equ…ddress)\n                }");
        ((r.c)localObject1).b(paramAddress);
        return;
      }
      return;
    }
  }
  
  public final void a(Integer paramInteger)
  {
    int m = R.id.btnAddressChange;
    if (paramInteger == null) {
      return;
    }
    int n = paramInteger.intValue();
    if (n == m)
    {
      paramInteger = "kyc";
      Object localObject = c;
      Address localAddress = null;
      if (localObject != null)
      {
        localObject = ((Address)localObject).getAddress_type();
      }
      else
      {
        m = 0;
        localObject = null;
      }
      boolean bool = paramInteger.equals(localObject);
      if (bool) {
        localAddress = c;
      }
      paramInteger = (r.c)b;
      if (paramInteger != null)
      {
        paramInteger.a("kyc", localAddress);
        return;
      }
    }
  }
  
  public final void a(String paramString1, String paramString2)
  {
    d = paramString1;
    e = paramString2;
    paramString1 = (CharSequence)paramString1;
    int m = 0;
    if (paramString1 != null)
    {
      n = paramString1.length();
      if (n != 0)
      {
        n = 0;
        paramString1 = null;
        break label48;
      }
    }
    int n = 1;
    label48:
    if (n == 0)
    {
      paramString2 = (CharSequence)paramString2;
      if (paramString2 != null)
      {
        n = paramString2.length();
        if (n != 0) {}
      }
      else
      {
        m = 1;
      }
      if (m == 0)
      {
        paramString1 = (r.c)b;
        if (paramString1 != null)
        {
          paramString1.i();
          return;
        }
        return;
      }
    }
    paramString1 = (r.c)b;
    if (paramString1 != null) {
      paramString1.j();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.b.ad
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */