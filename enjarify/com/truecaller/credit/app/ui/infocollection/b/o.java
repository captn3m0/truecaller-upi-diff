package com.truecaller.credit.app.ui.infocollection.b;

import dagger.a.d;
import javax.inject.Provider;

public final class o
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  
  private o(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
  }
  
  public static o a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    o localo = new com/truecaller/credit/app/ui/infocollection/b/o;
    localo.<init>(paramProvider1, paramProvider2, paramProvider3);
    return localo;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.b.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */