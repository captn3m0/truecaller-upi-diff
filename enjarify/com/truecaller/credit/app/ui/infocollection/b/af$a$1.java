package com.truecaller.credit.app.ui.infocollection.b;

import android.graphics.Bitmap;
import android.net.Uri;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.credit.app.util.s;
import java.lang.ref.WeakReference;
import kotlinx.coroutines.ag;

final class af$a$1
  extends c.d.b.a.k
  implements m
{
  Object a;
  int b;
  private ag d;
  
  af$a$1(af.a parama, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    1 local1 = new com/truecaller/credit/app/ui/infocollection/b/af$a$1;
    af.a locala = c;
    local1.<init>(locala, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return local1;
  }
  
  public final Object a(Object paramObject)
  {
    a locala = a.a;
    int i = b;
    boolean bool1;
    Object localObject;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 2: 
      boolean bool2 = paramObject instanceof o.b;
      if (!bool2) {
        break label207;
      }
      throw a;
    case 1: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        throw a;
      }
      break;
    case 0: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label214;
      }
      paramObject = af.b(c.b);
      localObject = c.c;
      int j = 1;
      b = j;
      paramObject = ((s)paramObject).a((Uri)localObject);
      if (paramObject == locala) {
        return locala;
      }
      break;
    }
    paramObject = (Bitmap)paramObject;
    if (paramObject != null)
    {
      localObject = af.b(c.b);
      WeakReference localWeakReference = new java/lang/ref/WeakReference;
      localWeakReference.<init>(paramObject);
      a = paramObject;
      int k = 2;
      b = k;
      paramObject = ((s)localObject).a(localWeakReference);
      if (paramObject == locala) {
        return locala;
      }
      label207:
      return (WeakReference)paramObject;
    }
    return null;
    label214:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (1)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((1)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.b.af.a.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */