package com.truecaller.credit.app.ui.infocollection.b;

import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.credit.data.models.IFSCSearchRequest;
import com.truecaller.credit.data.repository.CreditRepository;
import kotlinx.coroutines.ag;

final class h$c
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  h$c(h paramh, String paramString, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    c localc = new com/truecaller/credit/app/ui/infocollection/b/h$c;
    h localh = b;
    String str = c;
    localc.<init>(localh, str, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return localc;
  }
  
  public final Object a(Object paramObject)
  {
    a locala = a.a;
    int i = a;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label129;
      }
      paramObject = h.a(b);
      IFSCSearchRequest localIFSCSearchRequest = new com/truecaller/credit/data/models/IFSCSearchRequest;
      String str = c;
      localIFSCSearchRequest.<init>(str);
      int j = 1;
      a = j;
      paramObject = ((CreditRepository)paramObject).fetchIFSCSearchResults(localIFSCSearchRequest, this);
      if (paramObject == locala) {
        return locala;
      }
      break;
    }
    return paramObject;
    label129:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (c)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((c)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.b.h.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */