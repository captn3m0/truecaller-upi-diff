package com.truecaller.credit.app.ui.infocollection.b;

import android.net.Uri;
import c.d.a.a;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.credit.app.ui.assist.CreditDocumentType;
import com.truecaller.credit.app.ui.infocollection.views.c.j.b;
import com.truecaller.credit.data.Failure;
import com.truecaller.credit.data.Result;
import com.truecaller.credit.data.Success;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

final class p$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag g;
  
  p$a(j.b paramb, CreditDocumentType paramCreditDocumentType, Uri paramUri, String paramString, c paramc, p paramp)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/credit/app/ui/infocollection/b/p$a;
    j.b localb = b;
    CreditDocumentType localCreditDocumentType = c;
    Uri localUri = d;
    String str = e;
    p localp = f;
    locala.<init>(localb, localCreditDocumentType, localUri, str, paramc, localp);
    paramObject = (ag)paramObject;
    g = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    Object localObject2;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label196;
      }
      paramObject = p.a(f);
      localObject2 = new com/truecaller/credit/app/ui/infocollection/b/p$a$1;
      ((p.a.1)localObject2).<init>(this, null);
      localObject2 = (m)localObject2;
      int j = 1;
      a = j;
      paramObject = g.a((f)paramObject, (m)localObject2, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (Result)paramObject;
    boolean bool2 = paramObject instanceof Success;
    if (bool2)
    {
      paramObject = b;
      localObject1 = d;
      localObject2 = e;
      ((j.b)paramObject).a((Uri)localObject1, (String)localObject2);
    }
    else
    {
      boolean bool3 = paramObject instanceof Failure;
      if (bool3)
      {
        paramObject = b;
        ((j.b)paramObject).k();
      }
    }
    return x.a;
    label196:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.b.p.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */