package com.truecaller.credit.app.ui.infocollection.b;

import dagger.a.d;
import javax.inject.Provider;

public final class g
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  
  private g(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
    d = paramProvider4;
  }
  
  public static g a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    g localg = new com/truecaller/credit/app/ui/infocollection/b/g;
    localg.<init>(paramProvider1, paramProvider2, paramProvider3, paramProvider4);
    return localg;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.b.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */