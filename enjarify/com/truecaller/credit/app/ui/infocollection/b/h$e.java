package com.truecaller.credit.app.ui.infocollection.b;

import c.d.a.a;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.credit.R.string;
import com.truecaller.credit.app.ui.infocollection.views.c.f.b;
import com.truecaller.credit.data.Failure;
import com.truecaller.credit.data.Result;
import com.truecaller.credit.data.Success;
import com.truecaller.credit.data.models.BankAccountDetails;
import com.truecaller.utils.n;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

final class h$e
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag e;
  
  h$e(h paramh, String paramString1, String paramString2, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    e locale = new com/truecaller/credit/app/ui/infocollection/b/h$e;
    h localh = b;
    String str1 = c;
    String str2 = d;
    locale.<init>(localh, str1, str2, paramc);
    paramObject = (ag)paramObject;
    e = ((ag)paramObject);
    return locale;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    String str = null;
    int j = 1;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label439;
      }
      paramObject = h.d(b);
      localObject2 = new com/truecaller/credit/app/ui/infocollection/b/h$e$a;
      ((h.e.a)localObject2).<init>(this, null);
      localObject2 = (m)localObject2;
      a = j;
      paramObject = g.a((f)paramObject, (m)localObject2, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (Result)paramObject;
    boolean bool2 = paramObject instanceof Success;
    boolean bool1 = false;
    Object localObject2 = null;
    if (bool2)
    {
      paramObject = h.b(b);
      if (paramObject != null) {
        ((f.b)paramObject).q();
      }
      paramObject = b;
      e = j;
      paramObject = h.b((h)paramObject);
      if (paramObject != null)
      {
        localObject1 = h.c(b);
        int m = R.string.text_your_bank_account_has_been_verified;
        Object[] arrayOfObject = new Object[j];
        BankAccountDetails localBankAccountDetails = h.e(b);
        if (localBankAccountDetails != null) {
          str = localBankAccountDetails.getName();
        }
        arrayOfObject[0] = str;
        localObject1 = ((n)localObject1).a(m, arrayOfObject);
        localObject2 = "resourceProvider.getStri…bankAccountDetails?.name)";
        c.g.b.k.a(localObject1, (String)localObject2);
        ((f.b)paramObject).g((String)localObject1);
      }
      paramObject = b;
      localObject1 = "success";
      bool1 = h.g((h)paramObject);
      if (bool1) {
        localObject2 = "ifsc_searched";
      } else {
        localObject2 = "ifsc_entered";
      }
      h.a((h)paramObject, (String)localObject1, (String)localObject2);
    }
    else
    {
      bool2 = paramObject instanceof Failure;
      if (bool2)
      {
        b.e = false;
        paramObject = (Failure)paramObject;
        localObject1 = (CharSequence)((Failure)paramObject).getMessage();
        if (localObject1 != null)
        {
          int k = ((CharSequence)localObject1).length();
          if (k != 0) {}
        }
        else
        {
          bool1 = true;
        }
        if (!bool1)
        {
          localObject1 = h.b(b);
          if (localObject1 != null)
          {
            paramObject = ((Failure)paramObject).getMessage();
            ((f.b)localObject1).k((String)paramObject);
          }
        }
        paramObject = h.b(b);
        if (paramObject != null) {
          ((f.b)paramObject).q();
        }
        paramObject = b;
        localObject1 = "failure";
        bool1 = h.g((h)paramObject);
        if (bool1) {
          localObject2 = "ifsc_searched";
        } else {
          localObject2 = "ifsc_entered";
        }
        h.a((h)paramObject, (String)localObject1, (String)localObject2);
      }
    }
    return x.a;
    label439:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (e)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((e)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.b.h.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */