package com.truecaller.credit.app.ui.infocollection.b;

import c.d.a.a;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.credit.R.string;
import com.truecaller.credit.app.ui.infocollection.views.c.g.b;
import com.truecaller.credit.data.Failure;
import com.truecaller.credit.data.Result;
import com.truecaller.credit.data.Success;
import com.truecaller.credit.data.models.APIStatusMessage;
import com.truecaller.credit.data.models.UserInfoDataRequest;
import com.truecaller.utils.n;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

final class j$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  j$a(UserInfoDataRequest paramUserInfoDataRequest, c paramc, j paramj)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/credit/app/ui/infocollection/b/j$a;
    UserInfoDataRequest localUserInfoDataRequest = b;
    j localj = c;
    locala.<init>(localUserInfoDataRequest, paramc, localj);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    String str1 = null;
    Object localObject2;
    Object localObject3;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label379;
      }
      paramObject = j.a(c);
      if (paramObject != null)
      {
        localObject2 = new com/truecaller/credit/data/models/APIStatusMessage;
        int j = 1;
        localObject3 = j.b(c);
        int k = R.string.credit_saving_address_info;
        Object[] arrayOfObject = new Object[0];
        String str2 = ((n)localObject3).a(k, arrayOfObject);
        c.g.b.k.a(str2, "resourceProvider.getStri…edit_saving_address_info)");
        arrayOfObject = null;
        int m = 28;
        localObject3 = localObject2;
        ((APIStatusMessage)localObject2).<init>(j, str2, null, false, null, m, null);
        ((g.b)paramObject).a((APIStatusMessage)localObject2);
      }
      paramObject = j.c(c);
      localObject2 = new com/truecaller/credit/app/ui/infocollection/b/j$a$1;
      ((j.a.1)localObject2).<init>(this, null);
      localObject2 = (m)localObject2;
      int n = 1;
      a = n;
      paramObject = g.a((f)paramObject, (m)localObject2, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (Result)paramObject;
    boolean bool2 = paramObject instanceof Success;
    boolean bool3;
    if (bool2)
    {
      paramObject = c;
      bool3 = j.e((j)paramObject);
      if (bool3) {
        paramObject = "current_address_same";
      } else {
        paramObject = "current_address_changed";
      }
      localObject1 = c;
      localObject2 = "confirmed";
      localObject3 = "continue";
      g.b localb = j.a((j)localObject1);
      if (localb != null) {
        str1 = localb.i();
      }
      j.a((j)localObject1, (String)localObject2, (String)localObject3, (String)paramObject, str1);
      paramObject = j.a(c);
      if (paramObject != null)
      {
        ((g.b)paramObject).n();
        localObject1 = "address_confirmation";
        ((g.b)paramObject).b((String)localObject1);
        ((g.b)paramObject).m();
      }
    }
    else
    {
      bool3 = paramObject instanceof Failure;
      if (bool3)
      {
        paramObject = j.a(c);
        if (paramObject != null) {
          ((g.b)paramObject).n();
        }
      }
    }
    return x.a;
    label379:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.b.j.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */