package com.truecaller.credit.app.ui.infocollection.b;

import c.d.a.a;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.credit.app.ui.infocollection.views.c.r.c;
import com.truecaller.credit.data.Failure;
import com.truecaller.credit.data.Result;
import com.truecaller.credit.data.Success;
import com.truecaller.credit.data.models.Address;
import com.truecaller.credit.data.models.BookSlotRequest;
import com.truecaller.credit.data.models.UserInfoDataRequestKt;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

final class ad$a
  extends c.d.b.a.k
  implements m
{
  Object a;
  int b;
  private ag d;
  
  ad$a(ad paramad, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/credit/app/ui/infocollection/b/ad$a;
    ad localad = c;
    locala.<init>(localad, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = b;
    Object localObject2 = null;
    Object localObject3;
    Object localObject4;
    int j;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label382;
      }
      paramObject = new com/truecaller/credit/data/models/BookSlotRequest;
      localObject3 = String.valueOf(ad.d(c));
      localObject4 = String.valueOf(ad.e(c));
      ((BookSlotRequest)paramObject).<init>((String)localObject3, (String)localObject4);
      localObject3 = ad.b(c);
      localObject4 = new com/truecaller/credit/app/ui/infocollection/b/ad$a$a;
      ((ad.a.a)localObject4).<init>(this, (BookSlotRequest)paramObject, null);
      localObject4 = (m)localObject4;
      a = paramObject;
      j = 1;
      b = j;
      paramObject = g.a((f)localObject3, (m)localObject4, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (Result)paramObject;
    boolean bool2 = paramObject instanceof Success;
    if (bool2)
    {
      paramObject = ad.a(c);
      if (paramObject != null)
      {
        localObject1 = c;
        localObject3 = "success";
        localObject4 = ad.a((ad)localObject1);
        if (localObject4 != null) {
          localObject2 = ((r.c)localObject4).g();
        }
        ad.a((ad)localObject1, (String)localObject3, (String)localObject2);
        ((r.c)paramObject).r();
        localObject1 = String.valueOf(ad.d(c));
        localObject3 = String.valueOf(ad.e(c));
        localObject2 = ad.f(c);
        if (localObject2 != null)
        {
          localObject2 = UserInfoDataRequestKt.flattenToString((Address)localObject2);
          if (localObject2 != null) {}
        }
        else
        {
          localObject2 = "";
        }
        ((r.c)paramObject).a((String)localObject1, (String)localObject3, (String)localObject2);
      }
    }
    else
    {
      j = paramObject instanceof Failure;
      if (j != 0)
      {
        paramObject = ad.a(c);
        if (paramObject != null)
        {
          localObject1 = c;
          localObject3 = "failure";
          localObject4 = ad.a((ad)localObject1);
          if (localObject4 != null) {
            localObject2 = ((r.c)localObject4).g();
          }
          ad.a((ad)localObject1, (String)localObject3, (String)localObject2);
          ((r.c)paramObject).r();
        }
      }
    }
    return x.a;
    label382:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.b.ad.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */