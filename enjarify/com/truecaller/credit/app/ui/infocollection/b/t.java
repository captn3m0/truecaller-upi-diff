package com.truecaller.credit.app.ui.infocollection.b;

import c.g.b.k;
import com.truecaller.bb;
import com.truecaller.credit.R.dimen;
import com.truecaller.credit.R.drawable;
import com.truecaller.credit.app.a.a.a;
import com.truecaller.credit.app.a.b;
import com.truecaller.credit.app.core.g;
import com.truecaller.credit.app.ui.infocollection.views.c.l.a;
import com.truecaller.credit.app.ui.infocollection.views.c.l.b;

public final class t
  extends bb
  implements l.a
{
  private String a;
  private int c;
  private final com.truecaller.utils.n d;
  private final g e;
  private final b f;
  
  public t(com.truecaller.utils.n paramn, g paramg, b paramb)
  {
    d = paramn;
    e = paramg;
    f = paramb;
    a = "";
    c = -1;
  }
  
  private final void a(String paramString1, String paramString2, String paramString3)
  {
    Object localObject1 = new com/truecaller/credit/app/a/a$a;
    ((a.a)localObject1).<init>(paramString1, paramString1);
    int i = 3;
    paramString1 = new c.n[i];
    paramString3 = c.t.a("Status", paramString3);
    paramString1[0] = paramString3;
    paramString3 = "Type";
    paramString2 = c.t.a(paramString3, paramString2);
    int j = 1;
    paramString1[j] = paramString2;
    paramString2 = "Context";
    Object localObject2 = e;
    String str = "credit_next_page";
    localObject2 = (CharSequence)((g)localObject2).a(str);
    if (localObject2 != null)
    {
      k = ((CharSequence)localObject2).length();
      if (k != 0)
      {
        k = 0;
        localObject2 = null;
        break label110;
      }
    }
    int k = 1;
    label110:
    int m = 2;
    if (k != 0)
    {
      localObject2 = (l.b)b;
      if (localObject2 != null)
      {
        localObject2 = ((l.b)localObject2).m();
      }
      else
      {
        k = 0;
        localObject2 = null;
      }
    }
    else
    {
      localObject2 = "schedule_meeting";
    }
    paramString2 = c.t.a(paramString2, localObject2);
    paramString1[m] = paramString2;
    ((a.a)localObject1).a(paramString1);
    paramString1 = ((a.a)localObject1).a();
    a = false;
    paramString2 = e;
    localObject1 = "credit_next_page";
    paramString2 = (CharSequence)paramString2.a((String)localObject1);
    if (paramString2 != null)
    {
      int n = paramString2.length();
      if (n != 0)
      {
        j = 0;
        paramString3 = null;
      }
    }
    if (j == 0)
    {
      paramString3 = "reset";
      paramString2 = c.t.a("Custom", paramString3);
      paramString1.a(paramString2);
    }
    paramString2 = f;
    paramString1 = paramString1.b();
    paramString2.a(paramString1);
  }
  
  private final void i()
  {
    a("CreditScheduleVisit", null, "shown");
  }
  
  private final void j()
  {
    a("CreditBankDetails", null, "shown");
  }
  
  private final void k()
  {
    a("CreditPersonalInfo", "address_verification", "shown");
  }
  
  private final void l()
  {
    a("CreditPersonalInfo", "selfie", "shown");
  }
  
  public final void a()
  {
    l.b localb = (l.b)b;
    if (localb != null)
    {
      localb.l();
      return;
    }
  }
  
  public final void a(int paramInt1, int paramInt2)
  {
    int i = c;
    int j = -1;
    if (i == j) {
      c = paramInt1;
    }
    paramInt1 = c + paramInt2;
    if (paramInt1 == 0)
    {
      localb = (l.b)b;
      if (localb != null)
      {
        String str = a;
        localb.a(str);
      }
      return;
    }
    l.b localb = (l.b)b;
    if (localb != null)
    {
      localb.k();
      return;
    }
  }
  
  public final void a(String paramString)
  {
    Object localObject = (l.b)b;
    if (localObject != null)
    {
      String str = "deeplink";
      ((l.b)localObject).d(str);
    }
    if (paramString != null)
    {
      int i = paramString.hashCode();
      boolean bool;
      switch (i)
      {
      default: 
        break;
      case 1355283603: 
        localObject = "schedule_meeting";
        bool = paramString.equals(localObject);
        if (bool)
        {
          i();
          paramString = (l.b)b;
          if (paramString != null) {
            paramString.v();
          }
          return;
        }
        break;
      case 339204258: 
        localObject = "user_info";
        bool = paramString.equals(localObject);
        if (bool)
        {
          l();
          paramString = (l.b)b;
          if (paramString != null) {
            paramString.r();
          }
          return;
        }
        break;
      case -865750906: 
        localObject = "address_verification";
        bool = paramString.equals(localObject);
        if (bool)
        {
          k();
          paramString = (l.b)b;
          if (paramString != null) {
            paramString.s();
          }
          return;
        }
        break;
      case -1537590693: 
        localObject = "meeting_confirmed";
        bool = paramString.equals(localObject);
        if (bool)
        {
          paramString = (l.b)b;
          if (paramString != null) {
            paramString.u();
          }
          return;
        }
        break;
      case -1577163104: 
        localObject = "address_confirmation";
        bool = paramString.equals(localObject);
        if (bool)
        {
          paramString = (l.b)b;
          if (paramString != null) {
            paramString.t();
          }
          return;
        }
        break;
      case -2055773667: 
        localObject = "add_bank_details";
        bool = paramString.equals(localObject);
        if (bool)
        {
          j();
          paramString = (l.b)b;
          if (paramString != null) {
            paramString.w();
          }
          return;
        }
        break;
      }
    }
    l();
    paramString = (l.b)b;
    if (paramString != null)
    {
      paramString.r();
      return;
    }
  }
  
  public final void b()
  {
    l.b localb = (l.b)b;
    if (localb != null)
    {
      String str = e.b("credit_faq", "https://webapp.tcpay.in/credit-faq");
      localb.b(str);
      return;
    }
  }
  
  public final void b(String paramString)
  {
    Object localObject = "fragmentTag";
    k.b(paramString, (String)localObject);
    a = paramString;
    int i = paramString.hashCode();
    boolean bool;
    switch (i)
    {
    default: 
      break;
    case 1701562126: 
      localObject = "InfoManualEntryFragment";
      bool = paramString.equals(localObject);
      if (bool)
      {
        paramString = d;
        i = R.drawable.credit_banner_address_proof;
        paramString = paramString.c(i);
      }
      break;
    case 1579696538: 
      localObject = "BankInfoFragment";
      bool = paramString.equals(localObject);
      if (bool)
      {
        paramString = d;
        i = R.drawable.credit_banner_address_proof;
        paramString = paramString.c(i);
      }
      break;
    case 1105501196: 
      localObject = "AddressProofSelectionFragment";
      bool = paramString.equals(localObject);
      if (bool)
      {
        paramString = d;
        i = R.drawable.credit_banner_address_proof;
        paramString = paramString.c(i);
      }
      break;
    case 1085257895: 
      localObject = "ConfirmUserDetailsFragment";
      bool = paramString.equals(localObject);
      if (bool)
      {
        paramString = d;
        i = R.drawable.credit_banner_address_proof;
        paramString = paramString.c(i);
      }
      break;
    case -864436424: 
      localObject = "ProofSelectionFragment";
      bool = paramString.equals(localObject);
      if (bool)
      {
        paramString = d;
        i = R.drawable.credit_banner_address_proof;
        paramString = paramString.c(i);
      }
      break;
    case -1065888750: 
      localObject = "ScheduleVisitInfoFragment";
      bool = paramString.equals(localObject);
      if (bool)
      {
        paramString = d;
        i = R.drawable.credit_banner_documentation;
        paramString = paramString.c(i);
      }
      break;
    case -1097825982: 
      localObject = "MeetingScheduledFragment";
      bool = paramString.equals(localObject);
      if (bool)
      {
        paramString = d;
        i = R.drawable.credit_banner_documentation;
        paramString = paramString.c(i);
      }
      break;
    case -1354134842: 
      localObject = "SelfieInfoFragment";
      bool = paramString.equals(localObject);
      break;
    case -1523171931: 
      localObject = "IFSCSearchFragment";
      bool = paramString.equals(localObject);
      if (bool)
      {
        paramString = d;
        i = R.drawable.credit_banner_address_proof;
        paramString = paramString.c(i);
      }
      break;
    case -1874447548: 
      localObject = "ScheduleVisitFragment";
      bool = paramString.equals(localObject);
      if (bool)
      {
        paramString = d;
        i = R.drawable.credit_banner_documentation;
        paramString = paramString.c(i);
      }
      break;
    }
    paramString = d;
    i = R.drawable.credit_banner_selfie;
    paramString = paramString.c(i);
    k.a(paramString, "when (fragmentTag) {\n   …_banner_selfie)\n        }");
    localObject = (l.b)b;
    if (localObject != null) {
      ((l.b)localObject).a(paramString);
    }
    e();
  }
  
  public final void c()
  {
    l.b localb = (l.b)b;
    if (localb != null)
    {
      localb.j();
      com.truecaller.utils.n localn = d;
      int i = R.dimen.control_zero_space;
      int j = localn.b(i);
      localb.a(j, 60.0F);
      return;
    }
  }
  
  public final void e()
  {
    l.b localb = (l.b)b;
    if (localb != null)
    {
      localb.i();
      com.truecaller.utils.n localn = d;
      int i = R.dimen.control_mini_space_negative;
      int j = localn.b(i);
      localb.a(j, 72.0F);
      return;
    }
  }
  
  public final void f()
  {
    Object localObject = e;
    String str = "credit_next_page";
    localObject = ((g)localObject).a(str);
    if (localObject != null)
    {
      localObject = e;
      str = "credit_next_page";
      ((g)localObject).a(str, null);
    }
  }
  
  public final void g()
  {
    Object localObject = e;
    String str = "credit_next_page";
    localObject = ((g)localObject).a(str);
    if (localObject != null)
    {
      int i = ((String)localObject).hashCode();
      boolean bool;
      switch (i)
      {
      default: 
        break;
      case 1919677800: 
        str = "truecaller://credit/schedule_meeting";
        bool = ((String)localObject).equals(str);
        if (bool)
        {
          i();
          localObject = (l.b)b;
          if (localObject == null) {
            break label351;
          }
          ((l.b)localObject).v();
        }
        break;
      case 1001059693: 
        str = "truecaller://credit/user_info";
        bool = ((String)localObject).equals(str);
        if (bool)
        {
          l();
          localObject = (l.b)b;
          if (localObject == null) {
            break label351;
          }
          ((l.b)localObject).r();
        }
        break;
      case 386348763: 
        str = "truecaller://credit/address_verification";
        bool = ((String)localObject).equals(str);
        if (bool)
        {
          k();
          localObject = (l.b)b;
          if (localObject == null) {
            break label351;
          }
          ((l.b)localObject).s();
        }
        break;
      case -325063435: 
        str = "truecaller://credit/address_confirmation";
        bool = ((String)localObject).equals(str);
        if (bool)
        {
          localObject = (l.b)b;
          if (localObject == null) {
            break label351;
          }
          ((l.b)localObject).t();
        }
        break;
      case -1221239770: 
        str = "truecaller://credit/meeting_confirmed";
        bool = ((String)localObject).equals(str);
        if (bool)
        {
          localObject = (l.b)b;
          if (localObject == null) {
            break label351;
          }
          ((l.b)localObject).u();
        }
        break;
      case -1491379470: 
        str = "truecaller://credit/add_bank_details";
        bool = ((String)localObject).equals(str);
        if (bool)
        {
          j();
          localObject = (l.b)b;
          if (localObject == null) {
            break label351;
          }
          ((l.b)localObject).w();
        }
        break;
      }
    }
    l();
    localObject = (l.b)b;
    if (localObject != null) {
      ((l.b)localObject).r();
    }
    label351:
    f();
  }
  
  public final String h()
  {
    return e.a("credit_next_page");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.b.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */