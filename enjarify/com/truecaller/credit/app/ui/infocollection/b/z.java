package com.truecaller.credit.app.ui.infocollection.b;

import android.content.Intent;
import android.net.Uri;
import c.d.f;
import c.g.a.m;
import c.g.b.k;
import c.t;
import com.truecaller.ba;
import com.truecaller.credit.R.color;
import com.truecaller.credit.R.string;
import com.truecaller.credit.app.a.a.a;
import com.truecaller.credit.app.a.b;
import com.truecaller.credit.app.ui.assist.AadhaarDocument;
import com.truecaller.credit.app.ui.assist.CreditDocumentType;
import com.truecaller.credit.app.ui.assist.PanDocument;
import com.truecaller.credit.app.ui.infocollection.views.c.p.a;
import com.truecaller.credit.app.ui.infocollection.views.c.p.b;
import com.truecaller.credit.app.util.s;
import com.truecaller.credit.app.util.v;
import com.truecaller.credit.data.models.UserInfoDataRequest;
import kotlinx.coroutines.e;

public final class z
  extends ba
  implements p.a
{
  final s c;
  private int d;
  private int e;
  private boolean f;
  private boolean g;
  private boolean h;
  private boolean i;
  private String j;
  private final f k;
  private final f l;
  private final com.truecaller.utils.n m;
  private final v n;
  private final b o;
  
  public z(f paramf1, f paramf2, s params, com.truecaller.utils.n paramn, v paramv, b paramb)
  {
    super(paramf1);
    k = paramf1;
    l = paramf2;
    c = params;
    m = paramn;
    n = paramv;
    o = paramb;
    int i1 = -1;
    d = i1;
    e = i1;
  }
  
  private final UserInfoDataRequest a(String paramString)
  {
    return n.a(paramString);
  }
  
  private final void a(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    Object localObject = "CreditPersonalInfo";
    a.a locala = new com/truecaller/credit/app/a/a$a;
    locala.<init>((String)localObject, (String)localObject);
    localObject = new c.n[4];
    paramString1 = t.a("Status", paramString1);
    localObject[0] = paramString1;
    paramString1 = t.a("Action", paramString3);
    localObject[1] = paramString1;
    paramString1 = t.a("Type", paramString4);
    localObject[2] = paramString1;
    paramString1 = t.a("Context", paramString2);
    localObject[3] = paramString1;
    locala.a((c.n[])localObject);
    paramString1 = locala.a();
    a = false;
    paramString2 = o;
    paramString1 = paramString1.b();
    paramString2.a(paramString1);
  }
  
  private final void a(boolean paramBoolean, String paramString, CreditDocumentType paramCreditDocumentType)
  {
    p.b localb = (p.b)b;
    if (localb != null) {
      localb.a(paramBoolean, paramString, paramCreditDocumentType);
    }
    String str = "initiated";
    paramString = (p.b)b;
    if (paramString != null) {
      paramString = paramString.g();
    } else {
      paramString = null;
    }
    paramCreditDocumentType = g;
    a(str, paramString, "continue", paramCreditDocumentType);
  }
  
  public final void a()
  {
    Object localObject1 = (p.b)b;
    if (localObject1 == null) {
      return;
    }
    int i1 = d;
    Object localObject2 = null;
    Object localObject3;
    Object localObject4;
    Object localObject5;
    int i3;
    int i4;
    int i5;
    switch (i1)
    {
    default: 
      break;
    case 1: 
      boolean bool1 = f;
      if (bool1)
      {
        ((p.b)localObject1).k();
        return;
      }
      localObject1 = ((p.b)localObject1).g();
      localObject3 = "continue";
      localObject4 = "address_verification";
      a("clicked", (String)localObject1, (String)localObject3, (String)localObject4);
      localObject1 = "back";
      localObject5 = new com/truecaller/credit/app/ui/assist/PanDocument;
      i3 = R.string.credit_pan_camera_description;
      i4 = R.color.black_50;
      String str1 = "pan";
      i5 = R.string.credit_upload_image_pan;
      ((PanDocument)localObject5).<init>(i3, i4, str1, i5);
      localObject5 = (CreditDocumentType)localObject5;
      a(false, (String)localObject1, (CreditDocumentType)localObject5);
      break;
    case 0: 
      int i2 = e;
      i3 = 1;
      boolean bool2;
      int i6;
      int i7;
      switch (i2)
      {
      default: 
        break;
      case 1: 
        bool2 = i;
        if (!bool2)
        {
          localObject1 = new com/truecaller/credit/app/ui/assist/AadhaarDocument;
          i6 = R.string.credit_aadhaar_camera_description;
          i5 = R.color.black_50;
          i7 = R.string.credit_upload_image_aadhaar_long;
          localObject4 = localObject1;
          ((AadhaarDocument)localObject1).<init>(i6, i5, 4, 3, "aadhaar_card", null, "aadhaar_full", i7, 32);
          localObject1 = (CreditDocumentType)localObject1;
          a(i3, "back", (CreditDocumentType)localObject1);
          return;
        }
        localObject5 = new com/truecaller/credit/app/ui/assist/AadhaarDocument;
        localObject3 = m;
        i4 = R.string.credit_hint_aadhaar_number;
        localObject2 = new Object[0];
        String str2 = ((com.truecaller.utils.n)localObject3).a(i4, (Object[])localObject2);
        k.a(str2, "resourceProvider.getStri…edit_hint_aadhaar_number)");
        i7 = R.string.credit_upload_image_default;
        localObject4 = localObject5;
        ((AadhaarDocument)localObject5).<init>(0, 0, 0, 0, "aadhaar_card", str2, null, i7, 79);
        localObject5 = (CreditDocumentType)localObject5;
        localObject2 = j;
        localObject2 = a((String)localObject2);
        ((p.b)localObject1).a((CreditDocumentType)localObject5, (UserInfoDataRequest)localObject2, "camera");
        return;
      case 0: 
        bool2 = g;
        if (!bool2)
        {
          localObject1 = new com/truecaller/credit/app/ui/assist/AadhaarDocument;
          i6 = R.string.credit_aadhaar_camera_description;
          i5 = R.color.black_50;
          i7 = R.string.credit_upload_image_aadhaar_front;
          localObject4 = localObject1;
          ((AadhaarDocument)localObject1).<init>(i6, i5, 3, 4, "aadhaar_card", null, "aadhaar_front", i7, 32);
          localObject1 = (CreditDocumentType)localObject1;
          a(i3, "back", (CreditDocumentType)localObject1);
          return;
        }
        bool2 = h;
        if (!bool2)
        {
          localObject1 = new com/truecaller/credit/app/ui/assist/AadhaarDocument;
          i4 = R.string.credit_aadhaar_camera_description;
          i6 = R.color.black_50;
          i8 = R.string.credit_upload_image_aadhaar_back;
          localObject3 = localObject1;
          ((AadhaarDocument)localObject1).<init>(i4, i6, 3, 4, "aadhaar_card", null, "aadhaar_back", i8, 32);
          localObject1 = (CreditDocumentType)localObject1;
          a(false, "back", (CreditDocumentType)localObject1);
          return;
        }
        localObject5 = new com/truecaller/credit/app/ui/assist/AadhaarDocument;
        localObject3 = m;
        int i9 = R.string.credit_hint_aadhaar_number;
        localObject2 = new Object[0];
        String str3 = ((com.truecaller.utils.n)localObject3).a(i9, (Object[])localObject2);
        k.a(str3, "resourceProvider.getStri…edit_hint_aadhaar_number)");
        int i8 = R.string.credit_upload_image_default;
        localObject3 = localObject5;
        ((AadhaarDocument)localObject5).<init>(0, 0, 0, 0, "aadhaar_card", str3, null, i8, 79);
        localObject5 = (CreditDocumentType)localObject5;
        localObject2 = j;
        localObject2 = a((String)localObject2);
        ((p.b)localObject1).a((CreditDocumentType)localObject5, (UserInfoDataRequest)localObject2, "camera");
        return;
      case -1: 
        localObject5 = "clicked";
        localObject2 = ((p.b)localObject1).g();
        localObject3 = "continue";
        localObject4 = "address_verification";
        a((String)localObject5, (String)localObject2, (String)localObject3, (String)localObject4);
        ((p.b)localObject1).i();
      }
      return;
    }
  }
  
  public final void a(int paramInt)
  {
    int i1 = -1;
    if (paramInt != i1)
    {
      e = paramInt;
      i1 = 0;
      Object localObject1 = null;
      b(0);
      p.b localb;
      Object localObject2;
      int i2;
      switch (paramInt)
      {
      default: 
        break;
      case 1: 
        localb = (p.b)b;
        if (localb != null)
        {
          localObject2 = m;
          i2 = R.string.all_continue;
          localObject1 = new Object[0];
          localObject1 = ((com.truecaller.utils.n)localObject2).a(i2, (Object[])localObject1);
          localObject2 = "resourceProvider.getString(R.string.all_continue)";
          k.a(localObject1, (String)localObject2);
          localb.c((String)localObject1);
        }
        break;
      case 0: 
        localb = (p.b)b;
        if (localb != null)
        {
          localObject2 = m;
          i2 = R.string.credit_front_capture_button;
          localObject1 = new Object[0];
          localObject1 = ((com.truecaller.utils.n)localObject2).a(i2, (Object[])localObject1);
          localObject2 = "resourceProvider.getStri…dit_front_capture_button)";
          k.a(localObject1, (String)localObject2);
          localb.c((String)localObject1);
        }
        break;
      }
      a();
    }
  }
  
  public final void a(int paramInt1, int paramInt2, Intent paramIntent)
  {
    int i1 = 13;
    if (paramInt1 == i1)
    {
      paramInt1 = -1;
      Object localObject1;
      Object localObject2;
      Object localObject3;
      boolean bool;
      if (paramInt2 == paramInt1)
      {
        if (paramIntent != null)
        {
          localObject1 = (Uri)paramIntent.getParcelableExtra("image");
          localObject2 = (CreditDocumentType)paramIntent.getParcelableExtra("document_type");
          localObject3 = "scanned_text";
          bool = paramIntent.hasExtra((String)localObject3);
          if (bool)
          {
            localObject3 = "scanned_text";
            paramIntent = paramIntent.getStringExtra((String)localObject3);
            j = paramIntent;
          }
          k.a(localObject1, "uri");
          k.a(localObject2, "docType");
          paramIntent = new com/truecaller/credit/app/ui/infocollection/b/z$a;
          bool = false;
          localObject3 = null;
          paramIntent.<init>(this, (Uri)localObject1, (CreditDocumentType)localObject2, null);
          paramIntent = (m)paramIntent;
          paramInt1 = 3;
          e.b(this, null, paramIntent, paramInt1);
        }
        return;
      }
      if ((paramInt2 == 0) && (paramIntent != null))
      {
        localObject2 = "state";
        paramInt2 = paramIntent.hasExtra((String)localObject2);
        if (paramInt2 != 0)
        {
          localObject2 = "state";
          bool = false;
          localObject3 = null;
          paramInt2 = paramIntent.getIntExtra((String)localObject2, 0);
          int i2 = 1;
          if (paramInt2 == i2)
          {
            f = false;
            paramInt1 = d;
            switch (paramInt1)
            {
            default: 
              break;
            case 1: 
              localObject1 = (p.b)b;
              if (localObject1 != null)
              {
                localObject2 = m;
                i2 = R.string.upload_failed;
                localObject3 = new Object[0];
                localObject2 = ((com.truecaller.utils.n)localObject2).a(i2, (Object[])localObject3);
                paramIntent = "resourceProvider.getString(R.string.upload_failed)";
                k.a(localObject2, paramIntent);
                ((p.b)localObject1).b((String)localObject2);
              }
              return;
            case 0: 
              localObject1 = (p.b)b;
              if (localObject1 != null)
              {
                localObject2 = m;
                i2 = R.string.upload_failed;
                localObject3 = new Object[0];
                localObject2 = ((com.truecaller.utils.n)localObject2).a(i2, (Object[])localObject3);
                k.a(localObject2, "resourceProvider.getString(R.string.upload_failed)");
                ((p.b)localObject1).a((String)localObject2);
                return;
              }
              break;
            }
            return;
          }
          paramInt2 = d;
          if (paramInt2 == 0)
          {
            paramInt2 = i;
            if (paramInt2 == 0)
            {
              paramInt2 = g;
              if (paramInt2 == 0) {
                e = paramInt1;
              }
            }
          }
        }
      }
    }
  }
  
  public final void b(int paramInt)
  {
    d = paramInt;
    switch (paramInt)
    {
    default: 
      break;
    case 1: 
      localb = (p.b)b;
      if (localb != null) {
        localb.m();
      }
      break;
    case 0: 
      localb = (p.b)b;
      if (localb != null) {
        localb.l();
      }
      break;
    }
    p.b localb = (p.b)b;
    if (localb != null)
    {
      localb.n();
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.b.z
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */