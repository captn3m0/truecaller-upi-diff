package com.truecaller.credit.app.ui.infocollection.b;

import c.d.f;
import c.g.b.k;
import c.n.m;
import c.u;
import com.truecaller.ba;
import com.truecaller.credit.R.id;
import com.truecaller.credit.R.string;
import com.truecaller.credit.app.ui.infocollection.views.c.d.a;
import com.truecaller.credit.app.ui.infocollection.views.c.d.b;
import com.truecaller.credit.data.models.Address;
import com.truecaller.credit.data.repository.CreditRepository;
import com.truecaller.utils.n;

public final class e
  extends ba
  implements d.a
{
  public Address c;
  private final f d;
  private final f e;
  private final CreditRepository f;
  private final n g;
  
  public e(f paramf1, f paramf2, CreditRepository paramCreditRepository, n paramn)
  {
    super(paramf1);
    d = paramf1;
    e = paramf2;
    f = paramCreditRepository;
    g = paramn;
  }
  
  private final boolean a()
  {
    Object localObject = c;
    if (localObject == null)
    {
      String str1 = "address";
      k.a(str1);
    }
    localObject = (CharSequence)((Address)localObject).getCity();
    int i = ((CharSequence)localObject).length();
    boolean bool = true;
    if (i > 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject = null;
    }
    if (i != 0)
    {
      localObject = c;
      String str2;
      if (localObject == null)
      {
        str2 = "address";
        k.a(str2);
      }
      localObject = (CharSequence)((Address)localObject).getAddress_line_1();
      i = ((CharSequence)localObject).length();
      if (i > 0)
      {
        i = 1;
      }
      else
      {
        i = 0;
        localObject = null;
      }
      if (i != 0)
      {
        localObject = c;
        if (localObject == null)
        {
          str2 = "address";
          k.a(str2);
        }
        localObject = (CharSequence)((Address)localObject).getAddress_line_2();
        i = ((CharSequence)localObject).length();
        if (i > 0)
        {
          i = 1;
        }
        else
        {
          i = 0;
          localObject = null;
        }
        if (i != 0)
        {
          localObject = c;
          if (localObject == null)
          {
            str2 = "address";
            k.a(str2);
          }
          localObject = (CharSequence)((Address)localObject).getAddress_line_3();
          i = ((CharSequence)localObject).length();
          if (i > 0)
          {
            i = 1;
          }
          else
          {
            i = 0;
            localObject = null;
          }
          if (i != 0)
          {
            localObject = c;
            if (localObject == null)
            {
              str2 = "address";
              k.a(str2);
            }
            localObject = ((Address)localObject).getPincode();
            if (localObject != null)
            {
              localObject = (CharSequence)m.b((CharSequence)localObject).toString();
              i = ((CharSequence)localObject).length();
              if (i > 0)
              {
                i = 1;
              }
              else
              {
                i = 0;
                localObject = null;
              }
              if (i != 0)
              {
                localObject = c;
                if (localObject == null)
                {
                  str2 = "address";
                  k.a(str2);
                }
                localObject = (CharSequence)((Address)localObject).getState();
                i = ((CharSequence)localObject).length();
                if (i > 0)
                {
                  i = 1;
                }
                else
                {
                  i = 0;
                  localObject = null;
                }
                if (i != 0) {
                  return bool;
                }
              }
            }
            else
            {
              localObject = new c/u;
              ((u)localObject).<init>("null cannot be cast to non-null type kotlin.CharSequence");
              throw ((Throwable)localObject);
            }
          }
        }
      }
    }
    return false;
  }
  
  public final void a(int paramInt, String paramString)
  {
    k.b(paramString, "text");
    d.b localb = (d.b)b;
    if (localb == null) {
      return;
    }
    int i = R.id.textCity;
    Address localAddress;
    String str;
    if (paramInt == i)
    {
      localAddress = c;
      if (localAddress == null)
      {
        str = "address";
        k.a(str);
      }
      localAddress.setCity(paramString);
    }
    else
    {
      i = R.id.textAddressLine1;
      if (paramInt == i)
      {
        localAddress = c;
        if (localAddress == null)
        {
          str = "address";
          k.a(str);
        }
        localAddress.setAddress_line_1(paramString);
      }
      else
      {
        i = R.id.textAddressLine2;
        if (paramInt == i)
        {
          localAddress = c;
          if (localAddress == null)
          {
            str = "address";
            k.a(str);
          }
          localAddress.setAddress_line_2(paramString);
        }
        else
        {
          i = R.id.textAddressLine3;
          if (paramInt == i)
          {
            localAddress = c;
            if (localAddress == null)
            {
              str = "address";
              k.a(str);
            }
            localAddress.setAddress_line_3(paramString);
          }
          else
          {
            i = R.id.textPinCode;
            if (paramInt == i)
            {
              localAddress = c;
              if (localAddress == null)
              {
                str = "address";
                k.a(str);
              }
              localAddress.setPincode(paramString);
            }
            else
            {
              i = R.id.textState;
              if (paramInt == i)
              {
                localAddress = c;
                if (localAddress == null)
                {
                  str = "address";
                  k.a(str);
                }
                localAddress.setState(paramString);
              }
            }
          }
        }
      }
    }
    paramInt = a();
    if (paramInt != 0)
    {
      localb.g();
      return;
    }
    localb.h();
  }
  
  public final void a(d.b paramb)
  {
    k.b(paramb, "presenterView");
    super.a(paramb);
    Object localObject1 = g;
    int i = R.string.credit_add_address_button_text;
    Object[] arrayOfObject = new Object[0];
    localObject1 = ((n)localObject1).a(i, arrayOfObject);
    Object localObject2 = "resourceProvider.getStri…_add_address_button_text)";
    k.a(localObject1, (String)localObject2);
    paramb.a((String)localObject1);
    paramb.j();
    localObject1 = paramb.f();
    if (localObject1 == null)
    {
      localObject1 = new com/truecaller/credit/data/models/Address;
      arrayOfObject = null;
      int j = 127;
      localObject2 = localObject1;
      ((Address)localObject1).<init>(null, null, null, null, null, null, null, j, null);
    }
    c = ((Address)localObject1);
    boolean bool = a();
    if (bool)
    {
      localObject1 = c;
      if (localObject1 == null)
      {
        localObject2 = "address";
        k.a((String)localObject2);
      }
      paramb.a((Address)localObject1);
    }
    paramb.h();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.b.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */