package com.truecaller.credit.app.ui.infocollection.b;

import android.graphics.Bitmap;
import android.net.Uri;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.credit.app.ui.assist.CreditDocumentType;
import com.truecaller.credit.app.util.BitmapConverter;
import com.truecaller.credit.app.util.s;
import java.lang.ref.WeakReference;
import kotlinx.coroutines.ag;

final class l$a$a
  extends c.d.b.a.k
  implements m
{
  Object a;
  Object b;
  Object c;
  Object d;
  Object e;
  int f;
  private ag i;
  
  l$a$a(byte[] paramArrayOfByte, c paramc, l.a parama)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/credit/app/ui/infocollection/b/l$a$a;
    byte[] arrayOfByte = g;
    l.a locala1 = h;
    locala.<init>(arrayOfByte, paramc, locala1);
    paramObject = (ag)paramObject;
    i = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int j = f;
    Object localObject3;
    Object localObject4;
    boolean bool2;
    int n;
    boolean bool1;
    float f1;
    switch (j)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 4: 
      localObject1 = (l)e;
      localObject2 = (Bitmap)d;
      localObject3 = (WeakReference)b;
      localObject4 = (WeakReference)a;
      bool2 = paramObject instanceof o.b;
      if (!bool2) {
        break label591;
      }
      throw a;
    case 3: 
      localObject2 = (CreditDocumentType)c;
      localObject3 = (WeakReference)b;
      localObject4 = (WeakReference)a;
      bool2 = paramObject instanceof o.b;
      if (!bool2) {
        break label472;
      }
      throw a;
    case 2: 
      localObject2 = (WeakReference)a;
      n = paramObject instanceof o.b;
      if (n == 0) {
        break label370;
      }
      throw a;
    case 1: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        throw a;
      }
      break;
    case 0: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label661;
      }
      paramObject = l.b(h.c);
      localObject2 = g;
      n = 1;
      f1 = Float.MIN_VALUE;
      f = n;
      paramObject = ((BitmapConverter)paramObject).a((byte[])localObject2);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (Bitmap)paramObject;
    if (paramObject != null)
    {
      localObject2 = l.b(h.c);
      paramObject = ((BitmapConverter)localObject2).a((Bitmap)paramObject);
    }
    else
    {
      paramObject = null;
    }
    Object localObject2 = paramObject;
    if (paramObject != null)
    {
      paramObject = l.c(h.c);
      localObject3 = h.c;
      f1 = l.d((l)localObject3);
      a = localObject2;
      int i1 = 2;
      f = i1;
      paramObject = ((s)paramObject).a((WeakReference)localObject2, f1);
      if (paramObject == localObject1) {
        return localObject1;
      }
      label370:
      paramObject = (WeakReference)paramObject;
      if (paramObject != null)
      {
        localObject3 = l.e(h.c);
        if (localObject3 != null)
        {
          localObject4 = l.b(h.c);
          a = localObject2;
          b = paramObject;
          c = localObject3;
          int m = 3;
          f = m;
          localObject4 = ((BitmapConverter)localObject4).a((WeakReference)paramObject, (CreditDocumentType)localObject3);
          if (localObject4 == localObject1) {
            return localObject1;
          }
          Object localObject5 = localObject3;
          localObject3 = paramObject;
          paramObject = localObject4;
          localObject4 = localObject2;
          localObject2 = localObject5;
          label472:
          paramObject = (WeakReference)paramObject;
          if (paramObject == null) {
            break label635;
          }
          paramObject = (Bitmap)((WeakReference)paramObject).get();
          if (paramObject == null) {
            break label635;
          }
          l locall = h.c;
          s locals = l.c(h.c);
          String str = "bitmap";
          c.g.b.k.a(paramObject, str);
          a = localObject4;
          b = localObject3;
          c = localObject2;
          d = paramObject;
          e = locall;
          int k = 4;
          f = k;
          localObject2 = locals.a((Bitmap)paramObject);
          if (localObject2 == localObject1) {
            return localObject1;
          }
          localObject1 = locall;
          localObject5 = localObject2;
          localObject2 = paramObject;
          paramObject = localObject5;
          label591:
          paramObject = (Uri)paramObject;
          l.a((l)localObject1, (Uri)paramObject);
          paramObject = (Bitmap)((WeakReference)localObject3).get();
          if (paramObject != null) {
            ((Bitmap)paramObject).recycle();
          }
          ((WeakReference)localObject3).clear();
          ((Bitmap)localObject2).recycle();
          break label635;
        }
      }
      localObject4 = localObject2;
      label635:
      paramObject = (Bitmap)((WeakReference)localObject4).get();
      if (paramObject != null) {
        ((Bitmap)paramObject).recycle();
      }
      ((WeakReference)localObject4).clear();
    }
    return x.a;
    label661:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.b.l.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */