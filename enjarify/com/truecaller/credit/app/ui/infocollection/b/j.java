package com.truecaller.credit.app.ui.infocollection.b;

import c.d.f;
import c.g.b.k;
import c.u;
import com.truecaller.ba;
import com.truecaller.credit.app.a.b;
import com.truecaller.credit.app.ui.infocollection.views.c.g.a;
import com.truecaller.credit.app.ui.infocollection.views.c.g.b;
import com.truecaller.credit.data.models.Address;
import com.truecaller.credit.data.models.UserInfoDataRequest;
import com.truecaller.credit.data.repository.CreditRepository;
import com.truecaller.utils.n;
import java.util.List;
import kotlinx.coroutines.e;

public final class j
  extends ba
  implements g.a
{
  private Address c;
  private UserInfoDataRequest d;
  private boolean e;
  private final n f;
  private final CreditRepository g;
  private final f h;
  private final f i;
  private final b j;
  
  public j(n paramn, CreditRepository paramCreditRepository, f paramf1, f paramf2, b paramb)
  {
    super(paramf1);
    f = paramn;
    g = paramCreditRepository;
    h = paramf1;
    i = paramf2;
    j = paramb;
    e = true;
  }
  
  private final boolean e()
  {
    Object localObject1 = c;
    if (localObject1 != null)
    {
      Object localObject2 = (CharSequence)((Address)localObject1).getCity();
      int k = ((CharSequence)localObject2).length();
      boolean bool = true;
      if (k > 0)
      {
        k = 1;
      }
      else
      {
        k = 0;
        localObject2 = null;
      }
      if (k != 0)
      {
        localObject2 = (CharSequence)((Address)localObject1).getAddress_line_1();
        k = ((CharSequence)localObject2).length();
        if (k > 0)
        {
          k = 1;
        }
        else
        {
          k = 0;
          localObject2 = null;
        }
        if (k != 0)
        {
          localObject2 = (CharSequence)((Address)localObject1).getAddress_line_2();
          k = ((CharSequence)localObject2).length();
          if (k > 0)
          {
            k = 1;
          }
          else
          {
            k = 0;
            localObject2 = null;
          }
          if (k != 0)
          {
            localObject2 = (CharSequence)((Address)localObject1).getAddress_line_3();
            k = ((CharSequence)localObject2).length();
            if (k > 0)
            {
              k = 1;
            }
            else
            {
              k = 0;
              localObject2 = null;
            }
            if (k != 0)
            {
              localObject2 = ((Address)localObject1).getPincode();
              if (localObject2 != null)
              {
                localObject2 = (CharSequence)c.n.m.b((CharSequence)localObject2).toString();
                k = ((CharSequence)localObject2).length();
                if (k > 0)
                {
                  k = 1;
                }
                else
                {
                  k = 0;
                  localObject2 = null;
                }
                if (k != 0)
                {
                  localObject1 = (CharSequence)((Address)localObject1).getState();
                  int m = ((CharSequence)localObject1).length();
                  if (m > 0)
                  {
                    m = 1;
                  }
                  else
                  {
                    m = 0;
                    localObject1 = null;
                  }
                  if (m != 0) {
                    return bool;
                  }
                }
              }
              else
              {
                localObject1 = new c/u;
                ((u)localObject1).<init>("null cannot be cast to non-null type kotlin.CharSequence");
                throw ((Throwable)localObject1);
              }
            }
          }
        }
      }
      return false;
    }
    return false;
  }
  
  public final void a()
  {
    int k = e();
    int m = 0;
    Object localObject1 = null;
    Address localAddress;
    String str1;
    String str2;
    String str3;
    String str4;
    String str5;
    String str6;
    String str7;
    if (k != 0)
    {
      k = e;
      if (k == 0)
      {
        localObject2 = c;
        if (localObject2 != null)
        {
          localAddress = new com/truecaller/credit/data/models/Address;
          str1 = "current";
          str2 = ((Address)localObject2).getAddress_line_1();
          str3 = ((Address)localObject2).getAddress_line_2();
          str4 = ((Address)localObject2).getAddress_line_3();
          str5 = ((Address)localObject2).getCity();
          str6 = ((Address)localObject2).getPincode();
          str7 = ((Address)localObject2).getState();
          localAddress.<init>(str1, str2, str3, str4, str5, str6, str7);
          break label226;
        }
        localAddress = null;
        break label226;
      }
    }
    Object localObject2 = d;
    if (localObject2 != null)
    {
      localObject2 = ((UserInfoDataRequest)localObject2).getAddresses();
      if (localObject2 != null)
      {
        localObject2 = (Address)((List)localObject2).get(0);
        if (localObject2 != null)
        {
          localAddress = new com/truecaller/credit/data/models/Address;
          str1 = "current";
          str2 = ((Address)localObject2).getAddress_line_1();
          str3 = ((Address)localObject2).getAddress_line_2();
          str4 = ((Address)localObject2).getAddress_line_3();
          str5 = ((Address)localObject2).getCity();
          str6 = ((Address)localObject2).getPincode();
          str7 = ((Address)localObject2).getState();
          localAddress.<init>(str1, str2, str3, str4, str5, str6, str7);
          label226:
          localObject2 = d;
          if (localObject2 != null)
          {
            localObject2 = ((UserInfoDataRequest)localObject2).getAddresses();
            if (localObject2 != null) {
              m = ((List)localObject2).size();
            }
          }
          if (localAddress != null)
          {
            k = 1;
            if (m > k)
            {
              localObject1 = d;
              if (localObject1 != null)
              {
                localObject1 = ((UserInfoDataRequest)localObject1).getAddresses();
                if (localObject1 != null) {
                  ((List)localObject1).set(k, localAddress);
                }
              }
            }
            else
            {
              localObject2 = d;
              if (localObject2 != null)
              {
                localObject2 = ((UserInfoDataRequest)localObject2).getAddresses();
                if (localObject2 != null) {
                  ((List)localObject2).add(localAddress);
                }
              }
            }
          }
        }
      }
    }
    localObject2 = d;
    if (localObject2 != null)
    {
      localObject1 = new com/truecaller/credit/app/ui/infocollection/b/j$a;
      ((j.a)localObject1).<init>((UserInfoDataRequest)localObject2, null, this);
      localObject1 = (c.g.a.m)localObject1;
      e.b(this, null, (c.g.a.m)localObject1, 3);
      return;
    }
  }
  
  public final void a(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6)
  {
    k.b(paramString1, "cityVal");
    k.b(paramString2, "addressLine1");
    k.b(paramString3, "addressLine2");
    k.b(paramString4, "addressLine3");
    k.b(paramString5, "pincodeVal");
    k.b(paramString6, "stateVal");
    Address localAddress = c;
    Object localObject;
    if (localAddress == null)
    {
      localAddress = new com/truecaller/credit/data/models/Address;
      String str = "current";
      localObject = localAddress;
      localAddress.<init>(str, paramString2, paramString3, paramString4, paramString1, paramString5, paramString6);
      c = localAddress;
    }
    else if (localAddress != null)
    {
      localObject = "current";
      localAddress.setAddress_type((String)localObject);
      localAddress.setCity(paramString1);
      localAddress.setAddress_line_1(paramString2);
      localAddress.setAddress_line_2(paramString3);
      localAddress.setAddress_line_3(paramString4);
      localAddress.setPincode(paramString5);
      localAddress.setState(paramString6);
    }
    paramString1 = (g.b)b;
    if (paramString1 == null) {
      return;
    }
    boolean bool = e();
    if (bool)
    {
      paramString1.j();
      return;
    }
    paramString1.k();
  }
  
  public final void a(boolean paramBoolean)
  {
    e = paramBoolean;
    g.b localb = (g.b)b;
    if (localb != null)
    {
      boolean bool = paramBoolean ^ true;
      localb.a(bool);
      if (!paramBoolean)
      {
        paramBoolean = e();
        if (!paramBoolean)
        {
          localb.k();
          return;
        }
      }
      localb.j();
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.b.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */