package com.truecaller.credit.app.ui.infocollection.b;

import c.g.b.k;
import c.n.m;
import c.u;
import com.truecaller.ba;
import com.truecaller.credit.R.color;
import com.truecaller.credit.R.id;
import com.truecaller.credit.R.string;
import com.truecaller.credit.app.ui.assist.CreditDocumentType;
import com.truecaller.credit.app.ui.assist.QRScan;
import com.truecaller.credit.app.ui.infocollection.views.c.m.a;
import com.truecaller.credit.app.ui.infocollection.views.c.m.b;
import com.truecaller.credit.app.util.j;
import com.truecaller.credit.data.models.Address;
import com.truecaller.credit.data.models.UserInfoDataRequest;
import com.truecaller.credit.data.repository.CreditRepository;
import com.truecaller.utils.n;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

public final class v
  extends ba
  implements m.a
{
  private CreditDocumentType c;
  private UserInfoDataRequest d;
  private Address e;
  private final CreditRepository f;
  private final n g;
  private final com.truecaller.credit.app.util.f h;
  private final c.d.f i;
  private final c.d.f j;
  private final j k;
  private final com.truecaller.credit.app.util.v l;
  
  public v(CreditRepository paramCreditRepository, n paramn, com.truecaller.credit.app.util.f paramf, c.d.f paramf1, c.d.f paramf2, j paramj, com.truecaller.credit.app.util.v paramv)
  {
    super(paramf1);
    f = paramCreditRepository;
    g = paramn;
    h = paramf;
    i = paramf1;
    j = paramf2;
    k = paramj;
    l = paramv;
    localObject = new com/truecaller/credit/data/models/UserInfoDataRequest;
    ((UserInfoDataRequest)localObject).<init>(null, null, null, null, null, 31, null);
    d = ((UserInfoDataRequest)localObject);
  }
  
  private final void a(CreditDocumentType paramCreditDocumentType)
  {
    Object localObject1 = (m.b)b;
    if (localObject1 == null) {
      return;
    }
    if (paramCreditDocumentType != null)
    {
      c = paramCreditDocumentType;
      boolean bool = j();
      if (bool)
      {
        ((m.b)localObject1).k();
        localObject2 = ((m.b)localObject1).h();
        if (localObject2 != null) {
          ((m.b)localObject1).l();
        }
      }
      else
      {
        ((m.b)localObject1).l();
      }
      Object localObject2 = g;
      int m = R.string.credit_hint_postfix_number;
      Object[] arrayOfObject = new Object[1];
      String str = b;
      arrayOfObject[0] = str;
      localObject2 = ((n)localObject2).a(m, arrayOfObject);
      k.a(localObject2, "resourceProvider.getStri…_postfix_number, it.hint)");
      ((m.b)localObject1).j((String)localObject2);
      localObject1 = d;
      paramCreditDocumentType = f;
      ((UserInfoDataRequest)localObject1).setType(paramCreditDocumentType);
      return;
    }
  }
  
  private final void a(UserInfoDataRequest paramUserInfoDataRequest)
  {
    if (paramUserInfoDataRequest != null)
    {
      m.b localb = (m.b)b;
      if (localb != null)
      {
        String str = ((Address)paramUserInfoDataRequest.getAddresses().get(0)).getCity();
        localb.a(str);
        str = ((Address)paramUserInfoDataRequest.getAddresses().get(0)).getPincode();
        localb.b(str);
        str = ((Address)paramUserInfoDataRequest.getAddresses().get(0)).getAddress_line_1();
        localb.c(str);
        str = ((Address)paramUserInfoDataRequest.getAddresses().get(0)).getAddress_line_2();
        localb.d(str);
        str = ((Address)paramUserInfoDataRequest.getAddresses().get(0)).getAddress_line_3();
        localb.e(str);
        str = paramUserInfoDataRequest.getIdentifier();
        localb.f(str);
        paramUserInfoDataRequest = ((Address)paramUserInfoDataRequest.getAddresses().get(0)).getState();
        localb.g(paramUserInfoDataRequest);
        return;
      }
      return;
    }
  }
  
  private final void g()
  {
    boolean bool = h();
    if (bool)
    {
      localb = (m.b)b;
      if (localb != null) {
        localb.n();
      }
      return;
    }
    m.b localb = (m.b)b;
    if (localb != null)
    {
      localb.o();
      return;
    }
  }
  
  private final boolean h()
  {
    Object localObject1 = e;
    if (localObject1 != null)
    {
      Object localObject2 = (CharSequence)((Address)localObject1).getCity();
      int m = ((CharSequence)localObject2).length();
      boolean bool1 = true;
      if (m > 0)
      {
        m = 1;
      }
      else
      {
        m = 0;
        localObject2 = null;
      }
      if (m != 0)
      {
        localObject2 = (CharSequence)((Address)localObject1).getAddress_line_1();
        m = ((CharSequence)localObject2).length();
        if (m > 0)
        {
          m = 1;
        }
        else
        {
          m = 0;
          localObject2 = null;
        }
        if (m != 0)
        {
          localObject2 = (CharSequence)((Address)localObject1).getAddress_line_2();
          m = ((CharSequence)localObject2).length();
          if (m > 0)
          {
            m = 1;
          }
          else
          {
            m = 0;
            localObject2 = null;
          }
          if (m != 0)
          {
            localObject2 = (CharSequence)((Address)localObject1).getAddress_line_3();
            m = ((CharSequence)localObject2).length();
            if (m > 0)
            {
              m = 1;
            }
            else
            {
              m = 0;
              localObject2 = null;
            }
            if (m != 0)
            {
              localObject2 = ((Address)localObject1).getPincode();
              if (localObject2 != null)
              {
                localObject2 = (CharSequence)m.b((CharSequence)localObject2).toString();
                m = ((CharSequence)localObject2).length();
                if (m > 0)
                {
                  m = 1;
                }
                else
                {
                  m = 0;
                  localObject2 = null;
                }
                if (m != 0)
                {
                  localObject1 = (CharSequence)((Address)localObject1).getState();
                  int n = ((CharSequence)localObject1).length();
                  if (n > 0)
                  {
                    n = 1;
                  }
                  else
                  {
                    n = 0;
                    localObject1 = null;
                  }
                  if (n != 0)
                  {
                    localObject1 = d.getBirth_date();
                    long l1 = Long.parseLong((String)localObject1);
                    long l2 = -1;
                    boolean bool2 = l1 < l2;
                    if (bool2)
                    {
                      bool2 = i();
                      if (bool2) {
                        return bool1;
                      }
                    }
                  }
                }
              }
              else
              {
                localObject1 = new c/u;
                ((u)localObject1).<init>("null cannot be cast to non-null type kotlin.CharSequence");
                throw ((Throwable)localObject1);
              }
            }
          }
        }
      }
      return false;
    }
    return false;
  }
  
  private final boolean i()
  {
    Object localObject1 = d.getIdentifier();
    boolean bool1 = j();
    boolean bool2 = true;
    int n;
    if (!bool1)
    {
      localObject1 = (CharSequence)localObject1;
      n = ((CharSequence)localObject1).length();
      if (n > 0) {
        return bool2;
      }
      return false;
    }
    Object localObject2 = localObject1;
    localObject2 = (CharSequence)localObject1;
    boolean bool3 = m.a((CharSequence)localObject2);
    if (!bool3)
    {
      bool3 = m.a((CharSequence)localObject2);
      int i2 = 12;
      if (!bool3)
      {
        int i1 = ((String)localObject1).length();
        if (i1 < i2) {}
      }
      else
      {
        bool1 = m.a((CharSequence)localObject2);
        if (!bool1)
        {
          n = ((String)localObject1).length();
          if (n == i2)
          {
            localObject1 = (m.b)b;
            if (localObject1 != null) {
              ((m.b)localObject1).p();
            }
            return bool2;
          }
        }
        localObject1 = (m.b)b;
        if (localObject1 != null)
        {
          localObject2 = g;
          int m = R.string.credit_error_invalid_aadhaar;
          Object[] arrayOfObject = new Object[0];
          localObject2 = ((n)localObject2).a(m, arrayOfObject);
          String str = "resourceProvider.getStri…it_error_invalid_aadhaar)";
          k.a(localObject2, str);
          ((m.b)localObject1).l((String)localObject2);
        }
        return false;
      }
    }
    localObject1 = (m.b)b;
    if (localObject1 != null) {
      ((m.b)localObject1).p();
    }
    return false;
  }
  
  private final boolean j()
  {
    Object localObject = c;
    String str1 = null;
    if (localObject != null)
    {
      localObject = f;
    }
    else
    {
      bool = false;
      localObject = null;
    }
    String str2 = "aadhaar_card";
    boolean bool = k.a(localObject, str2);
    if (!bool)
    {
      localObject = c;
      if (localObject != null) {
        str1 = f;
      }
      localObject = "aadhaar_full";
      bool = k.a(str1, localObject);
      if (!bool) {
        return false;
      }
    }
    return true;
  }
  
  public final void a()
  {
    Object localObject = new com/truecaller/credit/app/ui/assist/QRScan;
    int m = R.string.credit_qr_scanning_help_text;
    String str = "qr_scan";
    int n = R.color.black_50;
    ((QRScan)localObject).<init>(m, str, n);
    m.b localb = (m.b)b;
    if (localb != null)
    {
      localObject = (CreditDocumentType)localObject;
      localb.a((CreditDocumentType)localObject, "back");
      return;
    }
  }
  
  public final void a(int paramInt1, int paramInt2, int paramInt3)
  {
    GregorianCalendar localGregorianCalendar = new java/util/GregorianCalendar;
    localGregorianCalendar.<init>(paramInt1, paramInt2, paramInt3);
    Object localObject1 = new java/text/SimpleDateFormat;
    Object localObject2 = Locale.ENGLISH;
    ((SimpleDateFormat)localObject1).<init>("dd/MM/yyyy", (Locale)localObject2);
    Object localObject3 = localGregorianCalendar.getTime();
    localObject1 = ((SimpleDateFormat)localObject1).format((Date)localObject3);
    localObject3 = d;
    long l1 = localGregorianCalendar.getTimeInMillis();
    long l2 = 1000L;
    l1 /= l2;
    localObject2 = String.valueOf(l1);
    ((UserInfoDataRequest)localObject3).setBirth_date((String)localObject2);
    localObject3 = (m.b)b;
    if (localObject3 != null)
    {
      localObject1 = ((String)localObject1).toString();
      ((m.b)localObject3).k((String)localObject1);
    }
    g();
  }
  
  public final void a(int paramInt1, int paramInt2, String paramString)
  {
    Object[] arrayOfObject = null;
    int m = 13;
    Object localObject2;
    int n;
    if (paramInt1 == m)
    {
      paramInt1 = -1;
      if (paramInt2 == paramInt1)
      {
        if (paramString != null)
        {
          localObject1 = l.a(paramString);
          if (localObject1 == null)
          {
            localObject1 = (m.b)b;
            if (localObject1 != null)
            {
              localObject2 = g;
              n = R.string.credit_qr_code_scan_failed;
              arrayOfObject = new Object[0];
              localObject2 = ((n)localObject2).a(n, arrayOfObject);
              k.a(localObject2, "resourceProvider.getStri…edit_qr_code_scan_failed)");
              ((m.b)localObject1).h((String)localObject2);
              return;
            }
            return;
          }
          a((UserInfoDataRequest)localObject1);
        }
        return;
      }
    }
    Object localObject1 = (m.b)b;
    if (localObject1 != null)
    {
      localObject2 = g;
      n = R.string.credit_qr_code_scan_failed;
      arrayOfObject = new Object[0];
      localObject2 = ((n)localObject2).a(n, arrayOfObject);
      k.a(localObject2, "resourceProvider.getStri…edit_qr_code_scan_failed)");
      ((m.b)localObject1).h((String)localObject2);
      return;
    }
  }
  
  public final void a(Integer paramInteger, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8)
  {
    v localv = this;
    String str = paramString7;
    Object localObject1 = paramString8;
    k.b(paramString1, "cityVal");
    k.b(paramString2, "addressLine1");
    k.b(paramString3, "addressLine2");
    k.b(paramString4, "addressLine3");
    k.b(paramString5, "pincodeVal");
    k.b(paramString6, "stateVal");
    k.b(paramString7, "identifier");
    k.b(paramString8, "name");
    Object localObject2 = e;
    Object localObject3;
    if (localObject2 == null)
    {
      localObject3 = new com/truecaller/credit/data/models/Address;
      localObject2 = "permanent";
      localObject1 = localObject3;
      ((Address)localObject3).<init>((String)localObject2, paramString2, paramString3, paramString4, paramString1, paramString5, paramString6);
      e = ((Address)localObject3);
    }
    else
    {
      if (localObject2 != null)
      {
        localObject3 = "permanent";
        ((Address)localObject2).setAddress_type((String)localObject3);
        ((Address)localObject2).setCity(paramString1);
        ((Address)localObject2).setAddress_line_1(paramString2);
        ((Address)localObject2).setAddress_line_2(paramString3);
        ((Address)localObject2).setAddress_line_3(paramString4);
        ((Address)localObject2).setPincode(paramString5);
        ((Address)localObject2).setState(paramString6);
      }
      localObject2 = d;
      ((UserInfoDataRequest)localObject2).setFull_name((String)localObject1);
    }
    int m = R.id.textAadhaarNumber;
    if (paramInteger != null)
    {
      int n = paramInteger.intValue();
      if (n == m)
      {
        localObject1 = d;
        ((UserInfoDataRequest)localObject1).setIdentifier(str);
        boolean bool = j();
        if (bool) {
          i();
        }
      }
    }
    g();
  }
  
  public final void e()
  {
    boolean bool = h();
    if (bool)
    {
      Object localObject1 = e;
      if (localObject1 != null)
      {
        ((Address)localObject1).setAddress_type("permanent");
        Object localObject2 = d.getAddresses();
        ((List)localObject2).add(localObject1);
        localObject1 = (m.b)b;
        if (localObject1 != null)
        {
          localObject2 = d;
          ((m.b)localObject1).a((UserInfoDataRequest)localObject2);
          return;
        }
        return;
      }
    }
  }
  
  public final void f()
  {
    Object localObject1 = Calendar.getInstance();
    k.a(localObject1, "c");
    j localj = k;
    long l1 = localj.a();
    ((Calendar)localObject1).setTimeInMillis(l1);
    int m = ((Calendar)localObject1).get(1);
    int n = ((Calendar)localObject1).get(2);
    int i1 = 5;
    int i2 = ((Calendar)localObject1).get(i1);
    localObject1 = b;
    Object localObject2 = localObject1;
    localObject2 = (m.b)localObject1;
    if (localObject2 != null)
    {
      long l2 = k.a();
      ((m.b)localObject2).a(m, n, i2, l2);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.b.v
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */