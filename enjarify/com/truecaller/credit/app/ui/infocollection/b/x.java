package com.truecaller.credit.app.ui.infocollection.b;

import c.d.f;
import c.g.b.k;
import c.t;
import com.truecaller.ba;
import com.truecaller.credit.R.string;
import com.truecaller.credit.app.a.a;
import com.truecaller.credit.app.a.a.a;
import com.truecaller.credit.app.a.b;
import com.truecaller.credit.app.ui.infocollection.views.c.o.a;
import com.truecaller.credit.app.ui.infocollection.views.c.o.b;
import com.truecaller.credit.app.util.j;
import com.truecaller.credit.app.util.j.a;
import com.truecaller.credit.data.repository.CreditRepository;

public final class x
  extends ba
  implements o.a
{
  private String c;
  private String d;
  private String e;
  private final f f;
  private final f g;
  private final j h;
  private final com.truecaller.utils.n i;
  private final CreditRepository j;
  private final b k;
  
  public x(f paramf1, f paramf2, j paramj, com.truecaller.utils.n paramn, CreditRepository paramCreditRepository, b paramb)
  {
    super(paramf1);
    f = paramf1;
    g = paramf2;
    h = paramj;
    i = paramn;
    j = paramCreditRepository;
    k = paramb;
  }
  
  private final String a(String paramString)
  {
    j localj = h;
    if (paramString != null)
    {
      Object localObject = j.a;
      localObject = j.a.b();
      paramString = localj.a(paramString, (String)localObject);
    }
    else
    {
      paramString = null;
    }
    if (paramString != null) {
      return h.a(paramString, "MMMM dd, yyyy");
    }
    return null;
  }
  
  private final void a(String paramString1, String paramString2, String paramString3)
  {
    o.b localb = (o.b)b;
    if (localb != null)
    {
      paramString2 = a(paramString2);
      Object localObject = i;
      int m = R.string.symbol_dot;
      Object[] arrayOfObject = new Object[0];
      localObject = ((com.truecaller.utils.n)localObject).a(m, arrayOfObject);
      k.a(localObject, "resourceProvider.getString(R.string.symbol_dot)");
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localStringBuilder.append(paramString2);
      char c1 = ' ';
      localStringBuilder.append(c1);
      localStringBuilder.append((String)localObject);
      localStringBuilder.append(c1);
      localStringBuilder.append(paramString3);
      paramString2 = localStringBuilder.toString();
      localb.b(paramString2);
      if (paramString1 != null)
      {
        localb.c(paramString1);
        return;
      }
      return;
    }
  }
  
  public final void a()
  {
    o.b localb = (o.b)b;
    if (localb != null)
    {
      localb.k();
      return;
    }
  }
  
  public final void e()
  {
    String str = "meeting_confirmed";
    Object localObject1 = "CreditScheduleVisit";
    a.a locala = new com/truecaller/credit/app/a/a$a;
    locala.<init>((String)localObject1, (String)localObject1);
    int m = 3;
    localObject1 = new c.n[m];
    Object localObject2 = t.a("Status", "clicked");
    localObject1[0] = localObject2;
    localObject2 = t.a("Action", "reschedule");
    localObject1[1] = localObject2;
    localObject2 = t.a("Context", str);
    int n = 2;
    localObject1[n] = localObject2;
    locala.a((c.n[])localObject1);
    localObject2 = locala.a();
    a = false;
    b localb = k;
    localObject2 = ((a.a)localObject2).b();
    localb.a((a)localObject2);
    localObject2 = (o.b)b;
    if (localObject2 != null)
    {
      ((o.b)localObject2).d("kyc");
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.b.x
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */