package com.truecaller.credit.app.ui.infocollection.b;

import android.graphics.Bitmap;
import android.net.Uri;
import c.d.a.a;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.credit.R.string;
import com.truecaller.credit.app.ui.assist.CreditDocumentType;
import com.truecaller.credit.app.ui.infocollection.views.c.p.b;
import com.truecaller.utils.n;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

final class z$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag e;
  
  z$a(z paramz, Uri paramUri, CreditDocumentType paramCreditDocumentType, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/credit/app/ui/infocollection/b/z$a;
    z localz = b;
    Uri localUri = c;
    CreditDocumentType localCreditDocumentType = d;
    locala.<init>(localz, localUri, localCreditDocumentType, paramc);
    paramObject = (ag)paramObject;
    e = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    boolean bool2;
    Object localObject2;
    Object localObject3;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label677;
      }
      paramObject = z.a(b);
      localObject2 = new com/truecaller/credit/app/ui/infocollection/b/z$a$a;
      localObject3 = null;
      ((z.a.a)localObject2).<init>(this, null);
      localObject2 = (m)localObject2;
      k = 1;
      a = k;
      paramObject = g.a((f)paramObject, (m)localObject2, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (Bitmap)paramObject;
    localObject1 = d.g;
    int j = ((String)localObject1).hashCode();
    int k = 110749;
    Object[] arrayOfObject1 = null;
    if (j != k)
    {
      k = 754210136;
      Object[] arrayOfObject2;
      if (j != k)
      {
        k = 754348800;
        if (j != k)
        {
          k = 1909890008;
          if (j == k)
          {
            localObject2 = "aadhaar_front";
            bool2 = ((String)localObject1).equals(localObject2);
            if (bool2)
            {
              localObject1 = z.b(b);
              if (localObject1 != null)
              {
                localObject2 = z.c(b);
                k = R.string.credit_aadhaar_front_uploaded_successfully;
                arrayOfObject2 = new Object[0];
                localObject2 = ((n)localObject2).a(k, arrayOfObject2);
                c.g.b.k.a(localObject2, "resourceProvider.getStri…nt_uploaded_successfully)");
                ((p.b)localObject1).a((Bitmap)paramObject, (String)localObject2);
                paramObject = z.c(b);
                j = R.string.credit_back_capture_button;
                localObject3 = new Object[0];
                paramObject = ((n)paramObject).a(j, (Object[])localObject3);
                localObject2 = "resourceProvider.getStri…edit_back_capture_button)";
                c.g.b.k.a(paramObject, (String)localObject2);
                ((p.b)localObject1).c((String)paramObject);
              }
              paramObject = b;
              z.e((z)paramObject);
            }
          }
        }
        else
        {
          localObject2 = "aadhaar_full";
          bool2 = ((String)localObject1).equals(localObject2);
          if (bool2)
          {
            localObject1 = z.b(b);
            if (localObject1 != null)
            {
              localObject2 = z.c(b);
              k = R.string.upload_success;
              arrayOfObject1 = new Object[0];
              localObject2 = ((n)localObject2).a(k, arrayOfObject1);
              localObject3 = "resourceProvider.getStri…(R.string.upload_success)";
              c.g.b.k.a(localObject2, (String)localObject3);
              ((p.b)localObject1).c((Bitmap)paramObject, (String)localObject2);
              ((p.b)localObject1).h();
            }
            paramObject = b;
            z.g((z)paramObject);
          }
        }
      }
      else
      {
        localObject2 = "aadhaar_back";
        bool2 = ((String)localObject1).equals(localObject2);
        if (bool2)
        {
          localObject1 = z.b(b);
          if (localObject1 != null)
          {
            localObject2 = z.c(b);
            k = R.string.all_continue;
            arrayOfObject2 = new Object[0];
            localObject2 = ((n)localObject2).a(k, arrayOfObject2);
            c.g.b.k.a(localObject2, "resourceProvider.getString(R.string.all_continue)");
            ((p.b)localObject1).c((String)localObject2);
            localObject2 = z.c(b);
            k = R.string.upload_success;
            arrayOfObject1 = new Object[0];
            localObject2 = ((n)localObject2).a(k, arrayOfObject1);
            localObject3 = "resourceProvider.getStri…(R.string.upload_success)";
            c.g.b.k.a(localObject2, (String)localObject3);
            ((p.b)localObject1).b((Bitmap)paramObject, (String)localObject2);
            ((p.b)localObject1).h();
          }
          paramObject = b;
          z.f((z)paramObject);
        }
      }
    }
    else
    {
      localObject2 = "pan";
      bool2 = ((String)localObject1).equals(localObject2);
      if (bool2)
      {
        localObject1 = z.b(b);
        if (localObject1 != null)
        {
          localObject2 = z.c(b);
          k = R.string.credit_pan_uploaded_successfully;
          arrayOfObject1 = new Object[0];
          localObject2 = ((n)localObject2).a(k, arrayOfObject1);
          localObject3 = "resourceProvider.getStri…an_uploaded_successfully)";
          c.g.b.k.a(localObject2, (String)localObject3);
          ((p.b)localObject1).d((Bitmap)paramObject, (String)localObject2);
          ((p.b)localObject1).j();
        }
        paramObject = b;
        z.d((z)paramObject);
      }
    }
    return x.a;
    label677:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.b.z.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */