package com.truecaller.credit.app.ui.infocollection.b;

import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.credit.R.string;
import com.truecaller.credit.app.ui.infocollection.views.c.f.b;
import com.truecaller.credit.domain.interactors.infocollection.models.IFSCDetails;
import com.truecaller.utils.n;
import kotlinx.coroutines.ag;

final class h$f
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  h$f(h paramh, IFSCDetails paramIFSCDetails, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    f localf = new com/truecaller/credit/app/ui/infocollection/b/h$f;
    h localh = b;
    IFSCDetails localIFSCDetails = c;
    localf.<init>(localh, localIFSCDetails, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return localf;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = b;
        localObject1 = c;
        d = ((IFSCDetails)localObject1);
        paramObject = h.b((h)paramObject);
        if (paramObject != null)
        {
          localObject1 = c.getIfsc();
          ((f.b)paramObject).j((String)localObject1);
          localObject1 = h.c(b);
          int j = R.string.ifsc_search_value;
          int k = 3;
          Object[] arrayOfObject = new Object[k];
          String str1 = c.getBank();
          arrayOfObject[0] = str1;
          n localn = h.c(b);
          int m = R.string.symbol_comma;
          Object localObject2 = new Object[0];
          localObject2 = localn.a(m, (Object[])localObject2);
          arrayOfObject[1] = localObject2;
          int n = 2;
          localObject2 = c.getBranch();
          arrayOfObject[n] = localObject2;
          localObject1 = ((n)localObject1).a(j, arrayOfObject);
          String str2 = "resourceProvider.getStri…nch\n                    )";
          c.g.b.k.a(localObject1, str2);
          ((f.b)paramObject).h((String)localObject1);
        }
        h.h(b);
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (f)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((f)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.b.h.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */