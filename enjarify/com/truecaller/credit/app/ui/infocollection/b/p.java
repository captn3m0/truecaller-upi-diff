package com.truecaller.credit.app.ui.infocollection.b;

import android.content.Context;
import c.d.f;
import com.truecaller.ba;
import com.truecaller.credit.app.ui.infocollection.views.c.j.a;
import com.truecaller.credit.app.util.m;
import com.truecaller.credit.data.repository.CreditRepository;
import com.truecaller.utils.a;
import com.truecaller.utils.n;

public final class p
  extends ba
  implements j.a
{
  private final f c;
  private final f d;
  private final Context e;
  private final CreditRepository f;
  private final m g;
  private final a h;
  private final n i;
  
  public p(f paramf1, f paramf2, Context paramContext, CreditRepository paramCreditRepository, m paramm, a parama, n paramn)
  {
    super(paramf1);
    c = paramf1;
    d = paramf2;
    e = paramContext;
    f = paramCreditRepository;
    g = paramm;
    h = parama;
    i = paramn;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.b.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */