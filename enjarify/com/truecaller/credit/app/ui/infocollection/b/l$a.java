package com.truecaller.credit.app.ui.infocollection.b;

import android.net.Uri;
import c.d.a.a;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.google.android.gms.vision.CameraSource;
import com.truecaller.credit.R.string;
import com.truecaller.credit.app.ui.infocollection.views.c.h.b;
import com.truecaller.utils.n;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

final class l$a
  extends c.d.b.a.k
  implements m
{
  Object a;
  int b;
  private ag e;
  
  l$a(l paraml, byte[] paramArrayOfByte, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/credit/app/ui/infocollection/b/l$a;
    l locall = c;
    byte[] arrayOfByte = d;
    locala.<init>(locall, arrayOfByte, paramc);
    paramObject = (ag)paramObject;
    e = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = b;
    int j = 1;
    boolean bool2;
    Object localObject2;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label338;
      }
      paramObject = d;
      if (paramObject == null) {
        break label334;
      }
      localObject2 = l.a(c);
      Object localObject3 = new com/truecaller/credit/app/ui/infocollection/b/l$a$a;
      ((l.a.a)localObject3).<init>((byte[])paramObject, null, this);
      localObject3 = (m)localObject3;
      a = paramObject;
      b = j;
      paramObject = g.a((f)localObject2, (m)localObject3, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = l.g(c);
    if (paramObject != null)
    {
      bool2 = false;
      localObject1 = null;
      ((h.b)paramObject).d(false);
      ((h.b)paramObject).e(j);
      ((h.b)paramObject).h(false);
      ((h.b)paramObject).g(j);
      ((h.b)paramObject).f(j);
      ((h.b)paramObject).c(false);
      ((h.b)paramObject).b(j);
      ((h.b)paramObject).a(false);
      localObject2 = l.f(c);
      ((h.b)paramObject).a((Uri)localObject2);
      ((h.b)paramObject).k();
      localObject2 = l.f(c);
      if (localObject2 == null)
      {
        localObject2 = l.h(c);
        int k = R.string.credit_image_capture_failed_try_again;
        localObject1 = new Object[0];
        localObject1 = ((n)localObject2).a(k, (Object[])localObject1);
        localObject2 = "resourceProvider.getStri…capture_failed_try_again)";
        c.g.b.k.a(localObject1, (String)localObject2);
        ((h.b)paramObject).b((String)localObject1);
        ((h.b)paramObject).l();
      }
    }
    paramObject = c.c;
    if (paramObject == null)
    {
      localObject1 = "camera";
      c.g.b.k.a((String)localObject1);
    }
    ((CameraSource)paramObject).b();
    label334:
    return x.a;
    label338:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.b.l.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */