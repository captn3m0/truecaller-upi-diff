package com.truecaller.credit.app.ui.infocollection.b;

import android.graphics.drawable.Drawable;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.credit.R.color;
import com.truecaller.credit.R.drawable;
import com.truecaller.credit.app.ui.assist.CreditDocumentType;
import com.truecaller.credit.app.ui.infocollection.views.c.m.b;
import com.truecaller.credit.data.Failure;
import com.truecaller.credit.data.Result;
import com.truecaller.credit.data.Success;
import com.truecaller.credit.domain.interactors.infocollection.models.PoaDetails;
import com.truecaller.utils.n;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

final class v$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  v$a(v paramv, m.b paramb, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/credit/app/ui/infocollection/b/v$a;
    v localv = b;
    m.b localb = c;
    locala.<init>(localv, localb, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    Object localObject2;
    Object localObject3;
    int j;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label307;
      }
      paramObject = v.a(b);
      localObject2 = new com/truecaller/credit/app/ui/infocollection/b/v$a$a;
      localObject3 = null;
      ((v.a.a)localObject2).<init>(this, null);
      localObject2 = (m)localObject2;
      j = 1;
      a = j;
      paramObject = g.a((c.d.f)paramObject, (m)localObject2, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (Result)paramObject;
    boolean bool2 = paramObject instanceof Success;
    if (bool2)
    {
      localObject1 = b;
      paramObject = (Success)paramObject;
      localObject2 = ((PoaDetails)((Success)paramObject).getData()).getType();
      localObject3 = ((PoaDetails)((Success)paramObject).getData()).getHint();
      localObject2 = v.a((String)localObject2, (String)localObject3);
      v.a((v)localObject1, (CreditDocumentType)localObject2);
      localObject1 = c;
      paramObject = ((PoaDetails)((Success)paramObject).getData()).getName();
      localObject2 = v.c(b);
      j = R.drawable.ic_tick_active;
      localObject2 = ((n)localObject2).c(j);
      c.g.b.k.a(localObject2, "resourceProvider.getDraw….drawable.ic_tick_active)");
      localObject3 = v.d(b);
      int k = R.color.pale_grey;
      j = ((com.truecaller.credit.app.util.f)localObject3).a(k);
      ((m.b)localObject1).a((String)paramObject, (Drawable)localObject2, j);
    }
    else
    {
      boolean bool3 = paramObject instanceof Failure;
      if (bool3)
      {
        paramObject = c;
        ((m.b)paramObject).m();
      }
    }
    return x.a;
    label307:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.b.v.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */