package com.truecaller.credit.app.ui.infocollection.b;

import c.d.a.a;
import c.d.c;
import c.d.f;
import c.g.b.k;
import c.o.b;
import c.t;
import c.x;
import com.truecaller.ba;
import com.truecaller.credit.R.string;
import com.truecaller.credit.app.a.a.a;
import com.truecaller.credit.app.a.b;
import com.truecaller.credit.app.ui.infocollection.views.c.f.a;
import com.truecaller.credit.app.ui.infocollection.views.c.f.b;
import com.truecaller.credit.data.Result;
import com.truecaller.credit.data.Success;
import com.truecaller.credit.data.models.BankAccountDetails;
import com.truecaller.credit.data.repository.CreditRepository;
import com.truecaller.credit.domain.interactors.infocollection.models.IFSCDetails;
import com.truecaller.credit.domain.interactors.infocollection.models.IFSCList;
import java.util.List;
import kotlinx.coroutines.e;
import kotlinx.coroutines.g;

public final class h
  extends ba
  implements f.a
{
  BankAccountDetails c;
  IFSCDetails d;
  boolean e;
  private int f;
  private String g;
  private String h;
  private boolean i;
  private final f j;
  private final f k;
  private final CreditRepository l;
  private final com.truecaller.utils.n m;
  private final b n;
  
  public h(f paramf1, f paramf2, CreditRepository paramCreditRepository, com.truecaller.utils.n paramn, b paramb)
  {
    super(paramf1);
    j = paramf1;
    k = paramf2;
    l = paramCreditRepository;
    m = paramn;
    n = paramb;
  }
  
  private final void a(BankAccountDetails paramBankAccountDetails)
  {
    Object localObject1;
    if (paramBankAccountDetails != null) {
      localObject1 = paramBankAccountDetails.getLogo();
    } else {
      localObject1 = null;
    }
    if (localObject1 != null)
    {
      localObject1 = paramBankAccountDetails.getName();
      if (localObject1 != null)
      {
        localObject1 = paramBankAccountDetails.getLast_4_digit();
        if (localObject1 != null)
        {
          localObject1 = (f.b)b;
          if (localObject1 != null) {
            ((f.b)localObject1).x();
          }
          localObject1 = b;
          Object localObject2 = localObject1;
          localObject2 = (f.b)localObject1;
          if (localObject2 != null)
          {
            String str1 = paramBankAccountDetails.getLogo().toString();
            String str2 = paramBankAccountDetails.getName().toString();
            String str3 = paramBankAccountDetails.getLast_4_digit().toString();
            localObject1 = m;
            int i1 = R.string.masked_account_number;
            int i2 = 1;
            Object[] arrayOfObject = new Object[i2];
            String str4 = paramBankAccountDetails.getLast_4_digit();
            arrayOfObject[0] = str4;
            String str5 = ((com.truecaller.utils.n)localObject1).a(i1, arrayOfObject);
            k.a(str5, "resourceProvider.getStri…ountDetails.last_4_digit)");
            localObject1 = m;
            int i3 = R.string.text_enter_your_account_number_ending_in;
            Object localObject3 = new Object[i2];
            paramBankAccountDetails = paramBankAccountDetails.getLast_4_digit();
            localObject3[0] = paramBankAccountDetails;
            localObject3 = ((com.truecaller.utils.n)localObject1).a(i3, (Object[])localObject3);
            paramBankAccountDetails = "resourceProvider.getStri…ountDetails.last_4_digit)";
            k.a(localObject3, paramBankAccountDetails);
            ((f.b)localObject2).a(str1, str2, str3, str5, (String)localObject3);
          }
          return;
        }
      }
    }
    paramBankAccountDetails = (f.b)b;
    if (paramBankAccountDetails != null)
    {
      paramBankAccountDetails.x();
      return;
    }
  }
  
  private final void b(String paramString1, String paramString2, String paramString3)
  {
    a.a locala = new com/truecaller/credit/app/a/a$a;
    locala.<init>("CreditBankDetails");
    c.n[] arrayOfn = new c.n[4];
    paramString1 = t.a("Status", paramString1);
    arrayOfn[0] = paramString1;
    paramString1 = t.a("Action", paramString2);
    arrayOfn[1] = paramString1;
    paramString1 = t.a("Custom", paramString3);
    arrayOfn[2] = paramString1;
    paramString1 = t.a("Context", null);
    arrayOfn[3] = paramString1;
    locala.a(arrayOfn);
    locala.a();
    paramString1 = n;
    paramString2 = locala.b();
    paramString1.a(paramString2);
  }
  
  private boolean c(String paramString1, String paramString2)
  {
    g = paramString1;
    h = paramString2;
    if ((paramString1 != null) && (paramString2 != null))
    {
      Object localObject1 = c;
      Object localObject2 = null;
      if (localObject1 != null)
      {
        localObject1 = ((BankAccountDetails)localObject1).getLast_4_digit();
      }
      else
      {
        bool1 = false;
        localObject1 = null;
      }
      k.b(paramString1, "enteredAccountNumber");
      Object localObject3 = paramString1;
      localObject3 = (CharSequence)paramString1;
      boolean bool3 = c.n.m.a((CharSequence)localObject3);
      int i4 = 8;
      boolean bool5 = true;
      if (!bool3)
      {
        int i3 = paramString1.length();
        if (i3 >= i4)
        {
          if (localObject1 != null)
          {
            boolean bool4 = c.n.m.c(paramString1, (String)localObject1, bool5);
            if (!bool4)
            {
              bool4 = c.n.m.c(paramString1, (String)localObject1, bool5);
              if (!bool4)
              {
                localObject3 = (f.b)b;
                if (localObject3 != null)
                {
                  Object localObject4 = m;
                  int i5 = R.string.error_bank_account_number_should_match;
                  Object[] arrayOfObject = new Object[bool5];
                  arrayOfObject[0] = localObject1;
                  localObject1 = ((com.truecaller.utils.n)localObject4).a(i5, arrayOfObject);
                  localObject4 = "resourceProvider.getStri…match, lastAccountDigits)";
                  k.a(localObject1, (String)localObject4);
                  ((f.b)localObject3).c((String)localObject1);
                }
                bool1 = false;
                localObject1 = null;
                break label258;
              }
              bool1 = false;
              localObject1 = null;
              break label258;
            }
          }
          localObject1 = (f.b)b;
          if (localObject1 != null) {
            ((f.b)localObject1).i();
          }
          bool1 = true;
          break label258;
        }
      }
      localObject1 = (f.b)b;
      if (localObject1 != null) {
        ((f.b)localObject1).i();
      }
      boolean bool1 = false;
      localObject1 = null;
      label258:
      if (bool1)
      {
        k.b(paramString2, "reEnteredAccountNumber");
        k.b(paramString1, "accountNumber");
        localObject1 = paramString2;
        localObject1 = (CharSequence)paramString2;
        bool1 = c.n.m.a((CharSequence)localObject1);
        if (!bool1)
        {
          int i1 = paramString2.length();
          if (i1 >= i4)
          {
            boolean bool2 = k.a(paramString2, paramString1) ^ bool5;
            if (bool2)
            {
              paramString1 = (f.b)b;
              if (paramString1 != null)
              {
                paramString2 = m;
                int i2 = R.string.error_account_number_doesnt_match;
                localObject3 = new Object[0];
                paramString2 = paramString2.a(i2, (Object[])localObject3);
                localObject1 = "resourceProvider.getStri…ount_number_doesnt_match)";
                k.a(paramString2, (String)localObject1);
                paramString1.d(paramString2);
              }
              bool6 = false;
              paramString1 = null;
              break label453;
            }
            bool6 = k.a(paramString2, paramString1);
            if (bool6)
            {
              paramString1 = (f.b)b;
              if (paramString1 != null) {
                paramString1.j();
              }
              bool6 = true;
              break label453;
            }
            bool6 = false;
            paramString1 = null;
            break label453;
          }
        }
        paramString1 = (f.b)b;
        if (paramString1 != null) {
          paramString1.j();
        }
        boolean bool6 = false;
        paramString1 = null;
        label453:
        if (bool6)
        {
          paramString1 = d;
          if (paramString1 != null) {
            localObject2 = paramString1.getIfsc();
          }
          localObject2 = (CharSequence)localObject2;
          if (localObject2 != null)
          {
            i6 = ((CharSequence)localObject2).length();
            if (i6 != 0)
            {
              i6 = 0;
              paramString1 = null;
              break label513;
            }
          }
          int i6 = 1;
          label513:
          if (i6 == 0)
          {
            paramString1 = (f.b)b;
            if (paramString1 != null) {
              paramString1.n();
            }
            return bool5;
          }
        }
      }
      paramString1 = (f.b)b;
      if (paramString1 != null) {
        paramString1.o();
      }
      return false;
    }
    paramString1 = (f.b)b;
    if (paramString1 != null) {
      paramString1.o();
    }
    return false;
  }
  
  private final void i()
  {
    f.b localb = (f.b)b;
    if (localb != null)
    {
      localb.t();
      localb.v();
      localb.l();
      localb.k();
      return;
    }
  }
  
  public final Object a(String paramString, c paramc)
  {
    boolean bool1 = paramc instanceof h.b;
    int i2;
    if (bool1)
    {
      localObject1 = paramc;
      localObject1 = (h.b)paramc;
      i2 = b;
      i3 = -1 << -1;
      i2 &= i3;
      if (i2 != 0)
      {
        int i5 = b - i3;
        b = i5;
        break label77;
      }
    }
    Object localObject1 = new com/truecaller/credit/app/ui/infocollection/b/h$b;
    ((h.b)localObject1).<init>(this, (c)paramc);
    label77:
    paramc = a;
    Object localObject2 = a.a;
    int i3 = b;
    int i7 = 0;
    Object[] arrayOfObject1 = null;
    int i8 = 1;
    int i6;
    int i4;
    Object localObject3;
    switch (i3)
    {
    default: 
      paramString = new java/lang/IllegalStateException;
      paramString.<init>("call to 'resume' before 'invoke' with coroutine");
      throw paramString;
    case 1: 
      paramString = (h)d;
      bool1 = paramc instanceof o.b;
      if (bool1) {
        throw a;
      }
      break;
    case 0: 
      boolean bool2 = paramc instanceof o.b;
      if (bool2) {
        break label840;
      }
      paramc = paramString;
      paramc = (CharSequence)paramString;
      boolean bool3 = c.n.m.a(paramc);
      if (bool3) {
        break label790;
      }
      i6 = paramString.length();
      i4 = 11;
      if (i6 < i4) {
        break label790;
      }
      i6 = paramString.length();
      if (i6 != i4) {
        break label836;
      }
      paramc = (f.b)b;
      if (paramc != null)
      {
        paramc.v();
        paramc.s();
      }
      paramc = k;
      localObject3 = new com/truecaller/credit/app/ui/infocollection/b/h$c;
      ((h.c)localObject3).<init>(this, paramString, null);
      localObject3 = (c.g.a.m)localObject3;
      d = this;
      e = paramString;
      b = i8;
      paramc = g.a(paramc, (c.g.a.m)localObject3, (c)localObject1);
      if (paramc == localObject2) {
        return localObject2;
      }
      paramString = this;
    }
    paramc = (Result)paramc;
    bool1 = paramc instanceof Success;
    if (bool1)
    {
      paramc = (Success)paramc;
      localObject1 = ((IFSCList)paramc.getData()).getResults();
      int i1 = ((List)localObject1).size();
      if (i1 > 0)
      {
        localObject1 = (f.b)b;
        if (localObject1 != null)
        {
          localObject2 = m;
          i4 = R.string.ifsc_search_value;
          i7 = 3;
          arrayOfObject1 = new Object[i7];
          Object localObject4 = ((IFSCDetails)((IFSCList)paramc.getData()).getResults().get(0)).getBank();
          arrayOfObject1[0] = localObject4;
          localObject4 = m;
          int i9 = R.string.symbol_comma;
          Object[] arrayOfObject2 = new Object[0];
          localObject4 = ((com.truecaller.utils.n)localObject4).a(i9, arrayOfObject2);
          arrayOfObject1[i8] = localObject4;
          int i10 = 2;
          String str = ((IFSCDetails)((IFSCList)paramc.getData()).getResults().get(0)).getBranch();
          arrayOfObject1[i10] = str;
          localObject2 = ((com.truecaller.utils.n)localObject2).a(i4, arrayOfObject1);
          localObject3 = "resourceProvider.getStri…                        )";
          k.a(localObject2, (String)localObject3);
          ((f.b)localObject1).h((String)localObject2);
          ((f.b)localObject1).y();
        }
        paramString.i();
        paramc = (IFSCDetails)((IFSCList)paramc.getData()).getResults().get(0);
        d = paramc;
        paramc = (CharSequence)g;
        if (paramc != null)
        {
          i6 = paramc.length();
          if (i6 != 0)
          {
            i6 = 0;
            paramc = null;
            break label607;
          }
        }
        i6 = 1;
        label607:
        if (i6 == 0)
        {
          paramc = (CharSequence)h;
          if (paramc != null)
          {
            i6 = paramc.length();
            if (i6 != 0) {
              i8 = 0;
            }
          }
          if (i8 == 0)
          {
            paramc = g;
            if (paramc == null) {
              k.a();
            }
            localObject1 = h;
            if (localObject1 == null) {
              k.a();
            }
            paramString.a(paramc, (String)localObject1);
          }
        }
      }
      else
      {
        paramc = (f.b)b;
        if (paramc != null)
        {
          paramc.t();
          paramc.u();
          paramc.m();
          localObject1 = m;
          i2 = R.string.error_wrong_ifsc;
          localObject3 = new Object[0];
          localObject1 = ((com.truecaller.utils.n)localObject1).a(i2, (Object[])localObject3);
          localObject2 = "resourceProvider.getStri….string.error_wrong_ifsc)";
          k.a(localObject1, (String)localObject2);
          paramc.e((String)localObject1);
        }
        paramc = (f.b)b;
        if (paramc != null) {
          paramc.o();
        }
        d = null;
        break label836;
        label790:
        paramString = (f.b)b;
        if (paramString != null)
        {
          paramString.u();
          paramString.k();
          paramString.o();
          paramString.m();
        }
        d = null;
        i = false;
      }
    }
    label836:
    return x.a;
    label840:
    throw a;
  }
  
  public final void a()
  {
    boolean bool = e;
    if (bool)
    {
      f.b localb = (f.b)b;
      if (localb != null)
      {
        localb.a("bank_verification");
        localb.p();
        return;
      }
    }
  }
  
  public final void a(IFSCDetails paramIFSCDetails)
  {
    k.b(paramIFSCDetails, "ifscDetails");
    i = true;
    Object localObject = new com/truecaller/credit/app/ui/infocollection/b/h$f;
    ((h.f)localObject).<init>(this, paramIFSCDetails, null);
    localObject = (c.g.a.m)localObject;
    e.b(this, null, (c.g.a.m)localObject, 3);
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "enteredAccountNumber");
    g = paramString;
  }
  
  public final void a(String paramString1, String paramString2)
  {
    k.b(paramString1, "enteredAccountNumber");
    String str = "reEnteredAccountNumber";
    k.b(paramString2, str);
    a(paramString1);
    b(paramString2);
    int i1 = paramString1.length();
    int i2 = 8;
    if (i1 >= i2)
    {
      i1 = paramString2.length();
      if (i1 >= i2)
      {
        paramString1 = d;
        if (paramString1 != null)
        {
          paramString1 = paramString1.getIfsc();
        }
        else
        {
          i1 = 0;
          paramString1 = null;
        }
        paramString1 = (CharSequence)paramString1;
        if (paramString1 != null)
        {
          i1 = paramString1.length();
          if (i1 != 0)
          {
            i1 = 0;
            paramString1 = null;
            break label112;
          }
        }
        i1 = 1;
        label112:
        if (i1 == 0)
        {
          paramString1 = (f.b)b;
          if (paramString1 == null) {
            break label156;
          }
          paramString1.n();
          break label156;
        }
      }
    }
    paramString1 = (f.b)b;
    if (paramString1 != null) {
      paramString1.o();
    }
    label156:
    paramString1 = (f.b)b;
    if (paramString1 != null) {
      paramString1.i();
    }
    paramString1 = (f.b)b;
    if (paramString1 != null)
    {
      paramString1.j();
      return;
    }
  }
  
  public final void a(String paramString1, String paramString2, String paramString3)
  {
    k.b(paramString1, "enteredAccountNumber");
    k.b(paramString2, "reEnteredAccountNumber");
    k.b(paramString3, "ifsc");
    Object localObject = new com/truecaller/credit/app/ui/infocollection/b/h$a;
    ((h.a)localObject).<init>(this, paramString1, paramString2, paramString3, null);
    localObject = (c.g.a.m)localObject;
    e.b(this, null, (c.g.a.m)localObject, 3);
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "reEnteredAccountNumber");
    h = paramString;
  }
  
  public final void b(String paramString1, String paramString2)
  {
    k.b(paramString1, "bankAccountNumber");
    k.b(paramString2, "ifsc");
    Object localObject1 = (f.b)b;
    if (localObject1 != null) {
      ((f.b)localObject1).q();
    }
    localObject1 = (f.b)b;
    if (localObject1 != null)
    {
      Object localObject2 = m;
      int i1 = R.string.bank_verification_in_progress;
      Object[] arrayOfObject = new Object[0];
      localObject2 = ((com.truecaller.utils.n)localObject2).a(i1, arrayOfObject);
      String str = "resourceProvider.getStri…verification_in_progress)";
      k.a(localObject2, str);
      ((f.b)localObject1).f((String)localObject2);
    }
    localObject1 = new com/truecaller/credit/app/ui/infocollection/b/h$e;
    ((h.e)localObject1).<init>(this, paramString1, paramString2, null);
    localObject1 = (c.g.a.m)localObject1;
    e.b(this, null, (c.g.a.m)localObject1, 3);
    b("initiated", "continue", null);
  }
  
  public final void e()
  {
    Object localObject = c;
    if (localObject != null)
    {
      localObject = ((BankAccountDetails)localObject).getName();
      if (localObject != null)
      {
        f.b localb = (f.b)b;
        if (localb != null) {
          localb.i((String)localObject);
        }
        return;
      }
    }
  }
  
  public final void f()
  {
    f.b localb = (f.b)b;
    if (localb != null)
    {
      Object localObject = m;
      int i1 = R.string.credit_button_continue;
      Object[] arrayOfObject = new Object[0];
      localObject = ((com.truecaller.utils.n)localObject).a(i1, arrayOfObject);
      k.a(localObject, "resourceProvider.getStri…g.credit_button_continue)");
      localb.b((String)localObject);
      return;
    }
  }
  
  public final void g()
  {
    h();
  }
  
  public final void h()
  {
    Object localObject = g;
    String str = h;
    boolean bool = c((String)localObject, str);
    if (bool)
    {
      localObject = (f.b)b;
      if (localObject != null)
      {
        ((f.b)localObject).r();
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.b.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */