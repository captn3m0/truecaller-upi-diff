package com.truecaller.credit.app.ui.infocollection.b;

import android.graphics.Bitmap;
import android.net.Uri;
import c.d.a.a;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.credit.R.string;
import com.truecaller.credit.app.ui.infocollection.views.c.s.b;
import com.truecaller.utils.n;
import java.lang.ref.WeakReference;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

final class af$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  af$a(af paramaf, Uri paramUri, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/credit/app/ui/infocollection/b/af$a;
    af localaf = b;
    Uri localUri = c;
    locala.<init>(localaf, localUri, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    Object localObject2 = null;
    Object localObject3;
    int k;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label219;
      }
      paramObject = af.a(b);
      localObject3 = new com/truecaller/credit/app/ui/infocollection/b/af$a$1;
      ((af.a.1)localObject3).<init>(this, null);
      localObject3 = (m)localObject3;
      k = 1;
      a = k;
      paramObject = g.a((f)paramObject, (m)localObject3, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (WeakReference)paramObject;
    localObject1 = af.c(b);
    if (localObject1 != null)
    {
      if (paramObject != null)
      {
        paramObject = ((WeakReference)paramObject).get();
        localObject2 = paramObject;
        localObject2 = (Bitmap)paramObject;
      }
      paramObject = af.d(b);
      int j = R.string.upload_success;
      k = 0;
      Object[] arrayOfObject = new Object[0];
      paramObject = ((n)paramObject).a(j, arrayOfObject);
      localObject3 = "resourceProvider.getStri…(R.string.upload_success)";
      c.g.b.k.a(paramObject, (String)localObject3);
      ((s.b)localObject1).a((Bitmap)localObject2, (String)paramObject);
    }
    return x.a;
    label219:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.b.af.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */