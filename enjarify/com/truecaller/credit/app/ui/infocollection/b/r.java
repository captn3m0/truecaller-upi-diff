package com.truecaller.credit.app.ui.infocollection.b;

import c.d.f;
import c.g.a.m;
import c.g.b.k;
import com.truecaller.ba;
import com.truecaller.credit.R.string;
import com.truecaller.credit.app.ui.infocollection.views.c.k.a;
import com.truecaller.credit.app.ui.infocollection.views.c.k.b;
import com.truecaller.credit.data.repository.CreditRepository;
import com.truecaller.credit.domain.interactors.infocollection.models.IFSCDetails;
import com.truecaller.utils.n;
import kotlinx.coroutines.e;

public final class r
  extends ba
  implements k.a
{
  private boolean c;
  private IFSCDetails d;
  private final f e;
  private final n f;
  private final f g;
  private final CreditRepository h;
  
  public r(f paramf1, n paramn, f paramf2, CreditRepository paramCreditRepository)
  {
    super(paramf1);
    e = paramf1;
    f = paramn;
    g = paramf2;
    h = paramCreditRepository;
  }
  
  public final void a(IFSCDetails paramIFSCDetails)
  {
    k.b(paramIFSCDetails, "ifscDetails");
    String str = "ifscDetails";
    k.b(paramIFSCDetails, str);
    d = paramIFSCDetails;
    boolean bool = true;
    c = bool;
    paramIFSCDetails = (k.b)b;
    if (paramIFSCDetails != null)
    {
      paramIFSCDetails.j();
      return;
    }
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "bankName");
    k.b localb = (k.b)b;
    if (localb != null)
    {
      localb.b(paramString);
      return;
    }
  }
  
  public final void a(String paramString1, String paramString2, String paramString3)
  {
    k.b(paramString1, "bankName");
    k.b(paramString2, "cityDistrict");
    String str = "branchName";
    k.b(paramString3, str);
    boolean bool = c;
    if (bool)
    {
      paramString1 = d;
      if (paramString1 != null)
      {
        paramString2 = (k.b)b;
        if (paramString2 != null)
        {
          paramString2.b(paramString1);
          return;
        }
      }
      return;
    }
    bool = false;
    str = null;
    Object localObject = new com/truecaller/credit/app/ui/infocollection/b/r$a;
    ((r.a)localObject).<init>(this, paramString1, paramString2, paramString3, null);
    localObject = (m)localObject;
    int i = 3;
    e.b(this, null, (m)localObject, i);
    paramString1 = (k.b)b;
    if (paramString1 != null)
    {
      paramString1.t();
      paramString1.q();
      paramString1.p();
      paramString1.m();
      paramString1.u();
      return;
    }
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "branchName");
    k.b localb = (k.b)b;
    if (localb != null)
    {
      paramString = (CharSequence)paramString;
      int i = paramString.length();
      Object localObject = null;
      if (i > 0)
      {
        i = 1;
      }
      else
      {
        i = 0;
        paramString = null;
      }
      if (i != 0)
      {
        boolean bool = c;
        if (bool)
        {
          paramString = f;
          j = R.string.credit_button_continue;
          localObject = new Object[0];
          paramString = paramString.a(j, (Object[])localObject);
          localObject = "resourceProvider.getStri…g.credit_button_continue)";
          k.a(paramString, (String)localObject);
          localb.a(paramString);
        }
        else
        {
          paramString = f;
          j = R.string.text_find;
          localObject = new Object[0];
          paramString = paramString.a(j, (Object[])localObject);
          localObject = "resourceProvider.getString(R.string.text_find)";
          k.a(paramString, (String)localObject);
          localb.a(paramString);
        }
        localb.j();
        return;
      }
      localb.k();
      paramString = f;
      int j = R.string.text_find;
      localObject = new Object[0];
      paramString = paramString.a(j, (Object[])localObject);
      k.a(paramString, "resourceProvider.getString(R.string.text_find)");
      localb.a(paramString);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.b.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */