package com.truecaller.credit.app.ui.infocollection.b;

import android.net.Uri;
import c.d.f;
import c.g.a.m;
import c.g.b.k;
import c.t;
import com.truecaller.ba;
import com.truecaller.credit.R.string;
import com.truecaller.credit.app.a.a;
import com.truecaller.credit.app.a.a.a;
import com.truecaller.credit.app.a.b;
import com.truecaller.credit.app.ui.assist.CreditDocumentType;
import com.truecaller.credit.app.ui.assist.SelfieDocument;
import com.truecaller.credit.app.ui.infocollection.views.c.s.a;
import com.truecaller.credit.app.ui.infocollection.views.c.s.b;
import com.truecaller.credit.app.util.s;
import kotlinx.coroutines.e;

public final class af
  extends ba
  implements s.a
{
  private boolean c;
  private final f d;
  private final f e;
  private final s f;
  private final com.truecaller.utils.n g;
  private final b h;
  
  public af(f paramf1, f paramf2, s params, com.truecaller.utils.n paramn, b paramb)
  {
    super(paramf1);
    d = paramf1;
    e = paramf2;
    f = params;
    g = paramn;
    h = paramb;
  }
  
  public final void a()
  {
    boolean bool = c;
    if (bool)
    {
      localObject1 = (s.b)b;
      if (localObject1 != null)
      {
        localObject2 = "selfie_uploaded";
        ((s.b)localObject1).b((String)localObject2);
      }
      return;
    }
    Object localObject1 = (s.b)b;
    if (localObject1 != null)
    {
      localObject2 = new com/truecaller/credit/app/ui/assist/SelfieDocument;
      int i = R.string.credit_upload_image_selfie;
      ((SelfieDocument)localObject2).<init>("selfie", i);
      localObject2 = (CreditDocumentType)localObject2;
      String str = "front";
      ((s.b)localObject1).a((CreditDocumentType)localObject2, str);
    }
    Object localObject3 = "CreditPersonalInfo";
    a.a locala = new com/truecaller/credit/app/a/a$a;
    locala.<init>((String)localObject3, (String)localObject3);
    localObject3 = new c.n[4];
    localObject1 = t.a("Status", "clicked");
    localObject3[0] = localObject1;
    Object localObject2 = t.a("Type", "selfie");
    localObject3[1] = localObject2;
    localObject2 = t.a("Context", "selfie_details_page");
    localObject3[2] = localObject2;
    localObject2 = t.a("Action", "continue");
    localObject3[3] = localObject2;
    locala.a((c.n[])localObject3);
    localObject1 = locala.a();
    a = false;
    localObject2 = h;
    localObject1 = ((a.a)localObject1).b();
    ((b)localObject2).a((a)localObject1);
  }
  
  public final void a(int paramInt)
  {
    s.b localb = (s.b)b;
    if (localb == null) {
      return;
    }
    Object localObject1 = null;
    Object localObject2;
    switch (paramInt)
    {
    default: 
      break;
    case 2: 
      c = true;
      paramInt = 0;
      localObject2 = null;
      Object localObject3 = g;
      int i = R.string.upload_success;
      localObject1 = new Object[0];
      localObject1 = ((com.truecaller.utils.n)localObject3).a(i, (Object[])localObject1);
      localObject3 = "resourceProvider.getStri…(R.string.upload_success)";
      k.a(localObject1, (String)localObject3);
      localb.a(null, (String)localObject1);
      break;
    case 1: 
      c = false;
      localObject2 = g;
      int j = R.string.upload_failed;
      localObject1 = new Object[0];
      localObject2 = ((com.truecaller.utils.n)localObject2).a(j, (Object[])localObject1);
      k.a(localObject2, "resourceProvider.getString(R.string.upload_failed)");
      localb.a((String)localObject2);
      return;
    }
  }
  
  public final void a(Uri paramUri)
  {
    Object localObject;
    if (paramUri == null)
    {
      paramUri = (s.b)b;
      if (paramUri != null)
      {
        localObject = g;
        int i = R.string.upload_success;
        Object[] arrayOfObject = new Object[0];
        localObject = ((com.truecaller.utils.n)localObject).a(i, arrayOfObject);
        String str = "resourceProvider.getStri…(R.string.upload_success)";
        k.a(localObject, str);
        paramUri.a(null, (String)localObject);
      }
    }
    else
    {
      localObject = new com/truecaller/credit/app/ui/infocollection/b/af$a;
      ((af.a)localObject).<init>(this, paramUri, null);
      localObject = (m)localObject;
      int j = 3;
      e.b(this, null, (m)localObject, j);
    }
    c = true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.b.af
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */