package com.truecaller.credit.app.ui.infocollection.b;

import android.net.Uri;
import android.util.SparseArray;
import c.g.a.m;
import c.g.b.k;
import c.u;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.CameraSource.PictureCallback;
import com.google.android.gms.vision.Detector.Detections;
import com.google.android.gms.vision.Detector.Processor;
import com.google.android.gms.vision.barcode.Barcode;
import com.truecaller.ba;
import com.truecaller.credit.app.ui.assist.CreditDocumentType;
import com.truecaller.credit.app.ui.infocollection.views.c.h.a;
import com.truecaller.credit.app.ui.infocollection.views.c.h.b;
import com.truecaller.credit.app.util.BitmapConverter;
import com.truecaller.credit.app.util.c;
import com.truecaller.credit.app.util.s;
import com.truecaller.utils.n;
import kotlinx.coroutines.e;

public final class l
  extends ba
  implements CameraSource.PictureCallback, Detector.Processor, h.a
{
  CameraSource c;
  private Uri d;
  private String e;
  private String f;
  private CreditDocumentType g;
  private final c.d.f h;
  private final c.d.f i;
  private final s j;
  private final c k;
  private final BitmapConverter l;
  private final n m;
  private final com.truecaller.credit.app.util.f n;
  
  public l(c.d.f paramf1, c.d.f paramf2, s params, c paramc, BitmapConverter paramBitmapConverter, n paramn, com.truecaller.credit.app.util.f paramf)
  {
    super(paramf1);
    h = paramf1;
    i = paramf2;
    j = params;
    k = paramc;
    l = paramBitmapConverter;
    m = paramn;
    n = paramf;
  }
  
  public final void a() {}
  
  public final void a(Detector.Detections paramDetections)
  {
    paramDetections = paramDetections.a();
    int i1 = paramDetections.size();
    if (i1 != 0)
    {
      i1 = 0;
      String str = null;
      paramDetections = valueAt0c;
      e = paramDetections;
      paramDetections = g;
      if (paramDetections != null)
      {
        paramDetections = f;
      }
      else
      {
        bool = false;
        paramDetections = null;
      }
      str = "qr_scan";
      boolean bool = k.a(paramDetections, str);
      if (bool)
      {
        paramDetections = (h.b)b;
        if (paramDetections != null)
        {
          str = e;
          if (str != null)
          {
            paramDetections.c(str);
            return;
          }
          paramDetections = new c/u;
          paramDetections.<init>("null cannot be cast to non-null type kotlin.String");
          throw paramDetections;
        }
      }
    }
  }
  
  public final void a(byte[] paramArrayOfByte)
  {
    c.d.f localf = h;
    Object localObject = new com/truecaller/credit/app/ui/infocollection/b/l$a;
    ((l.a)localObject).<init>(this, paramArrayOfByte, null);
    localObject = (m)localObject;
    e.b(this, localf, (m)localObject, 2);
  }
  
  public final void e()
  {
    h.b localb = (h.b)b;
    if (localb != null)
    {
      boolean bool1 = true;
      localb.a(bool1);
      localb.h(bool1);
      boolean bool2 = false;
      Object localObject1 = null;
      localb.b(false);
      localb.g(false);
      localb.f(false);
      localb.e(bool1);
      Object localObject2 = g;
      if (localObject2 != null)
      {
        Object localObject3 = f;
        String str = "selfie";
        boolean bool3 = k.a(localObject3, str) ^ bool1;
        if (bool3)
        {
          localObject3 = m;
          int i1 = a;
          localObject1 = new Object[0];
          localObject1 = ((n)localObject3).a(i1, (Object[])localObject1);
          localObject3 = "resourceProvider.getString(it.resId)";
          k.a(localObject1, (String)localObject3);
          localb.a((String)localObject1);
        }
        localObject1 = f;
        localObject2 = "qr_scan";
        bool2 = k.a(localObject1, localObject2);
        bool1 ^= bool2;
        localb.c(bool1);
      }
      CameraSource localCameraSource = c;
      if (localCameraSource == null)
      {
        localObject1 = "camera";
        k.a((String)localObject1);
      }
      localb.a(localCameraSource);
      return;
    }
  }
  
  public final void f()
  {
    CameraSource localCameraSource = c;
    if (localCameraSource == null)
    {
      String str = "camera";
      k.a(str);
    }
    localCameraSource.a();
  }
  
  public final void g()
  {
    Object localObject1 = c;
    if (localObject1 == null)
    {
      localObject2 = "camera";
      k.a((String)localObject2);
    }
    Object localObject2 = this;
    localObject2 = (CameraSource.PictureCallback)this;
    ((CameraSource)localObject1).a((CameraSource.PictureCallback)localObject2);
    localObject1 = (h.b)b;
    if (localObject1 != null)
    {
      ((h.b)localObject1).d(true);
      ((h.b)localObject1).e(false);
      return;
    }
  }
  
  public final void h()
  {
    h.b localb = (h.b)b;
    if (localb != null)
    {
      Uri localUri = d;
      String str = e;
      localb.a(localUri, str);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.b.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */