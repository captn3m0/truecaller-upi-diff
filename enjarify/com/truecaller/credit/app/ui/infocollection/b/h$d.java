package com.truecaller.credit.app.ui.infocollection.b;

import c.d.a.a;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.credit.R.string;
import com.truecaller.credit.app.ui.infocollection.views.c.f.b;
import com.truecaller.credit.data.Failure;
import com.truecaller.credit.data.Result;
import com.truecaller.credit.data.Success;
import com.truecaller.credit.data.models.BankAccountDetails;
import com.truecaller.utils.n;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

final class h$d
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  h$d(h paramh, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    d locald = new com/truecaller/credit/app/ui/infocollection/b/h$d;
    h localh = b;
    locald.<init>(localh, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return locald;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    Object localObject2;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label309;
      }
      paramObject = h.b(b);
      if (paramObject != null)
      {
        localObject2 = h.c(b);
        j = R.string.text_retrieving_your_bank_details;
        Object[] arrayOfObject = new Object[0];
        localObject2 = ((n)localObject2).a(j, arrayOfObject);
        str = "resourceProvider.getStri…ieving_your_bank_details)";
        c.g.b.k.a(localObject2, str);
        ((f.b)paramObject).f((String)localObject2);
      }
      paramObject = h.d(b);
      localObject2 = new com/truecaller/credit/app/ui/infocollection/b/h$d$a;
      String str = null;
      ((h.d.a)localObject2).<init>(this, null);
      localObject2 = (m)localObject2;
      int j = 1;
      a = j;
      paramObject = g.a((f)paramObject, (m)localObject2, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (Result)paramObject;
    boolean bool2 = paramObject instanceof Success;
    if (bool2)
    {
      localObject1 = b;
      paramObject = (BankAccountDetails)((Success)paramObject).getData();
      localObject2 = "bankAccountDetails";
      c.g.b.k.b(paramObject, (String)localObject2);
      c = ((BankAccountDetails)paramObject);
      paramObject = b;
      localObject1 = h.e((h)paramObject);
      h.a((h)paramObject, (BankAccountDetails)localObject1);
    }
    else
    {
      boolean bool3 = paramObject instanceof Failure;
      if (bool3)
      {
        paramObject = h.b(b);
        if (paramObject != null) {
          ((f.b)paramObject).x();
        }
      }
    }
    paramObject = h.b(b);
    if (paramObject != null) {
      ((f.b)paramObject).q();
    }
    return x.a;
    label309:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (d)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((d)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.b.h.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */