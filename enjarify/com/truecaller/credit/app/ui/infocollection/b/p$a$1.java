package com.truecaller.credit.app.ui.infocollection.b;

import android.content.Context;
import android.net.Uri;
import c.d.c;
import c.o.b;
import c.x;
import com.truecaller.credit.app.ui.assist.CreditDocumentType;
import com.truecaller.credit.app.util.h;
import com.truecaller.credit.data.repository.CreditRepository;
import kotlinx.coroutines.ag;
import okhttp3.ac;
import okhttp3.w;
import okhttp3.x.b;

final class p$a$1
  extends c.d.b.a.k
  implements c.g.a.m
{
  Object a;
  Object b;
  Object c;
  Object d;
  Object e;
  int f;
  private ag h;
  
  p$a$1(p.a parama, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    1 local1 = new com/truecaller/credit/app/ui/infocollection/b/p$a$1;
    p.a locala = g;
    local1.<init>(locala, paramc);
    paramObject = (ag)paramObject;
    h = ((ag)paramObject);
    return local1;
  }
  
  public final Object a(Object paramObject)
  {
    c.d.a.a locala = c.d.a.a.a;
    int i = f;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool = paramObject instanceof o.b;
      if (bool) {
        throw a;
      }
      break;
    case 0: 
      int j = paramObject instanceof o.b;
      if (j != 0) {
        break label319;
      }
      paramObject = w.b("form-data");
      Object localObject1 = g.c.g;
      paramObject = ac.a((w)paramObject, (String)localObject1);
      localObject1 = new com/truecaller/credit/app/util/h;
      Object localObject2 = p.b(g.f);
      Object localObject3 = g.d;
      ((h)localObject1).<init>((Context)localObject2, (Uri)localObject3);
      long l = p.c(g.f).a();
      localObject2 = String.valueOf(l);
      localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>();
      Object localObject4 = p.d(g.f).a((String)localObject2);
      ((StringBuilder)localObject3).append((String)localObject4);
      ((StringBuilder)localObject3).append(".jpg");
      localObject3 = ((StringBuilder)localObject3).toString();
      Object localObject5 = localObject1;
      localObject5 = (ac)localObject1;
      localObject4 = x.b.a("attachment", (String)localObject3, (ac)localObject5);
      localObject5 = p.e(g.f);
      c.g.b.k.a(paramObject, "documentType");
      String str = "attachment";
      c.g.b.k.a(localObject4, str);
      a = paramObject;
      b = localObject1;
      c = localObject2;
      d = localObject3;
      e = localObject4;
      j = 1;
      f = j;
      paramObject = ((CreditRepository)localObject5).uploadDocument((ac)paramObject, (x.b)localObject4, this);
      if (paramObject == locala) {
        return locala;
      }
      break;
    }
    return paramObject;
    label319:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (1)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((1)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.b.p.a.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */