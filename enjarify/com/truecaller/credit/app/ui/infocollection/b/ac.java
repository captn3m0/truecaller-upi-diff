package com.truecaller.credit.app.ui.infocollection.b;

import dagger.a.d;
import javax.inject.Provider;

public final class ac
  implements d
{
  private final Provider a;
  private final Provider b;
  
  private ac(Provider paramProvider1, Provider paramProvider2)
  {
    a = paramProvider1;
    b = paramProvider2;
  }
  
  public static ac a(Provider paramProvider1, Provider paramProvider2)
  {
    ac localac = new com/truecaller/credit/app/ui/infocollection/b/ac;
    localac.<init>(paramProvider1, paramProvider2);
    return localac;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.infocollection.b.ac
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */