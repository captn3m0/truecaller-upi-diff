package com.truecaller.credit.app.ui.customview;

import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import c.g.b.k;

final class CameraScannerView$a
  implements SurfaceHolder.Callback
{
  public CameraScannerView$a(CameraScannerView paramCameraScannerView) {}
  
  public final void surfaceChanged(SurfaceHolder paramSurfaceHolder, int paramInt1, int paramInt2, int paramInt3)
  {
    k.b(paramSurfaceHolder, "holder");
  }
  
  public final void surfaceCreated(SurfaceHolder paramSurfaceHolder)
  {
    k.b(paramSurfaceHolder, "surface");
    CameraScannerView.a(a, true);
    CameraScannerView.a(a);
  }
  
  public final void surfaceDestroyed(SurfaceHolder paramSurfaceHolder)
  {
    k.b(paramSurfaceHolder, "surface");
    CameraScannerView.a(a, false);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.customview.CameraScannerView.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */