package com.truecaller.credit.app.ui.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.b;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.credit.R.color;
import com.truecaller.credit.R.id;
import com.truecaller.credit.R.layout;
import com.truecaller.credit.R.styleable;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;

public final class CustomInfoCollectionRadioButton
  extends LinearLayout
{
  public boolean a;
  private AttributeSet b;
  private HashMap c;
  
  public CustomInfoCollectionRadioButton(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private CustomInfoCollectionRadioButton(Context paramContext, AttributeSet paramAttributeSet, char paramChar)
  {
    super(paramContext, paramAttributeSet, 0);
    b = paramAttributeSet;
    paramContext = LayoutInflater.from(getContext());
    int i = R.layout.layout_info_collection_radio_button;
    Object localObject = this;
    localObject = (ViewGroup)this;
    int j = 1;
    paramContext.inflate(i, (ViewGroup)localObject, j);
    setOrientation(j);
    paramContext = b;
    if (paramContext != null)
    {
      paramAttributeSet = getContext();
      localObject = R.styleable.CustomInfoCollectionRadioButton;
      paramContext = paramAttributeSet.obtainStyledAttributes(paramContext, (int[])localObject);
      i = R.styleable.CustomInfoCollectionRadioButton_title;
      paramAttributeSet = paramContext.getString(i);
      if (paramAttributeSet != null) {
        setTitle(paramAttributeSet);
      }
      i = R.styleable.CustomInfoCollectionRadioButton_sub_title;
      paramAttributeSet = paramContext.getString(i);
      localObject = paramAttributeSet;
      localObject = (CharSequence)paramAttributeSet;
      int k;
      if (localObject != null)
      {
        int m = ((CharSequence)localObject).length();
        if (m != 0) {
          k = 0;
        }
      }
      if (k == 0) {
        a(paramAttributeSet, false);
      }
      i = R.styleable.CustomInfoCollectionRadioButton_checked;
      boolean bool = paramContext.getBoolean(i, false);
      setChecked(bool);
      paramContext.recycle();
      return;
    }
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = c;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      c = ((HashMap)localObject1);
    }
    localObject1 = c;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = findViewById(paramInt);
      localObject2 = c;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a(String paramString, boolean paramBoolean)
  {
    k.b(paramString, "title");
    int i = R.id.tvSubTitle;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "tvSubTitle");
    localTextView.setVisibility(0);
    i = R.id.tvSubTitle;
    localTextView = (TextView)a(i);
    String str = "tvSubTitle";
    k.a(localTextView, str);
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
    if (paramBoolean)
    {
      j = R.id.tvSubTitle;
      paramString = (TextView)a(j);
      localContext = getContext();
      i = R.color.coral;
      paramBoolean = b.c(localContext, i);
      paramString.setTextColor(paramBoolean);
      return;
    }
    int j = R.id.tvSubTitle;
    paramString = (TextView)a(j);
    Context localContext = getContext();
    i = R.color.blue_grey;
    paramBoolean = b.c(localContext, i);
    paramString.setTextColor(paramBoolean);
  }
  
  public final boolean a()
  {
    int i = R.id.radioButton;
    ImageView localImageView1 = (ImageView)a(i);
    k.a(localImageView1, "radioButton");
    int j = R.id.radioButton;
    ImageView localImageView2 = (ImageView)a(j);
    k.a(localImageView2, "radioButton");
    boolean bool2 = localImageView2.isEnabled() ^ true;
    localImageView1.setEnabled(bool2);
    i = R.id.radioButton;
    localImageView1 = (ImageView)a(i);
    k.a(localImageView1, "radioButton");
    boolean bool1 = localImageView1.isEnabled();
    a = bool1;
    return a;
  }
  
  public final void b()
  {
    int i = R.id.imageOne;
    ImageView localImageView = (ImageView)a(i);
    k.a(localImageView, "imageOne");
    t.a((View)localImageView);
  }
  
  public final ImageView getBackImage()
  {
    int i = R.id.imageTwo;
    ImageView localImageView = (ImageView)a(i);
    k.a(localImageView, "imageTwo");
    return localImageView;
  }
  
  public final ImageView getFrontImage()
  {
    int i = R.id.imageOne;
    ImageView localImageView = (ImageView)a(i);
    k.a(localImageView, "imageOne");
    return localImageView;
  }
  
  public final void setChecked(boolean paramBoolean)
  {
    int i = R.id.radioButton;
    ImageView localImageView = (ImageView)a(i);
    k.a(localImageView, "radioButton");
    localImageView.setEnabled(paramBoolean);
    a = paramBoolean;
  }
  
  public final void setClicked(boolean paramBoolean)
  {
    a = paramBoolean;
  }
  
  public final void setContainerBackground(int paramInt)
  {
    int i = R.id.container;
    ((ConstraintLayout)a(i)).setBackgroundColor(paramInt);
  }
  
  public final void setImageOne(Bitmap paramBitmap)
  {
    if (paramBitmap != null)
    {
      int i = R.id.imageOne;
      ImageView localImageView = (ImageView)a(i);
      k.a(localImageView, "imageOne");
      localImageView.setVisibility(0);
      i = R.id.imageOne;
      ((ImageView)a(i)).setImageBitmap(paramBitmap);
      return;
    }
    int j = R.id.imageOne;
    ((ImageView)a(j)).setImageDrawable(null);
    j = R.id.imageOne;
    paramBitmap = (ImageView)a(j);
    k.a(paramBitmap, "imageOne");
    paramBitmap.setVisibility(8);
  }
  
  public final void setImageTwo(Bitmap paramBitmap)
  {
    if (paramBitmap != null)
    {
      int i = R.id.imageTwo;
      ImageView localImageView = (ImageView)a(i);
      k.a(localImageView, "imageTwo");
      localImageView.setVisibility(0);
      i = R.id.imageTwo;
      ((ImageView)a(i)).setImageBitmap(paramBitmap);
      return;
    }
    int j = R.id.imageTwo;
    ((ImageView)a(j)).setImageDrawable(null);
    j = R.id.imageTwo;
    paramBitmap = (ImageView)a(j);
    k.a(paramBitmap, "imageTwo");
    paramBitmap.setVisibility(8);
  }
  
  public final void setTitle(String paramString)
  {
    k.b(paramString, "title");
    int i = R.id.tvTitle;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "tvTitle");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.customview.CustomInfoCollectionRadioButton
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */