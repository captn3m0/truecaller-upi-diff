package com.truecaller.credit.app.ui.customview;

import android.os.Handler;
import android.widget.ProgressBar;
import c.g.b.k;
import com.truecaller.credit.R.id;

final class ThreeStepProgressView$c
  implements Runnable
{
  ThreeStepProgressView$c(ThreeStepProgressView paramThreeStepProgressView, int paramInt) {}
  
  public final void run()
  {
    ThreeStepProgressView localThreeStepProgressView = a;
    int i = b;
    int j = ThreeStepProgressView.b(localThreeStepProgressView);
    i -= j;
    j = R.id.pbStep3;
    ProgressBar localProgressBar = (ProgressBar)localThreeStepProgressView.c(j);
    k.a(localProgressBar, "pbStep3");
    long l = localThreeStepProgressView.a(localProgressBar, i, 0);
    int k = a;
    if (i == k)
    {
      Handler localHandler = h;
      if (localHandler == null)
      {
        localObject = "mHandler";
        k.a((String)localObject);
      }
      Object localObject = new com/truecaller/credit/app/ui/customview/ThreeStepProgressView$f;
      ((ThreeStepProgressView.f)localObject).<init>(localThreeStepProgressView);
      localObject = (Runnable)localObject;
      localHandler.postDelayed((Runnable)localObject, l);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.customview.ThreeStepProgressView.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */