package com.truecaller.credit.app.ui.customview.a;

import android.view.View;
import android.view.View.OnClickListener;
import com.truecaller.credit.domain.interactors.infocollection.models.AppointmentData;
import java.util.List;

final class c$a
  implements View.OnClickListener
{
  c$a(c paramc, b paramb) {}
  
  public final void onClick(View paramView)
  {
    paramView = c.a(a);
    int i = b.getAdapterPosition();
    List localList = c;
    int j = a;
    ((AppointmentData)localList.get(j)).setSelected(false);
    ((AppointmentData)c.get(i)).setSelected(true);
    a = i;
    paramView = c.b(a);
    i = b.getAdapterPosition();
    paramView.b(i);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.customview.a.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */