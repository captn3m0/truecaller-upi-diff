package com.truecaller.credit.app.ui.customview;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v4.content.b;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import c.g.b.k;
import c.u;
import com.truecaller.credit.R.color;
import com.truecaller.credit.R.drawable;
import com.truecaller.credit.R.id;
import com.truecaller.credit.R.layout;
import com.truecaller.credit.R.styleable;
import java.util.HashMap;

public final class ThreeStepProgressView
  extends LinearLayout
{
  final int a = 100;
  final int b = 200;
  public final int c = 300;
  final int d = 500;
  final int e = 1000;
  final long f;
  public final int g;
  Handler h;
  private AttributeSet i;
  private final long j = 20;
  private final String k;
  private final String l;
  private HashMap m;
  
  public ThreeStepProgressView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private ThreeStepProgressView(Context paramContext, AttributeSet paramAttributeSet, char paramChar)
  {
    super(paramContext, paramAttributeSet, 0);
    long l1 = 600L;
    f = l1;
    k = "progress";
    l = "alpha";
    paramContext = getContext();
    k.a(paramContext, "context");
    paramContext = paramContext.getResources();
    k.a(paramContext, "context.resources");
    int n = getDisplayMetricsdensityDpi / 160;
    g = n;
    i = paramAttributeSet;
    paramContext = LayoutInflater.from(getContext());
    int i1 = R.layout.layout_three_step_progress_view;
    Object localObject1 = this;
    localObject1 = (ViewGroup)this;
    int i2 = 1;
    paramContext.inflate(i1, (ViewGroup)localObject1, i2);
    setOrientation(i2);
    paramContext = i;
    if (paramContext != null)
    {
      paramAttributeSet = getContext();
      localObject1 = R.styleable.ThreeStepProgressView;
      paramContext = paramAttributeSet.obtainStyledAttributes(paramContext, (int[])localObject1);
      i1 = R.id.tvStepText1;
      paramAttributeSet = (TextView)c(i1);
      k.a(paramAttributeSet, "tvStepText1");
      int i4 = R.styleable.ThreeStepProgressView_title_step_1;
      localObject1 = (CharSequence)paramContext.getString(i4);
      paramAttributeSet.setText((CharSequence)localObject1);
      i1 = R.id.tvStepText2;
      paramAttributeSet = (TextView)c(i1);
      k.a(paramAttributeSet, "tvStepText2");
      i4 = R.styleable.ThreeStepProgressView_title_step_2;
      localObject1 = (CharSequence)paramContext.getString(i4);
      paramAttributeSet.setText((CharSequence)localObject1);
      i1 = R.id.tvStepText3;
      paramAttributeSet = (TextView)c(i1);
      k.a(paramAttributeSet, "tvStepText3");
      i4 = R.styleable.ThreeStepProgressView_title_step_3;
      localObject1 = (CharSequence)paramContext.getString(i4);
      paramAttributeSet.setText((CharSequence)localObject1);
      i1 = R.id.centralLine;
      paramAttributeSet = (ProgressBar)c(i1);
      k.a(paramAttributeSet, "centralLine");
      i4 = R.id.tvStepText1;
      localObject1 = (TextView)c(i4);
      String str = "tvStepText1";
      k.a(localObject1, str);
      localObject1 = a((TextView)localObject1);
      i4 = ((Rect)localObject1).width() / 2;
      int i3 = g * 12;
      int i5 = R.id.tvStepText2;
      Object localObject2 = (TextView)c(i5);
      k.a(localObject2, "tvStepText2");
      localObject2 = a((TextView)localObject2);
      i5 = ((Rect)localObject2).width() / 2;
      Object localObject3 = paramAttributeSet.getLayoutParams();
      boolean bool = localObject3 instanceof ViewGroup.MarginLayoutParams;
      if (bool)
      {
        localObject3 = paramAttributeSet.getLayoutParams();
        if (localObject3 != null)
        {
          localObject3 = (ViewGroup.MarginLayoutParams)localObject3;
          ((ViewGroup.MarginLayoutParams)localObject3).setMargins(i4, i3, i5, 0);
          paramAttributeSet.requestLayout();
        }
        else
        {
          paramContext = new c/u;
          paramContext.<init>("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
          throw paramContext;
        }
      }
      paramContext.recycle();
      paramContext = new android/os/Handler;
      paramContext.<init>();
      h = paramContext;
      return;
    }
  }
  
  private static Rect a(TextView paramTextView)
  {
    Rect localRect = new android/graphics/Rect;
    localRect.<init>();
    TextPaint localTextPaint = paramTextView.getPaint();
    Object localObject = paramTextView.getText();
    if (localObject != null)
    {
      localObject = (String)localObject;
      int n = paramTextView.getText().length();
      localTextPaint.getTextBounds((String)localObject, 0, n, localRect);
      return localRect;
    }
    paramTextView = new c/u;
    paramTextView.<init>("null cannot be cast to non-null type kotlin.String");
    throw paramTextView;
  }
  
  private long d(int paramInt)
  {
    int n = R.id.pbStep1;
    ProgressBar localProgressBar = (ProgressBar)c(n);
    k.a(localProgressBar, "pbStep1");
    long l1 = a(localProgressBar, paramInt, 0);
    int i1 = a;
    if (paramInt == i1)
    {
      Handler localHandler = h;
      if (localHandler == null)
      {
        localObject = "mHandler";
        k.a((String)localObject);
      }
      Object localObject = new com/truecaller/credit/app/ui/customview/ThreeStepProgressView$e;
      ((ThreeStepProgressView.e)localObject).<init>(this);
      localObject = (Runnable)localObject;
      localHandler.postDelayed((Runnable)localObject, l1);
      long l2 = f;
      l1 += l2;
    }
    return l1;
  }
  
  private final int e(int paramInt)
  {
    return b.c(getContext(), paramInt);
  }
  
  public final long a(int paramInt)
  {
    int n = R.id.pbStep2;
    ProgressBar localProgressBar = (ProgressBar)c(n);
    k.a(localProgressBar, "pbStep2");
    long l1 = a(localProgressBar, paramInt, 0);
    int i1 = a;
    if (paramInt == i1)
    {
      Handler localHandler = h;
      if (localHandler == null)
      {
        localObject = "mHandler";
        k.a((String)localObject);
      }
      Object localObject = new com/truecaller/credit/app/ui/customview/ThreeStepProgressView$h;
      ((ThreeStepProgressView.h)localObject).<init>(this);
      localObject = (Runnable)localObject;
      localHandler.postDelayed((Runnable)localObject, l1);
      long l2 = f;
      l1 += l2;
    }
    return l1;
  }
  
  final long a(ProgressBar paramProgressBar, int paramInt1, int paramInt2)
  {
    Object localObject = paramProgressBar.getTag();
    if (localObject == null) {
      localObject = Integer.valueOf(0);
    }
    if (localObject != null)
    {
      localObject = (Integer)localObject;
      int n = ((Integer)localObject).intValue();
      Integer localInteger = Integer.valueOf(paramInt1);
      paramProgressBar.setTag(localInteger);
      long l1;
      if (paramInt2 == 0)
      {
        paramInt2 = paramInt1 - n;
        l1 = paramInt2;
        long l2 = j;
        l1 = Math.abs(l1 * l2);
      }
      else
      {
        l1 = f;
      }
      String str = k;
      int[] arrayOfInt = new int[2];
      arrayOfInt[0] = n;
      arrayOfInt[1] = paramInt1;
      paramProgressBar = ObjectAnimator.ofInt(paramProgressBar, str, arrayOfInt);
      k.a(paramProgressBar, "animation");
      paramProgressBar.setDuration(l1);
      paramProgressBar.start();
      return l1;
    }
    paramProgressBar = new c/u;
    paramProgressBar.<init>("null cannot be cast to non-null type kotlin.Int");
    throw paramProgressBar;
  }
  
  public final void a()
  {
    Object localObject = Integer.valueOf(a);
    setTag(localObject);
    int n = R.id.pbStep1;
    localObject = (ProgressBar)c(n);
    int i1 = R.drawable.ic_tick_active;
    Drawable localDrawable = b(i1);
    ((ProgressBar)localObject).setBackgroundDrawable(localDrawable);
    n = R.id.tvStepCount1;
    localObject = (TextView)c(n);
    k.a(localObject, "tvStepCount1");
    ((TextView)localObject).setVisibility(8);
    n = R.id.centralLine;
    localObject = (ProgressBar)c(n);
    i1 = d;
    ((ProgressBar)localObject).setProgress(i1);
    n = R.id.tvStepCount2;
    localObject = (TextView)c(n);
    i1 = R.drawable.blue_circle_bg;
    localDrawable = b(i1);
    ((TextView)localObject).setBackgroundDrawable(localDrawable);
    n = R.id.tvStepCount2;
    localObject = (TextView)c(n);
    i1 = R.color.blue;
    i1 = e(i1);
    ((TextView)localObject).setTextColor(i1);
  }
  
  final void a(ProgressBar paramProgressBar, int paramInt)
  {
    Object localObject = b(paramInt);
    paramProgressBar.setBackgroundDrawable((Drawable)localObject);
    localObject = l;
    float[] arrayOfFloat = new float[2];
    float[] tmp23_21 = arrayOfFloat;
    tmp23_21[0] = 0.3F;
    tmp23_21[1] = 1.0F;
    paramProgressBar = ObjectAnimator.ofFloat(paramProgressBar, (String)localObject, arrayOfFloat);
    k.a(paramProgressBar, "fadeIn");
    long l1 = f;
    paramProgressBar.setDuration(l1);
    paramProgressBar.start();
  }
  
  public final Drawable b(int paramInt)
  {
    return b.a(getContext(), paramInt);
  }
  
  public final void b()
  {
    Object localObject = Integer.valueOf(b);
    setTag(localObject);
    int n = R.id.pbStep2;
    localObject = (ProgressBar)c(n);
    int i1 = R.drawable.ic_tick_active;
    Drawable localDrawable = b(i1);
    ((ProgressBar)localObject).setBackgroundDrawable(localDrawable);
    n = R.id.tvStepCount2;
    localObject = (TextView)c(n);
    k.a(localObject, "tvStepCount2");
    ((TextView)localObject).setVisibility(8);
    n = R.id.centralLine;
    localObject = (ProgressBar)c(n);
    i1 = e;
    ((ProgressBar)localObject).setProgress(i1);
    n = R.id.tvStepCount3;
    localObject = (TextView)c(n);
    i1 = R.drawable.blue_circle_bg;
    localDrawable = b(i1);
    ((TextView)localObject).setBackgroundDrawable(localDrawable);
    n = R.id.tvStepCount3;
    localObject = (TextView)c(n);
    i1 = R.color.blue;
    i1 = e(i1);
    ((TextView)localObject).setTextColor(i1);
  }
  
  public final View c(int paramInt)
  {
    Object localObject1 = m;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      m = ((HashMap)localObject1);
    }
    localObject1 = m;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = findViewById(paramInt);
      localObject2 = m;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void setProgress(int paramInt)
  {
    Object localObject1 = getTag();
    int n = 0;
    Object localObject2 = null;
    if (localObject1 == null) {
      localObject1 = Integer.valueOf(0);
    }
    if (localObject1 != null)
    {
      localObject1 = (Integer)localObject1;
      int i1 = ((Integer)localObject1).intValue();
      Object localObject3 = Integer.valueOf(paramInt);
      setTag(localObject3);
      Object localObject4;
      if (paramInt < i1)
      {
        localObject1 = Integer.valueOf(b);
        setTag(localObject1);
        i1 = R.id.pbStep3;
        localObject1 = (ProgressBar)c(i1);
        i2 = 0;
        ((ProgressBar)localObject1).setBackgroundDrawable(null);
        i1 = R.id.pbStep3;
        ((ProgressBar)c(i1)).setProgress(0);
        i1 = R.id.tvStepCount3;
        localObject1 = (TextView)c(i1);
        k.a(localObject1, "tvStepCount3");
        ((TextView)localObject1).setVisibility(0);
        i1 = R.id.tvStepCount3;
        localObject1 = (TextView)c(i1);
        int i3 = R.color.bluey_grey;
        i3 = e(i3);
        ((TextView)localObject1).setTextColor(i3);
        i1 = R.id.tvStepCount3;
        localObject1 = (TextView)c(i1);
        i3 = R.drawable.grey_circle_bg;
        localObject4 = b(i3);
        ((TextView)localObject1).setBackgroundDrawable((Drawable)localObject4);
        i1 = R.id.centralLine;
        localObject1 = (ProgressBar)c(i1);
        i3 = e;
        ((ProgressBar)localObject1).setProgress(i3);
        localObject1 = Integer.valueOf(a);
        setTag(localObject1);
        i1 = R.id.pbStep2;
        ((ProgressBar)c(i1)).setBackgroundDrawable(null);
        i1 = R.id.pbStep2;
        ((ProgressBar)c(i1)).setProgress(0);
        i1 = R.id.tvStepCount2;
        localObject1 = (TextView)c(i1);
        k.a(localObject1, "tvStepCount2");
        ((TextView)localObject1).setVisibility(0);
        i1 = R.id.tvStepCount2;
        localObject1 = (TextView)c(i1);
        i3 = R.color.bluey_grey;
        i3 = e(i3);
        ((TextView)localObject1).setTextColor(i3);
        i1 = R.id.tvStepCount2;
        localObject1 = (TextView)c(i1);
        i3 = R.drawable.grey_circle_bg;
        localObject4 = b(i3);
        ((TextView)localObject1).setBackgroundDrawable((Drawable)localObject4);
        i1 = R.id.centralLine;
        localObject1 = (ProgressBar)c(i1);
        i3 = d;
        ((ProgressBar)localObject1).setProgress(i3);
        localObject1 = Integer.valueOf(0);
        setTag(localObject1);
        i1 = R.id.pbStep1;
        ((ProgressBar)c(i1)).setBackgroundDrawable(null);
        i1 = R.id.pbStep1;
        ((ProgressBar)c(i1)).setProgress(0);
        i1 = R.id.tvStepCount1;
        localObject1 = (TextView)c(i1);
        localObject3 = "tvStepCount1";
        k.a(localObject1, (String)localObject3);
        ((TextView)localObject1).setVisibility(0);
        i1 = R.id.centralLine;
        localObject1 = (ProgressBar)c(i1);
        ((ProgressBar)localObject1).setProgress(0);
        i1 = b;
        if (paramInt == i1)
        {
          a();
          b();
          return;
        }
        i1 = a;
        if (paramInt == i1) {
          a();
        }
        return;
      }
      n = a;
      if (paramInt <= n)
      {
        d(paramInt);
        return;
      }
      int i2 = b;
      long l1 = 0L;
      if (paramInt <= i2)
      {
        if (i1 < n) {
          l1 = d(n);
        }
        localObject1 = h;
        if (localObject1 == null)
        {
          localObject2 = "mHandler";
          k.a((String)localObject2);
        }
        localObject2 = new com/truecaller/credit/app/ui/customview/ThreeStepProgressView$a;
        ((ThreeStepProgressView.a)localObject2).<init>(this, paramInt);
        localObject2 = (Runnable)localObject2;
        ((Handler)localObject1).postDelayed((Runnable)localObject2, l1);
        return;
      }
      int i4 = c;
      if (paramInt <= i4)
      {
        if (i1 < n)
        {
          long l2 = d(n);
          localObject3 = h;
          if (localObject3 == null)
          {
            localObject4 = "mHandler";
            k.a((String)localObject4);
          }
          localObject4 = new com/truecaller/credit/app/ui/customview/ThreeStepProgressView$b;
          ((ThreeStepProgressView.b)localObject4).<init>(this);
          localObject4 = (Runnable)localObject4;
          ((Handler)localObject3).postDelayed((Runnable)localObject4, l2);
          long l3 = j * 100;
          long l4 = f;
          l3 += l4;
          l2 += l3;
          l1 = l2;
        }
        else if (i1 < i2)
        {
          l1 = a(n);
        }
        localObject1 = h;
        if (localObject1 == null)
        {
          localObject2 = "mHandler";
          k.a((String)localObject2);
        }
        localObject2 = new com/truecaller/credit/app/ui/customview/ThreeStepProgressView$c;
        ((ThreeStepProgressView.c)localObject2).<init>(this, paramInt);
        localObject2 = (Runnable)localObject2;
        ((Handler)localObject1).postDelayed((Runnable)localObject2, l1);
      }
      return;
    }
    u localu = new c/u;
    localu.<init>("null cannot be cast to non-null type kotlin.Int");
    throw localu;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.customview.ThreeStepProgressView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */