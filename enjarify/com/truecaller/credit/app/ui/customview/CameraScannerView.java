package com.truecaller.credit.app.ui.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.common.images.Size;
import com.google.android.gms.vision.CameraSource;
import com.truecaller.log.AssertionUtil;
import java.io.IOException;

public final class CameraScannerView
  extends ViewGroup
{
  public SurfaceView a;
  public boolean b;
  public CameraSource c;
  private AttributeSet d;
  private boolean e;
  
  public CameraScannerView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private CameraScannerView(Context paramContext, AttributeSet paramAttributeSet, char paramChar)
  {
    super(paramContext, paramAttributeSet, 0);
    d = paramAttributeSet;
    b = false;
    e = false;
    paramContext = new android/view/SurfaceView;
    paramAttributeSet = getContext();
    paramContext.<init>(paramAttributeSet);
    a = paramContext;
    paramContext = a;
    if (paramContext != null)
    {
      paramContext = paramContext.getHolder();
      if (paramContext != null)
      {
        paramAttributeSet = new com/truecaller/credit/app/ui/customview/CameraScannerView$a;
        paramAttributeSet.<init>(this);
        paramAttributeSet = (SurfaceHolder.Callback)paramAttributeSet;
        paramContext.addCallback(paramAttributeSet);
      }
    }
    paramContext = (View)a;
    addView(paramContext);
  }
  
  public final void a()
  {
    try
    {
      boolean bool = b;
      if (bool)
      {
        bool = e;
        if (bool)
        {
          localObject1 = c;
          if (localObject1 != null)
          {
            Object localObject2 = a;
            if (localObject2 != null) {
              localObject2 = ((SurfaceView)localObject2).getHolder();
            } else {
              localObject2 = null;
            }
            ((CameraSource)localObject1).a((SurfaceHolder)localObject2);
          }
          bool = false;
          localObject1 = null;
          b = false;
          return;
        }
      }
    }
    catch (RuntimeException localRuntimeException)
    {
      Object localObject1 = (Throwable)localRuntimeException;
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localObject1);
      return;
    }
    catch (IOException localIOException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localIOException);
      return;
    }
    catch (SecurityException localSecurityException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localSecurityException);
    }
  }
  
  protected final void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    Object localObject = c;
    int i = 320;
    float f1 = 4.48E-43F;
    int j = 240;
    if (localObject != null)
    {
      localObject = ((CameraSource)localObject).c();
      if (localObject != null)
      {
        i = ((Size)localObject).a();
        paramBoolean = ((Size)localObject).b();
        k = paramBoolean;
      }
    }
    paramInt3 -= paramInt1;
    paramInt4 -= paramInt2;
    float f2 = paramInt3;
    float f3 = k;
    f2 /= f3;
    float f4 = paramInt4;
    f1 = i;
    f4 /= f1;
    int k = 0;
    boolean bool = f2 < f4;
    if (bool)
    {
      f1 *= f2;
      paramBoolean = (int)f1;
      paramInt1 = (paramBoolean - paramInt4) / 2;
      paramInt4 = paramBoolean;
      paramInt2 = paramInt1;
      paramBoolean = paramInt3;
      paramInt1 = 0;
      f3 = 0.0F;
    }
    else
    {
      f3 *= f4;
      paramBoolean = (int)f3;
      paramInt1 = (paramBoolean - paramInt3) / 2;
      paramInt2 = 0;
      f4 = 0.0F;
    }
    paramInt3 = getChildCount();
    while (k < paramInt3)
    {
      View localView = getChildAt(k);
      int m = paramInt1 * -1;
      int n = paramInt2 * -1;
      int i1 = paramBoolean - paramInt1;
      int i2 = paramInt4 - paramInt2;
      localView.layout(m, n, i1, i2);
      k += 1;
    }
    localObject = a;
    if (localObject != null) {
      a();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.customview.CameraScannerView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */