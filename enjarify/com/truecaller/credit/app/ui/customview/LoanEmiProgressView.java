package com.truecaller.credit.app.ui.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Join;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import com.truecaller.credit.R.styleable;

public final class LoanEmiProgressView
  extends View
{
  private Paint a;
  private Paint b;
  private Paint c;
  private RectF d;
  private RectF e;
  private RectF f;
  private int g;
  private int h;
  private int i;
  private int j;
  private int k;
  private int l;
  
  public LoanEmiProgressView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private LoanEmiProgressView(Context paramContext, AttributeSet paramAttributeSet, char paramChar)
  {
    super(paramContext, paramAttributeSet, 0);
    paramContext = new android/graphics/Paint;
    int m = 1;
    paramContext.<init>(m);
    a = paramContext;
    paramContext = new android/graphics/Paint;
    paramContext.<init>(m);
    b = paramContext;
    paramContext = new android/graphics/Paint;
    paramContext.<init>(m);
    c = paramContext;
    paramContext = new android/graphics/RectF;
    paramContext.<init>();
    d = paramContext;
    paramContext = new android/graphics/RectF;
    paramContext.<init>();
    e = paramContext;
    paramContext = new android/graphics/RectF;
    paramContext.<init>();
    f = paramContext;
    if (paramAttributeSet != null)
    {
      paramContext = getContext();
      int[] arrayOfInt = R.styleable.LoanEmiProgressView;
      paramContext = paramContext.obtainStyledAttributes(paramAttributeSet, arrayOfInt);
      int n = R.styleable.LoanEmiProgressView_fill_color_background;
      n = paramContext.getColor(n, -7829368);
      g = n;
      n = R.styleable.LoanEmiProgressView_fill_color_middle;
      n = paramContext.getColor(n, 65280);
      h = n;
      n = R.styleable.LoanEmiProgressView_fill_color_top;
      n = paramContext.getColor(n, -16776961);
      i = n;
      n = R.styleable.LoanEmiProgressView_total_emi;
      m = 12;
      n = paramContext.getInt(n, m);
      j = n;
      n = R.styleable.LoanEmiProgressView_next_emis;
      n = paramContext.getInt(n, 0);
      k = n;
      n = R.styleable.LoanEmiProgressView_emi_paid;
      n = paramContext.getInt(n, 0);
      l = n;
      paramAttributeSet = a;
      paramChar = g;
      paramAttributeSet.setColor(paramChar);
      localObject = Paint.Join.ROUND;
      paramAttributeSet.setStrokeJoin((Paint.Join)localObject);
      localObject = Paint.Cap.ROUND;
      paramAttributeSet.setStrokeCap((Paint.Cap)localObject);
      paramAttributeSet = b;
      paramChar = h;
      paramAttributeSet.setColor(paramChar);
      localObject = Paint.Join.ROUND;
      paramAttributeSet.setStrokeJoin((Paint.Join)localObject);
      localObject = Paint.Cap.ROUND;
      paramAttributeSet.setStrokeCap((Paint.Cap)localObject);
      paramAttributeSet = c;
      paramChar = i;
      paramAttributeSet.setColor(paramChar);
      localObject = Paint.Join.ROUND;
      paramAttributeSet.setStrokeJoin((Paint.Join)localObject);
      localObject = Paint.Cap.ROUND;
      paramAttributeSet.setStrokeCap((Paint.Cap)localObject);
      paramContext.recycle();
    }
  }
  
  private static void a(RectF paramRectF, float paramFloat, Paint paramPaint, Canvas paramCanvas)
  {
    Path localPath = new android/graphics/Path;
    localPath.<init>();
    float f1 = left;
    float f2 = top;
    localPath.moveTo(f1, f2);
    f1 = right;
    f2 = top;
    localPath.lineTo(f1, f2);
    f1 = right;
    f2 = bottom;
    localPath.lineTo(f1, f2);
    f1 = left + paramFloat;
    f2 = bottom;
    localPath.lineTo(f1, f2);
    f1 = left;
    f2 = bottom;
    float f3 = left;
    float f4 = bottom - paramFloat;
    localPath.quadTo(f1, f2, f3, f4);
    f1 = left;
    f2 = top + paramFloat;
    localPath.lineTo(f1, f2);
    f1 = left;
    f2 = top;
    f3 = left + paramFloat;
    float f5 = top;
    localPath.quadTo(f1, f2, f3, f5);
    localPath.close();
    if (paramPaint != null)
    {
      paramCanvas.drawPath(localPath, paramPaint);
      return;
    }
  }
  
  private static void b(RectF paramRectF, float paramFloat, Paint paramPaint, Canvas paramCanvas)
  {
    Path localPath = new android/graphics/Path;
    localPath.<init>();
    float f1 = left + paramFloat;
    float f2 = top;
    localPath.moveTo(f1, f2);
    f1 = right - paramFloat;
    f2 = top;
    localPath.lineTo(f1, f2);
    f1 = right;
    f2 = top;
    float f3 = right;
    float f4 = top + paramFloat;
    localPath.quadTo(f1, f2, f3, f4);
    f1 = right;
    f2 = bottom - paramFloat;
    localPath.lineTo(f1, f2);
    f1 = right;
    f2 = bottom;
    f3 = right - paramFloat;
    f4 = bottom;
    localPath.quadTo(f1, f2, f3, f4);
    f1 = left + paramFloat;
    f2 = bottom;
    localPath.lineTo(f1, f2);
    f1 = left;
    f2 = bottom;
    f3 = left;
    f4 = bottom - paramFloat;
    localPath.quadTo(f1, f2, f3, f4);
    f1 = left;
    f2 = top + paramFloat;
    localPath.lineTo(f1, f2);
    f1 = left;
    f2 = top;
    f3 = left + paramFloat;
    float f5 = top;
    localPath.quadTo(f1, f2, f3, f5);
    localPath.close();
    if (paramPaint != null)
    {
      paramCanvas.drawPath(localPath, paramPaint);
      return;
    }
  }
  
  protected final void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    if (paramCanvas != null)
    {
      RectF localRectF = d;
      int m = 0;
      float f1 = 0.0F;
      Paint localPaint1 = null;
      left = 0.0F;
      float f2 = getWidth();
      right = f2;
      top = 0.0F;
      f2 = getHeight();
      bottom = f2;
      f2 = getHeight();
      float f3 = 2.0F;
      f2 /= f3;
      Paint localPaint2 = a;
      b(localRectF, f2, localPaint2, paramCanvas);
      localRectF = f;
      left = 0.0F;
      int n = getWidth();
      int i1 = j;
      n /= i1;
      i1 = l;
      f2 = n * i1;
      right = f2;
      top = 0.0F;
      f2 = getHeight();
      bottom = f2;
      n = l;
      if (n != 0)
      {
        i1 = j;
        if (n < i1)
        {
          n = getHeight();
          f2 = n / f3;
          localPaint2 = c;
          a(localRectF, f2, localPaint2, paramCanvas);
          break label262;
        }
      }
      n = l;
      i1 = j;
      if (n == i1)
      {
        n = getHeight();
        f2 = n / f3;
        localPaint2 = c;
        b(localRectF, f2, localPaint2, paramCanvas);
      }
      label262:
      localRectF = e;
      n = getWidth();
      i1 = j;
      n /= i1;
      i1 = l;
      f2 = n * i1;
      left = f2;
      n = getWidth();
      i1 = j;
      n /= i1;
      i1 = l;
      n *= i1;
      f2 = n;
      i1 = getWidth();
      int i2 = j;
      i1 /= i2;
      i2 = k;
      i1 *= i2;
      float f4 = i1;
      f2 += f4;
      right = f2;
      top = 0.0F;
      f1 = getHeight();
      bottom = f1;
      m = l;
      Paint localPaint3;
      if (m == 0)
      {
        f1 = getHeight() / f3;
        localPaint3 = b;
        a(localRectF, f1, localPaint3, paramCanvas);
        return;
      }
      n = j + -1;
      if (m == n)
      {
        m = getHeight();
        f1 = m / f3;
        localPaint3 = b;
        Path localPath = new android/graphics/Path;
        localPath.<init>();
        f4 = left;
        float f5 = top;
        localPath.moveTo(f4, f5);
        f4 = right - f1;
        f5 = top;
        localPath.lineTo(f4, f5);
        f4 = right;
        f5 = top;
        float f6 = right;
        float f7 = top + f1;
        localPath.quadTo(f4, f5, f6, f7);
        f4 = right;
        f5 = bottom - f1;
        localPath.lineTo(f4, f5);
        f4 = right;
        f5 = bottom;
        f6 = right - f1;
        f1 = bottom;
        localPath.quadTo(f4, f5, f6, f1);
        f1 = left;
        float f8 = bottom;
        localPath.lineTo(f1, f8);
        localPath.close();
        if (localPaint3 != null)
        {
          paramCanvas.drawPath(localPath, localPaint3);
          return;
        }
        return;
      }
      localPaint1 = b;
      paramCanvas.drawRect(localRectF, localPaint1);
      return;
    }
  }
  
  public final void setFillColorBackground(int paramInt)
  {
    g = paramInt;
    postInvalidate();
  }
  
  public final void setFillColorMid(int paramInt)
  {
    h = paramInt;
    postInvalidate();
  }
  
  public final void setFillColorTop(int paramInt)
  {
    i = paramInt;
    postInvalidate();
  }
  
  public final void setNextEmis(int paramInt)
  {
    k = paramInt;
    postInvalidate();
  }
  
  public final void setPaidEmis(int paramInt)
  {
    l = paramInt;
    postInvalidate();
  }
  
  public final void setTotalEmi(int paramInt)
  {
    j = paramInt;
    postInvalidate();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.customview.LoanEmiProgressView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */