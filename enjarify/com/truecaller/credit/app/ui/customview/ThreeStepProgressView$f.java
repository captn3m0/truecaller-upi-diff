package com.truecaller.credit.app.ui.customview;

import android.widget.ProgressBar;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.credit.R.drawable;
import com.truecaller.credit.R.id;

final class ThreeStepProgressView$f
  implements Runnable
{
  ThreeStepProgressView$f(ThreeStepProgressView paramThreeStepProgressView) {}
  
  public final void run()
  {
    Object localObject1 = a;
    Object localObject2 = Integer.valueOf(c);
    ((ThreeStepProgressView)localObject1).setTag(localObject2);
    int i = R.id.pbStep3;
    localObject2 = (ProgressBar)((ThreeStepProgressView)localObject1).c(i);
    k.a(localObject2, "pbStep3");
    int j = R.drawable.ic_tick_active;
    ((ThreeStepProgressView)localObject1).a((ProgressBar)localObject2, j);
    i = R.id.tvStepCount3;
    localObject1 = (TextView)((ThreeStepProgressView)localObject1).c(i);
    k.a(localObject1, "tvStepCount3");
    ((TextView)localObject1).setVisibility(8);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.customview.ThreeStepProgressView.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */