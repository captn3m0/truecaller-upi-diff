package com.truecaller.credit.app.ui.customview;

import android.graphics.drawable.Drawable;
import android.widget.TextView;
import com.truecaller.credit.R.color;
import com.truecaller.credit.R.drawable;
import com.truecaller.credit.R.id;

final class ThreeStepProgressView$g
  implements Runnable
{
  ThreeStepProgressView$g(ThreeStepProgressView paramThreeStepProgressView) {}
  
  public final void run()
  {
    Object localObject1 = a;
    int i = R.id.tvStepCount3;
    localObject1 = (TextView)((ThreeStepProgressView)localObject1).c(i);
    Object localObject2 = a;
    int j = R.drawable.blue_circle_bg;
    localObject2 = ThreeStepProgressView.a((ThreeStepProgressView)localObject2, j);
    ((TextView)localObject1).setBackgroundDrawable((Drawable)localObject2);
    localObject1 = a;
    i = R.id.tvStepCount3;
    localObject1 = (TextView)((ThreeStepProgressView)localObject1).c(i);
    localObject2 = a;
    j = R.color.blue;
    i = ThreeStepProgressView.b((ThreeStepProgressView)localObject2, j);
    ((TextView)localObject1).setTextColor(i);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.customview.ThreeStepProgressView.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */