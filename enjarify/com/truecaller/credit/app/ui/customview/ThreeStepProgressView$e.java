package com.truecaller.credit.app.ui.customview;

import android.os.Handler;
import android.widget.ProgressBar;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.credit.R.drawable;
import com.truecaller.credit.R.id;

final class ThreeStepProgressView$e
  implements Runnable
{
  ThreeStepProgressView$e(ThreeStepProgressView paramThreeStepProgressView) {}
  
  public final void run()
  {
    ThreeStepProgressView localThreeStepProgressView = a;
    Object localObject1 = Integer.valueOf(a);
    localThreeStepProgressView.setTag(localObject1);
    int i = R.id.pbStep1;
    localObject1 = (ProgressBar)localThreeStepProgressView.c(i);
    k.a(localObject1, "pbStep1");
    int j = R.drawable.ic_tick_active;
    localThreeStepProgressView.a((ProgressBar)localObject1, j);
    i = R.id.tvStepCount1;
    localObject1 = (TextView)localThreeStepProgressView.c(i);
    k.a(localObject1, "tvStepCount1");
    ((TextView)localObject1).setVisibility(8);
    i = R.id.centralLine;
    localObject1 = (ProgressBar)localThreeStepProgressView.c(i);
    Object localObject2 = "centralLine";
    k.a(localObject1, (String)localObject2);
    j = d;
    int k = 1;
    localThreeStepProgressView.a((ProgressBar)localObject1, j, k);
    localObject1 = h;
    if (localObject1 == null)
    {
      localObject2 = "mHandler";
      k.a((String)localObject2);
    }
    localObject2 = new com/truecaller/credit/app/ui/customview/ThreeStepProgressView$d;
    ((ThreeStepProgressView.d)localObject2).<init>(localThreeStepProgressView);
    localObject2 = (Runnable)localObject2;
    long l = f;
    ((Handler)localObject1).postDelayed((Runnable)localObject2, l);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.customview.ThreeStepProgressView.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */