package com.truecaller.credit.app.ui.customview;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.LayoutParams;
import android.support.v7.widget.RecyclerView.State;
import android.view.View;
import c.g.b.k;
import c.u;

public final class b
  extends RecyclerView.ItemDecoration
{
  private final Drawable a;
  
  public b(Drawable paramDrawable)
  {
    a = paramDrawable;
  }
  
  public final void onDraw(Canvas paramCanvas, RecyclerView paramRecyclerView, RecyclerView.State paramState)
  {
    k.b(paramCanvas, "canvas");
    k.b(paramRecyclerView, "parent");
    String str = "state";
    k.b(paramState, str);
    int i = paramRecyclerView.getPaddingLeft();
    int j = paramRecyclerView.getWidth();
    int k = paramRecyclerView.getPaddingRight();
    j -= k;
    k = paramRecyclerView.getChildCount() + -1;
    if (k >= 0)
    {
      int m = 0;
      for (;;)
      {
        Object localObject1 = paramRecyclerView.getChildAt(m);
        k.a(localObject1, "child");
        Object localObject2 = ((View)localObject1).getLayoutParams();
        if (localObject2 == null) {
          break;
        }
        localObject2 = (RecyclerView.LayoutParams)localObject2;
        int n = ((View)localObject1).getBottom();
        int i1 = bottomMargin;
        n += i1;
        localObject2 = a;
        i1 = ((Drawable)localObject2).getIntrinsicHeight() + n;
        Drawable localDrawable = a;
        localDrawable.setBounds(i, n, j, i1);
        localObject1 = a;
        ((Drawable)localObject1).draw(paramCanvas);
        if (m == k) {
          return;
        }
        m += 1;
      }
      paramCanvas = new c/u;
      paramCanvas.<init>("null cannot be cast to non-null type android.support.v7.widget.RecyclerView.LayoutParams");
      throw paramCanvas;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.customview.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */