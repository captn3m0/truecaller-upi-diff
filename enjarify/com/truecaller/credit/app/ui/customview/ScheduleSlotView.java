package com.truecaller.credit.app.ui.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import c.g.b.k;
import com.truecaller.credit.R.layout;
import com.truecaller.credit.app.ui.customview.a.a;
import com.truecaller.credit.app.ui.customview.a.a.b;
import com.truecaller.credit.app.ui.customview.a.d;
import com.truecaller.credit.app.ui.customview.a.e;
import com.truecaller.credit.domain.interactors.infocollection.models.AppointmentData;
import com.truecaller.credit.domain.interactors.infocollection.models.Slot;
import java.util.HashMap;
import java.util.List;

public final class ScheduleSlotView
  extends LinearLayout
  implements a, e
{
  public com.truecaller.credit.app.ui.customview.a.c a;
  public d b;
  public c c;
  public int d;
  public int e = -1;
  public List f;
  private AttributeSet g;
  private HashMap h;
  
  public ScheduleSlotView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private ScheduleSlotView(Context paramContext, AttributeSet paramAttributeSet, char paramChar)
  {
    super(paramContext, paramAttributeSet, 0);
    g = paramAttributeSet;
    paramContext = LayoutInflater.from(getContext());
    int i = R.layout.layout_schedule_slot;
    Object localObject = this;
    localObject = (ViewGroup)this;
    int j = 1;
    paramContext.inflate(i, (ViewGroup)localObject, j);
    setOrientation(j);
  }
  
  private final void a()
  {
    Object localObject1 = f;
    if (localObject1 != null)
    {
      int i = d;
      Object localObject2 = (AppointmentData)((List)localObject1).get(i);
      int j = e;
      String str = null;
      if (j >= 0)
      {
        j = d;
        localObject1 = ((AppointmentData)((List)localObject1).get(j)).getSlots();
        j = e;
        localObject1 = (Slot)((List)localObject1).get(j);
      }
      else
      {
        localObject1 = null;
      }
      c localc = c;
      if (localc != null)
      {
        localObject2 = ((AppointmentData)localObject2).getDate();
        if (localObject1 != null) {
          str = ((Slot)localObject1).getSlot();
        }
        localc.a((String)localObject2, str);
        return;
      }
      return;
    }
  }
  
  public final void a(int paramInt)
  {
    d locald = b;
    if (locald != null)
    {
      locald.notifyItemChanged(paramInt);
      int i = e;
      if (i >= 0) {
        locald.notifyItemChanged(i);
      }
    }
    e = paramInt;
    a();
  }
  
  public final void b(int paramInt)
  {
    Object localObject1 = f;
    if (localObject1 != null)
    {
      Object localObject2 = a;
      int i;
      if (localObject2 != null)
      {
        ((com.truecaller.credit.app.ui.customview.a.c)localObject2).notifyItemChanged(paramInt);
        i = d;
        ((com.truecaller.credit.app.ui.customview.a.c)localObject2).notifyItemChanged(i);
      }
      int j = e;
      if (j >= 0)
      {
        j = d;
        localObject2 = ((AppointmentData)((List)localObject1).get(j)).getSlots();
        i = e;
        localObject2 = (Slot)((List)localObject2).get(i);
        i = 0;
        locald = null;
        ((Slot)localObject2).setSelected(false);
      }
      d = paramInt;
      j = -1;
      e = j;
      d locald = b;
      if (locald != null)
      {
        List localList = ((AppointmentData)((List)localObject1).get(paramInt)).getSlots();
        k.b(localList, "slots");
        localObject1 = a;
        String str = "slots";
        k.b(localList, str);
        a = j;
        b = localList;
        locald.notifyDataSetChanged();
      }
    }
    a();
  }
  
  public final View c(int paramInt)
  {
    Object localObject1 = h;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      h = ((HashMap)localObject1);
    }
    localObject1 = h;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = findViewById(paramInt);
      localObject2 = h;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.customview.ScheduleSlotView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */