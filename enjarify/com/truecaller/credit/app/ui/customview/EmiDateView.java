package com.truecaller.credit.app.ui.customview;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import c.g.b.k;
import com.truecaller.credit.R.id;
import com.truecaller.credit.R.layout;
import java.util.HashMap;

public final class EmiDateView
  extends FrameLayout
{
  private HashMap a;
  
  public EmiDateView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private EmiDateView(Context paramContext, AttributeSet paramAttributeSet, char paramChar)
  {
    super(paramContext, paramAttributeSet, 0);
    paramContext = LayoutInflater.from(paramContext);
    int i = R.layout.view_emi_date;
    Object localObject = this;
    localObject = (ViewGroup)this;
    paramContext.inflate(i, (ViewGroup)localObject, true);
  }
  
  private View a(int paramInt)
  {
    Object localObject1 = a;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      a = ((HashMap)localObject1);
    }
    localObject1 = a;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = findViewById(paramInt);
      localObject2 = a;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void setDay(String paramString)
  {
    k.b(paramString, "day");
    int i = R.id.textDay;
    AppCompatTextView localAppCompatTextView = (AppCompatTextView)a(i);
    k.a(localAppCompatTextView, "textDay");
    paramString = (CharSequence)paramString;
    localAppCompatTextView.setText(paramString);
  }
  
  public final void setMonth(String paramString)
  {
    k.b(paramString, "month");
    int i = R.id.textMonth;
    AppCompatTextView localAppCompatTextView = (AppCompatTextView)a(i);
    k.a(localAppCompatTextView, "textMonth");
    paramString = (CharSequence)paramString;
    localAppCompatTextView.setText(paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.customview.EmiDateView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */