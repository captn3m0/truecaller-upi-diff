package com.truecaller.credit.app.ui.customview;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Xfermode;
import android.support.v4.content.b;
import android.util.DisplayMetrics;
import android.view.View;
import c.g.b.k;
import com.truecaller.credit.R.color;
import com.truecaller.credit.app.ui.assist.CreditDocumentType;

public final class a
  extends View
{
  private float a;
  private int b;
  private int c;
  private float d;
  private float e;
  private float f;
  private float g;
  private float h;
  private final PorterDuffXfermode i;
  private final Paint j;
  private final Paint k;
  
  public a(Context paramContext, CreditDocumentType paramCreditDocumentType)
  {
    super(paramContext);
    Object localObject = paramContext.getResources();
    k.a(localObject, "context.resources");
    float f1 = getDisplayMetricsdensity;
    a = f1;
    int m = d;
    b = m;
    int n = e;
    c = n;
    float f2 = a;
    f1 = 32.0F * f2;
    d = f1;
    f2 *= 8.0F;
    e = f2;
    paramCreditDocumentType = new android/graphics/PorterDuffXfermode;
    localObject = PorterDuff.Mode.CLEAR;
    paramCreditDocumentType.<init>((PorterDuff.Mode)localObject);
    i = paramCreditDocumentType;
    paramCreditDocumentType = new android/graphics/Paint;
    paramCreditDocumentType.<init>(1);
    localObject = (Xfermode)i;
    paramCreditDocumentType.setXfermode((Xfermode)localObject);
    m = R.color.black_50;
    int i1 = b.c(paramContext, m);
    paramCreditDocumentType.setColor(i1);
    paramContext = Paint.Style.FILL;
    paramCreditDocumentType.setStyle(paramContext);
    j = paramCreditDocumentType;
    paramContext = new android/graphics/Paint;
    paramContext.<init>();
    paramCreditDocumentType = Paint.Style.STROKE;
    paramContext.setStyle(paramCreditDocumentType);
    paramContext.setColor(-1);
    paramContext.setStrokeWidth(5.0F);
    k = paramContext;
  }
  
  protected final void onDraw(Canvas paramCanvas)
  {
    k.b(paramCanvas, "canvas");
    super.onDraw(paramCanvas);
    float f1 = getWidth();
    float f2 = e;
    float f3 = 2.0F;
    f2 *= f3;
    f1 -= f2;
    f2 = b;
    f1 *= f2;
    f2 = c;
    f1 /= f2;
    f = f1;
    f1 = getHeight();
    f2 = f;
    f1 = (f1 - f2) / f3;
    g = f1;
    f1 = getHeight();
    float f4 = g;
    f1 -= f4;
    h = f1;
    float f5 = e;
    f1 = d;
    float f6 = f5 + f1;
    Paint localPaint = k;
    float f7 = f4;
    paramCanvas.drawLine(f5, f4, f6, f4, localPaint);
    f6 = e;
    f1 = g;
    float f8 = 2.5F;
    f7 = f1 - f8;
    f2 = d;
    f4 = f1 + f2;
    localPaint = k;
    f5 = f6;
    paramCanvas.drawLine(f6, f7, f6, f4, localPaint);
    f5 = e;
    f4 = h;
    f1 = d;
    f6 = f5 + f1;
    localPaint = k;
    f7 = f4;
    paramCanvas.drawLine(f5, f4, f6, f4, localPaint);
    f6 = e;
    f1 = h;
    f7 = f1 + f8;
    f2 = d;
    f4 = f1 - f2;
    localPaint = k;
    f5 = f6;
    paramCanvas.drawLine(f6, f7, f6, f4, localPaint);
    f1 = getWidth();
    f2 = e;
    f5 = d;
    f2 += f5;
    f5 = f1 - f2;
    f7 = g;
    f1 = getWidth();
    f2 = e;
    f6 = f1 - f2;
    f4 = g;
    localPaint = k;
    paramCanvas.drawLine(f5, f7, f6, f4, localPaint);
    f1 = getWidth();
    f2 = e;
    f5 = f1 - f2;
    f7 = g - f8;
    f1 = getWidth();
    f2 = e;
    f6 = f1 - f2;
    f1 = g;
    f2 = d;
    f4 = f1 + f2;
    localPaint = k;
    paramCanvas.drawLine(f5, f7, f6, f4, localPaint);
    f1 = getWidth();
    f2 = e;
    f5 = d;
    f2 += f5;
    f5 = f1 - f2;
    f7 = h;
    f1 = getWidth();
    f2 = e;
    f6 = f1 - f2;
    f4 = h;
    localPaint = k;
    paramCanvas.drawLine(f5, f7, f6, f4, localPaint);
    f1 = getWidth();
    f2 = e;
    f5 = f1 - f2;
    f7 = h + f8;
    f1 = getWidth();
    f2 = e;
    f6 = f1 - f2;
    f1 = h;
    f2 = d;
    f4 = f1 - f2;
    localPaint = k;
    paramCanvas.drawLine(f5, f7, f6, f4, localPaint);
    f1 = e;
    f5 = f1 * f3;
    f7 = g + f1;
    f1 = getWidth();
    f2 = e;
    f3 *= f2;
    f6 = f1 - f3;
    f4 = h - f2;
    localPaint = j;
    paramCanvas.drawRect(f5, f7, f6, f4, localPaint);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.customview.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */