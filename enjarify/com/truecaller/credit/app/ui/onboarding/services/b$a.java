package com.truecaller.credit.app.ui.onboarding.services;

import androidx.work.p;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.google.gson.l;
import com.google.gson.o;
import com.google.gson.q;
import com.google.gson.t;
import com.truecaller.credit.app.core.CreditFeatureSyncWorker;
import com.truecaller.credit.app.core.CreditFeatureSyncWorker.a;
import com.truecaller.credit.data.Result;
import com.truecaller.credit.data.Success;
import com.truecaller.credit.data.repository.CreditRepository;
import com.truecaller.credit.domain.interactors.onboarding.models.PreScoreDataRequest;
import com.truecaller.credit.j;
import kotlinx.coroutines.ag;

final class b$a
  extends c.d.b.a.k
  implements m
{
  boolean a;
  Object b;
  Object c;
  int d;
  private ag f;
  
  b$a(b paramb, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/credit/app/ui/onboarding/services/b$a;
    b localb = e;
    locala.<init>(localb, paramc);
    paramObject = (ag)paramObject;
    f = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = d;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 2: 
      boolean bool2 = paramObject instanceof o.b;
      if (!bool2) {
        break;
      }
    }
    try
    {
      paramObject = (o.b)paramObject;
      paramObject = a;
      throw ((Throwable)paramObject);
    }
    catch (t localt)
    {
      boolean bool1;
      int j;
      Object localObject2;
      Object localObject3;
      String str;
      int k;
      Object localObject4;
      boolean bool4;
      boolean bool3;
      paramObject = e;
      localObject1 = "failure";
      b.a((b)paramObject, (String)localObject1);
    }
    bool1 = a;
    j = paramObject instanceof o.b;
    if (j != 0)
    {
      throw a;
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label437;
      }
      bool1 = e.a.a();
      paramObject = e;
      localObject2 = "initiated";
      b.a((b)paramObject, (String)localObject2);
      paramObject = e.a;
      j = 1;
      a = j;
      d = j;
      paramObject = ((com.truecaller.credit.k)paramObject).a(this);
      if (paramObject == localObject1) {
        return localObject1;
      }
    }
    paramObject = (Result)paramObject;
    j = paramObject instanceof Success;
    if (j != 0)
    {
      localObject2 = paramObject;
      localObject2 = (Success)paramObject;
      localObject2 = ((Success)localObject2).getData();
      localObject2 = (String)localObject2;
      localObject2 = q.a((String)localObject2);
      localObject3 = "jsonParser.parse(result.data)";
      c.g.b.k.a(localObject2, (String)localObject3);
      localObject2 = ((l)localObject2).i();
      localObject3 = e;
      str = "score";
      c.g.b.k.a(localObject2, str);
      a = bool1;
      b = paramObject;
      c = localObject2;
      k = 2;
      d = k;
      paramObject = b;
      localObject4 = new com/truecaller/credit/domain/interactors/onboarding/models/PreScoreDataRequest;
      localObject3 = d;
      bool4 = ((j)localObject3).g();
      ((PreScoreDataRequest)localObject4).<init>((o)localObject2, bool4);
      paramObject = ((CreditRepository)paramObject).uploadPreScoreData((PreScoreDataRequest)localObject4, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      paramObject = (Result)paramObject;
      bool3 = paramObject instanceof Success;
      if (!bool3) {
        break label421;
      }
      paramObject = CreditFeatureSyncWorker.d;
      paramObject = p.a();
      localObject1 = "WorkManager.getInstance()";
      c.g.b.k.a(paramObject, (String)localObject1);
      localObject1 = "CreditFeatureSyncWorkerOneOff";
      localObject4 = androidx.work.g.a;
      localObject2 = CreditFeatureSyncWorker.d;
      localObject2 = ((CreditFeatureSyncWorker.a)localObject2).a();
      localObject2 = ((com.truecaller.common.background.g)localObject2).b();
      ((p)paramObject).a((String)localObject1, (androidx.work.g)localObject4, (androidx.work.k)localObject2);
    }
    label421:
    e.a.b();
    return x.a;
    label437:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.onboarding.services.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */