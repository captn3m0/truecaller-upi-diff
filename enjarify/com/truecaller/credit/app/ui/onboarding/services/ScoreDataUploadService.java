package com.truecaller.credit.app.ui.onboarding.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.d;
import c.g.b.k;
import com.google.gson.t;
import com.truecaller.credit.app.ui.onboarding.b.a.a.a;
import com.truecaller.credit.app.ui.onboarding.b.a.b;
import com.truecaller.credit.app.ui.onboarding.views.c.i.b;
import com.truecaller.credit.i;

public final class ScoreDataUploadService
  extends Service
  implements i.b
{
  public static final ScoreDataUploadService.a b;
  public com.truecaller.credit.app.ui.onboarding.views.c.i.a a;
  
  static
  {
    ScoreDataUploadService.a locala = new com/truecaller/credit/app/ui/onboarding/services/ScoreDataUploadService$a;
    locala.<init>((byte)0);
    b = locala;
  }
  
  private final void a(Intent paramIntent)
  {
    d.a((Context)this).a(paramIntent);
  }
  
  public final void a()
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("credit_line_offer");
    localIntent.putExtra("extra_state", "state_start");
    a(localIntent);
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "message");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("credit_line_offer");
    localIntent.putExtra("extra_state", "state_offer_generation_failed");
    localIntent.putExtra("extra_message", paramString);
    a(localIntent);
  }
  
  public final void b()
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("credit_line_offer");
    localIntent.putExtra("extra_state", "state_calculation");
    a(localIntent);
  }
  
  public final void c()
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("credit_line_offer");
    localIntent.putExtra("extra_state", "state_offer_generated");
    localIntent.putExtra("source", "pan_details");
    a(localIntent);
  }
  
  public final IBinder onBind(Intent paramIntent)
  {
    return null;
  }
  
  public final void onCreate()
  {
    super.onCreate();
    Object localObject1 = com.truecaller.credit.app.ui.onboarding.b.a.a.a();
    Object localObject2 = i.i;
    localObject2 = com.truecaller.credit.i.a.a();
    ((a.a)localObject1).a((com.truecaller.credit.app.c.a.a)localObject2).a().a(this);
    localObject1 = a;
    if (localObject1 == null)
    {
      localObject2 = "presenter";
      k.a((String)localObject2);
    }
    ((com.truecaller.credit.app.ui.onboarding.views.c.i.a)localObject1).a(this);
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    com.truecaller.credit.app.ui.onboarding.views.c.i.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    locala.y_();
  }
  
  public final int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
  {
    paramInt1 = 2;
    if (paramIntent == null) {
      return paramInt1;
    }
    try
    {
      paramIntent = paramIntent.getExtras();
      String str;
      if (paramIntent != null)
      {
        localObject = "extra_state";
        str = "state_start";
        paramIntent = paramIntent.getString((String)localObject, str);
      }
      else
      {
        paramIntent = null;
      }
      Object localObject = a;
      if (localObject == null)
      {
        str = "presenter";
        k.a(str);
      }
      ((com.truecaller.credit.app.ui.onboarding.views.c.i.a)localObject).a(paramIntent);
      return 1;
    }
    catch (t localt) {}
    return paramInt1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.onboarding.services.ScoreDataUploadService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */