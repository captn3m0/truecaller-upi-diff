package com.truecaller.credit.app.ui.onboarding.services;

import android.content.Context;
import android.content.Intent;
import c.g.b.k;

public final class ScoreDataUploadService$a
{
  public static Intent a(Context paramContext)
  {
    k.b(paramContext, "context");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, ScoreDataUploadService.class);
    return localIntent;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.onboarding.services.ScoreDataUploadService.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */