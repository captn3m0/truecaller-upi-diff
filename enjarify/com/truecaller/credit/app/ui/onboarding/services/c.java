package com.truecaller.credit.app.ui.onboarding.services;

import dagger.a.d;
import javax.inject.Provider;

public final class c
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  
  private c(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
    d = paramProvider4;
    e = paramProvider5;
  }
  
  public static c a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5)
  {
    c localc = new com/truecaller/credit/app/ui/onboarding/services/c;
    localc.<init>(paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5);
    return localc;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.onboarding.services.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */