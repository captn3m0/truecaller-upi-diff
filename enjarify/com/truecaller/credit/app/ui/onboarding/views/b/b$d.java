package com.truecaller.credit.app.ui.onboarding.views.b;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Telephony.Sms.Intents;
import android.telephony.SmsMessage;
import c.a.f;
import com.truecaller.credit.app.ui.onboarding.views.c.a.a;

public final class b$d
  extends BroadcastReceiver
{
  b$d(b paramb) {}
  
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    paramContext = Telephony.Sms.Intents.getMessagesFromIntent(paramIntent);
    if (paramContext != null) {
      paramContext = (SmsMessage)f.c(paramContext);
    } else {
      paramContext = null;
    }
    a.d().a(paramContext);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.onboarding.views.b.b.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */