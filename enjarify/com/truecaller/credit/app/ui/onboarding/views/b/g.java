package com.truecaller.credit.app.ui.onboarding.views.b;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v4.app.j;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import c.g.b.k;
import c.u;
import com.truecaller.credit.R.id;
import com.truecaller.credit.R.layout;
import com.truecaller.credit.R.string;
import com.truecaller.credit.app.ui.b.c;
import com.truecaller.credit.app.ui.faq.activities.CreditWebViewActivity;
import com.truecaller.credit.app.ui.onboarding.b.a.a.a;
import com.truecaller.credit.i;
import com.truecaller.credit.i.a;
import java.util.HashMap;

public final class g
  extends c
  implements View.OnClickListener, b.a, com.truecaller.credit.app.ui.onboarding.views.c.g.b
{
  public static final g.a c;
  private g.b d;
  private HashMap e;
  
  static
  {
    g.a locala = new com/truecaller/credit/app/ui/onboarding/views/b/g$a;
    locala.<init>((byte)0);
    c = locala;
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = e;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      e = ((HashMap)localObject1);
    }
    localObject1 = e;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = e;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "analyticsContext");
    g.b localb = d;
    if (localb != null)
    {
      localb.c(paramString);
      return;
    }
  }
  
  public final void a(String paramString1, String paramString2, String paramString3)
  {
    k.b(paramString1, "finalOffer");
    k.b(paramString2, "interestRate");
    k.b(paramString3, "tenure");
    int i = R.id.tvOfferAmount;
    Object localObject = (TextView)a(i);
    k.a(localObject, "tvOfferAmount");
    int j = R.string.rs_offer_amount;
    int k = 1;
    Object[] arrayOfObject = new Object[k];
    arrayOfObject[0] = paramString1;
    paramString1 = (CharSequence)getString(j, arrayOfObject);
    ((TextView)localObject).setText(paramString1);
    paramString1 = new android/text/SpannableStringBuilder;
    paramString1.<init>();
    i = R.string.final_offer_title_interest_rate;
    localObject = (CharSequence)getString(i);
    paramString1 = paramString1.append((CharSequence)localObject);
    k.a(paramString1, "SpannableStringBuilder()…fer_title_interest_rate))");
    localObject = new android/text/style/StyleSpan;
    ((StyleSpan)localObject).<init>(k);
    j = paramString1.length();
    paramString2 = (CharSequence)paramString2;
    paramString1.append(paramString2);
    int m = paramString1.length();
    int n = 17;
    paramString1.setSpan(localObject, j, m, n);
    paramString2 = new android/text/SpannableStringBuilder;
    paramString2.<init>();
    i = R.string.final_offer_title_repayment;
    localObject = (CharSequence)getString(i);
    paramString2 = paramString2.append((CharSequence)localObject);
    k.a(paramString2, "SpannableStringBuilder()…l_offer_title_repayment))");
    localObject = new android/text/style/StyleSpan;
    ((StyleSpan)localObject).<init>(k);
    j = paramString2.length();
    paramString3 = (CharSequence)paramString3;
    paramString2.append(paramString3);
    int i1 = paramString2.length();
    paramString2.setSpan(localObject, j, i1, n);
    i1 = R.id.tvOfferInterestRates;
    paramString3 = (TextView)a(i1);
    k.a(paramString3, "tvOfferInterestRates");
    paramString1 = (CharSequence)paramString1;
    paramString3.setText(paramString1);
    int i2 = R.id.tvOfferRepayment;
    paramString1 = (TextView)a(i2);
    k.a(paramString1, "tvOfferRepayment");
    paramString2 = (CharSequence)paramString2;
    paramString1.setText(paramString2);
  }
  
  public final void b()
  {
    a.a locala = com.truecaller.credit.app.ui.onboarding.b.a.a.a();
    Object localObject = i.i;
    localObject = i.a.a();
    locala.a((com.truecaller.credit.app.c.a.a)localObject).a().a(this);
  }
  
  public final void b(String paramString)
  {
    g.b localb = d;
    if (localb != null)
    {
      localb.b(paramString);
      return;
    }
  }
  
  public final int c()
  {
    return R.layout.fragment_display_offer;
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "termsAndConditionsUrl");
    Intent localIntent = new android/content/Intent;
    Context localContext = getContext();
    localIntent.<init>(localContext, CreditWebViewActivity.class);
    localIntent.putExtra("url", paramString);
    startActivity(localIntent);
  }
  
  public final void f()
  {
    HashMap localHashMap = e;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final void g()
  {
    int i = R.id.viewInfo;
    FrameLayout localFrameLayout = (FrameLayout)a(i);
    Object localObject = this;
    localObject = (View.OnClickListener)this;
    localFrameLayout.setOnClickListener((View.OnClickListener)localObject);
  }
  
  public final void h()
  {
    f localf = getActivity();
    if (localf != null)
    {
      localf.finish();
      return;
    }
  }
  
  public final void i()
  {
    b localb = new com/truecaller/credit/app/ui/onboarding/views/b/b;
    localb.<init>();
    Object localObject = this;
    localObject = (b.a)this;
    k.b(localObject, "listener");
    c = ((b.a)localObject);
    localObject = getFragmentManager();
    String str = b.class.getName();
    localb.show((j)localObject, str);
  }
  
  public final void j()
  {
    h localh = new com/truecaller/credit/app/ui/onboarding/views/b/h;
    localh.<init>();
    Object localObject = getActivity();
    if (localObject != null) {
      localObject = ((f)localObject).getSupportFragmentManager();
    } else {
      localObject = null;
    }
    String str = h.class.getSimpleName();
    localh.show((j)localObject, str);
  }
  
  public final void onAttach(Context paramContext)
  {
    Object localObject = "context";
    k.b(paramContext, (String)localObject);
    super.onAttach(paramContext);
    boolean bool = paramContext instanceof g.b;
    if (bool)
    {
      paramContext = (g.b)paramContext;
      d = paramContext;
      return;
    }
    localObject = new java/lang/RuntimeException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    paramContext = paramContext.toString();
    localStringBuilder.append(paramContext);
    localStringBuilder.append(" must implement OnFragmentInteractionListener");
    paramContext = localStringBuilder.toString();
    ((RuntimeException)localObject).<init>(paramContext);
    throw ((Throwable)localObject);
  }
  
  public final void onClick(View paramView)
  {
    int i = R.id.viewInfo;
    FrameLayout localFrameLayout = (FrameLayout)a(i);
    boolean bool = k.a(paramView, localFrameLayout);
    if (bool)
    {
      paramView = (com.truecaller.credit.app.ui.onboarding.views.c.g.a)a();
      paramView.a();
    }
  }
  
  public final void onDetach()
  {
    super.onDetach();
    d = null;
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    String str = "view";
    k.b(paramView, str);
    super.onViewCreated(paramView, paramBundle);
    ((com.truecaller.credit.app.ui.onboarding.views.c.g.a)a()).e();
    int i = R.id.btnClose;
    paramView = (ImageView)a(i);
    paramBundle = new com/truecaller/credit/app/ui/onboarding/views/b/g$c;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    i = R.id.tvOfferTermsAndConditions;
    paramView = (TextView)a(i);
    paramBundle = new com/truecaller/credit/app/ui/onboarding/views/b/g$d;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    i = R.id.btnGetStarted;
    paramView = (Button)a(i);
    paramBundle = new com/truecaller/credit/app/ui/onboarding/views/b/g$e;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    paramView = getActivity();
    if (paramView != null)
    {
      paramView = (AppCompatActivity)paramView;
      int j = R.id.initialOfferToolbar;
      paramBundle = (Toolbar)a(j);
      paramView.setSupportActionBar(paramBundle);
      paramView = paramView.getSupportActionBar();
      if (paramView != null)
      {
        paramView.setElevation(0.0F);
        return;
      }
      return;
    }
    paramView = new c/u;
    paramView.<init>("null cannot be cast to non-null type android.support.v7.app.AppCompatActivity");
    throw paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.onboarding.views.b.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */