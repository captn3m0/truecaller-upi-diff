package com.truecaller.credit.app.ui.onboarding.views.b;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.credit.R.id;
import com.truecaller.credit.R.layout;
import com.truecaller.credit.app.ui.onboarding.b.a.a.a;
import com.truecaller.credit.app.ui.onboarding.b.a.b;
import com.truecaller.credit.app.ui.onboarding.c.o;
import com.truecaller.credit.app.ui.onboarding.views.c.h.b;
import com.truecaller.credit.i;
import com.truecaller.credit.i.a;
import java.util.HashMap;

public final class h
  extends AppCompatDialogFragment
  implements View.OnClickListener, h.b
{
  public o a;
  private HashMap b;
  
  private View a(int paramInt)
  {
    Object localObject1 = b;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      b = ((HashMap)localObject1);
    }
    localObject1 = b;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = b;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a()
  {
    int i = R.id.btnClose;
    ImageView localImageView = (ImageView)a(i);
    Object localObject = this;
    localObject = (View.OnClickListener)this;
    localImageView.setOnClickListener((View.OnClickListener)localObject);
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "title");
    int i = R.id.tvTitle;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "tvTitle");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void b()
  {
    dismiss();
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "content");
    int i = R.id.tvContent;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "tvContent");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void onClick(View paramView)
  {
    k.b(paramView, "v");
    int i = R.id.btnClose;
    Object localObject = (ImageView)a(i);
    boolean bool = k.a(paramView, localObject);
    if (bool)
    {
      paramView = a;
      if (paramView == null)
      {
        localObject = "presenter";
        k.a((String)localObject);
      }
      paramView = (h.b)b;
      if (paramView != null)
      {
        paramView.b();
        return;
      }
    }
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = com.truecaller.credit.app.ui.onboarding.b.a.a.a();
    Object localObject = i.i;
    localObject = i.a.a();
    paramBundle.a((com.truecaller.credit.app.c.a.a)localObject).a().a(this);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    int i = R.layout.layout_offer_info_dialog;
    return paramLayoutInflater.inflate(i, paramViewGroup, false);
  }
  
  public final void onStart()
  {
    super.onStart();
    Object localObject = getDialog();
    String str = "dialog";
    k.a(localObject, str);
    localObject = ((Dialog)localObject).getWindow();
    if (localObject != null)
    {
      ((Window)localObject).setLayout(-1, -2);
      return;
    }
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    String str = "view";
    k.b(paramView, str);
    super.onViewCreated(paramView, paramBundle);
    paramView = a;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramBundle = this;
    paramBundle = (h.b)this;
    paramView.a(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.onboarding.views.b.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */