package com.truecaller.credit.app.ui.onboarding.views.b;

import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v4.app.j;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import c.g.b.k;
import c.u;
import com.truecaller.credit.R.drawable;
import com.truecaller.credit.R.id;
import com.truecaller.credit.R.layout;
import com.truecaller.credit.R.string;
import com.truecaller.credit.data.models.APIStatusMessage;
import com.truecaller.credit.i;
import com.truecaller.credit.i.a;
import com.truecaller.utils.extensions.p;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;
import java.util.List;

public final class c
  extends com.truecaller.credit.app.ui.b.c
  implements AdapterView.OnItemSelectedListener, e.a, com.truecaller.credit.app.ui.onboarding.views.c.c.b
{
  public static final c.a c;
  private c.b d;
  private com.truecaller.credit.app.ui.onboarding.views.a.a e;
  private HashMap f;
  
  static
  {
    c.a locala = new com/truecaller/credit/app/ui/onboarding/views/b/c$a;
    locala.<init>((byte)0);
    c = locala;
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = f;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      f = ((HashMap)localObject1);
    }
    localObject1 = f;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = f;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a(int paramInt, String[] paramArrayOfString)
  {
    k.b(paramArrayOfString, "requiredPermissions");
    requestPermissions(paramArrayOfString, paramInt);
  }
  
  public final void a(APIStatusMessage paramAPIStatusMessage)
  {
    k.b(paramAPIStatusMessage, "apiStatusMessage");
    Object localObject = getActivity();
    if (localObject != null)
    {
      localObject = com.truecaller.credit.app.ui.infocollection.views.b.a.d;
      paramAPIStatusMessage = com.truecaller.credit.app.ui.infocollection.views.b.a.a.a(paramAPIStatusMessage);
      localObject = getFragmentManager();
      String str = "APIStatusFragment";
      paramAPIStatusMessage.show((j)localObject, str);
    }
  }
  
  public final void a(com.truecaller.credit.domain.interactors.onboarding.models.b paramb)
  {
    k.b(paramb, "data");
    int i = R.id.tvPanName;
    Object localObject = (TextView)a(i);
    paramb = (CharSequence)a;
    ((TextView)localObject).setText(paramb);
    ((TextView)localObject).setVisibility(0);
    i = R.id.tilEtPanNumber;
    localObject = (TextInputLayout)a(i);
    k.a(localObject, "tilEtPanNumber");
    ((TextInputLayout)localObject).setErrorEnabled(false);
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "error");
    Context localContext = requireContext();
    paramString = (CharSequence)paramString;
    Toast.makeText(localContext, paramString, 0).show();
  }
  
  public final void a(List paramList)
  {
    k.b(paramList, "data");
    Object localObject1 = new com/truecaller/credit/app/ui/onboarding/views/a/a;
    Object localObject2 = requireContext();
    String str = "requireContext()";
    k.a(localObject2, str);
    ((com.truecaller.credit.app.ui.onboarding.views.a.a)localObject1).<init>((Context)localObject2, paramList);
    e = ((com.truecaller.credit.app.ui.onboarding.views.a.a)localObject1);
    paramList = e;
    if (paramList == null)
    {
      localObject1 = "sprCityAdapter";
      k.a((String)localObject1);
    }
    int i = 17367049;
    paramList.setDropDownViewResource(i);
    int j = R.id.sprCity;
    paramList = (AppCompatSpinner)a(j);
    localObject1 = e;
    if (localObject1 == null)
    {
      localObject2 = "sprCityAdapter";
      k.a((String)localObject2);
    }
    localObject1 = (SpinnerAdapter)localObject1;
    paramList.setAdapter((SpinnerAdapter)localObject1);
    paramList.setSelected(false);
  }
  
  public final void a(boolean paramBoolean)
  {
    int i = R.id.pbPanNumber;
    Object localObject = (ProgressBar)a(i);
    k.a(localObject, "pbPanNumber");
    t.a((View)localObject, paramBoolean);
    i = R.id.etPanNumber;
    localObject = (EditText)a(i);
    k.a(localObject, "etPanNumber");
    paramBoolean ^= true;
    ((EditText)localObject).setEnabled(paramBoolean);
  }
  
  public final void b()
  {
    com.truecaller.credit.app.ui.onboarding.b.a.a.a locala = com.truecaller.credit.app.ui.onboarding.b.a.a.a();
    Object localObject = i.i;
    localObject = i.a.a();
    locala.a((com.truecaller.credit.app.c.a.a)localObject).a().a(this);
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "error");
    int i = R.id.tilEtPanNumber;
    TextInputLayout localTextInputLayout = (TextInputLayout)a(i);
    k.a(localTextInputLayout, "tilEtPanNumber");
    paramString = (CharSequence)paramString;
    localTextInputLayout.setError(paramString);
    int j = R.id.tilEtPanNumber;
    paramString = (TextInputLayout)a(j);
    k.a(paramString, "tilEtPanNumber");
    paramString.setErrorEnabled(true);
    j = R.id.tvPanName;
    paramString = (TextView)a(j);
    k.a(paramString, "tvPanName");
    paramString.setVisibility(8);
    j = R.id.btnGet;
    paramString = (Button)a(j);
    k.a(paramString, "btnGet");
    paramString.setEnabled(false);
  }
  
  public final int c()
  {
    return R.layout.fragment_initial_offer_details;
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "city");
    int i = R.id.textSpinner;
    EditText localEditText = (EditText)a(i);
    paramString = (CharSequence)paramString;
    localEditText.setText(paramString);
  }
  
  public final void d(String paramString)
  {
    k.b(paramString, "message");
    Context localContext = requireContext();
    paramString = (CharSequence)paramString;
    Toast.makeText(localContext, paramString, 0).show();
  }
  
  public final void f()
  {
    HashMap localHashMap = f;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final void g()
  {
    Object localObject1 = getActivity();
    if (localObject1 != null)
    {
      localObject1 = (AppCompatActivity)localObject1;
      int i = R.id.toolbar;
      Object localObject2 = (Toolbar)a(i);
      ((AppCompatActivity)localObject1).setSupportActionBar((Toolbar)localObject2);
      localObject2 = ((AppCompatActivity)localObject1).getSupportActionBar();
      boolean bool;
      if (localObject2 != null)
      {
        bool = true;
        ((ActionBar)localObject2).setDisplayHomeAsUpEnabled(bool);
      }
      localObject2 = (CharSequence)"";
      ((AppCompatActivity)localObject1).setTitle((CharSequence)localObject2);
      localObject2 = ((AppCompatActivity)localObject1).getSupportActionBar();
      if (localObject2 != null)
      {
        bool = false;
        ((ActionBar)localObject2).setElevation(0.0F);
      }
      localObject1 = ((AppCompatActivity)localObject1).getSupportActionBar();
      if (localObject1 != null)
      {
        i = R.drawable.ic_credit_back_white;
        ((ActionBar)localObject1).setHomeAsUpIndicator(i);
      }
      int j = R.id.etPanNumber;
      localObject1 = (EditText)a(j);
      k.a(localObject1, "etPanNumber");
      localObject1 = (TextView)localObject1;
      localObject2 = new com/truecaller/credit/app/ui/onboarding/views/b/c$e;
      ((c.e)localObject2).<init>(this);
      localObject2 = (c.g.a.b)localObject2;
      p.a((TextView)localObject1, (c.g.a.b)localObject2);
      j = R.id.btnGet;
      localObject1 = (Button)a(j);
      localObject2 = new com/truecaller/credit/app/ui/onboarding/views/b/c$c;
      ((c.c)localObject2).<init>(this);
      localObject2 = (View.OnClickListener)localObject2;
      ((Button)localObject1).setOnClickListener((View.OnClickListener)localObject2);
      j = R.id.textSpinner;
      localObject1 = (EditText)a(j);
      localObject2 = new com/truecaller/credit/app/ui/onboarding/views/b/c$d;
      ((c.d)localObject2).<init>(this);
      localObject2 = (View.OnClickListener)localObject2;
      ((EditText)localObject1).setOnClickListener((View.OnClickListener)localObject2);
      j = R.id.sprCity;
      localObject1 = (AppCompatSpinner)a(j);
      k.a(localObject1, "sprCity");
      localObject2 = this;
      localObject2 = (AdapterView.OnItemSelectedListener)this;
      ((AppCompatSpinner)localObject1).setOnItemSelectedListener((AdapterView.OnItemSelectedListener)localObject2);
      return;
    }
    localObject1 = new c/u;
    ((u)localObject1).<init>("null cannot be cast to non-null type android.support.v7.app.AppCompatActivity");
    throw ((Throwable)localObject1);
  }
  
  public final void h()
  {
    int i = R.id.tilEtPanNumber;
    Object localObject = (TextInputLayout)a(i);
    k.a(localObject, "tilEtPanNumber");
    int j = R.string.inital_offer_pan_invalid_desc;
    CharSequence localCharSequence = (CharSequence)getString(j);
    ((TextInputLayout)localObject).setError(localCharSequence);
    i = R.id.tilEtPanNumber;
    localObject = (TextInputLayout)a(i);
    k.a(localObject, "tilEtPanNumber");
    ((TextInputLayout)localObject).setErrorEnabled(true);
    i = R.id.tvPanName;
    localObject = (TextView)a(i);
    k.a(localObject, "tvPanName");
    ((TextView)localObject).setVisibility(8);
    i = R.id.btnGet;
    localObject = (Button)a(i);
    k.a(localObject, "btnGet");
    ((Button)localObject).setEnabled(false);
  }
  
  public final void i()
  {
    int i = R.id.sprCity;
    Object localObject1 = (AppCompatSpinner)a(i);
    Object localObject2 = "sprCity";
    k.a(localObject1, (String)localObject2);
    localObject1 = ((AppCompatSpinner)localObject1).getSelectedItem();
    if (localObject1 != null)
    {
      localObject1 = (String)localObject1;
      int j = R.string.initial_offer_city_hint_title;
      localObject2 = getString(j);
      boolean bool = ((String)localObject1).equals(localObject2);
      if (bool)
      {
        localObject1 = (Context)getActivity();
        j = R.string.initial_offer_details_location_unselected_error;
        localObject2 = (CharSequence)getString(j);
        Toast.makeText((Context)localObject1, (CharSequence)localObject2, 0).show();
        return;
      }
      localObject1 = d;
      if (localObject1 != null)
      {
        ((c.b)localObject1).f();
        return;
      }
      return;
    }
    localObject1 = new c/u;
    ((u)localObject1).<init>("null cannot be cast to non-null type kotlin.String");
    throw ((Throwable)localObject1);
  }
  
  public final void j()
  {
    Object localObject1 = getActivity();
    if (localObject1 != null)
    {
      Object localObject2 = new android/support/v7/app/AlertDialog$Builder;
      localObject1 = (Context)localObject1;
      ((AlertDialog.Builder)localObject2).<init>((Context)localObject1);
      int i = R.string.credit_miui_permission_tittle;
      localObject1 = (CharSequence)getString(i);
      localObject1 = ((AlertDialog.Builder)localObject2).setTitle((CharSequence)localObject1);
      int j = R.string.credit_miui_permission_message;
      localObject2 = (CharSequence)getString(j);
      localObject1 = ((AlertDialog.Builder)localObject1).setMessage((CharSequence)localObject2).setCancelable(false);
      j = R.string.credit_miui_permission_negative_btn;
      localObject2 = (CharSequence)getString(j);
      Object localObject3 = (DialogInterface.OnClickListener)c.g.a;
      localObject1 = ((AlertDialog.Builder)localObject1).setNegativeButton((CharSequence)localObject2, (DialogInterface.OnClickListener)localObject3);
      j = R.string.credit_miui_permission_positive_btn;
      localObject2 = (CharSequence)getString(j);
      localObject3 = new com/truecaller/credit/app/ui/onboarding/views/b/c$f;
      ((c.f)localObject3).<init>(this);
      localObject3 = (DialogInterface.OnClickListener)localObject3;
      ((AlertDialog.Builder)localObject1).setPositiveButton((CharSequence)localObject2, (DialogInterface.OnClickListener)localObject3).create().show();
      return;
    }
  }
  
  public final void k()
  {
    int i = R.id.sprCity;
    ((AppCompatSpinner)a(i)).performClick();
  }
  
  public final void l()
  {
    f localf = getActivity();
    if (localf != null)
    {
      localf.finish();
      return;
    }
  }
  
  public final void m()
  {
    e locale = new com/truecaller/credit/app/ui/onboarding/views/b/e;
    locale.<init>();
    Object localObject = this;
    localObject = (e.a)this;
    k.b(localObject, "listener");
    c = ((e.a)localObject);
    localObject = getFragmentManager();
    String str = e.class.getName();
    locale.show((j)localObject, str);
  }
  
  public final String n()
  {
    int i = R.id.textSpinner;
    EditText localEditText = (EditText)a(i);
    k.a(localEditText, "textSpinner");
    return localEditText.getText().toString();
  }
  
  public final void o()
  {
    int i = R.id.btnGet;
    Button localButton = (Button)a(i);
    k.a(localButton, "btnGet");
    localButton.setEnabled(true);
  }
  
  public final void onAttach(Context paramContext)
  {
    Object localObject = "context";
    k.b(paramContext, (String)localObject);
    super.onAttach(paramContext);
    boolean bool = paramContext instanceof c.b;
    if (bool)
    {
      paramContext = (c.b)paramContext;
      d = paramContext;
      return;
    }
    localObject = new java/lang/RuntimeException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(paramContext);
    localStringBuilder.append(" must implement OnFragmentInteractionListener");
    paramContext = localStringBuilder.toString();
    ((RuntimeException)localObject).<init>(paramContext);
    throw ((Throwable)localObject);
  }
  
  public final void onDetach()
  {
    super.onDetach();
    d = null;
  }
  
  public final void onItemSelected(AdapterView paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    ((com.truecaller.credit.app.ui.onboarding.views.c.c.a)a()).a(paramInt);
  }
  
  public final void onNothingSelected(AdapterView paramAdapterView) {}
  
  public final void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    k.b(paramArrayOfString, "permissions");
    k.b(paramArrayOfInt, "grantResults");
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    ((com.truecaller.credit.app.ui.onboarding.views.c.c.a)a()).a(paramInt, paramArrayOfString, paramArrayOfInt);
  }
  
  public final void p()
  {
    int i = R.id.btnGet;
    Button localButton = (Button)a(i);
    k.a(localButton, "btnGet");
    localButton.setEnabled(false);
  }
  
  public final void q()
  {
    int i = R.id.pbCity;
    ProgressBar localProgressBar = (ProgressBar)a(i);
    k.a(localProgressBar, "pbCity");
    t.a((View)localProgressBar);
  }
  
  public final void r()
  {
    int i = R.id.pbCity;
    ProgressBar localProgressBar = (ProgressBar)a(i);
    k.a(localProgressBar, "pbCity");
    t.b((View)localProgressBar);
  }
  
  public final void s()
  {
    int i = R.id.textSpinner;
    EditText localEditText = (EditText)a(i);
    int j = R.drawable.ic_credit_down_arrow;
    localEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, j, 0);
  }
  
  public final void t()
  {
    Object localObject = getActivity();
    if (localObject != null)
    {
      localObject = getFragmentManager();
      if (localObject != null)
      {
        String str = "APIStatusFragment";
        localObject = ((j)localObject).a(str);
      }
      else
      {
        localObject = null;
      }
      if (localObject != null)
      {
        localObject = (com.truecaller.credit.app.ui.infocollection.views.b.a)localObject;
        ((com.truecaller.credit.app.ui.infocollection.views.b.a)localObject).e();
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.onboarding.views.b.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */