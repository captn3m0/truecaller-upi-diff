package com.truecaller.credit.app.ui.onboarding.views.b;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import c.g.b.k;
import c.u;
import com.truecaller.credit.R.id;
import com.truecaller.credit.R.layout;
import com.truecaller.credit.i;
import com.truecaller.credit.i.a;
import java.util.HashMap;

public final class a
  extends com.truecaller.credit.app.ui.b.a
{
  public static final a.a c;
  public com.truecaller.credit.app.a.b b;
  private a.b d;
  private HashMap e;
  
  static
  {
    a.a locala = new com/truecaller/credit/app/ui/onboarding/views/b/a$a;
    locala.<init>((byte)0);
    c = locala;
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = e;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      e = ((HashMap)localObject1);
    }
    localObject1 = e;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = e;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a()
  {
    com.truecaller.credit.app.ui.onboarding.b.a.a.a locala = com.truecaller.credit.app.ui.onboarding.b.a.a.a();
    Object localObject = i.i;
    localObject = i.a.a();
    locala.a((com.truecaller.credit.app.c.a.a)localObject).a().a(this);
  }
  
  public final int b()
  {
    return R.layout.fragment_credit_ineligible;
  }
  
  public final void c()
  {
    HashMap localHashMap = e;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = d;
    if (paramBundle == null)
    {
      paramBundle = getActivity();
      boolean bool = paramBundle instanceof a.b;
      if (bool)
      {
        paramBundle = getActivity();
        if (paramBundle != null)
        {
          paramBundle = (a.b)paramBundle;
          d = paramBundle;
          return;
        }
        paramBundle = new c/u;
        paramBundle.<init>("null cannot be cast to non-null type com.truecaller.credit.app.ui.onboarding.views.fragments.CreditIneligibleFragment.OnFragmentInteractionListener");
        throw paramBundle;
      }
    }
    paramBundle = new java/lang/RuntimeException;
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("parent activity should implement ");
    String str = a.b.class.getSimpleName();
    ((StringBuilder)localObject).append(str);
    localObject = ((StringBuilder)localObject).toString();
    paramBundle.<init>((String)localObject);
    throw ((Throwable)paramBundle);
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    String str = "view";
    k.b(paramView, str);
    super.onViewCreated(paramView, paramBundle);
    int i = R.id.btnBackToHome;
    paramView = (Button)a(i);
    paramBundle = new com/truecaller/credit/app/ui/onboarding/views/b/a$c;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    i = R.id.btnClose;
    paramView = (ImageView)a(i);
    paramBundle = new com/truecaller/credit/app/ui/onboarding/views/b/a$d;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    paramView = getArguments();
    if (paramView != null)
    {
      paramBundle = "api_status_message";
      paramView = paramView.getString(paramBundle);
      if (paramView != null)
      {
        int j = R.id.tvVerificationErrorMessage;
        paramBundle = (TextView)a(j);
        k.a(paramBundle, "tvVerificationErrorMessage");
        paramView = (CharSequence)paramView;
        paramBundle.setText(paramView);
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.onboarding.views.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */