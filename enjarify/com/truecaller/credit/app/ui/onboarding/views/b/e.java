package com.truecaller.credit.app.ui.onboarding.views.b;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface.OnShowListener;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;
import c.g.b.k;
import com.truecaller.credit.R.id;
import com.truecaller.credit.R.layout;
import com.truecaller.credit.R.string;
import com.truecaller.credit.app.ui.onboarding.b.a.a.a;
import com.truecaller.credit.app.ui.onboarding.b.a.b;
import com.truecaller.credit.i;
import com.truecaller.credit.i.a;
import java.util.HashMap;

public final class e
  extends com.truecaller.credit.app.ui.b.a
  implements com.truecaller.credit.app.ui.onboarding.views.c.e.b
{
  public com.truecaller.credit.app.ui.onboarding.views.c.e.a b;
  e.a c;
  private HashMap d;
  
  public final View a(int paramInt)
  {
    Object localObject1 = d;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      d = ((HashMap)localObject1);
    }
    localObject1 = d;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = d;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a()
  {
    a.a locala = com.truecaller.credit.app.ui.onboarding.b.a.a.a();
    Object localObject = i.i;
    localObject = i.a.a();
    locala.a((com.truecaller.credit.app.c.a.a)localObject).a().a(this);
  }
  
  public final int b()
  {
    return R.layout.fragment_location_unsupported;
  }
  
  public final void c()
  {
    HashMap localHashMap = d;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final void d()
  {
    Object localObject = (Context)getActivity();
    int i = R.string.initial_offer_notify_successful;
    CharSequence localCharSequence = (CharSequence)getString(i);
    Toast.makeText((Context)localObject, localCharSequence, 0).show();
    localObject = c;
    if (localObject != null) {
      ((e.a)localObject).l();
    }
    dismiss();
  }
  
  public final void e()
  {
    Context localContext = (Context)getActivity();
    int i = R.string.initial_offer_notify_failure;
    CharSequence localCharSequence = (CharSequence)getString(i);
    Toast.makeText(localContext, localCharSequence, 0).show();
  }
  
  public final Dialog onCreateDialog(Bundle paramBundle)
  {
    paramBundle = super.onCreateDialog(paramBundle);
    DialogInterface.OnShowListener localOnShowListener = (DialogInterface.OnShowListener)e.b.a;
    paramBundle.setOnShowListener(localOnShowListener);
    return paramBundle;
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    String str = "view";
    k.b(paramView, str);
    super.onViewCreated(paramView, paramBundle);
    paramView = b;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramView.a(this);
    int i = R.id.btnNotify;
    paramView = (Button)a(i);
    paramBundle = new com/truecaller/credit/app/ui/onboarding/views/b/e$c;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.onboarding.views.b.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */