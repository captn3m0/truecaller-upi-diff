package com.truecaller.credit.app.ui.onboarding.views.b;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import c.g.b.k;
import c.n.m;
import c.u;
import com.truecaller.credit.R.drawable;
import com.truecaller.credit.R.id;
import com.truecaller.credit.R.layout;
import com.truecaller.credit.R.string;
import com.truecaller.credit.app.ui.b.c;
import com.truecaller.credit.app.ui.onboarding.b.a.b;
import com.truecaller.credit.app.ui.onboarding.views.c.d.c;
import com.truecaller.credit.i;
import com.truecaller.credit.i.a;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;

public final class d
  extends c
  implements d.c
{
  private d.a c;
  private HashMap d;
  
  public final View a(int paramInt)
  {
    Object localObject1 = d;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      d = ((HashMap)localObject1);
    }
    localObject1 = d;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = d;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "initialOffer");
    int i = R.id.tvOfferAmount;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "tvOfferAmount");
    int j = R.string.rs_offer_amount;
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = paramString;
    paramString = (CharSequence)getString(j, arrayOfObject);
    localTextView.setText(paramString);
  }
  
  public final void a(String paramString1, String paramString2, String paramString3)
  {
    k.b(paramString1, "text");
    k.b(paramString2, "linkText");
    k.b(paramString3, "link");
    int i = R.id.textTerms;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "textTerms");
    k.b(localTextView, "receiver$0");
    k.b(paramString1, "text");
    k.b(paramString2, "linkText");
    k.b(paramString3, "link");
    Object localObject = new android/text/SpannableStringBuilder;
    paramString1 = (CharSequence)paramString1;
    ((SpannableStringBuilder)localObject).<init>(paramString1);
    com.truecaller.credit.app.ui.assist.a.a locala = new com/truecaller/credit/app/ui/assist/a$a;
    locala.<init>(localTextView, paramString3);
    int j = 6;
    int k = m.a(paramString1, paramString2, 0, false, j);
    int m = m.a(paramString1, paramString2, 0, false, j);
    int n = paramString2.length();
    m += n;
    ((SpannableStringBuilder)localObject).setSpan(locala, k, m, 33);
    localObject = (CharSequence)localObject;
    localTextView.setText((CharSequence)localObject);
    paramString1 = LinkMovementMethod.getInstance();
    localTextView.setMovementMethod(paramString1);
  }
  
  public final void b()
  {
    com.truecaller.credit.app.ui.onboarding.b.a.a.a locala = com.truecaller.credit.app.ui.onboarding.b.a.a.a();
    Object localObject = i.i;
    localObject = i.a.a();
    locala.a((com.truecaller.credit.app.c.a.a)localObject).a().a(this);
  }
  
  public final int c()
  {
    return R.layout.fragment_initial_offer;
  }
  
  public final void f()
  {
    HashMap localHashMap = d;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final void g()
  {
    int i = R.id.pbInitialOffer;
    ProgressBar localProgressBar = (ProgressBar)a(i);
    k.a(localProgressBar, "pbInitialOffer");
    t.a((View)localProgressBar);
  }
  
  public final void h()
  {
    int i = R.id.pbInitialOffer;
    ProgressBar localProgressBar = (ProgressBar)a(i);
    k.a(localProgressBar, "pbInitialOffer");
    t.b((View)localProgressBar);
  }
  
  public final void i()
  {
    d.a locala = c;
    if (locala != null)
    {
      locala.e();
      return;
    }
  }
  
  public final void onAttach(Context paramContext)
  {
    Object localObject = "context";
    k.b(paramContext, (String)localObject);
    super.onAttach(paramContext);
    boolean bool = paramContext instanceof d.a;
    if (bool)
    {
      paramContext = (d.a)paramContext;
      c = paramContext;
      return;
    }
    localObject = new java/lang/RuntimeException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(paramContext);
    localStringBuilder.append(" must implement OnFragmentInteractionListener");
    paramContext = localStringBuilder.toString();
    ((RuntimeException)localObject).<init>(paramContext);
    throw ((Throwable)localObject);
  }
  
  public final void onDetach()
  {
    super.onDetach();
    c = null;
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    String str = "view";
    k.b(paramView, str);
    super.onViewCreated(paramView, paramBundle);
    int i = R.id.btnGetStarted;
    paramView = (Button)a(i);
    paramBundle = new com/truecaller/credit/app/ui/onboarding/views/b/d$b;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    paramView = getActivity();
    if (paramView != null)
    {
      paramView = (AppCompatActivity)paramView;
      int j = R.id.initialOfferToolbar;
      paramBundle = (Toolbar)a(j);
      paramView.setSupportActionBar(paramBundle);
      paramBundle = (CharSequence)"";
      paramView.setTitle(paramBundle);
      paramBundle = paramView.getSupportActionBar();
      boolean bool;
      if (paramBundle != null)
      {
        bool = false;
        str = null;
        paramBundle.setElevation(0.0F);
      }
      paramBundle = paramView.getSupportActionBar();
      if (paramBundle != null)
      {
        bool = true;
        paramBundle.setDisplayHomeAsUpEnabled(bool);
      }
      paramView = paramView.getSupportActionBar();
      if (paramView != null)
      {
        j = R.drawable.ic_credit_close_white;
        paramView.setHomeAsUpIndicator(j);
        return;
      }
      return;
    }
    paramView = new c/u;
    paramView.<init>("null cannot be cast to non-null type android.support.v7.app.AppCompatActivity");
    throw paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.onboarding.views.b.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */