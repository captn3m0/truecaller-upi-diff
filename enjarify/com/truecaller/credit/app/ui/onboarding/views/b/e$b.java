package com.truecaller.credit.app.ui.onboarding.views.b;

import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;
import android.support.design.R.id;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.a;
import android.view.View;
import android.widget.FrameLayout;
import c.g.b.k;
import c.u;
import com.truecaller.log.d;

final class e$b
  implements DialogInterface.OnShowListener
{
  public static final b a;
  
  static
  {
    b localb = new com/truecaller/credit/app/ui/onboarding/views/b/e$b;
    localb.<init>();
    a = localb;
  }
  
  public final void onShow(DialogInterface paramDialogInterface)
  {
    if (paramDialogInterface != null) {}
    try
    {
      paramDialogInterface = (a)paramDialogInterface;
      int i = R.id.design_bottom_sheet;
      paramDialogInterface = paramDialogInterface.findViewById(i);
      paramDialogInterface = (FrameLayout)paramDialogInterface;
      paramDialogInterface = (View)paramDialogInterface;
      paramDialogInterface = BottomSheetBehavior.a(paramDialogInterface);
      str = "BottomSheetBehavior.from(bottomSheet)";
      k.a(paramDialogInterface, str);
      i = 3;
      paramDialogInterface.b(i);
      return;
    }
    catch (Exception localException)
    {
      String str;
      for (;;) {}
    }
    paramDialogInterface = new c/u;
    str = "null cannot be cast to non-null type android.support.design.widget.BottomSheetDialog";
    paramDialogInterface.<init>(str);
    throw paramDialogInterface;
    paramDialogInterface = new java/lang/AssertionError;
    paramDialogInterface.<init>("Bottom sheet unavailable");
    d.a((Throwable)paramDialogInterface);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.onboarding.views.b.e.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */