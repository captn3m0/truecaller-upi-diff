package com.truecaller.credit.app.ui.onboarding.views.b;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.d;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import c.g.b.k;
import c.u;
import com.truecaller.common.ui.PausingLottieAnimationView;
import com.truecaller.credit.R.id;
import com.truecaller.credit.R.layout;
import com.truecaller.credit.app.ui.b.c;
import com.truecaller.credit.app.ui.onboarding.b.a.a.a;
import com.truecaller.credit.app.ui.onboarding.b.a.b;
import com.truecaller.credit.app.ui.onboarding.services.ScoreDataUploadService;
import com.truecaller.credit.app.ui.onboarding.services.ScoreDataUploadService.a;
import com.truecaller.credit.i;
import com.truecaller.credit.i.a;
import java.util.HashMap;

public final class f
  extends c
  implements com.truecaller.credit.app.ui.onboarding.views.c.f.b
{
  public static final f.a c;
  private f.b d;
  private final f.d e;
  private HashMap f;
  
  static
  {
    f.a locala = new com/truecaller/credit/app/ui/onboarding/views/b/f$a;
    locala.<init>((byte)0);
    c = locala;
  }
  
  public f()
  {
    f.d locald = new com/truecaller/credit/app/ui/onboarding/views/b/f$d;
    locald.<init>(this);
    e = locald;
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = f;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      f = ((HashMap)localObject1);
    }
    localObject1 = f;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = f;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "assetName");
    int i = R.id.lottieOfferCalculation;
    ((PausingLottieAnimationView)a(i)).setAnimation(paramString);
    int j = R.id.lottieOfferCalculation;
    ((PausingLottieAnimationView)a(j)).a(true);
    j = R.id.lottieOfferCalculation;
    ((PausingLottieAnimationView)a(j)).a();
  }
  
  public final void b()
  {
    a.a locala = com.truecaller.credit.app.ui.onboarding.b.a.a.a();
    Object localObject = i.i;
    localObject = i.a.a();
    locala.a((com.truecaller.credit.app.c.a.a)localObject).a().a(this);
  }
  
  public final void b(String paramString)
  {
    boolean bool = isVisible();
    if (bool)
    {
      f.b localb = d;
      if (localb != null)
      {
        localb.b(paramString);
        return;
      }
    }
  }
  
  public final int c()
  {
    return R.layout.fragment_offer_calculation;
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "string");
    int i = R.id.tvOfferCalculationHeader;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "tvOfferCalculationHeader");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void f()
  {
    HashMap localHashMap = f;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final void g()
  {
    Object localObject = getContext();
    if (localObject != null)
    {
      localObject = d.a((Context)localObject);
      BroadcastReceiver localBroadcastReceiver = (BroadcastReceiver)e;
      IntentFilter localIntentFilter = new android/content/IntentFilter;
      localIntentFilter.<init>("credit_line_offer");
      ((d)localObject).a(localBroadcastReceiver, localIntentFilter);
      return;
    }
  }
  
  public final void h()
  {
    Context localContext = getContext();
    if (localContext != null)
    {
      Object localObject = ScoreDataUploadService.b;
      k.a(localContext, "it");
      localObject = ScoreDataUploadService.a.a(localContext);
      localContext.startService((Intent)localObject);
      return;
    }
  }
  
  public final void i()
  {
    Context localContext = getContext();
    if (localContext != null)
    {
      Object localObject = ScoreDataUploadService.b;
      k.a(localContext, "it");
      localObject = ScoreDataUploadService.a.a(localContext);
      localContext.stopService((Intent)localObject);
      return;
    }
  }
  
  public final void j()
  {
    boolean bool = isVisible();
    if (bool)
    {
      f.b localb = d;
      if (localb != null)
      {
        localb.g();
        return;
      }
    }
  }
  
  public final void onAttach(Context paramContext)
  {
    Object localObject = "context";
    k.b(paramContext, (String)localObject);
    super.onAttach(paramContext);
    boolean bool = paramContext instanceof f.b;
    if (bool)
    {
      paramContext = (f.b)paramContext;
      d = paramContext;
      return;
    }
    localObject = new java/lang/RuntimeException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(paramContext);
    localStringBuilder.append(" must implement OnFragmentInteractionListener");
    paramContext = localStringBuilder.toString();
    ((RuntimeException)localObject).<init>(paramContext);
    throw ((Throwable)localObject);
  }
  
  public final void onDetach()
  {
    super.onDetach();
    d = null;
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    String str = "view";
    k.b(paramView, str);
    super.onViewCreated(paramView, paramBundle);
    ((com.truecaller.credit.app.ui.onboarding.views.c.f.a)a()).a();
    int i = R.id.btnClose;
    paramView = (ImageView)a(i);
    paramBundle = new com/truecaller/credit/app/ui/onboarding/views/b/f$c;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    paramView = getActivity();
    if (paramView != null)
    {
      paramView = (AppCompatActivity)paramView;
      int j = R.id.calculateOfferToolbar;
      paramBundle = (Toolbar)a(j);
      paramView.setSupportActionBar(paramBundle);
      paramView = paramView.getSupportActionBar();
      if (paramView != null)
      {
        paramView.setElevation(0.0F);
        return;
      }
      return;
    }
    paramView = new c/u;
    paramView.<init>("null cannot be cast to non-null type android.support.v7.app.AppCompatActivity");
    throw paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.onboarding.views.b.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */