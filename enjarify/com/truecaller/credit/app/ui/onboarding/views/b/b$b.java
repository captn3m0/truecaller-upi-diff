package com.truecaller.credit.app.ui.onboarding.views.b;

import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;
import android.support.design.R.id;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.a;
import android.view.View;
import android.widget.FrameLayout;
import c.g.b.k;
import c.u;
import com.truecaller.log.d;

final class b$b
  implements DialogInterface.OnShowListener
{
  public static final b a;
  
  static
  {
    b localb = new com/truecaller/credit/app/ui/onboarding/views/b/b$b;
    localb.<init>();
    a = localb;
  }
  
  public final void onShow(DialogInterface paramDialogInterface)
  {
    if (paramDialogInterface != null) {}
    try
    {
      paramDialogInterface = (a)paramDialogInterface;
      int i = R.id.design_bottom_sheet;
      paramDialogInterface = paramDialogInterface.findViewById(i);
      paramDialogInterface = (FrameLayout)paramDialogInterface;
      localObject = paramDialogInterface;
      localObject = (View)paramDialogInterface;
      localObject = BottomSheetBehavior.a((View)localObject);
      String str = "it";
      k.a(localObject, str);
      str = null;
      if (paramDialogInterface != null)
      {
        j = paramDialogInterface.getHeight();
      }
      else
      {
        j = 0;
        paramDialogInterface = null;
      }
      ((BottomSheetBehavior)localObject).a(j);
      int j = 3;
      ((BottomSheetBehavior)localObject).b(j);
      ((BottomSheetBehavior)localObject).a(false);
      return;
    }
    catch (Exception localException)
    {
      Object localObject;
      for (;;) {}
    }
    paramDialogInterface = new c/u;
    localObject = "null cannot be cast to non-null type android.support.design.widget.BottomSheetDialog";
    paramDialogInterface.<init>((String)localObject);
    throw paramDialogInterface;
    paramDialogInterface = new java/lang/AssertionError;
    paramDialogInterface.<init>("Bottom sheet unavailable");
    d.a((Throwable)paramDialogInterface);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.onboarding.views.b.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */