package com.truecaller.credit.app.ui.onboarding.views.b;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface.OnShowListener;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import c.g.b.k;
import com.truecaller.credit.R.layout;
import com.truecaller.credit.app.ui.onboarding.views.c.a.b;
import com.truecaller.credit.i;
import com.truecaller.credit.i.a;
import com.truecaller.log.d;
import com.truecaller.utils.extensions.p;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;

public final class b
  extends com.truecaller.credit.app.ui.b.a
  implements View.OnClickListener, a.b
{
  public com.truecaller.credit.app.ui.onboarding.views.c.a.a b;
  b.a c;
  private final BroadcastReceiver d;
  private final c.g.a.b e;
  private HashMap f;
  
  public b()
  {
    Object localObject = new com/truecaller/credit/app/ui/onboarding/views/b/b$d;
    ((b.d)localObject).<init>(this);
    localObject = (BroadcastReceiver)localObject;
    d = ((BroadcastReceiver)localObject);
    localObject = new com/truecaller/credit/app/ui/onboarding/views/b/b$c;
    ((b.c)localObject).<init>(this);
    localObject = (c.g.a.b)localObject;
    e = ((c.g.a.b)localObject);
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = f;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      f = ((HashMap)localObject1);
    }
    localObject1 = f;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = f;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a()
  {
    com.truecaller.credit.app.ui.onboarding.b.a.a.a locala = com.truecaller.credit.app.ui.onboarding.b.a.a.a();
    Object localObject = i.i;
    localObject = i.a.a();
    locala.a((com.truecaller.credit.app.c.a.a)localObject).a().a(this);
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "otp");
    int i = com.truecaller.credit.R.id.etOtpFinalOffer;
    EditText localEditText = (EditText)a(i);
    paramString = (CharSequence)paramString;
    localEditText.setText(paramString);
  }
  
  public final int b()
  {
    return R.layout.fragment_final_offer_otp;
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "analyticsContext");
    b.a locala = c;
    if (locala != null) {
      locala.a(paramString);
    }
    dismiss();
  }
  
  public final void c()
  {
    HashMap localHashMap = f;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "text");
    int i = com.truecaller.credit.R.id.tvVerifySubtitle;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "tvVerifySubtitle");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final com.truecaller.credit.app.ui.onboarding.views.c.a.a d()
  {
    com.truecaller.credit.app.ui.onboarding.views.c.a.a locala = b;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return locala;
  }
  
  public final void d(String paramString)
  {
    k.b(paramString, "message");
    Context localContext = (Context)getActivity();
    paramString = (CharSequence)paramString;
    Toast.makeText(localContext, paramString, 0).show();
  }
  
  public final void e()
  {
    Context localContext = getContext();
    if (localContext != null)
    {
      BroadcastReceiver localBroadcastReceiver = d;
      IntentFilter localIntentFilter = new android/content/IntentFilter;
      localIntentFilter.<init>("android.provider.Telephony.SMS_RECEIVED");
      localContext.registerReceiver(localBroadcastReceiver, localIntentFilter);
      return;
    }
  }
  
  public final void f()
  {
    int i = com.truecaller.credit.R.id.btnContinue;
    Object localObject1 = (Button)a(i);
    Object localObject2 = this;
    localObject2 = (View.OnClickListener)this;
    ((Button)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    i = com.truecaller.credit.R.id.tvResendOtp;
    ((TextView)a(i)).setOnClickListener((View.OnClickListener)localObject2);
    i = com.truecaller.credit.R.id.etOtpFinalOffer;
    localObject1 = (EditText)a(i);
    k.a(localObject1, "etOtpFinalOffer");
    localObject1 = (TextView)localObject1;
    localObject2 = e;
    p.a((TextView)localObject1, (c.g.a.b)localObject2);
  }
  
  public final void g()
  {
    int i = com.truecaller.credit.R.id.tvOtpNotReceived;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "tvOtpNotReceived");
    t.a((View)localTextView);
    i = com.truecaller.credit.R.id.tvResendOtp;
    localTextView = (TextView)a(i);
    k.a(localTextView, "tvResendOtp");
    t.a((View)localTextView);
  }
  
  public final void h()
  {
    int i = com.truecaller.credit.R.id.tvOtpMismatch;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "tvOtpMismatch");
    t.c((View)localTextView);
  }
  
  public final void i()
  {
    int i = com.truecaller.credit.R.id.btnContinue;
    Button localButton = (Button)a(i);
    k.a(localButton, "btnContinue");
    localButton.setEnabled(true);
  }
  
  public final void j()
  {
    int i = com.truecaller.credit.R.id.btnContinue;
    Button localButton = (Button)a(i);
    k.a(localButton, "btnContinue");
    localButton.setEnabled(false);
  }
  
  public final String k()
  {
    int i = com.truecaller.credit.R.id.etOtpFinalOffer;
    EditText localEditText = (EditText)a(i);
    k.a(localEditText, "etOtpFinalOffer");
    return localEditText.getText().toString();
  }
  
  public final void l()
  {
    int i = com.truecaller.credit.R.id.tvOtpMismatch;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "tvOtpMismatch");
    t.a((View)localTextView);
  }
  
  public final void m()
  {
    int i = com.truecaller.credit.R.id.tvVerifySubtitle;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "tvVerifySubtitle");
    t.a((View)localTextView);
  }
  
  public final void onActivityCreated(Bundle paramBundle)
  {
    super.onActivityCreated(paramBundle);
    try
    {
      paramBundle = getDialog();
      String str = "dialog";
      k.a(paramBundle, str);
      paramBundle = paramBundle.getWindow();
      str = "dialog.window";
      k.a(paramBundle, str);
      paramBundle = paramBundle.getDecorView();
      int i = android.support.design.R.id.touch_outside;
      paramBundle = paramBundle.findViewById(i);
      i = 0;
      str = null;
      paramBundle.setOnClickListener(null);
      return;
    }
    catch (Exception localException)
    {
      paramBundle = new java/lang/AssertionError;
      paramBundle.<init>("Bottom sheet is closable");
      d.a((Throwable)paramBundle);
    }
  }
  
  public final void onClick(View paramView)
  {
    int i = com.truecaller.credit.R.id.btnContinue;
    Object localObject = (Button)a(i);
    boolean bool1 = k.a(paramView, localObject);
    if (bool1)
    {
      paramView = b;
      if (paramView == null)
      {
        localObject = "presenter";
        k.a((String)localObject);
      }
      paramView.a();
      return;
    }
    int j = com.truecaller.credit.R.id.tvResendOtp;
    localObject = (TextView)a(j);
    boolean bool2 = k.a(paramView, localObject);
    if (bool2)
    {
      paramView = b;
      if (paramView == null)
      {
        localObject = "presenter";
        k.a((String)localObject);
      }
      paramView.e();
    }
  }
  
  public final Dialog onCreateDialog(Bundle paramBundle)
  {
    paramBundle = super.onCreateDialog(paramBundle);
    DialogInterface.OnShowListener localOnShowListener = (DialogInterface.OnShowListener)b.b.a;
    paramBundle.setOnShowListener(localOnShowListener);
    return paramBundle;
  }
  
  public final void onDestroyView()
  {
    super.onDestroyView();
    Object localObject1 = getContext();
    Object localObject2;
    if (localObject1 != null)
    {
      localObject2 = d;
      ((Context)localObject1).unregisterReceiver((BroadcastReceiver)localObject2);
    }
    localObject1 = b;
    if (localObject1 == null)
    {
      localObject2 = "presenter";
      k.a((String)localObject2);
    }
    ((com.truecaller.credit.app.ui.onboarding.views.c.a.a)localObject1).y_();
    c();
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    String str = "view";
    k.b(paramView, str);
    super.onViewCreated(paramView, paramBundle);
    paramView = b;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramView.a(this);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.onboarding.views.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */