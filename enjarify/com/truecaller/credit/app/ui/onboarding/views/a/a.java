package com.truecaller.credit.app.ui.onboarding.views.a;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.content.b;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.credit.R.color;
import com.truecaller.credit.R.dimen;
import java.util.List;

public final class a
  extends ArrayAdapter
{
  public a(Context paramContext, List paramList)
  {
    super(paramContext, 17367048, paramList);
  }
  
  public final View getDropDownView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    paramView = super.getDropDownView(paramInt, paramView, paramViewGroup);
    paramViewGroup = (TextView)paramView.findViewById(16908308);
    k.a(paramViewGroup, "item");
    Object localObject = (CharSequence)getItem(paramInt);
    paramViewGroup.setText((CharSequence)localObject);
    localObject = getContext();
    int i = R.color.navy;
    paramInt = b.c((Context)localObject, i);
    paramViewGroup.setTextColor(paramInt);
    k.a(paramView, "dropDownView");
    return paramView;
  }
  
  public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    paramView = super.getView(paramInt, paramView, paramViewGroup);
    paramViewGroup = (TextView)paramView.findViewById(16908308);
    Object localObject = (CharSequence)getItem(paramInt);
    paramViewGroup.setText((CharSequence)localObject);
    localObject = paramViewGroup.getContext();
    int i = R.color.navy;
    paramInt = b.c((Context)localObject, i);
    paramViewGroup.setTextColor(paramInt);
    localObject = paramViewGroup.getContext();
    k.a(localObject, "context");
    localObject = ((Context)localObject).getResources();
    i = R.dimen.initial_offer_city_title_size;
    float f = ((Resources)localObject).getDimension(i);
    paramViewGroup.setTextSize(0, f);
    k.a(paramView, "view");
    paramInt = paramView.getPaddingTop();
    int j = paramView.getPaddingRight();
    int k = paramView.getPaddingBottom();
    paramView.setPaddingRelative(0, paramInt, j, k);
    return paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.onboarding.views.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */