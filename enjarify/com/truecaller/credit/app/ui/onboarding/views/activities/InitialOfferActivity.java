package com.truecaller.credit.app.ui.onboarding.views.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v4.app.o;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import c.g.b.k;
import com.truecaller.credit.R.id;
import com.truecaller.credit.R.layout;
import com.truecaller.credit.R.menu;
import com.truecaller.credit.app.ui.faq.activities.CreditWebViewActivity;
import com.truecaller.credit.app.ui.infocollection.views.activities.InfoCollectionActivity;
import com.truecaller.credit.app.ui.onboarding.b.a.a.a;
import com.truecaller.credit.app.ui.onboarding.views.b.a.b;
import com.truecaller.credit.app.ui.onboarding.views.b.c;
import com.truecaller.credit.app.ui.onboarding.views.b.c.b;
import com.truecaller.credit.app.ui.onboarding.views.b.d;
import com.truecaller.credit.app.ui.onboarding.views.b.d.a;
import com.truecaller.credit.app.ui.onboarding.views.b.f.b;
import com.truecaller.credit.app.ui.onboarding.views.b.g;
import com.truecaller.credit.app.ui.onboarding.views.b.g.a;
import com.truecaller.credit.app.ui.onboarding.views.b.g.b;
import com.truecaller.credit.app.ui.onboarding.views.c.b.a;
import com.truecaller.credit.app.ui.onboarding.views.c.b.b;
import com.truecaller.credit.i;
import com.truecaller.credit.i.a;
import java.util.HashMap;

public final class InitialOfferActivity
  extends com.truecaller.credit.app.ui.b.b
  implements a.b, c.b, d.a, f.b, g.b, b.b
{
  private Fragment b;
  private HashMap c;
  
  private final void i()
  {
    Object localObject = new com/truecaller/credit/app/ui/onboarding/views/b/d;
    ((d)localObject).<init>();
    localObject = (Fragment)localObject;
    b = ((Fragment)localObject);
    localObject = getSupportFragmentManager().a();
    int i = R.id.container;
    Fragment localFragment = b;
    if (localFragment == null)
    {
      str = "currentFragment";
      k.a(str);
    }
    String str = d.class.getSimpleName();
    ((o)localObject).b(i, localFragment, str).c();
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = c;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      c = ((HashMap)localObject1);
    }
    localObject1 = c;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = findViewById(paramInt);
      localObject2 = c;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "url");
    Intent localIntent = new android/content/Intent;
    Object localObject = this;
    localObject = (Context)this;
    localIntent.<init>((Context)localObject, CreditWebViewActivity.class);
    localIntent.putExtra("url", paramString);
    startActivity(localIntent);
  }
  
  public final int b()
  {
    return R.layout.activity_credit_initial_offer;
  }
  
  public final void b(String paramString)
  {
    Object localObject1 = com.truecaller.credit.app.ui.onboarding.views.b.a.c;
    localObject1 = new com/truecaller/credit/app/ui/onboarding/views/b/a;
    ((com.truecaller.credit.app.ui.onboarding.views.b.a)localObject1).<init>();
    Object localObject2 = new android/os/Bundle;
    ((Bundle)localObject2).<init>();
    String str = "api_status_message";
    ((Bundle)localObject2).putString(str, paramString);
    ((com.truecaller.credit.app.ui.onboarding.views.b.a)localObject1).setArguments((Bundle)localObject2);
    localObject1 = (Fragment)localObject1;
    b = ((Fragment)localObject1);
    paramString = getSupportFragmentManager().a();
    int i = R.id.container;
    localObject2 = b;
    if (localObject2 == null)
    {
      str = "currentFragment";
      k.a(str);
    }
    str = com.truecaller.credit.app.ui.onboarding.views.b.a.class.getSimpleName();
    paramString.b(i, (Fragment)localObject2, str).c();
  }
  
  public final void c()
  {
    a.a locala = com.truecaller.credit.app.ui.onboarding.b.a.a.a();
    Object localObject = i.i;
    localObject = i.a.a();
    locala.a((com.truecaller.credit.app.c.a.a)localObject).a().a(this);
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "analyticsContext");
    Intent localIntent = new android/content/Intent;
    Object localObject = this;
    localObject = (Context)this;
    localIntent.<init>((Context)localObject, InfoCollectionActivity.class);
    localIntent.putExtra("analytics_context", paramString);
    startActivity(localIntent);
    finish();
  }
  
  public final void d()
  {
    Object localObject = getIntent();
    String str = "is_deep_link_flag";
    int i = 0;
    boolean bool = ((Intent)localObject).getBooleanExtra(str, false);
    if (bool)
    {
      localObject = getIntent();
      str = "intent";
      k.a(localObject, str);
      localObject = ((Intent)localObject).getData();
      if (localObject == null) {
        return;
      }
      localObject = ((Uri)localObject).getLastPathSegment();
      if (localObject == null) {
        return;
      }
      int j = ((String)localObject).hashCode();
      i = -1870541229;
      if (j != i)
      {
        i = 185880993;
        if (j == i)
        {
          str = "initial_offer";
          bool = ((String)localObject).equals(str);
          if (!bool) {
            return;
          }
          break label121;
        }
      }
      else
      {
        str = "final_offer";
        bool = ((String)localObject).equals(str);
        if (!bool) {
          return;
        }
        g();
      }
      return;
    }
    label121:
    i();
  }
  
  public final void e()
  {
    Object localObject = c.c;
    localObject = new com/truecaller/credit/app/ui/onboarding/views/b/c;
    ((c)localObject).<init>();
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    ((c)localObject).setArguments(localBundle);
    localObject = (Fragment)localObject;
    b = ((Fragment)localObject);
    localObject = getSupportFragmentManager().a();
    int i = R.id.container;
    Fragment localFragment = b;
    if (localFragment == null)
    {
      str = "currentFragment";
      k.a(str);
    }
    String str = c.class.getSimpleName();
    ((o)localObject).a(i, localFragment, str).c();
  }
  
  public final void f()
  {
    Object localObject = com.truecaller.credit.app.ui.onboarding.views.b.f.c;
    localObject = new com/truecaller/credit/app/ui/onboarding/views/b/f;
    ((com.truecaller.credit.app.ui.onboarding.views.b.f)localObject).<init>();
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    ((com.truecaller.credit.app.ui.onboarding.views.b.f)localObject).setArguments(localBundle);
    localObject = (Fragment)localObject;
    b = ((Fragment)localObject);
    localObject = getSupportFragmentManager().a();
    int i = R.id.container;
    Fragment localFragment = b;
    if (localFragment == null)
    {
      str = "currentFragment";
      k.a(str);
    }
    String str = com.truecaller.credit.app.ui.onboarding.views.b.f.class.getSimpleName();
    ((o)localObject).b(i, localFragment, str).c();
  }
  
  public final void g()
  {
    Object localObject = g.c;
    localObject = (Fragment)g.a.a();
    b = ((Fragment)localObject);
    localObject = getSupportFragmentManager().a();
    int i = R.id.container;
    Fragment localFragment = b;
    if (localFragment == null)
    {
      str = "currentFragment";
      k.a(str);
    }
    String str = g.class.getSimpleName();
    ((o)localObject).b(i, localFragment, str).c();
  }
  
  public final void h()
  {
    finish();
  }
  
  public final void onBackPressed()
  {
    Fragment localFragment = b;
    if (localFragment == null)
    {
      String str = "currentFragment";
      k.a(str);
    }
    boolean bool = localFragment instanceof c;
    if (bool)
    {
      i();
      return;
    }
    finish();
  }
  
  public final boolean onCreateOptionsMenu(Menu paramMenu)
  {
    super.onCreateOptionsMenu(paramMenu);
    MenuInflater localMenuInflater = getMenuInflater();
    int i = R.menu.menu_initial_offer;
    localMenuInflater.inflate(i, paramMenu);
    return true;
  }
  
  public final boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    super.onOptionsItemSelected(paramMenuItem);
    int i;
    if (paramMenuItem != null)
    {
      i = paramMenuItem.getItemId();
      paramMenuItem = Integer.valueOf(i);
    }
    else
    {
      i = 0;
      paramMenuItem = null;
    }
    if (paramMenuItem != null)
    {
      j = paramMenuItem.intValue();
      int k = 16908332;
      if (j == k)
      {
        onBackPressed();
        break label94;
      }
    }
    int j = R.id.info;
    if (paramMenuItem != null)
    {
      i = paramMenuItem.intValue();
      if (i == j)
      {
        paramMenuItem = (b.a)a();
        paramMenuItem.a();
      }
    }
    label94:
    return true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.onboarding.views.activities.InitialOfferActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */