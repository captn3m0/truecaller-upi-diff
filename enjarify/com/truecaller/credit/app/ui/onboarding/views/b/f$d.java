package com.truecaller.credit.app.ui.onboarding.views.b;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import c.g.b.k;
import com.truecaller.credit.app.ui.onboarding.views.c.f.a;

public final class f$d
  extends BroadcastReceiver
{
  f$d(f paramf) {}
  
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    String str1 = "context";
    k.b(paramContext, str1);
    k.b(paramIntent, "intent");
    paramContext = paramIntent.getExtras();
    if (paramContext != null)
    {
      paramIntent = (f.a)a.a();
      str1 = paramContext.getString("extra_state", "state_start");
      k.a(str1, "it.getString(ScoreDataUp…ploadService.STATE_START)");
      String str2 = paramContext.getString("extra_message");
      paramContext = paramContext.getString("source");
      paramIntent.a(str1, str2, paramContext);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.onboarding.views.b.f.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */