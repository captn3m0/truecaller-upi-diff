package com.truecaller.credit.app.ui.onboarding.views.c;

import com.truecaller.credit.data.models.APIStatusMessage;
import com.truecaller.credit.domain.interactors.onboarding.models.b;
import java.util.List;

public abstract interface c$b
{
  public abstract void a(int paramInt, String[] paramArrayOfString);
  
  public abstract void a(APIStatusMessage paramAPIStatusMessage);
  
  public abstract void a(b paramb);
  
  public abstract void a(String paramString);
  
  public abstract void a(List paramList);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract void b(String paramString);
  
  public abstract void c(String paramString);
  
  public abstract void d(String paramString);
  
  public abstract void g();
  
  public abstract void h();
  
  public abstract void i();
  
  public abstract void j();
  
  public abstract void k();
  
  public abstract void m();
  
  public abstract String n();
  
  public abstract void o();
  
  public abstract void p();
  
  public abstract void q();
  
  public abstract void r();
  
  public abstract void s();
  
  public abstract void t();
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.onboarding.views.c.c.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */