package com.truecaller.credit.app.ui.onboarding.c;

import c.d.a.a;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.credit.data.Failure;
import com.truecaller.credit.data.Result;
import com.truecaller.credit.data.Success;
import com.truecaller.credit.domain.interactors.onboarding.models.CreditLineStatus;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ar;
import kotlinx.coroutines.g;

final class p$a
  extends c.d.b.a.k
  implements m
{
  Object a;
  int b;
  private ag d;
  
  p$a(p paramp, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/credit/app/ui/onboarding/c/p$a;
    p localp = c;
    locala.<init>(localp, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = b;
    boolean bool2;
    Object localObject2;
    int k;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 2: 
      bool2 = paramObject instanceof o.b;
      if (!bool2) {
        break label261;
      }
      throw a;
    case 1: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        throw a;
      }
      break;
    case 0: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label326;
      }
      paramObject = p.a(c);
      localObject2 = new com/truecaller/credit/app/ui/onboarding/c/p$a$a;
      ((p.a.a)localObject2).<init>(this, null);
      localObject2 = (m)localObject2;
      k = 1;
      b = k;
      paramObject = g.a((f)paramObject, (m)localObject2, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (Result)paramObject;
    boolean bool1 = paramObject instanceof Success;
    if (bool1)
    {
      localObject2 = paramObject;
      localObject2 = ((CreditLineStatus)((Success)paramObject).getData()).getStatus();
      String str = "app_in_process";
      bool1 = c.g.b.k.a(localObject2, str);
      if (bool1)
      {
        paramObject = c;
        p.e((p)paramObject);
      }
      else
      {
        localObject2 = c;
        int j = p.f((p)localObject2);
        k = 2;
        if (j < k)
        {
          long l = 20000L;
          a = paramObject;
          b = k;
          paramObject = ar.a(l, this);
          if (paramObject == localObject1) {
            return localObject1;
          }
          label261:
          p.d(c);
          paramObject = c;
          p.g((p)paramObject);
        }
        else
        {
          paramObject = c;
          p.b((p)paramObject, null);
        }
      }
    }
    else
    {
      bool2 = paramObject instanceof Failure;
      if (bool2)
      {
        localObject1 = c;
        paramObject = ((Failure)paramObject).getMessage();
        p.b((p)localObject1, (String)paramObject);
      }
    }
    return x.a;
    label326:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.onboarding.c.p.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */