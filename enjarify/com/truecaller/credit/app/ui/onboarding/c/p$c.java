package com.truecaller.credit.app.ui.onboarding.c;

import c.d.a.a;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.credit.data.Failure;
import com.truecaller.credit.data.Result;
import com.truecaller.credit.data.Success;
import com.truecaller.credit.data.models.ScoreDataRules;
import com.truecaller.credit.data.models.ScoreDataRulesResponseKt;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ar;
import kotlinx.coroutines.g;

final class p$c
  extends c.d.b.a.k
  implements m
{
  Object a;
  Object b;
  int c;
  private ag f;
  
  p$c(p paramp, String paramString, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    c localc = new com/truecaller/credit/app/ui/onboarding/c/p$c;
    p localp = d;
    String str = e;
    localc.<init>(localp, str, paramc);
    paramObject = (ag)paramObject;
    f = ((ag)paramObject);
    return localc;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = c;
    p localp = null;
    int j;
    boolean bool1;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 3: 
      boolean bool2 = paramObject instanceof o.b;
      if (!bool2) {
        break label348;
      }
      throw a;
    case 2: 
      localObject2 = (Result)a;
      j = paramObject instanceof o.b;
      if (j == 0) {
        break label282;
      }
      throw a;
    case 1: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        throw a;
      }
      break;
    case 0: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label412;
      }
      paramObject = d;
      localObject2 = e;
      p.a((p)paramObject, (String)localObject2);
      paramObject = p.a(d);
      localObject2 = new com/truecaller/credit/app/ui/onboarding/c/p$c$a;
      ((p.c.a)localObject2).<init>(this, null);
      localObject2 = (m)localObject2;
      j = 1;
      c = j;
      paramObject = g.a((f)paramObject, (m)localObject2, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    Object localObject2 = paramObject;
    localObject2 = (Result)paramObject;
    boolean bool3 = localObject2 instanceof Success;
    if (bool3)
    {
      paramObject = d;
      Object localObject3 = localObject2;
      localObject3 = ScoreDataRulesResponseKt.toJson((ScoreDataRules)((Success)localObject2).getData());
      a = localObject2;
      int m = 2;
      c = m;
      paramObject = ((p)paramObject).a((String)localObject3, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      label282:
      paramObject = (Result)paramObject;
      j = paramObject instanceof Success;
      if (j != 0)
      {
        localp = d;
        p.c(localp);
        long l = 20000L;
        a = localObject2;
        b = paramObject;
        int k = 3;
        c = k;
        paramObject = ar.a(l, this);
        if (paramObject == localObject1) {
          return localObject1;
        }
        label348:
        paramObject = d;
        p.d((p)paramObject);
      }
      else
      {
        paramObject = d;
        p.b((p)paramObject, null);
      }
    }
    else
    {
      boolean bool4 = localObject2 instanceof Failure;
      if (bool4)
      {
        paramObject = d;
        localObject2 = (Failure)localObject2;
        localObject1 = ((Failure)localObject2).getMessage();
        p.b((p)paramObject, (String)localObject1);
      }
    }
    return x.a;
    label412:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (c)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((c)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.onboarding.c.p.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */