package com.truecaller.credit.app.ui.onboarding.c;

import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.credit.R.string;
import com.truecaller.credit.data.Result;
import com.truecaller.credit.data.Success;
import com.truecaller.credit.data.models.RequestFinalOfferOtpResult;
import com.truecaller.utils.n;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

final class a$b
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  a$b(a parama, boolean paramBoolean, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/credit/app/ui/onboarding/c/a$b;
    a locala = b;
    boolean bool = c;
    localb.<init>(locala, bool, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return localb;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = a;
    int j = 1;
    n localn;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label411;
      }
      paramObject = a.a(b);
      localObject2 = new com/truecaller/credit/app/ui/onboarding/c/a$b$a;
      localn = null;
      ((a.b.a)localObject2).<init>(this, null);
      localObject2 = (m)localObject2;
      a = j;
      paramObject = g.a((f)paramObject, (m)localObject2, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (Result)paramObject;
    boolean bool2 = paramObject instanceof Success;
    boolean bool1 = false;
    Object localObject2 = null;
    if (bool2)
    {
      bool2 = c;
      if (bool2)
      {
        paramObject = a.c(b);
        if (paramObject != null)
        {
          localObject1 = a.d(b);
          j = R.string.io_send_otp_success;
          localObject2 = new Object[0];
          localObject1 = ((n)localObject1).a(j, (Object[])localObject2);
          localObject2 = "resourceProvider.getStri…ring.io_send_otp_success)";
          c.g.b.k.a(localObject1, (String)localObject2);
          ((com.truecaller.credit.app.ui.onboarding.views.c.a.b)paramObject).d((String)localObject1);
        }
      }
      else
      {
        localObject1 = a.c(b);
        if (localObject1 != null)
        {
          ((com.truecaller.credit.app.ui.onboarding.views.c.a.b)localObject1).m();
          localn = a.d(b);
          int k = R.string.fo_verify_subtitle;
          Object[] arrayOfObject1 = new Object[j];
          paramObject = ((RequestFinalOfferOtpResult)((Success)paramObject).getData()).getMsisdn();
          if (paramObject == null)
          {
            paramObject = a.d(b);
            int m = R.string.fo_verify_fallback_number;
            Object[] arrayOfObject2 = new Object[0];
            paramObject = ((n)paramObject).a(m, arrayOfObject2);
          }
          arrayOfObject1[0] = paramObject;
          paramObject = localn.a(k, arrayOfObject1);
          localObject2 = "resourceProvider.getStri…                        )";
          c.g.b.k.a(paramObject, (String)localObject2);
          ((com.truecaller.credit.app.ui.onboarding.views.c.a.b)localObject1).c((String)paramObject);
        }
      }
    }
    else
    {
      paramObject = a.c(b);
      if (paramObject != null)
      {
        localObject1 = a.d(b);
        j = R.string.io_send_otp_failure;
        localObject2 = new Object[0];
        localObject1 = ((n)localObject1).a(j, (Object[])localObject2);
        localObject2 = "resourceProvider.getStri…ring.io_send_otp_failure)";
        c.g.b.k.a(localObject1, (String)localObject2);
        ((com.truecaller.credit.app.ui.onboarding.views.c.a.b)paramObject).d((String)localObject1);
      }
    }
    return x.a;
    label411:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.onboarding.c.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */