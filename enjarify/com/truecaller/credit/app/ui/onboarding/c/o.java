package com.truecaller.credit.app.ui.onboarding.c;

import c.g.b.k;
import com.truecaller.bb;
import com.truecaller.credit.R.string;
import com.truecaller.credit.app.ui.onboarding.views.c.h.a;
import com.truecaller.credit.app.ui.onboarding.views.c.h.b;
import com.truecaller.utils.n;

public final class o
  extends bb
  implements h.a
{
  private final n a;
  
  public o(n paramn)
  {
    a = paramn;
  }
  
  public final void a(h.b paramb)
  {
    k.b(paramb, "presenterView");
    super.a(paramb);
    paramb.a();
    Object localObject = a;
    int i = R.string.credit_offer_info_title;
    Object[] arrayOfObject1 = new Object[0];
    localObject = ((n)localObject).a(i, arrayOfObject1);
    k.a(localObject, "resourceProvider.getStri….credit_offer_info_title)");
    paramb.a((String)localObject);
    localObject = a;
    i = R.string.credit_offer_info_content;
    Object[] arrayOfObject2 = new Object[0];
    localObject = ((n)localObject).a(i, arrayOfObject2);
    k.a(localObject, "resourceProvider.getStri…redit_offer_info_content)");
    paramb.b((String)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.onboarding.c.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */