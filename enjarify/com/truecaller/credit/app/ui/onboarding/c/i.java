package com.truecaller.credit.app.ui.onboarding.c;

import c.d.f;
import c.g.a.m;
import c.g.b.k;
import com.truecaller.ba;
import com.truecaller.credit.app.ui.onboarding.views.c.e.a;
import com.truecaller.credit.data.repository.CreditRepository;
import kotlinx.coroutines.e;

public final class i
  extends ba
  implements e.a
{
  private final f c;
  private final f d;
  private final CreditRepository e;
  
  public i(f paramf1, f paramf2, CreditRepository paramCreditRepository)
  {
    super(paramf1);
    c = paramf1;
    d = paramf2;
    e = paramCreditRepository;
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "location");
    Object localObject = paramString;
    localObject = (CharSequence)paramString;
    int i = ((CharSequence)localObject).length();
    if (i > 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject = null;
    }
    if (i != 0)
    {
      localObject = new com/truecaller/credit/app/ui/onboarding/c/i$a;
      ((i.a)localObject).<init>(this, paramString, null);
      localObject = (m)localObject;
      int j = 3;
      e.b(this, null, (m)localObject, j);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.onboarding.c.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */