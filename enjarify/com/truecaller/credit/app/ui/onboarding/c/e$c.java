package com.truecaller.credit.app.ui.onboarding.c;

import c.d.a.a;
import c.d.c;
import c.d.f;
import c.o.b;
import c.x;
import com.truecaller.credit.R.string;
import com.truecaller.credit.app.ui.onboarding.views.c.c.b;
import com.truecaller.credit.data.Failure;
import com.truecaller.credit.data.Result;
import com.truecaller.credit.data.Success;
import com.truecaller.credit.domain.interactors.onboarding.models.b;
import com.truecaller.utils.n;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

final class e$c
  extends c.d.b.a.k
  implements c.g.a.m
{
  int a;
  private ag d;
  
  e$c(e parame, String paramString, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    c localc = new com/truecaller/credit/app/ui/onboarding/c/e$c;
    e locale = b;
    String str = c;
    localc.<init>(locale, str, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return localc;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    int j = 1;
    String str = null;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label581;
      }
      paramObject = e.d(b);
      if (paramObject != null) {
        ((c.b)paramObject).a(j);
      }
      paramObject = e.a(b);
      localObject2 = new com/truecaller/credit/app/ui/onboarding/c/e$c$a;
      ((e.c.a)localObject2).<init>(this, null);
      localObject2 = (c.g.a.m)localObject2;
      a = j;
      paramObject = g.a((f)paramObject, (c.g.a.m)localObject2, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (Result)paramObject;
    localObject1 = e.d(b);
    boolean bool1 = false;
    Object localObject2 = null;
    if (localObject1 != null) {
      ((c.b)localObject1).a(false);
    }
    boolean bool2 = paramObject instanceof Success;
    boolean bool3;
    int k;
    if (bool2)
    {
      localObject1 = e.d(b);
      Object localObject3;
      if (localObject1 != null)
      {
        localObject3 = paramObject;
        localObject3 = (b)((Success)paramObject).getData();
        ((c.b)localObject1).a((b)localObject3);
      }
      localObject1 = e.d(b);
      if (localObject1 != null)
      {
        localObject1 = ((c.b)localObject1).n();
      }
      else
      {
        bool2 = false;
        localObject1 = null;
      }
      localObject1 = (CharSequence)localObject1;
      int m;
      Object localObject4;
      if (localObject1 != null)
      {
        m = ((CharSequence)localObject1).length();
        if (m != 0)
        {
          j = 0;
          localObject4 = null;
        }
      }
      if (j == 0)
      {
        localObject1 = e.d(b);
        if (localObject1 != null)
        {
          localObject1 = ((c.b)localObject1).n();
        }
        else
        {
          m = 0;
          localObject1 = null;
        }
        localObject4 = e.c(b);
        int n = R.string.initial_offer_other_city;
        Object[] arrayOfObject = new Object[0];
        localObject4 = ((n)localObject4).a(n, arrayOfObject);
        bool3 = c.n.m.a((String)localObject1, (String)localObject4, false);
        if (!bool3)
        {
          localObject1 = e.d(b);
          if (localObject1 != null) {
            str = ((c.b)localObject1).n();
          }
          localObject1 = e.c(b);
          k = R.string.initial_offer_city_title;
          localObject3 = new Object[0];
          localObject1 = ((n)localObject1).a(k, (Object[])localObject3);
          bool3 = c.n.m.a(str, (String)localObject1, false);
          if (!bool3)
          {
            localObject1 = e.d(b);
            if (localObject1 != null) {
              ((c.b)localObject1).o();
            }
          }
        }
      }
      localObject1 = b;
      paramObject = (b)((Success)paramObject).getData();
      e.a((e)localObject1, (b)paramObject);
      paramObject = b;
      localObject1 = "success";
      e.a((e)paramObject, (String)localObject1);
    }
    else
    {
      bool3 = paramObject instanceof Failure;
      if (bool3)
      {
        localObject1 = e.d(b);
        if (localObject1 != null)
        {
          paramObject = ((Failure)paramObject).getMessage();
          if (paramObject == null)
          {
            paramObject = e.c(b);
            k = R.string.server_error_message;
            localObject2 = new Object[0];
            paramObject = ((n)paramObject).a(k, (Object[])localObject2);
            localObject2 = "resourceProvider.getStri…ing.server_error_message)";
            c.g.b.k.a(paramObject, (String)localObject2);
          }
          ((c.b)localObject1).b((String)paramObject);
        }
        paramObject = b;
        localObject1 = "failure";
        e.a((e)paramObject, (String)localObject1);
      }
    }
    return x.a;
    label581:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (c)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((c)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.onboarding.c.e.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */