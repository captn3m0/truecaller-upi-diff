package com.truecaller.credit.app.ui.onboarding.c;

import c.t;
import com.truecaller.bb;
import com.truecaller.credit.R.string;
import com.truecaller.credit.app.a.a.a;
import com.truecaller.credit.app.a.b;
import com.truecaller.credit.app.ui.onboarding.views.c.f.a;
import com.truecaller.credit.app.ui.onboarding.views.c.f.b;

public final class k
  extends bb
  implements f.a
{
  private final com.truecaller.utils.n a;
  private final b c;
  
  public k(com.truecaller.utils.n paramn, b paramb)
  {
    a = paramn;
    c = paramb;
  }
  
  private final void a(String paramString1, String paramString2)
  {
    Object localObject = "CreditFinalOffer";
    a.a locala = new com/truecaller/credit/app/a/a$a;
    locala.<init>((String)localObject, (String)localObject);
    localObject = new c.n[2];
    paramString1 = t.a("Status", paramString1);
    localObject[0] = paramString1;
    paramString1 = t.a("Context", paramString2);
    localObject[1] = paramString1;
    locala.a((c.n[])localObject);
    paramString1 = locala.a();
    a = false;
    paramString2 = c;
    paramString1 = paramString1.b();
    paramString2.a(paramString1);
  }
  
  public final void a()
  {
    f.b localb = (f.b)b;
    if (localb != null)
    {
      localb.g();
      localb.h();
      return;
    }
  }
  
  public final void a(String paramString1, String paramString2, String paramString3)
  {
    c.g.b.k.b(paramString1, "status");
    f.b localb = (f.b)b;
    if (localb == null) {
      return;
    }
    int i = paramString1.hashCode();
    int j = -1812877285;
    int k;
    boolean bool;
    if (i != j)
    {
      j = -745535373;
      if (i != j)
      {
        k = 836387774;
        if (i != k)
        {
          k = 1534491444;
          if (i == k)
          {
            paramString2 = "state_start";
            bool = paramString1.equals(paramString2);
            if (bool)
            {
              localb.a("animations/lottie_gathering_information.json");
              paramString1 = a;
              k = R.string.offer_calculation_title_sit_back;
              paramString3 = new Object[0];
              paramString1 = paramString1.a(k, paramString3);
              c.g.b.k.a(paramString1, "resourceProvider.getStri…lculation_title_sit_back)");
              localb.c(paramString1);
            }
          }
        }
        else
        {
          paramString2 = "state_offer_generated";
          bool = paramString1.equals(paramString2);
          if (bool)
          {
            paramString1 = "eligible";
            a(paramString1, paramString3);
            localb.i();
            localb.j();
          }
        }
      }
      else
      {
        String str = "state_offer_generation_failed";
        bool = paramString1.equals(str);
        if (bool)
        {
          a("ineligible", paramString3);
          localb.i();
          localb.b(paramString2);
        }
      }
    }
    else
    {
      paramString2 = "state_calculation";
      bool = paramString1.equals(paramString2);
      if (bool)
      {
        localb.a("animations/lottie_calculating_loan_offer.json");
        paramString1 = a;
        k = R.string.offer_calculation_title_offer_generation;
        paramString3 = new Object[0];
        paramString1 = paramString1.a(k, paramString3);
        c.g.b.k.a(paramString1, "resourceProvider.getStri…n_title_offer_generation)");
        localb.c(paramString1);
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.onboarding.c.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */