package com.truecaller.credit.app.ui.onboarding.c;

import c.d.a.a;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.credit.R.string;
import com.truecaller.credit.app.ui.onboarding.views.c.c.b;
import com.truecaller.utils.n;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

final class e$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  e$a(e parame, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/credit/app/ui/onboarding/c/e$a;
    e locale = b;
    locala.<init>(locale, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    int j = 1;
    int k;
    String str;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label395;
      }
      paramObject = e.a(b);
      localObject2 = new com/truecaller/credit/app/ui/onboarding/c/e$a$a;
      k = 0;
      str = null;
      ((e.a.a)localObject2).<init>(this, null);
      localObject2 = (m)localObject2;
      a = j;
      paramObject = g.a((f)paramObject, (m)localObject2, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (Collection)paramObject;
    boolean bool2 = ((Collection)paramObject).isEmpty() ^ j;
    boolean bool1 = false;
    Object localObject2 = null;
    if (bool2)
    {
      localObject1 = new java/util/ArrayList;
      ((ArrayList)localObject1).<init>();
      localObject1 = (List)localObject1;
      Object localObject3 = e.c(b);
      k = R.string.initial_offer_city_title;
      Object[] arrayOfObject = new Object[0];
      localObject3 = ((n)localObject3).a(k, arrayOfObject);
      str = "resourceProvider.getStri…initial_offer_city_title)";
      c.g.b.k.a(localObject3, str);
      ((List)localObject1).add(localObject3);
      ((List)localObject1).addAll((Collection)paramObject);
      paramObject = e.c(b);
      j = R.string.initial_offer_other_city;
      localObject2 = new Object[0];
      paramObject = ((n)paramObject).a(j, (Object[])localObject2);
      localObject2 = "resourceProvider.getStri…initial_offer_other_city)";
      c.g.b.k.a(paramObject, (String)localObject2);
      ((List)localObject1).add(paramObject);
      e.a(b, (List)localObject1);
      paramObject = e.d(b);
      if (paramObject != null)
      {
        ((c.b)paramObject).a((List)localObject1);
        ((c.b)paramObject).s();
        ((c.b)paramObject).r();
      }
    }
    else
    {
      paramObject = e.d(b);
      if (paramObject != null)
      {
        localObject1 = e.c(b);
        j = R.string.server_error_message;
        localObject2 = new Object[0];
        localObject1 = ((n)localObject1).a(j, (Object[])localObject2);
        localObject2 = "resourceProvider.getStri…ing.server_error_message)";
        c.g.b.k.a(localObject1, (String)localObject2);
        ((c.b)paramObject).a((String)localObject1);
        ((c.b)paramObject).r();
      }
    }
    return x.a;
    label395:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.onboarding.c.e.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */