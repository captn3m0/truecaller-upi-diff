package com.truecaller.credit.app.ui.onboarding.c;

import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.credit.app.ui.onboarding.views.c.d.c;
import com.truecaller.credit.data.Result;
import com.truecaller.credit.data.Success;
import com.truecaller.credit.data.models.InitialOfferResponse.InitialOfferData;
import kotlinx.coroutines.ag;

final class g$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  g$a(g paramg, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/credit/app/ui/onboarding/c/g$a;
    g localg = b;
    locala.<init>(localg, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = a;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label250;
      }
      paramObject = g.a(b);
      if (paramObject != null) {
        ((d.c)paramObject).g();
      }
      paramObject = g.b(b);
      Object localObject2 = new com/truecaller/credit/app/ui/onboarding/c/g$a$a;
      ((g.a.a)localObject2).<init>(this, null);
      localObject2 = (m)localObject2;
      int j = 1;
      a = j;
      paramObject = kotlinx.coroutines.g.a((f)paramObject, (m)localObject2, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (Result)paramObject;
    boolean bool2 = paramObject instanceof Success;
    if (bool2)
    {
      localObject1 = g.a(b);
      if (localObject1 != null)
      {
        ((d.c)localObject1).h();
        int k = ((InitialOfferResponse.InitialOfferData)((Success)paramObject).getData()).getInitial_offer();
        paramObject = com.truecaller.credit.app.ui.assist.a.a(k);
        ((d.c)localObject1).a((String)paramObject);
        paramObject = b;
        g.d((g)paramObject);
      }
    }
    else
    {
      paramObject = g.a(b);
      if (paramObject != null)
      {
        ((d.c)paramObject).h();
        localObject1 = "1,00,000";
        ((d.c)paramObject).a((String)localObject1);
      }
    }
    return x.a;
    label250:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.onboarding.c.g.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */