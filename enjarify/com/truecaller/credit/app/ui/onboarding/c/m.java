package com.truecaller.credit.app.ui.onboarding.c;

import c.d.f;
import c.n;
import c.t;
import com.truecaller.ba;
import com.truecaller.credit.app.a.a.a;
import com.truecaller.credit.app.a.b;
import com.truecaller.credit.app.ui.onboarding.views.c.g.a;
import com.truecaller.credit.app.ui.onboarding.views.c.g.b;
import com.truecaller.credit.data.repository.CreditRepository;
import kotlinx.coroutines.e;

public final class m
  extends ba
  implements g.a
{
  String c;
  private final f d;
  private final f e;
  private final CreditRepository f;
  private final b g;
  
  public m(f paramf1, f paramf2, CreditRepository paramCreditRepository, b paramb)
  {
    super(paramf1);
    d = paramf1;
    e = paramf2;
    f = paramCreditRepository;
    g = paramb;
    c = "";
  }
  
  private final void a(String paramString1, String paramString2)
  {
    Object localObject = "CreditFinalOfferDetails";
    a.a locala = new com/truecaller/credit/app/a/a$a;
    locala.<init>((String)localObject, (String)localObject);
    localObject = new n[3];
    paramString2 = t.a("Status", paramString2);
    localObject[0] = paramString2;
    paramString2 = t.a("Context", "initial_offer_details");
    localObject[1] = paramString2;
    paramString1 = t.a("Action", paramString1);
    localObject[2] = paramString1;
    locala.a((n[])localObject);
    paramString1 = locala.a();
    a = false;
    paramString2 = g;
    paramString1 = paramString1.b();
    paramString2.a(paramString1);
  }
  
  public final void a()
  {
    g.b localb = (g.b)b;
    if (localb != null)
    {
      localb.j();
      return;
    }
  }
  
  public final void e()
  {
    Object localObject = new com/truecaller/credit/app/ui/onboarding/c/m$a;
    ((m.a)localObject).<init>(this, null);
    localObject = (c.g.a.m)localObject;
    e.b(this, null, (c.g.a.m)localObject, 3);
  }
  
  public final void f()
  {
    Object localObject = (CharSequence)c;
    int i = ((CharSequence)localObject).length();
    if (i > 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject = null;
    }
    if (i != 0)
    {
      localObject = (g.b)b;
      if (localObject != null)
      {
        String str = c;
        ((g.b)localObject).c(str);
        return;
      }
    }
  }
  
  public final void g()
  {
    g.b localb = (g.b)b;
    if (localb != null)
    {
      localb.h();
      return;
    }
  }
  
  public final void h()
  {
    g.b localb = (g.b)b;
    if (localb != null) {
      localb.i();
    }
    a("im_interested", "clicked");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.onboarding.c.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */