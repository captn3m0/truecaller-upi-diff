package com.truecaller.credit.app.ui.onboarding.c;

import c.d.a.a;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.credit.app.ui.onboarding.views.c.e.b;
import com.truecaller.credit.data.Failure;
import com.truecaller.credit.data.Result;
import com.truecaller.credit.data.Success;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

final class i$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  i$a(i parami, String paramString, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/credit/app/ui/onboarding/c/i$a;
    i locali = b;
    String str = c;
    locala.<init>(locali, str, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    a locala = a.a;
    int i = a;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label196;
      }
      paramObject = i.a(b);
      Object localObject = new com/truecaller/credit/app/ui/onboarding/c/i$a$a;
      ((i.a.a)localObject).<init>(this, null);
      localObject = (m)localObject;
      int j = 1;
      a = j;
      paramObject = g.a((f)paramObject, (m)localObject, this);
      if (paramObject == locala) {
        return locala;
      }
      break;
    }
    paramObject = (Result)paramObject;
    boolean bool2 = paramObject instanceof Success;
    if (bool2)
    {
      paramObject = i.c(b);
      if (paramObject != null) {
        ((e.b)paramObject).d();
      }
    }
    else
    {
      boolean bool3 = paramObject instanceof Failure;
      if (bool3)
      {
        paramObject = i.c(b);
        if (paramObject != null) {
          ((e.b)paramObject).e();
        }
      }
    }
    return x.a;
    label196:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.onboarding.c.i.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */