package com.truecaller.credit.app.ui.onboarding.c;

import c.d.f;
import c.g.b.k;
import c.t;
import com.truecaller.ba;
import com.truecaller.credit.R.string;
import com.truecaller.credit.app.a.a;
import com.truecaller.credit.app.a.a.a;
import com.truecaller.credit.app.ui.onboarding.a.d;
import com.truecaller.credit.app.ui.onboarding.views.c.c.a;
import com.truecaller.credit.app.ui.onboarding.views.c.c.b;
import com.truecaller.credit.data.models.APIStatusMessage;
import com.truecaller.credit.data.repository.CreditRepository;
import com.truecaller.tcpermissions.o;
import com.truecaller.utils.l;
import java.util.Arrays;
import java.util.List;

public final class e
  extends ba
  implements c.a
{
  private List c;
  private com.truecaller.credit.domain.interactors.onboarding.models.b d;
  private final String[] e;
  private final int f;
  private String g;
  private boolean h;
  private final CreditRepository i;
  private final f j;
  private final f k;
  private final com.truecaller.utils.n l;
  private final com.truecaller.credit.app.a.b m;
  private final l n;
  private final d o;
  private final o p;
  
  public e(CreditRepository paramCreditRepository, f paramf1, f paramf2, com.truecaller.utils.n paramn, com.truecaller.credit.app.a.b paramb, l paraml, d paramd, o paramo)
  {
    super(paramf2);
    i = paramCreditRepository;
    j = paramf1;
    k = paramf2;
    l = paramn;
    m = paramb;
    n = paraml;
    o = paramd;
    p = paramo;
    paramCreditRepository = new String[] { "android.permission.RECEIVE_SMS", "android.permission.READ_SMS" };
    e = paramCreditRepository;
    f = 100;
    g = "";
  }
  
  private final void c(String paramString)
  {
    Object localObject = new com/truecaller/credit/app/a/a$a;
    ((a.a)localObject).<init>("CreditPanVerification");
    c.n[] arrayOfn = new c.n[2];
    paramString = t.a("Status", paramString);
    arrayOfn[0] = paramString;
    paramString = t.a("Context", "initial_offer_details");
    arrayOfn[1] = paramString;
    ((a.a)localObject).a(arrayOfn);
    ((a.a)localObject).a();
    paramString = m;
    localObject = ((a.a)localObject).b();
    paramString.a((a)localObject);
  }
  
  private final void g()
  {
    Object localObject1 = (c.b)b;
    if (localObject1 != null)
    {
      APIStatusMessage localAPIStatusMessage = new com/truecaller/credit/data/models/APIStatusMessage;
      int i1 = 1;
      Object localObject2 = l;
      int i2 = R.string.credit_all_text_loading;
      Object[] arrayOfObject = new Object[0];
      String str = ((com.truecaller.utils.n)localObject2).a(i2, arrayOfObject);
      k.a(str, "resourceProvider.getStri….credit_all_text_loading)");
      arrayOfObject = null;
      int i3 = 28;
      localObject2 = localAPIStatusMessage;
      localAPIStatusMessage.<init>(i1, str, null, false, null, i3, null);
      ((c.b)localObject1).a(localAPIStatusMessage);
    }
    localObject1 = new com/truecaller/credit/app/ui/onboarding/c/e$b;
    ((e.b)localObject1).<init>(this, null);
    localObject1 = (c.g.a.m)localObject1;
    kotlinx.coroutines.e.b(this, null, (c.g.a.m)localObject1, 3);
  }
  
  public final void a()
  {
    Object localObject = n;
    String[] arrayOfString1 = e;
    int i1 = arrayOfString1.length;
    arrayOfString1 = (String[])Arrays.copyOf(arrayOfString1, i1);
    boolean bool = ((l)localObject).a(arrayOfString1);
    if (bool)
    {
      g();
      return;
    }
    localObject = (c.b)b;
    if (localObject != null)
    {
      int i2 = f;
      String[] arrayOfString2 = e;
      ((c.b)localObject).a(i2, arrayOfString2);
      return;
    }
  }
  
  public final void a(int paramInt)
  {
    Object localObject1 = c;
    if (localObject1 != null)
    {
      Object localObject2 = (String)((List)localObject1).get(paramInt);
      g = ((String)localObject2);
      localObject2 = (c.b)b;
      if (localObject2 != null)
      {
        localObject1 = g;
        ((c.b)localObject2).c((String)localObject1);
      }
      localObject2 = g;
      localObject1 = "CreditUserLocation";
      a.a locala = new com/truecaller/credit/app/a/a$a;
      locala.<init>((String)localObject1, (String)localObject1);
      int i1 = 2;
      localObject1 = new c.n[i1];
      Object localObject3 = t.a("Action", "city_selected");
      localObject1[0] = localObject3;
      int i2 = 1;
      String str = "Custom";
      localObject2 = t.a(str, localObject2);
      localObject1[i2] = localObject2;
      locala.a((c.n[])localObject1);
      localObject2 = locala.a();
      a = false;
      localObject1 = m;
      localObject2 = ((a.a)localObject2).b();
      ((com.truecaller.credit.app.a.b)localObject1).a((a)localObject2);
      localObject2 = g;
      localObject1 = l;
      int i3 = R.string.initial_offer_other_city;
      localObject3 = new Object[0];
      localObject1 = ((com.truecaller.utils.n)localObject1).a(i3, (Object[])localObject3);
      paramInt = k.a(localObject2, localObject1);
      if (paramInt != 0)
      {
        localObject2 = (c.b)b;
        if (localObject2 != null) {
          ((c.b)localObject2).m();
        }
        localObject2 = (c.b)b;
        if (localObject2 != null) {
          ((c.b)localObject2).p();
        }
        return;
      }
      localObject2 = g;
      localObject1 = l;
      i3 = R.string.initial_offer_city_title;
      localObject3 = new Object[0];
      localObject1 = ((com.truecaller.utils.n)localObject1).a(i3, (Object[])localObject3);
      paramInt = k.a(localObject2, localObject1);
      if (paramInt != 0)
      {
        localObject2 = (c.b)b;
        if (localObject2 != null) {
          ((c.b)localObject2).p();
        }
        return;
      }
      localObject2 = d;
      if (localObject2 != null)
      {
        localObject2 = (c.b)b;
        if (localObject2 != null) {
          ((c.b)localObject2).o();
        }
        return;
      }
      localObject2 = (c.b)b;
      if (localObject2 != null)
      {
        ((c.b)localObject2).p();
        return;
      }
      return;
    }
  }
  
  public final void a(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    k.b(paramArrayOfString, "permissions");
    Object localObject1 = "grantResults";
    k.b(paramArrayOfInt, (String)localObject1);
    int i1 = f;
    if (paramInt == i1)
    {
      Object localObject2 = n;
      localObject1 = e;
      int i2 = localObject1.length;
      localObject1 = (String[])Arrays.copyOf((Object[])localObject1, i2);
      paramInt = ((l)localObject2).a(paramArrayOfString, paramArrayOfInt, (String[])localObject1);
      if (paramInt != 0)
      {
        g();
        return;
      }
      localObject2 = (c.b)b;
      if (localObject2 != null)
      {
        paramArrayOfString = l;
        int i3 = R.string.sms_permission_denied;
        localObject1 = new Object[0];
        paramArrayOfString = paramArrayOfString.a(i3, (Object[])localObject1);
        k.a(paramArrayOfString, "resourceProvider.getStri…ng.sms_permission_denied)");
        ((c.b)localObject2).d(paramArrayOfString);
        return;
      }
    }
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "panNumber");
    c("initiated");
    f localf = k;
    Object localObject = new com/truecaller/credit/app/ui/onboarding/c/e$c;
    ((e.c)localObject).<init>(this, paramString, null);
    localObject = (c.g.a.m)localObject;
    kotlinx.coroutines.e.b(this, localf, (c.g.a.m)localObject, 2);
  }
  
  public final boolean b(String paramString)
  {
    k.b(paramString, "panNumber");
    Object localObject = paramString;
    localObject = (CharSequence)paramString;
    boolean bool1 = c.n.m.a((CharSequence)localObject);
    if (!bool1)
    {
      bool1 = c.n.m.a((CharSequence)localObject);
      int i2 = 10;
      if (!bool1)
      {
        int i1 = paramString.length();
        if (i1 < i2) {}
      }
      else
      {
        boolean bool2 = c.n.m.a((CharSequence)localObject);
        if (!bool2)
        {
          int i3 = paramString.length();
          if (i3 == i2) {
            return true;
          }
        }
        paramString = (c.b)b;
        if (paramString != null) {
          paramString.h();
        }
      }
    }
    return false;
  }
  
  public final void e()
  {
    Object localObject = (c.b)b;
    if (localObject != null)
    {
      localObject = p;
      boolean bool = ((o)localObject).a();
      h = bool;
    }
  }
  
  public final void f()
  {
    Object localObject1 = (c.b)b;
    if (localObject1 != null) {
      ((c.b)localObject1).k();
    }
    localObject1 = "CreditUserLocation";
    Object localObject2 = new com/truecaller/credit/app/a/a$a;
    ((a.a)localObject2).<init>((String)localObject1, (String)localObject1);
    localObject1 = t.a("Action", "clicked");
    ((a.a)localObject2).a((c.n)localObject1);
    localObject1 = ((a.a)localObject2).a();
    a = false;
    localObject2 = m;
    localObject1 = ((a.a)localObject1).b();
    ((com.truecaller.credit.app.a.b)localObject2).a((a)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.onboarding.c.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */