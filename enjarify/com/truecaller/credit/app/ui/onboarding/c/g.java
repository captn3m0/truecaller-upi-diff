package com.truecaller.credit.app.ui.onboarding.c;

import c.d.f;
import c.t;
import com.truecaller.ba;
import com.truecaller.credit.app.a.a;
import com.truecaller.credit.app.a.a.a;
import com.truecaller.credit.app.a.b;
import com.truecaller.credit.app.ui.onboarding.views.c.d.b;
import com.truecaller.credit.app.ui.onboarding.views.c.d.c;
import com.truecaller.credit.data.repository.CreditRepository;

public final class g
  extends ba
  implements d.b
{
  private final f c;
  private final f d;
  private final CreditRepository e;
  private final b f;
  private final com.truecaller.utils.n g;
  private final com.truecaller.credit.app.core.g h;
  
  public g(f paramf1, f paramf2, CreditRepository paramCreditRepository, b paramb, com.truecaller.utils.n paramn, com.truecaller.credit.app.core.g paramg)
  {
    super(paramf1);
    c = paramf1;
    d = paramf2;
    e = paramCreditRepository;
    f = paramb;
    g = paramn;
    h = paramg;
  }
  
  public final void a()
  {
    Object localObject1 = (d.c)b;
    if (localObject1 != null) {
      ((d.c)localObject1).i();
    }
    localObject1 = "CreditInitialOfferDetails";
    Object localObject2 = new com/truecaller/credit/app/a/a$a;
    ((a.a)localObject2).<init>((String)localObject1, (String)localObject1);
    localObject1 = new c.n[3];
    c.n localn1 = t.a("Status", "clicked");
    localObject1[0] = localn1;
    c.n localn2 = t.a("Context", "initial_offer_banner");
    localObject1[1] = localn2;
    localn2 = t.a("Action", "Next");
    localObject1[2] = localn2;
    ((a.a)localObject2).a((c.n[])localObject1);
    localObject1 = ((a.a)localObject2).a();
    a = false;
    localObject2 = f;
    localObject1 = ((a.a)localObject1).b();
    ((b)localObject2).a((a)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.onboarding.c.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */