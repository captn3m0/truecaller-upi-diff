package com.truecaller.credit.app.ui.onboarding.c;

import android.telephony.SmsMessage;
import c.d.f;
import com.truecaller.ba;
import com.truecaller.credit.app.ui.onboarding.views.c.a.a;
import com.truecaller.credit.data.repository.CreditRepository;
import com.truecaller.utils.n;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import kotlinx.coroutines.e;

public final class a
  extends ba
  implements a.a
{
  private final f c;
  private final f d;
  private final CreditRepository e;
  private final n f;
  
  public a(f paramf1, f paramf2, CreditRepository paramCreditRepository, n paramn)
  {
    super(paramf1);
    c = paramf1;
    d = paramf2;
    e = paramCreditRepository;
    f = paramn;
  }
  
  private final void a(boolean paramBoolean)
  {
    f localf = c;
    Object localObject = new com/truecaller/credit/app/ui/onboarding/c/a$b;
    ((a.b)localObject).<init>(this, paramBoolean, null);
    localObject = (c.g.a.m)localObject;
    e.b(this, localf, (c.g.a.m)localObject, 2);
  }
  
  private static boolean b(String paramString)
  {
    paramString = (CharSequence)paramString;
    int i = paramString.length();
    boolean bool1 = true;
    String str;
    if (i > 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      str = null;
    }
    if (i != 0)
    {
      str = "\\d+";
      c.n.k localk = new c/n/k;
      localk.<init>(str);
      boolean bool2 = localk.a(paramString);
      if (bool2) {
        return bool1;
      }
    }
    return false;
  }
  
  private final void c(String paramString)
  {
    f localf = c;
    Object localObject = new com/truecaller/credit/app/ui/onboarding/c/a$c;
    ((a.c)localObject).<init>(this, paramString, null);
    localObject = (c.g.a.m)localObject;
    e.b(this, localf, (c.g.a.m)localObject, 2);
  }
  
  public final void a()
  {
    Object localObject = (com.truecaller.credit.app.ui.onboarding.views.c.a.b)b;
    if (localObject != null) {
      ((com.truecaller.credit.app.ui.onboarding.views.c.a.b)localObject).h();
    }
    localObject = (com.truecaller.credit.app.ui.onboarding.views.c.a.b)b;
    if (localObject != null)
    {
      localObject = ((com.truecaller.credit.app.ui.onboarding.views.c.a.b)localObject).k();
      if (localObject != null) {}
    }
    else
    {
      localObject = "";
    }
    boolean bool = b((String)localObject);
    if (bool) {
      c((String)localObject);
    }
  }
  
  public final void a(SmsMessage paramSmsMessage)
  {
    if (paramSmsMessage != null)
    {
      Object localObject1 = paramSmsMessage.getOriginatingAddress();
      if (localObject1 != null)
      {
        localObject1 = (CharSequence)localObject1;
        Object localObject2 = (CharSequence)"CAPFLT";
        boolean bool1 = true;
        boolean bool2 = c.n.m.a((CharSequence)localObject1, (CharSequence)localObject2, bool1);
        if (bool2 == bool1)
        {
          localObject1 = Pattern.compile("\\d+");
          paramSmsMessage = (CharSequence)paramSmsMessage.getMessageBody();
          paramSmsMessage = ((Pattern)localObject1).matcher(paramSmsMessage);
          bool2 = paramSmsMessage.find();
          if (bool2)
          {
            paramSmsMessage = paramSmsMessage.group();
            localObject1 = (com.truecaller.credit.app.ui.onboarding.views.c.a.b)b;
            if (localObject1 != null)
            {
              localObject2 = "otp";
              c.g.b.k.a(paramSmsMessage, (String)localObject2);
              ((com.truecaller.credit.app.ui.onboarding.views.c.a.b)localObject1).a(paramSmsMessage);
              ((com.truecaller.credit.app.ui.onboarding.views.c.a.b)localObject1).i();
            }
            localObject1 = "otp";
            c.g.b.k.a(paramSmsMessage, (String)localObject1);
            c(paramSmsMessage);
          }
        }
        return;
      }
    }
  }
  
  public final void a(String paramString)
  {
    if (paramString == null) {
      return;
    }
    boolean bool = b(paramString);
    if (bool)
    {
      paramString = (com.truecaller.credit.app.ui.onboarding.views.c.a.b)b;
      if (paramString != null) {
        paramString.i();
      }
      return;
    }
    paramString = (com.truecaller.credit.app.ui.onboarding.views.c.a.b)b;
    if (paramString != null)
    {
      paramString.j();
      return;
    }
  }
  
  public final void e()
  {
    a(true);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.onboarding.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */