package com.truecaller.credit.app.ui.onboarding.c;

import c.d.f;
import c.g.a.m;
import c.t;
import com.google.gson.q;
import com.truecaller.ba;
import com.truecaller.credit.app.a.a.a;
import com.truecaller.credit.app.a.b;
import com.truecaller.credit.app.ui.onboarding.views.c.i.a;
import com.truecaller.credit.data.repository.CreditRepository;
import com.truecaller.credit.k;
import kotlinx.coroutines.e;

public final class p
  extends ba
  implements i.a
{
  private int c;
  private final f d;
  private final f e;
  private final k f;
  private final CreditRepository g;
  private final com.truecaller.credit.app.notifications.a h;
  private final q i;
  private final com.truecaller.utils.n j;
  private final b k;
  private final com.truecaller.credit.app.ui.a.a l;
  
  public p(f paramf1, f paramf2, k paramk, CreditRepository paramCreditRepository, com.truecaller.credit.app.notifications.a parama, q paramq, com.truecaller.utils.n paramn, b paramb, com.truecaller.credit.app.ui.a.a parama1)
  {
    super(paramf1);
    d = paramf1;
    e = paramf2;
    f = paramk;
    g = paramCreditRepository;
    h = parama;
    i = paramq;
    j = paramn;
    k = paramb;
    l = parama1;
  }
  
  private final void a(String paramString1, String paramString2, String paramString3)
  {
    Object localObject = "CreditDataEvaluation";
    a.a locala = new com/truecaller/credit/app/a/a$a;
    locala.<init>((String)localObject, (String)localObject);
    localObject = new c.n[4];
    paramString1 = t.a("Status", paramString1);
    localObject[0] = paramString1;
    paramString1 = t.a("Context", paramString2);
    localObject[1] = paramString1;
    paramString1 = t.a("Action", paramString3);
    localObject[2] = paramString1;
    paramString1 = t.a("Type", "final_offer");
    localObject[3] = paramString1;
    locala.a((c.n[])localObject);
    paramString1 = locala.a();
    a = false;
    paramString2 = k;
    paramString1 = paramString1.b();
    paramString2.a(paramString1);
  }
  
  public final int a(String paramString)
  {
    Object localObject = new com/truecaller/credit/app/ui/onboarding/c/p$c;
    ((p.c)localObject).<init>(this, paramString, null);
    localObject = (m)localObject;
    e.b(this, null, (m)localObject, 3);
    return 1;
  }
  
  public final void y_()
  {
    h.c();
    f.b();
    super.y_();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.onboarding.c.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */