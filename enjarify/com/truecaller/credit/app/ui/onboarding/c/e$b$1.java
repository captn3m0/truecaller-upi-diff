package com.truecaller.credit.app.ui.onboarding.c;

import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.credit.data.models.SaveLocationRequest;
import com.truecaller.credit.data.repository.CreditRepository;
import kotlinx.coroutines.ag;

final class e$b$1
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  e$b$1(e.b paramb, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    1 local1 = new com/truecaller/credit/app/ui/onboarding/c/e$b$1;
    e.b localb = b;
    local1.<init>(localb, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return local1;
  }
  
  public final Object a(Object paramObject)
  {
    a locala = a.a;
    int i = a;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label138;
      }
      paramObject = e.e(b.b);
      SaveLocationRequest localSaveLocationRequest = new com/truecaller/credit/data/models/SaveLocationRequest;
      String str = e.f(b.b);
      localSaveLocationRequest.<init>(str);
      int j = 1;
      a = j;
      paramObject = ((CreditRepository)paramObject).saveLocation(localSaveLocationRequest, this);
      if (paramObject == locala) {
        return locala;
      }
      break;
    }
    return paramObject;
    label138:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (1)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((1)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.onboarding.c.e.b.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */