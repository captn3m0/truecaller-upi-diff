package com.truecaller.credit.app.ui.onboarding.a;

import android.arch.persistence.room.i;
import android.arch.persistence.room.j;
import android.database.Cursor;
import c.g.b.k;
import com.truecaller.credit.domain.interactors.onboarding.models.d;
import java.lang.reflect.Type;
import java.util.List;

public final class c
  implements b
{
  private final android.arch.persistence.room.f a;
  private final android.arch.persistence.room.c b;
  private final a c;
  private final j d;
  
  public c(android.arch.persistence.room.f paramf)
  {
    Object localObject = new com/truecaller/credit/app/ui/onboarding/a/a;
    ((a)localObject).<init>();
    c = ((a)localObject);
    a = paramf;
    localObject = new com/truecaller/credit/app/ui/onboarding/a/c$1;
    ((c.1)localObject).<init>(this, paramf);
    b = ((android.arch.persistence.room.c)localObject);
    localObject = new com/truecaller/credit/app/ui/onboarding/a/c$2;
    ((c.2)localObject).<init>(this, paramf);
    d = ((j)localObject);
  }
  
  public final d a()
  {
    i locali = i.a("Select * from locations", 0);
    Cursor localCursor = a.a(locali);
    String str = "_id";
    try
    {
      int i = localCursor.getColumnIndexOrThrow(str);
      Object localObject2 = "locations";
      int j = localCursor.getColumnIndexOrThrow((String)localObject2);
      boolean bool = localCursor.moveToFirst();
      Object localObject3;
      if (bool)
      {
        localObject2 = localCursor.getString(j);
        localObject3 = "value";
        k.b(localObject2, (String)localObject3);
        localObject3 = new com/truecaller/credit/app/ui/onboarding/a/a$a;
        ((a.a)localObject3).<init>();
        localObject3 = b;
        com.google.gson.f localf = new com/google/gson/f;
        localf.<init>();
        localObject2 = localf.a((String)localObject2, (Type)localObject3);
        localObject3 = "Gson().fromJson(value, listType)";
        k.a(localObject2, (String)localObject3);
        localObject2 = (List)localObject2;
        localObject3 = new com/truecaller/credit/domain/interactors/onboarding/models/d;
        ((d)localObject3).<init>((List)localObject2);
        long l = localCursor.getLong(i);
        a = l;
      }
      else
      {
        bool = false;
        localObject3 = null;
      }
      return (d)localObject3;
    }
    finally
    {
      localCursor.close();
      locali.b();
    }
  }
  
  public final void a(d paramd)
  {
    Object localObject = a;
    ((android.arch.persistence.room.f)localObject).d();
    try
    {
      localObject = b;
      ((android.arch.persistence.room.c)localObject).a(paramd);
      paramd = a;
      paramd.f();
      return;
    }
    finally
    {
      a.e();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.onboarding.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */