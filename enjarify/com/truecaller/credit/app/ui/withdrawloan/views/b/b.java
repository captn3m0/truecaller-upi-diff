package com.truecaller.credit.app.ui.withdrawloan.views.b;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.view.View;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.common.ui.PausingLottieAnimationView;
import com.truecaller.credit.R.id;
import com.truecaller.credit.R.layout;
import com.truecaller.credit.app.ui.assist.LoanConfirmationStatus;
import com.truecaller.credit.app.ui.withdrawloan.a.a.a.a;
import com.truecaller.credit.app.ui.withdrawloan.views.c.d;
import com.truecaller.credit.i;
import com.truecaller.credit.i.a;
import java.util.HashMap;

public final class b
  extends com.truecaller.credit.app.ui.b.a
  implements Animator.AnimatorListener, com.truecaller.credit.app.ui.withdrawloan.views.c.b.b
{
  public static final b.a c;
  public com.truecaller.credit.app.ui.withdrawloan.views.c.b.a b;
  private d d;
  private final b.b e;
  private HashMap f;
  
  static
  {
    b.a locala = new com/truecaller/credit/app/ui/withdrawloan/views/b/b$a;
    locala.<init>((byte)0);
    c = locala;
  }
  
  public b()
  {
    b.b localb = new com/truecaller/credit/app/ui/withdrawloan/views/b/b$b;
    localb.<init>(this);
    e = localb;
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = f;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      f = ((HashMap)localObject1);
    }
    localObject1 = f;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = f;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a()
  {
    a.a locala = com.truecaller.credit.app.ui.withdrawloan.a.a.a.a();
    Object localObject = i.i;
    localObject = i.a.a();
    locala.a((com.truecaller.credit.app.c.a.a)localObject).a().a(this);
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "description");
    int i = R.id.textDescription;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "textDescription");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void a(String paramString, int paramInt)
  {
    k.b(paramString, "lottie");
    int i = R.id.lottieLoanConfirmation;
    PausingLottieAnimationView localPausingLottieAnimationView = (PausingLottieAnimationView)a(i);
    localPausingLottieAnimationView.setAnimation(paramString);
    localPausingLottieAnimationView.setRepeatCount(paramInt);
    localPausingLottieAnimationView.a();
    paramString = this;
    paramString = (Animator.AnimatorListener)this;
    localPausingLottieAnimationView.a(paramString);
  }
  
  public final int b()
  {
    return R.layout.fragment_loan_confirmation;
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "status");
    int i = R.id.textStatus;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "textStatus");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void c()
  {
    HashMap localHashMap = f;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final LoanConfirmationStatus d()
  {
    Bundle localBundle = getArguments();
    if (localBundle != null) {
      return (LoanConfirmationStatus)localBundle.getParcelable("status");
    }
    return null;
  }
  
  public final void e()
  {
    f localf = getActivity();
    if (localf != null)
    {
      localf.finish();
      return;
    }
  }
  
  public final void f()
  {
    d locald = d;
    if (locald != null)
    {
      locald.a(false);
      return;
    }
  }
  
  public final void onAnimationCancel(Animator paramAnimator) {}
  
  public final void onAnimationEnd(Animator paramAnimator)
  {
    paramAnimator = b;
    if (paramAnimator == null)
    {
      String str = "presenter";
      k.a(str);
    }
    paramAnimator.a();
  }
  
  public final void onAnimationRepeat(Animator paramAnimator) {}
  
  public final void onAnimationStart(Animator paramAnimator) {}
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    boolean bool = paramContext instanceof d;
    if (bool)
    {
      paramContext = (d)paramContext;
      d = paramContext;
      return;
    }
    RuntimeException localRuntimeException = new java/lang/RuntimeException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(paramContext);
    localStringBuilder.append(" must implement WithdrawLoanActionListener");
    paramContext = localStringBuilder.toString();
    localRuntimeException.<init>(paramContext);
    throw ((Throwable)localRuntimeException);
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    String str = "view";
    k.b(paramView, str);
    super.onViewCreated(paramView, paramBundle);
    paramView = b;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramView.a(this);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.withdrawloan.views.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */