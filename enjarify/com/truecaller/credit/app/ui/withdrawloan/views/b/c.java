package com.truecaller.credit.app.ui.withdrawloan.views.b;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import c.g.b.k;
import c.u;
import com.truecaller.credit.R.id;
import com.truecaller.credit.R.layout;
import com.truecaller.credit.app.ui.assist.LoanConfirmationStatus;
import com.truecaller.credit.app.ui.withdrawloan.a.a.a.a;
import com.truecaller.credit.app.ui.withdrawloan.views.a.f;
import com.truecaller.credit.app.ui.withdrawloan.views.a.f.a;
import com.truecaller.credit.app.ui.withdrawloan.views.a.g;
import com.truecaller.credit.app.ui.withdrawloan.views.activities.WithdrawLoanActivity;
import com.truecaller.credit.app.ui.withdrawloan.views.c.d;
import com.truecaller.credit.domain.interactors.withdrawloan.models.LoanCategory;
import com.truecaller.credit.i;
import com.truecaller.credit.i.a;
import com.truecaller.utils.extensions.p;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;
import java.util.List;

public final class c
  extends com.truecaller.credit.app.ui.b.c
  implements f.a, com.truecaller.credit.app.ui.withdrawloan.views.c.c.b
{
  public static final c.a d;
  public g c;
  private d e;
  private f f;
  private final c.g.a.b g;
  private HashMap h;
  
  static
  {
    c.a locala = new com/truecaller/credit/app/ui/withdrawloan/views/b/c$a;
    locala.<init>((byte)0);
    d = locala;
  }
  
  public c()
  {
    Object localObject = new com/truecaller/credit/app/ui/withdrawloan/views/b/c$b;
    ((c.b)localObject).<init>(this);
    localObject = (c.g.a.b)localObject;
    g = ((c.g.a.b)localObject);
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = h;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      h = ((HashMap)localObject1);
    }
    localObject1 = h;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = h;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a(LoanConfirmationStatus paramLoanConfirmationStatus, String paramString)
  {
    k.b(paramLoanConfirmationStatus, "status");
    k.b(paramString, "tag");
    Object localObject = b.c;
    k.b(paramLoanConfirmationStatus, "status");
    localObject = new com/truecaller/credit/app/ui/withdrawloan/views/b/b;
    ((b)localObject).<init>();
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    paramLoanConfirmationStatus = (Parcelable)paramLoanConfirmationStatus;
    localBundle.putParcelable("status", paramLoanConfirmationStatus);
    ((b)localObject).setArguments(localBundle);
    paramLoanConfirmationStatus = getFragmentManager();
    ((b)localObject).show(paramLoanConfirmationStatus, paramString);
  }
  
  public final void a(LoanCategory paramLoanCategory)
  {
    k.b(paramLoanCategory, "loanCategory");
    com.truecaller.credit.app.ui.withdrawloan.views.c.c.a locala = (com.truecaller.credit.app.ui.withdrawloan.views.c.c.a)a();
    String str = paramLoanCategory.getId();
    paramLoanCategory = paramLoanCategory.getName();
    locala.b(str, paramLoanCategory);
    paramLoanCategory = f;
    if (paramLoanCategory != null)
    {
      paramLoanCategory.notifyDataSetChanged();
      return;
    }
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "title");
    Object localObject = getActivity();
    if (localObject != null)
    {
      localObject = ((WithdrawLoanActivity)localObject).getSupportActionBar();
      if (localObject != null)
      {
        paramString = (CharSequence)paramString;
        ((ActionBar)localObject).setTitle(paramString);
        return;
      }
      return;
    }
    paramString = new c/u;
    paramString.<init>("null cannot be cast to non-null type com.truecaller.credit.app.ui.withdrawloan.views.activities.WithdrawLoanActivity");
    throw paramString;
  }
  
  public final void a(List paramList)
  {
    k.b(paramList, "loanCategories");
    Object localObject1 = getContext();
    if (localObject1 != null)
    {
      int i = R.id.checkEmiContainer;
      Object localObject2 = (FrameLayout)a(i);
      k.a(localObject2, "checkEmiContainer");
      t.a((View)localObject2);
      localObject2 = new com/truecaller/credit/app/ui/withdrawloan/views/a/f;
      k.a(localObject1, "it");
      g localg = c;
      if (localg == null)
      {
        localObject3 = "loanCategoryItemPresenter";
        k.a((String)localObject3);
      }
      Object localObject3 = this;
      localObject3 = (f.a)this;
      ((f)localObject2).<init>((Context)localObject1, paramList, localg, (f.a)localObject3);
      f = ((f)localObject2);
      int j = R.id.lisCategories;
      paramList = (RecyclerView)a(j);
      paramList.setHasFixedSize(true);
      localObject1 = (RecyclerView.Adapter)f;
      paramList.setAdapter((RecyclerView.Adapter)localObject1);
      return;
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    int i = R.id.containerCategoryDescription;
    TextInputLayout localTextInputLayout = (TextInputLayout)a(i);
    k.a(localTextInputLayout, "containerCategoryDescription");
    t.a((View)localTextInputLayout, paramBoolean);
  }
  
  public final void b()
  {
    a.a locala = com.truecaller.credit.app.ui.withdrawloan.a.a.a.a();
    Object localObject = i.i;
    localObject = i.a.a();
    locala.a((com.truecaller.credit.app.c.a.a)localObject).a().a(this);
  }
  
  public final void b(int paramInt)
  {
    Object localObject = getActivity();
    if (localObject != null)
    {
      localObject = ((WithdrawLoanActivity)localObject).getSupportActionBar();
      if (localObject != null)
      {
        ((ActionBar)localObject).setElevation(0.0F);
        ((ActionBar)localObject).setDisplayHomeAsUpEnabled(true);
        ((ActionBar)localObject).setHomeAsUpIndicator(paramInt);
        return;
      }
      return;
    }
    u localu = new c/u;
    localu.<init>("null cannot be cast to non-null type com.truecaller.credit.app.ui.withdrawloan.views.activities.WithdrawLoanActivity");
    throw localu;
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "text");
    d locald = e;
    if (locald != null)
    {
      locald.a(paramString);
      return;
    }
  }
  
  public final int c()
  {
    return R.layout.fragment_loan_description;
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "message");
    Context localContext = getContext();
    paramString = (CharSequence)paramString;
    Toast.makeText(localContext, paramString, 0).show();
  }
  
  public final boolean d()
  {
    return true;
  }
  
  public final void f()
  {
    HashMap localHashMap = h;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final void g()
  {
    int i = R.id.textLoanDescription;
    Object localObject = (TextInputEditText)a(i);
    k.a(localObject, "textLoanDescription");
    localObject = (TextView)localObject;
    c.g.a.b localb = g;
    p.a((TextView)localObject, localb);
    i = R.id.textCategoryDescription;
    localObject = (TextInputEditText)a(i);
    k.a(localObject, "textCategoryDescription");
    localObject = (TextView)localObject;
    localb = g;
    p.a((TextView)localObject, localb);
  }
  
  public final void h()
  {
    d locald = e;
    if (locald != null)
    {
      locald.h();
      return;
    }
  }
  
  public final void i()
  {
    d locald = e;
    if (locald != null)
    {
      locald.g();
      return;
    }
  }
  
  public final void j()
  {
    int i = R.id.loanDescriptionProgress;
    ProgressBar localProgressBar = (ProgressBar)a(i);
    k.a(localProgressBar, "loanDescriptionProgress");
    t.a((View)localProgressBar);
  }
  
  public final void k()
  {
    int i = R.id.loanDescriptionProgress;
    ProgressBar localProgressBar = (ProgressBar)a(i);
    k.a(localProgressBar, "loanDescriptionProgress");
    t.b((View)localProgressBar);
  }
  
  public final String l()
  {
    Bundle localBundle = getArguments();
    if (localBundle != null) {
      return localBundle.getString("amount");
    }
    return null;
  }
  
  public final void m()
  {
    d locald = e;
    if (locald != null)
    {
      locald.a(false);
      return;
    }
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    boolean bool = paramContext instanceof d;
    if (bool)
    {
      paramContext = (d)paramContext;
      e = paramContext;
      return;
    }
    RuntimeException localRuntimeException = new java/lang/RuntimeException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(paramContext);
    localStringBuilder.append(" must implement WithdrawLoanActionListener");
    paramContext = localStringBuilder.toString();
    localRuntimeException.<init>(paramContext);
    throw ((Throwable)localRuntimeException);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.withdrawloan.views.b.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */