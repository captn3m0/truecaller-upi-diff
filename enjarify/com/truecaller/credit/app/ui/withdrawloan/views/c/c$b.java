package com.truecaller.credit.app.ui.withdrawloan.views.c;

import com.truecaller.credit.app.ui.assist.LoanConfirmationStatus;
import java.util.List;

public abstract interface c$b
{
  public static final c.b.a a = c.b.a.a;
  
  public abstract void a(LoanConfirmationStatus paramLoanConfirmationStatus, String paramString);
  
  public abstract void a(String paramString);
  
  public abstract void a(List paramList);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract void b(int paramInt);
  
  public abstract void b(String paramString);
  
  public abstract void c(String paramString);
  
  public abstract void g();
  
  public abstract void h();
  
  public abstract void i();
  
  public abstract void j();
  
  public abstract void k();
  
  public abstract String l();
  
  public abstract void m();
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.withdrawloan.views.c.c.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */