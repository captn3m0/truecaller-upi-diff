package com.truecaller.credit.app.ui.withdrawloan.views.b;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import c.g.b.k;
import c.n.m;
import c.u;
import com.truecaller.credit.R.id;
import com.truecaller.credit.R.layout;
import com.truecaller.credit.app.ui.withdrawloan.views.activities.WithdrawLoanActivity;
import com.truecaller.credit.app.ui.withdrawloan.views.c.a.b;
import com.truecaller.credit.app.ui.withdrawloan.views.c.d;
import com.truecaller.credit.domain.interactors.withdrawloan.models.Emi;
import com.truecaller.credit.domain.interactors.withdrawloan.models.EmiTypes;
import com.truecaller.credit.i;
import com.truecaller.credit.i.a;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;

public final class a
  extends com.truecaller.credit.app.ui.b.c
  implements TextWatcher, com.truecaller.credit.app.ui.withdrawloan.views.a.a.a, a.b
{
  public static final a.a d;
  public com.truecaller.credit.app.ui.withdrawloan.views.a.b c;
  private d e;
  private com.truecaller.credit.app.ui.withdrawloan.views.a.a f;
  private HashMap g;
  
  static
  {
    a.a locala = new com/truecaller/credit/app/ui/withdrawloan/views/b/a$a;
    locala.<init>((byte)0);
    d = locala;
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = g;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      g = ((HashMap)localObject1);
    }
    localObject1 = g;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = g;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a(Emi paramEmi)
  {
    k.b(paramEmi, "emiData");
    com.truecaller.credit.app.ui.withdrawloan.views.a.a locala = f;
    if (locala != null) {
      locala.notifyDataSetChanged();
    }
    ((com.truecaller.credit.app.ui.withdrawloan.views.c.a.a)a()).a(paramEmi);
  }
  
  public final void a(EmiTypes paramEmiTypes)
  {
    k.b(paramEmiTypes, "emiTypes");
    Object localObject1 = getContext();
    if (localObject1 != null)
    {
      int i = R.id.checkEmiContainer;
      Object localObject2 = (FrameLayout)a(i);
      Object localObject3 = "checkEmiContainer";
      k.a(localObject2, (String)localObject3);
      t.a((View)localObject2);
      localObject2 = c;
      if (localObject2 == null)
      {
        localObject3 = "checkEmiItemPresenter";
        k.a((String)localObject3);
      }
      int j = -1;
      ((com.truecaller.credit.app.ui.withdrawloan.views.a.b)localObject2).a(j);
      localObject2 = new com/truecaller/credit/app/ui/withdrawloan/views/a/a;
      k.a(localObject1, "it");
      localObject3 = c;
      if (localObject3 == null)
      {
        localObject4 = "checkEmiItemPresenter";
        k.a((String)localObject4);
      }
      Object localObject4 = this;
      localObject4 = (com.truecaller.credit.app.ui.withdrawloan.views.a.a.a)this;
      ((com.truecaller.credit.app.ui.withdrawloan.views.a.a)localObject2).<init>((Context)localObject1, paramEmiTypes, (com.truecaller.credit.app.ui.withdrawloan.views.a.b)localObject3, (com.truecaller.credit.app.ui.withdrawloan.views.a.a.a)localObject4);
      f = ((com.truecaller.credit.app.ui.withdrawloan.views.a.a)localObject2);
      paramEmiTypes = new android/support/v7/widget/LinearLayoutManager;
      localObject1 = getContext();
      paramEmiTypes.<init>((Context)localObject1);
      int k = R.id.lisEmi;
      localObject1 = (RecyclerView)a(k);
      paramEmiTypes = (RecyclerView.LayoutManager)paramEmiTypes;
      ((RecyclerView)localObject1).setLayoutManager(paramEmiTypes);
      ((RecyclerView)localObject1).setHasFixedSize(true);
      paramEmiTypes = (RecyclerView.Adapter)f;
      ((RecyclerView)localObject1).setAdapter(paramEmiTypes);
      return;
    }
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "title");
    Object localObject = getActivity();
    if (localObject != null)
    {
      localObject = ((WithdrawLoanActivity)localObject).getSupportActionBar();
      if (localObject != null)
      {
        paramString = (CharSequence)paramString;
        ((ActionBar)localObject).setTitle(paramString);
        return;
      }
      return;
    }
    paramString = new c/u;
    paramString.<init>("null cannot be cast to non-null type com.truecaller.credit.app.ui.withdrawloan.views.activities.WithdrawLoanActivity");
    throw paramString;
  }
  
  public final void a(String paramString, int paramInt1, int paramInt2)
  {
    k.b(paramString, "textVal");
    int i = R.id.textAmount;
    AppCompatEditText localAppCompatEditText = (AppCompatEditText)a(i);
    Object localObject1 = this;
    localObject1 = (TextWatcher)this;
    localAppCompatEditText.removeTextChangedListener((TextWatcher)localObject1);
    paramString = (CharSequence)paramString;
    localAppCompatEditText.setText(paramString);
    int j = R.id.textAmount;
    paramString = (AppCompatEditText)a(j);
    int k = R.id.textAmount;
    Object localObject2 = (AppCompatEditText)a(k);
    k.a(localObject2, "textAmount");
    localObject2 = String.valueOf(((AppCompatEditText)localObject2).getText());
    String str = "receiver$0";
    k.b(localObject2, str);
    int m = ((String)localObject2).length();
    if (m > paramInt1)
    {
      m = ((String)localObject2).length();
      int n = 2;
      if (m != n)
      {
        int i1 = 6;
        if (m != i1)
        {
          if (paramInt1 != 0)
          {
            localObject2 = (CharSequence)localObject2;
            Object localObject3 = (CharSequence)",";
            m = 0;
            str = null;
            paramInt2 = m.a((CharSequence)localObject2, (CharSequence)localObject3, false);
            if (paramInt2 != 0)
            {
              localObject3 = ",";
              paramInt2 = m.a((CharSequence)localObject2, (String)localObject3, 0, false, i1);
              if (paramInt2 == n)
              {
                localObject3 = ",";
                paramInt2 = m.a((CharSequence)localObject2, (String)localObject3, 0, false, i1);
                if (paramInt2 >= paramInt1) {}
              }
            }
          }
          else
          {
            paramInt1 += 1;
          }
        }
        else
        {
          m = ((String)localObject2).length();
          if (paramInt2 <= m) {
            paramInt1 = ((String)localObject2).length();
          }
        }
      }
      else
      {
        paramInt1 += 1;
      }
    }
    else
    {
      paramInt1 = ((String)localObject2).length();
    }
    paramString.setSelection(paramInt1);
    localAppCompatEditText.addTextChangedListener((TextWatcher)localObject1);
  }
  
  public final void a(String paramString1, String paramString2)
  {
    k.b(paramString1, "tenure");
    k.b(paramString2, "amount");
    d locald = e;
    if (locald != null)
    {
      Object localObject = c.d;
      k.b(paramString1, "tenure");
      k.b(paramString2, "amount");
      localObject = new com/truecaller/credit/app/ui/withdrawloan/views/b/c;
      ((c)localObject).<init>();
      Bundle localBundle = new android/os/Bundle;
      localBundle.<init>();
      localBundle.putString("tenure", paramString1);
      localBundle.putString("amount", paramString2);
      ((c)localObject).setArguments(localBundle);
      localObject = (Fragment)localObject;
      locald.a((Fragment)localObject);
      return;
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    d locald = e;
    if (locald != null)
    {
      locald.a(paramBoolean);
      return;
    }
  }
  
  public final void afterTextChanged(Editable paramEditable)
  {
    Object localObject = getActivity();
    if (localObject != null)
    {
      localObject = ((f)localObject).getCurrentFocus();
    }
    else
    {
      bool = false;
      localObject = null;
    }
    int i = R.id.textAmount;
    AppCompatEditText localAppCompatEditText = (AppCompatEditText)a(i);
    boolean bool = k.a(localObject, localAppCompatEditText);
    if ((bool) && (paramEditable != null))
    {
      localObject = (com.truecaller.credit.app.ui.withdrawloan.views.c.a.a)a();
      paramEditable = paramEditable.toString();
      i = R.id.textAmount;
      localAppCompatEditText = (AppCompatEditText)a(i);
      k.a(localAppCompatEditText, "textAmount");
      i = localAppCompatEditText.getSelectionStart();
      ((com.truecaller.credit.app.ui.withdrawloan.views.c.a.a)localObject).a(paramEditable, i);
      return;
    }
  }
  
  public final void b()
  {
    com.truecaller.credit.app.ui.withdrawloan.a.a.a.a locala = com.truecaller.credit.app.ui.withdrawloan.a.a.a.a();
    Object localObject = i.i;
    localObject = i.a.a();
    locala.a((com.truecaller.credit.app.c.a.a)localObject).a().a(this);
  }
  
  public final void b(int paramInt)
  {
    Object localObject = getActivity();
    if (localObject != null)
    {
      localObject = ((WithdrawLoanActivity)localObject).getSupportActionBar();
      if (localObject != null)
      {
        ((ActionBar)localObject).setElevation(0.0F);
        ((ActionBar)localObject).setDisplayHomeAsUpEnabled(true);
        ((ActionBar)localObject).setHomeAsUpIndicator(paramInt);
        return;
      }
      return;
    }
    u localu = new c/u;
    localu.<init>("null cannot be cast to non-null type com.truecaller.credit.app.ui.withdrawloan.views.activities.WithdrawLoanActivity");
    throw localu;
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "description");
    d locald = e;
    if (locald != null)
    {
      locald.b(paramString);
      return;
    }
  }
  
  public final void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
  
  public final int c()
  {
    return R.layout.fragment_amount_entry;
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "message");
    int i = R.id.textHeader;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "textHeader");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void d(String paramString)
  {
    k.b(paramString, "hint");
    int i = R.id.bottomText;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "bottomText");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void e(String paramString)
  {
    k.b(paramString, "message");
    Context localContext = getContext();
    paramString = (CharSequence)paramString;
    Toast.makeText(localContext, paramString, 0).show();
  }
  
  public final void f()
  {
    HashMap localHashMap = g;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final void f(String paramString)
  {
    k.b(paramString, "text");
    d locald = e;
    if (locald != null)
    {
      locald.a(paramString);
      return;
    }
  }
  
  public final void g()
  {
    int i = R.id.textAmount;
    AppCompatEditText localAppCompatEditText = (AppCompatEditText)a(i);
    Object localObject = this;
    localObject = (TextWatcher)this;
    localAppCompatEditText.addTextChangedListener((TextWatcher)localObject);
  }
  
  public final void h()
  {
    int i = R.id.checkEmiProgress;
    ProgressBar localProgressBar = (ProgressBar)a(i);
    k.a(localProgressBar, "checkEmiProgress");
    t.a((View)localProgressBar);
  }
  
  public final void i()
  {
    int i = R.id.checkEmiProgress;
    ProgressBar localProgressBar = (ProgressBar)a(i);
    k.a(localProgressBar, "checkEmiProgress");
    t.b((View)localProgressBar);
  }
  
  public final void j()
  {
    int i = R.id.checkEmiContainer;
    FrameLayout localFrameLayout = (FrameLayout)a(i);
    k.a(localFrameLayout, "checkEmiContainer");
    t.b((View)localFrameLayout);
  }
  
  public final void k()
  {
    int i = R.id.textAmount;
    AppCompatEditText localAppCompatEditText = (AppCompatEditText)a(i);
    k.a(localAppCompatEditText, "textAmount");
    t.a((View)localAppCompatEditText, false, 5);
  }
  
  public final void l()
  {
    int i = R.id.textAmount;
    Object localObject = (AppCompatEditText)a(i);
    String str = "textAmount";
    k.a(localObject, str);
    localObject = ((AppCompatEditText)localObject).getText();
    if (localObject != null)
    {
      ((Editable)localObject).clear();
      return;
    }
  }
  
  public final void m()
  {
    d locald = e;
    if (locald != null)
    {
      locald.h();
      return;
    }
  }
  
  public final void n()
  {
    d locald = e;
    if (locald != null)
    {
      locald.g();
      return;
    }
  }
  
  public final void onAttach(Context paramContext)
  {
    Object localObject = "context";
    k.b(paramContext, (String)localObject);
    super.onAttach(paramContext);
    boolean bool = paramContext instanceof d;
    if (bool)
    {
      paramContext = (d)paramContext;
      e = paramContext;
      return;
    }
    localObject = new java/lang/RuntimeException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(paramContext);
    localStringBuilder.append(" must implement WithdrawLoanActionListener");
    paramContext = localStringBuilder.toString();
    ((RuntimeException)localObject).<init>(paramContext);
    throw ((Throwable)localObject);
  }
  
  public final void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.withdrawloan.views.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */