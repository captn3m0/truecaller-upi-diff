package com.truecaller.credit.app.ui.withdrawloan.views.a;

import android.widget.TextView;
import c.g.b.k;
import com.truecaller.credit.R.color;
import com.truecaller.credit.R.id;
import com.truecaller.credit.R.string;
import com.truecaller.credit.app.ui.customview.CustomInfoCollectionRadioButton;
import com.truecaller.credit.app.util.f;
import com.truecaller.credit.domain.interactors.withdrawloan.models.Emi;
import com.truecaller.utils.n;

public final class c
  implements b
{
  private int a;
  private final n b;
  private final f c;
  
  public c(n paramn, f paramf)
  {
    b = paramn;
    c = paramf;
    a = -1;
  }
  
  public final void a(int paramInt)
  {
    a = paramInt;
  }
  
  public final void a(e parame, Emi paramEmi, String paramString)
  {
    k.b(parame, "checkEmiItemView");
    k.b(paramEmi, "emiData");
    k.b(paramString, "interestRate");
    Object localObject1 = paramEmi.getEmi_amount();
    k.b(localObject1, "amount");
    int i = R.id.textAmount;
    Object localObject2 = (TextView)parame.a(i);
    k.a(localObject2, "textAmount");
    localObject1 = (CharSequence)localObject1;
    ((TextView)localObject2).setText((CharSequence)localObject1);
    localObject1 = b;
    localObject2 = paramEmi.getTenure_type();
    String str = "month";
    boolean bool1 = k.a(localObject2, str);
    int j;
    if (bool1) {
      j = R.string.credit_in_months;
    } else {
      j = R.string.credit_in_year;
    }
    boolean bool2 = true;
    Object localObject3 = new Object[bool2];
    paramEmi = paramEmi.getTenure();
    localObject3[0] = paramEmi;
    paramEmi = ((n)localObject1).a(j, (Object[])localObject3);
    k.a(paramEmi, "resourceProvider.getStri….tenure\n                )");
    k.b(paramEmi, "title");
    int k = R.id.textEmi;
    localObject1 = (CustomInfoCollectionRadioButton)parame.a(k);
    ((CustomInfoCollectionRadioButton)localObject1).setTitle(paramEmi);
    paramEmi = b;
    k = R.string.credit_emi_interest;
    localObject2 = new Object[bool2];
    localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>();
    ((StringBuilder)localObject3).append(paramString);
    ((StringBuilder)localObject3).append('%');
    paramString = ((StringBuilder)localObject3).toString();
    localObject2[0] = paramString;
    paramEmi = paramEmi.a(k, (Object[])localObject2);
    k.a(paramEmi, "resourceProvider.getStri…terest, \"$interestRate%\")");
    k.b(paramEmi, "subTitle");
    int m = R.id.textEmi;
    paramString = (CustomInfoCollectionRadioButton)parame.a(m);
    paramString.a(paramEmi, false);
    int n = a;
    m = -1;
    if (n != m)
    {
      m = R.id.textEmi;
      paramString = (CustomInfoCollectionRadioButton)parame.a(m);
      k = parame.getAdapterPosition();
      if (k != n)
      {
        bool2 = false;
        str = null;
      }
      paramString.setChecked(bool2);
    }
    paramEmi = c;
    m = R.color.white;
    n = paramEmi.a(m);
    m = R.id.textEmi;
    ((CustomInfoCollectionRadioButton)parame.a(m)).setContainerBackground(n);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.withdrawloan.views.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */