package com.truecaller.credit.app.ui.withdrawloan.views.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v4.app.j;
import android.support.v4.app.o;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.credit.R.id;
import com.truecaller.credit.R.layout;
import com.truecaller.credit.R.menu;
import com.truecaller.credit.app.ui.faq.activities.CreditWebViewActivity;
import com.truecaller.credit.app.ui.withdrawloan.views.b.c;
import com.truecaller.credit.app.ui.withdrawloan.views.c.c.a;
import com.truecaller.credit.app.ui.withdrawloan.views.c.e.a;
import com.truecaller.credit.app.ui.withdrawloan.views.c.e.b;
import com.truecaller.credit.i;
import com.truecaller.credit.i.a;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;

public final class WithdrawLoanActivity
  extends com.truecaller.credit.app.ui.b.b
  implements View.OnClickListener, com.truecaller.credit.app.ui.withdrawloan.views.c.d, e.b
{
  private HashMap b;
  
  public final View a(int paramInt)
  {
    Object localObject1 = b;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      b = ((HashMap)localObject1);
    }
    localObject1 = b;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = findViewById(paramInt);
      localObject2 = b;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a(Fragment paramFragment)
  {
    k.b(paramFragment, "fragment");
    o localo = getSupportFragmentManager().a();
    int i = R.id.container;
    String str1 = paramFragment.getClass().getSimpleName();
    localo = localo.b(i, paramFragment, str1);
    String str2 = "supportFragmentManager.b…t::class.java.simpleName)";
    k.a(localo, str2);
    paramFragment = (com.truecaller.credit.app.ui.b.d)paramFragment;
    boolean bool = paramFragment.d();
    if (bool)
    {
      bool = false;
      paramFragment = null;
      localo.a(null);
    }
    localo.d();
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "text");
    int i = R.id.btnContinue;
    Button localButton = (Button)a(i);
    k.a(localButton, "btnContinue");
    paramString = (CharSequence)paramString;
    localButton.setText(paramString);
  }
  
  public final void a(boolean paramBoolean)
  {
    int i = R.id.textProcessingFee;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "textProcessingFee");
    t.a((View)localTextView, paramBoolean);
  }
  
  public final int b()
  {
    return R.layout.activity_withdraw_loan;
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "description");
    int i = R.id.textProcessingFee;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "textProcessingFee");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void c()
  {
    com.truecaller.credit.app.ui.withdrawloan.a.a.a.a locala = com.truecaller.credit.app.ui.withdrawloan.a.a.a.a();
    Object localObject = i.i;
    localObject = i.a.a();
    locala.a((com.truecaller.credit.app.c.a.a)localObject).a().a(this);
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "url");
    Intent localIntent = new android/content/Intent;
    Object localObject = this;
    localObject = (Context)this;
    localIntent.<init>((Context)localObject, CreditWebViewActivity.class);
    localIntent.putExtra("url", paramString);
    startActivity(localIntent);
  }
  
  public final void d()
  {
    int i = R.id.toolbarWithdrawLoan;
    Object localObject1 = (Toolbar)a(i);
    setSupportActionBar((Toolbar)localObject1);
    localObject1 = getSupportActionBar();
    if (localObject1 != null)
    {
      localObject2 = null;
      ((ActionBar)localObject1).setElevation(0.0F);
    }
    i = R.id.btnContinue;
    localObject1 = (Button)a(i);
    Object localObject2 = this;
    localObject2 = (View.OnClickListener)this;
    ((Button)localObject1).setOnClickListener((View.OnClickListener)localObject2);
  }
  
  public final void e()
  {
    Object localObject = com.truecaller.credit.app.ui.withdrawloan.views.b.a.d;
    localObject = new com/truecaller/credit/app/ui/withdrawloan/views/b/a;
    ((com.truecaller.credit.app.ui.withdrawloan.views.b.a)localObject).<init>();
    localObject = (Fragment)localObject;
    o localo = getSupportFragmentManager().a();
    int i = R.id.container;
    String str = localObject.getClass().getSimpleName();
    localo.a(i, (Fragment)localObject, str).a(null).c();
  }
  
  public final void f()
  {
    Object localObject1 = getSupportFragmentManager();
    int i = R.id.container;
    localObject1 = ((j)localObject1).a(i);
    boolean bool1 = localObject1 instanceof com.truecaller.credit.app.ui.withdrawloan.views.b.a;
    Object localObject2;
    int j;
    Object localObject3;
    int k;
    boolean bool2;
    if (bool1)
    {
      localObject1 = (com.truecaller.credit.app.ui.withdrawloan.views.b.a)localObject1;
      localObject2 = (com.truecaller.credit.app.ui.withdrawloan.views.c.a.a)((com.truecaller.credit.app.ui.withdrawloan.views.b.a)localObject1).a();
      j = R.id.textAmount;
      localObject3 = (AppCompatEditText)((com.truecaller.credit.app.ui.withdrawloan.views.b.a)localObject1).a(j);
      k.a(localObject3, "textAmount");
      localObject3 = com.truecaller.credit.app.ui.assist.a.b(String.valueOf(((AppCompatEditText)localObject3).getText()));
      k = R.id.checkEmiContainer;
      localObject1 = (FrameLayout)((com.truecaller.credit.app.ui.withdrawloan.views.b.a)localObject1).a(k);
      k.a(localObject1, "checkEmiContainer");
      bool2 = t.d((View)localObject1);
      ((com.truecaller.credit.app.ui.withdrawloan.views.c.a.a)localObject2).a((String)localObject3, bool2);
      return;
    }
    bool1 = localObject1 instanceof c;
    if (bool1)
    {
      localObject1 = (c)localObject1;
      localObject2 = (c.a)((c)localObject1).a();
      j = R.id.textLoanDescription;
      localObject3 = (TextInputEditText)((c)localObject1).a(j);
      k.a(localObject3, "textLoanDescription");
      localObject3 = String.valueOf(((TextInputEditText)localObject3).getText());
      Object localObject4 = ((c)localObject1).getArguments();
      if (localObject4 != null)
      {
        localObject5 = "tenure";
        localObject4 = ((Bundle)localObject4).getString((String)localObject5);
      }
      else
      {
        k = 0;
        localObject4 = null;
      }
      int m = R.id.textCategoryDescription;
      Object localObject5 = (TextInputEditText)((c)localObject1).a(m);
      k.a(localObject5, "textCategoryDescription");
      localObject5 = String.valueOf(((TextInputEditText)localObject5).getText());
      int n = R.id.containerCategoryDescription;
      localObject1 = (TextInputLayout)((c)localObject1).a(n);
      String str = "containerCategoryDescription";
      k.a(localObject1, str);
      localObject1 = (View)localObject1;
      bool2 = t.d((View)localObject1);
      ((c.a)localObject2).a((String)localObject3, (String)localObject4, (String)localObject5, bool2);
    }
  }
  
  public final void g()
  {
    int i = R.id.btnContinue;
    Button localButton = (Button)a(i);
    k.a(localButton, "btnContinue");
    localButton.setEnabled(true);
  }
  
  public final void h()
  {
    int i = R.id.btnContinue;
    Button localButton = (Button)a(i);
    k.a(localButton, "btnContinue");
    localButton.setEnabled(false);
  }
  
  public final void onBackPressed()
  {
    j localj = getSupportFragmentManager();
    String str = "supportFragmentManager";
    k.a(localj, str);
    int i = localj.e();
    int j = 1;
    if (i != j)
    {
      super.onBackPressed();
      return;
    }
    finish();
  }
  
  public final void onClick(View paramView)
  {
    int i = R.id.btnContinue;
    Button localButton = (Button)a(i);
    boolean bool = k.a(paramView, localButton);
    if (bool)
    {
      paramView = (e.a)a();
      paramView.a();
    }
  }
  
  public final boolean onCreateOptionsMenu(Menu paramMenu)
  {
    super.onCreateOptionsMenu(paramMenu);
    MenuInflater localMenuInflater = getMenuInflater();
    int i = R.menu.menu_credit;
    localMenuInflater.inflate(i, paramMenu);
    return true;
  }
  
  public final boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    super.onOptionsItemSelected(paramMenuItem);
    int i;
    if (paramMenuItem != null)
    {
      i = paramMenuItem.getItemId();
      paramMenuItem = Integer.valueOf(i);
    }
    else
    {
      i = 0;
      paramMenuItem = null;
    }
    if (paramMenuItem != null)
    {
      j = paramMenuItem.intValue();
      int k = 16908332;
      if (j == k)
      {
        onBackPressed();
        break label94;
      }
    }
    int j = R.id.info;
    if (paramMenuItem != null)
    {
      i = paramMenuItem.intValue();
      if (i == j)
      {
        paramMenuItem = (e.a)a();
        paramMenuItem.b();
      }
    }
    label94:
    return true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.withdrawloan.views.activities.WithdrawLoanActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */