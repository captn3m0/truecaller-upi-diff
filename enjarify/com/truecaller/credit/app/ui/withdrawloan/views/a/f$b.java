package com.truecaller.credit.app.ui.withdrawloan.views.a;

import android.view.View;
import android.view.View.OnClickListener;
import c.g.b.k;
import c.u;
import com.truecaller.credit.domain.interactors.withdrawloan.models.LoanCategory;

final class f$b
  implements View.OnClickListener
{
  f$b(f paramf, j paramj) {}
  
  public final void onClick(View paramView)
  {
    Object localObject1 = f.a(a);
    Object localObject2 = b;
    int i = ((j)localObject2).getAdapterPosition();
    ((g)localObject1).a(i);
    localObject1 = f.b(a);
    if (localObject1 != null)
    {
      localObject2 = "it";
      k.a(paramView, (String)localObject2);
      paramView = paramView.getTag();
      if (paramView != null)
      {
        paramView = (LoanCategory)paramView;
        ((f.a)localObject1).a(paramView);
        return;
      }
      paramView = new c/u;
      paramView.<init>("null cannot be cast to non-null type com.truecaller.credit.domain.interactors.withdrawloan.models.LoanCategory");
      throw paramView;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.withdrawloan.views.a.f.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */