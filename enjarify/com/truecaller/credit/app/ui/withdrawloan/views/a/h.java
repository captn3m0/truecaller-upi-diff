package com.truecaller.credit.app.ui.withdrawloan.views.a;

import android.support.v7.widget.AppCompatImageView;
import android.widget.ImageView;
import android.widget.TextView;
import c.g.b.k;
import com.d.b.ab;
import com.d.b.ai;
import com.d.b.w;
import com.truecaller.common.h.aq.d;
import com.truecaller.credit.R.drawable;
import com.truecaller.credit.R.id;
import com.truecaller.credit.domain.interactors.withdrawloan.models.LoanCategory;

public final class h
  implements g
{
  private int a;
  private final w b;
  
  public h(w paramw)
  {
    b = paramw;
    a = -1;
  }
  
  public final void a(int paramInt)
  {
    a = paramInt;
  }
  
  public final void a(j paramj, LoanCategory paramLoanCategory)
  {
    k.b(paramj, "loanCategoryItemView");
    k.b(paramLoanCategory, "loanCategory");
    Object localObject1 = b;
    k.b(localObject1, "picasso");
    a = ((w)localObject1);
    localObject1 = paramLoanCategory.getName();
    k.b(localObject1, "title");
    int i = R.id.textCategoryName;
    Object localObject2 = (TextView)paramj.a(i);
    String str = "textCategoryName";
    k.a(localObject2, str);
    localObject1 = (CharSequence)localObject1;
    ((TextView)localObject2).setText((CharSequence)localObject1);
    paramLoanCategory = paramLoanCategory.getIcon();
    k.b(paramLoanCategory, "url");
    localObject1 = a;
    if (localObject1 == null)
    {
      localObject2 = "picasso";
      k.a((String)localObject2);
    }
    paramLoanCategory = ((w)localObject1).a(paramLoanCategory);
    int j = R.drawable.ic_credit_category_place_holder_round;
    paramLoanCategory = paramLoanCategory.a(j);
    localObject1 = (ai)aq.d.b();
    paramLoanCategory = paramLoanCategory.a((ai)localObject1);
    j = R.id.imageCategory;
    localObject1 = (AppCompatImageView)paramj.a(j);
    i = 0;
    localObject2 = null;
    paramLoanCategory.a((ImageView)localObject1, null);
    int k = a;
    j = -1;
    if (k != j)
    {
      j = R.drawable.ic_tick_active_large;
      i = paramj.getAdapterPosition();
      if (i == k)
      {
        k = R.id.imageCategory;
        paramj = (AppCompatImageView)paramj.a(k);
        paramj.setImageResource(j);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.withdrawloan.views.a.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */