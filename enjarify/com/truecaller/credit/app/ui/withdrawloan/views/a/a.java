package com.truecaller.credit.app.ui.withdrawloan.views.a;

import android.content.Context;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import c.g.b.k;
import com.truecaller.credit.domain.interactors.withdrawloan.models.EmiTypes;
import java.util.List;

public final class a
  extends RecyclerView.Adapter
{
  private final LayoutInflater a;
  private EmiTypes b;
  private final b c;
  private final a.a d;
  
  public a(Context paramContext, EmiTypes paramEmiTypes, b paramb, a.a parama)
  {
    b = paramEmiTypes;
    c = paramb;
    d = parama;
    paramContext = LayoutInflater.from(paramContext);
    k.a(paramContext, "LayoutInflater.from(context)");
    a = paramContext;
  }
  
  public final int getItemCount()
  {
    Object localObject = b;
    if (localObject != null)
    {
      localObject = ((EmiTypes)localObject).getEmis();
      if (localObject != null) {
        return ((List)localObject).size();
      }
    }
    return 0;
  }
  
  public final long getItemId(int paramInt)
  {
    return paramInt;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.withdrawloan.views.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */