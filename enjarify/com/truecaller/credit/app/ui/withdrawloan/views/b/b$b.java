package com.truecaller.credit.app.ui.withdrawloan.views.b;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import c.g.b.k;
import com.truecaller.credit.app.ui.assist.LoanConfirmationStatus;
import com.truecaller.credit.app.ui.withdrawloan.views.c.b.a;

public final class b$b
  extends BroadcastReceiver
{
  b$b(b paramb) {}
  
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    String str = "context";
    k.b(paramContext, str);
    k.b(paramIntent, "intent");
    paramContext = paramIntent.getExtras();
    if (paramContext != null)
    {
      paramIntent = a.b;
      if (paramIntent == null)
      {
        str = "presenter";
        k.a(str);
      }
      paramContext = (LoanConfirmationStatus)paramContext.getParcelable("loan_status");
      paramIntent.a(paramContext);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.withdrawloan.views.b.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */