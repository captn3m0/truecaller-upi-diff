package com.truecaller.credit.app.ui.withdrawloan.b;

import c.d.a.a;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.credit.app.ui.assist.LoanConfirmationStatus;
import com.truecaller.credit.app.ui.withdrawloan.views.c.c.b;
import com.truecaller.credit.data.Failure;
import com.truecaller.credit.data.Result;
import com.truecaller.credit.data.Success;
import com.truecaller.credit.domain.interactors.withdrawloan.models.LoanDetails;
import com.truecaller.credit.domain.interactors.withdrawloan.models.LoanDisplayData;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

final class e$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag g;
  
  e$a(e parame, String paramString1, String paramString2, String paramString3, String paramString4, c.d.c paramc)
  {
    super(2, paramc);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/credit/app/ui/withdrawloan/b/e$a;
    e locale = b;
    String str1 = c;
    String str2 = d;
    String str3 = e;
    String str4 = f;
    locala.<init>(locale, str1, str2, str3, str4, paramc);
    paramObject = (ag)paramObject;
    g = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    Object localObject2;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label370;
      }
      paramObject = e.a(b);
      if (paramObject != null) {
        ((c.b)paramObject).j();
      }
      paramObject = e.b(b);
      localObject2 = new com/truecaller/credit/app/ui/withdrawloan/b/e$a$a;
      ((e.a.a)localObject2).<init>(this, null);
      localObject2 = (m)localObject2;
      int j = 1;
      a = j;
      paramObject = g.a((f)paramObject, (m)localObject2, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (Result)paramObject;
    boolean bool2 = paramObject instanceof Success;
    if (bool2)
    {
      e.e(b).a();
      paramObject = (Success)paramObject;
      localObject1 = ((LoanDetails)((Success)paramObject).getData()).getDisplay_configuration();
      if (localObject1 != null)
      {
        localObject2 = e.a(b);
        if (localObject2 != null)
        {
          LoanConfirmationStatus localLoanConfirmationStatus = new com/truecaller/credit/app/ui/assist/LoanConfirmationStatus;
          String str1 = e.a(((LoanDetails)((Success)paramObject).getData()).getStatus());
          String str2 = ((LoanDisplayData)localObject1).getTitle();
          String str3 = ((LoanDisplayData)localObject1).getDescription();
          String str4 = ((LoanDetails)((Success)paramObject).getData()).getStatus();
          int k = e.b(((LoanDetails)((Success)paramObject).getData()).getStatus());
          localLoanConfirmationStatus.<init>(str1, str2, str3, str4, k);
          localObject1 = "loan_confirmation";
          ((c.b)localObject2).a(localLoanConfirmationStatus, (String)localObject1);
          ((c.b)localObject2).k();
        }
      }
      localObject1 = b;
      paramObject = ((LoanDetails)((Success)paramObject).getData()).getStatus();
      e.a((e)localObject1, (String)paramObject);
    }
    else
    {
      boolean bool3 = paramObject instanceof Failure;
      if (bool3)
      {
        paramObject = e.a(b);
        if (paramObject != null) {
          ((c.b)paramObject).k();
        }
        paramObject = b;
        localObject1 = "failure";
        e.a((e)paramObject, (String)localObject1);
      }
    }
    return x.a;
    label370:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.withdrawloan.b.e.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */