package com.truecaller.credit.app.ui.withdrawloan.b;

import c.d.f;
import c.g.a.m;
import c.g.b.k;
import com.truecaller.ba;
import com.truecaller.credit.app.a.b;
import com.truecaller.credit.app.ui.loanhistory.a.c;
import com.truecaller.credit.app.ui.withdrawloan.views.c.c.a;
import com.truecaller.credit.app.ui.withdrawloan.views.c.c.b;
import com.truecaller.credit.data.repository.CreditRepository;
import com.truecaller.utils.n;

public final class e
  extends ba
  implements c.a
{
  private String c;
  private String d;
  private String e;
  private boolean f;
  private final f g;
  private final f h;
  private final n i;
  private final CreditRepository j;
  private final c k;
  private final b l;
  
  public e(f paramf1, f paramf2, n paramn, CreditRepository paramCreditRepository, c paramc, b paramb)
  {
    super(paramf1);
    g = paramf1;
    h = paramf2;
    i = paramn;
    j = paramCreditRepository;
    k = paramc;
    l = paramb;
  }
  
  private final void a()
  {
    c.b localb = (c.b)b;
    if (localb == null) {
      return;
    }
    boolean bool = e();
    if (bool)
    {
      localb.i();
      return;
    }
    localb.h();
  }
  
  private final boolean e()
  {
    CharSequence localCharSequence = (CharSequence)e;
    boolean bool1 = true;
    if (localCharSequence != null)
    {
      m = localCharSequence.length();
      if (m != 0)
      {
        m = 0;
        localCharSequence = null;
        break label37;
      }
    }
    int m = 1;
    label37:
    if (m == 0)
    {
      localCharSequence = (CharSequence)c;
      if (localCharSequence != null)
      {
        m = localCharSequence.length();
        if (m != 0)
        {
          m = 0;
          localCharSequence = null;
          break label76;
        }
      }
      m = 1;
      label76:
      if (m == 0)
      {
        boolean bool2 = f;
        if (bool2)
        {
          localCharSequence = (CharSequence)d;
          if (localCharSequence != null)
          {
            n = localCharSequence.length();
            if (n != 0)
            {
              n = 0;
              localCharSequence = null;
              break label124;
            }
          }
          int n = 1;
          label124:
          if (n != 0) {
            return false;
          }
        }
        return bool1;
      }
    }
    return false;
  }
  
  public final void a(String paramString1, String paramString2)
  {
    d = paramString2;
    e = paramString1;
    a();
  }
  
  public final void a(String paramString1, String paramString2, String paramString3, boolean paramBoolean)
  {
    Object localObject = "description";
    k.b(paramString1, (String)localObject);
    boolean bool = e();
    if (bool)
    {
      localObject = (c.b)b;
      if (localObject != null)
      {
        String str1 = ((c.b)localObject).l();
        if (str1 != null)
        {
          if (paramString2 == null) {
            return;
          }
          bool = false;
          localObject = null;
          String str2;
          if (paramBoolean) {
            str2 = paramString3;
          } else {
            str2 = null;
          }
          paramString3 = new com/truecaller/credit/app/ui/withdrawloan/b/e$a;
          paramString3.<init>(this, paramString1, paramString2, str1, str2, null);
          paramString3 = (m)paramString3;
          int m = 3;
          kotlinx.coroutines.e.b(this, null, paramString3, m);
          return;
        }
      }
      return;
    }
  }
  
  public final void b(String paramString1, String paramString2)
  {
    k.b(paramString1, "categoryId");
    String str = "categoryName";
    k.b(paramString2, str);
    c = paramString1;
    paramString1 = paramString2.toLowerCase();
    k.a(paramString1, "(this as java.lang.String).toLowerCase()");
    paramString2 = "other";
    boolean bool1 = k.a(paramString1, paramString2);
    f = bool1;
    paramString1 = (c.b)b;
    if (paramString1 != null)
    {
      boolean bool2 = f;
      paramString1.a(bool2);
    }
    a();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.withdrawloan.b.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */