package com.truecaller.credit.app.ui.withdrawloan.b;

import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.credit.R.string;
import com.truecaller.credit.app.ui.withdrawloan.views.c.a.b;
import com.truecaller.credit.data.Failure;
import com.truecaller.credit.data.Result;
import com.truecaller.credit.data.Success;
import com.truecaller.credit.domain.interactors.withdrawloan.models.EmiTypes;
import com.truecaller.utils.n;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

final class a$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  a$a(a parama, String paramString, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/credit/app/ui/withdrawloan/b/a$a;
    a locala1 = b;
    String str = c;
    locala.<init>(locala1, str, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = a;
    int j = 1;
    int m;
    Object localObject3;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label446;
      }
      paramObject = a.a(b);
      if (paramObject != null)
      {
        ((a.b)paramObject).k();
        ((a.b)paramObject).h();
      }
      a.b(b);
      paramObject = a.c(b);
      localObject2 = new com/truecaller/credit/app/ui/withdrawloan/b/a$a$a;
      m = 0;
      localObject3 = null;
      ((a.a.a)localObject2).<init>(this, null);
      localObject2 = (m)localObject2;
      a = j;
      paramObject = g.a((f)paramObject, (m)localObject2, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (Result)paramObject;
    boolean bool2 = paramObject instanceof Success;
    boolean bool1 = false;
    Object localObject2 = null;
    int k;
    if (bool2)
    {
      localObject1 = a.a(b);
      if (localObject1 != null)
      {
        paramObject = (Success)paramObject;
        localObject3 = (EmiTypes)((Success)paramObject).getData();
        ((a.b)localObject1).a((EmiTypes)localObject3);
        paramObject = ((EmiTypes)((Success)paramObject).getData()).getDescription();
        ((a.b)localObject1).b((String)paramObject);
        ((a.b)localObject1).a(j);
        ((a.b)localObject1).i();
        ((a.b)localObject1).m();
        paramObject = a.e(b);
        k = R.string.credit_button_continue;
        localObject2 = new Object[0];
        paramObject = ((n)paramObject).a(k, (Object[])localObject2);
        localObject2 = "resourceProvider.getStri…g.credit_button_continue)";
        c.g.b.k.a(paramObject, (String)localObject2);
        ((a.b)localObject1).f((String)paramObject);
      }
    }
    else
    {
      bool2 = paramObject instanceof Failure;
      if (bool2)
      {
        localObject1 = a.a(b);
        if (localObject1 != null)
        {
          ((a.b)localObject1).i();
          paramObject = (Failure)paramObject;
          localObject3 = (CharSequence)((Failure)paramObject).getMessage();
          if (localObject3 != null)
          {
            m = ((CharSequence)localObject3).length();
            if (m != 0) {
              k = 0;
            }
          }
          if (k == 0)
          {
            paramObject = ((Failure)paramObject).getMessage();
            ((a.b)localObject1).e((String)paramObject);
          }
          else
          {
            paramObject = a.e(b);
            k = R.string.server_error_message;
            localObject2 = new Object[0];
            paramObject = ((n)paramObject).a(k, (Object[])localObject2);
            localObject2 = "resourceProvider.getStri…ing.server_error_message)";
            c.g.b.k.a(paramObject, (String)localObject2);
            ((a.b)localObject1).e((String)paramObject);
          }
        }
      }
    }
    return x.a;
    label446:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.withdrawloan.b.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */