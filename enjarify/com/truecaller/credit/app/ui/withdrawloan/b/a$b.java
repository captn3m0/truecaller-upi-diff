package com.truecaller.credit.app.ui.withdrawloan.b;

import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.credit.R.string;
import com.truecaller.credit.data.Failure;
import com.truecaller.credit.data.Result;
import com.truecaller.credit.data.Success;
import com.truecaller.credit.domain.interactors.withdrawloan.models.CreditLineDetails;
import com.truecaller.utils.n;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

final class a$b
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  a$b(a parama, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/credit/app/ui/withdrawloan/b/a$b;
    a locala = b;
    localb.<init>(locala, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return localb;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = a;
    int j = 1;
    int k;
    Object localObject3;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label465;
      }
      paramObject = a.c(b);
      localObject2 = new com/truecaller/credit/app/ui/withdrawloan/b/a$b$a;
      k = 0;
      localObject3 = null;
      ((a.b.a)localObject2).<init>(this, null);
      localObject2 = (m)localObject2;
      a = j;
      paramObject = g.a((f)paramObject, (m)localObject2, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (Result)paramObject;
    boolean bool2 = paramObject instanceof Success;
    boolean bool1 = false;
    Object localObject2 = null;
    if (bool2)
    {
      localObject1 = b;
      paramObject = (Success)paramObject;
      localObject3 = com.truecaller.credit.app.ui.assist.a.b(((CreditLineDetails)((Success)paramObject).getData()).getAvailable_amount());
      a.a((a)localObject1, (String)localObject3);
      localObject1 = b;
      localObject3 = com.truecaller.credit.app.ui.assist.a.b(((CreditLineDetails)((Success)paramObject).getData()).getMinimum_amount());
      a.b((a)localObject1, (String)localObject3);
      localObject1 = a.a(b);
      if (localObject1 != null)
      {
        paramObject = ((CreditLineDetails)((Success)paramObject).getData()).getTitle();
        ((com.truecaller.credit.app.ui.withdrawloan.views.c.a.b)localObject1).c((String)paramObject);
        paramObject = a.e(b);
        k = R.string.credit_tip_amount_entry;
        int m = 2;
        Object[] arrayOfObject = new Object[m];
        String str = com.truecaller.credit.app.ui.assist.a.a(a.g(b));
        arrayOfObject[0] = str;
        localObject2 = com.truecaller.credit.app.ui.assist.a.a(a.f(b));
        arrayOfObject[j] = localObject2;
        paramObject = ((n)paramObject).a(k, arrayOfObject);
        localObject2 = "resourceProvider.getStri…                        )";
        c.g.b.k.a(paramObject, (String)localObject2);
        ((com.truecaller.credit.app.ui.withdrawloan.views.c.a.b)localObject1).d((String)paramObject);
      }
    }
    else
    {
      bool2 = paramObject instanceof Failure;
      if (bool2)
      {
        localObject1 = a.a(b);
        if (localObject1 != null)
        {
          paramObject = (Failure)paramObject;
          localObject3 = (CharSequence)((Failure)paramObject).getMessage();
          if (localObject3 != null)
          {
            k = ((CharSequence)localObject3).length();
            if (k != 0) {
              j = 0;
            }
          }
          if (j == 0)
          {
            paramObject = ((Failure)paramObject).getMessage();
            ((com.truecaller.credit.app.ui.withdrawloan.views.c.a.b)localObject1).e((String)paramObject);
          }
          else
          {
            paramObject = a.e(b);
            j = R.string.server_error_message;
            localObject2 = new Object[0];
            paramObject = ((n)paramObject).a(j, (Object[])localObject2);
            localObject2 = "resourceProvider.getStri…ing.server_error_message)";
            c.g.b.k.a(paramObject, (String)localObject2);
            ((com.truecaller.credit.app.ui.withdrawloan.views.c.a.b)localObject1).e((String)paramObject);
          }
        }
      }
    }
    return x.a;
    label465:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.withdrawloan.b.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */