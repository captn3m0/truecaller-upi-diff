package com.truecaller.credit.app.ui.withdrawloan.b;

import c.d.f;
import c.g.b.k;
import com.truecaller.ba;
import com.truecaller.credit.R.string;
import com.truecaller.credit.app.a.b;
import com.truecaller.credit.app.ui.withdrawloan.views.c.a.b;
import com.truecaller.credit.data.repository.CreditRepository;
import com.truecaller.credit.domain.interactors.withdrawloan.models.Emi;
import com.truecaller.utils.n;
import kotlinx.coroutines.e;

public final class a
  extends ba
  implements com.truecaller.credit.app.ui.withdrawloan.views.c.a.a
{
  private String c;
  private String d;
  private Emi e;
  private int f;
  private int g;
  private final f h;
  private final f i;
  private final n j;
  private final CreditRepository k;
  private final b l;
  
  public a(f paramf1, f paramf2, n paramn, CreditRepository paramCreditRepository, b paramb)
  {
    super(paramf1);
    h = paramf1;
    i = paramf2;
    j = paramn;
    k = paramCreditRepository;
    l = paramb;
    c = "10000";
    d = "75000";
    int m = 1;
    f = m;
    g = m;
  }
  
  private final void a()
  {
    a.b localb = (a.b)b;
    if (localb != null)
    {
      Object localObject = j;
      int m = R.string.credit_title_enter_amount;
      Object[] arrayOfObject1 = new Object[0];
      localObject = ((n)localObject).a(m, arrayOfObject1);
      k.a(localObject, "resourceProvider.getStri…redit_title_enter_amount)");
      localb.a((String)localObject);
      localb.m();
      localb.j();
      localb.a(false);
      localObject = j;
      m = R.string.credit_button_check_emi;
      Object[] arrayOfObject2 = new Object[0];
      localObject = ((n)localObject).a(m, arrayOfObject2);
      k.a(localObject, "resourceProvider.getStri….credit_button_check_emi)");
      localb.f((String)localObject);
      return;
    }
  }
  
  public final void a(Emi paramEmi)
  {
    k.b(paramEmi, "emiData");
    a.b localb = (a.b)b;
    if (localb != null) {
      localb.n();
    }
    e = paramEmi;
  }
  
  public final void a(String paramString, int paramInt)
  {
    k.b(paramString, "text");
    int m = g;
    f = m;
    m = paramString.length();
    g = m;
    Object localObject1 = (CharSequence)com.truecaller.credit.app.ui.assist.a.b(paramString);
    boolean bool = c.n.m.a((CharSequence)localObject1);
    int i1 = 1;
    if (!bool)
    {
      localObject1 = c;
      int n = Integer.parseInt((String)localObject1);
      Object localObject2 = d;
      int i2 = Integer.parseInt((String)localObject2);
      k.b(paramString, "receiver$0");
      String str = com.truecaller.credit.app.ui.assist.a.b(paramString);
      int i3 = Integer.parseInt(str);
      if ((n <= i3) && (i2 >= i3))
      {
        n = 1;
      }
      else
      {
        n = 0;
        localObject1 = null;
      }
      if (n != 0)
      {
        localObject1 = (a.b)b;
        if (localObject1 != null)
        {
          ((a.b)localObject1).n();
          localObject2 = j;
          i3 = R.string.credit_title_know_emi;
          Object[] arrayOfObject = new Object[0];
          localObject2 = ((n)localObject2).a(i3, arrayOfObject);
          str = "resourceProvider.getStri…ng.credit_title_know_emi)";
          k.a(localObject2, str);
          ((a.b)localObject1).a((String)localObject2);
        }
      }
      else
      {
        a();
      }
      localObject1 = (a.b)b;
      if (localObject1 != null)
      {
        localObject2 = j;
        i3 = R.string.credit_rs_prefix;
        Object localObject3 = new Object[i1];
        paramString = com.truecaller.credit.app.ui.assist.a.a(paramString);
        localObject3[0] = paramString;
        paramString = ((n)localObject2).a(i3, (Object[])localObject3);
        localObject3 = "resourceProvider.getStri…tring()\n                )";
        k.a(paramString, (String)localObject3);
        i1 = f;
        ((a.b)localObject1).a(paramString, paramInt, i1);
      }
      return;
    }
    int i4 = paramString.length();
    if (i4 == i1)
    {
      paramString = (a.b)b;
      if (paramString != null) {
        paramString.l();
      }
      return;
    }
    a();
  }
  
  public final void a(String paramString, boolean paramBoolean)
  {
    Object localObject1 = "amount";
    k.b(paramString, (String)localObject1);
    if (!paramBoolean)
    {
      localObject2 = new com/truecaller/credit/app/ui/withdrawloan/b/a$a;
      ((a.a)localObject2).<init>(this, paramString, null);
      localObject2 = (c.g.a.m)localObject2;
      e.b(this, null, (c.g.a.m)localObject2, 3);
      return;
    }
    Object localObject2 = e;
    if (localObject2 != null)
    {
      localObject1 = (a.b)b;
      if (localObject1 != null)
      {
        localObject2 = ((Emi)localObject2).getTenure();
        ((a.b)localObject1).a((String)localObject2, paramString);
        return;
      }
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.withdrawloan.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */