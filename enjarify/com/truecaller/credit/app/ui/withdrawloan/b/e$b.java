package com.truecaller.credit.app.ui.withdrawloan.b;

import c.d.a.a;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.credit.R.string;
import com.truecaller.credit.app.ui.withdrawloan.views.c.c.b;
import com.truecaller.credit.data.Failure;
import com.truecaller.credit.data.Result;
import com.truecaller.credit.data.Success;
import com.truecaller.credit.domain.interactors.withdrawloan.models.LoanCategories;
import com.truecaller.utils.n;
import java.util.List;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

final class e$b
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  e$b(e parame, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/credit/app/ui/withdrawloan/b/e$b;
    e locale = b;
    localb.<init>(locale, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return localb;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    Object localObject2;
    Object[] arrayOfObject;
    int k;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label286;
      }
      paramObject = e.a(b);
      if (paramObject != null) {
        ((c.b)paramObject).j();
      }
      paramObject = e.b(b);
      localObject2 = new com/truecaller/credit/app/ui/withdrawloan/b/e$b$a;
      arrayOfObject = null;
      ((e.b.a)localObject2).<init>(this, null);
      localObject2 = (m)localObject2;
      k = 1;
      a = k;
      paramObject = g.a((f)paramObject, (m)localObject2, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (Result)paramObject;
    boolean bool2 = paramObject instanceof Success;
    if (bool2)
    {
      localObject1 = e.a(b);
      if (localObject1 != null)
      {
        paramObject = ((LoanCategories)((Success)paramObject).getData()).getCategories();
        ((c.b)localObject1).a((List)paramObject);
        ((c.b)localObject1).k();
      }
    }
    else
    {
      boolean bool3 = paramObject instanceof Failure;
      if (bool3)
      {
        paramObject = e.a(b);
        if (paramObject != null)
        {
          localObject1 = e.f(b);
          int j = R.string.server_error_message;
          k = 0;
          arrayOfObject = new Object[0];
          localObject1 = ((n)localObject1).a(j, arrayOfObject);
          localObject2 = "resourceProvider.getStri…ing.server_error_message)";
          c.g.b.k.a(localObject1, (String)localObject2);
          ((c.b)paramObject).c((String)localObject1);
          ((c.b)paramObject).k();
        }
      }
    }
    return x.a;
    label286:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.withdrawloan.b.e.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */