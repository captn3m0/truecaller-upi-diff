package com.truecaller.credit.app.ui.assist;

public final class PoaType
  extends CreditDocumentType
{
  private PoaType(int paramInt1, int paramInt2, int paramInt3, int paramInt4, String paramString1, String paramString2, String paramString3, int paramInt5)
  {
    super(paramInt1, paramString2, paramInt2, paramInt3, paramInt4, paramString1, paramString3, paramInt5);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.assist.PoaType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */