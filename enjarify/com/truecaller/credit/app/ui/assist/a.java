package com.truecaller.credit.app.ui.assist;

import android.widget.EditText;
import c.g.b.k;
import c.n.m;
import c.u;
import com.google.gson.l;
import com.google.gson.n;
import java.text.DecimalFormat;

public final class a
{
  public static final String a(int paramInt)
  {
    DecimalFormat localDecimalFormat = new java/text/DecimalFormat;
    localDecimalFormat.<init>("#,##,###");
    Object localObject = Integer.valueOf(paramInt);
    localObject = localDecimalFormat.format(localObject);
    k.a(localObject, "formatter.format(this)");
    return (String)localObject;
  }
  
  public static final String a(EditText paramEditText)
  {
    k.b(paramEditText, "receiver$0");
    return paramEditText.getText().toString();
  }
  
  public static final String a(l paraml)
  {
    String str = "receiver$0";
    k.b(paraml, str);
    boolean bool = paraml instanceof n;
    if (bool) {
      return null;
    }
    return paraml.c();
  }
  
  public static final String a(String paramString)
  {
    k.b(paramString, "receiver$0");
    DecimalFormat localDecimalFormat = new java/text/DecimalFormat;
    localDecimalFormat.<init>("#,###,###");
    paramString = Integer.valueOf(Integer.parseInt(b(paramString)));
    paramString = localDecimalFormat.format(paramString);
    k.a(paramString, "formatter.format(this.getActualAmount().toInt())");
    return paramString;
  }
  
  public static final String b(String paramString)
  {
    k.b(paramString, "receiver$0");
    String str = m.a(paramString, ",", "");
    Object localObject = (CharSequence)"₹";
    str = m.a(str, (CharSequence)localObject);
    localObject = str;
    localObject = (CharSequence)str;
    CharSequence localCharSequence = (CharSequence)".";
    boolean bool = m.a((CharSequence)localObject, localCharSequence, false);
    if (bool)
    {
      paramString = (CharSequence)paramString;
      localObject = ".";
      int i = 6;
      int j = m.a(paramString, (String)localObject, 0, false, i);
      if (str != null)
      {
        paramString = str.substring(0, j);
        k.a(paramString, "(this as java.lang.Strin…ing(startIndex, endIndex)");
        return paramString;
      }
      paramString = new c/u;
      paramString.<init>("null cannot be cast to non-null type java.lang.String");
      throw paramString;
    }
    return str;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.assist.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */