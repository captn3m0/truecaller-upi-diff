package com.truecaller.credit.app.ui.assist;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;

public class CreditDocumentType
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  public final int a;
  public final String b;
  public final int c;
  public final int d;
  public final int e;
  public final String f;
  public final String g;
  public final int h;
  
  static
  {
    CreditDocumentType.a locala = new com/truecaller/credit/app/ui/assist/CreditDocumentType$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public CreditDocumentType(int paramInt1, String paramString1, int paramInt2, int paramInt3, int paramInt4, String paramString2, String paramString3, int paramInt5)
  {
    a = paramInt1;
    b = paramString1;
    c = paramInt2;
    d = paramInt3;
    e = paramInt4;
    f = paramString2;
    g = paramString3;
    h = paramInt5;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    paramInt = a;
    paramParcel.writeInt(paramInt);
    String str = b;
    paramParcel.writeString(str);
    paramInt = c;
    paramParcel.writeInt(paramInt);
    paramInt = d;
    paramParcel.writeInt(paramInt);
    paramInt = e;
    paramParcel.writeInt(paramInt);
    str = f;
    paramParcel.writeString(str);
    str = g;
    paramParcel.writeString(str);
    paramInt = h;
    paramParcel.writeInt(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.assist.CreditDocumentType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */