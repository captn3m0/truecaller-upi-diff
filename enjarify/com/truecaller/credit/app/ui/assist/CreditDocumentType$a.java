package com.truecaller.credit.app.ui.assist;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class CreditDocumentType$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    CreditDocumentType localCreditDocumentType = new com/truecaller/credit/app/ui/assist/CreditDocumentType;
    int i = paramParcel.readInt();
    String str1 = paramParcel.readString();
    int j = paramParcel.readInt();
    int k = paramParcel.readInt();
    int m = paramParcel.readInt();
    String str2 = paramParcel.readString();
    String str3 = paramParcel.readString();
    int n = paramParcel.readInt();
    localCreditDocumentType.<init>(i, str1, j, k, m, str2, str3, n);
    return localCreditDocumentType;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new CreditDocumentType[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.assist.CreditDocumentType.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */