package com.truecaller.credit.app.ui.assist;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class LoanConfirmationStatus$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    LoanConfirmationStatus localLoanConfirmationStatus = new com/truecaller/credit/app/ui/assist/LoanConfirmationStatus;
    String str1 = paramParcel.readString();
    String str2 = paramParcel.readString();
    String str3 = paramParcel.readString();
    String str4 = paramParcel.readString();
    int i = paramParcel.readInt();
    localLoanConfirmationStatus.<init>(str1, str2, str3, str4, i);
    return localLoanConfirmationStatus;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new LoanConfirmationStatus[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.assist.LoanConfirmationStatus.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */