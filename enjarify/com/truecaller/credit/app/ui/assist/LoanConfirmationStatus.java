package com.truecaller.credit.app.ui.assist;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;

public class LoanConfirmationStatus
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  public String a;
  public final String b;
  public final String c;
  public final String d;
  public int e;
  
  static
  {
    LoanConfirmationStatus.a locala = new com/truecaller/credit/app/ui/assist/LoanConfirmationStatus$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public LoanConfirmationStatus(String paramString1, String paramString2, String paramString3, String paramString4, int paramInt)
  {
    a = paramString1;
    b = paramString2;
    c = paramString3;
    d = paramString4;
    e = paramInt;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    String str = a;
    paramParcel.writeString(str);
    str = b;
    paramParcel.writeString(str);
    str = c;
    paramParcel.writeString(str);
    str = d;
    paramParcel.writeString(str);
    paramInt = e;
    paramParcel.writeInt(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.assist.LoanConfirmationStatus
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */