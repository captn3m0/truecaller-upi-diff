package com.truecaller.credit.app.ui.assist;

public final class QRScan
  extends CreditDocumentType
{
  public QRScan(int paramInt1, String paramString, int paramInt2)
  {
    super(paramInt1, null, paramInt2, 1, 1, paramString, paramString, 0, 130);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.assist.QRScan
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */