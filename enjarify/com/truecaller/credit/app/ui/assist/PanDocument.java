package com.truecaller.credit.app.ui.assist;

public final class PanDocument
  extends CreditDocumentType
{
  public PanDocument(int paramInt1, int paramInt2, String paramString, int paramInt3)
  {
    super(paramInt1, null, paramInt2, 3, 4, paramString, paramString, paramInt3, 2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.assist.PanDocument
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */