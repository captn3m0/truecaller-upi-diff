package com.truecaller.credit.app.ui.assist;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.b;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.credit.R.color;
import com.truecaller.credit.app.ui.faq.activities.CreditWebViewActivity;

public final class a$a
  extends ClickableSpan
{
  public a$a(TextView paramTextView, String paramString) {}
  
  public final void onClick(View paramView)
  {
    k.b(paramView, "view");
    paramView = new android/content/Intent;
    Context localContext = a.getContext();
    paramView.<init>(localContext, CreditWebViewActivity.class);
    String str = b;
    paramView.putExtra("url", str);
    a.getContext().startActivity(paramView);
  }
  
  public final void updateDrawState(TextPaint paramTextPaint)
  {
    k.b(paramTextPaint, "ds");
    paramTextPaint.setUnderlineText(false);
    Context localContext = a.getContext();
    int i = R.color.azure;
    int j = b.c(localContext, i);
    paramTextPaint.setColor(j);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.assist.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */