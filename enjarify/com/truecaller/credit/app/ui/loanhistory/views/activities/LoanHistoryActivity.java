package com.truecaller.credit.app.ui.loanhistory.views.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v4.app.j;
import android.support.v4.app.o;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import c.g.b.k;
import com.truecaller.credit.R.id;
import com.truecaller.credit.R.layout;
import com.truecaller.credit.R.menu;
import com.truecaller.credit.app.ui.faq.activities.CreditWebViewActivity;
import com.truecaller.credit.app.ui.loanhistory.b.a.a.a;
import com.truecaller.credit.app.ui.loanhistory.views.c.d.a;
import com.truecaller.credit.app.ui.loanhistory.views.c.d.b;
import com.truecaller.credit.i;
import com.truecaller.credit.i.a;
import java.util.HashMap;

public final class LoanHistoryActivity
  extends com.truecaller.credit.app.ui.b.b
  implements com.truecaller.credit.app.ui.loanhistory.views.c.b, d.b
{
  private HashMap b;
  
  public final View a(int paramInt)
  {
    Object localObject1 = b;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      b = ((HashMap)localObject1);
    }
    localObject1 = b;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = findViewById(paramInt);
      localObject2 = b;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a(Fragment paramFragment)
  {
    k.b(paramFragment, "fragment");
    o localo = getSupportFragmentManager().a();
    int i = R.id.containerLoanHistory;
    String str = paramFragment.getClass().getSimpleName();
    localo = localo.b(i, paramFragment, str);
    paramFragment = paramFragment.getClass().getSimpleName();
    localo.a(paramFragment).d();
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "url");
    Intent localIntent = new android/content/Intent;
    Object localObject = this;
    localObject = (Context)this;
    localIntent.<init>((Context)localObject, CreditWebViewActivity.class);
    localIntent.putExtra("url", paramString);
    startActivity(localIntent);
  }
  
  public final int b()
  {
    return R.layout.activity_loan_history;
  }
  
  public final void c()
  {
    a.a locala = com.truecaller.credit.app.ui.loanhistory.b.a.a.a();
    Object localObject = i.i;
    localObject = i.a.a();
    locala.a((com.truecaller.credit.app.c.a.a)localObject).a().a(this);
  }
  
  public final void d()
  {
    int i = R.id.toolbarLoanHistory;
    Object localObject = (Toolbar)a(i);
    setSupportActionBar((Toolbar)localObject);
    localObject = getSupportActionBar();
    if (localObject != null)
    {
      ((ActionBar)localObject).setElevation(0.0F);
      return;
    }
  }
  
  public final void e()
  {
    Object localObject = new com/truecaller/credit/app/ui/loanhistory/views/b/b;
    ((com.truecaller.credit.app.ui.loanhistory.views.b.b)localObject).<init>();
    localObject = (Fragment)localObject;
    o localo = getSupportFragmentManager().a();
    int i = R.id.containerLoanHistory;
    String str = localObject.getClass().getSimpleName();
    localo.a(i, (Fragment)localObject, str).c();
  }
  
  public final void onBackPressed()
  {
    Object localObject = getSupportFragmentManager();
    String str = "supportFragmentManager";
    k.a(localObject, str);
    int i = ((j)localObject).e();
    int j = 1;
    if (i != j)
    {
      super.onBackPressed();
      return;
    }
    localObject = getSupportFragmentManager();
    j = R.id.containerLoanHistory;
    localObject = ((j)localObject).a(j);
    boolean bool = localObject instanceof com.truecaller.credit.app.ui.loanhistory.views.b.a;
    if (bool)
    {
      getSupportFragmentManager().c();
      return;
    }
    finish();
  }
  
  public final boolean onCreateOptionsMenu(Menu paramMenu)
  {
    super.onCreateOptionsMenu(paramMenu);
    MenuInflater localMenuInflater = getMenuInflater();
    int i = R.menu.menu_credit;
    localMenuInflater.inflate(i, paramMenu);
    return true;
  }
  
  public final boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    super.onOptionsItemSelected(paramMenuItem);
    int i;
    if (paramMenuItem != null)
    {
      i = paramMenuItem.getItemId();
      paramMenuItem = Integer.valueOf(i);
    }
    else
    {
      i = 0;
      paramMenuItem = null;
    }
    if (paramMenuItem != null)
    {
      j = paramMenuItem.intValue();
      int k = 16908332;
      if (j == k)
      {
        onBackPressed();
        break label93;
      }
    }
    int j = R.id.info;
    if (paramMenuItem != null)
    {
      i = paramMenuItem.intValue();
      if (i == j)
      {
        paramMenuItem = (d.a)a();
        paramMenuItem.a();
      }
    }
    label93:
    return true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.loanhistory.views.activities.LoanHistoryActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */