package com.truecaller.credit.app.ui.loanhistory.views.a;

import dagger.a.d;
import javax.inject.Provider;

public final class j
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  
  private j(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
  }
  
  public static j a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    j localj = new com/truecaller/credit/app/ui/loanhistory/views/a/j;
    localj.<init>(paramProvider1, paramProvider2, paramProvider3);
    return localj;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.loanhistory.views.a.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */