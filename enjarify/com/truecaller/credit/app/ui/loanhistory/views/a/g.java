package com.truecaller.credit.app.ui.loanhistory.views.a;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v4.app.n;
import com.truecaller.credit.R.string;
import com.truecaller.credit.app.ui.loanhistory.views.b.c;

public final class g
  extends n
{
  private final Integer[] a;
  private final Context b;
  
  public g(j paramj, Context paramContext)
  {
    super(paramj);
    b = paramContext;
    paramj = new Integer[2];
    paramContext = Integer.valueOf(R.string.credit_title_active_loans);
    paramj[0] = paramContext;
    paramContext = Integer.valueOf(R.string.credit_title_inactive_loans);
    paramj[1] = paramContext;
    a = paramj;
  }
  
  public final Fragment a(int paramInt)
  {
    Object localObject1 = c.d;
    switch (paramInt)
    {
    default: 
      localObject2 = new java/lang/IllegalStateException;
      ((IllegalStateException)localObject2).<init>("Invalid position");
      throw ((Throwable)localObject2);
    case 1: 
      paramInt = 1;
      break;
    case 0: 
      paramInt = 0;
      localObject2 = null;
    }
    localObject1 = new android/os/Bundle;
    ((Bundle)localObject1).<init>();
    ((Bundle)localObject1).putInt("type_filter", paramInt);
    Object localObject2 = new com/truecaller/credit/app/ui/loanhistory/views/b/c;
    ((c)localObject2).<init>();
    ((c)localObject2).setArguments((Bundle)localObject1);
    return (Fragment)localObject2;
  }
  
  public final int getCount()
  {
    return a.length;
  }
  
  public final CharSequence getPageTitle(int paramInt)
  {
    Context localContext = b;
    paramInt = a[paramInt].intValue();
    return (CharSequence)localContext.getString(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.loanhistory.views.a.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */