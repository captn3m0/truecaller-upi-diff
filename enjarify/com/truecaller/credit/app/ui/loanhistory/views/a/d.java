package com.truecaller.credit.app.ui.loanhistory.views.a;

import javax.inject.Provider;

public final class d
  implements dagger.a.d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  
  private d(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
  }
  
  public static d a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    d locald = new com/truecaller/credit/app/ui/loanhistory/views/a/d;
    locald.<init>(paramProvider1, paramProvider2, paramProvider3);
    return locald;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.loanhistory.views.a.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */