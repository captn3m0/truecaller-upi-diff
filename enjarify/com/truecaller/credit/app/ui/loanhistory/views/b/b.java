package com.truecaller.credit.app.ui.loanhistory.views.b;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.f;
import android.support.v4.view.o;
import android.view.View;
import c.g.b.k;
import com.truecaller.credit.R.id;
import com.truecaller.credit.R.layout;
import com.truecaller.credit.app.ui.b.c;
import com.truecaller.credit.app.ui.loanhistory.b.a.a.a;
import com.truecaller.credit.app.ui.loanhistory.views.a.g;
import com.truecaller.credit.app.ui.loanhistory.views.c.c.a;
import com.truecaller.credit.app.ui.loanhistory.views.c.c.b;
import com.truecaller.credit.i;
import com.truecaller.credit.i.a;
import java.util.HashMap;

public final class b
  extends c
  implements ViewPager.f, c.b
{
  private g c;
  private HashMap d;
  
  public final View a(int paramInt)
  {
    Object localObject1 = d;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      d = ((HashMap)localObject1);
    }
    localObject1 = d;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = d;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void b()
  {
    a.a locala = com.truecaller.credit.app.ui.loanhistory.b.a.a.a();
    Object localObject = i.i;
    localObject = i.a.a();
    locala.a((com.truecaller.credit.app.c.a.a)localObject).a().a(this);
  }
  
  public final int c()
  {
    return R.layout.fragment_loan_history;
  }
  
  public final void f()
  {
    HashMap localHashMap = d;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final void g()
  {
    Object localObject1 = getContext();
    if (localObject1 != null)
    {
      int i = R.id.loanFragmentPager;
      Object localObject2 = (ViewPager)a(i);
      k.a(localObject2, "loanFragmentPager");
      int j = 1;
      ((ViewPager)localObject2).setOffscreenPageLimit(j);
      localObject2 = new com/truecaller/credit/app/ui/loanhistory/views/a/g;
      Object localObject3 = getChildFragmentManager();
      k.a(localObject3, "childFragmentManager");
      String str = "it";
      k.a(localObject1, str);
      ((g)localObject2).<init>((j)localObject3, (Context)localObject1);
      c = ((g)localObject2);
      int k = R.id.loanFragmentPager;
      localObject1 = (ViewPager)a(k);
      k.a(localObject1, "loanFragmentPager");
      localObject2 = c;
      if (localObject2 == null)
      {
        localObject3 = "adapter";
        k.a((String)localObject3);
      }
      localObject2 = (o)localObject2;
      ((ViewPager)localObject1).setAdapter((o)localObject2);
      k = R.id.tabs_layout;
      localObject1 = (TabLayout)a(k);
      i = R.id.loanFragmentPager;
      localObject2 = (ViewPager)a(i);
      ((TabLayout)localObject1).setupWithViewPager((ViewPager)localObject2);
      k = R.id.loanFragmentPager;
      localObject1 = (ViewPager)a(k);
      localObject2 = this;
      localObject2 = (ViewPager.f)this;
      ((ViewPager)localObject1).a((ViewPager.f)localObject2);
      return;
    }
  }
  
  public final void onPageScrollStateChanged(int paramInt) {}
  
  public final void onPageScrolled(int paramInt1, float paramFloat, int paramInt2) {}
  
  public final void onPageSelected(int paramInt)
  {
    ((c.a)a()).a(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.loanhistory.views.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */