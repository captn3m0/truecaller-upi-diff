package com.truecaller.credit.app.ui.loanhistory.views.a;

import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.d.b.ab;
import com.d.b.ai;
import com.d.b.w;
import com.truecaller.common.h.aq.c;
import com.truecaller.common.payments.a.b;
import com.truecaller.credit.R.color;
import com.truecaller.credit.R.drawable;
import com.truecaller.credit.R.id;
import com.truecaller.credit.R.string;
import com.truecaller.credit.app.ui.customview.LoanEmiProgressView;
import com.truecaller.credit.app.util.f;
import com.truecaller.credit.app.util.j;
import com.truecaller.utils.n;

public final class i
  implements h
{
  private int a;
  private final n b;
  private final j c;
  private final f d;
  
  public i(n paramn, j paramj, f paramf)
  {
    b = paramn;
    c = paramj;
    d = paramf;
    a = -1;
  }
  
  public final void a(int paramInt)
  {
    int i = a;
    if (i == paramInt) {
      paramInt = -1;
    }
    a = paramInt;
  }
  
  public final void a(k paramk, b paramb)
  {
    c.g.b.k.b(paramk, "loanHistoryItemViewHolder");
    c.g.b.k.b(paramb, "loanData");
    Object localObject1 = b;
    c.g.b.k.b(localObject1, "loanDescription");
    int i = R.id.textCategoryName;
    Object localObject2 = (AppCompatTextView)paramk.b(i);
    Object localObject3 = "textCategoryName";
    c.g.b.k.a(localObject2, (String)localObject3);
    localObject1 = (CharSequence)localObject1;
    ((AppCompatTextView)localObject2).setText((CharSequence)localObject1);
    localObject1 = b;
    i = R.string.credit_rs_prefix;
    boolean bool1 = true;
    Object localObject4 = new Object[bool1];
    Object localObject5 = c;
    localObject4[0] = localObject5;
    localObject1 = ((n)localObject1).a(i, (Object[])localObject4);
    c.g.b.k.a(localObject1, "resourceProvider.getStri…_prefix, loanData.amount)");
    c.g.b.k.b(localObject1, "loanAmount");
    i = R.id.textLoanAmount;
    localObject2 = (TextView)paramk.b(i);
    c.g.b.k.a(localObject2, "textLoanAmount");
    localObject1 = (CharSequence)localObject1;
    ((TextView)localObject2).setText((CharSequence)localObject1);
    localObject1 = b;
    i = R.string.credit_disbursed_on;
    localObject4 = new Object[bool1];
    localObject5 = c;
    long l1 = d;
    String str = "dd/MM/yyyy";
    localObject5 = ((j)localObject5).a(l1, str);
    localObject4[0] = localObject5;
    localObject1 = ((n)localObject1).a(i, (Object[])localObject4);
    c.g.b.k.a(localObject1, "resourceProvider.getStri…FORMAT)\n                )");
    c.g.b.k.b(localObject1, "disbursedDate");
    i = R.id.textDisbursedDate;
    localObject2 = (TextView)paramk.b(i);
    c.g.b.k.a(localObject2, "textDisbursedDate");
    localObject1 = (CharSequence)localObject1;
    ((TextView)localObject2).setText((CharSequence)localObject1);
    localObject1 = k;
    c.g.b.k.b(localObject1, "loanId");
    i = R.id.textLoanId;
    localObject2 = (TextView)paramk.b(i);
    c.g.b.k.a(localObject2, "textLoanId");
    localObject1 = (CharSequence)localObject1;
    ((TextView)localObject2).setText((CharSequence)localObject1);
    localObject1 = k;
    c.g.b.k.b(localObject1, "loanId");
    i = R.id.textActiveLoanId;
    localObject2 = (TextView)paramk.b(i);
    localObject4 = "textActiveLoanId";
    c.g.b.k.a(localObject2, (String)localObject4);
    localObject1 = (CharSequence)localObject1;
    ((TextView)localObject2).setText((CharSequence)localObject1);
    localObject1 = i;
    i = ((String)localObject1).hashCode();
    int j = -934535297;
    if (i != j)
    {
      j = 96784904;
      if (i != j)
      {
        j = 1638128981;
        if (i == j)
        {
          localObject2 = "in_process";
          boolean bool2 = ((String)localObject1).equals(localObject2);
          if (bool2)
          {
            localObject1 = d;
            i = R.color.orange;
            int k = ((f)localObject1).a(i);
            break label540;
          }
        }
      }
      else
      {
        localObject2 = "error";
        boolean bool3 = ((String)localObject1).equals(localObject2);
        if (bool3)
        {
          localObject1 = d;
          i = R.color.coral;
          int m = ((f)localObject1).a(i);
          break label540;
        }
      }
    }
    else
    {
      localObject2 = "repaid";
      boolean bool4 = ((String)localObject1).equals(localObject2);
      if (bool4)
      {
        localObject1 = d;
        i = R.color.turquoise;
        n = ((f)localObject1).a(i);
        break label540;
      }
    }
    localObject1 = d;
    i = R.color.blue_grey;
    int n = ((f)localObject1).a(i);
    label540:
    localObject2 = j;
    c.g.b.k.b(localObject2, "statusText");
    j = R.id.textStatus;
    localObject4 = (TextView)paramk.b(j);
    ((TextView)localObject4).setTextColor(n);
    localObject2 = (CharSequence)localObject2;
    ((TextView)localObject4).setText((CharSequence)localObject2);
    localObject1 = n;
    c.g.b.k.b(localObject1, "url");
    localObject1 = w.a(a.getContext()).a((String)localObject1);
    i = R.drawable.ic_credit_category_place_holder;
    localObject1 = ((ab)localObject1).a(i);
    localObject2 = new com/truecaller/common/h/aq$c;
    ((aq.c)localObject2).<init>(4.0F);
    localObject2 = (ai)localObject2;
    localObject1 = ((ab)localObject1).a((ai)localObject2);
    i = R.id.imageCategory;
    localObject2 = (AppCompatImageView)paramk.b(i);
    j = 0;
    ((ab)localObject1).a((ImageView)localObject2, null);
    localObject1 = c;
    long l2 = d;
    localObject1 = ((j)localObject1).a(l2, "dd/MM/yyyy");
    c.g.b.k.b(localObject1, "date");
    i = R.id.textDisbursedDateHistory;
    localObject2 = (TextView)paramk.b(i);
    localObject4 = "textDisbursedDateHistory";
    c.g.b.k.a(localObject2, (String)localObject4);
    localObject1 = (CharSequence)localObject1;
    ((TextView)localObject2).setText((CharSequence)localObject1);
    localObject1 = i;
    localObject2 = "success";
    boolean bool5 = c.g.b.k.a(localObject1, localObject2);
    if (bool5)
    {
      i1 = a;
      i = paramk.getAdapterPosition();
      if (i1 == i) {
        paramk.c(bool1);
      } else {
        paramk.c(false);
      }
      paramk.b(false);
      paramk.e(bool1);
      paramk.a(false);
      paramk.d(false);
      localObject1 = r;
      if (localObject1 != null)
      {
        localObject2 = b;
        j = R.string.credit_rs_prefix;
        localObject5 = new Object[bool1];
        localObject5[0] = localObject1;
        localObject1 = ((n)localObject2).a(j, (Object[])localObject5);
        c.g.b.k.a(localObject1, "resourceProvider.getStri…ing.credit_rs_prefix, it)");
        c.g.b.k.b(localObject1, "processingFee");
        i = R.id.textProcessingFee;
        localObject2 = (TextView)paramk.b(i);
        localObject4 = "textProcessingFee";
        c.g.b.k.a(localObject2, (String)localObject4);
        localObject1 = (CharSequence)localObject1;
        ((TextView)localObject2).setText((CharSequence)localObject1);
      }
      localObject1 = q;
      if (localObject1 != null)
      {
        localObject2 = b;
        j = R.string.credit_rs_prefix;
        localObject5 = new Object[bool1];
        localObject5[0] = localObject1;
        localObject1 = ((n)localObject2).a(j, (Object[])localObject5);
        c.g.b.k.a(localObject1, "resourceProvider.getStri…ing.credit_rs_prefix, it)");
        c.g.b.k.b(localObject1, "disbursedAmount");
        i = R.id.textDisbursedAmount;
        localObject2 = (TextView)paramk.b(i);
        localObject4 = "textDisbursedAmount";
        c.g.b.k.a(localObject2, (String)localObject4);
        localObject1 = (CharSequence)localObject1;
        ((TextView)localObject2).setText((CharSequence)localObject1);
      }
      localObject1 = b;
      i = R.string.credit_emis_remaining;
      j = 2;
      localObject4 = new Object[j];
      localObject5 = f;
      localObject4[0] = localObject5;
      localObject5 = e;
      localObject4[bool1] = localObject5;
      localObject1 = ((n)localObject1).a(i, (Object[])localObject4);
      c.g.b.k.a(localObject1, "resourceProvider.getStri…ount, loanData.emisCount)");
      c.g.b.k.b(localObject1, "remainingEmi");
      i = R.id.textRemainingEmis;
      localObject2 = (TextView)paramk.b(i);
      c.g.b.k.a(localObject2, "textRemainingEmis");
      localObject1 = (CharSequence)localObject1;
      ((TextView)localObject2).setText((CharSequence)localObject1);
      localObject1 = b;
      i = R.string.credit_rs_prefix;
      localObject3 = new Object[bool1];
      localObject4 = h;
      localObject3[0] = localObject4;
      localObject1 = ((n)localObject1).a(i, (Object[])localObject3);
      c.g.b.k.a(localObject1, "resourceProvider.getStri…efix, loanData.emiAmount)");
      c.g.b.k.b(localObject1, "emiAmount");
      i = R.id.textEmiAmount;
      localObject2 = (TextView)paramk.b(i);
      localObject3 = "textEmiAmount";
      c.g.b.k.a(localObject2, (String)localObject3);
      localObject1 = (CharSequence)localObject1;
      ((TextView)localObject2).setText((CharSequence)localObject1);
      localObject1 = g;
      if (localObject1 != null)
      {
        ((Number)localObject1).longValue();
        localObject1 = c;
        long l3 = d;
        localObject4 = "dd/MM/yyyy";
        localObject1 = ((j)localObject1).a(l3, (String)localObject4);
        c.g.b.k.b(localObject1, "nextEmiDate");
        i = R.id.textNextEmi;
        localObject2 = (TextView)paramk.b(i);
        localObject3 = "textNextEmi";
        c.g.b.k.a(localObject2, (String)localObject3);
        localObject1 = (CharSequence)localObject1;
        ((TextView)localObject2).setText((CharSequence)localObject1);
      }
      localObject1 = e;
      if (localObject1 != null)
      {
        localObject1 = (Number)localObject1;
        i1 = ((Number)localObject1).intValue();
        i = R.id.progressLoanEmi;
        localObject2 = (LoanEmiProgressView)paramk.b(i);
        ((LoanEmiProgressView)localObject2).setTotalEmi(i1);
        paramb = f;
        if (paramb != null)
        {
          i2 = ((Number)paramb).intValue();
          i1 -= i2;
          paramk.a(i1);
          return;
        }
        paramk.a(i1);
      }
      return;
    }
    int i2 = a;
    int i1 = paramk.getAdapterPosition();
    if (i2 == i1) {
      paramk.d(bool1);
    } else {
      paramk.d(false);
    }
    paramk.c(false);
    paramk.a(bool1);
    paramk.b(bool1);
    paramk.e(false);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.loanhistory.views.a.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */