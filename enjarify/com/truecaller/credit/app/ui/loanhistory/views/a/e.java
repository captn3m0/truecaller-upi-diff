package com.truecaller.credit.app.ui.loanhistory.views.a;

import android.support.constraint.ConstraintLayout;
import android.support.constraint.Group;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.credit.R.id;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;
import kotlinx.a.a.a;

public final class e
  extends RecyclerView.ViewHolder
  implements a
{
  private final View a;
  private HashMap b;
  
  public e(View paramView)
  {
    super(paramView);
    a = paramView;
  }
  
  public final View a()
  {
    return a;
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = b;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      b = ((HashMap)localObject1);
    }
    localObject1 = b;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = a();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = b;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "transactionId");
    int i = R.id.textTransactionId;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "textTransactionId");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void a(boolean paramBoolean)
  {
    int i = R.id.transactionContainer;
    ConstraintLayout localConstraintLayout = (ConstraintLayout)a(i);
    k.a(localConstraintLayout, "transactionContainer");
    t.a((View)localConstraintLayout, paramBoolean);
  }
  
  public final void b(boolean paramBoolean)
  {
    int i = R.id.transactionViews;
    Group localGroup = (Group)a(i);
    k.a(localGroup, "transactionViews");
    t.a((View)localGroup, paramBoolean);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.loanhistory.views.a.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */