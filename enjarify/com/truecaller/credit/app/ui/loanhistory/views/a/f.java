package com.truecaller.credit.app.ui.loanhistory.views.a;

import android.content.Context;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import c.g.b.k;
import java.util.List;

public final class f
  extends RecyclerView.Adapter
{
  private final LayoutInflater a;
  private List b;
  private final h c;
  private final f.a d;
  
  public f(Context paramContext, List paramList, h paramh, f.a parama)
  {
    b = paramList;
    c = paramh;
    d = parama;
    paramContext = LayoutInflater.from(paramContext);
    k.a(paramContext, "LayoutInflater.from(context)");
    a = paramContext;
  }
  
  public final int getItemCount()
  {
    List localList = b;
    if (localList != null) {
      return localList.size();
    }
    return 0;
  }
  
  public final long getItemId(int paramInt)
  {
    return paramInt;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.loanhistory.views.a.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */