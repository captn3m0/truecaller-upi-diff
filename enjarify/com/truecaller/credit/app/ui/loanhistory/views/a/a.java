package com.truecaller.credit.app.ui.loanhistory.views.a;

import android.support.v7.widget.RecyclerView.Adapter;
import java.util.List;

public final class a
  extends RecyclerView.Adapter
{
  private List a;
  private final b b;
  
  public a(List paramList, b paramb)
  {
    a = paramList;
    b = paramb;
  }
  
  public final int getItemCount()
  {
    List localList = a;
    if (localList != null) {
      return localList.size();
    }
    return 0;
  }
  
  public final long getItemId(int paramInt)
  {
    return paramInt;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.loanhistory.views.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */