package com.truecaller.credit.app.ui.loanhistory.views.a;

import android.view.View;
import android.view.View.OnClickListener;
import c.g.b.k;
import c.u;
import com.truecaller.credit.domain.interactors.loanhistory.models.EmiData;

final class a$a
  implements View.OnClickListener
{
  a$a(a parama, e parame, int paramInt) {}
  
  public final void onClick(View paramView)
  {
    paramView = b.itemView;
    String str = "holder.itemView";
    k.a(paramView, str);
    paramView = paramView.getTag();
    if (paramView != null)
    {
      paramView = ((EmiData)paramView).getStatus();
      int i = paramView.hashCode();
      int j = -934535297;
      boolean bool;
      if (i != j)
      {
        j = 931947885;
        if (i != j) {
          break label123;
        }
        str = "late_payment";
        bool = paramView.equals(str);
        if (!bool) {
          break label123;
        }
      }
      else
      {
        str = "repaid";
        bool = paramView.equals(str);
        if (!bool) {
          break label123;
        }
      }
      paramView = a.a(a);
      i = c;
      paramView.a(i);
      paramView = a;
      paramView.notifyDataSetChanged();
      label123:
      return;
    }
    paramView = new c/u;
    paramView.<init>("null cannot be cast to non-null type com.truecaller.credit.domain.interactors.loanhistory.models.EmiData");
    throw paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.loanhistory.views.a.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */