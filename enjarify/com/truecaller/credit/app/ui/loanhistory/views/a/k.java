package com.truecaller.credit.app.ui.loanhistory.views.a;

import android.support.constraint.Group;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.TextView;
import com.truecaller.credit.R.id;
import com.truecaller.credit.app.ui.customview.LoanEmiProgressView;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;
import kotlinx.a.a.a;

public final class k
  extends RecyclerView.ViewHolder
  implements a
{
  final View a;
  private HashMap b;
  
  public k(View paramView)
  {
    super(paramView);
    a = paramView;
  }
  
  public final View a()
  {
    return a;
  }
  
  public final void a(int paramInt)
  {
    int i = R.id.progressLoanEmi;
    ((LoanEmiProgressView)b(i)).setPaidEmis(paramInt);
  }
  
  public final void a(boolean paramBoolean)
  {
    int i = R.id.textStatus;
    TextView localTextView = (TextView)b(i);
    c.g.b.k.a(localTextView, "textStatus");
    t.a((View)localTextView, paramBoolean);
  }
  
  public final View b(int paramInt)
  {
    Object localObject1 = b;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      b = ((HashMap)localObject1);
    }
    localObject1 = b;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = a();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = b;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void b(boolean paramBoolean)
  {
    int i = R.id.textDisbursedDateHistory;
    TextView localTextView = (TextView)b(i);
    c.g.b.k.a(localTextView, "textDisbursedDateHistory");
    t.a((View)localTextView, paramBoolean);
  }
  
  public final void c(boolean paramBoolean)
  {
    int i = R.id.emiViews;
    Group localGroup = (Group)b(i);
    c.g.b.k.a(localGroup, "emiViews");
    t.a((View)localGroup, paramBoolean);
  }
  
  public final void d(boolean paramBoolean)
  {
    int i = R.id.loanIdViews;
    Group localGroup = (Group)b(i);
    c.g.b.k.a(localGroup, "loanIdViews");
    t.a((View)localGroup, paramBoolean);
  }
  
  public final void e(boolean paramBoolean)
  {
    int i = R.id.disbursalViews;
    Group localGroup = (Group)b(i);
    c.g.b.k.a(localGroup, "disbursalViews");
    t.a((View)localGroup, paramBoolean);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.loanhistory.views.a.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */