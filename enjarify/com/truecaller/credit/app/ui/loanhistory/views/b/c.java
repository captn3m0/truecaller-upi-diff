package com.truecaller.credit.app.ui.loanhistory.views.b;

import android.content.Context;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.view.View;
import android.widget.ProgressBar;
import c.g.b.k;
import c.u;
import com.truecaller.credit.R.id;
import com.truecaller.credit.R.layout;
import com.truecaller.credit.app.ui.loanhistory.b.a.a.a;
import com.truecaller.credit.app.ui.loanhistory.views.a.f;
import com.truecaller.credit.app.ui.loanhistory.views.a.f.a;
import com.truecaller.credit.app.ui.loanhistory.views.a.h;
import com.truecaller.credit.app.ui.loanhistory.views.activities.LoanHistoryActivity;
import com.truecaller.credit.app.ui.loanhistory.views.c.e.a;
import com.truecaller.credit.app.ui.loanhistory.views.c.e.b;
import com.truecaller.credit.i;
import com.truecaller.credit.i.a;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;
import java.util.List;

public final class c
  extends com.truecaller.credit.app.ui.b.c
  implements f.a, e.b
{
  public static final c.a d;
  public h c;
  private com.truecaller.credit.app.ui.loanhistory.views.c.b e;
  private HashMap f;
  
  static
  {
    c.a locala = new com/truecaller/credit/app/ui/loanhistory/views/b/c$a;
    locala.<init>((byte)0);
    d = locala;
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = f;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      f = ((HashMap)localObject1);
    }
    localObject1 = f;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = f;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a(com.truecaller.common.payments.a.b paramb)
  {
    k.b(paramb, "loanData");
    ((e.a)a()).a(paramb);
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "title");
    Object localObject = getActivity();
    if (localObject != null)
    {
      localObject = ((LoanHistoryActivity)localObject).getSupportActionBar();
      if (localObject != null)
      {
        paramString = (CharSequence)paramString;
        ((ActionBar)localObject).setTitle(paramString);
        return;
      }
      return;
    }
    paramString = new c/u;
    paramString.<init>("null cannot be cast to non-null type com.truecaller.credit.app.ui.loanhistory.views.activities.LoanHistoryActivity");
    throw paramString;
  }
  
  public final void a(List paramList)
  {
    k.b(paramList, "loans");
    Object localObject1 = getContext();
    if (localObject1 != null)
    {
      int i = R.id.emptyContainer;
      Object localObject2 = (ConstraintLayout)a(i);
      k.a(localObject2, "emptyContainer");
      t.b((View)localObject2);
      localObject2 = new com/truecaller/credit/app/ui/loanhistory/views/a/f;
      k.a(localObject1, "it");
      h localh = c;
      if (localh == null)
      {
        localObject3 = "loanHistoryItemPresenter";
        k.a((String)localObject3);
      }
      Object localObject3 = this;
      localObject3 = (f.a)this;
      ((f)localObject2).<init>((Context)localObject1, paramList, localh, (f.a)localObject3);
      paramList = new android/support/v7/widget/LinearLayoutManager;
      localObject1 = getContext();
      paramList.<init>((Context)localObject1);
      int j = R.id.listLoanHistory;
      localObject1 = (RecyclerView)a(j);
      paramList = (RecyclerView.LayoutManager)paramList;
      ((RecyclerView)localObject1).setLayoutManager(paramList);
      ((RecyclerView)localObject1).setHasFixedSize(true);
      localObject2 = (RecyclerView.Adapter)localObject2;
      ((RecyclerView)localObject1).setAdapter((RecyclerView.Adapter)localObject2);
      return;
    }
  }
  
  public final void b()
  {
    a.a locala = com.truecaller.credit.app.ui.loanhistory.b.a.a.a();
    Object localObject = i.i;
    localObject = i.a.a();
    locala.a((com.truecaller.credit.app.c.a.a)localObject).a().a(this);
  }
  
  public final void b(int paramInt)
  {
    Object localObject = getActivity();
    if (localObject != null)
    {
      localObject = ((LoanHistoryActivity)localObject).getSupportActionBar();
      if (localObject != null)
      {
        ((ActionBar)localObject).setElevation(0.0F);
        ((ActionBar)localObject).setDisplayHomeAsUpEnabled(true);
        ((ActionBar)localObject).setHomeAsUpIndicator(paramInt);
        return;
      }
      return;
    }
    u localu = new c/u;
    localu.<init>("null cannot be cast to non-null type com.truecaller.credit.app.ui.loanhistory.views.activities.LoanHistoryActivity");
    throw localu;
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "loanId");
    com.truecaller.credit.app.ui.loanhistory.views.c.b localb = e;
    if (localb != null)
    {
      Object localObject = a.d;
      k.b(paramString, "loanId");
      localObject = new android/os/Bundle;
      ((Bundle)localObject).<init>();
      ((Bundle)localObject).putString("loan_id", paramString);
      paramString = new com/truecaller/credit/app/ui/loanhistory/views/b/a;
      paramString.<init>();
      paramString.setArguments((Bundle)localObject);
      paramString = (Fragment)paramString;
      localb.a(paramString);
      return;
    }
  }
  
  public final int c()
  {
    return R.layout.fragment_active_loan_history;
  }
  
  public final void f()
  {
    HashMap localHashMap = f;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final int g()
  {
    Bundle localBundle = getArguments();
    if (localBundle != null) {
      return localBundle.getInt("type_filter");
    }
    return 0;
  }
  
  public final void h()
  {
    int i = R.id.loanHistoryProgress;
    ProgressBar localProgressBar = (ProgressBar)a(i);
    k.a(localProgressBar, "loanHistoryProgress");
    t.a((View)localProgressBar);
  }
  
  public final void i()
  {
    int i = R.id.loanHistoryProgress;
    ProgressBar localProgressBar = (ProgressBar)a(i);
    k.a(localProgressBar, "loanHistoryProgress");
    t.b((View)localProgressBar);
  }
  
  public final void j()
  {
    int i = R.id.emptyContainer;
    ConstraintLayout localConstraintLayout = (ConstraintLayout)a(i);
    k.a(localConstraintLayout, "emptyContainer");
    t.a((View)localConstraintLayout);
  }
  
  public final void onAttach(Context paramContext)
  {
    Object localObject = "context";
    k.b(paramContext, (String)localObject);
    super.onAttach(paramContext);
    boolean bool = paramContext instanceof com.truecaller.credit.app.ui.loanhistory.views.c.b;
    if (bool)
    {
      paramContext = (com.truecaller.credit.app.ui.loanhistory.views.c.b)paramContext;
      e = paramContext;
      return;
    }
    localObject = new java/lang/RuntimeException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(paramContext);
    localStringBuilder.append(" must implement LoanHistoryActionListener");
    paramContext = localStringBuilder.toString();
    ((RuntimeException)localObject).<init>(paramContext);
    throw ((Throwable)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.loanhistory.views.b.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */