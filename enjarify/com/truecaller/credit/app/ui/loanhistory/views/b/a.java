package com.truecaller.credit.app.ui.loanhistory.views.b;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.view.View;
import android.widget.ProgressBar;
import c.g.b.k;
import c.u;
import com.truecaller.credit.R.id;
import com.truecaller.credit.R.layout;
import com.truecaller.credit.app.ui.b.c;
import com.truecaller.credit.app.ui.loanhistory.views.activities.LoanHistoryActivity;
import com.truecaller.credit.app.ui.loanhistory.views.c.a.b;
import com.truecaller.credit.i;
import com.truecaller.credit.i.a;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;
import java.util.List;

public final class a
  extends c
  implements a.b
{
  public static final a.a d;
  public com.truecaller.credit.app.ui.loanhistory.views.a.b c;
  private com.truecaller.credit.app.ui.loanhistory.views.c.b e;
  private HashMap f;
  
  static
  {
    a.a locala = new com/truecaller/credit/app/ui/loanhistory/views/b/a$a;
    locala.<init>((byte)0);
    d = locala;
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = f;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      f = ((HashMap)localObject1);
    }
    localObject1 = f;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = f;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "title");
    Object localObject = getActivity();
    if (localObject != null)
    {
      localObject = ((LoanHistoryActivity)localObject).getSupportActionBar();
      if (localObject != null)
      {
        paramString = (CharSequence)paramString;
        ((ActionBar)localObject).setTitle(paramString);
        return;
      }
      return;
    }
    paramString = new c/u;
    paramString.<init>("null cannot be cast to non-null type com.truecaller.credit.app.ui.loanhistory.views.activities.LoanHistoryActivity");
    throw paramString;
  }
  
  public final void a(List paramList)
  {
    k.b(paramList, "emis");
    Object localObject1 = new com/truecaller/credit/app/ui/loanhistory/views/a/a;
    Object localObject2 = c;
    if (localObject2 == null)
    {
      String str = "emiDetailsItemPresenter";
      k.a(str);
    }
    ((com.truecaller.credit.app.ui.loanhistory.views.a.a)localObject1).<init>(paramList, (com.truecaller.credit.app.ui.loanhistory.views.a.b)localObject2);
    paramList = new android/support/v7/widget/LinearLayoutManager;
    localObject2 = getContext();
    paramList.<init>((Context)localObject2);
    int i = R.id.listEmiHistory;
    localObject2 = (RecyclerView)a(i);
    paramList = (RecyclerView.LayoutManager)paramList;
    ((RecyclerView)localObject2).setLayoutManager(paramList);
    ((RecyclerView)localObject2).setHasFixedSize(true);
    localObject1 = (RecyclerView.Adapter)localObject1;
    ((RecyclerView)localObject2).setAdapter((RecyclerView.Adapter)localObject1);
  }
  
  public final void b()
  {
    com.truecaller.credit.app.ui.loanhistory.b.a.a.a locala = com.truecaller.credit.app.ui.loanhistory.b.a.a.a();
    Object localObject = i.i;
    localObject = i.a.a();
    locala.a((com.truecaller.credit.app.c.a.a)localObject).a().a(this);
  }
  
  public final void b(int paramInt)
  {
    Object localObject = getActivity();
    if (localObject != null)
    {
      localObject = ((LoanHistoryActivity)localObject).getSupportActionBar();
      if (localObject != null)
      {
        ((ActionBar)localObject).setElevation(0.0F);
        ((ActionBar)localObject).setDisplayHomeAsUpEnabled(true);
        ((ActionBar)localObject).setHomeAsUpIndicator(paramInt);
        return;
      }
      return;
    }
    u localu = new c/u;
    localu.<init>("null cannot be cast to non-null type com.truecaller.credit.app.ui.loanhistory.views.activities.LoanHistoryActivity");
    throw localu;
  }
  
  public final int c()
  {
    return R.layout.fragment_emi_history;
  }
  
  public final void f()
  {
    HashMap localHashMap = f;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final String g()
  {
    Object localObject = getArguments();
    if (localObject != null)
    {
      String str = "loan_id";
      localObject = ((Bundle)localObject).getString(str);
      if (localObject != null) {}
    }
    else
    {
      localObject = "";
    }
    return (String)localObject;
  }
  
  public final void h()
  {
    int i = R.id.emiHistoryProgress;
    ProgressBar localProgressBar = (ProgressBar)a(i);
    k.a(localProgressBar, "emiHistoryProgress");
    t.a((View)localProgressBar);
  }
  
  public final void i()
  {
    int i = R.id.emiHistoryProgress;
    ProgressBar localProgressBar = (ProgressBar)a(i);
    k.a(localProgressBar, "emiHistoryProgress");
    t.b((View)localProgressBar);
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    boolean bool = paramContext instanceof com.truecaller.credit.app.ui.loanhistory.views.c.b;
    if (bool)
    {
      paramContext = (com.truecaller.credit.app.ui.loanhistory.views.c.b)paramContext;
      e = paramContext;
      return;
    }
    RuntimeException localRuntimeException = new java/lang/RuntimeException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(paramContext);
    localStringBuilder.append(" must implement LoanHistoryActionListener");
    paramContext = localStringBuilder.toString();
    localRuntimeException.<init>(paramContext);
    throw ((Throwable)localRuntimeException);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.loanhistory.views.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */