package com.truecaller.credit.app.ui.loanhistory.views.a;

import android.widget.TextView;
import c.g.b.k;
import com.truecaller.credit.R.color;
import com.truecaller.credit.R.id;
import com.truecaller.credit.R.string;
import com.truecaller.credit.app.ui.customview.EmiDateView;
import com.truecaller.credit.app.util.f;
import com.truecaller.credit.app.util.j;
import com.truecaller.credit.domain.interactors.loanhistory.models.EmiData;
import com.truecaller.credit.domain.interactors.loanhistory.models.Payment;
import com.truecaller.utils.n;

public final class c
  implements b
{
  private int a;
  private final n b;
  private final j c;
  private final f d;
  
  public c(n paramn, j paramj, f paramf)
  {
    b = paramn;
    c = paramj;
    d = paramf;
    a = -1;
  }
  
  public final void a(int paramInt)
  {
    int i = a;
    if (i == paramInt) {
      paramInt = -1;
    }
    a = paramInt;
  }
  
  public final void a(e parame, EmiData paramEmiData)
  {
    k.b(parame, "emiDetailsViewHolder");
    k.b(paramEmiData, "emiData");
    Object localObject1 = c;
    long l1 = Long.parseLong(paramEmiData.getDueDate());
    localObject1 = ((j)localObject1).b(l1);
    k.b(localObject1, "day");
    int i = R.id.emiDateView;
    ((EmiDateView)parame.a(i)).setDay((String)localObject1);
    localObject1 = c;
    l1 = Long.parseLong(paramEmiData.getDueDate());
    localObject1 = ((j)localObject1).a(l1);
    k.b(localObject1, "month");
    i = R.id.emiDateView;
    ((EmiDateView)parame.a(i)).setMonth((String)localObject1);
    localObject1 = b;
    i = R.string.credit_rs_prefix;
    int n = 1;
    Object localObject2 = new Object[n];
    Object localObject3 = paramEmiData.getAmount();
    localObject2[0] = localObject3;
    localObject1 = ((n)localObject1).a(i, (Object[])localObject2);
    k.a(localObject1, "resourceProvider.getStri…s_prefix, emiData.amount)");
    k.b(localObject1, "amount");
    i = R.id.textEmiPaid;
    Object localObject4 = (TextView)parame.a(i);
    localObject2 = "textEmiPaid";
    k.a(localObject4, (String)localObject2);
    localObject1 = (CharSequence)localObject1;
    ((TextView)localObject4).setText((CharSequence)localObject1);
    int i1 = parame.getAdapterPosition() + n;
    switch (i1)
    {
    default: 
      localObject4 = b;
      i3 = R.string.credit_emi_counter;
      localObject3 = new Object[n];
      localObject1 = Integer.valueOf(i1);
      localObject3[0] = localObject1;
      localObject1 = ((n)localObject4).a(i3, (Object[])localObject3);
      localObject4 = "resourceProvider.getStri…it_emi_counter, position)";
      k.a(localObject1, (String)localObject4);
      break;
    case 3: 
      localObject1 = b;
      i = R.string.credit_third_emi;
      localObject2 = new Object[0];
      localObject1 = ((n)localObject1).a(i, (Object[])localObject2);
      localObject4 = "resourceProvider.getStri….string.credit_third_emi)";
      k.a(localObject1, (String)localObject4);
      break;
    case 2: 
      localObject1 = b;
      i = R.string.credit_second_emi;
      localObject2 = new Object[0];
      localObject1 = ((n)localObject1).a(i, (Object[])localObject2);
      localObject4 = "resourceProvider.getStri…string.credit_second_emi)";
      k.a(localObject1, (String)localObject4);
      break;
    case 1: 
      localObject1 = b;
      i = R.string.credit_first_emi;
      localObject2 = new Object[0];
      localObject1 = ((n)localObject1).a(i, (Object[])localObject2);
      localObject4 = "resourceProvider.getStri….string.credit_first_emi)";
      k.a(localObject1, (String)localObject4);
    }
    k.b(localObject1, "count");
    i = R.id.textEmiCount;
    localObject4 = (TextView)parame.a(i);
    localObject2 = "textEmiCount";
    k.a(localObject4, (String)localObject2);
    localObject1 = (CharSequence)localObject1;
    ((TextView)localObject4).setText((CharSequence)localObject1);
    localObject1 = paramEmiData.getStatusText();
    localObject4 = paramEmiData.getStatus();
    int i3 = ((String)localObject4).hashCode();
    int i4 = -934535297;
    if (i3 != i4)
    {
      i4 = -682587753;
      boolean bool2;
      if (i3 != i4)
      {
        i4 = 931947885;
        if (i3 != i4)
        {
          i4 = 1959784951;
          if (i3 != i4) {
            break label666;
          }
          localObject2 = "invalid";
          boolean bool1 = ((String)localObject4).equals(localObject2);
          if (!bool1) {
            break label666;
          }
          localObject4 = d;
          i3 = R.color.coral;
          int j = ((f)localObject4).a(i3);
          break label688;
        }
        localObject2 = "late_payment";
        bool2 = ((String)localObject4).equals(localObject2);
        if (!bool2) {
          break label666;
        }
      }
      else
      {
        localObject2 = "pending";
        bool2 = ((String)localObject4).equals(localObject2);
        if (!bool2) {
          break label666;
        }
        localObject4 = d;
        i3 = R.color.orange;
        int k = ((f)localObject4).a(i3);
        break label688;
      }
    }
    else
    {
      localObject2 = "repaid";
      boolean bool3 = ((String)localObject4).equals(localObject2);
      if (!bool3) {
        break label666;
      }
    }
    localObject4 = d;
    i3 = R.color.turquoise;
    int m = ((f)localObject4).a(i3);
    break label688;
    label666:
    localObject4 = d;
    i3 = R.color.blue_grey;
    m = ((f)localObject4).a(i3);
    label688:
    k.b(localObject1, "status");
    i3 = R.id.textPaymentStatus;
    localObject2 = (TextView)parame.a(i3);
    localObject1 = (CharSequence)localObject1;
    ((TextView)localObject2).setText((CharSequence)localObject1);
    ((TextView)localObject2).setTextColor(m);
    localObject1 = paramEmiData.getPaymentMode();
    if (localObject1 != null)
    {
      localObject4 = b;
      i3 = R.string.credit_emi_payment_mode;
      localObject3 = new Object[n];
      localObject3[0] = localObject1;
      localObject1 = ((n)localObject4).a(i3, (Object[])localObject3);
      k.a(localObject1, "resourceProvider.getStri…dit_emi_payment_mode, it)");
      k.b(localObject1, "paymentMethod");
      m = R.id.textPaymentMode;
      localObject4 = (TextView)parame.a(m);
      localObject2 = "textPaymentMode";
      k.a(localObject4, (String)localObject2);
      localObject1 = (CharSequence)localObject1;
      ((TextView)localObject4).setText((CharSequence)localObject1);
    }
    i1 = a;
    m = parame.getAdapterPosition();
    if (i1 == m)
    {
      localObject1 = paramEmiData.getStatus();
      localObject4 = "late_payment";
      boolean bool4 = k.a(localObject1, localObject4);
      if (bool4)
      {
        localObject1 = paramEmiData.getPayment();
        if (localObject1 == null) {
          return;
        }
        localObject4 = b;
        i3 = R.string.credit_rs_prefix;
        localObject3 = new Object[n];
        String str = ((Payment)localObject1).getEmiAmount();
        localObject3[0] = str;
        localObject4 = ((n)localObject4).a(i3, (Object[])localObject3);
        k.a(localObject4, "resourceProvider.getStri…x, paymentData.emiAmount)");
        k.b(localObject4, "amount");
        i3 = R.id.textEmiAmount;
        localObject2 = (TextView)parame.a(i3);
        localObject3 = "textEmiAmount";
        k.a(localObject2, (String)localObject3);
        localObject4 = (CharSequence)localObject4;
        ((TextView)localObject2).setText((CharSequence)localObject4);
        localObject4 = ((Payment)localObject1).getTransactionId();
        if (localObject4 != null) {
          parame.a((String)localObject4);
        }
        localObject1 = ((Payment)localObject1).getLatePaymentFee();
        if (localObject1 != null)
        {
          localObject4 = b;
          i3 = R.string.credit_rs_prefix;
          localObject3 = new Object[n];
          localObject3[0] = localObject1;
          localObject1 = ((n)localObject4).a(i3, (Object[])localObject3);
          k.a(localObject1, "resourceProvider.getStri…ing.credit_rs_prefix, it)");
          k.b(localObject1, "amount");
          m = R.id.textLatePaymentFee;
          localObject4 = (TextView)parame.a(m);
          localObject2 = "textLatePaymentFee";
          k.a(localObject4, (String)localObject2);
          localObject1 = (CharSequence)localObject1;
          ((TextView)localObject4).setText((CharSequence)localObject1);
        }
        localObject1 = c;
        long l2 = Long.parseLong(paramEmiData.getDueDate());
        paramEmiData = ((j)localObject1).a(l2, "MMM dd,yyyy");
        k.b(paramEmiData, "date");
        int i2 = R.id.textDefaultedOn;
        localObject1 = (TextView)parame.a(i2);
        k.a(localObject1, "textDefaultedOn");
        paramEmiData = (CharSequence)paramEmiData;
        ((TextView)localObject1).setText(paramEmiData);
        parame.a(n);
        parame.b(n);
        return;
      }
      localObject1 = paramEmiData.getStatus();
      localObject4 = "repaid";
      boolean bool5 = k.a(localObject1, localObject4);
      if (bool5)
      {
        paramEmiData = paramEmiData.getPayment();
        if (paramEmiData == null) {
          return;
        }
        paramEmiData = paramEmiData.getTransactionId();
        if (paramEmiData != null) {
          parame.a(paramEmiData);
        }
        parame.a(n);
        parame.b(false);
        return;
      }
      parame.a(false);
      parame.b(false);
      return;
    }
    parame.a(false);
    parame.b(false);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.loanhistory.views.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */