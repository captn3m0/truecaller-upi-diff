package com.truecaller.credit.app.ui.loanhistory.c;

import c.d.f;
import c.g.b.k;
import c.t;
import com.truecaller.ba;
import com.truecaller.credit.app.a.a.a;
import com.truecaller.credit.app.ui.loanhistory.a.c;
import com.truecaller.credit.app.ui.loanhistory.views.c.e.a;
import com.truecaller.credit.app.ui.loanhistory.views.c.e.b;

public final class e
  extends ba
  implements e.a
{
  private final f c;
  private final f d;
  private final c e;
  private final com.truecaller.utils.n f;
  private final com.truecaller.credit.app.a.b g;
  
  public e(f paramf1, f paramf2, c paramc, com.truecaller.utils.n paramn, com.truecaller.credit.app.a.b paramb)
  {
    super(paramf1);
    c = paramf1;
    d = paramf2;
    e = paramc;
    f = paramn;
    g = paramb;
  }
  
  public final void a(com.truecaller.common.payments.a.b paramb)
  {
    k.b(paramb, "loanData");
    Object localObject = (e.b)b;
    if (localObject != null)
    {
      paramb = k;
      ((e.b)localObject).b(paramb);
    }
    paramb = "CreditLoanDetails";
    localObject = new com/truecaller/credit/app/a/a$a;
    ((a.a)localObject).<init>(paramb, paramb);
    paramb = new c.n[3];
    c.n localn1 = t.a("Status", "shown");
    paramb[0] = localn1;
    c.n localn2 = t.a("Context", "active_loans");
    paramb[1] = localn2;
    localn2 = t.a("Action", "emi_history");
    paramb[2] = localn2;
    ((a.a)localObject).a(paramb);
    paramb = ((a.a)localObject).a();
    a = false;
    localObject = g;
    paramb = paramb.b();
    ((com.truecaller.credit.app.a.b)localObject).a(paramb);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.loanhistory.c.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */