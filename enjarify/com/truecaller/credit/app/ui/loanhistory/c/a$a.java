package com.truecaller.credit.app.ui.loanhistory.c;

import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.credit.app.ui.loanhistory.views.c.a.b;
import com.truecaller.credit.data.Failure;
import com.truecaller.credit.data.Result;
import com.truecaller.credit.data.Success;
import com.truecaller.credit.domain.interactors.loanhistory.models.EmiHistory;
import java.util.List;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

final class a$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  a$a(a parama, String paramString, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/credit/app/ui/loanhistory/c/a$a;
    a locala1 = b;
    String str = c;
    locala.<init>(locala1, str, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = a;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label235;
      }
      paramObject = a.a(b);
      if (paramObject != null) {
        ((a.b)paramObject).h();
      }
      paramObject = a.b(b);
      Object localObject2 = new com/truecaller/credit/app/ui/loanhistory/c/a$a$a;
      ((a.a.a)localObject2).<init>(this, null);
      localObject2 = (m)localObject2;
      int j = 1;
      a = j;
      paramObject = g.a((f)paramObject, (m)localObject2, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (Result)paramObject;
    boolean bool2 = paramObject instanceof Success;
    if (bool2)
    {
      localObject1 = a.a(b);
      if (localObject1 != null)
      {
        paramObject = ((EmiHistory)((Success)paramObject).getData()).getEmis();
        ((a.b)localObject1).a((List)paramObject);
        ((a.b)localObject1).i();
      }
    }
    else
    {
      boolean bool3 = paramObject instanceof Failure;
      if (bool3)
      {
        paramObject = a.a(b);
        if (paramObject != null) {
          ((a.b)paramObject).i();
        }
      }
    }
    return x.a;
    label235:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.loanhistory.c.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */