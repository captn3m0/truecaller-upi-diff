package com.truecaller.credit.app.ui.loanhistory.a;

import android.arch.persistence.db.e;
import android.arch.persistence.room.c;
import android.arch.persistence.room.i;
import android.arch.persistence.room.j;
import android.database.Cursor;
import c.g.b.k;
import java.util.ArrayList;
import java.util.List;

public final class b
  implements a
{
  private final android.arch.persistence.room.f a;
  private final c b;
  private final j c;
  
  public b(android.arch.persistence.room.f paramf)
  {
    a = paramf;
    Object localObject = new com/truecaller/credit/app/ui/loanhistory/a/b$1;
    ((b.1)localObject).<init>(this, paramf);
    b = ((c)localObject);
    localObject = new com/truecaller/credit/app/ui/loanhistory/a/b$2;
    ((b.2)localObject).<init>(this, paramf);
    c = ((j)localObject);
  }
  
  public final int a()
  {
    android.arch.persistence.db.f localf = c.b();
    android.arch.persistence.room.f localf1 = a;
    localf1.d();
    try
    {
      int i = localf.a();
      android.arch.persistence.room.f localf2 = a;
      localf2.f();
      return i;
    }
    finally
    {
      a.e();
      c.a(localf);
    }
  }
  
  public final List a(String[] paramArrayOfString)
  {
    Object localObject1 = android.arch.persistence.room.b.a.a();
    Object localObject4 = "Select * from loans where status IN (";
    ((StringBuilder)localObject1).append((String)localObject4);
    int i = 2;
    android.arch.persistence.room.b.a.a((StringBuilder)localObject1, i);
    ((StringBuilder)localObject1).append(")");
    localObject1 = ((StringBuilder)localObject1).toString();
    Object localObject5 = i.a((String)localObject1, i);
    int j = 1;
    int k = 0;
    Object localObject6 = null;
    int m = 1;
    Object localObject7;
    while (k < i)
    {
      localObject7 = paramArrayOfString[k];
      if (localObject7 == null)
      {
        localObject7 = e;
        localObject7[m] = j;
      }
      else
      {
        ((i)localObject5).a(m, (String)localObject7);
      }
      m += 1;
      k += 1;
    }
    localObject6 = this;
    localObject4 = a.a((e)localObject5);
    localObject1 = "_id";
    Object localObject10;
    try
    {
      j = ((Cursor)localObject4).getColumnIndexOrThrow((String)localObject1);
      String str1 = "name";
      m = ((Cursor)localObject4).getColumnIndexOrThrow(str1);
      localObject7 = "amount";
      int n = ((Cursor)localObject4).getColumnIndexOrThrow((String)localObject7);
      String str2 = "disbursed_on";
      int i1 = ((Cursor)localObject4).getColumnIndexOrThrow(str2);
      String str3 = "emis_count";
      int i2 = ((Cursor)localObject4).getColumnIndexOrThrow(str3);
      String str4 = "remaining_emis_count";
      int i3 = ((Cursor)localObject4).getColumnIndexOrThrow(str4);
      String str5 = "next_emi_due_date";
      int i4 = ((Cursor)localObject4).getColumnIndexOrThrow(str5);
      String str6 = "emi_amount";
      int i5 = ((Cursor)localObject4).getColumnIndexOrThrow(str6);
      String str7 = "status";
      int i6 = ((Cursor)localObject4).getColumnIndexOrThrow(str7);
      String str8 = "status_text";
      int i7 = ((Cursor)localObject4).getColumnIndexOrThrow(str8);
      String str9 = "loan_id";
      int i8 = ((Cursor)localObject4).getColumnIndexOrThrow(str9);
      Object localObject8 = "category_id";
      int i9 = ((Cursor)localObject4).getColumnIndexOrThrow((String)localObject8);
      Object localObject9 = "category_name";
      int i12 = ((Cursor)localObject4).getColumnIndexOrThrow((String)localObject9);
      localObject6 = "category_icon";
      k = ((Cursor)localObject4).getColumnIndexOrThrow((String)localObject6);
      localObject10 = localObject5;
      localObject5 = "repayment_link";
      try
      {
        int i13 = ((Cursor)localObject4).getColumnIndexOrThrow((String)localObject5);
        int i14 = i13;
        localObject5 = "repayment_message";
        i13 = ((Cursor)localObject4).getColumnIndexOrThrow((String)localObject5);
        int i15 = i13;
        localObject5 = "disbursed_amount";
        i13 = ((Cursor)localObject4).getColumnIndexOrThrow((String)localObject5);
        int i16 = i13;
        localObject5 = "processing_fee";
        i13 = ((Cursor)localObject4).getColumnIndexOrThrow((String)localObject5);
        int i17 = i13;
        localObject5 = new java/util/ArrayList;
        int i18 = k;
        k = ((Cursor)localObject4).getCount();
        ((ArrayList)localObject5).<init>(k);
        for (;;)
        {
          boolean bool1 = ((Cursor)localObject4).moveToNext();
          if (!bool1) {
            break;
          }
          localObject6 = new com/truecaller/common/payments/a/b;
          ((com.truecaller.common.payments.a.b)localObject6).<init>();
          int i19 = i9;
          int i20 = i12;
          long l = ((Cursor)localObject4).getLong(j);
          a = l;
          localObject8 = ((Cursor)localObject4).getString(m);
          ((com.truecaller.common.payments.a.b)localObject6).a((String)localObject8);
          localObject8 = ((Cursor)localObject4).getString(n);
          ((com.truecaller.common.payments.a.b)localObject6).b((String)localObject8);
          l = ((Cursor)localObject4).getLong(i1);
          d = l;
          boolean bool2 = ((Cursor)localObject4).isNull(i2);
          i12 = 0;
          localObject9 = null;
          if (bool2)
          {
            bool2 = false;
            localObject8 = null;
          }
          else
          {
            int i10 = ((Cursor)localObject4).getInt(i2);
            localObject8 = Integer.valueOf(i10);
          }
          e = ((Integer)localObject8);
          boolean bool3 = ((Cursor)localObject4).isNull(i3);
          if (bool3)
          {
            bool3 = false;
            localObject8 = null;
          }
          else
          {
            int i11 = ((Cursor)localObject4).getInt(i3);
            localObject8 = Integer.valueOf(i11);
          }
          f = ((Integer)localObject8);
          boolean bool4 = ((Cursor)localObject4).isNull(i4);
          if (!bool4)
          {
            l = ((Cursor)localObject4).getLong(i4);
            localObject9 = Long.valueOf(l);
          }
          g = ((Long)localObject9);
          localObject8 = ((Cursor)localObject4).getString(i5);
          h = ((String)localObject8);
          localObject8 = ((Cursor)localObject4).getString(i6);
          ((com.truecaller.common.payments.a.b)localObject6).c((String)localObject8);
          localObject8 = ((Cursor)localObject4).getString(i7);
          ((com.truecaller.common.payments.a.b)localObject6).d((String)localObject8);
          localObject8 = ((Cursor)localObject4).getString(i8);
          ((com.truecaller.common.payments.a.b)localObject6).e((String)localObject8);
          bool4 = i19;
          localObject9 = ((Cursor)localObject4).getString(i19);
          ((com.truecaller.common.payments.a.b)localObject6).f((String)localObject9);
          i19 = j;
          i12 = i20;
          localObject1 = ((Cursor)localObject4).getString(i20);
          ((com.truecaller.common.payments.a.b)localObject6).g((String)localObject1);
          j = i18;
          i18 = m;
          str1 = ((Cursor)localObject4).getString(j);
          ((com.truecaller.common.payments.a.b)localObject6).h(str1);
          m = i14;
          i20 = j;
          localObject1 = ((Cursor)localObject4).getString(i14);
          o = ((String)localObject1);
          j = i15;
          str1 = ((Cursor)localObject4).getString(i15);
          p = str1;
          m = i16;
          localObject1 = ((Cursor)localObject4).getString(i16);
          q = ((String)localObject1);
          j = i17;
          str1 = ((Cursor)localObject4).getString(i17);
          r = str1;
          ((List)localObject5).add(localObject6);
          m = i18;
          j = i19;
          i18 = i20;
        }
        ((Cursor)localObject4).close();
        ((i)localObject10).b();
        return (List)localObject5;
      }
      finally {}
      ((Cursor)localObject4).close();
    }
    finally
    {
      localObject10 = localObject5;
    }
    ((i)localObject10).b();
    throw ((Throwable)localObject3);
  }
  
  public final long[] a(List paramList)
  {
    Object localObject = a;
    ((android.arch.persistence.room.f)localObject).d();
    try
    {
      localObject = b;
      paramList = ((c)localObject).a(paramList);
      localObject = a;
      ((android.arch.persistence.room.f)localObject).f();
      return paramList;
    }
    finally
    {
      a.e();
    }
  }
  
  public final void b(List paramList)
  {
    a.d();
    String str = "loans";
    try
    {
      k.b(paramList, str);
      a();
      a(paramList);
      paramList = a;
      paramList.f();
      return;
    }
    finally
    {
      a.e();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.loanhistory.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */