package com.truecaller.credit.app.ui.loanhistory.a;

import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.credit.data.Result;
import com.truecaller.credit.data.Success;
import com.truecaller.credit.data.repository.CreditRepository;
import com.truecaller.credit.domain.interactors.loanhistory.models.LoanData;
import com.truecaller.credit.domain.interactors.loanhistory.models.LoanHistory;
import com.truecaller.credit.domain.interactors.withdrawloan.models.LoanCategory;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlinx.coroutines.ag;

final class d$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  d$a(d paramd, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/credit/app/ui/loanhistory/a/d$a;
    d locald = b;
    locala.<init>(locald, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = a;
    int j;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      bool = paramObject instanceof o.b;
      if (bool) {
        throw a;
      }
      break;
    case 0: 
      j = paramObject instanceof o.b;
      if (j != 0) {
        break label549;
      }
      paramObject = b.b;
      j = 1;
      a = j;
      paramObject = ((CreditRepository)paramObject).fetchLoanHistory(this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (Result)paramObject;
    boolean bool = paramObject instanceof Success;
    if (bool)
    {
      paramObject = ((LoanHistory)((Success)paramObject).getData()).getLoans();
      localObject1 = new java/util/ArrayList;
      ((ArrayList)localObject1).<init>();
      localObject1 = (List)localObject1;
      paramObject = ((Iterable)paramObject).iterator();
      for (;;)
      {
        j = ((Iterator)paramObject).hasNext();
        if (j == 0) {
          break;
        }
        Object localObject2 = (LoanData)((Iterator)paramObject).next();
        com.truecaller.common.payments.a.b localb = new com/truecaller/common/payments/a/b;
        localb.<init>();
        Object localObject3 = ((LoanData)localObject2).getId();
        localb.e((String)localObject3);
        localObject3 = ((LoanData)localObject2).getName();
        localb.a((String)localObject3);
        localObject3 = ((LoanData)localObject2).getAmount();
        localb.b((String)localObject3);
        long l = Long.parseLong(((LoanData)localObject2).getDisbursed_on());
        d = l;
        localObject3 = ((LoanData)localObject2).getEmis_count();
        Long localLong = null;
        int k;
        if (localObject3 != null)
        {
          k = Integer.parseInt((String)localObject3);
          localObject3 = Integer.valueOf(k);
        }
        else
        {
          k = 0;
          localObject3 = null;
        }
        e = ((Integer)localObject3);
        localObject3 = ((LoanData)localObject2).getRemaining_emis_count();
        if (localObject3 != null)
        {
          k = Integer.parseInt((String)localObject3);
          localObject3 = Integer.valueOf(k);
        }
        else
        {
          k = 0;
          localObject3 = null;
        }
        f = ((Integer)localObject3);
        localObject3 = ((LoanData)localObject2).getNext_emi_due_date();
        if (localObject3 != null)
        {
          l = Long.parseLong((String)localObject3);
          localLong = c.d.b.a.b.a(l);
        }
        g = localLong;
        localObject3 = ((LoanData)localObject2).getEmi_amount();
        h = ((String)localObject3);
        localObject3 = ((LoanData)localObject2).getCategory().getName();
        localb.g((String)localObject3);
        localObject3 = ((LoanData)localObject2).getCategory().getId();
        localb.f((String)localObject3);
        localObject3 = ((LoanData)localObject2).getCategory().getIcon();
        localb.h((String)localObject3);
        localObject3 = ((LoanData)localObject2).getStatus();
        localb.c((String)localObject3);
        localObject3 = ((LoanData)localObject2).getStatus_text();
        localb.d((String)localObject3);
        localObject3 = ((LoanData)localObject2).getRepayment_link();
        o = ((String)localObject3);
        localObject3 = ((LoanData)localObject2).getRepayment_message();
        p = ((String)localObject3);
        localObject3 = ((LoanData)localObject2).getDisbursed_amount();
        q = ((String)localObject3);
        localObject2 = ((LoanData)localObject2).getProcessing_fee();
        r = ((String)localObject2);
        ((List)localObject1).add(localb);
      }
      paramObject = b.a;
      ((a)paramObject).b((List)localObject1);
    }
    return x.a;
    label549:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.loanhistory.a.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */