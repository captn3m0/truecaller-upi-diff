package com.truecaller.credit.app.ui.loanhistory.a;

import android.arch.persistence.room.c;
import android.arch.persistence.room.f;

final class b$1
  extends c
{
  b$1(b paramb, f paramf)
  {
    super(paramf);
  }
  
  public final String a()
  {
    return "INSERT OR REPLACE INTO `loans`(`_id`,`name`,`amount`,`disbursed_on`,`emis_count`,`remaining_emis_count`,`next_emi_due_date`,`emi_amount`,`status`,`status_text`,`loan_id`,`category_id`,`category_name`,`category_icon`,`repayment_link`,`repayment_message`,`disbursed_amount`,`processing_fee`) VALUES (nullif(?, 0),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.loanhistory.a.b.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */