package com.truecaller.credit.app.ui.loanhistory.a;

import dagger.a.d;
import javax.inject.Provider;

public final class e
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  
  private e(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
  }
  
  public static e a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    e locale = new com/truecaller/credit/app/ui/loanhistory/a/e;
    locale.<init>(paramProvider1, paramProvider2, paramProvider3);
    return locale;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.loanhistory.a.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */