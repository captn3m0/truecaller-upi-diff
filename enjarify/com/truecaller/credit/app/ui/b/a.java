package com.truecaller.credit.app.ui.b;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.b;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import c.g.b.k;
import com.truecaller.credit.R.style;
import java.util.HashMap;

public abstract class a
  extends b
{
  private HashMap b;
  public View b_;
  
  public View a(int paramInt)
  {
    Object localObject1 = b;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      b = ((HashMap)localObject1);
    }
    localObject1 = b;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = b;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public abstract void a();
  
  public abstract int b();
  
  public void c()
  {
    HashMap localHashMap = b;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public int getTheme()
  {
    return R.style.BottomSheetDialogTheme;
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    a();
  }
  
  public Dialog onCreateDialog(Bundle paramBundle)
  {
    paramBundle = new android/support/design/widget/a;
    Context localContext = requireContext();
    int i = getTheme();
    paramBundle.<init>(localContext, i);
    return (Dialog)paramBundle;
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    int i = b();
    paramLayoutInflater = paramLayoutInflater.inflate(i, paramViewGroup, false);
    paramViewGroup = "inflater.inflate(getLayoutId(), container, false)";
    k.a(paramLayoutInflater, paramViewGroup);
    b_ = paramLayoutInflater;
    paramLayoutInflater = b_;
    if (paramLayoutInflater == null)
    {
      paramViewGroup = "rootView";
      k.a(paramViewGroup);
    }
    return paramLayoutInflater;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */