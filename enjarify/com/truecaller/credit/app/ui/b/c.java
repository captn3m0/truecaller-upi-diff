package com.truecaller.credit.app.ui.b;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import c.g.b.k;
import com.truecaller.bm;
import java.util.HashMap;

public abstract class c
  extends Fragment
  implements d
{
  public View b;
  private HashMap c;
  public bm c_;
  
  public View a(int paramInt)
  {
    Object localObject1 = c;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      c = ((HashMap)localObject1);
    }
    localObject1 = c;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = c;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final bm a()
  {
    bm localbm = c_;
    if (localbm == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return localbm;
  }
  
  protected abstract void b();
  
  protected abstract int c();
  
  public boolean d()
  {
    return false;
  }
  
  public String e()
  {
    return "";
  }
  
  public void f()
  {
    HashMap localHashMap = c;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    b();
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    int i = c();
    paramLayoutInflater = paramLayoutInflater.inflate(i, paramViewGroup, false);
    paramViewGroup = "inflater.inflate(getLayoutId(), container, false)";
    k.a(paramLayoutInflater, paramViewGroup);
    b = paramLayoutInflater;
    paramLayoutInflater = b;
    if (paramLayoutInflater == null)
    {
      paramViewGroup = "rootView";
      k.a(paramViewGroup);
    }
    return paramLayoutInflater;
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    bm localbm = c_;
    if (localbm == null)
    {
      String str = "presenter";
      k.a(str);
    }
    localbm.y_();
  }
  
  public void onViewCreated(View paramView, Bundle paramBundle)
  {
    String str = "view";
    k.b(paramView, str);
    super.onViewCreated(paramView, paramBundle);
    paramView = c_;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramBundle = this;
    paramBundle = (Object)this;
    paramView.a(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.b.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */