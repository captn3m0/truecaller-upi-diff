package com.truecaller.credit.app.ui.b;

import android.os.Bundle;
import android.support.v4.app.f;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import c.g.b.k;
import com.truecaller.bm;
import java.util.HashMap;

public abstract class b
  extends AppCompatActivity
{
  public bm a_;
  private HashMap b;
  
  public View a(int paramInt)
  {
    Object localObject1 = b;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      b = ((HashMap)localObject1);
    }
    localObject1 = b;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = findViewById(paramInt);
      localObject2 = b;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final bm a()
  {
    bm localbm = a_;
    if (localbm == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return localbm;
  }
  
  public abstract int b();
  
  public abstract void c();
  
  public abstract void d();
  
  public void onCreate(Bundle paramBundle)
  {
    int i = b();
    setContentView(i);
    c();
    super.onCreate(paramBundle);
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    bm localbm = a_;
    if (localbm == null)
    {
      String str = "presenter";
      k.a(str);
    }
    localbm.y_();
  }
  
  public void onPostCreate(Bundle paramBundle)
  {
    super.onPostCreate(paramBundle);
    paramBundle = a_;
    if (paramBundle == null)
    {
      localObject = "presenter";
      k.a((String)localObject);
    }
    Object localObject = this;
    localObject = (Object)this;
    paramBundle.a(localObject);
    d();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */