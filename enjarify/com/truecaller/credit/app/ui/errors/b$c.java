package com.truecaller.credit.app.ui.errors;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.google.gson.l;
import com.google.gson.o;
import kotlinx.coroutines.ag;

final class b$c
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag e;
  
  b$c(b paramb, String paramString, o paramo, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    c localc = new com/truecaller/credit/app/ui/errors/b$c;
    b localb = b;
    String str = c;
    o localo = d;
    localc.<init>(localb, str, localo, paramc);
    paramObject = (ag)paramObject;
    e = ((ag)paramObject);
    return localc;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = CreditErrorHandlerActivity.a;
        paramObject = b.a;
        localObject1 = c;
        if (localObject1 == null)
        {
          localObject1 = d;
          if (localObject1 != null)
          {
            localObject2 = "message";
            localObject1 = ((o)localObject1).b((String)localObject2);
            if (localObject1 != null)
            {
              localObject1 = ((l)localObject1).c();
              break label79;
            }
          }
          bool = false;
          localObject1 = null;
        }
        label79:
        c.g.b.k.b(paramObject, "context");
        Object localObject2 = new android/content/Intent;
        ((Intent)localObject2).<init>((Context)paramObject, CreditErrorHandlerActivity.class);
        Bundle localBundle = new android/os/Bundle;
        localBundle.<init>();
        localBundle.putString("message", (String)localObject1);
        ((Intent)localObject2).setFlags(276824064);
        ((Intent)localObject2).putExtras(localBundle);
        ((Context)paramObject).startActivity((Intent)localObject2);
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (c)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((c)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.errors.b.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */