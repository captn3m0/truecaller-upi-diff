package com.truecaller.credit.app.ui.errors;

import android.content.Context;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.google.gson.l;
import com.google.gson.o;
import com.truecaller.utils.extensions.i;
import kotlinx.coroutines.ag;

final class b$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag e;
  
  b$a(b paramb, String paramString, o paramo, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/credit/app/ui/errors/b$a;
    b localb = b;
    String str = c;
    o localo = d;
    locala.<init>(localb, str, localo, paramc);
    paramObject = (ag)paramObject;
    e = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = b.a;
        localObject = c;
        if (localObject == null)
        {
          localObject = d;
          if (localObject != null)
          {
            String str = "message";
            localObject = ((o)localObject).b(str);
            if (localObject != null)
            {
              localObject = ((l)localObject).c();
              break label75;
            }
          }
          bool = false;
          localObject = null;
        }
        label75:
        localObject = (CharSequence)localObject;
        i.a((Context)paramObject, 0, (CharSequence)localObject, 0, 5);
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.errors.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */