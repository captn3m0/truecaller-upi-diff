package com.truecaller.credit.app.ui.errors;

import android.content.Context;
import c.d.f;
import c.g.a.m;
import com.google.gson.o;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.e;

public final class b
  implements a, ag
{
  final Context a;
  private final f b;
  private final com.truecaller.credit.app.ui.a.a c;
  
  public b(Context paramContext, f paramf, com.truecaller.credit.app.ui.a.a parama)
  {
    a = paramContext;
    b = paramf;
    c = parama;
  }
  
  public final f V_()
  {
    return b;
  }
  
  public final void a(String paramString1, o paramo, String paramString2)
  {
    if (paramString1 == null) {
      return;
    }
    int i = paramString1.hashCode();
    int j = 3;
    String str;
    switch (i)
    {
    default: 
      break;
    case 1124446108: 
      str = "warning";
      bool = paramString1.equals(str);
      if (!bool) {
        return;
      }
      paramString1 = new com/truecaller/credit/app/ui/errors/b$a;
      paramString1.<init>(this, paramString2, paramo, null);
      paramString1 = (m)paramString1;
      e.b(this, null, paramString1, j);
      return;
    case 96784904: 
      str = "error";
      bool = paramString1.equals(str);
      if (!bool) {
        return;
      }
      break;
    case 3237038: 
      str = "info";
      bool = paramString1.equals(str);
      if (!bool) {
        return;
      }
      paramString1 = new com/truecaller/credit/app/ui/errors/b$c;
      paramString1.<init>(this, paramString2, paramo, null);
      paramString1 = (m)paramString1;
      e.b(this, null, paramString1, j);
      return;
    case -43562887: 
      paramo = "validation";
      bool = paramString1.equals(paramo);
      if (!bool) {
        return;
      }
      paramString1 = c;
      paramString1.b();
      break;
    }
    paramo = "no_network_error";
    boolean bool = paramString1.equals(paramo);
    if (bool)
    {
      paramString1 = new com/truecaller/credit/app/ui/errors/b$b;
      paramString1.<init>(this, paramString2, null);
      paramString1 = (m)paramString1;
      e.b(this, null, paramString1, j);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.errors.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */