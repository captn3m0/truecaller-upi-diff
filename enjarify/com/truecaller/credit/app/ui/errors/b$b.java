package com.truecaller.credit.app.ui.errors;

import android.content.Context;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.utils.extensions.i;
import kotlinx.coroutines.ag;

final class b$b
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  b$b(b paramb, String paramString, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/credit/app/ui/errors/b$b;
    b localb1 = b;
    String str = c;
    localb.<init>(localb1, str, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return localb;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = b.a;
        localObject = (CharSequence)c;
        i.a((Context)paramObject, 0, (CharSequence)localObject, 0, 5);
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.errors.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */