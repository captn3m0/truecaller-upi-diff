package com.truecaller.credit.app.ui.errors;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.j;
import android.support.v7.app.AppCompatActivity;
import c.g.b.k;
import com.truecaller.credit.R.layout;
import com.truecaller.credit.R.string;
import com.truecaller.credit.app.ui.infocollection.views.b.a;
import com.truecaller.credit.app.ui.infocollection.views.b.a.a;
import com.truecaller.credit.app.ui.infocollection.views.c.b;
import com.truecaller.credit.data.models.APIStatusMessage;

public final class CreditErrorHandlerActivity
  extends AppCompatActivity
  implements b
{
  public static final CreditErrorHandlerActivity.a a;
  
  static
  {
    CreditErrorHandlerActivity.a locala = new com/truecaller/credit/app/ui/errors/CreditErrorHandlerActivity$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  public final void G_()
  {
    onBackPressed();
  }
  
  public final void H_()
  {
    onBackPressed();
  }
  
  public final void I_()
  {
    onBackPressed();
  }
  
  public final void onBackPressed()
  {
    super.onBackPressed();
    finish();
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    int i = R.layout.activity_credit_error_handler;
    setContentView(i);
    paramBundle = new com/truecaller/credit/data/models/APIStatusMessage;
    int j = R.string.error_some_thing_went_wrong;
    String str1 = getString(j);
    k.a(str1, "getString(R.string.error_some_thing_went_wrong)");
    String str2 = getIntent().getStringExtra("message");
    k.a(str2, "intent.getStringExtra(KEY_MESSAGE)");
    j = R.string.dismiss;
    String str3 = getString(j);
    k.a(str3, "getString(R.string.dismiss)");
    Object localObject = paramBundle;
    paramBundle.<init>(3, str1, str2, false, str3);
    localObject = a.d;
    paramBundle = a.a.a(paramBundle);
    localObject = getSupportFragmentManager();
    paramBundle.show((j)localObject, "APIStatusFragment");
    localObject = this;
    localObject = (b)this;
    b = ((b)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.errors.CreditErrorHandlerActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */