package com.truecaller.credit.app.ui.a;

import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.credit.data.Result;
import com.truecaller.credit.data.Success;
import com.truecaller.credit.data.repository.CreditRepository;
import com.truecaller.credit.domain.interactors.banner.CreditBanner;
import kotlinx.coroutines.ag;

final class b$b
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  b$b(b paramb, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/credit/app/ui/a/b$b;
    b localb1 = b;
    localb.<init>(localb1, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return localb;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = a;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      bool = paramObject instanceof o.b;
      if (bool) {
        throw a;
      }
      break;
    case 0: 
      int j = paramObject instanceof o.b;
      if (j != 0) {
        break label289;
      }
      paramObject = b.a(b);
      j = 1;
      a = j;
      paramObject = ((CreditRepository)paramObject).syncBanner(this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (Result)paramObject;
    boolean bool = paramObject instanceof Success;
    if (bool)
    {
      paramObject = (CreditBanner)((Success)paramObject).getData();
      localObject1 = new com/truecaller/common/payments/a/a;
      ((com.truecaller.common.payments.a.a)localObject1).<init>();
      Object localObject2 = ((CreditBanner)paramObject).getAppState();
      b = ((String)localObject2);
      localObject2 = ((CreditBanner)paramObject).getBannerType();
      c = ((Integer)localObject2);
      localObject2 = ((CreditBanner)paramObject).getTitle();
      d = ((String)localObject2);
      localObject2 = ((CreditBanner)paramObject).getSubTitle();
      e = ((String)localObject2);
      localObject2 = ((CreditBanner)paramObject).getButtonAction();
      f = ((String)localObject2);
      localObject2 = ((CreditBanner)paramObject).getButtonText();
      g = ((String)localObject2);
      localObject2 = ((CreditBanner)paramObject).getHeaderLeft();
      h = ((String)localObject2);
      localObject2 = ((CreditBanner)paramObject).getHeaderRight();
      i = ((String)localObject2);
      localObject2 = ((CreditBanner)paramObject).getProgressType();
      j = ((String)localObject2);
      localObject2 = ((CreditBanner)paramObject).getProgressPercent();
      k = ((Integer)localObject2);
      paramObject = ((CreditBanner)paramObject).getImageUrl();
      l = ((String)paramObject);
      paramObject = b.b(b);
      ((com.truecaller.credit.app.ui.a.a.a)paramObject).b((com.truecaller.common.payments.a.a)localObject1);
    }
    return x.a;
    label289:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.a.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */