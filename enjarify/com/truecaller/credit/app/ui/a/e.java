package com.truecaller.credit.app.ui.a;

import c.d.f;
import c.g.a.m;
import com.truecaller.credit.app.core.a;
import com.truecaller.credit.app.core.g;
import com.truecaller.credit.app.ui.loanhistory.a.c;
import kotlinx.coroutines.ag;

public final class e
  implements d, ag
{
  final a a;
  final c b;
  private final g c;
  private final f d;
  
  public e(a parama, g paramg, c paramc, f paramf)
  {
    a = parama;
    c = paramg;
    b = paramc;
    d = paramf;
  }
  
  public final f V_()
  {
    return d;
  }
  
  public final void a()
  {
    Object localObject = new com/truecaller/credit/app/ui/a/e$a;
    ((e.a)localObject).<init>(this, null);
    localObject = (m)localObject;
    kotlinx.coroutines.e.b(this, null, (m)localObject, 3);
  }
  
  public final void b()
  {
    long l1 = System.currentTimeMillis();
    g localg = c;
    long l2 = System.currentTimeMillis();
    long l3 = localg.a("last_sync_timestamp", l2);
    l1 -= l3;
    localg = c;
    String str = "foreground_sync_interval";
    l2 = 43200000L;
    l3 = localg.a(str, l2);
    boolean bool = l1 < l3;
    if (bool) {
      a();
    }
  }
  
  public final void d()
  {
    c.a("credit_app_state", null);
    a();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.a.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */