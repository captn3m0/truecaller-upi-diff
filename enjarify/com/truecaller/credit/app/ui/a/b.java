package com.truecaller.credit.app.ui.a;

import android.arch.lifecycle.LiveData;
import c.g.a.m;
import c.g.b.t;
import c.g.b.u;
import c.g.b.w;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.e;

public final class b
  implements a, ag
{
  private final c.f b;
  private final com.truecaller.credit.app.ui.a.a.a c;
  private final c.d.f d;
  private final com.truecaller.credit.app.core.g e;
  
  static
  {
    c.l.g[] arrayOfg = new c.l.g[1];
    Object localObject = new c/g/b/u;
    c.l.b localb = w.a(b.class);
    ((u)localObject).<init>(localb, "creditRepository", "getCreditRepository()Lcom/truecaller/credit/data/repository/CreditRepository;");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[0] = localObject;
    a = arrayOfg;
  }
  
  public b(com.truecaller.credit.app.ui.a.a.a parama, c.d.f paramf, com.truecaller.credit.app.core.g paramg)
  {
    c = parama;
    d = paramf;
    e = paramg;
    parama = c.g.a((c.g.a.a)b.a.a);
    b = parama;
  }
  
  public final c.d.f V_()
  {
    return d;
  }
  
  public final Object a()
  {
    com.truecaller.credit.app.core.g localg = e;
    String str = "credit_banner";
    boolean bool = localg.a(str, false);
    if (bool) {
      return c.b();
    }
    return null;
  }
  
  public final void b()
  {
    Object localObject = new com/truecaller/credit/app/ui/a/b$b;
    ((b.b)localObject).<init>(this, null);
    localObject = (m)localObject;
    e.b(this, null, (m)localObject, 3);
  }
  
  public final LiveData d()
  {
    return c.c();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */