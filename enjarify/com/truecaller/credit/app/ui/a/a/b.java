package com.truecaller.credit.app.ui.a.a;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.c;
import android.arch.persistence.room.i;
import android.arch.persistence.room.j;
import android.database.Cursor;
import c.g.b.k;

public final class b
  implements a
{
  final android.arch.persistence.room.f a;
  private final c b;
  private final j c;
  
  public b(android.arch.persistence.room.f paramf)
  {
    a = paramf;
    Object localObject = new com/truecaller/credit/app/ui/a/a/b$1;
    ((b.1)localObject).<init>(this, paramf);
    b = ((c)localObject);
    localObject = new com/truecaller/credit/app/ui/a/a/b$2;
    ((b.2)localObject).<init>(this, paramf);
    c = ((j)localObject);
  }
  
  public final int a()
  {
    android.arch.persistence.db.f localf = c.b();
    android.arch.persistence.room.f localf1 = a;
    localf1.d();
    try
    {
      int i = localf.a();
      android.arch.persistence.room.f localf2 = a;
      localf2.f();
      return i;
    }
    finally
    {
      a.e();
      c.a(localf);
    }
  }
  
  public final long a(com.truecaller.common.payments.a.a parama)
  {
    Object localObject = a;
    ((android.arch.persistence.room.f)localObject).d();
    try
    {
      localObject = b;
      long l = ((c)localObject).b(parama);
      parama = a;
      parama.f();
      return l;
    }
    finally
    {
      a.e();
    }
  }
  
  public final com.truecaller.common.payments.a.a b()
  {
    i locali1 = i.a("Select * from banners LIMIT 1", 0);
    Cursor localCursor = a.a(locali1);
    Object localObject1 = "_id";
    try
    {
      int i = localCursor.getColumnIndexOrThrow((String)localObject1);
      String str1 = "app_state";
      int m = localCursor.getColumnIndexOrThrow(str1);
      String str2 = "banner_type";
      int n = localCursor.getColumnIndexOrThrow(str2);
      String str3 = "title";
      int i1 = localCursor.getColumnIndexOrThrow(str3);
      String str4 = "sub_title";
      int i2 = localCursor.getColumnIndexOrThrow(str4);
      String str5 = "button_action";
      int i3 = localCursor.getColumnIndexOrThrow(str5);
      String str6 = "button_text";
      int i4 = localCursor.getColumnIndexOrThrow(str6);
      String str7 = "header_left";
      int i5 = localCursor.getColumnIndexOrThrow(str7);
      String str8 = "header_right";
      int i6 = localCursor.getColumnIndexOrThrow(str8);
      String str9 = "progress_type";
      int i7 = localCursor.getColumnIndexOrThrow(str9);
      String str10 = "progress_percent";
      int i8 = localCursor.getColumnIndexOrThrow(str10);
      String str11 = "image_url";
      int i9 = localCursor.getColumnIndexOrThrow(str11);
      boolean bool3 = localCursor.moveToFirst();
      Integer localInteger = null;
      if (bool3)
      {
        locala = new com/truecaller/common/payments/a/a;
        locala.<init>();
        locali2 = locali1;
        try
        {
          long l = localCursor.getLong(i);
          a = l;
          localObject1 = localCursor.getString(m);
          b = ((String)localObject1);
          boolean bool1 = localCursor.isNull(n);
          if (bool1)
          {
            bool1 = false;
            localObject1 = null;
          }
          else
          {
            int j = localCursor.getInt(n);
            localObject1 = Integer.valueOf(j);
          }
          c = ((Integer)localObject1);
          localObject1 = localCursor.getString(i1);
          d = ((String)localObject1);
          localObject1 = localCursor.getString(i2);
          e = ((String)localObject1);
          localObject1 = localCursor.getString(i3);
          f = ((String)localObject1);
          localObject1 = localCursor.getString(i4);
          g = ((String)localObject1);
          localObject1 = localCursor.getString(i5);
          h = ((String)localObject1);
          localObject1 = localCursor.getString(i6);
          i = ((String)localObject1);
          localObject1 = localCursor.getString(i7);
          j = ((String)localObject1);
          boolean bool2 = localCursor.isNull(i8);
          if (bool2)
          {
            bool2 = false;
            localObject1 = null;
          }
          else
          {
            int k = localCursor.getInt(i8);
            localInteger = Integer.valueOf(k);
            localObject1 = localInteger;
          }
          k = ((Integer)localObject1);
          localObject1 = localCursor.getString(i9);
          l = ((String)localObject1);
        }
        finally
        {
          break label491;
        }
      }
      locali2 = locali1;
      bool3 = false;
      com.truecaller.common.payments.a.a locala = null;
      localCursor.close();
      locali2.b();
      return locala;
    }
    finally
    {
      i locali2 = locali1;
      label491:
      localCursor.close();
      locali2.b();
    }
  }
  
  public final void b(com.truecaller.common.payments.a.a parama)
  {
    a.d();
    String str = "creditBanner";
    try
    {
      k.b(parama, str);
      a();
      a(parama);
      parama = a;
      parama.f();
      return;
    }
    finally
    {
      a.e();
    }
  }
  
  public final LiveData c()
  {
    i locali = i.a("Select * from banners LIMIT 1", 0);
    b.3 local3 = new com/truecaller/credit/app/ui/a/a/b$3;
    local3.<init>(this, locali);
    return b;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.a.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */