package com.truecaller.credit.app.ui.faq.activities;

import android.graphics.Bitmap;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import c.g.b.k;
import com.truecaller.credit.app.ui.faq.b.a.a;

public final class CreditWebViewActivity$a
  extends WebViewClient
{
  CreditWebViewActivity$a(CreditWebViewActivity paramCreditWebViewActivity) {}
  
  public final void onPageFinished(WebView paramWebView, String paramString)
  {
    k.b(paramString, "url");
    super.onPageFinished(paramWebView, paramString);
    ((a.a)a.a()).b();
  }
  
  public final void onPageStarted(WebView paramWebView, String paramString, Bitmap paramBitmap)
  {
    super.onPageStarted(paramWebView, paramString, paramBitmap);
    ((a.a)a.a()).a();
  }
  
  public final void onReceivedError(WebView paramWebView, WebResourceRequest paramWebResourceRequest, WebResourceError paramWebResourceError)
  {
    super.onReceivedError(paramWebView, paramWebResourceRequest, paramWebResourceError);
    ((a.a)a.a()).c();
  }
  
  public final boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString)
  {
    k.b(paramString, "url");
    return ((a.a)a.a()).a(paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.faq.activities.CreditWebViewActivity.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */