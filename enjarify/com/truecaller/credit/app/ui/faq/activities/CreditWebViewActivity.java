package com.truecaller.credit.app.ui.faq.activities;

import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.f;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import c.g.b.k;
import com.truecaller.credit.R.drawable;
import com.truecaller.credit.R.id;
import com.truecaller.credit.R.layout;
import com.truecaller.credit.app.c.a.a;
import com.truecaller.credit.app.ui.b.b;
import com.truecaller.credit.app.ui.faq.b.a.b;
import com.truecaller.credit.i.a;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;

public final class CreditWebViewActivity
  extends b
  implements a.b
{
  private HashMap b;
  
  public final View a(int paramInt)
  {
    Object localObject1 = b;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      b = ((HashMap)localObject1);
    }
    localObject1 = b;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = findViewById(paramInt);
      localObject2 = b;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "url");
    Intent localIntent = new android/content/Intent;
    paramString = Uri.parse(paramString);
    localIntent.<init>("android.intent.action.SENDTO", paramString);
    startActivity(localIntent);
  }
  
  public final int b()
  {
    return R.layout.activity_credit_webview;
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "url");
    int i = R.id.webViewFaq;
    Object localObject1 = (WebView)a(i);
    k.a(localObject1, "webViewFaq");
    Object localObject2 = new com/truecaller/credit/app/ui/faq/activities/CreditWebViewActivity$a;
    ((CreditWebViewActivity.a)localObject2).<init>(this);
    localObject2 = (WebViewClient)localObject2;
    ((WebView)localObject1).setWebViewClient((WebViewClient)localObject2);
    i = R.id.webViewFaq;
    localObject1 = (WebView)a(i);
    k.a(localObject1, "webViewFaq");
    localObject1 = ((WebView)localObject1).getSettings();
    k.a(localObject1, "webViewFaq.settings");
    ((WebSettings)localObject1).setJavaScriptEnabled(true);
    i = R.id.webViewFaq;
    localObject1 = (WebView)a(i);
    k.a(localObject1, "webViewFaq");
    localObject1 = ((WebView)localObject1).getSettings();
    k.a(localObject1, "webViewFaq.settings");
    ((WebSettings)localObject1).setAllowContentAccess(false);
    i = R.id.webViewFaq;
    ((WebView)a(i)).loadUrl(paramString);
  }
  
  public final void c()
  {
    i.a.a().a(this);
  }
  
  public final void d() {}
  
  public final String e()
  {
    Intent localIntent = getIntent();
    if (localIntent != null) {
      return localIntent.getStringExtra("url");
    }
    return null;
  }
  
  public final boolean f()
  {
    Intent localIntent = getIntent();
    if (localIntent != null) {
      return localIntent.getBooleanExtra("show_toolbar", false);
    }
    return false;
  }
  
  public final void g()
  {
    finish();
  }
  
  public final void h()
  {
    int i = R.id.progressFaq;
    ProgressBar localProgressBar = (ProgressBar)a(i);
    k.a(localProgressBar, "progressFaq");
    t.b((View)localProgressBar);
  }
  
  public final void i()
  {
    int i = R.id.appBarFaq;
    AppBarLayout localAppBarLayout = (AppBarLayout)a(i);
    k.a(localAppBarLayout, "appBarFaq");
    t.b((View)localAppBarLayout);
  }
  
  public final void j()
  {
    int i = R.id.progressFaq;
    ProgressBar localProgressBar = (ProgressBar)a(i);
    k.a(localProgressBar, "progressFaq");
    t.a((View)localProgressBar);
  }
  
  public final void k()
  {
    String str = getIntent().getStringExtra("title");
    int i = R.id.toolbarFaq;
    Object localObject1 = (Toolbar)a(i);
    setSupportActionBar((Toolbar)localObject1);
    localObject1 = getSupportActionBar();
    if (localObject1 != null)
    {
      ((ActionBar)localObject1).setElevation(0.0F);
      localObject2 = str;
      localObject2 = (CharSequence)str;
      ((ActionBar)localObject1).setTitle((CharSequence)localObject2);
      boolean bool = true;
      ((ActionBar)localObject1).setDisplayShowTitleEnabled(bool);
      ((ActionBar)localObject1).setDisplayHomeAsUpEnabled(bool);
      int j = R.drawable.ic_credit_close_white;
      ((ActionBar)localObject1).setHomeAsUpIndicator(j);
    }
    i = R.id.toolbarFaq;
    localObject1 = (Toolbar)a(i);
    Object localObject2 = new com/truecaller/credit/app/ui/faq/activities/CreditWebViewActivity$b;
    ((CreditWebViewActivity.b)localObject2).<init>(this, str);
    localObject2 = (View.OnClickListener)localObject2;
    ((Toolbar)localObject1).setNavigationOnClickListener((View.OnClickListener)localObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.app.ui.faq.activities.CreditWebViewActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */