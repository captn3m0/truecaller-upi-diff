package com.truecaller.credit;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.content.Intent;
import c.a.m;
import c.d.a.a;
import c.d.c;
import c.g.b.k;
import c.o.b;
import com.google.gson.o;
import com.truecaller.common.payments.a.b;
import com.truecaller.credit.app.ui.onboarding.views.activities.InitialOfferActivity;
import com.truecaller.truepay.TcPayCreditLoanItem;
import com.truecaller.truepay.Truepay;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class f
  implements e, j
{
  private final h a;
  
  public f()
  {
    Object localObject = new com/truecaller/credit/i;
    ((i)localObject).<init>();
    localObject = (h)localObject;
    a = ((h)localObject);
  }
  
  public final Object a(c paramc)
  {
    Object localObject1 = this;
    Object localObject2 = paramc;
    boolean bool1 = paramc instanceof f.a;
    if (bool1)
    {
      localObject3 = paramc;
      localObject3 = (f.a)paramc;
      i = b;
      j = -1 << -1;
      i &= j;
      if (i != 0)
      {
        int m = b - j;
        b = m;
        break label83;
      }
    }
    Object localObject3 = new com/truecaller/credit/f$a;
    ((f.a)localObject3).<init>((f)localObject1, (c)localObject2);
    label83:
    localObject2 = a;
    Object localObject4 = a.a;
    int j = b;
    switch (j)
    {
    default: 
      localObject1 = new java/lang/IllegalStateException;
      ((IllegalStateException)localObject1).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)localObject1);
    case 1: 
      bool1 = localObject2 instanceof o.b;
      if (bool1) {
        throw a;
      }
      break;
    case 0: 
      int k = localObject2 instanceof o.b;
      if (k != 0) {
        break label446;
      }
      localObject2 = a;
      d = localObject1;
      k = 1;
      b = k;
      localObject2 = ((h)localObject2).b();
      if (localObject2 == localObject4) {
        return localObject4;
      }
      break;
    }
    localObject2 = (Iterable)localObject2;
    localObject3 = new java/util/ArrayList;
    int i = m.a((Iterable)localObject2, 10);
    ((ArrayList)localObject3).<init>(i);
    localObject3 = (Collection)localObject3;
    localObject2 = ((Iterable)localObject2).iterator();
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject2).hasNext();
      if (!bool2) {
        break;
      }
      localObject4 = (b)((Iterator)localObject2).next();
      Object localObject5 = new com/truecaller/truepay/TcPayCreditLoanItem;
      Object localObject6 = localObject5;
      String str1 = k;
      String str2 = b;
      String str3 = c;
      long l = d;
      Integer localInteger1 = e;
      Integer localInteger2 = f;
      Long localLong = g;
      String str4 = h;
      String str5 = m;
      localObject1 = l;
      paramc = (c)localObject2;
      localObject2 = localObject5;
      localObject5 = localObject1;
      String str6 = n;
      String str7 = i;
      String str8 = j;
      String str9 = o;
      localObject1 = p;
      ((TcPayCreditLoanItem)localObject6).<init>(str1, str2, str3, l, localInteger1, localInteger2, localLong, str4, str5, (String)localObject5, str6, str7, str8, str9, (String)localObject1);
      ((Collection)localObject3).add(localObject6);
      localObject2 = paramc;
      localObject1 = this;
    }
    return (List)localObject3;
    label446:
    throw a;
  }
  
  public final void a()
  {
    a.c();
  }
  
  public final void a(Application paramApplication)
  {
    k.b(paramApplication, "application");
    h localh = a;
    Object localObject = this;
    localObject = (j)this;
    localh.a(paramApplication, (j)localObject);
  }
  
  public final void a(Context paramContext)
  {
    k.b(paramContext, "context");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, InitialOfferActivity.class);
    paramContext.startActivity(localIntent);
  }
  
  public final void a(o paramo)
  {
    k.b(paramo, "notification");
    a.a(paramo);
  }
  
  public final void a(boolean paramBoolean)
  {
    a.a(paramBoolean);
  }
  
  public final void a(boolean paramBoolean, String paramString1, String paramString2)
  {
    a.a(paramBoolean, paramString1, paramString2);
  }
  
  public final void b()
  {
    a.f();
  }
  
  public final void b(Context paramContext)
  {
    k.b(paramContext, "context");
    a.a(paramContext);
  }
  
  public final Object c()
  {
    return a.a();
  }
  
  public final LiveData d()
  {
    return a.g();
  }
  
  public final void e()
  {
    a.d();
  }
  
  public final void f()
  {
    a.e();
  }
  
  public final boolean g()
  {
    Truepay localTruepay = Truepay.getInstance();
    k.a(localTruepay, "Truepay.getInstance()");
    return localTruepay.isRegistrationComplete();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */