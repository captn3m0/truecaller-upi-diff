package com.truecaller.credit;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.content.Context;
import c.g.b.k;
import c.n;
import c.u;
import com.google.gson.o;
import com.truecaller.credit.app.a.a.a;
import com.truecaller.credit.app.c.a.b.a;
import com.truecaller.credit.app.core.g;
import com.truecaller.tcpermissions.c.a;
import com.truecaller.tcpermissions.e;
import com.truecaller.utils.t.a;

public final class i
  implements h
{
  public static com.truecaller.credit.app.c.a.a h;
  public static final i.a i;
  public com.truecaller.credit.app.ui.a.a a;
  public com.truecaller.credit.app.ui.loanhistory.a.c b;
  public com.truecaller.credit.app.notifications.a c;
  public com.truecaller.credit.app.ui.a.d d;
  public com.truecaller.credit.app.a.b e;
  public g f;
  public com.truecaller.credit.app.core.d g;
  
  static
  {
    i.a locala = new com/truecaller/credit/i$a;
    locala.<init>((byte)0);
    i = locala;
  }
  
  public final Object a()
  {
    com.truecaller.credit.app.ui.a.a locala = a;
    if (locala == null)
    {
      String str = "creditBannerManager";
      k.a(str);
    }
    return locala.a();
  }
  
  public final void a(Application paramApplication, j paramj)
  {
    k.b(paramApplication, "application");
    k.b(paramj, "payHelper");
    paramApplication = (Context)paramApplication;
    k.b(paramApplication, "receiver$0");
    k.b(paramj, "payHelper");
    Object localObject1 = paramApplication.getApplicationContext();
    if (localObject1 != null)
    {
      localObject1 = ((com.truecaller.common.b.a)localObject1).u();
      k.a(localObject1, "(applicationContext as A…licationBase).commonGraph");
      Object localObject2 = paramApplication.getApplicationContext();
      if (localObject2 != null)
      {
        localObject2 = ((com.truecaller.common.b.a)localObject2).v();
        k.a(localObject2, "(applicationContext as A…nBase).analyticsComponent");
        com.truecaller.utils.t localt = com.truecaller.utils.c.a().a(paramApplication).a();
        Object localObject3 = com.truecaller.tcpermissions.c.a().a(localt).a((com.truecaller.common.a)localObject1).a();
        localObject1 = com.truecaller.credit.app.c.a.b.v().a((com.truecaller.common.a)localObject1).a(localt);
        localObject3 = (e)localObject3;
        localObject1 = ((b.a)localObject1).a((e)localObject3).a((com.truecaller.analytics.d)localObject2);
        localObject2 = new com/truecaller/credit/app/c/b/a;
        paramApplication = (Application)paramApplication;
        ((com.truecaller.credit.app.c.b.a)localObject2).<init>(paramApplication, paramj);
        paramApplication = ((b.a)localObject1).a((com.truecaller.credit.app.c.b.a)localObject2).a();
        k.a(paramApplication, "DaggerCreditComponent.bu…Helper))\n        .build()");
        paramApplication.a(this);
        h = paramApplication;
        return;
      }
      paramApplication = new c/u;
      paramApplication.<init>("null cannot be cast to non-null type com.truecaller.common.app.ApplicationBase");
      throw paramApplication;
    }
    paramApplication = new c/u;
    paramApplication.<init>("null cannot be cast to non-null type com.truecaller.common.app.ApplicationBase");
    throw paramApplication;
  }
  
  public final void a(Context paramContext)
  {
    k.b(paramContext, "context");
  }
  
  public final void a(o paramo)
  {
    k.b(paramo, "notification");
    com.truecaller.credit.app.notifications.a locala = c;
    if (locala == null)
    {
      String str = "creditNotificationManager";
      k.a(str);
    }
    locala.a(paramo);
  }
  
  public final void a(boolean paramBoolean)
  {
    Object localObject1 = "CreditLoanDetails";
    Object localObject2 = new com/truecaller/credit/app/a/a$a;
    ((a.a)localObject2).<init>((String)localObject1, (String)localObject1);
    int j = 3;
    localObject1 = new n[j];
    String str1 = "Status";
    if (paramBoolean) {
      localObject3 = "clicked";
    } else {
      localObject3 = "shown";
    }
    Object localObject3 = c.t.a(str1, localObject3);
    str1 = null;
    localObject1[0] = localObject3;
    n localn = c.t.a("Context", "home_screen");
    localObject1[1] = localn;
    paramBoolean = true;
    String str2 = "view_details";
    localn = c.t.a("Action", str2);
    localObject1[paramBoolean] = localn;
    ((a.a)localObject2).a((n[])localObject1);
    localObject3 = ((a.a)localObject2).a();
    a = false;
    localObject1 = e;
    if (localObject1 == null)
    {
      localObject2 = "analyticsManager";
      k.a((String)localObject2);
    }
    localObject3 = ((a.a)localObject3).b();
    ((com.truecaller.credit.app.a.b)localObject1).a((com.truecaller.credit.app.a.a)localObject3);
  }
  
  public final void a(boolean paramBoolean, String paramString1, String paramString2)
  {
    Object localObject1 = "CreditBanner";
    a.a locala = new com/truecaller/credit/app/a/a$a;
    locala.<init>((String)localObject1, (String)localObject1);
    int j = 4;
    localObject1 = new n[j];
    String str1 = "Status";
    if (paramBoolean) {
      localObject2 = "clicked";
    } else {
      localObject2 = "shown";
    }
    Object localObject2 = c.t.a(str1, localObject2);
    int k = 0;
    str1 = null;
    localObject1[0] = localObject2;
    localObject2 = "Context";
    Object localObject3 = f;
    if (localObject3 == null)
    {
      str2 = "creditSettings";
      k.a(str2);
    }
    String str2 = "credit_next_page";
    localObject3 = (CharSequence)((g)localObject3).a(str2);
    int m = 1;
    if (localObject3 != null)
    {
      n = ((CharSequence)localObject3).length();
      if (n != 0)
      {
        n = 0;
        localObject3 = null;
        break label147;
      }
    }
    int n = 1;
    label147:
    if (n != 0) {
      localObject3 = "home_screen";
    } else {
      localObject3 = "schedule_meeting";
    }
    localObject2 = c.t.a(localObject2, localObject3);
    localObject1[m] = localObject2;
    localObject3 = "Action";
    paramString1 = c.t.a(localObject3, paramString1);
    localObject1[2] = paramString1;
    paramBoolean = true;
    paramString1 = c.t.a("Type", paramString2);
    localObject1[paramBoolean] = paramString1;
    locala.a((n[])localObject1);
    localObject2 = locala.a();
    a = false;
    paramString1 = f;
    if (paramString1 == null)
    {
      paramString2 = "creditSettings";
      k.a(paramString2);
    }
    paramString2 = "credit_next_page";
    paramString1 = (CharSequence)paramString1.a(paramString2);
    if (paramString1 != null)
    {
      int i1 = paramString1.length();
      if (i1 != 0) {}
    }
    else
    {
      k = 1;
    }
    if (k == 0)
    {
      paramString2 = "reset";
      paramString1 = c.t.a("Custom", paramString2);
      ((a.a)localObject2).a(paramString1);
    }
    paramString1 = e;
    if (paramString1 == null)
    {
      paramString2 = "analyticsManager";
      k.a(paramString2);
    }
    localObject2 = ((a.a)localObject2).b();
    paramString1.a((com.truecaller.credit.app.a.a)localObject2);
  }
  
  public final Object b()
  {
    com.truecaller.credit.app.ui.loanhistory.a.c localc = b;
    if (localc == null)
    {
      String str = "creditLoanHistoryManager";
      k.a(str);
    }
    return localc.b();
  }
  
  public final void c()
  {
    com.truecaller.credit.app.ui.a.d locald = d;
    if (locald == null)
    {
      String str = "featureSyncManager";
      k.a(str);
    }
    locald.a();
  }
  
  public final void d()
  {
    com.truecaller.credit.app.ui.a.d locald = d;
    if (locald == null)
    {
      String str = "featureSyncManager";
      k.a(str);
    }
    locald.b();
  }
  
  public final void e()
  {
    com.truecaller.credit.app.ui.a.d locald = d;
    if (locald == null)
    {
      String str = "featureSyncManager";
      k.a(str);
    }
    locald.d();
  }
  
  public final void f()
  {
    com.truecaller.credit.app.core.d locald = g;
    if (locald == null)
    {
      String str = "creditResetManager";
      k.a(str);
    }
    locald.a();
  }
  
  public final LiveData g()
  {
    com.truecaller.credit.app.ui.a.a locala = a;
    if (locala == null)
    {
      String str = "creditBannerManager";
      k.a(str);
    }
    return locala.d();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */