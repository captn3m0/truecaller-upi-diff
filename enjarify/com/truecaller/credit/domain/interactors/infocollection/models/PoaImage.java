package com.truecaller.credit.domain.interactors.infocollection.models;

import android.net.Uri;
import c.g.b.k;

public final class PoaImage
{
  private final boolean status;
  private final Uri uri;
  
  public PoaImage()
  {
    this(null, false, 3, null);
  }
  
  public PoaImage(Uri paramUri, boolean paramBoolean)
  {
    uri = paramUri;
    status = paramBoolean;
  }
  
  public final Uri component1()
  {
    return uri;
  }
  
  public final boolean component2()
  {
    return status;
  }
  
  public final PoaImage copy(Uri paramUri, boolean paramBoolean)
  {
    k.b(paramUri, "uri");
    PoaImage localPoaImage = new com/truecaller/credit/domain/interactors/infocollection/models/PoaImage;
    localPoaImage.<init>(paramUri, paramBoolean);
    return localPoaImage;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof PoaImage;
      if (bool2)
      {
        paramObject = (PoaImage)paramObject;
        Uri localUri1 = uri;
        Uri localUri2 = uri;
        bool2 = k.a(localUri1, localUri2);
        if (bool2)
        {
          bool2 = status;
          boolean bool3 = status;
          if (bool2 == bool3)
          {
            bool3 = true;
          }
          else
          {
            bool3 = false;
            paramObject = null;
          }
          if (bool3) {
            return bool1;
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final boolean getStatus()
  {
    return status;
  }
  
  public final Uri getUri()
  {
    return uri;
  }
  
  public final int hashCode()
  {
    Uri localUri = uri;
    int i;
    if (localUri != null)
    {
      i = localUri.hashCode();
    }
    else
    {
      i = 0;
      localUri = null;
    }
    i *= 31;
    int j = status;
    if (j != 0) {
      j = 1;
    }
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("PoaImage(uri=");
    Uri localUri = uri;
    localStringBuilder.append(localUri);
    localStringBuilder.append(", status=");
    boolean bool = status;
    localStringBuilder.append(bool);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.domain.interactors.infocollection.models.PoaImage
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */