package com.truecaller.credit.domain.interactors.infocollection.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class IFSCDetails$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    IFSCDetails localIFSCDetails = new com/truecaller/credit/domain/interactors/infocollection/models/IFSCDetails;
    String str1 = paramParcel.readString();
    String str2 = paramParcel.readString();
    String str3 = paramParcel.readString();
    String str4 = paramParcel.readString();
    String str5 = paramParcel.readString();
    String str6 = paramParcel.readString();
    localIFSCDetails.<init>(str1, str2, str3, str4, str5, str6);
    return localIFSCDetails;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new IFSCDetails[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.domain.interactors.infocollection.models.IFSCDetails.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */