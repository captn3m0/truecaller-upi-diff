package com.truecaller.credit.domain.interactors.infocollection.models;

import c.g.b.k;

public final class Slot
{
  private boolean available;
  private boolean selected;
  private String slot;
  
  public Slot(String paramString, boolean paramBoolean1, boolean paramBoolean2)
  {
    slot = paramString;
    available = paramBoolean1;
    selected = paramBoolean2;
  }
  
  public final String component1()
  {
    return slot;
  }
  
  public final boolean component2()
  {
    return available;
  }
  
  public final boolean component3()
  {
    return selected;
  }
  
  public final Slot copy(String paramString, boolean paramBoolean1, boolean paramBoolean2)
  {
    k.b(paramString, "slot");
    Slot localSlot = new com/truecaller/credit/domain/interactors/infocollection/models/Slot;
    localSlot.<init>(paramString, paramBoolean1, paramBoolean2);
    return localSlot;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof Slot;
      if (bool2)
      {
        paramObject = (Slot)paramObject;
        String str1 = slot;
        String str2 = slot;
        bool2 = k.a(str1, str2);
        if (bool2)
        {
          bool2 = available;
          boolean bool3 = available;
          if (bool2 == bool3)
          {
            bool2 = true;
          }
          else
          {
            bool2 = false;
            str1 = null;
          }
          if (bool2)
          {
            bool2 = selected;
            boolean bool4 = selected;
            if (bool2 == bool4)
            {
              bool4 = true;
            }
            else
            {
              bool4 = false;
              paramObject = null;
            }
            if (bool4) {
              return bool1;
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final boolean getAvailable()
  {
    return available;
  }
  
  public final boolean getSelected()
  {
    return selected;
  }
  
  public final String getSlot()
  {
    return slot;
  }
  
  public final int hashCode()
  {
    String str = slot;
    if (str != null)
    {
      i = str.hashCode();
    }
    else
    {
      i = 0;
      str = null;
    }
    i *= 31;
    int j = available;
    if (j != 0) {
      j = 1;
    }
    int i = (i + j) * 31;
    int k = selected;
    if (k != 0) {
      k = 1;
    }
    return i + k;
  }
  
  public final void setAvailable(boolean paramBoolean)
  {
    available = paramBoolean;
  }
  
  public final void setSelected(boolean paramBoolean)
  {
    selected = paramBoolean;
  }
  
  public final void setSlot(String paramString)
  {
    k.b(paramString, "<set-?>");
    slot = paramString;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Slot(slot=");
    String str = slot;
    localStringBuilder.append(str);
    localStringBuilder.append(", available=");
    boolean bool = available;
    localStringBuilder.append(bool);
    localStringBuilder.append(", selected=");
    bool = selected;
    localStringBuilder.append(bool);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.domain.interactors.infocollection.models.Slot
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */