package com.truecaller.credit.domain.interactors.infocollection.models;

import c.g.b.k;
import java.util.List;

public final class IFSCList
{
  private final List results;
  
  public IFSCList(List paramList)
  {
    results = paramList;
  }
  
  public final List component1()
  {
    return results;
  }
  
  public final IFSCList copy(List paramList)
  {
    k.b(paramList, "results");
    IFSCList localIFSCList = new com/truecaller/credit/domain/interactors/infocollection/models/IFSCList;
    localIFSCList.<init>(paramList);
    return localIFSCList;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof IFSCList;
      if (bool1)
      {
        paramObject = (IFSCList)paramObject;
        List localList = results;
        paramObject = results;
        boolean bool2 = k.a(localList, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final List getResults()
  {
    return results;
  }
  
  public final int hashCode()
  {
    List localList = results;
    if (localList != null) {
      return localList.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("IFSCList(results=");
    List localList = results;
    localStringBuilder.append(localList);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.domain.interactors.infocollection.models.IFSCList
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */