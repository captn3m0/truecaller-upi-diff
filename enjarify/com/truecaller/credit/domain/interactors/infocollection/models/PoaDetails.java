package com.truecaller.credit.domain.interactors.infocollection.models;

import c.g.b.k;

public final class PoaDetails
{
  private final String hint;
  private final String name;
  private final String type;
  
  public PoaDetails(String paramString1, String paramString2, String paramString3)
  {
    name = paramString1;
    type = paramString2;
    hint = paramString3;
  }
  
  public final String component1()
  {
    return name;
  }
  
  public final String component2()
  {
    return type;
  }
  
  public final String component3()
  {
    return hint;
  }
  
  public final PoaDetails copy(String paramString1, String paramString2, String paramString3)
  {
    k.b(paramString1, "name");
    k.b(paramString2, "type");
    k.b(paramString3, "hint");
    PoaDetails localPoaDetails = new com/truecaller/credit/domain/interactors/infocollection/models/PoaDetails;
    localPoaDetails.<init>(paramString1, paramString2, paramString3);
    return localPoaDetails;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof PoaDetails;
      if (bool1)
      {
        paramObject = (PoaDetails)paramObject;
        String str1 = name;
        String str2 = name;
        bool1 = k.a(str1, str2);
        if (bool1)
        {
          str1 = type;
          str2 = type;
          bool1 = k.a(str1, str2);
          if (bool1)
          {
            str1 = hint;
            paramObject = hint;
            boolean bool2 = k.a(str1, paramObject);
            if (bool2) {
              break label90;
            }
          }
        }
      }
      return false;
    }
    label90:
    return true;
  }
  
  public final String getHint()
  {
    return hint;
  }
  
  public final String getName()
  {
    return name;
  }
  
  public final String getType()
  {
    return type;
  }
  
  public final int hashCode()
  {
    String str1 = name;
    int i = 0;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = type;
    int k;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    int j = (j + k) * 31;
    str2 = hint;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("PoaDetails(name=");
    String str = name;
    localStringBuilder.append(str);
    localStringBuilder.append(", type=");
    str = type;
    localStringBuilder.append(str);
    localStringBuilder.append(", hint=");
    str = hint;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.domain.interactors.infocollection.models.PoaDetails
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */