package com.truecaller.credit.domain.interactors.infocollection.models;

import c.g.b.k;

public final class KycDetails
{
  private final String id;
  private final String type;
  
  public KycDetails(String paramString1, String paramString2)
  {
    id = paramString1;
    type = paramString2;
  }
  
  public final String component1()
  {
    return id;
  }
  
  public final String component2()
  {
    return type;
  }
  
  public final KycDetails copy(String paramString1, String paramString2)
  {
    KycDetails localKycDetails = new com/truecaller/credit/domain/interactors/infocollection/models/KycDetails;
    localKycDetails.<init>(paramString1, paramString2);
    return localKycDetails;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof KycDetails;
      if (bool1)
      {
        paramObject = (KycDetails)paramObject;
        String str1 = id;
        String str2 = id;
        bool1 = k.a(str1, str2);
        if (bool1)
        {
          str1 = type;
          paramObject = type;
          boolean bool2 = k.a(str1, paramObject);
          if (bool2) {
            break label68;
          }
        }
      }
      return false;
    }
    label68:
    return true;
  }
  
  public final String getId()
  {
    return id;
  }
  
  public final String getType()
  {
    return type;
  }
  
  public final int hashCode()
  {
    String str1 = id;
    int i = 0;
    int j;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = type;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("KycDetails(id=");
    String str = id;
    localStringBuilder.append(str);
    localStringBuilder.append(", type=");
    str = type;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.domain.interactors.infocollection.models.KycDetails
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */