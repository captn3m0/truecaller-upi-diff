package com.truecaller.credit.domain.interactors.infocollection.models;

import c.g.b.k;
import java.util.List;

public final class PoaData
{
  private List images;
  private final String type;
  private final String value;
  
  public PoaData(String paramString1, String paramString2, List paramList)
  {
    type = paramString1;
    value = paramString2;
    images = paramList;
  }
  
  public final String component1()
  {
    return type;
  }
  
  public final String component2()
  {
    return value;
  }
  
  public final List component3()
  {
    return images;
  }
  
  public final PoaData copy(String paramString1, String paramString2, List paramList)
  {
    k.b(paramString1, "type");
    k.b(paramString2, "value");
    PoaData localPoaData = new com/truecaller/credit/domain/interactors/infocollection/models/PoaData;
    localPoaData.<init>(paramString1, paramString2, paramList);
    return localPoaData;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof PoaData;
      if (bool1)
      {
        paramObject = (PoaData)paramObject;
        Object localObject = type;
        String str = type;
        bool1 = k.a(localObject, str);
        if (bool1)
        {
          localObject = value;
          str = value;
          bool1 = k.a(localObject, str);
          if (bool1)
          {
            localObject = images;
            paramObject = images;
            boolean bool2 = k.a(localObject, paramObject);
            if (bool2) {
              break label90;
            }
          }
        }
      }
      return false;
    }
    label90:
    return true;
  }
  
  public final List getImages()
  {
    return images;
  }
  
  public final String getType()
  {
    return type;
  }
  
  public final String getValue()
  {
    return value;
  }
  
  public final int hashCode()
  {
    String str = type;
    int i = 0;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    j *= 31;
    Object localObject = value;
    int k;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    int j = (j + k) * 31;
    localObject = images;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final void setImages(List paramList)
  {
    images = paramList;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("PoaData(type=");
    Object localObject = type;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", value=");
    localObject = value;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", images=");
    localObject = images;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.domain.interactors.infocollection.models.PoaData
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */