package com.truecaller.credit.domain.interactors.infocollection.models;

import c.g.b.k;
import java.util.List;

public final class AppointmentData
{
  private String date;
  private boolean selected;
  private final List slots;
  
  public AppointmentData(String paramString, List paramList, boolean paramBoolean)
  {
    date = paramString;
    slots = paramList;
    selected = paramBoolean;
  }
  
  public final String component1()
  {
    return date;
  }
  
  public final List component2()
  {
    return slots;
  }
  
  public final boolean component3()
  {
    return selected;
  }
  
  public final AppointmentData copy(String paramString, List paramList, boolean paramBoolean)
  {
    k.b(paramString, "date");
    k.b(paramList, "slots");
    AppointmentData localAppointmentData = new com/truecaller/credit/domain/interactors/infocollection/models/AppointmentData;
    localAppointmentData.<init>(paramString, paramList, paramBoolean);
    return localAppointmentData;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof AppointmentData;
      if (bool2)
      {
        paramObject = (AppointmentData)paramObject;
        Object localObject1 = date;
        Object localObject2 = date;
        bool2 = k.a(localObject1, localObject2);
        if (bool2)
        {
          localObject1 = slots;
          localObject2 = slots;
          bool2 = k.a(localObject1, localObject2);
          if (bool2)
          {
            bool2 = selected;
            boolean bool3 = selected;
            if (bool2 == bool3)
            {
              bool3 = true;
            }
            else
            {
              bool3 = false;
              paramObject = null;
            }
            if (bool3) {
              return bool1;
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final String getDate()
  {
    return date;
  }
  
  public final boolean getSelected()
  {
    return selected;
  }
  
  public final List getSlots()
  {
    return slots;
  }
  
  public final int hashCode()
  {
    String str = date;
    int i = 0;
    if (str != null)
    {
      k = str.hashCode();
    }
    else
    {
      k = 0;
      str = null;
    }
    k *= 31;
    List localList = slots;
    if (localList != null) {
      i = localList.hashCode();
    }
    int k = (k + i) * 31;
    int j = selected;
    if (j != 0) {
      j = 1;
    }
    return k + j;
  }
  
  public final void setDate(String paramString)
  {
    k.b(paramString, "<set-?>");
    date = paramString;
  }
  
  public final void setSelected(boolean paramBoolean)
  {
    selected = paramBoolean;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("AppointmentData(date=");
    Object localObject = date;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", slots=");
    localObject = slots;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", selected=");
    boolean bool = selected;
    localStringBuilder.append(bool);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.domain.interactors.infocollection.models.AppointmentData
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */