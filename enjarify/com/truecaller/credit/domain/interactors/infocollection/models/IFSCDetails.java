package com.truecaller.credit.domain.interactors.infocollection.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class IFSCDetails
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  private final String address;
  private final String bank;
  private final String branch;
  private final String city;
  private final String ifsc;
  private final String state;
  
  static
  {
    IFSCDetails.a locala = new com/truecaller/credit/domain/interactors/infocollection/models/IFSCDetails$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public IFSCDetails(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6)
  {
    city = paramString1;
    ifsc = paramString2;
    state = paramString3;
    branch = paramString4;
    address = paramString5;
    bank = paramString6;
  }
  
  public final String component1()
  {
    return city;
  }
  
  public final String component2()
  {
    return ifsc;
  }
  
  public final String component3()
  {
    return state;
  }
  
  public final String component4()
  {
    return branch;
  }
  
  public final String component5()
  {
    return address;
  }
  
  public final String component6()
  {
    return bank;
  }
  
  public final IFSCDetails copy(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6)
  {
    k.b(paramString1, "city");
    k.b(paramString2, "ifsc");
    k.b(paramString3, "state");
    k.b(paramString4, "branch");
    k.b(paramString5, "address");
    k.b(paramString6, "bank");
    IFSCDetails localIFSCDetails = new com/truecaller/credit/domain/interactors/infocollection/models/IFSCDetails;
    localIFSCDetails.<init>(paramString1, paramString2, paramString3, paramString4, paramString5, paramString6);
    return localIFSCDetails;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof IFSCDetails;
      if (bool1)
      {
        paramObject = (IFSCDetails)paramObject;
        String str1 = city;
        String str2 = city;
        bool1 = k.a(str1, str2);
        if (bool1)
        {
          str1 = ifsc;
          str2 = ifsc;
          bool1 = k.a(str1, str2);
          if (bool1)
          {
            str1 = state;
            str2 = state;
            bool1 = k.a(str1, str2);
            if (bool1)
            {
              str1 = branch;
              str2 = branch;
              bool1 = k.a(str1, str2);
              if (bool1)
              {
                str1 = address;
                str2 = address;
                bool1 = k.a(str1, str2);
                if (bool1)
                {
                  str1 = bank;
                  paramObject = bank;
                  boolean bool2 = k.a(str1, paramObject);
                  if (bool2) {
                    break label156;
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    label156:
    return true;
  }
  
  public final String getAddress()
  {
    return address;
  }
  
  public final String getBank()
  {
    return bank;
  }
  
  public final String getBranch()
  {
    return branch;
  }
  
  public final String getCity()
  {
    return city;
  }
  
  public final String getIfsc()
  {
    return ifsc;
  }
  
  public final String getState()
  {
    return state;
  }
  
  public final int hashCode()
  {
    String str1 = city;
    int i = 0;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = ifsc;
    int k;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    int j = (j + k) * 31;
    str2 = state;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    j = (j + k) * 31;
    str2 = branch;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    j = (j + k) * 31;
    str2 = address;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    j = (j + k) * 31;
    str2 = bank;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("IFSCDetails(city=");
    String str = city;
    localStringBuilder.append(str);
    localStringBuilder.append(", ifsc=");
    str = ifsc;
    localStringBuilder.append(str);
    localStringBuilder.append(", state=");
    str = state;
    localStringBuilder.append(str);
    localStringBuilder.append(", branch=");
    str = branch;
    localStringBuilder.append(str);
    localStringBuilder.append(", address=");
    str = address;
    localStringBuilder.append(str);
    localStringBuilder.append(", bank=");
    str = bank;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    String str = city;
    paramParcel.writeString(str);
    str = ifsc;
    paramParcel.writeString(str);
    str = state;
    paramParcel.writeString(str);
    str = branch;
    paramParcel.writeString(str);
    str = address;
    paramParcel.writeString(str);
    str = bank;
    paramParcel.writeString(str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.domain.interactors.infocollection.models.IFSCDetails
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */