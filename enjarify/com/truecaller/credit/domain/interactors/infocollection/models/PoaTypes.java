package com.truecaller.credit.domain.interactors.infocollection.models;

import c.g.b.k;
import java.util.List;

public final class PoaTypes
{
  private final List kyc_types;
  
  public PoaTypes(List paramList)
  {
    kyc_types = paramList;
  }
  
  public final List component1()
  {
    return kyc_types;
  }
  
  public final PoaTypes copy(List paramList)
  {
    k.b(paramList, "kyc_types");
    PoaTypes localPoaTypes = new com/truecaller/credit/domain/interactors/infocollection/models/PoaTypes;
    localPoaTypes.<init>(paramList);
    return localPoaTypes;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof PoaTypes;
      if (bool1)
      {
        paramObject = (PoaTypes)paramObject;
        List localList = kyc_types;
        paramObject = kyc_types;
        boolean bool2 = k.a(localList, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final List getKyc_types()
  {
    return kyc_types;
  }
  
  public final int hashCode()
  {
    List localList = kyc_types;
    if (localList != null) {
      return localList.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("PoaTypes(kyc_types=");
    List localList = kyc_types;
    localStringBuilder.append(localList);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.domain.interactors.infocollection.models.PoaTypes
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */