package com.truecaller.credit.domain.interactors.onboarding.models;

import c.g.b.k;

public final class ScoreDataResult
{
  private final String status;
  
  public ScoreDataResult(String paramString)
  {
    status = paramString;
  }
  
  public final String component1()
  {
    return status;
  }
  
  public final ScoreDataResult copy(String paramString)
  {
    k.b(paramString, "status");
    ScoreDataResult localScoreDataResult = new com/truecaller/credit/domain/interactors/onboarding/models/ScoreDataResult;
    localScoreDataResult.<init>(paramString);
    return localScoreDataResult;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof ScoreDataResult;
      if (bool1)
      {
        paramObject = (ScoreDataResult)paramObject;
        String str = status;
        paramObject = status;
        boolean bool2 = k.a(str, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String getStatus()
  {
    return status;
  }
  
  public final int hashCode()
  {
    String str = status;
    if (str != null) {
      return str.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("ScoreDataResult(status=");
    String str = status;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.domain.interactors.onboarding.models.ScoreDataResult
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */