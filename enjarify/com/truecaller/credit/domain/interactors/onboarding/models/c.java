package com.truecaller.credit.domain.interactors.onboarding.models;

import c.g.b.k;

public final class c
{
  private final Integer a;
  
  public c(Integer paramInteger)
  {
    a = paramInteger;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof c;
      if (bool1)
      {
        paramObject = (c)paramObject;
        Integer localInteger = a;
        paramObject = a;
        boolean bool2 = k.a(localInteger, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final int hashCode()
  {
    Integer localInteger = a;
    if (localInteger != null) {
      return localInteger.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("PreScoreDataResult(preScore=");
    Integer localInteger = a;
    localStringBuilder.append(localInteger);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.domain.interactors.onboarding.models.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */