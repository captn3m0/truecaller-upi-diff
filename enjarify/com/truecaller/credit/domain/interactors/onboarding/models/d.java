package com.truecaller.credit.domain.interactors.onboarding.models;

import c.g.b.k;
import java.util.List;

public final class d
{
  public long a;
  public final List b;
  
  public d(List paramList)
  {
    b = paramList;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof d;
      if (bool1)
      {
        paramObject = (d)paramObject;
        List localList = b;
        paramObject = b;
        boolean bool2 = k.a(localList, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final int hashCode()
  {
    List localList = b;
    if (localList != null) {
      return localList.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("SupportedCities(cities=");
    List localList = b;
    localStringBuilder.append(localList);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.domain.interactors.onboarding.models.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */