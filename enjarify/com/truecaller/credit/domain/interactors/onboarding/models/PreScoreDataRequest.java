package com.truecaller.credit.domain.interactors.onboarding.models;

import c.g.b.k;
import com.google.gson.o;

public final class PreScoreDataRequest
{
  private final o data;
  private final boolean tc_pay_user;
  
  public PreScoreDataRequest(o paramo, boolean paramBoolean)
  {
    data = paramo;
    tc_pay_user = paramBoolean;
  }
  
  public final o component1()
  {
    return data;
  }
  
  public final boolean component2()
  {
    return tc_pay_user;
  }
  
  public final PreScoreDataRequest copy(o paramo, boolean paramBoolean)
  {
    k.b(paramo, "data");
    PreScoreDataRequest localPreScoreDataRequest = new com/truecaller/credit/domain/interactors/onboarding/models/PreScoreDataRequest;
    localPreScoreDataRequest.<init>(paramo, paramBoolean);
    return localPreScoreDataRequest;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof PreScoreDataRequest;
      if (bool2)
      {
        paramObject = (PreScoreDataRequest)paramObject;
        o localo1 = data;
        o localo2 = data;
        bool2 = k.a(localo1, localo2);
        if (bool2)
        {
          bool2 = tc_pay_user;
          boolean bool3 = tc_pay_user;
          if (bool2 == bool3)
          {
            bool3 = true;
          }
          else
          {
            bool3 = false;
            paramObject = null;
          }
          if (bool3) {
            return bool1;
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final o getData()
  {
    return data;
  }
  
  public final boolean getTc_pay_user()
  {
    return tc_pay_user;
  }
  
  public final int hashCode()
  {
    o localo = data;
    int i;
    if (localo != null)
    {
      i = localo.hashCode();
    }
    else
    {
      i = 0;
      localo = null;
    }
    i *= 31;
    int j = tc_pay_user;
    if (j != 0) {
      j = 1;
    }
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("PreScoreDataRequest(data=");
    o localo = data;
    localStringBuilder.append(localo);
    localStringBuilder.append(", tc_pay_user=");
    boolean bool = tc_pay_user;
    localStringBuilder.append(bool);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.domain.interactors.onboarding.models.PreScoreDataRequest
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */