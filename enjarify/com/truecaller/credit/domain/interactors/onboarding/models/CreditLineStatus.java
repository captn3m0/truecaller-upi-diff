package com.truecaller.credit.domain.interactors.onboarding.models;

import c.g.b.k;

public final class CreditLineStatus
{
  private final String amount;
  private final String id;
  private final String interest_rate;
  private final String status;
  
  public CreditLineStatus(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    id = paramString1;
    status = paramString2;
    amount = paramString3;
    interest_rate = paramString4;
  }
  
  public final String component1()
  {
    return id;
  }
  
  public final String component2()
  {
    return status;
  }
  
  public final String component3()
  {
    return amount;
  }
  
  public final String component4()
  {
    return interest_rate;
  }
  
  public final CreditLineStatus copy(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    CreditLineStatus localCreditLineStatus = new com/truecaller/credit/domain/interactors/onboarding/models/CreditLineStatus;
    localCreditLineStatus.<init>(paramString1, paramString2, paramString3, paramString4);
    return localCreditLineStatus;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof CreditLineStatus;
      if (bool1)
      {
        paramObject = (CreditLineStatus)paramObject;
        String str1 = id;
        String str2 = id;
        bool1 = k.a(str1, str2);
        if (bool1)
        {
          str1 = status;
          str2 = status;
          bool1 = k.a(str1, str2);
          if (bool1)
          {
            str1 = amount;
            str2 = amount;
            bool1 = k.a(str1, str2);
            if (bool1)
            {
              str1 = interest_rate;
              paramObject = interest_rate;
              boolean bool2 = k.a(str1, paramObject);
              if (bool2) {
                break label112;
              }
            }
          }
        }
      }
      return false;
    }
    label112:
    return true;
  }
  
  public final String getAmount()
  {
    return amount;
  }
  
  public final String getId()
  {
    return id;
  }
  
  public final String getInterest_rate()
  {
    return interest_rate;
  }
  
  public final String getStatus()
  {
    return status;
  }
  
  public final int hashCode()
  {
    String str1 = id;
    int i = 0;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = status;
    int k;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    int j = (j + k) * 31;
    str2 = amount;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    j = (j + k) * 31;
    str2 = interest_rate;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("CreditLineStatus(id=");
    String str = id;
    localStringBuilder.append(str);
    localStringBuilder.append(", status=");
    str = status;
    localStringBuilder.append(str);
    localStringBuilder.append(", amount=");
    str = amount;
    localStringBuilder.append(str);
    localStringBuilder.append(", interest_rate=");
    str = interest_rate;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.domain.interactors.onboarding.models.CreditLineStatus
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */