package com.truecaller.credit.domain.interactors.onboarding.models;

import c.g.b.k;

public final class a
{
  public final int a;
  public final String b;
  public final String c;
  public final String d;
  
  public a(int paramInt, String paramString1, String paramString2, String paramString3)
  {
    a = paramInt;
    b = paramString1;
    c = paramString2;
    d = paramString3;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof a;
      if (bool2)
      {
        paramObject = (a)paramObject;
        int i = a;
        int j = a;
        String str1;
        if (i == j)
        {
          i = 1;
        }
        else
        {
          i = 0;
          str1 = null;
        }
        if (i != 0)
        {
          str1 = b;
          String str2 = b;
          boolean bool3 = k.a(str1, str2);
          if (bool3)
          {
            str1 = c;
            str2 = c;
            bool3 = k.a(str1, str2);
            if (bool3)
            {
              str1 = d;
              paramObject = d;
              boolean bool4 = k.a(str1, paramObject);
              if (bool4) {
                return bool1;
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    int i = a * 31;
    String str = b;
    int j = 0;
    int k;
    if (str != null)
    {
      k = str.hashCode();
    }
    else
    {
      k = 0;
      str = null;
    }
    i = (i + k) * 31;
    str = c;
    if (str != null)
    {
      k = str.hashCode();
    }
    else
    {
      k = 0;
      str = null;
    }
    i = (i + k) * 31;
    str = d;
    if (str != null) {
      j = str.hashCode();
    }
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("FinalOfferDetails(finalOffer=");
    int i = a;
    localStringBuilder.append(i);
    localStringBuilder.append(", interestRate=");
    String str = b;
    localStringBuilder.append(str);
    localStringBuilder.append(", tenure=");
    str = c;
    localStringBuilder.append(str);
    localStringBuilder.append(", terms=");
    str = d;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.domain.interactors.onboarding.models.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */