package com.truecaller.credit.domain.interactors.withdrawloan.models;

import c.g.b.k;
import java.util.List;

public final class LoanCategories
{
  private final List categories;
  
  public LoanCategories(List paramList)
  {
    categories = paramList;
  }
  
  public final List component1()
  {
    return categories;
  }
  
  public final LoanCategories copy(List paramList)
  {
    k.b(paramList, "categories");
    LoanCategories localLoanCategories = new com/truecaller/credit/domain/interactors/withdrawloan/models/LoanCategories;
    localLoanCategories.<init>(paramList);
    return localLoanCategories;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof LoanCategories;
      if (bool1)
      {
        paramObject = (LoanCategories)paramObject;
        List localList = categories;
        paramObject = categories;
        boolean bool2 = k.a(localList, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final List getCategories()
  {
    return categories;
  }
  
  public final int hashCode()
  {
    List localList = categories;
    if (localList != null) {
      return localList.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("LoanCategories(categories=");
    List localList = categories;
    localStringBuilder.append(localList);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.domain.interactors.withdrawloan.models.LoanCategories
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */