package com.truecaller.credit.domain.interactors.withdrawloan.models;

import c.g.b.k;

public final class CreditLineDetails
{
  private final String available_amount;
  private final String minimum_amount;
  private final String title;
  private final String total_amount;
  
  public CreditLineDetails(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    total_amount = paramString1;
    available_amount = paramString2;
    minimum_amount = paramString3;
    title = paramString4;
  }
  
  public final String component1()
  {
    return total_amount;
  }
  
  public final String component2()
  {
    return available_amount;
  }
  
  public final String component3()
  {
    return minimum_amount;
  }
  
  public final String component4()
  {
    return title;
  }
  
  public final CreditLineDetails copy(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    k.b(paramString1, "total_amount");
    k.b(paramString2, "available_amount");
    k.b(paramString3, "minimum_amount");
    k.b(paramString4, "title");
    CreditLineDetails localCreditLineDetails = new com/truecaller/credit/domain/interactors/withdrawloan/models/CreditLineDetails;
    localCreditLineDetails.<init>(paramString1, paramString2, paramString3, paramString4);
    return localCreditLineDetails;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof CreditLineDetails;
      if (bool1)
      {
        paramObject = (CreditLineDetails)paramObject;
        String str1 = total_amount;
        String str2 = total_amount;
        bool1 = k.a(str1, str2);
        if (bool1)
        {
          str1 = available_amount;
          str2 = available_amount;
          bool1 = k.a(str1, str2);
          if (bool1)
          {
            str1 = minimum_amount;
            str2 = minimum_amount;
            bool1 = k.a(str1, str2);
            if (bool1)
            {
              str1 = title;
              paramObject = title;
              boolean bool2 = k.a(str1, paramObject);
              if (bool2) {
                break label112;
              }
            }
          }
        }
      }
      return false;
    }
    label112:
    return true;
  }
  
  public final String getAvailable_amount()
  {
    return available_amount;
  }
  
  public final String getMinimum_amount()
  {
    return minimum_amount;
  }
  
  public final String getTitle()
  {
    return title;
  }
  
  public final String getTotal_amount()
  {
    return total_amount;
  }
  
  public final int hashCode()
  {
    String str1 = total_amount;
    int i = 0;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = available_amount;
    int k;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    int j = (j + k) * 31;
    str2 = minimum_amount;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    j = (j + k) * 31;
    str2 = title;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("CreditLineDetails(total_amount=");
    String str = total_amount;
    localStringBuilder.append(str);
    localStringBuilder.append(", available_amount=");
    str = available_amount;
    localStringBuilder.append(str);
    localStringBuilder.append(", minimum_amount=");
    str = minimum_amount;
    localStringBuilder.append(str);
    localStringBuilder.append(", title=");
    str = title;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.domain.interactors.withdrawloan.models.CreditLineDetails
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */