package com.truecaller.credit.domain.interactors.withdrawloan.models;

import c.g.b.k;

public final class LoanCategory
{
  private final String icon;
  private final String id;
  private final String name;
  
  public LoanCategory(String paramString1, String paramString2, String paramString3)
  {
    id = paramString1;
    name = paramString2;
    icon = paramString3;
  }
  
  public final String component1()
  {
    return id;
  }
  
  public final String component2()
  {
    return name;
  }
  
  public final String component3()
  {
    return icon;
  }
  
  public final LoanCategory copy(String paramString1, String paramString2, String paramString3)
  {
    k.b(paramString1, "id");
    k.b(paramString2, "name");
    k.b(paramString3, "icon");
    LoanCategory localLoanCategory = new com/truecaller/credit/domain/interactors/withdrawloan/models/LoanCategory;
    localLoanCategory.<init>(paramString1, paramString2, paramString3);
    return localLoanCategory;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof LoanCategory;
      if (bool1)
      {
        paramObject = (LoanCategory)paramObject;
        String str1 = id;
        String str2 = id;
        bool1 = k.a(str1, str2);
        if (bool1)
        {
          str1 = name;
          str2 = name;
          bool1 = k.a(str1, str2);
          if (bool1)
          {
            str1 = icon;
            paramObject = icon;
            boolean bool2 = k.a(str1, paramObject);
            if (bool2) {
              break label90;
            }
          }
        }
      }
      return false;
    }
    label90:
    return true;
  }
  
  public final String getIcon()
  {
    return icon;
  }
  
  public final String getId()
  {
    return id;
  }
  
  public final String getName()
  {
    return name;
  }
  
  public final int hashCode()
  {
    String str1 = id;
    int i = 0;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = name;
    int k;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    int j = (j + k) * 31;
    str2 = icon;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("LoanCategory(id=");
    String str = id;
    localStringBuilder.append(str);
    localStringBuilder.append(", name=");
    str = name;
    localStringBuilder.append(str);
    localStringBuilder.append(", icon=");
    str = icon;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.domain.interactors.withdrawloan.models.LoanCategory
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */