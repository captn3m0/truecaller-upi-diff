package com.truecaller.credit.domain.interactors.withdrawloan.models;

import c.g.b.k;

public final class LoanDetails
{
  private final String amount;
  private final LoanCategory category;
  private final LoanDisplayData display_configuration;
  private final String id;
  private final String name;
  private final String status;
  private final String tenure;
  private final String tenure_type;
  private final String vendor_uuid;
  
  public LoanDetails(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, LoanCategory paramLoanCategory, String paramString6, String paramString7, LoanDisplayData paramLoanDisplayData)
  {
    id = paramString1;
    name = paramString2;
    status = paramString3;
    tenure = paramString4;
    amount = paramString5;
    category = paramLoanCategory;
    tenure_type = paramString6;
    vendor_uuid = paramString7;
    display_configuration = paramLoanDisplayData;
  }
  
  public final String component1()
  {
    return id;
  }
  
  public final String component2()
  {
    return name;
  }
  
  public final String component3()
  {
    return status;
  }
  
  public final String component4()
  {
    return tenure;
  }
  
  public final String component5()
  {
    return amount;
  }
  
  public final LoanCategory component6()
  {
    return category;
  }
  
  public final String component7()
  {
    return tenure_type;
  }
  
  public final String component8()
  {
    return vendor_uuid;
  }
  
  public final LoanDisplayData component9()
  {
    return display_configuration;
  }
  
  public final LoanDetails copy(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, LoanCategory paramLoanCategory, String paramString6, String paramString7, LoanDisplayData paramLoanDisplayData)
  {
    k.b(paramString1, "id");
    k.b(paramString2, "name");
    k.b(paramString3, "status");
    k.b(paramString4, "tenure");
    k.b(paramString5, "amount");
    k.b(paramLoanCategory, "category");
    k.b(paramString6, "tenure_type");
    k.b(paramString7, "vendor_uuid");
    LoanDetails localLoanDetails = new com/truecaller/credit/domain/interactors/withdrawloan/models/LoanDetails;
    localLoanDetails.<init>(paramString1, paramString2, paramString3, paramString4, paramString5, paramLoanCategory, paramString6, paramString7, paramLoanDisplayData);
    return localLoanDetails;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof LoanDetails;
      if (bool1)
      {
        paramObject = (LoanDetails)paramObject;
        Object localObject1 = id;
        Object localObject2 = id;
        bool1 = k.a(localObject1, localObject2);
        if (bool1)
        {
          localObject1 = name;
          localObject2 = name;
          bool1 = k.a(localObject1, localObject2);
          if (bool1)
          {
            localObject1 = status;
            localObject2 = status;
            bool1 = k.a(localObject1, localObject2);
            if (bool1)
            {
              localObject1 = tenure;
              localObject2 = tenure;
              bool1 = k.a(localObject1, localObject2);
              if (bool1)
              {
                localObject1 = amount;
                localObject2 = amount;
                bool1 = k.a(localObject1, localObject2);
                if (bool1)
                {
                  localObject1 = category;
                  localObject2 = category;
                  bool1 = k.a(localObject1, localObject2);
                  if (bool1)
                  {
                    localObject1 = tenure_type;
                    localObject2 = tenure_type;
                    bool1 = k.a(localObject1, localObject2);
                    if (bool1)
                    {
                      localObject1 = vendor_uuid;
                      localObject2 = vendor_uuid;
                      bool1 = k.a(localObject1, localObject2);
                      if (bool1)
                      {
                        localObject1 = display_configuration;
                        paramObject = display_configuration;
                        boolean bool2 = k.a(localObject1, paramObject);
                        if (bool2) {
                          break label222;
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    label222:
    return true;
  }
  
  public final String getAmount()
  {
    return amount;
  }
  
  public final LoanCategory getCategory()
  {
    return category;
  }
  
  public final LoanDisplayData getDisplay_configuration()
  {
    return display_configuration;
  }
  
  public final String getId()
  {
    return id;
  }
  
  public final String getName()
  {
    return name;
  }
  
  public final String getStatus()
  {
    return status;
  }
  
  public final String getTenure()
  {
    return tenure;
  }
  
  public final String getTenure_type()
  {
    return tenure_type;
  }
  
  public final String getVendor_uuid()
  {
    return vendor_uuid;
  }
  
  public final int hashCode()
  {
    String str = id;
    int i = 0;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    j *= 31;
    Object localObject = name;
    int k;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    int j = (j + k) * 31;
    localObject = status;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = tenure;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = amount;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = category;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = tenure_type;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = vendor_uuid;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = display_configuration;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("LoanDetails(id=");
    Object localObject = id;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", name=");
    localObject = name;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", status=");
    localObject = status;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", tenure=");
    localObject = tenure;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", amount=");
    localObject = amount;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", category=");
    localObject = category;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", tenure_type=");
    localObject = tenure_type;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", vendor_uuid=");
    localObject = vendor_uuid;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", display_configuration=");
    localObject = display_configuration;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.domain.interactors.withdrawloan.models.LoanDetails
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */