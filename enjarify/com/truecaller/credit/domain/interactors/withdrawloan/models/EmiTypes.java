package com.truecaller.credit.domain.interactors.withdrawloan.models;

import c.g.b.k;
import java.util.List;

public final class EmiTypes
{
  private final String description;
  private final List emis;
  private final String interest_rate;
  
  public EmiTypes(List paramList, String paramString1, String paramString2)
  {
    emis = paramList;
    interest_rate = paramString1;
    description = paramString2;
  }
  
  public final List component1()
  {
    return emis;
  }
  
  public final String component2()
  {
    return interest_rate;
  }
  
  public final String component3()
  {
    return description;
  }
  
  public final EmiTypes copy(List paramList, String paramString1, String paramString2)
  {
    k.b(paramList, "emis");
    k.b(paramString1, "interest_rate");
    k.b(paramString2, "description");
    EmiTypes localEmiTypes = new com/truecaller/credit/domain/interactors/withdrawloan/models/EmiTypes;
    localEmiTypes.<init>(paramList, paramString1, paramString2);
    return localEmiTypes;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof EmiTypes;
      if (bool1)
      {
        paramObject = (EmiTypes)paramObject;
        Object localObject1 = emis;
        Object localObject2 = emis;
        bool1 = k.a(localObject1, localObject2);
        if (bool1)
        {
          localObject1 = interest_rate;
          localObject2 = interest_rate;
          bool1 = k.a(localObject1, localObject2);
          if (bool1)
          {
            localObject1 = description;
            paramObject = description;
            boolean bool2 = k.a(localObject1, paramObject);
            if (bool2) {
              break label90;
            }
          }
        }
      }
      return false;
    }
    label90:
    return true;
  }
  
  public final String getDescription()
  {
    return description;
  }
  
  public final List getEmis()
  {
    return emis;
  }
  
  public final String getInterest_rate()
  {
    return interest_rate;
  }
  
  public final int hashCode()
  {
    List localList = emis;
    int i = 0;
    if (localList != null)
    {
      j = localList.hashCode();
    }
    else
    {
      j = 0;
      localList = null;
    }
    j *= 31;
    String str = interest_rate;
    int k;
    if (str != null)
    {
      k = str.hashCode();
    }
    else
    {
      k = 0;
      str = null;
    }
    int j = (j + k) * 31;
    str = description;
    if (str != null) {
      i = str.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("EmiTypes(emis=");
    Object localObject = emis;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", interest_rate=");
    localObject = interest_rate;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", description=");
    localObject = description;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.domain.interactors.withdrawloan.models.EmiTypes
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */