package com.truecaller.credit.domain.interactors.withdrawloan.models;

import c.g.b.k;

public final class LoanDisplayData
{
  private final String description;
  private final String title;
  
  public LoanDisplayData(String paramString1, String paramString2)
  {
    title = paramString1;
    description = paramString2;
  }
  
  public final String component1()
  {
    return title;
  }
  
  public final String component2()
  {
    return description;
  }
  
  public final LoanDisplayData copy(String paramString1, String paramString2)
  {
    k.b(paramString1, "title");
    k.b(paramString2, "description");
    LoanDisplayData localLoanDisplayData = new com/truecaller/credit/domain/interactors/withdrawloan/models/LoanDisplayData;
    localLoanDisplayData.<init>(paramString1, paramString2);
    return localLoanDisplayData;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof LoanDisplayData;
      if (bool1)
      {
        paramObject = (LoanDisplayData)paramObject;
        String str1 = title;
        String str2 = title;
        bool1 = k.a(str1, str2);
        if (bool1)
        {
          str1 = description;
          paramObject = description;
          boolean bool2 = k.a(str1, paramObject);
          if (bool2) {
            break label68;
          }
        }
      }
      return false;
    }
    label68:
    return true;
  }
  
  public final String getDescription()
  {
    return description;
  }
  
  public final String getTitle()
  {
    return title;
  }
  
  public final int hashCode()
  {
    String str1 = title;
    int i = 0;
    int j;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = description;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("LoanDisplayData(title=");
    String str = title;
    localStringBuilder.append(str);
    localStringBuilder.append(", description=");
    str = description;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.domain.interactors.withdrawloan.models.LoanDisplayData
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */