package com.truecaller.credit.domain.interactors.withdrawloan.models;

import c.g.b.k;

public final class Emi
{
  private final boolean available;
  private final String emi_amount;
  private final String tenure;
  private final String tenure_type;
  
  public Emi(String paramString1, String paramString2, String paramString3, boolean paramBoolean)
  {
    tenure = paramString1;
    emi_amount = paramString2;
    tenure_type = paramString3;
    available = paramBoolean;
  }
  
  public final String component1()
  {
    return tenure;
  }
  
  public final String component2()
  {
    return emi_amount;
  }
  
  public final String component3()
  {
    return tenure_type;
  }
  
  public final boolean component4()
  {
    return available;
  }
  
  public final Emi copy(String paramString1, String paramString2, String paramString3, boolean paramBoolean)
  {
    k.b(paramString1, "tenure");
    k.b(paramString2, "emi_amount");
    k.b(paramString3, "tenure_type");
    Emi localEmi = new com/truecaller/credit/domain/interactors/withdrawloan/models/Emi;
    localEmi.<init>(paramString1, paramString2, paramString3, paramBoolean);
    return localEmi;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof Emi;
      if (bool2)
      {
        paramObject = (Emi)paramObject;
        String str1 = tenure;
        String str2 = tenure;
        bool2 = k.a(str1, str2);
        if (bool2)
        {
          str1 = emi_amount;
          str2 = emi_amount;
          bool2 = k.a(str1, str2);
          if (bool2)
          {
            str1 = tenure_type;
            str2 = tenure_type;
            bool2 = k.a(str1, str2);
            if (bool2)
            {
              bool2 = available;
              boolean bool3 = available;
              if (bool2 == bool3)
              {
                bool3 = true;
              }
              else
              {
                bool3 = false;
                paramObject = null;
              }
              if (bool3) {
                return bool1;
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final boolean getAvailable()
  {
    return available;
  }
  
  public final String getEmi_amount()
  {
    return emi_amount;
  }
  
  public final String getTenure()
  {
    return tenure;
  }
  
  public final String getTenure_type()
  {
    return tenure_type;
  }
  
  public final int hashCode()
  {
    String str1 = tenure;
    int i = 0;
    if (str1 != null)
    {
      k = str1.hashCode();
    }
    else
    {
      k = 0;
      str1 = null;
    }
    k *= 31;
    String str2 = emi_amount;
    int m;
    if (str2 != null)
    {
      m = str2.hashCode();
    }
    else
    {
      m = 0;
      str2 = null;
    }
    int k = (k + m) * 31;
    str2 = tenure_type;
    if (str2 != null) {
      i = str2.hashCode();
    }
    k = (k + i) * 31;
    int j = available;
    if (j != 0) {
      j = 1;
    }
    return k + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Emi(tenure=");
    String str = tenure;
    localStringBuilder.append(str);
    localStringBuilder.append(", emi_amount=");
    str = emi_amount;
    localStringBuilder.append(str);
    localStringBuilder.append(", tenure_type=");
    str = tenure_type;
    localStringBuilder.append(str);
    localStringBuilder.append(", available=");
    boolean bool = available;
    localStringBuilder.append(bool);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.domain.interactors.withdrawloan.models.Emi
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */