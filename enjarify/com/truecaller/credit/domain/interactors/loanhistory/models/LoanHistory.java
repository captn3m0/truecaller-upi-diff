package com.truecaller.credit.domain.interactors.loanhistory.models;

import c.g.b.k;
import java.util.List;

public final class LoanHistory
{
  private final List loans;
  
  public LoanHistory(List paramList)
  {
    loans = paramList;
  }
  
  public final List component1()
  {
    return loans;
  }
  
  public final LoanHistory copy(List paramList)
  {
    k.b(paramList, "loans");
    LoanHistory localLoanHistory = new com/truecaller/credit/domain/interactors/loanhistory/models/LoanHistory;
    localLoanHistory.<init>(paramList);
    return localLoanHistory;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof LoanHistory;
      if (bool1)
      {
        paramObject = (LoanHistory)paramObject;
        List localList = loans;
        paramObject = loans;
        boolean bool2 = k.a(localList, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final List getLoans()
  {
    return loans;
  }
  
  public final int hashCode()
  {
    List localList = loans;
    if (localList != null) {
      return localList.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("LoanHistory(loans=");
    List localList = loans;
    localStringBuilder.append(localList);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.domain.interactors.loanhistory.models.LoanHistory
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */