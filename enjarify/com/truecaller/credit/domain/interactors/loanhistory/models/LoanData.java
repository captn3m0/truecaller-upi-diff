package com.truecaller.credit.domain.interactors.loanhistory.models;

import c.g.b.k;
import com.truecaller.credit.domain.interactors.withdrawloan.models.LoanCategory;

public final class LoanData
{
  private final String amount;
  private final LoanCategory category;
  private final String disbursed_amount;
  private final String disbursed_on;
  private final String emi_amount;
  private final String emis_count;
  private final String id;
  private final String name;
  private final String next_emi_due_date;
  private final String processing_fee;
  private final String remaining_emis_count;
  private final String repayment_link;
  private final String repayment_message;
  private final String status;
  private final String status_text;
  
  public LoanData(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, LoanCategory paramLoanCategory, String paramString8, String paramString9, String paramString10, String paramString11, String paramString12, String paramString13, String paramString14)
  {
    name = paramString1;
    amount = paramString2;
    disbursed_on = paramString3;
    emis_count = paramString4;
    remaining_emis_count = paramString5;
    next_emi_due_date = paramString6;
    emi_amount = paramString7;
    category = paramLoanCategory;
    status = paramString8;
    id = paramString9;
    status_text = paramString10;
    repayment_link = paramString11;
    repayment_message = paramString12;
    disbursed_amount = paramString13;
    processing_fee = paramString14;
  }
  
  public final String component1()
  {
    return name;
  }
  
  public final String component10()
  {
    return id;
  }
  
  public final String component11()
  {
    return status_text;
  }
  
  public final String component12()
  {
    return repayment_link;
  }
  
  public final String component13()
  {
    return repayment_message;
  }
  
  public final String component14()
  {
    return disbursed_amount;
  }
  
  public final String component15()
  {
    return processing_fee;
  }
  
  public final String component2()
  {
    return amount;
  }
  
  public final String component3()
  {
    return disbursed_on;
  }
  
  public final String component4()
  {
    return emis_count;
  }
  
  public final String component5()
  {
    return remaining_emis_count;
  }
  
  public final String component6()
  {
    return next_emi_due_date;
  }
  
  public final String component7()
  {
    return emi_amount;
  }
  
  public final LoanCategory component8()
  {
    return category;
  }
  
  public final String component9()
  {
    return status;
  }
  
  public final LoanData copy(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, LoanCategory paramLoanCategory, String paramString8, String paramString9, String paramString10, String paramString11, String paramString12, String paramString13, String paramString14)
  {
    k.b(paramString1, "name");
    k.b(paramString2, "amount");
    k.b(paramString3, "disbursed_on");
    k.b(paramLoanCategory, "category");
    k.b(paramString8, "status");
    k.b(paramString9, "id");
    k.b(paramString10, "status_text");
    LoanData localLoanData = new com/truecaller/credit/domain/interactors/loanhistory/models/LoanData;
    localLoanData.<init>(paramString1, paramString2, paramString3, paramString4, paramString5, paramString6, paramString7, paramLoanCategory, paramString8, paramString9, paramString10, paramString11, paramString12, paramString13, paramString14);
    return localLoanData;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof LoanData;
      if (bool1)
      {
        paramObject = (LoanData)paramObject;
        Object localObject1 = name;
        Object localObject2 = name;
        bool1 = k.a(localObject1, localObject2);
        if (bool1)
        {
          localObject1 = amount;
          localObject2 = amount;
          bool1 = k.a(localObject1, localObject2);
          if (bool1)
          {
            localObject1 = disbursed_on;
            localObject2 = disbursed_on;
            bool1 = k.a(localObject1, localObject2);
            if (bool1)
            {
              localObject1 = emis_count;
              localObject2 = emis_count;
              bool1 = k.a(localObject1, localObject2);
              if (bool1)
              {
                localObject1 = remaining_emis_count;
                localObject2 = remaining_emis_count;
                bool1 = k.a(localObject1, localObject2);
                if (bool1)
                {
                  localObject1 = next_emi_due_date;
                  localObject2 = next_emi_due_date;
                  bool1 = k.a(localObject1, localObject2);
                  if (bool1)
                  {
                    localObject1 = emi_amount;
                    localObject2 = emi_amount;
                    bool1 = k.a(localObject1, localObject2);
                    if (bool1)
                    {
                      localObject1 = category;
                      localObject2 = category;
                      bool1 = k.a(localObject1, localObject2);
                      if (bool1)
                      {
                        localObject1 = status;
                        localObject2 = status;
                        bool1 = k.a(localObject1, localObject2);
                        if (bool1)
                        {
                          localObject1 = id;
                          localObject2 = id;
                          bool1 = k.a(localObject1, localObject2);
                          if (bool1)
                          {
                            localObject1 = status_text;
                            localObject2 = status_text;
                            bool1 = k.a(localObject1, localObject2);
                            if (bool1)
                            {
                              localObject1 = repayment_link;
                              localObject2 = repayment_link;
                              bool1 = k.a(localObject1, localObject2);
                              if (bool1)
                              {
                                localObject1 = repayment_message;
                                localObject2 = repayment_message;
                                bool1 = k.a(localObject1, localObject2);
                                if (bool1)
                                {
                                  localObject1 = disbursed_amount;
                                  localObject2 = disbursed_amount;
                                  bool1 = k.a(localObject1, localObject2);
                                  if (bool1)
                                  {
                                    localObject1 = processing_fee;
                                    paramObject = processing_fee;
                                    boolean bool2 = k.a(localObject1, paramObject);
                                    if (bool2) {
                                      break label354;
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    label354:
    return true;
  }
  
  public final String getAmount()
  {
    return amount;
  }
  
  public final LoanCategory getCategory()
  {
    return category;
  }
  
  public final String getDisbursed_amount()
  {
    return disbursed_amount;
  }
  
  public final String getDisbursed_on()
  {
    return disbursed_on;
  }
  
  public final String getEmi_amount()
  {
    return emi_amount;
  }
  
  public final String getEmis_count()
  {
    return emis_count;
  }
  
  public final String getId()
  {
    return id;
  }
  
  public final String getName()
  {
    return name;
  }
  
  public final String getNext_emi_due_date()
  {
    return next_emi_due_date;
  }
  
  public final String getProcessing_fee()
  {
    return processing_fee;
  }
  
  public final String getRemaining_emis_count()
  {
    return remaining_emis_count;
  }
  
  public final String getRepayment_link()
  {
    return repayment_link;
  }
  
  public final String getRepayment_message()
  {
    return repayment_message;
  }
  
  public final String getStatus()
  {
    return status;
  }
  
  public final String getStatus_text()
  {
    return status_text;
  }
  
  public final int hashCode()
  {
    String str = name;
    int i = 0;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    j *= 31;
    Object localObject = amount;
    int k;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    int j = (j + k) * 31;
    localObject = disbursed_on;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = emis_count;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = remaining_emis_count;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = next_emi_due_date;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = emi_amount;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = category;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = status;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = id;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = status_text;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = repayment_link;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = repayment_message;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = disbursed_amount;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = processing_fee;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("LoanData(name=");
    Object localObject = name;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", amount=");
    localObject = amount;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", disbursed_on=");
    localObject = disbursed_on;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", emis_count=");
    localObject = emis_count;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", remaining_emis_count=");
    localObject = remaining_emis_count;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", next_emi_due_date=");
    localObject = next_emi_due_date;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", emi_amount=");
    localObject = emi_amount;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", category=");
    localObject = category;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", status=");
    localObject = status;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", id=");
    localObject = id;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", status_text=");
    localObject = status_text;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", repayment_link=");
    localObject = repayment_link;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", repayment_message=");
    localObject = repayment_message;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", disbursed_amount=");
    localObject = disbursed_amount;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", processing_fee=");
    localObject = processing_fee;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.domain.interactors.loanhistory.models.LoanData
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */