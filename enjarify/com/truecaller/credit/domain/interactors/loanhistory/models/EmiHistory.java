package com.truecaller.credit.domain.interactors.loanhistory.models;

import c.g.b.k;
import java.util.List;

public final class EmiHistory
{
  private final List emis;
  
  public EmiHistory(List paramList)
  {
    emis = paramList;
  }
  
  public final List component1()
  {
    return emis;
  }
  
  public final EmiHistory copy(List paramList)
  {
    k.b(paramList, "emis");
    EmiHistory localEmiHistory = new com/truecaller/credit/domain/interactors/loanhistory/models/EmiHistory;
    localEmiHistory.<init>(paramList);
    return localEmiHistory;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof EmiHistory;
      if (bool1)
      {
        paramObject = (EmiHistory)paramObject;
        List localList = emis;
        paramObject = emis;
        boolean bool2 = k.a(localList, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final List getEmis()
  {
    return emis;
  }
  
  public final int hashCode()
  {
    List localList = emis;
    if (localList != null) {
      return localList.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("EmiHistory(emis=");
    List localList = emis;
    localStringBuilder.append(localList);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.domain.interactors.loanhistory.models.EmiHistory
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */