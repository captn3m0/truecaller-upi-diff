package com.truecaller.credit.domain.interactors.loanhistory.models;

import c.g.b.k;

public final class Payment
{
  private final String emiAmount;
  private final String latePaymentFee;
  private final String transactionId;
  
  public Payment(String paramString1, String paramString2, String paramString3)
  {
    transactionId = paramString1;
    emiAmount = paramString2;
    latePaymentFee = paramString3;
  }
  
  public final String component1()
  {
    return transactionId;
  }
  
  public final String component2()
  {
    return emiAmount;
  }
  
  public final String component3()
  {
    return latePaymentFee;
  }
  
  public final Payment copy(String paramString1, String paramString2, String paramString3)
  {
    Payment localPayment = new com/truecaller/credit/domain/interactors/loanhistory/models/Payment;
    localPayment.<init>(paramString1, paramString2, paramString3);
    return localPayment;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof Payment;
      if (bool1)
      {
        paramObject = (Payment)paramObject;
        String str1 = transactionId;
        String str2 = transactionId;
        bool1 = k.a(str1, str2);
        if (bool1)
        {
          str1 = emiAmount;
          str2 = emiAmount;
          bool1 = k.a(str1, str2);
          if (bool1)
          {
            str1 = latePaymentFee;
            paramObject = latePaymentFee;
            boolean bool2 = k.a(str1, paramObject);
            if (bool2) {
              break label90;
            }
          }
        }
      }
      return false;
    }
    label90:
    return true;
  }
  
  public final String getEmiAmount()
  {
    return emiAmount;
  }
  
  public final String getLatePaymentFee()
  {
    return latePaymentFee;
  }
  
  public final String getTransactionId()
  {
    return transactionId;
  }
  
  public final int hashCode()
  {
    String str1 = transactionId;
    int i = 0;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = emiAmount;
    int k;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    int j = (j + k) * 31;
    str2 = latePaymentFee;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Payment(transactionId=");
    String str = transactionId;
    localStringBuilder.append(str);
    localStringBuilder.append(", emiAmount=");
    str = emiAmount;
    localStringBuilder.append(str);
    localStringBuilder.append(", latePaymentFee=");
    str = latePaymentFee;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.domain.interactors.loanhistory.models.Payment
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */