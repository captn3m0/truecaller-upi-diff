package com.truecaller.credit.domain.interactors.loanhistory.models;

import c.g.b.k;

public final class EmiData
{
  private final String amount;
  private final String dueDate;
  private final Payment payment;
  private final String paymentDate;
  private final String paymentMode;
  private final String status;
  private final String statusText;
  
  public EmiData(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, Payment paramPayment)
  {
    dueDate = paramString1;
    paymentDate = paramString2;
    amount = paramString3;
    status = paramString4;
    statusText = paramString5;
    paymentMode = paramString6;
    payment = paramPayment;
  }
  
  public final String component1()
  {
    return dueDate;
  }
  
  public final String component2()
  {
    return paymentDate;
  }
  
  public final String component3()
  {
    return amount;
  }
  
  public final String component4()
  {
    return status;
  }
  
  public final String component5()
  {
    return statusText;
  }
  
  public final String component6()
  {
    return paymentMode;
  }
  
  public final Payment component7()
  {
    return payment;
  }
  
  public final EmiData copy(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, Payment paramPayment)
  {
    k.b(paramString1, "dueDate");
    k.b(paramString2, "paymentDate");
    k.b(paramString3, "amount");
    k.b(paramString4, "status");
    k.b(paramString5, "statusText");
    EmiData localEmiData = new com/truecaller/credit/domain/interactors/loanhistory/models/EmiData;
    localEmiData.<init>(paramString1, paramString2, paramString3, paramString4, paramString5, paramString6, paramPayment);
    return localEmiData;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof EmiData;
      if (bool1)
      {
        paramObject = (EmiData)paramObject;
        Object localObject = dueDate;
        String str = dueDate;
        bool1 = k.a(localObject, str);
        if (bool1)
        {
          localObject = paymentDate;
          str = paymentDate;
          bool1 = k.a(localObject, str);
          if (bool1)
          {
            localObject = amount;
            str = amount;
            bool1 = k.a(localObject, str);
            if (bool1)
            {
              localObject = status;
              str = status;
              bool1 = k.a(localObject, str);
              if (bool1)
              {
                localObject = statusText;
                str = statusText;
                bool1 = k.a(localObject, str);
                if (bool1)
                {
                  localObject = paymentMode;
                  str = paymentMode;
                  bool1 = k.a(localObject, str);
                  if (bool1)
                  {
                    localObject = payment;
                    paramObject = payment;
                    boolean bool2 = k.a(localObject, paramObject);
                    if (bool2) {
                      break label178;
                    }
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    label178:
    return true;
  }
  
  public final String getAmount()
  {
    return amount;
  }
  
  public final String getDueDate()
  {
    return dueDate;
  }
  
  public final Payment getPayment()
  {
    return payment;
  }
  
  public final String getPaymentDate()
  {
    return paymentDate;
  }
  
  public final String getPaymentMode()
  {
    return paymentMode;
  }
  
  public final String getStatus()
  {
    return status;
  }
  
  public final String getStatusText()
  {
    return statusText;
  }
  
  public final int hashCode()
  {
    String str = dueDate;
    int i = 0;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    j *= 31;
    Object localObject = paymentDate;
    int k;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    int j = (j + k) * 31;
    localObject = amount;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = status;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = statusText;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = paymentMode;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = payment;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("EmiData(dueDate=");
    Object localObject = dueDate;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", paymentDate=");
    localObject = paymentDate;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", amount=");
    localObject = amount;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", status=");
    localObject = status;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", statusText=");
    localObject = statusText;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", paymentMode=");
    localObject = paymentMode;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", payment=");
    localObject = payment;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.domain.interactors.loanhistory.models.EmiData
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */