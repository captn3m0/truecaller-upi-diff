package com.truecaller.credit.domain.interactors.banner;

import c.g.b.k;

public final class CreditBanner
{
  private final String appState;
  private final Integer bannerType;
  private final String buttonAction;
  private final String buttonText;
  private final String headerLeft;
  private final String headerRight;
  private final String imageUrl;
  private final Integer progressPercent;
  private final String progressType;
  private final String subTitle;
  private final String title;
  
  public CreditBanner(String paramString1, Integer paramInteger1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, Integer paramInteger2, String paramString9)
  {
    appState = paramString1;
    bannerType = paramInteger1;
    title = paramString2;
    subTitle = paramString3;
    buttonAction = paramString4;
    buttonText = paramString5;
    headerLeft = paramString6;
    headerRight = paramString7;
    progressType = paramString8;
    progressPercent = paramInteger2;
    imageUrl = paramString9;
  }
  
  public final String component1()
  {
    return appState;
  }
  
  public final Integer component10()
  {
    return progressPercent;
  }
  
  public final String component11()
  {
    return imageUrl;
  }
  
  public final Integer component2()
  {
    return bannerType;
  }
  
  public final String component3()
  {
    return title;
  }
  
  public final String component4()
  {
    return subTitle;
  }
  
  public final String component5()
  {
    return buttonAction;
  }
  
  public final String component6()
  {
    return buttonText;
  }
  
  public final String component7()
  {
    return headerLeft;
  }
  
  public final String component8()
  {
    return headerRight;
  }
  
  public final String component9()
  {
    return progressType;
  }
  
  public final CreditBanner copy(String paramString1, Integer paramInteger1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, Integer paramInteger2, String paramString9)
  {
    CreditBanner localCreditBanner = new com/truecaller/credit/domain/interactors/banner/CreditBanner;
    localCreditBanner.<init>(paramString1, paramInteger1, paramString2, paramString3, paramString4, paramString5, paramString6, paramString7, paramString8, paramInteger2, paramString9);
    return localCreditBanner;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof CreditBanner;
      if (bool1)
      {
        paramObject = (CreditBanner)paramObject;
        Object localObject1 = appState;
        Object localObject2 = appState;
        bool1 = k.a(localObject1, localObject2);
        if (bool1)
        {
          localObject1 = bannerType;
          localObject2 = bannerType;
          bool1 = k.a(localObject1, localObject2);
          if (bool1)
          {
            localObject1 = title;
            localObject2 = title;
            bool1 = k.a(localObject1, localObject2);
            if (bool1)
            {
              localObject1 = subTitle;
              localObject2 = subTitle;
              bool1 = k.a(localObject1, localObject2);
              if (bool1)
              {
                localObject1 = buttonAction;
                localObject2 = buttonAction;
                bool1 = k.a(localObject1, localObject2);
                if (bool1)
                {
                  localObject1 = buttonText;
                  localObject2 = buttonText;
                  bool1 = k.a(localObject1, localObject2);
                  if (bool1)
                  {
                    localObject1 = headerLeft;
                    localObject2 = headerLeft;
                    bool1 = k.a(localObject1, localObject2);
                    if (bool1)
                    {
                      localObject1 = headerRight;
                      localObject2 = headerRight;
                      bool1 = k.a(localObject1, localObject2);
                      if (bool1)
                      {
                        localObject1 = progressType;
                        localObject2 = progressType;
                        bool1 = k.a(localObject1, localObject2);
                        if (bool1)
                        {
                          localObject1 = progressPercent;
                          localObject2 = progressPercent;
                          bool1 = k.a(localObject1, localObject2);
                          if (bool1)
                          {
                            localObject1 = imageUrl;
                            paramObject = imageUrl;
                            boolean bool2 = k.a(localObject1, paramObject);
                            if (bool2) {
                              break label266;
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    label266:
    return true;
  }
  
  public final String getAppState()
  {
    return appState;
  }
  
  public final Integer getBannerType()
  {
    return bannerType;
  }
  
  public final String getButtonAction()
  {
    return buttonAction;
  }
  
  public final String getButtonText()
  {
    return buttonText;
  }
  
  public final String getHeaderLeft()
  {
    return headerLeft;
  }
  
  public final String getHeaderRight()
  {
    return headerRight;
  }
  
  public final String getImageUrl()
  {
    return imageUrl;
  }
  
  public final Integer getProgressPercent()
  {
    return progressPercent;
  }
  
  public final String getProgressType()
  {
    return progressType;
  }
  
  public final String getSubTitle()
  {
    return subTitle;
  }
  
  public final String getTitle()
  {
    return title;
  }
  
  public final int hashCode()
  {
    String str = appState;
    int i = 0;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    j *= 31;
    Object localObject = bannerType;
    int k;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    int j = (j + k) * 31;
    localObject = title;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = subTitle;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = buttonAction;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = buttonText;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = headerLeft;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = headerRight;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = progressType;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = progressPercent;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = imageUrl;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("CreditBanner(appState=");
    Object localObject = appState;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", bannerType=");
    localObject = bannerType;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", title=");
    localObject = title;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", subTitle=");
    localObject = subTitle;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", buttonAction=");
    localObject = buttonAction;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", buttonText=");
    localObject = buttonText;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", headerLeft=");
    localObject = headerLeft;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", headerRight=");
    localObject = headerRight;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", progressType=");
    localObject = progressType;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", progressPercent=");
    localObject = progressPercent;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", imageUrl=");
    localObject = imageUrl;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.domain.interactors.banner.CreditBanner
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */