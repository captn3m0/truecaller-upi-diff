package com.truecaller.credit;

import android.content.Context;
import c.d.a.a;
import c.d.a.b;
import c.d.c;
import com.daamitt.prime.sdk.PreScoreCallback;
import com.daamitt.prime.sdk.PrimeSdk;
import com.daamitt.prime.sdk.ScoreCallback;
import kotlinx.coroutines.j;

public final class l
  implements k
{
  private PrimeSdk a;
  private final Context b;
  
  public l(Context paramContext)
  {
    b = paramContext;
  }
  
  public final Object a(c paramc)
  {
    Object localObject1 = new kotlinx/coroutines/k;
    Object localObject2 = b.a(paramc);
    int i = 1;
    ((kotlinx.coroutines.k)localObject1).<init>((c)localObject2, i);
    localObject2 = localObject1;
    localObject2 = (j)localObject1;
    PrimeSdk localPrimeSdk = a(this);
    Context localContext = b;
    Object localObject3 = new com/truecaller/credit/l$a;
    ((l.a)localObject3).<init>((j)localObject2);
    localObject3 = (PreScoreCallback)localObject3;
    localPrimeSdk.getPreScoreData(localContext, (PreScoreCallback)localObject3);
    localObject1 = ((kotlinx.coroutines.k)localObject1).h();
    localObject2 = a.a;
    if (localObject1 == localObject2)
    {
      localObject2 = "frame";
      c.g.b.k.b(paramc, (String)localObject2);
    }
    return localObject1;
  }
  
  public final Object a(String paramString, c paramc)
  {
    Object localObject1 = new kotlinx/coroutines/k;
    Object localObject2 = b.a(paramc);
    int i = 1;
    ((kotlinx.coroutines.k)localObject1).<init>((c)localObject2, i);
    localObject2 = localObject1;
    localObject2 = (j)localObject1;
    PrimeSdk localPrimeSdk = a(this);
    Context localContext = b;
    Object localObject3 = new com/truecaller/credit/l$b;
    ((l.b)localObject3).<init>((j)localObject2);
    localObject3 = (ScoreCallback)localObject3;
    localPrimeSdk.getScoreData(localContext, paramString, (ScoreCallback)localObject3);
    paramString = ((kotlinx.coroutines.k)localObject1).h();
    localObject1 = a.a;
    if (paramString == localObject1)
    {
      localObject1 = "frame";
      c.g.b.k.b(paramc, (String)localObject1);
    }
    return paramString;
  }
  
  public final boolean a()
  {
    PrimeSdk localPrimeSdk = PrimeSdk.getInstance();
    String str = "PrimeSdk.getInstance()";
    c.g.b.k.a(localPrimeSdk, str);
    a = localPrimeSdk;
    localPrimeSdk = a;
    if (localPrimeSdk == null)
    {
      str = "primeSdk";
      c.g.b.k.a(str);
    }
    localPrimeSdk.enableLogging(false);
    return true;
  }
  
  public final void b()
  {
    PrimeSdk localPrimeSdk = a;
    if (localPrimeSdk == null)
    {
      localObject = "primeSdk";
      c.g.b.k.a((String)localObject);
    }
    Object localObject = b;
    localPrimeSdk.destroy((Context)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */