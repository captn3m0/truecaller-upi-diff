package com.truecaller.smsparser;

import javax.inject.Provider;

public final class f
  implements dagger.a.d
{
  private final d a;
  private final Provider b;
  
  private f(d paramd, Provider paramProvider)
  {
    a = paramd;
    b = paramProvider;
  }
  
  public static f a(d paramd, Provider paramProvider)
  {
    f localf = new com/truecaller/smsparser/f;
    localf.<init>(paramd, paramProvider);
    return localf;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.smsparser.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */