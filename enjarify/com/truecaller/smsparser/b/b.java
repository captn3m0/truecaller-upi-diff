package com.truecaller.smsparser.b;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.support.v4.app.z.a;
import android.support.v4.app.z.c;
import android.support.v4.app.z.d;
import android.support.v4.app.z.g;
import c.d.f;
import c.g.a.m;
import c.g.b.k;
import c.u;
import com.d.b.ab;
import com.d.b.w;
import com.truecaller.log.AssertionUtil;
import com.truecaller.smsparser.R.color;
import com.truecaller.smsparser.R.drawable;
import com.truecaller.smsparser.R.string;
import com.truecaller.smsparser.models.MatchedNotificationAttributes;
import com.truecaller.smsparser.models.NotificationAttribute;
import com.truecaller.utils.extensions.d;
import com.truecaller.utils.i;
import java.io.Closeable;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;

public final class b
  implements a
{
  HashSet a;
  private final Context b;
  private final i c;
  private final com.truecaller.featuretoggles.e d;
  private final w e;
  private final f f;
  
  public b(Context paramContext, i parami, com.truecaller.featuretoggles.e parame, w paramw, f paramf)
  {
    b = paramContext;
    c = parami;
    d = parame;
    e = paramw;
    f = paramf;
    paramContext = new java/util/HashSet;
    paramContext.<init>();
    a = paramContext;
  }
  
  private final z.a a(c paramc, MatchedNotificationAttributes paramMatchedNotificationAttributes, boolean paramBoolean, String paramString)
  {
    Object localObject = d.x();
    boolean bool1 = ((com.truecaller.featuretoggles.b)localObject).a();
    if (bool1)
    {
      localObject = b;
      int j = ((String)localObject).hashCode();
      boolean bool3;
      switch (j)
      {
      default: 
        break;
      case 1540463468: 
        paramString = "POSTPAID";
        boolean bool2 = ((String)localObject).equals(paramString);
        if (!bool2) {
          break label273;
        }
        paramMatchedNotificationAttributes = e;
        return a(paramc, paramMatchedNotificationAttributes, paramBoolean);
      case 1280945827: 
        paramMatchedNotificationAttributes = "DEBIT_CARD";
        bool3 = ((String)localObject).equals(paramMatchedNotificationAttributes);
        if (!bool3) {
          break label273;
        }
        break;
      case 84238: 
        paramMatchedNotificationAttributes = "UPI";
        bool3 = ((String)localObject).equals(paramMatchedNotificationAttributes);
        if (!bool3) {
          break label273;
        }
        break;
      case 65146: 
        paramMatchedNotificationAttributes = "ATM";
        bool3 = ((String)localObject).equals(paramMatchedNotificationAttributes);
        if (!bool3) {
          break label273;
        }
        break;
      case -459336179: 
        paramMatchedNotificationAttributes = "ACCOUNT";
        bool3 = ((String)localObject).equals(paramMatchedNotificationAttributes);
        if (!bool3) {
          break label273;
        }
      }
      paramMatchedNotificationAttributes = paramc.a(paramString);
      if (paramMatchedNotificationAttributes != null)
      {
        Context localContext = b;
        paramc = paramc.b(localContext, paramMatchedNotificationAttributes);
        paramMatchedNotificationAttributes = new android/support/v4/app/z$a;
        paramString = b;
        int i = R.string.CheckBalance;
        paramString = (CharSequence)paramString.getString(i);
        paramMatchedNotificationAttributes.<init>(0, paramString, paramc);
        return paramMatchedNotificationAttributes;
      }
      paramMatchedNotificationAttributes = this;
      return ((b)this).a(paramc, paramBoolean);
      label273:
      return a(paramc, paramBoolean);
    }
    return a(paramc, paramBoolean);
  }
  
  private final z.a a(c paramc, String paramString, boolean paramBoolean)
  {
    com.truecaller.featuretoggles.b localb = d.o();
    boolean bool = localb.a();
    if (bool)
    {
      z.a locala = new android/support/v4/app/z$a;
      Object localObject = b;
      int i = R.string.PayBill;
      localObject = (CharSequence)((Context)localObject).getString(i);
      Context localContext = b;
      paramc = paramc.a(localContext, paramString);
      locala.<init>(0, (CharSequence)localObject, paramc);
      return locala;
    }
    return a(paramc, paramBoolean);
  }
  
  private final z.a a(c paramc, boolean paramBoolean)
  {
    if (paramBoolean)
    {
      localObject1 = b;
      paramc = paramc.a((Context)localObject1);
    }
    else
    {
      localObject1 = b;
      paramc = paramc.c((Context)localObject1);
    }
    Object localObject1 = new android/support/v4/app/z$a;
    Object localObject2 = b;
    int i = R.string.MarkAsRead;
    localObject2 = (CharSequence)((Context)localObject2).getString(i);
    ((z.a)localObject1).<init>(0, (CharSequence)localObject2, paramc);
    return (z.a)localObject1;
  }
  
  /* Error */
  final java.util.Set a()
  {
    // Byte code:
    //   0: new 49	java/util/HashSet
    //   3: astore_1
    //   4: aload_1
    //   5: invokespecial 50	java/util/HashSet:<init>	()V
    //   8: aload_0
    //   9: getfield 39	com/truecaller/smsparser/b/b:b	Landroid/content/Context;
    //   12: astore_2
    //   13: ldc -115
    //   15: astore_3
    //   16: aload_2
    //   17: aload_3
    //   18: invokevirtual 145	android/content/Context:openFileInput	(Ljava/lang/String;)Ljava/io/FileInputStream;
    //   21: astore_2
    //   22: new 147	java/io/DataInputStream
    //   25: astore_3
    //   26: aload_2
    //   27: astore 4
    //   29: aload_2
    //   30: checkcast 149	java/io/InputStream
    //   33: astore 4
    //   35: aload_3
    //   36: aload 4
    //   38: invokespecial 152	java/io/DataInputStream:<init>	(Ljava/io/InputStream;)V
    //   41: aload_3
    //   42: invokevirtual 155	java/io/DataInputStream:readInt	()I
    //   45: istore 5
    //   47: iload 5
    //   49: invokestatic 161	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   52: astore 4
    //   54: aload_1
    //   55: aload 4
    //   57: invokevirtual 164	java/util/HashSet:add	(Ljava/lang/Object;)Z
    //   60: pop
    //   61: aload_3
    //   62: invokevirtual 155	java/io/DataInputStream:readInt	()I
    //   65: istore 5
    //   67: goto -20 -> 47
    //   70: astore_3
    //   71: aload_2
    //   72: checkcast 166	java/io/Closeable
    //   75: astore_2
    //   76: aload_2
    //   77: invokestatic 171	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   80: aload_3
    //   81: athrow
    //   82: aload_2
    //   83: checkcast 166	java/io/Closeable
    //   86: astore_2
    //   87: aload_2
    //   88: invokestatic 171	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   91: aload_1
    //   92: checkcast 173	java/util/Set
    //   95: areturn
    //   96: pop
    //   97: goto -15 -> 82
    //   100: pop
    //   101: goto -10 -> 91
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	104	0	this	b
    //   3	89	1	localHashSet	HashSet
    //   12	76	2	localObject1	Object
    //   15	47	3	localObject2	Object
    //   70	11	3	localObject3	Object
    //   27	29	4	localObject4	Object
    //   45	21	5	i	int
    //   96	1	7	localEOFException	java.io.EOFException
    //   100	1	8	localIOException	IOException
    // Exception table:
    //   from	to	target	type
    //   22	25	70	finally
    //   29	33	70	finally
    //   36	41	70	finally
    //   41	45	70	finally
    //   47	52	70	finally
    //   55	61	70	finally
    //   61	65	70	finally
    //   22	25	96	java/io/EOFException
    //   29	33	96	java/io/EOFException
    //   36	41	96	java/io/EOFException
    //   41	45	96	java/io/EOFException
    //   47	52	96	java/io/EOFException
    //   55	61	96	java/io/EOFException
    //   61	65	96	java/io/EOFException
    //   8	12	100	java/io/IOException
    //   17	21	100	java/io/IOException
    //   71	75	100	java/io/IOException
    //   76	80	100	java/io/IOException
    //   80	82	100	java/io/IOException
    //   82	86	100	java/io/IOException
    //   87	91	100	java/io/IOException
  }
  
  public final void a(MatchedNotificationAttributes paramMatchedNotificationAttributes, c paramc, boolean paramBoolean1, boolean paramBoolean2, String paramString, int paramInt)
  {
    k.b(paramMatchedNotificationAttributes, "matchedNotificationAttributes");
    k.b(paramc, "smartNotificationsHelper");
    k.b(paramString, "senderId");
    String str1 = "";
    Object localObject1 = "";
    Object localObject2 = paramc.a();
    Object localObject3 = b;
    localObject3 = paramc.e((Context)localObject3);
    Object localObject4 = new android/support/v4/app/z$d;
    Object localObject5 = b;
    ((z.d)localObject4).<init>((Context)localObject5, (String)localObject2);
    localObject2 = b;
    int i = R.color.notification_channels_notification_light_default;
    int j = android.support.v4.content.b.c((Context)localObject2, i);
    localObject2 = ((z.d)localObject4).f(j);
    localObject4 = b;
    localObject5 = c;
    int k = ((String)localObject4).hashCode();
    switch (k)
    {
    default: 
      break;
    case 1878720662: 
      localObject5 = "CREDIT_CARD";
      boolean bool1 = ((String)localObject4).equals(localObject5);
      if (bool1) {
        int m = R.drawable.ic_credit_card;
      }
      break;
    case 1540463468: 
      localObject5 = "POSTPAID";
      boolean bool2 = ((String)localObject4).equals(localObject5);
      if (bool2) {
        int n = R.drawable.ic_postpaid;
      }
      break;
    case 1280945827: 
      localObject5 = "DEBIT_CARD";
      boolean bool3 = ((String)localObject4).equals(localObject5);
      if (bool3) {
        int i1 = R.drawable.ic_credit_card;
      }
      break;
    case 1167688599: 
      localObject5 = "BROADBAND";
      boolean bool4 = ((String)localObject4).equals(localObject5);
      if (bool4) {
        int i2 = R.drawable.ic_broadband;
      }
      break;
    case 399611855: 
      localObject5 = "PREPAID";
      boolean bool5 = ((String)localObject4).equals(localObject5);
      if (bool5) {
        int i3 = R.drawable.ic_prepaid;
      }
      break;
    case 84238: 
      localObject5 = "UPI";
      boolean bool6 = ((String)localObject4).equals(localObject5);
      if (bool6) {
        int i4 = R.drawable.ic_rupee_mobile;
      }
      break;
    case 65146: 
      localObject5 = "ATM";
      boolean bool7 = ((String)localObject4).equals(localObject5);
      if (bool7) {
        int i5 = R.drawable.ic_money_bill_alt;
      }
      break;
    case -459336179: 
      String str2 = "ACCOUNT";
      boolean bool8 = ((String)localObject4).equals(str2);
      if (bool8)
      {
        localObject4 = "CREDIT";
        bool8 = k.a(localObject5, localObject4);
        if (bool8) {
          i6 = R.drawable.ic_rupee_circle;
        } else {
          i6 = R.drawable.ic_wallet;
        }
      }
      break;
    }
    int i6 = R.drawable.ic_wallet;
    localObject2 = ((z.d)localObject2).a(i6).b((PendingIntent)localObject3);
    localObject3 = null;
    Object localObject6;
    boolean bool9;
    if (!paramBoolean2)
    {
      localObject6 = b;
      localObject6 = paramc.d((Context)localObject6);
      paramc = a(paramc, paramMatchedNotificationAttributes, paramBoolean1, paramString);
      ((z.d)localObject2).a(paramc);
      paramc = new android/support/v4/app/z$a;
      localObject7 = b;
      bool9 = R.string.WhatsThis;
      localObject7 = (CharSequence)((Context)localObject7).getString(bool9);
      paramc.<init>(0, (CharSequence)localObject7, (PendingIntent)localObject6);
      ((z.d)localObject2).a(paramc);
      ((z.d)localObject2).a((PendingIntent)localObject6);
    }
    else if (!paramBoolean1)
    {
      localObject6 = b;
      localObject6 = paramc.c((Context)localObject6);
      localObject4 = new android/support/v4/app/z$a;
      localObject5 = b;
      k = R.string.ShowSMS;
      localObject5 = (CharSequence)((Context)localObject5).getString(k);
      ((z.a)localObject4).<init>(0, (CharSequence)localObject5, (PendingIntent)localObject6);
      ((z.d)localObject2).a((z.a)localObject4);
      paramc = a(paramc, paramMatchedNotificationAttributes, paramBoolean1, paramString);
      ((z.d)localObject2).a(paramc);
      ((z.d)localObject2).a((PendingIntent)localObject6);
    }
    else
    {
      localObject6 = b;
      localObject6 = paramc.b((Context)localObject6);
      localObject4 = new android/support/v4/app/z$a;
      localObject5 = b;
      k = R.string.ShowSMS;
      localObject5 = (CharSequence)((Context)localObject5).getString(k);
      ((z.a)localObject4).<init>(0, (CharSequence)localObject5, (PendingIntent)localObject6);
      ((z.d)localObject2).a((z.a)localObject4);
      paramc = a(paramc, paramMatchedNotificationAttributes, paramBoolean1, paramString);
      ((z.d)localObject2).a(paramc);
      ((z.d)localObject2).a((PendingIntent)localObject6);
    }
    paramc = c;
    boolean bool13 = paramc.a();
    if (bool13) {}
    try
    {
      paramc = e;
      localObject7 = a;
      paramc = paramc.a((String)localObject7);
      paramc = paramc.d();
      ((z.d)localObject2).a(paramc);
    }
    catch (IOException localIOException)
    {
      int i7;
      boolean bool14;
      for (;;) {}
    }
    paramMatchedNotificationAttributes = d.iterator();
    for (;;)
    {
      bool13 = paramMatchedNotificationAttributes.hasNext();
      if (!bool13) {
        break;
      }
      paramc = (NotificationAttribute)paramMatchedNotificationAttributes.next();
      localObject7 = a;
      paramBoolean2 = ((String)localObject7).hashCode();
      bool9 = -1868540019;
      if (paramBoolean2 != bool9)
      {
        boolean bool10 = -973978124;
        if (paramBoolean2 != bool10)
        {
          boolean bool11 = -389150394;
          if (paramBoolean2 != bool11)
          {
            boolean bool12 = 821354847;
            if (paramBoolean2 == bool12)
            {
              localObject6 = "contentTitle";
              paramBoolean1 = ((String)localObject7).equals(localObject6);
              if (paramBoolean1)
              {
                localObject7 = new java/lang/StringBuilder;
                ((StringBuilder)localObject7).<init>();
                localObject6 = b;
                ((StringBuilder)localObject7).append((String)localObject6);
                paramc = d;
                ((StringBuilder)localObject7).append(paramc);
                paramc = (CharSequence)((StringBuilder)localObject7).toString();
                ((z.d)localObject2).a(paramc);
              }
            }
          }
          else
          {
            localObject6 = "contentText";
            paramBoolean1 = ((String)localObject7).equals(localObject6);
            if (paramBoolean1)
            {
              localObject7 = new java/lang/StringBuilder;
              ((StringBuilder)localObject7).<init>();
              ((StringBuilder)localObject7).append(str1);
              localObject6 = b;
              ((StringBuilder)localObject7).append((String)localObject6);
              paramc = d;
              ((StringBuilder)localObject7).append(paramc);
              str1 = ((StringBuilder)localObject7).toString();
              paramc = str1;
              paramc = (CharSequence)str1;
              ((z.d)localObject2).b(paramc);
            }
          }
        }
        else
        {
          localObject6 = "additionalText";
          paramBoolean1 = ((String)localObject7).equals(localObject6);
          if (paramBoolean1)
          {
            localObject7 = new java/lang/StringBuilder;
            ((StringBuilder)localObject7).<init>();
            ((StringBuilder)localObject7).append((String)localObject1);
            localObject6 = b;
            ((StringBuilder)localObject7).append((String)localObject6);
            paramc = d;
            ((StringBuilder)localObject7).append(paramc);
            paramc = ((StringBuilder)localObject7).toString();
            localObject1 = paramc;
          }
        }
      }
      else
      {
        localObject6 = "subText";
        paramBoolean1 = ((String)localObject7).equals(localObject6);
        if (paramBoolean1)
        {
          localObject7 = new java/lang/StringBuilder;
          ((StringBuilder)localObject7).<init>();
          localObject6 = b;
          ((StringBuilder)localObject7).append((String)localObject6);
          paramc = d;
          ((StringBuilder)localObject7).append(paramc);
          paramc = (CharSequence)((StringBuilder)localObject7).toString();
          ((z.d)localObject2).c(paramc);
        }
      }
    }
    paramMatchedNotificationAttributes = (MatchedNotificationAttributes)localObject1;
    paramMatchedNotificationAttributes = (CharSequence)localObject1;
    i7 = paramMatchedNotificationAttributes.length();
    bool13 = true;
    if (i7 > 0)
    {
      i7 = 1;
    }
    else
    {
      i7 = 0;
      paramMatchedNotificationAttributes = null;
    }
    if (i7 != 0)
    {
      paramMatchedNotificationAttributes = new android/support/v4/app/z$c;
      paramMatchedNotificationAttributes.<init>();
      localObject7 = new java/lang/StringBuilder;
      ((StringBuilder)localObject7).<init>();
      ((StringBuilder)localObject7).append(str1);
      localObject6 = "\n";
      ((StringBuilder)localObject7).append((String)localObject6);
      ((StringBuilder)localObject7).append((String)localObject1);
      localObject7 = (CharSequence)((StringBuilder)localObject7).toString();
      paramMatchedNotificationAttributes = (z.g)paramMatchedNotificationAttributes.b((CharSequence)localObject7);
      ((z.d)localObject2).a(paramMatchedNotificationAttributes);
    }
    paramMatchedNotificationAttributes = b;
    Object localObject7 = "notification";
    paramMatchedNotificationAttributes = paramMatchedNotificationAttributes.getSystemService((String)localObject7);
    if (paramMatchedNotificationAttributes != null)
    {
      paramMatchedNotificationAttributes = (NotificationManager)paramMatchedNotificationAttributes;
      localObject7 = ((z.d)localObject2).h();
      paramMatchedNotificationAttributes.notify(paramInt, (Notification)localObject7);
      paramMatchedNotificationAttributes = (Collection)a;
      if (paramMatchedNotificationAttributes != null)
      {
        bool14 = paramMatchedNotificationAttributes.isEmpty();
        if (!bool14)
        {
          bool13 = false;
          paramc = null;
        }
      }
      if (bool13)
      {
        paramMatchedNotificationAttributes = (HashSet)a();
        a = paramMatchedNotificationAttributes;
      }
      paramMatchedNotificationAttributes = a;
      paramc = Integer.valueOf(paramInt);
      paramMatchedNotificationAttributes.add(paramc);
      paramMatchedNotificationAttributes = (Collection)a;
      a(paramMatchedNotificationAttributes);
      return;
    }
    paramMatchedNotificationAttributes = new c/u;
    paramMatchedNotificationAttributes.<init>("null cannot be cast to non-null type android.app.NotificationManager");
    throw paramMatchedNotificationAttributes;
  }
  
  final void a(Collection paramCollection)
  {
    try
    {
      Object localObject1 = b;
      Object localObject2 = "smartNotifications.state";
      boolean bool = false;
      Object localObject3 = null;
      localObject1 = ((Context)localObject1).openFileOutput((String)localObject2, 0);
      try
      {
        localObject2 = new java/io/DataOutputStream;
        localObject3 = localObject1;
        localObject3 = (OutputStream)localObject1;
        ((DataOutputStream)localObject2).<init>((OutputStream)localObject3);
        paramCollection = paramCollection.iterator();
        for (;;)
        {
          bool = paramCollection.hasNext();
          if (!bool) {
            break;
          }
          localObject3 = paramCollection.next();
          localObject3 = (Number)localObject3;
          int i = ((Number)localObject3).intValue();
          ((DataOutputStream)localObject2).writeInt(i);
        }
        return;
      }
      finally
      {
        localObject1 = (Closeable)localObject1;
        d.a((Closeable)localObject1);
      }
      return;
    }
    catch (IOException localIOException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localIOException);
    }
  }
  
  public final boolean a(int paramInt)
  {
    Object localObject = (Collection)a;
    if (localObject != null)
    {
      bool = ((Collection)localObject).isEmpty();
      if (!bool)
      {
        bool = false;
        localObject = null;
        break label35;
      }
    }
    boolean bool = true;
    label35:
    if (bool)
    {
      localObject = (HashSet)a();
      a = ((HashSet)localObject);
    }
    localObject = a;
    Integer localInteger = Integer.valueOf(paramInt);
    return ((HashSet)localObject).contains(localInteger);
  }
  
  public final void b(int paramInt)
  {
    ag localag = (ag)bg.a;
    f localf = f;
    Object localObject = new com/truecaller/smsparser/b/b$a;
    ((b.a)localObject).<init>(this, paramInt, null);
    localObject = (m)localObject;
    kotlinx.coroutines.e.b(localag, localf, (m)localObject, 2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.smsparser.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */