package com.truecaller.smsparser.b;

import android.app.PendingIntent;
import android.content.Context;

public abstract interface c
{
  public abstract PendingIntent a(Context paramContext);
  
  public abstract PendingIntent a(Context paramContext, String paramString);
  
  public abstract String a();
  
  public abstract String a(String paramString);
  
  public abstract PendingIntent b(Context paramContext);
  
  public abstract PendingIntent b(Context paramContext, String paramString);
  
  public abstract PendingIntent c(Context paramContext);
  
  public abstract PendingIntent d(Context paramContext);
  
  public abstract PendingIntent e(Context paramContext);
}

/* Location:
 * Qualified Name:     com.truecaller.smsparser.b.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */