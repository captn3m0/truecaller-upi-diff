package com.truecaller.smsparser.models;

import c.g.b.k;
import java.util.List;

public final class b
{
  public String a;
  public String b;
  public String c;
  public String d;
  public List e;
  public List f;
  
  public final String a()
  {
    String str1 = b;
    if (str1 == null)
    {
      String str2 = "reminderType";
      k.a(str2);
    }
    return str1;
  }
  
  public final String b()
  {
    String str1 = c;
    if (str1 == null)
    {
      String str2 = "title";
      k.a(str2);
    }
    return str1;
  }
  
  public final String c()
  {
    String str1 = d;
    if (str1 == null)
    {
      String str2 = "messageType";
      k.a(str2);
    }
    return str1;
  }
  
  public final List d()
  {
    List localList = e;
    if (localList == null)
    {
      String str = "attributes";
      k.a(str);
    }
    return localList;
  }
  
  public final List e()
  {
    List localList = f;
    if (localList == null)
    {
      String str = "notificationAttributes";
      k.a(str);
    }
    return localList;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.smsparser.models.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */