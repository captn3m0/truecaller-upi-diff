package com.truecaller.smsparser.models;

import c.u;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class f
  extends HashMap
{
  public f(JSONObject paramJSONObject)
  {
    String str1 = "senders";
    try
    {
      paramJSONObject = paramJSONObject.getJSONArray(str1);
      int i = paramJSONObject.length();
      int j = 0;
      while (j < i)
      {
        Object localObject = paramJSONObject.get(j);
        if (localObject != null)
        {
          localObject = (JSONObject)localObject;
          String str2 = "file";
          str2 = ((JSONObject)localObject).optString(str2);
          String str3 = "tags";
          localObject = ((JSONObject)localObject).optJSONArray(str3);
          int k = ((JSONArray)localObject).length();
          int m = 0;
          while (m < k)
          {
            String str4 = ((JSONArray)localObject).optString(m);
            put(str4, str2);
            m += 1;
          }
          j += 1;
        }
        else
        {
          paramJSONObject = new c/u;
          str1 = "null cannot be cast to non-null type org.json.JSONObject";
          paramJSONObject.<init>(str1);
          throw paramJSONObject;
        }
      }
    }
    catch (JSONException localJSONException)
    {
      for (;;) {}
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.smsparser.models.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */