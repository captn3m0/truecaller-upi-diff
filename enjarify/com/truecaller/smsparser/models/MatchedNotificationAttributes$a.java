package com.truecaller.smsparser.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;
import java.util.ArrayList;

public final class MatchedNotificationAttributes$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    MatchedNotificationAttributes localMatchedNotificationAttributes = new com/truecaller/smsparser/models/MatchedNotificationAttributes;
    String str1 = paramParcel.readString();
    String str2 = paramParcel.readString();
    String str3 = paramParcel.readString();
    int i = paramParcel.readInt();
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>(i);
    while (i != 0)
    {
      localObject = (NotificationAttribute)NotificationAttribute.CREATOR.createFromParcel(paramParcel);
      localArrayList.add(localObject);
      i += -1;
    }
    Object localObject = paramParcel.readString();
    localMatchedNotificationAttributes.<init>(str1, str2, str3, localArrayList, (String)localObject);
    return localMatchedNotificationAttributes;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new MatchedNotificationAttributes[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.smsparser.models.MatchedNotificationAttributes.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */