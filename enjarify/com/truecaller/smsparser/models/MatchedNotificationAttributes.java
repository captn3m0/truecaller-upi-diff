package com.truecaller.smsparser.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class MatchedNotificationAttributes
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  public final String a;
  public final String b;
  public final String c;
  public final List d;
  public final String e;
  
  static
  {
    MatchedNotificationAttributes.a locala = new com/truecaller/smsparser/models/MatchedNotificationAttributes$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public MatchedNotificationAttributes(String paramString1, String paramString2, String paramString3, List paramList, String paramString4)
  {
    a = paramString1;
    b = paramString2;
    c = paramString3;
    d = paramList;
    e = paramString4;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof MatchedNotificationAttributes;
      if (bool1)
      {
        paramObject = (MatchedNotificationAttributes)paramObject;
        Object localObject1 = a;
        Object localObject2 = a;
        bool1 = k.a(localObject1, localObject2);
        if (bool1)
        {
          localObject1 = b;
          localObject2 = b;
          bool1 = k.a(localObject1, localObject2);
          if (bool1)
          {
            localObject1 = c;
            localObject2 = c;
            bool1 = k.a(localObject1, localObject2);
            if (bool1)
            {
              localObject1 = d;
              localObject2 = d;
              bool1 = k.a(localObject1, localObject2);
              if (bool1)
              {
                localObject1 = e;
                paramObject = e;
                boolean bool2 = k.a(localObject1, paramObject);
                if (bool2) {
                  break label134;
                }
              }
            }
          }
        }
      }
      return false;
    }
    label134:
    return true;
  }
  
  public final int hashCode()
  {
    String str = a;
    int i = 0;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    j *= 31;
    Object localObject = b;
    int k;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    int j = (j + k) * 31;
    localObject = c;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = d;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = e;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("MatchedNotificationAttributes(senderIconUrl=");
    Object localObject = a;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", reminderType=");
    localObject = b;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", messageType=");
    localObject = c;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", notificationAttributes=");
    localObject = d;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", deepLink=");
    localObject = e;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    Object localObject = a;
    paramParcel.writeString((String)localObject);
    localObject = b;
    paramParcel.writeString((String)localObject);
    localObject = c;
    paramParcel.writeString((String)localObject);
    localObject = d;
    int i = ((Collection)localObject).size();
    paramParcel.writeInt(i);
    localObject = ((Collection)localObject).iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject).hasNext();
      if (!bool) {
        break;
      }
      NotificationAttribute localNotificationAttribute = (NotificationAttribute)((Iterator)localObject).next();
      localNotificationAttribute.writeToParcel(paramParcel, 0);
    }
    localObject = e;
    paramParcel.writeString((String)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.smsparser.models.MatchedNotificationAttributes
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */