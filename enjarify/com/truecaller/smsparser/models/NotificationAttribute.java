package com.truecaller.smsparser.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class NotificationAttribute
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  public final String a;
  public final String b;
  public final int c;
  public String d;
  
  static
  {
    NotificationAttribute.a locala = new com/truecaller/smsparser/models/NotificationAttribute$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public NotificationAttribute(String paramString1, String paramString2, int paramInt, String paramString3)
  {
    a = paramString1;
    b = paramString2;
    c = paramInt;
    d = paramString3;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof NotificationAttribute;
      if (bool2)
      {
        paramObject = (NotificationAttribute)paramObject;
        String str1 = a;
        String str2 = a;
        bool2 = k.a(str1, str2);
        if (bool2)
        {
          str1 = b;
          str2 = b;
          bool2 = k.a(str1, str2);
          if (bool2)
          {
            int i = c;
            int j = c;
            if (i == j)
            {
              i = 1;
            }
            else
            {
              i = 0;
              str1 = null;
            }
            if (i != 0)
            {
              str1 = d;
              paramObject = d;
              boolean bool3 = k.a(str1, paramObject);
              if (bool3) {
                return bool1;
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    String str1 = a;
    int i = 0;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = b;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    int j = (j + k) * 31;
    int k = c;
    j = (j + k) * 31;
    str2 = d;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("NotificationAttribute(name=");
    String str = a;
    localStringBuilder.append(str);
    localStringBuilder.append(", prefix=");
    str = b;
    localStringBuilder.append(str);
    localStringBuilder.append(", attributeIndex=");
    int i = c;
    localStringBuilder.append(i);
    localStringBuilder.append(", value=");
    str = d;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    String str = a;
    paramParcel.writeString(str);
    str = b;
    paramParcel.writeString(str);
    paramInt = c;
    paramParcel.writeInt(paramInt);
    str = d;
    paramParcel.writeString(str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.smsparser.models.NotificationAttribute
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */