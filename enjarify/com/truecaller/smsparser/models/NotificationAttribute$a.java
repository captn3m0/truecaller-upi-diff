package com.truecaller.smsparser.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class NotificationAttribute$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    NotificationAttribute localNotificationAttribute = new com/truecaller/smsparser/models/NotificationAttribute;
    String str1 = paramParcel.readString();
    String str2 = paramParcel.readString();
    int i = paramParcel.readInt();
    paramParcel = paramParcel.readString();
    localNotificationAttribute.<init>(str1, str2, i, paramParcel);
    return localNotificationAttribute;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new NotificationAttribute[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.smsparser.models.NotificationAttribute.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */