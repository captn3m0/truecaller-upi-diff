package com.truecaller.smsparser;

import android.app.PendingIntent;
import android.content.Context;
import c.a.m;
import c.g.b.k;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.notifications.NotificationBroadcastReceiver;
import com.truecaller.messaging.notifications.NotificationIdentifier;
import com.truecaller.smsparser.b.c;
import com.truecaller.truepay.SenderInfo;
import com.truecaller.truepay.d;
import java.util.List;
import org.a.a.a.g;

public final class j
  implements c
{
  private final int a;
  private final Message b;
  private final com.truecaller.notificationchannels.j c;
  private final d d;
  
  public j(Message paramMessage, com.truecaller.notificationchannels.j paramj, d paramd)
  {
    b = paramMessage;
    c = paramj;
    d = paramd;
    paramMessage = b.e;
    k.a(paramMessage, "message.date");
    int i = (int)a;
    a = i;
  }
  
  public final PendingIntent a(Context paramContext)
  {
    k.b(paramContext, "context");
    NotificationIdentifier localNotificationIdentifier = new com/truecaller/messaging/notifications/NotificationIdentifier;
    int i = a;
    localNotificationIdentifier.<init>(i, i, 2);
    Object localObject = new Message[1];
    Message localMessage = b;
    localObject[0] = localMessage;
    localObject = m.c((Object[])localObject);
    paramContext = NotificationBroadcastReceiver.d(paramContext, (List)localObject, localNotificationIdentifier);
    k.a(paramContext, "NotificationBroadcastRec…ationIdentifier\n        )");
    return paramContext;
  }
  
  public final PendingIntent a(Context paramContext, String paramString)
  {
    k.b(paramContext, "context");
    k.b(paramString, "deepLink");
    NotificationIdentifier localNotificationIdentifier = new com/truecaller/messaging/notifications/NotificationIdentifier;
    int i = a;
    localNotificationIdentifier.<init>(i, i, 2);
    paramContext = NotificationBroadcastReceiver.a(paramContext, paramString, localNotificationIdentifier);
    k.a(paramContext, "NotificationBroadcastRec…, notificationIdentifier)");
    return paramContext;
  }
  
  public final String a()
  {
    return c.d();
  }
  
  public final String a(String paramString)
  {
    k.b(paramString, "senderId");
    d locald = d;
    paramString = locald.a(paramString);
    if (paramString != null) {
      return paramString.getSymbol();
    }
    return null;
  }
  
  public final PendingIntent b(Context paramContext)
  {
    k.b(paramContext, "context");
    NotificationIdentifier localNotificationIdentifier = new com/truecaller/messaging/notifications/NotificationIdentifier;
    int i = a;
    localNotificationIdentifier.<init>(i, i, 2);
    Object localObject = new Message[1];
    Message localMessage = b;
    localObject[0] = localMessage;
    localObject = m.c((Object[])localObject);
    paramContext = NotificationBroadcastReceiver.e(paramContext, (List)localObject, localNotificationIdentifier);
    k.a(paramContext, "NotificationBroadcastRec…, notificationIdentifier)");
    return paramContext;
  }
  
  public final PendingIntent b(Context paramContext, String paramString)
  {
    k.b(paramContext, "appContext");
    k.b(paramString, "bankSymbol");
    NotificationIdentifier localNotificationIdentifier = new com/truecaller/messaging/notifications/NotificationIdentifier;
    int i = a;
    localNotificationIdentifier.<init>(i, i, 2);
    i = 1;
    Object localObject = new Message[i];
    Message localMessage = b;
    localObject[0] = localMessage;
    localObject = m.c((Object[])localObject);
    paramContext = NotificationBroadcastReceiver.a(paramContext, localNotificationIdentifier, paramString, i, (List)localObject);
    k.a(paramContext, "NotificationBroadcastRec…nkSymbol, true, messages)");
    return paramContext;
  }
  
  public final PendingIntent c(Context paramContext)
  {
    k.b(paramContext, "appContext");
    NotificationIdentifier localNotificationIdentifier = new com/truecaller/messaging/notifications/NotificationIdentifier;
    int i = a;
    localNotificationIdentifier.<init>(i, i, 2);
    paramContext = NotificationBroadcastReceiver.a(paramContext, localNotificationIdentifier, true);
    k.a(paramContext, "NotificationBroadcastRec…ficationIdentifier, true)");
    return paramContext;
  }
  
  public final PendingIntent d(Context paramContext)
  {
    k.b(paramContext, "appContext");
    NotificationIdentifier localNotificationIdentifier = new com/truecaller/messaging/notifications/NotificationIdentifier;
    int i = a;
    localNotificationIdentifier.<init>(i, i, 2);
    paramContext = NotificationBroadcastReceiver.a(paramContext, localNotificationIdentifier);
    k.a(paramContext, "NotificationBroadcastRec…, notificationIdentifier)");
    return paramContext;
  }
  
  public final PendingIntent e(Context paramContext)
  {
    k.b(paramContext, "appContext");
    NotificationIdentifier localNotificationIdentifier = new com/truecaller/messaging/notifications/NotificationIdentifier;
    int i = a;
    localNotificationIdentifier.<init>(i, i, 2);
    Object localObject = new Message[1];
    Message localMessage = b;
    localObject[0] = localMessage;
    localObject = m.c((Object[])localObject);
    paramContext = NotificationBroadcastReceiver.f(paramContext, (List)localObject, localNotificationIdentifier);
    k.a(paramContext, "NotificationBroadcastRec…, notificationIdentifier)");
    return paramContext;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.smsparser.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */