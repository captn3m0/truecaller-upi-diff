package com.truecaller.smsparser;

import c.a.ag;
import c.g.b.k;
import c.n;
import c.t;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.e;
import com.truecaller.analytics.e.a;
import com.truecaller.androidactors.f;
import com.truecaller.smsparser.models.MatchedNotificationAttributes;
import com.truecaller.tracking.events.ao;
import com.truecaller.tracking.events.ao.a;
import java.util.List;
import java.util.Map;
import org.apache.a.d.d;

public final class b
  implements a
{
  private final com.truecaller.analytics.b a;
  private final com.truecaller.smsparser.b.a b;
  private final com.truecaller.smsparser.a.a c;
  private final com.truecaller.smsparser.a.c d;
  private final f e;
  
  public b(com.truecaller.analytics.b paramb, com.truecaller.smsparser.b.a parama, com.truecaller.smsparser.a.a parama1, com.truecaller.smsparser.a.c paramc, f paramf)
  {
    a = paramb;
    b = parama;
    c = parama1;
    d = paramc;
    e = paramf;
  }
  
  private final void a(boolean paramBoolean, String paramString1, String paramString2, String paramString3)
  {
    Object localObject1 = new com/truecaller/analytics/e$a;
    ((e.a)localObject1).<init>("SmartNotification");
    ((e.a)localObject1).a("IsDefaultApp", paramBoolean);
    ((e.a)localObject1).a("SenderId", paramString1);
    ((e.a)localObject1).a("SenderName", paramString2);
    ((e.a)localObject1).a("MessageType", paramString3);
    Object localObject2 = a;
    localObject1 = ((e.a)localObject1).a();
    k.a(localObject1, "event.build()");
    ((com.truecaller.analytics.b)localObject2).a((e)localObject1);
    localObject1 = (ae)e.a();
    localObject2 = ao.b();
    Object localObject3 = (CharSequence)"SmartNotification";
    localObject2 = ((ao.a)localObject2).a((CharSequence)localObject3);
    localObject3 = new n[4];
    Object localObject4 = String.valueOf(paramBoolean);
    localObject4 = t.a("IsDefaultApp", localObject4);
    localObject3[0] = localObject4;
    localObject4 = t.a("SenderId", paramString1);
    localObject3[1] = localObject4;
    localObject4 = t.a("SenderName", paramString2);
    localObject3[2] = localObject4;
    localObject4 = t.a("MessageType", paramString3);
    localObject3[3] = localObject4;
    localObject4 = ag.a((n[])localObject3);
    localObject4 = (d)((ao.a)localObject2).a((Map)localObject4).a();
    ((ae)localObject1).a((d)localObject4);
  }
  
  public final boolean a(String paramString1, String paramString2, com.truecaller.smsparser.b.c paramc, boolean paramBoolean1, boolean paramBoolean2, long paramLong)
  {
    Object localObject1 = paramString2;
    k.b(paramString1, "senderId");
    k.b(paramString2, "message");
    Object localObject2 = paramc;
    k.b(paramc, "smartNotificationsHelper");
    Object localObject3 = c;
    com.truecaller.smsparser.models.c localc = ((com.truecaller.smsparser.a.a)localObject3).a(paramString1);
    if (localc == null) {
      return false;
    }
    localObject3 = d;
    MatchedNotificationAttributes localMatchedNotificationAttributes = ((com.truecaller.smsparser.a.c)localObject3).a(paramString2, localc);
    if (localMatchedNotificationAttributes != null)
    {
      localObject1 = b;
      int i = (int)paramLong;
      boolean bool = ((com.truecaller.smsparser.b.a)localObject1).a(i);
      if (!bool)
      {
        localObject1 = b;
        localObject3 = localMatchedNotificationAttributes;
        ((com.truecaller.smsparser.b.a)localObject1).a(localMatchedNotificationAttributes, paramc, paramBoolean1, paramBoolean2, paramString1, i);
        localObject1 = ((com.truecaller.smsparser.models.b)a.get(0)).b();
        localObject3 = b;
        localObject2 = c;
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>((String)localObject3);
        localStringBuilder.append("_");
        localStringBuilder.append((String)localObject2);
        localObject3 = localStringBuilder.toString();
        localObject2 = "builder.append(\"_\").append(messageType).toString()";
        k.a(localObject3, (String)localObject2);
        a(paramBoolean1, paramString1, (String)localObject1, (String)localObject3);
      }
      return true;
    }
    localObject1 = ((com.truecaller.smsparser.models.b)a.get(0)).b();
    a(paramBoolean1, paramString1, (String)localObject1, "NotParsed");
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.smsparser.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */