package com.truecaller.smsparser.a;

import c.g.b.k;
import c.n.m;
import com.truecaller.log.UnmutedException.d;
import com.truecaller.smsparser.models.MatchedNotificationAttributes;
import com.truecaller.smsparser.models.NotificationAttribute;
import com.truecaller.smsparser.models.a;
import com.truecaller.smsparser.models.b;
import java.text.NumberFormat;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class d
  implements c
{
  private final NumberFormat a;
  
  public d()
  {
    Object localObject = new java/util/Locale;
    ((Locale)localObject).<init>("en", "in");
    localObject = NumberFormat.getNumberInstance((Locale)localObject);
    a = ((NumberFormat)localObject);
  }
  
  private final String a(String paramString)
  {
    try
    {
      Object localObject = a;
      String str1 = ",";
      String str2 = "";
      str1 = m.a(paramString, str1, str2);
      double d = Double.parseDouble(str1);
      localObject = ((NumberFormat)localObject).format(d);
      str1 = "numberFormat.format(valu…lace(\",\", \"\").toDouble())";
      k.a(localObject, str1);
      paramString = (String)localObject;
    }
    catch (NumberFormatException localNumberFormatException)
    {
      for (;;) {}
    }
    return paramString;
  }
  
  private static String a(Matcher paramMatcher, a parama)
  {
    String str1 = "";
    Iterator localIterator = ((Iterable)parama.a()).iterator();
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      int i = ((Number)localIterator.next()).intValue();
      String str2 = paramMatcher.group(i);
      if (str2 != null)
      {
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        localStringBuilder.append(str1);
        localStringBuilder.append(str2);
        str1 = localStringBuilder.toString();
      }
    }
    paramMatcher = parama.b();
    parama = "NUMBER";
    boolean bool2 = k.a(paramMatcher, parama);
    if (bool2) {
      return b(str1);
    }
    return str1;
  }
  
  private final String a(Matcher paramMatcher, a parama, b paramb)
  {
    String str = "";
    Object localObject1 = parama.b();
    int i = ((String)localObject1).hashCode();
    int j = 2090926;
    if (i == j)
    {
      localObject2 = "DATE";
      boolean bool1 = ((String)localObject1).equals(localObject2);
      if (bool1)
      {
        localObject1 = "-";
        break label60;
      }
    }
    localObject1 = "";
    label60:
    i = 0;
    Object localObject2 = null;
    try
    {
      for (;;)
      {
        localObject3 = parama.a();
        int k = ((List)localObject3).size() + -1;
        if (i >= k) {
          break;
        }
        localObject3 = parama.a();
        localObject3 = ((List)localObject3).get(i);
        localObject3 = (Number)localObject3;
        int m = ((Number)localObject3).intValue();
        localObject3 = paramMatcher.group(m);
        if (localObject3 != null)
        {
          localObject4 = new java/lang/StringBuilder;
          ((StringBuilder)localObject4).<init>();
          ((StringBuilder)localObject4).append(str);
          ((StringBuilder)localObject4).append((String)localObject3);
          ((StringBuilder)localObject4).append((String)localObject1);
          str = ((StringBuilder)localObject4).toString();
        }
        i += 1;
      }
      localObject1 = parama.a();
      localObject1 = ((List)localObject1).get(i);
      localObject1 = (Number)localObject1;
      int n = ((Number)localObject1).intValue();
      localObject1 = paramMatcher.group(n);
      if (localObject1 != null)
      {
        localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>();
        ((StringBuilder)localObject3).append(str);
        ((StringBuilder)localObject3).append((String)localObject1);
        str = ((StringBuilder)localObject3).toString();
      }
    }
    catch (Exception localException)
    {
      localObject1 = new com/truecaller/log/UnmutedException$d;
      Object localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>("groupIndexSearched: ");
      localObject2 = (Number)parama.a().get(i);
      i = ((Number)localObject2).intValue();
      ((StringBuilder)localObject3).append(i);
      i = 10;
      ((StringBuilder)localObject3).append(i);
      Object localObject4 = "totalGroupCount: ";
      ((StringBuilder)localObject3).append((String)localObject4);
      int i1 = paramMatcher.groupCount();
      ((StringBuilder)localObject3).append(i1);
      ((StringBuilder)localObject3).append(i);
      ((StringBuilder)localObject3).append("Bank name: ");
      paramMatcher = paramb.b();
      ((StringBuilder)localObject3).append(paramMatcher);
      ((StringBuilder)localObject3).append(i);
      ((StringBuilder)localObject3).append("ReminderType: ");
      paramMatcher = paramb.a();
      ((StringBuilder)localObject3).append(paramMatcher);
      ((StringBuilder)localObject3).append(i);
      ((StringBuilder)localObject3).append("MessageType: ");
      paramMatcher = paramb.c();
      ((StringBuilder)localObject3).append(paramMatcher);
      paramMatcher = ((StringBuilder)localObject3).toString();
      ((UnmutedException.d)localObject1).<init>(paramMatcher);
      localObject1 = (Throwable)localObject1;
      com.truecaller.log.d.a((Throwable)localObject1);
    }
    paramMatcher = parama.b();
    paramb = "NUMBER";
    boolean bool2 = k.a(paramMatcher, paramb);
    if (!bool2)
    {
      paramMatcher = parama.b();
      parama = "NUMBER_COMMA";
      bool2 = k.a(paramMatcher, parama);
      if (!bool2) {
        return str;
      }
    }
    return a(str);
  }
  
  private static String a(Matcher paramMatcher, b paramb)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("truecaller://utility/postpaid");
    String str1 = "?recharge_number=";
    String str2 = "&amount=";
    paramb = ((Iterable)paramb.d()).iterator();
    for (;;)
    {
      boolean bool1 = paramb.hasNext();
      if (!bool1) {
        break;
      }
      a locala = (a)paramb.next();
      Object localObject = a;
      String str3;
      if (localObject == null)
      {
        str3 = "name";
        k.a(str3);
      }
      int i = ((String)localObject).hashCode();
      int j = 107058794;
      boolean bool2;
      if (i != j)
      {
        j = 1901669035;
        if (i == j)
        {
          str3 = "MobileNumber";
          bool2 = ((String)localObject).equals(str3);
          if (bool2)
          {
            localObject = new java/lang/StringBuilder;
            ((StringBuilder)localObject).<init>();
            ((StringBuilder)localObject).append(str1);
            str1 = a(paramMatcher, locala);
            ((StringBuilder)localObject).append(str1);
            str1 = ((StringBuilder)localObject).toString();
          }
        }
      }
      else
      {
        str3 = "AmountPayable";
        bool2 = ((String)localObject).equals(str3);
        if (bool2)
        {
          localObject = new java/lang/StringBuilder;
          ((StringBuilder)localObject).<init>();
          ((StringBuilder)localObject).append(str2);
          str2 = a(paramMatcher, locala);
          ((StringBuilder)localObject).append(str2);
          str2 = ((StringBuilder)localObject).toString();
        }
      }
    }
    localStringBuilder.append(str1);
    localStringBuilder.append(str2);
    paramMatcher = localStringBuilder.toString();
    k.a(paramMatcher, "deepLinkBuilder.append(r…append(amount).toString()");
    return paramMatcher;
  }
  
  private static String b(String paramString)
  {
    String str1 = ",";
    String str2 = "";
    try
    {
      str1 = m.a(paramString, str1, str2);
      double d = Double.parseDouble(str1);
      d = Math.ceil(d);
      int i = (int)d;
      paramString = String.valueOf(i);
    }
    catch (NumberFormatException localNumberFormatException)
    {
      for (;;) {}
    }
    return paramString;
  }
  
  public final MatchedNotificationAttributes a(String paramString, com.truecaller.smsparser.models.c paramc)
  {
    k.b(paramString, "message");
    k.b(paramc, "formatList");
    Object localObject1 = ((Iterable)a).iterator();
    b localb;
    Object localObject2;
    Object localObject3;
    boolean bool2;
    do
    {
      boolean bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      localb = (b)((Iterator)localObject1).next();
      localObject2 = a;
      if (localObject2 == null)
      {
        localObject3 = "messageFormat";
        k.a((String)localObject3);
      }
      localObject2 = Pattern.compile((String)localObject2, 2);
      k.a(localObject2, "Pattern.compile(format.m…Pattern.CASE_INSENSITIVE)");
      localObject3 = paramString;
      localObject3 = (CharSequence)paramString;
      localObject2 = ((Pattern)localObject2).matcher((CharSequence)localObject3);
      localObject3 = "pattern.matcher(message)";
      k.a(localObject2, (String)localObject3);
      bool2 = ((Matcher)localObject2).find();
    } while (!bool2);
    paramString = localb.d();
    localObject1 = ((Iterable)localb.e()).iterator();
    for (;;)
    {
      bool2 = ((Iterator)localObject1).hasNext();
      if (!bool2) {
        break;
      }
      localObject3 = (NotificationAttribute)((Iterator)localObject1).next();
      int j = c;
      int k = -1;
      if (j != k)
      {
        Object localObject4 = (a)paramString.get(j);
        localObject4 = a((Matcher)localObject2, (a)localObject4, localb);
        Object localObject5 = localObject4;
        localObject5 = (CharSequence)localObject4;
        boolean bool3 = m.a((CharSequence)localObject5) ^ true;
        if (bool3)
        {
          localObject5 = "<set-?>";
          k.b(localObject4, (String)localObject5);
          d = ((String)localObject4);
        }
      }
    }
    paramString = new com/truecaller/smsparser/models/MatchedNotificationAttributes;
    String str1 = b;
    String str2 = localb.a();
    String str3 = localb.c();
    List localList = localb.e();
    paramc = localb.a();
    int m = paramc.hashCode();
    int i = 1540463468;
    if (m == i)
    {
      localObject1 = "POSTPAID";
      boolean bool4 = paramc.equals(localObject1);
      if (bool4)
      {
        paramc = a((Matcher)localObject2, localb);
        localc = paramc;
        break label353;
      }
    }
    paramc = "";
    com.truecaller.smsparser.models.c localc = paramc;
    label353:
    paramString.<init>(str1, str2, str3, localList, localc);
    return paramString;
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.smsparser.a.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */