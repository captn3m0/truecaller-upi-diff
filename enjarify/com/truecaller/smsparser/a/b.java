package com.truecaller.smsparser.a;

import android.content.Context;
import android.content.res.AssetManager;
import c.g.b.k;
import com.truecaller.smsparser.models.c;
import com.truecaller.smsparser.models.e;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public final class b
  implements a
{
  private final com.truecaller.smsparser.models.f a;
  private final Map b;
  private final Context c;
  private final com.google.gson.f d;
  
  private b(Context paramContext, com.google.gson.f paramf)
  {
    c = paramContext;
    d = paramf;
    paramContext = new java/util/HashMap;
    paramContext.<init>();
    paramContext = (Map)paramContext;
    b = paramContext;
    try
    {
      paramContext = new com/truecaller/smsparser/models/f;
      paramf = new org/json/JSONObject;
      str = "senderTagsFileNameSchema.json";
      str = c(str);
      paramf.<init>(str);
      paramContext.<init>(paramf);
    }
    catch (IOException localIOException)
    {
      paramContext = null;
    }
    a = paramContext;
  }
  
  private final e b(String paramString)
  {
    Object localObject = (e)b.get(paramString);
    if (localObject == null) {
      localObject = null;
    }
    try
    {
      localObject = c(paramString);
    }
    catch (IOException|JSONException localIOException)
    {
      Map localMap;
      String str;
      for (;;) {}
    }
    localObject = (e)d.a((String)localObject, e.class);
    localMap = b;
    str = "senderList";
    k.a(localObject, str);
    localMap.put(paramString, localObject);
    return (e)localObject;
  }
  
  private final String c(String paramString)
  {
    paramString = c.getAssets().open(paramString);
    byte[] arrayOfByte = new byte[paramString.available()];
    paramString.read(arrayOfByte);
    paramString.close();
    paramString = c.n.d.a;
    String str = new java/lang/String;
    str.<init>(arrayOfByte, paramString);
    return str;
  }
  
  public final c a(String paramString)
  {
    k.b(paramString, "tag");
    Object localObject1 = a;
    String str1 = null;
    if (localObject1 == null) {
      return null;
    }
    paramString = paramString.toLowerCase();
    k.a(paramString, "(this as java.lang.String).toLowerCase()");
    localObject1 = (String)a.get(paramString);
    if (localObject1 == null) {
      return null;
    }
    localObject1 = b((String)localObject1);
    if (localObject1 == null) {
      return null;
    }
    str1 = b;
    if (str1 == null)
    {
      localObject2 = "iconLink";
      k.a((String)localObject2);
    }
    Object localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>();
    localObject2 = (List)localObject2;
    Object localObject3 = a;
    Object localObject4;
    if (localObject3 == null)
    {
      localObject4 = "senders";
      k.a((String)localObject4);
    }
    localObject3 = ((List)localObject3).iterator();
    boolean bool1 = ((Iterator)localObject3).hasNext();
    if (bool1)
    {
      localObject4 = (com.truecaller.smsparser.models.d)((Iterator)localObject3).next();
      Object localObject5 = a;
      Object localObject6;
      if (localObject5 == null)
      {
        localObject6 = "tags";
        k.a((String)localObject6);
      }
      localObject5 = ((List)localObject5).iterator();
      for (;;)
      {
        boolean bool2 = ((Iterator)localObject5).hasNext();
        if (!bool2) {
          break;
        }
        localObject6 = (String)((Iterator)localObject5).next();
        bool2 = k.a(localObject6, paramString);
        if (bool2)
        {
          localObject6 = b;
          if (localObject6 == null)
          {
            String str2 = "formats";
            k.a(str2);
          }
          localObject6 = (Collection)localObject6;
          ((List)localObject2).addAll((Collection)localObject6);
        }
      }
    }
    paramString = new com/truecaller/smsparser/models/c;
    localObject1 = c;
    if (localObject1 == null)
    {
      localObject3 = "senderType";
      k.a((String)localObject3);
    }
    paramString.<init>((List)localObject2, str1, (String)localObject1);
    return paramString;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.smsparser.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */