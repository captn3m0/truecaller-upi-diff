package com.truecaller.smsparser;

import android.content.Context;
import c.d.f;
import c.g.b.k;
import com.d.b.w;
import com.truecaller.featuretoggles.e;
import com.truecaller.smsparser.b.a;
import com.truecaller.smsparser.b.b;
import com.truecaller.utils.i;
import dagger.a.g;
import javax.inject.Provider;

public final class h
  implements dagger.a.d
{
  private final d a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  private final Provider f;
  
  private h(d paramd, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5)
  {
    a = paramd;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
  }
  
  public static a a(Context paramContext, i parami, e parame, w paramw, f paramf)
  {
    k.b(paramContext, "context");
    k.b(parami, "networkUtil");
    k.b(parame, "featuresRegistry");
    k.b(paramw, "picasso");
    k.b(paramf, "asyncContext");
    b localb = new com/truecaller/smsparser/b/b;
    localb.<init>(paramContext, parami, parame, paramw, paramf);
    return (a)g.a((a)localb, "Cannot return null from a non-@Nullable @Provides method");
  }
  
  public static h a(d paramd, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5)
  {
    h localh = new com/truecaller/smsparser/h;
    localh.<init>(paramd, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5);
    return localh;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.smsparser.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */