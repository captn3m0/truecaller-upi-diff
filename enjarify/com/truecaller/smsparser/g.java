package com.truecaller.smsparser;

import android.content.Context;
import c.g.b.k;
import com.d.b.w;
import javax.inject.Provider;

public final class g
  implements dagger.a.d
{
  private final d a;
  private final Provider b;
  
  private g(d paramd, Provider paramProvider)
  {
    a = paramd;
    b = paramProvider;
  }
  
  public static w a(Context paramContext)
  {
    k.b(paramContext, "context");
    paramContext = w.a(paramContext);
    k.a(paramContext, "Picasso.with(context)");
    return (w)dagger.a.g.a(paramContext, "Cannot return null from a non-@Nullable @Provides method");
  }
  
  public static g a(d paramd, Provider paramProvider)
  {
    g localg = new com/truecaller/smsparser/g;
    localg.<init>(paramd, paramProvider);
    return localg;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.smsparser.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */