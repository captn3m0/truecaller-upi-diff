package com.truecaller.c;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.net.Uri;
import android.os.RemoteException;
import android.provider.CallLog.Calls;
import android.util.Pair;
import android.util.SparseBooleanArray;
import android.widget.ListView;
import com.truecaller.TrueApp;
import com.truecaller.content.TruecallerContract;
import com.truecaller.content.TruecallerContract.n;
import com.truecaller.log.d;
import com.truecaller.old.a.c;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class a
  extends com.truecaller.old.a.a
{
  private final List a;
  
  public a(c paramc, List paramList)
  {
    super(paramc, false, arrayOfObject);
    a = paramList;
  }
  
  public static List a(ListView paramListView)
  {
    if (paramListView != null)
    {
      int i = paramListView.getCount();
      ArrayList localArrayList = new java/util/ArrayList;
      localArrayList.<init>(i);
      int j = 0;
      while (j < i)
      {
        Object localObject = paramListView.getItemAtPosition(j);
        boolean bool1 = localObject instanceof Cursor;
        if (bool1)
        {
          localObject = (Cursor)localObject;
          long l1 = paramListView.getItemIdAtPosition(j);
          String str = "call_log_id";
          int k = ((Cursor)localObject).getColumnIndex(str);
          long l2 = ((Cursor)localObject).getLong(k);
          long l3 = 0L;
          boolean bool2 = l1 < l3;
          if (bool2)
          {
            localObject = new android/util/Pair;
            Long localLong1 = Long.valueOf(l1);
            Long localLong2 = Long.valueOf(l2);
            ((Pair)localObject).<init>(localLong1, localLong2);
            localArrayList.add(localObject);
          }
        }
        j += 1;
      }
      return localArrayList;
    }
    return null;
  }
  
  public static List b(ListView paramListView)
  {
    SparseBooleanArray localSparseBooleanArray;
    if (paramListView == null) {
      localSparseBooleanArray = null;
    } else {
      localSparseBooleanArray = paramListView.getCheckedItemPositions();
    }
    if (localSparseBooleanArray != null)
    {
      int i = localSparseBooleanArray.size();
      if (i > 0)
      {
        ArrayList localArrayList = new java/util/ArrayList;
        localArrayList.<init>(i);
        int j = 0;
        int k = -1;
        int m = -1;
        for (;;)
        {
          if (j < i) {
            try
            {
              boolean bool1 = localSparseBooleanArray.valueAt(j);
              if (bool1)
              {
                int n = localSparseBooleanArray.keyAt(j);
                Object localObject = paramListView.getItemAtPosition(n);
                boolean bool2 = localObject instanceof Cursor;
                if (bool2)
                {
                  localObject = (Cursor)localObject;
                  if (m == k)
                  {
                    String str = "call_log_id";
                    m = ((Cursor)localObject).getColumnIndexOrThrow(str);
                  }
                  long l1 = paramListView.getItemIdAtPosition(n);
                  long l2 = ((Cursor)localObject).getLong(m);
                  long l3 = 0L;
                  boolean bool3 = l1 < l3;
                  if (bool3)
                  {
                    Pair localPair = new android/util/Pair;
                    Long localLong1 = Long.valueOf(l1);
                    Long localLong2 = Long.valueOf(l2);
                    localPair.<init>(localLong1, localLong2);
                    localArrayList.add(localPair);
                  }
                }
              }
              j += 1;
            }
            catch (IllegalArgumentException paramListView)
            {
              d.a(paramListView);
              break label209;
            }
          }
        }
        return localArrayList;
      }
    }
    label209:
    return null;
  }
  
  protected Object doInBackground(Object... paramVarArgs)
  {
    paramVarArgs = TrueApp.x();
    int i = 0;
    if (paramVarArgs != null)
    {
      ArrayList localArrayList = new java/util/ArrayList;
      localArrayList.<init>();
      Object localObject1 = new java/util/ArrayList;
      ((ArrayList)localObject1).<init>();
      Object localObject2 = a.iterator();
      Object localObject3;
      for (;;)
      {
        boolean bool1 = ((Iterator)localObject2).hasNext();
        if (!bool1) {
          break;
        }
        localObject3 = (Pair)((Iterator)localObject2).next();
        Object localObject4 = TruecallerContract.n.a();
        Object localObject5 = (Long)first;
        long l1 = ((Long)localObject5).longValue();
        localObject4 = ContentProviderOperation.newDelete(ContentUris.withAppendedId((Uri)localObject4, l1)).build();
        localArrayList.add(localObject4);
        localObject4 = (Long)second;
        long l2 = ((Long)localObject4).longValue();
        long l3 = 0L;
        boolean bool2 = l2 < l3;
        if (bool2)
        {
          localObject4 = ContentProviderOperation.newDelete(CallLog.Calls.CONTENT_URI);
          localObject5 = "_id=?";
          int j = 1;
          String[] arrayOfString = new String[j];
          localObject3 = String.valueOf(second);
          arrayOfString[0] = localObject3;
          localObject3 = ((ContentProviderOperation.Builder)localObject4).withSelection((String)localObject5, arrayOfString).build();
          ((ArrayList)localObject1).add(localObject3);
        }
      }
      try
      {
        boolean bool3 = ((ArrayList)localObject1).isEmpty();
        if (!bool3)
        {
          localObject2 = paramVarArgs.getContentResolver();
          localObject3 = "call_log";
          ((ContentResolver)localObject2).applyBatch((String)localObject3, (ArrayList)localObject1);
        }
        int m = localArrayList.isEmpty();
        if (m == 0)
        {
          paramVarArgs = paramVarArgs.getContentResolver();
          localObject1 = TruecallerContract.a;
          paramVarArgs = paramVarArgs.applyBatch((String)localObject1, localArrayList);
          int i1 = paramVarArgs.length;
          m = 0;
          localObject1 = null;
          int n;
          for (;;)
          {
            if (i < i1) {
              try
              {
                localObject2 = paramVarArgs[i];
                localObject2 = count;
                int k = ((Integer)localObject2).intValue();
                m += k;
                i += 1;
              }
              catch (OperationApplicationException paramVarArgs)
              {
                i = n;
                break label342;
              }
              catch (RemoteException paramVarArgs)
              {
                i = n;
                break label350;
              }
              catch (RuntimeException paramVarArgs)
              {
                i = n;
                break label358;
              }
            }
          }
          i = n;
        }
      }
      catch (OperationApplicationException paramVarArgs)
      {
        d.a(paramVarArgs);
      }
      catch (RemoteException paramVarArgs)
      {
        d.a(paramVarArgs);
      }
      catch (RuntimeException paramVarArgs)
      {
        label342:
        label350:
        label358:
        d.a(paramVarArgs);
      }
    }
    return Integer.valueOf(i);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */