package com.truecaller.api.services.presence.v1;

import com.google.f.ae;
import com.google.f.j;
import io.grpc.ap;
import io.grpc.ap.a;
import io.grpc.ap.b;
import io.grpc.ap.c;
import io.grpc.b.a.b;

public final class e
{
  private static volatile ap a;
  private static volatile ap b;
  private static volatile ap c;
  
  public static e.b a(io.grpc.f paramf)
  {
    e.b localb = new com/truecaller/api/services/presence/v1/e$b;
    localb.<init>(paramf, (byte)0);
    return localb;
  }
  
  public static ap a()
  {
    Object localObject1 = a;
    if (localObject1 == null) {
      synchronized (e.class)
      {
        localObject1 = a;
        if (localObject1 == null)
        {
          localObject1 = ap.a();
          Object localObject2 = ap.c.a;
          c = ((ap.c)localObject2);
          localObject2 = "truecaller.presence.v1.Presence";
          String str = "SetLastSeen";
          localObject2 = ap.a((String)localObject2, str);
          d = ((String)localObject2);
          boolean bool = true;
          h = bool;
          localObject2 = f.b();
          localObject2 = b.a((ae)localObject2);
          a = ((ap.b)localObject2);
          localObject2 = j.getDefaultInstance();
          localObject2 = b.a((ae)localObject2);
          b = ((ap.b)localObject2);
          localObject1 = ((ap.a)localObject1).a();
          a = (ap)localObject1;
        }
      }
    }
    return localap;
  }
  
  public static e.a b(io.grpc.f paramf)
  {
    e.a locala = new com/truecaller/api/services/presence/v1/e$a;
    locala.<init>(paramf, (byte)0);
    return locala;
  }
  
  public static ap b()
  {
    Object localObject1 = b;
    if (localObject1 == null) {
      synchronized (e.class)
      {
        localObject1 = b;
        if (localObject1 == null)
        {
          localObject1 = ap.a();
          Object localObject2 = ap.c.a;
          c = ((ap.c)localObject2);
          localObject2 = "truecaller.presence.v1.Presence";
          String str = "SetPresence";
          localObject2 = ap.a((String)localObject2, str);
          d = ((String)localObject2);
          boolean bool = true;
          h = bool;
          localObject2 = h.b();
          localObject2 = b.a((ae)localObject2);
          a = ((ap.b)localObject2);
          localObject2 = j.getDefaultInstance();
          localObject2 = b.a((ae)localObject2);
          b = ((ap.b)localObject2);
          localObject1 = ((ap.a)localObject1).a();
          b = (ap)localObject1;
        }
      }
    }
    return localap;
  }
  
  public static ap c()
  {
    Object localObject1 = c;
    if (localObject1 == null) {
      synchronized (e.class)
      {
        localObject1 = c;
        if (localObject1 == null)
        {
          localObject1 = ap.a();
          Object localObject2 = ap.c.a;
          c = ((ap.c)localObject2);
          localObject2 = "truecaller.presence.v1.Presence";
          String str = "GetPresence";
          localObject2 = ap.a((String)localObject2, str);
          d = ((String)localObject2);
          boolean bool = true;
          h = bool;
          localObject2 = a.b();
          localObject2 = b.a((ae)localObject2);
          a = ((ap.b)localObject2);
          localObject2 = c.b();
          localObject2 = b.a((ae)localObject2);
          b = ((ap.b)localObject2);
          localObject1 = ((ap.a)localObject1).a();
          c = (ap)localObject1;
        }
      }
    }
    return localap;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.presence.v1.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */