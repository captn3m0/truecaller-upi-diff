package com.truecaller.api.services.presence.v1.models;

import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;
import com.google.f.r;

public final class b
  extends q
  implements c
{
  private static final b c;
  private static volatile ah d;
  public boolean a;
  private r b;
  
  static
  {
    b localb = new com/truecaller/api/services/presence/v1/models/b;
    localb.<init>();
    c = localb;
    localb.makeImmutable();
  }
  
  public static b.a c()
  {
    return (b.a)c.toBuilder();
  }
  
  public static b d()
  {
    return c;
  }
  
  public static ah e()
  {
    return c.getParserForType();
  }
  
  public final boolean a()
  {
    return a;
  }
  
  public final r b()
  {
    r localr = b;
    if (localr == null) {
      localr = r.getDefaultInstance();
    }
    return localr;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 55	com/truecaller/api/services/presence/v1/models/b$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 61	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_0
    //   19: istore 6
    //   21: aconst_null
    //   22: astore 4
    //   24: iload 5
    //   26: tableswitch	default:+46->72, 1:+458->484, 2:+454->480, 3:+452->478, 4:+441->467, 5:+368->394, 6:+108->134, 7:+364->390, 8:+56->82
    //   72: new 63	java/lang/UnsupportedOperationException
    //   75: astore_1
    //   76: aload_1
    //   77: invokespecial 64	java/lang/UnsupportedOperationException:<init>	()V
    //   80: aload_1
    //   81: athrow
    //   82: getstatic 66	com/truecaller/api/services/presence/v1/models/b:d	Lcom/google/f/ah;
    //   85: astore_1
    //   86: aload_1
    //   87: ifnonnull +43 -> 130
    //   90: ldc 2
    //   92: astore_1
    //   93: aload_1
    //   94: monitorenter
    //   95: getstatic 66	com/truecaller/api/services/presence/v1/models/b:d	Lcom/google/f/ah;
    //   98: astore_2
    //   99: aload_2
    //   100: ifnonnull +20 -> 120
    //   103: new 68	com/google/f/q$b
    //   106: astore_2
    //   107: getstatic 20	com/truecaller/api/services/presence/v1/models/b:c	Lcom/truecaller/api/services/presence/v1/models/b;
    //   110: astore_3
    //   111: aload_2
    //   112: aload_3
    //   113: invokespecial 71	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   116: aload_2
    //   117: putstatic 66	com/truecaller/api/services/presence/v1/models/b:d	Lcom/google/f/ah;
    //   120: aload_1
    //   121: monitorexit
    //   122: goto +8 -> 130
    //   125: astore_2
    //   126: aload_1
    //   127: monitorexit
    //   128: aload_2
    //   129: athrow
    //   130: getstatic 66	com/truecaller/api/services/presence/v1/models/b:d	Lcom/google/f/ah;
    //   133: areturn
    //   134: aload_2
    //   135: checkcast 73	com/google/f/g
    //   138: astore_2
    //   139: aload_3
    //   140: checkcast 75	com/google/f/n
    //   143: astore_3
    //   144: iconst_1
    //   145: istore 5
    //   147: iload 6
    //   149: ifne +241 -> 390
    //   152: aload_2
    //   153: invokevirtual 79	com/google/f/g:readTag	()I
    //   156: istore 7
    //   158: iload 7
    //   160: ifeq +168 -> 328
    //   163: bipush 8
    //   165: istore 8
    //   167: iload 7
    //   169: iload 8
    //   171: if_icmpeq +142 -> 313
    //   174: bipush 18
    //   176: istore 8
    //   178: iload 7
    //   180: iload 8
    //   182: if_icmpeq +22 -> 204
    //   185: aload_2
    //   186: iload 7
    //   188: invokevirtual 85	com/google/f/g:skipField	(I)Z
    //   191: istore 7
    //   193: iload 7
    //   195: ifne -48 -> 147
    //   198: iconst_1
    //   199: istore 6
    //   201: goto -54 -> 147
    //   204: aload_0
    //   205: getfield 34	com/truecaller/api/services/presence/v1/models/b:b	Lcom/google/f/r;
    //   208: astore 9
    //   210: aload 9
    //   212: ifnull +26 -> 238
    //   215: aload_0
    //   216: getfield 34	com/truecaller/api/services/presence/v1/models/b:b	Lcom/google/f/r;
    //   219: astore 9
    //   221: aload 9
    //   223: invokevirtual 86	com/google/f/r:toBuilder	()Lcom/google/f/q$a;
    //   226: astore 9
    //   228: aload 9
    //   230: checkcast 26	com/google/f/r$a
    //   233: astore 9
    //   235: goto +9 -> 244
    //   238: iconst_0
    //   239: istore 7
    //   241: aconst_null
    //   242: astore 9
    //   244: invokestatic 89	com/google/f/r:parser	()Lcom/google/f/ah;
    //   247: astore 10
    //   249: aload_2
    //   250: aload 10
    //   252: aload_3
    //   253: invokevirtual 93	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   256: astore 10
    //   258: aload 10
    //   260: checkcast 32	com/google/f/r
    //   263: astore 10
    //   265: aload_0
    //   266: aload 10
    //   268: putfield 34	com/truecaller/api/services/presence/v1/models/b:b	Lcom/google/f/r;
    //   271: aload 9
    //   273: ifnull -126 -> 147
    //   276: aload_0
    //   277: getfield 34	com/truecaller/api/services/presence/v1/models/b:b	Lcom/google/f/r;
    //   280: astore 10
    //   282: aload 9
    //   284: aload 10
    //   286: invokevirtual 97	com/google/f/r$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   289: pop
    //   290: aload 9
    //   292: invokevirtual 100	com/google/f/r$a:buildPartial	()Lcom/google/f/q;
    //   295: astore 9
    //   297: aload 9
    //   299: checkcast 32	com/google/f/r
    //   302: astore 9
    //   304: aload_0
    //   305: aload 9
    //   307: putfield 34	com/truecaller/api/services/presence/v1/models/b:b	Lcom/google/f/r;
    //   310: goto -163 -> 147
    //   313: aload_2
    //   314: invokevirtual 104	com/google/f/g:readBool	()Z
    //   317: istore 7
    //   319: aload_0
    //   320: iload 7
    //   322: putfield 36	com/truecaller/api/services/presence/v1/models/b:a	Z
    //   325: goto -178 -> 147
    //   328: iconst_1
    //   329: istore 6
    //   331: goto -184 -> 147
    //   334: astore_1
    //   335: goto +53 -> 388
    //   338: astore_1
    //   339: new 106	java/lang/RuntimeException
    //   342: astore_2
    //   343: new 108	com/google/f/x
    //   346: astore_3
    //   347: aload_1
    //   348: invokevirtual 114	java/io/IOException:getMessage	()Ljava/lang/String;
    //   351: astore_1
    //   352: aload_3
    //   353: aload_1
    //   354: invokespecial 117	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   357: aload_3
    //   358: aload_0
    //   359: invokevirtual 121	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   362: astore_1
    //   363: aload_2
    //   364: aload_1
    //   365: invokespecial 124	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   368: aload_2
    //   369: athrow
    //   370: astore_1
    //   371: new 106	java/lang/RuntimeException
    //   374: astore_2
    //   375: aload_1
    //   376: aload_0
    //   377: invokevirtual 121	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   380: astore_1
    //   381: aload_2
    //   382: aload_1
    //   383: invokespecial 124	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   386: aload_2
    //   387: athrow
    //   388: aload_1
    //   389: athrow
    //   390: getstatic 20	com/truecaller/api/services/presence/v1/models/b:c	Lcom/truecaller/api/services/presence/v1/models/b;
    //   393: areturn
    //   394: aload_2
    //   395: checkcast 126	com/google/f/q$k
    //   398: astore_2
    //   399: aload_3
    //   400: checkcast 2	com/truecaller/api/services/presence/v1/models/b
    //   403: astore_3
    //   404: aload_0
    //   405: getfield 36	com/truecaller/api/services/presence/v1/models/b:a	Z
    //   408: istore 5
    //   410: aload_3
    //   411: getfield 36	com/truecaller/api/services/presence/v1/models/b:a	Z
    //   414: istore 6
    //   416: aload_2
    //   417: iload 5
    //   419: iload 5
    //   421: iload 6
    //   423: iload 6
    //   425: invokeinterface 130 5 0
    //   430: istore 5
    //   432: aload_0
    //   433: iload 5
    //   435: putfield 36	com/truecaller/api/services/presence/v1/models/b:a	Z
    //   438: aload_0
    //   439: getfield 34	com/truecaller/api/services/presence/v1/models/b:b	Lcom/google/f/r;
    //   442: astore_1
    //   443: aload_3
    //   444: getfield 34	com/truecaller/api/services/presence/v1/models/b:b	Lcom/google/f/r;
    //   447: astore_3
    //   448: aload_2
    //   449: aload_1
    //   450: aload_3
    //   451: invokeinterface 134 3 0
    //   456: checkcast 32	com/google/f/r
    //   459: astore_1
    //   460: aload_0
    //   461: aload_1
    //   462: putfield 34	com/truecaller/api/services/presence/v1/models/b:b	Lcom/google/f/r;
    //   465: aload_0
    //   466: areturn
    //   467: new 42	com/truecaller/api/services/presence/v1/models/b$a
    //   470: astore_1
    //   471: aload_1
    //   472: iconst_0
    //   473: invokespecial 137	com/truecaller/api/services/presence/v1/models/b$a:<init>	(B)V
    //   476: aload_1
    //   477: areturn
    //   478: aconst_null
    //   479: areturn
    //   480: getstatic 20	com/truecaller/api/services/presence/v1/models/b:c	Lcom/truecaller/api/services/presence/v1/models/b;
    //   483: areturn
    //   484: new 2	com/truecaller/api/services/presence/v1/models/b
    //   487: astore_1
    //   488: aload_1
    //   489: invokespecial 18	com/truecaller/api/services/presence/v1/models/b:<init>	()V
    //   492: aload_1
    //   493: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	494	0	this	b
    //   0	494	1	paramj	com.google.f.q.j
    //   0	494	2	paramObject1	Object
    //   0	494	3	paramObject2	Object
    //   3	20	4	arrayOfInt	int[]
    //   9	137	5	i	int
    //   408	26	5	bool1	boolean
    //   19	405	6	bool2	boolean
    //   156	31	7	j	int
    //   191	130	7	bool3	boolean
    //   165	18	8	k	int
    //   208	98	9	localObject1	Object
    //   247	38	10	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   95	98	125	finally
    //   103	106	125	finally
    //   107	110	125	finally
    //   112	116	125	finally
    //   116	120	125	finally
    //   120	122	125	finally
    //   126	128	125	finally
    //   152	156	334	finally
    //   186	191	334	finally
    //   204	208	334	finally
    //   215	219	334	finally
    //   221	226	334	finally
    //   228	233	334	finally
    //   244	247	334	finally
    //   252	256	334	finally
    //   258	263	334	finally
    //   266	271	334	finally
    //   276	280	334	finally
    //   284	290	334	finally
    //   290	295	334	finally
    //   297	302	334	finally
    //   305	310	334	finally
    //   313	317	334	finally
    //   320	325	334	finally
    //   339	342	334	finally
    //   343	346	334	finally
    //   347	351	334	finally
    //   353	357	334	finally
    //   358	362	334	finally
    //   364	368	334	finally
    //   368	370	334	finally
    //   371	374	334	finally
    //   376	380	334	finally
    //   382	386	334	finally
    //   386	388	334	finally
    //   152	156	338	java/io/IOException
    //   186	191	338	java/io/IOException
    //   204	208	338	java/io/IOException
    //   215	219	338	java/io/IOException
    //   221	226	338	java/io/IOException
    //   228	233	338	java/io/IOException
    //   244	247	338	java/io/IOException
    //   252	256	338	java/io/IOException
    //   258	263	338	java/io/IOException
    //   266	271	338	java/io/IOException
    //   276	280	338	java/io/IOException
    //   284	290	338	java/io/IOException
    //   290	295	338	java/io/IOException
    //   297	302	338	java/io/IOException
    //   305	310	338	java/io/IOException
    //   313	317	338	java/io/IOException
    //   320	325	338	java/io/IOException
    //   152	156	370	com/google/f/x
    //   186	191	370	com/google/f/x
    //   204	208	370	com/google/f/x
    //   215	219	370	com/google/f/x
    //   221	226	370	com/google/f/x
    //   228	233	370	com/google/f/x
    //   244	247	370	com/google/f/x
    //   252	256	370	com/google/f/x
    //   258	263	370	com/google/f/x
    //   266	271	370	com/google/f/x
    //   276	280	370	com/google/f/x
    //   284	290	370	com/google/f/x
    //   290	295	370	com/google/f/x
    //   297	302	370	com/google/f/x
    //   305	310	370	com/google/f/x
    //   313	317	370	com/google/f/x
    //   320	325	370	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int k = -1;
    if (i != k) {
      return i;
    }
    boolean bool = a;
    k = 0;
    int j;
    if (bool)
    {
      int m = 1;
      j = h.computeBoolSize(m, bool);
      k = 0 + j;
    }
    r localr1 = b;
    if (localr1 != null)
    {
      r localr2 = b();
      j = h.computeMessageSize(2, localr2);
      k += j;
    }
    memoizedSerializedSize = k;
    return k;
  }
  
  public final void writeTo(h paramh)
  {
    boolean bool = a;
    if (bool)
    {
      int j = 1;
      paramh.writeBool(j, bool);
    }
    r localr1 = b;
    if (localr1 != null)
    {
      int i = 2;
      r localr2 = b();
      paramh.writeMessage(i, localr2);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.presence.v1.models.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */