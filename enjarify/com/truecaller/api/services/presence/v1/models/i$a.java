package com.truecaller.api.services.presence.v1.models;

import com.google.f.q.a;

public final class i$a
  extends q.a
  implements j
{
  private i$a()
  {
    super(locali);
  }
  
  public final a a()
  {
    copyOnWrite();
    i.a((i)instance);
    return this;
  }
  
  public final a a(boolean paramBoolean)
  {
    copyOnWrite();
    i.a((i)instance, paramBoolean);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.presence.v1.models.i.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */