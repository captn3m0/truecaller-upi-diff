package com.truecaller.api.services.presence.v1.models;

import com.google.f.ae;
import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;
import com.google.f.t;

public final class Availability
  extends q
  implements a
{
  private static final Availability d;
  private static volatile ah e;
  private int a;
  private t b;
  private int c;
  
  static
  {
    Availability localAvailability = new com/truecaller/api/services/presence/v1/models/Availability;
    localAvailability.<init>();
    d = localAvailability;
    localAvailability.makeImmutable();
  }
  
  public static Availability.a c()
  {
    return (Availability.a)d.toBuilder();
  }
  
  public static Availability d()
  {
    return d;
  }
  
  public static ah e()
  {
    return d.getParserForType();
  }
  
  private t g()
  {
    t localt = b;
    if (localt == null) {
      localt = t.getDefaultInstance();
    }
    return localt;
  }
  
  public final Availability.Status a()
  {
    int i = a;
    Availability.Status localStatus = Availability.Status.forNumber(i);
    if (localStatus == null) {
      localStatus = Availability.Status.UNRECOGNIZED;
    }
    return localStatus;
  }
  
  public final Availability.Context b()
  {
    int i = c;
    Availability.Context localContext = Availability.Context.forNumber(i);
    if (localContext == null) {
      localContext = Availability.Context.UNRECOGNIZED;
    }
    return localContext;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 84	com/truecaller/api/services/presence/v1/models/Availability$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 89	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_0
    //   19: istore 6
    //   21: aconst_null
    //   22: astore 4
    //   24: iconst_1
    //   25: istore 7
    //   27: iconst_0
    //   28: istore 8
    //   30: iload 5
    //   32: tableswitch	default:+48->80, 1:+588->620, 2:+584->616, 3:+582->614, 4:+571->603, 5:+378->410, 6:+110->142, 7:+374->406, 8:+58->90
    //   80: new 92	java/lang/UnsupportedOperationException
    //   83: astore_1
    //   84: aload_1
    //   85: invokespecial 93	java/lang/UnsupportedOperationException:<init>	()V
    //   88: aload_1
    //   89: athrow
    //   90: getstatic 95	com/truecaller/api/services/presence/v1/models/Availability:e	Lcom/google/f/ah;
    //   93: astore_1
    //   94: aload_1
    //   95: ifnonnull +43 -> 138
    //   98: ldc 2
    //   100: astore_1
    //   101: aload_1
    //   102: monitorenter
    //   103: getstatic 95	com/truecaller/api/services/presence/v1/models/Availability:e	Lcom/google/f/ah;
    //   106: astore_2
    //   107: aload_2
    //   108: ifnonnull +20 -> 128
    //   111: new 97	com/google/f/q$b
    //   114: astore_2
    //   115: getstatic 21	com/truecaller/api/services/presence/v1/models/Availability:d	Lcom/truecaller/api/services/presence/v1/models/Availability;
    //   118: astore_3
    //   119: aload_2
    //   120: aload_3
    //   121: invokespecial 100	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   124: aload_2
    //   125: putstatic 95	com/truecaller/api/services/presence/v1/models/Availability:e	Lcom/google/f/ah;
    //   128: aload_1
    //   129: monitorexit
    //   130: goto +8 -> 138
    //   133: astore_2
    //   134: aload_1
    //   135: monitorexit
    //   136: aload_2
    //   137: athrow
    //   138: getstatic 95	com/truecaller/api/services/presence/v1/models/Availability:e	Lcom/google/f/ah;
    //   141: areturn
    //   142: aload_2
    //   143: checkcast 102	com/google/f/g
    //   146: astore_2
    //   147: aload_3
    //   148: checkcast 104	com/google/f/n
    //   151: astore_3
    //   152: iload 8
    //   154: ifne +252 -> 406
    //   157: aload_2
    //   158: invokevirtual 107	com/google/f/g:readTag	()I
    //   161: istore 5
    //   163: iload 5
    //   165: ifeq +179 -> 344
    //   168: bipush 8
    //   170: istore 9
    //   172: iload 5
    //   174: iload 9
    //   176: if_icmpeq +153 -> 329
    //   179: bipush 18
    //   181: istore 9
    //   183: iload 5
    //   185: iload 9
    //   187: if_icmpeq +48 -> 235
    //   190: bipush 24
    //   192: istore 9
    //   194: iload 5
    //   196: iload 9
    //   198: if_icmpeq +22 -> 220
    //   201: aload_2
    //   202: iload 5
    //   204: invokevirtual 114	com/google/f/g:skipField	(I)Z
    //   207: istore 5
    //   209: iload 5
    //   211: ifne -59 -> 152
    //   214: iconst_1
    //   215: istore 8
    //   217: goto -65 -> 152
    //   220: aload_2
    //   221: invokevirtual 117	com/google/f/g:readEnum	()I
    //   224: istore 5
    //   226: aload_0
    //   227: iload 5
    //   229: putfield 43	com/truecaller/api/services/presence/v1/models/Availability:c	I
    //   232: goto -80 -> 152
    //   235: aload_0
    //   236: getfield 35	com/truecaller/api/services/presence/v1/models/Availability:b	Lcom/google/f/t;
    //   239: astore_1
    //   240: aload_1
    //   241: ifnull +21 -> 262
    //   244: aload_0
    //   245: getfield 35	com/truecaller/api/services/presence/v1/models/Availability:b	Lcom/google/f/t;
    //   248: astore_1
    //   249: aload_1
    //   250: invokevirtual 118	com/google/f/t:toBuilder	()Lcom/google/f/q$a;
    //   253: astore_1
    //   254: aload_1
    //   255: checkcast 27	com/google/f/t$a
    //   258: astore_1
    //   259: goto +8 -> 267
    //   262: iconst_0
    //   263: istore 5
    //   265: aconst_null
    //   266: astore_1
    //   267: invokestatic 121	com/google/f/t:parser	()Lcom/google/f/ah;
    //   270: astore 10
    //   272: aload_2
    //   273: aload 10
    //   275: aload_3
    //   276: invokevirtual 125	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   279: astore 10
    //   281: aload 10
    //   283: checkcast 33	com/google/f/t
    //   286: astore 10
    //   288: aload_0
    //   289: aload 10
    //   291: putfield 35	com/truecaller/api/services/presence/v1/models/Availability:b	Lcom/google/f/t;
    //   294: aload_1
    //   295: ifnull -143 -> 152
    //   298: aload_0
    //   299: getfield 35	com/truecaller/api/services/presence/v1/models/Availability:b	Lcom/google/f/t;
    //   302: astore 10
    //   304: aload_1
    //   305: aload 10
    //   307: invokevirtual 129	com/google/f/t$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   310: pop
    //   311: aload_1
    //   312: invokevirtual 132	com/google/f/t$a:buildPartial	()Lcom/google/f/q;
    //   315: astore_1
    //   316: aload_1
    //   317: checkcast 33	com/google/f/t
    //   320: astore_1
    //   321: aload_0
    //   322: aload_1
    //   323: putfield 35	com/truecaller/api/services/presence/v1/models/Availability:b	Lcom/google/f/t;
    //   326: goto -174 -> 152
    //   329: aload_2
    //   330: invokevirtual 117	com/google/f/g:readEnum	()I
    //   333: istore 5
    //   335: aload_0
    //   336: iload 5
    //   338: putfield 51	com/truecaller/api/services/presence/v1/models/Availability:a	I
    //   341: goto -189 -> 152
    //   344: iconst_1
    //   345: istore 8
    //   347: goto -195 -> 152
    //   350: astore_1
    //   351: goto +53 -> 404
    //   354: astore_1
    //   355: new 134	java/lang/RuntimeException
    //   358: astore_2
    //   359: new 136	com/google/f/x
    //   362: astore_3
    //   363: aload_1
    //   364: invokevirtual 142	java/io/IOException:getMessage	()Ljava/lang/String;
    //   367: astore_1
    //   368: aload_3
    //   369: aload_1
    //   370: invokespecial 145	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   373: aload_3
    //   374: aload_0
    //   375: invokevirtual 149	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   378: astore_1
    //   379: aload_2
    //   380: aload_1
    //   381: invokespecial 152	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   384: aload_2
    //   385: athrow
    //   386: astore_1
    //   387: new 134	java/lang/RuntimeException
    //   390: astore_2
    //   391: aload_1
    //   392: aload_0
    //   393: invokevirtual 149	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   396: astore_1
    //   397: aload_2
    //   398: aload_1
    //   399: invokespecial 152	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   402: aload_2
    //   403: athrow
    //   404: aload_1
    //   405: athrow
    //   406: getstatic 21	com/truecaller/api/services/presence/v1/models/Availability:d	Lcom/truecaller/api/services/presence/v1/models/Availability;
    //   409: areturn
    //   410: aload_2
    //   411: checkcast 154	com/google/f/q$k
    //   414: astore_2
    //   415: aload_3
    //   416: checkcast 2	com/truecaller/api/services/presence/v1/models/Availability
    //   419: astore_3
    //   420: aload_0
    //   421: getfield 51	com/truecaller/api/services/presence/v1/models/Availability:a	I
    //   424: istore 5
    //   426: iload 5
    //   428: ifeq +9 -> 437
    //   431: iconst_1
    //   432: istore 5
    //   434: goto +8 -> 442
    //   437: iconst_0
    //   438: istore 5
    //   440: aconst_null
    //   441: astore_1
    //   442: aload_0
    //   443: getfield 51	com/truecaller/api/services/presence/v1/models/Availability:a	I
    //   446: istore 6
    //   448: aload_3
    //   449: getfield 51	com/truecaller/api/services/presence/v1/models/Availability:a	I
    //   452: istore 9
    //   454: iload 9
    //   456: ifeq +9 -> 465
    //   459: iconst_1
    //   460: istore 9
    //   462: goto +9 -> 471
    //   465: iconst_0
    //   466: istore 9
    //   468: aconst_null
    //   469: astore 10
    //   471: aload_3
    //   472: getfield 51	com/truecaller/api/services/presence/v1/models/Availability:a	I
    //   475: istore 11
    //   477: aload_2
    //   478: iload 5
    //   480: iload 6
    //   482: iload 9
    //   484: iload 11
    //   486: invokeinterface 158 5 0
    //   491: istore 5
    //   493: aload_0
    //   494: iload 5
    //   496: putfield 51	com/truecaller/api/services/presence/v1/models/Availability:a	I
    //   499: aload_0
    //   500: getfield 35	com/truecaller/api/services/presence/v1/models/Availability:b	Lcom/google/f/t;
    //   503: astore_1
    //   504: aload_3
    //   505: getfield 35	com/truecaller/api/services/presence/v1/models/Availability:b	Lcom/google/f/t;
    //   508: astore 4
    //   510: aload_2
    //   511: aload_1
    //   512: aload 4
    //   514: invokeinterface 162 3 0
    //   519: checkcast 33	com/google/f/t
    //   522: astore_1
    //   523: aload_0
    //   524: aload_1
    //   525: putfield 35	com/truecaller/api/services/presence/v1/models/Availability:b	Lcom/google/f/t;
    //   528: aload_0
    //   529: getfield 43	com/truecaller/api/services/presence/v1/models/Availability:c	I
    //   532: istore 5
    //   534: iload 5
    //   536: ifeq +9 -> 545
    //   539: iconst_1
    //   540: istore 5
    //   542: goto +8 -> 550
    //   545: iconst_0
    //   546: istore 5
    //   548: aconst_null
    //   549: astore_1
    //   550: aload_0
    //   551: getfield 43	com/truecaller/api/services/presence/v1/models/Availability:c	I
    //   554: istore 6
    //   556: aload_3
    //   557: getfield 43	com/truecaller/api/services/presence/v1/models/Availability:c	I
    //   560: istore 9
    //   562: iload 9
    //   564: ifeq +6 -> 570
    //   567: goto +6 -> 573
    //   570: iconst_0
    //   571: istore 7
    //   573: aload_3
    //   574: getfield 43	com/truecaller/api/services/presence/v1/models/Availability:c	I
    //   577: istore 12
    //   579: aload_2
    //   580: iload 5
    //   582: iload 6
    //   584: iload 7
    //   586: iload 12
    //   588: invokeinterface 158 5 0
    //   593: istore 5
    //   595: aload_0
    //   596: iload 5
    //   598: putfield 43	com/truecaller/api/services/presence/v1/models/Availability:c	I
    //   601: aload_0
    //   602: areturn
    //   603: new 57	com/truecaller/api/services/presence/v1/models/Availability$a
    //   606: astore_1
    //   607: aload_1
    //   608: iconst_0
    //   609: invokespecial 165	com/truecaller/api/services/presence/v1/models/Availability$a:<init>	(B)V
    //   612: aload_1
    //   613: areturn
    //   614: aconst_null
    //   615: areturn
    //   616: getstatic 21	com/truecaller/api/services/presence/v1/models/Availability:d	Lcom/truecaller/api/services/presence/v1/models/Availability;
    //   619: areturn
    //   620: new 2	com/truecaller/api/services/presence/v1/models/Availability
    //   623: astore_1
    //   624: aload_1
    //   625: invokespecial 19	com/truecaller/api/services/presence/v1/models/Availability:<init>	()V
    //   628: aload_1
    //   629: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	630	0	this	Availability
    //   0	630	1	paramj	com.google.f.q.j
    //   0	630	2	paramObject1	Object
    //   0	630	3	paramObject2	Object
    //   3	510	4	localObject1	Object
    //   9	194	5	i	int
    //   207	3	5	bool1	boolean
    //   224	255	5	j	int
    //   491	90	5	k	int
    //   593	4	5	m	int
    //   19	564	6	n	int
    //   25	560	7	bool2	boolean
    //   28	318	8	i1	int
    //   170	313	9	i2	int
    //   560	3	9	i3	int
    //   270	200	10	localObject2	Object
    //   475	10	11	i4	int
    //   577	10	12	i5	int
    // Exception table:
    //   from	to	target	type
    //   103	106	133	finally
    //   111	114	133	finally
    //   115	118	133	finally
    //   120	124	133	finally
    //   124	128	133	finally
    //   128	130	133	finally
    //   134	136	133	finally
    //   157	161	350	finally
    //   202	207	350	finally
    //   220	224	350	finally
    //   227	232	350	finally
    //   235	239	350	finally
    //   244	248	350	finally
    //   249	253	350	finally
    //   254	258	350	finally
    //   267	270	350	finally
    //   275	279	350	finally
    //   281	286	350	finally
    //   289	294	350	finally
    //   298	302	350	finally
    //   305	311	350	finally
    //   311	315	350	finally
    //   316	320	350	finally
    //   322	326	350	finally
    //   329	333	350	finally
    //   336	341	350	finally
    //   355	358	350	finally
    //   359	362	350	finally
    //   363	367	350	finally
    //   369	373	350	finally
    //   374	378	350	finally
    //   380	384	350	finally
    //   384	386	350	finally
    //   387	390	350	finally
    //   392	396	350	finally
    //   398	402	350	finally
    //   402	404	350	finally
    //   157	161	354	java/io/IOException
    //   202	207	354	java/io/IOException
    //   220	224	354	java/io/IOException
    //   227	232	354	java/io/IOException
    //   235	239	354	java/io/IOException
    //   244	248	354	java/io/IOException
    //   249	253	354	java/io/IOException
    //   254	258	354	java/io/IOException
    //   267	270	354	java/io/IOException
    //   275	279	354	java/io/IOException
    //   281	286	354	java/io/IOException
    //   289	294	354	java/io/IOException
    //   298	302	354	java/io/IOException
    //   305	311	354	java/io/IOException
    //   311	315	354	java/io/IOException
    //   316	320	354	java/io/IOException
    //   322	326	354	java/io/IOException
    //   329	333	354	java/io/IOException
    //   336	341	354	java/io/IOException
    //   157	161	386	com/google/f/x
    //   202	207	386	com/google/f/x
    //   220	224	386	com/google/f/x
    //   227	232	386	com/google/f/x
    //   235	239	386	com/google/f/x
    //   244	248	386	com/google/f/x
    //   249	253	386	com/google/f/x
    //   254	258	386	com/google/f/x
    //   267	270	386	com/google/f/x
    //   275	279	386	com/google/f/x
    //   281	286	386	com/google/f/x
    //   289	294	386	com/google/f/x
    //   298	302	386	com/google/f/x
    //   305	311	386	com/google/f/x
    //   311	315	386	com/google/f/x
    //   316	320	386	com/google/f/x
    //   322	326	386	com/google/f/x
    //   329	333	386	com/google/f/x
    //   336	341	386	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int j = -1;
    if (i != j) {
      return i;
    }
    i = a;
    Object localObject = Availability.Status.UNKNOWN;
    j = ((Availability.Status)localObject).getNumber();
    int k = 0;
    if (i != j)
    {
      j = a;
      i = h.computeEnumSize(1, j);
      k = 0 + i;
    }
    t localt = b;
    if (localt != null)
    {
      localObject = g();
      i = h.computeMessageSize(2, (ae)localObject);
      k += i;
    }
    i = c;
    localObject = Availability.Context.NOTSET;
    j = ((Availability.Context)localObject).getNumber();
    if (i != j)
    {
      j = c;
      i = h.computeEnumSize(3, j);
      k += i;
    }
    memoizedSerializedSize = k;
    return k;
  }
  
  public final void writeTo(h paramh)
  {
    int i = a;
    Object localObject = Availability.Status.UNKNOWN;
    int j = ((Availability.Status)localObject).getNumber();
    if (i != j)
    {
      i = 1;
      j = a;
      paramh.writeEnum(i, j);
    }
    t localt = b;
    if (localt != null)
    {
      i = 2;
      localObject = g();
      paramh.writeMessage(i, (ae)localObject);
    }
    i = c;
    localObject = Availability.Context.NOTSET;
    j = ((Availability.Context)localObject).getNumber();
    if (i != j)
    {
      i = 3;
      j = c;
      paramh.writeEnum(i, j);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.presence.v1.models.Availability
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */