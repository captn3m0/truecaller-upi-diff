package com.truecaller.api.services.presence.v1.models;

import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;

public final class f
  extends q
  implements g
{
  private static final f d;
  private static volatile ah e;
  private boolean a;
  private int b;
  private int c;
  
  static
  {
    f localf = new com/truecaller/api/services/presence/v1/models/f;
    localf.<init>();
    d = localf;
    localf.makeImmutable();
  }
  
  public static f.a d()
  {
    return (f.a)d.toBuilder();
  }
  
  public static f e()
  {
    return d;
  }
  
  public static ah f()
  {
    return d.getParserForType();
  }
  
  public final boolean a()
  {
    return a;
  }
  
  public final int b()
  {
    return b;
  }
  
  public final int c()
  {
    return c;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 46	com/truecaller/api/services/presence/v1/models/f$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 52	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_1
    //   19: istore 6
    //   21: iconst_0
    //   22: istore 7
    //   24: iload 5
    //   26: tableswitch	default:+46->72, 1:+507->533, 2:+503->529, 3:+501->527, 4:+490->516, 5:+292->318, 6:+108->134, 7:+288->314, 8:+56->82
    //   72: new 55	java/lang/UnsupportedOperationException
    //   75: astore_1
    //   76: aload_1
    //   77: invokespecial 56	java/lang/UnsupportedOperationException:<init>	()V
    //   80: aload_1
    //   81: athrow
    //   82: getstatic 58	com/truecaller/api/services/presence/v1/models/f:e	Lcom/google/f/ah;
    //   85: astore_1
    //   86: aload_1
    //   87: ifnonnull +43 -> 130
    //   90: ldc 2
    //   92: astore_1
    //   93: aload_1
    //   94: monitorenter
    //   95: getstatic 58	com/truecaller/api/services/presence/v1/models/f:e	Lcom/google/f/ah;
    //   98: astore_2
    //   99: aload_2
    //   100: ifnonnull +20 -> 120
    //   103: new 60	com/google/f/q$b
    //   106: astore_2
    //   107: getstatic 21	com/truecaller/api/services/presence/v1/models/f:d	Lcom/truecaller/api/services/presence/v1/models/f;
    //   110: astore_3
    //   111: aload_2
    //   112: aload_3
    //   113: invokespecial 63	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   116: aload_2
    //   117: putstatic 58	com/truecaller/api/services/presence/v1/models/f:e	Lcom/google/f/ah;
    //   120: aload_1
    //   121: monitorexit
    //   122: goto +8 -> 130
    //   125: astore_2
    //   126: aload_1
    //   127: monitorexit
    //   128: aload_2
    //   129: athrow
    //   130: getstatic 58	com/truecaller/api/services/presence/v1/models/f:e	Lcom/google/f/ah;
    //   133: areturn
    //   134: aload_2
    //   135: checkcast 65	com/google/f/g
    //   138: astore_2
    //   139: iload 7
    //   141: ifne +173 -> 314
    //   144: aload_2
    //   145: invokevirtual 68	com/google/f/g:readTag	()I
    //   148: istore 5
    //   150: iload 5
    //   152: ifeq +100 -> 252
    //   155: bipush 8
    //   157: istore 8
    //   159: iload 5
    //   161: iload 8
    //   163: if_icmpeq +74 -> 237
    //   166: bipush 16
    //   168: istore 8
    //   170: iload 5
    //   172: iload 8
    //   174: if_icmpeq +48 -> 222
    //   177: bipush 24
    //   179: istore 8
    //   181: iload 5
    //   183: iload 8
    //   185: if_icmpeq +22 -> 207
    //   188: aload_2
    //   189: iload 5
    //   191: invokevirtual 75	com/google/f/g:skipField	(I)Z
    //   194: istore 5
    //   196: iload 5
    //   198: ifne -59 -> 139
    //   201: iconst_1
    //   202: istore 7
    //   204: goto -65 -> 139
    //   207: aload_2
    //   208: invokevirtual 78	com/google/f/g:readInt32	()I
    //   211: istore 5
    //   213: aload_0
    //   214: iload 5
    //   216: putfield 31	com/truecaller/api/services/presence/v1/models/f:c	I
    //   219: goto -80 -> 139
    //   222: aload_2
    //   223: invokevirtual 78	com/google/f/g:readInt32	()I
    //   226: istore 5
    //   228: aload_0
    //   229: iload 5
    //   231: putfield 27	com/truecaller/api/services/presence/v1/models/f:b	I
    //   234: goto -95 -> 139
    //   237: aload_2
    //   238: invokevirtual 82	com/google/f/g:readBool	()Z
    //   241: istore 5
    //   243: aload_0
    //   244: iload 5
    //   246: putfield 29	com/truecaller/api/services/presence/v1/models/f:a	Z
    //   249: goto -110 -> 139
    //   252: iconst_1
    //   253: istore 7
    //   255: goto -116 -> 139
    //   258: astore_1
    //   259: goto +53 -> 312
    //   262: astore_1
    //   263: new 84	java/lang/RuntimeException
    //   266: astore_2
    //   267: new 86	com/google/f/x
    //   270: astore_3
    //   271: aload_1
    //   272: invokevirtual 92	java/io/IOException:getMessage	()Ljava/lang/String;
    //   275: astore_1
    //   276: aload_3
    //   277: aload_1
    //   278: invokespecial 95	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   281: aload_3
    //   282: aload_0
    //   283: invokevirtual 99	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   286: astore_1
    //   287: aload_2
    //   288: aload_1
    //   289: invokespecial 102	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   292: aload_2
    //   293: athrow
    //   294: astore_1
    //   295: new 84	java/lang/RuntimeException
    //   298: astore_2
    //   299: aload_1
    //   300: aload_0
    //   301: invokevirtual 99	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   304: astore_1
    //   305: aload_2
    //   306: aload_1
    //   307: invokespecial 102	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   310: aload_2
    //   311: athrow
    //   312: aload_1
    //   313: athrow
    //   314: getstatic 21	com/truecaller/api/services/presence/v1/models/f:d	Lcom/truecaller/api/services/presence/v1/models/f;
    //   317: areturn
    //   318: aload_2
    //   319: checkcast 104	com/google/f/q$k
    //   322: astore_2
    //   323: aload_3
    //   324: checkcast 2	com/truecaller/api/services/presence/v1/models/f
    //   327: astore_3
    //   328: aload_0
    //   329: getfield 29	com/truecaller/api/services/presence/v1/models/f:a	Z
    //   332: istore 5
    //   334: aload_3
    //   335: getfield 29	com/truecaller/api/services/presence/v1/models/f:a	Z
    //   338: istore 9
    //   340: aload_2
    //   341: iload 5
    //   343: iload 5
    //   345: iload 9
    //   347: iload 9
    //   349: invokeinterface 108 5 0
    //   354: istore 5
    //   356: aload_0
    //   357: iload 5
    //   359: putfield 29	com/truecaller/api/services/presence/v1/models/f:a	Z
    //   362: aload_0
    //   363: getfield 27	com/truecaller/api/services/presence/v1/models/f:b	I
    //   366: istore 5
    //   368: iload 5
    //   370: ifeq +9 -> 379
    //   373: iconst_1
    //   374: istore 5
    //   376: goto +8 -> 384
    //   379: iconst_0
    //   380: istore 5
    //   382: aconst_null
    //   383: astore_1
    //   384: aload_0
    //   385: getfield 27	com/truecaller/api/services/presence/v1/models/f:b	I
    //   388: istore 9
    //   390: aload_3
    //   391: getfield 27	com/truecaller/api/services/presence/v1/models/f:b	I
    //   394: istore 10
    //   396: iload 10
    //   398: ifeq +9 -> 407
    //   401: iconst_1
    //   402: istore 10
    //   404: goto +6 -> 410
    //   407: iconst_0
    //   408: istore 10
    //   410: aload_3
    //   411: getfield 27	com/truecaller/api/services/presence/v1/models/f:b	I
    //   414: istore 11
    //   416: aload_2
    //   417: iload 5
    //   419: iload 9
    //   421: iload 10
    //   423: iload 11
    //   425: invokeinterface 112 5 0
    //   430: istore 5
    //   432: aload_0
    //   433: iload 5
    //   435: putfield 27	com/truecaller/api/services/presence/v1/models/f:b	I
    //   438: aload_0
    //   439: getfield 31	com/truecaller/api/services/presence/v1/models/f:c	I
    //   442: istore 5
    //   444: iload 5
    //   446: ifeq +9 -> 455
    //   449: iconst_1
    //   450: istore 5
    //   452: goto +8 -> 460
    //   455: iconst_0
    //   456: istore 5
    //   458: aconst_null
    //   459: astore_1
    //   460: aload_0
    //   461: getfield 31	com/truecaller/api/services/presence/v1/models/f:c	I
    //   464: istore 9
    //   466: aload_3
    //   467: getfield 31	com/truecaller/api/services/presence/v1/models/f:c	I
    //   470: istore 10
    //   472: iload 10
    //   474: ifeq +6 -> 480
    //   477: goto +9 -> 486
    //   480: iconst_0
    //   481: istore 6
    //   483: aconst_null
    //   484: astore 4
    //   486: aload_3
    //   487: getfield 31	com/truecaller/api/services/presence/v1/models/f:c	I
    //   490: istore 8
    //   492: aload_2
    //   493: iload 5
    //   495: iload 9
    //   497: iload 6
    //   499: iload 8
    //   501: invokeinterface 112 5 0
    //   506: istore 5
    //   508: aload_0
    //   509: iload 5
    //   511: putfield 31	com/truecaller/api/services/presence/v1/models/f:c	I
    //   514: aload_0
    //   515: areturn
    //   516: new 37	com/truecaller/api/services/presence/v1/models/f$a
    //   519: astore_1
    //   520: aload_1
    //   521: iconst_0
    //   522: invokespecial 115	com/truecaller/api/services/presence/v1/models/f$a:<init>	(B)V
    //   525: aload_1
    //   526: areturn
    //   527: aconst_null
    //   528: areturn
    //   529: getstatic 21	com/truecaller/api/services/presence/v1/models/f:d	Lcom/truecaller/api/services/presence/v1/models/f;
    //   532: areturn
    //   533: new 2	com/truecaller/api/services/presence/v1/models/f
    //   536: astore_1
    //   537: aload_1
    //   538: invokespecial 19	com/truecaller/api/services/presence/v1/models/f:<init>	()V
    //   541: aload_1
    //   542: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	543	0	this	f
    //   0	543	1	paramj	com.google.f.q.j
    //   0	543	2	paramObject1	Object
    //   0	543	3	paramObject2	Object
    //   3	482	4	arrayOfInt	int[]
    //   9	181	5	i	int
    //   194	3	5	bool1	boolean
    //   211	19	5	j	int
    //   241	117	5	bool2	boolean
    //   366	52	5	k	int
    //   430	64	5	m	int
    //   506	4	5	n	int
    //   19	479	6	bool3	boolean
    //   22	232	7	i1	int
    //   157	343	8	i2	int
    //   338	10	9	bool4	boolean
    //   388	108	9	i3	int
    //   394	28	10	i4	int
    //   470	3	10	i5	int
    //   414	10	11	i6	int
    // Exception table:
    //   from	to	target	type
    //   95	98	125	finally
    //   103	106	125	finally
    //   107	110	125	finally
    //   112	116	125	finally
    //   116	120	125	finally
    //   120	122	125	finally
    //   126	128	125	finally
    //   144	148	258	finally
    //   189	194	258	finally
    //   207	211	258	finally
    //   214	219	258	finally
    //   222	226	258	finally
    //   229	234	258	finally
    //   237	241	258	finally
    //   244	249	258	finally
    //   263	266	258	finally
    //   267	270	258	finally
    //   271	275	258	finally
    //   277	281	258	finally
    //   282	286	258	finally
    //   288	292	258	finally
    //   292	294	258	finally
    //   295	298	258	finally
    //   300	304	258	finally
    //   306	310	258	finally
    //   310	312	258	finally
    //   144	148	262	java/io/IOException
    //   189	194	262	java/io/IOException
    //   207	211	262	java/io/IOException
    //   214	219	262	java/io/IOException
    //   222	226	262	java/io/IOException
    //   229	234	262	java/io/IOException
    //   237	241	262	java/io/IOException
    //   244	249	262	java/io/IOException
    //   144	148	294	com/google/f/x
    //   189	194	294	com/google/f/x
    //   207	211	294	com/google/f/x
    //   214	219	294	com/google/f/x
    //   222	226	294	com/google/f/x
    //   229	234	294	com/google/f/x
    //   237	241	294	com/google/f/x
    //   244	249	294	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int k = -1;
    if (i != k) {
      return i;
    }
    boolean bool = a;
    k = 0;
    int m;
    if (bool)
    {
      m = 1;
      j = h.computeBoolSize(m, bool);
      k = 0 + j;
    }
    int j = b;
    if (j != 0)
    {
      m = 2;
      j = h.computeInt32Size(m, j);
      k += j;
    }
    j = c;
    if (j != 0)
    {
      m = 3;
      j = h.computeInt32Size(m, j);
      k += j;
    }
    memoizedSerializedSize = k;
    return k;
  }
  
  public final void writeTo(h paramh)
  {
    boolean bool = a;
    int j;
    if (bool)
    {
      j = 1;
      paramh.writeBool(j, bool);
    }
    int i = b;
    if (i != 0)
    {
      j = 2;
      paramh.writeInt32(j, i);
    }
    i = c;
    if (i != 0)
    {
      j = 3;
      paramh.writeInt32(j, i);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.presence.v1.models.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */