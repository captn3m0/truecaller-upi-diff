package com.truecaller.api.services.presence.v1.models;

import com.google.f.ah;
import com.google.f.q;

public final class Premium
  extends q
  implements h
{
  private static final Premium c;
  private static volatile ah d;
  private int a;
  private int b;
  
  static
  {
    Premium localPremium = new com/truecaller/api/services/presence/v1/models/Premium;
    localPremium.<init>();
    c = localPremium;
    localPremium.makeImmutable();
  }
  
  public static Premium a()
  {
    return c;
  }
  
  public static ah b()
  {
    return c.getParserForType();
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 32	com/truecaller/api/services/presence/v1/models/Premium$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 38	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_1
    //   19: istore 6
    //   21: iconst_0
    //   22: istore 7
    //   24: iload 5
    //   26: tableswitch	default:+46->72, 1:+447->473, 2:+443->469, 3:+441->467, 4:+430->456, 5:+266->292, 6:+108->134, 7:+262->288, 8:+56->82
    //   72: new 41	java/lang/UnsupportedOperationException
    //   75: astore_1
    //   76: aload_1
    //   77: invokespecial 42	java/lang/UnsupportedOperationException:<init>	()V
    //   80: aload_1
    //   81: athrow
    //   82: getstatic 44	com/truecaller/api/services/presence/v1/models/Premium:d	Lcom/google/f/ah;
    //   85: astore_1
    //   86: aload_1
    //   87: ifnonnull +43 -> 130
    //   90: ldc 2
    //   92: astore_1
    //   93: aload_1
    //   94: monitorenter
    //   95: getstatic 44	com/truecaller/api/services/presence/v1/models/Premium:d	Lcom/google/f/ah;
    //   98: astore_2
    //   99: aload_2
    //   100: ifnonnull +20 -> 120
    //   103: new 46	com/google/f/q$b
    //   106: astore_2
    //   107: getstatic 19	com/truecaller/api/services/presence/v1/models/Premium:c	Lcom/truecaller/api/services/presence/v1/models/Premium;
    //   110: astore_3
    //   111: aload_2
    //   112: aload_3
    //   113: invokespecial 49	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   116: aload_2
    //   117: putstatic 44	com/truecaller/api/services/presence/v1/models/Premium:d	Lcom/google/f/ah;
    //   120: aload_1
    //   121: monitorexit
    //   122: goto +8 -> 130
    //   125: astore_2
    //   126: aload_1
    //   127: monitorexit
    //   128: aload_2
    //   129: athrow
    //   130: getstatic 44	com/truecaller/api/services/presence/v1/models/Premium:d	Lcom/google/f/ah;
    //   133: areturn
    //   134: aload_2
    //   135: checkcast 51	com/google/f/g
    //   138: astore_2
    //   139: iload 7
    //   141: ifne +147 -> 288
    //   144: aload_2
    //   145: invokevirtual 54	com/google/f/g:readTag	()I
    //   148: istore 5
    //   150: iload 5
    //   152: ifeq +74 -> 226
    //   155: bipush 8
    //   157: istore 8
    //   159: iload 5
    //   161: iload 8
    //   163: if_icmpeq +48 -> 211
    //   166: bipush 16
    //   168: istore 8
    //   170: iload 5
    //   172: iload 8
    //   174: if_icmpeq +22 -> 196
    //   177: aload_2
    //   178: iload 5
    //   180: invokevirtual 60	com/google/f/g:skipField	(I)Z
    //   183: istore 5
    //   185: iload 5
    //   187: ifne -48 -> 139
    //   190: iconst_1
    //   191: istore 7
    //   193: goto -54 -> 139
    //   196: aload_2
    //   197: invokevirtual 63	com/google/f/g:readEnum	()I
    //   200: istore 5
    //   202: aload_0
    //   203: iload 5
    //   205: putfield 65	com/truecaller/api/services/presence/v1/models/Premium:b	I
    //   208: goto -69 -> 139
    //   211: aload_2
    //   212: invokevirtual 63	com/google/f/g:readEnum	()I
    //   215: istore 5
    //   217: aload_0
    //   218: iload 5
    //   220: putfield 67	com/truecaller/api/services/presence/v1/models/Premium:a	I
    //   223: goto -84 -> 139
    //   226: iconst_1
    //   227: istore 7
    //   229: goto -90 -> 139
    //   232: astore_1
    //   233: goto +53 -> 286
    //   236: astore_1
    //   237: new 69	java/lang/RuntimeException
    //   240: astore_2
    //   241: new 71	com/google/f/x
    //   244: astore_3
    //   245: aload_1
    //   246: invokevirtual 77	java/io/IOException:getMessage	()Ljava/lang/String;
    //   249: astore_1
    //   250: aload_3
    //   251: aload_1
    //   252: invokespecial 80	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   255: aload_3
    //   256: aload_0
    //   257: invokevirtual 84	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   260: astore_1
    //   261: aload_2
    //   262: aload_1
    //   263: invokespecial 87	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   266: aload_2
    //   267: athrow
    //   268: astore_1
    //   269: new 69	java/lang/RuntimeException
    //   272: astore_2
    //   273: aload_1
    //   274: aload_0
    //   275: invokevirtual 84	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   278: astore_1
    //   279: aload_2
    //   280: aload_1
    //   281: invokespecial 87	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   284: aload_2
    //   285: athrow
    //   286: aload_1
    //   287: athrow
    //   288: getstatic 19	com/truecaller/api/services/presence/v1/models/Premium:c	Lcom/truecaller/api/services/presence/v1/models/Premium;
    //   291: areturn
    //   292: aload_2
    //   293: checkcast 89	com/google/f/q$k
    //   296: astore_2
    //   297: aload_3
    //   298: checkcast 2	com/truecaller/api/services/presence/v1/models/Premium
    //   301: astore_3
    //   302: aload_0
    //   303: getfield 67	com/truecaller/api/services/presence/v1/models/Premium:a	I
    //   306: istore 5
    //   308: iload 5
    //   310: ifeq +9 -> 319
    //   313: iconst_1
    //   314: istore 5
    //   316: goto +8 -> 324
    //   319: iconst_0
    //   320: istore 5
    //   322: aconst_null
    //   323: astore_1
    //   324: aload_0
    //   325: getfield 67	com/truecaller/api/services/presence/v1/models/Premium:a	I
    //   328: istore 9
    //   330: aload_3
    //   331: getfield 67	com/truecaller/api/services/presence/v1/models/Premium:a	I
    //   334: istore 10
    //   336: iload 10
    //   338: ifeq +9 -> 347
    //   341: iconst_1
    //   342: istore 10
    //   344: goto +6 -> 350
    //   347: iconst_0
    //   348: istore 10
    //   350: aload_3
    //   351: getfield 67	com/truecaller/api/services/presence/v1/models/Premium:a	I
    //   354: istore 11
    //   356: aload_2
    //   357: iload 5
    //   359: iload 9
    //   361: iload 10
    //   363: iload 11
    //   365: invokeinterface 93 5 0
    //   370: istore 5
    //   372: aload_0
    //   373: iload 5
    //   375: putfield 67	com/truecaller/api/services/presence/v1/models/Premium:a	I
    //   378: aload_0
    //   379: getfield 65	com/truecaller/api/services/presence/v1/models/Premium:b	I
    //   382: istore 5
    //   384: iload 5
    //   386: ifeq +9 -> 395
    //   389: iconst_1
    //   390: istore 5
    //   392: goto +8 -> 400
    //   395: iconst_0
    //   396: istore 5
    //   398: aconst_null
    //   399: astore_1
    //   400: aload_0
    //   401: getfield 65	com/truecaller/api/services/presence/v1/models/Premium:b	I
    //   404: istore 9
    //   406: aload_3
    //   407: getfield 65	com/truecaller/api/services/presence/v1/models/Premium:b	I
    //   410: istore 10
    //   412: iload 10
    //   414: ifeq +6 -> 420
    //   417: goto +9 -> 426
    //   420: iconst_0
    //   421: istore 6
    //   423: aconst_null
    //   424: astore 4
    //   426: aload_3
    //   427: getfield 65	com/truecaller/api/services/presence/v1/models/Premium:b	I
    //   430: istore 8
    //   432: aload_2
    //   433: iload 5
    //   435: iload 9
    //   437: iload 6
    //   439: iload 8
    //   441: invokeinterface 93 5 0
    //   446: istore 5
    //   448: aload_0
    //   449: iload 5
    //   451: putfield 65	com/truecaller/api/services/presence/v1/models/Premium:b	I
    //   454: aload_0
    //   455: areturn
    //   456: new 95	com/truecaller/api/services/presence/v1/models/Premium$a
    //   459: astore_1
    //   460: aload_1
    //   461: iconst_0
    //   462: invokespecial 98	com/truecaller/api/services/presence/v1/models/Premium$a:<init>	(B)V
    //   465: aload_1
    //   466: areturn
    //   467: aconst_null
    //   468: areturn
    //   469: getstatic 19	com/truecaller/api/services/presence/v1/models/Premium:c	Lcom/truecaller/api/services/presence/v1/models/Premium;
    //   472: areturn
    //   473: new 2	com/truecaller/api/services/presence/v1/models/Premium
    //   476: astore_1
    //   477: aload_1
    //   478: invokespecial 17	com/truecaller/api/services/presence/v1/models/Premium:<init>	()V
    //   481: aload_1
    //   482: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	483	0	this	Premium
    //   0	483	1	paramj	com.google.f.q.j
    //   0	483	2	paramObject1	Object
    //   0	483	3	paramObject2	Object
    //   3	422	4	arrayOfInt	int[]
    //   9	170	5	i	int
    //   183	3	5	bool1	boolean
    //   200	158	5	j	int
    //   370	64	5	k	int
    //   446	4	5	m	int
    //   19	419	6	bool2	boolean
    //   22	206	7	n	int
    //   157	283	8	i1	int
    //   328	108	9	i2	int
    //   334	28	10	i3	int
    //   410	3	10	i4	int
    //   354	10	11	i5	int
    // Exception table:
    //   from	to	target	type
    //   95	98	125	finally
    //   103	106	125	finally
    //   107	110	125	finally
    //   112	116	125	finally
    //   116	120	125	finally
    //   120	122	125	finally
    //   126	128	125	finally
    //   144	148	232	finally
    //   178	183	232	finally
    //   196	200	232	finally
    //   203	208	232	finally
    //   211	215	232	finally
    //   218	223	232	finally
    //   237	240	232	finally
    //   241	244	232	finally
    //   245	249	232	finally
    //   251	255	232	finally
    //   256	260	232	finally
    //   262	266	232	finally
    //   266	268	232	finally
    //   269	272	232	finally
    //   274	278	232	finally
    //   280	284	232	finally
    //   284	286	232	finally
    //   144	148	236	java/io/IOException
    //   178	183	236	java/io/IOException
    //   196	200	236	java/io/IOException
    //   203	208	236	java/io/IOException
    //   211	215	236	java/io/IOException
    //   218	223	236	java/io/IOException
    //   144	148	268	com/google/f/x
    //   178	183	268	com/google/f/x
    //   196	200	268	com/google/f/x
    //   203	208	268	com/google/f/x
    //   211	215	268	com/google/f/x
    //   218	223	268	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int j = -1;
    if (i != j) {
      return i;
    }
    i = a;
    Object localObject = Premium.ProductLevel.None;
    j = ((Premium.ProductLevel)localObject).getNumber();
    int k = 0;
    if (i != j)
    {
      j = a;
      i = com.google.f.h.computeEnumSize(1, j);
      k = 0 + i;
    }
    i = b;
    localObject = Premium.Scope.NoneScope;
    j = ((Premium.Scope)localObject).getNumber();
    if (i != j)
    {
      j = b;
      i = com.google.f.h.computeEnumSize(2, j);
      k += i;
    }
    memoizedSerializedSize = k;
    return k;
  }
  
  public final void writeTo(com.google.f.h paramh)
  {
    int i = a;
    Object localObject = Premium.ProductLevel.None;
    int j = ((Premium.ProductLevel)localObject).getNumber();
    if (i != j)
    {
      i = 1;
      j = a;
      paramh.writeEnum(i, j);
    }
    i = b;
    localObject = Premium.Scope.NoneScope;
    j = ((Premium.Scope)localObject).getNumber();
    if (i != j)
    {
      i = 2;
      j = b;
      paramh.writeEnum(i, j);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.presence.v1.models.Premium
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */