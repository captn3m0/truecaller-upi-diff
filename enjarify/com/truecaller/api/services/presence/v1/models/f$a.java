package com.truecaller.api.services.presence.v1.models;

import com.google.f.q.a;

public final class f$a
  extends q.a
  implements g
{
  private f$a()
  {
    super(localf);
  }
  
  public final a a(int paramInt)
  {
    copyOnWrite();
    f.a((f)instance, paramInt);
    return this;
  }
  
  public final a a(boolean paramBoolean)
  {
    copyOnWrite();
    f.a((f)instance, paramBoolean);
    return this;
  }
  
  public final a b(int paramInt)
  {
    copyOnWrite();
    f.b((f)instance, paramInt);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.presence.v1.models.f.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */