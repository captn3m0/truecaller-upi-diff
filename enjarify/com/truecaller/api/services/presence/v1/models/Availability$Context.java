package com.truecaller.api.services.presence.v1.models;

import com.google.f.w.c;
import com.google.f.w.d;

public enum Availability$Context
  implements w.c
{
  public static final int CALL_VALUE = 1;
  public static final int MEETING_VALUE = 2;
  public static final int NOTSET_VALUE = 0;
  public static final int SLEEP_VALUE = 3;
  private static final w.d internalValueMap;
  private final int value;
  
  static
  {
    Object localObject = new com/truecaller/api/services/presence/v1/models/Availability$Context;
    ((Context)localObject).<init>("NOTSET", 0, 0);
    NOTSET = (Context)localObject;
    localObject = new com/truecaller/api/services/presence/v1/models/Availability$Context;
    int i = 1;
    ((Context)localObject).<init>("CALL", i, i);
    CALL = (Context)localObject;
    localObject = new com/truecaller/api/services/presence/v1/models/Availability$Context;
    int j = 2;
    ((Context)localObject).<init>("MEETING", j, j);
    MEETING = (Context)localObject;
    localObject = new com/truecaller/api/services/presence/v1/models/Availability$Context;
    int k = 3;
    ((Context)localObject).<init>("SLEEP", k, k);
    SLEEP = (Context)localObject;
    localObject = new com/truecaller/api/services/presence/v1/models/Availability$Context;
    int m = 4;
    ((Context)localObject).<init>("UNRECOGNIZED", m, -1);
    UNRECOGNIZED = (Context)localObject;
    localObject = new Context[5];
    Context localContext = NOTSET;
    localObject[0] = localContext;
    localContext = CALL;
    localObject[i] = localContext;
    localContext = MEETING;
    localObject[j] = localContext;
    localContext = SLEEP;
    localObject[k] = localContext;
    localContext = UNRECOGNIZED;
    localObject[m] = localContext;
    $VALUES = (Context[])localObject;
    localObject = new com/truecaller/api/services/presence/v1/models/Availability$Context$1;
    ((Availability.Context.1)localObject).<init>();
    internalValueMap = (w.d)localObject;
  }
  
  private Availability$Context(int paramInt1)
  {
    value = paramInt1;
  }
  
  public static Context forNumber(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return null;
    case 3: 
      return SLEEP;
    case 2: 
      return MEETING;
    case 1: 
      return CALL;
    }
    return NOTSET;
  }
  
  public static w.d internalGetValueMap()
  {
    return internalValueMap;
  }
  
  public static Context valueOf(int paramInt)
  {
    return forNumber(paramInt);
  }
  
  public final int getNumber()
  {
    return value;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.presence.v1.models.Availability.Context
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */