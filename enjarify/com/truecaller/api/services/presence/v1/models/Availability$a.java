package com.truecaller.api.services.presence.v1.models;

import com.google.f.q.a;
import com.google.f.t.a;

public final class Availability$a
  extends q.a
  implements a
{
  private Availability$a()
  {
    super(localAvailability);
  }
  
  public final a a(t.a parama)
  {
    copyOnWrite();
    Availability.a((Availability)instance, parama);
    return this;
  }
  
  public final a a(Availability.Context paramContext)
  {
    copyOnWrite();
    Availability.a((Availability)instance, paramContext);
    return this;
  }
  
  public final a a(Availability.Status paramStatus)
  {
    copyOnWrite();
    Availability.a((Availability)instance, paramStatus);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.presence.v1.models.Availability.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */