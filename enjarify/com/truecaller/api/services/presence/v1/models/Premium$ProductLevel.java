package com.truecaller.api.services.presence.v1.models;

import com.google.f.w.c;
import com.google.f.w.d;

public enum Premium$ProductLevel
  implements w.c
{
  public static final int Gold_VALUE = 2;
  public static final int None_VALUE = 0;
  public static final int Regular_VALUE = 1;
  private static final w.d internalValueMap;
  private final int value;
  
  static
  {
    Object localObject = new com/truecaller/api/services/presence/v1/models/Premium$ProductLevel;
    ((ProductLevel)localObject).<init>("None", 0, 0);
    None = (ProductLevel)localObject;
    localObject = new com/truecaller/api/services/presence/v1/models/Premium$ProductLevel;
    int i = 1;
    ((ProductLevel)localObject).<init>("Regular", i, i);
    Regular = (ProductLevel)localObject;
    localObject = new com/truecaller/api/services/presence/v1/models/Premium$ProductLevel;
    int j = 2;
    ((ProductLevel)localObject).<init>("Gold", j, j);
    Gold = (ProductLevel)localObject;
    localObject = new com/truecaller/api/services/presence/v1/models/Premium$ProductLevel;
    int k = 3;
    ((ProductLevel)localObject).<init>("UNRECOGNIZED", k, -1);
    UNRECOGNIZED = (ProductLevel)localObject;
    localObject = new ProductLevel[4];
    ProductLevel localProductLevel = None;
    localObject[0] = localProductLevel;
    localProductLevel = Regular;
    localObject[i] = localProductLevel;
    localProductLevel = Gold;
    localObject[j] = localProductLevel;
    localProductLevel = UNRECOGNIZED;
    localObject[k] = localProductLevel;
    $VALUES = (ProductLevel[])localObject;
    localObject = new com/truecaller/api/services/presence/v1/models/Premium$ProductLevel$1;
    ((Premium.ProductLevel.1)localObject).<init>();
    internalValueMap = (w.d)localObject;
  }
  
  private Premium$ProductLevel(int paramInt1)
  {
    value = paramInt1;
  }
  
  public static ProductLevel forNumber(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return null;
    case 2: 
      return Gold;
    case 1: 
      return Regular;
    }
    return None;
  }
  
  public static w.d internalGetValueMap()
  {
    return internalValueMap;
  }
  
  public static ProductLevel valueOf(int paramInt)
  {
    return forNumber(paramInt);
  }
  
  public final int getNumber()
  {
    return value;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.presence.v1.models.Premium.ProductLevel
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */