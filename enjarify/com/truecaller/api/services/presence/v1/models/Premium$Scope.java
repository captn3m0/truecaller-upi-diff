package com.truecaller.api.services.presence.v1.models;

import com.google.f.w.c;
import com.google.f.w.d;

public enum Premium$Scope
  implements w.c
{
  public static final int CustomerSupport_VALUE = 5;
  public static final int NoneScope_VALUE = 0;
  public static final int Offerwall_VALUE = 3;
  public static final int Other_VALUE = 1;
  public static final int PaidPremium_VALUE = 2;
  public static final int Partner_VALUE = 8;
  public static final int ProCampaigns_VALUE = 6;
  public static final int Promotion_VALUE = 4;
  public static final int Referrals_VALUE = 7;
  public static final int TcPay_VALUE = 9;
  public static final int TcSupport_VALUE = 10;
  public static final int Testing_VALUE = 11;
  private static final w.d internalValueMap;
  private final int value;
  
  static
  {
    Object localObject = new com/truecaller/api/services/presence/v1/models/Premium$Scope;
    ((Scope)localObject).<init>("NoneScope", 0, 0);
    NoneScope = (Scope)localObject;
    localObject = new com/truecaller/api/services/presence/v1/models/Premium$Scope;
    int i = 1;
    ((Scope)localObject).<init>("Other", i, i);
    Other = (Scope)localObject;
    localObject = new com/truecaller/api/services/presence/v1/models/Premium$Scope;
    int j = 2;
    ((Scope)localObject).<init>("PaidPremium", j, j);
    PaidPremium = (Scope)localObject;
    localObject = new com/truecaller/api/services/presence/v1/models/Premium$Scope;
    int k = 3;
    ((Scope)localObject).<init>("Offerwall", k, k);
    Offerwall = (Scope)localObject;
    localObject = new com/truecaller/api/services/presence/v1/models/Premium$Scope;
    int m = 4;
    ((Scope)localObject).<init>("Promotion", m, m);
    Promotion = (Scope)localObject;
    localObject = new com/truecaller/api/services/presence/v1/models/Premium$Scope;
    int n = 5;
    ((Scope)localObject).<init>("CustomerSupport", n, n);
    CustomerSupport = (Scope)localObject;
    localObject = new com/truecaller/api/services/presence/v1/models/Premium$Scope;
    int i1 = 6;
    ((Scope)localObject).<init>("ProCampaigns", i1, i1);
    ProCampaigns = (Scope)localObject;
    localObject = new com/truecaller/api/services/presence/v1/models/Premium$Scope;
    int i2 = 7;
    ((Scope)localObject).<init>("Referrals", i2, i2);
    Referrals = (Scope)localObject;
    localObject = new com/truecaller/api/services/presence/v1/models/Premium$Scope;
    int i3 = 8;
    ((Scope)localObject).<init>("Partner", i3, i3);
    Partner = (Scope)localObject;
    localObject = new com/truecaller/api/services/presence/v1/models/Premium$Scope;
    int i4 = 9;
    ((Scope)localObject).<init>("TcPay", i4, i4);
    TcPay = (Scope)localObject;
    localObject = new com/truecaller/api/services/presence/v1/models/Premium$Scope;
    int i5 = 10;
    ((Scope)localObject).<init>("TcSupport", i5, i5);
    TcSupport = (Scope)localObject;
    localObject = new com/truecaller/api/services/presence/v1/models/Premium$Scope;
    int i6 = 11;
    ((Scope)localObject).<init>("Testing", i6, i6);
    Testing = (Scope)localObject;
    localObject = new com/truecaller/api/services/presence/v1/models/Premium$Scope;
    int i7 = 12;
    ((Scope)localObject).<init>("UNRECOGNIZED", i7, -1);
    UNRECOGNIZED = (Scope)localObject;
    localObject = new Scope[13];
    Scope localScope = NoneScope;
    localObject[0] = localScope;
    localScope = Other;
    localObject[i] = localScope;
    localScope = PaidPremium;
    localObject[j] = localScope;
    localScope = Offerwall;
    localObject[k] = localScope;
    localScope = Promotion;
    localObject[m] = localScope;
    localScope = CustomerSupport;
    localObject[n] = localScope;
    localScope = ProCampaigns;
    localObject[i1] = localScope;
    localScope = Referrals;
    localObject[i2] = localScope;
    localScope = Partner;
    localObject[i3] = localScope;
    localScope = TcPay;
    localObject[i4] = localScope;
    localScope = TcSupport;
    localObject[i5] = localScope;
    localScope = Testing;
    localObject[i6] = localScope;
    localScope = UNRECOGNIZED;
    localObject[i7] = localScope;
    $VALUES = (Scope[])localObject;
    localObject = new com/truecaller/api/services/presence/v1/models/Premium$Scope$1;
    ((Premium.Scope.1)localObject).<init>();
    internalValueMap = (w.d)localObject;
  }
  
  private Premium$Scope(int paramInt1)
  {
    value = paramInt1;
  }
  
  public static Scope forNumber(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return null;
    case 11: 
      return Testing;
    case 10: 
      return TcSupport;
    case 9: 
      return TcPay;
    case 8: 
      return Partner;
    case 7: 
      return Referrals;
    case 6: 
      return ProCampaigns;
    case 5: 
      return CustomerSupport;
    case 4: 
      return Promotion;
    case 3: 
      return Offerwall;
    case 2: 
      return PaidPremium;
    case 1: 
      return Other;
    }
    return NoneScope;
  }
  
  public static w.d internalGetValueMap()
  {
    return internalValueMap;
  }
  
  public static Scope valueOf(int paramInt)
  {
    return forNumber(paramInt);
  }
  
  public final int getNumber()
  {
    return value;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.presence.v1.models.Premium.Scope
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */