package com.truecaller.api.services.presence.v1.models;

import com.google.f.q.a;
import com.google.f.r.a;

public final class b$a
  extends q.a
  implements c
{
  private b$a()
  {
    super(localb);
  }
  
  public final a a(r.a parama)
  {
    copyOnWrite();
    b.a((b)instance, parama);
    return this;
  }
  
  public final a a(boolean paramBoolean)
  {
    copyOnWrite();
    b.a((b)instance, paramBoolean);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.presence.v1.models.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */