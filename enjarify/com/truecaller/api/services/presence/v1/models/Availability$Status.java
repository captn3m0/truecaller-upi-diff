package com.truecaller.api.services.presence.v1.models;

import com.google.f.w.c;
import com.google.f.w.d;

public enum Availability$Status
  implements w.c
{
  public static final int AVAILABLE_VALUE = 1;
  public static final int BUSY_VALUE = 2;
  public static final int DISABLED_VALUE = 3;
  public static final int UNKNOWN_VALUE;
  private static final w.d internalValueMap;
  private final int value;
  
  static
  {
    Object localObject = new com/truecaller/api/services/presence/v1/models/Availability$Status;
    ((Status)localObject).<init>("UNKNOWN", 0, 0);
    UNKNOWN = (Status)localObject;
    localObject = new com/truecaller/api/services/presence/v1/models/Availability$Status;
    int i = 1;
    ((Status)localObject).<init>("AVAILABLE", i, i);
    AVAILABLE = (Status)localObject;
    localObject = new com/truecaller/api/services/presence/v1/models/Availability$Status;
    int j = 2;
    ((Status)localObject).<init>("BUSY", j, j);
    BUSY = (Status)localObject;
    localObject = new com/truecaller/api/services/presence/v1/models/Availability$Status;
    int k = 3;
    ((Status)localObject).<init>("DISABLED", k, k);
    DISABLED = (Status)localObject;
    localObject = new com/truecaller/api/services/presence/v1/models/Availability$Status;
    int m = 4;
    ((Status)localObject).<init>("UNRECOGNIZED", m, -1);
    UNRECOGNIZED = (Status)localObject;
    localObject = new Status[5];
    Status localStatus = UNKNOWN;
    localObject[0] = localStatus;
    localStatus = AVAILABLE;
    localObject[i] = localStatus;
    localStatus = BUSY;
    localObject[j] = localStatus;
    localStatus = DISABLED;
    localObject[k] = localStatus;
    localStatus = UNRECOGNIZED;
    localObject[m] = localStatus;
    $VALUES = (Status[])localObject;
    localObject = new com/truecaller/api/services/presence/v1/models/Availability$Status$1;
    ((Availability.Status.1)localObject).<init>();
    internalValueMap = (w.d)localObject;
  }
  
  private Availability$Status(int paramInt1)
  {
    value = paramInt1;
  }
  
  public static Status forNumber(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return null;
    case 3: 
      return DISABLED;
    case 2: 
      return BUSY;
    case 1: 
      return AVAILABLE;
    }
    return UNKNOWN;
  }
  
  public static w.d internalGetValueMap()
  {
    return internalValueMap;
  }
  
  public static Status valueOf(int paramInt)
  {
    return forNumber(paramInt);
  }
  
  public final int getNumber()
  {
    return value;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.presence.v1.models.Availability.Status
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */