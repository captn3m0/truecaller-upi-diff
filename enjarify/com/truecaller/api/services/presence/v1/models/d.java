package com.truecaller.api.services.presence.v1.models;

import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;

public final class d
  extends q
  implements e
{
  private static final d d;
  private static volatile ah e;
  public boolean a;
  private int b;
  private String c = "";
  
  static
  {
    d locald = new com/truecaller/api/services/presence/v1/models/d;
    locald.<init>();
    d = locald;
    locald.makeImmutable();
  }
  
  public static d.a a()
  {
    return (d.a)d.toBuilder();
  }
  
  public static d b()
  {
    return d;
  }
  
  public static ah c()
  {
    return d.getParserForType();
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 47	com/truecaller/api/services/presence/v1/models/d$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 53	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_0
    //   19: istore 6
    //   21: aconst_null
    //   22: astore 4
    //   24: iconst_1
    //   25: istore 7
    //   27: iload 5
    //   29: tableswitch	default:+47->76, 1:+485->514, 2:+481->510, 3:+479->508, 4:+468->497, 5:+292->321, 6:+109->138, 7:+288->317, 8:+57->86
    //   76: new 56	java/lang/UnsupportedOperationException
    //   79: astore_1
    //   80: aload_1
    //   81: invokespecial 57	java/lang/UnsupportedOperationException:<init>	()V
    //   84: aload_1
    //   85: athrow
    //   86: getstatic 59	com/truecaller/api/services/presence/v1/models/d:e	Lcom/google/f/ah;
    //   89: astore_1
    //   90: aload_1
    //   91: ifnonnull +43 -> 134
    //   94: ldc 2
    //   96: astore_1
    //   97: aload_1
    //   98: monitorenter
    //   99: getstatic 59	com/truecaller/api/services/presence/v1/models/d:e	Lcom/google/f/ah;
    //   102: astore_2
    //   103: aload_2
    //   104: ifnonnull +20 -> 124
    //   107: new 61	com/google/f/q$b
    //   110: astore_2
    //   111: getstatic 22	com/truecaller/api/services/presence/v1/models/d:d	Lcom/truecaller/api/services/presence/v1/models/d;
    //   114: astore_3
    //   115: aload_2
    //   116: aload_3
    //   117: invokespecial 64	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   120: aload_2
    //   121: putstatic 59	com/truecaller/api/services/presence/v1/models/d:e	Lcom/google/f/ah;
    //   124: aload_1
    //   125: monitorexit
    //   126: goto +8 -> 134
    //   129: astore_2
    //   130: aload_1
    //   131: monitorexit
    //   132: aload_2
    //   133: athrow
    //   134: getstatic 59	com/truecaller/api/services/presence/v1/models/d:e	Lcom/google/f/ah;
    //   137: areturn
    //   138: aload_2
    //   139: checkcast 66	com/google/f/g
    //   142: astore_2
    //   143: iload 6
    //   145: ifne +172 -> 317
    //   148: aload_2
    //   149: invokevirtual 69	com/google/f/g:readTag	()I
    //   152: istore 5
    //   154: iload 5
    //   156: ifeq +99 -> 255
    //   159: bipush 8
    //   161: istore 8
    //   163: iload 5
    //   165: iload 8
    //   167: if_icmpeq +73 -> 240
    //   170: bipush 18
    //   172: istore 8
    //   174: iload 5
    //   176: iload 8
    //   178: if_icmpeq +49 -> 227
    //   181: sipush 792
    //   184: istore 8
    //   186: iload 5
    //   188: iload 8
    //   190: if_icmpeq +22 -> 212
    //   193: aload_2
    //   194: iload 5
    //   196: invokevirtual 76	com/google/f/g:skipField	(I)Z
    //   199: istore 5
    //   201: iload 5
    //   203: ifne -60 -> 143
    //   206: iconst_1
    //   207: istore 6
    //   209: goto -66 -> 143
    //   212: aload_2
    //   213: invokevirtual 80	com/google/f/g:readBool	()Z
    //   216: istore 5
    //   218: aload_0
    //   219: iload 5
    //   221: putfield 38	com/truecaller/api/services/presence/v1/models/d:a	Z
    //   224: goto -81 -> 143
    //   227: aload_2
    //   228: invokevirtual 84	com/google/f/g:readStringRequireUtf8	()Ljava/lang/String;
    //   231: astore_1
    //   232: aload_0
    //   233: aload_1
    //   234: putfield 30	com/truecaller/api/services/presence/v1/models/d:c	Ljava/lang/String;
    //   237: goto -94 -> 143
    //   240: aload_2
    //   241: invokevirtual 87	com/google/f/g:readInt32	()I
    //   244: istore 5
    //   246: aload_0
    //   247: iload 5
    //   249: putfield 89	com/truecaller/api/services/presence/v1/models/d:b	I
    //   252: goto -109 -> 143
    //   255: iconst_1
    //   256: istore 6
    //   258: goto -115 -> 143
    //   261: astore_1
    //   262: goto +53 -> 315
    //   265: astore_1
    //   266: new 91	java/lang/RuntimeException
    //   269: astore_2
    //   270: new 93	com/google/f/x
    //   273: astore_3
    //   274: aload_1
    //   275: invokevirtual 98	java/io/IOException:getMessage	()Ljava/lang/String;
    //   278: astore_1
    //   279: aload_3
    //   280: aload_1
    //   281: invokespecial 101	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   284: aload_3
    //   285: aload_0
    //   286: invokevirtual 105	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   289: astore_1
    //   290: aload_2
    //   291: aload_1
    //   292: invokespecial 108	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   295: aload_2
    //   296: athrow
    //   297: astore_1
    //   298: new 91	java/lang/RuntimeException
    //   301: astore_2
    //   302: aload_1
    //   303: aload_0
    //   304: invokevirtual 105	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   307: astore_1
    //   308: aload_2
    //   309: aload_1
    //   310: invokespecial 108	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   313: aload_2
    //   314: athrow
    //   315: aload_1
    //   316: athrow
    //   317: getstatic 22	com/truecaller/api/services/presence/v1/models/d:d	Lcom/truecaller/api/services/presence/v1/models/d;
    //   320: areturn
    //   321: aload_2
    //   322: checkcast 110	com/google/f/q$k
    //   325: astore_2
    //   326: aload_3
    //   327: checkcast 2	com/truecaller/api/services/presence/v1/models/d
    //   330: astore_3
    //   331: aload_0
    //   332: getfield 89	com/truecaller/api/services/presence/v1/models/d:b	I
    //   335: istore 5
    //   337: iload 5
    //   339: ifeq +9 -> 348
    //   342: iconst_1
    //   343: istore 5
    //   345: goto +8 -> 353
    //   348: iconst_0
    //   349: istore 5
    //   351: aconst_null
    //   352: astore_1
    //   353: aload_0
    //   354: getfield 89	com/truecaller/api/services/presence/v1/models/d:b	I
    //   357: istore 9
    //   359: aload_3
    //   360: getfield 89	com/truecaller/api/services/presence/v1/models/d:b	I
    //   363: istore 10
    //   365: iload 10
    //   367: ifeq +6 -> 373
    //   370: iconst_1
    //   371: istore 6
    //   373: aload_3
    //   374: getfield 89	com/truecaller/api/services/presence/v1/models/d:b	I
    //   377: istore 10
    //   379: aload_2
    //   380: iload 5
    //   382: iload 9
    //   384: iload 6
    //   386: iload 10
    //   388: invokeinterface 114 5 0
    //   393: istore 5
    //   395: aload_0
    //   396: iload 5
    //   398: putfield 89	com/truecaller/api/services/presence/v1/models/d:b	I
    //   401: aload_0
    //   402: getfield 30	com/truecaller/api/services/presence/v1/models/d:c	Ljava/lang/String;
    //   405: invokevirtual 119	java/lang/String:isEmpty	()Z
    //   408: iload 7
    //   410: ixor
    //   411: istore 5
    //   413: aload_0
    //   414: getfield 30	com/truecaller/api/services/presence/v1/models/d:c	Ljava/lang/String;
    //   417: astore 4
    //   419: aload_3
    //   420: getfield 30	com/truecaller/api/services/presence/v1/models/d:c	Ljava/lang/String;
    //   423: invokevirtual 119	java/lang/String:isEmpty	()Z
    //   426: istore 9
    //   428: iload 7
    //   430: iload 9
    //   432: ixor
    //   433: istore 7
    //   435: aload_3
    //   436: getfield 30	com/truecaller/api/services/presence/v1/models/d:c	Ljava/lang/String;
    //   439: astore 11
    //   441: aload_2
    //   442: iload 5
    //   444: aload 4
    //   446: iload 7
    //   448: aload 11
    //   450: invokeinterface 123 5 0
    //   455: astore_1
    //   456: aload_0
    //   457: aload_1
    //   458: putfield 30	com/truecaller/api/services/presence/v1/models/d:c	Ljava/lang/String;
    //   461: aload_0
    //   462: getfield 38	com/truecaller/api/services/presence/v1/models/d:a	Z
    //   465: istore 5
    //   467: aload_3
    //   468: getfield 38	com/truecaller/api/services/presence/v1/models/d:a	Z
    //   471: istore 8
    //   473: aload_2
    //   474: iload 5
    //   476: iload 5
    //   478: iload 8
    //   480: iload 8
    //   482: invokeinterface 127 5 0
    //   487: istore 5
    //   489: aload_0
    //   490: iload 5
    //   492: putfield 38	com/truecaller/api/services/presence/v1/models/d:a	Z
    //   495: aload_0
    //   496: areturn
    //   497: new 36	com/truecaller/api/services/presence/v1/models/d$a
    //   500: astore_1
    //   501: aload_1
    //   502: iconst_0
    //   503: invokespecial 130	com/truecaller/api/services/presence/v1/models/d$a:<init>	(B)V
    //   506: aload_1
    //   507: areturn
    //   508: aconst_null
    //   509: areturn
    //   510: getstatic 22	com/truecaller/api/services/presence/v1/models/d:d	Lcom/truecaller/api/services/presence/v1/models/d;
    //   513: areturn
    //   514: new 2	com/truecaller/api/services/presence/v1/models/d
    //   517: astore_1
    //   518: aload_1
    //   519: invokespecial 20	com/truecaller/api/services/presence/v1/models/d:<init>	()V
    //   522: aload_1
    //   523: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	524	0	this	d
    //   0	524	1	paramj	com.google.f.q.j
    //   0	524	2	paramObject1	Object
    //   0	524	3	paramObject2	Object
    //   3	442	4	localObject	Object
    //   9	186	5	i	int
    //   199	21	5	bool1	boolean
    //   244	137	5	j	int
    //   393	4	5	k	int
    //   411	80	5	bool2	boolean
    //   19	366	6	bool3	boolean
    //   25	422	7	bool4	boolean
    //   161	30	8	m	int
    //   471	10	8	bool5	boolean
    //   357	26	9	n	int
    //   426	7	9	bool6	boolean
    //   363	24	10	i1	int
    //   439	10	11	str	String
    // Exception table:
    //   from	to	target	type
    //   99	102	129	finally
    //   107	110	129	finally
    //   111	114	129	finally
    //   116	120	129	finally
    //   120	124	129	finally
    //   124	126	129	finally
    //   130	132	129	finally
    //   148	152	261	finally
    //   194	199	261	finally
    //   212	216	261	finally
    //   219	224	261	finally
    //   227	231	261	finally
    //   233	237	261	finally
    //   240	244	261	finally
    //   247	252	261	finally
    //   266	269	261	finally
    //   270	273	261	finally
    //   274	278	261	finally
    //   280	284	261	finally
    //   285	289	261	finally
    //   291	295	261	finally
    //   295	297	261	finally
    //   298	301	261	finally
    //   303	307	261	finally
    //   309	313	261	finally
    //   313	315	261	finally
    //   148	152	265	java/io/IOException
    //   194	199	265	java/io/IOException
    //   212	216	265	java/io/IOException
    //   219	224	265	java/io/IOException
    //   227	231	265	java/io/IOException
    //   233	237	265	java/io/IOException
    //   240	244	265	java/io/IOException
    //   247	252	265	java/io/IOException
    //   148	152	297	com/google/f/x
    //   194	199	297	com/google/f/x
    //   212	216	297	com/google/f/x
    //   219	224	297	com/google/f/x
    //   227	231	297	com/google/f/x
    //   233	237	297	com/google/f/x
    //   240	244	297	com/google/f/x
    //   247	252	297	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int m = -1;
    if (i != m) {
      return i;
    }
    i = b;
    m = 0;
    int n;
    if (i != 0)
    {
      n = 1;
      i = h.computeInt32Size(n, i);
      m = 0 + i;
    }
    String str1 = c;
    boolean bool1 = str1.isEmpty();
    if (!bool1)
    {
      String str2 = c;
      int j = h.computeStringSize(2, str2);
      m += j;
    }
    boolean bool2 = a;
    if (bool2)
    {
      n = 99;
      int k = h.computeBoolSize(n, bool2);
      m += k;
    }
    memoizedSerializedSize = m;
    return m;
  }
  
  public final void writeTo(h paramh)
  {
    int i = b;
    int k;
    if (i != 0)
    {
      k = 1;
      paramh.writeInt32(k, i);
    }
    String str1 = c;
    boolean bool1 = str1.isEmpty();
    if (!bool1)
    {
      int j = 2;
      String str2 = c;
      paramh.writeString(j, str2);
    }
    boolean bool2 = a;
    if (bool2)
    {
      k = 99;
      paramh.writeBool(k, bool2);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.presence.v1.models.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */