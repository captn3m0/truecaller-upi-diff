package com.truecaller.api.services.presence.v1.models;

import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;

public final class i
  extends q
  implements j
{
  private static final i c;
  private static volatile ah d;
  private boolean a;
  private int b;
  
  static
  {
    i locali = new com/truecaller/api/services/presence/v1/models/i;
    locali.<init>();
    c = locali;
    locali.makeImmutable();
  }
  
  public static i.a c()
  {
    return (i.a)c.toBuilder();
  }
  
  public static i d()
  {
    return c;
  }
  
  public static ah e()
  {
    return c.getParserForType();
  }
  
  public final boolean a()
  {
    return a;
  }
  
  public final int b()
  {
    return b;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 44	com/truecaller/api/services/presence/v1/models/i$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 50	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_1
    //   19: istore 6
    //   21: iconst_0
    //   22: istore 7
    //   24: iload 5
    //   26: tableswitch	default:+46->72, 1:+405->431, 2:+401->427, 3:+399->425, 4:+388->414, 5:+266->292, 6:+108->134, 7:+262->288, 8:+56->82
    //   72: new 52	java/lang/UnsupportedOperationException
    //   75: astore_1
    //   76: aload_1
    //   77: invokespecial 53	java/lang/UnsupportedOperationException:<init>	()V
    //   80: aload_1
    //   81: athrow
    //   82: getstatic 55	com/truecaller/api/services/presence/v1/models/i:d	Lcom/google/f/ah;
    //   85: astore_1
    //   86: aload_1
    //   87: ifnonnull +43 -> 130
    //   90: ldc 2
    //   92: astore_1
    //   93: aload_1
    //   94: monitorenter
    //   95: getstatic 55	com/truecaller/api/services/presence/v1/models/i:d	Lcom/google/f/ah;
    //   98: astore_2
    //   99: aload_2
    //   100: ifnonnull +20 -> 120
    //   103: new 57	com/google/f/q$b
    //   106: astore_2
    //   107: getstatic 20	com/truecaller/api/services/presence/v1/models/i:c	Lcom/truecaller/api/services/presence/v1/models/i;
    //   110: astore_3
    //   111: aload_2
    //   112: aload_3
    //   113: invokespecial 60	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   116: aload_2
    //   117: putstatic 55	com/truecaller/api/services/presence/v1/models/i:d	Lcom/google/f/ah;
    //   120: aload_1
    //   121: monitorexit
    //   122: goto +8 -> 130
    //   125: astore_2
    //   126: aload_1
    //   127: monitorexit
    //   128: aload_2
    //   129: athrow
    //   130: getstatic 55	com/truecaller/api/services/presence/v1/models/i:d	Lcom/google/f/ah;
    //   133: areturn
    //   134: aload_2
    //   135: checkcast 62	com/google/f/g
    //   138: astore_2
    //   139: iload 7
    //   141: ifne +147 -> 288
    //   144: aload_2
    //   145: invokevirtual 65	com/google/f/g:readTag	()I
    //   148: istore 5
    //   150: iload 5
    //   152: ifeq +74 -> 226
    //   155: bipush 8
    //   157: istore 8
    //   159: iload 5
    //   161: iload 8
    //   163: if_icmpeq +48 -> 211
    //   166: bipush 16
    //   168: istore 8
    //   170: iload 5
    //   172: iload 8
    //   174: if_icmpeq +22 -> 196
    //   177: aload_2
    //   178: iload 5
    //   180: invokevirtual 71	com/google/f/g:skipField	(I)Z
    //   183: istore 5
    //   185: iload 5
    //   187: ifne -48 -> 139
    //   190: iconst_1
    //   191: istore 7
    //   193: goto -54 -> 139
    //   196: aload_2
    //   197: invokevirtual 74	com/google/f/g:readInt32	()I
    //   200: istore 5
    //   202: aload_0
    //   203: iload 5
    //   205: putfield 27	com/truecaller/api/services/presence/v1/models/i:b	I
    //   208: goto -69 -> 139
    //   211: aload_2
    //   212: invokevirtual 78	com/google/f/g:readBool	()Z
    //   215: istore 5
    //   217: aload_0
    //   218: iload 5
    //   220: putfield 29	com/truecaller/api/services/presence/v1/models/i:a	Z
    //   223: goto -84 -> 139
    //   226: iconst_1
    //   227: istore 7
    //   229: goto -90 -> 139
    //   232: astore_1
    //   233: goto +53 -> 286
    //   236: astore_1
    //   237: new 80	java/lang/RuntimeException
    //   240: astore_2
    //   241: new 82	com/google/f/x
    //   244: astore_3
    //   245: aload_1
    //   246: invokevirtual 88	java/io/IOException:getMessage	()Ljava/lang/String;
    //   249: astore_1
    //   250: aload_3
    //   251: aload_1
    //   252: invokespecial 91	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   255: aload_3
    //   256: aload_0
    //   257: invokevirtual 95	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   260: astore_1
    //   261: aload_2
    //   262: aload_1
    //   263: invokespecial 98	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   266: aload_2
    //   267: athrow
    //   268: astore_1
    //   269: new 80	java/lang/RuntimeException
    //   272: astore_2
    //   273: aload_1
    //   274: aload_0
    //   275: invokevirtual 95	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   278: astore_1
    //   279: aload_2
    //   280: aload_1
    //   281: invokespecial 98	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   284: aload_2
    //   285: athrow
    //   286: aload_1
    //   287: athrow
    //   288: getstatic 20	com/truecaller/api/services/presence/v1/models/i:c	Lcom/truecaller/api/services/presence/v1/models/i;
    //   291: areturn
    //   292: aload_2
    //   293: checkcast 100	com/google/f/q$k
    //   296: astore_2
    //   297: aload_3
    //   298: checkcast 2	com/truecaller/api/services/presence/v1/models/i
    //   301: astore_3
    //   302: aload_0
    //   303: getfield 29	com/truecaller/api/services/presence/v1/models/i:a	Z
    //   306: istore 5
    //   308: aload_3
    //   309: getfield 29	com/truecaller/api/services/presence/v1/models/i:a	Z
    //   312: istore 9
    //   314: aload_2
    //   315: iload 5
    //   317: iload 5
    //   319: iload 9
    //   321: iload 9
    //   323: invokeinterface 104 5 0
    //   328: istore 5
    //   330: aload_0
    //   331: iload 5
    //   333: putfield 29	com/truecaller/api/services/presence/v1/models/i:a	Z
    //   336: aload_0
    //   337: getfield 27	com/truecaller/api/services/presence/v1/models/i:b	I
    //   340: istore 5
    //   342: iload 5
    //   344: ifeq +9 -> 353
    //   347: iconst_1
    //   348: istore 5
    //   350: goto +8 -> 358
    //   353: iconst_0
    //   354: istore 5
    //   356: aconst_null
    //   357: astore_1
    //   358: aload_0
    //   359: getfield 27	com/truecaller/api/services/presence/v1/models/i:b	I
    //   362: istore 9
    //   364: aload_3
    //   365: getfield 27	com/truecaller/api/services/presence/v1/models/i:b	I
    //   368: istore 10
    //   370: iload 10
    //   372: ifeq +6 -> 378
    //   375: goto +9 -> 384
    //   378: iconst_0
    //   379: istore 6
    //   381: aconst_null
    //   382: astore 4
    //   384: aload_3
    //   385: getfield 27	com/truecaller/api/services/presence/v1/models/i:b	I
    //   388: istore 8
    //   390: aload_2
    //   391: iload 5
    //   393: iload 9
    //   395: iload 6
    //   397: iload 8
    //   399: invokeinterface 108 5 0
    //   404: istore 5
    //   406: aload_0
    //   407: iload 5
    //   409: putfield 27	com/truecaller/api/services/presence/v1/models/i:b	I
    //   412: aload_0
    //   413: areturn
    //   414: new 35	com/truecaller/api/services/presence/v1/models/i$a
    //   417: astore_1
    //   418: aload_1
    //   419: iconst_0
    //   420: invokespecial 111	com/truecaller/api/services/presence/v1/models/i$a:<init>	(B)V
    //   423: aload_1
    //   424: areturn
    //   425: aconst_null
    //   426: areturn
    //   427: getstatic 20	com/truecaller/api/services/presence/v1/models/i:c	Lcom/truecaller/api/services/presence/v1/models/i;
    //   430: areturn
    //   431: new 2	com/truecaller/api/services/presence/v1/models/i
    //   434: astore_1
    //   435: aload_1
    //   436: invokespecial 18	com/truecaller/api/services/presence/v1/models/i:<init>	()V
    //   439: aload_1
    //   440: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	441	0	this	i
    //   0	441	1	paramj	com.google.f.q.j
    //   0	441	2	paramObject1	Object
    //   0	441	3	paramObject2	Object
    //   3	380	4	arrayOfInt	int[]
    //   9	170	5	i	int
    //   183	3	5	bool1	boolean
    //   200	4	5	j	int
    //   215	117	5	bool2	boolean
    //   340	52	5	k	int
    //   404	4	5	m	int
    //   19	377	6	bool3	boolean
    //   22	206	7	n	int
    //   157	241	8	i1	int
    //   312	10	9	bool4	boolean
    //   362	32	9	i2	int
    //   368	3	10	i3	int
    // Exception table:
    //   from	to	target	type
    //   95	98	125	finally
    //   103	106	125	finally
    //   107	110	125	finally
    //   112	116	125	finally
    //   116	120	125	finally
    //   120	122	125	finally
    //   126	128	125	finally
    //   144	148	232	finally
    //   178	183	232	finally
    //   196	200	232	finally
    //   203	208	232	finally
    //   211	215	232	finally
    //   218	223	232	finally
    //   237	240	232	finally
    //   241	244	232	finally
    //   245	249	232	finally
    //   251	255	232	finally
    //   256	260	232	finally
    //   262	266	232	finally
    //   266	268	232	finally
    //   269	272	232	finally
    //   274	278	232	finally
    //   280	284	232	finally
    //   284	286	232	finally
    //   144	148	236	java/io/IOException
    //   178	183	236	java/io/IOException
    //   196	200	236	java/io/IOException
    //   203	208	236	java/io/IOException
    //   211	215	236	java/io/IOException
    //   218	223	236	java/io/IOException
    //   144	148	268	com/google/f/x
    //   178	183	268	com/google/f/x
    //   196	200	268	com/google/f/x
    //   203	208	268	com/google/f/x
    //   211	215	268	com/google/f/x
    //   218	223	268	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int k = -1;
    if (i != k) {
      return i;
    }
    boolean bool = a;
    k = 0;
    int m;
    if (bool)
    {
      m = 1;
      j = h.computeBoolSize(m, bool);
      k = 0 + j;
    }
    int j = b;
    if (j != 0)
    {
      m = 2;
      j = h.computeInt32Size(m, j);
      k += j;
    }
    memoizedSerializedSize = k;
    return k;
  }
  
  public final void writeTo(h paramh)
  {
    boolean bool = a;
    int j;
    if (bool)
    {
      j = 1;
      paramh.writeBool(j, bool);
    }
    int i = b;
    if (i != 0)
    {
      j = 2;
      paramh.writeInt32(j, i);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.presence.v1.models.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */