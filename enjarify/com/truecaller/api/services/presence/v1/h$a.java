package com.truecaller.api.services.presence.v1;

import com.google.f.am.a;
import com.google.f.q.a;
import com.truecaller.api.services.presence.v1.models.Availability;
import com.truecaller.api.services.presence.v1.models.b;
import com.truecaller.api.services.presence.v1.models.d;
import com.truecaller.api.services.presence.v1.models.f;

public final class h$a
  extends q.a
  implements i
{
  private h$a()
  {
    super(localh);
  }
  
  public final a a(am.a parama)
  {
    copyOnWrite();
    h.a((h)instance, parama);
    return this;
  }
  
  public final a a(Availability paramAvailability)
  {
    copyOnWrite();
    h.a((h)instance, paramAvailability);
    return this;
  }
  
  public final a a(b paramb)
  {
    copyOnWrite();
    h.a((h)instance, paramb);
    return this;
  }
  
  public final a a(d paramd)
  {
    copyOnWrite();
    h.a((h)instance, paramd);
    return this;
  }
  
  public final a a(f paramf)
  {
    copyOnWrite();
    h.a((h)instance, paramf);
    return this;
  }
  
  public final a a(com.truecaller.api.services.presence.v1.models.i parami)
  {
    copyOnWrite();
    h.a((h)instance, parami);
    return this;
  }
  
  public final a a(boolean paramBoolean)
  {
    copyOnWrite();
    h.a((h)instance, paramBoolean);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.presence.v1.h.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */