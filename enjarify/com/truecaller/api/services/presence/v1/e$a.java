package com.truecaller.api.services.presence.v1;

import com.google.f.j;
import io.grpc.ap;
import io.grpc.c.d;
import io.grpc.f;

public final class e$a
  extends io.grpc.c.a
{
  private e$a(f paramf)
  {
    super(paramf);
  }
  
  private e$a(f paramf, io.grpc.e parame)
  {
    super(paramf, parame);
  }
  
  public final j a(h paramh)
  {
    f localf = getChannel();
    ap localap = e.b();
    io.grpc.e locale = getCallOptions();
    return (j)d.a(localf, localap, locale, paramh);
  }
  
  public final c a(a parama)
  {
    f localf = getChannel();
    ap localap = e.c();
    io.grpc.e locale = getCallOptions();
    return (c)d.a(localf, localap, locale, parama);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.presence.v1.e.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */