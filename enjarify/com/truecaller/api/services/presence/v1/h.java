package com.truecaller.api.services.presence.v1;

import com.google.f.ae;
import com.google.f.ah;
import com.google.f.am;
import com.google.f.q;
import com.truecaller.api.services.presence.v1.models.Availability;
import com.truecaller.api.services.presence.v1.models.Premium;
import com.truecaller.api.services.presence.v1.models.b;
import com.truecaller.api.services.presence.v1.models.d;
import com.truecaller.api.services.presence.v1.models.f;

public final class h
  extends q
  implements i
{
  private static final h i;
  private static volatile ah j;
  private boolean a;
  private am b;
  private Availability c;
  private b d;
  private d e;
  private com.truecaller.api.services.presence.v1.models.i f;
  private Premium g;
  private f h;
  
  static
  {
    h localh = new com/truecaller/api/services/presence/v1/h;
    localh.<init>();
    i = localh;
    localh.makeImmutable();
  }
  
  public static h.a a()
  {
    return (h.a)i.toBuilder();
  }
  
  public static h b()
  {
    return i;
  }
  
  private am d()
  {
    am localam = b;
    if (localam == null) {
      localam = am.getDefaultInstance();
    }
    return localam;
  }
  
  private Availability e()
  {
    Availability localAvailability = c;
    if (localAvailability == null) {
      localAvailability = Availability.d();
    }
    return localAvailability;
  }
  
  private b f()
  {
    b localb = d;
    if (localb == null) {
      localb = b.d();
    }
    return localb;
  }
  
  private d g()
  {
    d locald = e;
    if (locald == null) {
      locald = d.b();
    }
    return locald;
  }
  
  private com.truecaller.api.services.presence.v1.models.i h()
  {
    com.truecaller.api.services.presence.v1.models.i locali = f;
    if (locali == null) {
      locali = com.truecaller.api.services.presence.v1.models.i.d();
    }
    return locali;
  }
  
  private Premium i()
  {
    Premium localPremium = g;
    if (localPremium == null) {
      localPremium = Premium.a();
    }
    return localPremium;
  }
  
  private f j()
  {
    f localf = h;
    if (localf == null) {
      localf = f.e();
    }
    return localf;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 108	com/truecaller/api/services/presence/v1/h$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 114	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_0
    //   19: istore 6
    //   21: aconst_null
    //   22: astore 4
    //   24: iload 5
    //   26: tableswitch	default:+46->72, 1:+1352->1378, 2:+1348->1374, 3:+1346->1372, 4:+1335->1361, 5:+1088->1114, 6:+108->134, 7:+1084->1110, 8:+56->82
    //   72: new 116	java/lang/UnsupportedOperationException
    //   75: astore_1
    //   76: aload_1
    //   77: invokespecial 117	java/lang/UnsupportedOperationException:<init>	()V
    //   80: aload_1
    //   81: athrow
    //   82: getstatic 119	com/truecaller/api/services/presence/v1/h:j	Lcom/google/f/ah;
    //   85: astore_1
    //   86: aload_1
    //   87: ifnonnull +43 -> 130
    //   90: ldc 2
    //   92: astore_1
    //   93: aload_1
    //   94: monitorenter
    //   95: getstatic 119	com/truecaller/api/services/presence/v1/h:j	Lcom/google/f/ah;
    //   98: astore_2
    //   99: aload_2
    //   100: ifnonnull +20 -> 120
    //   103: new 121	com/google/f/q$b
    //   106: astore_2
    //   107: getstatic 32	com/truecaller/api/services/presence/v1/h:i	Lcom/truecaller/api/services/presence/v1/h;
    //   110: astore_3
    //   111: aload_2
    //   112: aload_3
    //   113: invokespecial 124	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   116: aload_2
    //   117: putstatic 119	com/truecaller/api/services/presence/v1/h:j	Lcom/google/f/ah;
    //   120: aload_1
    //   121: monitorexit
    //   122: goto +8 -> 130
    //   125: astore_2
    //   126: aload_1
    //   127: monitorexit
    //   128: aload_2
    //   129: athrow
    //   130: getstatic 119	com/truecaller/api/services/presence/v1/h:j	Lcom/google/f/ah;
    //   133: areturn
    //   134: aload_2
    //   135: checkcast 126	com/google/f/g
    //   138: astore_2
    //   139: aload_3
    //   140: checkcast 128	com/google/f/n
    //   143: astore_3
    //   144: iconst_1
    //   145: istore 5
    //   147: iload 6
    //   149: ifne +961 -> 1110
    //   152: aload_2
    //   153: invokevirtual 132	com/google/f/g:readTag	()I
    //   156: istore 7
    //   158: iload 7
    //   160: ifeq +888 -> 1048
    //   163: bipush 10
    //   165: istore 8
    //   167: iload 7
    //   169: iload 8
    //   171: if_icmpeq +768 -> 939
    //   174: bipush 18
    //   176: istore 8
    //   178: iload 7
    //   180: iload 8
    //   182: if_icmpeq +648 -> 830
    //   185: bipush 26
    //   187: istore 8
    //   189: iload 7
    //   191: iload 8
    //   193: if_icmpeq +528 -> 721
    //   196: bipush 34
    //   198: istore 8
    //   200: iload 7
    //   202: iload 8
    //   204: if_icmpeq +408 -> 612
    //   207: bipush 40
    //   209: istore 8
    //   211: iload 7
    //   213: iload 8
    //   215: if_icmpeq +382 -> 597
    //   218: bipush 50
    //   220: istore 8
    //   222: iload 7
    //   224: iload 8
    //   226: if_icmpeq +262 -> 488
    //   229: bipush 58
    //   231: istore 8
    //   233: iload 7
    //   235: iload 8
    //   237: if_icmpeq +142 -> 379
    //   240: bipush 66
    //   242: istore 8
    //   244: iload 7
    //   246: iload 8
    //   248: if_icmpeq +22 -> 270
    //   251: aload_2
    //   252: iload 7
    //   254: invokevirtual 144	com/google/f/g:skipField	(I)Z
    //   257: istore 7
    //   259: iload 7
    //   261: ifne -114 -> 147
    //   264: iconst_1
    //   265: istore 6
    //   267: goto -120 -> 147
    //   270: aload_0
    //   271: getfield 63	com/truecaller/api/services/presence/v1/h:h	Lcom/truecaller/api/services/presence/v1/models/f;
    //   274: astore 9
    //   276: aload 9
    //   278: ifnull +26 -> 304
    //   281: aload_0
    //   282: getfield 63	com/truecaller/api/services/presence/v1/h:h	Lcom/truecaller/api/services/presence/v1/models/f;
    //   285: astore 9
    //   287: aload 9
    //   289: invokevirtual 145	com/truecaller/api/services/presence/v1/models/f:toBuilder	()Lcom/google/f/q$a;
    //   292: astore 9
    //   294: aload 9
    //   296: checkcast 147	com/truecaller/api/services/presence/v1/models/f$a
    //   299: astore 9
    //   301: goto +9 -> 310
    //   304: iconst_0
    //   305: istore 7
    //   307: aconst_null
    //   308: astore 9
    //   310: invokestatic 150	com/truecaller/api/services/presence/v1/models/f:f	()Lcom/google/f/ah;
    //   313: astore 10
    //   315: aload_2
    //   316: aload 10
    //   318: aload_3
    //   319: invokevirtual 154	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   322: astore 10
    //   324: aload 10
    //   326: checkcast 100	com/truecaller/api/services/presence/v1/models/f
    //   329: astore 10
    //   331: aload_0
    //   332: aload 10
    //   334: putfield 63	com/truecaller/api/services/presence/v1/h:h	Lcom/truecaller/api/services/presence/v1/models/f;
    //   337: aload 9
    //   339: ifnull -192 -> 147
    //   342: aload_0
    //   343: getfield 63	com/truecaller/api/services/presence/v1/h:h	Lcom/truecaller/api/services/presence/v1/models/f;
    //   346: astore 10
    //   348: aload 9
    //   350: aload 10
    //   352: invokevirtual 158	com/truecaller/api/services/presence/v1/models/f$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   355: pop
    //   356: aload 9
    //   358: invokevirtual 161	com/truecaller/api/services/presence/v1/models/f$a:buildPartial	()Lcom/google/f/q;
    //   361: astore 9
    //   363: aload 9
    //   365: checkcast 100	com/truecaller/api/services/presence/v1/models/f
    //   368: astore 9
    //   370: aload_0
    //   371: aload 9
    //   373: putfield 63	com/truecaller/api/services/presence/v1/h:h	Lcom/truecaller/api/services/presence/v1/models/f;
    //   376: goto -229 -> 147
    //   379: aload_0
    //   380: getfield 93	com/truecaller/api/services/presence/v1/h:g	Lcom/truecaller/api/services/presence/v1/models/Premium;
    //   383: astore 9
    //   385: aload 9
    //   387: ifnull +26 -> 413
    //   390: aload_0
    //   391: getfield 93	com/truecaller/api/services/presence/v1/h:g	Lcom/truecaller/api/services/presence/v1/models/Premium;
    //   394: astore 9
    //   396: aload 9
    //   398: invokevirtual 162	com/truecaller/api/services/presence/v1/models/Premium:toBuilder	()Lcom/google/f/q$a;
    //   401: astore 9
    //   403: aload 9
    //   405: checkcast 164	com/truecaller/api/services/presence/v1/models/Premium$a
    //   408: astore 9
    //   410: goto +9 -> 419
    //   413: iconst_0
    //   414: istore 7
    //   416: aconst_null
    //   417: astore 9
    //   419: invokestatic 166	com/truecaller/api/services/presence/v1/models/Premium:b	()Lcom/google/f/ah;
    //   422: astore 10
    //   424: aload_2
    //   425: aload 10
    //   427: aload_3
    //   428: invokevirtual 154	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   431: astore 10
    //   433: aload 10
    //   435: checkcast 95	com/truecaller/api/services/presence/v1/models/Premium
    //   438: astore 10
    //   440: aload_0
    //   441: aload 10
    //   443: putfield 93	com/truecaller/api/services/presence/v1/h:g	Lcom/truecaller/api/services/presence/v1/models/Premium;
    //   446: aload 9
    //   448: ifnull -301 -> 147
    //   451: aload_0
    //   452: getfield 93	com/truecaller/api/services/presence/v1/h:g	Lcom/truecaller/api/services/presence/v1/models/Premium;
    //   455: astore 10
    //   457: aload 9
    //   459: aload 10
    //   461: invokevirtual 167	com/truecaller/api/services/presence/v1/models/Premium$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   464: pop
    //   465: aload 9
    //   467: invokevirtual 168	com/truecaller/api/services/presence/v1/models/Premium$a:buildPartial	()Lcom/google/f/q;
    //   470: astore 9
    //   472: aload 9
    //   474: checkcast 95	com/truecaller/api/services/presence/v1/models/Premium
    //   477: astore 9
    //   479: aload_0
    //   480: aload 9
    //   482: putfield 93	com/truecaller/api/services/presence/v1/h:g	Lcom/truecaller/api/services/presence/v1/models/Premium;
    //   485: goto -338 -> 147
    //   488: aload_0
    //   489: getfield 65	com/truecaller/api/services/presence/v1/h:f	Lcom/truecaller/api/services/presence/v1/models/i;
    //   492: astore 9
    //   494: aload 9
    //   496: ifnull +26 -> 522
    //   499: aload_0
    //   500: getfield 65	com/truecaller/api/services/presence/v1/h:f	Lcom/truecaller/api/services/presence/v1/models/i;
    //   503: astore 9
    //   505: aload 9
    //   507: invokevirtual 169	com/truecaller/api/services/presence/v1/models/i:toBuilder	()Lcom/google/f/q$a;
    //   510: astore 9
    //   512: aload 9
    //   514: checkcast 171	com/truecaller/api/services/presence/v1/models/i$a
    //   517: astore 9
    //   519: goto +9 -> 528
    //   522: iconst_0
    //   523: istore 7
    //   525: aconst_null
    //   526: astore 9
    //   528: invokestatic 173	com/truecaller/api/services/presence/v1/models/i:e	()Lcom/google/f/ah;
    //   531: astore 10
    //   533: aload_2
    //   534: aload 10
    //   536: aload_3
    //   537: invokevirtual 154	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   540: astore 10
    //   542: aload 10
    //   544: checkcast 88	com/truecaller/api/services/presence/v1/models/i
    //   547: astore 10
    //   549: aload_0
    //   550: aload 10
    //   552: putfield 65	com/truecaller/api/services/presence/v1/h:f	Lcom/truecaller/api/services/presence/v1/models/i;
    //   555: aload 9
    //   557: ifnull -410 -> 147
    //   560: aload_0
    //   561: getfield 65	com/truecaller/api/services/presence/v1/h:f	Lcom/truecaller/api/services/presence/v1/models/i;
    //   564: astore 10
    //   566: aload 9
    //   568: aload 10
    //   570: invokevirtual 174	com/truecaller/api/services/presence/v1/models/i$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   573: pop
    //   574: aload 9
    //   576: invokevirtual 175	com/truecaller/api/services/presence/v1/models/i$a:buildPartial	()Lcom/google/f/q;
    //   579: astore 9
    //   581: aload 9
    //   583: checkcast 88	com/truecaller/api/services/presence/v1/models/i
    //   586: astore 9
    //   588: aload_0
    //   589: aload 9
    //   591: putfield 65	com/truecaller/api/services/presence/v1/h:f	Lcom/truecaller/api/services/presence/v1/models/i;
    //   594: goto -447 -> 147
    //   597: aload_2
    //   598: invokevirtual 179	com/google/f/g:readBool	()Z
    //   601: istore 7
    //   603: aload_0
    //   604: iload 7
    //   606: putfield 67	com/truecaller/api/services/presence/v1/h:a	Z
    //   609: goto -462 -> 147
    //   612: aload_0
    //   613: getfield 61	com/truecaller/api/services/presence/v1/h:e	Lcom/truecaller/api/services/presence/v1/models/d;
    //   616: astore 9
    //   618: aload 9
    //   620: ifnull +26 -> 646
    //   623: aload_0
    //   624: getfield 61	com/truecaller/api/services/presence/v1/h:e	Lcom/truecaller/api/services/presence/v1/models/d;
    //   627: astore 9
    //   629: aload 9
    //   631: invokevirtual 180	com/truecaller/api/services/presence/v1/models/d:toBuilder	()Lcom/google/f/q$a;
    //   634: astore 9
    //   636: aload 9
    //   638: checkcast 182	com/truecaller/api/services/presence/v1/models/d$a
    //   641: astore 9
    //   643: goto +9 -> 652
    //   646: iconst_0
    //   647: istore 7
    //   649: aconst_null
    //   650: astore 9
    //   652: invokestatic 184	com/truecaller/api/services/presence/v1/models/d:c	()Lcom/google/f/ah;
    //   655: astore 10
    //   657: aload_2
    //   658: aload 10
    //   660: aload_3
    //   661: invokevirtual 154	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   664: astore 10
    //   666: aload 10
    //   668: checkcast 83	com/truecaller/api/services/presence/v1/models/d
    //   671: astore 10
    //   673: aload_0
    //   674: aload 10
    //   676: putfield 61	com/truecaller/api/services/presence/v1/h:e	Lcom/truecaller/api/services/presence/v1/models/d;
    //   679: aload 9
    //   681: ifnull -534 -> 147
    //   684: aload_0
    //   685: getfield 61	com/truecaller/api/services/presence/v1/h:e	Lcom/truecaller/api/services/presence/v1/models/d;
    //   688: astore 10
    //   690: aload 9
    //   692: aload 10
    //   694: invokevirtual 185	com/truecaller/api/services/presence/v1/models/d$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   697: pop
    //   698: aload 9
    //   700: invokevirtual 186	com/truecaller/api/services/presence/v1/models/d$a:buildPartial	()Lcom/google/f/q;
    //   703: astore 9
    //   705: aload 9
    //   707: checkcast 83	com/truecaller/api/services/presence/v1/models/d
    //   710: astore 9
    //   712: aload_0
    //   713: aload 9
    //   715: putfield 61	com/truecaller/api/services/presence/v1/h:e	Lcom/truecaller/api/services/presence/v1/models/d;
    //   718: goto -571 -> 147
    //   721: aload_0
    //   722: getfield 52	com/truecaller/api/services/presence/v1/h:b	Lcom/google/f/am;
    //   725: astore 9
    //   727: aload 9
    //   729: ifnull +26 -> 755
    //   732: aload_0
    //   733: getfield 52	com/truecaller/api/services/presence/v1/h:b	Lcom/google/f/am;
    //   736: astore 9
    //   738: aload 9
    //   740: invokevirtual 187	com/google/f/am:toBuilder	()Lcom/google/f/q$a;
    //   743: astore 9
    //   745: aload 9
    //   747: checkcast 44	com/google/f/am$a
    //   750: astore 9
    //   752: goto +9 -> 761
    //   755: iconst_0
    //   756: istore 7
    //   758: aconst_null
    //   759: astore 9
    //   761: invokestatic 190	com/google/f/am:parser	()Lcom/google/f/ah;
    //   764: astore 10
    //   766: aload_2
    //   767: aload 10
    //   769: aload_3
    //   770: invokevirtual 154	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   773: astore 10
    //   775: aload 10
    //   777: checkcast 50	com/google/f/am
    //   780: astore 10
    //   782: aload_0
    //   783: aload 10
    //   785: putfield 52	com/truecaller/api/services/presence/v1/h:b	Lcom/google/f/am;
    //   788: aload 9
    //   790: ifnull -643 -> 147
    //   793: aload_0
    //   794: getfield 52	com/truecaller/api/services/presence/v1/h:b	Lcom/google/f/am;
    //   797: astore 10
    //   799: aload 9
    //   801: aload 10
    //   803: invokevirtual 191	com/google/f/am$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   806: pop
    //   807: aload 9
    //   809: invokevirtual 192	com/google/f/am$a:buildPartial	()Lcom/google/f/q;
    //   812: astore 9
    //   814: aload 9
    //   816: checkcast 50	com/google/f/am
    //   819: astore 9
    //   821: aload_0
    //   822: aload 9
    //   824: putfield 52	com/truecaller/api/services/presence/v1/h:b	Lcom/google/f/am;
    //   827: goto -680 -> 147
    //   830: aload_0
    //   831: getfield 59	com/truecaller/api/services/presence/v1/h:d	Lcom/truecaller/api/services/presence/v1/models/b;
    //   834: astore 9
    //   836: aload 9
    //   838: ifnull +26 -> 864
    //   841: aload_0
    //   842: getfield 59	com/truecaller/api/services/presence/v1/h:d	Lcom/truecaller/api/services/presence/v1/models/b;
    //   845: astore 9
    //   847: aload 9
    //   849: invokevirtual 193	com/truecaller/api/services/presence/v1/models/b:toBuilder	()Lcom/google/f/q$a;
    //   852: astore 9
    //   854: aload 9
    //   856: checkcast 195	com/truecaller/api/services/presence/v1/models/b$a
    //   859: astore 9
    //   861: goto +9 -> 870
    //   864: iconst_0
    //   865: istore 7
    //   867: aconst_null
    //   868: astore 9
    //   870: invokestatic 196	com/truecaller/api/services/presence/v1/models/b:e	()Lcom/google/f/ah;
    //   873: astore 10
    //   875: aload_2
    //   876: aload 10
    //   878: aload_3
    //   879: invokevirtual 154	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   882: astore 10
    //   884: aload 10
    //   886: checkcast 78	com/truecaller/api/services/presence/v1/models/b
    //   889: astore 10
    //   891: aload_0
    //   892: aload 10
    //   894: putfield 59	com/truecaller/api/services/presence/v1/h:d	Lcom/truecaller/api/services/presence/v1/models/b;
    //   897: aload 9
    //   899: ifnull -752 -> 147
    //   902: aload_0
    //   903: getfield 59	com/truecaller/api/services/presence/v1/h:d	Lcom/truecaller/api/services/presence/v1/models/b;
    //   906: astore 10
    //   908: aload 9
    //   910: aload 10
    //   912: invokevirtual 197	com/truecaller/api/services/presence/v1/models/b$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   915: pop
    //   916: aload 9
    //   918: invokevirtual 198	com/truecaller/api/services/presence/v1/models/b$a:buildPartial	()Lcom/google/f/q;
    //   921: astore 9
    //   923: aload 9
    //   925: checkcast 78	com/truecaller/api/services/presence/v1/models/b
    //   928: astore 9
    //   930: aload_0
    //   931: aload 9
    //   933: putfield 59	com/truecaller/api/services/presence/v1/h:d	Lcom/truecaller/api/services/presence/v1/models/b;
    //   936: goto -789 -> 147
    //   939: aload_0
    //   940: getfield 54	com/truecaller/api/services/presence/v1/h:c	Lcom/truecaller/api/services/presence/v1/models/Availability;
    //   943: astore 9
    //   945: aload 9
    //   947: ifnull +26 -> 973
    //   950: aload_0
    //   951: getfield 54	com/truecaller/api/services/presence/v1/h:c	Lcom/truecaller/api/services/presence/v1/models/Availability;
    //   954: astore 9
    //   956: aload 9
    //   958: invokevirtual 199	com/truecaller/api/services/presence/v1/models/Availability:toBuilder	()Lcom/google/f/q$a;
    //   961: astore 9
    //   963: aload 9
    //   965: checkcast 201	com/truecaller/api/services/presence/v1/models/Availability$a
    //   968: astore 9
    //   970: goto +9 -> 979
    //   973: iconst_0
    //   974: istore 7
    //   976: aconst_null
    //   977: astore 9
    //   979: invokestatic 202	com/truecaller/api/services/presence/v1/models/Availability:e	()Lcom/google/f/ah;
    //   982: astore 10
    //   984: aload_2
    //   985: aload 10
    //   987: aload_3
    //   988: invokevirtual 154	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   991: astore 10
    //   993: aload 10
    //   995: checkcast 73	com/truecaller/api/services/presence/v1/models/Availability
    //   998: astore 10
    //   1000: aload_0
    //   1001: aload 10
    //   1003: putfield 54	com/truecaller/api/services/presence/v1/h:c	Lcom/truecaller/api/services/presence/v1/models/Availability;
    //   1006: aload 9
    //   1008: ifnull -861 -> 147
    //   1011: aload_0
    //   1012: getfield 54	com/truecaller/api/services/presence/v1/h:c	Lcom/truecaller/api/services/presence/v1/models/Availability;
    //   1015: astore 10
    //   1017: aload 9
    //   1019: aload 10
    //   1021: invokevirtual 203	com/truecaller/api/services/presence/v1/models/Availability$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   1024: pop
    //   1025: aload 9
    //   1027: invokevirtual 204	com/truecaller/api/services/presence/v1/models/Availability$a:buildPartial	()Lcom/google/f/q;
    //   1030: astore 9
    //   1032: aload 9
    //   1034: checkcast 73	com/truecaller/api/services/presence/v1/models/Availability
    //   1037: astore 9
    //   1039: aload_0
    //   1040: aload 9
    //   1042: putfield 54	com/truecaller/api/services/presence/v1/h:c	Lcom/truecaller/api/services/presence/v1/models/Availability;
    //   1045: goto -898 -> 147
    //   1048: iconst_1
    //   1049: istore 6
    //   1051: goto -904 -> 147
    //   1054: astore_1
    //   1055: goto +53 -> 1108
    //   1058: astore_1
    //   1059: new 206	java/lang/RuntimeException
    //   1062: astore_2
    //   1063: new 208	com/google/f/x
    //   1066: astore_3
    //   1067: aload_1
    //   1068: invokevirtual 214	java/io/IOException:getMessage	()Ljava/lang/String;
    //   1071: astore_1
    //   1072: aload_3
    //   1073: aload_1
    //   1074: invokespecial 217	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   1077: aload_3
    //   1078: aload_0
    //   1079: invokevirtual 221	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   1082: astore_1
    //   1083: aload_2
    //   1084: aload_1
    //   1085: invokespecial 224	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   1088: aload_2
    //   1089: athrow
    //   1090: astore_1
    //   1091: new 206	java/lang/RuntimeException
    //   1094: astore_2
    //   1095: aload_1
    //   1096: aload_0
    //   1097: invokevirtual 221	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   1100: astore_1
    //   1101: aload_2
    //   1102: aload_1
    //   1103: invokespecial 224	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   1106: aload_2
    //   1107: athrow
    //   1108: aload_1
    //   1109: athrow
    //   1110: getstatic 32	com/truecaller/api/services/presence/v1/h:i	Lcom/truecaller/api/services/presence/v1/h;
    //   1113: areturn
    //   1114: aload_2
    //   1115: checkcast 226	com/google/f/q$k
    //   1118: astore_2
    //   1119: aload_3
    //   1120: checkcast 2	com/truecaller/api/services/presence/v1/h
    //   1123: astore_3
    //   1124: aload_0
    //   1125: getfield 67	com/truecaller/api/services/presence/v1/h:a	Z
    //   1128: istore 5
    //   1130: aload_3
    //   1131: getfield 67	com/truecaller/api/services/presence/v1/h:a	Z
    //   1134: istore 6
    //   1136: aload_2
    //   1137: iload 5
    //   1139: iload 5
    //   1141: iload 6
    //   1143: iload 6
    //   1145: invokeinterface 230 5 0
    //   1150: istore 5
    //   1152: aload_0
    //   1153: iload 5
    //   1155: putfield 67	com/truecaller/api/services/presence/v1/h:a	Z
    //   1158: aload_0
    //   1159: getfield 52	com/truecaller/api/services/presence/v1/h:b	Lcom/google/f/am;
    //   1162: astore_1
    //   1163: aload_3
    //   1164: getfield 52	com/truecaller/api/services/presence/v1/h:b	Lcom/google/f/am;
    //   1167: astore 4
    //   1169: aload_2
    //   1170: aload_1
    //   1171: aload 4
    //   1173: invokeinterface 234 3 0
    //   1178: checkcast 50	com/google/f/am
    //   1181: astore_1
    //   1182: aload_0
    //   1183: aload_1
    //   1184: putfield 52	com/truecaller/api/services/presence/v1/h:b	Lcom/google/f/am;
    //   1187: aload_0
    //   1188: getfield 54	com/truecaller/api/services/presence/v1/h:c	Lcom/truecaller/api/services/presence/v1/models/Availability;
    //   1191: astore_1
    //   1192: aload_3
    //   1193: getfield 54	com/truecaller/api/services/presence/v1/h:c	Lcom/truecaller/api/services/presence/v1/models/Availability;
    //   1196: astore 4
    //   1198: aload_2
    //   1199: aload_1
    //   1200: aload 4
    //   1202: invokeinterface 234 3 0
    //   1207: checkcast 73	com/truecaller/api/services/presence/v1/models/Availability
    //   1210: astore_1
    //   1211: aload_0
    //   1212: aload_1
    //   1213: putfield 54	com/truecaller/api/services/presence/v1/h:c	Lcom/truecaller/api/services/presence/v1/models/Availability;
    //   1216: aload_0
    //   1217: getfield 59	com/truecaller/api/services/presence/v1/h:d	Lcom/truecaller/api/services/presence/v1/models/b;
    //   1220: astore_1
    //   1221: aload_3
    //   1222: getfield 59	com/truecaller/api/services/presence/v1/h:d	Lcom/truecaller/api/services/presence/v1/models/b;
    //   1225: astore 4
    //   1227: aload_2
    //   1228: aload_1
    //   1229: aload 4
    //   1231: invokeinterface 234 3 0
    //   1236: checkcast 78	com/truecaller/api/services/presence/v1/models/b
    //   1239: astore_1
    //   1240: aload_0
    //   1241: aload_1
    //   1242: putfield 59	com/truecaller/api/services/presence/v1/h:d	Lcom/truecaller/api/services/presence/v1/models/b;
    //   1245: aload_0
    //   1246: getfield 61	com/truecaller/api/services/presence/v1/h:e	Lcom/truecaller/api/services/presence/v1/models/d;
    //   1249: astore_1
    //   1250: aload_3
    //   1251: getfield 61	com/truecaller/api/services/presence/v1/h:e	Lcom/truecaller/api/services/presence/v1/models/d;
    //   1254: astore 4
    //   1256: aload_2
    //   1257: aload_1
    //   1258: aload 4
    //   1260: invokeinterface 234 3 0
    //   1265: checkcast 83	com/truecaller/api/services/presence/v1/models/d
    //   1268: astore_1
    //   1269: aload_0
    //   1270: aload_1
    //   1271: putfield 61	com/truecaller/api/services/presence/v1/h:e	Lcom/truecaller/api/services/presence/v1/models/d;
    //   1274: aload_0
    //   1275: getfield 65	com/truecaller/api/services/presence/v1/h:f	Lcom/truecaller/api/services/presence/v1/models/i;
    //   1278: astore_1
    //   1279: aload_3
    //   1280: getfield 65	com/truecaller/api/services/presence/v1/h:f	Lcom/truecaller/api/services/presence/v1/models/i;
    //   1283: astore 4
    //   1285: aload_2
    //   1286: aload_1
    //   1287: aload 4
    //   1289: invokeinterface 234 3 0
    //   1294: checkcast 88	com/truecaller/api/services/presence/v1/models/i
    //   1297: astore_1
    //   1298: aload_0
    //   1299: aload_1
    //   1300: putfield 65	com/truecaller/api/services/presence/v1/h:f	Lcom/truecaller/api/services/presence/v1/models/i;
    //   1303: aload_0
    //   1304: getfield 93	com/truecaller/api/services/presence/v1/h:g	Lcom/truecaller/api/services/presence/v1/models/Premium;
    //   1307: astore_1
    //   1308: aload_3
    //   1309: getfield 93	com/truecaller/api/services/presence/v1/h:g	Lcom/truecaller/api/services/presence/v1/models/Premium;
    //   1312: astore 4
    //   1314: aload_2
    //   1315: aload_1
    //   1316: aload 4
    //   1318: invokeinterface 234 3 0
    //   1323: checkcast 95	com/truecaller/api/services/presence/v1/models/Premium
    //   1326: astore_1
    //   1327: aload_0
    //   1328: aload_1
    //   1329: putfield 93	com/truecaller/api/services/presence/v1/h:g	Lcom/truecaller/api/services/presence/v1/models/Premium;
    //   1332: aload_0
    //   1333: getfield 63	com/truecaller/api/services/presence/v1/h:h	Lcom/truecaller/api/services/presence/v1/models/f;
    //   1336: astore_1
    //   1337: aload_3
    //   1338: getfield 63	com/truecaller/api/services/presence/v1/h:h	Lcom/truecaller/api/services/presence/v1/models/f;
    //   1341: astore_3
    //   1342: aload_2
    //   1343: aload_1
    //   1344: aload_3
    //   1345: invokeinterface 234 3 0
    //   1350: checkcast 100	com/truecaller/api/services/presence/v1/models/f
    //   1353: astore_1
    //   1354: aload_0
    //   1355: aload_1
    //   1356: putfield 63	com/truecaller/api/services/presence/v1/h:h	Lcom/truecaller/api/services/presence/v1/models/f;
    //   1359: aload_0
    //   1360: areturn
    //   1361: new 42	com/truecaller/api/services/presence/v1/h$a
    //   1364: astore_1
    //   1365: aload_1
    //   1366: iconst_0
    //   1367: invokespecial 237	com/truecaller/api/services/presence/v1/h$a:<init>	(B)V
    //   1370: aload_1
    //   1371: areturn
    //   1372: aconst_null
    //   1373: areturn
    //   1374: getstatic 32	com/truecaller/api/services/presence/v1/h:i	Lcom/truecaller/api/services/presence/v1/h;
    //   1377: areturn
    //   1378: new 2	com/truecaller/api/services/presence/v1/h
    //   1381: astore_1
    //   1382: aload_1
    //   1383: invokespecial 30	com/truecaller/api/services/presence/v1/h:<init>	()V
    //   1386: aload_1
    //   1387: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1388	0	this	h
    //   0	1388	1	paramj	com.google.f.q.j
    //   0	1388	2	paramObject1	Object
    //   0	1388	3	paramObject2	Object
    //   3	1314	4	localObject1	Object
    //   9	137	5	k	int
    //   1128	26	5	bool1	boolean
    //   19	1125	6	bool2	boolean
    //   156	97	7	m	int
    //   257	718	7	bool3	boolean
    //   165	84	8	n	int
    //   274	767	9	localObject2	Object
    //   313	707	10	localObject3	Object
    // Exception table:
    //   from	to	target	type
    //   95	98	125	finally
    //   103	106	125	finally
    //   107	110	125	finally
    //   112	116	125	finally
    //   116	120	125	finally
    //   120	122	125	finally
    //   126	128	125	finally
    //   152	156	1054	finally
    //   252	257	1054	finally
    //   270	274	1054	finally
    //   281	285	1054	finally
    //   287	292	1054	finally
    //   294	299	1054	finally
    //   310	313	1054	finally
    //   318	322	1054	finally
    //   324	329	1054	finally
    //   332	337	1054	finally
    //   342	346	1054	finally
    //   350	356	1054	finally
    //   356	361	1054	finally
    //   363	368	1054	finally
    //   371	376	1054	finally
    //   379	383	1054	finally
    //   390	394	1054	finally
    //   396	401	1054	finally
    //   403	408	1054	finally
    //   419	422	1054	finally
    //   427	431	1054	finally
    //   433	438	1054	finally
    //   441	446	1054	finally
    //   451	455	1054	finally
    //   459	465	1054	finally
    //   465	470	1054	finally
    //   472	477	1054	finally
    //   480	485	1054	finally
    //   488	492	1054	finally
    //   499	503	1054	finally
    //   505	510	1054	finally
    //   512	517	1054	finally
    //   528	531	1054	finally
    //   536	540	1054	finally
    //   542	547	1054	finally
    //   550	555	1054	finally
    //   560	564	1054	finally
    //   568	574	1054	finally
    //   574	579	1054	finally
    //   581	586	1054	finally
    //   589	594	1054	finally
    //   597	601	1054	finally
    //   604	609	1054	finally
    //   612	616	1054	finally
    //   623	627	1054	finally
    //   629	634	1054	finally
    //   636	641	1054	finally
    //   652	655	1054	finally
    //   660	664	1054	finally
    //   666	671	1054	finally
    //   674	679	1054	finally
    //   684	688	1054	finally
    //   692	698	1054	finally
    //   698	703	1054	finally
    //   705	710	1054	finally
    //   713	718	1054	finally
    //   721	725	1054	finally
    //   732	736	1054	finally
    //   738	743	1054	finally
    //   745	750	1054	finally
    //   761	764	1054	finally
    //   769	773	1054	finally
    //   775	780	1054	finally
    //   783	788	1054	finally
    //   793	797	1054	finally
    //   801	807	1054	finally
    //   807	812	1054	finally
    //   814	819	1054	finally
    //   822	827	1054	finally
    //   830	834	1054	finally
    //   841	845	1054	finally
    //   847	852	1054	finally
    //   854	859	1054	finally
    //   870	873	1054	finally
    //   878	882	1054	finally
    //   884	889	1054	finally
    //   892	897	1054	finally
    //   902	906	1054	finally
    //   910	916	1054	finally
    //   916	921	1054	finally
    //   923	928	1054	finally
    //   931	936	1054	finally
    //   939	943	1054	finally
    //   950	954	1054	finally
    //   956	961	1054	finally
    //   963	968	1054	finally
    //   979	982	1054	finally
    //   987	991	1054	finally
    //   993	998	1054	finally
    //   1001	1006	1054	finally
    //   1011	1015	1054	finally
    //   1019	1025	1054	finally
    //   1025	1030	1054	finally
    //   1032	1037	1054	finally
    //   1040	1045	1054	finally
    //   1059	1062	1054	finally
    //   1063	1066	1054	finally
    //   1067	1071	1054	finally
    //   1073	1077	1054	finally
    //   1078	1082	1054	finally
    //   1084	1088	1054	finally
    //   1088	1090	1054	finally
    //   1091	1094	1054	finally
    //   1096	1100	1054	finally
    //   1102	1106	1054	finally
    //   1106	1108	1054	finally
    //   152	156	1058	java/io/IOException
    //   252	257	1058	java/io/IOException
    //   270	274	1058	java/io/IOException
    //   281	285	1058	java/io/IOException
    //   287	292	1058	java/io/IOException
    //   294	299	1058	java/io/IOException
    //   310	313	1058	java/io/IOException
    //   318	322	1058	java/io/IOException
    //   324	329	1058	java/io/IOException
    //   332	337	1058	java/io/IOException
    //   342	346	1058	java/io/IOException
    //   350	356	1058	java/io/IOException
    //   356	361	1058	java/io/IOException
    //   363	368	1058	java/io/IOException
    //   371	376	1058	java/io/IOException
    //   379	383	1058	java/io/IOException
    //   390	394	1058	java/io/IOException
    //   396	401	1058	java/io/IOException
    //   403	408	1058	java/io/IOException
    //   419	422	1058	java/io/IOException
    //   427	431	1058	java/io/IOException
    //   433	438	1058	java/io/IOException
    //   441	446	1058	java/io/IOException
    //   451	455	1058	java/io/IOException
    //   459	465	1058	java/io/IOException
    //   465	470	1058	java/io/IOException
    //   472	477	1058	java/io/IOException
    //   480	485	1058	java/io/IOException
    //   488	492	1058	java/io/IOException
    //   499	503	1058	java/io/IOException
    //   505	510	1058	java/io/IOException
    //   512	517	1058	java/io/IOException
    //   528	531	1058	java/io/IOException
    //   536	540	1058	java/io/IOException
    //   542	547	1058	java/io/IOException
    //   550	555	1058	java/io/IOException
    //   560	564	1058	java/io/IOException
    //   568	574	1058	java/io/IOException
    //   574	579	1058	java/io/IOException
    //   581	586	1058	java/io/IOException
    //   589	594	1058	java/io/IOException
    //   597	601	1058	java/io/IOException
    //   604	609	1058	java/io/IOException
    //   612	616	1058	java/io/IOException
    //   623	627	1058	java/io/IOException
    //   629	634	1058	java/io/IOException
    //   636	641	1058	java/io/IOException
    //   652	655	1058	java/io/IOException
    //   660	664	1058	java/io/IOException
    //   666	671	1058	java/io/IOException
    //   674	679	1058	java/io/IOException
    //   684	688	1058	java/io/IOException
    //   692	698	1058	java/io/IOException
    //   698	703	1058	java/io/IOException
    //   705	710	1058	java/io/IOException
    //   713	718	1058	java/io/IOException
    //   721	725	1058	java/io/IOException
    //   732	736	1058	java/io/IOException
    //   738	743	1058	java/io/IOException
    //   745	750	1058	java/io/IOException
    //   761	764	1058	java/io/IOException
    //   769	773	1058	java/io/IOException
    //   775	780	1058	java/io/IOException
    //   783	788	1058	java/io/IOException
    //   793	797	1058	java/io/IOException
    //   801	807	1058	java/io/IOException
    //   807	812	1058	java/io/IOException
    //   814	819	1058	java/io/IOException
    //   822	827	1058	java/io/IOException
    //   830	834	1058	java/io/IOException
    //   841	845	1058	java/io/IOException
    //   847	852	1058	java/io/IOException
    //   854	859	1058	java/io/IOException
    //   870	873	1058	java/io/IOException
    //   878	882	1058	java/io/IOException
    //   884	889	1058	java/io/IOException
    //   892	897	1058	java/io/IOException
    //   902	906	1058	java/io/IOException
    //   910	916	1058	java/io/IOException
    //   916	921	1058	java/io/IOException
    //   923	928	1058	java/io/IOException
    //   931	936	1058	java/io/IOException
    //   939	943	1058	java/io/IOException
    //   950	954	1058	java/io/IOException
    //   956	961	1058	java/io/IOException
    //   963	968	1058	java/io/IOException
    //   979	982	1058	java/io/IOException
    //   987	991	1058	java/io/IOException
    //   993	998	1058	java/io/IOException
    //   1001	1006	1058	java/io/IOException
    //   1011	1015	1058	java/io/IOException
    //   1019	1025	1058	java/io/IOException
    //   1025	1030	1058	java/io/IOException
    //   1032	1037	1058	java/io/IOException
    //   1040	1045	1058	java/io/IOException
    //   152	156	1090	com/google/f/x
    //   252	257	1090	com/google/f/x
    //   270	274	1090	com/google/f/x
    //   281	285	1090	com/google/f/x
    //   287	292	1090	com/google/f/x
    //   294	299	1090	com/google/f/x
    //   310	313	1090	com/google/f/x
    //   318	322	1090	com/google/f/x
    //   324	329	1090	com/google/f/x
    //   332	337	1090	com/google/f/x
    //   342	346	1090	com/google/f/x
    //   350	356	1090	com/google/f/x
    //   356	361	1090	com/google/f/x
    //   363	368	1090	com/google/f/x
    //   371	376	1090	com/google/f/x
    //   379	383	1090	com/google/f/x
    //   390	394	1090	com/google/f/x
    //   396	401	1090	com/google/f/x
    //   403	408	1090	com/google/f/x
    //   419	422	1090	com/google/f/x
    //   427	431	1090	com/google/f/x
    //   433	438	1090	com/google/f/x
    //   441	446	1090	com/google/f/x
    //   451	455	1090	com/google/f/x
    //   459	465	1090	com/google/f/x
    //   465	470	1090	com/google/f/x
    //   472	477	1090	com/google/f/x
    //   480	485	1090	com/google/f/x
    //   488	492	1090	com/google/f/x
    //   499	503	1090	com/google/f/x
    //   505	510	1090	com/google/f/x
    //   512	517	1090	com/google/f/x
    //   528	531	1090	com/google/f/x
    //   536	540	1090	com/google/f/x
    //   542	547	1090	com/google/f/x
    //   550	555	1090	com/google/f/x
    //   560	564	1090	com/google/f/x
    //   568	574	1090	com/google/f/x
    //   574	579	1090	com/google/f/x
    //   581	586	1090	com/google/f/x
    //   589	594	1090	com/google/f/x
    //   597	601	1090	com/google/f/x
    //   604	609	1090	com/google/f/x
    //   612	616	1090	com/google/f/x
    //   623	627	1090	com/google/f/x
    //   629	634	1090	com/google/f/x
    //   636	641	1090	com/google/f/x
    //   652	655	1090	com/google/f/x
    //   660	664	1090	com/google/f/x
    //   666	671	1090	com/google/f/x
    //   674	679	1090	com/google/f/x
    //   684	688	1090	com/google/f/x
    //   692	698	1090	com/google/f/x
    //   698	703	1090	com/google/f/x
    //   705	710	1090	com/google/f/x
    //   713	718	1090	com/google/f/x
    //   721	725	1090	com/google/f/x
    //   732	736	1090	com/google/f/x
    //   738	743	1090	com/google/f/x
    //   745	750	1090	com/google/f/x
    //   761	764	1090	com/google/f/x
    //   769	773	1090	com/google/f/x
    //   775	780	1090	com/google/f/x
    //   783	788	1090	com/google/f/x
    //   793	797	1090	com/google/f/x
    //   801	807	1090	com/google/f/x
    //   807	812	1090	com/google/f/x
    //   814	819	1090	com/google/f/x
    //   822	827	1090	com/google/f/x
    //   830	834	1090	com/google/f/x
    //   841	845	1090	com/google/f/x
    //   847	852	1090	com/google/f/x
    //   854	859	1090	com/google/f/x
    //   870	873	1090	com/google/f/x
    //   878	882	1090	com/google/f/x
    //   884	889	1090	com/google/f/x
    //   892	897	1090	com/google/f/x
    //   902	906	1090	com/google/f/x
    //   910	916	1090	com/google/f/x
    //   916	921	1090	com/google/f/x
    //   923	928	1090	com/google/f/x
    //   931	936	1090	com/google/f/x
    //   939	943	1090	com/google/f/x
    //   950	954	1090	com/google/f/x
    //   956	961	1090	com/google/f/x
    //   963	968	1090	com/google/f/x
    //   979	982	1090	com/google/f/x
    //   987	991	1090	com/google/f/x
    //   993	998	1090	com/google/f/x
    //   1001	1006	1090	com/google/f/x
    //   1011	1015	1090	com/google/f/x
    //   1019	1025	1090	com/google/f/x
    //   1025	1030	1090	com/google/f/x
    //   1032	1037	1090	com/google/f/x
    //   1040	1045	1090	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int k = memoizedSerializedSize;
    int n = -1;
    if (k != n) {
      return k;
    }
    Object localObject1 = c;
    n = 0;
    Object localObject2;
    if (localObject1 != null)
    {
      localObject2 = e();
      k = com.google.f.h.computeMessageSize(1, (ae)localObject2);
      n = 0 + k;
    }
    localObject1 = d;
    if (localObject1 != null)
    {
      localObject2 = f();
      k = com.google.f.h.computeMessageSize(2, (ae)localObject2);
      n += k;
    }
    localObject1 = b;
    if (localObject1 != null)
    {
      localObject2 = d();
      k = com.google.f.h.computeMessageSize(3, (ae)localObject2);
      n += k;
    }
    localObject1 = e;
    if (localObject1 != null)
    {
      localObject2 = g();
      k = com.google.f.h.computeMessageSize(4, (ae)localObject2);
      n += k;
    }
    boolean bool = a;
    int m;
    if (bool)
    {
      int i1 = 5;
      m = com.google.f.h.computeBoolSize(i1, bool);
      n += m;
    }
    localObject1 = f;
    if (localObject1 != null)
    {
      localObject2 = h();
      m = com.google.f.h.computeMessageSize(6, (ae)localObject2);
      n += m;
    }
    localObject1 = g;
    if (localObject1 != null)
    {
      localObject2 = i();
      m = com.google.f.h.computeMessageSize(7, (ae)localObject2);
      n += m;
    }
    localObject1 = h;
    if (localObject1 != null)
    {
      localObject2 = j();
      m = com.google.f.h.computeMessageSize(8, (ae)localObject2);
      n += m;
    }
    memoizedSerializedSize = n;
    return n;
  }
  
  public final void writeTo(com.google.f.h paramh)
  {
    Object localObject1 = c;
    int k;
    Object localObject2;
    if (localObject1 != null)
    {
      k = 1;
      localObject2 = e();
      paramh.writeMessage(k, (ae)localObject2);
    }
    localObject1 = d;
    if (localObject1 != null)
    {
      k = 2;
      localObject2 = f();
      paramh.writeMessage(k, (ae)localObject2);
    }
    localObject1 = b;
    if (localObject1 != null)
    {
      k = 3;
      localObject2 = d();
      paramh.writeMessage(k, (ae)localObject2);
    }
    localObject1 = e;
    if (localObject1 != null)
    {
      k = 4;
      localObject2 = g();
      paramh.writeMessage(k, (ae)localObject2);
    }
    boolean bool = a;
    if (bool)
    {
      int n = 5;
      paramh.writeBool(n, bool);
    }
    localObject1 = f;
    int m;
    if (localObject1 != null)
    {
      m = 6;
      localObject2 = h();
      paramh.writeMessage(m, (ae)localObject2);
    }
    localObject1 = g;
    if (localObject1 != null)
    {
      m = 7;
      localObject2 = i();
      paramh.writeMessage(m, (ae)localObject2);
    }
    localObject1 = h;
    if (localObject1 != null)
    {
      m = 8;
      localObject2 = j();
      paramh.writeMessage(m, (ae)localObject2);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.presence.v1.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */