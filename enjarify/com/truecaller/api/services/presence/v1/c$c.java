package com.truecaller.api.services.presence.v1;

import com.google.f.ae;
import com.google.f.ah;
import com.google.f.am;
import com.google.f.h;
import com.google.f.q;
import com.truecaller.api.services.presence.v1.models.Availability;
import com.truecaller.api.services.presence.v1.models.Premium;
import com.truecaller.api.services.presence.v1.models.b;
import com.truecaller.api.services.presence.v1.models.d;
import com.truecaller.api.services.presence.v1.models.f;
import com.truecaller.api.services.presence.v1.models.i;

public final class c$c
  extends q
  implements c.d
{
  private static final c h;
  private static volatile ah i;
  private Availability a;
  private am b;
  private b c;
  private d d;
  private i e;
  private Premium f;
  private f g;
  
  static
  {
    c localc = new com/truecaller/api/services/presence/v1/c$c;
    localc.<init>();
    h = localc;
    localc.makeImmutable();
  }
  
  public static c l()
  {
    return h;
  }
  
  private Premium n()
  {
    Premium localPremium = f;
    if (localPremium == null) {
      localPremium = Premium.a();
    }
    return localPremium;
  }
  
  public final boolean a()
  {
    Availability localAvailability = a;
    return localAvailability != null;
  }
  
  public final Availability b()
  {
    Availability localAvailability = a;
    if (localAvailability == null) {
      localAvailability = Availability.d();
    }
    return localAvailability;
  }
  
  public final boolean c()
  {
    am localam = b;
    return localam != null;
  }
  
  public final am d()
  {
    am localam = b;
    if (localam == null) {
      localam = am.getDefaultInstance();
    }
    return localam;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 62	com/truecaller/api/services/presence/v1/c$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 68	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_0
    //   19: istore 6
    //   21: aconst_null
    //   22: astore 4
    //   24: iload 5
    //   26: tableswitch	default:+46->72, 1:+1292->1318, 2:+1288->1314, 3:+1286->1312, 4:+1275->1301, 5:+1062->1088, 6:+108->134, 7:+1058->1084, 8:+56->82
    //   72: new 70	java/lang/UnsupportedOperationException
    //   75: astore_1
    //   76: aload_1
    //   77: invokespecial 71	java/lang/UnsupportedOperationException:<init>	()V
    //   80: aload_1
    //   81: athrow
    //   82: getstatic 73	com/truecaller/api/services/presence/v1/c$c:i	Lcom/google/f/ah;
    //   85: astore_1
    //   86: aload_1
    //   87: ifnonnull +43 -> 130
    //   90: ldc 2
    //   92: astore_1
    //   93: aload_1
    //   94: monitorenter
    //   95: getstatic 73	com/truecaller/api/services/presence/v1/c$c:i	Lcom/google/f/ah;
    //   98: astore_2
    //   99: aload_2
    //   100: ifnonnull +20 -> 120
    //   103: new 75	com/google/f/q$b
    //   106: astore_2
    //   107: getstatic 30	com/truecaller/api/services/presence/v1/c$c:h	Lcom/truecaller/api/services/presence/v1/c$c;
    //   110: astore_3
    //   111: aload_2
    //   112: aload_3
    //   113: invokespecial 78	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   116: aload_2
    //   117: putstatic 73	com/truecaller/api/services/presence/v1/c$c:i	Lcom/google/f/ah;
    //   120: aload_1
    //   121: monitorexit
    //   122: goto +8 -> 130
    //   125: astore_2
    //   126: aload_1
    //   127: monitorexit
    //   128: aload_2
    //   129: athrow
    //   130: getstatic 73	com/truecaller/api/services/presence/v1/c$c:i	Lcom/google/f/ah;
    //   133: areturn
    //   134: aload_2
    //   135: checkcast 80	com/google/f/g
    //   138: astore_2
    //   139: aload_3
    //   140: checkcast 82	com/google/f/n
    //   143: astore_3
    //   144: iconst_1
    //   145: istore 5
    //   147: iload 6
    //   149: ifne +935 -> 1084
    //   152: aload_2
    //   153: invokevirtual 85	com/google/f/g:readTag	()I
    //   156: istore 7
    //   158: iload 7
    //   160: ifeq +862 -> 1022
    //   163: bipush 10
    //   165: istore 8
    //   167: iload 7
    //   169: iload 8
    //   171: if_icmpeq +742 -> 913
    //   174: bipush 18
    //   176: istore 8
    //   178: iload 7
    //   180: iload 8
    //   182: if_icmpeq +622 -> 804
    //   185: bipush 26
    //   187: istore 8
    //   189: iload 7
    //   191: iload 8
    //   193: if_icmpeq +502 -> 695
    //   196: bipush 34
    //   198: istore 8
    //   200: iload 7
    //   202: iload 8
    //   204: if_icmpeq +382 -> 586
    //   207: bipush 42
    //   209: istore 8
    //   211: iload 7
    //   213: iload 8
    //   215: if_icmpeq +262 -> 477
    //   218: bipush 50
    //   220: istore 8
    //   222: iload 7
    //   224: iload 8
    //   226: if_icmpeq +142 -> 368
    //   229: bipush 58
    //   231: istore 8
    //   233: iload 7
    //   235: iload 8
    //   237: if_icmpeq +22 -> 259
    //   240: aload_2
    //   241: iload 7
    //   243: invokevirtual 96	com/google/f/g:skipField	(I)Z
    //   246: istore 7
    //   248: iload 7
    //   250: ifne -103 -> 147
    //   253: iconst_1
    //   254: istore 6
    //   256: goto -109 -> 147
    //   259: aload_0
    //   260: getfield 98	com/truecaller/api/services/presence/v1/c$c:g	Lcom/truecaller/api/services/presence/v1/models/f;
    //   263: astore 9
    //   265: aload 9
    //   267: ifnull +26 -> 293
    //   270: aload_0
    //   271: getfield 98	com/truecaller/api/services/presence/v1/c$c:g	Lcom/truecaller/api/services/presence/v1/models/f;
    //   274: astore 9
    //   276: aload 9
    //   278: invokevirtual 104	com/truecaller/api/services/presence/v1/models/f:toBuilder	()Lcom/google/f/q$a;
    //   281: astore 9
    //   283: aload 9
    //   285: checkcast 106	com/truecaller/api/services/presence/v1/models/f$a
    //   288: astore 9
    //   290: goto +9 -> 299
    //   293: iconst_0
    //   294: istore 7
    //   296: aconst_null
    //   297: astore 9
    //   299: invokestatic 109	com/truecaller/api/services/presence/v1/models/f:f	()Lcom/google/f/ah;
    //   302: astore 10
    //   304: aload_2
    //   305: aload 10
    //   307: aload_3
    //   308: invokevirtual 113	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   311: astore 10
    //   313: aload 10
    //   315: checkcast 100	com/truecaller/api/services/presence/v1/models/f
    //   318: astore 10
    //   320: aload_0
    //   321: aload 10
    //   323: putfield 98	com/truecaller/api/services/presence/v1/c$c:g	Lcom/truecaller/api/services/presence/v1/models/f;
    //   326: aload 9
    //   328: ifnull -181 -> 147
    //   331: aload_0
    //   332: getfield 98	com/truecaller/api/services/presence/v1/c$c:g	Lcom/truecaller/api/services/presence/v1/models/f;
    //   335: astore 10
    //   337: aload 9
    //   339: aload 10
    //   341: invokevirtual 117	com/truecaller/api/services/presence/v1/models/f$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   344: pop
    //   345: aload 9
    //   347: invokevirtual 121	com/truecaller/api/services/presence/v1/models/f$a:buildPartial	()Lcom/google/f/q;
    //   350: astore 9
    //   352: aload 9
    //   354: checkcast 100	com/truecaller/api/services/presence/v1/models/f
    //   357: astore 9
    //   359: aload_0
    //   360: aload 9
    //   362: putfield 98	com/truecaller/api/services/presence/v1/c$c:g	Lcom/truecaller/api/services/presence/v1/models/f;
    //   365: goto -218 -> 147
    //   368: aload_0
    //   369: getfield 36	com/truecaller/api/services/presence/v1/c$c:f	Lcom/truecaller/api/services/presence/v1/models/Premium;
    //   372: astore 9
    //   374: aload 9
    //   376: ifnull +26 -> 402
    //   379: aload_0
    //   380: getfield 36	com/truecaller/api/services/presence/v1/c$c:f	Lcom/truecaller/api/services/presence/v1/models/Premium;
    //   383: astore 9
    //   385: aload 9
    //   387: invokevirtual 122	com/truecaller/api/services/presence/v1/models/Premium:toBuilder	()Lcom/google/f/q$a;
    //   390: astore 9
    //   392: aload 9
    //   394: checkcast 124	com/truecaller/api/services/presence/v1/models/Premium$a
    //   397: astore 9
    //   399: goto +9 -> 408
    //   402: iconst_0
    //   403: istore 7
    //   405: aconst_null
    //   406: astore 9
    //   408: invokestatic 126	com/truecaller/api/services/presence/v1/models/Premium:b	()Lcom/google/f/ah;
    //   411: astore 10
    //   413: aload_2
    //   414: aload 10
    //   416: aload_3
    //   417: invokevirtual 113	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   420: astore 10
    //   422: aload 10
    //   424: checkcast 38	com/truecaller/api/services/presence/v1/models/Premium
    //   427: astore 10
    //   429: aload_0
    //   430: aload 10
    //   432: putfield 36	com/truecaller/api/services/presence/v1/c$c:f	Lcom/truecaller/api/services/presence/v1/models/Premium;
    //   435: aload 9
    //   437: ifnull -290 -> 147
    //   440: aload_0
    //   441: getfield 36	com/truecaller/api/services/presence/v1/c$c:f	Lcom/truecaller/api/services/presence/v1/models/Premium;
    //   444: astore 10
    //   446: aload 9
    //   448: aload 10
    //   450: invokevirtual 127	com/truecaller/api/services/presence/v1/models/Premium$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   453: pop
    //   454: aload 9
    //   456: invokevirtual 128	com/truecaller/api/services/presence/v1/models/Premium$a:buildPartial	()Lcom/google/f/q;
    //   459: astore 9
    //   461: aload 9
    //   463: checkcast 38	com/truecaller/api/services/presence/v1/models/Premium
    //   466: astore 9
    //   468: aload_0
    //   469: aload 9
    //   471: putfield 36	com/truecaller/api/services/presence/v1/c$c:f	Lcom/truecaller/api/services/presence/v1/models/Premium;
    //   474: goto -327 -> 147
    //   477: aload_0
    //   478: getfield 130	com/truecaller/api/services/presence/v1/c$c:e	Lcom/truecaller/api/services/presence/v1/models/i;
    //   481: astore 9
    //   483: aload 9
    //   485: ifnull +26 -> 511
    //   488: aload_0
    //   489: getfield 130	com/truecaller/api/services/presence/v1/c$c:e	Lcom/truecaller/api/services/presence/v1/models/i;
    //   492: astore 9
    //   494: aload 9
    //   496: invokevirtual 133	com/truecaller/api/services/presence/v1/models/i:toBuilder	()Lcom/google/f/q$a;
    //   499: astore 9
    //   501: aload 9
    //   503: checkcast 135	com/truecaller/api/services/presence/v1/models/i$a
    //   506: astore 9
    //   508: goto +9 -> 517
    //   511: iconst_0
    //   512: istore 7
    //   514: aconst_null
    //   515: astore 9
    //   517: invokestatic 137	com/truecaller/api/services/presence/v1/models/i:e	()Lcom/google/f/ah;
    //   520: astore 10
    //   522: aload_2
    //   523: aload 10
    //   525: aload_3
    //   526: invokevirtual 113	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   529: astore 10
    //   531: aload 10
    //   533: checkcast 132	com/truecaller/api/services/presence/v1/models/i
    //   536: astore 10
    //   538: aload_0
    //   539: aload 10
    //   541: putfield 130	com/truecaller/api/services/presence/v1/c$c:e	Lcom/truecaller/api/services/presence/v1/models/i;
    //   544: aload 9
    //   546: ifnull -399 -> 147
    //   549: aload_0
    //   550: getfield 130	com/truecaller/api/services/presence/v1/c$c:e	Lcom/truecaller/api/services/presence/v1/models/i;
    //   553: astore 10
    //   555: aload 9
    //   557: aload 10
    //   559: invokevirtual 138	com/truecaller/api/services/presence/v1/models/i$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   562: pop
    //   563: aload 9
    //   565: invokevirtual 139	com/truecaller/api/services/presence/v1/models/i$a:buildPartial	()Lcom/google/f/q;
    //   568: astore 9
    //   570: aload 9
    //   572: checkcast 132	com/truecaller/api/services/presence/v1/models/i
    //   575: astore 9
    //   577: aload_0
    //   578: aload 9
    //   580: putfield 130	com/truecaller/api/services/presence/v1/c$c:e	Lcom/truecaller/api/services/presence/v1/models/i;
    //   583: goto -436 -> 147
    //   586: aload_0
    //   587: getfield 141	com/truecaller/api/services/presence/v1/c$c:d	Lcom/truecaller/api/services/presence/v1/models/d;
    //   590: astore 9
    //   592: aload 9
    //   594: ifnull +26 -> 620
    //   597: aload_0
    //   598: getfield 141	com/truecaller/api/services/presence/v1/c$c:d	Lcom/truecaller/api/services/presence/v1/models/d;
    //   601: astore 9
    //   603: aload 9
    //   605: invokevirtual 144	com/truecaller/api/services/presence/v1/models/d:toBuilder	()Lcom/google/f/q$a;
    //   608: astore 9
    //   610: aload 9
    //   612: checkcast 146	com/truecaller/api/services/presence/v1/models/d$a
    //   615: astore 9
    //   617: goto +9 -> 626
    //   620: iconst_0
    //   621: istore 7
    //   623: aconst_null
    //   624: astore 9
    //   626: invokestatic 148	com/truecaller/api/services/presence/v1/models/d:c	()Lcom/google/f/ah;
    //   629: astore 10
    //   631: aload_2
    //   632: aload 10
    //   634: aload_3
    //   635: invokevirtual 113	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   638: astore 10
    //   640: aload 10
    //   642: checkcast 143	com/truecaller/api/services/presence/v1/models/d
    //   645: astore 10
    //   647: aload_0
    //   648: aload 10
    //   650: putfield 141	com/truecaller/api/services/presence/v1/c$c:d	Lcom/truecaller/api/services/presence/v1/models/d;
    //   653: aload 9
    //   655: ifnull -508 -> 147
    //   658: aload_0
    //   659: getfield 141	com/truecaller/api/services/presence/v1/c$c:d	Lcom/truecaller/api/services/presence/v1/models/d;
    //   662: astore 10
    //   664: aload 9
    //   666: aload 10
    //   668: invokevirtual 149	com/truecaller/api/services/presence/v1/models/d$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   671: pop
    //   672: aload 9
    //   674: invokevirtual 150	com/truecaller/api/services/presence/v1/models/d$a:buildPartial	()Lcom/google/f/q;
    //   677: astore 9
    //   679: aload 9
    //   681: checkcast 143	com/truecaller/api/services/presence/v1/models/d
    //   684: astore 9
    //   686: aload_0
    //   687: aload 9
    //   689: putfield 141	com/truecaller/api/services/presence/v1/c$c:d	Lcom/truecaller/api/services/presence/v1/models/d;
    //   692: goto -545 -> 147
    //   695: aload_0
    //   696: getfield 152	com/truecaller/api/services/presence/v1/c$c:c	Lcom/truecaller/api/services/presence/v1/models/b;
    //   699: astore 9
    //   701: aload 9
    //   703: ifnull +26 -> 729
    //   706: aload_0
    //   707: getfield 152	com/truecaller/api/services/presence/v1/c$c:c	Lcom/truecaller/api/services/presence/v1/models/b;
    //   710: astore 9
    //   712: aload 9
    //   714: invokevirtual 155	com/truecaller/api/services/presence/v1/models/b:toBuilder	()Lcom/google/f/q$a;
    //   717: astore 9
    //   719: aload 9
    //   721: checkcast 157	com/truecaller/api/services/presence/v1/models/b$a
    //   724: astore 9
    //   726: goto +9 -> 735
    //   729: iconst_0
    //   730: istore 7
    //   732: aconst_null
    //   733: astore 9
    //   735: invokestatic 158	com/truecaller/api/services/presence/v1/models/b:e	()Lcom/google/f/ah;
    //   738: astore 10
    //   740: aload_2
    //   741: aload 10
    //   743: aload_3
    //   744: invokevirtual 113	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   747: astore 10
    //   749: aload 10
    //   751: checkcast 154	com/truecaller/api/services/presence/v1/models/b
    //   754: astore 10
    //   756: aload_0
    //   757: aload 10
    //   759: putfield 152	com/truecaller/api/services/presence/v1/c$c:c	Lcom/truecaller/api/services/presence/v1/models/b;
    //   762: aload 9
    //   764: ifnull -617 -> 147
    //   767: aload_0
    //   768: getfield 152	com/truecaller/api/services/presence/v1/c$c:c	Lcom/truecaller/api/services/presence/v1/models/b;
    //   771: astore 10
    //   773: aload 9
    //   775: aload 10
    //   777: invokevirtual 159	com/truecaller/api/services/presence/v1/models/b$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   780: pop
    //   781: aload 9
    //   783: invokevirtual 160	com/truecaller/api/services/presence/v1/models/b$a:buildPartial	()Lcom/google/f/q;
    //   786: astore 9
    //   788: aload 9
    //   790: checkcast 154	com/truecaller/api/services/presence/v1/models/b
    //   793: astore 9
    //   795: aload_0
    //   796: aload 9
    //   798: putfield 152	com/truecaller/api/services/presence/v1/c$c:c	Lcom/truecaller/api/services/presence/v1/models/b;
    //   801: goto -654 -> 147
    //   804: aload_0
    //   805: getfield 51	com/truecaller/api/services/presence/v1/c$c:b	Lcom/google/f/am;
    //   808: astore 9
    //   810: aload 9
    //   812: ifnull +26 -> 838
    //   815: aload_0
    //   816: getfield 51	com/truecaller/api/services/presence/v1/c$c:b	Lcom/google/f/am;
    //   819: astore 9
    //   821: aload 9
    //   823: invokevirtual 161	com/google/f/am:toBuilder	()Lcom/google/f/q$a;
    //   826: astore 9
    //   828: aload 9
    //   830: checkcast 163	com/google/f/am$a
    //   833: astore 9
    //   835: goto +9 -> 844
    //   838: iconst_0
    //   839: istore 7
    //   841: aconst_null
    //   842: astore 9
    //   844: invokestatic 166	com/google/f/am:parser	()Lcom/google/f/ah;
    //   847: astore 10
    //   849: aload_2
    //   850: aload 10
    //   852: aload_3
    //   853: invokevirtual 113	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   856: astore 10
    //   858: aload 10
    //   860: checkcast 53	com/google/f/am
    //   863: astore 10
    //   865: aload_0
    //   866: aload 10
    //   868: putfield 51	com/truecaller/api/services/presence/v1/c$c:b	Lcom/google/f/am;
    //   871: aload 9
    //   873: ifnull -726 -> 147
    //   876: aload_0
    //   877: getfield 51	com/truecaller/api/services/presence/v1/c$c:b	Lcom/google/f/am;
    //   880: astore 10
    //   882: aload 9
    //   884: aload 10
    //   886: invokevirtual 167	com/google/f/am$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   889: pop
    //   890: aload 9
    //   892: invokevirtual 168	com/google/f/am$a:buildPartial	()Lcom/google/f/q;
    //   895: astore 9
    //   897: aload 9
    //   899: checkcast 53	com/google/f/am
    //   902: astore 9
    //   904: aload_0
    //   905: aload 9
    //   907: putfield 51	com/truecaller/api/services/presence/v1/c$c:b	Lcom/google/f/am;
    //   910: goto -763 -> 147
    //   913: aload_0
    //   914: getfield 43	com/truecaller/api/services/presence/v1/c$c:a	Lcom/truecaller/api/services/presence/v1/models/Availability;
    //   917: astore 9
    //   919: aload 9
    //   921: ifnull +26 -> 947
    //   924: aload_0
    //   925: getfield 43	com/truecaller/api/services/presence/v1/c$c:a	Lcom/truecaller/api/services/presence/v1/models/Availability;
    //   928: astore 9
    //   930: aload 9
    //   932: invokevirtual 169	com/truecaller/api/services/presence/v1/models/Availability:toBuilder	()Lcom/google/f/q$a;
    //   935: astore 9
    //   937: aload 9
    //   939: checkcast 171	com/truecaller/api/services/presence/v1/models/Availability$a
    //   942: astore 9
    //   944: goto +9 -> 953
    //   947: iconst_0
    //   948: istore 7
    //   950: aconst_null
    //   951: astore 9
    //   953: invokestatic 172	com/truecaller/api/services/presence/v1/models/Availability:e	()Lcom/google/f/ah;
    //   956: astore 10
    //   958: aload_2
    //   959: aload 10
    //   961: aload_3
    //   962: invokevirtual 113	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   965: astore 10
    //   967: aload 10
    //   969: checkcast 46	com/truecaller/api/services/presence/v1/models/Availability
    //   972: astore 10
    //   974: aload_0
    //   975: aload 10
    //   977: putfield 43	com/truecaller/api/services/presence/v1/c$c:a	Lcom/truecaller/api/services/presence/v1/models/Availability;
    //   980: aload 9
    //   982: ifnull -835 -> 147
    //   985: aload_0
    //   986: getfield 43	com/truecaller/api/services/presence/v1/c$c:a	Lcom/truecaller/api/services/presence/v1/models/Availability;
    //   989: astore 10
    //   991: aload 9
    //   993: aload 10
    //   995: invokevirtual 173	com/truecaller/api/services/presence/v1/models/Availability$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   998: pop
    //   999: aload 9
    //   1001: invokevirtual 174	com/truecaller/api/services/presence/v1/models/Availability$a:buildPartial	()Lcom/google/f/q;
    //   1004: astore 9
    //   1006: aload 9
    //   1008: checkcast 46	com/truecaller/api/services/presence/v1/models/Availability
    //   1011: astore 9
    //   1013: aload_0
    //   1014: aload 9
    //   1016: putfield 43	com/truecaller/api/services/presence/v1/c$c:a	Lcom/truecaller/api/services/presence/v1/models/Availability;
    //   1019: goto -872 -> 147
    //   1022: iconst_1
    //   1023: istore 6
    //   1025: goto -878 -> 147
    //   1028: astore_1
    //   1029: goto +53 -> 1082
    //   1032: astore_1
    //   1033: new 176	java/lang/RuntimeException
    //   1036: astore_2
    //   1037: new 178	com/google/f/x
    //   1040: astore_3
    //   1041: aload_1
    //   1042: invokevirtual 184	java/io/IOException:getMessage	()Ljava/lang/String;
    //   1045: astore_1
    //   1046: aload_3
    //   1047: aload_1
    //   1048: invokespecial 187	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   1051: aload_3
    //   1052: aload_0
    //   1053: invokevirtual 191	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   1056: astore_1
    //   1057: aload_2
    //   1058: aload_1
    //   1059: invokespecial 194	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   1062: aload_2
    //   1063: athrow
    //   1064: astore_1
    //   1065: new 176	java/lang/RuntimeException
    //   1068: astore_2
    //   1069: aload_1
    //   1070: aload_0
    //   1071: invokevirtual 191	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   1074: astore_1
    //   1075: aload_2
    //   1076: aload_1
    //   1077: invokespecial 194	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   1080: aload_2
    //   1081: athrow
    //   1082: aload_1
    //   1083: athrow
    //   1084: getstatic 30	com/truecaller/api/services/presence/v1/c$c:h	Lcom/truecaller/api/services/presence/v1/c$c;
    //   1087: areturn
    //   1088: aload_2
    //   1089: checkcast 196	com/google/f/q$k
    //   1092: astore_2
    //   1093: aload_3
    //   1094: checkcast 2	com/truecaller/api/services/presence/v1/c$c
    //   1097: astore_3
    //   1098: aload_0
    //   1099: getfield 43	com/truecaller/api/services/presence/v1/c$c:a	Lcom/truecaller/api/services/presence/v1/models/Availability;
    //   1102: astore_1
    //   1103: aload_3
    //   1104: getfield 43	com/truecaller/api/services/presence/v1/c$c:a	Lcom/truecaller/api/services/presence/v1/models/Availability;
    //   1107: astore 4
    //   1109: aload_2
    //   1110: aload_1
    //   1111: aload 4
    //   1113: invokeinterface 200 3 0
    //   1118: checkcast 46	com/truecaller/api/services/presence/v1/models/Availability
    //   1121: astore_1
    //   1122: aload_0
    //   1123: aload_1
    //   1124: putfield 43	com/truecaller/api/services/presence/v1/c$c:a	Lcom/truecaller/api/services/presence/v1/models/Availability;
    //   1127: aload_0
    //   1128: getfield 51	com/truecaller/api/services/presence/v1/c$c:b	Lcom/google/f/am;
    //   1131: astore_1
    //   1132: aload_3
    //   1133: getfield 51	com/truecaller/api/services/presence/v1/c$c:b	Lcom/google/f/am;
    //   1136: astore 4
    //   1138: aload_2
    //   1139: aload_1
    //   1140: aload 4
    //   1142: invokeinterface 200 3 0
    //   1147: checkcast 53	com/google/f/am
    //   1150: astore_1
    //   1151: aload_0
    //   1152: aload_1
    //   1153: putfield 51	com/truecaller/api/services/presence/v1/c$c:b	Lcom/google/f/am;
    //   1156: aload_0
    //   1157: getfield 152	com/truecaller/api/services/presence/v1/c$c:c	Lcom/truecaller/api/services/presence/v1/models/b;
    //   1160: astore_1
    //   1161: aload_3
    //   1162: getfield 152	com/truecaller/api/services/presence/v1/c$c:c	Lcom/truecaller/api/services/presence/v1/models/b;
    //   1165: astore 4
    //   1167: aload_2
    //   1168: aload_1
    //   1169: aload 4
    //   1171: invokeinterface 200 3 0
    //   1176: checkcast 154	com/truecaller/api/services/presence/v1/models/b
    //   1179: astore_1
    //   1180: aload_0
    //   1181: aload_1
    //   1182: putfield 152	com/truecaller/api/services/presence/v1/c$c:c	Lcom/truecaller/api/services/presence/v1/models/b;
    //   1185: aload_0
    //   1186: getfield 141	com/truecaller/api/services/presence/v1/c$c:d	Lcom/truecaller/api/services/presence/v1/models/d;
    //   1189: astore_1
    //   1190: aload_3
    //   1191: getfield 141	com/truecaller/api/services/presence/v1/c$c:d	Lcom/truecaller/api/services/presence/v1/models/d;
    //   1194: astore 4
    //   1196: aload_2
    //   1197: aload_1
    //   1198: aload 4
    //   1200: invokeinterface 200 3 0
    //   1205: checkcast 143	com/truecaller/api/services/presence/v1/models/d
    //   1208: astore_1
    //   1209: aload_0
    //   1210: aload_1
    //   1211: putfield 141	com/truecaller/api/services/presence/v1/c$c:d	Lcom/truecaller/api/services/presence/v1/models/d;
    //   1214: aload_0
    //   1215: getfield 130	com/truecaller/api/services/presence/v1/c$c:e	Lcom/truecaller/api/services/presence/v1/models/i;
    //   1218: astore_1
    //   1219: aload_3
    //   1220: getfield 130	com/truecaller/api/services/presence/v1/c$c:e	Lcom/truecaller/api/services/presence/v1/models/i;
    //   1223: astore 4
    //   1225: aload_2
    //   1226: aload_1
    //   1227: aload 4
    //   1229: invokeinterface 200 3 0
    //   1234: checkcast 132	com/truecaller/api/services/presence/v1/models/i
    //   1237: astore_1
    //   1238: aload_0
    //   1239: aload_1
    //   1240: putfield 130	com/truecaller/api/services/presence/v1/c$c:e	Lcom/truecaller/api/services/presence/v1/models/i;
    //   1243: aload_0
    //   1244: getfield 36	com/truecaller/api/services/presence/v1/c$c:f	Lcom/truecaller/api/services/presence/v1/models/Premium;
    //   1247: astore_1
    //   1248: aload_3
    //   1249: getfield 36	com/truecaller/api/services/presence/v1/c$c:f	Lcom/truecaller/api/services/presence/v1/models/Premium;
    //   1252: astore 4
    //   1254: aload_2
    //   1255: aload_1
    //   1256: aload 4
    //   1258: invokeinterface 200 3 0
    //   1263: checkcast 38	com/truecaller/api/services/presence/v1/models/Premium
    //   1266: astore_1
    //   1267: aload_0
    //   1268: aload_1
    //   1269: putfield 36	com/truecaller/api/services/presence/v1/c$c:f	Lcom/truecaller/api/services/presence/v1/models/Premium;
    //   1272: aload_0
    //   1273: getfield 98	com/truecaller/api/services/presence/v1/c$c:g	Lcom/truecaller/api/services/presence/v1/models/f;
    //   1276: astore_1
    //   1277: aload_3
    //   1278: getfield 98	com/truecaller/api/services/presence/v1/c$c:g	Lcom/truecaller/api/services/presence/v1/models/f;
    //   1281: astore_3
    //   1282: aload_2
    //   1283: aload_1
    //   1284: aload_3
    //   1285: invokeinterface 200 3 0
    //   1290: checkcast 100	com/truecaller/api/services/presence/v1/models/f
    //   1293: astore_1
    //   1294: aload_0
    //   1295: aload_1
    //   1296: putfield 98	com/truecaller/api/services/presence/v1/c$c:g	Lcom/truecaller/api/services/presence/v1/models/f;
    //   1299: aload_0
    //   1300: areturn
    //   1301: new 202	com/truecaller/api/services/presence/v1/c$c$a
    //   1304: astore_1
    //   1305: aload_1
    //   1306: iconst_0
    //   1307: invokespecial 205	com/truecaller/api/services/presence/v1/c$c$a:<init>	(B)V
    //   1310: aload_1
    //   1311: areturn
    //   1312: aconst_null
    //   1313: areturn
    //   1314: getstatic 30	com/truecaller/api/services/presence/v1/c$c:h	Lcom/truecaller/api/services/presence/v1/c$c;
    //   1317: areturn
    //   1318: new 2	com/truecaller/api/services/presence/v1/c$c
    //   1321: astore_1
    //   1322: aload_1
    //   1323: invokespecial 28	com/truecaller/api/services/presence/v1/c$c:<init>	()V
    //   1326: aload_1
    //   1327: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1328	0	this	c
    //   0	1328	1	paramj	com.google.f.q.j
    //   0	1328	2	paramObject1	Object
    //   0	1328	3	paramObject2	Object
    //   3	1254	4	localObject1	Object
    //   9	137	5	j	int
    //   19	1005	6	k	int
    //   156	86	7	m	int
    //   246	703	7	bool	boolean
    //   165	73	8	n	int
    //   263	752	9	localObject2	Object
    //   302	692	10	localObject3	Object
    // Exception table:
    //   from	to	target	type
    //   95	98	125	finally
    //   103	106	125	finally
    //   107	110	125	finally
    //   112	116	125	finally
    //   116	120	125	finally
    //   120	122	125	finally
    //   126	128	125	finally
    //   152	156	1028	finally
    //   241	246	1028	finally
    //   259	263	1028	finally
    //   270	274	1028	finally
    //   276	281	1028	finally
    //   283	288	1028	finally
    //   299	302	1028	finally
    //   307	311	1028	finally
    //   313	318	1028	finally
    //   321	326	1028	finally
    //   331	335	1028	finally
    //   339	345	1028	finally
    //   345	350	1028	finally
    //   352	357	1028	finally
    //   360	365	1028	finally
    //   368	372	1028	finally
    //   379	383	1028	finally
    //   385	390	1028	finally
    //   392	397	1028	finally
    //   408	411	1028	finally
    //   416	420	1028	finally
    //   422	427	1028	finally
    //   430	435	1028	finally
    //   440	444	1028	finally
    //   448	454	1028	finally
    //   454	459	1028	finally
    //   461	466	1028	finally
    //   469	474	1028	finally
    //   477	481	1028	finally
    //   488	492	1028	finally
    //   494	499	1028	finally
    //   501	506	1028	finally
    //   517	520	1028	finally
    //   525	529	1028	finally
    //   531	536	1028	finally
    //   539	544	1028	finally
    //   549	553	1028	finally
    //   557	563	1028	finally
    //   563	568	1028	finally
    //   570	575	1028	finally
    //   578	583	1028	finally
    //   586	590	1028	finally
    //   597	601	1028	finally
    //   603	608	1028	finally
    //   610	615	1028	finally
    //   626	629	1028	finally
    //   634	638	1028	finally
    //   640	645	1028	finally
    //   648	653	1028	finally
    //   658	662	1028	finally
    //   666	672	1028	finally
    //   672	677	1028	finally
    //   679	684	1028	finally
    //   687	692	1028	finally
    //   695	699	1028	finally
    //   706	710	1028	finally
    //   712	717	1028	finally
    //   719	724	1028	finally
    //   735	738	1028	finally
    //   743	747	1028	finally
    //   749	754	1028	finally
    //   757	762	1028	finally
    //   767	771	1028	finally
    //   775	781	1028	finally
    //   781	786	1028	finally
    //   788	793	1028	finally
    //   796	801	1028	finally
    //   804	808	1028	finally
    //   815	819	1028	finally
    //   821	826	1028	finally
    //   828	833	1028	finally
    //   844	847	1028	finally
    //   852	856	1028	finally
    //   858	863	1028	finally
    //   866	871	1028	finally
    //   876	880	1028	finally
    //   884	890	1028	finally
    //   890	895	1028	finally
    //   897	902	1028	finally
    //   905	910	1028	finally
    //   913	917	1028	finally
    //   924	928	1028	finally
    //   930	935	1028	finally
    //   937	942	1028	finally
    //   953	956	1028	finally
    //   961	965	1028	finally
    //   967	972	1028	finally
    //   975	980	1028	finally
    //   985	989	1028	finally
    //   993	999	1028	finally
    //   999	1004	1028	finally
    //   1006	1011	1028	finally
    //   1014	1019	1028	finally
    //   1033	1036	1028	finally
    //   1037	1040	1028	finally
    //   1041	1045	1028	finally
    //   1047	1051	1028	finally
    //   1052	1056	1028	finally
    //   1058	1062	1028	finally
    //   1062	1064	1028	finally
    //   1065	1068	1028	finally
    //   1070	1074	1028	finally
    //   1076	1080	1028	finally
    //   1080	1082	1028	finally
    //   152	156	1032	java/io/IOException
    //   241	246	1032	java/io/IOException
    //   259	263	1032	java/io/IOException
    //   270	274	1032	java/io/IOException
    //   276	281	1032	java/io/IOException
    //   283	288	1032	java/io/IOException
    //   299	302	1032	java/io/IOException
    //   307	311	1032	java/io/IOException
    //   313	318	1032	java/io/IOException
    //   321	326	1032	java/io/IOException
    //   331	335	1032	java/io/IOException
    //   339	345	1032	java/io/IOException
    //   345	350	1032	java/io/IOException
    //   352	357	1032	java/io/IOException
    //   360	365	1032	java/io/IOException
    //   368	372	1032	java/io/IOException
    //   379	383	1032	java/io/IOException
    //   385	390	1032	java/io/IOException
    //   392	397	1032	java/io/IOException
    //   408	411	1032	java/io/IOException
    //   416	420	1032	java/io/IOException
    //   422	427	1032	java/io/IOException
    //   430	435	1032	java/io/IOException
    //   440	444	1032	java/io/IOException
    //   448	454	1032	java/io/IOException
    //   454	459	1032	java/io/IOException
    //   461	466	1032	java/io/IOException
    //   469	474	1032	java/io/IOException
    //   477	481	1032	java/io/IOException
    //   488	492	1032	java/io/IOException
    //   494	499	1032	java/io/IOException
    //   501	506	1032	java/io/IOException
    //   517	520	1032	java/io/IOException
    //   525	529	1032	java/io/IOException
    //   531	536	1032	java/io/IOException
    //   539	544	1032	java/io/IOException
    //   549	553	1032	java/io/IOException
    //   557	563	1032	java/io/IOException
    //   563	568	1032	java/io/IOException
    //   570	575	1032	java/io/IOException
    //   578	583	1032	java/io/IOException
    //   586	590	1032	java/io/IOException
    //   597	601	1032	java/io/IOException
    //   603	608	1032	java/io/IOException
    //   610	615	1032	java/io/IOException
    //   626	629	1032	java/io/IOException
    //   634	638	1032	java/io/IOException
    //   640	645	1032	java/io/IOException
    //   648	653	1032	java/io/IOException
    //   658	662	1032	java/io/IOException
    //   666	672	1032	java/io/IOException
    //   672	677	1032	java/io/IOException
    //   679	684	1032	java/io/IOException
    //   687	692	1032	java/io/IOException
    //   695	699	1032	java/io/IOException
    //   706	710	1032	java/io/IOException
    //   712	717	1032	java/io/IOException
    //   719	724	1032	java/io/IOException
    //   735	738	1032	java/io/IOException
    //   743	747	1032	java/io/IOException
    //   749	754	1032	java/io/IOException
    //   757	762	1032	java/io/IOException
    //   767	771	1032	java/io/IOException
    //   775	781	1032	java/io/IOException
    //   781	786	1032	java/io/IOException
    //   788	793	1032	java/io/IOException
    //   796	801	1032	java/io/IOException
    //   804	808	1032	java/io/IOException
    //   815	819	1032	java/io/IOException
    //   821	826	1032	java/io/IOException
    //   828	833	1032	java/io/IOException
    //   844	847	1032	java/io/IOException
    //   852	856	1032	java/io/IOException
    //   858	863	1032	java/io/IOException
    //   866	871	1032	java/io/IOException
    //   876	880	1032	java/io/IOException
    //   884	890	1032	java/io/IOException
    //   890	895	1032	java/io/IOException
    //   897	902	1032	java/io/IOException
    //   905	910	1032	java/io/IOException
    //   913	917	1032	java/io/IOException
    //   924	928	1032	java/io/IOException
    //   930	935	1032	java/io/IOException
    //   937	942	1032	java/io/IOException
    //   953	956	1032	java/io/IOException
    //   961	965	1032	java/io/IOException
    //   967	972	1032	java/io/IOException
    //   975	980	1032	java/io/IOException
    //   985	989	1032	java/io/IOException
    //   993	999	1032	java/io/IOException
    //   999	1004	1032	java/io/IOException
    //   1006	1011	1032	java/io/IOException
    //   1014	1019	1032	java/io/IOException
    //   152	156	1064	com/google/f/x
    //   241	246	1064	com/google/f/x
    //   259	263	1064	com/google/f/x
    //   270	274	1064	com/google/f/x
    //   276	281	1064	com/google/f/x
    //   283	288	1064	com/google/f/x
    //   299	302	1064	com/google/f/x
    //   307	311	1064	com/google/f/x
    //   313	318	1064	com/google/f/x
    //   321	326	1064	com/google/f/x
    //   331	335	1064	com/google/f/x
    //   339	345	1064	com/google/f/x
    //   345	350	1064	com/google/f/x
    //   352	357	1064	com/google/f/x
    //   360	365	1064	com/google/f/x
    //   368	372	1064	com/google/f/x
    //   379	383	1064	com/google/f/x
    //   385	390	1064	com/google/f/x
    //   392	397	1064	com/google/f/x
    //   408	411	1064	com/google/f/x
    //   416	420	1064	com/google/f/x
    //   422	427	1064	com/google/f/x
    //   430	435	1064	com/google/f/x
    //   440	444	1064	com/google/f/x
    //   448	454	1064	com/google/f/x
    //   454	459	1064	com/google/f/x
    //   461	466	1064	com/google/f/x
    //   469	474	1064	com/google/f/x
    //   477	481	1064	com/google/f/x
    //   488	492	1064	com/google/f/x
    //   494	499	1064	com/google/f/x
    //   501	506	1064	com/google/f/x
    //   517	520	1064	com/google/f/x
    //   525	529	1064	com/google/f/x
    //   531	536	1064	com/google/f/x
    //   539	544	1064	com/google/f/x
    //   549	553	1064	com/google/f/x
    //   557	563	1064	com/google/f/x
    //   563	568	1064	com/google/f/x
    //   570	575	1064	com/google/f/x
    //   578	583	1064	com/google/f/x
    //   586	590	1064	com/google/f/x
    //   597	601	1064	com/google/f/x
    //   603	608	1064	com/google/f/x
    //   610	615	1064	com/google/f/x
    //   626	629	1064	com/google/f/x
    //   634	638	1064	com/google/f/x
    //   640	645	1064	com/google/f/x
    //   648	653	1064	com/google/f/x
    //   658	662	1064	com/google/f/x
    //   666	672	1064	com/google/f/x
    //   672	677	1064	com/google/f/x
    //   679	684	1064	com/google/f/x
    //   687	692	1064	com/google/f/x
    //   695	699	1064	com/google/f/x
    //   706	710	1064	com/google/f/x
    //   712	717	1064	com/google/f/x
    //   719	724	1064	com/google/f/x
    //   735	738	1064	com/google/f/x
    //   743	747	1064	com/google/f/x
    //   749	754	1064	com/google/f/x
    //   757	762	1064	com/google/f/x
    //   767	771	1064	com/google/f/x
    //   775	781	1064	com/google/f/x
    //   781	786	1064	com/google/f/x
    //   788	793	1064	com/google/f/x
    //   796	801	1064	com/google/f/x
    //   804	808	1064	com/google/f/x
    //   815	819	1064	com/google/f/x
    //   821	826	1064	com/google/f/x
    //   828	833	1064	com/google/f/x
    //   844	847	1064	com/google/f/x
    //   852	856	1064	com/google/f/x
    //   858	863	1064	com/google/f/x
    //   866	871	1064	com/google/f/x
    //   876	880	1064	com/google/f/x
    //   884	890	1064	com/google/f/x
    //   890	895	1064	com/google/f/x
    //   897	902	1064	com/google/f/x
    //   905	910	1064	com/google/f/x
    //   913	917	1064	com/google/f/x
    //   924	928	1064	com/google/f/x
    //   930	935	1064	com/google/f/x
    //   937	942	1064	com/google/f/x
    //   953	956	1064	com/google/f/x
    //   961	965	1064	com/google/f/x
    //   967	972	1064	com/google/f/x
    //   975	980	1064	com/google/f/x
    //   985	989	1064	com/google/f/x
    //   993	999	1064	com/google/f/x
    //   999	1004	1064	com/google/f/x
    //   1006	1011	1064	com/google/f/x
    //   1014	1019	1064	com/google/f/x
  }
  
  public final boolean e()
  {
    b localb = c;
    return localb != null;
  }
  
  public final b f()
  {
    b localb = c;
    if (localb == null) {
      localb = b.d();
    }
    return localb;
  }
  
  public final boolean g()
  {
    d locald = d;
    return locald != null;
  }
  
  public final int getSerializedSize()
  {
    int j = memoizedSerializedSize;
    int k = -1;
    if (j != k) {
      return j;
    }
    Object localObject1 = a;
    k = 0;
    Object localObject2;
    if (localObject1 != null)
    {
      localObject2 = b();
      j = h.computeMessageSize(1, (ae)localObject2);
      k = 0 + j;
    }
    localObject1 = b;
    if (localObject1 != null)
    {
      localObject2 = d();
      j = h.computeMessageSize(2, (ae)localObject2);
      k += j;
    }
    localObject1 = c;
    if (localObject1 != null)
    {
      localObject2 = f();
      j = h.computeMessageSize(3, (ae)localObject2);
      k += j;
    }
    localObject1 = d;
    if (localObject1 != null)
    {
      localObject2 = h();
      j = h.computeMessageSize(4, (ae)localObject2);
      k += j;
    }
    localObject1 = e;
    if (localObject1 != null)
    {
      localObject2 = i();
      j = h.computeMessageSize(5, (ae)localObject2);
      k += j;
    }
    localObject1 = f;
    if (localObject1 != null)
    {
      localObject2 = n();
      j = h.computeMessageSize(6, (ae)localObject2);
      k += j;
    }
    localObject1 = g;
    if (localObject1 != null)
    {
      localObject2 = k();
      j = h.computeMessageSize(7, (ae)localObject2);
      k += j;
    }
    memoizedSerializedSize = k;
    return k;
  }
  
  public final d h()
  {
    d locald = d;
    if (locald == null) {
      locald = d.b();
    }
    return locald;
  }
  
  public final i i()
  {
    i locali = e;
    if (locali == null) {
      locali = i.d();
    }
    return locali;
  }
  
  public final boolean j()
  {
    f localf = g;
    return localf != null;
  }
  
  public final f k()
  {
    f localf = g;
    if (localf == null) {
      localf = f.e();
    }
    return localf;
  }
  
  public final void writeTo(h paramh)
  {
    Object localObject1 = a;
    int j;
    Object localObject2;
    if (localObject1 != null)
    {
      j = 1;
      localObject2 = b();
      paramh.writeMessage(j, (ae)localObject2);
    }
    localObject1 = b;
    if (localObject1 != null)
    {
      j = 2;
      localObject2 = d();
      paramh.writeMessage(j, (ae)localObject2);
    }
    localObject1 = c;
    if (localObject1 != null)
    {
      j = 3;
      localObject2 = f();
      paramh.writeMessage(j, (ae)localObject2);
    }
    localObject1 = d;
    if (localObject1 != null)
    {
      j = 4;
      localObject2 = h();
      paramh.writeMessage(j, (ae)localObject2);
    }
    localObject1 = e;
    if (localObject1 != null)
    {
      j = 5;
      localObject2 = i();
      paramh.writeMessage(j, (ae)localObject2);
    }
    localObject1 = f;
    if (localObject1 != null)
    {
      j = 6;
      localObject2 = n();
      paramh.writeMessage(j, (ae)localObject2);
    }
    localObject1 = g;
    if (localObject1 != null)
    {
      j = 7;
      localObject2 = k();
      paramh.writeMessage(j, (ae)localObject2);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.presence.v1.c.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */