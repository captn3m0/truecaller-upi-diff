package com.truecaller.api.services.messenger.v1;

import com.google.f.q.a;
import com.truecaller.api.services.messenger.v1.models.input.InputPeer;

public final class a$b$a
  extends q.a
  implements a.c
{
  private a$b$a()
  {
    super(localb);
  }
  
  public final a a(long paramLong)
  {
    copyOnWrite();
    a.b.a((a.b)instance, paramLong);
    return this;
  }
  
  public final a a(InputPeer paramInputPeer)
  {
    copyOnWrite();
    a.b.a((a.b)instance, paramInputPeer);
    return this;
  }
  
  public final a a(Iterable paramIterable)
  {
    copyOnWrite();
    a.b.a((a.b)instance, paramIterable);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.a.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */