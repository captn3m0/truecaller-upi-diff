package com.truecaller.api.services.messenger.v1;

import com.google.f.ae;
import com.truecaller.api.services.messenger.v1.events.Event;
import com.truecaller.api.services.messenger.v1.events.Event.a;
import io.grpc.ap;
import io.grpc.ap.a;
import io.grpc.ap.b;
import io.grpc.ap.c;
import io.grpc.b.a.b;
import io.grpc.f;

public final class k
{
  private static volatile ap a;
  private static volatile ap b;
  private static volatile ap c;
  private static volatile ap d;
  private static volatile ap e;
  private static volatile ap f;
  private static volatile ap g;
  private static volatile ap h;
  private static volatile ap i;
  private static volatile ap j;
  private static volatile ap k;
  private static volatile ap l;
  private static volatile ap m;
  private static volatile ap n;
  
  public static k.b a(f paramf)
  {
    k.b localb = new com/truecaller/api/services/messenger/v1/k$b;
    localb.<init>(paramf, (byte)0);
    return localb;
  }
  
  public static ap a()
  {
    Object localObject1 = a;
    if (localObject1 == null) {
      synchronized (k.class)
      {
        localObject1 = a;
        if (localObject1 == null)
        {
          localObject1 = ap.a();
          Object localObject2 = ap.c.a;
          c = ((ap.c)localObject2);
          localObject2 = "truecaller.messenger.v1.Messenger";
          String str = "Register";
          localObject2 = ap.a((String)localObject2, str);
          d = ((String)localObject2);
          boolean bool = true;
          h = bool;
          localObject2 = l.b.a();
          localObject2 = b.a((ae)localObject2);
          a = ((ap.b)localObject2);
          localObject2 = l.d.b();
          localObject2 = b.a((ae)localObject2);
          b = ((ap.b)localObject2);
          localObject1 = ((ap.a)localObject1).a();
          a = (ap)localObject1;
        }
      }
    }
    return localap;
  }
  
  public static k.a b(f paramf)
  {
    k.a locala = new com/truecaller/api/services/messenger/v1/k$a;
    locala.<init>(paramf, (byte)0);
    return locala;
  }
  
  public static ap b()
  {
    Object localObject1 = b;
    if (localObject1 == null) {
      synchronized (k.class)
      {
        localObject1 = b;
        if (localObject1 == null)
        {
          localObject1 = ap.a();
          Object localObject2 = ap.c.a;
          c = ((ap.c)localObject2);
          localObject2 = "truecaller.messenger.v1.Messenger";
          String str = "GetUsers";
          localObject2 = ap.a((String)localObject2, str);
          d = ((String)localObject2);
          boolean bool = true;
          h = bool;
          localObject2 = h.b.b();
          localObject2 = b.a((ae)localObject2);
          a = ((ap.b)localObject2);
          localObject2 = h.d.b();
          localObject2 = b.a((ae)localObject2);
          b = ((ap.b)localObject2);
          localObject1 = ((ap.a)localObject1).a();
          b = (ap)localObject1;
        }
      }
    }
    return localap;
  }
  
  public static ap c()
  {
    Object localObject1 = c;
    if (localObject1 == null) {
      synchronized (k.class)
      {
        localObject1 = c;
        if (localObject1 == null)
        {
          localObject1 = ap.a();
          Object localObject2 = ap.c.d;
          c = ((ap.c)localObject2);
          localObject2 = "truecaller.messenger.v1.Messenger";
          String str = "Subscribe";
          localObject2 = ap.a((String)localObject2, str);
          d = ((String)localObject2);
          boolean bool = true;
          h = bool;
          localObject2 = Event.a.b();
          localObject2 = b.a((ae)localObject2);
          a = ((ap.b)localObject2);
          localObject2 = Event.m();
          localObject2 = b.a((ae)localObject2);
          b = ((ap.b)localObject2);
          localObject1 = ((ap.a)localObject1).a();
          c = (ap)localObject1;
        }
      }
    }
    return localap;
  }
  
  public static ap d()
  {
    Object localObject1 = d;
    if (localObject1 == null) {
      synchronized (k.class)
      {
        localObject1 = d;
        if (localObject1 == null)
        {
          localObject1 = ap.a();
          Object localObject2 = ap.c.a;
          c = ((ap.c)localObject2);
          localObject2 = "truecaller.messenger.v1.Messenger";
          String str = "SendMessage";
          localObject2 = ap.a((String)localObject2, str);
          d = ((String)localObject2);
          boolean bool = true;
          h = bool;
          localObject2 = p.b.b();
          localObject2 = b.a((ae)localObject2);
          a = ((ap.b)localObject2);
          localObject2 = p.d.b();
          localObject2 = b.a((ae)localObject2);
          b = ((ap.b)localObject2);
          localObject1 = ((ap.a)localObject1).a();
          d = (ap)localObject1;
        }
      }
    }
    return localap;
  }
  
  public static ap e()
  {
    Object localObject1 = e;
    if (localObject1 == null) {
      synchronized (k.class)
      {
        localObject1 = e;
        if (localObject1 == null)
        {
          localObject1 = ap.a();
          Object localObject2 = ap.c.a;
          c = ((ap.c)localObject2);
          localObject2 = "truecaller.messenger.v1.Messenger";
          String str = "SendReport";
          localObject2 = ap.a((String)localObject2, str);
          d = ((String)localObject2);
          boolean bool = true;
          h = bool;
          localObject2 = t.b.b();
          localObject2 = b.a((ae)localObject2);
          a = ((ap.b)localObject2);
          localObject2 = t.d.a();
          localObject2 = b.a((ae)localObject2);
          b = ((ap.b)localObject2);
          localObject1 = ((ap.a)localObject1).a();
          e = (ap)localObject1;
        }
      }
    }
    return localap;
  }
  
  public static ap f()
  {
    Object localObject1 = f;
    if (localObject1 == null) {
      synchronized (k.class)
      {
        localObject1 = f;
        if (localObject1 == null)
        {
          localObject1 = ap.a();
          Object localObject2 = ap.c.a;
          c = ((ap.c)localObject2);
          localObject2 = "truecaller.messenger.v1.Messenger";
          String str = "GetMediaHandles";
          localObject2 = ap.a((String)localObject2, str);
          d = ((String)localObject2);
          boolean bool = true;
          h = bool;
          localObject2 = MediaHandles.Request.b();
          localObject2 = b.a((ae)localObject2);
          a = ((ap.b)localObject2);
          localObject2 = MediaHandles.c.d();
          localObject2 = b.a((ae)localObject2);
          b = ((ap.b)localObject2);
          localObject1 = ((ap.a)localObject1).a();
          f = (ap)localObject1;
        }
      }
    }
    return localap;
  }
  
  public static ap g()
  {
    Object localObject1 = g;
    if (localObject1 == null) {
      synchronized (k.class)
      {
        localObject1 = g;
        if (localObject1 == null)
        {
          localObject1 = ap.a();
          Object localObject2 = ap.c.a;
          c = ((ap.c)localObject2);
          localObject2 = "truecaller.messenger.v1.Messenger";
          String str = "UpdateNotificationSettings";
          localObject2 = ap.a((String)localObject2, str);
          d = ((String)localObject2);
          boolean bool = true;
          h = bool;
          localObject2 = x.b.b();
          localObject2 = b.a((ae)localObject2);
          a = ((ap.b)localObject2);
          localObject2 = x.d.a();
          localObject2 = b.a((ae)localObject2);
          b = ((ap.b)localObject2);
          localObject1 = ((ap.a)localObject1).a();
          g = (ap)localObject1;
        }
      }
    }
    return localap;
  }
  
  public static ap h()
  {
    Object localObject1 = h;
    if (localObject1 == null) {
      synchronized (k.class)
      {
        localObject1 = h;
        if (localObject1 == null)
        {
          localObject1 = ap.a();
          Object localObject2 = ap.c.a;
          c = ((ap.c)localObject2);
          localObject2 = "truecaller.messenger.v1.Messenger";
          String str = "SendReaction";
          localObject2 = ap.a((String)localObject2, str);
          d = ((String)localObject2);
          boolean bool = true;
          h = bool;
          localObject2 = r.b.b();
          localObject2 = b.a((ae)localObject2);
          a = ((ap.b)localObject2);
          localObject2 = r.d.b();
          localObject2 = b.a((ae)localObject2);
          b = ((ap.b)localObject2);
          localObject1 = ((ap.a)localObject1).a();
          h = (ap)localObject1;
        }
      }
    }
    return localap;
  }
  
  public static ap i()
  {
    Object localObject1 = i;
    if (localObject1 == null) {
      synchronized (k.class)
      {
        localObject1 = i;
        if (localObject1 == null)
        {
          localObject1 = ap.a();
          Object localObject2 = ap.c.a;
          c = ((ap.c)localObject2);
          localObject2 = "truecaller.messenger.v1.Messenger";
          String str = "GetChangelog";
          localObject2 = ap.a((String)localObject2, str);
          d = ((String)localObject2);
          boolean bool = true;
          h = bool;
          localObject2 = f.b.b();
          localObject2 = b.a((ae)localObject2);
          a = ((ap.b)localObject2);
          localObject2 = f.d.b();
          localObject2 = b.a((ae)localObject2);
          b = ((ap.b)localObject2);
          localObject1 = ((ap.a)localObject1).a();
          i = (ap)localObject1;
        }
      }
    }
    return localap;
  }
  
  public static ap j()
  {
    Object localObject1 = j;
    if (localObject1 == null) {
      synchronized (k.class)
      {
        localObject1 = j;
        if (localObject1 == null)
        {
          localObject1 = ap.a();
          Object localObject2 = ap.c.a;
          c = ((ap.c)localObject2);
          localObject2 = "truecaller.messenger.v1.Messenger";
          String str = "CreateGroup";
          localObject2 = ap.a((String)localObject2, str);
          d = ((String)localObject2);
          boolean bool = true;
          h = bool;
          localObject2 = d.b.b();
          localObject2 = b.a((ae)localObject2);
          a = ((ap.b)localObject2);
          localObject2 = d.d.h();
          localObject2 = b.a((ae)localObject2);
          b = ((ap.b)localObject2);
          localObject1 = ((ap.a)localObject1).a();
          j = (ap)localObject1;
        }
      }
    }
    return localap;
  }
  
  public static ap k()
  {
    Object localObject1 = k;
    if (localObject1 == null) {
      synchronized (k.class)
      {
        localObject1 = k;
        if (localObject1 == null)
        {
          localObject1 = ap.a();
          Object localObject2 = ap.c.a;
          c = ((ap.c)localObject2);
          localObject2 = "truecaller.messenger.v1.Messenger";
          String str = "UpdateGroupInfo";
          localObject2 = ap.a((String)localObject2, str);
          d = ((String)localObject2);
          boolean bool = true;
          h = bool;
          localObject2 = v.b.b();
          localObject2 = b.a((ae)localObject2);
          a = ((ap.b)localObject2);
          localObject2 = v.d.c();
          localObject2 = b.a((ae)localObject2);
          b = ((ap.b)localObject2);
          localObject1 = ((ap.a)localObject1).a();
          k = (ap)localObject1;
        }
      }
    }
    return localap;
  }
  
  public static ap l()
  {
    Object localObject1 = l;
    if (localObject1 == null) {
      synchronized (k.class)
      {
        localObject1 = l;
        if (localObject1 == null)
        {
          localObject1 = ap.a();
          Object localObject2 = ap.c.a;
          c = ((ap.c)localObject2);
          localObject2 = "truecaller.messenger.v1.Messenger";
          String str = "AddParticipants";
          localObject2 = ap.a((String)localObject2, str);
          d = ((String)localObject2);
          boolean bool = true;
          h = bool;
          localObject2 = a.b.b();
          localObject2 = b.a((ae)localObject2);
          a = ((ap.b)localObject2);
          localObject2 = a.d.d();
          localObject2 = b.a((ae)localObject2);
          b = ((ap.b)localObject2);
          localObject1 = ((ap.a)localObject1).a();
          l = (ap)localObject1;
        }
      }
    }
    return localap;
  }
  
  public static ap m()
  {
    Object localObject1 = m;
    if (localObject1 == null) {
      synchronized (k.class)
      {
        localObject1 = m;
        if (localObject1 == null)
        {
          localObject1 = ap.a();
          Object localObject2 = ap.c.a;
          c = ((ap.c)localObject2);
          localObject2 = "truecaller.messenger.v1.Messenger";
          String str = "RemoveParticipants";
          localObject2 = ap.a((String)localObject2, str);
          d = ((String)localObject2);
          boolean bool = true;
          h = bool;
          localObject2 = n.b.b();
          localObject2 = b.a((ae)localObject2);
          a = ((ap.b)localObject2);
          localObject2 = n.d.b();
          localObject2 = b.a((ae)localObject2);
          b = ((ap.b)localObject2);
          localObject1 = ((ap.a)localObject1).a();
          m = (ap)localObject1;
        }
      }
    }
    return localap;
  }
  
  public static ap n()
  {
    Object localObject1 = n;
    if (localObject1 == null) {
      synchronized (k.class)
      {
        localObject1 = n;
        if (localObject1 == null)
        {
          localObject1 = ap.a();
          Object localObject2 = ap.c.a;
          c = ((ap.c)localObject2);
          localObject2 = "truecaller.messenger.v1.Messenger";
          String str = "UpdateRoles";
          localObject2 = ap.a((String)localObject2, str);
          d = ((String)localObject2);
          boolean bool = true;
          h = bool;
          localObject2 = z.b.b();
          localObject2 = b.a((ae)localObject2);
          a = ((ap.b)localObject2);
          localObject2 = z.d.c();
          localObject2 = b.a((ae)localObject2);
          b = ((ap.b)localObject2);
          localObject1 = ((ap.a)localObject1).a();
          n = (ap)localObject1;
        }
      }
    }
    return localap;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */