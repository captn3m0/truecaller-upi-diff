package com.truecaller.api.services.messenger.v1;

import com.google.f.w.c;

public enum Batched$Request$Payload$DataCase
  implements w.c
{
  private final int value;
  
  static
  {
    Object localObject = new com/truecaller/api/services/messenger/v1/Batched$Request$Payload$DataCase;
    int i = 2;
    ((DataCase)localObject).<init>("SENDMESSAGE", 0, i);
    SENDMESSAGE = (DataCase)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/Batched$Request$Payload$DataCase;
    int j = 1;
    int k = 3;
    ((DataCase)localObject).<init>("SENDREPORT", j, k);
    SENDREPORT = (DataCase)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/Batched$Request$Payload$DataCase;
    int m = 4;
    ((DataCase)localObject).<init>("SENDREACTION", i, m);
    SENDREACTION = (DataCase)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/Batched$Request$Payload$DataCase;
    ((DataCase)localObject).<init>("DATA_NOT_SET", k, 0);
    DATA_NOT_SET = (DataCase)localObject;
    localObject = new DataCase[m];
    DataCase localDataCase = SENDMESSAGE;
    localObject[0] = localDataCase;
    localDataCase = SENDREPORT;
    localObject[j] = localDataCase;
    localDataCase = SENDREACTION;
    localObject[i] = localDataCase;
    localDataCase = DATA_NOT_SET;
    localObject[k] = localDataCase;
    $VALUES = (DataCase[])localObject;
  }
  
  private Batched$Request$Payload$DataCase(int paramInt1)
  {
    value = paramInt1;
  }
  
  public static DataCase forNumber(int paramInt)
  {
    if (paramInt != 0)
    {
      switch (paramInt)
      {
      default: 
        return null;
      case 4: 
        return SENDREACTION;
      case 3: 
        return SENDREPORT;
      }
      return SENDMESSAGE;
    }
    return DATA_NOT_SET;
  }
  
  public static DataCase valueOf(int paramInt)
  {
    return forNumber(paramInt);
  }
  
  public final int getNumber()
  {
    return value;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.Batched.Request.Payload.DataCase
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */