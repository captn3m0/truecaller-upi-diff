package com.truecaller.api.services.messenger.v1;

import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;

public final class f$b
  extends q
  implements f.c
{
  private static final b d;
  private static volatile ah e;
  private int a;
  private int b;
  private String c = "";
  
  static
  {
    b localb = new com/truecaller/api/services/messenger/v1/f$b;
    localb.<init>();
    d = localb;
    localb.makeImmutable();
  }
  
  public static f.b.a a()
  {
    return (f.b.a)d.toBuilder();
  }
  
  public static b b()
  {
    return d;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 47	com/truecaller/api/services/messenger/v1/f$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 53	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_0
    //   19: istore 6
    //   21: aconst_null
    //   22: astore 4
    //   24: iconst_1
    //   25: istore 7
    //   27: iload 5
    //   29: tableswitch	default:+47->76, 1:+524->553, 2:+520->549, 3:+518->547, 4:+507->536, 5:+291->320, 6:+109->138, 7:+287->316, 8:+57->86
    //   76: new 56	java/lang/UnsupportedOperationException
    //   79: astore_1
    //   80: aload_1
    //   81: invokespecial 57	java/lang/UnsupportedOperationException:<init>	()V
    //   84: aload_1
    //   85: athrow
    //   86: getstatic 59	com/truecaller/api/services/messenger/v1/f$b:e	Lcom/google/f/ah;
    //   89: astore_1
    //   90: aload_1
    //   91: ifnonnull +43 -> 134
    //   94: ldc 2
    //   96: astore_1
    //   97: aload_1
    //   98: monitorenter
    //   99: getstatic 59	com/truecaller/api/services/messenger/v1/f$b:e	Lcom/google/f/ah;
    //   102: astore_2
    //   103: aload_2
    //   104: ifnonnull +20 -> 124
    //   107: new 61	com/google/f/q$b
    //   110: astore_2
    //   111: getstatic 21	com/truecaller/api/services/messenger/v1/f$b:d	Lcom/truecaller/api/services/messenger/v1/f$b;
    //   114: astore_3
    //   115: aload_2
    //   116: aload_3
    //   117: invokespecial 64	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   120: aload_2
    //   121: putstatic 59	com/truecaller/api/services/messenger/v1/f$b:e	Lcom/google/f/ah;
    //   124: aload_1
    //   125: monitorexit
    //   126: goto +8 -> 134
    //   129: astore_2
    //   130: aload_1
    //   131: monitorexit
    //   132: aload_2
    //   133: athrow
    //   134: getstatic 59	com/truecaller/api/services/messenger/v1/f$b:e	Lcom/google/f/ah;
    //   137: areturn
    //   138: aload_2
    //   139: checkcast 66	com/google/f/g
    //   142: astore_2
    //   143: iload 6
    //   145: ifne +171 -> 316
    //   148: aload_2
    //   149: invokevirtual 69	com/google/f/g:readTag	()I
    //   152: istore 5
    //   154: iload 5
    //   156: ifeq +98 -> 254
    //   159: bipush 8
    //   161: istore 8
    //   163: iload 5
    //   165: iload 8
    //   167: if_icmpeq +72 -> 239
    //   170: bipush 16
    //   172: istore 8
    //   174: iload 5
    //   176: iload 8
    //   178: if_icmpeq +46 -> 224
    //   181: bipush 26
    //   183: istore 8
    //   185: iload 5
    //   187: iload 8
    //   189: if_icmpeq +22 -> 211
    //   192: aload_2
    //   193: iload 5
    //   195: invokevirtual 76	com/google/f/g:skipField	(I)Z
    //   198: istore 5
    //   200: iload 5
    //   202: ifne -59 -> 143
    //   205: iconst_1
    //   206: istore 6
    //   208: goto -65 -> 143
    //   211: aload_2
    //   212: invokevirtual 80	com/google/f/g:readStringRequireUtf8	()Ljava/lang/String;
    //   215: astore_1
    //   216: aload_0
    //   217: aload_1
    //   218: putfield 29	com/truecaller/api/services/messenger/v1/f$b:c	Ljava/lang/String;
    //   221: goto -78 -> 143
    //   224: aload_2
    //   225: invokevirtual 83	com/google/f/g:readInt32	()I
    //   228: istore 5
    //   230: aload_0
    //   231: iload 5
    //   233: putfield 42	com/truecaller/api/services/messenger/v1/f$b:b	I
    //   236: goto -93 -> 143
    //   239: aload_2
    //   240: invokevirtual 83	com/google/f/g:readInt32	()I
    //   243: istore 5
    //   245: aload_0
    //   246: iload 5
    //   248: putfield 37	com/truecaller/api/services/messenger/v1/f$b:a	I
    //   251: goto -108 -> 143
    //   254: iconst_1
    //   255: istore 6
    //   257: goto -114 -> 143
    //   260: astore_1
    //   261: goto +53 -> 314
    //   264: astore_1
    //   265: new 85	java/lang/RuntimeException
    //   268: astore_2
    //   269: new 87	com/google/f/x
    //   272: astore_3
    //   273: aload_1
    //   274: invokevirtual 92	java/io/IOException:getMessage	()Ljava/lang/String;
    //   277: astore_1
    //   278: aload_3
    //   279: aload_1
    //   280: invokespecial 95	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   283: aload_3
    //   284: aload_0
    //   285: invokevirtual 99	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   288: astore_1
    //   289: aload_2
    //   290: aload_1
    //   291: invokespecial 102	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   294: aload_2
    //   295: athrow
    //   296: astore_1
    //   297: new 85	java/lang/RuntimeException
    //   300: astore_2
    //   301: aload_1
    //   302: aload_0
    //   303: invokevirtual 99	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   306: astore_1
    //   307: aload_2
    //   308: aload_1
    //   309: invokespecial 102	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   312: aload_2
    //   313: athrow
    //   314: aload_1
    //   315: athrow
    //   316: getstatic 21	com/truecaller/api/services/messenger/v1/f$b:d	Lcom/truecaller/api/services/messenger/v1/f$b;
    //   319: areturn
    //   320: aload_2
    //   321: checkcast 104	com/google/f/q$k
    //   324: astore_2
    //   325: aload_3
    //   326: checkcast 2	com/truecaller/api/services/messenger/v1/f$b
    //   329: astore_3
    //   330: aload_0
    //   331: getfield 37	com/truecaller/api/services/messenger/v1/f$b:a	I
    //   334: istore 5
    //   336: iload 5
    //   338: ifeq +9 -> 347
    //   341: iconst_1
    //   342: istore 5
    //   344: goto +8 -> 352
    //   347: iconst_0
    //   348: istore 5
    //   350: aconst_null
    //   351: astore_1
    //   352: aload_0
    //   353: getfield 37	com/truecaller/api/services/messenger/v1/f$b:a	I
    //   356: istore 9
    //   358: aload_3
    //   359: getfield 37	com/truecaller/api/services/messenger/v1/f$b:a	I
    //   362: istore 10
    //   364: iload 10
    //   366: ifeq +9 -> 375
    //   369: iconst_1
    //   370: istore 10
    //   372: goto +6 -> 378
    //   375: iconst_0
    //   376: istore 10
    //   378: aload_3
    //   379: getfield 37	com/truecaller/api/services/messenger/v1/f$b:a	I
    //   382: istore 11
    //   384: aload_2
    //   385: iload 5
    //   387: iload 9
    //   389: iload 10
    //   391: iload 11
    //   393: invokeinterface 108 5 0
    //   398: istore 5
    //   400: aload_0
    //   401: iload 5
    //   403: putfield 37	com/truecaller/api/services/messenger/v1/f$b:a	I
    //   406: aload_0
    //   407: getfield 42	com/truecaller/api/services/messenger/v1/f$b:b	I
    //   410: istore 5
    //   412: iload 5
    //   414: ifeq +9 -> 423
    //   417: iconst_1
    //   418: istore 5
    //   420: goto +8 -> 428
    //   423: iconst_0
    //   424: istore 5
    //   426: aconst_null
    //   427: astore_1
    //   428: aload_0
    //   429: getfield 42	com/truecaller/api/services/messenger/v1/f$b:b	I
    //   432: istore 9
    //   434: aload_3
    //   435: getfield 42	com/truecaller/api/services/messenger/v1/f$b:b	I
    //   438: istore 10
    //   440: iload 10
    //   442: ifeq +6 -> 448
    //   445: iconst_1
    //   446: istore 6
    //   448: aload_3
    //   449: getfield 42	com/truecaller/api/services/messenger/v1/f$b:b	I
    //   452: istore 10
    //   454: aload_2
    //   455: iload 5
    //   457: iload 9
    //   459: iload 6
    //   461: iload 10
    //   463: invokeinterface 108 5 0
    //   468: istore 5
    //   470: aload_0
    //   471: iload 5
    //   473: putfield 42	com/truecaller/api/services/messenger/v1/f$b:b	I
    //   476: aload_0
    //   477: getfield 29	com/truecaller/api/services/messenger/v1/f$b:c	Ljava/lang/String;
    //   480: invokevirtual 114	java/lang/String:isEmpty	()Z
    //   483: iload 7
    //   485: ixor
    //   486: istore 5
    //   488: aload_0
    //   489: getfield 29	com/truecaller/api/services/messenger/v1/f$b:c	Ljava/lang/String;
    //   492: astore 4
    //   494: aload_3
    //   495: getfield 29	com/truecaller/api/services/messenger/v1/f$b:c	Ljava/lang/String;
    //   498: invokevirtual 114	java/lang/String:isEmpty	()Z
    //   501: istore 9
    //   503: iload 7
    //   505: iload 9
    //   507: ixor
    //   508: istore 7
    //   510: aload_3
    //   511: getfield 29	com/truecaller/api/services/messenger/v1/f$b:c	Ljava/lang/String;
    //   514: astore_3
    //   515: aload_2
    //   516: iload 5
    //   518: aload 4
    //   520: iload 7
    //   522: aload_3
    //   523: invokeinterface 118 5 0
    //   528: astore_1
    //   529: aload_0
    //   530: aload_1
    //   531: putfield 29	com/truecaller/api/services/messenger/v1/f$b:c	Ljava/lang/String;
    //   534: aload_0
    //   535: areturn
    //   536: new 35	com/truecaller/api/services/messenger/v1/f$b$a
    //   539: astore_1
    //   540: aload_1
    //   541: iconst_0
    //   542: invokespecial 121	com/truecaller/api/services/messenger/v1/f$b$a:<init>	(B)V
    //   545: aload_1
    //   546: areturn
    //   547: aconst_null
    //   548: areturn
    //   549: getstatic 21	com/truecaller/api/services/messenger/v1/f$b:d	Lcom/truecaller/api/services/messenger/v1/f$b;
    //   552: areturn
    //   553: new 2	com/truecaller/api/services/messenger/v1/f$b
    //   556: astore_1
    //   557: aload_1
    //   558: invokespecial 19	com/truecaller/api/services/messenger/v1/f$b:<init>	()V
    //   561: aload_1
    //   562: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	563	0	this	b
    //   0	563	1	paramj	com.google.f.q.j
    //   0	563	2	paramObject1	Object
    //   0	563	3	paramObject2	Object
    //   3	516	4	localObject	Object
    //   9	185	5	i	int
    //   198	3	5	bool1	boolean
    //   228	158	5	j	int
    //   398	58	5	k	int
    //   468	4	5	m	int
    //   486	31	5	bool2	boolean
    //   19	441	6	bool3	boolean
    //   25	496	7	bool4	boolean
    //   161	29	8	n	int
    //   356	102	9	i1	int
    //   501	7	9	bool5	boolean
    //   362	28	10	i2	int
    //   438	24	10	i3	int
    //   382	10	11	i4	int
    // Exception table:
    //   from	to	target	type
    //   99	102	129	finally
    //   107	110	129	finally
    //   111	114	129	finally
    //   116	120	129	finally
    //   120	124	129	finally
    //   124	126	129	finally
    //   130	132	129	finally
    //   148	152	260	finally
    //   193	198	260	finally
    //   211	215	260	finally
    //   217	221	260	finally
    //   224	228	260	finally
    //   231	236	260	finally
    //   239	243	260	finally
    //   246	251	260	finally
    //   265	268	260	finally
    //   269	272	260	finally
    //   273	277	260	finally
    //   279	283	260	finally
    //   284	288	260	finally
    //   290	294	260	finally
    //   294	296	260	finally
    //   297	300	260	finally
    //   302	306	260	finally
    //   308	312	260	finally
    //   312	314	260	finally
    //   148	152	264	java/io/IOException
    //   193	198	264	java/io/IOException
    //   211	215	264	java/io/IOException
    //   217	221	264	java/io/IOException
    //   224	228	264	java/io/IOException
    //   231	236	264	java/io/IOException
    //   239	243	264	java/io/IOException
    //   246	251	264	java/io/IOException
    //   148	152	296	com/google/f/x
    //   193	198	296	com/google/f/x
    //   211	215	296	com/google/f/x
    //   217	221	296	com/google/f/x
    //   224	228	296	com/google/f/x
    //   231	236	296	com/google/f/x
    //   239	243	296	com/google/f/x
    //   246	251	296	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int k = -1;
    if (i != k) {
      return i;
    }
    i = a;
    k = 0;
    int m;
    if (i != 0)
    {
      m = 1;
      i = h.computeInt32Size(m, i);
      k = 0 + i;
    }
    i = b;
    if (i != 0)
    {
      m = 2;
      i = h.computeInt32Size(m, i);
      k += i;
    }
    String str1 = c;
    boolean bool = str1.isEmpty();
    if (!bool)
    {
      String str2 = c;
      int j = h.computeStringSize(3, str2);
      k += j;
    }
    memoizedSerializedSize = k;
    return k;
  }
  
  public final void writeTo(h paramh)
  {
    int i = a;
    int k;
    if (i != 0)
    {
      k = 1;
      paramh.writeInt32(k, i);
    }
    i = b;
    if (i != 0)
    {
      k = 2;
      paramh.writeInt32(k, i);
    }
    String str1 = c;
    boolean bool = str1.isEmpty();
    if (!bool)
    {
      int j = 3;
      String str2 = c;
      paramh.writeString(j, str2);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.f.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */