package com.truecaller.api.services.messenger.v1;

import com.google.f.ae;
import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;
import com.truecaller.api.services.messenger.v1.models.input.InputPeer;
import com.truecaller.api.services.messenger.v1.models.input.InputReportType;

public final class t$b
  extends q
  implements t.c
{
  private static final b d;
  private static volatile ah e;
  private InputPeer a;
  private String b = "";
  private int c;
  
  static
  {
    b localb = new com/truecaller/api/services/messenger/v1/t$b;
    localb.<init>();
    d = localb;
    localb.makeImmutable();
  }
  
  public static t.b.a a()
  {
    return (t.b.a)d.toBuilder();
  }
  
  public static b b()
  {
    return d;
  }
  
  public static ah c()
  {
    return d.getParserForType();
  }
  
  private InputPeer e()
  {
    InputPeer localInputPeer = a;
    if (localInputPeer == null) {
      localInputPeer = InputPeer.b();
    }
    return localInputPeer;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 63	com/truecaller/api/services/messenger/v1/t$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 68	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_0
    //   19: istore 6
    //   21: aconst_null
    //   22: astore 4
    //   24: iconst_0
    //   25: istore 7
    //   27: iconst_1
    //   28: istore 8
    //   30: iload 5
    //   32: tableswitch	default:+48->80, 1:+564->596, 2:+560->592, 3:+558->590, 4:+547->579, 5:+376->408, 6:+110->142, 7:+372->404, 8:+58->90
    //   80: new 71	java/lang/UnsupportedOperationException
    //   83: astore_1
    //   84: aload_1
    //   85: invokespecial 72	java/lang/UnsupportedOperationException:<init>	()V
    //   88: aload_1
    //   89: athrow
    //   90: getstatic 74	com/truecaller/api/services/messenger/v1/t$b:e	Lcom/google/f/ah;
    //   93: astore_1
    //   94: aload_1
    //   95: ifnonnull +43 -> 138
    //   98: ldc 2
    //   100: astore_1
    //   101: aload_1
    //   102: monitorenter
    //   103: getstatic 74	com/truecaller/api/services/messenger/v1/t$b:e	Lcom/google/f/ah;
    //   106: astore_2
    //   107: aload_2
    //   108: ifnonnull +20 -> 128
    //   111: new 76	com/google/f/q$b
    //   114: astore_2
    //   115: getstatic 22	com/truecaller/api/services/messenger/v1/t$b:d	Lcom/truecaller/api/services/messenger/v1/t$b;
    //   118: astore_3
    //   119: aload_2
    //   120: aload_3
    //   121: invokespecial 79	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   124: aload_2
    //   125: putstatic 74	com/truecaller/api/services/messenger/v1/t$b:e	Lcom/google/f/ah;
    //   128: aload_1
    //   129: monitorexit
    //   130: goto +8 -> 138
    //   133: astore_2
    //   134: aload_1
    //   135: monitorexit
    //   136: aload_2
    //   137: athrow
    //   138: getstatic 74	com/truecaller/api/services/messenger/v1/t$b:e	Lcom/google/f/ah;
    //   141: areturn
    //   142: aload_2
    //   143: checkcast 81	com/google/f/g
    //   146: astore_2
    //   147: aload_3
    //   148: checkcast 83	com/google/f/n
    //   151: astore_3
    //   152: iload 7
    //   154: ifne +250 -> 404
    //   157: aload_2
    //   158: invokevirtual 86	com/google/f/g:readTag	()I
    //   161: istore 5
    //   163: iload 5
    //   165: ifeq +177 -> 342
    //   168: bipush 10
    //   170: istore 9
    //   172: iload 5
    //   174: iload 9
    //   176: if_icmpeq +72 -> 248
    //   179: bipush 18
    //   181: istore 9
    //   183: iload 5
    //   185: iload 9
    //   187: if_icmpeq +48 -> 235
    //   190: bipush 24
    //   192: istore 9
    //   194: iload 5
    //   196: iload 9
    //   198: if_icmpeq +22 -> 220
    //   201: aload_2
    //   202: iload 5
    //   204: invokevirtual 93	com/google/f/g:skipField	(I)Z
    //   207: istore 5
    //   209: iload 5
    //   211: ifne -59 -> 152
    //   214: iconst_1
    //   215: istore 7
    //   217: goto -65 -> 152
    //   220: aload_2
    //   221: invokevirtual 96	com/google/f/g:readEnum	()I
    //   224: istore 5
    //   226: aload_0
    //   227: iload 5
    //   229: putfield 49	com/truecaller/api/services/messenger/v1/t$b:c	I
    //   232: goto -80 -> 152
    //   235: aload_2
    //   236: invokevirtual 100	com/google/f/g:readStringRequireUtf8	()Ljava/lang/String;
    //   239: astore_1
    //   240: aload_0
    //   241: aload_1
    //   242: putfield 30	com/truecaller/api/services/messenger/v1/t$b:b	Ljava/lang/String;
    //   245: goto -93 -> 152
    //   248: aload_0
    //   249: getfield 38	com/truecaller/api/services/messenger/v1/t$b:a	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   252: astore_1
    //   253: aload_1
    //   254: ifnull +21 -> 275
    //   257: aload_0
    //   258: getfield 38	com/truecaller/api/services/messenger/v1/t$b:a	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   261: astore_1
    //   262: aload_1
    //   263: invokevirtual 101	com/truecaller/api/services/messenger/v1/models/input/InputPeer:toBuilder	()Lcom/google/f/q$a;
    //   266: astore_1
    //   267: aload_1
    //   268: checkcast 103	com/truecaller/api/services/messenger/v1/models/input/InputPeer$a
    //   271: astore_1
    //   272: goto +8 -> 280
    //   275: iconst_0
    //   276: istore 5
    //   278: aconst_null
    //   279: astore_1
    //   280: invokestatic 105	com/truecaller/api/services/messenger/v1/models/input/InputPeer:c	()Lcom/google/f/ah;
    //   283: astore 10
    //   285: aload_2
    //   286: aload 10
    //   288: aload_3
    //   289: invokevirtual 109	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   292: astore 10
    //   294: aload 10
    //   296: checkcast 55	com/truecaller/api/services/messenger/v1/models/input/InputPeer
    //   299: astore 10
    //   301: aload_0
    //   302: aload 10
    //   304: putfield 38	com/truecaller/api/services/messenger/v1/t$b:a	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   307: aload_1
    //   308: ifnull -156 -> 152
    //   311: aload_0
    //   312: getfield 38	com/truecaller/api/services/messenger/v1/t$b:a	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   315: astore 10
    //   317: aload_1
    //   318: aload 10
    //   320: invokevirtual 113	com/truecaller/api/services/messenger/v1/models/input/InputPeer$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   323: pop
    //   324: aload_1
    //   325: invokevirtual 117	com/truecaller/api/services/messenger/v1/models/input/InputPeer$a:buildPartial	()Lcom/google/f/q;
    //   328: astore_1
    //   329: aload_1
    //   330: checkcast 55	com/truecaller/api/services/messenger/v1/models/input/InputPeer
    //   333: astore_1
    //   334: aload_0
    //   335: aload_1
    //   336: putfield 38	com/truecaller/api/services/messenger/v1/t$b:a	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   339: goto -187 -> 152
    //   342: iconst_1
    //   343: istore 7
    //   345: goto -193 -> 152
    //   348: astore_1
    //   349: goto +53 -> 402
    //   352: astore_1
    //   353: new 119	java/lang/RuntimeException
    //   356: astore_2
    //   357: new 121	com/google/f/x
    //   360: astore_3
    //   361: aload_1
    //   362: invokevirtual 126	java/io/IOException:getMessage	()Ljava/lang/String;
    //   365: astore_1
    //   366: aload_3
    //   367: aload_1
    //   368: invokespecial 129	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   371: aload_3
    //   372: aload_0
    //   373: invokevirtual 133	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   376: astore_1
    //   377: aload_2
    //   378: aload_1
    //   379: invokespecial 136	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   382: aload_2
    //   383: athrow
    //   384: astore_1
    //   385: new 119	java/lang/RuntimeException
    //   388: astore_2
    //   389: aload_1
    //   390: aload_0
    //   391: invokevirtual 133	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   394: astore_1
    //   395: aload_2
    //   396: aload_1
    //   397: invokespecial 136	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   400: aload_2
    //   401: athrow
    //   402: aload_1
    //   403: athrow
    //   404: getstatic 22	com/truecaller/api/services/messenger/v1/t$b:d	Lcom/truecaller/api/services/messenger/v1/t$b;
    //   407: areturn
    //   408: aload_2
    //   409: checkcast 138	com/google/f/q$k
    //   412: astore_2
    //   413: aload_3
    //   414: checkcast 2	com/truecaller/api/services/messenger/v1/t$b
    //   417: astore_3
    //   418: aload_0
    //   419: getfield 38	com/truecaller/api/services/messenger/v1/t$b:a	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   422: astore_1
    //   423: aload_3
    //   424: getfield 38	com/truecaller/api/services/messenger/v1/t$b:a	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   427: astore 4
    //   429: aload_2
    //   430: aload_1
    //   431: aload 4
    //   433: invokeinterface 142 3 0
    //   438: checkcast 55	com/truecaller/api/services/messenger/v1/models/input/InputPeer
    //   441: astore_1
    //   442: aload_0
    //   443: aload_1
    //   444: putfield 38	com/truecaller/api/services/messenger/v1/t$b:a	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   447: aload_0
    //   448: getfield 30	com/truecaller/api/services/messenger/v1/t$b:b	Ljava/lang/String;
    //   451: invokevirtual 148	java/lang/String:isEmpty	()Z
    //   454: iload 8
    //   456: ixor
    //   457: istore 5
    //   459: aload_0
    //   460: getfield 30	com/truecaller/api/services/messenger/v1/t$b:b	Ljava/lang/String;
    //   463: astore 4
    //   465: aload_3
    //   466: getfield 30	com/truecaller/api/services/messenger/v1/t$b:b	Ljava/lang/String;
    //   469: astore 10
    //   471: aload 10
    //   473: invokevirtual 148	java/lang/String:isEmpty	()Z
    //   476: iload 8
    //   478: ixor
    //   479: istore 9
    //   481: aload_3
    //   482: getfield 30	com/truecaller/api/services/messenger/v1/t$b:b	Ljava/lang/String;
    //   485: astore 11
    //   487: aload_2
    //   488: iload 5
    //   490: aload 4
    //   492: iload 9
    //   494: aload 11
    //   496: invokeinterface 152 5 0
    //   501: astore_1
    //   502: aload_0
    //   503: aload_1
    //   504: putfield 30	com/truecaller/api/services/messenger/v1/t$b:b	Ljava/lang/String;
    //   507: aload_0
    //   508: getfield 49	com/truecaller/api/services/messenger/v1/t$b:c	I
    //   511: istore 5
    //   513: iload 5
    //   515: ifeq +9 -> 524
    //   518: iconst_1
    //   519: istore 5
    //   521: goto +8 -> 529
    //   524: iconst_0
    //   525: istore 5
    //   527: aconst_null
    //   528: astore_1
    //   529: aload_0
    //   530: getfield 49	com/truecaller/api/services/messenger/v1/t$b:c	I
    //   533: istore 6
    //   535: aload_3
    //   536: getfield 49	com/truecaller/api/services/messenger/v1/t$b:c	I
    //   539: istore 9
    //   541: iload 9
    //   543: ifeq +6 -> 549
    //   546: iconst_1
    //   547: istore 7
    //   549: aload_3
    //   550: getfield 49	com/truecaller/api/services/messenger/v1/t$b:c	I
    //   553: istore 12
    //   555: aload_2
    //   556: iload 5
    //   558: iload 6
    //   560: iload 7
    //   562: iload 12
    //   564: invokeinterface 156 5 0
    //   569: istore 5
    //   571: aload_0
    //   572: iload 5
    //   574: putfield 49	com/truecaller/api/services/messenger/v1/t$b:c	I
    //   577: aload_0
    //   578: areturn
    //   579: new 36	com/truecaller/api/services/messenger/v1/t$b$a
    //   582: astore_1
    //   583: aload_1
    //   584: iconst_0
    //   585: invokespecial 159	com/truecaller/api/services/messenger/v1/t$b$a:<init>	(B)V
    //   588: aload_1
    //   589: areturn
    //   590: aconst_null
    //   591: areturn
    //   592: getstatic 22	com/truecaller/api/services/messenger/v1/t$b:d	Lcom/truecaller/api/services/messenger/v1/t$b;
    //   595: areturn
    //   596: new 2	com/truecaller/api/services/messenger/v1/t$b
    //   599: astore_1
    //   600: aload_1
    //   601: invokespecial 20	com/truecaller/api/services/messenger/v1/t$b:<init>	()V
    //   604: aload_1
    //   605: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	606	0	this	b
    //   0	606	1	paramj	com.google.f.q.j
    //   0	606	2	paramObject1	Object
    //   0	606	3	paramObject2	Object
    //   3	488	4	localObject1	Object
    //   9	194	5	i	int
    //   207	3	5	bool1	boolean
    //   224	53	5	j	int
    //   457	32	5	bool2	boolean
    //   511	46	5	k	int
    //   569	4	5	m	int
    //   19	540	6	n	int
    //   25	536	7	bool3	boolean
    //   28	451	8	i1	int
    //   170	29	9	i2	int
    //   479	14	9	bool4	boolean
    //   539	3	9	i3	int
    //   283	189	10	localObject2	Object
    //   485	10	11	str	String
    //   553	10	12	i4	int
    // Exception table:
    //   from	to	target	type
    //   103	106	133	finally
    //   111	114	133	finally
    //   115	118	133	finally
    //   120	124	133	finally
    //   124	128	133	finally
    //   128	130	133	finally
    //   134	136	133	finally
    //   157	161	348	finally
    //   202	207	348	finally
    //   220	224	348	finally
    //   227	232	348	finally
    //   235	239	348	finally
    //   241	245	348	finally
    //   248	252	348	finally
    //   257	261	348	finally
    //   262	266	348	finally
    //   267	271	348	finally
    //   280	283	348	finally
    //   288	292	348	finally
    //   294	299	348	finally
    //   302	307	348	finally
    //   311	315	348	finally
    //   318	324	348	finally
    //   324	328	348	finally
    //   329	333	348	finally
    //   335	339	348	finally
    //   353	356	348	finally
    //   357	360	348	finally
    //   361	365	348	finally
    //   367	371	348	finally
    //   372	376	348	finally
    //   378	382	348	finally
    //   382	384	348	finally
    //   385	388	348	finally
    //   390	394	348	finally
    //   396	400	348	finally
    //   400	402	348	finally
    //   157	161	352	java/io/IOException
    //   202	207	352	java/io/IOException
    //   220	224	352	java/io/IOException
    //   227	232	352	java/io/IOException
    //   235	239	352	java/io/IOException
    //   241	245	352	java/io/IOException
    //   248	252	352	java/io/IOException
    //   257	261	352	java/io/IOException
    //   262	266	352	java/io/IOException
    //   267	271	352	java/io/IOException
    //   280	283	352	java/io/IOException
    //   288	292	352	java/io/IOException
    //   294	299	352	java/io/IOException
    //   302	307	352	java/io/IOException
    //   311	315	352	java/io/IOException
    //   318	324	352	java/io/IOException
    //   324	328	352	java/io/IOException
    //   329	333	352	java/io/IOException
    //   335	339	352	java/io/IOException
    //   157	161	384	com/google/f/x
    //   202	207	384	com/google/f/x
    //   220	224	384	com/google/f/x
    //   227	232	384	com/google/f/x
    //   235	239	384	com/google/f/x
    //   241	245	384	com/google/f/x
    //   248	252	384	com/google/f/x
    //   257	261	384	com/google/f/x
    //   262	266	384	com/google/f/x
    //   267	271	384	com/google/f/x
    //   280	283	384	com/google/f/x
    //   288	292	384	com/google/f/x
    //   294	299	384	com/google/f/x
    //   302	307	384	com/google/f/x
    //   311	315	384	com/google/f/x
    //   318	324	384	com/google/f/x
    //   324	328	384	com/google/f/x
    //   329	333	384	com/google/f/x
    //   335	339	384	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int k = -1;
    if (i != k) {
      return i;
    }
    Object localObject1 = a;
    k = 0;
    if (localObject1 != null)
    {
      localObject2 = e();
      i = h.computeMessageSize(1, (ae)localObject2);
      k = 0 + i;
    }
    localObject1 = b;
    boolean bool = ((String)localObject1).isEmpty();
    if (!bool)
    {
      localObject2 = b;
      j = h.computeStringSize(2, (String)localObject2);
      k += j;
    }
    int j = c;
    Object localObject2 = InputReportType.UNKNOWN_REPORT_TYPE;
    int m = ((InputReportType)localObject2).getNumber();
    if (j != m)
    {
      m = c;
      j = h.computeEnumSize(3, m);
      k += j;
    }
    memoizedSerializedSize = k;
    return k;
  }
  
  public final void writeTo(h paramh)
  {
    Object localObject1 = a;
    if (localObject1 != null)
    {
      int i = 1;
      localObject2 = e();
      paramh.writeMessage(i, (ae)localObject2);
    }
    localObject1 = b;
    boolean bool = ((String)localObject1).isEmpty();
    if (!bool)
    {
      j = 2;
      localObject2 = b;
      paramh.writeString(j, (String)localObject2);
    }
    int j = c;
    Object localObject2 = InputReportType.UNKNOWN_REPORT_TYPE;
    int k = ((InputReportType)localObject2).getNumber();
    if (j != k)
    {
      j = 3;
      k = c;
      paramh.writeEnum(j, k);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.t.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */