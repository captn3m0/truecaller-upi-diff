package com.truecaller.api.services.messenger.v1;

import com.google.f.q.a;
import com.truecaller.api.services.messenger.v1.models.input.InputMessageContent;
import com.truecaller.api.services.messenger.v1.models.input.InputPeer;

public final class p$b$a
  extends q.a
  implements p.c
{
  private p$b$a()
  {
    super(localb);
  }
  
  public final a a(long paramLong)
  {
    copyOnWrite();
    p.b.a((p.b)instance, paramLong);
    return this;
  }
  
  public final a a(InputMessageContent paramInputMessageContent)
  {
    copyOnWrite();
    p.b.a((p.b)instance, paramInputMessageContent);
    return this;
  }
  
  public final a a(InputPeer paramInputPeer)
  {
    copyOnWrite();
    p.b.a((p.b)instance, paramInputPeer);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.p.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */