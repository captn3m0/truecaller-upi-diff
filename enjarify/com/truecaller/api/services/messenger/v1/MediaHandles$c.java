package com.truecaller.api.services.messenger.v1;

import com.google.f.ac;
import com.google.f.ad;
import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class MediaHandles$c
  extends q
  implements MediaHandles.d
{
  private static final c f;
  private static volatile ah g;
  private int a;
  private String b;
  private String c;
  private long d;
  private ad e;
  
  static
  {
    c localc = new com/truecaller/api/services/messenger/v1/MediaHandles$c;
    localc.<init>();
    f = localc;
    localc.makeImmutable();
  }
  
  private MediaHandles$c()
  {
    ad localad = ad.emptyMapField();
    e = localad;
    b = "";
    c = "";
  }
  
  public static c d()
  {
    return f;
  }
  
  public final String a()
  {
    return b;
  }
  
  public final String b()
  {
    return c;
  }
  
  public final Map c()
  {
    return Collections.unmodifiableMap(e);
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 54	com/truecaller/api/services/messenger/v1/MediaHandles$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 60	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_0
    //   19: istore 6
    //   21: aconst_null
    //   22: astore 4
    //   24: iconst_1
    //   25: istore 7
    //   27: iload 5
    //   29: tableswitch	default:+47->76, 1:+674->703, 2:+670->699, 3:+661->690, 4:+650->679, 5:+357->386, 6:+109->138, 7:+353->382, 8:+57->86
    //   76: new 63	java/lang/UnsupportedOperationException
    //   79: astore_1
    //   80: aload_1
    //   81: invokespecial 64	java/lang/UnsupportedOperationException:<init>	()V
    //   84: aload_1
    //   85: athrow
    //   86: getstatic 66	com/truecaller/api/services/messenger/v1/MediaHandles$c:g	Lcom/google/f/ah;
    //   89: astore_1
    //   90: aload_1
    //   91: ifnonnull +43 -> 134
    //   94: ldc 2
    //   96: astore_1
    //   97: aload_1
    //   98: monitorenter
    //   99: getstatic 66	com/truecaller/api/services/messenger/v1/MediaHandles$c:g	Lcom/google/f/ah;
    //   102: astore_2
    //   103: aload_2
    //   104: ifnonnull +20 -> 124
    //   107: new 68	com/google/f/q$b
    //   110: astore_2
    //   111: getstatic 25	com/truecaller/api/services/messenger/v1/MediaHandles$c:f	Lcom/truecaller/api/services/messenger/v1/MediaHandles$c;
    //   114: astore_3
    //   115: aload_2
    //   116: aload_3
    //   117: invokespecial 71	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   120: aload_2
    //   121: putstatic 66	com/truecaller/api/services/messenger/v1/MediaHandles$c:g	Lcom/google/f/ah;
    //   124: aload_1
    //   125: monitorexit
    //   126: goto +8 -> 134
    //   129: astore_2
    //   130: aload_1
    //   131: monitorexit
    //   132: aload_2
    //   133: athrow
    //   134: getstatic 66	com/truecaller/api/services/messenger/v1/MediaHandles$c:g	Lcom/google/f/ah;
    //   137: areturn
    //   138: aload_2
    //   139: checkcast 73	com/google/f/g
    //   142: astore_2
    //   143: aload_3
    //   144: checkcast 75	com/google/f/n
    //   147: astore_3
    //   148: iload 6
    //   150: ifne +232 -> 382
    //   153: aload_2
    //   154: invokevirtual 78	com/google/f/g:readTag	()I
    //   157: istore 5
    //   159: iload 5
    //   161: ifeq +159 -> 320
    //   164: bipush 10
    //   166: istore 8
    //   168: iload 5
    //   170: iload 8
    //   172: if_icmpeq +135 -> 307
    //   175: bipush 18
    //   177: istore 8
    //   179: iload 5
    //   181: iload 8
    //   183: if_icmpeq +111 -> 294
    //   186: bipush 24
    //   188: istore 8
    //   190: iload 5
    //   192: iload 8
    //   194: if_icmpeq +85 -> 279
    //   197: bipush 34
    //   199: istore 8
    //   201: iload 5
    //   203: iload 8
    //   205: if_icmpeq +22 -> 227
    //   208: aload_2
    //   209: iload 5
    //   211: invokevirtual 86	com/google/f/g:skipField	(I)Z
    //   214: istore 5
    //   216: iload 5
    //   218: ifne -70 -> 148
    //   221: iconst_1
    //   222: istore 6
    //   224: goto -76 -> 148
    //   227: aload_0
    //   228: getfield 37	com/truecaller/api/services/messenger/v1/MediaHandles$c:e	Lcom/google/f/ad;
    //   231: astore_1
    //   232: aload_1
    //   233: invokevirtual 90	com/google/f/ad:isMutable	()Z
    //   236: istore 5
    //   238: iload 5
    //   240: ifne +18 -> 258
    //   243: aload_0
    //   244: getfield 37	com/truecaller/api/services/messenger/v1/MediaHandles$c:e	Lcom/google/f/ad;
    //   247: astore_1
    //   248: aload_1
    //   249: invokevirtual 93	com/google/f/ad:mutableCopy	()Lcom/google/f/ad;
    //   252: astore_1
    //   253: aload_0
    //   254: aload_1
    //   255: putfield 37	com/truecaller/api/services/messenger/v1/MediaHandles$c:e	Lcom/google/f/ad;
    //   258: getstatic 98	com/truecaller/api/services/messenger/v1/MediaHandles$c$b:a	Lcom/google/f/ac;
    //   261: astore_1
    //   262: aload_0
    //   263: getfield 37	com/truecaller/api/services/messenger/v1/MediaHandles$c:e	Lcom/google/f/ad;
    //   266: astore 9
    //   268: aload_1
    //   269: aload 9
    //   271: aload_2
    //   272: aload_3
    //   273: invokevirtual 104	com/google/f/ac:parseInto	(Lcom/google/f/ad;Lcom/google/f/g;Lcom/google/f/n;)V
    //   276: goto -128 -> 148
    //   279: aload_2
    //   280: invokevirtual 108	com/google/f/g:readInt64	()J
    //   283: lstore 10
    //   285: aload_0
    //   286: lload 10
    //   288: putfield 110	com/truecaller/api/services/messenger/v1/MediaHandles$c:d	J
    //   291: goto -143 -> 148
    //   294: aload_2
    //   295: invokevirtual 114	com/google/f/g:readStringRequireUtf8	()Ljava/lang/String;
    //   298: astore_1
    //   299: aload_0
    //   300: aload_1
    //   301: putfield 43	com/truecaller/api/services/messenger/v1/MediaHandles$c:c	Ljava/lang/String;
    //   304: goto -156 -> 148
    //   307: aload_2
    //   308: invokevirtual 114	com/google/f/g:readStringRequireUtf8	()Ljava/lang/String;
    //   311: astore_1
    //   312: aload_0
    //   313: aload_1
    //   314: putfield 41	com/truecaller/api/services/messenger/v1/MediaHandles$c:b	Ljava/lang/String;
    //   317: goto -169 -> 148
    //   320: iconst_1
    //   321: istore 6
    //   323: goto -175 -> 148
    //   326: astore_1
    //   327: goto +53 -> 380
    //   330: astore_1
    //   331: new 116	java/lang/RuntimeException
    //   334: astore_2
    //   335: new 118	com/google/f/x
    //   338: astore_3
    //   339: aload_1
    //   340: invokevirtual 123	java/io/IOException:getMessage	()Ljava/lang/String;
    //   343: astore_1
    //   344: aload_3
    //   345: aload_1
    //   346: invokespecial 126	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   349: aload_3
    //   350: aload_0
    //   351: invokevirtual 130	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   354: astore_1
    //   355: aload_2
    //   356: aload_1
    //   357: invokespecial 133	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   360: aload_2
    //   361: athrow
    //   362: astore_1
    //   363: new 116	java/lang/RuntimeException
    //   366: astore_2
    //   367: aload_1
    //   368: aload_0
    //   369: invokevirtual 130	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   372: astore_1
    //   373: aload_2
    //   374: aload_1
    //   375: invokespecial 133	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   378: aload_2
    //   379: athrow
    //   380: aload_1
    //   381: athrow
    //   382: getstatic 25	com/truecaller/api/services/messenger/v1/MediaHandles$c:f	Lcom/truecaller/api/services/messenger/v1/MediaHandles$c;
    //   385: areturn
    //   386: aload_2
    //   387: astore_1
    //   388: aload_2
    //   389: checkcast 135	com/google/f/q$k
    //   392: astore_1
    //   393: aload_3
    //   394: checkcast 2	com/truecaller/api/services/messenger/v1/MediaHandles$c
    //   397: astore_3
    //   398: aload_0
    //   399: getfield 41	com/truecaller/api/services/messenger/v1/MediaHandles$c:b	Ljava/lang/String;
    //   402: invokevirtual 140	java/lang/String:isEmpty	()Z
    //   405: iload 7
    //   407: ixor
    //   408: istore 12
    //   410: aload_0
    //   411: getfield 41	com/truecaller/api/services/messenger/v1/MediaHandles$c:b	Ljava/lang/String;
    //   414: astore 9
    //   416: aload_3
    //   417: getfield 41	com/truecaller/api/services/messenger/v1/MediaHandles$c:b	Ljava/lang/String;
    //   420: invokevirtual 140	java/lang/String:isEmpty	()Z
    //   423: iload 7
    //   425: ixor
    //   426: istore 13
    //   428: aload_3
    //   429: getfield 41	com/truecaller/api/services/messenger/v1/MediaHandles$c:b	Ljava/lang/String;
    //   432: astore 14
    //   434: aload_1
    //   435: iload 12
    //   437: aload 9
    //   439: iload 13
    //   441: aload 14
    //   443: invokeinterface 144 5 0
    //   448: astore_2
    //   449: aload_0
    //   450: aload_2
    //   451: putfield 41	com/truecaller/api/services/messenger/v1/MediaHandles$c:b	Ljava/lang/String;
    //   454: aload_0
    //   455: getfield 43	com/truecaller/api/services/messenger/v1/MediaHandles$c:c	Ljava/lang/String;
    //   458: invokevirtual 140	java/lang/String:isEmpty	()Z
    //   461: iload 7
    //   463: ixor
    //   464: istore 12
    //   466: aload_0
    //   467: getfield 43	com/truecaller/api/services/messenger/v1/MediaHandles$c:c	Ljava/lang/String;
    //   470: astore 9
    //   472: aload_3
    //   473: getfield 43	com/truecaller/api/services/messenger/v1/MediaHandles$c:c	Ljava/lang/String;
    //   476: astore 15
    //   478: aload 15
    //   480: invokevirtual 140	java/lang/String:isEmpty	()Z
    //   483: iload 7
    //   485: ixor
    //   486: istore 13
    //   488: aload_3
    //   489: getfield 43	com/truecaller/api/services/messenger/v1/MediaHandles$c:c	Ljava/lang/String;
    //   492: astore 14
    //   494: aload_1
    //   495: iload 12
    //   497: aload 9
    //   499: iload 13
    //   501: aload 14
    //   503: invokeinterface 144 5 0
    //   508: astore_2
    //   509: aload_0
    //   510: aload_2
    //   511: putfield 43	com/truecaller/api/services/messenger/v1/MediaHandles$c:c	Ljava/lang/String;
    //   514: aload_0
    //   515: getfield 110	com/truecaller/api/services/messenger/v1/MediaHandles$c:d	J
    //   518: lstore 10
    //   520: lconst_0
    //   521: lstore 16
    //   523: lload 10
    //   525: lload 16
    //   527: lcmp
    //   528: istore 12
    //   530: iload 12
    //   532: ifeq +9 -> 541
    //   535: iconst_1
    //   536: istore 12
    //   538: goto +8 -> 546
    //   541: iconst_0
    //   542: istore 12
    //   544: aconst_null
    //   545: astore_2
    //   546: aload_0
    //   547: getfield 110	com/truecaller/api/services/messenger/v1/MediaHandles$c:d	J
    //   550: lstore 10
    //   552: aload_3
    //   553: getfield 110	com/truecaller/api/services/messenger/v1/MediaHandles$c:d	J
    //   556: lstore 18
    //   558: lload 18
    //   560: lload 16
    //   562: lcmp
    //   563: istore 20
    //   565: iload 20
    //   567: ifeq +9 -> 576
    //   570: iconst_1
    //   571: istore 21
    //   573: goto +9 -> 582
    //   576: iconst_0
    //   577: istore 21
    //   579: aconst_null
    //   580: astore 14
    //   582: aload_3
    //   583: getfield 110	com/truecaller/api/services/messenger/v1/MediaHandles$c:d	J
    //   586: lstore 22
    //   588: aload_1
    //   589: astore 4
    //   591: iload 12
    //   593: istore 7
    //   595: aload_1
    //   596: iload 12
    //   598: lload 10
    //   600: iload 21
    //   602: lload 22
    //   604: invokeinterface 148 7 0
    //   609: lstore 24
    //   611: aload_0
    //   612: lload 24
    //   614: putfield 110	com/truecaller/api/services/messenger/v1/MediaHandles$c:d	J
    //   617: aload_0
    //   618: getfield 37	com/truecaller/api/services/messenger/v1/MediaHandles$c:e	Lcom/google/f/ad;
    //   621: astore_2
    //   622: aload_3
    //   623: getfield 37	com/truecaller/api/services/messenger/v1/MediaHandles$c:e	Lcom/google/f/ad;
    //   626: astore 4
    //   628: aload_1
    //   629: aload_2
    //   630: aload 4
    //   632: invokeinterface 152 3 0
    //   637: astore_2
    //   638: aload_0
    //   639: aload_2
    //   640: putfield 37	com/truecaller/api/services/messenger/v1/MediaHandles$c:e	Lcom/google/f/ad;
    //   643: getstatic 158	com/google/f/q$i:INSTANCE	Lcom/google/f/q$i;
    //   646: astore_2
    //   647: aload_1
    //   648: aload_2
    //   649: if_acmpne +28 -> 677
    //   652: aload_0
    //   653: getfield 160	com/truecaller/api/services/messenger/v1/MediaHandles$c:a	I
    //   656: istore 5
    //   658: aload_3
    //   659: getfield 160	com/truecaller/api/services/messenger/v1/MediaHandles$c:a	I
    //   662: istore 12
    //   664: iload 5
    //   666: iload 12
    //   668: ior
    //   669: istore 5
    //   671: aload_0
    //   672: iload 5
    //   674: putfield 160	com/truecaller/api/services/messenger/v1/MediaHandles$c:a	I
    //   677: aload_0
    //   678: areturn
    //   679: new 162	com/truecaller/api/services/messenger/v1/MediaHandles$c$a
    //   682: astore_1
    //   683: aload_1
    //   684: iconst_0
    //   685: invokespecial 165	com/truecaller/api/services/messenger/v1/MediaHandles$c$a:<init>	(B)V
    //   688: aload_1
    //   689: areturn
    //   690: aload_0
    //   691: getfield 37	com/truecaller/api/services/messenger/v1/MediaHandles$c:e	Lcom/google/f/ad;
    //   694: invokevirtual 166	com/google/f/ad:makeImmutable	()V
    //   697: aconst_null
    //   698: areturn
    //   699: getstatic 25	com/truecaller/api/services/messenger/v1/MediaHandles$c:f	Lcom/truecaller/api/services/messenger/v1/MediaHandles$c;
    //   702: areturn
    //   703: new 2	com/truecaller/api/services/messenger/v1/MediaHandles$c
    //   706: astore_1
    //   707: aload_1
    //   708: invokespecial 23	com/truecaller/api/services/messenger/v1/MediaHandles$c:<init>	()V
    //   711: aload_1
    //   712: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	713	0	this	c
    //   0	713	1	paramj	com.google.f.q.j
    //   0	713	2	paramObject1	Object
    //   0	713	3	paramObject2	Object
    //   3	628	4	localObject1	Object
    //   9	201	5	i	int
    //   214	25	5	bool1	boolean
    //   656	17	5	j	int
    //   19	303	6	k	int
    //   25	569	7	m	int
    //   166	40	8	n	int
    //   266	232	9	localObject2	Object
    //   283	316	10	l1	long
    //   408	189	12	bool2	boolean
    //   662	7	12	i1	int
    //   426	74	13	bool3	boolean
    //   432	149	14	str1	String
    //   476	3	15	str2	String
    //   521	40	16	l2	long
    //   556	3	18	l3	long
    //   563	3	20	bool4	boolean
    //   571	30	21	bool5	boolean
    //   586	17	22	l4	long
    //   609	4	24	l5	long
    // Exception table:
    //   from	to	target	type
    //   99	102	129	finally
    //   107	110	129	finally
    //   111	114	129	finally
    //   116	120	129	finally
    //   120	124	129	finally
    //   124	126	129	finally
    //   130	132	129	finally
    //   153	157	326	finally
    //   209	214	326	finally
    //   227	231	326	finally
    //   232	236	326	finally
    //   243	247	326	finally
    //   248	252	326	finally
    //   254	258	326	finally
    //   258	261	326	finally
    //   262	266	326	finally
    //   272	276	326	finally
    //   279	283	326	finally
    //   286	291	326	finally
    //   294	298	326	finally
    //   300	304	326	finally
    //   307	311	326	finally
    //   313	317	326	finally
    //   331	334	326	finally
    //   335	338	326	finally
    //   339	343	326	finally
    //   345	349	326	finally
    //   350	354	326	finally
    //   356	360	326	finally
    //   360	362	326	finally
    //   363	366	326	finally
    //   368	372	326	finally
    //   374	378	326	finally
    //   378	380	326	finally
    //   153	157	330	java/io/IOException
    //   209	214	330	java/io/IOException
    //   227	231	330	java/io/IOException
    //   232	236	330	java/io/IOException
    //   243	247	330	java/io/IOException
    //   248	252	330	java/io/IOException
    //   254	258	330	java/io/IOException
    //   258	261	330	java/io/IOException
    //   262	266	330	java/io/IOException
    //   272	276	330	java/io/IOException
    //   279	283	330	java/io/IOException
    //   286	291	330	java/io/IOException
    //   294	298	330	java/io/IOException
    //   300	304	330	java/io/IOException
    //   307	311	330	java/io/IOException
    //   313	317	330	java/io/IOException
    //   153	157	362	com/google/f/x
    //   209	214	362	com/google/f/x
    //   227	231	362	com/google/f/x
    //   232	236	362	com/google/f/x
    //   243	247	362	com/google/f/x
    //   248	252	362	com/google/f/x
    //   254	258	362	com/google/f/x
    //   258	261	362	com/google/f/x
    //   262	266	362	com/google/f/x
    //   272	276	362	com/google/f/x
    //   279	283	362	com/google/f/x
    //   286	291	362	com/google/f/x
    //   294	298	362	com/google/f/x
    //   300	304	362	com/google/f/x
    //   307	311	362	com/google/f/x
    //   313	317	362	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int n = -1;
    if (i != n) {
      return i;
    }
    Object localObject1 = b;
    boolean bool1 = ((String)localObject1).isEmpty();
    n = 0;
    Object localObject2;
    if (!bool1)
    {
      localObject2 = b;
      int j = h.computeStringSize(1, (String)localObject2);
      n = 0 + j;
    }
    localObject1 = c;
    boolean bool2 = ((String)localObject1).isEmpty();
    if (!bool2)
    {
      localObject2 = c;
      int k = h.computeStringSize(2, (String)localObject2);
      n += k;
    }
    long l1 = d;
    long l2 = 0L;
    boolean bool3 = l1 < l2;
    if (bool3)
    {
      int m = h.computeInt64Size(3, l1);
      n += m;
    }
    localObject1 = e.entrySet().iterator();
    for (;;)
    {
      boolean bool4 = ((Iterator)localObject1).hasNext();
      if (!bool4) {
        break;
      }
      localObject2 = (Map.Entry)((Iterator)localObject1).next();
      ac localac = MediaHandles.c.b.a;
      int i2 = 4;
      Object localObject3 = ((Map.Entry)localObject2).getKey();
      localObject2 = ((Map.Entry)localObject2).getValue();
      int i1 = localac.computeMessageSize(i2, localObject3, localObject2);
      n += i1;
    }
    memoizedSerializedSize = n;
    return n;
  }
  
  public final void writeTo(h paramh)
  {
    Object localObject1 = b;
    int i = ((String)localObject1).isEmpty();
    Object localObject2;
    if (i == 0)
    {
      i = 1;
      localObject2 = b;
      paramh.writeString(i, (String)localObject2);
    }
    localObject1 = c;
    boolean bool1 = ((String)localObject1).isEmpty();
    if (!bool1)
    {
      int j = 2;
      localObject2 = c;
      paramh.writeString(j, (String)localObject2);
    }
    long l1 = d;
    long l2 = 0L;
    boolean bool2 = l1 < l2;
    if (bool2)
    {
      int k = 3;
      paramh.writeInt64(k, l1);
    }
    localObject1 = e.entrySet().iterator();
    for (;;)
    {
      boolean bool3 = ((Iterator)localObject1).hasNext();
      if (!bool3) {
        break;
      }
      localObject2 = (Map.Entry)((Iterator)localObject1).next();
      ac localac = MediaHandles.c.b.a;
      int m = 4;
      Object localObject3 = ((Map.Entry)localObject2).getKey();
      localObject2 = ((Map.Entry)localObject2).getValue();
      localac.serializeTo(paramh, m, localObject3, localObject2);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.MediaHandles.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */