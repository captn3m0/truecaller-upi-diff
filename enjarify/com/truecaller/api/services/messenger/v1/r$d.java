package com.truecaller.api.services.messenger.v1;

import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;

public final class r$d
  extends q
  implements r.e
{
  private static final d b;
  private static volatile ah c;
  private int a;
  
  static
  {
    d locald = new com/truecaller/api/services/messenger/v1/r$d;
    locald.<init>();
    b = locald;
    locald.makeImmutable();
  }
  
  public static d b()
  {
    return b;
  }
  
  public static ah c()
  {
    return b.getParserForType();
  }
  
  public final int a()
  {
    return a;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 33	com/truecaller/api/services/messenger/v1/r$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 39	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_1
    //   19: istore 6
    //   21: iconst_0
    //   22: istore 7
    //   24: iload 5
    //   26: tableswitch	default:+46->72, 1:+345->371, 2:+341->367, 3:+339->365, 4:+328->354, 5:+240->266, 6:+108->134, 7:+236->262, 8:+56->82
    //   72: new 42	java/lang/UnsupportedOperationException
    //   75: astore_1
    //   76: aload_1
    //   77: invokespecial 43	java/lang/UnsupportedOperationException:<init>	()V
    //   80: aload_1
    //   81: athrow
    //   82: getstatic 45	com/truecaller/api/services/messenger/v1/r$d:c	Lcom/google/f/ah;
    //   85: astore_1
    //   86: aload_1
    //   87: ifnonnull +43 -> 130
    //   90: ldc 2
    //   92: astore_1
    //   93: aload_1
    //   94: monitorenter
    //   95: getstatic 45	com/truecaller/api/services/messenger/v1/r$d:c	Lcom/google/f/ah;
    //   98: astore_2
    //   99: aload_2
    //   100: ifnonnull +20 -> 120
    //   103: new 47	com/google/f/q$b
    //   106: astore_2
    //   107: getstatic 18	com/truecaller/api/services/messenger/v1/r$d:b	Lcom/truecaller/api/services/messenger/v1/r$d;
    //   110: astore_3
    //   111: aload_2
    //   112: aload_3
    //   113: invokespecial 50	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   116: aload_2
    //   117: putstatic 45	com/truecaller/api/services/messenger/v1/r$d:c	Lcom/google/f/ah;
    //   120: aload_1
    //   121: monitorexit
    //   122: goto +8 -> 130
    //   125: astore_2
    //   126: aload_1
    //   127: monitorexit
    //   128: aload_2
    //   129: athrow
    //   130: getstatic 45	com/truecaller/api/services/messenger/v1/r$d:c	Lcom/google/f/ah;
    //   133: areturn
    //   134: aload_2
    //   135: checkcast 52	com/google/f/g
    //   138: astore_2
    //   139: iload 7
    //   141: ifne +121 -> 262
    //   144: aload_2
    //   145: invokevirtual 55	com/google/f/g:readTag	()I
    //   148: istore 5
    //   150: iload 5
    //   152: ifeq +48 -> 200
    //   155: bipush 8
    //   157: istore 8
    //   159: iload 5
    //   161: iload 8
    //   163: if_icmpeq +22 -> 185
    //   166: aload_2
    //   167: iload 5
    //   169: invokevirtual 60	com/google/f/g:skipField	(I)Z
    //   172: istore 5
    //   174: iload 5
    //   176: ifne -37 -> 139
    //   179: iconst_1
    //   180: istore 7
    //   182: goto -43 -> 139
    //   185: aload_2
    //   186: invokevirtual 63	com/google/f/g:readInt32	()I
    //   189: istore 5
    //   191: aload_0
    //   192: iload 5
    //   194: putfield 28	com/truecaller/api/services/messenger/v1/r$d:a	I
    //   197: goto -58 -> 139
    //   200: iconst_1
    //   201: istore 7
    //   203: goto -64 -> 139
    //   206: astore_1
    //   207: goto +53 -> 260
    //   210: astore_1
    //   211: new 65	java/lang/RuntimeException
    //   214: astore_2
    //   215: new 67	com/google/f/x
    //   218: astore_3
    //   219: aload_1
    //   220: invokevirtual 73	java/io/IOException:getMessage	()Ljava/lang/String;
    //   223: astore_1
    //   224: aload_3
    //   225: aload_1
    //   226: invokespecial 76	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   229: aload_3
    //   230: aload_0
    //   231: invokevirtual 80	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   234: astore_1
    //   235: aload_2
    //   236: aload_1
    //   237: invokespecial 83	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   240: aload_2
    //   241: athrow
    //   242: astore_1
    //   243: new 65	java/lang/RuntimeException
    //   246: astore_2
    //   247: aload_1
    //   248: aload_0
    //   249: invokevirtual 80	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   252: astore_1
    //   253: aload_2
    //   254: aload_1
    //   255: invokespecial 83	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   258: aload_2
    //   259: athrow
    //   260: aload_1
    //   261: athrow
    //   262: getstatic 18	com/truecaller/api/services/messenger/v1/r$d:b	Lcom/truecaller/api/services/messenger/v1/r$d;
    //   265: areturn
    //   266: aload_2
    //   267: checkcast 85	com/google/f/q$k
    //   270: astore_2
    //   271: aload_3
    //   272: checkcast 2	com/truecaller/api/services/messenger/v1/r$d
    //   275: astore_3
    //   276: aload_0
    //   277: getfield 28	com/truecaller/api/services/messenger/v1/r$d:a	I
    //   280: istore 5
    //   282: iload 5
    //   284: ifeq +9 -> 293
    //   287: iconst_1
    //   288: istore 5
    //   290: goto +8 -> 298
    //   293: iconst_0
    //   294: istore 5
    //   296: aconst_null
    //   297: astore_1
    //   298: aload_0
    //   299: getfield 28	com/truecaller/api/services/messenger/v1/r$d:a	I
    //   302: istore 9
    //   304: aload_3
    //   305: getfield 28	com/truecaller/api/services/messenger/v1/r$d:a	I
    //   308: istore 10
    //   310: iload 10
    //   312: ifeq +6 -> 318
    //   315: goto +9 -> 324
    //   318: iconst_0
    //   319: istore 6
    //   321: aconst_null
    //   322: astore 4
    //   324: aload_3
    //   325: getfield 28	com/truecaller/api/services/messenger/v1/r$d:a	I
    //   328: istore 8
    //   330: aload_2
    //   331: iload 5
    //   333: iload 9
    //   335: iload 6
    //   337: iload 8
    //   339: invokeinterface 89 5 0
    //   344: istore 5
    //   346: aload_0
    //   347: iload 5
    //   349: putfield 28	com/truecaller/api/services/messenger/v1/r$d:a	I
    //   352: aload_0
    //   353: areturn
    //   354: new 91	com/truecaller/api/services/messenger/v1/r$d$a
    //   357: astore_1
    //   358: aload_1
    //   359: iconst_0
    //   360: invokespecial 94	com/truecaller/api/services/messenger/v1/r$d$a:<init>	(B)V
    //   363: aload_1
    //   364: areturn
    //   365: aconst_null
    //   366: areturn
    //   367: getstatic 18	com/truecaller/api/services/messenger/v1/r$d:b	Lcom/truecaller/api/services/messenger/v1/r$d;
    //   370: areturn
    //   371: new 2	com/truecaller/api/services/messenger/v1/r$d
    //   374: astore_1
    //   375: aload_1
    //   376: invokespecial 16	com/truecaller/api/services/messenger/v1/r$d:<init>	()V
    //   379: aload_1
    //   380: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	381	0	this	d
    //   0	381	1	paramj	com.google.f.q.j
    //   0	381	2	paramObject1	Object
    //   0	381	3	paramObject2	Object
    //   3	320	4	arrayOfInt	int[]
    //   9	159	5	i	int
    //   172	3	5	bool1	boolean
    //   189	143	5	j	int
    //   344	4	5	k	int
    //   19	317	6	bool2	boolean
    //   22	180	7	m	int
    //   157	181	8	n	int
    //   302	32	9	i1	int
    //   308	3	10	i2	int
    // Exception table:
    //   from	to	target	type
    //   95	98	125	finally
    //   103	106	125	finally
    //   107	110	125	finally
    //   112	116	125	finally
    //   116	120	125	finally
    //   120	122	125	finally
    //   126	128	125	finally
    //   144	148	206	finally
    //   167	172	206	finally
    //   185	189	206	finally
    //   192	197	206	finally
    //   211	214	206	finally
    //   215	218	206	finally
    //   219	223	206	finally
    //   225	229	206	finally
    //   230	234	206	finally
    //   236	240	206	finally
    //   240	242	206	finally
    //   243	246	206	finally
    //   248	252	206	finally
    //   254	258	206	finally
    //   258	260	206	finally
    //   144	148	210	java/io/IOException
    //   167	172	210	java/io/IOException
    //   185	189	210	java/io/IOException
    //   192	197	210	java/io/IOException
    //   144	148	242	com/google/f/x
    //   167	172	242	com/google/f/x
    //   185	189	242	com/google/f/x
    //   192	197	242	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int j = -1;
    if (i != j) {
      return i;
    }
    i = a;
    j = 0;
    if (i != 0)
    {
      int k = 1;
      i = h.computeInt32Size(k, i);
      j = 0 + i;
    }
    memoizedSerializedSize = j;
    return j;
  }
  
  public final void writeTo(h paramh)
  {
    int i = a;
    if (i != 0)
    {
      int j = 1;
      paramh.writeInt32(j, i);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.r.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */