package com.truecaller.api.services.messenger.v1.models.input;

import com.google.f.f;
import com.google.f.q.a;

public final class InputMessageContent$j$a
  extends q.a
  implements InputMessageContent.k
{
  private InputMessageContent$j$a()
  {
    super(localj);
  }
  
  public final a a(int paramInt)
  {
    copyOnWrite();
    InputMessageContent.j.a((InputMessageContent.j)instance, paramInt);
    return this;
  }
  
  public final a a(f paramf)
  {
    copyOnWrite();
    InputMessageContent.j.a((InputMessageContent.j)instance, paramf);
    return this;
  }
  
  public final a a(String paramString)
  {
    copyOnWrite();
    InputMessageContent.j.a((InputMessageContent.j)instance, paramString);
    return this;
  }
  
  public final a b(int paramInt)
  {
    copyOnWrite();
    InputMessageContent.j.b((InputMessageContent.j)instance, paramInt);
    return this;
  }
  
  public final a b(String paramString)
  {
    copyOnWrite();
    InputMessageContent.j.b((InputMessageContent.j)instance, paramString);
    return this;
  }
  
  public final a c(int paramInt)
  {
    copyOnWrite();
    InputMessageContent.j.c((InputMessageContent.j)instance, paramInt);
    return this;
  }
  
  public final a d(int paramInt)
  {
    copyOnWrite();
    InputMessageContent.j.d((InputMessageContent.j)instance, paramInt);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.models.input.InputMessageContent.j.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */