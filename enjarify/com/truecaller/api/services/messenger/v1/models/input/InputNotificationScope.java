package com.truecaller.api.services.messenger.v1.models.input;

import com.google.f.ae;
import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;
import com.google.f.t;

public final class InputNotificationScope
  extends q
  implements g
{
  private static final InputNotificationScope c;
  private static volatile ah d;
  private int a = 0;
  private Object b;
  
  static
  {
    InputNotificationScope localInputNotificationScope = new com/truecaller/api/services/messenger/v1/models/input/InputNotificationScope;
    localInputNotificationScope.<init>();
    c = localInputNotificationScope;
    localInputNotificationScope.makeImmutable();
  }
  
  public static InputNotificationScope.a a()
  {
    return (InputNotificationScope.a)c.toBuilder();
  }
  
  public static InputNotificationScope b()
  {
    return c;
  }
  
  public static ah c()
  {
    return c.getParserForType();
  }
  
  private String e()
  {
    String str = "";
    int i = a;
    int j = 2;
    if (i == j) {
      str = (String)b;
    }
    return str;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 51	com/truecaller/api/services/messenger/v1/models/input/InputNotificationScope$1:b	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 57	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_2
    //   19: istore 6
    //   21: iconst_0
    //   22: istore 7
    //   24: aconst_null
    //   25: astore 8
    //   27: iconst_0
    //   28: istore 9
    //   30: iconst_1
    //   31: istore 10
    //   33: iload 5
    //   35: tableswitch	default:+45->80, 1:+593->628, 2:+589->624, 3:+587->622, 4:+576->611, 5:+363->398, 6:+107->142, 7:+359->394, 8:+55->90
    //   80: new 60	java/lang/UnsupportedOperationException
    //   83: astore_1
    //   84: aload_1
    //   85: invokespecial 61	java/lang/UnsupportedOperationException:<init>	()V
    //   88: aload_1
    //   89: athrow
    //   90: getstatic 63	com/truecaller/api/services/messenger/v1/models/input/InputNotificationScope:d	Lcom/google/f/ah;
    //   93: astore_1
    //   94: aload_1
    //   95: ifnonnull +43 -> 138
    //   98: ldc 2
    //   100: astore_1
    //   101: aload_1
    //   102: monitorenter
    //   103: getstatic 63	com/truecaller/api/services/messenger/v1/models/input/InputNotificationScope:d	Lcom/google/f/ah;
    //   106: astore_2
    //   107: aload_2
    //   108: ifnonnull +20 -> 128
    //   111: new 65	com/google/f/q$b
    //   114: astore_2
    //   115: getstatic 20	com/truecaller/api/services/messenger/v1/models/input/InputNotificationScope:c	Lcom/truecaller/api/services/messenger/v1/models/input/InputNotificationScope;
    //   118: astore_3
    //   119: aload_2
    //   120: aload_3
    //   121: invokespecial 68	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   124: aload_2
    //   125: putstatic 63	com/truecaller/api/services/messenger/v1/models/input/InputNotificationScope:d	Lcom/google/f/ah;
    //   128: aload_1
    //   129: monitorexit
    //   130: goto +8 -> 138
    //   133: astore_2
    //   134: aload_1
    //   135: monitorexit
    //   136: aload_2
    //   137: athrow
    //   138: getstatic 63	com/truecaller/api/services/messenger/v1/models/input/InputNotificationScope:d	Lcom/google/f/ah;
    //   141: areturn
    //   142: aload_2
    //   143: checkcast 70	com/google/f/g
    //   146: astore_2
    //   147: aload_3
    //   148: checkcast 72	com/google/f/n
    //   151: astore_3
    //   152: iload 9
    //   154: ifne +240 -> 394
    //   157: aload_2
    //   158: invokevirtual 75	com/google/f/g:readTag	()I
    //   161: istore 5
    //   163: iload 5
    //   165: ifeq +167 -> 332
    //   168: bipush 10
    //   170: istore 11
    //   172: iload 5
    //   174: iload 11
    //   176: if_icmpeq +52 -> 228
    //   179: bipush 18
    //   181: istore 11
    //   183: iload 5
    //   185: iload 11
    //   187: if_icmpeq +22 -> 209
    //   190: aload_2
    //   191: iload 5
    //   193: invokevirtual 81	com/google/f/g:skipField	(I)Z
    //   196: istore 5
    //   198: iload 5
    //   200: ifne -48 -> 152
    //   203: iconst_1
    //   204: istore 9
    //   206: goto -54 -> 152
    //   209: aload_2
    //   210: invokevirtual 85	com/google/f/g:readStringRequireUtf8	()Ljava/lang/String;
    //   213: astore_1
    //   214: aload_0
    //   215: iload 6
    //   217: putfield 26	com/truecaller/api/services/messenger/v1/models/input/InputNotificationScope:a	I
    //   220: aload_0
    //   221: aload_1
    //   222: putfield 35	com/truecaller/api/services/messenger/v1/models/input/InputNotificationScope:b	Ljava/lang/Object;
    //   225: goto -73 -> 152
    //   228: aload_0
    //   229: getfield 26	com/truecaller/api/services/messenger/v1/models/input/InputNotificationScope:a	I
    //   232: istore 5
    //   234: iload 5
    //   236: iload 10
    //   238: if_icmpne +26 -> 264
    //   241: aload_0
    //   242: getfield 35	com/truecaller/api/services/messenger/v1/models/input/InputNotificationScope:b	Ljava/lang/Object;
    //   245: astore_1
    //   246: aload_1
    //   247: checkcast 87	com/google/f/t
    //   250: astore_1
    //   251: aload_1
    //   252: invokevirtual 88	com/google/f/t:toBuilder	()Lcom/google/f/q$a;
    //   255: astore_1
    //   256: aload_1
    //   257: checkcast 90	com/google/f/t$a
    //   260: astore_1
    //   261: goto +8 -> 269
    //   264: iconst_0
    //   265: istore 5
    //   267: aconst_null
    //   268: astore_1
    //   269: invokestatic 93	com/google/f/t:parser	()Lcom/google/f/ah;
    //   272: astore 12
    //   274: aload_2
    //   275: aload 12
    //   277: aload_3
    //   278: invokevirtual 97	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   281: astore 12
    //   283: aload_0
    //   284: aload 12
    //   286: putfield 35	com/truecaller/api/services/messenger/v1/models/input/InputNotificationScope:b	Ljava/lang/Object;
    //   289: aload_1
    //   290: ifnull +33 -> 323
    //   293: aload_0
    //   294: getfield 35	com/truecaller/api/services/messenger/v1/models/input/InputNotificationScope:b	Ljava/lang/Object;
    //   297: astore 12
    //   299: aload 12
    //   301: checkcast 87	com/google/f/t
    //   304: astore 12
    //   306: aload_1
    //   307: aload 12
    //   309: invokevirtual 101	com/google/f/t$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   312: pop
    //   313: aload_1
    //   314: invokevirtual 105	com/google/f/t$a:buildPartial	()Lcom/google/f/q;
    //   317: astore_1
    //   318: aload_0
    //   319: aload_1
    //   320: putfield 35	com/truecaller/api/services/messenger/v1/models/input/InputNotificationScope:b	Ljava/lang/Object;
    //   323: aload_0
    //   324: iload 10
    //   326: putfield 26	com/truecaller/api/services/messenger/v1/models/input/InputNotificationScope:a	I
    //   329: goto -177 -> 152
    //   332: iconst_1
    //   333: istore 9
    //   335: goto -183 -> 152
    //   338: astore_1
    //   339: goto +53 -> 392
    //   342: astore_1
    //   343: new 107	java/lang/RuntimeException
    //   346: astore_2
    //   347: new 109	com/google/f/x
    //   350: astore_3
    //   351: aload_1
    //   352: invokevirtual 114	java/io/IOException:getMessage	()Ljava/lang/String;
    //   355: astore_1
    //   356: aload_3
    //   357: aload_1
    //   358: invokespecial 117	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   361: aload_3
    //   362: aload_0
    //   363: invokevirtual 121	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   366: astore_1
    //   367: aload_2
    //   368: aload_1
    //   369: invokespecial 124	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   372: aload_2
    //   373: athrow
    //   374: astore_1
    //   375: new 107	java/lang/RuntimeException
    //   378: astore_2
    //   379: aload_1
    //   380: aload_0
    //   381: invokevirtual 121	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   384: astore_1
    //   385: aload_2
    //   386: aload_1
    //   387: invokespecial 124	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   390: aload_2
    //   391: athrow
    //   392: aload_1
    //   393: athrow
    //   394: getstatic 20	com/truecaller/api/services/messenger/v1/models/input/InputNotificationScope:c	Lcom/truecaller/api/services/messenger/v1/models/input/InputNotificationScope;
    //   397: areturn
    //   398: aload_2
    //   399: checkcast 126	com/google/f/q$k
    //   402: astore_2
    //   403: aload_3
    //   404: checkcast 2	com/truecaller/api/services/messenger/v1/models/input/InputNotificationScope
    //   407: astore_3
    //   408: getstatic 128	com/truecaller/api/services/messenger/v1/models/input/InputNotificationScope$1:a	[I
    //   411: astore_1
    //   412: aload_3
    //   413: getfield 26	com/truecaller/api/services/messenger/v1/models/input/InputNotificationScope:a	I
    //   416: invokestatic 134	com/truecaller/api/services/messenger/v1/models/input/InputNotificationScope$ValueCase:forNumber	(I)Lcom/truecaller/api/services/messenger/v1/models/input/InputNotificationScope$ValueCase;
    //   419: astore 8
    //   421: aload 8
    //   423: invokevirtual 135	com/truecaller/api/services/messenger/v1/models/input/InputNotificationScope$ValueCase:ordinal	()I
    //   426: istore 7
    //   428: aload_1
    //   429: iload 7
    //   431: iaload
    //   432: istore 5
    //   434: iload 5
    //   436: tableswitch	default:+28->464, 1:+103->539, 2:+56->492, 3:+31->467
    //   464: goto +119 -> 583
    //   467: aload_0
    //   468: getfield 26	com/truecaller/api/services/messenger/v1/models/input/InputNotificationScope:a	I
    //   471: istore 5
    //   473: iload 5
    //   475: ifeq +6 -> 481
    //   478: iconst_1
    //   479: istore 9
    //   481: aload_2
    //   482: iload 9
    //   484: invokeinterface 139 2 0
    //   489: goto +94 -> 583
    //   492: aload_0
    //   493: getfield 26	com/truecaller/api/services/messenger/v1/models/input/InputNotificationScope:a	I
    //   496: istore 5
    //   498: iload 5
    //   500: iload 6
    //   502: if_icmpne +6 -> 508
    //   505: iconst_1
    //   506: istore 9
    //   508: aload_0
    //   509: getfield 35	com/truecaller/api/services/messenger/v1/models/input/InputNotificationScope:b	Ljava/lang/Object;
    //   512: astore_1
    //   513: aload_3
    //   514: getfield 35	com/truecaller/api/services/messenger/v1/models/input/InputNotificationScope:b	Ljava/lang/Object;
    //   517: astore 4
    //   519: aload_2
    //   520: iload 9
    //   522: aload_1
    //   523: aload 4
    //   525: invokeinterface 143 4 0
    //   530: astore_1
    //   531: aload_0
    //   532: aload_1
    //   533: putfield 35	com/truecaller/api/services/messenger/v1/models/input/InputNotificationScope:b	Ljava/lang/Object;
    //   536: goto +47 -> 583
    //   539: aload_0
    //   540: getfield 26	com/truecaller/api/services/messenger/v1/models/input/InputNotificationScope:a	I
    //   543: istore 5
    //   545: iload 5
    //   547: iload 10
    //   549: if_icmpne +6 -> 555
    //   552: iconst_1
    //   553: istore 9
    //   555: aload_0
    //   556: getfield 35	com/truecaller/api/services/messenger/v1/models/input/InputNotificationScope:b	Ljava/lang/Object;
    //   559: astore_1
    //   560: aload_3
    //   561: getfield 35	com/truecaller/api/services/messenger/v1/models/input/InputNotificationScope:b	Ljava/lang/Object;
    //   564: astore 4
    //   566: aload_2
    //   567: iload 9
    //   569: aload_1
    //   570: aload 4
    //   572: invokeinterface 146 4 0
    //   577: astore_1
    //   578: aload_0
    //   579: aload_1
    //   580: putfield 35	com/truecaller/api/services/messenger/v1/models/input/InputNotificationScope:b	Ljava/lang/Object;
    //   583: getstatic 152	com/google/f/q$i:INSTANCE	Lcom/google/f/q$i;
    //   586: astore_1
    //   587: aload_2
    //   588: aload_1
    //   589: if_acmpne +20 -> 609
    //   592: aload_3
    //   593: getfield 26	com/truecaller/api/services/messenger/v1/models/input/InputNotificationScope:a	I
    //   596: istore 5
    //   598: iload 5
    //   600: ifeq +9 -> 609
    //   603: aload_0
    //   604: iload 5
    //   606: putfield 26	com/truecaller/api/services/messenger/v1/models/input/InputNotificationScope:a	I
    //   609: aload_0
    //   610: areturn
    //   611: new 32	com/truecaller/api/services/messenger/v1/models/input/InputNotificationScope$a
    //   614: astore_1
    //   615: aload_1
    //   616: iconst_0
    //   617: invokespecial 155	com/truecaller/api/services/messenger/v1/models/input/InputNotificationScope$a:<init>	(B)V
    //   620: aload_1
    //   621: areturn
    //   622: aconst_null
    //   623: areturn
    //   624: getstatic 20	com/truecaller/api/services/messenger/v1/models/input/InputNotificationScope:c	Lcom/truecaller/api/services/messenger/v1/models/input/InputNotificationScope;
    //   627: areturn
    //   628: new 2	com/truecaller/api/services/messenger/v1/models/input/InputNotificationScope
    //   631: astore_1
    //   632: aload_1
    //   633: invokespecial 18	com/truecaller/api/services/messenger/v1/models/input/InputNotificationScope:<init>	()V
    //   636: aload_1
    //   637: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	638	0	this	InputNotificationScope
    //   0	638	1	paramj	com.google.f.q.j
    //   0	638	2	paramObject1	Object
    //   0	638	3	paramObject2	Object
    //   3	568	4	localObject1	Object
    //   9	183	5	i	int
    //   196	3	5	bool1	boolean
    //   232	373	5	j	int
    //   19	484	6	k	int
    //   22	408	7	m	int
    //   25	397	8	localValueCase	InputNotificationScope.ValueCase
    //   28	540	9	bool2	boolean
    //   31	519	10	n	int
    //   170	18	11	i1	int
    //   272	36	12	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   103	106	133	finally
    //   111	114	133	finally
    //   115	118	133	finally
    //   120	124	133	finally
    //   124	128	133	finally
    //   128	130	133	finally
    //   134	136	133	finally
    //   157	161	338	finally
    //   191	196	338	finally
    //   209	213	338	finally
    //   215	220	338	finally
    //   221	225	338	finally
    //   228	232	338	finally
    //   241	245	338	finally
    //   246	250	338	finally
    //   251	255	338	finally
    //   256	260	338	finally
    //   269	272	338	finally
    //   277	281	338	finally
    //   284	289	338	finally
    //   293	297	338	finally
    //   299	304	338	finally
    //   307	313	338	finally
    //   313	317	338	finally
    //   319	323	338	finally
    //   324	329	338	finally
    //   343	346	338	finally
    //   347	350	338	finally
    //   351	355	338	finally
    //   357	361	338	finally
    //   362	366	338	finally
    //   368	372	338	finally
    //   372	374	338	finally
    //   375	378	338	finally
    //   380	384	338	finally
    //   386	390	338	finally
    //   390	392	338	finally
    //   157	161	342	java/io/IOException
    //   191	196	342	java/io/IOException
    //   209	213	342	java/io/IOException
    //   215	220	342	java/io/IOException
    //   221	225	342	java/io/IOException
    //   228	232	342	java/io/IOException
    //   241	245	342	java/io/IOException
    //   246	250	342	java/io/IOException
    //   251	255	342	java/io/IOException
    //   256	260	342	java/io/IOException
    //   269	272	342	java/io/IOException
    //   277	281	342	java/io/IOException
    //   284	289	342	java/io/IOException
    //   293	297	342	java/io/IOException
    //   299	304	342	java/io/IOException
    //   307	313	342	java/io/IOException
    //   313	317	342	java/io/IOException
    //   319	323	342	java/io/IOException
    //   324	329	342	java/io/IOException
    //   157	161	374	com/google/f/x
    //   191	196	374	com/google/f/x
    //   209	213	374	com/google/f/x
    //   215	220	374	com/google/f/x
    //   221	225	374	com/google/f/x
    //   228	232	374	com/google/f/x
    //   241	245	374	com/google/f/x
    //   246	250	374	com/google/f/x
    //   251	255	374	com/google/f/x
    //   256	260	374	com/google/f/x
    //   269	272	374	com/google/f/x
    //   277	281	374	com/google/f/x
    //   284	289	374	com/google/f/x
    //   293	297	374	com/google/f/x
    //   299	304	374	com/google/f/x
    //   307	313	374	com/google/f/x
    //   313	317	374	com/google/f/x
    //   319	323	374	com/google/f/x
    //   324	329	374	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int j = -1;
    if (i != j) {
      return i;
    }
    i = a;
    j = 1;
    int k = 0;
    Object localObject;
    if (i == j)
    {
      localObject = (t)b;
      i = h.computeMessageSize(j, (ae)localObject);
      k = 0 + i;
    }
    i = a;
    j = 2;
    if (i == j)
    {
      localObject = e();
      i = h.computeStringSize(j, (String)localObject);
      k += i;
    }
    memoizedSerializedSize = k;
    return k;
  }
  
  public final void writeTo(h paramh)
  {
    int i = a;
    int j = 1;
    Object localObject;
    if (i == j)
    {
      localObject = (t)b;
      paramh.writeMessage(j, (ae)localObject);
    }
    i = a;
    j = 2;
    if (i == j)
    {
      localObject = e();
      paramh.writeString(j, (String)localObject);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.models.input.InputNotificationScope
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */