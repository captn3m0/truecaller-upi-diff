package com.truecaller.api.services.messenger.v1.models;

import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;

public final class MessageContent$f
  extends q
  implements MessageContent.g
{
  private static final f d;
  private static volatile ah e;
  private double a;
  private double b;
  private double c;
  
  static
  {
    f localf = new com/truecaller/api/services/messenger/v1/models/MessageContent$f;
    localf.<init>();
    d = localf;
    localf.makeImmutable();
  }
  
  public static ah a()
  {
    return d.getParserForType();
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 33	com/truecaller/api/services/messenger/v1/models/MessageContent$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 39	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_0
    //   19: istore 6
    //   21: iload 5
    //   23: tableswitch	default:+45->68, 1:+598->621, 2:+594->617, 3:+592->615, 4:+581->604, 5:+291->314, 6:+107->130, 7:+287->310, 8:+55->78
    //   68: new 42	java/lang/UnsupportedOperationException
    //   71: astore_1
    //   72: aload_1
    //   73: invokespecial 43	java/lang/UnsupportedOperationException:<init>	()V
    //   76: aload_1
    //   77: athrow
    //   78: getstatic 45	com/truecaller/api/services/messenger/v1/models/MessageContent$f:e	Lcom/google/f/ah;
    //   81: astore_1
    //   82: aload_1
    //   83: ifnonnull +43 -> 126
    //   86: ldc 2
    //   88: astore_1
    //   89: aload_1
    //   90: monitorenter
    //   91: getstatic 45	com/truecaller/api/services/messenger/v1/models/MessageContent$f:e	Lcom/google/f/ah;
    //   94: astore_2
    //   95: aload_2
    //   96: ifnonnull +20 -> 116
    //   99: new 47	com/google/f/q$b
    //   102: astore_2
    //   103: getstatic 20	com/truecaller/api/services/messenger/v1/models/MessageContent$f:d	Lcom/truecaller/api/services/messenger/v1/models/MessageContent$f;
    //   106: astore_3
    //   107: aload_2
    //   108: aload_3
    //   109: invokespecial 50	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   112: aload_2
    //   113: putstatic 45	com/truecaller/api/services/messenger/v1/models/MessageContent$f:e	Lcom/google/f/ah;
    //   116: aload_1
    //   117: monitorexit
    //   118: goto +8 -> 126
    //   121: astore_2
    //   122: aload_1
    //   123: monitorexit
    //   124: aload_2
    //   125: athrow
    //   126: getstatic 45	com/truecaller/api/services/messenger/v1/models/MessageContent$f:e	Lcom/google/f/ah;
    //   129: areturn
    //   130: aload_2
    //   131: checkcast 52	com/google/f/g
    //   134: astore_2
    //   135: iload 6
    //   137: ifne +173 -> 310
    //   140: aload_2
    //   141: invokevirtual 55	com/google/f/g:readTag	()I
    //   144: istore 5
    //   146: iload 5
    //   148: ifeq +100 -> 248
    //   151: bipush 9
    //   153: istore 7
    //   155: iload 5
    //   157: iload 7
    //   159: if_icmpeq +74 -> 233
    //   162: bipush 17
    //   164: istore 7
    //   166: iload 5
    //   168: iload 7
    //   170: if_icmpeq +48 -> 218
    //   173: bipush 25
    //   175: istore 7
    //   177: iload 5
    //   179: iload 7
    //   181: if_icmpeq +22 -> 203
    //   184: aload_2
    //   185: iload 5
    //   187: invokevirtual 62	com/google/f/g:skipField	(I)Z
    //   190: istore 5
    //   192: iload 5
    //   194: ifne -59 -> 135
    //   197: iconst_1
    //   198: istore 6
    //   200: goto -65 -> 135
    //   203: aload_2
    //   204: invokevirtual 66	com/google/f/g:readDouble	()D
    //   207: dstore 8
    //   209: aload_0
    //   210: dload 8
    //   212: putfield 68	com/truecaller/api/services/messenger/v1/models/MessageContent$f:c	D
    //   215: goto -80 -> 135
    //   218: aload_2
    //   219: invokevirtual 66	com/google/f/g:readDouble	()D
    //   222: dstore 8
    //   224: aload_0
    //   225: dload 8
    //   227: putfield 70	com/truecaller/api/services/messenger/v1/models/MessageContent$f:b	D
    //   230: goto -95 -> 135
    //   233: aload_2
    //   234: invokevirtual 66	com/google/f/g:readDouble	()D
    //   237: dstore 8
    //   239: aload_0
    //   240: dload 8
    //   242: putfield 72	com/truecaller/api/services/messenger/v1/models/MessageContent$f:a	D
    //   245: goto -110 -> 135
    //   248: iconst_1
    //   249: istore 6
    //   251: goto -116 -> 135
    //   254: astore_1
    //   255: goto +53 -> 308
    //   258: astore_1
    //   259: new 74	java/lang/RuntimeException
    //   262: astore_2
    //   263: new 76	com/google/f/x
    //   266: astore_3
    //   267: aload_1
    //   268: invokevirtual 82	java/io/IOException:getMessage	()Ljava/lang/String;
    //   271: astore_1
    //   272: aload_3
    //   273: aload_1
    //   274: invokespecial 85	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   277: aload_3
    //   278: aload_0
    //   279: invokevirtual 89	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   282: astore_1
    //   283: aload_2
    //   284: aload_1
    //   285: invokespecial 92	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   288: aload_2
    //   289: athrow
    //   290: astore_1
    //   291: new 74	java/lang/RuntimeException
    //   294: astore_2
    //   295: aload_1
    //   296: aload_0
    //   297: invokevirtual 89	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   300: astore_1
    //   301: aload_2
    //   302: aload_1
    //   303: invokespecial 92	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   306: aload_2
    //   307: athrow
    //   308: aload_1
    //   309: athrow
    //   310: getstatic 20	com/truecaller/api/services/messenger/v1/models/MessageContent$f:d	Lcom/truecaller/api/services/messenger/v1/models/MessageContent$f;
    //   313: areturn
    //   314: aload_2
    //   315: astore_1
    //   316: aload_2
    //   317: checkcast 94	com/google/f/q$k
    //   320: astore_1
    //   321: aload_3
    //   322: checkcast 2	com/truecaller/api/services/messenger/v1/models/MessageContent$f
    //   325: astore_3
    //   326: aload_0
    //   327: getfield 72	com/truecaller/api/services/messenger/v1/models/MessageContent$f:a	D
    //   330: dstore 8
    //   332: dconst_0
    //   333: dstore 10
    //   335: dload 8
    //   337: dload 10
    //   339: dcmpl
    //   340: istore 12
    //   342: iload 12
    //   344: ifeq +9 -> 353
    //   347: iconst_1
    //   348: istore 13
    //   350: goto +6 -> 356
    //   353: iconst_0
    //   354: istore 13
    //   356: aload_0
    //   357: getfield 72	com/truecaller/api/services/messenger/v1/models/MessageContent$f:a	D
    //   360: dstore 14
    //   362: aload_3
    //   363: getfield 72	com/truecaller/api/services/messenger/v1/models/MessageContent$f:a	D
    //   366: dstore 16
    //   368: dload 16
    //   370: dload 10
    //   372: dcmpl
    //   373: istore 12
    //   375: iload 12
    //   377: ifeq +9 -> 386
    //   380: iconst_1
    //   381: istore 18
    //   383: goto +6 -> 389
    //   386: iconst_0
    //   387: istore 18
    //   389: aload_3
    //   390: getfield 72	com/truecaller/api/services/messenger/v1/models/MessageContent$f:a	D
    //   393: dstore 19
    //   395: aload_1
    //   396: iload 13
    //   398: dload 14
    //   400: iload 18
    //   402: dload 19
    //   404: invokeinterface 98 7 0
    //   409: dstore 8
    //   411: aload_0
    //   412: dload 8
    //   414: putfield 72	com/truecaller/api/services/messenger/v1/models/MessageContent$f:a	D
    //   417: aload_0
    //   418: getfield 70	com/truecaller/api/services/messenger/v1/models/MessageContent$f:b	D
    //   421: dstore 8
    //   423: dload 8
    //   425: dload 10
    //   427: dcmpl
    //   428: istore 12
    //   430: iload 12
    //   432: ifeq +9 -> 441
    //   435: iconst_1
    //   436: istore 13
    //   438: goto +6 -> 444
    //   441: iconst_0
    //   442: istore 13
    //   444: aload_0
    //   445: getfield 70	com/truecaller/api/services/messenger/v1/models/MessageContent$f:b	D
    //   448: dstore 14
    //   450: aload_3
    //   451: getfield 70	com/truecaller/api/services/messenger/v1/models/MessageContent$f:b	D
    //   454: dstore 16
    //   456: dload 16
    //   458: dload 10
    //   460: dcmpl
    //   461: istore 12
    //   463: iload 12
    //   465: ifeq +9 -> 474
    //   468: iconst_1
    //   469: istore 18
    //   471: goto +6 -> 477
    //   474: iconst_0
    //   475: istore 18
    //   477: aload_3
    //   478: getfield 70	com/truecaller/api/services/messenger/v1/models/MessageContent$f:b	D
    //   481: dstore 19
    //   483: aload_1
    //   484: iload 13
    //   486: dload 14
    //   488: iload 18
    //   490: dload 19
    //   492: invokeinterface 98 7 0
    //   497: dstore 8
    //   499: aload_0
    //   500: dload 8
    //   502: putfield 70	com/truecaller/api/services/messenger/v1/models/MessageContent$f:b	D
    //   505: aload_0
    //   506: getfield 68	com/truecaller/api/services/messenger/v1/models/MessageContent$f:c	D
    //   509: dstore 8
    //   511: dload 8
    //   513: dload 10
    //   515: dcmpl
    //   516: istore 12
    //   518: iload 12
    //   520: ifeq +9 -> 529
    //   523: iconst_1
    //   524: istore 12
    //   526: goto +8 -> 534
    //   529: iconst_0
    //   530: istore 12
    //   532: aconst_null
    //   533: astore_2
    //   534: aload_0
    //   535: getfield 68	com/truecaller/api/services/messenger/v1/models/MessageContent$f:c	D
    //   538: dstore 8
    //   540: aload_3
    //   541: getfield 68	com/truecaller/api/services/messenger/v1/models/MessageContent$f:c	D
    //   544: dstore 14
    //   546: dload 14
    //   548: dload 10
    //   550: dcmpl
    //   551: istore 18
    //   553: iload 18
    //   555: ifeq +9 -> 564
    //   558: iconst_1
    //   559: istore 21
    //   561: goto +6 -> 567
    //   564: iconst_0
    //   565: istore 21
    //   567: aload_3
    //   568: getfield 68	com/truecaller/api/services/messenger/v1/models/MessageContent$f:c	D
    //   571: dstore 22
    //   573: aload_1
    //   574: astore 4
    //   576: iload 12
    //   578: istore 6
    //   580: aload_1
    //   581: iload 12
    //   583: dload 8
    //   585: iload 21
    //   587: dload 22
    //   589: invokeinterface 98 7 0
    //   594: dstore 24
    //   596: aload_0
    //   597: dload 24
    //   599: putfield 68	com/truecaller/api/services/messenger/v1/models/MessageContent$f:c	D
    //   602: aload_0
    //   603: areturn
    //   604: new 100	com/truecaller/api/services/messenger/v1/models/MessageContent$f$a
    //   607: astore_1
    //   608: aload_1
    //   609: iconst_0
    //   610: invokespecial 103	com/truecaller/api/services/messenger/v1/models/MessageContent$f$a:<init>	(B)V
    //   613: aload_1
    //   614: areturn
    //   615: aconst_null
    //   616: areturn
    //   617: getstatic 20	com/truecaller/api/services/messenger/v1/models/MessageContent$f:d	Lcom/truecaller/api/services/messenger/v1/models/MessageContent$f;
    //   620: areturn
    //   621: new 2	com/truecaller/api/services/messenger/v1/models/MessageContent$f
    //   624: astore_1
    //   625: aload_1
    //   626: invokespecial 18	com/truecaller/api/services/messenger/v1/models/MessageContent$f:<init>	()V
    //   629: aload_1
    //   630: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	631	0	this	f
    //   0	631	1	paramj	com.google.f.q.j
    //   0	631	2	paramObject1	Object
    //   0	631	3	paramObject2	Object
    //   3	572	4	localObject	Object
    //   9	177	5	i	int
    //   190	3	5	bool1	boolean
    //   19	560	6	j	int
    //   153	29	7	k	int
    //   207	377	8	d1	double
    //   333	216	10	d2	double
    //   340	242	12	bool2	boolean
    //   348	137	13	bool3	boolean
    //   360	187	14	d3	double
    //   366	91	16	d4	double
    //   381	173	18	bool4	boolean
    //   393	98	19	d5	double
    //   559	27	21	bool5	boolean
    //   571	17	22	d6	double
    //   594	4	24	d7	double
    // Exception table:
    //   from	to	target	type
    //   91	94	121	finally
    //   99	102	121	finally
    //   103	106	121	finally
    //   108	112	121	finally
    //   112	116	121	finally
    //   116	118	121	finally
    //   122	124	121	finally
    //   140	144	254	finally
    //   185	190	254	finally
    //   203	207	254	finally
    //   210	215	254	finally
    //   218	222	254	finally
    //   225	230	254	finally
    //   233	237	254	finally
    //   240	245	254	finally
    //   259	262	254	finally
    //   263	266	254	finally
    //   267	271	254	finally
    //   273	277	254	finally
    //   278	282	254	finally
    //   284	288	254	finally
    //   288	290	254	finally
    //   291	294	254	finally
    //   296	300	254	finally
    //   302	306	254	finally
    //   306	308	254	finally
    //   140	144	258	java/io/IOException
    //   185	190	258	java/io/IOException
    //   203	207	258	java/io/IOException
    //   210	215	258	java/io/IOException
    //   218	222	258	java/io/IOException
    //   225	230	258	java/io/IOException
    //   233	237	258	java/io/IOException
    //   240	245	258	java/io/IOException
    //   140	144	290	com/google/f/x
    //   185	190	290	com/google/f/x
    //   203	207	290	com/google/f/x
    //   210	215	290	com/google/f/x
    //   218	222	290	com/google/f/x
    //   225	230	290	com/google/f/x
    //   233	237	290	com/google/f/x
    //   240	245	290	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int j = -1;
    if (i != j) {
      return i;
    }
    double d1 = a;
    int k = 0;
    double d2 = 0.0D;
    int m = d1 < d2;
    if (m != 0)
    {
      m = 1;
      i = h.computeDoubleSize(m, d1);
      k = 0 + i;
    }
    d1 = b;
    boolean bool1 = d1 < d2;
    if (bool1)
    {
      int n = 2;
      i = h.computeDoubleSize(n, d1);
      k += i;
    }
    d1 = c;
    boolean bool2 = d1 < d2;
    if (bool2)
    {
      int i1 = 3;
      i = h.computeDoubleSize(i1, d1);
      k += i;
    }
    memoizedSerializedSize = k;
    return k;
  }
  
  public final void writeTo(h paramh)
  {
    double d1 = a;
    double d2 = 0.0D;
    int i = d1 < d2;
    if (i != 0)
    {
      i = 1;
      paramh.writeDouble(i, d1);
    }
    d1 = b;
    boolean bool1 = d1 < d2;
    if (bool1)
    {
      int j = 2;
      paramh.writeDouble(j, d1);
    }
    d1 = c;
    boolean bool2 = d1 < d2;
    if (bool2)
    {
      int k = 3;
      paramh.writeDouble(k, d1);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.models.MessageContent.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */