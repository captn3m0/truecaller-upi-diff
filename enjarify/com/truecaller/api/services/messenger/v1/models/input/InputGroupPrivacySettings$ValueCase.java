package com.truecaller.api.services.messenger.v1.models.input;

import com.google.f.w.c;

public enum InputGroupPrivacySettings$ValueCase
  implements w.c
{
  private final int value;
  
  static
  {
    Object localObject = new com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings$ValueCase;
    int i = 1;
    ((ValueCase)localObject).<init>("INVITE_ONLY", 0, i);
    INVITE_ONLY = (ValueCase)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings$ValueCase;
    ((ValueCase)localObject).<init>("VALUE_NOT_SET", i, 0);
    VALUE_NOT_SET = (ValueCase)localObject;
    localObject = new ValueCase[2];
    ValueCase localValueCase = INVITE_ONLY;
    localObject[0] = localValueCase;
    localValueCase = VALUE_NOT_SET;
    localObject[i] = localValueCase;
    $VALUES = (ValueCase[])localObject;
  }
  
  private InputGroupPrivacySettings$ValueCase(int paramInt1)
  {
    value = paramInt1;
  }
  
  public static ValueCase forNumber(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return null;
    case 1: 
      return INVITE_ONLY;
    }
    return VALUE_NOT_SET;
  }
  
  public static ValueCase valueOf(int paramInt)
  {
    return forNumber(paramInt);
  }
  
  public final int getNumber()
  {
    return value;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.models.input.InputGroupPrivacySettings.ValueCase
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */