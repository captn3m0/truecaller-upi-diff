package com.truecaller.api.services.messenger.v1.models;

import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;

public final class MessageContent$h
  extends q
  implements MessageContent.i
{
  private static final h c;
  private static volatile ah d;
  private String a = "";
  private int b;
  
  static
  {
    h localh = new com/truecaller/api/services/messenger/v1/models/MessageContent$h;
    localh.<init>();
    c = localh;
    localh.makeImmutable();
  }
  
  public static h c()
  {
    return c;
  }
  
  public static ah d()
  {
    return c.getParserForType();
  }
  
  public final String a()
  {
    return a;
  }
  
  public final int b()
  {
    return b;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 39	com/truecaller/api/services/messenger/v1/models/MessageContent$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 45	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_0
    //   19: istore 6
    //   21: aconst_null
    //   22: astore 4
    //   24: iconst_1
    //   25: istore 7
    //   27: iload 5
    //   29: tableswitch	default:+47->76, 1:+424->453, 2:+420->449, 3:+418->447, 4:+407->436, 5:+265->294, 6:+109->138, 7:+261->290, 8:+57->86
    //   76: new 48	java/lang/UnsupportedOperationException
    //   79: astore_1
    //   80: aload_1
    //   81: invokespecial 49	java/lang/UnsupportedOperationException:<init>	()V
    //   84: aload_1
    //   85: athrow
    //   86: getstatic 51	com/truecaller/api/services/messenger/v1/models/MessageContent$h:d	Lcom/google/f/ah;
    //   89: astore_1
    //   90: aload_1
    //   91: ifnonnull +43 -> 134
    //   94: ldc 2
    //   96: astore_1
    //   97: aload_1
    //   98: monitorenter
    //   99: getstatic 51	com/truecaller/api/services/messenger/v1/models/MessageContent$h:d	Lcom/google/f/ah;
    //   102: astore_2
    //   103: aload_2
    //   104: ifnonnull +20 -> 124
    //   107: new 53	com/google/f/q$b
    //   110: astore_2
    //   111: getstatic 20	com/truecaller/api/services/messenger/v1/models/MessageContent$h:c	Lcom/truecaller/api/services/messenger/v1/models/MessageContent$h;
    //   114: astore_3
    //   115: aload_2
    //   116: aload_3
    //   117: invokespecial 56	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   120: aload_2
    //   121: putstatic 51	com/truecaller/api/services/messenger/v1/models/MessageContent$h:d	Lcom/google/f/ah;
    //   124: aload_1
    //   125: monitorexit
    //   126: goto +8 -> 134
    //   129: astore_2
    //   130: aload_1
    //   131: monitorexit
    //   132: aload_2
    //   133: athrow
    //   134: getstatic 51	com/truecaller/api/services/messenger/v1/models/MessageContent$h:d	Lcom/google/f/ah;
    //   137: areturn
    //   138: aload_2
    //   139: checkcast 58	com/google/f/g
    //   142: astore_2
    //   143: iload 6
    //   145: ifne +145 -> 290
    //   148: aload_2
    //   149: invokevirtual 61	com/google/f/g:readTag	()I
    //   152: istore 5
    //   154: iload 5
    //   156: ifeq +72 -> 228
    //   159: bipush 10
    //   161: istore 8
    //   163: iload 5
    //   165: iload 8
    //   167: if_icmpeq +48 -> 215
    //   170: bipush 16
    //   172: istore 8
    //   174: iload 5
    //   176: iload 8
    //   178: if_icmpeq +22 -> 200
    //   181: aload_2
    //   182: iload 5
    //   184: invokevirtual 67	com/google/f/g:skipField	(I)Z
    //   187: istore 5
    //   189: iload 5
    //   191: ifne -48 -> 143
    //   194: iconst_1
    //   195: istore 6
    //   197: goto -54 -> 143
    //   200: aload_2
    //   201: invokevirtual 70	com/google/f/g:readInt32	()I
    //   204: istore 5
    //   206: aload_0
    //   207: iload 5
    //   209: putfield 34	com/truecaller/api/services/messenger/v1/models/MessageContent$h:b	I
    //   212: goto -69 -> 143
    //   215: aload_2
    //   216: invokevirtual 74	com/google/f/g:readStringRequireUtf8	()Ljava/lang/String;
    //   219: astore_1
    //   220: aload_0
    //   221: aload_1
    //   222: putfield 28	com/truecaller/api/services/messenger/v1/models/MessageContent$h:a	Ljava/lang/String;
    //   225: goto -82 -> 143
    //   228: iconst_1
    //   229: istore 6
    //   231: goto -88 -> 143
    //   234: astore_1
    //   235: goto +53 -> 288
    //   238: astore_1
    //   239: new 76	java/lang/RuntimeException
    //   242: astore_2
    //   243: new 78	com/google/f/x
    //   246: astore_3
    //   247: aload_1
    //   248: invokevirtual 83	java/io/IOException:getMessage	()Ljava/lang/String;
    //   251: astore_1
    //   252: aload_3
    //   253: aload_1
    //   254: invokespecial 86	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   257: aload_3
    //   258: aload_0
    //   259: invokevirtual 90	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   262: astore_1
    //   263: aload_2
    //   264: aload_1
    //   265: invokespecial 93	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   268: aload_2
    //   269: athrow
    //   270: astore_1
    //   271: new 76	java/lang/RuntimeException
    //   274: astore_2
    //   275: aload_1
    //   276: aload_0
    //   277: invokevirtual 90	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   280: astore_1
    //   281: aload_2
    //   282: aload_1
    //   283: invokespecial 93	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   286: aload_2
    //   287: athrow
    //   288: aload_1
    //   289: athrow
    //   290: getstatic 20	com/truecaller/api/services/messenger/v1/models/MessageContent$h:c	Lcom/truecaller/api/services/messenger/v1/models/MessageContent$h;
    //   293: areturn
    //   294: aload_2
    //   295: checkcast 95	com/google/f/q$k
    //   298: astore_2
    //   299: aload_3
    //   300: checkcast 2	com/truecaller/api/services/messenger/v1/models/MessageContent$h
    //   303: astore_3
    //   304: aload_0
    //   305: getfield 28	com/truecaller/api/services/messenger/v1/models/MessageContent$h:a	Ljava/lang/String;
    //   308: invokevirtual 101	java/lang/String:isEmpty	()Z
    //   311: iload 7
    //   313: ixor
    //   314: istore 5
    //   316: aload_0
    //   317: getfield 28	com/truecaller/api/services/messenger/v1/models/MessageContent$h:a	Ljava/lang/String;
    //   320: astore 9
    //   322: aload_3
    //   323: getfield 28	com/truecaller/api/services/messenger/v1/models/MessageContent$h:a	Ljava/lang/String;
    //   326: astore 10
    //   328: aload 10
    //   330: invokevirtual 101	java/lang/String:isEmpty	()Z
    //   333: iload 7
    //   335: ixor
    //   336: istore 11
    //   338: aload_3
    //   339: getfield 28	com/truecaller/api/services/messenger/v1/models/MessageContent$h:a	Ljava/lang/String;
    //   342: astore 12
    //   344: aload_2
    //   345: iload 5
    //   347: aload 9
    //   349: iload 11
    //   351: aload 12
    //   353: invokeinterface 105 5 0
    //   358: astore_1
    //   359: aload_0
    //   360: aload_1
    //   361: putfield 28	com/truecaller/api/services/messenger/v1/models/MessageContent$h:a	Ljava/lang/String;
    //   364: aload_0
    //   365: getfield 34	com/truecaller/api/services/messenger/v1/models/MessageContent$h:b	I
    //   368: istore 5
    //   370: iload 5
    //   372: ifeq +9 -> 381
    //   375: iconst_1
    //   376: istore 5
    //   378: goto +8 -> 386
    //   381: iconst_0
    //   382: istore 5
    //   384: aconst_null
    //   385: astore_1
    //   386: aload_0
    //   387: getfield 34	com/truecaller/api/services/messenger/v1/models/MessageContent$h:b	I
    //   390: istore 13
    //   392: aload_3
    //   393: getfield 34	com/truecaller/api/services/messenger/v1/models/MessageContent$h:b	I
    //   396: istore 11
    //   398: iload 11
    //   400: ifeq +6 -> 406
    //   403: iconst_1
    //   404: istore 6
    //   406: aload_3
    //   407: getfield 34	com/truecaller/api/services/messenger/v1/models/MessageContent$h:b	I
    //   410: istore 8
    //   412: aload_2
    //   413: iload 5
    //   415: iload 13
    //   417: iload 6
    //   419: iload 8
    //   421: invokeinterface 109 5 0
    //   426: istore 5
    //   428: aload_0
    //   429: iload 5
    //   431: putfield 34	com/truecaller/api/services/messenger/v1/models/MessageContent$h:b	I
    //   434: aload_0
    //   435: areturn
    //   436: new 111	com/truecaller/api/services/messenger/v1/models/MessageContent$h$a
    //   439: astore_1
    //   440: aload_1
    //   441: iconst_0
    //   442: invokespecial 114	com/truecaller/api/services/messenger/v1/models/MessageContent$h$a:<init>	(B)V
    //   445: aload_1
    //   446: areturn
    //   447: aconst_null
    //   448: areturn
    //   449: getstatic 20	com/truecaller/api/services/messenger/v1/models/MessageContent$h:c	Lcom/truecaller/api/services/messenger/v1/models/MessageContent$h;
    //   452: areturn
    //   453: new 2	com/truecaller/api/services/messenger/v1/models/MessageContent$h
    //   456: astore_1
    //   457: aload_1
    //   458: invokespecial 18	com/truecaller/api/services/messenger/v1/models/MessageContent$h:<init>	()V
    //   461: aload_1
    //   462: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	463	0	this	h
    //   0	463	1	paramj	com.google.f.q.j
    //   0	463	2	paramObject1	Object
    //   0	463	3	paramObject2	Object
    //   3	20	4	arrayOfInt	int[]
    //   9	174	5	i	int
    //   187	3	5	bool1	boolean
    //   204	4	5	j	int
    //   314	32	5	bool2	boolean
    //   368	46	5	k	int
    //   426	4	5	m	int
    //   19	399	6	bool3	boolean
    //   25	311	7	n	int
    //   161	259	8	i1	int
    //   320	28	9	str1	String
    //   326	3	10	str2	String
    //   336	14	11	bool4	boolean
    //   396	3	11	i2	int
    //   342	10	12	str3	String
    //   390	26	13	i3	int
    // Exception table:
    //   from	to	target	type
    //   99	102	129	finally
    //   107	110	129	finally
    //   111	114	129	finally
    //   116	120	129	finally
    //   120	124	129	finally
    //   124	126	129	finally
    //   130	132	129	finally
    //   148	152	234	finally
    //   182	187	234	finally
    //   200	204	234	finally
    //   207	212	234	finally
    //   215	219	234	finally
    //   221	225	234	finally
    //   239	242	234	finally
    //   243	246	234	finally
    //   247	251	234	finally
    //   253	257	234	finally
    //   258	262	234	finally
    //   264	268	234	finally
    //   268	270	234	finally
    //   271	274	234	finally
    //   276	280	234	finally
    //   282	286	234	finally
    //   286	288	234	finally
    //   148	152	238	java/io/IOException
    //   182	187	238	java/io/IOException
    //   200	204	238	java/io/IOException
    //   207	212	238	java/io/IOException
    //   215	219	238	java/io/IOException
    //   221	225	238	java/io/IOException
    //   148	152	270	com/google/f/x
    //   182	187	270	com/google/f/x
    //   200	204	270	com/google/f/x
    //   207	212	270	com/google/f/x
    //   215	219	270	com/google/f/x
    //   221	225	270	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int k = -1;
    if (i != k) {
      return i;
    }
    String str1 = a;
    boolean bool = str1.isEmpty();
    k = 0;
    if (!bool)
    {
      String str2 = a;
      j = h.computeStringSize(1, str2);
      k = 0 + j;
    }
    int j = b;
    if (j != 0)
    {
      int m = 2;
      j = h.computeInt32Size(m, j);
      k += j;
    }
    memoizedSerializedSize = k;
    return k;
  }
  
  public final void writeTo(h paramh)
  {
    String str1 = a;
    int i = str1.isEmpty();
    if (i == 0)
    {
      i = 1;
      String str2 = a;
      paramh.writeString(i, str2);
    }
    int j = b;
    if (j != 0)
    {
      int k = 2;
      paramh.writeInt32(k, j);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.models.MessageContent.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */