package com.truecaller.api.services.messenger.v1.models.input;

import com.google.f.q.a;

public final class InputMessageContent$c
  extends q.a
  implements f
{
  private InputMessageContent$c()
  {
    super(localInputMessageContent);
  }
  
  public final c a(InputMessageContent.a parama)
  {
    copyOnWrite();
    InputMessageContent.a((InputMessageContent)instance, parama);
    return this;
  }
  
  public final c a(InputMessageContent.d paramd)
  {
    copyOnWrite();
    InputMessageContent.a((InputMessageContent)instance, paramd);
    return this;
  }
  
  public final c a(InputMessageContent.h paramh)
  {
    copyOnWrite();
    InputMessageContent.a((InputMessageContent)instance, paramh);
    return this;
  }
  
  public final c a(InputMessageContent.j paramj)
  {
    copyOnWrite();
    InputMessageContent.a((InputMessageContent)instance, paramj);
    return this;
  }
  
  public final c a(String paramString)
  {
    copyOnWrite();
    InputMessageContent.a((InputMessageContent)instance, paramString);
    return this;
  }
  
  public final c b(String paramString)
  {
    copyOnWrite();
    InputMessageContent.b((InputMessageContent)instance, paramString);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.models.input.InputMessageContent.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */