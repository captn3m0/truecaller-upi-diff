package com.truecaller.api.services.messenger.v1.models;

import com.google.f.w.c;
import com.google.f.w.d;

public enum Role
  implements w.c
{
  public static final int ADMIN_VALUE = 536870912;
  public static final int INVITEE_VALUE = 2;
  public static final int NO_ROLE_VALUE = 0;
  public static final int SUPER_ADMIN_VALUE = 1073741824;
  public static final int USER_VALUE = 8;
  private static final w.d internalValueMap;
  private final int value;
  
  static
  {
    Object localObject = new com/truecaller/api/services/messenger/v1/models/Role;
    ((Role)localObject).<init>("NO_ROLE", 0, 0);
    NO_ROLE = (Role)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/models/Role;
    int i = 1;
    int j = 2;
    ((Role)localObject).<init>("INVITEE", i, j);
    INVITEE = (Role)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/models/Role;
    ((Role)localObject).<init>("USER", j, 8);
    USER = (Role)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/models/Role;
    int k = 3;
    ((Role)localObject).<init>("ADMIN", k, 536870912);
    ADMIN = (Role)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/models/Role;
    int m = 4;
    ((Role)localObject).<init>("SUPER_ADMIN", m, 1073741824);
    SUPER_ADMIN = (Role)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/models/Role;
    int n = 5;
    ((Role)localObject).<init>("UNRECOGNIZED", n, -1);
    UNRECOGNIZED = (Role)localObject;
    localObject = new Role[6];
    Role localRole = NO_ROLE;
    localObject[0] = localRole;
    localRole = INVITEE;
    localObject[i] = localRole;
    localRole = USER;
    localObject[j] = localRole;
    localRole = ADMIN;
    localObject[k] = localRole;
    localRole = SUPER_ADMIN;
    localObject[m] = localRole;
    localRole = UNRECOGNIZED;
    localObject[n] = localRole;
    $VALUES = (Role[])localObject;
    localObject = new com/truecaller/api/services/messenger/v1/models/Role$1;
    ((Role.1)localObject).<init>();
    internalValueMap = (w.d)localObject;
  }
  
  private Role(int paramInt1)
  {
    value = paramInt1;
  }
  
  public static Role forNumber(int paramInt)
  {
    if (paramInt != 0)
    {
      int i = 2;
      if (paramInt != i)
      {
        i = 8;
        if (paramInt != i)
        {
          i = 536870912;
          if (paramInt != i)
          {
            i = 1073741824;
            if (paramInt != i) {
              return null;
            }
            return SUPER_ADMIN;
          }
          return ADMIN;
        }
        return USER;
      }
      return INVITEE;
    }
    return NO_ROLE;
  }
  
  public static w.d internalGetValueMap()
  {
    return internalValueMap;
  }
  
  public static Role valueOf(int paramInt)
  {
    return forNumber(paramInt);
  }
  
  public final int getNumber()
  {
    return value;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.models.Role
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */