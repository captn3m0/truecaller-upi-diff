package com.truecaller.api.services.messenger.v1.models;

import com.google.f.ae;
import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;
import com.google.f.t;

public final class Peer$d
  extends q
  implements Peer.e
{
  private static final d c;
  private static volatile ah d;
  private String a = "";
  private t b;
  
  static
  {
    d locald = new com/truecaller/api/services/messenger/v1/models/Peer$d;
    locald.<init>();
    c = locald;
    locald.makeImmutable();
  }
  
  public static d d()
  {
    return c;
  }
  
  public static ah e()
  {
    return c.getParserForType();
  }
  
  public final String a()
  {
    return a;
  }
  
  public final boolean b()
  {
    t localt = b;
    return localt != null;
  }
  
  public final t c()
  {
    t localt = b;
    if (localt == null) {
      localt = t.getDefaultInstance();
    }
    return localt;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 46	com/truecaller/api/services/messenger/v1/models/Peer$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 52	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_0
    //   19: istore 6
    //   21: aconst_null
    //   22: astore 4
    //   24: iconst_0
    //   25: istore 7
    //   27: iconst_1
    //   28: istore 8
    //   30: iload 5
    //   32: tableswitch	default:+48->80, 1:+462->494, 2:+458->490, 3:+456->488, 4:+445->477, 5:+350->382, 6:+110->142, 7:+346->378, 8:+58->90
    //   80: new 54	java/lang/UnsupportedOperationException
    //   83: astore_1
    //   84: aload_1
    //   85: invokespecial 55	java/lang/UnsupportedOperationException:<init>	()V
    //   88: aload_1
    //   89: athrow
    //   90: getstatic 57	com/truecaller/api/services/messenger/v1/models/Peer$d:d	Lcom/google/f/ah;
    //   93: astore_1
    //   94: aload_1
    //   95: ifnonnull +43 -> 138
    //   98: ldc 2
    //   100: astore_1
    //   101: aload_1
    //   102: monitorenter
    //   103: getstatic 57	com/truecaller/api/services/messenger/v1/models/Peer$d:d	Lcom/google/f/ah;
    //   106: astore_2
    //   107: aload_2
    //   108: ifnonnull +20 -> 128
    //   111: new 59	com/google/f/q$b
    //   114: astore_2
    //   115: getstatic 20	com/truecaller/api/services/messenger/v1/models/Peer$d:c	Lcom/truecaller/api/services/messenger/v1/models/Peer$d;
    //   118: astore_3
    //   119: aload_2
    //   120: aload_3
    //   121: invokespecial 62	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   124: aload_2
    //   125: putstatic 57	com/truecaller/api/services/messenger/v1/models/Peer$d:d	Lcom/google/f/ah;
    //   128: aload_1
    //   129: monitorexit
    //   130: goto +8 -> 138
    //   133: astore_2
    //   134: aload_1
    //   135: monitorexit
    //   136: aload_2
    //   137: athrow
    //   138: getstatic 57	com/truecaller/api/services/messenger/v1/models/Peer$d:d	Lcom/google/f/ah;
    //   141: areturn
    //   142: aload_2
    //   143: checkcast 64	com/google/f/g
    //   146: astore_2
    //   147: aload_3
    //   148: checkcast 66	com/google/f/n
    //   151: astore_3
    //   152: iload 6
    //   154: ifne +224 -> 378
    //   157: aload_2
    //   158: invokevirtual 69	com/google/f/g:readTag	()I
    //   161: istore 5
    //   163: iload 5
    //   165: ifeq +151 -> 316
    //   168: bipush 10
    //   170: istore 9
    //   172: iload 5
    //   174: iload 9
    //   176: if_icmpeq +127 -> 303
    //   179: bipush 18
    //   181: istore 9
    //   183: iload 5
    //   185: iload 9
    //   187: if_icmpeq +22 -> 209
    //   190: aload_2
    //   191: iload 5
    //   193: invokevirtual 75	com/google/f/g:skipField	(I)Z
    //   196: istore 5
    //   198: iload 5
    //   200: ifne -48 -> 152
    //   203: iconst_1
    //   204: istore 6
    //   206: goto -54 -> 152
    //   209: aload_0
    //   210: getfield 34	com/truecaller/api/services/messenger/v1/models/Peer$d:b	Lcom/google/f/t;
    //   213: astore_1
    //   214: aload_1
    //   215: ifnull +21 -> 236
    //   218: aload_0
    //   219: getfield 34	com/truecaller/api/services/messenger/v1/models/Peer$d:b	Lcom/google/f/t;
    //   222: astore_1
    //   223: aload_1
    //   224: invokevirtual 79	com/google/f/t:toBuilder	()Lcom/google/f/q$a;
    //   227: astore_1
    //   228: aload_1
    //   229: checkcast 81	com/google/f/t$a
    //   232: astore_1
    //   233: goto +8 -> 241
    //   236: iconst_0
    //   237: istore 5
    //   239: aconst_null
    //   240: astore_1
    //   241: invokestatic 84	com/google/f/t:parser	()Lcom/google/f/ah;
    //   244: astore 10
    //   246: aload_2
    //   247: aload 10
    //   249: aload_3
    //   250: invokevirtual 88	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   253: astore 10
    //   255: aload 10
    //   257: checkcast 37	com/google/f/t
    //   260: astore 10
    //   262: aload_0
    //   263: aload 10
    //   265: putfield 34	com/truecaller/api/services/messenger/v1/models/Peer$d:b	Lcom/google/f/t;
    //   268: aload_1
    //   269: ifnull -117 -> 152
    //   272: aload_0
    //   273: getfield 34	com/truecaller/api/services/messenger/v1/models/Peer$d:b	Lcom/google/f/t;
    //   276: astore 10
    //   278: aload_1
    //   279: aload 10
    //   281: invokevirtual 92	com/google/f/t$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   284: pop
    //   285: aload_1
    //   286: invokevirtual 96	com/google/f/t$a:buildPartial	()Lcom/google/f/q;
    //   289: astore_1
    //   290: aload_1
    //   291: checkcast 37	com/google/f/t
    //   294: astore_1
    //   295: aload_0
    //   296: aload_1
    //   297: putfield 34	com/truecaller/api/services/messenger/v1/models/Peer$d:b	Lcom/google/f/t;
    //   300: goto -148 -> 152
    //   303: aload_2
    //   304: invokevirtual 100	com/google/f/g:readStringRequireUtf8	()Ljava/lang/String;
    //   307: astore_1
    //   308: aload_0
    //   309: aload_1
    //   310: putfield 28	com/truecaller/api/services/messenger/v1/models/Peer$d:a	Ljava/lang/String;
    //   313: goto -161 -> 152
    //   316: iconst_1
    //   317: istore 6
    //   319: goto -167 -> 152
    //   322: astore_1
    //   323: goto +53 -> 376
    //   326: astore_1
    //   327: new 102	java/lang/RuntimeException
    //   330: astore_2
    //   331: new 104	com/google/f/x
    //   334: astore_3
    //   335: aload_1
    //   336: invokevirtual 109	java/io/IOException:getMessage	()Ljava/lang/String;
    //   339: astore_1
    //   340: aload_3
    //   341: aload_1
    //   342: invokespecial 112	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   345: aload_3
    //   346: aload_0
    //   347: invokevirtual 116	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   350: astore_1
    //   351: aload_2
    //   352: aload_1
    //   353: invokespecial 119	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   356: aload_2
    //   357: athrow
    //   358: astore_1
    //   359: new 102	java/lang/RuntimeException
    //   362: astore_2
    //   363: aload_1
    //   364: aload_0
    //   365: invokevirtual 116	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   368: astore_1
    //   369: aload_2
    //   370: aload_1
    //   371: invokespecial 119	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   374: aload_2
    //   375: athrow
    //   376: aload_1
    //   377: athrow
    //   378: getstatic 20	com/truecaller/api/services/messenger/v1/models/Peer$d:c	Lcom/truecaller/api/services/messenger/v1/models/Peer$d;
    //   381: areturn
    //   382: aload_2
    //   383: checkcast 121	com/google/f/q$k
    //   386: astore_2
    //   387: aload_3
    //   388: checkcast 2	com/truecaller/api/services/messenger/v1/models/Peer$d
    //   391: astore_3
    //   392: aload_0
    //   393: getfield 28	com/truecaller/api/services/messenger/v1/models/Peer$d:a	Ljava/lang/String;
    //   396: invokevirtual 127	java/lang/String:isEmpty	()Z
    //   399: iload 8
    //   401: ixor
    //   402: istore 5
    //   404: aload_0
    //   405: getfield 28	com/truecaller/api/services/messenger/v1/models/Peer$d:a	Ljava/lang/String;
    //   408: astore 4
    //   410: aload_3
    //   411: getfield 28	com/truecaller/api/services/messenger/v1/models/Peer$d:a	Ljava/lang/String;
    //   414: invokevirtual 127	java/lang/String:isEmpty	()Z
    //   417: iload 8
    //   419: ixor
    //   420: istore 7
    //   422: aload_3
    //   423: getfield 28	com/truecaller/api/services/messenger/v1/models/Peer$d:a	Ljava/lang/String;
    //   426: astore 11
    //   428: aload_2
    //   429: iload 5
    //   431: aload 4
    //   433: iload 7
    //   435: aload 11
    //   437: invokeinterface 131 5 0
    //   442: astore_1
    //   443: aload_0
    //   444: aload_1
    //   445: putfield 28	com/truecaller/api/services/messenger/v1/models/Peer$d:a	Ljava/lang/String;
    //   448: aload_0
    //   449: getfield 34	com/truecaller/api/services/messenger/v1/models/Peer$d:b	Lcom/google/f/t;
    //   452: astore_1
    //   453: aload_3
    //   454: getfield 34	com/truecaller/api/services/messenger/v1/models/Peer$d:b	Lcom/google/f/t;
    //   457: astore_3
    //   458: aload_2
    //   459: aload_1
    //   460: aload_3
    //   461: invokeinterface 135 3 0
    //   466: checkcast 37	com/google/f/t
    //   469: astore_1
    //   470: aload_0
    //   471: aload_1
    //   472: putfield 34	com/truecaller/api/services/messenger/v1/models/Peer$d:b	Lcom/google/f/t;
    //   475: aload_0
    //   476: areturn
    //   477: new 137	com/truecaller/api/services/messenger/v1/models/Peer$d$a
    //   480: astore_1
    //   481: aload_1
    //   482: iconst_0
    //   483: invokespecial 140	com/truecaller/api/services/messenger/v1/models/Peer$d$a:<init>	(B)V
    //   486: aload_1
    //   487: areturn
    //   488: aconst_null
    //   489: areturn
    //   490: getstatic 20	com/truecaller/api/services/messenger/v1/models/Peer$d:c	Lcom/truecaller/api/services/messenger/v1/models/Peer$d;
    //   493: areturn
    //   494: new 2	com/truecaller/api/services/messenger/v1/models/Peer$d
    //   497: astore_1
    //   498: aload_1
    //   499: invokespecial 18	com/truecaller/api/services/messenger/v1/models/Peer$d:<init>	()V
    //   502: aload_1
    //   503: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	504	0	this	d
    //   0	504	1	paramj	com.google.f.q.j
    //   0	504	2	paramObject1	Object
    //   0	504	3	paramObject2	Object
    //   3	429	4	localObject1	Object
    //   9	183	5	i	int
    //   196	234	5	bool1	boolean
    //   19	299	6	j	int
    //   25	409	7	bool2	boolean
    //   28	392	8	k	int
    //   170	18	9	m	int
    //   244	36	10	localObject2	Object
    //   426	10	11	str	String
    // Exception table:
    //   from	to	target	type
    //   103	106	133	finally
    //   111	114	133	finally
    //   115	118	133	finally
    //   120	124	133	finally
    //   124	128	133	finally
    //   128	130	133	finally
    //   134	136	133	finally
    //   157	161	322	finally
    //   191	196	322	finally
    //   209	213	322	finally
    //   218	222	322	finally
    //   223	227	322	finally
    //   228	232	322	finally
    //   241	244	322	finally
    //   249	253	322	finally
    //   255	260	322	finally
    //   263	268	322	finally
    //   272	276	322	finally
    //   279	285	322	finally
    //   285	289	322	finally
    //   290	294	322	finally
    //   296	300	322	finally
    //   303	307	322	finally
    //   309	313	322	finally
    //   327	330	322	finally
    //   331	334	322	finally
    //   335	339	322	finally
    //   341	345	322	finally
    //   346	350	322	finally
    //   352	356	322	finally
    //   356	358	322	finally
    //   359	362	322	finally
    //   364	368	322	finally
    //   370	374	322	finally
    //   374	376	322	finally
    //   157	161	326	java/io/IOException
    //   191	196	326	java/io/IOException
    //   209	213	326	java/io/IOException
    //   218	222	326	java/io/IOException
    //   223	227	326	java/io/IOException
    //   228	232	326	java/io/IOException
    //   241	244	326	java/io/IOException
    //   249	253	326	java/io/IOException
    //   255	260	326	java/io/IOException
    //   263	268	326	java/io/IOException
    //   272	276	326	java/io/IOException
    //   279	285	326	java/io/IOException
    //   285	289	326	java/io/IOException
    //   290	294	326	java/io/IOException
    //   296	300	326	java/io/IOException
    //   303	307	326	java/io/IOException
    //   309	313	326	java/io/IOException
    //   157	161	358	com/google/f/x
    //   191	196	358	com/google/f/x
    //   209	213	358	com/google/f/x
    //   218	222	358	com/google/f/x
    //   223	227	358	com/google/f/x
    //   228	232	358	com/google/f/x
    //   241	244	358	com/google/f/x
    //   249	253	358	com/google/f/x
    //   255	260	358	com/google/f/x
    //   263	268	358	com/google/f/x
    //   272	276	358	com/google/f/x
    //   279	285	358	com/google/f/x
    //   285	289	358	com/google/f/x
    //   290	294	358	com/google/f/x
    //   296	300	358	com/google/f/x
    //   303	307	358	com/google/f/x
    //   309	313	358	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int k = -1;
    if (i != k) {
      return i;
    }
    Object localObject1 = a;
    boolean bool = ((String)localObject1).isEmpty();
    k = 0;
    Object localObject2;
    int j;
    if (!bool)
    {
      localObject2 = a;
      j = h.computeStringSize(1, (String)localObject2);
      k = 0 + j;
    }
    localObject1 = b;
    if (localObject1 != null)
    {
      localObject2 = c();
      j = h.computeMessageSize(2, (ae)localObject2);
      k += j;
    }
    memoizedSerializedSize = k;
    return k;
  }
  
  public final void writeTo(h paramh)
  {
    Object localObject1 = a;
    int i = ((String)localObject1).isEmpty();
    Object localObject2;
    if (i == 0)
    {
      i = 1;
      localObject2 = a;
      paramh.writeString(i, (String)localObject2);
    }
    localObject1 = b;
    if (localObject1 != null)
    {
      int j = 2;
      localObject2 = c();
      paramh.writeMessage(j, (ae)localObject2);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.models.Peer.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */