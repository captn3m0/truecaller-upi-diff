package com.truecaller.api.services.messenger.v1.models;

import com.google.f.w.c;
import com.google.f.w.d;

public enum GroupAction
  implements w.c
{
  public static final int INVITE_VALUE = 4;
  public static final int KICK_OUT_VALUE = 8;
  public static final int NO_ACTION_VALUE = 0;
  public static final int SEND_MESSAGE_VALUE = 16;
  public static final int UPDATE_INFO_VALUE = 2;
  public static final int UPDATE_SETTINGS_VALUE = 1;
  private static final w.d internalValueMap;
  private final int value;
  
  static
  {
    Object localObject = new com/truecaller/api/services/messenger/v1/models/GroupAction;
    ((GroupAction)localObject).<init>("NO_ACTION", 0, 0);
    NO_ACTION = (GroupAction)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/models/GroupAction;
    int i = 1;
    ((GroupAction)localObject).<init>("UPDATE_SETTINGS", i, i);
    UPDATE_SETTINGS = (GroupAction)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/models/GroupAction;
    int j = 2;
    ((GroupAction)localObject).<init>("UPDATE_INFO", j, j);
    UPDATE_INFO = (GroupAction)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/models/GroupAction;
    int k = 3;
    int m = 4;
    ((GroupAction)localObject).<init>("INVITE", k, m);
    INVITE = (GroupAction)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/models/GroupAction;
    ((GroupAction)localObject).<init>("KICK_OUT", m, 8);
    KICK_OUT = (GroupAction)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/models/GroupAction;
    int n = 5;
    ((GroupAction)localObject).<init>("SEND_MESSAGE", n, 16);
    SEND_MESSAGE = (GroupAction)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/models/GroupAction;
    int i1 = 6;
    ((GroupAction)localObject).<init>("UNRECOGNIZED", i1, -1);
    UNRECOGNIZED = (GroupAction)localObject;
    localObject = new GroupAction[7];
    GroupAction localGroupAction = NO_ACTION;
    localObject[0] = localGroupAction;
    localGroupAction = UPDATE_SETTINGS;
    localObject[i] = localGroupAction;
    localGroupAction = UPDATE_INFO;
    localObject[j] = localGroupAction;
    localGroupAction = INVITE;
    localObject[k] = localGroupAction;
    localGroupAction = KICK_OUT;
    localObject[m] = localGroupAction;
    localGroupAction = SEND_MESSAGE;
    localObject[n] = localGroupAction;
    localGroupAction = UNRECOGNIZED;
    localObject[i1] = localGroupAction;
    $VALUES = (GroupAction[])localObject;
    localObject = new com/truecaller/api/services/messenger/v1/models/GroupAction$1;
    ((GroupAction.1)localObject).<init>();
    internalValueMap = (w.d)localObject;
  }
  
  private GroupAction(int paramInt1)
  {
    value = paramInt1;
  }
  
  public static GroupAction forNumber(int paramInt)
  {
    int i = 4;
    if (paramInt != i)
    {
      i = 8;
      if (paramInt != i)
      {
        i = 16;
        if (paramInt != i)
        {
          switch (paramInt)
          {
          default: 
            return null;
          case 2: 
            return UPDATE_INFO;
          case 1: 
            return UPDATE_SETTINGS;
          }
          return NO_ACTION;
        }
        return SEND_MESSAGE;
      }
      return KICK_OUT;
    }
    return INVITE;
  }
  
  public static w.d internalGetValueMap()
  {
    return internalValueMap;
  }
  
  public static GroupAction valueOf(int paramInt)
  {
    return forNumber(paramInt);
  }
  
  public final int getNumber()
  {
    return value;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.models.GroupAction
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */