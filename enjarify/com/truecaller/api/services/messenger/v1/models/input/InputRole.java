package com.truecaller.api.services.messenger.v1.models.input;

import com.google.f.w.c;
import com.google.f.w.d;

public enum InputRole
  implements w.c
{
  public static final int ADMIN_VALUE = 536870912;
  public static final int INVITEE_VALUE = 2;
  public static final int NO_ROLE_VALUE = 0;
  public static final int SUPER_ADMIN_VALUE = 1073741824;
  public static final int USER_VALUE = 8;
  private static final w.d internalValueMap;
  private final int value;
  
  static
  {
    Object localObject = new com/truecaller/api/services/messenger/v1/models/input/InputRole;
    ((InputRole)localObject).<init>("NO_ROLE", 0, 0);
    NO_ROLE = (InputRole)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/models/input/InputRole;
    int i = 1;
    int j = 2;
    ((InputRole)localObject).<init>("INVITEE", i, j);
    INVITEE = (InputRole)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/models/input/InputRole;
    ((InputRole)localObject).<init>("USER", j, 8);
    USER = (InputRole)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/models/input/InputRole;
    int k = 3;
    ((InputRole)localObject).<init>("ADMIN", k, 536870912);
    ADMIN = (InputRole)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/models/input/InputRole;
    int m = 4;
    ((InputRole)localObject).<init>("SUPER_ADMIN", m, 1073741824);
    SUPER_ADMIN = (InputRole)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/models/input/InputRole;
    int n = 5;
    ((InputRole)localObject).<init>("UNRECOGNIZED", n, -1);
    UNRECOGNIZED = (InputRole)localObject;
    localObject = new InputRole[6];
    InputRole localInputRole = NO_ROLE;
    localObject[0] = localInputRole;
    localInputRole = INVITEE;
    localObject[i] = localInputRole;
    localInputRole = USER;
    localObject[j] = localInputRole;
    localInputRole = ADMIN;
    localObject[k] = localInputRole;
    localInputRole = SUPER_ADMIN;
    localObject[m] = localInputRole;
    localInputRole = UNRECOGNIZED;
    localObject[n] = localInputRole;
    $VALUES = (InputRole[])localObject;
    localObject = new com/truecaller/api/services/messenger/v1/models/input/InputRole$1;
    ((InputRole.1)localObject).<init>();
    internalValueMap = (w.d)localObject;
  }
  
  private InputRole(int paramInt1)
  {
    value = paramInt1;
  }
  
  public static InputRole forNumber(int paramInt)
  {
    if (paramInt != 0)
    {
      int i = 2;
      if (paramInt != i)
      {
        i = 8;
        if (paramInt != i)
        {
          i = 536870912;
          if (paramInt != i)
          {
            i = 1073741824;
            if (paramInt != i) {
              return null;
            }
            return SUPER_ADMIN;
          }
          return ADMIN;
        }
        return USER;
      }
      return INVITEE;
    }
    return NO_ROLE;
  }
  
  public static w.d internalGetValueMap()
  {
    return internalValueMap;
  }
  
  public static InputRole valueOf(int paramInt)
  {
    return forNumber(paramInt);
  }
  
  public final int getNumber()
  {
    return value;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.models.input.InputRole
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */