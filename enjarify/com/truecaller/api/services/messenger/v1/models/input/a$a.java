package com.truecaller.api.services.messenger.v1.models.input;

import com.google.f.q.a;

public final class a$a
  extends q.a
  implements b
{
  private a$a()
  {
    super(locala);
  }
  
  public final a a(String paramString)
  {
    copyOnWrite();
    a.a((a)instance, paramString);
    return this;
  }
  
  public final a b(String paramString)
  {
    copyOnWrite();
    a.b((a)instance, paramString);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.models.input.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */