package com.truecaller.api.services.messenger.v1.models.input;

import com.google.f.q.a;

public final class InputPeer$b$a
  extends q.a
  implements InputPeer.c
{
  private InputPeer$b$a()
  {
    super(localb);
  }
  
  public final a a(String paramString)
  {
    copyOnWrite();
    InputPeer.b.a((InputPeer.b)instance, paramString);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.models.input.InputPeer.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */