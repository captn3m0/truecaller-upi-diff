package com.truecaller.api.services.messenger.v1.models.input;

import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;

public final class InputSpan$Tag
  extends q
  implements InputSpan.c
{
  private static final Tag c;
  private static volatile ah d;
  private int a = 0;
  private Object b;
  
  static
  {
    Tag localTag = new com/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag;
    localTag.<init>();
    c = localTag;
    localTag.makeImmutable();
  }
  
  public static Tag a()
  {
    return c;
  }
  
  private String c()
  {
    String str = "";
    int i = a;
    int j = 2;
    if (i == j) {
      str = (String)b;
    }
    return str;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 38	com/truecaller/api/services/messenger/v1/models/input/InputSpan$1:b	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 44	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_3
    //   19: istore 6
    //   21: iconst_2
    //   22: istore 7
    //   24: iconst_0
    //   25: istore 8
    //   27: iconst_1
    //   28: istore 9
    //   30: iload 5
    //   32: tableswitch	default:+48->80, 1:+599->631, 2:+595->627, 3:+593->625, 4:+582->614, 5:+320->352, 6:+110->142, 7:+316->348, 8:+58->90
    //   80: new 48	java/lang/UnsupportedOperationException
    //   83: astore_1
    //   84: aload_1
    //   85: invokespecial 49	java/lang/UnsupportedOperationException:<init>	()V
    //   88: aload_1
    //   89: athrow
    //   90: getstatic 51	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag:d	Lcom/google/f/ah;
    //   93: astore_1
    //   94: aload_1
    //   95: ifnonnull +43 -> 138
    //   98: ldc 2
    //   100: astore_1
    //   101: aload_1
    //   102: monitorenter
    //   103: getstatic 51	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag:d	Lcom/google/f/ah;
    //   106: astore_2
    //   107: aload_2
    //   108: ifnonnull +20 -> 128
    //   111: new 53	com/google/f/q$b
    //   114: astore_2
    //   115: getstatic 20	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag:c	Lcom/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag;
    //   118: astore_3
    //   119: aload_2
    //   120: aload_3
    //   121: invokespecial 56	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   124: aload_2
    //   125: putstatic 51	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag:d	Lcom/google/f/ah;
    //   128: aload_1
    //   129: monitorexit
    //   130: goto +8 -> 138
    //   133: astore_2
    //   134: aload_1
    //   135: monitorexit
    //   136: aload_2
    //   137: athrow
    //   138: getstatic 51	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag:d	Lcom/google/f/ah;
    //   141: areturn
    //   142: aload_2
    //   143: checkcast 58	com/google/f/g
    //   146: astore_2
    //   147: iload 8
    //   149: ifne +199 -> 348
    //   152: aload_2
    //   153: invokevirtual 61	com/google/f/g:readTag	()I
    //   156: istore 5
    //   158: iload 5
    //   160: ifeq +126 -> 286
    //   163: bipush 8
    //   165: istore 10
    //   167: iload 5
    //   169: iload 10
    //   171: if_icmpeq +89 -> 260
    //   174: bipush 18
    //   176: istore 10
    //   178: iload 5
    //   180: iload 10
    //   182: if_icmpeq +59 -> 241
    //   185: bipush 24
    //   187: istore 10
    //   189: iload 5
    //   191: iload 10
    //   193: if_icmpeq +22 -> 215
    //   196: aload_2
    //   197: iload 5
    //   199: invokevirtual 68	com/google/f/g:skipField	(I)Z
    //   202: istore 5
    //   204: iload 5
    //   206: ifne -59 -> 147
    //   209: iconst_1
    //   210: istore 8
    //   212: goto -65 -> 147
    //   215: aload_0
    //   216: iload 6
    //   218: putfield 26	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag:a	I
    //   221: aload_2
    //   222: invokevirtual 72	com/google/f/g:readInt64	()J
    //   225: lstore 11
    //   227: lload 11
    //   229: invokestatic 78	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   232: astore_1
    //   233: aload_0
    //   234: aload_1
    //   235: putfield 31	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag:b	Ljava/lang/Object;
    //   238: goto -91 -> 147
    //   241: aload_2
    //   242: invokevirtual 82	com/google/f/g:readStringRequireUtf8	()Ljava/lang/String;
    //   245: astore_1
    //   246: aload_0
    //   247: iload 7
    //   249: putfield 26	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag:a	I
    //   252: aload_0
    //   253: aload_1
    //   254: putfield 31	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag:b	Ljava/lang/Object;
    //   257: goto -110 -> 147
    //   260: aload_0
    //   261: iload 9
    //   263: putfield 26	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag:a	I
    //   266: aload_2
    //   267: invokevirtual 86	com/google/f/g:readBool	()Z
    //   270: istore 5
    //   272: iload 5
    //   274: invokestatic 91	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   277: astore_1
    //   278: aload_0
    //   279: aload_1
    //   280: putfield 31	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag:b	Ljava/lang/Object;
    //   283: goto -136 -> 147
    //   286: iconst_1
    //   287: istore 8
    //   289: goto -142 -> 147
    //   292: astore_1
    //   293: goto +53 -> 346
    //   296: astore_1
    //   297: new 93	java/lang/RuntimeException
    //   300: astore_2
    //   301: new 95	com/google/f/x
    //   304: astore_3
    //   305: aload_1
    //   306: invokevirtual 100	java/io/IOException:getMessage	()Ljava/lang/String;
    //   309: astore_1
    //   310: aload_3
    //   311: aload_1
    //   312: invokespecial 103	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   315: aload_3
    //   316: aload_0
    //   317: invokevirtual 107	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   320: astore_1
    //   321: aload_2
    //   322: aload_1
    //   323: invokespecial 110	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   326: aload_2
    //   327: athrow
    //   328: astore_1
    //   329: new 93	java/lang/RuntimeException
    //   332: astore_2
    //   333: aload_1
    //   334: aload_0
    //   335: invokevirtual 107	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   338: astore_1
    //   339: aload_2
    //   340: aload_1
    //   341: invokespecial 110	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   344: aload_2
    //   345: athrow
    //   346: aload_1
    //   347: athrow
    //   348: getstatic 20	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag:c	Lcom/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag;
    //   351: areturn
    //   352: aload_2
    //   353: checkcast 112	com/google/f/q$k
    //   356: astore_2
    //   357: aload_3
    //   358: checkcast 2	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag
    //   361: astore_3
    //   362: getstatic 114	com/truecaller/api/services/messenger/v1/models/input/InputSpan$1:c	[I
    //   365: astore_1
    //   366: aload_3
    //   367: getfield 26	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag:a	I
    //   370: invokestatic 120	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag$ValueCase:forNumber	(I)Lcom/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag$ValueCase;
    //   373: astore 13
    //   375: aload 13
    //   377: invokevirtual 121	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag$ValueCase:ordinal	()I
    //   380: istore 14
    //   382: aload_1
    //   383: iload 14
    //   385: iaload
    //   386: istore 5
    //   388: iload 5
    //   390: tableswitch	default:+30->420, 1:+152->542, 2:+105->495, 3:+58->448, 4:+33->423
    //   420: goto +166 -> 586
    //   423: aload_0
    //   424: getfield 26	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag:a	I
    //   427: istore 5
    //   429: iload 5
    //   431: ifeq +6 -> 437
    //   434: iconst_1
    //   435: istore 8
    //   437: aload_2
    //   438: iload 8
    //   440: invokeinterface 125 2 0
    //   445: goto +141 -> 586
    //   448: aload_0
    //   449: getfield 26	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag:a	I
    //   452: istore 5
    //   454: iload 5
    //   456: iload 6
    //   458: if_icmpne +6 -> 464
    //   461: iconst_1
    //   462: istore 8
    //   464: aload_0
    //   465: getfield 31	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag:b	Ljava/lang/Object;
    //   468: astore_1
    //   469: aload_3
    //   470: getfield 31	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag:b	Ljava/lang/Object;
    //   473: astore 4
    //   475: aload_2
    //   476: iload 8
    //   478: aload_1
    //   479: aload 4
    //   481: invokeinterface 129 4 0
    //   486: astore_1
    //   487: aload_0
    //   488: aload_1
    //   489: putfield 31	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag:b	Ljava/lang/Object;
    //   492: goto +94 -> 586
    //   495: aload_0
    //   496: getfield 26	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag:a	I
    //   499: istore 5
    //   501: iload 5
    //   503: iload 7
    //   505: if_icmpne +6 -> 511
    //   508: iconst_1
    //   509: istore 8
    //   511: aload_0
    //   512: getfield 31	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag:b	Ljava/lang/Object;
    //   515: astore_1
    //   516: aload_3
    //   517: getfield 31	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag:b	Ljava/lang/Object;
    //   520: astore 4
    //   522: aload_2
    //   523: iload 8
    //   525: aload_1
    //   526: aload 4
    //   528: invokeinterface 132 4 0
    //   533: astore_1
    //   534: aload_0
    //   535: aload_1
    //   536: putfield 31	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag:b	Ljava/lang/Object;
    //   539: goto +47 -> 586
    //   542: aload_0
    //   543: getfield 26	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag:a	I
    //   546: istore 5
    //   548: iload 5
    //   550: iload 9
    //   552: if_icmpne +6 -> 558
    //   555: iconst_1
    //   556: istore 8
    //   558: aload_0
    //   559: getfield 31	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag:b	Ljava/lang/Object;
    //   562: astore_1
    //   563: aload_3
    //   564: getfield 31	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag:b	Ljava/lang/Object;
    //   567: astore 4
    //   569: aload_2
    //   570: iload 8
    //   572: aload_1
    //   573: aload 4
    //   575: invokeinterface 135 4 0
    //   580: astore_1
    //   581: aload_0
    //   582: aload_1
    //   583: putfield 31	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag:b	Ljava/lang/Object;
    //   586: getstatic 141	com/google/f/q$i:INSTANCE	Lcom/google/f/q$i;
    //   589: astore_1
    //   590: aload_2
    //   591: aload_1
    //   592: if_acmpne +20 -> 612
    //   595: aload_3
    //   596: getfield 26	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag:a	I
    //   599: istore 5
    //   601: iload 5
    //   603: ifeq +9 -> 612
    //   606: aload_0
    //   607: iload 5
    //   609: putfield 26	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag:a	I
    //   612: aload_0
    //   613: areturn
    //   614: new 143	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag$a
    //   617: astore_1
    //   618: aload_1
    //   619: iconst_0
    //   620: invokespecial 146	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag$a:<init>	(B)V
    //   623: aload_1
    //   624: areturn
    //   625: aconst_null
    //   626: areturn
    //   627: getstatic 20	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag:c	Lcom/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag;
    //   630: areturn
    //   631: new 2	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag
    //   634: astore_1
    //   635: aload_1
    //   636: invokespecial 18	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag:<init>	()V
    //   639: aload_1
    //   640: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	641	0	this	Tag
    //   0	641	1	paramj	com.google.f.q.j
    //   0	641	2	paramObject1	Object
    //   0	641	3	paramObject2	Object
    //   3	571	4	localObject	Object
    //   9	189	5	i	int
    //   202	71	5	bool1	boolean
    //   386	222	5	j	int
    //   19	440	6	k	int
    //   22	484	7	m	int
    //   25	546	8	bool2	boolean
    //   28	525	9	n	int
    //   165	29	10	i1	int
    //   225	3	11	l	long
    //   373	3	13	localValueCase	InputSpan.Tag.ValueCase
    //   380	4	14	i2	int
    // Exception table:
    //   from	to	target	type
    //   103	106	133	finally
    //   111	114	133	finally
    //   115	118	133	finally
    //   120	124	133	finally
    //   124	128	133	finally
    //   128	130	133	finally
    //   134	136	133	finally
    //   152	156	292	finally
    //   197	202	292	finally
    //   216	221	292	finally
    //   221	225	292	finally
    //   227	232	292	finally
    //   234	238	292	finally
    //   241	245	292	finally
    //   247	252	292	finally
    //   253	257	292	finally
    //   261	266	292	finally
    //   266	270	292	finally
    //   272	277	292	finally
    //   279	283	292	finally
    //   297	300	292	finally
    //   301	304	292	finally
    //   305	309	292	finally
    //   311	315	292	finally
    //   316	320	292	finally
    //   322	326	292	finally
    //   326	328	292	finally
    //   329	332	292	finally
    //   334	338	292	finally
    //   340	344	292	finally
    //   344	346	292	finally
    //   152	156	296	java/io/IOException
    //   197	202	296	java/io/IOException
    //   216	221	296	java/io/IOException
    //   221	225	296	java/io/IOException
    //   227	232	296	java/io/IOException
    //   234	238	296	java/io/IOException
    //   241	245	296	java/io/IOException
    //   247	252	296	java/io/IOException
    //   253	257	296	java/io/IOException
    //   261	266	296	java/io/IOException
    //   266	270	296	java/io/IOException
    //   272	277	296	java/io/IOException
    //   279	283	296	java/io/IOException
    //   152	156	328	com/google/f/x
    //   197	202	328	com/google/f/x
    //   216	221	328	com/google/f/x
    //   221	225	328	com/google/f/x
    //   227	232	328	com/google/f/x
    //   234	238	328	com/google/f/x
    //   241	245	328	com/google/f/x
    //   247	252	328	com/google/f/x
    //   253	257	328	com/google/f/x
    //   261	266	328	com/google/f/x
    //   266	270	328	com/google/f/x
    //   272	277	328	com/google/f/x
    //   279	283	328	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int k = -1;
    if (i != k) {
      return i;
    }
    i = a;
    k = 1;
    int m = 0;
    Object localObject;
    if (i == k)
    {
      localObject = (Boolean)b;
      boolean bool = ((Boolean)localObject).booleanValue();
      j = h.computeBoolSize(k, bool);
      m = 0 + j;
    }
    int j = a;
    k = 2;
    if (j == k)
    {
      localObject = c();
      j = h.computeStringSize(k, (String)localObject);
      m += j;
    }
    j = a;
    k = 3;
    if (j == k)
    {
      localObject = (Long)b;
      long l = ((Long)localObject).longValue();
      j = h.computeInt64Size(k, l);
      m += j;
    }
    memoizedSerializedSize = m;
    return m;
  }
  
  public final void writeTo(h paramh)
  {
    int i = a;
    int k = 1;
    Object localObject;
    if (i == k)
    {
      localObject = (Boolean)b;
      boolean bool = ((Boolean)localObject).booleanValue();
      paramh.writeBool(k, bool);
    }
    int j = a;
    k = 2;
    if (j == k)
    {
      localObject = c();
      paramh.writeString(k, (String)localObject);
    }
    j = a;
    k = 3;
    if (j == k)
    {
      localObject = (Long)b;
      long l = ((Long)localObject).longValue();
      paramh.writeInt64(k, l);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.models.input.InputSpan.Tag
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */