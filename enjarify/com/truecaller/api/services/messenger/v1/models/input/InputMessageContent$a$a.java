package com.truecaller.api.services.messenger.v1.models.input;

import com.google.f.q.a;

public final class InputMessageContent$a$a
  extends q.a
  implements InputMessageContent.b
{
  private InputMessageContent$a$a()
  {
    super(locala);
  }
  
  public final a a(int paramInt)
  {
    copyOnWrite();
    InputMessageContent.a.a((InputMessageContent.a)instance, paramInt);
    return this;
  }
  
  public final a a(String paramString)
  {
    copyOnWrite();
    InputMessageContent.a.a((InputMessageContent.a)instance, paramString);
    return this;
  }
  
  public final a b(int paramInt)
  {
    copyOnWrite();
    InputMessageContent.a.b((InputMessageContent.a)instance, paramInt);
    return this;
  }
  
  public final a b(String paramString)
  {
    copyOnWrite();
    InputMessageContent.a.b((InputMessageContent.a)instance, paramString);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.models.input.InputMessageContent.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */