package com.truecaller.api.services.messenger.v1.models;

import com.google.f.w.c;
import com.google.f.w.d;

public enum ReportType
  implements w.c
{
  public static final int READ_VALUE = 1;
  public static final int RECEIVED_VALUE = 2;
  public static final int UNKNOWN_REPORT_TYPE_VALUE;
  private static final w.d internalValueMap;
  private final int value;
  
  static
  {
    Object localObject = new com/truecaller/api/services/messenger/v1/models/ReportType;
    ((ReportType)localObject).<init>("UNKNOWN_REPORT_TYPE", 0, 0);
    UNKNOWN_REPORT_TYPE = (ReportType)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/models/ReportType;
    int i = 1;
    ((ReportType)localObject).<init>("READ", i, i);
    READ = (ReportType)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/models/ReportType;
    int j = 2;
    ((ReportType)localObject).<init>("RECEIVED", j, j);
    RECEIVED = (ReportType)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/models/ReportType;
    int k = 3;
    ((ReportType)localObject).<init>("UNRECOGNIZED", k, -1);
    UNRECOGNIZED = (ReportType)localObject;
    localObject = new ReportType[4];
    ReportType localReportType = UNKNOWN_REPORT_TYPE;
    localObject[0] = localReportType;
    localReportType = READ;
    localObject[i] = localReportType;
    localReportType = RECEIVED;
    localObject[j] = localReportType;
    localReportType = UNRECOGNIZED;
    localObject[k] = localReportType;
    $VALUES = (ReportType[])localObject;
    localObject = new com/truecaller/api/services/messenger/v1/models/ReportType$1;
    ((ReportType.1)localObject).<init>();
    internalValueMap = (w.d)localObject;
  }
  
  private ReportType(int paramInt1)
  {
    value = paramInt1;
  }
  
  public static ReportType forNumber(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return null;
    case 2: 
      return RECEIVED;
    case 1: 
      return READ;
    }
    return UNKNOWN_REPORT_TYPE;
  }
  
  public static w.d internalGetValueMap()
  {
    return internalValueMap;
  }
  
  public static ReportType valueOf(int paramInt)
  {
    return forNumber(paramInt);
  }
  
  public final int getNumber()
  {
    return value;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.models.ReportType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */