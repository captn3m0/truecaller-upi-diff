package com.truecaller.api.services.messenger.v1.models.input;

import com.google.f.q.a;

public final class InputNotificationScope$a
  extends q.a
  implements g
{
  private InputNotificationScope$a()
  {
    super(localInputNotificationScope);
  }
  
  public final a a(String paramString)
  {
    copyOnWrite();
    InputNotificationScope.a((InputNotificationScope)instance, paramString);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.models.input.InputNotificationScope.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */