package com.truecaller.api.services.messenger.v1.models;

import com.google.f.w.c;

public enum ReactionContent$ValueCase
  implements w.c
{
  private final int value;
  
  static
  {
    Object localObject = new com/truecaller/api/services/messenger/v1/models/ReactionContent$ValueCase;
    int i = 1;
    ((ValueCase)localObject).<init>("EMOJI", 0, i);
    EMOJI = (ValueCase)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/models/ReactionContent$ValueCase;
    ((ValueCase)localObject).<init>("VALUE_NOT_SET", i, 0);
    VALUE_NOT_SET = (ValueCase)localObject;
    localObject = new ValueCase[2];
    ValueCase localValueCase = EMOJI;
    localObject[0] = localValueCase;
    localValueCase = VALUE_NOT_SET;
    localObject[i] = localValueCase;
    $VALUES = (ValueCase[])localObject;
  }
  
  private ReactionContent$ValueCase(int paramInt1)
  {
    value = paramInt1;
  }
  
  public static ValueCase forNumber(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return null;
    case 1: 
      return EMOJI;
    }
    return VALUE_NOT_SET;
  }
  
  public static ValueCase valueOf(int paramInt)
  {
    return forNumber(paramInt);
  }
  
  public final int getNumber()
  {
    return value;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.models.ReactionContent.ValueCase
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */