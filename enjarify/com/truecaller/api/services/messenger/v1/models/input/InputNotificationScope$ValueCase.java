package com.truecaller.api.services.messenger.v1.models.input;

import com.google.f.w.c;

public enum InputNotificationScope$ValueCase
  implements w.c
{
  private final int value;
  
  static
  {
    Object localObject = new com/truecaller/api/services/messenger/v1/models/input/InputNotificationScope$ValueCase;
    int i = 1;
    ((ValueCase)localObject).<init>("PHONE_NUMBER", 0, i);
    PHONE_NUMBER = (ValueCase)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/models/input/InputNotificationScope$ValueCase;
    int j = 2;
    ((ValueCase)localObject).<init>("GROUP_ID", i, j);
    GROUP_ID = (ValueCase)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/models/input/InputNotificationScope$ValueCase;
    ((ValueCase)localObject).<init>("VALUE_NOT_SET", j, 0);
    VALUE_NOT_SET = (ValueCase)localObject;
    localObject = new ValueCase[3];
    ValueCase localValueCase = PHONE_NUMBER;
    localObject[0] = localValueCase;
    localValueCase = GROUP_ID;
    localObject[i] = localValueCase;
    localValueCase = VALUE_NOT_SET;
    localObject[j] = localValueCase;
    $VALUES = (ValueCase[])localObject;
  }
  
  private InputNotificationScope$ValueCase(int paramInt1)
  {
    value = paramInt1;
  }
  
  public static ValueCase forNumber(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return null;
    case 2: 
      return GROUP_ID;
    case 1: 
      return PHONE_NUMBER;
    }
    return VALUE_NOT_SET;
  }
  
  public static ValueCase valueOf(int paramInt)
  {
    return forNumber(paramInt);
  }
  
  public final int getNumber()
  {
    return value;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.models.input.InputNotificationScope.ValueCase
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */