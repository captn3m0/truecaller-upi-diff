package com.truecaller.api.services.messenger.v1.models;

import com.google.f.ah;
import com.google.f.f;
import com.google.f.h;
import com.google.f.q;

public final class MessageContent$j
  extends q
  implements MessageContent.k
{
  private static final j h;
  private static volatile ah i;
  private String a = "";
  private String b = "";
  private int c;
  private int d;
  private int e;
  private int f;
  private f g;
  
  static
  {
    j localj = new com/truecaller/api/services/messenger/v1/models/MessageContent$j;
    localj.<init>();
    h = localj;
    localj.makeImmutable();
  }
  
  private MessageContent$j()
  {
    f localf = f.EMPTY;
    g = localf;
  }
  
  public static j h()
  {
    return h;
  }
  
  public static ah i()
  {
    return h.getParserForType();
  }
  
  public final String a()
  {
    return a;
  }
  
  public final String b()
  {
    return b;
  }
  
  public final int c()
  {
    return c;
  }
  
  public final int d()
  {
    return d;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 56	com/truecaller/api/services/messenger/v1/models/MessageContent$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 62	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_0
    //   19: istore 6
    //   21: aconst_null
    //   22: astore 4
    //   24: iconst_1
    //   25: istore 7
    //   27: iload 5
    //   29: tableswitch	default:+47->76, 1:+930->959, 2:+926->955, 3:+924->953, 4:+913->942, 5:+391->420, 6:+109->138, 7:+387->416, 8:+57->86
    //   76: new 65	java/lang/UnsupportedOperationException
    //   79: astore_1
    //   80: aload_1
    //   81: invokespecial 66	java/lang/UnsupportedOperationException:<init>	()V
    //   84: aload_1
    //   85: athrow
    //   86: getstatic 68	com/truecaller/api/services/messenger/v1/models/MessageContent$j:i	Lcom/google/f/ah;
    //   89: astore_1
    //   90: aload_1
    //   91: ifnonnull +43 -> 134
    //   94: ldc 2
    //   96: astore_1
    //   97: aload_1
    //   98: monitorenter
    //   99: getstatic 68	com/truecaller/api/services/messenger/v1/models/MessageContent$j:i	Lcom/google/f/ah;
    //   102: astore_2
    //   103: aload_2
    //   104: ifnonnull +20 -> 124
    //   107: new 70	com/google/f/q$b
    //   110: astore_2
    //   111: getstatic 26	com/truecaller/api/services/messenger/v1/models/MessageContent$j:h	Lcom/truecaller/api/services/messenger/v1/models/MessageContent$j;
    //   114: astore_3
    //   115: aload_2
    //   116: aload_3
    //   117: invokespecial 73	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   120: aload_2
    //   121: putstatic 68	com/truecaller/api/services/messenger/v1/models/MessageContent$j:i	Lcom/google/f/ah;
    //   124: aload_1
    //   125: monitorexit
    //   126: goto +8 -> 134
    //   129: astore_2
    //   130: aload_1
    //   131: monitorexit
    //   132: aload_2
    //   133: athrow
    //   134: getstatic 68	com/truecaller/api/services/messenger/v1/models/MessageContent$j:i	Lcom/google/f/ah;
    //   137: areturn
    //   138: aload_2
    //   139: checkcast 75	com/google/f/g
    //   142: astore_2
    //   143: iload 6
    //   145: ifne +271 -> 416
    //   148: aload_2
    //   149: invokevirtual 78	com/google/f/g:readTag	()I
    //   152: istore 5
    //   154: iload 5
    //   156: ifeq +198 -> 354
    //   159: bipush 10
    //   161: istore 8
    //   163: iload 5
    //   165: iload 8
    //   167: if_icmpeq +174 -> 341
    //   170: bipush 18
    //   172: istore 8
    //   174: iload 5
    //   176: iload 8
    //   178: if_icmpeq +150 -> 328
    //   181: bipush 24
    //   183: istore 8
    //   185: iload 5
    //   187: iload 8
    //   189: if_icmpeq +124 -> 313
    //   192: bipush 32
    //   194: istore 8
    //   196: iload 5
    //   198: iload 8
    //   200: if_icmpeq +98 -> 298
    //   203: bipush 40
    //   205: istore 8
    //   207: iload 5
    //   209: iload 8
    //   211: if_icmpeq +72 -> 283
    //   214: bipush 48
    //   216: istore 8
    //   218: iload 5
    //   220: iload 8
    //   222: if_icmpeq +46 -> 268
    //   225: bipush 58
    //   227: istore 8
    //   229: iload 5
    //   231: iload 8
    //   233: if_icmpeq +22 -> 255
    //   236: aload_2
    //   237: iload 5
    //   239: invokevirtual 89	com/google/f/g:skipField	(I)Z
    //   242: istore 5
    //   244: iload 5
    //   246: ifne -103 -> 143
    //   249: iconst_1
    //   250: istore 6
    //   252: goto -109 -> 143
    //   255: aload_2
    //   256: invokevirtual 93	com/google/f/g:readBytes	()Lcom/google/f/f;
    //   259: astore_1
    //   260: aload_0
    //   261: aload_1
    //   262: putfield 43	com/truecaller/api/services/messenger/v1/models/MessageContent$j:g	Lcom/google/f/f;
    //   265: goto -122 -> 143
    //   268: aload_2
    //   269: invokevirtual 96	com/google/f/g:readInt32	()I
    //   272: istore 5
    //   274: aload_0
    //   275: iload 5
    //   277: putfield 98	com/truecaller/api/services/messenger/v1/models/MessageContent$j:f	I
    //   280: goto -137 -> 143
    //   283: aload_2
    //   284: invokevirtual 96	com/google/f/g:readInt32	()I
    //   287: istore 5
    //   289: aload_0
    //   290: iload 5
    //   292: putfield 100	com/truecaller/api/services/messenger/v1/models/MessageContent$j:e	I
    //   295: goto -152 -> 143
    //   298: aload_2
    //   299: invokevirtual 96	com/google/f/g:readInt32	()I
    //   302: istore 5
    //   304: aload_0
    //   305: iload 5
    //   307: putfield 51	com/truecaller/api/services/messenger/v1/models/MessageContent$j:d	I
    //   310: goto -167 -> 143
    //   313: aload_2
    //   314: invokevirtual 96	com/google/f/g:readInt32	()I
    //   317: istore 5
    //   319: aload_0
    //   320: iload 5
    //   322: putfield 49	com/truecaller/api/services/messenger/v1/models/MessageContent$j:c	I
    //   325: goto -182 -> 143
    //   328: aload_2
    //   329: invokevirtual 104	com/google/f/g:readStringRequireUtf8	()Ljava/lang/String;
    //   332: astore_1
    //   333: aload_0
    //   334: aload_1
    //   335: putfield 36	com/truecaller/api/services/messenger/v1/models/MessageContent$j:b	Ljava/lang/String;
    //   338: goto -195 -> 143
    //   341: aload_2
    //   342: invokevirtual 104	com/google/f/g:readStringRequireUtf8	()Ljava/lang/String;
    //   345: astore_1
    //   346: aload_0
    //   347: aload_1
    //   348: putfield 34	com/truecaller/api/services/messenger/v1/models/MessageContent$j:a	Ljava/lang/String;
    //   351: goto -208 -> 143
    //   354: iconst_1
    //   355: istore 6
    //   357: goto -214 -> 143
    //   360: astore_1
    //   361: goto +53 -> 414
    //   364: astore_1
    //   365: new 106	java/lang/RuntimeException
    //   368: astore_2
    //   369: new 108	com/google/f/x
    //   372: astore_3
    //   373: aload_1
    //   374: invokevirtual 113	java/io/IOException:getMessage	()Ljava/lang/String;
    //   377: astore_1
    //   378: aload_3
    //   379: aload_1
    //   380: invokespecial 116	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   383: aload_3
    //   384: aload_0
    //   385: invokevirtual 120	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   388: astore_1
    //   389: aload_2
    //   390: aload_1
    //   391: invokespecial 123	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   394: aload_2
    //   395: athrow
    //   396: astore_1
    //   397: new 106	java/lang/RuntimeException
    //   400: astore_2
    //   401: aload_1
    //   402: aload_0
    //   403: invokevirtual 120	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   406: astore_1
    //   407: aload_2
    //   408: aload_1
    //   409: invokespecial 123	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   412: aload_2
    //   413: athrow
    //   414: aload_1
    //   415: athrow
    //   416: getstatic 26	com/truecaller/api/services/messenger/v1/models/MessageContent$j:h	Lcom/truecaller/api/services/messenger/v1/models/MessageContent$j;
    //   419: areturn
    //   420: aload_2
    //   421: checkcast 125	com/google/f/q$k
    //   424: astore_2
    //   425: aload_3
    //   426: checkcast 2	com/truecaller/api/services/messenger/v1/models/MessageContent$j
    //   429: astore_3
    //   430: aload_0
    //   431: getfield 34	com/truecaller/api/services/messenger/v1/models/MessageContent$j:a	Ljava/lang/String;
    //   434: invokevirtual 131	java/lang/String:isEmpty	()Z
    //   437: iload 7
    //   439: ixor
    //   440: istore 5
    //   442: aload_0
    //   443: getfield 34	com/truecaller/api/services/messenger/v1/models/MessageContent$j:a	Ljava/lang/String;
    //   446: astore 9
    //   448: aload_3
    //   449: getfield 34	com/truecaller/api/services/messenger/v1/models/MessageContent$j:a	Ljava/lang/String;
    //   452: invokevirtual 131	java/lang/String:isEmpty	()Z
    //   455: iload 7
    //   457: ixor
    //   458: istore 10
    //   460: aload_3
    //   461: getfield 34	com/truecaller/api/services/messenger/v1/models/MessageContent$j:a	Ljava/lang/String;
    //   464: astore 11
    //   466: aload_2
    //   467: iload 5
    //   469: aload 9
    //   471: iload 10
    //   473: aload 11
    //   475: invokeinterface 135 5 0
    //   480: astore_1
    //   481: aload_0
    //   482: aload_1
    //   483: putfield 34	com/truecaller/api/services/messenger/v1/models/MessageContent$j:a	Ljava/lang/String;
    //   486: aload_0
    //   487: getfield 36	com/truecaller/api/services/messenger/v1/models/MessageContent$j:b	Ljava/lang/String;
    //   490: invokevirtual 131	java/lang/String:isEmpty	()Z
    //   493: iload 7
    //   495: ixor
    //   496: istore 5
    //   498: aload_0
    //   499: getfield 36	com/truecaller/api/services/messenger/v1/models/MessageContent$j:b	Ljava/lang/String;
    //   502: astore 9
    //   504: aload_3
    //   505: getfield 36	com/truecaller/api/services/messenger/v1/models/MessageContent$j:b	Ljava/lang/String;
    //   508: astore 12
    //   510: aload 12
    //   512: invokevirtual 131	java/lang/String:isEmpty	()Z
    //   515: iload 7
    //   517: ixor
    //   518: istore 10
    //   520: aload_3
    //   521: getfield 36	com/truecaller/api/services/messenger/v1/models/MessageContent$j:b	Ljava/lang/String;
    //   524: astore 11
    //   526: aload_2
    //   527: iload 5
    //   529: aload 9
    //   531: iload 10
    //   533: aload 11
    //   535: invokeinterface 135 5 0
    //   540: astore_1
    //   541: aload_0
    //   542: aload_1
    //   543: putfield 36	com/truecaller/api/services/messenger/v1/models/MessageContent$j:b	Ljava/lang/String;
    //   546: aload_0
    //   547: getfield 49	com/truecaller/api/services/messenger/v1/models/MessageContent$j:c	I
    //   550: istore 5
    //   552: iload 5
    //   554: ifeq +9 -> 563
    //   557: iconst_1
    //   558: istore 5
    //   560: goto +8 -> 568
    //   563: iconst_0
    //   564: istore 5
    //   566: aconst_null
    //   567: astore_1
    //   568: aload_0
    //   569: getfield 49	com/truecaller/api/services/messenger/v1/models/MessageContent$j:c	I
    //   572: istore 13
    //   574: aload_3
    //   575: getfield 49	com/truecaller/api/services/messenger/v1/models/MessageContent$j:c	I
    //   578: istore 10
    //   580: iload 10
    //   582: ifeq +9 -> 591
    //   585: iconst_1
    //   586: istore 10
    //   588: goto +9 -> 597
    //   591: iconst_0
    //   592: istore 10
    //   594: aconst_null
    //   595: astore 12
    //   597: aload_3
    //   598: getfield 49	com/truecaller/api/services/messenger/v1/models/MessageContent$j:c	I
    //   601: istore 14
    //   603: aload_2
    //   604: iload 5
    //   606: iload 13
    //   608: iload 10
    //   610: iload 14
    //   612: invokeinterface 139 5 0
    //   617: istore 5
    //   619: aload_0
    //   620: iload 5
    //   622: putfield 49	com/truecaller/api/services/messenger/v1/models/MessageContent$j:c	I
    //   625: aload_0
    //   626: getfield 51	com/truecaller/api/services/messenger/v1/models/MessageContent$j:d	I
    //   629: istore 5
    //   631: iload 5
    //   633: ifeq +9 -> 642
    //   636: iconst_1
    //   637: istore 5
    //   639: goto +8 -> 647
    //   642: iconst_0
    //   643: istore 5
    //   645: aconst_null
    //   646: astore_1
    //   647: aload_0
    //   648: getfield 51	com/truecaller/api/services/messenger/v1/models/MessageContent$j:d	I
    //   651: istore 13
    //   653: aload_3
    //   654: getfield 51	com/truecaller/api/services/messenger/v1/models/MessageContent$j:d	I
    //   657: istore 10
    //   659: iload 10
    //   661: ifeq +9 -> 670
    //   664: iconst_1
    //   665: istore 10
    //   667: goto +9 -> 676
    //   670: iconst_0
    //   671: istore 10
    //   673: aconst_null
    //   674: astore 12
    //   676: aload_3
    //   677: getfield 51	com/truecaller/api/services/messenger/v1/models/MessageContent$j:d	I
    //   680: istore 14
    //   682: aload_2
    //   683: iload 5
    //   685: iload 13
    //   687: iload 10
    //   689: iload 14
    //   691: invokeinterface 139 5 0
    //   696: istore 5
    //   698: aload_0
    //   699: iload 5
    //   701: putfield 51	com/truecaller/api/services/messenger/v1/models/MessageContent$j:d	I
    //   704: aload_0
    //   705: getfield 100	com/truecaller/api/services/messenger/v1/models/MessageContent$j:e	I
    //   708: istore 5
    //   710: iload 5
    //   712: ifeq +9 -> 721
    //   715: iconst_1
    //   716: istore 5
    //   718: goto +8 -> 726
    //   721: iconst_0
    //   722: istore 5
    //   724: aconst_null
    //   725: astore_1
    //   726: aload_0
    //   727: getfield 100	com/truecaller/api/services/messenger/v1/models/MessageContent$j:e	I
    //   730: istore 13
    //   732: aload_3
    //   733: getfield 100	com/truecaller/api/services/messenger/v1/models/MessageContent$j:e	I
    //   736: istore 10
    //   738: iload 10
    //   740: ifeq +9 -> 749
    //   743: iconst_1
    //   744: istore 10
    //   746: goto +9 -> 755
    //   749: iconst_0
    //   750: istore 10
    //   752: aconst_null
    //   753: astore 12
    //   755: aload_3
    //   756: getfield 100	com/truecaller/api/services/messenger/v1/models/MessageContent$j:e	I
    //   759: istore 14
    //   761: aload_2
    //   762: iload 5
    //   764: iload 13
    //   766: iload 10
    //   768: iload 14
    //   770: invokeinterface 139 5 0
    //   775: istore 5
    //   777: aload_0
    //   778: iload 5
    //   780: putfield 100	com/truecaller/api/services/messenger/v1/models/MessageContent$j:e	I
    //   783: aload_0
    //   784: getfield 98	com/truecaller/api/services/messenger/v1/models/MessageContent$j:f	I
    //   787: istore 5
    //   789: iload 5
    //   791: ifeq +9 -> 800
    //   794: iconst_1
    //   795: istore 5
    //   797: goto +8 -> 805
    //   800: iconst_0
    //   801: istore 5
    //   803: aconst_null
    //   804: astore_1
    //   805: aload_0
    //   806: getfield 98	com/truecaller/api/services/messenger/v1/models/MessageContent$j:f	I
    //   809: istore 13
    //   811: aload_3
    //   812: getfield 98	com/truecaller/api/services/messenger/v1/models/MessageContent$j:f	I
    //   815: istore 10
    //   817: iload 10
    //   819: ifeq +9 -> 828
    //   822: iconst_1
    //   823: istore 10
    //   825: goto +9 -> 834
    //   828: iconst_0
    //   829: istore 10
    //   831: aconst_null
    //   832: astore 12
    //   834: aload_3
    //   835: getfield 98	com/truecaller/api/services/messenger/v1/models/MessageContent$j:f	I
    //   838: istore 14
    //   840: aload_2
    //   841: iload 5
    //   843: iload 13
    //   845: iload 10
    //   847: iload 14
    //   849: invokeinterface 139 5 0
    //   854: istore 5
    //   856: aload_0
    //   857: iload 5
    //   859: putfield 98	com/truecaller/api/services/messenger/v1/models/MessageContent$j:f	I
    //   862: aload_0
    //   863: getfield 43	com/truecaller/api/services/messenger/v1/models/MessageContent$j:g	Lcom/google/f/f;
    //   866: astore_1
    //   867: getstatic 41	com/google/f/f:EMPTY	Lcom/google/f/f;
    //   870: astore 9
    //   872: aload_1
    //   873: aload 9
    //   875: if_acmpeq +9 -> 884
    //   878: iconst_1
    //   879: istore 5
    //   881: goto +8 -> 889
    //   884: iconst_0
    //   885: istore 5
    //   887: aconst_null
    //   888: astore_1
    //   889: aload_0
    //   890: getfield 43	com/truecaller/api/services/messenger/v1/models/MessageContent$j:g	Lcom/google/f/f;
    //   893: astore 9
    //   895: aload_3
    //   896: getfield 43	com/truecaller/api/services/messenger/v1/models/MessageContent$j:g	Lcom/google/f/f;
    //   899: astore 12
    //   901: getstatic 41	com/google/f/f:EMPTY	Lcom/google/f/f;
    //   904: astore 11
    //   906: aload 12
    //   908: aload 11
    //   910: if_acmpeq +6 -> 916
    //   913: iconst_1
    //   914: istore 6
    //   916: aload_3
    //   917: getfield 43	com/truecaller/api/services/messenger/v1/models/MessageContent$j:g	Lcom/google/f/f;
    //   920: astore_3
    //   921: aload_2
    //   922: iload 5
    //   924: aload 9
    //   926: iload 6
    //   928: aload_3
    //   929: invokeinterface 143 5 0
    //   934: astore_1
    //   935: aload_0
    //   936: aload_1
    //   937: putfield 43	com/truecaller/api/services/messenger/v1/models/MessageContent$j:g	Lcom/google/f/f;
    //   940: aload_0
    //   941: areturn
    //   942: new 145	com/truecaller/api/services/messenger/v1/models/MessageContent$j$a
    //   945: astore_1
    //   946: aload_1
    //   947: iconst_0
    //   948: invokespecial 148	com/truecaller/api/services/messenger/v1/models/MessageContent$j$a:<init>	(B)V
    //   951: aload_1
    //   952: areturn
    //   953: aconst_null
    //   954: areturn
    //   955: getstatic 26	com/truecaller/api/services/messenger/v1/models/MessageContent$j:h	Lcom/truecaller/api/services/messenger/v1/models/MessageContent$j;
    //   958: areturn
    //   959: new 2	com/truecaller/api/services/messenger/v1/models/MessageContent$j
    //   962: astore_1
    //   963: aload_1
    //   964: invokespecial 24	com/truecaller/api/services/messenger/v1/models/MessageContent$j:<init>	()V
    //   967: aload_1
    //   968: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	969	0	this	j
    //   0	969	1	paramj	com.google.f.q.j
    //   0	969	2	paramObject1	Object
    //   0	969	3	paramObject2	Object
    //   3	20	4	arrayOfInt	int[]
    //   9	229	5	j	int
    //   242	3	5	bool1	boolean
    //   272	49	5	k	int
    //   440	88	5	bool2	boolean
    //   550	55	5	m	int
    //   617	67	5	n	int
    //   696	67	5	i1	int
    //   775	67	5	i2	int
    //   854	69	5	i3	int
    //   19	908	6	bool3	boolean
    //   25	493	7	i4	int
    //   161	73	8	i5	int
    //   446	479	9	localObject1	Object
    //   458	74	10	bool4	boolean
    //   578	31	10	i6	int
    //   657	31	10	i7	int
    //   736	31	10	i8	int
    //   815	31	10	i9	int
    //   464	445	11	localObject2	Object
    //   508	399	12	localObject3	Object
    //   572	272	13	i10	int
    //   601	247	14	i11	int
    // Exception table:
    //   from	to	target	type
    //   99	102	129	finally
    //   107	110	129	finally
    //   111	114	129	finally
    //   116	120	129	finally
    //   120	124	129	finally
    //   124	126	129	finally
    //   130	132	129	finally
    //   148	152	360	finally
    //   237	242	360	finally
    //   255	259	360	finally
    //   261	265	360	finally
    //   268	272	360	finally
    //   275	280	360	finally
    //   283	287	360	finally
    //   290	295	360	finally
    //   298	302	360	finally
    //   305	310	360	finally
    //   313	317	360	finally
    //   320	325	360	finally
    //   328	332	360	finally
    //   334	338	360	finally
    //   341	345	360	finally
    //   347	351	360	finally
    //   365	368	360	finally
    //   369	372	360	finally
    //   373	377	360	finally
    //   379	383	360	finally
    //   384	388	360	finally
    //   390	394	360	finally
    //   394	396	360	finally
    //   397	400	360	finally
    //   402	406	360	finally
    //   408	412	360	finally
    //   412	414	360	finally
    //   148	152	364	java/io/IOException
    //   237	242	364	java/io/IOException
    //   255	259	364	java/io/IOException
    //   261	265	364	java/io/IOException
    //   268	272	364	java/io/IOException
    //   275	280	364	java/io/IOException
    //   283	287	364	java/io/IOException
    //   290	295	364	java/io/IOException
    //   298	302	364	java/io/IOException
    //   305	310	364	java/io/IOException
    //   313	317	364	java/io/IOException
    //   320	325	364	java/io/IOException
    //   328	332	364	java/io/IOException
    //   334	338	364	java/io/IOException
    //   341	345	364	java/io/IOException
    //   347	351	364	java/io/IOException
    //   148	152	396	com/google/f/x
    //   237	242	396	com/google/f/x
    //   255	259	396	com/google/f/x
    //   261	265	396	com/google/f/x
    //   268	272	396	com/google/f/x
    //   275	280	396	com/google/f/x
    //   283	287	396	com/google/f/x
    //   290	295	396	com/google/f/x
    //   298	302	396	com/google/f/x
    //   305	310	396	com/google/f/x
    //   313	317	396	com/google/f/x
    //   320	325	396	com/google/f/x
    //   328	332	396	com/google/f/x
    //   334	338	396	com/google/f/x
    //   341	345	396	com/google/f/x
    //   347	351	396	com/google/f/x
  }
  
  public final int e()
  {
    return e;
  }
  
  public final int f()
  {
    return f;
  }
  
  public final f g()
  {
    return g;
  }
  
  public final int getSerializedSize()
  {
    int j = memoizedSerializedSize;
    int i1 = -1;
    if (j != i1) {
      return j;
    }
    Object localObject1 = a;
    boolean bool1 = ((String)localObject1).isEmpty();
    i1 = 0;
    Object localObject2;
    if (!bool1)
    {
      localObject2 = a;
      int k = h.computeStringSize(1, (String)localObject2);
      i1 = 0 + k;
    }
    localObject1 = b;
    boolean bool2 = ((String)localObject1).isEmpty();
    if (!bool2)
    {
      localObject2 = b;
      m = h.computeStringSize(2, (String)localObject2);
      i1 += m;
    }
    int m = c;
    int i2;
    if (m != 0)
    {
      i2 = 3;
      m = h.computeInt32Size(i2, m);
      i1 += m;
    }
    m = d;
    if (m != 0)
    {
      i2 = 4;
      m = h.computeInt32Size(i2, m);
      i1 += m;
    }
    m = e;
    if (m != 0)
    {
      i2 = 5;
      m = h.computeInt32Size(i2, m);
      i1 += m;
    }
    m = f;
    if (m != 0)
    {
      i2 = 6;
      m = h.computeInt32Size(i2, m);
      i1 += m;
    }
    localObject1 = g;
    boolean bool3 = ((f)localObject1).isEmpty();
    if (!bool3)
    {
      localObject2 = g;
      int n = h.computeBytesSize(7, (f)localObject2);
      i1 += n;
    }
    memoizedSerializedSize = i1;
    return i1;
  }
  
  public final void writeTo(h paramh)
  {
    Object localObject1 = a;
    int j = ((String)localObject1).isEmpty();
    Object localObject2;
    if (j == 0)
    {
      j = 1;
      localObject2 = a;
      paramh.writeString(j, (String)localObject2);
    }
    localObject1 = b;
    boolean bool1 = ((String)localObject1).isEmpty();
    if (!bool1)
    {
      k = 2;
      localObject2 = b;
      paramh.writeString(k, (String)localObject2);
    }
    int k = c;
    int n;
    if (k != 0)
    {
      n = 3;
      paramh.writeInt32(n, k);
    }
    k = d;
    if (k != 0)
    {
      n = 4;
      paramh.writeInt32(n, k);
    }
    k = e;
    if (k != 0)
    {
      n = 5;
      paramh.writeInt32(n, k);
    }
    k = f;
    if (k != 0)
    {
      n = 6;
      paramh.writeInt32(n, k);
    }
    localObject1 = g;
    boolean bool2 = ((f)localObject1).isEmpty();
    if (!bool2)
    {
      int m = 7;
      localObject2 = g;
      paramh.writeBytes(m, (f)localObject2);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.models.MessageContent.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */