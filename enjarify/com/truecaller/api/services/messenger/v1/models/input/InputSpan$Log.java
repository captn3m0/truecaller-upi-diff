package com.truecaller.api.services.messenger.v1.models.input;

import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;

public final class InputSpan$Log
  extends q
  implements InputSpan.b
{
  private static final Log d;
  private static volatile ah e;
  private int a = 0;
  private Object b;
  private long c;
  
  static
  {
    Log localLog = new com/truecaller/api/services/messenger/v1/models/input/InputSpan$Log;
    localLog.<init>();
    d = localLog;
    localLog.makeImmutable();
  }
  
  public static ah a()
  {
    return d.getParserForType();
  }
  
  private String c()
  {
    String str = "";
    int i = a;
    int j = 2;
    if (i == j) {
      str = (String)b;
    }
    return str;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 44	com/truecaller/api/services/messenger/v1/models/input/InputSpan$1:b	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 50	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_2
    //   19: istore 6
    //   21: iconst_1
    //   22: istore 7
    //   24: iconst_0
    //   25: istore 8
    //   27: iload 5
    //   29: tableswitch	default:+47->76, 1:+558->587, 2:+554->583, 3:+552->581, 4:+541->570, 5:+271->300, 6:+109->138, 7:+267->296, 8:+57->86
    //   76: new 53	java/lang/UnsupportedOperationException
    //   79: astore_1
    //   80: aload_1
    //   81: invokespecial 54	java/lang/UnsupportedOperationException:<init>	()V
    //   84: aload_1
    //   85: athrow
    //   86: getstatic 56	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Log:e	Lcom/google/f/ah;
    //   89: astore_1
    //   90: aload_1
    //   91: ifnonnull +43 -> 134
    //   94: ldc 2
    //   96: astore_1
    //   97: aload_1
    //   98: monitorenter
    //   99: getstatic 56	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Log:e	Lcom/google/f/ah;
    //   102: astore_2
    //   103: aload_2
    //   104: ifnonnull +20 -> 124
    //   107: new 58	com/google/f/q$b
    //   110: astore_2
    //   111: getstatic 22	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Log:d	Lcom/truecaller/api/services/messenger/v1/models/input/InputSpan$Log;
    //   114: astore_3
    //   115: aload_2
    //   116: aload_3
    //   117: invokespecial 61	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   120: aload_2
    //   121: putstatic 56	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Log:e	Lcom/google/f/ah;
    //   124: aload_1
    //   125: monitorexit
    //   126: goto +8 -> 134
    //   129: astore_2
    //   130: aload_1
    //   131: monitorexit
    //   132: aload_2
    //   133: athrow
    //   134: getstatic 56	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Log:e	Lcom/google/f/ah;
    //   137: areturn
    //   138: aload_2
    //   139: checkcast 63	com/google/f/g
    //   142: astore_2
    //   143: iload 8
    //   145: ifne +151 -> 296
    //   148: aload_2
    //   149: invokevirtual 66	com/google/f/g:readTag	()I
    //   152: istore 5
    //   154: iload 5
    //   156: ifeq +78 -> 234
    //   159: bipush 8
    //   161: istore 9
    //   163: iload 5
    //   165: iload 9
    //   167: if_icmpeq +52 -> 219
    //   170: bipush 18
    //   172: istore 9
    //   174: iload 5
    //   176: iload 9
    //   178: if_icmpeq +22 -> 200
    //   181: aload_2
    //   182: iload 5
    //   184: invokevirtual 72	com/google/f/g:skipField	(I)Z
    //   187: istore 5
    //   189: iload 5
    //   191: ifne -48 -> 143
    //   194: iconst_1
    //   195: istore 8
    //   197: goto -54 -> 143
    //   200: aload_2
    //   201: invokevirtual 76	com/google/f/g:readStringRequireUtf8	()Ljava/lang/String;
    //   204: astore_1
    //   205: aload_0
    //   206: iload 6
    //   208: putfield 28	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Log:a	I
    //   211: aload_0
    //   212: aload_1
    //   213: putfield 37	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Log:b	Ljava/lang/Object;
    //   216: goto -73 -> 143
    //   219: aload_2
    //   220: invokevirtual 80	com/google/f/g:readInt64	()J
    //   223: lstore 10
    //   225: aload_0
    //   226: lload 10
    //   228: putfield 82	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Log:c	J
    //   231: goto -88 -> 143
    //   234: iconst_1
    //   235: istore 8
    //   237: goto -94 -> 143
    //   240: astore_1
    //   241: goto +53 -> 294
    //   244: astore_1
    //   245: new 84	java/lang/RuntimeException
    //   248: astore_2
    //   249: new 86	com/google/f/x
    //   252: astore_3
    //   253: aload_1
    //   254: invokevirtual 91	java/io/IOException:getMessage	()Ljava/lang/String;
    //   257: astore_1
    //   258: aload_3
    //   259: aload_1
    //   260: invokespecial 94	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   263: aload_3
    //   264: aload_0
    //   265: invokevirtual 98	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   268: astore_1
    //   269: aload_2
    //   270: aload_1
    //   271: invokespecial 101	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   274: aload_2
    //   275: athrow
    //   276: astore_1
    //   277: new 84	java/lang/RuntimeException
    //   280: astore_2
    //   281: aload_1
    //   282: aload_0
    //   283: invokevirtual 98	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   286: astore_1
    //   287: aload_2
    //   288: aload_1
    //   289: invokespecial 101	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   292: aload_2
    //   293: athrow
    //   294: aload_1
    //   295: athrow
    //   296: getstatic 22	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Log:d	Lcom/truecaller/api/services/messenger/v1/models/input/InputSpan$Log;
    //   299: areturn
    //   300: aload_2
    //   301: astore_1
    //   302: aload_2
    //   303: checkcast 103	com/google/f/q$k
    //   306: astore_1
    //   307: aload_3
    //   308: checkcast 2	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Log
    //   311: astore_3
    //   312: aload_0
    //   313: getfield 82	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Log:c	J
    //   316: lstore 10
    //   318: lconst_0
    //   319: lstore 12
    //   321: lload 10
    //   323: lload 12
    //   325: lcmp
    //   326: istore 14
    //   328: iload 14
    //   330: ifeq +9 -> 339
    //   333: iconst_1
    //   334: istore 15
    //   336: goto +6 -> 342
    //   339: iconst_0
    //   340: istore 15
    //   342: aload_0
    //   343: getfield 82	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Log:c	J
    //   346: lstore 16
    //   348: aload_3
    //   349: getfield 82	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Log:c	J
    //   352: lstore 18
    //   354: lload 18
    //   356: lload 12
    //   358: lcmp
    //   359: istore 14
    //   361: iload 14
    //   363: ifeq +9 -> 372
    //   366: iconst_1
    //   367: istore 14
    //   369: goto +8 -> 377
    //   372: iconst_0
    //   373: istore 14
    //   375: aconst_null
    //   376: astore_2
    //   377: aload_3
    //   378: getfield 82	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Log:c	J
    //   381: lstore 18
    //   383: aload_1
    //   384: astore 20
    //   386: lload 16
    //   388: lstore 12
    //   390: aload_1
    //   391: iload 15
    //   393: lload 16
    //   395: iload 14
    //   397: lload 18
    //   399: invokeinterface 107 7 0
    //   404: lstore 10
    //   406: aload_0
    //   407: lload 10
    //   409: putfield 82	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Log:c	J
    //   412: getstatic 109	com/truecaller/api/services/messenger/v1/models/input/InputSpan$1:a	[I
    //   415: astore_2
    //   416: aload_3
    //   417: getfield 28	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Log:a	I
    //   420: invokestatic 115	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Log$ValueCase:forNumber	(I)Lcom/truecaller/api/services/messenger/v1/models/input/InputSpan$Log$ValueCase;
    //   423: astore 20
    //   425: aload 20
    //   427: invokevirtual 116	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Log$ValueCase:ordinal	()I
    //   430: istore 21
    //   432: aload_2
    //   433: iload 21
    //   435: iaload
    //   436: istore 14
    //   438: iload 14
    //   440: tableswitch	default:+24->464, 1:+55->495, 2:+27->467
    //   464: goto +78 -> 542
    //   467: aload_0
    //   468: getfield 28	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Log:a	I
    //   471: istore 14
    //   473: iload 14
    //   475: ifeq +6 -> 481
    //   478: goto +6 -> 484
    //   481: iconst_0
    //   482: istore 7
    //   484: aload_1
    //   485: iload 7
    //   487: invokeinterface 120 2 0
    //   492: goto +50 -> 542
    //   495: aload_0
    //   496: getfield 28	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Log:a	I
    //   499: istore 14
    //   501: iload 14
    //   503: iload 6
    //   505: if_icmpne +6 -> 511
    //   508: goto +6 -> 514
    //   511: iconst_0
    //   512: istore 7
    //   514: aload_0
    //   515: getfield 37	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Log:b	Ljava/lang/Object;
    //   518: astore_2
    //   519: aload_3
    //   520: getfield 37	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Log:b	Ljava/lang/Object;
    //   523: astore 4
    //   525: aload_1
    //   526: iload 7
    //   528: aload_2
    //   529: aload 4
    //   531: invokeinterface 124 4 0
    //   536: astore_2
    //   537: aload_0
    //   538: aload_2
    //   539: putfield 37	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Log:b	Ljava/lang/Object;
    //   542: getstatic 130	com/google/f/q$i:INSTANCE	Lcom/google/f/q$i;
    //   545: astore_2
    //   546: aload_1
    //   547: aload_2
    //   548: if_acmpne +20 -> 568
    //   551: aload_3
    //   552: getfield 28	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Log:a	I
    //   555: istore 5
    //   557: iload 5
    //   559: ifeq +9 -> 568
    //   562: aload_0
    //   563: iload 5
    //   565: putfield 28	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Log:a	I
    //   568: aload_0
    //   569: areturn
    //   570: new 132	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Log$a
    //   573: astore_1
    //   574: aload_1
    //   575: iconst_0
    //   576: invokespecial 135	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Log$a:<init>	(B)V
    //   579: aload_1
    //   580: areturn
    //   581: aconst_null
    //   582: areturn
    //   583: getstatic 22	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Log:d	Lcom/truecaller/api/services/messenger/v1/models/input/InputSpan$Log;
    //   586: areturn
    //   587: new 2	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Log
    //   590: astore_1
    //   591: aload_1
    //   592: invokespecial 20	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Log:<init>	()V
    //   595: aload_1
    //   596: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	597	0	this	Log
    //   0	597	1	paramj	com.google.f.q.j
    //   0	597	2	paramObject1	Object
    //   0	597	3	paramObject2	Object
    //   3	527	4	localObject1	Object
    //   9	174	5	i	int
    //   187	3	5	bool1	boolean
    //   555	9	5	j	int
    //   19	487	6	k	int
    //   22	505	7	bool2	boolean
    //   25	211	8	m	int
    //   161	18	9	n	int
    //   223	185	10	l1	long
    //   319	70	12	l2	long
    //   326	70	14	bool3	boolean
    //   436	70	14	i1	int
    //   334	58	15	bool4	boolean
    //   346	48	16	l3	long
    //   352	46	18	l4	long
    //   384	42	20	localObject2	Object
    //   430	4	21	i2	int
    // Exception table:
    //   from	to	target	type
    //   99	102	129	finally
    //   107	110	129	finally
    //   111	114	129	finally
    //   116	120	129	finally
    //   120	124	129	finally
    //   124	126	129	finally
    //   130	132	129	finally
    //   148	152	240	finally
    //   182	187	240	finally
    //   200	204	240	finally
    //   206	211	240	finally
    //   212	216	240	finally
    //   219	223	240	finally
    //   226	231	240	finally
    //   245	248	240	finally
    //   249	252	240	finally
    //   253	257	240	finally
    //   259	263	240	finally
    //   264	268	240	finally
    //   270	274	240	finally
    //   274	276	240	finally
    //   277	280	240	finally
    //   282	286	240	finally
    //   288	292	240	finally
    //   292	294	240	finally
    //   148	152	244	java/io/IOException
    //   182	187	244	java/io/IOException
    //   200	204	244	java/io/IOException
    //   206	211	244	java/io/IOException
    //   212	216	244	java/io/IOException
    //   219	223	244	java/io/IOException
    //   226	231	244	java/io/IOException
    //   148	152	276	com/google/f/x
    //   182	187	276	com/google/f/x
    //   200	204	276	com/google/f/x
    //   206	211	276	com/google/f/x
    //   212	216	276	com/google/f/x
    //   219	223	276	com/google/f/x
    //   226	231	276	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int j = -1;
    if (i != j) {
      return i;
    }
    long l1 = c;
    long l2 = 0L;
    int k = 0;
    boolean bool = l1 < l2;
    if (bool)
    {
      int m = 1;
      i = h.computeInt64Size(m, l1);
      k = 0 + i;
    }
    i = a;
    j = 2;
    if (i == j)
    {
      String str = c();
      i = h.computeStringSize(j, str);
      k += i;
    }
    memoizedSerializedSize = k;
    return k;
  }
  
  public final void writeTo(h paramh)
  {
    long l1 = c;
    long l2 = 0L;
    boolean bool = l1 < l2;
    if (bool)
    {
      int i = 1;
      paramh.writeInt64(i, l1);
    }
    int j = a;
    int k = 2;
    if (j == k)
    {
      String str = c();
      paramh.writeString(k, str);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.models.input.InputSpan.Log
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */