package com.truecaller.api.services.messenger.v1.models.input;

import com.google.f.ae;
import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;

public final class InputMessageContent
  extends q
  implements f
{
  private static final InputMessageContent e;
  private static volatile ah f;
  private int a = 0;
  private Object b;
  private String c = "";
  private String d = "";
  
  static
  {
    InputMessageContent localInputMessageContent = new com/truecaller/api/services/messenger/v1/models/input/InputMessageContent;
    localInputMessageContent.<init>();
    e = localInputMessageContent;
    localInputMessageContent.makeImmutable();
  }
  
  public static InputMessageContent.c a()
  {
    return (InputMessageContent.c)e.toBuilder();
  }
  
  public static InputMessageContent b()
  {
    return e;
  }
  
  public static ah c()
  {
    return e.getParserForType();
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 59	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 65	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: bipush 6
    //   20: istore 6
    //   22: iconst_5
    //   23: istore 7
    //   25: iconst_4
    //   26: istore 8
    //   28: iconst_3
    //   29: istore 9
    //   31: iconst_2
    //   32: istore 10
    //   34: iconst_0
    //   35: istore 11
    //   37: aconst_null
    //   38: astore 12
    //   40: iconst_0
    //   41: istore 13
    //   43: iconst_1
    //   44: istore 14
    //   46: iload 5
    //   48: tableswitch	default:+48->96, 1:+1341->1389, 2:+1337->1385, 3:+1335->1383, 4:+1324->1372, 5:+845->893, 6:+110->158, 7:+841->889, 8:+58->106
    //   96: new 69	java/lang/UnsupportedOperationException
    //   99: astore_1
    //   100: aload_1
    //   101: invokespecial 70	java/lang/UnsupportedOperationException:<init>	()V
    //   104: aload_1
    //   105: athrow
    //   106: getstatic 72	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:f	Lcom/google/f/ah;
    //   109: astore_1
    //   110: aload_1
    //   111: ifnonnull +43 -> 154
    //   114: ldc 2
    //   116: astore_1
    //   117: aload_1
    //   118: monitorenter
    //   119: getstatic 72	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:f	Lcom/google/f/ah;
    //   122: astore_2
    //   123: aload_2
    //   124: ifnonnull +20 -> 144
    //   127: new 74	com/google/f/q$b
    //   130: astore_2
    //   131: getstatic 23	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:e	Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent;
    //   134: astore_3
    //   135: aload_2
    //   136: aload_3
    //   137: invokespecial 77	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   140: aload_2
    //   141: putstatic 72	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:f	Lcom/google/f/ah;
    //   144: aload_1
    //   145: monitorexit
    //   146: goto +8 -> 154
    //   149: astore_2
    //   150: aload_1
    //   151: monitorexit
    //   152: aload_2
    //   153: athrow
    //   154: getstatic 72	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:f	Lcom/google/f/ah;
    //   157: areturn
    //   158: aload_2
    //   159: checkcast 79	com/google/f/g
    //   162: astore_2
    //   163: aload_3
    //   164: checkcast 81	com/google/f/n
    //   167: astore_3
    //   168: iload 13
    //   170: ifne +719 -> 889
    //   173: aload_2
    //   174: invokevirtual 84	com/google/f/g:readTag	()I
    //   177: istore 5
    //   179: iload 5
    //   181: ifeq +646 -> 827
    //   184: bipush 10
    //   186: istore 15
    //   188: iload 5
    //   190: iload 15
    //   192: if_icmpeq +622 -> 814
    //   195: bipush 18
    //   197: istore 15
    //   199: iload 5
    //   201: iload 15
    //   203: if_icmpeq +507 -> 710
    //   206: bipush 26
    //   208: istore 15
    //   210: iload 5
    //   212: iload 15
    //   214: if_icmpeq +392 -> 606
    //   217: bipush 34
    //   219: istore 15
    //   221: iload 5
    //   223: iload 15
    //   225: if_icmpeq +277 -> 502
    //   228: bipush 42
    //   230: istore 15
    //   232: iload 5
    //   234: iload 15
    //   236: if_icmpeq +162 -> 398
    //   239: bipush 50
    //   241: istore 15
    //   243: iload 5
    //   245: iload 15
    //   247: if_icmpeq +47 -> 294
    //   250: sipush 7994
    //   253: istore 15
    //   255: iload 5
    //   257: iload 15
    //   259: if_icmpeq +22 -> 281
    //   262: aload_2
    //   263: iload 5
    //   265: invokevirtual 95	com/google/f/g:skipField	(I)Z
    //   268: istore 5
    //   270: iload 5
    //   272: ifne -104 -> 168
    //   275: iconst_1
    //   276: istore 13
    //   278: goto -110 -> 168
    //   281: aload_2
    //   282: invokevirtual 99	com/google/f/g:readStringRequireUtf8	()Ljava/lang/String;
    //   285: astore_1
    //   286: aload_0
    //   287: aload_1
    //   288: putfield 35	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:d	Ljava/lang/String;
    //   291: goto -123 -> 168
    //   294: aload_0
    //   295: getfield 29	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:a	I
    //   298: istore 5
    //   300: iload 5
    //   302: iload 6
    //   304: if_icmpne +26 -> 330
    //   307: aload_0
    //   308: getfield 43	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:b	Ljava/lang/Object;
    //   311: astore_1
    //   312: aload_1
    //   313: checkcast 101	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a
    //   316: astore_1
    //   317: aload_1
    //   318: invokevirtual 102	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a:toBuilder	()Lcom/google/f/q$a;
    //   321: astore_1
    //   322: aload_1
    //   323: checkcast 104	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a$a
    //   326: astore_1
    //   327: goto +8 -> 335
    //   330: iconst_0
    //   331: istore 5
    //   333: aconst_null
    //   334: astore_1
    //   335: invokestatic 106	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a:b	()Lcom/google/f/ah;
    //   338: astore 16
    //   340: aload_2
    //   341: aload 16
    //   343: aload_3
    //   344: invokevirtual 110	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   347: astore 16
    //   349: aload_0
    //   350: aload 16
    //   352: putfield 43	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:b	Ljava/lang/Object;
    //   355: aload_1
    //   356: ifnull +33 -> 389
    //   359: aload_0
    //   360: getfield 43	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:b	Ljava/lang/Object;
    //   363: astore 16
    //   365: aload 16
    //   367: checkcast 101	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a
    //   370: astore 16
    //   372: aload_1
    //   373: aload 16
    //   375: invokevirtual 114	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   378: pop
    //   379: aload_1
    //   380: invokevirtual 118	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a$a:buildPartial	()Lcom/google/f/q;
    //   383: astore_1
    //   384: aload_0
    //   385: aload_1
    //   386: putfield 43	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:b	Ljava/lang/Object;
    //   389: aload_0
    //   390: iload 6
    //   392: putfield 29	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:a	I
    //   395: goto -227 -> 168
    //   398: aload_0
    //   399: getfield 29	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:a	I
    //   402: istore 5
    //   404: iload 5
    //   406: iload 7
    //   408: if_icmpne +26 -> 434
    //   411: aload_0
    //   412: getfield 43	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:b	Ljava/lang/Object;
    //   415: astore_1
    //   416: aload_1
    //   417: checkcast 120	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j
    //   420: astore_1
    //   421: aload_1
    //   422: invokevirtual 121	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j:toBuilder	()Lcom/google/f/q$a;
    //   425: astore_1
    //   426: aload_1
    //   427: checkcast 123	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j$a
    //   430: astore_1
    //   431: goto +8 -> 439
    //   434: iconst_0
    //   435: istore 5
    //   437: aconst_null
    //   438: astore_1
    //   439: invokestatic 124	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j:b	()Lcom/google/f/ah;
    //   442: astore 16
    //   444: aload_2
    //   445: aload 16
    //   447: aload_3
    //   448: invokevirtual 110	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   451: astore 16
    //   453: aload_0
    //   454: aload 16
    //   456: putfield 43	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:b	Ljava/lang/Object;
    //   459: aload_1
    //   460: ifnull +33 -> 493
    //   463: aload_0
    //   464: getfield 43	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:b	Ljava/lang/Object;
    //   467: astore 16
    //   469: aload 16
    //   471: checkcast 120	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j
    //   474: astore 16
    //   476: aload_1
    //   477: aload 16
    //   479: invokevirtual 125	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   482: pop
    //   483: aload_1
    //   484: invokevirtual 126	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j$a:buildPartial	()Lcom/google/f/q;
    //   487: astore_1
    //   488: aload_0
    //   489: aload_1
    //   490: putfield 43	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:b	Ljava/lang/Object;
    //   493: aload_0
    //   494: iload 7
    //   496: putfield 29	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:a	I
    //   499: goto -331 -> 168
    //   502: aload_0
    //   503: getfield 29	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:a	I
    //   506: istore 5
    //   508: iload 5
    //   510: iload 8
    //   512: if_icmpne +26 -> 538
    //   515: aload_0
    //   516: getfield 43	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:b	Ljava/lang/Object;
    //   519: astore_1
    //   520: aload_1
    //   521: checkcast 128	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$f
    //   524: astore_1
    //   525: aload_1
    //   526: invokevirtual 129	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$f:toBuilder	()Lcom/google/f/q$a;
    //   529: astore_1
    //   530: aload_1
    //   531: checkcast 131	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$f$a
    //   534: astore_1
    //   535: goto +8 -> 543
    //   538: iconst_0
    //   539: istore 5
    //   541: aconst_null
    //   542: astore_1
    //   543: invokestatic 133	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$f:a	()Lcom/google/f/ah;
    //   546: astore 16
    //   548: aload_2
    //   549: aload 16
    //   551: aload_3
    //   552: invokevirtual 110	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   555: astore 16
    //   557: aload_0
    //   558: aload 16
    //   560: putfield 43	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:b	Ljava/lang/Object;
    //   563: aload_1
    //   564: ifnull +33 -> 597
    //   567: aload_0
    //   568: getfield 43	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:b	Ljava/lang/Object;
    //   571: astore 16
    //   573: aload 16
    //   575: checkcast 128	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$f
    //   578: astore 16
    //   580: aload_1
    //   581: aload 16
    //   583: invokevirtual 134	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$f$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   586: pop
    //   587: aload_1
    //   588: invokevirtual 135	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$f$a:buildPartial	()Lcom/google/f/q;
    //   591: astore_1
    //   592: aload_0
    //   593: aload_1
    //   594: putfield 43	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:b	Ljava/lang/Object;
    //   597: aload_0
    //   598: iload 8
    //   600: putfield 29	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:a	I
    //   603: goto -435 -> 168
    //   606: aload_0
    //   607: getfield 29	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:a	I
    //   610: istore 5
    //   612: iload 5
    //   614: iload 9
    //   616: if_icmpne +26 -> 642
    //   619: aload_0
    //   620: getfield 43	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:b	Ljava/lang/Object;
    //   623: astore_1
    //   624: aload_1
    //   625: checkcast 137	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$h
    //   628: astore_1
    //   629: aload_1
    //   630: invokevirtual 138	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$h:toBuilder	()Lcom/google/f/q$a;
    //   633: astore_1
    //   634: aload_1
    //   635: checkcast 140	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$h$a
    //   638: astore_1
    //   639: goto +8 -> 647
    //   642: iconst_0
    //   643: istore 5
    //   645: aconst_null
    //   646: astore_1
    //   647: invokestatic 141	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$h:b	()Lcom/google/f/ah;
    //   650: astore 16
    //   652: aload_2
    //   653: aload 16
    //   655: aload_3
    //   656: invokevirtual 110	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   659: astore 16
    //   661: aload_0
    //   662: aload 16
    //   664: putfield 43	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:b	Ljava/lang/Object;
    //   667: aload_1
    //   668: ifnull +33 -> 701
    //   671: aload_0
    //   672: getfield 43	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:b	Ljava/lang/Object;
    //   675: astore 16
    //   677: aload 16
    //   679: checkcast 137	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$h
    //   682: astore 16
    //   684: aload_1
    //   685: aload 16
    //   687: invokevirtual 142	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$h$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   690: pop
    //   691: aload_1
    //   692: invokevirtual 143	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$h$a:buildPartial	()Lcom/google/f/q;
    //   695: astore_1
    //   696: aload_0
    //   697: aload_1
    //   698: putfield 43	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:b	Ljava/lang/Object;
    //   701: aload_0
    //   702: iload 9
    //   704: putfield 29	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:a	I
    //   707: goto -539 -> 168
    //   710: aload_0
    //   711: getfield 29	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:a	I
    //   714: istore 5
    //   716: iload 5
    //   718: iload 10
    //   720: if_icmpne +26 -> 746
    //   723: aload_0
    //   724: getfield 43	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:b	Ljava/lang/Object;
    //   727: astore_1
    //   728: aload_1
    //   729: checkcast 145	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$d
    //   732: astore_1
    //   733: aload_1
    //   734: invokevirtual 146	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$d:toBuilder	()Lcom/google/f/q$a;
    //   737: astore_1
    //   738: aload_1
    //   739: checkcast 148	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$d$a
    //   742: astore_1
    //   743: goto +8 -> 751
    //   746: iconst_0
    //   747: istore 5
    //   749: aconst_null
    //   750: astore_1
    //   751: invokestatic 149	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$d:b	()Lcom/google/f/ah;
    //   754: astore 16
    //   756: aload_2
    //   757: aload 16
    //   759: aload_3
    //   760: invokevirtual 110	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   763: astore 16
    //   765: aload_0
    //   766: aload 16
    //   768: putfield 43	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:b	Ljava/lang/Object;
    //   771: aload_1
    //   772: ifnull +33 -> 805
    //   775: aload_0
    //   776: getfield 43	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:b	Ljava/lang/Object;
    //   779: astore 16
    //   781: aload 16
    //   783: checkcast 145	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$d
    //   786: astore 16
    //   788: aload_1
    //   789: aload 16
    //   791: invokevirtual 150	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$d$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   794: pop
    //   795: aload_1
    //   796: invokevirtual 151	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$d$a:buildPartial	()Lcom/google/f/q;
    //   799: astore_1
    //   800: aload_0
    //   801: aload_1
    //   802: putfield 43	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:b	Ljava/lang/Object;
    //   805: aload_0
    //   806: iload 10
    //   808: putfield 29	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:a	I
    //   811: goto -643 -> 168
    //   814: aload_2
    //   815: invokevirtual 99	com/google/f/g:readStringRequireUtf8	()Ljava/lang/String;
    //   818: astore_1
    //   819: aload_0
    //   820: aload_1
    //   821: putfield 33	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:c	Ljava/lang/String;
    //   824: goto -656 -> 168
    //   827: iconst_1
    //   828: istore 13
    //   830: goto -662 -> 168
    //   833: astore_1
    //   834: goto +53 -> 887
    //   837: astore_1
    //   838: new 153	java/lang/RuntimeException
    //   841: astore_2
    //   842: new 155	com/google/f/x
    //   845: astore_3
    //   846: aload_1
    //   847: invokevirtual 160	java/io/IOException:getMessage	()Ljava/lang/String;
    //   850: astore_1
    //   851: aload_3
    //   852: aload_1
    //   853: invokespecial 163	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   856: aload_3
    //   857: aload_0
    //   858: invokevirtual 167	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   861: astore_1
    //   862: aload_2
    //   863: aload_1
    //   864: invokespecial 170	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   867: aload_2
    //   868: athrow
    //   869: astore_1
    //   870: new 153	java/lang/RuntimeException
    //   873: astore_2
    //   874: aload_1
    //   875: aload_0
    //   876: invokevirtual 167	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   879: astore_1
    //   880: aload_2
    //   881: aload_1
    //   882: invokespecial 170	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   885: aload_2
    //   886: athrow
    //   887: aload_1
    //   888: athrow
    //   889: getstatic 23	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:e	Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent;
    //   892: areturn
    //   893: aload_2
    //   894: checkcast 172	com/google/f/q$k
    //   897: astore_2
    //   898: aload_3
    //   899: checkcast 2	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent
    //   902: astore_3
    //   903: aload_0
    //   904: getfield 33	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:c	Ljava/lang/String;
    //   907: invokevirtual 178	java/lang/String:isEmpty	()Z
    //   910: iload 14
    //   912: ixor
    //   913: istore 5
    //   915: aload_0
    //   916: getfield 33	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:c	Ljava/lang/String;
    //   919: astore 12
    //   921: aload_3
    //   922: getfield 33	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:c	Ljava/lang/String;
    //   925: invokevirtual 178	java/lang/String:isEmpty	()Z
    //   928: iload 14
    //   930: ixor
    //   931: istore 15
    //   933: aload_3
    //   934: getfield 33	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:c	Ljava/lang/String;
    //   937: astore 17
    //   939: aload_2
    //   940: iload 5
    //   942: aload 12
    //   944: iload 15
    //   946: aload 17
    //   948: invokeinterface 182 5 0
    //   953: astore_1
    //   954: aload_0
    //   955: aload_1
    //   956: putfield 33	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:c	Ljava/lang/String;
    //   959: aload_0
    //   960: getfield 35	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:d	Ljava/lang/String;
    //   963: invokevirtual 178	java/lang/String:isEmpty	()Z
    //   966: iload 14
    //   968: ixor
    //   969: istore 5
    //   971: aload_0
    //   972: getfield 35	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:d	Ljava/lang/String;
    //   975: astore 12
    //   977: aload_3
    //   978: getfield 35	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:d	Ljava/lang/String;
    //   981: astore 16
    //   983: aload 16
    //   985: invokevirtual 178	java/lang/String:isEmpty	()Z
    //   988: iload 14
    //   990: ixor
    //   991: istore 15
    //   993: aload_3
    //   994: getfield 35	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:d	Ljava/lang/String;
    //   997: astore 17
    //   999: aload_2
    //   1000: iload 5
    //   1002: aload 12
    //   1004: iload 15
    //   1006: aload 17
    //   1008: invokeinterface 182 5 0
    //   1013: astore_1
    //   1014: aload_0
    //   1015: aload_1
    //   1016: putfield 35	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:d	Ljava/lang/String;
    //   1019: getstatic 184	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$1:b	[I
    //   1022: astore_1
    //   1023: aload_3
    //   1024: getfield 29	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:a	I
    //   1027: invokestatic 190	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$AttachmentCase:forNumber	(I)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$AttachmentCase;
    //   1030: astore 12
    //   1032: aload 12
    //   1034: invokevirtual 191	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$AttachmentCase:ordinal	()I
    //   1037: istore 11
    //   1039: aload_1
    //   1040: iload 11
    //   1042: iaload
    //   1043: istore 5
    //   1045: iload 5
    //   1047: tableswitch	default:+37->1084, 1:+253->1300, 2:+206->1253, 3:+159->1206, 4:+112->1159, 5:+65->1112, 6:+40->1087
    //   1084: goto +260 -> 1344
    //   1087: aload_0
    //   1088: getfield 29	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:a	I
    //   1091: istore 5
    //   1093: iload 5
    //   1095: ifeq +6 -> 1101
    //   1098: iconst_1
    //   1099: istore 13
    //   1101: aload_2
    //   1102: iload 13
    //   1104: invokeinterface 195 2 0
    //   1109: goto +235 -> 1344
    //   1112: aload_0
    //   1113: getfield 29	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:a	I
    //   1116: istore 5
    //   1118: iload 5
    //   1120: iload 6
    //   1122: if_icmpne +6 -> 1128
    //   1125: iconst_1
    //   1126: istore 13
    //   1128: aload_0
    //   1129: getfield 43	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:b	Ljava/lang/Object;
    //   1132: astore_1
    //   1133: aload_3
    //   1134: getfield 43	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:b	Ljava/lang/Object;
    //   1137: astore 4
    //   1139: aload_2
    //   1140: iload 13
    //   1142: aload_1
    //   1143: aload 4
    //   1145: invokeinterface 199 4 0
    //   1150: astore_1
    //   1151: aload_0
    //   1152: aload_1
    //   1153: putfield 43	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:b	Ljava/lang/Object;
    //   1156: goto +188 -> 1344
    //   1159: aload_0
    //   1160: getfield 29	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:a	I
    //   1163: istore 5
    //   1165: iload 5
    //   1167: iload 7
    //   1169: if_icmpne +6 -> 1175
    //   1172: iconst_1
    //   1173: istore 13
    //   1175: aload_0
    //   1176: getfield 43	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:b	Ljava/lang/Object;
    //   1179: astore_1
    //   1180: aload_3
    //   1181: getfield 43	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:b	Ljava/lang/Object;
    //   1184: astore 4
    //   1186: aload_2
    //   1187: iload 13
    //   1189: aload_1
    //   1190: aload 4
    //   1192: invokeinterface 199 4 0
    //   1197: astore_1
    //   1198: aload_0
    //   1199: aload_1
    //   1200: putfield 43	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:b	Ljava/lang/Object;
    //   1203: goto +141 -> 1344
    //   1206: aload_0
    //   1207: getfield 29	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:a	I
    //   1210: istore 5
    //   1212: iload 5
    //   1214: iload 8
    //   1216: if_icmpne +6 -> 1222
    //   1219: iconst_1
    //   1220: istore 13
    //   1222: aload_0
    //   1223: getfield 43	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:b	Ljava/lang/Object;
    //   1226: astore_1
    //   1227: aload_3
    //   1228: getfield 43	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:b	Ljava/lang/Object;
    //   1231: astore 4
    //   1233: aload_2
    //   1234: iload 13
    //   1236: aload_1
    //   1237: aload 4
    //   1239: invokeinterface 199 4 0
    //   1244: astore_1
    //   1245: aload_0
    //   1246: aload_1
    //   1247: putfield 43	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:b	Ljava/lang/Object;
    //   1250: goto +94 -> 1344
    //   1253: aload_0
    //   1254: getfield 29	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:a	I
    //   1257: istore 5
    //   1259: iload 5
    //   1261: iload 9
    //   1263: if_icmpne +6 -> 1269
    //   1266: iconst_1
    //   1267: istore 13
    //   1269: aload_0
    //   1270: getfield 43	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:b	Ljava/lang/Object;
    //   1273: astore_1
    //   1274: aload_3
    //   1275: getfield 43	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:b	Ljava/lang/Object;
    //   1278: astore 4
    //   1280: aload_2
    //   1281: iload 13
    //   1283: aload_1
    //   1284: aload 4
    //   1286: invokeinterface 199 4 0
    //   1291: astore_1
    //   1292: aload_0
    //   1293: aload_1
    //   1294: putfield 43	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:b	Ljava/lang/Object;
    //   1297: goto +47 -> 1344
    //   1300: aload_0
    //   1301: getfield 29	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:a	I
    //   1304: istore 5
    //   1306: iload 5
    //   1308: iload 10
    //   1310: if_icmpne +6 -> 1316
    //   1313: iconst_1
    //   1314: istore 13
    //   1316: aload_0
    //   1317: getfield 43	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:b	Ljava/lang/Object;
    //   1320: astore_1
    //   1321: aload_3
    //   1322: getfield 43	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:b	Ljava/lang/Object;
    //   1325: astore 4
    //   1327: aload_2
    //   1328: iload 13
    //   1330: aload_1
    //   1331: aload 4
    //   1333: invokeinterface 199 4 0
    //   1338: astore_1
    //   1339: aload_0
    //   1340: aload_1
    //   1341: putfield 43	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:b	Ljava/lang/Object;
    //   1344: getstatic 205	com/google/f/q$i:INSTANCE	Lcom/google/f/q$i;
    //   1347: astore_1
    //   1348: aload_2
    //   1349: aload_1
    //   1350: if_acmpne +20 -> 1370
    //   1353: aload_3
    //   1354: getfield 29	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:a	I
    //   1357: istore 5
    //   1359: iload 5
    //   1361: ifeq +9 -> 1370
    //   1364: aload_0
    //   1365: iload 5
    //   1367: putfield 29	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:a	I
    //   1370: aload_0
    //   1371: areturn
    //   1372: new 41	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$c
    //   1375: astore_1
    //   1376: aload_1
    //   1377: iconst_0
    //   1378: invokespecial 208	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$c:<init>	(B)V
    //   1381: aload_1
    //   1382: areturn
    //   1383: aconst_null
    //   1384: areturn
    //   1385: getstatic 23	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:e	Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent;
    //   1388: areturn
    //   1389: new 2	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent
    //   1392: astore_1
    //   1393: aload_1
    //   1394: invokespecial 21	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:<init>	()V
    //   1397: aload_1
    //   1398: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1399	0	this	InputMessageContent
    //   0	1399	1	paramj	com.google.f.q.j
    //   0	1399	2	paramObject1	Object
    //   0	1399	3	paramObject2	Object
    //   3	1329	4	localObject1	Object
    //   9	255	5	i	int
    //   268	3	5	bool1	boolean
    //   298	450	5	j	int
    //   913	88	5	bool2	boolean
    //   1043	323	5	k	int
    //   20	1103	6	m	int
    //   23	1147	7	n	int
    //   26	1191	8	i1	int
    //   29	1235	9	i2	int
    //   32	1279	10	i3	int
    //   35	1006	11	i4	int
    //   38	995	12	localObject2	Object
    //   41	1288	13	bool3	boolean
    //   44	947	14	i5	int
    //   186	74	15	i6	int
    //   931	74	15	bool4	boolean
    //   338	646	16	localObject3	Object
    //   937	70	17	str	String
    // Exception table:
    //   from	to	target	type
    //   119	122	149	finally
    //   127	130	149	finally
    //   131	134	149	finally
    //   136	140	149	finally
    //   140	144	149	finally
    //   144	146	149	finally
    //   150	152	149	finally
    //   173	177	833	finally
    //   263	268	833	finally
    //   281	285	833	finally
    //   287	291	833	finally
    //   294	298	833	finally
    //   307	311	833	finally
    //   312	316	833	finally
    //   317	321	833	finally
    //   322	326	833	finally
    //   335	338	833	finally
    //   343	347	833	finally
    //   350	355	833	finally
    //   359	363	833	finally
    //   365	370	833	finally
    //   373	379	833	finally
    //   379	383	833	finally
    //   385	389	833	finally
    //   390	395	833	finally
    //   398	402	833	finally
    //   411	415	833	finally
    //   416	420	833	finally
    //   421	425	833	finally
    //   426	430	833	finally
    //   439	442	833	finally
    //   447	451	833	finally
    //   454	459	833	finally
    //   463	467	833	finally
    //   469	474	833	finally
    //   477	483	833	finally
    //   483	487	833	finally
    //   489	493	833	finally
    //   494	499	833	finally
    //   502	506	833	finally
    //   515	519	833	finally
    //   520	524	833	finally
    //   525	529	833	finally
    //   530	534	833	finally
    //   543	546	833	finally
    //   551	555	833	finally
    //   558	563	833	finally
    //   567	571	833	finally
    //   573	578	833	finally
    //   581	587	833	finally
    //   587	591	833	finally
    //   593	597	833	finally
    //   598	603	833	finally
    //   606	610	833	finally
    //   619	623	833	finally
    //   624	628	833	finally
    //   629	633	833	finally
    //   634	638	833	finally
    //   647	650	833	finally
    //   655	659	833	finally
    //   662	667	833	finally
    //   671	675	833	finally
    //   677	682	833	finally
    //   685	691	833	finally
    //   691	695	833	finally
    //   697	701	833	finally
    //   702	707	833	finally
    //   710	714	833	finally
    //   723	727	833	finally
    //   728	732	833	finally
    //   733	737	833	finally
    //   738	742	833	finally
    //   751	754	833	finally
    //   759	763	833	finally
    //   766	771	833	finally
    //   775	779	833	finally
    //   781	786	833	finally
    //   789	795	833	finally
    //   795	799	833	finally
    //   801	805	833	finally
    //   806	811	833	finally
    //   814	818	833	finally
    //   820	824	833	finally
    //   838	841	833	finally
    //   842	845	833	finally
    //   846	850	833	finally
    //   852	856	833	finally
    //   857	861	833	finally
    //   863	867	833	finally
    //   867	869	833	finally
    //   870	873	833	finally
    //   875	879	833	finally
    //   881	885	833	finally
    //   885	887	833	finally
    //   173	177	837	java/io/IOException
    //   263	268	837	java/io/IOException
    //   281	285	837	java/io/IOException
    //   287	291	837	java/io/IOException
    //   294	298	837	java/io/IOException
    //   307	311	837	java/io/IOException
    //   312	316	837	java/io/IOException
    //   317	321	837	java/io/IOException
    //   322	326	837	java/io/IOException
    //   335	338	837	java/io/IOException
    //   343	347	837	java/io/IOException
    //   350	355	837	java/io/IOException
    //   359	363	837	java/io/IOException
    //   365	370	837	java/io/IOException
    //   373	379	837	java/io/IOException
    //   379	383	837	java/io/IOException
    //   385	389	837	java/io/IOException
    //   390	395	837	java/io/IOException
    //   398	402	837	java/io/IOException
    //   411	415	837	java/io/IOException
    //   416	420	837	java/io/IOException
    //   421	425	837	java/io/IOException
    //   426	430	837	java/io/IOException
    //   439	442	837	java/io/IOException
    //   447	451	837	java/io/IOException
    //   454	459	837	java/io/IOException
    //   463	467	837	java/io/IOException
    //   469	474	837	java/io/IOException
    //   477	483	837	java/io/IOException
    //   483	487	837	java/io/IOException
    //   489	493	837	java/io/IOException
    //   494	499	837	java/io/IOException
    //   502	506	837	java/io/IOException
    //   515	519	837	java/io/IOException
    //   520	524	837	java/io/IOException
    //   525	529	837	java/io/IOException
    //   530	534	837	java/io/IOException
    //   543	546	837	java/io/IOException
    //   551	555	837	java/io/IOException
    //   558	563	837	java/io/IOException
    //   567	571	837	java/io/IOException
    //   573	578	837	java/io/IOException
    //   581	587	837	java/io/IOException
    //   587	591	837	java/io/IOException
    //   593	597	837	java/io/IOException
    //   598	603	837	java/io/IOException
    //   606	610	837	java/io/IOException
    //   619	623	837	java/io/IOException
    //   624	628	837	java/io/IOException
    //   629	633	837	java/io/IOException
    //   634	638	837	java/io/IOException
    //   647	650	837	java/io/IOException
    //   655	659	837	java/io/IOException
    //   662	667	837	java/io/IOException
    //   671	675	837	java/io/IOException
    //   677	682	837	java/io/IOException
    //   685	691	837	java/io/IOException
    //   691	695	837	java/io/IOException
    //   697	701	837	java/io/IOException
    //   702	707	837	java/io/IOException
    //   710	714	837	java/io/IOException
    //   723	727	837	java/io/IOException
    //   728	732	837	java/io/IOException
    //   733	737	837	java/io/IOException
    //   738	742	837	java/io/IOException
    //   751	754	837	java/io/IOException
    //   759	763	837	java/io/IOException
    //   766	771	837	java/io/IOException
    //   775	779	837	java/io/IOException
    //   781	786	837	java/io/IOException
    //   789	795	837	java/io/IOException
    //   795	799	837	java/io/IOException
    //   801	805	837	java/io/IOException
    //   806	811	837	java/io/IOException
    //   814	818	837	java/io/IOException
    //   820	824	837	java/io/IOException
    //   173	177	869	com/google/f/x
    //   263	268	869	com/google/f/x
    //   281	285	869	com/google/f/x
    //   287	291	869	com/google/f/x
    //   294	298	869	com/google/f/x
    //   307	311	869	com/google/f/x
    //   312	316	869	com/google/f/x
    //   317	321	869	com/google/f/x
    //   322	326	869	com/google/f/x
    //   335	338	869	com/google/f/x
    //   343	347	869	com/google/f/x
    //   350	355	869	com/google/f/x
    //   359	363	869	com/google/f/x
    //   365	370	869	com/google/f/x
    //   373	379	869	com/google/f/x
    //   379	383	869	com/google/f/x
    //   385	389	869	com/google/f/x
    //   390	395	869	com/google/f/x
    //   398	402	869	com/google/f/x
    //   411	415	869	com/google/f/x
    //   416	420	869	com/google/f/x
    //   421	425	869	com/google/f/x
    //   426	430	869	com/google/f/x
    //   439	442	869	com/google/f/x
    //   447	451	869	com/google/f/x
    //   454	459	869	com/google/f/x
    //   463	467	869	com/google/f/x
    //   469	474	869	com/google/f/x
    //   477	483	869	com/google/f/x
    //   483	487	869	com/google/f/x
    //   489	493	869	com/google/f/x
    //   494	499	869	com/google/f/x
    //   502	506	869	com/google/f/x
    //   515	519	869	com/google/f/x
    //   520	524	869	com/google/f/x
    //   525	529	869	com/google/f/x
    //   530	534	869	com/google/f/x
    //   543	546	869	com/google/f/x
    //   551	555	869	com/google/f/x
    //   558	563	869	com/google/f/x
    //   567	571	869	com/google/f/x
    //   573	578	869	com/google/f/x
    //   581	587	869	com/google/f/x
    //   587	591	869	com/google/f/x
    //   593	597	869	com/google/f/x
    //   598	603	869	com/google/f/x
    //   606	610	869	com/google/f/x
    //   619	623	869	com/google/f/x
    //   624	628	869	com/google/f/x
    //   629	633	869	com/google/f/x
    //   634	638	869	com/google/f/x
    //   647	650	869	com/google/f/x
    //   655	659	869	com/google/f/x
    //   662	667	869	com/google/f/x
    //   671	675	869	com/google/f/x
    //   677	682	869	com/google/f/x
    //   685	691	869	com/google/f/x
    //   691	695	869	com/google/f/x
    //   697	701	869	com/google/f/x
    //   702	707	869	com/google/f/x
    //   710	714	869	com/google/f/x
    //   723	727	869	com/google/f/x
    //   728	732	869	com/google/f/x
    //   733	737	869	com/google/f/x
    //   738	742	869	com/google/f/x
    //   751	754	869	com/google/f/x
    //   759	763	869	com/google/f/x
    //   766	771	869	com/google/f/x
    //   775	779	869	com/google/f/x
    //   781	786	869	com/google/f/x
    //   789	795	869	com/google/f/x
    //   795	799	869	com/google/f/x
    //   801	805	869	com/google/f/x
    //   806	811	869	com/google/f/x
    //   814	818	869	com/google/f/x
    //   820	824	869	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int m = -1;
    if (i != m) {
      return i;
    }
    Object localObject = c;
    boolean bool1 = ((String)localObject).isEmpty();
    m = 0;
    String str;
    if (!bool1)
    {
      str = c;
      j = h.computeStringSize(1, str);
      m = 0 + j;
    }
    int j = a;
    int n = 2;
    if (j == n)
    {
      localObject = (InputMessageContent.d)b;
      j = h.computeMessageSize(n, (ae)localObject);
      m += j;
    }
    j = a;
    n = 3;
    if (j == n)
    {
      localObject = (InputMessageContent.h)b;
      j = h.computeMessageSize(n, (ae)localObject);
      m += j;
    }
    j = a;
    n = 4;
    if (j == n)
    {
      localObject = (InputMessageContent.f)b;
      j = h.computeMessageSize(n, (ae)localObject);
      m += j;
    }
    j = a;
    n = 5;
    if (j == n)
    {
      localObject = (InputMessageContent.j)b;
      j = h.computeMessageSize(n, (ae)localObject);
      m += j;
    }
    j = a;
    n = 6;
    if (j == n)
    {
      localObject = (InputMessageContent.a)b;
      j = h.computeMessageSize(n, (ae)localObject);
      m += j;
    }
    localObject = d;
    boolean bool2 = ((String)localObject).isEmpty();
    if (!bool2)
    {
      str = d;
      int k = h.computeStringSize(999, str);
      m += k;
    }
    memoizedSerializedSize = m;
    return m;
  }
  
  public final void writeTo(h paramh)
  {
    Object localObject = c;
    int i = ((String)localObject).isEmpty();
    String str;
    if (i == 0)
    {
      i = 1;
      str = c;
      paramh.writeString(i, str);
    }
    int j = a;
    int m = 2;
    if (j == m)
    {
      localObject = (InputMessageContent.d)b;
      paramh.writeMessage(m, (ae)localObject);
    }
    j = a;
    m = 3;
    if (j == m)
    {
      localObject = (InputMessageContent.h)b;
      paramh.writeMessage(m, (ae)localObject);
    }
    j = a;
    m = 4;
    if (j == m)
    {
      localObject = (InputMessageContent.f)b;
      paramh.writeMessage(m, (ae)localObject);
    }
    j = a;
    m = 5;
    if (j == m)
    {
      localObject = (InputMessageContent.j)b;
      paramh.writeMessage(m, (ae)localObject);
    }
    j = a;
    m = 6;
    if (j == m)
    {
      localObject = (InputMessageContent.a)b;
      paramh.writeMessage(m, (ae)localObject);
    }
    localObject = d;
    boolean bool = ((String)localObject).isEmpty();
    if (!bool)
    {
      int k = 999;
      str = d;
      paramh.writeString(k, str);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.models.input.InputMessageContent
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */