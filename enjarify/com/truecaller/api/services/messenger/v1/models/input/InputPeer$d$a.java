package com.truecaller.api.services.messenger.v1.models.input;

import com.google.f.q.a;

public final class InputPeer$d$a
  extends q.a
  implements InputPeer.e
{
  private InputPeer$d$a()
  {
    super(locald);
  }
  
  public final a a(String paramString)
  {
    copyOnWrite();
    InputPeer.d.a((InputPeer.d)instance, paramString);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.models.input.InputPeer.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */