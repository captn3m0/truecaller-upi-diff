package com.truecaller.api.services.messenger.v1.models.input;

import com.google.f.w.c;
import com.google.f.w.d;

public enum InputReportType
  implements w.c
{
  public static final int READ_VALUE = 1;
  public static final int RECEIVED_VALUE = 2;
  public static final int UNKNOWN_REPORT_TYPE_VALUE;
  private static final w.d internalValueMap;
  private final int value;
  
  static
  {
    Object localObject = new com/truecaller/api/services/messenger/v1/models/input/InputReportType;
    ((InputReportType)localObject).<init>("UNKNOWN_REPORT_TYPE", 0, 0);
    UNKNOWN_REPORT_TYPE = (InputReportType)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/models/input/InputReportType;
    int i = 1;
    ((InputReportType)localObject).<init>("READ", i, i);
    READ = (InputReportType)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/models/input/InputReportType;
    int j = 2;
    ((InputReportType)localObject).<init>("RECEIVED", j, j);
    RECEIVED = (InputReportType)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/models/input/InputReportType;
    int k = 3;
    ((InputReportType)localObject).<init>("UNRECOGNIZED", k, -1);
    UNRECOGNIZED = (InputReportType)localObject;
    localObject = new InputReportType[4];
    InputReportType localInputReportType = UNKNOWN_REPORT_TYPE;
    localObject[0] = localInputReportType;
    localInputReportType = READ;
    localObject[i] = localInputReportType;
    localInputReportType = RECEIVED;
    localObject[j] = localInputReportType;
    localInputReportType = UNRECOGNIZED;
    localObject[k] = localInputReportType;
    $VALUES = (InputReportType[])localObject;
    localObject = new com/truecaller/api/services/messenger/v1/models/input/InputReportType$1;
    ((InputReportType.1)localObject).<init>();
    internalValueMap = (w.d)localObject;
  }
  
  private InputReportType(int paramInt1)
  {
    value = paramInt1;
  }
  
  public static InputReportType forNumber(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return null;
    case 2: 
      return RECEIVED;
    case 1: 
      return READ;
    }
    return UNKNOWN_REPORT_TYPE;
  }
  
  public static w.d internalGetValueMap()
  {
    return internalValueMap;
  }
  
  public static InputReportType valueOf(int paramInt)
  {
    return forNumber(paramInt);
  }
  
  public final int getNumber()
  {
    return value;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.models.input.InputReportType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */