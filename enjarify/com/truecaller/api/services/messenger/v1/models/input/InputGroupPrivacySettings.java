package com.truecaller.api.services.messenger.v1.models.input;

import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;

public final class InputGroupPrivacySettings
  extends q
  implements e
{
  private static final InputGroupPrivacySettings c;
  private static volatile ah d;
  private int a = 0;
  private Object b;
  
  static
  {
    InputGroupPrivacySettings localInputGroupPrivacySettings = new com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings;
    localInputGroupPrivacySettings.<init>();
    c = localInputGroupPrivacySettings;
    localInputGroupPrivacySettings.makeImmutable();
  }
  
  public static InputGroupPrivacySettings a()
  {
    return c;
  }
  
  public static ah b()
  {
    return c.getParserForType();
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 35	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 41	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_0
    //   19: istore 6
    //   21: aconst_null
    //   22: astore 4
    //   24: iconst_0
    //   25: istore 7
    //   27: iconst_1
    //   28: istore 8
    //   30: iload 5
    //   32: tableswitch	default:+48->80, 1:+513->545, 2:+509->541, 3:+507->539, 4:+496->528, 5:+336->368, 6:+110->142, 7:+332->364, 8:+58->90
    //   80: new 44	java/lang/UnsupportedOperationException
    //   83: astore_1
    //   84: aload_1
    //   85: invokespecial 45	java/lang/UnsupportedOperationException:<init>	()V
    //   88: aload_1
    //   89: athrow
    //   90: getstatic 47	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings:d	Lcom/google/f/ah;
    //   93: astore_1
    //   94: aload_1
    //   95: ifnonnull +43 -> 138
    //   98: ldc 2
    //   100: astore_1
    //   101: aload_1
    //   102: monitorenter
    //   103: getstatic 47	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings:d	Lcom/google/f/ah;
    //   106: astore_2
    //   107: aload_2
    //   108: ifnonnull +20 -> 128
    //   111: new 49	com/google/f/q$b
    //   114: astore_2
    //   115: getstatic 20	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings:c	Lcom/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings;
    //   118: astore_3
    //   119: aload_2
    //   120: aload_3
    //   121: invokespecial 52	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   124: aload_2
    //   125: putstatic 47	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings:d	Lcom/google/f/ah;
    //   128: aload_1
    //   129: monitorexit
    //   130: goto +8 -> 138
    //   133: astore_2
    //   134: aload_1
    //   135: monitorexit
    //   136: aload_2
    //   137: athrow
    //   138: getstatic 47	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings:d	Lcom/google/f/ah;
    //   141: areturn
    //   142: aload_2
    //   143: checkcast 54	com/google/f/g
    //   146: astore_2
    //   147: aload_3
    //   148: checkcast 56	com/google/f/n
    //   151: astore_3
    //   152: iload 7
    //   154: ifne +210 -> 364
    //   157: aload_2
    //   158: invokevirtual 59	com/google/f/g:readTag	()I
    //   161: istore 5
    //   163: iload 5
    //   165: ifeq +137 -> 302
    //   168: bipush 10
    //   170: istore 9
    //   172: iload 5
    //   174: iload 9
    //   176: if_icmpeq +22 -> 198
    //   179: aload_2
    //   180: iload 5
    //   182: invokevirtual 64	com/google/f/g:skipField	(I)Z
    //   185: istore 5
    //   187: iload 5
    //   189: ifne -37 -> 152
    //   192: iconst_1
    //   193: istore 7
    //   195: goto -43 -> 152
    //   198: aload_0
    //   199: getfield 26	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings:a	I
    //   202: istore 5
    //   204: iload 5
    //   206: iload 8
    //   208: if_icmpne +26 -> 234
    //   211: aload_0
    //   212: getfield 66	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings:b	Ljava/lang/Object;
    //   215: astore_1
    //   216: aload_1
    //   217: checkcast 68	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings$b
    //   220: astore_1
    //   221: aload_1
    //   222: invokevirtual 72	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings$b:toBuilder	()Lcom/google/f/q$a;
    //   225: astore_1
    //   226: aload_1
    //   227: checkcast 74	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings$b$a
    //   230: astore_1
    //   231: goto +8 -> 239
    //   234: iconst_0
    //   235: istore 5
    //   237: aconst_null
    //   238: astore_1
    //   239: invokestatic 76	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings$b:a	()Lcom/google/f/ah;
    //   242: astore 10
    //   244: aload_2
    //   245: aload 10
    //   247: aload_3
    //   248: invokevirtual 80	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   251: astore 10
    //   253: aload_0
    //   254: aload 10
    //   256: putfield 66	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings:b	Ljava/lang/Object;
    //   259: aload_1
    //   260: ifnull +33 -> 293
    //   263: aload_0
    //   264: getfield 66	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings:b	Ljava/lang/Object;
    //   267: astore 10
    //   269: aload 10
    //   271: checkcast 68	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings$b
    //   274: astore 10
    //   276: aload_1
    //   277: aload 10
    //   279: invokevirtual 84	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings$b$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   282: pop
    //   283: aload_1
    //   284: invokevirtual 88	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings$b$a:buildPartial	()Lcom/google/f/q;
    //   287: astore_1
    //   288: aload_0
    //   289: aload_1
    //   290: putfield 66	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings:b	Ljava/lang/Object;
    //   293: aload_0
    //   294: iload 8
    //   296: putfield 26	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings:a	I
    //   299: goto -147 -> 152
    //   302: iconst_1
    //   303: istore 7
    //   305: goto -153 -> 152
    //   308: astore_1
    //   309: goto +53 -> 362
    //   312: astore_1
    //   313: new 90	java/lang/RuntimeException
    //   316: astore_2
    //   317: new 92	com/google/f/x
    //   320: astore_3
    //   321: aload_1
    //   322: invokevirtual 98	java/io/IOException:getMessage	()Ljava/lang/String;
    //   325: astore_1
    //   326: aload_3
    //   327: aload_1
    //   328: invokespecial 101	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   331: aload_3
    //   332: aload_0
    //   333: invokevirtual 105	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   336: astore_1
    //   337: aload_2
    //   338: aload_1
    //   339: invokespecial 108	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   342: aload_2
    //   343: athrow
    //   344: astore_1
    //   345: new 90	java/lang/RuntimeException
    //   348: astore_2
    //   349: aload_1
    //   350: aload_0
    //   351: invokevirtual 105	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   354: astore_1
    //   355: aload_2
    //   356: aload_1
    //   357: invokespecial 108	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   360: aload_2
    //   361: athrow
    //   362: aload_1
    //   363: athrow
    //   364: getstatic 20	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings:c	Lcom/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings;
    //   367: areturn
    //   368: aload_2
    //   369: checkcast 110	com/google/f/q$k
    //   372: astore_2
    //   373: aload_3
    //   374: checkcast 2	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings
    //   377: astore_3
    //   378: getstatic 112	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings$1:b	[I
    //   381: astore_1
    //   382: aload_3
    //   383: getfield 26	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings:a	I
    //   386: invokestatic 118	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings$ValueCase:forNumber	(I)Lcom/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings$ValueCase;
    //   389: astore 4
    //   391: aload 4
    //   393: invokevirtual 119	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings$ValueCase:ordinal	()I
    //   396: istore 6
    //   398: aload_1
    //   399: iload 6
    //   401: iaload
    //   402: istore 5
    //   404: iload 5
    //   406: tableswitch	default:+22->428, 1:+50->456, 2:+25->431
    //   428: goto +72 -> 500
    //   431: aload_0
    //   432: getfield 26	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings:a	I
    //   435: istore 5
    //   437: iload 5
    //   439: ifeq +6 -> 445
    //   442: iconst_1
    //   443: istore 7
    //   445: aload_2
    //   446: iload 7
    //   448: invokeinterface 123 2 0
    //   453: goto +47 -> 500
    //   456: aload_0
    //   457: getfield 26	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings:a	I
    //   460: istore 5
    //   462: iload 5
    //   464: iload 8
    //   466: if_icmpne +6 -> 472
    //   469: iconst_1
    //   470: istore 7
    //   472: aload_0
    //   473: getfield 66	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings:b	Ljava/lang/Object;
    //   476: astore_1
    //   477: aload_3
    //   478: getfield 66	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings:b	Ljava/lang/Object;
    //   481: astore 4
    //   483: aload_2
    //   484: iload 7
    //   486: aload_1
    //   487: aload 4
    //   489: invokeinterface 127 4 0
    //   494: astore_1
    //   495: aload_0
    //   496: aload_1
    //   497: putfield 66	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings:b	Ljava/lang/Object;
    //   500: getstatic 133	com/google/f/q$i:INSTANCE	Lcom/google/f/q$i;
    //   503: astore_1
    //   504: aload_2
    //   505: aload_1
    //   506: if_acmpne +20 -> 526
    //   509: aload_3
    //   510: getfield 26	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings:a	I
    //   513: istore 5
    //   515: iload 5
    //   517: ifeq +9 -> 526
    //   520: aload_0
    //   521: iload 5
    //   523: putfield 26	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings:a	I
    //   526: aload_0
    //   527: areturn
    //   528: new 135	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings$a
    //   531: astore_1
    //   532: aload_1
    //   533: iconst_0
    //   534: invokespecial 138	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings$a:<init>	(B)V
    //   537: aload_1
    //   538: areturn
    //   539: aconst_null
    //   540: areturn
    //   541: getstatic 20	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings:c	Lcom/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings;
    //   544: areturn
    //   545: new 2	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings
    //   548: astore_1
    //   549: aload_1
    //   550: invokespecial 18	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings:<init>	()V
    //   553: aload_1
    //   554: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	555	0	this	InputGroupPrivacySettings
    //   0	555	1	paramj	com.google.f.q.j
    //   0	555	2	paramObject1	Object
    //   0	555	3	paramObject2	Object
    //   3	485	4	localObject1	Object
    //   9	172	5	i	int
    //   185	3	5	bool1	boolean
    //   202	320	5	j	int
    //   19	381	6	k	int
    //   25	460	7	bool2	boolean
    //   28	439	8	m	int
    //   170	7	9	n	int
    //   242	36	10	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   103	106	133	finally
    //   111	114	133	finally
    //   115	118	133	finally
    //   120	124	133	finally
    //   124	128	133	finally
    //   128	130	133	finally
    //   134	136	133	finally
    //   157	161	308	finally
    //   180	185	308	finally
    //   198	202	308	finally
    //   211	215	308	finally
    //   216	220	308	finally
    //   221	225	308	finally
    //   226	230	308	finally
    //   239	242	308	finally
    //   247	251	308	finally
    //   254	259	308	finally
    //   263	267	308	finally
    //   269	274	308	finally
    //   277	283	308	finally
    //   283	287	308	finally
    //   289	293	308	finally
    //   294	299	308	finally
    //   313	316	308	finally
    //   317	320	308	finally
    //   321	325	308	finally
    //   327	331	308	finally
    //   332	336	308	finally
    //   338	342	308	finally
    //   342	344	308	finally
    //   345	348	308	finally
    //   350	354	308	finally
    //   356	360	308	finally
    //   360	362	308	finally
    //   157	161	312	java/io/IOException
    //   180	185	312	java/io/IOException
    //   198	202	312	java/io/IOException
    //   211	215	312	java/io/IOException
    //   216	220	312	java/io/IOException
    //   221	225	312	java/io/IOException
    //   226	230	312	java/io/IOException
    //   239	242	312	java/io/IOException
    //   247	251	312	java/io/IOException
    //   254	259	312	java/io/IOException
    //   263	267	312	java/io/IOException
    //   269	274	312	java/io/IOException
    //   277	283	312	java/io/IOException
    //   283	287	312	java/io/IOException
    //   289	293	312	java/io/IOException
    //   294	299	312	java/io/IOException
    //   157	161	344	com/google/f/x
    //   180	185	344	com/google/f/x
    //   198	202	344	com/google/f/x
    //   211	215	344	com/google/f/x
    //   216	220	344	com/google/f/x
    //   221	225	344	com/google/f/x
    //   226	230	344	com/google/f/x
    //   239	242	344	com/google/f/x
    //   247	251	344	com/google/f/x
    //   254	259	344	com/google/f/x
    //   263	267	344	com/google/f/x
    //   269	274	344	com/google/f/x
    //   277	283	344	com/google/f/x
    //   283	287	344	com/google/f/x
    //   289	293	344	com/google/f/x
    //   294	299	344	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int j = -1;
    if (i != j) {
      return i;
    }
    i = a;
    j = 1;
    int k = 0;
    if (i == j)
    {
      InputGroupPrivacySettings.b localb = (InputGroupPrivacySettings.b)b;
      i = h.computeMessageSize(j, localb);
      k = 0 + i;
    }
    memoizedSerializedSize = k;
    return k;
  }
  
  public final void writeTo(h paramh)
  {
    int i = a;
    int j = 1;
    if (i == j)
    {
      InputGroupPrivacySettings.b localb = (InputGroupPrivacySettings.b)b;
      paramh.writeMessage(j, localb);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.models.input.InputGroupPrivacySettings
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */