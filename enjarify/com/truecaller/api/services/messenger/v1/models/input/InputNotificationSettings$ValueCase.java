package com.truecaller.api.services.messenger.v1.models.input;

import com.google.f.w.c;

public enum InputNotificationSettings$ValueCase
  implements w.c
{
  private final int value;
  
  static
  {
    Object localObject = new com/truecaller/api/services/messenger/v1/models/input/InputNotificationSettings$ValueCase;
    int i = 1;
    ((ValueCase)localObject).<init>("DEFAULT", 0, i);
    DEFAULT = (ValueCase)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/models/input/InputNotificationSettings$ValueCase;
    int j = 2;
    ((ValueCase)localObject).<init>("MUTED", i, j);
    MUTED = (ValueCase)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/models/input/InputNotificationSettings$ValueCase;
    ((ValueCase)localObject).<init>("VALUE_NOT_SET", j, 0);
    VALUE_NOT_SET = (ValueCase)localObject;
    localObject = new ValueCase[3];
    ValueCase localValueCase = DEFAULT;
    localObject[0] = localValueCase;
    localValueCase = MUTED;
    localObject[i] = localValueCase;
    localValueCase = VALUE_NOT_SET;
    localObject[j] = localValueCase;
    $VALUES = (ValueCase[])localObject;
  }
  
  private InputNotificationSettings$ValueCase(int paramInt1)
  {
    value = paramInt1;
  }
  
  public static ValueCase forNumber(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return null;
    case 2: 
      return MUTED;
    case 1: 
      return DEFAULT;
    }
    return VALUE_NOT_SET;
  }
  
  public static ValueCase valueOf(int paramInt)
  {
    return forNumber(paramInt);
  }
  
  public final int getNumber()
  {
    return value;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.models.input.InputNotificationSettings.ValueCase
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */