package com.truecaller.api.services.messenger.v1.models;

import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;

public final class f
  extends q
  implements g
{
  private static final f c;
  private static volatile ah d;
  private Peer a;
  private int b;
  
  static
  {
    f localf = new com/truecaller/api/services/messenger/v1/models/f;
    localf.<init>();
    c = localf;
    localf.makeImmutable();
  }
  
  public static ah c()
  {
    return c.getParserForType();
  }
  
  public final Peer a()
  {
    Peer localPeer = a;
    if (localPeer == null) {
      localPeer = Peer.d();
    }
    return localPeer;
  }
  
  public final int b()
  {
    return b;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 42	com/truecaller/api/services/messenger/v1/models/f$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 48	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_0
    //   19: istore 6
    //   21: aconst_null
    //   22: astore 4
    //   24: iconst_1
    //   25: istore 7
    //   27: iconst_0
    //   28: istore 8
    //   30: iload 5
    //   32: tableswitch	default:+48->80, 1:+483->515, 2:+479->511, 3:+477->509, 4:+466->498, 5:+352->384, 6:+110->142, 7:+348->380, 8:+58->90
    //   80: new 51	java/lang/UnsupportedOperationException
    //   83: astore_1
    //   84: aload_1
    //   85: invokespecial 52	java/lang/UnsupportedOperationException:<init>	()V
    //   88: aload_1
    //   89: athrow
    //   90: getstatic 54	com/truecaller/api/services/messenger/v1/models/f:d	Lcom/google/f/ah;
    //   93: astore_1
    //   94: aload_1
    //   95: ifnonnull +43 -> 138
    //   98: ldc 2
    //   100: astore_1
    //   101: aload_1
    //   102: monitorenter
    //   103: getstatic 54	com/truecaller/api/services/messenger/v1/models/f:d	Lcom/google/f/ah;
    //   106: astore_2
    //   107: aload_2
    //   108: ifnonnull +20 -> 128
    //   111: new 56	com/google/f/q$b
    //   114: astore_2
    //   115: getstatic 20	com/truecaller/api/services/messenger/v1/models/f:c	Lcom/truecaller/api/services/messenger/v1/models/f;
    //   118: astore_3
    //   119: aload_2
    //   120: aload_3
    //   121: invokespecial 59	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   124: aload_2
    //   125: putstatic 54	com/truecaller/api/services/messenger/v1/models/f:d	Lcom/google/f/ah;
    //   128: aload_1
    //   129: monitorexit
    //   130: goto +8 -> 138
    //   133: astore_2
    //   134: aload_1
    //   135: monitorexit
    //   136: aload_2
    //   137: athrow
    //   138: getstatic 54	com/truecaller/api/services/messenger/v1/models/f:d	Lcom/google/f/ah;
    //   141: areturn
    //   142: aload_2
    //   143: checkcast 61	com/google/f/g
    //   146: astore_2
    //   147: aload_3
    //   148: checkcast 63	com/google/f/n
    //   151: astore_3
    //   152: iload 8
    //   154: ifne +226 -> 380
    //   157: aload_2
    //   158: invokevirtual 66	com/google/f/g:readTag	()I
    //   161: istore 5
    //   163: iload 5
    //   165: ifeq +153 -> 318
    //   168: bipush 10
    //   170: istore 9
    //   172: iload 5
    //   174: iload 9
    //   176: if_icmpeq +48 -> 224
    //   179: bipush 16
    //   181: istore 9
    //   183: iload 5
    //   185: iload 9
    //   187: if_icmpeq +22 -> 209
    //   190: aload_2
    //   191: iload 5
    //   193: invokevirtual 72	com/google/f/g:skipField	(I)Z
    //   196: istore 5
    //   198: iload 5
    //   200: ifne -48 -> 152
    //   203: iconst_1
    //   204: istore 8
    //   206: goto -54 -> 152
    //   209: aload_2
    //   210: invokevirtual 75	com/google/f/g:readInt32	()I
    //   213: istore 5
    //   215: aload_0
    //   216: iload 5
    //   218: putfield 37	com/truecaller/api/services/messenger/v1/models/f:b	I
    //   221: goto -69 -> 152
    //   224: aload_0
    //   225: getfield 30	com/truecaller/api/services/messenger/v1/models/f:a	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   228: astore_1
    //   229: aload_1
    //   230: ifnull +21 -> 251
    //   233: aload_0
    //   234: getfield 30	com/truecaller/api/services/messenger/v1/models/f:a	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   237: astore_1
    //   238: aload_1
    //   239: invokevirtual 79	com/truecaller/api/services/messenger/v1/models/Peer:toBuilder	()Lcom/google/f/q$a;
    //   242: astore_1
    //   243: aload_1
    //   244: checkcast 81	com/truecaller/api/services/messenger/v1/models/Peer$a
    //   247: astore_1
    //   248: goto +8 -> 256
    //   251: iconst_0
    //   252: istore 5
    //   254: aconst_null
    //   255: astore_1
    //   256: invokestatic 84	com/truecaller/api/services/messenger/v1/models/Peer:e	()Lcom/google/f/ah;
    //   259: astore 10
    //   261: aload_2
    //   262: aload 10
    //   264: aload_3
    //   265: invokevirtual 88	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   268: astore 10
    //   270: aload 10
    //   272: checkcast 32	com/truecaller/api/services/messenger/v1/models/Peer
    //   275: astore 10
    //   277: aload_0
    //   278: aload 10
    //   280: putfield 30	com/truecaller/api/services/messenger/v1/models/f:a	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   283: aload_1
    //   284: ifnull -132 -> 152
    //   287: aload_0
    //   288: getfield 30	com/truecaller/api/services/messenger/v1/models/f:a	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   291: astore 10
    //   293: aload_1
    //   294: aload 10
    //   296: invokevirtual 92	com/truecaller/api/services/messenger/v1/models/Peer$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   299: pop
    //   300: aload_1
    //   301: invokevirtual 96	com/truecaller/api/services/messenger/v1/models/Peer$a:buildPartial	()Lcom/google/f/q;
    //   304: astore_1
    //   305: aload_1
    //   306: checkcast 32	com/truecaller/api/services/messenger/v1/models/Peer
    //   309: astore_1
    //   310: aload_0
    //   311: aload_1
    //   312: putfield 30	com/truecaller/api/services/messenger/v1/models/f:a	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   315: goto -163 -> 152
    //   318: iconst_1
    //   319: istore 8
    //   321: goto -169 -> 152
    //   324: astore_1
    //   325: goto +53 -> 378
    //   328: astore_1
    //   329: new 98	java/lang/RuntimeException
    //   332: astore_2
    //   333: new 100	com/google/f/x
    //   336: astore_3
    //   337: aload_1
    //   338: invokevirtual 106	java/io/IOException:getMessage	()Ljava/lang/String;
    //   341: astore_1
    //   342: aload_3
    //   343: aload_1
    //   344: invokespecial 109	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   347: aload_3
    //   348: aload_0
    //   349: invokevirtual 113	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   352: astore_1
    //   353: aload_2
    //   354: aload_1
    //   355: invokespecial 116	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   358: aload_2
    //   359: athrow
    //   360: astore_1
    //   361: new 98	java/lang/RuntimeException
    //   364: astore_2
    //   365: aload_1
    //   366: aload_0
    //   367: invokevirtual 113	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   370: astore_1
    //   371: aload_2
    //   372: aload_1
    //   373: invokespecial 116	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   376: aload_2
    //   377: athrow
    //   378: aload_1
    //   379: athrow
    //   380: getstatic 20	com/truecaller/api/services/messenger/v1/models/f:c	Lcom/truecaller/api/services/messenger/v1/models/f;
    //   383: areturn
    //   384: aload_2
    //   385: checkcast 118	com/google/f/q$k
    //   388: astore_2
    //   389: aload_3
    //   390: checkcast 2	com/truecaller/api/services/messenger/v1/models/f
    //   393: astore_3
    //   394: aload_0
    //   395: getfield 30	com/truecaller/api/services/messenger/v1/models/f:a	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   398: astore_1
    //   399: aload_3
    //   400: getfield 30	com/truecaller/api/services/messenger/v1/models/f:a	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   403: astore 4
    //   405: aload_2
    //   406: aload_1
    //   407: aload 4
    //   409: invokeinterface 122 3 0
    //   414: checkcast 32	com/truecaller/api/services/messenger/v1/models/Peer
    //   417: astore_1
    //   418: aload_0
    //   419: aload_1
    //   420: putfield 30	com/truecaller/api/services/messenger/v1/models/f:a	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   423: aload_0
    //   424: getfield 37	com/truecaller/api/services/messenger/v1/models/f:b	I
    //   427: istore 5
    //   429: iload 5
    //   431: ifeq +9 -> 440
    //   434: iconst_1
    //   435: istore 5
    //   437: goto +8 -> 445
    //   440: iconst_0
    //   441: istore 5
    //   443: aconst_null
    //   444: astore_1
    //   445: aload_0
    //   446: getfield 37	com/truecaller/api/services/messenger/v1/models/f:b	I
    //   449: istore 6
    //   451: aload_3
    //   452: getfield 37	com/truecaller/api/services/messenger/v1/models/f:b	I
    //   455: istore 9
    //   457: iload 9
    //   459: ifeq +6 -> 465
    //   462: goto +6 -> 468
    //   465: iconst_0
    //   466: istore 7
    //   468: aload_3
    //   469: getfield 37	com/truecaller/api/services/messenger/v1/models/f:b	I
    //   472: istore 11
    //   474: aload_2
    //   475: iload 5
    //   477: iload 6
    //   479: iload 7
    //   481: iload 11
    //   483: invokeinterface 126 5 0
    //   488: istore 5
    //   490: aload_0
    //   491: iload 5
    //   493: putfield 37	com/truecaller/api/services/messenger/v1/models/f:b	I
    //   496: aload_0
    //   497: areturn
    //   498: new 128	com/truecaller/api/services/messenger/v1/models/f$a
    //   501: astore_1
    //   502: aload_1
    //   503: iconst_0
    //   504: invokespecial 131	com/truecaller/api/services/messenger/v1/models/f$a:<init>	(B)V
    //   507: aload_1
    //   508: areturn
    //   509: aconst_null
    //   510: areturn
    //   511: getstatic 20	com/truecaller/api/services/messenger/v1/models/f:c	Lcom/truecaller/api/services/messenger/v1/models/f;
    //   514: areturn
    //   515: new 2	com/truecaller/api/services/messenger/v1/models/f
    //   518: astore_1
    //   519: aload_1
    //   520: invokespecial 18	com/truecaller/api/services/messenger/v1/models/f:<init>	()V
    //   523: aload_1
    //   524: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	525	0	this	f
    //   0	525	1	paramj	com.google.f.q.j
    //   0	525	2	paramObject1	Object
    //   0	525	3	paramObject2	Object
    //   3	405	4	localObject1	Object
    //   9	183	5	i	int
    //   196	3	5	bool1	boolean
    //   213	263	5	j	int
    //   488	4	5	k	int
    //   19	459	6	m	int
    //   25	455	7	bool2	boolean
    //   28	292	8	n	int
    //   170	288	9	i1	int
    //   259	36	10	localObject2	Object
    //   472	10	11	i2	int
    // Exception table:
    //   from	to	target	type
    //   103	106	133	finally
    //   111	114	133	finally
    //   115	118	133	finally
    //   120	124	133	finally
    //   124	128	133	finally
    //   128	130	133	finally
    //   134	136	133	finally
    //   157	161	324	finally
    //   191	196	324	finally
    //   209	213	324	finally
    //   216	221	324	finally
    //   224	228	324	finally
    //   233	237	324	finally
    //   238	242	324	finally
    //   243	247	324	finally
    //   256	259	324	finally
    //   264	268	324	finally
    //   270	275	324	finally
    //   278	283	324	finally
    //   287	291	324	finally
    //   294	300	324	finally
    //   300	304	324	finally
    //   305	309	324	finally
    //   311	315	324	finally
    //   329	332	324	finally
    //   333	336	324	finally
    //   337	341	324	finally
    //   343	347	324	finally
    //   348	352	324	finally
    //   354	358	324	finally
    //   358	360	324	finally
    //   361	364	324	finally
    //   366	370	324	finally
    //   372	376	324	finally
    //   376	378	324	finally
    //   157	161	328	java/io/IOException
    //   191	196	328	java/io/IOException
    //   209	213	328	java/io/IOException
    //   216	221	328	java/io/IOException
    //   224	228	328	java/io/IOException
    //   233	237	328	java/io/IOException
    //   238	242	328	java/io/IOException
    //   243	247	328	java/io/IOException
    //   256	259	328	java/io/IOException
    //   264	268	328	java/io/IOException
    //   270	275	328	java/io/IOException
    //   278	283	328	java/io/IOException
    //   287	291	328	java/io/IOException
    //   294	300	328	java/io/IOException
    //   300	304	328	java/io/IOException
    //   305	309	328	java/io/IOException
    //   311	315	328	java/io/IOException
    //   157	161	360	com/google/f/x
    //   191	196	360	com/google/f/x
    //   209	213	360	com/google/f/x
    //   216	221	360	com/google/f/x
    //   224	228	360	com/google/f/x
    //   233	237	360	com/google/f/x
    //   238	242	360	com/google/f/x
    //   243	247	360	com/google/f/x
    //   256	259	360	com/google/f/x
    //   264	268	360	com/google/f/x
    //   270	275	360	com/google/f/x
    //   278	283	360	com/google/f/x
    //   287	291	360	com/google/f/x
    //   294	300	360	com/google/f/x
    //   300	304	360	com/google/f/x
    //   305	309	360	com/google/f/x
    //   311	315	360	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int j = -1;
    if (i != j) {
      return i;
    }
    Peer localPeer1 = a;
    j = 0;
    if (localPeer1 != null)
    {
      Peer localPeer2 = a();
      i = h.computeMessageSize(1, localPeer2);
      j = 0 + i;
    }
    i = b;
    if (i != 0)
    {
      int k = 2;
      i = h.computeInt32Size(k, i);
      j += i;
    }
    memoizedSerializedSize = j;
    return j;
  }
  
  public final void writeTo(h paramh)
  {
    Peer localPeer1 = a;
    if (localPeer1 != null)
    {
      i = 1;
      Peer localPeer2 = a();
      paramh.writeMessage(i, localPeer2);
    }
    int i = b;
    if (i != 0)
    {
      int j = 2;
      paramh.writeInt32(j, i);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.models.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */