package com.truecaller.api.services.messenger.v1.models;

import com.google.f.ah;
import com.google.f.f;
import com.google.f.h;
import com.google.f.q;

public final class MessageContent$d
  extends q
  implements MessageContent.e
{
  private static final d g;
  private static volatile ah h;
  private String a = "";
  private String b = "";
  private int c;
  private int d;
  private int e;
  private f f;
  
  static
  {
    d locald = new com/truecaller/api/services/messenger/v1/models/MessageContent$d;
    locald.<init>();
    g = locald;
    locald.makeImmutable();
  }
  
  private MessageContent$d()
  {
    f localf = f.EMPTY;
    f = localf;
  }
  
  public static d g()
  {
    return g;
  }
  
  public static ah h()
  {
    return g.getParserForType();
  }
  
  public final String a()
  {
    return a;
  }
  
  public final String b()
  {
    return b;
  }
  
  public final int c()
  {
    return c;
  }
  
  public final int d()
  {
    return d;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 55	com/truecaller/api/services/messenger/v1/models/MessageContent$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 61	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_0
    //   19: istore 6
    //   21: aconst_null
    //   22: astore 4
    //   24: iconst_1
    //   25: istore 7
    //   27: iload 5
    //   29: tableswitch	default:+47->76, 1:+825->854, 2:+821->850, 3:+819->848, 4:+808->837, 5:+365->394, 6:+109->138, 7:+361->390, 8:+57->86
    //   76: new 64	java/lang/UnsupportedOperationException
    //   79: astore_1
    //   80: aload_1
    //   81: invokespecial 65	java/lang/UnsupportedOperationException:<init>	()V
    //   84: aload_1
    //   85: athrow
    //   86: getstatic 67	com/truecaller/api/services/messenger/v1/models/MessageContent$d:h	Lcom/google/f/ah;
    //   89: astore_1
    //   90: aload_1
    //   91: ifnonnull +43 -> 134
    //   94: ldc 2
    //   96: astore_1
    //   97: aload_1
    //   98: monitorenter
    //   99: getstatic 67	com/truecaller/api/services/messenger/v1/models/MessageContent$d:h	Lcom/google/f/ah;
    //   102: astore_2
    //   103: aload_2
    //   104: ifnonnull +20 -> 124
    //   107: new 69	com/google/f/q$b
    //   110: astore_2
    //   111: getstatic 25	com/truecaller/api/services/messenger/v1/models/MessageContent$d:g	Lcom/truecaller/api/services/messenger/v1/models/MessageContent$d;
    //   114: astore_3
    //   115: aload_2
    //   116: aload_3
    //   117: invokespecial 72	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   120: aload_2
    //   121: putstatic 67	com/truecaller/api/services/messenger/v1/models/MessageContent$d:h	Lcom/google/f/ah;
    //   124: aload_1
    //   125: monitorexit
    //   126: goto +8 -> 134
    //   129: astore_2
    //   130: aload_1
    //   131: monitorexit
    //   132: aload_2
    //   133: athrow
    //   134: getstatic 67	com/truecaller/api/services/messenger/v1/models/MessageContent$d:h	Lcom/google/f/ah;
    //   137: areturn
    //   138: aload_2
    //   139: checkcast 74	com/google/f/g
    //   142: astore_2
    //   143: iload 6
    //   145: ifne +245 -> 390
    //   148: aload_2
    //   149: invokevirtual 77	com/google/f/g:readTag	()I
    //   152: istore 5
    //   154: iload 5
    //   156: ifeq +172 -> 328
    //   159: bipush 10
    //   161: istore 8
    //   163: iload 5
    //   165: iload 8
    //   167: if_icmpeq +148 -> 315
    //   170: bipush 18
    //   172: istore 8
    //   174: iload 5
    //   176: iload 8
    //   178: if_icmpeq +124 -> 302
    //   181: bipush 24
    //   183: istore 8
    //   185: iload 5
    //   187: iload 8
    //   189: if_icmpeq +98 -> 287
    //   192: bipush 32
    //   194: istore 8
    //   196: iload 5
    //   198: iload 8
    //   200: if_icmpeq +72 -> 272
    //   203: bipush 40
    //   205: istore 8
    //   207: iload 5
    //   209: iload 8
    //   211: if_icmpeq +46 -> 257
    //   214: bipush 50
    //   216: istore 8
    //   218: iload 5
    //   220: iload 8
    //   222: if_icmpeq +22 -> 244
    //   225: aload_2
    //   226: iload 5
    //   228: invokevirtual 87	com/google/f/g:skipField	(I)Z
    //   231: istore 5
    //   233: iload 5
    //   235: ifne -92 -> 143
    //   238: iconst_1
    //   239: istore 6
    //   241: goto -98 -> 143
    //   244: aload_2
    //   245: invokevirtual 91	com/google/f/g:readBytes	()Lcom/google/f/f;
    //   248: astore_1
    //   249: aload_0
    //   250: aload_1
    //   251: putfield 42	com/truecaller/api/services/messenger/v1/models/MessageContent$d:f	Lcom/google/f/f;
    //   254: goto -111 -> 143
    //   257: aload_2
    //   258: invokevirtual 94	com/google/f/g:readInt32	()I
    //   261: istore 5
    //   263: aload_0
    //   264: iload 5
    //   266: putfield 96	com/truecaller/api/services/messenger/v1/models/MessageContent$d:e	I
    //   269: goto -126 -> 143
    //   272: aload_2
    //   273: invokevirtual 94	com/google/f/g:readInt32	()I
    //   276: istore 5
    //   278: aload_0
    //   279: iload 5
    //   281: putfield 50	com/truecaller/api/services/messenger/v1/models/MessageContent$d:d	I
    //   284: goto -141 -> 143
    //   287: aload_2
    //   288: invokevirtual 94	com/google/f/g:readInt32	()I
    //   291: istore 5
    //   293: aload_0
    //   294: iload 5
    //   296: putfield 48	com/truecaller/api/services/messenger/v1/models/MessageContent$d:c	I
    //   299: goto -156 -> 143
    //   302: aload_2
    //   303: invokevirtual 100	com/google/f/g:readStringRequireUtf8	()Ljava/lang/String;
    //   306: astore_1
    //   307: aload_0
    //   308: aload_1
    //   309: putfield 35	com/truecaller/api/services/messenger/v1/models/MessageContent$d:b	Ljava/lang/String;
    //   312: goto -169 -> 143
    //   315: aload_2
    //   316: invokevirtual 100	com/google/f/g:readStringRequireUtf8	()Ljava/lang/String;
    //   319: astore_1
    //   320: aload_0
    //   321: aload_1
    //   322: putfield 33	com/truecaller/api/services/messenger/v1/models/MessageContent$d:a	Ljava/lang/String;
    //   325: goto -182 -> 143
    //   328: iconst_1
    //   329: istore 6
    //   331: goto -188 -> 143
    //   334: astore_1
    //   335: goto +53 -> 388
    //   338: astore_1
    //   339: new 102	java/lang/RuntimeException
    //   342: astore_2
    //   343: new 104	com/google/f/x
    //   346: astore_3
    //   347: aload_1
    //   348: invokevirtual 109	java/io/IOException:getMessage	()Ljava/lang/String;
    //   351: astore_1
    //   352: aload_3
    //   353: aload_1
    //   354: invokespecial 112	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   357: aload_3
    //   358: aload_0
    //   359: invokevirtual 116	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   362: astore_1
    //   363: aload_2
    //   364: aload_1
    //   365: invokespecial 119	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   368: aload_2
    //   369: athrow
    //   370: astore_1
    //   371: new 102	java/lang/RuntimeException
    //   374: astore_2
    //   375: aload_1
    //   376: aload_0
    //   377: invokevirtual 116	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   380: astore_1
    //   381: aload_2
    //   382: aload_1
    //   383: invokespecial 119	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   386: aload_2
    //   387: athrow
    //   388: aload_1
    //   389: athrow
    //   390: getstatic 25	com/truecaller/api/services/messenger/v1/models/MessageContent$d:g	Lcom/truecaller/api/services/messenger/v1/models/MessageContent$d;
    //   393: areturn
    //   394: aload_2
    //   395: checkcast 121	com/google/f/q$k
    //   398: astore_2
    //   399: aload_3
    //   400: checkcast 2	com/truecaller/api/services/messenger/v1/models/MessageContent$d
    //   403: astore_3
    //   404: aload_0
    //   405: getfield 33	com/truecaller/api/services/messenger/v1/models/MessageContent$d:a	Ljava/lang/String;
    //   408: invokevirtual 127	java/lang/String:isEmpty	()Z
    //   411: iload 7
    //   413: ixor
    //   414: istore 5
    //   416: aload_0
    //   417: getfield 33	com/truecaller/api/services/messenger/v1/models/MessageContent$d:a	Ljava/lang/String;
    //   420: astore 9
    //   422: aload_3
    //   423: getfield 33	com/truecaller/api/services/messenger/v1/models/MessageContent$d:a	Ljava/lang/String;
    //   426: invokevirtual 127	java/lang/String:isEmpty	()Z
    //   429: iload 7
    //   431: ixor
    //   432: istore 10
    //   434: aload_3
    //   435: getfield 33	com/truecaller/api/services/messenger/v1/models/MessageContent$d:a	Ljava/lang/String;
    //   438: astore 11
    //   440: aload_2
    //   441: iload 5
    //   443: aload 9
    //   445: iload 10
    //   447: aload 11
    //   449: invokeinterface 131 5 0
    //   454: astore_1
    //   455: aload_0
    //   456: aload_1
    //   457: putfield 33	com/truecaller/api/services/messenger/v1/models/MessageContent$d:a	Ljava/lang/String;
    //   460: aload_0
    //   461: getfield 35	com/truecaller/api/services/messenger/v1/models/MessageContent$d:b	Ljava/lang/String;
    //   464: invokevirtual 127	java/lang/String:isEmpty	()Z
    //   467: iload 7
    //   469: ixor
    //   470: istore 5
    //   472: aload_0
    //   473: getfield 35	com/truecaller/api/services/messenger/v1/models/MessageContent$d:b	Ljava/lang/String;
    //   476: astore 9
    //   478: aload_3
    //   479: getfield 35	com/truecaller/api/services/messenger/v1/models/MessageContent$d:b	Ljava/lang/String;
    //   482: astore 12
    //   484: aload 12
    //   486: invokevirtual 127	java/lang/String:isEmpty	()Z
    //   489: iload 7
    //   491: ixor
    //   492: istore 10
    //   494: aload_3
    //   495: getfield 35	com/truecaller/api/services/messenger/v1/models/MessageContent$d:b	Ljava/lang/String;
    //   498: astore 11
    //   500: aload_2
    //   501: iload 5
    //   503: aload 9
    //   505: iload 10
    //   507: aload 11
    //   509: invokeinterface 131 5 0
    //   514: astore_1
    //   515: aload_0
    //   516: aload_1
    //   517: putfield 35	com/truecaller/api/services/messenger/v1/models/MessageContent$d:b	Ljava/lang/String;
    //   520: aload_0
    //   521: getfield 48	com/truecaller/api/services/messenger/v1/models/MessageContent$d:c	I
    //   524: istore 5
    //   526: iload 5
    //   528: ifeq +9 -> 537
    //   531: iconst_1
    //   532: istore 5
    //   534: goto +8 -> 542
    //   537: iconst_0
    //   538: istore 5
    //   540: aconst_null
    //   541: astore_1
    //   542: aload_0
    //   543: getfield 48	com/truecaller/api/services/messenger/v1/models/MessageContent$d:c	I
    //   546: istore 13
    //   548: aload_3
    //   549: getfield 48	com/truecaller/api/services/messenger/v1/models/MessageContent$d:c	I
    //   552: istore 10
    //   554: iload 10
    //   556: ifeq +9 -> 565
    //   559: iconst_1
    //   560: istore 10
    //   562: goto +9 -> 571
    //   565: iconst_0
    //   566: istore 10
    //   568: aconst_null
    //   569: astore 12
    //   571: aload_3
    //   572: getfield 48	com/truecaller/api/services/messenger/v1/models/MessageContent$d:c	I
    //   575: istore 14
    //   577: aload_2
    //   578: iload 5
    //   580: iload 13
    //   582: iload 10
    //   584: iload 14
    //   586: invokeinterface 135 5 0
    //   591: istore 5
    //   593: aload_0
    //   594: iload 5
    //   596: putfield 48	com/truecaller/api/services/messenger/v1/models/MessageContent$d:c	I
    //   599: aload_0
    //   600: getfield 50	com/truecaller/api/services/messenger/v1/models/MessageContent$d:d	I
    //   603: istore 5
    //   605: iload 5
    //   607: ifeq +9 -> 616
    //   610: iconst_1
    //   611: istore 5
    //   613: goto +8 -> 621
    //   616: iconst_0
    //   617: istore 5
    //   619: aconst_null
    //   620: astore_1
    //   621: aload_0
    //   622: getfield 50	com/truecaller/api/services/messenger/v1/models/MessageContent$d:d	I
    //   625: istore 13
    //   627: aload_3
    //   628: getfield 50	com/truecaller/api/services/messenger/v1/models/MessageContent$d:d	I
    //   631: istore 10
    //   633: iload 10
    //   635: ifeq +9 -> 644
    //   638: iconst_1
    //   639: istore 10
    //   641: goto +9 -> 650
    //   644: iconst_0
    //   645: istore 10
    //   647: aconst_null
    //   648: astore 12
    //   650: aload_3
    //   651: getfield 50	com/truecaller/api/services/messenger/v1/models/MessageContent$d:d	I
    //   654: istore 14
    //   656: aload_2
    //   657: iload 5
    //   659: iload 13
    //   661: iload 10
    //   663: iload 14
    //   665: invokeinterface 135 5 0
    //   670: istore 5
    //   672: aload_0
    //   673: iload 5
    //   675: putfield 50	com/truecaller/api/services/messenger/v1/models/MessageContent$d:d	I
    //   678: aload_0
    //   679: getfield 96	com/truecaller/api/services/messenger/v1/models/MessageContent$d:e	I
    //   682: istore 5
    //   684: iload 5
    //   686: ifeq +9 -> 695
    //   689: iconst_1
    //   690: istore 5
    //   692: goto +8 -> 700
    //   695: iconst_0
    //   696: istore 5
    //   698: aconst_null
    //   699: astore_1
    //   700: aload_0
    //   701: getfield 96	com/truecaller/api/services/messenger/v1/models/MessageContent$d:e	I
    //   704: istore 13
    //   706: aload_3
    //   707: getfield 96	com/truecaller/api/services/messenger/v1/models/MessageContent$d:e	I
    //   710: istore 10
    //   712: iload 10
    //   714: ifeq +9 -> 723
    //   717: iconst_1
    //   718: istore 10
    //   720: goto +9 -> 729
    //   723: iconst_0
    //   724: istore 10
    //   726: aconst_null
    //   727: astore 12
    //   729: aload_3
    //   730: getfield 96	com/truecaller/api/services/messenger/v1/models/MessageContent$d:e	I
    //   733: istore 14
    //   735: aload_2
    //   736: iload 5
    //   738: iload 13
    //   740: iload 10
    //   742: iload 14
    //   744: invokeinterface 135 5 0
    //   749: istore 5
    //   751: aload_0
    //   752: iload 5
    //   754: putfield 96	com/truecaller/api/services/messenger/v1/models/MessageContent$d:e	I
    //   757: aload_0
    //   758: getfield 42	com/truecaller/api/services/messenger/v1/models/MessageContent$d:f	Lcom/google/f/f;
    //   761: astore_1
    //   762: getstatic 40	com/google/f/f:EMPTY	Lcom/google/f/f;
    //   765: astore 9
    //   767: aload_1
    //   768: aload 9
    //   770: if_acmpeq +9 -> 779
    //   773: iconst_1
    //   774: istore 5
    //   776: goto +8 -> 784
    //   779: iconst_0
    //   780: istore 5
    //   782: aconst_null
    //   783: astore_1
    //   784: aload_0
    //   785: getfield 42	com/truecaller/api/services/messenger/v1/models/MessageContent$d:f	Lcom/google/f/f;
    //   788: astore 9
    //   790: aload_3
    //   791: getfield 42	com/truecaller/api/services/messenger/v1/models/MessageContent$d:f	Lcom/google/f/f;
    //   794: astore 12
    //   796: getstatic 40	com/google/f/f:EMPTY	Lcom/google/f/f;
    //   799: astore 11
    //   801: aload 12
    //   803: aload 11
    //   805: if_acmpeq +6 -> 811
    //   808: iconst_1
    //   809: istore 6
    //   811: aload_3
    //   812: getfield 42	com/truecaller/api/services/messenger/v1/models/MessageContent$d:f	Lcom/google/f/f;
    //   815: astore_3
    //   816: aload_2
    //   817: iload 5
    //   819: aload 9
    //   821: iload 6
    //   823: aload_3
    //   824: invokeinterface 139 5 0
    //   829: astore_1
    //   830: aload_0
    //   831: aload_1
    //   832: putfield 42	com/truecaller/api/services/messenger/v1/models/MessageContent$d:f	Lcom/google/f/f;
    //   835: aload_0
    //   836: areturn
    //   837: new 141	com/truecaller/api/services/messenger/v1/models/MessageContent$d$a
    //   840: astore_1
    //   841: aload_1
    //   842: iconst_0
    //   843: invokespecial 144	com/truecaller/api/services/messenger/v1/models/MessageContent$d$a:<init>	(B)V
    //   846: aload_1
    //   847: areturn
    //   848: aconst_null
    //   849: areturn
    //   850: getstatic 25	com/truecaller/api/services/messenger/v1/models/MessageContent$d:g	Lcom/truecaller/api/services/messenger/v1/models/MessageContent$d;
    //   853: areturn
    //   854: new 2	com/truecaller/api/services/messenger/v1/models/MessageContent$d
    //   857: astore_1
    //   858: aload_1
    //   859: invokespecial 23	com/truecaller/api/services/messenger/v1/models/MessageContent$d:<init>	()V
    //   862: aload_1
    //   863: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	864	0	this	d
    //   0	864	1	paramj	com.google.f.q.j
    //   0	864	2	paramObject1	Object
    //   0	864	3	paramObject2	Object
    //   3	20	4	arrayOfInt	int[]
    //   9	218	5	i	int
    //   231	3	5	bool1	boolean
    //   261	34	5	j	int
    //   414	88	5	bool2	boolean
    //   524	55	5	k	int
    //   591	67	5	m	int
    //   670	67	5	n	int
    //   749	69	5	i1	int
    //   19	803	6	bool3	boolean
    //   25	467	7	i2	int
    //   161	62	8	i3	int
    //   420	400	9	localObject1	Object
    //   432	74	10	bool4	boolean
    //   552	31	10	i4	int
    //   631	31	10	i5	int
    //   710	31	10	i6	int
    //   438	366	11	localObject2	Object
    //   482	320	12	localObject3	Object
    //   546	193	13	i7	int
    //   575	168	14	i8	int
    // Exception table:
    //   from	to	target	type
    //   99	102	129	finally
    //   107	110	129	finally
    //   111	114	129	finally
    //   116	120	129	finally
    //   120	124	129	finally
    //   124	126	129	finally
    //   130	132	129	finally
    //   148	152	334	finally
    //   226	231	334	finally
    //   244	248	334	finally
    //   250	254	334	finally
    //   257	261	334	finally
    //   264	269	334	finally
    //   272	276	334	finally
    //   279	284	334	finally
    //   287	291	334	finally
    //   294	299	334	finally
    //   302	306	334	finally
    //   308	312	334	finally
    //   315	319	334	finally
    //   321	325	334	finally
    //   339	342	334	finally
    //   343	346	334	finally
    //   347	351	334	finally
    //   353	357	334	finally
    //   358	362	334	finally
    //   364	368	334	finally
    //   368	370	334	finally
    //   371	374	334	finally
    //   376	380	334	finally
    //   382	386	334	finally
    //   386	388	334	finally
    //   148	152	338	java/io/IOException
    //   226	231	338	java/io/IOException
    //   244	248	338	java/io/IOException
    //   250	254	338	java/io/IOException
    //   257	261	338	java/io/IOException
    //   264	269	338	java/io/IOException
    //   272	276	338	java/io/IOException
    //   279	284	338	java/io/IOException
    //   287	291	338	java/io/IOException
    //   294	299	338	java/io/IOException
    //   302	306	338	java/io/IOException
    //   308	312	338	java/io/IOException
    //   315	319	338	java/io/IOException
    //   321	325	338	java/io/IOException
    //   148	152	370	com/google/f/x
    //   226	231	370	com/google/f/x
    //   244	248	370	com/google/f/x
    //   250	254	370	com/google/f/x
    //   257	261	370	com/google/f/x
    //   264	269	370	com/google/f/x
    //   272	276	370	com/google/f/x
    //   279	284	370	com/google/f/x
    //   287	291	370	com/google/f/x
    //   294	299	370	com/google/f/x
    //   302	306	370	com/google/f/x
    //   308	312	370	com/google/f/x
    //   315	319	370	com/google/f/x
    //   321	325	370	com/google/f/x
  }
  
  public final int e()
  {
    return e;
  }
  
  public final f f()
  {
    return f;
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int n = -1;
    if (i != n) {
      return i;
    }
    Object localObject1 = a;
    boolean bool1 = ((String)localObject1).isEmpty();
    n = 0;
    Object localObject2;
    if (!bool1)
    {
      localObject2 = a;
      int j = h.computeStringSize(1, (String)localObject2);
      n = 0 + j;
    }
    localObject1 = b;
    boolean bool2 = ((String)localObject1).isEmpty();
    if (!bool2)
    {
      localObject2 = b;
      k = h.computeStringSize(2, (String)localObject2);
      n += k;
    }
    int k = c;
    int i1;
    if (k != 0)
    {
      i1 = 3;
      k = h.computeInt32Size(i1, k);
      n += k;
    }
    k = d;
    if (k != 0)
    {
      i1 = 4;
      k = h.computeInt32Size(i1, k);
      n += k;
    }
    k = e;
    if (k != 0)
    {
      i1 = 5;
      k = h.computeInt32Size(i1, k);
      n += k;
    }
    localObject1 = f;
    boolean bool3 = ((f)localObject1).isEmpty();
    if (!bool3)
    {
      localObject2 = f;
      int m = h.computeBytesSize(6, (f)localObject2);
      n += m;
    }
    memoizedSerializedSize = n;
    return n;
  }
  
  public final void writeTo(h paramh)
  {
    Object localObject1 = a;
    int i = ((String)localObject1).isEmpty();
    Object localObject2;
    if (i == 0)
    {
      i = 1;
      localObject2 = a;
      paramh.writeString(i, (String)localObject2);
    }
    localObject1 = b;
    boolean bool1 = ((String)localObject1).isEmpty();
    if (!bool1)
    {
      j = 2;
      localObject2 = b;
      paramh.writeString(j, (String)localObject2);
    }
    int j = c;
    int m;
    if (j != 0)
    {
      m = 3;
      paramh.writeInt32(m, j);
    }
    j = d;
    if (j != 0)
    {
      m = 4;
      paramh.writeInt32(m, j);
    }
    j = e;
    if (j != 0)
    {
      m = 5;
      paramh.writeInt32(m, j);
    }
    localObject1 = f;
    boolean bool2 = ((f)localObject1).isEmpty();
    if (!bool2)
    {
      int k = 6;
      localObject2 = f;
      paramh.writeBytes(k, (f)localObject2);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.models.MessageContent.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */