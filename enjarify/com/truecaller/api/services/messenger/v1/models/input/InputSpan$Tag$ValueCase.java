package com.truecaller.api.services.messenger.v1.models.input;

import com.google.f.w.c;

public enum InputSpan$Tag$ValueCase
  implements w.c
{
  private final int value;
  
  static
  {
    Object localObject = new com/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag$ValueCase;
    int i = 1;
    ((ValueCase)localObject).<init>("BOOLEAN_VALUE", 0, i);
    BOOLEAN_VALUE = (ValueCase)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag$ValueCase;
    int j = 2;
    ((ValueCase)localObject).<init>("STRING_VALUE", i, j);
    STRING_VALUE = (ValueCase)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag$ValueCase;
    int k = 3;
    ((ValueCase)localObject).<init>("LONG_VALUE", j, k);
    LONG_VALUE = (ValueCase)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/models/input/InputSpan$Tag$ValueCase;
    ((ValueCase)localObject).<init>("VALUE_NOT_SET", k, 0);
    VALUE_NOT_SET = (ValueCase)localObject;
    localObject = new ValueCase[4];
    ValueCase localValueCase = BOOLEAN_VALUE;
    localObject[0] = localValueCase;
    localValueCase = STRING_VALUE;
    localObject[i] = localValueCase;
    localValueCase = LONG_VALUE;
    localObject[j] = localValueCase;
    localValueCase = VALUE_NOT_SET;
    localObject[k] = localValueCase;
    $VALUES = (ValueCase[])localObject;
  }
  
  private InputSpan$Tag$ValueCase(int paramInt1)
  {
    value = paramInt1;
  }
  
  public static ValueCase forNumber(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return null;
    case 3: 
      return LONG_VALUE;
    case 2: 
      return STRING_VALUE;
    case 1: 
      return BOOLEAN_VALUE;
    }
    return VALUE_NOT_SET;
  }
  
  public static ValueCase valueOf(int paramInt)
  {
    return forNumber(paramInt);
  }
  
  public final int getNumber()
  {
    return value;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.models.input.InputSpan.Tag.ValueCase
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */