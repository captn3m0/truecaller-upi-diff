package com.truecaller.api.services.messenger.v1.models.input;

import com.google.f.q.a;

public final class InputNotificationSettings$a
  extends q.a
  implements h
{
  private InputNotificationSettings$a()
  {
    super(localInputNotificationSettings);
  }
  
  public final a a(InputNotificationSettings.b paramb)
  {
    copyOnWrite();
    InputNotificationSettings.a((InputNotificationSettings)instance, paramb);
    return this;
  }
  
  public final a a(InputNotificationSettings.d paramd)
  {
    copyOnWrite();
    InputNotificationSettings.a((InputNotificationSettings)instance, paramd);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.models.input.InputNotificationSettings.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */