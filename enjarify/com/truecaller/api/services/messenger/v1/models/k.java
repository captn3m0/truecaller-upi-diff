package com.truecaller.api.services.messenger.v1.models;

import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;

public final class k
  extends q
  implements l
{
  private static final k d;
  private static volatile ah e;
  private String a = "";
  private String b = "";
  private String c = "";
  
  static
  {
    k localk = new com/truecaller/api/services/messenger/v1/models/k;
    localk.<init>();
    d = localk;
    localk.makeImmutable();
  }
  
  public static k d()
  {
    return d;
  }
  
  public final String a()
  {
    return a;
  }
  
  public final String b()
  {
    return b;
  }
  
  public final String c()
  {
    return c;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 37	com/truecaller/api/services/messenger/v1/models/k$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 43	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_0
    //   19: istore 6
    //   21: aconst_null
    //   22: astore 4
    //   24: iconst_1
    //   25: istore 7
    //   27: iload 5
    //   29: tableswitch	default:+47->76, 1:+486->515, 2:+482->511, 3:+480->509, 4:+469->498, 5:+287->316, 6:+109->138, 7:+283->312, 8:+57->86
    //   76: new 46	java/lang/UnsupportedOperationException
    //   79: astore_1
    //   80: aload_1
    //   81: invokespecial 47	java/lang/UnsupportedOperationException:<init>	()V
    //   84: aload_1
    //   85: athrow
    //   86: getstatic 49	com/truecaller/api/services/messenger/v1/models/k:e	Lcom/google/f/ah;
    //   89: astore_1
    //   90: aload_1
    //   91: ifnonnull +43 -> 134
    //   94: ldc 2
    //   96: astore_1
    //   97: aload_1
    //   98: monitorenter
    //   99: getstatic 49	com/truecaller/api/services/messenger/v1/models/k:e	Lcom/google/f/ah;
    //   102: astore_2
    //   103: aload_2
    //   104: ifnonnull +20 -> 124
    //   107: new 51	com/google/f/q$b
    //   110: astore_2
    //   111: getstatic 20	com/truecaller/api/services/messenger/v1/models/k:d	Lcom/truecaller/api/services/messenger/v1/models/k;
    //   114: astore_3
    //   115: aload_2
    //   116: aload_3
    //   117: invokespecial 54	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   120: aload_2
    //   121: putstatic 49	com/truecaller/api/services/messenger/v1/models/k:e	Lcom/google/f/ah;
    //   124: aload_1
    //   125: monitorexit
    //   126: goto +8 -> 134
    //   129: astore_2
    //   130: aload_1
    //   131: monitorexit
    //   132: aload_2
    //   133: athrow
    //   134: getstatic 49	com/truecaller/api/services/messenger/v1/models/k:e	Lcom/google/f/ah;
    //   137: areturn
    //   138: aload_2
    //   139: checkcast 56	com/google/f/g
    //   142: astore_2
    //   143: iload 6
    //   145: ifne +167 -> 312
    //   148: aload_2
    //   149: invokevirtual 59	com/google/f/g:readTag	()I
    //   152: istore 5
    //   154: iload 5
    //   156: ifeq +94 -> 250
    //   159: bipush 18
    //   161: istore 8
    //   163: iload 5
    //   165: iload 8
    //   167: if_icmpeq +70 -> 237
    //   170: bipush 26
    //   172: istore 8
    //   174: iload 5
    //   176: iload 8
    //   178: if_icmpeq +46 -> 224
    //   181: bipush 34
    //   183: istore 8
    //   185: iload 5
    //   187: iload 8
    //   189: if_icmpeq +22 -> 211
    //   192: aload_2
    //   193: iload 5
    //   195: invokevirtual 66	com/google/f/g:skipField	(I)Z
    //   198: istore 5
    //   200: iload 5
    //   202: ifne -59 -> 143
    //   205: iconst_1
    //   206: istore 6
    //   208: goto -65 -> 143
    //   211: aload_2
    //   212: invokevirtual 70	com/google/f/g:readStringRequireUtf8	()Ljava/lang/String;
    //   215: astore_1
    //   216: aload_0
    //   217: aload_1
    //   218: putfield 32	com/truecaller/api/services/messenger/v1/models/k:c	Ljava/lang/String;
    //   221: goto -78 -> 143
    //   224: aload_2
    //   225: invokevirtual 70	com/google/f/g:readStringRequireUtf8	()Ljava/lang/String;
    //   228: astore_1
    //   229: aload_0
    //   230: aload_1
    //   231: putfield 30	com/truecaller/api/services/messenger/v1/models/k:b	Ljava/lang/String;
    //   234: goto -91 -> 143
    //   237: aload_2
    //   238: invokevirtual 70	com/google/f/g:readStringRequireUtf8	()Ljava/lang/String;
    //   241: astore_1
    //   242: aload_0
    //   243: aload_1
    //   244: putfield 28	com/truecaller/api/services/messenger/v1/models/k:a	Ljava/lang/String;
    //   247: goto -104 -> 143
    //   250: iconst_1
    //   251: istore 6
    //   253: goto -110 -> 143
    //   256: astore_1
    //   257: goto +53 -> 310
    //   260: astore_1
    //   261: new 72	java/lang/RuntimeException
    //   264: astore_2
    //   265: new 74	com/google/f/x
    //   268: astore_3
    //   269: aload_1
    //   270: invokevirtual 79	java/io/IOException:getMessage	()Ljava/lang/String;
    //   273: astore_1
    //   274: aload_3
    //   275: aload_1
    //   276: invokespecial 82	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   279: aload_3
    //   280: aload_0
    //   281: invokevirtual 86	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   284: astore_1
    //   285: aload_2
    //   286: aload_1
    //   287: invokespecial 89	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   290: aload_2
    //   291: athrow
    //   292: astore_1
    //   293: new 72	java/lang/RuntimeException
    //   296: astore_2
    //   297: aload_1
    //   298: aload_0
    //   299: invokevirtual 86	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   302: astore_1
    //   303: aload_2
    //   304: aload_1
    //   305: invokespecial 89	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   308: aload_2
    //   309: athrow
    //   310: aload_1
    //   311: athrow
    //   312: getstatic 20	com/truecaller/api/services/messenger/v1/models/k:d	Lcom/truecaller/api/services/messenger/v1/models/k;
    //   315: areturn
    //   316: aload_2
    //   317: checkcast 91	com/google/f/q$k
    //   320: astore_2
    //   321: aload_3
    //   322: checkcast 2	com/truecaller/api/services/messenger/v1/models/k
    //   325: astore_3
    //   326: aload_0
    //   327: getfield 28	com/truecaller/api/services/messenger/v1/models/k:a	Ljava/lang/String;
    //   330: invokevirtual 97	java/lang/String:isEmpty	()Z
    //   333: iload 7
    //   335: ixor
    //   336: istore 5
    //   338: aload_0
    //   339: getfield 28	com/truecaller/api/services/messenger/v1/models/k:a	Ljava/lang/String;
    //   342: astore 4
    //   344: aload_3
    //   345: getfield 28	com/truecaller/api/services/messenger/v1/models/k:a	Ljava/lang/String;
    //   348: invokevirtual 97	java/lang/String:isEmpty	()Z
    //   351: iload 7
    //   353: ixor
    //   354: istore 9
    //   356: aload_3
    //   357: getfield 28	com/truecaller/api/services/messenger/v1/models/k:a	Ljava/lang/String;
    //   360: astore 10
    //   362: aload_2
    //   363: iload 5
    //   365: aload 4
    //   367: iload 9
    //   369: aload 10
    //   371: invokeinterface 101 5 0
    //   376: astore_1
    //   377: aload_0
    //   378: aload_1
    //   379: putfield 28	com/truecaller/api/services/messenger/v1/models/k:a	Ljava/lang/String;
    //   382: aload_0
    //   383: getfield 30	com/truecaller/api/services/messenger/v1/models/k:b	Ljava/lang/String;
    //   386: invokevirtual 97	java/lang/String:isEmpty	()Z
    //   389: iload 7
    //   391: ixor
    //   392: istore 5
    //   394: aload_0
    //   395: getfield 30	com/truecaller/api/services/messenger/v1/models/k:b	Ljava/lang/String;
    //   398: astore 4
    //   400: aload_3
    //   401: getfield 30	com/truecaller/api/services/messenger/v1/models/k:b	Ljava/lang/String;
    //   404: invokevirtual 97	java/lang/String:isEmpty	()Z
    //   407: iload 7
    //   409: ixor
    //   410: istore 9
    //   412: aload_3
    //   413: getfield 30	com/truecaller/api/services/messenger/v1/models/k:b	Ljava/lang/String;
    //   416: astore 10
    //   418: aload_2
    //   419: iload 5
    //   421: aload 4
    //   423: iload 9
    //   425: aload 10
    //   427: invokeinterface 101 5 0
    //   432: astore_1
    //   433: aload_0
    //   434: aload_1
    //   435: putfield 30	com/truecaller/api/services/messenger/v1/models/k:b	Ljava/lang/String;
    //   438: aload_0
    //   439: getfield 32	com/truecaller/api/services/messenger/v1/models/k:c	Ljava/lang/String;
    //   442: invokevirtual 97	java/lang/String:isEmpty	()Z
    //   445: iload 7
    //   447: ixor
    //   448: istore 5
    //   450: aload_0
    //   451: getfield 32	com/truecaller/api/services/messenger/v1/models/k:c	Ljava/lang/String;
    //   454: astore 4
    //   456: aload_3
    //   457: getfield 32	com/truecaller/api/services/messenger/v1/models/k:c	Ljava/lang/String;
    //   460: invokevirtual 97	java/lang/String:isEmpty	()Z
    //   463: istore 9
    //   465: iload 7
    //   467: iload 9
    //   469: ixor
    //   470: istore 7
    //   472: aload_3
    //   473: getfield 32	com/truecaller/api/services/messenger/v1/models/k:c	Ljava/lang/String;
    //   476: astore_3
    //   477: aload_2
    //   478: iload 5
    //   480: aload 4
    //   482: iload 7
    //   484: aload_3
    //   485: invokeinterface 101 5 0
    //   490: astore_1
    //   491: aload_0
    //   492: aload_1
    //   493: putfield 32	com/truecaller/api/services/messenger/v1/models/k:c	Ljava/lang/String;
    //   496: aload_0
    //   497: areturn
    //   498: new 103	com/truecaller/api/services/messenger/v1/models/k$a
    //   501: astore_1
    //   502: aload_1
    //   503: iconst_0
    //   504: invokespecial 106	com/truecaller/api/services/messenger/v1/models/k$a:<init>	(B)V
    //   507: aload_1
    //   508: areturn
    //   509: aconst_null
    //   510: areturn
    //   511: getstatic 20	com/truecaller/api/services/messenger/v1/models/k:d	Lcom/truecaller/api/services/messenger/v1/models/k;
    //   514: areturn
    //   515: new 2	com/truecaller/api/services/messenger/v1/models/k
    //   518: astore_1
    //   519: aload_1
    //   520: invokespecial 18	com/truecaller/api/services/messenger/v1/models/k:<init>	()V
    //   523: aload_1
    //   524: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	525	0	this	k
    //   0	525	1	paramj	com.google.f.q.j
    //   0	525	2	paramObject1	Object
    //   0	525	3	paramObject2	Object
    //   3	478	4	localObject	Object
    //   9	185	5	i	int
    //   198	281	5	bool1	boolean
    //   19	233	6	j	int
    //   25	458	7	bool2	boolean
    //   161	29	8	k	int
    //   354	116	9	bool3	boolean
    //   360	66	10	str	String
    // Exception table:
    //   from	to	target	type
    //   99	102	129	finally
    //   107	110	129	finally
    //   111	114	129	finally
    //   116	120	129	finally
    //   120	124	129	finally
    //   124	126	129	finally
    //   130	132	129	finally
    //   148	152	256	finally
    //   193	198	256	finally
    //   211	215	256	finally
    //   217	221	256	finally
    //   224	228	256	finally
    //   230	234	256	finally
    //   237	241	256	finally
    //   243	247	256	finally
    //   261	264	256	finally
    //   265	268	256	finally
    //   269	273	256	finally
    //   275	279	256	finally
    //   280	284	256	finally
    //   286	290	256	finally
    //   290	292	256	finally
    //   293	296	256	finally
    //   298	302	256	finally
    //   304	308	256	finally
    //   308	310	256	finally
    //   148	152	260	java/io/IOException
    //   193	198	260	java/io/IOException
    //   211	215	260	java/io/IOException
    //   217	221	260	java/io/IOException
    //   224	228	260	java/io/IOException
    //   230	234	260	java/io/IOException
    //   237	241	260	java/io/IOException
    //   243	247	260	java/io/IOException
    //   148	152	292	com/google/f/x
    //   193	198	292	com/google/f/x
    //   211	215	292	com/google/f/x
    //   217	221	292	com/google/f/x
    //   224	228	292	com/google/f/x
    //   230	234	292	com/google/f/x
    //   237	241	292	com/google/f/x
    //   243	247	292	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int n = -1;
    if (i != n) {
      return i;
    }
    String str1 = a;
    boolean bool1 = str1.isEmpty();
    n = 0;
    String str2;
    if (!bool1)
    {
      str2 = a;
      int j = h.computeStringSize(2, str2);
      n = 0 + j;
    }
    str1 = b;
    boolean bool2 = str1.isEmpty();
    if (!bool2)
    {
      str2 = b;
      int k = h.computeStringSize(3, str2);
      n += k;
    }
    str1 = c;
    boolean bool3 = str1.isEmpty();
    if (!bool3)
    {
      str2 = c;
      int m = h.computeStringSize(4, str2);
      n += m;
    }
    memoizedSerializedSize = n;
    return n;
  }
  
  public final void writeTo(h paramh)
  {
    String str1 = a;
    boolean bool1 = str1.isEmpty();
    String str2;
    if (!bool1)
    {
      int i = 2;
      str2 = a;
      paramh.writeString(i, str2);
    }
    str1 = b;
    boolean bool2 = str1.isEmpty();
    if (!bool2)
    {
      int j = 3;
      str2 = b;
      paramh.writeString(j, str2);
    }
    str1 = c;
    boolean bool3 = str1.isEmpty();
    if (!bool3)
    {
      int k = 4;
      str2 = c;
      paramh.writeString(k, str2);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.models.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */