package com.truecaller.api.services.messenger.v1.models.input;

import com.google.f.q.a;

public final class InputReactionContent$b$a
  extends q.a
  implements InputReactionContent.c
{
  private InputReactionContent$b$a()
  {
    super(localb);
  }
  
  public final a a(String paramString)
  {
    copyOnWrite();
    InputReactionContent.b.a((InputReactionContent.b)instance, paramString);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.models.input.InputReactionContent.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */