package com.truecaller.api.services.messenger.v1.models.input;

import com.google.f.ac;
import com.google.f.ad;
import com.google.f.ae;
import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;
import com.google.f.w.h;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

public final class InputSpan
  extends q
  implements k
{
  private static final InputSpan h;
  private static volatile ah i;
  private int a;
  private String b;
  private String c;
  private long d;
  private long e;
  private ad f;
  private w.h g;
  
  static
  {
    InputSpan localInputSpan = new com/truecaller/api/services/messenger/v1/models/input/InputSpan;
    localInputSpan.<init>();
    h = localInputSpan;
    localInputSpan.makeImmutable();
  }
  
  private InputSpan()
  {
    Object localObject = ad.emptyMapField();
    f = ((ad)localObject);
    b = "";
    c = "";
    localObject = emptyProtobufList();
    g = ((w.h)localObject);
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 57	com/truecaller/api/services/messenger/v1/models/input/InputSpan$1:b	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 63	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_0
    //   19: istore 6
    //   21: aconst_null
    //   22: astore 4
    //   24: iconst_1
    //   25: istore 7
    //   27: iload 5
    //   29: tableswitch	default:+47->76, 1:+904->933, 2:+900->929, 3:+882->911, 4:+871->900, 5:+458->487, 6:+109->138, 7:+454->483, 8:+57->86
    //   76: new 66	java/lang/UnsupportedOperationException
    //   79: astore_1
    //   80: aload_1
    //   81: invokespecial 67	java/lang/UnsupportedOperationException:<init>	()V
    //   84: aload_1
    //   85: athrow
    //   86: getstatic 69	com/truecaller/api/services/messenger/v1/models/input/InputSpan:i	Lcom/google/f/ah;
    //   89: astore_1
    //   90: aload_1
    //   91: ifnonnull +43 -> 134
    //   94: ldc 2
    //   96: astore_1
    //   97: aload_1
    //   98: monitorenter
    //   99: getstatic 69	com/truecaller/api/services/messenger/v1/models/input/InputSpan:i	Lcom/google/f/ah;
    //   102: astore_2
    //   103: aload_2
    //   104: ifnonnull +20 -> 124
    //   107: new 71	com/google/f/q$b
    //   110: astore_2
    //   111: getstatic 28	com/truecaller/api/services/messenger/v1/models/input/InputSpan:h	Lcom/truecaller/api/services/messenger/v1/models/input/InputSpan;
    //   114: astore_3
    //   115: aload_2
    //   116: aload_3
    //   117: invokespecial 74	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   120: aload_2
    //   121: putstatic 69	com/truecaller/api/services/messenger/v1/models/input/InputSpan:i	Lcom/google/f/ah;
    //   124: aload_1
    //   125: monitorexit
    //   126: goto +8 -> 134
    //   129: astore_2
    //   130: aload_1
    //   131: monitorexit
    //   132: aload_2
    //   133: athrow
    //   134: getstatic 69	com/truecaller/api/services/messenger/v1/models/input/InputSpan:i	Lcom/google/f/ah;
    //   137: areturn
    //   138: aload_2
    //   139: checkcast 76	com/google/f/g
    //   142: astore_2
    //   143: aload_3
    //   144: checkcast 78	com/google/f/n
    //   147: astore_3
    //   148: iload 6
    //   150: ifne +333 -> 483
    //   153: aload_2
    //   154: invokevirtual 81	com/google/f/g:readTag	()I
    //   157: istore 5
    //   159: iload 5
    //   161: ifeq +260 -> 421
    //   164: bipush 10
    //   166: istore 8
    //   168: iload 5
    //   170: iload 8
    //   172: if_icmpeq +236 -> 408
    //   175: bipush 18
    //   177: istore 8
    //   179: iload 5
    //   181: iload 8
    //   183: if_icmpeq +212 -> 395
    //   186: bipush 24
    //   188: istore 8
    //   190: iload 5
    //   192: iload 8
    //   194: if_icmpeq +186 -> 380
    //   197: bipush 32
    //   199: istore 8
    //   201: iload 5
    //   203: iload 8
    //   205: if_icmpeq +160 -> 365
    //   208: bipush 42
    //   210: istore 8
    //   212: iload 5
    //   214: iload 8
    //   216: if_icmpeq +97 -> 313
    //   219: bipush 50
    //   221: istore 8
    //   223: iload 5
    //   225: iload 8
    //   227: if_icmpeq +22 -> 249
    //   230: aload_2
    //   231: iload 5
    //   233: invokevirtual 91	com/google/f/g:skipField	(I)Z
    //   236: istore 5
    //   238: iload 5
    //   240: ifne -92 -> 148
    //   243: iconst_1
    //   244: istore 6
    //   246: goto -98 -> 148
    //   249: aload_0
    //   250: getfield 52	com/truecaller/api/services/messenger/v1/models/input/InputSpan:g	Lcom/google/f/w$h;
    //   253: astore_1
    //   254: aload_1
    //   255: invokeinterface 97 1 0
    //   260: istore 5
    //   262: iload 5
    //   264: ifne +18 -> 282
    //   267: aload_0
    //   268: getfield 52	com/truecaller/api/services/messenger/v1/models/input/InputSpan:g	Lcom/google/f/w$h;
    //   271: astore_1
    //   272: aload_1
    //   273: invokestatic 101	com/google/f/q:mutableCopy	(Lcom/google/f/w$h;)Lcom/google/f/w$h;
    //   276: astore_1
    //   277: aload_0
    //   278: aload_1
    //   279: putfield 52	com/truecaller/api/services/messenger/v1/models/input/InputSpan:g	Lcom/google/f/w$h;
    //   282: aload_0
    //   283: getfield 52	com/truecaller/api/services/messenger/v1/models/input/InputSpan:g	Lcom/google/f/w$h;
    //   286: astore_1
    //   287: invokestatic 106	com/truecaller/api/services/messenger/v1/models/input/InputSpan$Log:a	()Lcom/google/f/ah;
    //   290: astore 9
    //   292: aload_2
    //   293: aload 9
    //   295: aload_3
    //   296: invokevirtual 110	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   299: astore 9
    //   301: aload_1
    //   302: aload 9
    //   304: invokeinterface 114 2 0
    //   309: pop
    //   310: goto -162 -> 148
    //   313: aload_0
    //   314: getfield 40	com/truecaller/api/services/messenger/v1/models/input/InputSpan:f	Lcom/google/f/ad;
    //   317: astore_1
    //   318: aload_1
    //   319: invokevirtual 117	com/google/f/ad:isMutable	()Z
    //   322: istore 5
    //   324: iload 5
    //   326: ifne +18 -> 344
    //   329: aload_0
    //   330: getfield 40	com/truecaller/api/services/messenger/v1/models/input/InputSpan:f	Lcom/google/f/ad;
    //   333: astore_1
    //   334: aload_1
    //   335: invokevirtual 119	com/google/f/ad:mutableCopy	()Lcom/google/f/ad;
    //   338: astore_1
    //   339: aload_0
    //   340: aload_1
    //   341: putfield 40	com/truecaller/api/services/messenger/v1/models/input/InputSpan:f	Lcom/google/f/ad;
    //   344: getstatic 124	com/truecaller/api/services/messenger/v1/models/input/InputSpan$d:a	Lcom/google/f/ac;
    //   347: astore_1
    //   348: aload_0
    //   349: getfield 40	com/truecaller/api/services/messenger/v1/models/input/InputSpan:f	Lcom/google/f/ad;
    //   352: astore 9
    //   354: aload_1
    //   355: aload 9
    //   357: aload_2
    //   358: aload_3
    //   359: invokevirtual 130	com/google/f/ac:parseInto	(Lcom/google/f/ad;Lcom/google/f/g;Lcom/google/f/n;)V
    //   362: goto -214 -> 148
    //   365: aload_2
    //   366: invokevirtual 134	com/google/f/g:readInt64	()J
    //   369: lstore 10
    //   371: aload_0
    //   372: lload 10
    //   374: putfield 136	com/truecaller/api/services/messenger/v1/models/input/InputSpan:e	J
    //   377: goto -229 -> 148
    //   380: aload_2
    //   381: invokevirtual 134	com/google/f/g:readInt64	()J
    //   384: lstore 10
    //   386: aload_0
    //   387: lload 10
    //   389: putfield 138	com/truecaller/api/services/messenger/v1/models/input/InputSpan:d	J
    //   392: goto -244 -> 148
    //   395: aload_2
    //   396: invokevirtual 142	com/google/f/g:readStringRequireUtf8	()Ljava/lang/String;
    //   399: astore_1
    //   400: aload_0
    //   401: aload_1
    //   402: putfield 46	com/truecaller/api/services/messenger/v1/models/input/InputSpan:c	Ljava/lang/String;
    //   405: goto -257 -> 148
    //   408: aload_2
    //   409: invokevirtual 142	com/google/f/g:readStringRequireUtf8	()Ljava/lang/String;
    //   412: astore_1
    //   413: aload_0
    //   414: aload_1
    //   415: putfield 44	com/truecaller/api/services/messenger/v1/models/input/InputSpan:b	Ljava/lang/String;
    //   418: goto -270 -> 148
    //   421: iconst_1
    //   422: istore 6
    //   424: goto -276 -> 148
    //   427: astore_1
    //   428: goto +53 -> 481
    //   431: astore_1
    //   432: new 144	java/lang/RuntimeException
    //   435: astore_2
    //   436: new 146	com/google/f/x
    //   439: astore_3
    //   440: aload_1
    //   441: invokevirtual 151	java/io/IOException:getMessage	()Ljava/lang/String;
    //   444: astore_1
    //   445: aload_3
    //   446: aload_1
    //   447: invokespecial 154	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   450: aload_3
    //   451: aload_0
    //   452: invokevirtual 158	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   455: astore_1
    //   456: aload_2
    //   457: aload_1
    //   458: invokespecial 161	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   461: aload_2
    //   462: athrow
    //   463: astore_1
    //   464: new 144	java/lang/RuntimeException
    //   467: astore_2
    //   468: aload_1
    //   469: aload_0
    //   470: invokevirtual 158	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   473: astore_1
    //   474: aload_2
    //   475: aload_1
    //   476: invokespecial 161	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   479: aload_2
    //   480: athrow
    //   481: aload_1
    //   482: athrow
    //   483: getstatic 28	com/truecaller/api/services/messenger/v1/models/input/InputSpan:h	Lcom/truecaller/api/services/messenger/v1/models/input/InputSpan;
    //   486: areturn
    //   487: aload_2
    //   488: astore_1
    //   489: aload_2
    //   490: checkcast 163	com/google/f/q$k
    //   493: astore_1
    //   494: aload_3
    //   495: checkcast 2	com/truecaller/api/services/messenger/v1/models/input/InputSpan
    //   498: astore_3
    //   499: aload_0
    //   500: getfield 44	com/truecaller/api/services/messenger/v1/models/input/InputSpan:b	Ljava/lang/String;
    //   503: invokevirtual 168	java/lang/String:isEmpty	()Z
    //   506: iload 7
    //   508: ixor
    //   509: istore 12
    //   511: aload_0
    //   512: getfield 44	com/truecaller/api/services/messenger/v1/models/input/InputSpan:b	Ljava/lang/String;
    //   515: astore 9
    //   517: aload_3
    //   518: getfield 44	com/truecaller/api/services/messenger/v1/models/input/InputSpan:b	Ljava/lang/String;
    //   521: invokevirtual 168	java/lang/String:isEmpty	()Z
    //   524: iload 7
    //   526: ixor
    //   527: istore 13
    //   529: aload_3
    //   530: getfield 44	com/truecaller/api/services/messenger/v1/models/input/InputSpan:b	Ljava/lang/String;
    //   533: astore 14
    //   535: aload_1
    //   536: iload 12
    //   538: aload 9
    //   540: iload 13
    //   542: aload 14
    //   544: invokeinterface 172 5 0
    //   549: astore_2
    //   550: aload_0
    //   551: aload_2
    //   552: putfield 44	com/truecaller/api/services/messenger/v1/models/input/InputSpan:b	Ljava/lang/String;
    //   555: aload_0
    //   556: getfield 46	com/truecaller/api/services/messenger/v1/models/input/InputSpan:c	Ljava/lang/String;
    //   559: invokevirtual 168	java/lang/String:isEmpty	()Z
    //   562: iload 7
    //   564: ixor
    //   565: istore 12
    //   567: aload_0
    //   568: getfield 46	com/truecaller/api/services/messenger/v1/models/input/InputSpan:c	Ljava/lang/String;
    //   571: astore 9
    //   573: aload_3
    //   574: getfield 46	com/truecaller/api/services/messenger/v1/models/input/InputSpan:c	Ljava/lang/String;
    //   577: astore 15
    //   579: aload 15
    //   581: invokevirtual 168	java/lang/String:isEmpty	()Z
    //   584: iload 7
    //   586: ixor
    //   587: istore 13
    //   589: aload_3
    //   590: getfield 46	com/truecaller/api/services/messenger/v1/models/input/InputSpan:c	Ljava/lang/String;
    //   593: astore 14
    //   595: aload_1
    //   596: iload 12
    //   598: aload 9
    //   600: iload 13
    //   602: aload 14
    //   604: invokeinterface 172 5 0
    //   609: astore_2
    //   610: aload_0
    //   611: aload_2
    //   612: putfield 46	com/truecaller/api/services/messenger/v1/models/input/InputSpan:c	Ljava/lang/String;
    //   615: aload_0
    //   616: getfield 138	com/truecaller/api/services/messenger/v1/models/input/InputSpan:d	J
    //   619: lstore 10
    //   621: lconst_0
    //   622: lstore 16
    //   624: lload 10
    //   626: lload 16
    //   628: lcmp
    //   629: istore 12
    //   631: iload 12
    //   633: ifeq +9 -> 642
    //   636: iconst_1
    //   637: istore 13
    //   639: goto +9 -> 648
    //   642: iconst_0
    //   643: istore 13
    //   645: aconst_null
    //   646: astore 15
    //   648: aload_0
    //   649: getfield 138	com/truecaller/api/services/messenger/v1/models/input/InputSpan:d	J
    //   652: lstore 18
    //   654: aload_3
    //   655: getfield 138	com/truecaller/api/services/messenger/v1/models/input/InputSpan:d	J
    //   658: lstore 20
    //   660: lload 20
    //   662: lload 16
    //   664: lcmp
    //   665: istore 12
    //   667: iload 12
    //   669: ifeq +9 -> 678
    //   672: iconst_1
    //   673: istore 22
    //   675: goto +6 -> 681
    //   678: iconst_0
    //   679: istore 22
    //   681: aload_3
    //   682: getfield 138	com/truecaller/api/services/messenger/v1/models/input/InputSpan:d	J
    //   685: lstore 23
    //   687: aload_1
    //   688: astore 9
    //   690: aload_1
    //   691: iload 13
    //   693: lload 18
    //   695: iload 22
    //   697: lload 23
    //   699: invokeinterface 176 7 0
    //   704: lstore 10
    //   706: aload_0
    //   707: lload 10
    //   709: putfield 138	com/truecaller/api/services/messenger/v1/models/input/InputSpan:d	J
    //   712: aload_0
    //   713: getfield 136	com/truecaller/api/services/messenger/v1/models/input/InputSpan:e	J
    //   716: lstore 10
    //   718: lload 10
    //   720: lload 16
    //   722: lcmp
    //   723: istore 12
    //   725: iload 12
    //   727: ifeq +9 -> 736
    //   730: iconst_1
    //   731: istore 12
    //   733: goto +8 -> 741
    //   736: iconst_0
    //   737: istore 12
    //   739: aconst_null
    //   740: astore_2
    //   741: aload_0
    //   742: getfield 136	com/truecaller/api/services/messenger/v1/models/input/InputSpan:e	J
    //   745: lstore 10
    //   747: aload_3
    //   748: getfield 136	com/truecaller/api/services/messenger/v1/models/input/InputSpan:e	J
    //   751: lstore 18
    //   753: lload 18
    //   755: lload 16
    //   757: lcmp
    //   758: istore 22
    //   760: iload 22
    //   762: ifeq +9 -> 771
    //   765: iconst_1
    //   766: istore 25
    //   768: goto +9 -> 777
    //   771: iconst_0
    //   772: istore 25
    //   774: aconst_null
    //   775: astore 14
    //   777: aload_3
    //   778: getfield 136	com/truecaller/api/services/messenger/v1/models/input/InputSpan:e	J
    //   781: lstore 26
    //   783: aload_1
    //   784: astore 4
    //   786: iload 12
    //   788: istore 7
    //   790: aload_1
    //   791: iload 12
    //   793: lload 10
    //   795: iload 25
    //   797: lload 26
    //   799: invokeinterface 176 7 0
    //   804: lstore 28
    //   806: aload_0
    //   807: lload 28
    //   809: putfield 136	com/truecaller/api/services/messenger/v1/models/input/InputSpan:e	J
    //   812: aload_0
    //   813: getfield 40	com/truecaller/api/services/messenger/v1/models/input/InputSpan:f	Lcom/google/f/ad;
    //   816: astore_2
    //   817: aload_3
    //   818: getfield 40	com/truecaller/api/services/messenger/v1/models/input/InputSpan:f	Lcom/google/f/ad;
    //   821: astore 4
    //   823: aload_1
    //   824: aload_2
    //   825: aload 4
    //   827: invokeinterface 180 3 0
    //   832: astore_2
    //   833: aload_0
    //   834: aload_2
    //   835: putfield 40	com/truecaller/api/services/messenger/v1/models/input/InputSpan:f	Lcom/google/f/ad;
    //   838: aload_0
    //   839: getfield 52	com/truecaller/api/services/messenger/v1/models/input/InputSpan:g	Lcom/google/f/w$h;
    //   842: astore_2
    //   843: aload_3
    //   844: getfield 52	com/truecaller/api/services/messenger/v1/models/input/InputSpan:g	Lcom/google/f/w$h;
    //   847: astore 4
    //   849: aload_1
    //   850: aload_2
    //   851: aload 4
    //   853: invokeinterface 184 3 0
    //   858: astore_2
    //   859: aload_0
    //   860: aload_2
    //   861: putfield 52	com/truecaller/api/services/messenger/v1/models/input/InputSpan:g	Lcom/google/f/w$h;
    //   864: getstatic 190	com/google/f/q$i:INSTANCE	Lcom/google/f/q$i;
    //   867: astore_2
    //   868: aload_1
    //   869: aload_2
    //   870: if_acmpne +28 -> 898
    //   873: aload_0
    //   874: getfield 192	com/truecaller/api/services/messenger/v1/models/input/InputSpan:a	I
    //   877: istore 5
    //   879: aload_3
    //   880: getfield 192	com/truecaller/api/services/messenger/v1/models/input/InputSpan:a	I
    //   883: istore 12
    //   885: iload 5
    //   887: iload 12
    //   889: ior
    //   890: istore 5
    //   892: aload_0
    //   893: iload 5
    //   895: putfield 192	com/truecaller/api/services/messenger/v1/models/input/InputSpan:a	I
    //   898: aload_0
    //   899: areturn
    //   900: new 194	com/truecaller/api/services/messenger/v1/models/input/InputSpan$a
    //   903: astore_1
    //   904: aload_1
    //   905: iconst_0
    //   906: invokespecial 197	com/truecaller/api/services/messenger/v1/models/input/InputSpan$a:<init>	(B)V
    //   909: aload_1
    //   910: areturn
    //   911: aload_0
    //   912: getfield 40	com/truecaller/api/services/messenger/v1/models/input/InputSpan:f	Lcom/google/f/ad;
    //   915: invokevirtual 198	com/google/f/ad:makeImmutable	()V
    //   918: aload_0
    //   919: getfield 52	com/truecaller/api/services/messenger/v1/models/input/InputSpan:g	Lcom/google/f/w$h;
    //   922: invokeinterface 199 1 0
    //   927: aconst_null
    //   928: areturn
    //   929: getstatic 28	com/truecaller/api/services/messenger/v1/models/input/InputSpan:h	Lcom/truecaller/api/services/messenger/v1/models/input/InputSpan;
    //   932: areturn
    //   933: new 2	com/truecaller/api/services/messenger/v1/models/input/InputSpan
    //   936: astore_1
    //   937: aload_1
    //   938: invokespecial 26	com/truecaller/api/services/messenger/v1/models/input/InputSpan:<init>	()V
    //   941: aload_1
    //   942: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	943	0	this	InputSpan
    //   0	943	1	paramj	com.google.f.q.j
    //   0	943	2	paramObject1	Object
    //   0	943	3	paramObject2	Object
    //   3	849	4	localObject1	Object
    //   9	223	5	j	int
    //   236	89	5	bool1	boolean
    //   877	17	5	k	int
    //   19	404	6	m	int
    //   25	764	7	n	int
    //   166	62	8	i1	int
    //   290	399	9	localObject2	Object
    //   369	425	10	l1	long
    //   509	283	12	bool2	boolean
    //   883	7	12	i2	int
    //   527	165	13	bool3	boolean
    //   533	243	14	str1	String
    //   577	70	15	str2	String
    //   622	134	16	l2	long
    //   652	102	18	l3	long
    //   658	3	20	l4	long
    //   673	88	22	bool4	boolean
    //   685	13	23	l5	long
    //   766	30	25	bool5	boolean
    //   781	17	26	l6	long
    //   804	4	28	l7	long
    // Exception table:
    //   from	to	target	type
    //   99	102	129	finally
    //   107	110	129	finally
    //   111	114	129	finally
    //   116	120	129	finally
    //   120	124	129	finally
    //   124	126	129	finally
    //   130	132	129	finally
    //   153	157	427	finally
    //   231	236	427	finally
    //   249	253	427	finally
    //   254	260	427	finally
    //   267	271	427	finally
    //   272	276	427	finally
    //   278	282	427	finally
    //   282	286	427	finally
    //   287	290	427	finally
    //   295	299	427	finally
    //   302	310	427	finally
    //   313	317	427	finally
    //   318	322	427	finally
    //   329	333	427	finally
    //   334	338	427	finally
    //   340	344	427	finally
    //   344	347	427	finally
    //   348	352	427	finally
    //   358	362	427	finally
    //   365	369	427	finally
    //   372	377	427	finally
    //   380	384	427	finally
    //   387	392	427	finally
    //   395	399	427	finally
    //   401	405	427	finally
    //   408	412	427	finally
    //   414	418	427	finally
    //   432	435	427	finally
    //   436	439	427	finally
    //   440	444	427	finally
    //   446	450	427	finally
    //   451	455	427	finally
    //   457	461	427	finally
    //   461	463	427	finally
    //   464	467	427	finally
    //   469	473	427	finally
    //   475	479	427	finally
    //   479	481	427	finally
    //   153	157	431	java/io/IOException
    //   231	236	431	java/io/IOException
    //   249	253	431	java/io/IOException
    //   254	260	431	java/io/IOException
    //   267	271	431	java/io/IOException
    //   272	276	431	java/io/IOException
    //   278	282	431	java/io/IOException
    //   282	286	431	java/io/IOException
    //   287	290	431	java/io/IOException
    //   295	299	431	java/io/IOException
    //   302	310	431	java/io/IOException
    //   313	317	431	java/io/IOException
    //   318	322	431	java/io/IOException
    //   329	333	431	java/io/IOException
    //   334	338	431	java/io/IOException
    //   340	344	431	java/io/IOException
    //   344	347	431	java/io/IOException
    //   348	352	431	java/io/IOException
    //   358	362	431	java/io/IOException
    //   365	369	431	java/io/IOException
    //   372	377	431	java/io/IOException
    //   380	384	431	java/io/IOException
    //   387	392	431	java/io/IOException
    //   395	399	431	java/io/IOException
    //   401	405	431	java/io/IOException
    //   408	412	431	java/io/IOException
    //   414	418	431	java/io/IOException
    //   153	157	463	com/google/f/x
    //   231	236	463	com/google/f/x
    //   249	253	463	com/google/f/x
    //   254	260	463	com/google/f/x
    //   267	271	463	com/google/f/x
    //   272	276	463	com/google/f/x
    //   278	282	463	com/google/f/x
    //   282	286	463	com/google/f/x
    //   287	290	463	com/google/f/x
    //   295	299	463	com/google/f/x
    //   302	310	463	com/google/f/x
    //   313	317	463	com/google/f/x
    //   318	322	463	com/google/f/x
    //   329	333	463	com/google/f/x
    //   334	338	463	com/google/f/x
    //   340	344	463	com/google/f/x
    //   344	347	463	com/google/f/x
    //   348	352	463	com/google/f/x
    //   358	362	463	com/google/f/x
    //   365	369	463	com/google/f/x
    //   372	377	463	com/google/f/x
    //   380	384	463	com/google/f/x
    //   387	392	463	com/google/f/x
    //   395	399	463	com/google/f/x
    //   401	405	463	com/google/f/x
    //   408	412	463	com/google/f/x
    //   414	418	463	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int j = memoizedSerializedSize;
    int m = -1;
    if (j != m) {
      return j;
    }
    String str = b;
    boolean bool1 = str.isEmpty();
    m = 0;
    int k;
    if (!bool1)
    {
      str = b;
      int n = 1;
      k = h.computeStringSize(n, str) + 0;
    }
    else
    {
      k = 0;
      str = null;
    }
    Object localObject1 = c;
    boolean bool2 = ((String)localObject1).isEmpty();
    Object localObject2;
    int i1;
    if (!bool2)
    {
      localObject2 = c;
      i1 = h.computeStringSize(2, (String)localObject2);
      k += i1;
    }
    long l1 = d;
    long l2 = 0L;
    boolean bool3 = l1 < l2;
    if (bool3)
    {
      int i2 = 3;
      i1 = h.computeInt64Size(i2, l1);
      k += i1;
    }
    l1 = e;
    boolean bool4 = l1 < l2;
    if (bool4)
    {
      int i3 = 4;
      i1 = h.computeInt64Size(i3, l1);
      k += i1;
    }
    localObject1 = f.entrySet().iterator();
    for (;;)
    {
      boolean bool5 = ((Iterator)localObject1).hasNext();
      if (!bool5) {
        break;
      }
      localObject2 = (Map.Entry)((Iterator)localObject1).next();
      ac localac = InputSpan.d.a;
      int i5 = 5;
      Object localObject3 = ((Map.Entry)localObject2).getKey();
      localObject2 = ((Map.Entry)localObject2).getValue();
      int i4 = localac.computeMessageSize(i5, localObject3, localObject2);
      k += i4;
    }
    for (;;)
    {
      localObject1 = g;
      i1 = ((w.h)localObject1).size();
      if (m >= i1) {
        break;
      }
      localObject2 = (ae)g.get(m);
      i1 = h.computeMessageSize(6, (ae)localObject2);
      k += i1;
      m += 1;
    }
    memoizedSerializedSize = k;
    return k;
  }
  
  public final void writeTo(h paramh)
  {
    Object localObject1 = b;
    boolean bool1 = ((String)localObject1).isEmpty();
    if (!bool1)
    {
      localObject1 = b;
      int k = 1;
      paramh.writeString(k, (String)localObject1);
    }
    localObject1 = c;
    bool1 = ((String)localObject1).isEmpty();
    Object localObject2;
    if (!bool1)
    {
      j = 2;
      localObject2 = c;
      paramh.writeString(j, (String)localObject2);
    }
    long l1 = d;
    long l2 = 0L;
    boolean bool3 = l1 < l2;
    if (bool3)
    {
      int n = 3;
      paramh.writeInt64(n, l1);
    }
    l1 = e;
    boolean bool4 = l1 < l2;
    if (bool4)
    {
      int i1 = 4;
      paramh.writeInt64(i1, l1);
    }
    localObject1 = f.entrySet().iterator();
    Object localObject3;
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject1).hasNext();
      if (!bool2) {
        break;
      }
      localObject2 = (Map.Entry)((Iterator)localObject1).next();
      localObject3 = InputSpan.d.a;
      int i2 = 5;
      Object localObject4 = ((Map.Entry)localObject2).getKey();
      localObject2 = ((Map.Entry)localObject2).getValue();
      ((ac)localObject3).serializeTo(paramh, i2, localObject4, localObject2);
    }
    int j = 0;
    localObject1 = null;
    for (;;)
    {
      localObject2 = g;
      int m = ((w.h)localObject2).size();
      if (j >= m) {
        break;
      }
      m = 6;
      localObject3 = (ae)g.get(j);
      paramh.writeMessage(m, (ae)localObject3);
      j += 1;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.models.input.InputSpan
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */