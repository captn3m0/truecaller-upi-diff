package com.truecaller.api.services.messenger.v1.models;

import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;

public final class a
  extends q
  implements b
{
  private static final a e;
  private static volatile ah f;
  private int a;
  private int b;
  private int c;
  private int d;
  
  static
  {
    a locala = new com/truecaller/api/services/messenger/v1/models/a;
    locala.<init>();
    e = locala;
    locala.makeImmutable();
  }
  
  public static a e()
  {
    return e;
  }
  
  public static ah f()
  {
    return e.getParserForType();
  }
  
  public final int a()
  {
    return a;
  }
  
  public final int b()
  {
    return b;
  }
  
  public final int c()
  {
    return c;
  }
  
  public final int d()
  {
    return d;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 42	com/truecaller/api/services/messenger/v1/models/a$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 48	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_1
    //   19: istore 6
    //   21: iconst_0
    //   22: istore 7
    //   24: iload 5
    //   26: tableswitch	default:+46->72, 1:+651->677, 2:+647->673, 3:+645->671, 4:+634->660, 5:+318->344, 6:+108->134, 7:+314->340, 8:+56->82
    //   72: new 51	java/lang/UnsupportedOperationException
    //   75: astore_1
    //   76: aload_1
    //   77: invokespecial 52	java/lang/UnsupportedOperationException:<init>	()V
    //   80: aload_1
    //   81: athrow
    //   82: getstatic 54	com/truecaller/api/services/messenger/v1/models/a:f	Lcom/google/f/ah;
    //   85: astore_1
    //   86: aload_1
    //   87: ifnonnull +43 -> 130
    //   90: ldc 2
    //   92: astore_1
    //   93: aload_1
    //   94: monitorenter
    //   95: getstatic 54	com/truecaller/api/services/messenger/v1/models/a:f	Lcom/google/f/ah;
    //   98: astore_2
    //   99: aload_2
    //   100: ifnonnull +20 -> 120
    //   103: new 56	com/google/f/q$b
    //   106: astore_2
    //   107: getstatic 21	com/truecaller/api/services/messenger/v1/models/a:e	Lcom/truecaller/api/services/messenger/v1/models/a;
    //   110: astore_3
    //   111: aload_2
    //   112: aload_3
    //   113: invokespecial 59	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   116: aload_2
    //   117: putstatic 54	com/truecaller/api/services/messenger/v1/models/a:f	Lcom/google/f/ah;
    //   120: aload_1
    //   121: monitorexit
    //   122: goto +8 -> 130
    //   125: astore_2
    //   126: aload_1
    //   127: monitorexit
    //   128: aload_2
    //   129: athrow
    //   130: getstatic 54	com/truecaller/api/services/messenger/v1/models/a:f	Lcom/google/f/ah;
    //   133: areturn
    //   134: aload_2
    //   135: checkcast 61	com/google/f/g
    //   138: astore_2
    //   139: iload 7
    //   141: ifne +199 -> 340
    //   144: aload_2
    //   145: invokevirtual 64	com/google/f/g:readTag	()I
    //   148: istore 5
    //   150: iload 5
    //   152: ifeq +126 -> 278
    //   155: bipush 8
    //   157: istore 8
    //   159: iload 5
    //   161: iload 8
    //   163: if_icmpeq +100 -> 263
    //   166: bipush 16
    //   168: istore 8
    //   170: iload 5
    //   172: iload 8
    //   174: if_icmpeq +74 -> 248
    //   177: bipush 24
    //   179: istore 8
    //   181: iload 5
    //   183: iload 8
    //   185: if_icmpeq +48 -> 233
    //   188: bipush 32
    //   190: istore 8
    //   192: iload 5
    //   194: iload 8
    //   196: if_icmpeq +22 -> 218
    //   199: aload_2
    //   200: iload 5
    //   202: invokevirtual 72	com/google/f/g:skipField	(I)Z
    //   205: istore 5
    //   207: iload 5
    //   209: ifne -70 -> 139
    //   212: iconst_1
    //   213: istore 7
    //   215: goto -76 -> 139
    //   218: aload_2
    //   219: invokevirtual 75	com/google/f/g:readInt32	()I
    //   222: istore 5
    //   224: aload_0
    //   225: iload 5
    //   227: putfield 37	com/truecaller/api/services/messenger/v1/models/a:d	I
    //   230: goto -91 -> 139
    //   233: aload_2
    //   234: invokevirtual 75	com/google/f/g:readInt32	()I
    //   237: istore 5
    //   239: aload_0
    //   240: iload 5
    //   242: putfield 35	com/truecaller/api/services/messenger/v1/models/a:c	I
    //   245: goto -106 -> 139
    //   248: aload_2
    //   249: invokevirtual 75	com/google/f/g:readInt32	()I
    //   252: istore 5
    //   254: aload_0
    //   255: iload 5
    //   257: putfield 33	com/truecaller/api/services/messenger/v1/models/a:b	I
    //   260: goto -121 -> 139
    //   263: aload_2
    //   264: invokevirtual 75	com/google/f/g:readInt32	()I
    //   267: istore 5
    //   269: aload_0
    //   270: iload 5
    //   272: putfield 31	com/truecaller/api/services/messenger/v1/models/a:a	I
    //   275: goto -136 -> 139
    //   278: iconst_1
    //   279: istore 7
    //   281: goto -142 -> 139
    //   284: astore_1
    //   285: goto +53 -> 338
    //   288: astore_1
    //   289: new 77	java/lang/RuntimeException
    //   292: astore_2
    //   293: new 79	com/google/f/x
    //   296: astore_3
    //   297: aload_1
    //   298: invokevirtual 85	java/io/IOException:getMessage	()Ljava/lang/String;
    //   301: astore_1
    //   302: aload_3
    //   303: aload_1
    //   304: invokespecial 88	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   307: aload_3
    //   308: aload_0
    //   309: invokevirtual 92	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   312: astore_1
    //   313: aload_2
    //   314: aload_1
    //   315: invokespecial 95	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   318: aload_2
    //   319: athrow
    //   320: astore_1
    //   321: new 77	java/lang/RuntimeException
    //   324: astore_2
    //   325: aload_1
    //   326: aload_0
    //   327: invokevirtual 92	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   330: astore_1
    //   331: aload_2
    //   332: aload_1
    //   333: invokespecial 95	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   336: aload_2
    //   337: athrow
    //   338: aload_1
    //   339: athrow
    //   340: getstatic 21	com/truecaller/api/services/messenger/v1/models/a:e	Lcom/truecaller/api/services/messenger/v1/models/a;
    //   343: areturn
    //   344: aload_2
    //   345: checkcast 97	com/google/f/q$k
    //   348: astore_2
    //   349: aload_3
    //   350: checkcast 2	com/truecaller/api/services/messenger/v1/models/a
    //   353: astore_3
    //   354: aload_0
    //   355: getfield 31	com/truecaller/api/services/messenger/v1/models/a:a	I
    //   358: istore 5
    //   360: iload 5
    //   362: ifeq +9 -> 371
    //   365: iconst_1
    //   366: istore 5
    //   368: goto +8 -> 376
    //   371: iconst_0
    //   372: istore 5
    //   374: aconst_null
    //   375: astore_1
    //   376: aload_0
    //   377: getfield 31	com/truecaller/api/services/messenger/v1/models/a:a	I
    //   380: istore 9
    //   382: aload_3
    //   383: getfield 31	com/truecaller/api/services/messenger/v1/models/a:a	I
    //   386: istore 10
    //   388: iload 10
    //   390: ifeq +9 -> 399
    //   393: iconst_1
    //   394: istore 10
    //   396: goto +6 -> 402
    //   399: iconst_0
    //   400: istore 10
    //   402: aload_3
    //   403: getfield 31	com/truecaller/api/services/messenger/v1/models/a:a	I
    //   406: istore 11
    //   408: aload_2
    //   409: iload 5
    //   411: iload 9
    //   413: iload 10
    //   415: iload 11
    //   417: invokeinterface 101 5 0
    //   422: istore 5
    //   424: aload_0
    //   425: iload 5
    //   427: putfield 31	com/truecaller/api/services/messenger/v1/models/a:a	I
    //   430: aload_0
    //   431: getfield 33	com/truecaller/api/services/messenger/v1/models/a:b	I
    //   434: istore 5
    //   436: iload 5
    //   438: ifeq +9 -> 447
    //   441: iconst_1
    //   442: istore 5
    //   444: goto +8 -> 452
    //   447: iconst_0
    //   448: istore 5
    //   450: aconst_null
    //   451: astore_1
    //   452: aload_0
    //   453: getfield 33	com/truecaller/api/services/messenger/v1/models/a:b	I
    //   456: istore 9
    //   458: aload_3
    //   459: getfield 33	com/truecaller/api/services/messenger/v1/models/a:b	I
    //   462: istore 10
    //   464: iload 10
    //   466: ifeq +9 -> 475
    //   469: iconst_1
    //   470: istore 10
    //   472: goto +6 -> 478
    //   475: iconst_0
    //   476: istore 10
    //   478: aload_3
    //   479: getfield 33	com/truecaller/api/services/messenger/v1/models/a:b	I
    //   482: istore 11
    //   484: aload_2
    //   485: iload 5
    //   487: iload 9
    //   489: iload 10
    //   491: iload 11
    //   493: invokeinterface 101 5 0
    //   498: istore 5
    //   500: aload_0
    //   501: iload 5
    //   503: putfield 33	com/truecaller/api/services/messenger/v1/models/a:b	I
    //   506: aload_0
    //   507: getfield 35	com/truecaller/api/services/messenger/v1/models/a:c	I
    //   510: istore 5
    //   512: iload 5
    //   514: ifeq +9 -> 523
    //   517: iconst_1
    //   518: istore 5
    //   520: goto +8 -> 528
    //   523: iconst_0
    //   524: istore 5
    //   526: aconst_null
    //   527: astore_1
    //   528: aload_0
    //   529: getfield 35	com/truecaller/api/services/messenger/v1/models/a:c	I
    //   532: istore 9
    //   534: aload_3
    //   535: getfield 35	com/truecaller/api/services/messenger/v1/models/a:c	I
    //   538: istore 10
    //   540: iload 10
    //   542: ifeq +9 -> 551
    //   545: iconst_1
    //   546: istore 10
    //   548: goto +6 -> 554
    //   551: iconst_0
    //   552: istore 10
    //   554: aload_3
    //   555: getfield 35	com/truecaller/api/services/messenger/v1/models/a:c	I
    //   558: istore 11
    //   560: aload_2
    //   561: iload 5
    //   563: iload 9
    //   565: iload 10
    //   567: iload 11
    //   569: invokeinterface 101 5 0
    //   574: istore 5
    //   576: aload_0
    //   577: iload 5
    //   579: putfield 35	com/truecaller/api/services/messenger/v1/models/a:c	I
    //   582: aload_0
    //   583: getfield 37	com/truecaller/api/services/messenger/v1/models/a:d	I
    //   586: istore 5
    //   588: iload 5
    //   590: ifeq +9 -> 599
    //   593: iconst_1
    //   594: istore 5
    //   596: goto +8 -> 604
    //   599: iconst_0
    //   600: istore 5
    //   602: aconst_null
    //   603: astore_1
    //   604: aload_0
    //   605: getfield 37	com/truecaller/api/services/messenger/v1/models/a:d	I
    //   608: istore 9
    //   610: aload_3
    //   611: getfield 37	com/truecaller/api/services/messenger/v1/models/a:d	I
    //   614: istore 10
    //   616: iload 10
    //   618: ifeq +6 -> 624
    //   621: goto +9 -> 630
    //   624: iconst_0
    //   625: istore 6
    //   627: aconst_null
    //   628: astore 4
    //   630: aload_3
    //   631: getfield 37	com/truecaller/api/services/messenger/v1/models/a:d	I
    //   634: istore 8
    //   636: aload_2
    //   637: iload 5
    //   639: iload 9
    //   641: iload 6
    //   643: iload 8
    //   645: invokeinterface 101 5 0
    //   650: istore 5
    //   652: aload_0
    //   653: iload 5
    //   655: putfield 37	com/truecaller/api/services/messenger/v1/models/a:d	I
    //   658: aload_0
    //   659: areturn
    //   660: new 103	com/truecaller/api/services/messenger/v1/models/a$a
    //   663: astore_1
    //   664: aload_1
    //   665: iconst_0
    //   666: invokespecial 106	com/truecaller/api/services/messenger/v1/models/a$a:<init>	(B)V
    //   669: aload_1
    //   670: areturn
    //   671: aconst_null
    //   672: areturn
    //   673: getstatic 21	com/truecaller/api/services/messenger/v1/models/a:e	Lcom/truecaller/api/services/messenger/v1/models/a;
    //   676: areturn
    //   677: new 2	com/truecaller/api/services/messenger/v1/models/a
    //   680: astore_1
    //   681: aload_1
    //   682: invokespecial 19	com/truecaller/api/services/messenger/v1/models/a:<init>	()V
    //   685: aload_1
    //   686: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	687	0	this	a
    //   0	687	1	paramj	com.google.f.q.j
    //   0	687	2	paramObject1	Object
    //   0	687	3	paramObject2	Object
    //   3	626	4	arrayOfInt	int[]
    //   9	192	5	i	int
    //   205	3	5	bool1	boolean
    //   222	188	5	j	int
    //   422	64	5	k	int
    //   498	64	5	m	int
    //   574	64	5	n	int
    //   650	4	5	i1	int
    //   19	623	6	bool2	boolean
    //   22	258	7	i2	int
    //   157	487	8	i3	int
    //   380	260	9	i4	int
    //   386	28	10	i5	int
    //   462	28	10	i6	int
    //   538	28	10	i7	int
    //   614	3	10	i8	int
    //   406	162	11	i9	int
    // Exception table:
    //   from	to	target	type
    //   95	98	125	finally
    //   103	106	125	finally
    //   107	110	125	finally
    //   112	116	125	finally
    //   116	120	125	finally
    //   120	122	125	finally
    //   126	128	125	finally
    //   144	148	284	finally
    //   200	205	284	finally
    //   218	222	284	finally
    //   225	230	284	finally
    //   233	237	284	finally
    //   240	245	284	finally
    //   248	252	284	finally
    //   255	260	284	finally
    //   263	267	284	finally
    //   270	275	284	finally
    //   289	292	284	finally
    //   293	296	284	finally
    //   297	301	284	finally
    //   303	307	284	finally
    //   308	312	284	finally
    //   314	318	284	finally
    //   318	320	284	finally
    //   321	324	284	finally
    //   326	330	284	finally
    //   332	336	284	finally
    //   336	338	284	finally
    //   144	148	288	java/io/IOException
    //   200	205	288	java/io/IOException
    //   218	222	288	java/io/IOException
    //   225	230	288	java/io/IOException
    //   233	237	288	java/io/IOException
    //   240	245	288	java/io/IOException
    //   248	252	288	java/io/IOException
    //   255	260	288	java/io/IOException
    //   263	267	288	java/io/IOException
    //   270	275	288	java/io/IOException
    //   144	148	320	com/google/f/x
    //   200	205	320	com/google/f/x
    //   218	222	320	com/google/f/x
    //   225	230	320	com/google/f/x
    //   233	237	320	com/google/f/x
    //   240	245	320	com/google/f/x
    //   248	252	320	com/google/f/x
    //   255	260	320	com/google/f/x
    //   263	267	320	com/google/f/x
    //   270	275	320	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int j = -1;
    if (i != j) {
      return i;
    }
    i = a;
    j = 0;
    int k;
    if (i != 0)
    {
      k = 1;
      i = h.computeInt32Size(k, i);
      j = 0 + i;
    }
    i = b;
    if (i != 0)
    {
      k = 2;
      i = h.computeInt32Size(k, i);
      j += i;
    }
    i = c;
    if (i != 0)
    {
      k = 3;
      i = h.computeInt32Size(k, i);
      j += i;
    }
    i = d;
    if (i != 0)
    {
      k = 4;
      i = h.computeInt32Size(k, i);
      j += i;
    }
    memoizedSerializedSize = j;
    return j;
  }
  
  public final void writeTo(h paramh)
  {
    int i = a;
    int j;
    if (i != 0)
    {
      j = 1;
      paramh.writeInt32(j, i);
    }
    i = b;
    if (i != 0)
    {
      j = 2;
      paramh.writeInt32(j, i);
    }
    i = c;
    if (i != 0)
    {
      j = 3;
      paramh.writeInt32(j, i);
    }
    i = d;
    if (i != 0)
    {
      j = 4;
      paramh.writeInt32(j, i);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.models.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */