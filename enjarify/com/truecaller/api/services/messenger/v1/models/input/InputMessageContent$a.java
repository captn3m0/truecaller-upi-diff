package com.truecaller.api.services.messenger.v1.models.input;

import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;

public final class InputMessageContent$a
  extends q
  implements InputMessageContent.b
{
  private static final a e;
  private static volatile ah f;
  private String a = "";
  private String b = "";
  private int c;
  private int d;
  
  static
  {
    a locala = new com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a;
    locala.<init>();
    e = locala;
    locala.makeImmutable();
  }
  
  public static InputMessageContent.a.a a()
  {
    return (InputMessageContent.a.a)e.toBuilder();
  }
  
  public static ah b()
  {
    return e.getParserForType();
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 54	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 60	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_0
    //   19: istore 6
    //   21: aconst_null
    //   22: astore 4
    //   24: iconst_1
    //   25: istore 7
    //   27: iload 5
    //   29: tableswitch	default:+47->76, 1:+609->638, 2:+605->634, 3:+603->632, 4:+592->621, 5:+315->344, 6:+109->138, 7:+311->340, 8:+57->86
    //   76: new 63	java/lang/UnsupportedOperationException
    //   79: astore_1
    //   80: aload_1
    //   81: invokespecial 64	java/lang/UnsupportedOperationException:<init>	()V
    //   84: aload_1
    //   85: athrow
    //   86: getstatic 66	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a:f	Lcom/google/f/ah;
    //   89: astore_1
    //   90: aload_1
    //   91: ifnonnull +43 -> 134
    //   94: ldc 2
    //   96: astore_1
    //   97: aload_1
    //   98: monitorenter
    //   99: getstatic 66	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a:f	Lcom/google/f/ah;
    //   102: astore_2
    //   103: aload_2
    //   104: ifnonnull +20 -> 124
    //   107: new 68	com/google/f/q$b
    //   110: astore_2
    //   111: getstatic 22	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a:e	Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a;
    //   114: astore_3
    //   115: aload_2
    //   116: aload_3
    //   117: invokespecial 71	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   120: aload_2
    //   121: putstatic 66	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a:f	Lcom/google/f/ah;
    //   124: aload_1
    //   125: monitorexit
    //   126: goto +8 -> 134
    //   129: astore_2
    //   130: aload_1
    //   131: monitorexit
    //   132: aload_2
    //   133: athrow
    //   134: getstatic 66	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a:f	Lcom/google/f/ah;
    //   137: areturn
    //   138: aload_2
    //   139: checkcast 73	com/google/f/g
    //   142: astore_2
    //   143: iload 6
    //   145: ifne +195 -> 340
    //   148: aload_2
    //   149: invokevirtual 76	com/google/f/g:readTag	()I
    //   152: istore 5
    //   154: iload 5
    //   156: ifeq +122 -> 278
    //   159: bipush 10
    //   161: istore 8
    //   163: iload 5
    //   165: iload 8
    //   167: if_icmpeq +98 -> 265
    //   170: bipush 18
    //   172: istore 8
    //   174: iload 5
    //   176: iload 8
    //   178: if_icmpeq +74 -> 252
    //   181: bipush 24
    //   183: istore 8
    //   185: iload 5
    //   187: iload 8
    //   189: if_icmpeq +48 -> 237
    //   192: bipush 32
    //   194: istore 8
    //   196: iload 5
    //   198: iload 8
    //   200: if_icmpeq +22 -> 222
    //   203: aload_2
    //   204: iload 5
    //   206: invokevirtual 84	com/google/f/g:skipField	(I)Z
    //   209: istore 5
    //   211: iload 5
    //   213: ifne -70 -> 143
    //   216: iconst_1
    //   217: istore 6
    //   219: goto -76 -> 143
    //   222: aload_2
    //   223: invokevirtual 87	com/google/f/g:readInt32	()I
    //   226: istore 5
    //   228: aload_0
    //   229: iload 5
    //   231: putfield 49	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a:d	I
    //   234: goto -91 -> 143
    //   237: aload_2
    //   238: invokevirtual 87	com/google/f/g:readInt32	()I
    //   241: istore 5
    //   243: aload_0
    //   244: iload 5
    //   246: putfield 40	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a:c	I
    //   249: goto -106 -> 143
    //   252: aload_2
    //   253: invokevirtual 91	com/google/f/g:readStringRequireUtf8	()Ljava/lang/String;
    //   256: astore_1
    //   257: aload_0
    //   258: aload_1
    //   259: putfield 32	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a:b	Ljava/lang/String;
    //   262: goto -119 -> 143
    //   265: aload_2
    //   266: invokevirtual 91	com/google/f/g:readStringRequireUtf8	()Ljava/lang/String;
    //   269: astore_1
    //   270: aload_0
    //   271: aload_1
    //   272: putfield 30	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a:a	Ljava/lang/String;
    //   275: goto -132 -> 143
    //   278: iconst_1
    //   279: istore 6
    //   281: goto -138 -> 143
    //   284: astore_1
    //   285: goto +53 -> 338
    //   288: astore_1
    //   289: new 93	java/lang/RuntimeException
    //   292: astore_2
    //   293: new 95	com/google/f/x
    //   296: astore_3
    //   297: aload_1
    //   298: invokevirtual 100	java/io/IOException:getMessage	()Ljava/lang/String;
    //   301: astore_1
    //   302: aload_3
    //   303: aload_1
    //   304: invokespecial 103	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   307: aload_3
    //   308: aload_0
    //   309: invokevirtual 107	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   312: astore_1
    //   313: aload_2
    //   314: aload_1
    //   315: invokespecial 110	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   318: aload_2
    //   319: athrow
    //   320: astore_1
    //   321: new 93	java/lang/RuntimeException
    //   324: astore_2
    //   325: aload_1
    //   326: aload_0
    //   327: invokevirtual 107	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   330: astore_1
    //   331: aload_2
    //   332: aload_1
    //   333: invokespecial 110	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   336: aload_2
    //   337: athrow
    //   338: aload_1
    //   339: athrow
    //   340: getstatic 22	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a:e	Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a;
    //   343: areturn
    //   344: aload_2
    //   345: checkcast 112	com/google/f/q$k
    //   348: astore_2
    //   349: aload_3
    //   350: checkcast 2	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a
    //   353: astore_3
    //   354: aload_0
    //   355: getfield 30	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a:a	Ljava/lang/String;
    //   358: invokevirtual 118	java/lang/String:isEmpty	()Z
    //   361: iload 7
    //   363: ixor
    //   364: istore 5
    //   366: aload_0
    //   367: getfield 30	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a:a	Ljava/lang/String;
    //   370: astore 9
    //   372: aload_3
    //   373: getfield 30	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a:a	Ljava/lang/String;
    //   376: invokevirtual 118	java/lang/String:isEmpty	()Z
    //   379: iload 7
    //   381: ixor
    //   382: istore 10
    //   384: aload_3
    //   385: getfield 30	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a:a	Ljava/lang/String;
    //   388: astore 11
    //   390: aload_2
    //   391: iload 5
    //   393: aload 9
    //   395: iload 10
    //   397: aload 11
    //   399: invokeinterface 122 5 0
    //   404: astore_1
    //   405: aload_0
    //   406: aload_1
    //   407: putfield 30	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a:a	Ljava/lang/String;
    //   410: aload_0
    //   411: getfield 32	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a:b	Ljava/lang/String;
    //   414: invokevirtual 118	java/lang/String:isEmpty	()Z
    //   417: iload 7
    //   419: ixor
    //   420: istore 5
    //   422: aload_0
    //   423: getfield 32	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a:b	Ljava/lang/String;
    //   426: astore 9
    //   428: aload_3
    //   429: getfield 32	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a:b	Ljava/lang/String;
    //   432: astore 12
    //   434: aload 12
    //   436: invokevirtual 118	java/lang/String:isEmpty	()Z
    //   439: iload 7
    //   441: ixor
    //   442: istore 10
    //   444: aload_3
    //   445: getfield 32	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a:b	Ljava/lang/String;
    //   448: astore 11
    //   450: aload_2
    //   451: iload 5
    //   453: aload 9
    //   455: iload 10
    //   457: aload 11
    //   459: invokeinterface 122 5 0
    //   464: astore_1
    //   465: aload_0
    //   466: aload_1
    //   467: putfield 32	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a:b	Ljava/lang/String;
    //   470: aload_0
    //   471: getfield 40	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a:c	I
    //   474: istore 5
    //   476: iload 5
    //   478: ifeq +9 -> 487
    //   481: iconst_1
    //   482: istore 5
    //   484: goto +8 -> 492
    //   487: iconst_0
    //   488: istore 5
    //   490: aconst_null
    //   491: astore_1
    //   492: aload_0
    //   493: getfield 40	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a:c	I
    //   496: istore 13
    //   498: aload_3
    //   499: getfield 40	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a:c	I
    //   502: istore 10
    //   504: iload 10
    //   506: ifeq +9 -> 515
    //   509: iconst_1
    //   510: istore 10
    //   512: goto +9 -> 521
    //   515: iconst_0
    //   516: istore 10
    //   518: aconst_null
    //   519: astore 12
    //   521: aload_3
    //   522: getfield 40	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a:c	I
    //   525: istore 14
    //   527: aload_2
    //   528: iload 5
    //   530: iload 13
    //   532: iload 10
    //   534: iload 14
    //   536: invokeinterface 126 5 0
    //   541: istore 5
    //   543: aload_0
    //   544: iload 5
    //   546: putfield 40	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a:c	I
    //   549: aload_0
    //   550: getfield 49	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a:d	I
    //   553: istore 5
    //   555: iload 5
    //   557: ifeq +9 -> 566
    //   560: iconst_1
    //   561: istore 5
    //   563: goto +8 -> 571
    //   566: iconst_0
    //   567: istore 5
    //   569: aconst_null
    //   570: astore_1
    //   571: aload_0
    //   572: getfield 49	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a:d	I
    //   575: istore 13
    //   577: aload_3
    //   578: getfield 49	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a:d	I
    //   581: istore 10
    //   583: iload 10
    //   585: ifeq +6 -> 591
    //   588: iconst_1
    //   589: istore 6
    //   591: aload_3
    //   592: getfield 49	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a:d	I
    //   595: istore 8
    //   597: aload_2
    //   598: iload 5
    //   600: iload 13
    //   602: iload 6
    //   604: iload 8
    //   606: invokeinterface 126 5 0
    //   611: istore 5
    //   613: aload_0
    //   614: iload 5
    //   616: putfield 49	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a:d	I
    //   619: aload_0
    //   620: areturn
    //   621: new 38	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a$a
    //   624: astore_1
    //   625: aload_1
    //   626: iconst_0
    //   627: invokespecial 129	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a$a:<init>	(B)V
    //   630: aload_1
    //   631: areturn
    //   632: aconst_null
    //   633: areturn
    //   634: getstatic 22	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a:e	Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a;
    //   637: areturn
    //   638: new 2	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a
    //   641: astore_1
    //   642: aload_1
    //   643: invokespecial 20	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a:<init>	()V
    //   646: aload_1
    //   647: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	648	0	this	a
    //   0	648	1	paramj	com.google.f.q.j
    //   0	648	2	paramObject1	Object
    //   0	648	3	paramObject2	Object
    //   3	20	4	arrayOfInt	int[]
    //   9	196	5	i	int
    //   209	3	5	bool1	boolean
    //   226	19	5	j	int
    //   364	88	5	bool2	boolean
    //   474	55	5	k	int
    //   541	58	5	m	int
    //   611	4	5	n	int
    //   19	584	6	bool3	boolean
    //   25	417	7	i1	int
    //   161	444	8	i2	int
    //   370	84	9	str1	String
    //   382	74	10	bool4	boolean
    //   502	31	10	i3	int
    //   581	3	10	i4	int
    //   388	70	11	str2	String
    //   432	88	12	str3	String
    //   496	105	13	i5	int
    //   525	10	14	i6	int
    // Exception table:
    //   from	to	target	type
    //   99	102	129	finally
    //   107	110	129	finally
    //   111	114	129	finally
    //   116	120	129	finally
    //   120	124	129	finally
    //   124	126	129	finally
    //   130	132	129	finally
    //   148	152	284	finally
    //   204	209	284	finally
    //   222	226	284	finally
    //   229	234	284	finally
    //   237	241	284	finally
    //   244	249	284	finally
    //   252	256	284	finally
    //   258	262	284	finally
    //   265	269	284	finally
    //   271	275	284	finally
    //   289	292	284	finally
    //   293	296	284	finally
    //   297	301	284	finally
    //   303	307	284	finally
    //   308	312	284	finally
    //   314	318	284	finally
    //   318	320	284	finally
    //   321	324	284	finally
    //   326	330	284	finally
    //   332	336	284	finally
    //   336	338	284	finally
    //   148	152	288	java/io/IOException
    //   204	209	288	java/io/IOException
    //   222	226	288	java/io/IOException
    //   229	234	288	java/io/IOException
    //   237	241	288	java/io/IOException
    //   244	249	288	java/io/IOException
    //   252	256	288	java/io/IOException
    //   258	262	288	java/io/IOException
    //   265	269	288	java/io/IOException
    //   271	275	288	java/io/IOException
    //   148	152	320	com/google/f/x
    //   204	209	320	com/google/f/x
    //   222	226	320	com/google/f/x
    //   229	234	320	com/google/f/x
    //   237	241	320	com/google/f/x
    //   244	249	320	com/google/f/x
    //   252	256	320	com/google/f/x
    //   258	262	320	com/google/f/x
    //   265	269	320	com/google/f/x
    //   271	275	320	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int m = -1;
    if (i != m) {
      return i;
    }
    String str1 = a;
    boolean bool1 = str1.isEmpty();
    m = 0;
    String str2;
    if (!bool1)
    {
      str2 = a;
      int j = h.computeStringSize(1, str2);
      m = 0 + j;
    }
    str1 = b;
    boolean bool2 = str1.isEmpty();
    if (!bool2)
    {
      str2 = b;
      k = h.computeStringSize(2, str2);
      m += k;
    }
    int k = c;
    int n;
    if (k != 0)
    {
      n = 3;
      k = h.computeInt32Size(n, k);
      m += k;
    }
    k = d;
    if (k != 0)
    {
      n = 4;
      k = h.computeInt32Size(n, k);
      m += k;
    }
    memoizedSerializedSize = m;
    return m;
  }
  
  public final void writeTo(h paramh)
  {
    String str1 = a;
    int i = str1.isEmpty();
    String str2;
    if (i == 0)
    {
      i = 1;
      str2 = a;
      paramh.writeString(i, str2);
    }
    str1 = b;
    boolean bool = str1.isEmpty();
    if (!bool)
    {
      j = 2;
      str2 = b;
      paramh.writeString(j, str2);
    }
    int j = c;
    int k;
    if (j != 0)
    {
      k = 3;
      paramh.writeInt32(k, j);
    }
    j = d;
    if (j != 0)
    {
      k = 4;
      paramh.writeInt32(k, j);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.models.input.InputMessageContent.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */