package com.truecaller.api.services.messenger.v1.models;

import com.google.f.w.c;

public enum Peer$TypeCase
  implements w.c
{
  private final int value;
  
  static
  {
    Object localObject = new com/truecaller/api/services/messenger/v1/models/Peer$TypeCase;
    int i = 1;
    ((TypeCase)localObject).<init>("USER", 0, i);
    USER = (TypeCase)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/models/Peer$TypeCase;
    int j = 2;
    ((TypeCase)localObject).<init>("GROUP", i, j);
    GROUP = (TypeCase)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/models/Peer$TypeCase;
    ((TypeCase)localObject).<init>("TYPE_NOT_SET", j, 0);
    TYPE_NOT_SET = (TypeCase)localObject;
    localObject = new TypeCase[3];
    TypeCase localTypeCase = USER;
    localObject[0] = localTypeCase;
    localTypeCase = GROUP;
    localObject[i] = localTypeCase;
    localTypeCase = TYPE_NOT_SET;
    localObject[j] = localTypeCase;
    $VALUES = (TypeCase[])localObject;
  }
  
  private Peer$TypeCase(int paramInt1)
  {
    value = paramInt1;
  }
  
  public static TypeCase forNumber(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return null;
    case 2: 
      return GROUP;
    case 1: 
      return USER;
    }
    return TYPE_NOT_SET;
  }
  
  public static TypeCase valueOf(int paramInt)
  {
    return forNumber(paramInt);
  }
  
  public final int getNumber()
  {
    return value;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.models.Peer.TypeCase
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */