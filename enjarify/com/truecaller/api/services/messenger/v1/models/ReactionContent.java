package com.truecaller.api.services.messenger.v1.models;

import com.google.f.ae;
import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;

public final class ReactionContent
  extends q
  implements i
{
  private static final ReactionContent d;
  private static volatile ah e;
  private int a = 0;
  private Object b;
  private String c = "";
  
  static
  {
    ReactionContent localReactionContent = new com/truecaller/api/services/messenger/v1/models/ReactionContent;
    localReactionContent.<init>();
    d = localReactionContent;
    localReactionContent.makeImmutable();
  }
  
  public static ReactionContent c()
  {
    return d;
  }
  
  public static ah d()
  {
    return d.getParserForType();
  }
  
  public final ReactionContent.b a()
  {
    int i = a;
    int j = 1;
    if (i == j) {
      return (ReactionContent.b)b;
    }
    return ReactionContent.b.b();
  }
  
  public final String b()
  {
    return c;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 49	com/truecaller/api/services/messenger/v1/models/ReactionContent$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 55	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_0
    //   19: istore 6
    //   21: aconst_null
    //   22: astore 4
    //   24: iconst_0
    //   25: istore 7
    //   27: iconst_1
    //   28: istore 8
    //   30: iload 5
    //   32: tableswitch	default:+48->80, 1:+597->629, 2:+593->625, 3:+591->623, 4:+580->612, 5:+361->393, 6:+110->142, 7:+357->389, 8:+58->90
    //   80: new 57	java/lang/UnsupportedOperationException
    //   83: astore_1
    //   84: aload_1
    //   85: invokespecial 58	java/lang/UnsupportedOperationException:<init>	()V
    //   88: aload_1
    //   89: athrow
    //   90: getstatic 60	com/truecaller/api/services/messenger/v1/models/ReactionContent:e	Lcom/google/f/ah;
    //   93: astore_1
    //   94: aload_1
    //   95: ifnonnull +43 -> 138
    //   98: ldc 2
    //   100: astore_1
    //   101: aload_1
    //   102: monitorenter
    //   103: getstatic 60	com/truecaller/api/services/messenger/v1/models/ReactionContent:e	Lcom/google/f/ah;
    //   106: astore_2
    //   107: aload_2
    //   108: ifnonnull +20 -> 128
    //   111: new 62	com/google/f/q$b
    //   114: astore_2
    //   115: getstatic 22	com/truecaller/api/services/messenger/v1/models/ReactionContent:d	Lcom/truecaller/api/services/messenger/v1/models/ReactionContent;
    //   118: astore_3
    //   119: aload_2
    //   120: aload_3
    //   121: invokespecial 65	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   124: aload_2
    //   125: putstatic 60	com/truecaller/api/services/messenger/v1/models/ReactionContent:e	Lcom/google/f/ah;
    //   128: aload_1
    //   129: monitorexit
    //   130: goto +8 -> 138
    //   133: astore_2
    //   134: aload_1
    //   135: monitorexit
    //   136: aload_2
    //   137: athrow
    //   138: getstatic 60	com/truecaller/api/services/messenger/v1/models/ReactionContent:e	Lcom/google/f/ah;
    //   141: areturn
    //   142: aload_2
    //   143: checkcast 67	com/google/f/g
    //   146: astore_2
    //   147: aload_3
    //   148: checkcast 69	com/google/f/n
    //   151: astore_3
    //   152: iload 7
    //   154: ifne +235 -> 389
    //   157: aload_2
    //   158: invokevirtual 72	com/google/f/g:readTag	()I
    //   161: istore 5
    //   163: iload 5
    //   165: ifeq +162 -> 327
    //   168: bipush 10
    //   170: istore 9
    //   172: iload 5
    //   174: iload 9
    //   176: if_icmpeq +47 -> 223
    //   179: sipush 7994
    //   182: istore 9
    //   184: iload 5
    //   186: iload 9
    //   188: if_icmpeq +22 -> 210
    //   191: aload_2
    //   192: iload 5
    //   194: invokevirtual 78	com/google/f/g:skipField	(I)Z
    //   197: istore 5
    //   199: iload 5
    //   201: ifne -49 -> 152
    //   204: iconst_1
    //   205: istore 7
    //   207: goto -55 -> 152
    //   210: aload_2
    //   211: invokevirtual 82	com/google/f/g:readStringRequireUtf8	()Ljava/lang/String;
    //   214: astore_1
    //   215: aload_0
    //   216: aload_1
    //   217: putfield 32	com/truecaller/api/services/messenger/v1/models/ReactionContent:c	Ljava/lang/String;
    //   220: goto -68 -> 152
    //   223: aload_0
    //   224: getfield 28	com/truecaller/api/services/messenger/v1/models/ReactionContent:a	I
    //   227: istore 5
    //   229: iload 5
    //   231: iload 8
    //   233: if_icmpne +26 -> 259
    //   236: aload_0
    //   237: getfield 39	com/truecaller/api/services/messenger/v1/models/ReactionContent:b	Ljava/lang/Object;
    //   240: astore_1
    //   241: aload_1
    //   242: checkcast 41	com/truecaller/api/services/messenger/v1/models/ReactionContent$b
    //   245: astore_1
    //   246: aload_1
    //   247: invokevirtual 86	com/truecaller/api/services/messenger/v1/models/ReactionContent$b:toBuilder	()Lcom/google/f/q$a;
    //   250: astore_1
    //   251: aload_1
    //   252: checkcast 88	com/truecaller/api/services/messenger/v1/models/ReactionContent$b$a
    //   255: astore_1
    //   256: goto +8 -> 264
    //   259: iconst_0
    //   260: istore 5
    //   262: aconst_null
    //   263: astore_1
    //   264: invokestatic 90	com/truecaller/api/services/messenger/v1/models/ReactionContent$b:c	()Lcom/google/f/ah;
    //   267: astore 10
    //   269: aload_2
    //   270: aload 10
    //   272: aload_3
    //   273: invokevirtual 94	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   276: astore 10
    //   278: aload_0
    //   279: aload 10
    //   281: putfield 39	com/truecaller/api/services/messenger/v1/models/ReactionContent:b	Ljava/lang/Object;
    //   284: aload_1
    //   285: ifnull +33 -> 318
    //   288: aload_0
    //   289: getfield 39	com/truecaller/api/services/messenger/v1/models/ReactionContent:b	Ljava/lang/Object;
    //   292: astore 10
    //   294: aload 10
    //   296: checkcast 41	com/truecaller/api/services/messenger/v1/models/ReactionContent$b
    //   299: astore 10
    //   301: aload_1
    //   302: aload 10
    //   304: invokevirtual 98	com/truecaller/api/services/messenger/v1/models/ReactionContent$b$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   307: pop
    //   308: aload_1
    //   309: invokevirtual 102	com/truecaller/api/services/messenger/v1/models/ReactionContent$b$a:buildPartial	()Lcom/google/f/q;
    //   312: astore_1
    //   313: aload_0
    //   314: aload_1
    //   315: putfield 39	com/truecaller/api/services/messenger/v1/models/ReactionContent:b	Ljava/lang/Object;
    //   318: aload_0
    //   319: iload 8
    //   321: putfield 28	com/truecaller/api/services/messenger/v1/models/ReactionContent:a	I
    //   324: goto -172 -> 152
    //   327: iconst_1
    //   328: istore 7
    //   330: goto -178 -> 152
    //   333: astore_1
    //   334: goto +53 -> 387
    //   337: astore_1
    //   338: new 104	java/lang/RuntimeException
    //   341: astore_2
    //   342: new 106	com/google/f/x
    //   345: astore_3
    //   346: aload_1
    //   347: invokevirtual 111	java/io/IOException:getMessage	()Ljava/lang/String;
    //   350: astore_1
    //   351: aload_3
    //   352: aload_1
    //   353: invokespecial 114	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   356: aload_3
    //   357: aload_0
    //   358: invokevirtual 118	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   361: astore_1
    //   362: aload_2
    //   363: aload_1
    //   364: invokespecial 121	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   367: aload_2
    //   368: athrow
    //   369: astore_1
    //   370: new 104	java/lang/RuntimeException
    //   373: astore_2
    //   374: aload_1
    //   375: aload_0
    //   376: invokevirtual 118	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   379: astore_1
    //   380: aload_2
    //   381: aload_1
    //   382: invokespecial 121	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   385: aload_2
    //   386: athrow
    //   387: aload_1
    //   388: athrow
    //   389: getstatic 22	com/truecaller/api/services/messenger/v1/models/ReactionContent:d	Lcom/truecaller/api/services/messenger/v1/models/ReactionContent;
    //   392: areturn
    //   393: aload_2
    //   394: checkcast 123	com/google/f/q$k
    //   397: astore_2
    //   398: aload_3
    //   399: checkcast 2	com/truecaller/api/services/messenger/v1/models/ReactionContent
    //   402: astore_3
    //   403: aload_0
    //   404: getfield 32	com/truecaller/api/services/messenger/v1/models/ReactionContent:c	Ljava/lang/String;
    //   407: invokevirtual 129	java/lang/String:isEmpty	()Z
    //   410: iload 8
    //   412: ixor
    //   413: istore 5
    //   415: aload_0
    //   416: getfield 32	com/truecaller/api/services/messenger/v1/models/ReactionContent:c	Ljava/lang/String;
    //   419: astore 4
    //   421: aload_3
    //   422: getfield 32	com/truecaller/api/services/messenger/v1/models/ReactionContent:c	Ljava/lang/String;
    //   425: astore 10
    //   427: aload 10
    //   429: invokevirtual 129	java/lang/String:isEmpty	()Z
    //   432: iload 8
    //   434: ixor
    //   435: istore 9
    //   437: aload_3
    //   438: getfield 32	com/truecaller/api/services/messenger/v1/models/ReactionContent:c	Ljava/lang/String;
    //   441: astore 11
    //   443: aload_2
    //   444: iload 5
    //   446: aload 4
    //   448: iload 9
    //   450: aload 11
    //   452: invokeinterface 133 5 0
    //   457: astore_1
    //   458: aload_0
    //   459: aload_1
    //   460: putfield 32	com/truecaller/api/services/messenger/v1/models/ReactionContent:c	Ljava/lang/String;
    //   463: getstatic 135	com/truecaller/api/services/messenger/v1/models/ReactionContent$1:b	[I
    //   466: astore_1
    //   467: aload_3
    //   468: getfield 28	com/truecaller/api/services/messenger/v1/models/ReactionContent:a	I
    //   471: invokestatic 141	com/truecaller/api/services/messenger/v1/models/ReactionContent$ValueCase:forNumber	(I)Lcom/truecaller/api/services/messenger/v1/models/ReactionContent$ValueCase;
    //   474: astore 4
    //   476: aload 4
    //   478: invokevirtual 142	com/truecaller/api/services/messenger/v1/models/ReactionContent$ValueCase:ordinal	()I
    //   481: istore 6
    //   483: aload_1
    //   484: iload 6
    //   486: iaload
    //   487: istore 5
    //   489: iload 5
    //   491: tableswitch	default:+21->512, 1:+49->540, 2:+24->515
    //   512: goto +72 -> 584
    //   515: aload_0
    //   516: getfield 28	com/truecaller/api/services/messenger/v1/models/ReactionContent:a	I
    //   519: istore 5
    //   521: iload 5
    //   523: ifeq +6 -> 529
    //   526: iconst_1
    //   527: istore 7
    //   529: aload_2
    //   530: iload 7
    //   532: invokeinterface 146 2 0
    //   537: goto +47 -> 584
    //   540: aload_0
    //   541: getfield 28	com/truecaller/api/services/messenger/v1/models/ReactionContent:a	I
    //   544: istore 5
    //   546: iload 5
    //   548: iload 8
    //   550: if_icmpne +6 -> 556
    //   553: iconst_1
    //   554: istore 7
    //   556: aload_0
    //   557: getfield 39	com/truecaller/api/services/messenger/v1/models/ReactionContent:b	Ljava/lang/Object;
    //   560: astore_1
    //   561: aload_3
    //   562: getfield 39	com/truecaller/api/services/messenger/v1/models/ReactionContent:b	Ljava/lang/Object;
    //   565: astore 4
    //   567: aload_2
    //   568: iload 7
    //   570: aload_1
    //   571: aload 4
    //   573: invokeinterface 150 4 0
    //   578: astore_1
    //   579: aload_0
    //   580: aload_1
    //   581: putfield 39	com/truecaller/api/services/messenger/v1/models/ReactionContent:b	Ljava/lang/Object;
    //   584: getstatic 156	com/google/f/q$i:INSTANCE	Lcom/google/f/q$i;
    //   587: astore_1
    //   588: aload_2
    //   589: aload_1
    //   590: if_acmpne +20 -> 610
    //   593: aload_3
    //   594: getfield 28	com/truecaller/api/services/messenger/v1/models/ReactionContent:a	I
    //   597: istore 5
    //   599: iload 5
    //   601: ifeq +9 -> 610
    //   604: aload_0
    //   605: iload 5
    //   607: putfield 28	com/truecaller/api/services/messenger/v1/models/ReactionContent:a	I
    //   610: aload_0
    //   611: areturn
    //   612: new 158	com/truecaller/api/services/messenger/v1/models/ReactionContent$a
    //   615: astore_1
    //   616: aload_1
    //   617: iconst_0
    //   618: invokespecial 161	com/truecaller/api/services/messenger/v1/models/ReactionContent$a:<init>	(B)V
    //   621: aload_1
    //   622: areturn
    //   623: aconst_null
    //   624: areturn
    //   625: getstatic 22	com/truecaller/api/services/messenger/v1/models/ReactionContent:d	Lcom/truecaller/api/services/messenger/v1/models/ReactionContent;
    //   628: areturn
    //   629: new 2	com/truecaller/api/services/messenger/v1/models/ReactionContent
    //   632: astore_1
    //   633: aload_1
    //   634: invokespecial 20	com/truecaller/api/services/messenger/v1/models/ReactionContent:<init>	()V
    //   637: aload_1
    //   638: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	639	0	this	ReactionContent
    //   0	639	1	paramj	com.google.f.q.j
    //   0	639	2	paramObject1	Object
    //   0	639	3	paramObject2	Object
    //   3	569	4	localObject1	Object
    //   9	184	5	i	int
    //   197	3	5	bool1	boolean
    //   227	34	5	j	int
    //   413	32	5	bool2	boolean
    //   487	119	5	k	int
    //   19	466	6	m	int
    //   25	544	7	bool3	boolean
    //   28	523	8	n	int
    //   170	19	9	i1	int
    //   435	14	9	bool4	boolean
    //   267	161	10	localObject2	Object
    //   441	10	11	str	String
    // Exception table:
    //   from	to	target	type
    //   103	106	133	finally
    //   111	114	133	finally
    //   115	118	133	finally
    //   120	124	133	finally
    //   124	128	133	finally
    //   128	130	133	finally
    //   134	136	133	finally
    //   157	161	333	finally
    //   192	197	333	finally
    //   210	214	333	finally
    //   216	220	333	finally
    //   223	227	333	finally
    //   236	240	333	finally
    //   241	245	333	finally
    //   246	250	333	finally
    //   251	255	333	finally
    //   264	267	333	finally
    //   272	276	333	finally
    //   279	284	333	finally
    //   288	292	333	finally
    //   294	299	333	finally
    //   302	308	333	finally
    //   308	312	333	finally
    //   314	318	333	finally
    //   319	324	333	finally
    //   338	341	333	finally
    //   342	345	333	finally
    //   346	350	333	finally
    //   352	356	333	finally
    //   357	361	333	finally
    //   363	367	333	finally
    //   367	369	333	finally
    //   370	373	333	finally
    //   375	379	333	finally
    //   381	385	333	finally
    //   385	387	333	finally
    //   157	161	337	java/io/IOException
    //   192	197	337	java/io/IOException
    //   210	214	337	java/io/IOException
    //   216	220	337	java/io/IOException
    //   223	227	337	java/io/IOException
    //   236	240	337	java/io/IOException
    //   241	245	337	java/io/IOException
    //   246	250	337	java/io/IOException
    //   251	255	337	java/io/IOException
    //   264	267	337	java/io/IOException
    //   272	276	337	java/io/IOException
    //   279	284	337	java/io/IOException
    //   288	292	337	java/io/IOException
    //   294	299	337	java/io/IOException
    //   302	308	337	java/io/IOException
    //   308	312	337	java/io/IOException
    //   314	318	337	java/io/IOException
    //   319	324	337	java/io/IOException
    //   157	161	369	com/google/f/x
    //   192	197	369	com/google/f/x
    //   210	214	369	com/google/f/x
    //   216	220	369	com/google/f/x
    //   223	227	369	com/google/f/x
    //   236	240	369	com/google/f/x
    //   241	245	369	com/google/f/x
    //   246	250	369	com/google/f/x
    //   251	255	369	com/google/f/x
    //   264	267	369	com/google/f/x
    //   272	276	369	com/google/f/x
    //   279	284	369	com/google/f/x
    //   288	292	369	com/google/f/x
    //   294	299	369	com/google/f/x
    //   302	308	369	com/google/f/x
    //   308	312	369	com/google/f/x
    //   314	318	369	com/google/f/x
    //   319	324	369	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int k = -1;
    if (i != k) {
      return i;
    }
    i = a;
    k = 1;
    int m = 0;
    if (i == k)
    {
      localObject = (ReactionContent.b)b;
      i = h.computeMessageSize(k, (ae)localObject);
      m = 0 + i;
    }
    Object localObject = c;
    boolean bool = ((String)localObject).isEmpty();
    if (!bool)
    {
      String str = c;
      int j = h.computeStringSize(999, str);
      m += j;
    }
    memoizedSerializedSize = m;
    return m;
  }
  
  public final void writeTo(h paramh)
  {
    int i = a;
    int k = 1;
    if (i == k)
    {
      localObject = (ReactionContent.b)b;
      paramh.writeMessage(k, (ae)localObject);
    }
    Object localObject = c;
    boolean bool = ((String)localObject).isEmpty();
    if (!bool)
    {
      int j = 999;
      String str = c;
      paramh.writeString(j, str);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.models.ReactionContent
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */