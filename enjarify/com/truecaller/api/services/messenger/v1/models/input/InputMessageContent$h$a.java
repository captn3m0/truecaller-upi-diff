package com.truecaller.api.services.messenger.v1.models.input;

import com.google.f.q.a;

public final class InputMessageContent$h$a
  extends q.a
  implements InputMessageContent.i
{
  private InputMessageContent$h$a()
  {
    super(localh);
  }
  
  public final a a(int paramInt)
  {
    copyOnWrite();
    InputMessageContent.h.a((InputMessageContent.h)instance, paramInt);
    return this;
  }
  
  public final a a(String paramString)
  {
    copyOnWrite();
    InputMessageContent.h.a((InputMessageContent.h)instance, paramString);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.models.input.InputMessageContent.h.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */