package com.truecaller.api.services.messenger.v1.models.input;

import com.google.f.w.c;

public enum InputMessageContent$AttachmentCase
  implements w.c
{
  private final int value;
  
  static
  {
    Object localObject = new com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$AttachmentCase;
    int i = 2;
    ((AttachmentCase)localObject).<init>("IMAGE", 0, i);
    IMAGE = (AttachmentCase)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$AttachmentCase;
    int j = 1;
    int k = 3;
    ((AttachmentCase)localObject).<init>("VCARD", j, k);
    VCARD = (AttachmentCase)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$AttachmentCase;
    int m = 4;
    ((AttachmentCase)localObject).<init>("LOCATION", i, m);
    LOCATION = (AttachmentCase)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$AttachmentCase;
    int n = 5;
    ((AttachmentCase)localObject).<init>("VIDEO", k, n);
    VIDEO = (AttachmentCase)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$AttachmentCase;
    int i1 = 6;
    ((AttachmentCase)localObject).<init>("AUDIO", m, i1);
    AUDIO = (AttachmentCase)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$AttachmentCase;
    ((AttachmentCase)localObject).<init>("ATTACHMENT_NOT_SET", n, 0);
    ATTACHMENT_NOT_SET = (AttachmentCase)localObject;
    localObject = new AttachmentCase[i1];
    AttachmentCase localAttachmentCase = IMAGE;
    localObject[0] = localAttachmentCase;
    localAttachmentCase = VCARD;
    localObject[j] = localAttachmentCase;
    localAttachmentCase = LOCATION;
    localObject[i] = localAttachmentCase;
    localAttachmentCase = VIDEO;
    localObject[k] = localAttachmentCase;
    localAttachmentCase = AUDIO;
    localObject[m] = localAttachmentCase;
    localAttachmentCase = ATTACHMENT_NOT_SET;
    localObject[n] = localAttachmentCase;
    $VALUES = (AttachmentCase[])localObject;
  }
  
  private InputMessageContent$AttachmentCase(int paramInt1)
  {
    value = paramInt1;
  }
  
  public static AttachmentCase forNumber(int paramInt)
  {
    if (paramInt != 0)
    {
      switch (paramInt)
      {
      default: 
        return null;
      case 6: 
        return AUDIO;
      case 5: 
        return VIDEO;
      case 4: 
        return LOCATION;
      case 3: 
        return VCARD;
      }
      return IMAGE;
    }
    return ATTACHMENT_NOT_SET;
  }
  
  public static AttachmentCase valueOf(int paramInt)
  {
    return forNumber(paramInt);
  }
  
  public final int getNumber()
  {
    return value;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.models.input.InputMessageContent.AttachmentCase
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */