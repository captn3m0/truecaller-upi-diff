package com.truecaller.api.services.messenger.v1.models.input;

import com.google.f.q.a;

public final class InputPeer$a
  extends q.a
  implements i
{
  private InputPeer$a()
  {
    super(localInputPeer);
  }
  
  public final a a(InputPeer.b.a parama)
  {
    copyOnWrite();
    InputPeer.a((InputPeer)instance, parama);
    return this;
  }
  
  public final a a(InputPeer.d.a parama)
  {
    copyOnWrite();
    InputPeer.a((InputPeer)instance, parama);
    return this;
  }
  
  public final a a(InputPeer.d paramd)
  {
    copyOnWrite();
    InputPeer.a((InputPeer)instance, paramd);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.models.input.InputPeer.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */