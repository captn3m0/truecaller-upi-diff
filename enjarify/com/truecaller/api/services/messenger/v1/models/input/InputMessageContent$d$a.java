package com.truecaller.api.services.messenger.v1.models.input;

import com.google.f.f;
import com.google.f.q.a;

public final class InputMessageContent$d$a
  extends q.a
  implements InputMessageContent.e
{
  private InputMessageContent$d$a()
  {
    super(locald);
  }
  
  public final a a(int paramInt)
  {
    copyOnWrite();
    InputMessageContent.d.a((InputMessageContent.d)instance, paramInt);
    return this;
  }
  
  public final a a(f paramf)
  {
    copyOnWrite();
    InputMessageContent.d.a((InputMessageContent.d)instance, paramf);
    return this;
  }
  
  public final a a(String paramString)
  {
    copyOnWrite();
    InputMessageContent.d.a((InputMessageContent.d)instance, paramString);
    return this;
  }
  
  public final a b(int paramInt)
  {
    copyOnWrite();
    InputMessageContent.d.b((InputMessageContent.d)instance, paramInt);
    return this;
  }
  
  public final a b(String paramString)
  {
    copyOnWrite();
    InputMessageContent.d.b((InputMessageContent.d)instance, paramString);
    return this;
  }
  
  public final a c(int paramInt)
  {
    copyOnWrite();
    InputMessageContent.d.c((InputMessageContent.d)instance, paramInt);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.models.input.InputMessageContent.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */