package com.truecaller.api.services.messenger.v1.models;

import com.google.f.ae;
import com.google.f.ah;
import com.google.f.q;

public final class Peer
  extends q
  implements h
{
  private static final Peer c;
  private static volatile ah d;
  private int a = 0;
  private Object b;
  
  static
  {
    Peer localPeer = new com/truecaller/api/services/messenger/v1/models/Peer;
    localPeer.<init>();
    c = localPeer;
    localPeer.makeImmutable();
  }
  
  public static Peer d()
  {
    return c;
  }
  
  public static ah e()
  {
    return c.getParserForType();
  }
  
  public final Peer.TypeCase a()
  {
    return Peer.TypeCase.forNumber(a);
  }
  
  public final Peer.d b()
  {
    int i = a;
    int j = 1;
    if (i == j) {
      return (Peer.d)b;
    }
    return Peer.d.d();
  }
  
  public final Peer.b c()
  {
    int i = a;
    int j = 2;
    if (i == j) {
      return (Peer.b)b;
    }
    return Peer.b.b();
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 55	com/truecaller/api/services/messenger/v1/models/Peer$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 61	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_2
    //   19: istore 6
    //   21: iconst_0
    //   22: istore 7
    //   24: aconst_null
    //   25: astore 8
    //   27: iconst_0
    //   28: istore 9
    //   30: iconst_1
    //   31: istore 10
    //   33: iload 5
    //   35: tableswitch	default:+45->80, 1:+677->712, 2:+673->708, 3:+671->706, 4:+660->695, 5:+448->483, 6:+107->142, 7:+444->479, 8:+55->90
    //   80: new 63	java/lang/UnsupportedOperationException
    //   83: astore_1
    //   84: aload_1
    //   85: invokespecial 64	java/lang/UnsupportedOperationException:<init>	()V
    //   88: aload_1
    //   89: athrow
    //   90: getstatic 66	com/truecaller/api/services/messenger/v1/models/Peer:d	Lcom/google/f/ah;
    //   93: astore_1
    //   94: aload_1
    //   95: ifnonnull +43 -> 138
    //   98: ldc 2
    //   100: astore_1
    //   101: aload_1
    //   102: monitorenter
    //   103: getstatic 66	com/truecaller/api/services/messenger/v1/models/Peer:d	Lcom/google/f/ah;
    //   106: astore_2
    //   107: aload_2
    //   108: ifnonnull +20 -> 128
    //   111: new 68	com/google/f/q$b
    //   114: astore_2
    //   115: getstatic 20	com/truecaller/api/services/messenger/v1/models/Peer:c	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   118: astore_3
    //   119: aload_2
    //   120: aload_3
    //   121: invokespecial 71	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   124: aload_2
    //   125: putstatic 66	com/truecaller/api/services/messenger/v1/models/Peer:d	Lcom/google/f/ah;
    //   128: aload_1
    //   129: monitorexit
    //   130: goto +8 -> 138
    //   133: astore_2
    //   134: aload_1
    //   135: monitorexit
    //   136: aload_2
    //   137: athrow
    //   138: getstatic 66	com/truecaller/api/services/messenger/v1/models/Peer:d	Lcom/google/f/ah;
    //   141: areturn
    //   142: aload_2
    //   143: checkcast 73	com/google/f/g
    //   146: astore_2
    //   147: aload_3
    //   148: checkcast 75	com/google/f/n
    //   151: astore_3
    //   152: iload 9
    //   154: ifne +325 -> 479
    //   157: aload_2
    //   158: invokevirtual 78	com/google/f/g:readTag	()I
    //   161: istore 5
    //   163: iload 5
    //   165: ifeq +252 -> 417
    //   168: bipush 10
    //   170: istore 11
    //   172: iload 5
    //   174: iload 11
    //   176: if_icmpeq +137 -> 313
    //   179: bipush 18
    //   181: istore 11
    //   183: iload 5
    //   185: iload 11
    //   187: if_icmpeq +22 -> 209
    //   190: aload_2
    //   191: iload 5
    //   193: invokevirtual 84	com/google/f/g:skipField	(I)Z
    //   196: istore 5
    //   198: iload 5
    //   200: ifne -48 -> 152
    //   203: iconst_1
    //   204: istore 9
    //   206: goto -54 -> 152
    //   209: aload_0
    //   210: getfield 26	com/truecaller/api/services/messenger/v1/models/Peer:a	I
    //   213: istore 5
    //   215: iload 5
    //   217: iload 6
    //   219: if_icmpne +26 -> 245
    //   222: aload_0
    //   223: getfield 39	com/truecaller/api/services/messenger/v1/models/Peer:b	Ljava/lang/Object;
    //   226: astore_1
    //   227: aload_1
    //   228: checkcast 47	com/truecaller/api/services/messenger/v1/models/Peer$b
    //   231: astore_1
    //   232: aload_1
    //   233: invokevirtual 88	com/truecaller/api/services/messenger/v1/models/Peer$b:toBuilder	()Lcom/google/f/q$a;
    //   236: astore_1
    //   237: aload_1
    //   238: checkcast 90	com/truecaller/api/services/messenger/v1/models/Peer$b$a
    //   241: astore_1
    //   242: goto +8 -> 250
    //   245: iconst_0
    //   246: istore 5
    //   248: aconst_null
    //   249: astore_1
    //   250: invokestatic 92	com/truecaller/api/services/messenger/v1/models/Peer$b:c	()Lcom/google/f/ah;
    //   253: astore 12
    //   255: aload_2
    //   256: aload 12
    //   258: aload_3
    //   259: invokevirtual 96	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   262: astore 12
    //   264: aload_0
    //   265: aload 12
    //   267: putfield 39	com/truecaller/api/services/messenger/v1/models/Peer:b	Ljava/lang/Object;
    //   270: aload_1
    //   271: ifnull +33 -> 304
    //   274: aload_0
    //   275: getfield 39	com/truecaller/api/services/messenger/v1/models/Peer:b	Ljava/lang/Object;
    //   278: astore 12
    //   280: aload 12
    //   282: checkcast 47	com/truecaller/api/services/messenger/v1/models/Peer$b
    //   285: astore 12
    //   287: aload_1
    //   288: aload 12
    //   290: invokevirtual 100	com/truecaller/api/services/messenger/v1/models/Peer$b$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   293: pop
    //   294: aload_1
    //   295: invokevirtual 104	com/truecaller/api/services/messenger/v1/models/Peer$b$a:buildPartial	()Lcom/google/f/q;
    //   298: astore_1
    //   299: aload_0
    //   300: aload_1
    //   301: putfield 39	com/truecaller/api/services/messenger/v1/models/Peer:b	Ljava/lang/Object;
    //   304: aload_0
    //   305: iload 6
    //   307: putfield 26	com/truecaller/api/services/messenger/v1/models/Peer:a	I
    //   310: goto -158 -> 152
    //   313: aload_0
    //   314: getfield 26	com/truecaller/api/services/messenger/v1/models/Peer:a	I
    //   317: istore 5
    //   319: iload 5
    //   321: iload 10
    //   323: if_icmpne +26 -> 349
    //   326: aload_0
    //   327: getfield 39	com/truecaller/api/services/messenger/v1/models/Peer:b	Ljava/lang/Object;
    //   330: astore_1
    //   331: aload_1
    //   332: checkcast 41	com/truecaller/api/services/messenger/v1/models/Peer$d
    //   335: astore_1
    //   336: aload_1
    //   337: invokevirtual 105	com/truecaller/api/services/messenger/v1/models/Peer$d:toBuilder	()Lcom/google/f/q$a;
    //   340: astore_1
    //   341: aload_1
    //   342: checkcast 107	com/truecaller/api/services/messenger/v1/models/Peer$d$a
    //   345: astore_1
    //   346: goto +8 -> 354
    //   349: iconst_0
    //   350: istore 5
    //   352: aconst_null
    //   353: astore_1
    //   354: invokestatic 110	com/truecaller/api/services/messenger/v1/models/Peer$d:e	()Lcom/google/f/ah;
    //   357: astore 12
    //   359: aload_2
    //   360: aload 12
    //   362: aload_3
    //   363: invokevirtual 96	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   366: astore 12
    //   368: aload_0
    //   369: aload 12
    //   371: putfield 39	com/truecaller/api/services/messenger/v1/models/Peer:b	Ljava/lang/Object;
    //   374: aload_1
    //   375: ifnull +33 -> 408
    //   378: aload_0
    //   379: getfield 39	com/truecaller/api/services/messenger/v1/models/Peer:b	Ljava/lang/Object;
    //   382: astore 12
    //   384: aload 12
    //   386: checkcast 41	com/truecaller/api/services/messenger/v1/models/Peer$d
    //   389: astore 12
    //   391: aload_1
    //   392: aload 12
    //   394: invokevirtual 111	com/truecaller/api/services/messenger/v1/models/Peer$d$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   397: pop
    //   398: aload_1
    //   399: invokevirtual 112	com/truecaller/api/services/messenger/v1/models/Peer$d$a:buildPartial	()Lcom/google/f/q;
    //   402: astore_1
    //   403: aload_0
    //   404: aload_1
    //   405: putfield 39	com/truecaller/api/services/messenger/v1/models/Peer:b	Ljava/lang/Object;
    //   408: aload_0
    //   409: iload 10
    //   411: putfield 26	com/truecaller/api/services/messenger/v1/models/Peer:a	I
    //   414: goto -262 -> 152
    //   417: iconst_1
    //   418: istore 9
    //   420: goto -268 -> 152
    //   423: astore_1
    //   424: goto +53 -> 477
    //   427: astore_1
    //   428: new 114	java/lang/RuntimeException
    //   431: astore_2
    //   432: new 116	com/google/f/x
    //   435: astore_3
    //   436: aload_1
    //   437: invokevirtual 122	java/io/IOException:getMessage	()Ljava/lang/String;
    //   440: astore_1
    //   441: aload_3
    //   442: aload_1
    //   443: invokespecial 125	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   446: aload_3
    //   447: aload_0
    //   448: invokevirtual 129	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   451: astore_1
    //   452: aload_2
    //   453: aload_1
    //   454: invokespecial 132	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   457: aload_2
    //   458: athrow
    //   459: astore_1
    //   460: new 114	java/lang/RuntimeException
    //   463: astore_2
    //   464: aload_1
    //   465: aload_0
    //   466: invokevirtual 129	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   469: astore_1
    //   470: aload_2
    //   471: aload_1
    //   472: invokespecial 132	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   475: aload_2
    //   476: athrow
    //   477: aload_1
    //   478: athrow
    //   479: getstatic 20	com/truecaller/api/services/messenger/v1/models/Peer:c	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   482: areturn
    //   483: aload_2
    //   484: checkcast 134	com/google/f/q$k
    //   487: astore_2
    //   488: aload_3
    //   489: checkcast 2	com/truecaller/api/services/messenger/v1/models/Peer
    //   492: astore_3
    //   493: getstatic 136	com/truecaller/api/services/messenger/v1/models/Peer$1:b	[I
    //   496: astore_1
    //   497: aload_3
    //   498: getfield 26	com/truecaller/api/services/messenger/v1/models/Peer:a	I
    //   501: invokestatic 36	com/truecaller/api/services/messenger/v1/models/Peer$TypeCase:forNumber	(I)Lcom/truecaller/api/services/messenger/v1/models/Peer$TypeCase;
    //   504: astore 8
    //   506: aload 8
    //   508: invokevirtual 137	com/truecaller/api/services/messenger/v1/models/Peer$TypeCase:ordinal	()I
    //   511: istore 7
    //   513: aload_1
    //   514: iload 7
    //   516: iaload
    //   517: istore 5
    //   519: iload 5
    //   521: tableswitch	default:+27->548, 1:+102->623, 2:+55->576, 3:+30->551
    //   548: goto +119 -> 667
    //   551: aload_0
    //   552: getfield 26	com/truecaller/api/services/messenger/v1/models/Peer:a	I
    //   555: istore 5
    //   557: iload 5
    //   559: ifeq +6 -> 565
    //   562: iconst_1
    //   563: istore 9
    //   565: aload_2
    //   566: iload 9
    //   568: invokeinterface 141 2 0
    //   573: goto +94 -> 667
    //   576: aload_0
    //   577: getfield 26	com/truecaller/api/services/messenger/v1/models/Peer:a	I
    //   580: istore 5
    //   582: iload 5
    //   584: iload 6
    //   586: if_icmpne +6 -> 592
    //   589: iconst_1
    //   590: istore 9
    //   592: aload_0
    //   593: getfield 39	com/truecaller/api/services/messenger/v1/models/Peer:b	Ljava/lang/Object;
    //   596: astore_1
    //   597: aload_3
    //   598: getfield 39	com/truecaller/api/services/messenger/v1/models/Peer:b	Ljava/lang/Object;
    //   601: astore 4
    //   603: aload_2
    //   604: iload 9
    //   606: aload_1
    //   607: aload 4
    //   609: invokeinterface 145 4 0
    //   614: astore_1
    //   615: aload_0
    //   616: aload_1
    //   617: putfield 39	com/truecaller/api/services/messenger/v1/models/Peer:b	Ljava/lang/Object;
    //   620: goto +47 -> 667
    //   623: aload_0
    //   624: getfield 26	com/truecaller/api/services/messenger/v1/models/Peer:a	I
    //   627: istore 5
    //   629: iload 5
    //   631: iload 10
    //   633: if_icmpne +6 -> 639
    //   636: iconst_1
    //   637: istore 9
    //   639: aload_0
    //   640: getfield 39	com/truecaller/api/services/messenger/v1/models/Peer:b	Ljava/lang/Object;
    //   643: astore_1
    //   644: aload_3
    //   645: getfield 39	com/truecaller/api/services/messenger/v1/models/Peer:b	Ljava/lang/Object;
    //   648: astore 4
    //   650: aload_2
    //   651: iload 9
    //   653: aload_1
    //   654: aload 4
    //   656: invokeinterface 145 4 0
    //   661: astore_1
    //   662: aload_0
    //   663: aload_1
    //   664: putfield 39	com/truecaller/api/services/messenger/v1/models/Peer:b	Ljava/lang/Object;
    //   667: getstatic 151	com/google/f/q$i:INSTANCE	Lcom/google/f/q$i;
    //   670: astore_1
    //   671: aload_2
    //   672: aload_1
    //   673: if_acmpne +20 -> 693
    //   676: aload_3
    //   677: getfield 26	com/truecaller/api/services/messenger/v1/models/Peer:a	I
    //   680: istore 5
    //   682: iload 5
    //   684: ifeq +9 -> 693
    //   687: aload_0
    //   688: iload 5
    //   690: putfield 26	com/truecaller/api/services/messenger/v1/models/Peer:a	I
    //   693: aload_0
    //   694: areturn
    //   695: new 153	com/truecaller/api/services/messenger/v1/models/Peer$a
    //   698: astore_1
    //   699: aload_1
    //   700: iconst_0
    //   701: invokespecial 156	com/truecaller/api/services/messenger/v1/models/Peer$a:<init>	(B)V
    //   704: aload_1
    //   705: areturn
    //   706: aconst_null
    //   707: areturn
    //   708: getstatic 20	com/truecaller/api/services/messenger/v1/models/Peer:c	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   711: areturn
    //   712: new 2	com/truecaller/api/services/messenger/v1/models/Peer
    //   715: astore_1
    //   716: aload_1
    //   717: invokespecial 18	com/truecaller/api/services/messenger/v1/models/Peer:<init>	()V
    //   720: aload_1
    //   721: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	722	0	this	Peer
    //   0	722	1	paramj	com.google.f.q.j
    //   0	722	2	paramObject1	Object
    //   0	722	3	paramObject2	Object
    //   3	652	4	localObject1	Object
    //   9	183	5	i	int
    //   196	3	5	bool1	boolean
    //   213	476	5	j	int
    //   19	568	6	k	int
    //   22	493	7	m	int
    //   25	482	8	localTypeCase	Peer.TypeCase
    //   28	624	9	bool2	boolean
    //   31	603	10	n	int
    //   170	18	11	i1	int
    //   253	140	12	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   103	106	133	finally
    //   111	114	133	finally
    //   115	118	133	finally
    //   120	124	133	finally
    //   124	128	133	finally
    //   128	130	133	finally
    //   134	136	133	finally
    //   157	161	423	finally
    //   191	196	423	finally
    //   209	213	423	finally
    //   222	226	423	finally
    //   227	231	423	finally
    //   232	236	423	finally
    //   237	241	423	finally
    //   250	253	423	finally
    //   258	262	423	finally
    //   265	270	423	finally
    //   274	278	423	finally
    //   280	285	423	finally
    //   288	294	423	finally
    //   294	298	423	finally
    //   300	304	423	finally
    //   305	310	423	finally
    //   313	317	423	finally
    //   326	330	423	finally
    //   331	335	423	finally
    //   336	340	423	finally
    //   341	345	423	finally
    //   354	357	423	finally
    //   362	366	423	finally
    //   369	374	423	finally
    //   378	382	423	finally
    //   384	389	423	finally
    //   392	398	423	finally
    //   398	402	423	finally
    //   404	408	423	finally
    //   409	414	423	finally
    //   428	431	423	finally
    //   432	435	423	finally
    //   436	440	423	finally
    //   442	446	423	finally
    //   447	451	423	finally
    //   453	457	423	finally
    //   457	459	423	finally
    //   460	463	423	finally
    //   465	469	423	finally
    //   471	475	423	finally
    //   475	477	423	finally
    //   157	161	427	java/io/IOException
    //   191	196	427	java/io/IOException
    //   209	213	427	java/io/IOException
    //   222	226	427	java/io/IOException
    //   227	231	427	java/io/IOException
    //   232	236	427	java/io/IOException
    //   237	241	427	java/io/IOException
    //   250	253	427	java/io/IOException
    //   258	262	427	java/io/IOException
    //   265	270	427	java/io/IOException
    //   274	278	427	java/io/IOException
    //   280	285	427	java/io/IOException
    //   288	294	427	java/io/IOException
    //   294	298	427	java/io/IOException
    //   300	304	427	java/io/IOException
    //   305	310	427	java/io/IOException
    //   313	317	427	java/io/IOException
    //   326	330	427	java/io/IOException
    //   331	335	427	java/io/IOException
    //   336	340	427	java/io/IOException
    //   341	345	427	java/io/IOException
    //   354	357	427	java/io/IOException
    //   362	366	427	java/io/IOException
    //   369	374	427	java/io/IOException
    //   378	382	427	java/io/IOException
    //   384	389	427	java/io/IOException
    //   392	398	427	java/io/IOException
    //   398	402	427	java/io/IOException
    //   404	408	427	java/io/IOException
    //   409	414	427	java/io/IOException
    //   157	161	459	com/google/f/x
    //   191	196	459	com/google/f/x
    //   209	213	459	com/google/f/x
    //   222	226	459	com/google/f/x
    //   227	231	459	com/google/f/x
    //   232	236	459	com/google/f/x
    //   237	241	459	com/google/f/x
    //   250	253	459	com/google/f/x
    //   258	262	459	com/google/f/x
    //   265	270	459	com/google/f/x
    //   274	278	459	com/google/f/x
    //   280	285	459	com/google/f/x
    //   288	294	459	com/google/f/x
    //   294	298	459	com/google/f/x
    //   300	304	459	com/google/f/x
    //   305	310	459	com/google/f/x
    //   313	317	459	com/google/f/x
    //   326	330	459	com/google/f/x
    //   331	335	459	com/google/f/x
    //   336	340	459	com/google/f/x
    //   341	345	459	com/google/f/x
    //   354	357	459	com/google/f/x
    //   362	366	459	com/google/f/x
    //   369	374	459	com/google/f/x
    //   378	382	459	com/google/f/x
    //   384	389	459	com/google/f/x
    //   392	398	459	com/google/f/x
    //   398	402	459	com/google/f/x
    //   404	408	459	com/google/f/x
    //   409	414	459	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int j = -1;
    if (i != j) {
      return i;
    }
    i = a;
    j = 1;
    int k = 0;
    Object localObject;
    if (i == j)
    {
      localObject = (Peer.d)b;
      i = com.google.f.h.computeMessageSize(j, (ae)localObject);
      k = 0 + i;
    }
    i = a;
    j = 2;
    if (i == j)
    {
      localObject = (Peer.b)b;
      i = com.google.f.h.computeMessageSize(j, (ae)localObject);
      k += i;
    }
    memoizedSerializedSize = k;
    return k;
  }
  
  public final void writeTo(com.google.f.h paramh)
  {
    int i = a;
    int j = 1;
    Object localObject;
    if (i == j)
    {
      localObject = (Peer.d)b;
      paramh.writeMessage(j, (ae)localObject);
    }
    i = a;
    j = 2;
    if (i == j)
    {
      localObject = (Peer.b)b;
      paramh.writeMessage(j, (ae)localObject);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.models.Peer
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */