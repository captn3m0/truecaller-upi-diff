package com.truecaller.api.services.messenger.v1.models.input;

import com.google.f.w.c;

public enum InputSpan$Log$ValueCase
  implements w.c
{
  private final int value;
  
  static
  {
    Object localObject = new com/truecaller/api/services/messenger/v1/models/input/InputSpan$Log$ValueCase;
    int i = 2;
    ((ValueCase)localObject).<init>("TEXT", 0, i);
    TEXT = (ValueCase)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/models/input/InputSpan$Log$ValueCase;
    int j = 1;
    ((ValueCase)localObject).<init>("VALUE_NOT_SET", j, 0);
    VALUE_NOT_SET = (ValueCase)localObject;
    localObject = new ValueCase[i];
    ValueCase localValueCase = TEXT;
    localObject[0] = localValueCase;
    localValueCase = VALUE_NOT_SET;
    localObject[j] = localValueCase;
    $VALUES = (ValueCase[])localObject;
  }
  
  private InputSpan$Log$ValueCase(int paramInt1)
  {
    value = paramInt1;
  }
  
  public static ValueCase forNumber(int paramInt)
  {
    if (paramInt != 0)
    {
      int i = 2;
      if (paramInt != i) {
        return null;
      }
      return TEXT;
    }
    return VALUE_NOT_SET;
  }
  
  public static ValueCase valueOf(int paramInt)
  {
    return forNumber(paramInt);
  }
  
  public final int getNumber()
  {
    return value;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.models.input.InputSpan.Log.ValueCase
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */