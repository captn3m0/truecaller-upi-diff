package com.truecaller.api.services.messenger.v1.models;

import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;

public final class c
  extends q
  implements d
{
  private static final c c;
  private static volatile ah d;
  private String a = "";
  private String b = "";
  
  static
  {
    c localc = new com/truecaller/api/services/messenger/v1/models/c;
    localc.<init>();
    c = localc;
    localc.makeImmutable();
  }
  
  public static c c()
  {
    return c;
  }
  
  public static ah d()
  {
    return c.getParserForType();
  }
  
  public final String a()
  {
    return a;
  }
  
  public final String b()
  {
    return b;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 38	com/truecaller/api/services/messenger/v1/models/c$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 44	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_0
    //   19: istore 6
    //   21: aconst_null
    //   22: astore 4
    //   24: iconst_1
    //   25: istore 7
    //   27: iload 5
    //   29: tableswitch	default:+47->76, 1:+406->435, 2:+402->431, 3:+400->429, 4:+389->418, 5:+263->292, 6:+109->138, 7:+259->288, 8:+57->86
    //   76: new 47	java/lang/UnsupportedOperationException
    //   79: astore_1
    //   80: aload_1
    //   81: invokespecial 48	java/lang/UnsupportedOperationException:<init>	()V
    //   84: aload_1
    //   85: athrow
    //   86: getstatic 50	com/truecaller/api/services/messenger/v1/models/c:d	Lcom/google/f/ah;
    //   89: astore_1
    //   90: aload_1
    //   91: ifnonnull +43 -> 134
    //   94: ldc 2
    //   96: astore_1
    //   97: aload_1
    //   98: monitorenter
    //   99: getstatic 50	com/truecaller/api/services/messenger/v1/models/c:d	Lcom/google/f/ah;
    //   102: astore_2
    //   103: aload_2
    //   104: ifnonnull +20 -> 124
    //   107: new 52	com/google/f/q$b
    //   110: astore_2
    //   111: getstatic 19	com/truecaller/api/services/messenger/v1/models/c:c	Lcom/truecaller/api/services/messenger/v1/models/c;
    //   114: astore_3
    //   115: aload_2
    //   116: aload_3
    //   117: invokespecial 55	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   120: aload_2
    //   121: putstatic 50	com/truecaller/api/services/messenger/v1/models/c:d	Lcom/google/f/ah;
    //   124: aload_1
    //   125: monitorexit
    //   126: goto +8 -> 134
    //   129: astore_2
    //   130: aload_1
    //   131: monitorexit
    //   132: aload_2
    //   133: athrow
    //   134: getstatic 50	com/truecaller/api/services/messenger/v1/models/c:d	Lcom/google/f/ah;
    //   137: areturn
    //   138: aload_2
    //   139: checkcast 57	com/google/f/g
    //   142: astore_2
    //   143: iload 6
    //   145: ifne +143 -> 288
    //   148: aload_2
    //   149: invokevirtual 60	com/google/f/g:readTag	()I
    //   152: istore 5
    //   154: iload 5
    //   156: ifeq +70 -> 226
    //   159: bipush 10
    //   161: istore 8
    //   163: iload 5
    //   165: iload 8
    //   167: if_icmpeq +46 -> 213
    //   170: bipush 18
    //   172: istore 8
    //   174: iload 5
    //   176: iload 8
    //   178: if_icmpeq +22 -> 200
    //   181: aload_2
    //   182: iload 5
    //   184: invokevirtual 66	com/google/f/g:skipField	(I)Z
    //   187: istore 5
    //   189: iload 5
    //   191: ifne -48 -> 143
    //   194: iconst_1
    //   195: istore 6
    //   197: goto -54 -> 143
    //   200: aload_2
    //   201: invokevirtual 70	com/google/f/g:readStringRequireUtf8	()Ljava/lang/String;
    //   204: astore_1
    //   205: aload_0
    //   206: aload_1
    //   207: putfield 29	com/truecaller/api/services/messenger/v1/models/c:b	Ljava/lang/String;
    //   210: goto -67 -> 143
    //   213: aload_2
    //   214: invokevirtual 70	com/google/f/g:readStringRequireUtf8	()Ljava/lang/String;
    //   217: astore_1
    //   218: aload_0
    //   219: aload_1
    //   220: putfield 27	com/truecaller/api/services/messenger/v1/models/c:a	Ljava/lang/String;
    //   223: goto -80 -> 143
    //   226: iconst_1
    //   227: istore 6
    //   229: goto -86 -> 143
    //   232: astore_1
    //   233: goto +53 -> 286
    //   236: astore_1
    //   237: new 72	java/lang/RuntimeException
    //   240: astore_2
    //   241: new 74	com/google/f/x
    //   244: astore_3
    //   245: aload_1
    //   246: invokevirtual 79	java/io/IOException:getMessage	()Ljava/lang/String;
    //   249: astore_1
    //   250: aload_3
    //   251: aload_1
    //   252: invokespecial 82	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   255: aload_3
    //   256: aload_0
    //   257: invokevirtual 86	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   260: astore_1
    //   261: aload_2
    //   262: aload_1
    //   263: invokespecial 89	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   266: aload_2
    //   267: athrow
    //   268: astore_1
    //   269: new 72	java/lang/RuntimeException
    //   272: astore_2
    //   273: aload_1
    //   274: aload_0
    //   275: invokevirtual 86	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   278: astore_1
    //   279: aload_2
    //   280: aload_1
    //   281: invokespecial 89	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   284: aload_2
    //   285: athrow
    //   286: aload_1
    //   287: athrow
    //   288: getstatic 19	com/truecaller/api/services/messenger/v1/models/c:c	Lcom/truecaller/api/services/messenger/v1/models/c;
    //   291: areturn
    //   292: aload_2
    //   293: checkcast 91	com/google/f/q$k
    //   296: astore_2
    //   297: aload_3
    //   298: checkcast 2	com/truecaller/api/services/messenger/v1/models/c
    //   301: astore_3
    //   302: aload_0
    //   303: getfield 27	com/truecaller/api/services/messenger/v1/models/c:a	Ljava/lang/String;
    //   306: invokevirtual 97	java/lang/String:isEmpty	()Z
    //   309: iload 7
    //   311: ixor
    //   312: istore 5
    //   314: aload_0
    //   315: getfield 27	com/truecaller/api/services/messenger/v1/models/c:a	Ljava/lang/String;
    //   318: astore 4
    //   320: aload_3
    //   321: getfield 27	com/truecaller/api/services/messenger/v1/models/c:a	Ljava/lang/String;
    //   324: invokevirtual 97	java/lang/String:isEmpty	()Z
    //   327: iload 7
    //   329: ixor
    //   330: istore 9
    //   332: aload_3
    //   333: getfield 27	com/truecaller/api/services/messenger/v1/models/c:a	Ljava/lang/String;
    //   336: astore 10
    //   338: aload_2
    //   339: iload 5
    //   341: aload 4
    //   343: iload 9
    //   345: aload 10
    //   347: invokeinterface 101 5 0
    //   352: astore_1
    //   353: aload_0
    //   354: aload_1
    //   355: putfield 27	com/truecaller/api/services/messenger/v1/models/c:a	Ljava/lang/String;
    //   358: aload_0
    //   359: getfield 29	com/truecaller/api/services/messenger/v1/models/c:b	Ljava/lang/String;
    //   362: invokevirtual 97	java/lang/String:isEmpty	()Z
    //   365: iload 7
    //   367: ixor
    //   368: istore 5
    //   370: aload_0
    //   371: getfield 29	com/truecaller/api/services/messenger/v1/models/c:b	Ljava/lang/String;
    //   374: astore 4
    //   376: aload_3
    //   377: getfield 29	com/truecaller/api/services/messenger/v1/models/c:b	Ljava/lang/String;
    //   380: invokevirtual 97	java/lang/String:isEmpty	()Z
    //   383: istore 9
    //   385: iload 7
    //   387: iload 9
    //   389: ixor
    //   390: istore 7
    //   392: aload_3
    //   393: getfield 29	com/truecaller/api/services/messenger/v1/models/c:b	Ljava/lang/String;
    //   396: astore_3
    //   397: aload_2
    //   398: iload 5
    //   400: aload 4
    //   402: iload 7
    //   404: aload_3
    //   405: invokeinterface 101 5 0
    //   410: astore_1
    //   411: aload_0
    //   412: aload_1
    //   413: putfield 29	com/truecaller/api/services/messenger/v1/models/c:b	Ljava/lang/String;
    //   416: aload_0
    //   417: areturn
    //   418: new 103	com/truecaller/api/services/messenger/v1/models/c$a
    //   421: astore_1
    //   422: aload_1
    //   423: iconst_0
    //   424: invokespecial 106	com/truecaller/api/services/messenger/v1/models/c$a:<init>	(B)V
    //   427: aload_1
    //   428: areturn
    //   429: aconst_null
    //   430: areturn
    //   431: getstatic 19	com/truecaller/api/services/messenger/v1/models/c:c	Lcom/truecaller/api/services/messenger/v1/models/c;
    //   434: areturn
    //   435: new 2	com/truecaller/api/services/messenger/v1/models/c
    //   438: astore_1
    //   439: aload_1
    //   440: invokespecial 17	com/truecaller/api/services/messenger/v1/models/c:<init>	()V
    //   443: aload_1
    //   444: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	445	0	this	c
    //   0	445	1	paramj	com.google.f.q.j
    //   0	445	2	paramObject1	Object
    //   0	445	3	paramObject2	Object
    //   3	398	4	localObject	Object
    //   9	174	5	i	int
    //   187	212	5	bool1	boolean
    //   19	209	6	j	int
    //   25	378	7	bool2	boolean
    //   161	18	8	k	int
    //   330	60	9	bool3	boolean
    //   336	10	10	str	String
    // Exception table:
    //   from	to	target	type
    //   99	102	129	finally
    //   107	110	129	finally
    //   111	114	129	finally
    //   116	120	129	finally
    //   120	124	129	finally
    //   124	126	129	finally
    //   130	132	129	finally
    //   148	152	232	finally
    //   182	187	232	finally
    //   200	204	232	finally
    //   206	210	232	finally
    //   213	217	232	finally
    //   219	223	232	finally
    //   237	240	232	finally
    //   241	244	232	finally
    //   245	249	232	finally
    //   251	255	232	finally
    //   256	260	232	finally
    //   262	266	232	finally
    //   266	268	232	finally
    //   269	272	232	finally
    //   274	278	232	finally
    //   280	284	232	finally
    //   284	286	232	finally
    //   148	152	236	java/io/IOException
    //   182	187	236	java/io/IOException
    //   200	204	236	java/io/IOException
    //   206	210	236	java/io/IOException
    //   213	217	236	java/io/IOException
    //   219	223	236	java/io/IOException
    //   148	152	268	com/google/f/x
    //   182	187	268	com/google/f/x
    //   200	204	268	com/google/f/x
    //   206	210	268	com/google/f/x
    //   213	217	268	com/google/f/x
    //   219	223	268	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int m = -1;
    if (i != m) {
      return i;
    }
    String str1 = a;
    boolean bool1 = str1.isEmpty();
    m = 0;
    String str2;
    if (!bool1)
    {
      str2 = a;
      int j = h.computeStringSize(1, str2);
      m = 0 + j;
    }
    str1 = b;
    boolean bool2 = str1.isEmpty();
    if (!bool2)
    {
      str2 = b;
      int k = h.computeStringSize(2, str2);
      m += k;
    }
    memoizedSerializedSize = m;
    return m;
  }
  
  public final void writeTo(h paramh)
  {
    String str1 = a;
    int i = str1.isEmpty();
    String str2;
    if (i == 0)
    {
      i = 1;
      str2 = a;
      paramh.writeString(i, str2);
    }
    str1 = b;
    boolean bool = str1.isEmpty();
    if (!bool)
    {
      int j = 2;
      str2 = b;
      paramh.writeString(j, str2);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.models.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */