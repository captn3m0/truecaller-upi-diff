package com.truecaller.api.services.messenger.v1.models.input;

import com.google.f.q.a;

public final class InputReactionContent$a
  extends q.a
  implements j
{
  private InputReactionContent$a()
  {
    super(localInputReactionContent);
  }
  
  public final a a(InputReactionContent.b.a parama)
  {
    copyOnWrite();
    InputReactionContent.a((InputReactionContent)instance, parama);
    return this;
  }
  
  public final a a(String paramString)
  {
    copyOnWrite();
    InputReactionContent.a((InputReactionContent)instance, paramString);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.models.input.InputReactionContent.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */