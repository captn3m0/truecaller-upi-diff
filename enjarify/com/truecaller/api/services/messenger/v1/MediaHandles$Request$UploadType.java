package com.truecaller.api.services.messenger.v1;

import com.google.f.w.c;
import com.google.f.w.d;

public enum MediaHandles$Request$UploadType
  implements w.c
{
  public static final int AVATAR_VALUE = 1;
  public static final int MEDIA_VALUE;
  private static final w.d internalValueMap;
  private final int value;
  
  static
  {
    Object localObject = new com/truecaller/api/services/messenger/v1/MediaHandles$Request$UploadType;
    ((UploadType)localObject).<init>("MEDIA", 0, 0);
    MEDIA = (UploadType)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/MediaHandles$Request$UploadType;
    int i = 1;
    ((UploadType)localObject).<init>("AVATAR", i, i);
    AVATAR = (UploadType)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/MediaHandles$Request$UploadType;
    int j = 2;
    ((UploadType)localObject).<init>("UNRECOGNIZED", j, -1);
    UNRECOGNIZED = (UploadType)localObject;
    localObject = new UploadType[3];
    UploadType localUploadType = MEDIA;
    localObject[0] = localUploadType;
    localUploadType = AVATAR;
    localObject[i] = localUploadType;
    localUploadType = UNRECOGNIZED;
    localObject[j] = localUploadType;
    $VALUES = (UploadType[])localObject;
    localObject = new com/truecaller/api/services/messenger/v1/MediaHandles$Request$UploadType$1;
    ((MediaHandles.Request.UploadType.1)localObject).<init>();
    internalValueMap = (w.d)localObject;
  }
  
  private MediaHandles$Request$UploadType(int paramInt1)
  {
    value = paramInt1;
  }
  
  public static UploadType forNumber(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return null;
    case 1: 
      return AVATAR;
    }
    return MEDIA;
  }
  
  public static w.d internalGetValueMap()
  {
    return internalValueMap;
  }
  
  public static UploadType valueOf(int paramInt)
  {
    return forNumber(paramInt);
  }
  
  public final int getNumber()
  {
    return value;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.MediaHandles.Request.UploadType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */