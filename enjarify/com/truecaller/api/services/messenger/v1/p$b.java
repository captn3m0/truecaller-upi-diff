package com.truecaller.api.services.messenger.v1;

import com.google.f.ae;
import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;
import com.truecaller.api.services.messenger.v1.models.input.InputMessageContent;
import com.truecaller.api.services.messenger.v1.models.input.InputPeer;

public final class p$b
  extends q
  implements p.c
{
  private static final b d;
  private static volatile ah e;
  private long a;
  private InputPeer b;
  private InputMessageContent c;
  
  static
  {
    b localb = new com/truecaller/api/services/messenger/v1/p$b;
    localb.<init>();
    d = localb;
    localb.makeImmutable();
  }
  
  public static p.b.a a()
  {
    return (p.b.a)d.toBuilder();
  }
  
  public static b b()
  {
    return d;
  }
  
  public static ah c()
  {
    return d.getParserForType();
  }
  
  private InputPeer e()
  {
    InputPeer localInputPeer = b;
    if (localInputPeer == null) {
      localInputPeer = InputPeer.b();
    }
    return localInputPeer;
  }
  
  private InputMessageContent f()
  {
    InputMessageContent localInputMessageContent = c;
    if (localInputMessageContent == null) {
      localInputMessageContent = InputMessageContent.b();
    }
    return localInputMessageContent;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 60	com/truecaller/api/services/messenger/v1/p$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 66	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_0
    //   19: istore 6
    //   21: aconst_null
    //   22: astore 4
    //   24: iconst_0
    //   25: istore 7
    //   27: iload 5
    //   29: tableswitch	default:+47->76, 1:+643->672, 2:+639->668, 3:+637->666, 4:+626->655, 5:+456->485, 6:+109->138, 7:+452->481, 8:+57->86
    //   76: new 69	java/lang/UnsupportedOperationException
    //   79: astore_1
    //   80: aload_1
    //   81: invokespecial 70	java/lang/UnsupportedOperationException:<init>	()V
    //   84: aload_1
    //   85: athrow
    //   86: getstatic 72	com/truecaller/api/services/messenger/v1/p$b:e	Lcom/google/f/ah;
    //   89: astore_1
    //   90: aload_1
    //   91: ifnonnull +43 -> 134
    //   94: ldc 2
    //   96: astore_1
    //   97: aload_1
    //   98: monitorenter
    //   99: getstatic 72	com/truecaller/api/services/messenger/v1/p$b:e	Lcom/google/f/ah;
    //   102: astore_2
    //   103: aload_2
    //   104: ifnonnull +20 -> 124
    //   107: new 74	com/google/f/q$b
    //   110: astore_2
    //   111: getstatic 22	com/truecaller/api/services/messenger/v1/p$b:d	Lcom/truecaller/api/services/messenger/v1/p$b;
    //   114: astore_3
    //   115: aload_2
    //   116: aload_3
    //   117: invokespecial 77	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   120: aload_2
    //   121: putstatic 72	com/truecaller/api/services/messenger/v1/p$b:e	Lcom/google/f/ah;
    //   124: aload_1
    //   125: monitorexit
    //   126: goto +8 -> 134
    //   129: astore_2
    //   130: aload_1
    //   131: monitorexit
    //   132: aload_2
    //   133: athrow
    //   134: getstatic 72	com/truecaller/api/services/messenger/v1/p$b:e	Lcom/google/f/ah;
    //   137: areturn
    //   138: aload_2
    //   139: checkcast 79	com/google/f/g
    //   142: astore_2
    //   143: aload_3
    //   144: checkcast 81	com/google/f/n
    //   147: astore_3
    //   148: iload 7
    //   150: ifne +331 -> 481
    //   153: aload_2
    //   154: invokevirtual 84	com/google/f/g:readTag	()I
    //   157: istore 5
    //   159: iload 5
    //   161: ifeq +258 -> 419
    //   164: bipush 8
    //   166: istore 8
    //   168: iload 5
    //   170: iload 8
    //   172: if_icmpeq +232 -> 404
    //   175: bipush 18
    //   177: istore 8
    //   179: iload 5
    //   181: iload 8
    //   183: if_icmpeq +127 -> 310
    //   186: bipush 26
    //   188: istore 8
    //   190: iload 5
    //   192: iload 8
    //   194: if_icmpeq +22 -> 216
    //   197: aload_2
    //   198: iload 5
    //   200: invokevirtual 91	com/google/f/g:skipField	(I)Z
    //   203: istore 5
    //   205: iload 5
    //   207: ifne -59 -> 148
    //   210: iconst_1
    //   211: istore 7
    //   213: goto -65 -> 148
    //   216: aload_0
    //   217: getfield 36	com/truecaller/api/services/messenger/v1/p$b:c	Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent;
    //   220: astore_1
    //   221: aload_1
    //   222: ifnull +21 -> 243
    //   225: aload_0
    //   226: getfield 36	com/truecaller/api/services/messenger/v1/p$b:c	Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent;
    //   229: astore_1
    //   230: aload_1
    //   231: invokevirtual 92	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:toBuilder	()Lcom/google/f/q$a;
    //   234: astore_1
    //   235: aload_1
    //   236: checkcast 94	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$c
    //   239: astore_1
    //   240: goto +8 -> 248
    //   243: iconst_0
    //   244: istore 5
    //   246: aconst_null
    //   247: astore_1
    //   248: invokestatic 96	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:c	()Lcom/google/f/ah;
    //   251: astore 9
    //   253: aload_2
    //   254: aload 9
    //   256: aload_3
    //   257: invokevirtual 100	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   260: astore 9
    //   262: aload 9
    //   264: checkcast 52	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent
    //   267: astore 9
    //   269: aload_0
    //   270: aload 9
    //   272: putfield 36	com/truecaller/api/services/messenger/v1/p$b:c	Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent;
    //   275: aload_1
    //   276: ifnull -128 -> 148
    //   279: aload_0
    //   280: getfield 36	com/truecaller/api/services/messenger/v1/p$b:c	Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent;
    //   283: astore 9
    //   285: aload_1
    //   286: aload 9
    //   288: invokevirtual 104	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$c:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   291: pop
    //   292: aload_1
    //   293: invokevirtual 108	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$c:buildPartial	()Lcom/google/f/q;
    //   296: astore_1
    //   297: aload_1
    //   298: checkcast 52	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent
    //   301: astore_1
    //   302: aload_0
    //   303: aload_1
    //   304: putfield 36	com/truecaller/api/services/messenger/v1/p$b:c	Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent;
    //   307: goto -159 -> 148
    //   310: aload_0
    //   311: getfield 41	com/truecaller/api/services/messenger/v1/p$b:b	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   314: astore_1
    //   315: aload_1
    //   316: ifnull +21 -> 337
    //   319: aload_0
    //   320: getfield 41	com/truecaller/api/services/messenger/v1/p$b:b	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   323: astore_1
    //   324: aload_1
    //   325: invokevirtual 109	com/truecaller/api/services/messenger/v1/models/input/InputPeer:toBuilder	()Lcom/google/f/q$a;
    //   328: astore_1
    //   329: aload_1
    //   330: checkcast 111	com/truecaller/api/services/messenger/v1/models/input/InputPeer$a
    //   333: astore_1
    //   334: goto +8 -> 342
    //   337: iconst_0
    //   338: istore 5
    //   340: aconst_null
    //   341: astore_1
    //   342: invokestatic 112	com/truecaller/api/services/messenger/v1/models/input/InputPeer:c	()Lcom/google/f/ah;
    //   345: astore 9
    //   347: aload_2
    //   348: aload 9
    //   350: aload_3
    //   351: invokevirtual 100	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   354: astore 9
    //   356: aload 9
    //   358: checkcast 47	com/truecaller/api/services/messenger/v1/models/input/InputPeer
    //   361: astore 9
    //   363: aload_0
    //   364: aload 9
    //   366: putfield 41	com/truecaller/api/services/messenger/v1/p$b:b	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   369: aload_1
    //   370: ifnull -222 -> 148
    //   373: aload_0
    //   374: getfield 41	com/truecaller/api/services/messenger/v1/p$b:b	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   377: astore 9
    //   379: aload_1
    //   380: aload 9
    //   382: invokevirtual 113	com/truecaller/api/services/messenger/v1/models/input/InputPeer$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   385: pop
    //   386: aload_1
    //   387: invokevirtual 114	com/truecaller/api/services/messenger/v1/models/input/InputPeer$a:buildPartial	()Lcom/google/f/q;
    //   390: astore_1
    //   391: aload_1
    //   392: checkcast 47	com/truecaller/api/services/messenger/v1/models/input/InputPeer
    //   395: astore_1
    //   396: aload_0
    //   397: aload_1
    //   398: putfield 41	com/truecaller/api/services/messenger/v1/p$b:b	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   401: goto -253 -> 148
    //   404: aload_2
    //   405: invokevirtual 118	com/google/f/g:readInt64	()J
    //   408: lstore 10
    //   410: aload_0
    //   411: lload 10
    //   413: putfield 34	com/truecaller/api/services/messenger/v1/p$b:a	J
    //   416: goto -268 -> 148
    //   419: iconst_1
    //   420: istore 7
    //   422: goto -274 -> 148
    //   425: astore_1
    //   426: goto +53 -> 479
    //   429: astore_1
    //   430: new 120	java/lang/RuntimeException
    //   433: astore_2
    //   434: new 122	com/google/f/x
    //   437: astore_3
    //   438: aload_1
    //   439: invokevirtual 128	java/io/IOException:getMessage	()Ljava/lang/String;
    //   442: astore_1
    //   443: aload_3
    //   444: aload_1
    //   445: invokespecial 131	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   448: aload_3
    //   449: aload_0
    //   450: invokevirtual 135	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   453: astore_1
    //   454: aload_2
    //   455: aload_1
    //   456: invokespecial 138	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   459: aload_2
    //   460: athrow
    //   461: astore_1
    //   462: new 120	java/lang/RuntimeException
    //   465: astore_2
    //   466: aload_1
    //   467: aload_0
    //   468: invokevirtual 135	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   471: astore_1
    //   472: aload_2
    //   473: aload_1
    //   474: invokespecial 138	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   477: aload_2
    //   478: athrow
    //   479: aload_1
    //   480: athrow
    //   481: getstatic 22	com/truecaller/api/services/messenger/v1/p$b:d	Lcom/truecaller/api/services/messenger/v1/p$b;
    //   484: areturn
    //   485: aload_2
    //   486: astore_1
    //   487: aload_2
    //   488: checkcast 140	com/google/f/q$k
    //   491: astore_1
    //   492: aload_3
    //   493: checkcast 2	com/truecaller/api/services/messenger/v1/p$b
    //   496: astore_3
    //   497: aload_0
    //   498: getfield 34	com/truecaller/api/services/messenger/v1/p$b:a	J
    //   501: lstore 10
    //   503: lconst_0
    //   504: lstore 12
    //   506: lload 10
    //   508: lload 12
    //   510: lcmp
    //   511: istore 14
    //   513: iload 14
    //   515: ifeq +9 -> 524
    //   518: iconst_1
    //   519: istore 14
    //   521: goto +8 -> 529
    //   524: iconst_0
    //   525: istore 14
    //   527: aconst_null
    //   528: astore_2
    //   529: aload_0
    //   530: getfield 34	com/truecaller/api/services/messenger/v1/p$b:a	J
    //   533: lstore 10
    //   535: aload_3
    //   536: getfield 34	com/truecaller/api/services/messenger/v1/p$b:a	J
    //   539: lstore 15
    //   541: lload 15
    //   543: lload 12
    //   545: lcmp
    //   546: istore 6
    //   548: iload 6
    //   550: ifeq +9 -> 559
    //   553: iconst_1
    //   554: istore 17
    //   556: goto +6 -> 562
    //   559: iconst_0
    //   560: istore 17
    //   562: aload_3
    //   563: getfield 34	com/truecaller/api/services/messenger/v1/p$b:a	J
    //   566: lstore 18
    //   568: aload_1
    //   569: astore 4
    //   571: lload 18
    //   573: lstore 12
    //   575: aload_1
    //   576: iload 14
    //   578: lload 10
    //   580: iload 17
    //   582: lload 18
    //   584: invokeinterface 144 7 0
    //   589: lstore 20
    //   591: aload_0
    //   592: lload 20
    //   594: putfield 34	com/truecaller/api/services/messenger/v1/p$b:a	J
    //   597: aload_0
    //   598: getfield 41	com/truecaller/api/services/messenger/v1/p$b:b	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   601: astore_2
    //   602: aload_3
    //   603: getfield 41	com/truecaller/api/services/messenger/v1/p$b:b	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   606: astore 4
    //   608: aload_1
    //   609: aload_2
    //   610: aload 4
    //   612: invokeinterface 148 3 0
    //   617: checkcast 47	com/truecaller/api/services/messenger/v1/models/input/InputPeer
    //   620: astore_2
    //   621: aload_0
    //   622: aload_2
    //   623: putfield 41	com/truecaller/api/services/messenger/v1/p$b:b	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   626: aload_0
    //   627: getfield 36	com/truecaller/api/services/messenger/v1/p$b:c	Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent;
    //   630: astore_2
    //   631: aload_3
    //   632: getfield 36	com/truecaller/api/services/messenger/v1/p$b:c	Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent;
    //   635: astore_3
    //   636: aload_1
    //   637: aload_2
    //   638: aload_3
    //   639: invokeinterface 148 3 0
    //   644: checkcast 52	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent
    //   647: astore_1
    //   648: aload_0
    //   649: aload_1
    //   650: putfield 36	com/truecaller/api/services/messenger/v1/p$b:c	Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent;
    //   653: aload_0
    //   654: areturn
    //   655: new 32	com/truecaller/api/services/messenger/v1/p$b$a
    //   658: astore_1
    //   659: aload_1
    //   660: iconst_0
    //   661: invokespecial 151	com/truecaller/api/services/messenger/v1/p$b$a:<init>	(B)V
    //   664: aload_1
    //   665: areturn
    //   666: aconst_null
    //   667: areturn
    //   668: getstatic 22	com/truecaller/api/services/messenger/v1/p$b:d	Lcom/truecaller/api/services/messenger/v1/p$b;
    //   671: areturn
    //   672: new 2	com/truecaller/api/services/messenger/v1/p$b
    //   675: astore_1
    //   676: aload_1
    //   677: invokespecial 20	com/truecaller/api/services/messenger/v1/p$b:<init>	()V
    //   680: aload_1
    //   681: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	682	0	this	b
    //   0	682	1	paramj	com.google.f.q.j
    //   0	682	2	paramObject1	Object
    //   0	682	3	paramObject2	Object
    //   3	608	4	localObject1	Object
    //   9	190	5	i	int
    //   203	136	5	bool1	boolean
    //   19	530	6	bool2	boolean
    //   25	396	7	j	int
    //   166	29	8	k	int
    //   251	130	9	localObject2	Object
    //   408	171	10	l1	long
    //   504	70	12	l2	long
    //   511	66	14	bool3	boolean
    //   539	3	15	l3	long
    //   554	27	17	bool4	boolean
    //   566	17	18	l4	long
    //   589	4	20	l5	long
    // Exception table:
    //   from	to	target	type
    //   99	102	129	finally
    //   107	110	129	finally
    //   111	114	129	finally
    //   116	120	129	finally
    //   120	124	129	finally
    //   124	126	129	finally
    //   130	132	129	finally
    //   153	157	425	finally
    //   198	203	425	finally
    //   216	220	425	finally
    //   225	229	425	finally
    //   230	234	425	finally
    //   235	239	425	finally
    //   248	251	425	finally
    //   256	260	425	finally
    //   262	267	425	finally
    //   270	275	425	finally
    //   279	283	425	finally
    //   286	292	425	finally
    //   292	296	425	finally
    //   297	301	425	finally
    //   303	307	425	finally
    //   310	314	425	finally
    //   319	323	425	finally
    //   324	328	425	finally
    //   329	333	425	finally
    //   342	345	425	finally
    //   350	354	425	finally
    //   356	361	425	finally
    //   364	369	425	finally
    //   373	377	425	finally
    //   380	386	425	finally
    //   386	390	425	finally
    //   391	395	425	finally
    //   397	401	425	finally
    //   404	408	425	finally
    //   411	416	425	finally
    //   430	433	425	finally
    //   434	437	425	finally
    //   438	442	425	finally
    //   444	448	425	finally
    //   449	453	425	finally
    //   455	459	425	finally
    //   459	461	425	finally
    //   462	465	425	finally
    //   467	471	425	finally
    //   473	477	425	finally
    //   477	479	425	finally
    //   153	157	429	java/io/IOException
    //   198	203	429	java/io/IOException
    //   216	220	429	java/io/IOException
    //   225	229	429	java/io/IOException
    //   230	234	429	java/io/IOException
    //   235	239	429	java/io/IOException
    //   248	251	429	java/io/IOException
    //   256	260	429	java/io/IOException
    //   262	267	429	java/io/IOException
    //   270	275	429	java/io/IOException
    //   279	283	429	java/io/IOException
    //   286	292	429	java/io/IOException
    //   292	296	429	java/io/IOException
    //   297	301	429	java/io/IOException
    //   303	307	429	java/io/IOException
    //   310	314	429	java/io/IOException
    //   319	323	429	java/io/IOException
    //   324	328	429	java/io/IOException
    //   329	333	429	java/io/IOException
    //   342	345	429	java/io/IOException
    //   350	354	429	java/io/IOException
    //   356	361	429	java/io/IOException
    //   364	369	429	java/io/IOException
    //   373	377	429	java/io/IOException
    //   380	386	429	java/io/IOException
    //   386	390	429	java/io/IOException
    //   391	395	429	java/io/IOException
    //   397	401	429	java/io/IOException
    //   404	408	429	java/io/IOException
    //   411	416	429	java/io/IOException
    //   153	157	461	com/google/f/x
    //   198	203	461	com/google/f/x
    //   216	220	461	com/google/f/x
    //   225	229	461	com/google/f/x
    //   230	234	461	com/google/f/x
    //   235	239	461	com/google/f/x
    //   248	251	461	com/google/f/x
    //   256	260	461	com/google/f/x
    //   262	267	461	com/google/f/x
    //   270	275	461	com/google/f/x
    //   279	283	461	com/google/f/x
    //   286	292	461	com/google/f/x
    //   292	296	461	com/google/f/x
    //   297	301	461	com/google/f/x
    //   303	307	461	com/google/f/x
    //   310	314	461	com/google/f/x
    //   319	323	461	com/google/f/x
    //   324	328	461	com/google/f/x
    //   329	333	461	com/google/f/x
    //   342	345	461	com/google/f/x
    //   350	354	461	com/google/f/x
    //   356	361	461	com/google/f/x
    //   364	369	461	com/google/f/x
    //   373	377	461	com/google/f/x
    //   380	386	461	com/google/f/x
    //   386	390	461	com/google/f/x
    //   391	395	461	com/google/f/x
    //   397	401	461	com/google/f/x
    //   404	408	461	com/google/f/x
    //   411	416	461	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int j = -1;
    if (i != j) {
      return i;
    }
    long l1 = a;
    long l2 = 0L;
    int k = 0;
    boolean bool = l1 < l2;
    if (bool)
    {
      int m = 1;
      i = h.computeInt64Size(m, l1);
      k = 0 + i;
    }
    Object localObject1 = b;
    Object localObject2;
    if (localObject1 != null)
    {
      localObject2 = e();
      i = h.computeMessageSize(2, (ae)localObject2);
      k += i;
    }
    localObject1 = c;
    if (localObject1 != null)
    {
      localObject2 = f();
      i = h.computeMessageSize(3, (ae)localObject2);
      k += i;
    }
    memoizedSerializedSize = k;
    return k;
  }
  
  public final void writeTo(h paramh)
  {
    long l1 = a;
    long l2 = 0L;
    boolean bool = l1 < l2;
    if (bool)
    {
      int i = 1;
      paramh.writeInt64(i, l1);
    }
    Object localObject1 = b;
    int j;
    Object localObject2;
    if (localObject1 != null)
    {
      j = 2;
      localObject2 = e();
      paramh.writeMessage(j, (ae)localObject2);
    }
    localObject1 = c;
    if (localObject1 != null)
    {
      j = 3;
      localObject2 = f();
      paramh.writeMessage(j, (ae)localObject2);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.p.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */