package com.truecaller.api.services.messenger.v1;

import com.google.f.q.a;
import com.truecaller.api.services.messenger.v1.models.input.InputPeer.a;
import com.truecaller.api.services.messenger.v1.models.input.InputReactionContent.a;

public final class r$b$a
  extends q.a
  implements r.c
{
  private r$b$a()
  {
    super(localb);
  }
  
  public final a a(InputPeer.a parama)
  {
    copyOnWrite();
    r.b.a((r.b)instance, parama);
    return this;
  }
  
  public final a a(InputReactionContent.a parama)
  {
    copyOnWrite();
    r.b.a((r.b)instance, parama);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.r.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */