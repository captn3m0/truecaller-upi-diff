package com.truecaller.api.services.messenger.v1;

import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;

public final class Batched$Response$a
  extends q
  implements Batched.Response.b
{
  private static final a c;
  private static volatile ah d;
  private int a;
  private String b = "";
  
  static
  {
    a locala = new com/truecaller/api/services/messenger/v1/Batched$Response$a;
    locala.<init>();
    c = locala;
    locala.makeImmutable();
  }
  
  public static ah a()
  {
    return c.getParserForType();
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 37	com/truecaller/api/services/messenger/v1/Batched$1:b	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 43	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_0
    //   19: istore 6
    //   21: aconst_null
    //   22: astore 4
    //   24: iconst_1
    //   25: istore 7
    //   27: iload 5
    //   29: tableswitch	default:+47->76, 1:+422->451, 2:+418->447, 3:+416->445, 4:+405->434, 5:+265->294, 6:+109->138, 7:+261->290, 8:+57->86
    //   76: new 46	java/lang/UnsupportedOperationException
    //   79: astore_1
    //   80: aload_1
    //   81: invokespecial 47	java/lang/UnsupportedOperationException:<init>	()V
    //   84: aload_1
    //   85: athrow
    //   86: getstatic 49	com/truecaller/api/services/messenger/v1/Batched$Response$a:d	Lcom/google/f/ah;
    //   89: astore_1
    //   90: aload_1
    //   91: ifnonnull +43 -> 134
    //   94: ldc 2
    //   96: astore_1
    //   97: aload_1
    //   98: monitorenter
    //   99: getstatic 49	com/truecaller/api/services/messenger/v1/Batched$Response$a:d	Lcom/google/f/ah;
    //   102: astore_2
    //   103: aload_2
    //   104: ifnonnull +20 -> 124
    //   107: new 51	com/google/f/q$b
    //   110: astore_2
    //   111: getstatic 20	com/truecaller/api/services/messenger/v1/Batched$Response$a:c	Lcom/truecaller/api/services/messenger/v1/Batched$Response$a;
    //   114: astore_3
    //   115: aload_2
    //   116: aload_3
    //   117: invokespecial 54	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   120: aload_2
    //   121: putstatic 49	com/truecaller/api/services/messenger/v1/Batched$Response$a:d	Lcom/google/f/ah;
    //   124: aload_1
    //   125: monitorexit
    //   126: goto +8 -> 134
    //   129: astore_2
    //   130: aload_1
    //   131: monitorexit
    //   132: aload_2
    //   133: athrow
    //   134: getstatic 49	com/truecaller/api/services/messenger/v1/Batched$Response$a:d	Lcom/google/f/ah;
    //   137: areturn
    //   138: aload_2
    //   139: checkcast 56	com/google/f/g
    //   142: astore_2
    //   143: iload 6
    //   145: ifne +145 -> 290
    //   148: aload_2
    //   149: invokevirtual 59	com/google/f/g:readTag	()I
    //   152: istore 5
    //   154: iload 5
    //   156: ifeq +72 -> 228
    //   159: bipush 8
    //   161: istore 8
    //   163: iload 5
    //   165: iload 8
    //   167: if_icmpeq +46 -> 213
    //   170: bipush 18
    //   172: istore 8
    //   174: iload 5
    //   176: iload 8
    //   178: if_icmpeq +22 -> 200
    //   181: aload_2
    //   182: iload 5
    //   184: invokevirtual 65	com/google/f/g:skipField	(I)Z
    //   187: istore 5
    //   189: iload 5
    //   191: ifne -48 -> 143
    //   194: iconst_1
    //   195: istore 6
    //   197: goto -54 -> 143
    //   200: aload_2
    //   201: invokevirtual 69	com/google/f/g:readStringRequireUtf8	()Ljava/lang/String;
    //   204: astore_1
    //   205: aload_0
    //   206: aload_1
    //   207: putfield 28	com/truecaller/api/services/messenger/v1/Batched$Response$a:b	Ljava/lang/String;
    //   210: goto -67 -> 143
    //   213: aload_2
    //   214: invokevirtual 72	com/google/f/g:readInt32	()I
    //   217: istore 5
    //   219: aload_0
    //   220: iload 5
    //   222: putfield 74	com/truecaller/api/services/messenger/v1/Batched$Response$a:a	I
    //   225: goto -82 -> 143
    //   228: iconst_1
    //   229: istore 6
    //   231: goto -88 -> 143
    //   234: astore_1
    //   235: goto +53 -> 288
    //   238: astore_1
    //   239: new 76	java/lang/RuntimeException
    //   242: astore_2
    //   243: new 78	com/google/f/x
    //   246: astore_3
    //   247: aload_1
    //   248: invokevirtual 83	java/io/IOException:getMessage	()Ljava/lang/String;
    //   251: astore_1
    //   252: aload_3
    //   253: aload_1
    //   254: invokespecial 86	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   257: aload_3
    //   258: aload_0
    //   259: invokevirtual 90	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   262: astore_1
    //   263: aload_2
    //   264: aload_1
    //   265: invokespecial 93	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   268: aload_2
    //   269: athrow
    //   270: astore_1
    //   271: new 76	java/lang/RuntimeException
    //   274: astore_2
    //   275: aload_1
    //   276: aload_0
    //   277: invokevirtual 90	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   280: astore_1
    //   281: aload_2
    //   282: aload_1
    //   283: invokespecial 93	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   286: aload_2
    //   287: athrow
    //   288: aload_1
    //   289: athrow
    //   290: getstatic 20	com/truecaller/api/services/messenger/v1/Batched$Response$a:c	Lcom/truecaller/api/services/messenger/v1/Batched$Response$a;
    //   293: areturn
    //   294: aload_2
    //   295: checkcast 95	com/google/f/q$k
    //   298: astore_2
    //   299: aload_3
    //   300: checkcast 2	com/truecaller/api/services/messenger/v1/Batched$Response$a
    //   303: astore_3
    //   304: aload_0
    //   305: getfield 74	com/truecaller/api/services/messenger/v1/Batched$Response$a:a	I
    //   308: istore 5
    //   310: iload 5
    //   312: ifeq +9 -> 321
    //   315: iconst_1
    //   316: istore 5
    //   318: goto +8 -> 326
    //   321: iconst_0
    //   322: istore 5
    //   324: aconst_null
    //   325: astore_1
    //   326: aload_0
    //   327: getfield 74	com/truecaller/api/services/messenger/v1/Batched$Response$a:a	I
    //   330: istore 9
    //   332: aload_3
    //   333: getfield 74	com/truecaller/api/services/messenger/v1/Batched$Response$a:a	I
    //   336: istore 10
    //   338: iload 10
    //   340: ifeq +6 -> 346
    //   343: iconst_1
    //   344: istore 6
    //   346: aload_3
    //   347: getfield 74	com/truecaller/api/services/messenger/v1/Batched$Response$a:a	I
    //   350: istore 10
    //   352: aload_2
    //   353: iload 5
    //   355: iload 9
    //   357: iload 6
    //   359: iload 10
    //   361: invokeinterface 99 5 0
    //   366: istore 5
    //   368: aload_0
    //   369: iload 5
    //   371: putfield 74	com/truecaller/api/services/messenger/v1/Batched$Response$a:a	I
    //   374: aload_0
    //   375: getfield 28	com/truecaller/api/services/messenger/v1/Batched$Response$a:b	Ljava/lang/String;
    //   378: invokevirtual 105	java/lang/String:isEmpty	()Z
    //   381: iload 7
    //   383: ixor
    //   384: istore 5
    //   386: aload_0
    //   387: getfield 28	com/truecaller/api/services/messenger/v1/Batched$Response$a:b	Ljava/lang/String;
    //   390: astore 4
    //   392: aload_3
    //   393: getfield 28	com/truecaller/api/services/messenger/v1/Batched$Response$a:b	Ljava/lang/String;
    //   396: invokevirtual 105	java/lang/String:isEmpty	()Z
    //   399: istore 9
    //   401: iload 7
    //   403: iload 9
    //   405: ixor
    //   406: istore 7
    //   408: aload_3
    //   409: getfield 28	com/truecaller/api/services/messenger/v1/Batched$Response$a:b	Ljava/lang/String;
    //   412: astore_3
    //   413: aload_2
    //   414: iload 5
    //   416: aload 4
    //   418: iload 7
    //   420: aload_3
    //   421: invokeinterface 109 5 0
    //   426: astore_1
    //   427: aload_0
    //   428: aload_1
    //   429: putfield 28	com/truecaller/api/services/messenger/v1/Batched$Response$a:b	Ljava/lang/String;
    //   432: aload_0
    //   433: areturn
    //   434: new 111	com/truecaller/api/services/messenger/v1/Batched$Response$a$a
    //   437: astore_1
    //   438: aload_1
    //   439: iconst_0
    //   440: invokespecial 114	com/truecaller/api/services/messenger/v1/Batched$Response$a$a:<init>	(B)V
    //   443: aload_1
    //   444: areturn
    //   445: aconst_null
    //   446: areturn
    //   447: getstatic 20	com/truecaller/api/services/messenger/v1/Batched$Response$a:c	Lcom/truecaller/api/services/messenger/v1/Batched$Response$a;
    //   450: areturn
    //   451: new 2	com/truecaller/api/services/messenger/v1/Batched$Response$a
    //   454: astore_1
    //   455: aload_1
    //   456: invokespecial 18	com/truecaller/api/services/messenger/v1/Batched$Response$a:<init>	()V
    //   459: aload_1
    //   460: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	461	0	this	a
    //   0	461	1	paramj	com.google.f.q.j
    //   0	461	2	paramObject1	Object
    //   0	461	3	paramObject2	Object
    //   3	414	4	localObject	Object
    //   9	174	5	i	int
    //   187	3	5	bool1	boolean
    //   217	137	5	j	int
    //   366	4	5	k	int
    //   384	31	5	bool2	boolean
    //   19	339	6	bool3	boolean
    //   25	394	7	bool4	boolean
    //   161	18	8	m	int
    //   330	26	9	n	int
    //   399	7	9	bool5	boolean
    //   336	24	10	i1	int
    // Exception table:
    //   from	to	target	type
    //   99	102	129	finally
    //   107	110	129	finally
    //   111	114	129	finally
    //   116	120	129	finally
    //   120	124	129	finally
    //   124	126	129	finally
    //   130	132	129	finally
    //   148	152	234	finally
    //   182	187	234	finally
    //   200	204	234	finally
    //   206	210	234	finally
    //   213	217	234	finally
    //   220	225	234	finally
    //   239	242	234	finally
    //   243	246	234	finally
    //   247	251	234	finally
    //   253	257	234	finally
    //   258	262	234	finally
    //   264	268	234	finally
    //   268	270	234	finally
    //   271	274	234	finally
    //   276	280	234	finally
    //   282	286	234	finally
    //   286	288	234	finally
    //   148	152	238	java/io/IOException
    //   182	187	238	java/io/IOException
    //   200	204	238	java/io/IOException
    //   206	210	238	java/io/IOException
    //   213	217	238	java/io/IOException
    //   220	225	238	java/io/IOException
    //   148	152	270	com/google/f/x
    //   182	187	270	com/google/f/x
    //   200	204	270	com/google/f/x
    //   206	210	270	com/google/f/x
    //   213	217	270	com/google/f/x
    //   220	225	270	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int k = -1;
    if (i != k) {
      return i;
    }
    i = a;
    k = 0;
    if (i != 0)
    {
      int m = 1;
      i = h.computeInt32Size(m, i);
      k = 0 + i;
    }
    String str1 = b;
    boolean bool = str1.isEmpty();
    if (!bool)
    {
      String str2 = b;
      int j = h.computeStringSize(2, str2);
      k += j;
    }
    memoizedSerializedSize = k;
    return k;
  }
  
  public final void writeTo(h paramh)
  {
    int i = a;
    if (i != 0)
    {
      int k = 1;
      paramh.writeInt32(k, i);
    }
    String str1 = b;
    boolean bool = str1.isEmpty();
    if (!bool)
    {
      int j = 2;
      String str2 = b;
      paramh.writeString(j, str2);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.Batched.Response.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */