package com.truecaller.api.services.messenger.v1;

import io.grpc.ap;
import io.grpc.c.a;
import io.grpc.c.d;
import io.grpc.e;
import io.grpc.f;

public final class k$a
  extends a
{
  private k$a(f paramf)
  {
    super(paramf);
  }
  
  private k$a(f paramf, e parame)
  {
    super(paramf, parame);
  }
  
  public final MediaHandles.c a(MediaHandles.Request paramRequest)
  {
    f localf = getChannel();
    ap localap = k.f();
    e locale = getCallOptions();
    return (MediaHandles.c)d.a(localf, localap, locale, paramRequest);
  }
  
  public final a.d a(a.b paramb)
  {
    f localf = getChannel();
    ap localap = k.l();
    e locale = getCallOptions();
    return (a.d)d.a(localf, localap, locale, paramb);
  }
  
  public final d.d a(d.b paramb)
  {
    f localf = getChannel();
    ap localap = k.j();
    e locale = getCallOptions();
    return (d.d)d.a(localf, localap, locale, paramb);
  }
  
  public final f.d a(f.b paramb)
  {
    f localf = getChannel();
    ap localap = k.i();
    e locale = getCallOptions();
    return (f.d)d.a(localf, localap, locale, paramb);
  }
  
  public final h.d a(h.b paramb)
  {
    f localf = getChannel();
    ap localap = k.b();
    e locale = getCallOptions();
    return (h.d)d.a(localf, localap, locale, paramb);
  }
  
  public final l.d a(l.b paramb)
  {
    f localf = getChannel();
    ap localap = k.a();
    e locale = getCallOptions();
    return (l.d)d.a(localf, localap, locale, paramb);
  }
  
  public final n.d a(n.b paramb)
  {
    f localf = getChannel();
    ap localap = k.m();
    e locale = getCallOptions();
    return (n.d)d.a(localf, localap, locale, paramb);
  }
  
  public final p.d a(p.b paramb)
  {
    f localf = getChannel();
    ap localap = k.d();
    e locale = getCallOptions();
    return (p.d)d.a(localf, localap, locale, paramb);
  }
  
  public final r.d a(r.b paramb)
  {
    f localf = getChannel();
    ap localap = k.h();
    e locale = getCallOptions();
    return (r.d)d.a(localf, localap, locale, paramb);
  }
  
  public final t.d a(t.b paramb)
  {
    f localf = getChannel();
    ap localap = k.e();
    e locale = getCallOptions();
    return (t.d)d.a(localf, localap, locale, paramb);
  }
  
  public final v.d a(v.b paramb)
  {
    f localf = getChannel();
    ap localap = k.k();
    e locale = getCallOptions();
    return (v.d)d.a(localf, localap, locale, paramb);
  }
  
  public final x.d a(x.b paramb)
  {
    f localf = getChannel();
    ap localap = k.g();
    e locale = getCallOptions();
    return (x.d)d.a(localf, localap, locale, paramb);
  }
  
  public final z.d a(z.b paramb)
  {
    f localf = getChannel();
    ap localap = k.n();
    e locale = getCallOptions();
    return (z.d)d.a(localf, localap, locale, paramb);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.k.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */