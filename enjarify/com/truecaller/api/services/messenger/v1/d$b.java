package com.truecaller.api.services.messenger.v1;

import com.google.f.ae;
import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;
import com.google.f.w.h;
import com.truecaller.api.services.messenger.v1.models.input.InputGroupPrivacySettings;
import com.truecaller.api.services.messenger.v1.models.input.a;
import com.truecaller.api.services.messenger.v1.models.input.c;

public final class d$b
  extends q
  implements d.c
{
  private static final b g;
  private static volatile ah h;
  private int a;
  private long b;
  private InputGroupPrivacySettings c;
  private c d;
  private w.h e;
  private a f;
  
  static
  {
    b localb = new com/truecaller/api/services/messenger/v1/d$b;
    localb.<init>();
    g = localb;
    localb.makeImmutable();
  }
  
  private d$b()
  {
    w.h localh = emptyProtobufList();
    e = localh;
  }
  
  public static d.b.a a()
  {
    return (d.b.a)g.toBuilder();
  }
  
  public static b b()
  {
    return g;
  }
  
  private InputGroupPrivacySettings d()
  {
    InputGroupPrivacySettings localInputGroupPrivacySettings = c;
    if (localInputGroupPrivacySettings == null) {
      localInputGroupPrivacySettings = InputGroupPrivacySettings.a();
    }
    return localInputGroupPrivacySettings;
  }
  
  private c e()
  {
    c localc = d;
    if (localc == null) {
      localc = c.a();
    }
    return localc;
  }
  
  private a f()
  {
    a locala = f;
    if (locala == null) {
      locala = a.b();
    }
    return locala;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 94	com/truecaller/api/services/messenger/v1/d$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 100	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_0
    //   19: istore 6
    //   21: iload 5
    //   23: tableswitch	default:+45->68, 1:+921->944, 2:+917->940, 3:+906->929, 4:+895->918, 5:+634->657, 6:+107->130, 7:+630->653, 8:+55->78
    //   68: new 103	java/lang/UnsupportedOperationException
    //   71: astore_1
    //   72: aload_1
    //   73: invokespecial 104	java/lang/UnsupportedOperationException:<init>	()V
    //   76: aload_1
    //   77: athrow
    //   78: getstatic 106	com/truecaller/api/services/messenger/v1/d$b:h	Lcom/google/f/ah;
    //   81: astore_1
    //   82: aload_1
    //   83: ifnonnull +43 -> 126
    //   86: ldc 2
    //   88: astore_1
    //   89: aload_1
    //   90: monitorenter
    //   91: getstatic 106	com/truecaller/api/services/messenger/v1/d$b:h	Lcom/google/f/ah;
    //   94: astore_2
    //   95: aload_2
    //   96: ifnonnull +20 -> 116
    //   99: new 108	com/google/f/q$b
    //   102: astore_2
    //   103: getstatic 28	com/truecaller/api/services/messenger/v1/d$b:g	Lcom/truecaller/api/services/messenger/v1/d$b;
    //   106: astore_3
    //   107: aload_2
    //   108: aload_3
    //   109: invokespecial 111	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   112: aload_2
    //   113: putstatic 106	com/truecaller/api/services/messenger/v1/d$b:h	Lcom/google/f/ah;
    //   116: aload_1
    //   117: monitorexit
    //   118: goto +8 -> 126
    //   121: astore_2
    //   122: aload_1
    //   123: monitorexit
    //   124: aload_2
    //   125: athrow
    //   126: getstatic 106	com/truecaller/api/services/messenger/v1/d$b:h	Lcom/google/f/ah;
    //   129: areturn
    //   130: aload_2
    //   131: checkcast 113	com/google/f/g
    //   134: astore_2
    //   135: aload_3
    //   136: checkcast 115	com/google/f/n
    //   139: astore_3
    //   140: iload 6
    //   142: ifne +511 -> 653
    //   145: aload_2
    //   146: invokevirtual 118	com/google/f/g:readTag	()I
    //   149: istore 5
    //   151: iload 5
    //   153: ifeq +438 -> 591
    //   156: bipush 8
    //   158: istore 7
    //   160: iload 5
    //   162: iload 7
    //   164: if_icmpeq +412 -> 576
    //   167: bipush 18
    //   169: istore 7
    //   171: iload 5
    //   173: iload 7
    //   175: if_icmpeq +307 -> 482
    //   178: bipush 26
    //   180: istore 7
    //   182: iload 5
    //   184: iload 7
    //   186: if_icmpeq +202 -> 388
    //   189: bipush 34
    //   191: istore 7
    //   193: iload 5
    //   195: iload 7
    //   197: if_icmpeq +127 -> 324
    //   200: bipush 42
    //   202: istore 7
    //   204: iload 5
    //   206: iload 7
    //   208: if_icmpeq +22 -> 230
    //   211: aload_2
    //   212: iload 5
    //   214: invokevirtual 127	com/google/f/g:skipField	(I)Z
    //   217: istore 5
    //   219: iload 5
    //   221: ifne -81 -> 140
    //   224: iconst_1
    //   225: istore 6
    //   227: goto -87 -> 140
    //   230: aload_0
    //   231: getfield 56	com/truecaller/api/services/messenger/v1/d$b:f	Lcom/truecaller/api/services/messenger/v1/models/input/a;
    //   234: astore_1
    //   235: aload_1
    //   236: ifnull +21 -> 257
    //   239: aload_0
    //   240: getfield 56	com/truecaller/api/services/messenger/v1/d$b:f	Lcom/truecaller/api/services/messenger/v1/models/input/a;
    //   243: astore_1
    //   244: aload_1
    //   245: invokevirtual 128	com/truecaller/api/services/messenger/v1/models/input/a:toBuilder	()Lcom/google/f/q$a;
    //   248: astore_1
    //   249: aload_1
    //   250: checkcast 48	com/truecaller/api/services/messenger/v1/models/input/a$a
    //   253: astore_1
    //   254: goto +8 -> 262
    //   257: iconst_0
    //   258: istore 5
    //   260: aconst_null
    //   261: astore_1
    //   262: invokestatic 131	com/truecaller/api/services/messenger/v1/models/input/a:c	()Lcom/google/f/ah;
    //   265: astore 8
    //   267: aload_2
    //   268: aload 8
    //   270: aload_3
    //   271: invokevirtual 135	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   274: astore 8
    //   276: aload 8
    //   278: checkcast 54	com/truecaller/api/services/messenger/v1/models/input/a
    //   281: astore 8
    //   283: aload_0
    //   284: aload 8
    //   286: putfield 56	com/truecaller/api/services/messenger/v1/d$b:f	Lcom/truecaller/api/services/messenger/v1/models/input/a;
    //   289: aload_1
    //   290: ifnull -150 -> 140
    //   293: aload_0
    //   294: getfield 56	com/truecaller/api/services/messenger/v1/d$b:f	Lcom/truecaller/api/services/messenger/v1/models/input/a;
    //   297: astore 8
    //   299: aload_1
    //   300: aload 8
    //   302: invokevirtual 139	com/truecaller/api/services/messenger/v1/models/input/a$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   305: pop
    //   306: aload_1
    //   307: invokevirtual 142	com/truecaller/api/services/messenger/v1/models/input/a$a:buildPartial	()Lcom/google/f/q;
    //   310: astore_1
    //   311: aload_1
    //   312: checkcast 54	com/truecaller/api/services/messenger/v1/models/input/a
    //   315: astore_1
    //   316: aload_0
    //   317: aload_1
    //   318: putfield 56	com/truecaller/api/services/messenger/v1/d$b:f	Lcom/truecaller/api/services/messenger/v1/models/input/a;
    //   321: goto -181 -> 140
    //   324: aload_0
    //   325: getfield 38	com/truecaller/api/services/messenger/v1/d$b:e	Lcom/google/f/w$h;
    //   328: astore_1
    //   329: aload_1
    //   330: invokeinterface 62 1 0
    //   335: istore 5
    //   337: iload 5
    //   339: ifne +18 -> 357
    //   342: aload_0
    //   343: getfield 38	com/truecaller/api/services/messenger/v1/d$b:e	Lcom/google/f/w$h;
    //   346: astore_1
    //   347: aload_1
    //   348: invokestatic 66	com/google/f/q:mutableCopy	(Lcom/google/f/w$h;)Lcom/google/f/w$h;
    //   351: astore_1
    //   352: aload_0
    //   353: aload_1
    //   354: putfield 38	com/truecaller/api/services/messenger/v1/d$b:e	Lcom/google/f/w$h;
    //   357: aload_0
    //   358: getfield 38	com/truecaller/api/services/messenger/v1/d$b:e	Lcom/google/f/w$h;
    //   361: astore_1
    //   362: invokestatic 145	com/truecaller/api/services/messenger/v1/models/input/InputPeer:c	()Lcom/google/f/ah;
    //   365: astore 8
    //   367: aload_2
    //   368: aload 8
    //   370: aload_3
    //   371: invokevirtual 135	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   374: astore 8
    //   376: aload_1
    //   377: aload 8
    //   379: invokeinterface 149 2 0
    //   384: pop
    //   385: goto -245 -> 140
    //   388: aload_0
    //   389: getfield 81	com/truecaller/api/services/messenger/v1/d$b:d	Lcom/truecaller/api/services/messenger/v1/models/input/c;
    //   392: astore_1
    //   393: aload_1
    //   394: ifnull +21 -> 415
    //   397: aload_0
    //   398: getfield 81	com/truecaller/api/services/messenger/v1/d$b:d	Lcom/truecaller/api/services/messenger/v1/models/input/c;
    //   401: astore_1
    //   402: aload_1
    //   403: invokevirtual 150	com/truecaller/api/services/messenger/v1/models/input/c:toBuilder	()Lcom/google/f/q$a;
    //   406: astore_1
    //   407: aload_1
    //   408: checkcast 152	com/truecaller/api/services/messenger/v1/models/input/c$a
    //   411: astore_1
    //   412: goto +8 -> 420
    //   415: iconst_0
    //   416: istore 5
    //   418: aconst_null
    //   419: astore_1
    //   420: invokestatic 154	com/truecaller/api/services/messenger/v1/models/input/c:b	()Lcom/google/f/ah;
    //   423: astore 8
    //   425: aload_2
    //   426: aload 8
    //   428: aload_3
    //   429: invokevirtual 135	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   432: astore 8
    //   434: aload 8
    //   436: checkcast 83	com/truecaller/api/services/messenger/v1/models/input/c
    //   439: astore 8
    //   441: aload_0
    //   442: aload 8
    //   444: putfield 81	com/truecaller/api/services/messenger/v1/d$b:d	Lcom/truecaller/api/services/messenger/v1/models/input/c;
    //   447: aload_1
    //   448: ifnull -308 -> 140
    //   451: aload_0
    //   452: getfield 81	com/truecaller/api/services/messenger/v1/d$b:d	Lcom/truecaller/api/services/messenger/v1/models/input/c;
    //   455: astore 8
    //   457: aload_1
    //   458: aload 8
    //   460: invokevirtual 155	com/truecaller/api/services/messenger/v1/models/input/c$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   463: pop
    //   464: aload_1
    //   465: invokevirtual 156	com/truecaller/api/services/messenger/v1/models/input/c$a:buildPartial	()Lcom/google/f/q;
    //   468: astore_1
    //   469: aload_1
    //   470: checkcast 83	com/truecaller/api/services/messenger/v1/models/input/c
    //   473: astore_1
    //   474: aload_0
    //   475: aload_1
    //   476: putfield 81	com/truecaller/api/services/messenger/v1/d$b:d	Lcom/truecaller/api/services/messenger/v1/models/input/c;
    //   479: goto -339 -> 140
    //   482: aload_0
    //   483: getfield 74	com/truecaller/api/services/messenger/v1/d$b:c	Lcom/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings;
    //   486: astore_1
    //   487: aload_1
    //   488: ifnull +21 -> 509
    //   491: aload_0
    //   492: getfield 74	com/truecaller/api/services/messenger/v1/d$b:c	Lcom/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings;
    //   495: astore_1
    //   496: aload_1
    //   497: invokevirtual 157	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings:toBuilder	()Lcom/google/f/q$a;
    //   500: astore_1
    //   501: aload_1
    //   502: checkcast 159	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings$a
    //   505: astore_1
    //   506: goto +8 -> 514
    //   509: iconst_0
    //   510: istore 5
    //   512: aconst_null
    //   513: astore_1
    //   514: invokestatic 160	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings:b	()Lcom/google/f/ah;
    //   517: astore 8
    //   519: aload_2
    //   520: aload 8
    //   522: aload_3
    //   523: invokevirtual 135	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   526: astore 8
    //   528: aload 8
    //   530: checkcast 76	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings
    //   533: astore 8
    //   535: aload_0
    //   536: aload 8
    //   538: putfield 74	com/truecaller/api/services/messenger/v1/d$b:c	Lcom/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings;
    //   541: aload_1
    //   542: ifnull -402 -> 140
    //   545: aload_0
    //   546: getfield 74	com/truecaller/api/services/messenger/v1/d$b:c	Lcom/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings;
    //   549: astore 8
    //   551: aload_1
    //   552: aload 8
    //   554: invokevirtual 161	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   557: pop
    //   558: aload_1
    //   559: invokevirtual 162	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings$a:buildPartial	()Lcom/google/f/q;
    //   562: astore_1
    //   563: aload_1
    //   564: checkcast 76	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings
    //   567: astore_1
    //   568: aload_0
    //   569: aload_1
    //   570: putfield 74	com/truecaller/api/services/messenger/v1/d$b:c	Lcom/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings;
    //   573: goto -433 -> 140
    //   576: aload_2
    //   577: invokevirtual 166	com/google/f/g:readInt64	()J
    //   580: lstore 9
    //   582: aload_0
    //   583: lload 9
    //   585: putfield 46	com/truecaller/api/services/messenger/v1/d$b:b	J
    //   588: goto -448 -> 140
    //   591: iconst_1
    //   592: istore 6
    //   594: goto -454 -> 140
    //   597: astore_1
    //   598: goto +53 -> 651
    //   601: astore_1
    //   602: new 168	java/lang/RuntimeException
    //   605: astore_2
    //   606: new 170	com/google/f/x
    //   609: astore_3
    //   610: aload_1
    //   611: invokevirtual 176	java/io/IOException:getMessage	()Ljava/lang/String;
    //   614: astore_1
    //   615: aload_3
    //   616: aload_1
    //   617: invokespecial 179	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   620: aload_3
    //   621: aload_0
    //   622: invokevirtual 183	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   625: astore_1
    //   626: aload_2
    //   627: aload_1
    //   628: invokespecial 186	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   631: aload_2
    //   632: athrow
    //   633: astore_1
    //   634: new 168	java/lang/RuntimeException
    //   637: astore_2
    //   638: aload_1
    //   639: aload_0
    //   640: invokevirtual 183	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   643: astore_1
    //   644: aload_2
    //   645: aload_1
    //   646: invokespecial 186	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   649: aload_2
    //   650: athrow
    //   651: aload_1
    //   652: athrow
    //   653: getstatic 28	com/truecaller/api/services/messenger/v1/d$b:g	Lcom/truecaller/api/services/messenger/v1/d$b;
    //   656: areturn
    //   657: aload_2
    //   658: astore_1
    //   659: aload_2
    //   660: checkcast 188	com/google/f/q$k
    //   663: astore_1
    //   664: aload_3
    //   665: checkcast 2	com/truecaller/api/services/messenger/v1/d$b
    //   668: astore_3
    //   669: aload_0
    //   670: getfield 46	com/truecaller/api/services/messenger/v1/d$b:b	J
    //   673: lstore 11
    //   675: lconst_0
    //   676: lstore 13
    //   678: lload 11
    //   680: lload 13
    //   682: lcmp
    //   683: istore 15
    //   685: iload 15
    //   687: ifeq +9 -> 696
    //   690: iconst_1
    //   691: istore 15
    //   693: goto +8 -> 701
    //   696: iconst_0
    //   697: istore 15
    //   699: aconst_null
    //   700: astore_2
    //   701: aload_0
    //   702: getfield 46	com/truecaller/api/services/messenger/v1/d$b:b	J
    //   705: lstore 11
    //   707: aload_3
    //   708: getfield 46	com/truecaller/api/services/messenger/v1/d$b:b	J
    //   711: lstore 16
    //   713: lload 16
    //   715: lload 13
    //   717: lcmp
    //   718: istore 18
    //   720: iload 18
    //   722: ifeq +9 -> 731
    //   725: iconst_1
    //   726: istore 19
    //   728: goto +6 -> 734
    //   731: iconst_0
    //   732: istore 19
    //   734: aload_3
    //   735: getfield 46	com/truecaller/api/services/messenger/v1/d$b:b	J
    //   738: lstore 20
    //   740: aload_1
    //   741: astore 4
    //   743: iload 15
    //   745: istore 6
    //   747: aload_1
    //   748: iload 15
    //   750: lload 11
    //   752: iload 19
    //   754: lload 20
    //   756: invokeinterface 192 7 0
    //   761: lstore 22
    //   763: aload_0
    //   764: lload 22
    //   766: putfield 46	com/truecaller/api/services/messenger/v1/d$b:b	J
    //   769: aload_0
    //   770: getfield 74	com/truecaller/api/services/messenger/v1/d$b:c	Lcom/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings;
    //   773: astore_2
    //   774: aload_3
    //   775: getfield 74	com/truecaller/api/services/messenger/v1/d$b:c	Lcom/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings;
    //   778: astore 4
    //   780: aload_1
    //   781: aload_2
    //   782: aload 4
    //   784: invokeinterface 196 3 0
    //   789: checkcast 76	com/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings
    //   792: astore_2
    //   793: aload_0
    //   794: aload_2
    //   795: putfield 74	com/truecaller/api/services/messenger/v1/d$b:c	Lcom/truecaller/api/services/messenger/v1/models/input/InputGroupPrivacySettings;
    //   798: aload_0
    //   799: getfield 81	com/truecaller/api/services/messenger/v1/d$b:d	Lcom/truecaller/api/services/messenger/v1/models/input/c;
    //   802: astore_2
    //   803: aload_3
    //   804: getfield 81	com/truecaller/api/services/messenger/v1/d$b:d	Lcom/truecaller/api/services/messenger/v1/models/input/c;
    //   807: astore 4
    //   809: aload_1
    //   810: aload_2
    //   811: aload 4
    //   813: invokeinterface 196 3 0
    //   818: checkcast 83	com/truecaller/api/services/messenger/v1/models/input/c
    //   821: astore_2
    //   822: aload_0
    //   823: aload_2
    //   824: putfield 81	com/truecaller/api/services/messenger/v1/d$b:d	Lcom/truecaller/api/services/messenger/v1/models/input/c;
    //   827: aload_0
    //   828: getfield 38	com/truecaller/api/services/messenger/v1/d$b:e	Lcom/google/f/w$h;
    //   831: astore_2
    //   832: aload_3
    //   833: getfield 38	com/truecaller/api/services/messenger/v1/d$b:e	Lcom/google/f/w$h;
    //   836: astore 4
    //   838: aload_1
    //   839: aload_2
    //   840: aload 4
    //   842: invokeinterface 200 3 0
    //   847: astore_2
    //   848: aload_0
    //   849: aload_2
    //   850: putfield 38	com/truecaller/api/services/messenger/v1/d$b:e	Lcom/google/f/w$h;
    //   853: aload_0
    //   854: getfield 56	com/truecaller/api/services/messenger/v1/d$b:f	Lcom/truecaller/api/services/messenger/v1/models/input/a;
    //   857: astore_2
    //   858: aload_3
    //   859: getfield 56	com/truecaller/api/services/messenger/v1/d$b:f	Lcom/truecaller/api/services/messenger/v1/models/input/a;
    //   862: astore 4
    //   864: aload_1
    //   865: aload_2
    //   866: aload 4
    //   868: invokeinterface 196 3 0
    //   873: checkcast 54	com/truecaller/api/services/messenger/v1/models/input/a
    //   876: astore_2
    //   877: aload_0
    //   878: aload_2
    //   879: putfield 56	com/truecaller/api/services/messenger/v1/d$b:f	Lcom/truecaller/api/services/messenger/v1/models/input/a;
    //   882: getstatic 206	com/google/f/q$i:INSTANCE	Lcom/google/f/q$i;
    //   885: astore_2
    //   886: aload_1
    //   887: aload_2
    //   888: if_acmpne +28 -> 916
    //   891: aload_0
    //   892: getfield 208	com/truecaller/api/services/messenger/v1/d$b:a	I
    //   895: istore 5
    //   897: aload_3
    //   898: getfield 208	com/truecaller/api/services/messenger/v1/d$b:a	I
    //   901: istore 15
    //   903: iload 5
    //   905: iload 15
    //   907: ior
    //   908: istore 5
    //   910: aload_0
    //   911: iload 5
    //   913: putfield 208	com/truecaller/api/services/messenger/v1/d$b:a	I
    //   916: aload_0
    //   917: areturn
    //   918: new 44	com/truecaller/api/services/messenger/v1/d$b$a
    //   921: astore_1
    //   922: aload_1
    //   923: iconst_0
    //   924: invokespecial 211	com/truecaller/api/services/messenger/v1/d$b$a:<init>	(B)V
    //   927: aload_1
    //   928: areturn
    //   929: aload_0
    //   930: getfield 38	com/truecaller/api/services/messenger/v1/d$b:e	Lcom/google/f/w$h;
    //   933: invokeinterface 212 1 0
    //   938: aconst_null
    //   939: areturn
    //   940: getstatic 28	com/truecaller/api/services/messenger/v1/d$b:g	Lcom/truecaller/api/services/messenger/v1/d$b;
    //   943: areturn
    //   944: new 2	com/truecaller/api/services/messenger/v1/d$b
    //   947: astore_1
    //   948: aload_1
    //   949: invokespecial 26	com/truecaller/api/services/messenger/v1/d$b:<init>	()V
    //   952: aload_1
    //   953: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	954	0	this	b
    //   0	954	1	paramj	com.google.f.q.j
    //   0	954	2	paramObject1	Object
    //   0	954	3	paramObject2	Object
    //   3	864	4	localObject1	Object
    //   9	204	5	i	int
    //   217	294	5	bool1	boolean
    //   895	17	5	j	int
    //   19	727	6	k	int
    //   158	51	7	m	int
    //   265	288	8	localObject2	Object
    //   580	4	9	l1	long
    //   673	78	11	l2	long
    //   676	40	13	l3	long
    //   683	66	15	bool2	boolean
    //   901	7	15	n	int
    //   711	3	16	l4	long
    //   718	3	18	bool3	boolean
    //   726	27	19	bool4	boolean
    //   738	17	20	l5	long
    //   761	4	22	l6	long
    // Exception table:
    //   from	to	target	type
    //   91	94	121	finally
    //   99	102	121	finally
    //   103	106	121	finally
    //   108	112	121	finally
    //   112	116	121	finally
    //   116	118	121	finally
    //   122	124	121	finally
    //   145	149	597	finally
    //   212	217	597	finally
    //   230	234	597	finally
    //   239	243	597	finally
    //   244	248	597	finally
    //   249	253	597	finally
    //   262	265	597	finally
    //   270	274	597	finally
    //   276	281	597	finally
    //   284	289	597	finally
    //   293	297	597	finally
    //   300	306	597	finally
    //   306	310	597	finally
    //   311	315	597	finally
    //   317	321	597	finally
    //   324	328	597	finally
    //   329	335	597	finally
    //   342	346	597	finally
    //   347	351	597	finally
    //   353	357	597	finally
    //   357	361	597	finally
    //   362	365	597	finally
    //   370	374	597	finally
    //   377	385	597	finally
    //   388	392	597	finally
    //   397	401	597	finally
    //   402	406	597	finally
    //   407	411	597	finally
    //   420	423	597	finally
    //   428	432	597	finally
    //   434	439	597	finally
    //   442	447	597	finally
    //   451	455	597	finally
    //   458	464	597	finally
    //   464	468	597	finally
    //   469	473	597	finally
    //   475	479	597	finally
    //   482	486	597	finally
    //   491	495	597	finally
    //   496	500	597	finally
    //   501	505	597	finally
    //   514	517	597	finally
    //   522	526	597	finally
    //   528	533	597	finally
    //   536	541	597	finally
    //   545	549	597	finally
    //   552	558	597	finally
    //   558	562	597	finally
    //   563	567	597	finally
    //   569	573	597	finally
    //   576	580	597	finally
    //   583	588	597	finally
    //   602	605	597	finally
    //   606	609	597	finally
    //   610	614	597	finally
    //   616	620	597	finally
    //   621	625	597	finally
    //   627	631	597	finally
    //   631	633	597	finally
    //   634	637	597	finally
    //   639	643	597	finally
    //   645	649	597	finally
    //   649	651	597	finally
    //   145	149	601	java/io/IOException
    //   212	217	601	java/io/IOException
    //   230	234	601	java/io/IOException
    //   239	243	601	java/io/IOException
    //   244	248	601	java/io/IOException
    //   249	253	601	java/io/IOException
    //   262	265	601	java/io/IOException
    //   270	274	601	java/io/IOException
    //   276	281	601	java/io/IOException
    //   284	289	601	java/io/IOException
    //   293	297	601	java/io/IOException
    //   300	306	601	java/io/IOException
    //   306	310	601	java/io/IOException
    //   311	315	601	java/io/IOException
    //   317	321	601	java/io/IOException
    //   324	328	601	java/io/IOException
    //   329	335	601	java/io/IOException
    //   342	346	601	java/io/IOException
    //   347	351	601	java/io/IOException
    //   353	357	601	java/io/IOException
    //   357	361	601	java/io/IOException
    //   362	365	601	java/io/IOException
    //   370	374	601	java/io/IOException
    //   377	385	601	java/io/IOException
    //   388	392	601	java/io/IOException
    //   397	401	601	java/io/IOException
    //   402	406	601	java/io/IOException
    //   407	411	601	java/io/IOException
    //   420	423	601	java/io/IOException
    //   428	432	601	java/io/IOException
    //   434	439	601	java/io/IOException
    //   442	447	601	java/io/IOException
    //   451	455	601	java/io/IOException
    //   458	464	601	java/io/IOException
    //   464	468	601	java/io/IOException
    //   469	473	601	java/io/IOException
    //   475	479	601	java/io/IOException
    //   482	486	601	java/io/IOException
    //   491	495	601	java/io/IOException
    //   496	500	601	java/io/IOException
    //   501	505	601	java/io/IOException
    //   514	517	601	java/io/IOException
    //   522	526	601	java/io/IOException
    //   528	533	601	java/io/IOException
    //   536	541	601	java/io/IOException
    //   545	549	601	java/io/IOException
    //   552	558	601	java/io/IOException
    //   558	562	601	java/io/IOException
    //   563	567	601	java/io/IOException
    //   569	573	601	java/io/IOException
    //   576	580	601	java/io/IOException
    //   583	588	601	java/io/IOException
    //   145	149	633	com/google/f/x
    //   212	217	633	com/google/f/x
    //   230	234	633	com/google/f/x
    //   239	243	633	com/google/f/x
    //   244	248	633	com/google/f/x
    //   249	253	633	com/google/f/x
    //   262	265	633	com/google/f/x
    //   270	274	633	com/google/f/x
    //   276	281	633	com/google/f/x
    //   284	289	633	com/google/f/x
    //   293	297	633	com/google/f/x
    //   300	306	633	com/google/f/x
    //   306	310	633	com/google/f/x
    //   311	315	633	com/google/f/x
    //   317	321	633	com/google/f/x
    //   324	328	633	com/google/f/x
    //   329	335	633	com/google/f/x
    //   342	346	633	com/google/f/x
    //   347	351	633	com/google/f/x
    //   353	357	633	com/google/f/x
    //   357	361	633	com/google/f/x
    //   362	365	633	com/google/f/x
    //   370	374	633	com/google/f/x
    //   377	385	633	com/google/f/x
    //   388	392	633	com/google/f/x
    //   397	401	633	com/google/f/x
    //   402	406	633	com/google/f/x
    //   407	411	633	com/google/f/x
    //   420	423	633	com/google/f/x
    //   428	432	633	com/google/f/x
    //   434	439	633	com/google/f/x
    //   442	447	633	com/google/f/x
    //   451	455	633	com/google/f/x
    //   458	464	633	com/google/f/x
    //   464	468	633	com/google/f/x
    //   469	473	633	com/google/f/x
    //   475	479	633	com/google/f/x
    //   482	486	633	com/google/f/x
    //   491	495	633	com/google/f/x
    //   496	500	633	com/google/f/x
    //   501	505	633	com/google/f/x
    //   514	517	633	com/google/f/x
    //   522	526	633	com/google/f/x
    //   528	533	633	com/google/f/x
    //   536	541	633	com/google/f/x
    //   545	549	633	com/google/f/x
    //   552	558	633	com/google/f/x
    //   558	562	633	com/google/f/x
    //   563	567	633	com/google/f/x
    //   569	573	633	com/google/f/x
    //   576	580	633	com/google/f/x
    //   583	588	633	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int j = -1;
    if (i != j) {
      return i;
    }
    long l1 = b;
    long l2 = 0L;
    int k = 0;
    boolean bool = l1 < l2;
    if (bool)
    {
      int m = 1;
      i = h.computeInt64Size(m, l1) + 0;
    }
    else
    {
      i = 0;
    }
    Object localObject1 = c;
    Object localObject2;
    if (localObject1 != null)
    {
      localObject2 = d();
      j = h.computeMessageSize(2, (ae)localObject2);
      i += j;
    }
    localObject1 = d;
    if (localObject1 != null)
    {
      localObject2 = e();
      j = h.computeMessageSize(3, (ae)localObject2);
      i += j;
    }
    for (;;)
    {
      localObject1 = e;
      j = ((w.h)localObject1).size();
      if (k >= j) {
        break;
      }
      localObject2 = (ae)e.get(k);
      j = h.computeMessageSize(4, (ae)localObject2);
      i += j;
      k += 1;
    }
    localObject1 = f;
    if (localObject1 != null)
    {
      localObject2 = f();
      j = h.computeMessageSize(5, (ae)localObject2);
      i += j;
    }
    memoizedSerializedSize = i;
    return i;
  }
  
  public final void writeTo(h paramh)
  {
    long l1 = b;
    long l2 = 0L;
    boolean bool = l1 < l2;
    if (bool)
    {
      int i = 1;
      paramh.writeInt64(i, l1);
    }
    Object localObject1 = c;
    Object localObject2;
    if (localObject1 != null)
    {
      j = 2;
      localObject2 = d();
      paramh.writeMessage(j, (ae)localObject2);
    }
    localObject1 = d;
    if (localObject1 != null)
    {
      j = 3;
      localObject2 = e();
      paramh.writeMessage(j, (ae)localObject2);
    }
    int j = 0;
    localObject1 = null;
    for (;;)
    {
      localObject2 = e;
      int k = ((w.h)localObject2).size();
      if (j >= k) {
        break;
      }
      k = 4;
      ae localae = (ae)e.get(j);
      paramh.writeMessage(k, localae);
      j += 1;
    }
    localObject1 = f;
    if (localObject1 != null)
    {
      j = 5;
      localObject2 = f();
      paramh.writeMessage(j, (ae)localObject2);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.d.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */