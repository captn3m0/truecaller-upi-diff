package com.truecaller.api.services.messenger.v1;

import com.google.f.ae;
import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;
import com.truecaller.api.services.messenger.v1.models.input.a;

public final class v$b
  extends q
  implements v.c
{
  private static final b d;
  private static volatile ah e;
  private long a;
  private String b = "";
  private a c;
  
  static
  {
    b localb = new com/truecaller/api/services/messenger/v1/v$b;
    localb.<init>();
    d = localb;
    localb.makeImmutable();
  }
  
  public static v.b.a a()
  {
    return (v.b.a)d.toBuilder();
  }
  
  public static b b()
  {
    return d;
  }
  
  private a d()
  {
    a locala = c;
    if (locala == null) {
      locala = a.b();
    }
    return locala;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 59	com/truecaller/api/services/messenger/v1/v$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 65	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: aconst_null
    //   19: astore 4
    //   21: iconst_0
    //   22: istore 6
    //   24: iconst_1
    //   25: istore 7
    //   27: iload 5
    //   29: tableswitch	default:+47->76, 1:+581->610, 2:+577->606, 3:+575->604, 4:+564->593, 5:+375->404, 6:+109->138, 7:+371->400, 8:+57->86
    //   76: new 68	java/lang/UnsupportedOperationException
    //   79: astore_1
    //   80: aload_1
    //   81: invokespecial 69	java/lang/UnsupportedOperationException:<init>	()V
    //   84: aload_1
    //   85: athrow
    //   86: getstatic 71	com/truecaller/api/services/messenger/v1/v$b:e	Lcom/google/f/ah;
    //   89: astore_1
    //   90: aload_1
    //   91: ifnonnull +43 -> 134
    //   94: ldc 2
    //   96: astore_1
    //   97: aload_1
    //   98: monitorenter
    //   99: getstatic 71	com/truecaller/api/services/messenger/v1/v$b:e	Lcom/google/f/ah;
    //   102: astore_2
    //   103: aload_2
    //   104: ifnonnull +20 -> 124
    //   107: new 73	com/google/f/q$b
    //   110: astore_2
    //   111: getstatic 22	com/truecaller/api/services/messenger/v1/v$b:d	Lcom/truecaller/api/services/messenger/v1/v$b;
    //   114: astore_3
    //   115: aload_2
    //   116: aload_3
    //   117: invokespecial 76	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   120: aload_2
    //   121: putstatic 71	com/truecaller/api/services/messenger/v1/v$b:e	Lcom/google/f/ah;
    //   124: aload_1
    //   125: monitorexit
    //   126: goto +8 -> 134
    //   129: astore_2
    //   130: aload_1
    //   131: monitorexit
    //   132: aload_2
    //   133: athrow
    //   134: getstatic 71	com/truecaller/api/services/messenger/v1/v$b:e	Lcom/google/f/ah;
    //   137: areturn
    //   138: aload_2
    //   139: checkcast 78	com/google/f/g
    //   142: astore_2
    //   143: aload_3
    //   144: checkcast 80	com/google/f/n
    //   147: astore_3
    //   148: iload 6
    //   150: ifne +250 -> 400
    //   153: aload_2
    //   154: invokevirtual 83	com/google/f/g:readTag	()I
    //   157: istore 5
    //   159: iload 5
    //   161: ifeq +177 -> 338
    //   164: bipush 8
    //   166: istore 8
    //   168: iload 5
    //   170: iload 8
    //   172: if_icmpeq +151 -> 323
    //   175: bipush 18
    //   177: istore 8
    //   179: iload 5
    //   181: iload 8
    //   183: if_icmpeq +127 -> 310
    //   186: bipush 26
    //   188: istore 8
    //   190: iload 5
    //   192: iload 8
    //   194: if_icmpeq +22 -> 216
    //   197: aload_2
    //   198: iload 5
    //   200: invokevirtual 90	com/google/f/g:skipField	(I)Z
    //   203: istore 5
    //   205: iload 5
    //   207: ifne -59 -> 148
    //   210: iconst_1
    //   211: istore 6
    //   213: goto -65 -> 148
    //   216: aload_0
    //   217: getfield 48	com/truecaller/api/services/messenger/v1/v$b:c	Lcom/truecaller/api/services/messenger/v1/models/input/a;
    //   220: astore_1
    //   221: aload_1
    //   222: ifnull +21 -> 243
    //   225: aload_0
    //   226: getfield 48	com/truecaller/api/services/messenger/v1/v$b:c	Lcom/truecaller/api/services/messenger/v1/models/input/a;
    //   229: astore_1
    //   230: aload_1
    //   231: invokevirtual 91	com/truecaller/api/services/messenger/v1/models/input/a:toBuilder	()Lcom/google/f/q$a;
    //   234: astore_1
    //   235: aload_1
    //   236: checkcast 40	com/truecaller/api/services/messenger/v1/models/input/a$a
    //   239: astore_1
    //   240: goto +8 -> 248
    //   243: iconst_0
    //   244: istore 5
    //   246: aconst_null
    //   247: astore_1
    //   248: invokestatic 94	com/truecaller/api/services/messenger/v1/models/input/a:c	()Lcom/google/f/ah;
    //   251: astore 9
    //   253: aload_2
    //   254: aload 9
    //   256: aload_3
    //   257: invokevirtual 98	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   260: astore 9
    //   262: aload 9
    //   264: checkcast 46	com/truecaller/api/services/messenger/v1/models/input/a
    //   267: astore 9
    //   269: aload_0
    //   270: aload 9
    //   272: putfield 48	com/truecaller/api/services/messenger/v1/v$b:c	Lcom/truecaller/api/services/messenger/v1/models/input/a;
    //   275: aload_1
    //   276: ifnull -128 -> 148
    //   279: aload_0
    //   280: getfield 48	com/truecaller/api/services/messenger/v1/v$b:c	Lcom/truecaller/api/services/messenger/v1/models/input/a;
    //   283: astore 9
    //   285: aload_1
    //   286: aload 9
    //   288: invokevirtual 102	com/truecaller/api/services/messenger/v1/models/input/a$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   291: pop
    //   292: aload_1
    //   293: invokevirtual 105	com/truecaller/api/services/messenger/v1/models/input/a$a:buildPartial	()Lcom/google/f/q;
    //   296: astore_1
    //   297: aload_1
    //   298: checkcast 46	com/truecaller/api/services/messenger/v1/models/input/a
    //   301: astore_1
    //   302: aload_0
    //   303: aload_1
    //   304: putfield 48	com/truecaller/api/services/messenger/v1/v$b:c	Lcom/truecaller/api/services/messenger/v1/models/input/a;
    //   307: goto -159 -> 148
    //   310: aload_2
    //   311: invokevirtual 109	com/google/f/g:readStringRequireUtf8	()Ljava/lang/String;
    //   314: astore_1
    //   315: aload_0
    //   316: aload_1
    //   317: putfield 30	com/truecaller/api/services/messenger/v1/v$b:b	Ljava/lang/String;
    //   320: goto -172 -> 148
    //   323: aload_2
    //   324: invokevirtual 113	com/google/f/g:readInt64	()J
    //   327: lstore 10
    //   329: aload_0
    //   330: lload 10
    //   332: putfield 38	com/truecaller/api/services/messenger/v1/v$b:a	J
    //   335: goto -187 -> 148
    //   338: iconst_1
    //   339: istore 6
    //   341: goto -193 -> 148
    //   344: astore_1
    //   345: goto +53 -> 398
    //   348: astore_1
    //   349: new 115	java/lang/RuntimeException
    //   352: astore_2
    //   353: new 117	com/google/f/x
    //   356: astore_3
    //   357: aload_1
    //   358: invokevirtual 122	java/io/IOException:getMessage	()Ljava/lang/String;
    //   361: astore_1
    //   362: aload_3
    //   363: aload_1
    //   364: invokespecial 125	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   367: aload_3
    //   368: aload_0
    //   369: invokevirtual 129	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   372: astore_1
    //   373: aload_2
    //   374: aload_1
    //   375: invokespecial 132	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   378: aload_2
    //   379: athrow
    //   380: astore_1
    //   381: new 115	java/lang/RuntimeException
    //   384: astore_2
    //   385: aload_1
    //   386: aload_0
    //   387: invokevirtual 129	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   390: astore_1
    //   391: aload_2
    //   392: aload_1
    //   393: invokespecial 132	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   396: aload_2
    //   397: athrow
    //   398: aload_1
    //   399: athrow
    //   400: getstatic 22	com/truecaller/api/services/messenger/v1/v$b:d	Lcom/truecaller/api/services/messenger/v1/v$b;
    //   403: areturn
    //   404: aload_2
    //   405: astore_1
    //   406: aload_2
    //   407: checkcast 134	com/google/f/q$k
    //   410: astore_1
    //   411: aload_3
    //   412: checkcast 2	com/truecaller/api/services/messenger/v1/v$b
    //   415: astore_3
    //   416: aload_0
    //   417: getfield 38	com/truecaller/api/services/messenger/v1/v$b:a	J
    //   420: lstore 10
    //   422: lconst_0
    //   423: lstore 12
    //   425: lload 10
    //   427: lload 12
    //   429: lcmp
    //   430: istore 14
    //   432: iload 14
    //   434: ifeq +9 -> 443
    //   437: iconst_1
    //   438: istore 15
    //   440: goto +6 -> 446
    //   443: iconst_0
    //   444: istore 15
    //   446: aload_0
    //   447: getfield 38	com/truecaller/api/services/messenger/v1/v$b:a	J
    //   450: lstore 16
    //   452: aload_3
    //   453: getfield 38	com/truecaller/api/services/messenger/v1/v$b:a	J
    //   456: lstore 18
    //   458: lload 18
    //   460: lload 12
    //   462: lcmp
    //   463: istore 14
    //   465: iload 14
    //   467: ifeq +6 -> 473
    //   470: iconst_1
    //   471: istore 6
    //   473: aload_3
    //   474: getfield 38	com/truecaller/api/services/messenger/v1/v$b:a	J
    //   477: lstore 18
    //   479: aload_1
    //   480: astore 9
    //   482: lload 16
    //   484: lstore 12
    //   486: aload_1
    //   487: iload 15
    //   489: lload 16
    //   491: iload 6
    //   493: lload 18
    //   495: invokeinterface 138 7 0
    //   500: lstore 20
    //   502: aload_0
    //   503: lload 20
    //   505: putfield 38	com/truecaller/api/services/messenger/v1/v$b:a	J
    //   508: aload_0
    //   509: getfield 30	com/truecaller/api/services/messenger/v1/v$b:b	Ljava/lang/String;
    //   512: invokevirtual 144	java/lang/String:isEmpty	()Z
    //   515: iload 7
    //   517: ixor
    //   518: istore 14
    //   520: aload_0
    //   521: getfield 30	com/truecaller/api/services/messenger/v1/v$b:b	Ljava/lang/String;
    //   524: astore 4
    //   526: aload_3
    //   527: getfield 30	com/truecaller/api/services/messenger/v1/v$b:b	Ljava/lang/String;
    //   530: invokevirtual 144	java/lang/String:isEmpty	()Z
    //   533: iload 7
    //   535: ixor
    //   536: istore 6
    //   538: aload_3
    //   539: getfield 30	com/truecaller/api/services/messenger/v1/v$b:b	Ljava/lang/String;
    //   542: astore 22
    //   544: aload_1
    //   545: iload 14
    //   547: aload 4
    //   549: iload 6
    //   551: aload 22
    //   553: invokeinterface 148 5 0
    //   558: astore_2
    //   559: aload_0
    //   560: aload_2
    //   561: putfield 30	com/truecaller/api/services/messenger/v1/v$b:b	Ljava/lang/String;
    //   564: aload_0
    //   565: getfield 48	com/truecaller/api/services/messenger/v1/v$b:c	Lcom/truecaller/api/services/messenger/v1/models/input/a;
    //   568: astore_2
    //   569: aload_3
    //   570: getfield 48	com/truecaller/api/services/messenger/v1/v$b:c	Lcom/truecaller/api/services/messenger/v1/models/input/a;
    //   573: astore_3
    //   574: aload_1
    //   575: aload_2
    //   576: aload_3
    //   577: invokeinterface 152 3 0
    //   582: checkcast 46	com/truecaller/api/services/messenger/v1/models/input/a
    //   585: astore_1
    //   586: aload_0
    //   587: aload_1
    //   588: putfield 48	com/truecaller/api/services/messenger/v1/v$b:c	Lcom/truecaller/api/services/messenger/v1/models/input/a;
    //   591: aload_0
    //   592: areturn
    //   593: new 36	com/truecaller/api/services/messenger/v1/v$b$a
    //   596: astore_1
    //   597: aload_1
    //   598: iconst_0
    //   599: invokespecial 155	com/truecaller/api/services/messenger/v1/v$b$a:<init>	(B)V
    //   602: aload_1
    //   603: areturn
    //   604: aconst_null
    //   605: areturn
    //   606: getstatic 22	com/truecaller/api/services/messenger/v1/v$b:d	Lcom/truecaller/api/services/messenger/v1/v$b;
    //   609: areturn
    //   610: new 2	com/truecaller/api/services/messenger/v1/v$b
    //   613: astore_1
    //   614: aload_1
    //   615: invokespecial 20	com/truecaller/api/services/messenger/v1/v$b:<init>	()V
    //   618: aload_1
    //   619: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	620	0	this	b
    //   0	620	1	paramj	com.google.f.q.j
    //   0	620	2	paramObject1	Object
    //   0	620	3	paramObject2	Object
    //   3	545	4	localObject1	Object
    //   9	190	5	i	int
    //   203	42	5	bool1	boolean
    //   22	528	6	bool2	boolean
    //   25	511	7	j	int
    //   166	29	8	k	int
    //   251	230	9	localObject2	Object
    //   327	99	10	l1	long
    //   423	62	12	l2	long
    //   430	116	14	bool3	boolean
    //   438	50	15	bool4	boolean
    //   450	40	16	l3	long
    //   456	38	18	l4	long
    //   500	4	20	l5	long
    //   542	10	22	str	String
    // Exception table:
    //   from	to	target	type
    //   99	102	129	finally
    //   107	110	129	finally
    //   111	114	129	finally
    //   116	120	129	finally
    //   120	124	129	finally
    //   124	126	129	finally
    //   130	132	129	finally
    //   153	157	344	finally
    //   198	203	344	finally
    //   216	220	344	finally
    //   225	229	344	finally
    //   230	234	344	finally
    //   235	239	344	finally
    //   248	251	344	finally
    //   256	260	344	finally
    //   262	267	344	finally
    //   270	275	344	finally
    //   279	283	344	finally
    //   286	292	344	finally
    //   292	296	344	finally
    //   297	301	344	finally
    //   303	307	344	finally
    //   310	314	344	finally
    //   316	320	344	finally
    //   323	327	344	finally
    //   330	335	344	finally
    //   349	352	344	finally
    //   353	356	344	finally
    //   357	361	344	finally
    //   363	367	344	finally
    //   368	372	344	finally
    //   374	378	344	finally
    //   378	380	344	finally
    //   381	384	344	finally
    //   386	390	344	finally
    //   392	396	344	finally
    //   396	398	344	finally
    //   153	157	348	java/io/IOException
    //   198	203	348	java/io/IOException
    //   216	220	348	java/io/IOException
    //   225	229	348	java/io/IOException
    //   230	234	348	java/io/IOException
    //   235	239	348	java/io/IOException
    //   248	251	348	java/io/IOException
    //   256	260	348	java/io/IOException
    //   262	267	348	java/io/IOException
    //   270	275	348	java/io/IOException
    //   279	283	348	java/io/IOException
    //   286	292	348	java/io/IOException
    //   292	296	348	java/io/IOException
    //   297	301	348	java/io/IOException
    //   303	307	348	java/io/IOException
    //   310	314	348	java/io/IOException
    //   316	320	348	java/io/IOException
    //   323	327	348	java/io/IOException
    //   330	335	348	java/io/IOException
    //   153	157	380	com/google/f/x
    //   198	203	380	com/google/f/x
    //   216	220	380	com/google/f/x
    //   225	229	380	com/google/f/x
    //   230	234	380	com/google/f/x
    //   235	239	380	com/google/f/x
    //   248	251	380	com/google/f/x
    //   256	260	380	com/google/f/x
    //   262	267	380	com/google/f/x
    //   270	275	380	com/google/f/x
    //   279	283	380	com/google/f/x
    //   286	292	380	com/google/f/x
    //   292	296	380	com/google/f/x
    //   297	301	380	com/google/f/x
    //   303	307	380	com/google/f/x
    //   310	314	380	com/google/f/x
    //   316	320	380	com/google/f/x
    //   323	327	380	com/google/f/x
    //   330	335	380	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int k = -1;
    if (i != k) {
      return i;
    }
    long l1 = a;
    long l2 = 0L;
    int m = 0;
    boolean bool2 = l1 < l2;
    if (bool2)
    {
      int n = 1;
      i = h.computeInt64Size(n, l1);
      m = 0 + i;
    }
    Object localObject1 = b;
    boolean bool1 = ((String)localObject1).isEmpty();
    Object localObject2;
    int j;
    if (!bool1)
    {
      localObject2 = b;
      j = h.computeStringSize(2, (String)localObject2);
      m += j;
    }
    localObject1 = c;
    if (localObject1 != null)
    {
      localObject2 = d();
      j = h.computeMessageSize(3, (ae)localObject2);
      m += j;
    }
    memoizedSerializedSize = m;
    return m;
  }
  
  public final void writeTo(h paramh)
  {
    long l1 = a;
    long l2 = 0L;
    boolean bool1 = l1 < l2;
    if (bool1)
    {
      int i = 1;
      paramh.writeInt64(i, l1);
    }
    Object localObject1 = b;
    boolean bool2 = ((String)localObject1).isEmpty();
    int j;
    Object localObject2;
    if (!bool2)
    {
      j = 2;
      localObject2 = b;
      paramh.writeString(j, (String)localObject2);
    }
    localObject1 = c;
    if (localObject1 != null)
    {
      j = 3;
      localObject2 = d();
      paramh.writeMessage(j, (ae)localObject2);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.v.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */