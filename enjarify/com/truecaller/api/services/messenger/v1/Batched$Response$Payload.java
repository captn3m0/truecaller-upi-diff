package com.truecaller.api.services.messenger.v1;

import com.google.f.ae;
import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;

public final class Batched$Response$Payload
  extends q
  implements Batched.Response.d
{
  private static final Payload c;
  private static volatile ah d;
  private int a = 0;
  private Object b;
  
  static
  {
    Payload localPayload = new com/truecaller/api/services/messenger/v1/Batched$Response$Payload;
    localPayload.<init>();
    c = localPayload;
    localPayload.makeImmutable();
  }
  
  public static Payload a()
  {
    return c;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 31	com/truecaller/api/services/messenger/v1/Batched$1:b	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 37	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_4
    //   19: istore 6
    //   21: iconst_3
    //   22: istore 7
    //   24: iconst_2
    //   25: istore 8
    //   27: iconst_0
    //   28: istore 9
    //   30: aconst_null
    //   31: astore 10
    //   33: iconst_0
    //   34: istore 11
    //   36: iconst_1
    //   37: istore 12
    //   39: iload 5
    //   41: tableswitch	default:+47->88, 1:+1009->1050, 2:+1005->1046, 3:+1003->1044, 4:+992->1033, 5:+680->721, 6:+109->150, 7:+676->717, 8:+57->98
    //   88: new 43	java/lang/UnsupportedOperationException
    //   91: astore_1
    //   92: aload_1
    //   93: invokespecial 44	java/lang/UnsupportedOperationException:<init>	()V
    //   96: aload_1
    //   97: athrow
    //   98: getstatic 46	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:d	Lcom/google/f/ah;
    //   101: astore_1
    //   102: aload_1
    //   103: ifnonnull +43 -> 146
    //   106: ldc 2
    //   108: astore_1
    //   109: aload_1
    //   110: monitorenter
    //   111: getstatic 46	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:d	Lcom/google/f/ah;
    //   114: astore_2
    //   115: aload_2
    //   116: ifnonnull +20 -> 136
    //   119: new 48	com/google/f/q$b
    //   122: astore_2
    //   123: getstatic 20	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:c	Lcom/truecaller/api/services/messenger/v1/Batched$Response$Payload;
    //   126: astore_3
    //   127: aload_2
    //   128: aload_3
    //   129: invokespecial 51	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   132: aload_2
    //   133: putstatic 46	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:d	Lcom/google/f/ah;
    //   136: aload_1
    //   137: monitorexit
    //   138: goto +8 -> 146
    //   141: astore_2
    //   142: aload_1
    //   143: monitorexit
    //   144: aload_2
    //   145: athrow
    //   146: getstatic 46	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:d	Lcom/google/f/ah;
    //   149: areturn
    //   150: aload_2
    //   151: checkcast 53	com/google/f/g
    //   154: astore_2
    //   155: aload_3
    //   156: checkcast 55	com/google/f/n
    //   159: astore_3
    //   160: iload 11
    //   162: ifne +555 -> 717
    //   165: aload_2
    //   166: invokevirtual 58	com/google/f/g:readTag	()I
    //   169: istore 5
    //   171: iload 5
    //   173: ifeq +482 -> 655
    //   176: bipush 10
    //   178: istore 13
    //   180: iload 5
    //   182: iload 13
    //   184: if_icmpeq +367 -> 551
    //   187: bipush 18
    //   189: istore 13
    //   191: iload 5
    //   193: iload 13
    //   195: if_icmpeq +252 -> 447
    //   198: bipush 26
    //   200: istore 13
    //   202: iload 5
    //   204: iload 13
    //   206: if_icmpeq +137 -> 343
    //   209: bipush 34
    //   211: istore 13
    //   213: iload 5
    //   215: iload 13
    //   217: if_icmpeq +22 -> 239
    //   220: aload_2
    //   221: iload 5
    //   223: invokevirtual 66	com/google/f/g:skipField	(I)Z
    //   226: istore 5
    //   228: iload 5
    //   230: ifne -70 -> 160
    //   233: iconst_1
    //   234: istore 11
    //   236: goto -76 -> 160
    //   239: aload_0
    //   240: getfield 26	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:a	I
    //   243: istore 5
    //   245: iload 5
    //   247: iload 6
    //   249: if_icmpne +26 -> 275
    //   252: aload_0
    //   253: getfield 68	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:b	Ljava/lang/Object;
    //   256: astore_1
    //   257: aload_1
    //   258: checkcast 70	com/truecaller/api/services/messenger/v1/r$d
    //   261: astore_1
    //   262: aload_1
    //   263: invokevirtual 74	com/truecaller/api/services/messenger/v1/r$d:toBuilder	()Lcom/google/f/q$a;
    //   266: astore_1
    //   267: aload_1
    //   268: checkcast 76	com/truecaller/api/services/messenger/v1/r$d$a
    //   271: astore_1
    //   272: goto +8 -> 280
    //   275: iconst_0
    //   276: istore 5
    //   278: aconst_null
    //   279: astore_1
    //   280: invokestatic 79	com/truecaller/api/services/messenger/v1/r$d:c	()Lcom/google/f/ah;
    //   283: astore 14
    //   285: aload_2
    //   286: aload 14
    //   288: aload_3
    //   289: invokevirtual 83	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   292: astore 14
    //   294: aload_0
    //   295: aload 14
    //   297: putfield 68	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:b	Ljava/lang/Object;
    //   300: aload_1
    //   301: ifnull +33 -> 334
    //   304: aload_0
    //   305: getfield 68	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:b	Ljava/lang/Object;
    //   308: astore 14
    //   310: aload 14
    //   312: checkcast 70	com/truecaller/api/services/messenger/v1/r$d
    //   315: astore 14
    //   317: aload_1
    //   318: aload 14
    //   320: invokevirtual 87	com/truecaller/api/services/messenger/v1/r$d$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   323: pop
    //   324: aload_1
    //   325: invokevirtual 91	com/truecaller/api/services/messenger/v1/r$d$a:buildPartial	()Lcom/google/f/q;
    //   328: astore_1
    //   329: aload_0
    //   330: aload_1
    //   331: putfield 68	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:b	Ljava/lang/Object;
    //   334: aload_0
    //   335: iload 6
    //   337: putfield 26	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:a	I
    //   340: goto -180 -> 160
    //   343: aload_0
    //   344: getfield 26	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:a	I
    //   347: istore 5
    //   349: iload 5
    //   351: iload 7
    //   353: if_icmpne +26 -> 379
    //   356: aload_0
    //   357: getfield 68	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:b	Ljava/lang/Object;
    //   360: astore_1
    //   361: aload_1
    //   362: checkcast 93	com/truecaller/api/services/messenger/v1/t$d
    //   365: astore_1
    //   366: aload_1
    //   367: invokevirtual 94	com/truecaller/api/services/messenger/v1/t$d:toBuilder	()Lcom/google/f/q$a;
    //   370: astore_1
    //   371: aload_1
    //   372: checkcast 96	com/truecaller/api/services/messenger/v1/t$d$a
    //   375: astore_1
    //   376: goto +8 -> 384
    //   379: iconst_0
    //   380: istore 5
    //   382: aconst_null
    //   383: astore_1
    //   384: invokestatic 98	com/truecaller/api/services/messenger/v1/t$d:b	()Lcom/google/f/ah;
    //   387: astore 14
    //   389: aload_2
    //   390: aload 14
    //   392: aload_3
    //   393: invokevirtual 83	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   396: astore 14
    //   398: aload_0
    //   399: aload 14
    //   401: putfield 68	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:b	Ljava/lang/Object;
    //   404: aload_1
    //   405: ifnull +33 -> 438
    //   408: aload_0
    //   409: getfield 68	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:b	Ljava/lang/Object;
    //   412: astore 14
    //   414: aload 14
    //   416: checkcast 93	com/truecaller/api/services/messenger/v1/t$d
    //   419: astore 14
    //   421: aload_1
    //   422: aload 14
    //   424: invokevirtual 99	com/truecaller/api/services/messenger/v1/t$d$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   427: pop
    //   428: aload_1
    //   429: invokevirtual 100	com/truecaller/api/services/messenger/v1/t$d$a:buildPartial	()Lcom/google/f/q;
    //   432: astore_1
    //   433: aload_0
    //   434: aload_1
    //   435: putfield 68	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:b	Ljava/lang/Object;
    //   438: aload_0
    //   439: iload 7
    //   441: putfield 26	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:a	I
    //   444: goto -284 -> 160
    //   447: aload_0
    //   448: getfield 26	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:a	I
    //   451: istore 5
    //   453: iload 5
    //   455: iload 8
    //   457: if_icmpne +26 -> 483
    //   460: aload_0
    //   461: getfield 68	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:b	Ljava/lang/Object;
    //   464: astore_1
    //   465: aload_1
    //   466: checkcast 102	com/truecaller/api/services/messenger/v1/p$d
    //   469: astore_1
    //   470: aload_1
    //   471: invokevirtual 103	com/truecaller/api/services/messenger/v1/p$d:toBuilder	()Lcom/google/f/q$a;
    //   474: astore_1
    //   475: aload_1
    //   476: checkcast 105	com/truecaller/api/services/messenger/v1/p$d$a
    //   479: astore_1
    //   480: goto +8 -> 488
    //   483: iconst_0
    //   484: istore 5
    //   486: aconst_null
    //   487: astore_1
    //   488: invokestatic 106	com/truecaller/api/services/messenger/v1/p$d:c	()Lcom/google/f/ah;
    //   491: astore 14
    //   493: aload_2
    //   494: aload 14
    //   496: aload_3
    //   497: invokevirtual 83	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   500: astore 14
    //   502: aload_0
    //   503: aload 14
    //   505: putfield 68	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:b	Ljava/lang/Object;
    //   508: aload_1
    //   509: ifnull +33 -> 542
    //   512: aload_0
    //   513: getfield 68	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:b	Ljava/lang/Object;
    //   516: astore 14
    //   518: aload 14
    //   520: checkcast 102	com/truecaller/api/services/messenger/v1/p$d
    //   523: astore 14
    //   525: aload_1
    //   526: aload 14
    //   528: invokevirtual 107	com/truecaller/api/services/messenger/v1/p$d$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   531: pop
    //   532: aload_1
    //   533: invokevirtual 108	com/truecaller/api/services/messenger/v1/p$d$a:buildPartial	()Lcom/google/f/q;
    //   536: astore_1
    //   537: aload_0
    //   538: aload_1
    //   539: putfield 68	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:b	Ljava/lang/Object;
    //   542: aload_0
    //   543: iload 8
    //   545: putfield 26	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:a	I
    //   548: goto -388 -> 160
    //   551: aload_0
    //   552: getfield 26	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:a	I
    //   555: istore 5
    //   557: iload 5
    //   559: iload 12
    //   561: if_icmpne +26 -> 587
    //   564: aload_0
    //   565: getfield 68	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:b	Ljava/lang/Object;
    //   568: astore_1
    //   569: aload_1
    //   570: checkcast 110	com/truecaller/api/services/messenger/v1/Batched$Response$a
    //   573: astore_1
    //   574: aload_1
    //   575: invokevirtual 111	com/truecaller/api/services/messenger/v1/Batched$Response$a:toBuilder	()Lcom/google/f/q$a;
    //   578: astore_1
    //   579: aload_1
    //   580: checkcast 113	com/truecaller/api/services/messenger/v1/Batched$Response$a$a
    //   583: astore_1
    //   584: goto +8 -> 592
    //   587: iconst_0
    //   588: istore 5
    //   590: aconst_null
    //   591: astore_1
    //   592: invokestatic 115	com/truecaller/api/services/messenger/v1/Batched$Response$a:a	()Lcom/google/f/ah;
    //   595: astore 14
    //   597: aload_2
    //   598: aload 14
    //   600: aload_3
    //   601: invokevirtual 83	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   604: astore 14
    //   606: aload_0
    //   607: aload 14
    //   609: putfield 68	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:b	Ljava/lang/Object;
    //   612: aload_1
    //   613: ifnull +33 -> 646
    //   616: aload_0
    //   617: getfield 68	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:b	Ljava/lang/Object;
    //   620: astore 14
    //   622: aload 14
    //   624: checkcast 110	com/truecaller/api/services/messenger/v1/Batched$Response$a
    //   627: astore 14
    //   629: aload_1
    //   630: aload 14
    //   632: invokevirtual 116	com/truecaller/api/services/messenger/v1/Batched$Response$a$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   635: pop
    //   636: aload_1
    //   637: invokevirtual 117	com/truecaller/api/services/messenger/v1/Batched$Response$a$a:buildPartial	()Lcom/google/f/q;
    //   640: astore_1
    //   641: aload_0
    //   642: aload_1
    //   643: putfield 68	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:b	Ljava/lang/Object;
    //   646: aload_0
    //   647: iload 12
    //   649: putfield 26	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:a	I
    //   652: goto -492 -> 160
    //   655: iconst_1
    //   656: istore 11
    //   658: goto -498 -> 160
    //   661: astore_1
    //   662: goto +53 -> 715
    //   665: astore_1
    //   666: new 119	java/lang/RuntimeException
    //   669: astore_2
    //   670: new 121	com/google/f/x
    //   673: astore_3
    //   674: aload_1
    //   675: invokevirtual 127	java/io/IOException:getMessage	()Ljava/lang/String;
    //   678: astore_1
    //   679: aload_3
    //   680: aload_1
    //   681: invokespecial 130	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   684: aload_3
    //   685: aload_0
    //   686: invokevirtual 134	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   689: astore_1
    //   690: aload_2
    //   691: aload_1
    //   692: invokespecial 137	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   695: aload_2
    //   696: athrow
    //   697: astore_1
    //   698: new 119	java/lang/RuntimeException
    //   701: astore_2
    //   702: aload_1
    //   703: aload_0
    //   704: invokevirtual 134	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   707: astore_1
    //   708: aload_2
    //   709: aload_1
    //   710: invokespecial 137	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   713: aload_2
    //   714: athrow
    //   715: aload_1
    //   716: athrow
    //   717: getstatic 20	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:c	Lcom/truecaller/api/services/messenger/v1/Batched$Response$Payload;
    //   720: areturn
    //   721: aload_2
    //   722: checkcast 139	com/google/f/q$k
    //   725: astore_2
    //   726: aload_3
    //   727: checkcast 2	com/truecaller/api/services/messenger/v1/Batched$Response$Payload
    //   730: astore_3
    //   731: getstatic 141	com/truecaller/api/services/messenger/v1/Batched$1:c	[I
    //   734: astore_1
    //   735: aload_3
    //   736: getfield 26	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:a	I
    //   739: invokestatic 147	com/truecaller/api/services/messenger/v1/Batched$Response$Payload$DataCase:forNumber	(I)Lcom/truecaller/api/services/messenger/v1/Batched$Response$Payload$DataCase;
    //   742: astore 10
    //   744: aload 10
    //   746: invokevirtual 148	com/truecaller/api/services/messenger/v1/Batched$Response$Payload$DataCase:ordinal	()I
    //   749: istore 9
    //   751: aload_1
    //   752: iload 9
    //   754: iaload
    //   755: istore 5
    //   757: iload 5
    //   759: tableswitch	default:+33->792, 1:+202->961, 2:+155->914, 3:+108->867, 4:+61->820, 5:+36->795
    //   792: goto +213 -> 1005
    //   795: aload_0
    //   796: getfield 26	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:a	I
    //   799: istore 5
    //   801: iload 5
    //   803: ifeq +6 -> 809
    //   806: iconst_1
    //   807: istore 11
    //   809: aload_2
    //   810: iload 11
    //   812: invokeinterface 152 2 0
    //   817: goto +188 -> 1005
    //   820: aload_0
    //   821: getfield 26	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:a	I
    //   824: istore 5
    //   826: iload 5
    //   828: iload 6
    //   830: if_icmpne +6 -> 836
    //   833: iconst_1
    //   834: istore 11
    //   836: aload_0
    //   837: getfield 68	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:b	Ljava/lang/Object;
    //   840: astore_1
    //   841: aload_3
    //   842: getfield 68	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:b	Ljava/lang/Object;
    //   845: astore 4
    //   847: aload_2
    //   848: iload 11
    //   850: aload_1
    //   851: aload 4
    //   853: invokeinterface 156 4 0
    //   858: astore_1
    //   859: aload_0
    //   860: aload_1
    //   861: putfield 68	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:b	Ljava/lang/Object;
    //   864: goto +141 -> 1005
    //   867: aload_0
    //   868: getfield 26	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:a	I
    //   871: istore 5
    //   873: iload 5
    //   875: iload 7
    //   877: if_icmpne +6 -> 883
    //   880: iconst_1
    //   881: istore 11
    //   883: aload_0
    //   884: getfield 68	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:b	Ljava/lang/Object;
    //   887: astore_1
    //   888: aload_3
    //   889: getfield 68	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:b	Ljava/lang/Object;
    //   892: astore 4
    //   894: aload_2
    //   895: iload 11
    //   897: aload_1
    //   898: aload 4
    //   900: invokeinterface 156 4 0
    //   905: astore_1
    //   906: aload_0
    //   907: aload_1
    //   908: putfield 68	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:b	Ljava/lang/Object;
    //   911: goto +94 -> 1005
    //   914: aload_0
    //   915: getfield 26	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:a	I
    //   918: istore 5
    //   920: iload 5
    //   922: iload 8
    //   924: if_icmpne +6 -> 930
    //   927: iconst_1
    //   928: istore 11
    //   930: aload_0
    //   931: getfield 68	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:b	Ljava/lang/Object;
    //   934: astore_1
    //   935: aload_3
    //   936: getfield 68	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:b	Ljava/lang/Object;
    //   939: astore 4
    //   941: aload_2
    //   942: iload 11
    //   944: aload_1
    //   945: aload 4
    //   947: invokeinterface 156 4 0
    //   952: astore_1
    //   953: aload_0
    //   954: aload_1
    //   955: putfield 68	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:b	Ljava/lang/Object;
    //   958: goto +47 -> 1005
    //   961: aload_0
    //   962: getfield 26	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:a	I
    //   965: istore 5
    //   967: iload 5
    //   969: iload 12
    //   971: if_icmpne +6 -> 977
    //   974: iconst_1
    //   975: istore 11
    //   977: aload_0
    //   978: getfield 68	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:b	Ljava/lang/Object;
    //   981: astore_1
    //   982: aload_3
    //   983: getfield 68	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:b	Ljava/lang/Object;
    //   986: astore 4
    //   988: aload_2
    //   989: iload 11
    //   991: aload_1
    //   992: aload 4
    //   994: invokeinterface 156 4 0
    //   999: astore_1
    //   1000: aload_0
    //   1001: aload_1
    //   1002: putfield 68	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:b	Ljava/lang/Object;
    //   1005: getstatic 162	com/google/f/q$i:INSTANCE	Lcom/google/f/q$i;
    //   1008: astore_1
    //   1009: aload_2
    //   1010: aload_1
    //   1011: if_acmpne +20 -> 1031
    //   1014: aload_3
    //   1015: getfield 26	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:a	I
    //   1018: istore 5
    //   1020: iload 5
    //   1022: ifeq +9 -> 1031
    //   1025: aload_0
    //   1026: iload 5
    //   1028: putfield 26	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:a	I
    //   1031: aload_0
    //   1032: areturn
    //   1033: new 164	com/truecaller/api/services/messenger/v1/Batched$Response$Payload$a
    //   1036: astore_1
    //   1037: aload_1
    //   1038: iconst_0
    //   1039: invokespecial 167	com/truecaller/api/services/messenger/v1/Batched$Response$Payload$a:<init>	(B)V
    //   1042: aload_1
    //   1043: areturn
    //   1044: aconst_null
    //   1045: areturn
    //   1046: getstatic 20	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:c	Lcom/truecaller/api/services/messenger/v1/Batched$Response$Payload;
    //   1049: areturn
    //   1050: new 2	com/truecaller/api/services/messenger/v1/Batched$Response$Payload
    //   1053: astore_1
    //   1054: aload_1
    //   1055: invokespecial 18	com/truecaller/api/services/messenger/v1/Batched$Response$Payload:<init>	()V
    //   1058: aload_1
    //   1059: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1060	0	this	Payload
    //   0	1060	1	paramj	com.google.f.q.j
    //   0	1060	2	paramObject1	Object
    //   0	1060	3	paramObject2	Object
    //   3	990	4	localObject1	Object
    //   9	213	5	i	int
    //   226	3	5	bool1	boolean
    //   243	784	5	j	int
    //   19	812	6	k	int
    //   22	856	7	m	int
    //   25	900	8	n	int
    //   28	725	9	i1	int
    //   31	714	10	localDataCase	Batched.Response.Payload.DataCase
    //   34	956	11	bool2	boolean
    //   37	935	12	i2	int
    //   178	40	13	i3	int
    //   283	348	14	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   111	114	141	finally
    //   119	122	141	finally
    //   123	126	141	finally
    //   128	132	141	finally
    //   132	136	141	finally
    //   136	138	141	finally
    //   142	144	141	finally
    //   165	169	661	finally
    //   221	226	661	finally
    //   239	243	661	finally
    //   252	256	661	finally
    //   257	261	661	finally
    //   262	266	661	finally
    //   267	271	661	finally
    //   280	283	661	finally
    //   288	292	661	finally
    //   295	300	661	finally
    //   304	308	661	finally
    //   310	315	661	finally
    //   318	324	661	finally
    //   324	328	661	finally
    //   330	334	661	finally
    //   335	340	661	finally
    //   343	347	661	finally
    //   356	360	661	finally
    //   361	365	661	finally
    //   366	370	661	finally
    //   371	375	661	finally
    //   384	387	661	finally
    //   392	396	661	finally
    //   399	404	661	finally
    //   408	412	661	finally
    //   414	419	661	finally
    //   422	428	661	finally
    //   428	432	661	finally
    //   434	438	661	finally
    //   439	444	661	finally
    //   447	451	661	finally
    //   460	464	661	finally
    //   465	469	661	finally
    //   470	474	661	finally
    //   475	479	661	finally
    //   488	491	661	finally
    //   496	500	661	finally
    //   503	508	661	finally
    //   512	516	661	finally
    //   518	523	661	finally
    //   526	532	661	finally
    //   532	536	661	finally
    //   538	542	661	finally
    //   543	548	661	finally
    //   551	555	661	finally
    //   564	568	661	finally
    //   569	573	661	finally
    //   574	578	661	finally
    //   579	583	661	finally
    //   592	595	661	finally
    //   600	604	661	finally
    //   607	612	661	finally
    //   616	620	661	finally
    //   622	627	661	finally
    //   630	636	661	finally
    //   636	640	661	finally
    //   642	646	661	finally
    //   647	652	661	finally
    //   666	669	661	finally
    //   670	673	661	finally
    //   674	678	661	finally
    //   680	684	661	finally
    //   685	689	661	finally
    //   691	695	661	finally
    //   695	697	661	finally
    //   698	701	661	finally
    //   703	707	661	finally
    //   709	713	661	finally
    //   713	715	661	finally
    //   165	169	665	java/io/IOException
    //   221	226	665	java/io/IOException
    //   239	243	665	java/io/IOException
    //   252	256	665	java/io/IOException
    //   257	261	665	java/io/IOException
    //   262	266	665	java/io/IOException
    //   267	271	665	java/io/IOException
    //   280	283	665	java/io/IOException
    //   288	292	665	java/io/IOException
    //   295	300	665	java/io/IOException
    //   304	308	665	java/io/IOException
    //   310	315	665	java/io/IOException
    //   318	324	665	java/io/IOException
    //   324	328	665	java/io/IOException
    //   330	334	665	java/io/IOException
    //   335	340	665	java/io/IOException
    //   343	347	665	java/io/IOException
    //   356	360	665	java/io/IOException
    //   361	365	665	java/io/IOException
    //   366	370	665	java/io/IOException
    //   371	375	665	java/io/IOException
    //   384	387	665	java/io/IOException
    //   392	396	665	java/io/IOException
    //   399	404	665	java/io/IOException
    //   408	412	665	java/io/IOException
    //   414	419	665	java/io/IOException
    //   422	428	665	java/io/IOException
    //   428	432	665	java/io/IOException
    //   434	438	665	java/io/IOException
    //   439	444	665	java/io/IOException
    //   447	451	665	java/io/IOException
    //   460	464	665	java/io/IOException
    //   465	469	665	java/io/IOException
    //   470	474	665	java/io/IOException
    //   475	479	665	java/io/IOException
    //   488	491	665	java/io/IOException
    //   496	500	665	java/io/IOException
    //   503	508	665	java/io/IOException
    //   512	516	665	java/io/IOException
    //   518	523	665	java/io/IOException
    //   526	532	665	java/io/IOException
    //   532	536	665	java/io/IOException
    //   538	542	665	java/io/IOException
    //   543	548	665	java/io/IOException
    //   551	555	665	java/io/IOException
    //   564	568	665	java/io/IOException
    //   569	573	665	java/io/IOException
    //   574	578	665	java/io/IOException
    //   579	583	665	java/io/IOException
    //   592	595	665	java/io/IOException
    //   600	604	665	java/io/IOException
    //   607	612	665	java/io/IOException
    //   616	620	665	java/io/IOException
    //   622	627	665	java/io/IOException
    //   630	636	665	java/io/IOException
    //   636	640	665	java/io/IOException
    //   642	646	665	java/io/IOException
    //   647	652	665	java/io/IOException
    //   165	169	697	com/google/f/x
    //   221	226	697	com/google/f/x
    //   239	243	697	com/google/f/x
    //   252	256	697	com/google/f/x
    //   257	261	697	com/google/f/x
    //   262	266	697	com/google/f/x
    //   267	271	697	com/google/f/x
    //   280	283	697	com/google/f/x
    //   288	292	697	com/google/f/x
    //   295	300	697	com/google/f/x
    //   304	308	697	com/google/f/x
    //   310	315	697	com/google/f/x
    //   318	324	697	com/google/f/x
    //   324	328	697	com/google/f/x
    //   330	334	697	com/google/f/x
    //   335	340	697	com/google/f/x
    //   343	347	697	com/google/f/x
    //   356	360	697	com/google/f/x
    //   361	365	697	com/google/f/x
    //   366	370	697	com/google/f/x
    //   371	375	697	com/google/f/x
    //   384	387	697	com/google/f/x
    //   392	396	697	com/google/f/x
    //   399	404	697	com/google/f/x
    //   408	412	697	com/google/f/x
    //   414	419	697	com/google/f/x
    //   422	428	697	com/google/f/x
    //   428	432	697	com/google/f/x
    //   434	438	697	com/google/f/x
    //   439	444	697	com/google/f/x
    //   447	451	697	com/google/f/x
    //   460	464	697	com/google/f/x
    //   465	469	697	com/google/f/x
    //   470	474	697	com/google/f/x
    //   475	479	697	com/google/f/x
    //   488	491	697	com/google/f/x
    //   496	500	697	com/google/f/x
    //   503	508	697	com/google/f/x
    //   512	516	697	com/google/f/x
    //   518	523	697	com/google/f/x
    //   526	532	697	com/google/f/x
    //   532	536	697	com/google/f/x
    //   538	542	697	com/google/f/x
    //   543	548	697	com/google/f/x
    //   551	555	697	com/google/f/x
    //   564	568	697	com/google/f/x
    //   569	573	697	com/google/f/x
    //   574	578	697	com/google/f/x
    //   579	583	697	com/google/f/x
    //   592	595	697	com/google/f/x
    //   600	604	697	com/google/f/x
    //   607	612	697	com/google/f/x
    //   616	620	697	com/google/f/x
    //   622	627	697	com/google/f/x
    //   630	636	697	com/google/f/x
    //   636	640	697	com/google/f/x
    //   642	646	697	com/google/f/x
    //   647	652	697	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int j = -1;
    if (i != j) {
      return i;
    }
    i = a;
    j = 1;
    int k = 0;
    Object localObject;
    if (i == j)
    {
      localObject = (Batched.Response.a)b;
      i = h.computeMessageSize(j, (ae)localObject);
      k = 0 + i;
    }
    i = a;
    j = 2;
    if (i == j)
    {
      localObject = (p.d)b;
      i = h.computeMessageSize(j, (ae)localObject);
      k += i;
    }
    i = a;
    j = 3;
    if (i == j)
    {
      localObject = (t.d)b;
      i = h.computeMessageSize(j, (ae)localObject);
      k += i;
    }
    i = a;
    j = 4;
    if (i == j)
    {
      localObject = (r.d)b;
      i = h.computeMessageSize(j, (ae)localObject);
      k += i;
    }
    memoizedSerializedSize = k;
    return k;
  }
  
  public final void writeTo(h paramh)
  {
    int i = a;
    int j = 1;
    Object localObject;
    if (i == j)
    {
      localObject = (Batched.Response.a)b;
      paramh.writeMessage(j, (ae)localObject);
    }
    i = a;
    j = 2;
    if (i == j)
    {
      localObject = (p.d)b;
      paramh.writeMessage(j, (ae)localObject);
    }
    i = a;
    j = 3;
    if (i == j)
    {
      localObject = (t.d)b;
      paramh.writeMessage(j, (ae)localObject);
    }
    i = a;
    j = 4;
    if (i == j)
    {
      localObject = (r.d)b;
      paramh.writeMessage(j, (ae)localObject);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.Batched.Response.Payload
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */