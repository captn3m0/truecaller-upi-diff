package com.truecaller.api.services.messenger.v1;

import com.google.f.q.a;
import com.truecaller.api.services.messenger.v1.models.input.InputPeer;
import com.truecaller.api.services.messenger.v1.models.input.InputPeer.a;

public final class z$b$a
  extends q.a
  implements z.c
{
  private z$b$a()
  {
    super(localb);
  }
  
  public final a a(int paramInt)
  {
    copyOnWrite();
    z.b.a((z.b)instance, paramInt);
    return this;
  }
  
  public final a a(long paramLong)
  {
    copyOnWrite();
    z.b.a((z.b)instance, paramLong);
    return this;
  }
  
  public final a a(InputPeer.a parama)
  {
    copyOnWrite();
    z.b.a((z.b)instance, parama);
    return this;
  }
  
  public final a a(InputPeer paramInputPeer)
  {
    copyOnWrite();
    z.b.a((z.b)instance, paramInputPeer);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.z.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */