package com.truecaller.api.services.messenger.v1;

import com.google.f.q.a;

public final class f$b$a
  extends q.a
  implements f.c
{
  private f$b$a()
  {
    super(localb);
  }
  
  public final a a(int paramInt)
  {
    copyOnWrite();
    f.b.a((f.b)instance, paramInt);
    return this;
  }
  
  public final a a(String paramString)
  {
    copyOnWrite();
    f.b.a((f.b)instance, paramString);
    return this;
  }
  
  public final a b(int paramInt)
  {
    copyOnWrite();
    f.b.b((f.b)instance, paramInt);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.f.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */