package com.truecaller.api.services.messenger.v1;

import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;
import com.truecaller.api.services.messenger.v1.models.input.InputPeer;

public final class z$b
  extends q
  implements z.c
{
  private static final b e;
  private static volatile ah f;
  private long a;
  private InputPeer b;
  private InputPeer c;
  private int d;
  
  static
  {
    b localb = new com/truecaller/api/services/messenger/v1/z$b;
    localb.<init>();
    e = localb;
    localb.makeImmutable();
  }
  
  public static z.b.a a()
  {
    return (z.b.a)e.toBuilder();
  }
  
  public static b b()
  {
    return e;
  }
  
  private InputPeer d()
  {
    InputPeer localInputPeer = b;
    if (localInputPeer == null) {
      localInputPeer = InputPeer.b();
    }
    return localInputPeer;
  }
  
  private InputPeer e()
  {
    InputPeer localInputPeer = c;
    if (localInputPeer == null) {
      localInputPeer = InputPeer.b();
    }
    return localInputPeer;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 60	com/truecaller/api/services/messenger/v1/z$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 66	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_0
    //   19: istore 6
    //   21: aconst_null
    //   22: astore 4
    //   24: iconst_1
    //   25: istore 7
    //   27: iconst_0
    //   28: istore 8
    //   30: iload 5
    //   32: tableswitch	default:+48->80, 1:+745->777, 2:+741->773, 3:+739->771, 4:+728->760, 5:+483->515, 6:+110->142, 7:+479->511, 8:+58->90
    //   80: new 69	java/lang/UnsupportedOperationException
    //   83: astore_1
    //   84: aload_1
    //   85: invokespecial 70	java/lang/UnsupportedOperationException:<init>	()V
    //   88: aload_1
    //   89: athrow
    //   90: getstatic 72	com/truecaller/api/services/messenger/v1/z$b:f	Lcom/google/f/ah;
    //   93: astore_1
    //   94: aload_1
    //   95: ifnonnull +43 -> 138
    //   98: ldc 2
    //   100: astore_1
    //   101: aload_1
    //   102: monitorenter
    //   103: getstatic 72	com/truecaller/api/services/messenger/v1/z$b:f	Lcom/google/f/ah;
    //   106: astore_2
    //   107: aload_2
    //   108: ifnonnull +20 -> 128
    //   111: new 74	com/google/f/q$b
    //   114: astore_2
    //   115: getstatic 23	com/truecaller/api/services/messenger/v1/z$b:e	Lcom/truecaller/api/services/messenger/v1/z$b;
    //   118: astore_3
    //   119: aload_2
    //   120: aload_3
    //   121: invokespecial 77	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   124: aload_2
    //   125: putstatic 72	com/truecaller/api/services/messenger/v1/z$b:f	Lcom/google/f/ah;
    //   128: aload_1
    //   129: monitorexit
    //   130: goto +8 -> 138
    //   133: astore_2
    //   134: aload_1
    //   135: monitorexit
    //   136: aload_2
    //   137: athrow
    //   138: getstatic 72	com/truecaller/api/services/messenger/v1/z$b:f	Lcom/google/f/ah;
    //   141: areturn
    //   142: aload_2
    //   143: checkcast 79	com/google/f/g
    //   146: astore_2
    //   147: aload_3
    //   148: checkcast 81	com/google/f/n
    //   151: astore_3
    //   152: iload 8
    //   154: ifne +357 -> 511
    //   157: aload_2
    //   158: invokevirtual 84	com/google/f/g:readTag	()I
    //   161: istore 5
    //   163: iload 5
    //   165: ifeq +284 -> 449
    //   168: bipush 8
    //   170: istore 9
    //   172: iload 5
    //   174: iload 9
    //   176: if_icmpeq +258 -> 434
    //   179: bipush 18
    //   181: istore 9
    //   183: iload 5
    //   185: iload 9
    //   187: if_icmpeq +153 -> 340
    //   190: bipush 26
    //   192: istore 9
    //   194: iload 5
    //   196: iload 9
    //   198: if_icmpeq +48 -> 246
    //   201: bipush 32
    //   203: istore 9
    //   205: iload 5
    //   207: iload 9
    //   209: if_icmpeq +22 -> 231
    //   212: aload_2
    //   213: iload 5
    //   215: invokevirtual 92	com/google/f/g:skipField	(I)Z
    //   218: istore 5
    //   220: iload 5
    //   222: ifne -70 -> 152
    //   225: iconst_1
    //   226: istore 8
    //   228: goto -76 -> 152
    //   231: aload_2
    //   232: invokevirtual 95	com/google/f/g:readInt32	()I
    //   235: istore 5
    //   237: aload_0
    //   238: iload 5
    //   240: putfield 35	com/truecaller/api/services/messenger/v1/z$b:d	I
    //   243: goto -91 -> 152
    //   246: aload_0
    //   247: getfield 47	com/truecaller/api/services/messenger/v1/z$b:c	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   250: astore_1
    //   251: aload_1
    //   252: ifnull +21 -> 273
    //   255: aload_0
    //   256: getfield 47	com/truecaller/api/services/messenger/v1/z$b:c	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   259: astore_1
    //   260: aload_1
    //   261: invokevirtual 96	com/truecaller/api/services/messenger/v1/models/input/InputPeer:toBuilder	()Lcom/google/f/q$a;
    //   264: astore_1
    //   265: aload_1
    //   266: checkcast 39	com/truecaller/api/services/messenger/v1/models/input/InputPeer$a
    //   269: astore_1
    //   270: goto +8 -> 278
    //   273: iconst_0
    //   274: istore 5
    //   276: aconst_null
    //   277: astore_1
    //   278: invokestatic 99	com/truecaller/api/services/messenger/v1/models/input/InputPeer:c	()Lcom/google/f/ah;
    //   281: astore 10
    //   283: aload_2
    //   284: aload 10
    //   286: aload_3
    //   287: invokevirtual 103	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   290: astore 10
    //   292: aload 10
    //   294: checkcast 45	com/truecaller/api/services/messenger/v1/models/input/InputPeer
    //   297: astore 10
    //   299: aload_0
    //   300: aload 10
    //   302: putfield 47	com/truecaller/api/services/messenger/v1/z$b:c	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   305: aload_1
    //   306: ifnull -154 -> 152
    //   309: aload_0
    //   310: getfield 47	com/truecaller/api/services/messenger/v1/z$b:c	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   313: astore 10
    //   315: aload_1
    //   316: aload 10
    //   318: invokevirtual 107	com/truecaller/api/services/messenger/v1/models/input/InputPeer$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   321: pop
    //   322: aload_1
    //   323: invokevirtual 110	com/truecaller/api/services/messenger/v1/models/input/InputPeer$a:buildPartial	()Lcom/google/f/q;
    //   326: astore_1
    //   327: aload_1
    //   328: checkcast 45	com/truecaller/api/services/messenger/v1/models/input/InputPeer
    //   331: astore_1
    //   332: aload_0
    //   333: aload_1
    //   334: putfield 47	com/truecaller/api/services/messenger/v1/z$b:c	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   337: goto -185 -> 152
    //   340: aload_0
    //   341: getfield 49	com/truecaller/api/services/messenger/v1/z$b:b	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   344: astore_1
    //   345: aload_1
    //   346: ifnull +21 -> 367
    //   349: aload_0
    //   350: getfield 49	com/truecaller/api/services/messenger/v1/z$b:b	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   353: astore_1
    //   354: aload_1
    //   355: invokevirtual 96	com/truecaller/api/services/messenger/v1/models/input/InputPeer:toBuilder	()Lcom/google/f/q$a;
    //   358: astore_1
    //   359: aload_1
    //   360: checkcast 39	com/truecaller/api/services/messenger/v1/models/input/InputPeer$a
    //   363: astore_1
    //   364: goto +8 -> 372
    //   367: iconst_0
    //   368: istore 5
    //   370: aconst_null
    //   371: astore_1
    //   372: invokestatic 99	com/truecaller/api/services/messenger/v1/models/input/InputPeer:c	()Lcom/google/f/ah;
    //   375: astore 10
    //   377: aload_2
    //   378: aload 10
    //   380: aload_3
    //   381: invokevirtual 103	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   384: astore 10
    //   386: aload 10
    //   388: checkcast 45	com/truecaller/api/services/messenger/v1/models/input/InputPeer
    //   391: astore 10
    //   393: aload_0
    //   394: aload 10
    //   396: putfield 49	com/truecaller/api/services/messenger/v1/z$b:b	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   399: aload_1
    //   400: ifnull -248 -> 152
    //   403: aload_0
    //   404: getfield 49	com/truecaller/api/services/messenger/v1/z$b:b	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   407: astore 10
    //   409: aload_1
    //   410: aload 10
    //   412: invokevirtual 107	com/truecaller/api/services/messenger/v1/models/input/InputPeer$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   415: pop
    //   416: aload_1
    //   417: invokevirtual 110	com/truecaller/api/services/messenger/v1/models/input/InputPeer$a:buildPartial	()Lcom/google/f/q;
    //   420: astore_1
    //   421: aload_1
    //   422: checkcast 45	com/truecaller/api/services/messenger/v1/models/input/InputPeer
    //   425: astore_1
    //   426: aload_0
    //   427: aload_1
    //   428: putfield 49	com/truecaller/api/services/messenger/v1/z$b:b	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   431: goto -279 -> 152
    //   434: aload_2
    //   435: invokevirtual 114	com/google/f/g:readInt64	()J
    //   438: lstore 11
    //   440: aload_0
    //   441: lload 11
    //   443: putfield 37	com/truecaller/api/services/messenger/v1/z$b:a	J
    //   446: goto -294 -> 152
    //   449: iconst_1
    //   450: istore 8
    //   452: goto -300 -> 152
    //   455: astore_1
    //   456: goto +53 -> 509
    //   459: astore_1
    //   460: new 116	java/lang/RuntimeException
    //   463: astore_2
    //   464: new 118	com/google/f/x
    //   467: astore_3
    //   468: aload_1
    //   469: invokevirtual 124	java/io/IOException:getMessage	()Ljava/lang/String;
    //   472: astore_1
    //   473: aload_3
    //   474: aload_1
    //   475: invokespecial 127	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   478: aload_3
    //   479: aload_0
    //   480: invokevirtual 131	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   483: astore_1
    //   484: aload_2
    //   485: aload_1
    //   486: invokespecial 134	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   489: aload_2
    //   490: athrow
    //   491: astore_1
    //   492: new 116	java/lang/RuntimeException
    //   495: astore_2
    //   496: aload_1
    //   497: aload_0
    //   498: invokevirtual 131	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   501: astore_1
    //   502: aload_2
    //   503: aload_1
    //   504: invokespecial 134	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   507: aload_2
    //   508: athrow
    //   509: aload_1
    //   510: athrow
    //   511: getstatic 23	com/truecaller/api/services/messenger/v1/z$b:e	Lcom/truecaller/api/services/messenger/v1/z$b;
    //   514: areturn
    //   515: aload_2
    //   516: astore_1
    //   517: aload_2
    //   518: checkcast 136	com/google/f/q$k
    //   521: astore_1
    //   522: aload_3
    //   523: checkcast 2	com/truecaller/api/services/messenger/v1/z$b
    //   526: astore_3
    //   527: aload_0
    //   528: getfield 37	com/truecaller/api/services/messenger/v1/z$b:a	J
    //   531: lstore 11
    //   533: lconst_0
    //   534: lstore 13
    //   536: lload 11
    //   538: lload 13
    //   540: lcmp
    //   541: istore 15
    //   543: iload 15
    //   545: ifeq +9 -> 554
    //   548: iconst_1
    //   549: istore 16
    //   551: goto +6 -> 557
    //   554: iconst_0
    //   555: istore 16
    //   557: aload_0
    //   558: getfield 37	com/truecaller/api/services/messenger/v1/z$b:a	J
    //   561: lstore 17
    //   563: aload_3
    //   564: getfield 37	com/truecaller/api/services/messenger/v1/z$b:a	J
    //   567: lstore 19
    //   569: lload 19
    //   571: lload 13
    //   573: lcmp
    //   574: istore 15
    //   576: iload 15
    //   578: ifeq +9 -> 587
    //   581: iconst_1
    //   582: istore 15
    //   584: goto +8 -> 592
    //   587: iconst_0
    //   588: istore 15
    //   590: aconst_null
    //   591: astore_2
    //   592: aload_3
    //   593: getfield 37	com/truecaller/api/services/messenger/v1/z$b:a	J
    //   596: lstore 19
    //   598: aload_1
    //   599: astore 10
    //   601: lload 17
    //   603: lstore 13
    //   605: aload_1
    //   606: iload 16
    //   608: lload 17
    //   610: iload 15
    //   612: lload 19
    //   614: invokeinterface 140 7 0
    //   619: lstore 11
    //   621: aload_0
    //   622: lload 11
    //   624: putfield 37	com/truecaller/api/services/messenger/v1/z$b:a	J
    //   627: aload_0
    //   628: getfield 49	com/truecaller/api/services/messenger/v1/z$b:b	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   631: astore_2
    //   632: aload_3
    //   633: getfield 49	com/truecaller/api/services/messenger/v1/z$b:b	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   636: astore 4
    //   638: aload_1
    //   639: aload_2
    //   640: aload 4
    //   642: invokeinterface 144 3 0
    //   647: checkcast 45	com/truecaller/api/services/messenger/v1/models/input/InputPeer
    //   650: astore_2
    //   651: aload_0
    //   652: aload_2
    //   653: putfield 49	com/truecaller/api/services/messenger/v1/z$b:b	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   656: aload_0
    //   657: getfield 47	com/truecaller/api/services/messenger/v1/z$b:c	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   660: astore_2
    //   661: aload_3
    //   662: getfield 47	com/truecaller/api/services/messenger/v1/z$b:c	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   665: astore 4
    //   667: aload_1
    //   668: aload_2
    //   669: aload 4
    //   671: invokeinterface 144 3 0
    //   676: checkcast 45	com/truecaller/api/services/messenger/v1/models/input/InputPeer
    //   679: astore_2
    //   680: aload_0
    //   681: aload_2
    //   682: putfield 47	com/truecaller/api/services/messenger/v1/z$b:c	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   685: aload_0
    //   686: getfield 35	com/truecaller/api/services/messenger/v1/z$b:d	I
    //   689: istore 15
    //   691: iload 15
    //   693: ifeq +9 -> 702
    //   696: iconst_1
    //   697: istore 15
    //   699: goto +8 -> 707
    //   702: iconst_0
    //   703: istore 15
    //   705: aconst_null
    //   706: astore_2
    //   707: aload_0
    //   708: getfield 35	com/truecaller/api/services/messenger/v1/z$b:d	I
    //   711: istore 6
    //   713: aload_3
    //   714: getfield 35	com/truecaller/api/services/messenger/v1/z$b:d	I
    //   717: istore 9
    //   719: iload 9
    //   721: ifeq +6 -> 727
    //   724: goto +6 -> 730
    //   727: iconst_0
    //   728: istore 7
    //   730: aload_3
    //   731: getfield 35	com/truecaller/api/services/messenger/v1/z$b:d	I
    //   734: istore 21
    //   736: aload_1
    //   737: iload 15
    //   739: iload 6
    //   741: iload 7
    //   743: iload 21
    //   745: invokeinterface 148 5 0
    //   750: istore 5
    //   752: aload_0
    //   753: iload 5
    //   755: putfield 35	com/truecaller/api/services/messenger/v1/z$b:d	I
    //   758: aload_0
    //   759: areturn
    //   760: new 33	com/truecaller/api/services/messenger/v1/z$b$a
    //   763: astore_1
    //   764: aload_1
    //   765: iconst_0
    //   766: invokespecial 151	com/truecaller/api/services/messenger/v1/z$b$a:<init>	(B)V
    //   769: aload_1
    //   770: areturn
    //   771: aconst_null
    //   772: areturn
    //   773: getstatic 23	com/truecaller/api/services/messenger/v1/z$b:e	Lcom/truecaller/api/services/messenger/v1/z$b;
    //   776: areturn
    //   777: new 2	com/truecaller/api/services/messenger/v1/z$b
    //   780: astore_1
    //   781: aload_1
    //   782: invokespecial 21	com/truecaller/api/services/messenger/v1/z$b:<init>	()V
    //   785: aload_1
    //   786: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	787	0	this	b
    //   0	787	1	paramj	com.google.f.q.j
    //   0	787	2	paramObject1	Object
    //   0	787	3	paramObject2	Object
    //   3	667	4	localObject1	Object
    //   9	205	5	i	int
    //   218	3	5	bool1	boolean
    //   235	519	5	j	int
    //   19	721	6	k	int
    //   25	717	7	bool2	boolean
    //   28	423	8	m	int
    //   170	550	9	n	int
    //   281	319	10	localObject2	Object
    //   438	185	11	l1	long
    //   534	70	13	l2	long
    //   541	70	15	bool3	boolean
    //   689	49	15	i1	int
    //   549	58	16	bool4	boolean
    //   561	48	17	l3	long
    //   567	46	19	l4	long
    //   734	10	21	i2	int
    // Exception table:
    //   from	to	target	type
    //   103	106	133	finally
    //   111	114	133	finally
    //   115	118	133	finally
    //   120	124	133	finally
    //   124	128	133	finally
    //   128	130	133	finally
    //   134	136	133	finally
    //   157	161	455	finally
    //   213	218	455	finally
    //   231	235	455	finally
    //   238	243	455	finally
    //   246	250	455	finally
    //   255	259	455	finally
    //   260	264	455	finally
    //   265	269	455	finally
    //   278	281	455	finally
    //   286	290	455	finally
    //   292	297	455	finally
    //   300	305	455	finally
    //   309	313	455	finally
    //   316	322	455	finally
    //   322	326	455	finally
    //   327	331	455	finally
    //   333	337	455	finally
    //   340	344	455	finally
    //   349	353	455	finally
    //   354	358	455	finally
    //   359	363	455	finally
    //   372	375	455	finally
    //   380	384	455	finally
    //   386	391	455	finally
    //   394	399	455	finally
    //   403	407	455	finally
    //   410	416	455	finally
    //   416	420	455	finally
    //   421	425	455	finally
    //   427	431	455	finally
    //   434	438	455	finally
    //   441	446	455	finally
    //   460	463	455	finally
    //   464	467	455	finally
    //   468	472	455	finally
    //   474	478	455	finally
    //   479	483	455	finally
    //   485	489	455	finally
    //   489	491	455	finally
    //   492	495	455	finally
    //   497	501	455	finally
    //   503	507	455	finally
    //   507	509	455	finally
    //   157	161	459	java/io/IOException
    //   213	218	459	java/io/IOException
    //   231	235	459	java/io/IOException
    //   238	243	459	java/io/IOException
    //   246	250	459	java/io/IOException
    //   255	259	459	java/io/IOException
    //   260	264	459	java/io/IOException
    //   265	269	459	java/io/IOException
    //   278	281	459	java/io/IOException
    //   286	290	459	java/io/IOException
    //   292	297	459	java/io/IOException
    //   300	305	459	java/io/IOException
    //   309	313	459	java/io/IOException
    //   316	322	459	java/io/IOException
    //   322	326	459	java/io/IOException
    //   327	331	459	java/io/IOException
    //   333	337	459	java/io/IOException
    //   340	344	459	java/io/IOException
    //   349	353	459	java/io/IOException
    //   354	358	459	java/io/IOException
    //   359	363	459	java/io/IOException
    //   372	375	459	java/io/IOException
    //   380	384	459	java/io/IOException
    //   386	391	459	java/io/IOException
    //   394	399	459	java/io/IOException
    //   403	407	459	java/io/IOException
    //   410	416	459	java/io/IOException
    //   416	420	459	java/io/IOException
    //   421	425	459	java/io/IOException
    //   427	431	459	java/io/IOException
    //   434	438	459	java/io/IOException
    //   441	446	459	java/io/IOException
    //   157	161	491	com/google/f/x
    //   213	218	491	com/google/f/x
    //   231	235	491	com/google/f/x
    //   238	243	491	com/google/f/x
    //   246	250	491	com/google/f/x
    //   255	259	491	com/google/f/x
    //   260	264	491	com/google/f/x
    //   265	269	491	com/google/f/x
    //   278	281	491	com/google/f/x
    //   286	290	491	com/google/f/x
    //   292	297	491	com/google/f/x
    //   300	305	491	com/google/f/x
    //   309	313	491	com/google/f/x
    //   316	322	491	com/google/f/x
    //   322	326	491	com/google/f/x
    //   327	331	491	com/google/f/x
    //   333	337	491	com/google/f/x
    //   340	344	491	com/google/f/x
    //   349	353	491	com/google/f/x
    //   354	358	491	com/google/f/x
    //   359	363	491	com/google/f/x
    //   372	375	491	com/google/f/x
    //   380	384	491	com/google/f/x
    //   386	391	491	com/google/f/x
    //   394	399	491	com/google/f/x
    //   403	407	491	com/google/f/x
    //   410	416	491	com/google/f/x
    //   416	420	491	com/google/f/x
    //   421	425	491	com/google/f/x
    //   427	431	491	com/google/f/x
    //   434	438	491	com/google/f/x
    //   441	446	491	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int j = -1;
    if (i != j) {
      return i;
    }
    long l1 = a;
    long l2 = 0L;
    int k = 0;
    boolean bool = l1 < l2;
    if (bool)
    {
      int m = 1;
      i = h.computeInt64Size(m, l1);
      k = 0 + i;
    }
    InputPeer localInputPeer1 = b;
    InputPeer localInputPeer2;
    if (localInputPeer1 != null)
    {
      localInputPeer2 = d();
      i = h.computeMessageSize(2, localInputPeer2);
      k += i;
    }
    localInputPeer1 = c;
    if (localInputPeer1 != null)
    {
      localInputPeer2 = e();
      i = h.computeMessageSize(3, localInputPeer2);
      k += i;
    }
    i = d;
    if (i != 0)
    {
      j = 4;
      i = h.computeInt32Size(j, i);
      k += i;
    }
    memoizedSerializedSize = k;
    return k;
  }
  
  public final void writeTo(h paramh)
  {
    long l1 = a;
    long l2 = 0L;
    boolean bool = l1 < l2;
    if (bool)
    {
      int i = 1;
      paramh.writeInt64(i, l1);
    }
    InputPeer localInputPeer1 = b;
    InputPeer localInputPeer2;
    if (localInputPeer1 != null)
    {
      j = 2;
      localInputPeer2 = d();
      paramh.writeMessage(j, localInputPeer2);
    }
    localInputPeer1 = c;
    if (localInputPeer1 != null)
    {
      j = 3;
      localInputPeer2 = e();
      paramh.writeMessage(j, localInputPeer2);
    }
    int j = d;
    if (j != 0)
    {
      int k = 4;
      paramh.writeInt32(k, j);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.z.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */