package com.truecaller.api.services.messenger.v1;

import io.grpc.ap;
import io.grpc.c.a;
import io.grpc.c.d;
import io.grpc.e;

public final class k$b
  extends a
{
  private k$b(io.grpc.f paramf)
  {
    super(paramf);
  }
  
  private k$b(io.grpc.f paramf, e parame)
  {
    super(paramf, parame);
  }
  
  public final io.grpc.c.f a(io.grpc.c.f paramf)
  {
    io.grpc.f localf = getChannel();
    ap localap = k.c();
    e locale = getCallOptions();
    return d.a(localf.a(localap, locale), paramf);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.k.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */