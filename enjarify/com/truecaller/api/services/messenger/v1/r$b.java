package com.truecaller.api.services.messenger.v1;

import com.google.f.ae;
import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;
import com.truecaller.api.services.messenger.v1.models.input.InputPeer;
import com.truecaller.api.services.messenger.v1.models.input.InputReactionContent;

public final class r$b
  extends q
  implements r.c
{
  private static final b c;
  private static volatile ah d;
  private InputPeer a;
  private InputReactionContent b;
  
  static
  {
    b localb = new com/truecaller/api/services/messenger/v1/r$b;
    localb.<init>();
    c = localb;
    localb.makeImmutable();
  }
  
  public static r.b.a a()
  {
    return (r.b.a)c.toBuilder();
  }
  
  public static b b()
  {
    return c;
  }
  
  public static ah c()
  {
    return c.getParserForType();
  }
  
  private InputPeer e()
  {
    InputPeer localInputPeer = a;
    if (localInputPeer == null) {
      localInputPeer = InputPeer.b();
    }
    return localInputPeer;
  }
  
  private InputReactionContent f()
  {
    InputReactionContent localInputReactionContent = b;
    if (localInputReactionContent == null) {
      localInputReactionContent = InputReactionContent.b();
    }
    return localInputReactionContent;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 62	com/truecaller/api/services/messenger/v1/r$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 68	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_0
    //   19: istore 6
    //   21: aconst_null
    //   22: astore 4
    //   24: iload 5
    //   26: tableswitch	default:+46->72, 1:+547->573, 2:+543->569, 3:+541->567, 4:+530->556, 5:+462->488, 6:+108->134, 7:+458->484, 8:+56->82
    //   72: new 70	java/lang/UnsupportedOperationException
    //   75: astore_1
    //   76: aload_1
    //   77: invokespecial 71	java/lang/UnsupportedOperationException:<init>	()V
    //   80: aload_1
    //   81: athrow
    //   82: getstatic 73	com/truecaller/api/services/messenger/v1/r$b:d	Lcom/google/f/ah;
    //   85: astore_1
    //   86: aload_1
    //   87: ifnonnull +43 -> 130
    //   90: ldc 2
    //   92: astore_1
    //   93: aload_1
    //   94: monitorenter
    //   95: getstatic 73	com/truecaller/api/services/messenger/v1/r$b:d	Lcom/google/f/ah;
    //   98: astore_2
    //   99: aload_2
    //   100: ifnonnull +20 -> 120
    //   103: new 75	com/google/f/q$b
    //   106: astore_2
    //   107: getstatic 20	com/truecaller/api/services/messenger/v1/r$b:c	Lcom/truecaller/api/services/messenger/v1/r$b;
    //   110: astore_3
    //   111: aload_2
    //   112: aload_3
    //   113: invokespecial 78	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   116: aload_2
    //   117: putstatic 73	com/truecaller/api/services/messenger/v1/r$b:d	Lcom/google/f/ah;
    //   120: aload_1
    //   121: monitorexit
    //   122: goto +8 -> 130
    //   125: astore_2
    //   126: aload_1
    //   127: monitorexit
    //   128: aload_2
    //   129: athrow
    //   130: getstatic 73	com/truecaller/api/services/messenger/v1/r$b:d	Lcom/google/f/ah;
    //   133: areturn
    //   134: aload_2
    //   135: checkcast 80	com/google/f/g
    //   138: astore_2
    //   139: aload_3
    //   140: checkcast 82	com/google/f/n
    //   143: astore_3
    //   144: iconst_1
    //   145: istore 5
    //   147: iload 6
    //   149: ifne +335 -> 484
    //   152: aload_2
    //   153: invokevirtual 86	com/google/f/g:readTag	()I
    //   156: istore 7
    //   158: iload 7
    //   160: ifeq +262 -> 422
    //   163: bipush 10
    //   165: istore 8
    //   167: iload 7
    //   169: iload 8
    //   171: if_icmpeq +142 -> 313
    //   174: bipush 18
    //   176: istore 8
    //   178: iload 7
    //   180: iload 8
    //   182: if_icmpeq +22 -> 204
    //   185: aload_2
    //   186: iload 7
    //   188: invokevirtual 92	com/google/f/g:skipField	(I)Z
    //   191: istore 7
    //   193: iload 7
    //   195: ifne -48 -> 147
    //   198: iconst_1
    //   199: istore 6
    //   201: goto -54 -> 147
    //   204: aload_0
    //   205: getfield 47	com/truecaller/api/services/messenger/v1/r$b:b	Lcom/truecaller/api/services/messenger/v1/models/input/InputReactionContent;
    //   208: astore 9
    //   210: aload 9
    //   212: ifnull +26 -> 238
    //   215: aload_0
    //   216: getfield 47	com/truecaller/api/services/messenger/v1/r$b:b	Lcom/truecaller/api/services/messenger/v1/models/input/InputReactionContent;
    //   219: astore 9
    //   221: aload 9
    //   223: invokevirtual 93	com/truecaller/api/services/messenger/v1/models/input/InputReactionContent:toBuilder	()Lcom/google/f/q$a;
    //   226: astore 9
    //   228: aload 9
    //   230: checkcast 42	com/truecaller/api/services/messenger/v1/models/input/InputReactionContent$a
    //   233: astore 9
    //   235: goto +9 -> 244
    //   238: iconst_0
    //   239: istore 7
    //   241: aconst_null
    //   242: astore 9
    //   244: invokestatic 95	com/truecaller/api/services/messenger/v1/models/input/InputReactionContent:c	()Lcom/google/f/ah;
    //   247: astore 10
    //   249: aload_2
    //   250: aload 10
    //   252: aload_3
    //   253: invokevirtual 99	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   256: astore 10
    //   258: aload 10
    //   260: checkcast 45	com/truecaller/api/services/messenger/v1/models/input/InputReactionContent
    //   263: astore 10
    //   265: aload_0
    //   266: aload 10
    //   268: putfield 47	com/truecaller/api/services/messenger/v1/r$b:b	Lcom/truecaller/api/services/messenger/v1/models/input/InputReactionContent;
    //   271: aload 9
    //   273: ifnull -126 -> 147
    //   276: aload_0
    //   277: getfield 47	com/truecaller/api/services/messenger/v1/r$b:b	Lcom/truecaller/api/services/messenger/v1/models/input/InputReactionContent;
    //   280: astore 10
    //   282: aload 9
    //   284: aload 10
    //   286: invokevirtual 103	com/truecaller/api/services/messenger/v1/models/input/InputReactionContent$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   289: pop
    //   290: aload 9
    //   292: invokevirtual 106	com/truecaller/api/services/messenger/v1/models/input/InputReactionContent$a:buildPartial	()Lcom/google/f/q;
    //   295: astore 9
    //   297: aload 9
    //   299: checkcast 45	com/truecaller/api/services/messenger/v1/models/input/InputReactionContent
    //   302: astore 9
    //   304: aload_0
    //   305: aload 9
    //   307: putfield 47	com/truecaller/api/services/messenger/v1/r$b:b	Lcom/truecaller/api/services/messenger/v1/models/input/InputReactionContent;
    //   310: goto -163 -> 147
    //   313: aload_0
    //   314: getfield 40	com/truecaller/api/services/messenger/v1/r$b:a	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   317: astore 9
    //   319: aload 9
    //   321: ifnull +26 -> 347
    //   324: aload_0
    //   325: getfield 40	com/truecaller/api/services/messenger/v1/r$b:a	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   328: astore 9
    //   330: aload 9
    //   332: invokevirtual 107	com/truecaller/api/services/messenger/v1/models/input/InputPeer:toBuilder	()Lcom/google/f/q$a;
    //   335: astore 9
    //   337: aload 9
    //   339: checkcast 32	com/truecaller/api/services/messenger/v1/models/input/InputPeer$a
    //   342: astore 9
    //   344: goto +9 -> 353
    //   347: iconst_0
    //   348: istore 7
    //   350: aconst_null
    //   351: astore 9
    //   353: invokestatic 108	com/truecaller/api/services/messenger/v1/models/input/InputPeer:c	()Lcom/google/f/ah;
    //   356: astore 10
    //   358: aload_2
    //   359: aload 10
    //   361: aload_3
    //   362: invokevirtual 99	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   365: astore 10
    //   367: aload 10
    //   369: checkcast 38	com/truecaller/api/services/messenger/v1/models/input/InputPeer
    //   372: astore 10
    //   374: aload_0
    //   375: aload 10
    //   377: putfield 40	com/truecaller/api/services/messenger/v1/r$b:a	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   380: aload 9
    //   382: ifnull -235 -> 147
    //   385: aload_0
    //   386: getfield 40	com/truecaller/api/services/messenger/v1/r$b:a	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   389: astore 10
    //   391: aload 9
    //   393: aload 10
    //   395: invokevirtual 109	com/truecaller/api/services/messenger/v1/models/input/InputPeer$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   398: pop
    //   399: aload 9
    //   401: invokevirtual 110	com/truecaller/api/services/messenger/v1/models/input/InputPeer$a:buildPartial	()Lcom/google/f/q;
    //   404: astore 9
    //   406: aload 9
    //   408: checkcast 38	com/truecaller/api/services/messenger/v1/models/input/InputPeer
    //   411: astore 9
    //   413: aload_0
    //   414: aload 9
    //   416: putfield 40	com/truecaller/api/services/messenger/v1/r$b:a	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   419: goto -272 -> 147
    //   422: iconst_1
    //   423: istore 6
    //   425: goto -278 -> 147
    //   428: astore_1
    //   429: goto +53 -> 482
    //   432: astore_1
    //   433: new 112	java/lang/RuntimeException
    //   436: astore_2
    //   437: new 114	com/google/f/x
    //   440: astore_3
    //   441: aload_1
    //   442: invokevirtual 120	java/io/IOException:getMessage	()Ljava/lang/String;
    //   445: astore_1
    //   446: aload_3
    //   447: aload_1
    //   448: invokespecial 123	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   451: aload_3
    //   452: aload_0
    //   453: invokevirtual 127	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   456: astore_1
    //   457: aload_2
    //   458: aload_1
    //   459: invokespecial 130	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   462: aload_2
    //   463: athrow
    //   464: astore_1
    //   465: new 112	java/lang/RuntimeException
    //   468: astore_2
    //   469: aload_1
    //   470: aload_0
    //   471: invokevirtual 127	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   474: astore_1
    //   475: aload_2
    //   476: aload_1
    //   477: invokespecial 130	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   480: aload_2
    //   481: athrow
    //   482: aload_1
    //   483: athrow
    //   484: getstatic 20	com/truecaller/api/services/messenger/v1/r$b:c	Lcom/truecaller/api/services/messenger/v1/r$b;
    //   487: areturn
    //   488: aload_2
    //   489: checkcast 132	com/google/f/q$k
    //   492: astore_2
    //   493: aload_3
    //   494: checkcast 2	com/truecaller/api/services/messenger/v1/r$b
    //   497: astore_3
    //   498: aload_0
    //   499: getfield 40	com/truecaller/api/services/messenger/v1/r$b:a	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   502: astore_1
    //   503: aload_3
    //   504: getfield 40	com/truecaller/api/services/messenger/v1/r$b:a	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   507: astore 4
    //   509: aload_2
    //   510: aload_1
    //   511: aload 4
    //   513: invokeinterface 136 3 0
    //   518: checkcast 38	com/truecaller/api/services/messenger/v1/models/input/InputPeer
    //   521: astore_1
    //   522: aload_0
    //   523: aload_1
    //   524: putfield 40	com/truecaller/api/services/messenger/v1/r$b:a	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   527: aload_0
    //   528: getfield 47	com/truecaller/api/services/messenger/v1/r$b:b	Lcom/truecaller/api/services/messenger/v1/models/input/InputReactionContent;
    //   531: astore_1
    //   532: aload_3
    //   533: getfield 47	com/truecaller/api/services/messenger/v1/r$b:b	Lcom/truecaller/api/services/messenger/v1/models/input/InputReactionContent;
    //   536: astore_3
    //   537: aload_2
    //   538: aload_1
    //   539: aload_3
    //   540: invokeinterface 136 3 0
    //   545: checkcast 45	com/truecaller/api/services/messenger/v1/models/input/InputReactionContent
    //   548: astore_1
    //   549: aload_0
    //   550: aload_1
    //   551: putfield 47	com/truecaller/api/services/messenger/v1/r$b:b	Lcom/truecaller/api/services/messenger/v1/models/input/InputReactionContent;
    //   554: aload_0
    //   555: areturn
    //   556: new 30	com/truecaller/api/services/messenger/v1/r$b$a
    //   559: astore_1
    //   560: aload_1
    //   561: iconst_0
    //   562: invokespecial 139	com/truecaller/api/services/messenger/v1/r$b$a:<init>	(B)V
    //   565: aload_1
    //   566: areturn
    //   567: aconst_null
    //   568: areturn
    //   569: getstatic 20	com/truecaller/api/services/messenger/v1/r$b:c	Lcom/truecaller/api/services/messenger/v1/r$b;
    //   572: areturn
    //   573: new 2	com/truecaller/api/services/messenger/v1/r$b
    //   576: astore_1
    //   577: aload_1
    //   578: invokespecial 18	com/truecaller/api/services/messenger/v1/r$b:<init>	()V
    //   581: aload_1
    //   582: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	583	0	this	b
    //   0	583	1	paramj	com.google.f.q.j
    //   0	583	2	paramObject1	Object
    //   0	583	3	paramObject2	Object
    //   3	509	4	localObject1	Object
    //   9	137	5	i	int
    //   19	405	6	j	int
    //   156	31	7	k	int
    //   191	158	7	bool	boolean
    //   165	18	8	m	int
    //   208	207	9	localObject2	Object
    //   247	147	10	localObject3	Object
    // Exception table:
    //   from	to	target	type
    //   95	98	125	finally
    //   103	106	125	finally
    //   107	110	125	finally
    //   112	116	125	finally
    //   116	120	125	finally
    //   120	122	125	finally
    //   126	128	125	finally
    //   152	156	428	finally
    //   186	191	428	finally
    //   204	208	428	finally
    //   215	219	428	finally
    //   221	226	428	finally
    //   228	233	428	finally
    //   244	247	428	finally
    //   252	256	428	finally
    //   258	263	428	finally
    //   266	271	428	finally
    //   276	280	428	finally
    //   284	290	428	finally
    //   290	295	428	finally
    //   297	302	428	finally
    //   305	310	428	finally
    //   313	317	428	finally
    //   324	328	428	finally
    //   330	335	428	finally
    //   337	342	428	finally
    //   353	356	428	finally
    //   361	365	428	finally
    //   367	372	428	finally
    //   375	380	428	finally
    //   385	389	428	finally
    //   393	399	428	finally
    //   399	404	428	finally
    //   406	411	428	finally
    //   414	419	428	finally
    //   433	436	428	finally
    //   437	440	428	finally
    //   441	445	428	finally
    //   447	451	428	finally
    //   452	456	428	finally
    //   458	462	428	finally
    //   462	464	428	finally
    //   465	468	428	finally
    //   470	474	428	finally
    //   476	480	428	finally
    //   480	482	428	finally
    //   152	156	432	java/io/IOException
    //   186	191	432	java/io/IOException
    //   204	208	432	java/io/IOException
    //   215	219	432	java/io/IOException
    //   221	226	432	java/io/IOException
    //   228	233	432	java/io/IOException
    //   244	247	432	java/io/IOException
    //   252	256	432	java/io/IOException
    //   258	263	432	java/io/IOException
    //   266	271	432	java/io/IOException
    //   276	280	432	java/io/IOException
    //   284	290	432	java/io/IOException
    //   290	295	432	java/io/IOException
    //   297	302	432	java/io/IOException
    //   305	310	432	java/io/IOException
    //   313	317	432	java/io/IOException
    //   324	328	432	java/io/IOException
    //   330	335	432	java/io/IOException
    //   337	342	432	java/io/IOException
    //   353	356	432	java/io/IOException
    //   361	365	432	java/io/IOException
    //   367	372	432	java/io/IOException
    //   375	380	432	java/io/IOException
    //   385	389	432	java/io/IOException
    //   393	399	432	java/io/IOException
    //   399	404	432	java/io/IOException
    //   406	411	432	java/io/IOException
    //   414	419	432	java/io/IOException
    //   152	156	464	com/google/f/x
    //   186	191	464	com/google/f/x
    //   204	208	464	com/google/f/x
    //   215	219	464	com/google/f/x
    //   221	226	464	com/google/f/x
    //   228	233	464	com/google/f/x
    //   244	247	464	com/google/f/x
    //   252	256	464	com/google/f/x
    //   258	263	464	com/google/f/x
    //   266	271	464	com/google/f/x
    //   276	280	464	com/google/f/x
    //   284	290	464	com/google/f/x
    //   290	295	464	com/google/f/x
    //   297	302	464	com/google/f/x
    //   305	310	464	com/google/f/x
    //   313	317	464	com/google/f/x
    //   324	328	464	com/google/f/x
    //   330	335	464	com/google/f/x
    //   337	342	464	com/google/f/x
    //   353	356	464	com/google/f/x
    //   361	365	464	com/google/f/x
    //   367	372	464	com/google/f/x
    //   375	380	464	com/google/f/x
    //   385	389	464	com/google/f/x
    //   393	399	464	com/google/f/x
    //   399	404	464	com/google/f/x
    //   406	411	464	com/google/f/x
    //   414	419	464	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int j = -1;
    if (i != j) {
      return i;
    }
    Object localObject1 = a;
    j = 0;
    Object localObject2;
    if (localObject1 != null)
    {
      localObject2 = e();
      i = h.computeMessageSize(1, (ae)localObject2);
      j = 0 + i;
    }
    localObject1 = b;
    if (localObject1 != null)
    {
      localObject2 = f();
      i = h.computeMessageSize(2, (ae)localObject2);
      j += i;
    }
    memoizedSerializedSize = j;
    return j;
  }
  
  public final void writeTo(h paramh)
  {
    Object localObject1 = a;
    int i;
    Object localObject2;
    if (localObject1 != null)
    {
      i = 1;
      localObject2 = e();
      paramh.writeMessage(i, (ae)localObject2);
    }
    localObject1 = b;
    if (localObject1 != null)
    {
      i = 2;
      localObject2 = f();
      paramh.writeMessage(i, (ae)localObject2);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.r.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */