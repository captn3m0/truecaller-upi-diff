package com.truecaller.api.services.messenger.v1;

import com.google.f.q.a;
import com.truecaller.api.services.messenger.v1.models.input.a.a;

public final class v$b$a
  extends q.a
  implements v.c
{
  private v$b$a()
  {
    super(localb);
  }
  
  public final a a(long paramLong)
  {
    copyOnWrite();
    v.b.a((v.b)instance, paramLong);
    return this;
  }
  
  public final a a(a.a parama)
  {
    copyOnWrite();
    v.b.a((v.b)instance, parama);
    return this;
  }
  
  public final a a(String paramString)
  {
    copyOnWrite();
    v.b.a((v.b)instance, paramString);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.v.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */