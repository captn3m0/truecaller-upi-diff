package com.truecaller.api.services.messenger.v1.events;

import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;

public final class Event$a
  extends q
  implements Event.b
{
  private static final a b;
  private static volatile ah c;
  private long a;
  
  static
  {
    a locala = new com/truecaller/api/services/messenger/v1/events/Event$a;
    locala.<init>();
    b = locala;
    locala.makeImmutable();
  }
  
  public static Event.a.a a()
  {
    return (Event.a.a)b.toBuilder();
  }
  
  public static a b()
  {
    return b;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 35	com/truecaller/api/services/messenger/v1/events/Event$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 41	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_0
    //   19: istore 6
    //   21: iload 5
    //   23: tableswitch	default:+45->68, 1:+370->393, 2:+366->389, 3:+364->387, 4:+353->376, 5:+239->262, 6:+107->130, 7:+235->258, 8:+55->78
    //   68: new 44	java/lang/UnsupportedOperationException
    //   71: astore_1
    //   72: aload_1
    //   73: invokespecial 45	java/lang/UnsupportedOperationException:<init>	()V
    //   76: aload_1
    //   77: athrow
    //   78: getstatic 47	com/truecaller/api/services/messenger/v1/events/Event$a:c	Lcom/google/f/ah;
    //   81: astore_1
    //   82: aload_1
    //   83: ifnonnull +43 -> 126
    //   86: ldc 2
    //   88: astore_1
    //   89: aload_1
    //   90: monitorenter
    //   91: getstatic 47	com/truecaller/api/services/messenger/v1/events/Event$a:c	Lcom/google/f/ah;
    //   94: astore_2
    //   95: aload_2
    //   96: ifnonnull +20 -> 116
    //   99: new 49	com/google/f/q$b
    //   102: astore_2
    //   103: getstatic 18	com/truecaller/api/services/messenger/v1/events/Event$a:b	Lcom/truecaller/api/services/messenger/v1/events/Event$a;
    //   106: astore_3
    //   107: aload_2
    //   108: aload_3
    //   109: invokespecial 52	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   112: aload_2
    //   113: putstatic 47	com/truecaller/api/services/messenger/v1/events/Event$a:c	Lcom/google/f/ah;
    //   116: aload_1
    //   117: monitorexit
    //   118: goto +8 -> 126
    //   121: astore_2
    //   122: aload_1
    //   123: monitorexit
    //   124: aload_2
    //   125: athrow
    //   126: getstatic 47	com/truecaller/api/services/messenger/v1/events/Event$a:c	Lcom/google/f/ah;
    //   129: areturn
    //   130: aload_2
    //   131: checkcast 54	com/google/f/g
    //   134: astore_2
    //   135: iload 6
    //   137: ifne +121 -> 258
    //   140: aload_2
    //   141: invokevirtual 57	com/google/f/g:readTag	()I
    //   144: istore 5
    //   146: iload 5
    //   148: ifeq +48 -> 196
    //   151: bipush 8
    //   153: istore 7
    //   155: iload 5
    //   157: iload 7
    //   159: if_icmpeq +22 -> 181
    //   162: aload_2
    //   163: iload 5
    //   165: invokevirtual 62	com/google/f/g:skipField	(I)Z
    //   168: istore 5
    //   170: iload 5
    //   172: ifne -37 -> 135
    //   175: iconst_1
    //   176: istore 6
    //   178: goto -43 -> 135
    //   181: aload_2
    //   182: invokevirtual 66	com/google/f/g:readInt64	()J
    //   185: lstore 8
    //   187: aload_0
    //   188: lload 8
    //   190: putfield 30	com/truecaller/api/services/messenger/v1/events/Event$a:a	J
    //   193: goto -58 -> 135
    //   196: iconst_1
    //   197: istore 6
    //   199: goto -64 -> 135
    //   202: astore_1
    //   203: goto +53 -> 256
    //   206: astore_1
    //   207: new 68	java/lang/RuntimeException
    //   210: astore_2
    //   211: new 70	com/google/f/x
    //   214: astore_3
    //   215: aload_1
    //   216: invokevirtual 76	java/io/IOException:getMessage	()Ljava/lang/String;
    //   219: astore_1
    //   220: aload_3
    //   221: aload_1
    //   222: invokespecial 79	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   225: aload_3
    //   226: aload_0
    //   227: invokevirtual 83	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   230: astore_1
    //   231: aload_2
    //   232: aload_1
    //   233: invokespecial 86	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   236: aload_2
    //   237: athrow
    //   238: astore_1
    //   239: new 68	java/lang/RuntimeException
    //   242: astore_2
    //   243: aload_1
    //   244: aload_0
    //   245: invokevirtual 83	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   248: astore_1
    //   249: aload_2
    //   250: aload_1
    //   251: invokespecial 86	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   254: aload_2
    //   255: athrow
    //   256: aload_1
    //   257: athrow
    //   258: getstatic 18	com/truecaller/api/services/messenger/v1/events/Event$a:b	Lcom/truecaller/api/services/messenger/v1/events/Event$a;
    //   261: areturn
    //   262: aload_2
    //   263: astore_1
    //   264: aload_2
    //   265: checkcast 88	com/google/f/q$k
    //   268: astore_1
    //   269: aload_3
    //   270: checkcast 2	com/truecaller/api/services/messenger/v1/events/Event$a
    //   273: astore_3
    //   274: aload_0
    //   275: getfield 30	com/truecaller/api/services/messenger/v1/events/Event$a:a	J
    //   278: lstore 8
    //   280: lconst_0
    //   281: lstore 10
    //   283: lload 8
    //   285: lload 10
    //   287: lcmp
    //   288: istore 12
    //   290: iload 12
    //   292: ifeq +9 -> 301
    //   295: iconst_1
    //   296: istore 12
    //   298: goto +8 -> 306
    //   301: iconst_0
    //   302: istore 12
    //   304: aconst_null
    //   305: astore_2
    //   306: aload_0
    //   307: getfield 30	com/truecaller/api/services/messenger/v1/events/Event$a:a	J
    //   310: lstore 8
    //   312: aload_3
    //   313: getfield 30	com/truecaller/api/services/messenger/v1/events/Event$a:a	J
    //   316: lstore 13
    //   318: lload 13
    //   320: lload 10
    //   322: lcmp
    //   323: istore 15
    //   325: iload 15
    //   327: ifeq +9 -> 336
    //   330: iconst_1
    //   331: istore 16
    //   333: goto +6 -> 339
    //   336: iconst_0
    //   337: istore 16
    //   339: aload_3
    //   340: getfield 30	com/truecaller/api/services/messenger/v1/events/Event$a:a	J
    //   343: lstore 17
    //   345: aload_1
    //   346: astore 4
    //   348: iload 12
    //   350: istore 6
    //   352: aload_1
    //   353: iload 12
    //   355: lload 8
    //   357: iload 16
    //   359: lload 17
    //   361: invokeinterface 92 7 0
    //   366: lstore 19
    //   368: aload_0
    //   369: lload 19
    //   371: putfield 30	com/truecaller/api/services/messenger/v1/events/Event$a:a	J
    //   374: aload_0
    //   375: areturn
    //   376: new 28	com/truecaller/api/services/messenger/v1/events/Event$a$a
    //   379: astore_1
    //   380: aload_1
    //   381: iconst_0
    //   382: invokespecial 95	com/truecaller/api/services/messenger/v1/events/Event$a$a:<init>	(B)V
    //   385: aload_1
    //   386: areturn
    //   387: aconst_null
    //   388: areturn
    //   389: getstatic 18	com/truecaller/api/services/messenger/v1/events/Event$a:b	Lcom/truecaller/api/services/messenger/v1/events/Event$a;
    //   392: areturn
    //   393: new 2	com/truecaller/api/services/messenger/v1/events/Event$a
    //   396: astore_1
    //   397: aload_1
    //   398: invokespecial 16	com/truecaller/api/services/messenger/v1/events/Event$a:<init>	()V
    //   401: aload_1
    //   402: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	403	0	this	a
    //   0	403	1	paramj	com.google.f.q.j
    //   0	403	2	paramObject1	Object
    //   0	403	3	paramObject2	Object
    //   3	344	4	localObject	Object
    //   9	155	5	i	int
    //   168	3	5	bool1	boolean
    //   19	332	6	j	int
    //   153	7	7	k	int
    //   185	171	8	l1	long
    //   281	40	10	l2	long
    //   288	66	12	bool2	boolean
    //   316	3	13	l3	long
    //   323	3	15	bool3	boolean
    //   331	27	16	bool4	boolean
    //   343	17	17	l4	long
    //   366	4	19	l5	long
    // Exception table:
    //   from	to	target	type
    //   91	94	121	finally
    //   99	102	121	finally
    //   103	106	121	finally
    //   108	112	121	finally
    //   112	116	121	finally
    //   116	118	121	finally
    //   122	124	121	finally
    //   140	144	202	finally
    //   163	168	202	finally
    //   181	185	202	finally
    //   188	193	202	finally
    //   207	210	202	finally
    //   211	214	202	finally
    //   215	219	202	finally
    //   221	225	202	finally
    //   226	230	202	finally
    //   232	236	202	finally
    //   236	238	202	finally
    //   239	242	202	finally
    //   244	248	202	finally
    //   250	254	202	finally
    //   254	256	202	finally
    //   140	144	206	java/io/IOException
    //   163	168	206	java/io/IOException
    //   181	185	206	java/io/IOException
    //   188	193	206	java/io/IOException
    //   140	144	238	com/google/f/x
    //   163	168	238	com/google/f/x
    //   181	185	238	com/google/f/x
    //   188	193	238	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int j = -1;
    if (i != j) {
      return i;
    }
    long l1 = a;
    long l2 = 0L;
    int k = 0;
    boolean bool = l1 < l2;
    if (bool)
    {
      int m = 1;
      i = h.computeInt64Size(m, l1);
      k = 0 + i;
    }
    memoizedSerializedSize = k;
    return k;
  }
  
  public final void writeTo(h paramh)
  {
    long l1 = a;
    long l2 = 0L;
    boolean bool = l1 < l2;
    if (bool)
    {
      int i = 1;
      paramh.writeInt64(i, l1);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.events.Event.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */