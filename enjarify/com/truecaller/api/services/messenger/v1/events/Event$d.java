package com.truecaller.api.services.messenger.v1.events;

import com.google.f.ac;
import com.google.f.ad;
import com.google.f.ae;
import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;
import com.google.f.w.h;
import com.truecaller.api.services.messenger.v1.models.Peer;
import com.truecaller.api.services.messenger.v1.models.a;
import com.truecaller.api.services.messenger.v1.models.c;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class Event$d
  extends q
  implements Event.e
{
  private static final d m;
  private static volatile ah n;
  private int a;
  private Peer b;
  private String c;
  private String d;
  private int e;
  private w.h f;
  private c g;
  private ad h;
  private int i;
  private int j;
  private w.h k;
  private a l;
  
  static
  {
    d locald = new com/truecaller/api/services/messenger/v1/events/Event$d;
    locald.<init>();
    m = locald;
    locald.makeImmutable();
  }
  
  private Event$d()
  {
    Object localObject = ad.emptyMapField();
    h = ((ad)localObject);
    c = "";
    d = "";
    localObject = emptyProtobufList();
    f = ((w.h)localObject);
    localObject = emptyProtobufList();
    k = ((w.h)localObject);
  }
  
  public static d j()
  {
    return m;
  }
  
  public static ah k()
  {
    return m.getParserForType();
  }
  
  public final Peer a()
  {
    Peer localPeer = b;
    if (localPeer == null) {
      localPeer = Peer.d();
    }
    return localPeer;
  }
  
  public final String b()
  {
    return c;
  }
  
  public final String c()
  {
    return d;
  }
  
  public final int d()
  {
    return e;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 79	com/truecaller/api/services/messenger/v1/events/Event$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 85	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_0
    //   19: istore 6
    //   21: aconst_null
    //   22: astore 4
    //   24: iconst_0
    //   25: istore 7
    //   27: iconst_1
    //   28: istore 8
    //   30: iload 5
    //   32: tableswitch	default:+48->80, 1:+1458->1490, 2:+1454->1486, 3:+1427->1459, 4:+1416->1448, 5:+861->893, 6:+110->142, 7:+857->889, 8:+58->90
    //   80: new 88	java/lang/UnsupportedOperationException
    //   83: astore_1
    //   84: aload_1
    //   85: invokespecial 89	java/lang/UnsupportedOperationException:<init>	()V
    //   88: aload_1
    //   89: athrow
    //   90: getstatic 91	com/truecaller/api/services/messenger/v1/events/Event$d:n	Lcom/google/f/ah;
    //   93: astore_1
    //   94: aload_1
    //   95: ifnonnull +43 -> 138
    //   98: ldc 2
    //   100: astore_1
    //   101: aload_1
    //   102: monitorenter
    //   103: getstatic 91	com/truecaller/api/services/messenger/v1/events/Event$d:n	Lcom/google/f/ah;
    //   106: astore_2
    //   107: aload_2
    //   108: ifnonnull +20 -> 128
    //   111: new 93	com/google/f/q$b
    //   114: astore_2
    //   115: getstatic 35	com/truecaller/api/services/messenger/v1/events/Event$d:m	Lcom/truecaller/api/services/messenger/v1/events/Event$d;
    //   118: astore_3
    //   119: aload_2
    //   120: aload_3
    //   121: invokespecial 96	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   124: aload_2
    //   125: putstatic 91	com/truecaller/api/services/messenger/v1/events/Event$d:n	Lcom/google/f/ah;
    //   128: aload_1
    //   129: monitorexit
    //   130: goto +8 -> 138
    //   133: astore_2
    //   134: aload_1
    //   135: monitorexit
    //   136: aload_2
    //   137: athrow
    //   138: getstatic 91	com/truecaller/api/services/messenger/v1/events/Event$d:n	Lcom/google/f/ah;
    //   141: areturn
    //   142: aload_2
    //   143: checkcast 98	com/google/f/g
    //   146: astore_2
    //   147: aload_3
    //   148: checkcast 100	com/google/f/n
    //   151: astore_3
    //   152: iload 7
    //   154: ifne +735 -> 889
    //   157: aload_2
    //   158: invokevirtual 103	com/google/f/g:readTag	()I
    //   161: istore 5
    //   163: iload 5
    //   165: lookupswitch	default:+107->272, 0:+651->816, 10:+557->722, 18:+544->709, 26:+531->696, 32:+516->681, 42:+452->617, 50:+358->523, 58:+306->471, 64:+291->456, 72:+276->441, 82:+212->377, 90:+118->283
    //   272: aload_2
    //   273: iload 5
    //   275: invokevirtual 107	com/google/f/g:skipField	(I)Z
    //   278: istore 5
    //   280: goto +542 -> 822
    //   283: aload_0
    //   284: getfield 109	com/truecaller/api/services/messenger/v1/events/Event$d:l	Lcom/truecaller/api/services/messenger/v1/models/a;
    //   287: astore_1
    //   288: aload_1
    //   289: ifnull +21 -> 310
    //   292: aload_0
    //   293: getfield 109	com/truecaller/api/services/messenger/v1/events/Event$d:l	Lcom/truecaller/api/services/messenger/v1/models/a;
    //   296: astore_1
    //   297: aload_1
    //   298: invokevirtual 115	com/truecaller/api/services/messenger/v1/models/a:toBuilder	()Lcom/google/f/q$a;
    //   301: astore_1
    //   302: aload_1
    //   303: checkcast 117	com/truecaller/api/services/messenger/v1/models/a$a
    //   306: astore_1
    //   307: goto +8 -> 315
    //   310: iconst_0
    //   311: istore 5
    //   313: aconst_null
    //   314: astore_1
    //   315: invokestatic 119	com/truecaller/api/services/messenger/v1/models/a:f	()Lcom/google/f/ah;
    //   318: astore 9
    //   320: aload_2
    //   321: aload 9
    //   323: aload_3
    //   324: invokevirtual 123	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   327: astore 9
    //   329: aload 9
    //   331: checkcast 111	com/truecaller/api/services/messenger/v1/models/a
    //   334: astore 9
    //   336: aload_0
    //   337: aload 9
    //   339: putfield 109	com/truecaller/api/services/messenger/v1/events/Event$d:l	Lcom/truecaller/api/services/messenger/v1/models/a;
    //   342: aload_1
    //   343: ifnull -191 -> 152
    //   346: aload_0
    //   347: getfield 109	com/truecaller/api/services/messenger/v1/events/Event$d:l	Lcom/truecaller/api/services/messenger/v1/models/a;
    //   350: astore 9
    //   352: aload_1
    //   353: aload 9
    //   355: invokevirtual 127	com/truecaller/api/services/messenger/v1/models/a$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   358: pop
    //   359: aload_1
    //   360: invokevirtual 131	com/truecaller/api/services/messenger/v1/models/a$a:buildPartial	()Lcom/google/f/q;
    //   363: astore_1
    //   364: aload_1
    //   365: checkcast 111	com/truecaller/api/services/messenger/v1/models/a
    //   368: astore_1
    //   369: aload_0
    //   370: aload_1
    //   371: putfield 109	com/truecaller/api/services/messenger/v1/events/Event$d:l	Lcom/truecaller/api/services/messenger/v1/models/a;
    //   374: goto -222 -> 152
    //   377: aload_0
    //   378: getfield 61	com/truecaller/api/services/messenger/v1/events/Event$d:k	Lcom/google/f/w$h;
    //   381: astore_1
    //   382: aload_1
    //   383: invokeinterface 137 1 0
    //   388: istore 5
    //   390: iload 5
    //   392: ifne +18 -> 410
    //   395: aload_0
    //   396: getfield 61	com/truecaller/api/services/messenger/v1/events/Event$d:k	Lcom/google/f/w$h;
    //   399: astore_1
    //   400: aload_1
    //   401: invokestatic 141	com/google/f/q:mutableCopy	(Lcom/google/f/w$h;)Lcom/google/f/w$h;
    //   404: astore_1
    //   405: aload_0
    //   406: aload_1
    //   407: putfield 61	com/truecaller/api/services/messenger/v1/events/Event$d:k	Lcom/google/f/w$h;
    //   410: aload_0
    //   411: getfield 61	com/truecaller/api/services/messenger/v1/events/Event$d:k	Lcom/google/f/w$h;
    //   414: astore_1
    //   415: invokestatic 145	com/truecaller/api/services/messenger/v1/models/f:c	()Lcom/google/f/ah;
    //   418: astore 9
    //   420: aload_2
    //   421: aload 9
    //   423: aload_3
    //   424: invokevirtual 123	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   427: astore 9
    //   429: aload_1
    //   430: aload 9
    //   432: invokeinterface 149 2 0
    //   437: pop
    //   438: goto -286 -> 152
    //   441: aload_2
    //   442: invokevirtual 152	com/google/f/g:readInt32	()I
    //   445: istore 5
    //   447: aload_0
    //   448: iload 5
    //   450: putfield 154	com/truecaller/api/services/messenger/v1/events/Event$d:j	I
    //   453: goto -301 -> 152
    //   456: aload_2
    //   457: invokevirtual 152	com/google/f/g:readInt32	()I
    //   460: istore 5
    //   462: aload_0
    //   463: iload 5
    //   465: putfield 156	com/truecaller/api/services/messenger/v1/events/Event$d:i	I
    //   468: goto -316 -> 152
    //   471: aload_0
    //   472: getfield 47	com/truecaller/api/services/messenger/v1/events/Event$d:h	Lcom/google/f/ad;
    //   475: astore_1
    //   476: aload_1
    //   477: invokevirtual 159	com/google/f/ad:isMutable	()Z
    //   480: istore 5
    //   482: iload 5
    //   484: ifne +18 -> 502
    //   487: aload_0
    //   488: getfield 47	com/truecaller/api/services/messenger/v1/events/Event$d:h	Lcom/google/f/ad;
    //   491: astore_1
    //   492: aload_1
    //   493: invokevirtual 161	com/google/f/ad:mutableCopy	()Lcom/google/f/ad;
    //   496: astore_1
    //   497: aload_0
    //   498: aload_1
    //   499: putfield 47	com/truecaller/api/services/messenger/v1/events/Event$d:h	Lcom/google/f/ad;
    //   502: getstatic 166	com/truecaller/api/services/messenger/v1/events/Event$d$b:a	Lcom/google/f/ac;
    //   505: astore_1
    //   506: aload_0
    //   507: getfield 47	com/truecaller/api/services/messenger/v1/events/Event$d:h	Lcom/google/f/ad;
    //   510: astore 9
    //   512: aload_1
    //   513: aload 9
    //   515: aload_2
    //   516: aload_3
    //   517: invokevirtual 172	com/google/f/ac:parseInto	(Lcom/google/f/ad;Lcom/google/f/g;Lcom/google/f/n;)V
    //   520: goto -368 -> 152
    //   523: aload_0
    //   524: getfield 174	com/truecaller/api/services/messenger/v1/events/Event$d:g	Lcom/truecaller/api/services/messenger/v1/models/c;
    //   527: astore_1
    //   528: aload_1
    //   529: ifnull +21 -> 550
    //   532: aload_0
    //   533: getfield 174	com/truecaller/api/services/messenger/v1/events/Event$d:g	Lcom/truecaller/api/services/messenger/v1/models/c;
    //   536: astore_1
    //   537: aload_1
    //   538: invokevirtual 177	com/truecaller/api/services/messenger/v1/models/c:toBuilder	()Lcom/google/f/q$a;
    //   541: astore_1
    //   542: aload_1
    //   543: checkcast 179	com/truecaller/api/services/messenger/v1/models/c$a
    //   546: astore_1
    //   547: goto +8 -> 555
    //   550: iconst_0
    //   551: istore 5
    //   553: aconst_null
    //   554: astore_1
    //   555: invokestatic 181	com/truecaller/api/services/messenger/v1/models/c:d	()Lcom/google/f/ah;
    //   558: astore 9
    //   560: aload_2
    //   561: aload 9
    //   563: aload_3
    //   564: invokevirtual 123	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   567: astore 9
    //   569: aload 9
    //   571: checkcast 176	com/truecaller/api/services/messenger/v1/models/c
    //   574: astore 9
    //   576: aload_0
    //   577: aload 9
    //   579: putfield 174	com/truecaller/api/services/messenger/v1/events/Event$d:g	Lcom/truecaller/api/services/messenger/v1/models/c;
    //   582: aload_1
    //   583: ifnull -431 -> 152
    //   586: aload_0
    //   587: getfield 174	com/truecaller/api/services/messenger/v1/events/Event$d:g	Lcom/truecaller/api/services/messenger/v1/models/c;
    //   590: astore 9
    //   592: aload_1
    //   593: aload 9
    //   595: invokevirtual 182	com/truecaller/api/services/messenger/v1/models/c$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   598: pop
    //   599: aload_1
    //   600: invokevirtual 183	com/truecaller/api/services/messenger/v1/models/c$a:buildPartial	()Lcom/google/f/q;
    //   603: astore_1
    //   604: aload_1
    //   605: checkcast 176	com/truecaller/api/services/messenger/v1/models/c
    //   608: astore_1
    //   609: aload_0
    //   610: aload_1
    //   611: putfield 174	com/truecaller/api/services/messenger/v1/events/Event$d:g	Lcom/truecaller/api/services/messenger/v1/models/c;
    //   614: goto -462 -> 152
    //   617: aload_0
    //   618: getfield 59	com/truecaller/api/services/messenger/v1/events/Event$d:f	Lcom/google/f/w$h;
    //   621: astore_1
    //   622: aload_1
    //   623: invokeinterface 137 1 0
    //   628: istore 5
    //   630: iload 5
    //   632: ifne +18 -> 650
    //   635: aload_0
    //   636: getfield 59	com/truecaller/api/services/messenger/v1/events/Event$d:f	Lcom/google/f/w$h;
    //   639: astore_1
    //   640: aload_1
    //   641: invokestatic 141	com/google/f/q:mutableCopy	(Lcom/google/f/w$h;)Lcom/google/f/w$h;
    //   644: astore_1
    //   645: aload_0
    //   646: aload_1
    //   647: putfield 59	com/truecaller/api/services/messenger/v1/events/Event$d:f	Lcom/google/f/w$h;
    //   650: aload_0
    //   651: getfield 59	com/truecaller/api/services/messenger/v1/events/Event$d:f	Lcom/google/f/w$h;
    //   654: astore_1
    //   655: invokestatic 185	com/truecaller/api/services/messenger/v1/models/Peer:e	()Lcom/google/f/ah;
    //   658: astore 9
    //   660: aload_2
    //   661: aload 9
    //   663: aload_3
    //   664: invokevirtual 123	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   667: astore 9
    //   669: aload_1
    //   670: aload 9
    //   672: invokeinterface 149 2 0
    //   677: pop
    //   678: goto -526 -> 152
    //   681: aload_2
    //   682: invokevirtual 152	com/google/f/g:readInt32	()I
    //   685: istore 5
    //   687: aload_0
    //   688: iload 5
    //   690: putfield 74	com/truecaller/api/services/messenger/v1/events/Event$d:e	I
    //   693: goto -541 -> 152
    //   696: aload_2
    //   697: invokevirtual 189	com/google/f/g:readStringRequireUtf8	()Ljava/lang/String;
    //   700: astore_1
    //   701: aload_0
    //   702: aload_1
    //   703: putfield 53	com/truecaller/api/services/messenger/v1/events/Event$d:d	Ljava/lang/String;
    //   706: goto -554 -> 152
    //   709: aload_2
    //   710: invokevirtual 189	com/google/f/g:readStringRequireUtf8	()Ljava/lang/String;
    //   713: astore_1
    //   714: aload_0
    //   715: aload_1
    //   716: putfield 51	com/truecaller/api/services/messenger/v1/events/Event$d:c	Ljava/lang/String;
    //   719: goto -567 -> 152
    //   722: aload_0
    //   723: getfield 67	com/truecaller/api/services/messenger/v1/events/Event$d:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   726: astore_1
    //   727: aload_1
    //   728: ifnull +21 -> 749
    //   731: aload_0
    //   732: getfield 67	com/truecaller/api/services/messenger/v1/events/Event$d:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   735: astore_1
    //   736: aload_1
    //   737: invokevirtual 190	com/truecaller/api/services/messenger/v1/models/Peer:toBuilder	()Lcom/google/f/q$a;
    //   740: astore_1
    //   741: aload_1
    //   742: checkcast 192	com/truecaller/api/services/messenger/v1/models/Peer$a
    //   745: astore_1
    //   746: goto +8 -> 754
    //   749: iconst_0
    //   750: istore 5
    //   752: aconst_null
    //   753: astore_1
    //   754: invokestatic 185	com/truecaller/api/services/messenger/v1/models/Peer:e	()Lcom/google/f/ah;
    //   757: astore 9
    //   759: aload_2
    //   760: aload 9
    //   762: aload_3
    //   763: invokevirtual 123	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   766: astore 9
    //   768: aload 9
    //   770: checkcast 69	com/truecaller/api/services/messenger/v1/models/Peer
    //   773: astore 9
    //   775: aload_0
    //   776: aload 9
    //   778: putfield 67	com/truecaller/api/services/messenger/v1/events/Event$d:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   781: aload_1
    //   782: ifnull -630 -> 152
    //   785: aload_0
    //   786: getfield 67	com/truecaller/api/services/messenger/v1/events/Event$d:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   789: astore 9
    //   791: aload_1
    //   792: aload 9
    //   794: invokevirtual 193	com/truecaller/api/services/messenger/v1/models/Peer$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   797: pop
    //   798: aload_1
    //   799: invokevirtual 194	com/truecaller/api/services/messenger/v1/models/Peer$a:buildPartial	()Lcom/google/f/q;
    //   802: astore_1
    //   803: aload_1
    //   804: checkcast 69	com/truecaller/api/services/messenger/v1/models/Peer
    //   807: astore_1
    //   808: aload_0
    //   809: aload_1
    //   810: putfield 67	com/truecaller/api/services/messenger/v1/events/Event$d:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   813: goto -661 -> 152
    //   816: iconst_1
    //   817: istore 7
    //   819: goto -667 -> 152
    //   822: iload 5
    //   824: ifne -672 -> 152
    //   827: iconst_1
    //   828: istore 7
    //   830: goto -678 -> 152
    //   833: astore_1
    //   834: goto +53 -> 887
    //   837: astore_1
    //   838: new 196	java/lang/RuntimeException
    //   841: astore_2
    //   842: new 198	com/google/f/x
    //   845: astore_3
    //   846: aload_1
    //   847: invokevirtual 203	java/io/IOException:getMessage	()Ljava/lang/String;
    //   850: astore_1
    //   851: aload_3
    //   852: aload_1
    //   853: invokespecial 206	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   856: aload_3
    //   857: aload_0
    //   858: invokevirtual 210	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   861: astore_1
    //   862: aload_2
    //   863: aload_1
    //   864: invokespecial 213	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   867: aload_2
    //   868: athrow
    //   869: astore_1
    //   870: new 196	java/lang/RuntimeException
    //   873: astore_2
    //   874: aload_1
    //   875: aload_0
    //   876: invokevirtual 210	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   879: astore_1
    //   880: aload_2
    //   881: aload_1
    //   882: invokespecial 213	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   885: aload_2
    //   886: athrow
    //   887: aload_1
    //   888: athrow
    //   889: getstatic 35	com/truecaller/api/services/messenger/v1/events/Event$d:m	Lcom/truecaller/api/services/messenger/v1/events/Event$d;
    //   892: areturn
    //   893: aload_2
    //   894: checkcast 215	com/google/f/q$k
    //   897: astore_2
    //   898: aload_3
    //   899: checkcast 2	com/truecaller/api/services/messenger/v1/events/Event$d
    //   902: astore_3
    //   903: aload_0
    //   904: getfield 67	com/truecaller/api/services/messenger/v1/events/Event$d:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   907: astore_1
    //   908: aload_3
    //   909: getfield 67	com/truecaller/api/services/messenger/v1/events/Event$d:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   912: astore 4
    //   914: aload_2
    //   915: aload_1
    //   916: aload 4
    //   918: invokeinterface 219 3 0
    //   923: checkcast 69	com/truecaller/api/services/messenger/v1/models/Peer
    //   926: astore_1
    //   927: aload_0
    //   928: aload_1
    //   929: putfield 67	com/truecaller/api/services/messenger/v1/events/Event$d:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   932: aload_0
    //   933: getfield 51	com/truecaller/api/services/messenger/v1/events/Event$d:c	Ljava/lang/String;
    //   936: invokevirtual 224	java/lang/String:isEmpty	()Z
    //   939: iload 8
    //   941: ixor
    //   942: istore 5
    //   944: aload_0
    //   945: getfield 51	com/truecaller/api/services/messenger/v1/events/Event$d:c	Ljava/lang/String;
    //   948: astore 4
    //   950: aload_3
    //   951: getfield 51	com/truecaller/api/services/messenger/v1/events/Event$d:c	Ljava/lang/String;
    //   954: invokevirtual 224	java/lang/String:isEmpty	()Z
    //   957: iload 8
    //   959: ixor
    //   960: istore 10
    //   962: aload_3
    //   963: getfield 51	com/truecaller/api/services/messenger/v1/events/Event$d:c	Ljava/lang/String;
    //   966: astore 11
    //   968: aload_2
    //   969: iload 5
    //   971: aload 4
    //   973: iload 10
    //   975: aload 11
    //   977: invokeinterface 228 5 0
    //   982: astore_1
    //   983: aload_0
    //   984: aload_1
    //   985: putfield 51	com/truecaller/api/services/messenger/v1/events/Event$d:c	Ljava/lang/String;
    //   988: aload_0
    //   989: getfield 53	com/truecaller/api/services/messenger/v1/events/Event$d:d	Ljava/lang/String;
    //   992: invokevirtual 224	java/lang/String:isEmpty	()Z
    //   995: iload 8
    //   997: ixor
    //   998: istore 5
    //   1000: aload_0
    //   1001: getfield 53	com/truecaller/api/services/messenger/v1/events/Event$d:d	Ljava/lang/String;
    //   1004: astore 4
    //   1006: aload_3
    //   1007: getfield 53	com/truecaller/api/services/messenger/v1/events/Event$d:d	Ljava/lang/String;
    //   1010: astore 9
    //   1012: aload 9
    //   1014: invokevirtual 224	java/lang/String:isEmpty	()Z
    //   1017: iload 8
    //   1019: ixor
    //   1020: istore 10
    //   1022: aload_3
    //   1023: getfield 53	com/truecaller/api/services/messenger/v1/events/Event$d:d	Ljava/lang/String;
    //   1026: astore 11
    //   1028: aload_2
    //   1029: iload 5
    //   1031: aload 4
    //   1033: iload 10
    //   1035: aload 11
    //   1037: invokeinterface 228 5 0
    //   1042: astore_1
    //   1043: aload_0
    //   1044: aload_1
    //   1045: putfield 53	com/truecaller/api/services/messenger/v1/events/Event$d:d	Ljava/lang/String;
    //   1048: aload_0
    //   1049: getfield 74	com/truecaller/api/services/messenger/v1/events/Event$d:e	I
    //   1052: istore 5
    //   1054: iload 5
    //   1056: ifeq +9 -> 1065
    //   1059: iconst_1
    //   1060: istore 5
    //   1062: goto +8 -> 1070
    //   1065: iconst_0
    //   1066: istore 5
    //   1068: aconst_null
    //   1069: astore_1
    //   1070: aload_0
    //   1071: getfield 74	com/truecaller/api/services/messenger/v1/events/Event$d:e	I
    //   1074: istore 6
    //   1076: aload_3
    //   1077: getfield 74	com/truecaller/api/services/messenger/v1/events/Event$d:e	I
    //   1080: istore 10
    //   1082: iload 10
    //   1084: ifeq +9 -> 1093
    //   1087: iconst_1
    //   1088: istore 10
    //   1090: goto +9 -> 1099
    //   1093: iconst_0
    //   1094: istore 10
    //   1096: aconst_null
    //   1097: astore 9
    //   1099: aload_3
    //   1100: getfield 74	com/truecaller/api/services/messenger/v1/events/Event$d:e	I
    //   1103: istore 12
    //   1105: aload_2
    //   1106: iload 5
    //   1108: iload 6
    //   1110: iload 10
    //   1112: iload 12
    //   1114: invokeinterface 232 5 0
    //   1119: istore 5
    //   1121: aload_0
    //   1122: iload 5
    //   1124: putfield 74	com/truecaller/api/services/messenger/v1/events/Event$d:e	I
    //   1127: aload_0
    //   1128: getfield 59	com/truecaller/api/services/messenger/v1/events/Event$d:f	Lcom/google/f/w$h;
    //   1131: astore_1
    //   1132: aload_3
    //   1133: getfield 59	com/truecaller/api/services/messenger/v1/events/Event$d:f	Lcom/google/f/w$h;
    //   1136: astore 4
    //   1138: aload_2
    //   1139: aload_1
    //   1140: aload 4
    //   1142: invokeinterface 236 3 0
    //   1147: astore_1
    //   1148: aload_0
    //   1149: aload_1
    //   1150: putfield 59	com/truecaller/api/services/messenger/v1/events/Event$d:f	Lcom/google/f/w$h;
    //   1153: aload_0
    //   1154: getfield 174	com/truecaller/api/services/messenger/v1/events/Event$d:g	Lcom/truecaller/api/services/messenger/v1/models/c;
    //   1157: astore_1
    //   1158: aload_3
    //   1159: getfield 174	com/truecaller/api/services/messenger/v1/events/Event$d:g	Lcom/truecaller/api/services/messenger/v1/models/c;
    //   1162: astore 4
    //   1164: aload_2
    //   1165: aload_1
    //   1166: aload 4
    //   1168: invokeinterface 219 3 0
    //   1173: checkcast 176	com/truecaller/api/services/messenger/v1/models/c
    //   1176: astore_1
    //   1177: aload_0
    //   1178: aload_1
    //   1179: putfield 174	com/truecaller/api/services/messenger/v1/events/Event$d:g	Lcom/truecaller/api/services/messenger/v1/models/c;
    //   1182: aload_0
    //   1183: getfield 47	com/truecaller/api/services/messenger/v1/events/Event$d:h	Lcom/google/f/ad;
    //   1186: astore_1
    //   1187: aload_3
    //   1188: getfield 47	com/truecaller/api/services/messenger/v1/events/Event$d:h	Lcom/google/f/ad;
    //   1191: astore 4
    //   1193: aload_2
    //   1194: aload_1
    //   1195: aload 4
    //   1197: invokeinterface 240 3 0
    //   1202: astore_1
    //   1203: aload_0
    //   1204: aload_1
    //   1205: putfield 47	com/truecaller/api/services/messenger/v1/events/Event$d:h	Lcom/google/f/ad;
    //   1208: aload_0
    //   1209: getfield 156	com/truecaller/api/services/messenger/v1/events/Event$d:i	I
    //   1212: istore 5
    //   1214: iload 5
    //   1216: ifeq +9 -> 1225
    //   1219: iconst_1
    //   1220: istore 5
    //   1222: goto +8 -> 1230
    //   1225: iconst_0
    //   1226: istore 5
    //   1228: aconst_null
    //   1229: astore_1
    //   1230: aload_0
    //   1231: getfield 156	com/truecaller/api/services/messenger/v1/events/Event$d:i	I
    //   1234: istore 6
    //   1236: aload_3
    //   1237: getfield 156	com/truecaller/api/services/messenger/v1/events/Event$d:i	I
    //   1240: istore 10
    //   1242: iload 10
    //   1244: ifeq +9 -> 1253
    //   1247: iconst_1
    //   1248: istore 10
    //   1250: goto +9 -> 1259
    //   1253: iconst_0
    //   1254: istore 10
    //   1256: aconst_null
    //   1257: astore 9
    //   1259: aload_3
    //   1260: getfield 156	com/truecaller/api/services/messenger/v1/events/Event$d:i	I
    //   1263: istore 12
    //   1265: aload_2
    //   1266: iload 5
    //   1268: iload 6
    //   1270: iload 10
    //   1272: iload 12
    //   1274: invokeinterface 232 5 0
    //   1279: istore 5
    //   1281: aload_0
    //   1282: iload 5
    //   1284: putfield 156	com/truecaller/api/services/messenger/v1/events/Event$d:i	I
    //   1287: aload_0
    //   1288: getfield 154	com/truecaller/api/services/messenger/v1/events/Event$d:j	I
    //   1291: istore 5
    //   1293: iload 5
    //   1295: ifeq +9 -> 1304
    //   1298: iconst_1
    //   1299: istore 5
    //   1301: goto +8 -> 1309
    //   1304: iconst_0
    //   1305: istore 5
    //   1307: aconst_null
    //   1308: astore_1
    //   1309: aload_0
    //   1310: getfield 154	com/truecaller/api/services/messenger/v1/events/Event$d:j	I
    //   1313: istore 6
    //   1315: aload_3
    //   1316: getfield 154	com/truecaller/api/services/messenger/v1/events/Event$d:j	I
    //   1319: istore 10
    //   1321: iload 10
    //   1323: ifeq +6 -> 1329
    //   1326: iconst_1
    //   1327: istore 7
    //   1329: aload_3
    //   1330: getfield 154	com/truecaller/api/services/messenger/v1/events/Event$d:j	I
    //   1333: istore 8
    //   1335: aload_2
    //   1336: iload 5
    //   1338: iload 6
    //   1340: iload 7
    //   1342: iload 8
    //   1344: invokeinterface 232 5 0
    //   1349: istore 5
    //   1351: aload_0
    //   1352: iload 5
    //   1354: putfield 154	com/truecaller/api/services/messenger/v1/events/Event$d:j	I
    //   1357: aload_0
    //   1358: getfield 61	com/truecaller/api/services/messenger/v1/events/Event$d:k	Lcom/google/f/w$h;
    //   1361: astore_1
    //   1362: aload_3
    //   1363: getfield 61	com/truecaller/api/services/messenger/v1/events/Event$d:k	Lcom/google/f/w$h;
    //   1366: astore 4
    //   1368: aload_2
    //   1369: aload_1
    //   1370: aload 4
    //   1372: invokeinterface 236 3 0
    //   1377: astore_1
    //   1378: aload_0
    //   1379: aload_1
    //   1380: putfield 61	com/truecaller/api/services/messenger/v1/events/Event$d:k	Lcom/google/f/w$h;
    //   1383: aload_0
    //   1384: getfield 109	com/truecaller/api/services/messenger/v1/events/Event$d:l	Lcom/truecaller/api/services/messenger/v1/models/a;
    //   1387: astore_1
    //   1388: aload_3
    //   1389: getfield 109	com/truecaller/api/services/messenger/v1/events/Event$d:l	Lcom/truecaller/api/services/messenger/v1/models/a;
    //   1392: astore 4
    //   1394: aload_2
    //   1395: aload_1
    //   1396: aload 4
    //   1398: invokeinterface 219 3 0
    //   1403: checkcast 111	com/truecaller/api/services/messenger/v1/models/a
    //   1406: astore_1
    //   1407: aload_0
    //   1408: aload_1
    //   1409: putfield 109	com/truecaller/api/services/messenger/v1/events/Event$d:l	Lcom/truecaller/api/services/messenger/v1/models/a;
    //   1412: getstatic 246	com/google/f/q$i:INSTANCE	Lcom/google/f/q$i;
    //   1415: astore_1
    //   1416: aload_2
    //   1417: aload_1
    //   1418: if_acmpne +28 -> 1446
    //   1421: aload_0
    //   1422: getfield 248	com/truecaller/api/services/messenger/v1/events/Event$d:a	I
    //   1425: istore 5
    //   1427: aload_3
    //   1428: getfield 248	com/truecaller/api/services/messenger/v1/events/Event$d:a	I
    //   1431: istore 13
    //   1433: iload 5
    //   1435: iload 13
    //   1437: ior
    //   1438: istore 5
    //   1440: aload_0
    //   1441: iload 5
    //   1443: putfield 248	com/truecaller/api/services/messenger/v1/events/Event$d:a	I
    //   1446: aload_0
    //   1447: areturn
    //   1448: new 250	com/truecaller/api/services/messenger/v1/events/Event$d$a
    //   1451: astore_1
    //   1452: aload_1
    //   1453: iconst_0
    //   1454: invokespecial 253	com/truecaller/api/services/messenger/v1/events/Event$d$a:<init>	(B)V
    //   1457: aload_1
    //   1458: areturn
    //   1459: aload_0
    //   1460: getfield 59	com/truecaller/api/services/messenger/v1/events/Event$d:f	Lcom/google/f/w$h;
    //   1463: invokeinterface 254 1 0
    //   1468: aload_0
    //   1469: getfield 47	com/truecaller/api/services/messenger/v1/events/Event$d:h	Lcom/google/f/ad;
    //   1472: invokevirtual 255	com/google/f/ad:makeImmutable	()V
    //   1475: aload_0
    //   1476: getfield 61	com/truecaller/api/services/messenger/v1/events/Event$d:k	Lcom/google/f/w$h;
    //   1479: invokeinterface 254 1 0
    //   1484: aconst_null
    //   1485: areturn
    //   1486: getstatic 35	com/truecaller/api/services/messenger/v1/events/Event$d:m	Lcom/truecaller/api/services/messenger/v1/events/Event$d;
    //   1489: areturn
    //   1490: new 2	com/truecaller/api/services/messenger/v1/events/Event$d
    //   1493: astore_1
    //   1494: aload_1
    //   1495: invokespecial 33	com/truecaller/api/services/messenger/v1/events/Event$d:<init>	()V
    //   1498: aload_1
    //   1499: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1500	0	this	d
    //   0	1500	1	paramj	com.google.f.q.j
    //   0	1500	2	paramObject1	Object
    //   0	1500	3	paramObject2	Object
    //   3	1394	4	localObject1	Object
    //   9	265	5	i1	int
    //   278	113	5	bool1	boolean
    //   445	19	5	i2	int
    //   480	151	5	bool2	boolean
    //   685	138	5	i3	int
    //   942	88	5	bool3	boolean
    //   1052	55	5	i4	int
    //   1119	148	5	i5	int
    //   1279	58	5	i6	int
    //   1349	93	5	i7	int
    //   19	1320	6	i8	int
    //   25	1316	7	bool4	boolean
    //   28	1315	8	i9	int
    //   318	940	9	localObject2	Object
    //   960	74	10	bool5	boolean
    //   1080	31	10	i10	int
    //   1240	31	10	i11	int
    //   1319	3	10	i12	int
    //   966	70	11	str	String
    //   1103	170	12	i13	int
    //   1431	7	13	i14	int
    // Exception table:
    //   from	to	target	type
    //   103	106	133	finally
    //   111	114	133	finally
    //   115	118	133	finally
    //   120	124	133	finally
    //   124	128	133	finally
    //   128	130	133	finally
    //   134	136	133	finally
    //   157	161	833	finally
    //   273	278	833	finally
    //   283	287	833	finally
    //   292	296	833	finally
    //   297	301	833	finally
    //   302	306	833	finally
    //   315	318	833	finally
    //   323	327	833	finally
    //   329	334	833	finally
    //   337	342	833	finally
    //   346	350	833	finally
    //   353	359	833	finally
    //   359	363	833	finally
    //   364	368	833	finally
    //   370	374	833	finally
    //   377	381	833	finally
    //   382	388	833	finally
    //   395	399	833	finally
    //   400	404	833	finally
    //   406	410	833	finally
    //   410	414	833	finally
    //   415	418	833	finally
    //   423	427	833	finally
    //   430	438	833	finally
    //   441	445	833	finally
    //   448	453	833	finally
    //   456	460	833	finally
    //   463	468	833	finally
    //   471	475	833	finally
    //   476	480	833	finally
    //   487	491	833	finally
    //   492	496	833	finally
    //   498	502	833	finally
    //   502	505	833	finally
    //   506	510	833	finally
    //   516	520	833	finally
    //   523	527	833	finally
    //   532	536	833	finally
    //   537	541	833	finally
    //   542	546	833	finally
    //   555	558	833	finally
    //   563	567	833	finally
    //   569	574	833	finally
    //   577	582	833	finally
    //   586	590	833	finally
    //   593	599	833	finally
    //   599	603	833	finally
    //   604	608	833	finally
    //   610	614	833	finally
    //   617	621	833	finally
    //   622	628	833	finally
    //   635	639	833	finally
    //   640	644	833	finally
    //   646	650	833	finally
    //   650	654	833	finally
    //   655	658	833	finally
    //   663	667	833	finally
    //   670	678	833	finally
    //   681	685	833	finally
    //   688	693	833	finally
    //   696	700	833	finally
    //   702	706	833	finally
    //   709	713	833	finally
    //   715	719	833	finally
    //   722	726	833	finally
    //   731	735	833	finally
    //   736	740	833	finally
    //   741	745	833	finally
    //   754	757	833	finally
    //   762	766	833	finally
    //   768	773	833	finally
    //   776	781	833	finally
    //   785	789	833	finally
    //   792	798	833	finally
    //   798	802	833	finally
    //   803	807	833	finally
    //   809	813	833	finally
    //   838	841	833	finally
    //   842	845	833	finally
    //   846	850	833	finally
    //   852	856	833	finally
    //   857	861	833	finally
    //   863	867	833	finally
    //   867	869	833	finally
    //   870	873	833	finally
    //   875	879	833	finally
    //   881	885	833	finally
    //   885	887	833	finally
    //   157	161	837	java/io/IOException
    //   273	278	837	java/io/IOException
    //   283	287	837	java/io/IOException
    //   292	296	837	java/io/IOException
    //   297	301	837	java/io/IOException
    //   302	306	837	java/io/IOException
    //   315	318	837	java/io/IOException
    //   323	327	837	java/io/IOException
    //   329	334	837	java/io/IOException
    //   337	342	837	java/io/IOException
    //   346	350	837	java/io/IOException
    //   353	359	837	java/io/IOException
    //   359	363	837	java/io/IOException
    //   364	368	837	java/io/IOException
    //   370	374	837	java/io/IOException
    //   377	381	837	java/io/IOException
    //   382	388	837	java/io/IOException
    //   395	399	837	java/io/IOException
    //   400	404	837	java/io/IOException
    //   406	410	837	java/io/IOException
    //   410	414	837	java/io/IOException
    //   415	418	837	java/io/IOException
    //   423	427	837	java/io/IOException
    //   430	438	837	java/io/IOException
    //   441	445	837	java/io/IOException
    //   448	453	837	java/io/IOException
    //   456	460	837	java/io/IOException
    //   463	468	837	java/io/IOException
    //   471	475	837	java/io/IOException
    //   476	480	837	java/io/IOException
    //   487	491	837	java/io/IOException
    //   492	496	837	java/io/IOException
    //   498	502	837	java/io/IOException
    //   502	505	837	java/io/IOException
    //   506	510	837	java/io/IOException
    //   516	520	837	java/io/IOException
    //   523	527	837	java/io/IOException
    //   532	536	837	java/io/IOException
    //   537	541	837	java/io/IOException
    //   542	546	837	java/io/IOException
    //   555	558	837	java/io/IOException
    //   563	567	837	java/io/IOException
    //   569	574	837	java/io/IOException
    //   577	582	837	java/io/IOException
    //   586	590	837	java/io/IOException
    //   593	599	837	java/io/IOException
    //   599	603	837	java/io/IOException
    //   604	608	837	java/io/IOException
    //   610	614	837	java/io/IOException
    //   617	621	837	java/io/IOException
    //   622	628	837	java/io/IOException
    //   635	639	837	java/io/IOException
    //   640	644	837	java/io/IOException
    //   646	650	837	java/io/IOException
    //   650	654	837	java/io/IOException
    //   655	658	837	java/io/IOException
    //   663	667	837	java/io/IOException
    //   670	678	837	java/io/IOException
    //   681	685	837	java/io/IOException
    //   688	693	837	java/io/IOException
    //   696	700	837	java/io/IOException
    //   702	706	837	java/io/IOException
    //   709	713	837	java/io/IOException
    //   715	719	837	java/io/IOException
    //   722	726	837	java/io/IOException
    //   731	735	837	java/io/IOException
    //   736	740	837	java/io/IOException
    //   741	745	837	java/io/IOException
    //   754	757	837	java/io/IOException
    //   762	766	837	java/io/IOException
    //   768	773	837	java/io/IOException
    //   776	781	837	java/io/IOException
    //   785	789	837	java/io/IOException
    //   792	798	837	java/io/IOException
    //   798	802	837	java/io/IOException
    //   803	807	837	java/io/IOException
    //   809	813	837	java/io/IOException
    //   157	161	869	com/google/f/x
    //   273	278	869	com/google/f/x
    //   283	287	869	com/google/f/x
    //   292	296	869	com/google/f/x
    //   297	301	869	com/google/f/x
    //   302	306	869	com/google/f/x
    //   315	318	869	com/google/f/x
    //   323	327	869	com/google/f/x
    //   329	334	869	com/google/f/x
    //   337	342	869	com/google/f/x
    //   346	350	869	com/google/f/x
    //   353	359	869	com/google/f/x
    //   359	363	869	com/google/f/x
    //   364	368	869	com/google/f/x
    //   370	374	869	com/google/f/x
    //   377	381	869	com/google/f/x
    //   382	388	869	com/google/f/x
    //   395	399	869	com/google/f/x
    //   400	404	869	com/google/f/x
    //   406	410	869	com/google/f/x
    //   410	414	869	com/google/f/x
    //   415	418	869	com/google/f/x
    //   423	427	869	com/google/f/x
    //   430	438	869	com/google/f/x
    //   441	445	869	com/google/f/x
    //   448	453	869	com/google/f/x
    //   456	460	869	com/google/f/x
    //   463	468	869	com/google/f/x
    //   471	475	869	com/google/f/x
    //   476	480	869	com/google/f/x
    //   487	491	869	com/google/f/x
    //   492	496	869	com/google/f/x
    //   498	502	869	com/google/f/x
    //   502	505	869	com/google/f/x
    //   506	510	869	com/google/f/x
    //   516	520	869	com/google/f/x
    //   523	527	869	com/google/f/x
    //   532	536	869	com/google/f/x
    //   537	541	869	com/google/f/x
    //   542	546	869	com/google/f/x
    //   555	558	869	com/google/f/x
    //   563	567	869	com/google/f/x
    //   569	574	869	com/google/f/x
    //   577	582	869	com/google/f/x
    //   586	590	869	com/google/f/x
    //   593	599	869	com/google/f/x
    //   599	603	869	com/google/f/x
    //   604	608	869	com/google/f/x
    //   610	614	869	com/google/f/x
    //   617	621	869	com/google/f/x
    //   622	628	869	com/google/f/x
    //   635	639	869	com/google/f/x
    //   640	644	869	com/google/f/x
    //   646	650	869	com/google/f/x
    //   650	654	869	com/google/f/x
    //   655	658	869	com/google/f/x
    //   663	667	869	com/google/f/x
    //   670	678	869	com/google/f/x
    //   681	685	869	com/google/f/x
    //   688	693	869	com/google/f/x
    //   696	700	869	com/google/f/x
    //   702	706	869	com/google/f/x
    //   709	713	869	com/google/f/x
    //   715	719	869	com/google/f/x
    //   722	726	869	com/google/f/x
    //   731	735	869	com/google/f/x
    //   736	740	869	com/google/f/x
    //   741	745	869	com/google/f/x
    //   754	757	869	com/google/f/x
    //   762	766	869	com/google/f/x
    //   768	773	869	com/google/f/x
    //   776	781	869	com/google/f/x
    //   785	789	869	com/google/f/x
    //   792	798	869	com/google/f/x
    //   798	802	869	com/google/f/x
    //   803	807	869	com/google/f/x
    //   809	813	869	com/google/f/x
  }
  
  public final c e()
  {
    c localc = g;
    if (localc == null) {
      localc = c.c();
    }
    return localc;
  }
  
  public final Map f()
  {
    return Collections.unmodifiableMap(h);
  }
  
  public final int g()
  {
    return i;
  }
  
  public final int getSerializedSize()
  {
    int i1 = memoizedSerializedSize;
    int i2 = -1;
    if (i1 != i2) {
      return i1;
    }
    Object localObject1 = b;
    i2 = 0;
    a locala = null;
    if (localObject1 != null)
    {
      localObject1 = a();
      int i3 = 1;
      i1 = h.computeMessageSize(i3, (ae)localObject1) + 0;
    }
    else
    {
      i1 = 0;
      localObject1 = null;
    }
    String str = c;
    boolean bool1 = str.isEmpty();
    Object localObject2;
    if (!bool1)
    {
      localObject2 = c;
      int i4 = h.computeStringSize(2, (String)localObject2);
      i1 += i4;
    }
    str = d;
    boolean bool2 = str.isEmpty();
    if (!bool2)
    {
      localObject2 = d;
      i5 = h.computeStringSize(3, (String)localObject2);
      i1 += i5;
    }
    int i5 = e;
    int i6;
    if (i5 != 0)
    {
      i6 = 4;
      i5 = h.computeInt32Size(i6, i5);
      i1 += i5;
    }
    i5 = i1;
    i1 = 0;
    localObject1 = null;
    Object localObject3;
    for (;;)
    {
      localObject2 = f;
      i6 = ((w.h)localObject2).size();
      if (i1 >= i6) {
        break;
      }
      localObject3 = (ae)f.get(i1);
      i6 = h.computeMessageSize(5, (ae)localObject3);
      i5 += i6;
      i1 += 1;
    }
    localObject1 = g;
    if (localObject1 != null)
    {
      localObject2 = e();
      i1 = h.computeMessageSize(6, (ae)localObject2);
      i5 += i1;
    }
    localObject1 = h.entrySet().iterator();
    int i7;
    for (;;)
    {
      boolean bool3 = ((Iterator)localObject1).hasNext();
      if (!bool3) {
        break;
      }
      localObject2 = (Map.Entry)((Iterator)localObject1).next();
      localObject3 = Event.d.b.a;
      int i8 = 7;
      Object localObject4 = ((Map.Entry)localObject2).getKey();
      localObject2 = ((Map.Entry)localObject2).getValue();
      i7 = ((ac)localObject3).computeMessageSize(i8, localObject4, localObject2);
      i5 += i7;
    }
    i1 = i;
    if (i1 != 0)
    {
      i7 = 8;
      i1 = h.computeInt32Size(i7, i1);
      i5 += i1;
    }
    i1 = j;
    if (i1 != 0)
    {
      i7 = 9;
      i1 = h.computeInt32Size(i7, i1);
      i5 += i1;
    }
    for (;;)
    {
      localObject1 = k;
      i1 = ((w.h)localObject1).size();
      if (i2 >= i1) {
        break;
      }
      localObject2 = (ae)k.get(i2);
      i1 = h.computeMessageSize(10, (ae)localObject2);
      i5 += i1;
      i2 += 1;
    }
    localObject1 = l;
    if (localObject1 != null)
    {
      locala = i();
      i1 = h.computeMessageSize(11, locala);
      i5 += i1;
    }
    memoizedSerializedSize = i5;
    return i5;
  }
  
  public final List h()
  {
    return k;
  }
  
  public final a i()
  {
    a locala = l;
    if (locala == null) {
      locala = a.e();
    }
    return locala;
  }
  
  public final void writeTo(h paramh)
  {
    Object localObject1 = b;
    if (localObject1 != null)
    {
      localObject1 = a();
      i1 = 1;
      paramh.writeMessage(i1, (ae)localObject1);
    }
    localObject1 = c;
    boolean bool1 = ((String)localObject1).isEmpty();
    if (!bool1)
    {
      int i2 = 2;
      localObject2 = c;
      paramh.writeString(i2, (String)localObject2);
    }
    localObject1 = d;
    boolean bool2 = ((String)localObject1).isEmpty();
    if (!bool2)
    {
      i3 = 3;
      localObject2 = d;
      paramh.writeString(i3, (String)localObject2);
    }
    int i3 = e;
    if (i3 != 0)
    {
      i1 = 4;
      paramh.writeInt32(i1, i3);
    }
    i3 = 0;
    localObject1 = null;
    int i1 = 0;
    Object localObject2 = null;
    Object localObject3;
    Object localObject4;
    for (;;)
    {
      localObject3 = f;
      int i4 = ((w.h)localObject3).size();
      if (i1 >= i4) {
        break;
      }
      i4 = 5;
      localObject4 = (ae)f.get(i1);
      paramh.writeMessage(i4, (ae)localObject4);
      i1 += 1;
    }
    localObject2 = g;
    if (localObject2 != null)
    {
      i1 = 6;
      localObject3 = e();
      paramh.writeMessage(i1, (ae)localObject3);
    }
    localObject2 = h.entrySet().iterator();
    for (;;)
    {
      boolean bool3 = ((Iterator)localObject2).hasNext();
      if (!bool3) {
        break;
      }
      localObject3 = (Map.Entry)((Iterator)localObject2).next();
      localObject4 = Event.d.b.a;
      int i6 = 7;
      Object localObject5 = ((Map.Entry)localObject3).getKey();
      localObject3 = ((Map.Entry)localObject3).getValue();
      ((ac)localObject4).serializeTo(paramh, i6, localObject5, localObject3);
    }
    i1 = i;
    int i5;
    if (i1 != 0)
    {
      i5 = 8;
      paramh.writeInt32(i5, i1);
    }
    i1 = j;
    if (i1 != 0)
    {
      i5 = 9;
      paramh.writeInt32(i5, i1);
    }
    for (;;)
    {
      localObject2 = k;
      i1 = ((w.h)localObject2).size();
      if (i3 >= i1) {
        break;
      }
      i1 = 10;
      localObject3 = (ae)k.get(i3);
      paramh.writeMessage(i1, (ae)localObject3);
      i3 += 1;
    }
    localObject1 = l;
    if (localObject1 != null)
    {
      i3 = 11;
      localObject2 = i();
      paramh.writeMessage(i3, (ae)localObject2);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.events.Event.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */