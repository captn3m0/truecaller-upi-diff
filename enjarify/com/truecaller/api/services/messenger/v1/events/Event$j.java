package com.truecaller.api.services.messenger.v1.events;

import com.google.f.ae;
import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;
import com.truecaller.api.services.messenger.v1.models.MessageContent;
import com.truecaller.api.services.messenger.v1.models.Peer;
import com.truecaller.api.services.messenger.v1.models.Peer.d;

public final class Event$j
  extends q
  implements Event.k
{
  private static final j f;
  private static volatile ah g;
  private Peer.d a;
  private Peer b;
  private String c = "";
  private int d;
  private MessageContent e;
  
  static
  {
    j localj = new com/truecaller/api/services/messenger/v1/events/Event$j;
    localj.<init>();
    f = localj;
    localj.makeImmutable();
  }
  
  public static j f()
  {
    return f;
  }
  
  public static ah g()
  {
    return f.getParserForType();
  }
  
  public final Peer.d a()
  {
    Peer.d locald = a;
    if (locald == null) {
      locald = Peer.d.d();
    }
    return locald;
  }
  
  public final Peer b()
  {
    Peer localPeer = b;
    if (localPeer == null) {
      localPeer = Peer.d();
    }
    return localPeer;
  }
  
  public final String c()
  {
    return c;
  }
  
  public final int d()
  {
    return d;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 59	com/truecaller/api/services/messenger/v1/events/Event$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 65	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_0
    //   19: istore 6
    //   21: aconst_null
    //   22: astore 4
    //   24: iconst_0
    //   25: istore 7
    //   27: aconst_null
    //   28: astore 8
    //   30: iconst_1
    //   31: istore 9
    //   33: iload 5
    //   35: tableswitch	default:+45->80, 1:+827->862, 2:+823->858, 3:+821->856, 4:+810->845, 5:+583->618, 6:+107->142, 7:+579->614, 8:+55->90
    //   80: new 68	java/lang/UnsupportedOperationException
    //   83: astore_1
    //   84: aload_1
    //   85: invokespecial 69	java/lang/UnsupportedOperationException:<init>	()V
    //   88: aload_1
    //   89: athrow
    //   90: getstatic 71	com/truecaller/api/services/messenger/v1/events/Event$j:g	Lcom/google/f/ah;
    //   93: astore_1
    //   94: aload_1
    //   95: ifnonnull +43 -> 138
    //   98: ldc 2
    //   100: astore_1
    //   101: aload_1
    //   102: monitorenter
    //   103: getstatic 71	com/truecaller/api/services/messenger/v1/events/Event$j:g	Lcom/google/f/ah;
    //   106: astore_2
    //   107: aload_2
    //   108: ifnonnull +20 -> 128
    //   111: new 73	com/google/f/q$b
    //   114: astore_2
    //   115: getstatic 26	com/truecaller/api/services/messenger/v1/events/Event$j:f	Lcom/truecaller/api/services/messenger/v1/events/Event$j;
    //   118: astore_3
    //   119: aload_2
    //   120: aload_3
    //   121: invokespecial 76	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   124: aload_2
    //   125: putstatic 71	com/truecaller/api/services/messenger/v1/events/Event$j:g	Lcom/google/f/ah;
    //   128: aload_1
    //   129: monitorexit
    //   130: goto +8 -> 138
    //   133: astore_2
    //   134: aload_1
    //   135: monitorexit
    //   136: aload_2
    //   137: athrow
    //   138: getstatic 71	com/truecaller/api/services/messenger/v1/events/Event$j:g	Lcom/google/f/ah;
    //   141: areturn
    //   142: aload_2
    //   143: checkcast 78	com/google/f/g
    //   146: astore_2
    //   147: aload_3
    //   148: checkcast 80	com/google/f/n
    //   151: astore_3
    //   152: iload 6
    //   154: ifne +460 -> 614
    //   157: aload_2
    //   158: invokevirtual 83	com/google/f/g:readTag	()I
    //   161: istore 5
    //   163: iload 5
    //   165: ifeq +387 -> 552
    //   168: bipush 10
    //   170: istore 10
    //   172: iload 5
    //   174: iload 10
    //   176: if_icmpeq +282 -> 458
    //   179: bipush 18
    //   181: istore 10
    //   183: iload 5
    //   185: iload 10
    //   187: if_icmpeq +177 -> 364
    //   190: bipush 26
    //   192: istore 10
    //   194: iload 5
    //   196: iload 10
    //   198: if_icmpeq +153 -> 351
    //   201: bipush 32
    //   203: istore 10
    //   205: iload 5
    //   207: iload 10
    //   209: if_icmpeq +127 -> 336
    //   212: bipush 42
    //   214: istore 10
    //   216: iload 5
    //   218: iload 10
    //   220: if_icmpeq +22 -> 242
    //   223: aload_2
    //   224: iload 5
    //   226: invokevirtual 92	com/google/f/g:skipField	(I)Z
    //   229: istore 5
    //   231: iload 5
    //   233: ifne -81 -> 152
    //   236: iconst_1
    //   237: istore 6
    //   239: goto -87 -> 152
    //   242: aload_0
    //   243: getfield 94	com/truecaller/api/services/messenger/v1/events/Event$j:e	Lcom/truecaller/api/services/messenger/v1/models/MessageContent;
    //   246: astore_1
    //   247: aload_1
    //   248: ifnull +21 -> 269
    //   251: aload_0
    //   252: getfield 94	com/truecaller/api/services/messenger/v1/events/Event$j:e	Lcom/truecaller/api/services/messenger/v1/models/MessageContent;
    //   255: astore_1
    //   256: aload_1
    //   257: invokevirtual 100	com/truecaller/api/services/messenger/v1/models/MessageContent:toBuilder	()Lcom/google/f/q$a;
    //   260: astore_1
    //   261: aload_1
    //   262: checkcast 102	com/truecaller/api/services/messenger/v1/models/MessageContent$c
    //   265: astore_1
    //   266: goto +8 -> 274
    //   269: iconst_0
    //   270: istore 5
    //   272: aconst_null
    //   273: astore_1
    //   274: invokestatic 105	com/truecaller/api/services/messenger/v1/models/MessageContent:i	()Lcom/google/f/ah;
    //   277: astore 11
    //   279: aload_2
    //   280: aload 11
    //   282: aload_3
    //   283: invokevirtual 109	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   286: astore 11
    //   288: aload 11
    //   290: checkcast 96	com/truecaller/api/services/messenger/v1/models/MessageContent
    //   293: astore 11
    //   295: aload_0
    //   296: aload 11
    //   298: putfield 94	com/truecaller/api/services/messenger/v1/events/Event$j:e	Lcom/truecaller/api/services/messenger/v1/models/MessageContent;
    //   301: aload_1
    //   302: ifnull -150 -> 152
    //   305: aload_0
    //   306: getfield 94	com/truecaller/api/services/messenger/v1/events/Event$j:e	Lcom/truecaller/api/services/messenger/v1/models/MessageContent;
    //   309: astore 11
    //   311: aload_1
    //   312: aload 11
    //   314: invokevirtual 113	com/truecaller/api/services/messenger/v1/models/MessageContent$c:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   317: pop
    //   318: aload_1
    //   319: invokevirtual 117	com/truecaller/api/services/messenger/v1/models/MessageContent$c:buildPartial	()Lcom/google/f/q;
    //   322: astore_1
    //   323: aload_1
    //   324: checkcast 96	com/truecaller/api/services/messenger/v1/models/MessageContent
    //   327: astore_1
    //   328: aload_0
    //   329: aload_1
    //   330: putfield 94	com/truecaller/api/services/messenger/v1/events/Event$j:e	Lcom/truecaller/api/services/messenger/v1/models/MessageContent;
    //   333: goto -181 -> 152
    //   336: aload_2
    //   337: invokevirtual 120	com/google/f/g:readInt32	()I
    //   340: istore 5
    //   342: aload_0
    //   343: iload 5
    //   345: putfield 54	com/truecaller/api/services/messenger/v1/events/Event$j:d	I
    //   348: goto -196 -> 152
    //   351: aload_2
    //   352: invokevirtual 124	com/google/f/g:readStringRequireUtf8	()Ljava/lang/String;
    //   355: astore_1
    //   356: aload_0
    //   357: aload_1
    //   358: putfield 34	com/truecaller/api/services/messenger/v1/events/Event$j:c	Ljava/lang/String;
    //   361: goto -209 -> 152
    //   364: aload_0
    //   365: getfield 47	com/truecaller/api/services/messenger/v1/events/Event$j:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   368: astore_1
    //   369: aload_1
    //   370: ifnull +21 -> 391
    //   373: aload_0
    //   374: getfield 47	com/truecaller/api/services/messenger/v1/events/Event$j:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   377: astore_1
    //   378: aload_1
    //   379: invokevirtual 125	com/truecaller/api/services/messenger/v1/models/Peer:toBuilder	()Lcom/google/f/q$a;
    //   382: astore_1
    //   383: aload_1
    //   384: checkcast 127	com/truecaller/api/services/messenger/v1/models/Peer$a
    //   387: astore_1
    //   388: goto +8 -> 396
    //   391: iconst_0
    //   392: istore 5
    //   394: aconst_null
    //   395: astore_1
    //   396: invokestatic 129	com/truecaller/api/services/messenger/v1/models/Peer:e	()Lcom/google/f/ah;
    //   399: astore 11
    //   401: aload_2
    //   402: aload 11
    //   404: aload_3
    //   405: invokevirtual 109	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   408: astore 11
    //   410: aload 11
    //   412: checkcast 49	com/truecaller/api/services/messenger/v1/models/Peer
    //   415: astore 11
    //   417: aload_0
    //   418: aload 11
    //   420: putfield 47	com/truecaller/api/services/messenger/v1/events/Event$j:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   423: aload_1
    //   424: ifnull -272 -> 152
    //   427: aload_0
    //   428: getfield 47	com/truecaller/api/services/messenger/v1/events/Event$j:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   431: astore 11
    //   433: aload_1
    //   434: aload 11
    //   436: invokevirtual 130	com/truecaller/api/services/messenger/v1/models/Peer$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   439: pop
    //   440: aload_1
    //   441: invokevirtual 131	com/truecaller/api/services/messenger/v1/models/Peer$a:buildPartial	()Lcom/google/f/q;
    //   444: astore_1
    //   445: aload_1
    //   446: checkcast 49	com/truecaller/api/services/messenger/v1/models/Peer
    //   449: astore_1
    //   450: aload_0
    //   451: aload_1
    //   452: putfield 47	com/truecaller/api/services/messenger/v1/events/Event$j:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   455: goto -303 -> 152
    //   458: aload_0
    //   459: getfield 40	com/truecaller/api/services/messenger/v1/events/Event$j:a	Lcom/truecaller/api/services/messenger/v1/models/Peer$d;
    //   462: astore_1
    //   463: aload_1
    //   464: ifnull +21 -> 485
    //   467: aload_0
    //   468: getfield 40	com/truecaller/api/services/messenger/v1/events/Event$j:a	Lcom/truecaller/api/services/messenger/v1/models/Peer$d;
    //   471: astore_1
    //   472: aload_1
    //   473: invokevirtual 132	com/truecaller/api/services/messenger/v1/models/Peer$d:toBuilder	()Lcom/google/f/q$a;
    //   476: astore_1
    //   477: aload_1
    //   478: checkcast 134	com/truecaller/api/services/messenger/v1/models/Peer$d$a
    //   481: astore_1
    //   482: goto +8 -> 490
    //   485: iconst_0
    //   486: istore 5
    //   488: aconst_null
    //   489: astore_1
    //   490: invokestatic 135	com/truecaller/api/services/messenger/v1/models/Peer$d:e	()Lcom/google/f/ah;
    //   493: astore 11
    //   495: aload_2
    //   496: aload 11
    //   498: aload_3
    //   499: invokevirtual 109	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   502: astore 11
    //   504: aload 11
    //   506: checkcast 42	com/truecaller/api/services/messenger/v1/models/Peer$d
    //   509: astore 11
    //   511: aload_0
    //   512: aload 11
    //   514: putfield 40	com/truecaller/api/services/messenger/v1/events/Event$j:a	Lcom/truecaller/api/services/messenger/v1/models/Peer$d;
    //   517: aload_1
    //   518: ifnull -366 -> 152
    //   521: aload_0
    //   522: getfield 40	com/truecaller/api/services/messenger/v1/events/Event$j:a	Lcom/truecaller/api/services/messenger/v1/models/Peer$d;
    //   525: astore 11
    //   527: aload_1
    //   528: aload 11
    //   530: invokevirtual 136	com/truecaller/api/services/messenger/v1/models/Peer$d$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   533: pop
    //   534: aload_1
    //   535: invokevirtual 137	com/truecaller/api/services/messenger/v1/models/Peer$d$a:buildPartial	()Lcom/google/f/q;
    //   538: astore_1
    //   539: aload_1
    //   540: checkcast 42	com/truecaller/api/services/messenger/v1/models/Peer$d
    //   543: astore_1
    //   544: aload_0
    //   545: aload_1
    //   546: putfield 40	com/truecaller/api/services/messenger/v1/events/Event$j:a	Lcom/truecaller/api/services/messenger/v1/models/Peer$d;
    //   549: goto -397 -> 152
    //   552: iconst_1
    //   553: istore 6
    //   555: goto -403 -> 152
    //   558: astore_1
    //   559: goto +53 -> 612
    //   562: astore_1
    //   563: new 139	java/lang/RuntimeException
    //   566: astore_2
    //   567: new 141	com/google/f/x
    //   570: astore_3
    //   571: aload_1
    //   572: invokevirtual 146	java/io/IOException:getMessage	()Ljava/lang/String;
    //   575: astore_1
    //   576: aload_3
    //   577: aload_1
    //   578: invokespecial 149	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   581: aload_3
    //   582: aload_0
    //   583: invokevirtual 153	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   586: astore_1
    //   587: aload_2
    //   588: aload_1
    //   589: invokespecial 156	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   592: aload_2
    //   593: athrow
    //   594: astore_1
    //   595: new 139	java/lang/RuntimeException
    //   598: astore_2
    //   599: aload_1
    //   600: aload_0
    //   601: invokevirtual 153	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   604: astore_1
    //   605: aload_2
    //   606: aload_1
    //   607: invokespecial 156	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   610: aload_2
    //   611: athrow
    //   612: aload_1
    //   613: athrow
    //   614: getstatic 26	com/truecaller/api/services/messenger/v1/events/Event$j:f	Lcom/truecaller/api/services/messenger/v1/events/Event$j;
    //   617: areturn
    //   618: aload_2
    //   619: checkcast 158	com/google/f/q$k
    //   622: astore_2
    //   623: aload_3
    //   624: checkcast 2	com/truecaller/api/services/messenger/v1/events/Event$j
    //   627: astore_3
    //   628: aload_0
    //   629: getfield 40	com/truecaller/api/services/messenger/v1/events/Event$j:a	Lcom/truecaller/api/services/messenger/v1/models/Peer$d;
    //   632: astore_1
    //   633: aload_3
    //   634: getfield 40	com/truecaller/api/services/messenger/v1/events/Event$j:a	Lcom/truecaller/api/services/messenger/v1/models/Peer$d;
    //   637: astore 8
    //   639: aload_2
    //   640: aload_1
    //   641: aload 8
    //   643: invokeinterface 162 3 0
    //   648: checkcast 42	com/truecaller/api/services/messenger/v1/models/Peer$d
    //   651: astore_1
    //   652: aload_0
    //   653: aload_1
    //   654: putfield 40	com/truecaller/api/services/messenger/v1/events/Event$j:a	Lcom/truecaller/api/services/messenger/v1/models/Peer$d;
    //   657: aload_0
    //   658: getfield 47	com/truecaller/api/services/messenger/v1/events/Event$j:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   661: astore_1
    //   662: aload_3
    //   663: getfield 47	com/truecaller/api/services/messenger/v1/events/Event$j:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   666: astore 8
    //   668: aload_2
    //   669: aload_1
    //   670: aload 8
    //   672: invokeinterface 162 3 0
    //   677: checkcast 49	com/truecaller/api/services/messenger/v1/models/Peer
    //   680: astore_1
    //   681: aload_0
    //   682: aload_1
    //   683: putfield 47	com/truecaller/api/services/messenger/v1/events/Event$j:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   686: aload_0
    //   687: getfield 34	com/truecaller/api/services/messenger/v1/events/Event$j:c	Ljava/lang/String;
    //   690: invokevirtual 168	java/lang/String:isEmpty	()Z
    //   693: iload 9
    //   695: ixor
    //   696: istore 5
    //   698: aload_0
    //   699: getfield 34	com/truecaller/api/services/messenger/v1/events/Event$j:c	Ljava/lang/String;
    //   702: astore 8
    //   704: aload_3
    //   705: getfield 34	com/truecaller/api/services/messenger/v1/events/Event$j:c	Ljava/lang/String;
    //   708: astore 11
    //   710: aload 11
    //   712: invokevirtual 168	java/lang/String:isEmpty	()Z
    //   715: iload 9
    //   717: ixor
    //   718: istore 10
    //   720: aload_3
    //   721: getfield 34	com/truecaller/api/services/messenger/v1/events/Event$j:c	Ljava/lang/String;
    //   724: astore 12
    //   726: aload_2
    //   727: iload 5
    //   729: aload 8
    //   731: iload 10
    //   733: aload 12
    //   735: invokeinterface 172 5 0
    //   740: astore_1
    //   741: aload_0
    //   742: aload_1
    //   743: putfield 34	com/truecaller/api/services/messenger/v1/events/Event$j:c	Ljava/lang/String;
    //   746: aload_0
    //   747: getfield 54	com/truecaller/api/services/messenger/v1/events/Event$j:d	I
    //   750: istore 5
    //   752: iload 5
    //   754: ifeq +9 -> 763
    //   757: iconst_1
    //   758: istore 5
    //   760: goto +8 -> 768
    //   763: iconst_0
    //   764: istore 5
    //   766: aconst_null
    //   767: astore_1
    //   768: aload_0
    //   769: getfield 54	com/truecaller/api/services/messenger/v1/events/Event$j:d	I
    //   772: istore 7
    //   774: aload_3
    //   775: getfield 54	com/truecaller/api/services/messenger/v1/events/Event$j:d	I
    //   778: istore 10
    //   780: iload 10
    //   782: ifeq +6 -> 788
    //   785: iconst_1
    //   786: istore 6
    //   788: aload_3
    //   789: getfield 54	com/truecaller/api/services/messenger/v1/events/Event$j:d	I
    //   792: istore 9
    //   794: aload_2
    //   795: iload 5
    //   797: iload 7
    //   799: iload 6
    //   801: iload 9
    //   803: invokeinterface 176 5 0
    //   808: istore 5
    //   810: aload_0
    //   811: iload 5
    //   813: putfield 54	com/truecaller/api/services/messenger/v1/events/Event$j:d	I
    //   816: aload_0
    //   817: getfield 94	com/truecaller/api/services/messenger/v1/events/Event$j:e	Lcom/truecaller/api/services/messenger/v1/models/MessageContent;
    //   820: astore_1
    //   821: aload_3
    //   822: getfield 94	com/truecaller/api/services/messenger/v1/events/Event$j:e	Lcom/truecaller/api/services/messenger/v1/models/MessageContent;
    //   825: astore_3
    //   826: aload_2
    //   827: aload_1
    //   828: aload_3
    //   829: invokeinterface 162 3 0
    //   834: checkcast 96	com/truecaller/api/services/messenger/v1/models/MessageContent
    //   837: astore_1
    //   838: aload_0
    //   839: aload_1
    //   840: putfield 94	com/truecaller/api/services/messenger/v1/events/Event$j:e	Lcom/truecaller/api/services/messenger/v1/models/MessageContent;
    //   843: aload_0
    //   844: areturn
    //   845: new 178	com/truecaller/api/services/messenger/v1/events/Event$j$a
    //   848: astore_1
    //   849: aload_1
    //   850: iconst_0
    //   851: invokespecial 181	com/truecaller/api/services/messenger/v1/events/Event$j$a:<init>	(B)V
    //   854: aload_1
    //   855: areturn
    //   856: aconst_null
    //   857: areturn
    //   858: getstatic 26	com/truecaller/api/services/messenger/v1/events/Event$j:f	Lcom/truecaller/api/services/messenger/v1/events/Event$j;
    //   861: areturn
    //   862: new 2	com/truecaller/api/services/messenger/v1/events/Event$j
    //   865: astore_1
    //   866: aload_1
    //   867: invokespecial 24	com/truecaller/api/services/messenger/v1/events/Event$j:<init>	()V
    //   870: aload_1
    //   871: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	872	0	this	j
    //   0	872	1	paramj	com.google.f.q.j
    //   0	872	2	paramObject1	Object
    //   0	872	3	paramObject2	Object
    //   3	20	4	arrayOfInt	int[]
    //   9	216	5	i	int
    //   229	42	5	bool1	boolean
    //   340	147	5	j	int
    //   696	32	5	bool2	boolean
    //   750	46	5	k	int
    //   808	4	5	m	int
    //   19	781	6	bool3	boolean
    //   25	773	7	n	int
    //   28	702	8	localObject1	Object
    //   31	771	9	i1	int
    //   170	51	10	i2	int
    //   718	14	10	bool4	boolean
    //   778	3	10	i3	int
    //   277	434	11	localObject2	Object
    //   724	10	12	str	String
    // Exception table:
    //   from	to	target	type
    //   103	106	133	finally
    //   111	114	133	finally
    //   115	118	133	finally
    //   120	124	133	finally
    //   124	128	133	finally
    //   128	130	133	finally
    //   134	136	133	finally
    //   157	161	558	finally
    //   224	229	558	finally
    //   242	246	558	finally
    //   251	255	558	finally
    //   256	260	558	finally
    //   261	265	558	finally
    //   274	277	558	finally
    //   282	286	558	finally
    //   288	293	558	finally
    //   296	301	558	finally
    //   305	309	558	finally
    //   312	318	558	finally
    //   318	322	558	finally
    //   323	327	558	finally
    //   329	333	558	finally
    //   336	340	558	finally
    //   343	348	558	finally
    //   351	355	558	finally
    //   357	361	558	finally
    //   364	368	558	finally
    //   373	377	558	finally
    //   378	382	558	finally
    //   383	387	558	finally
    //   396	399	558	finally
    //   404	408	558	finally
    //   410	415	558	finally
    //   418	423	558	finally
    //   427	431	558	finally
    //   434	440	558	finally
    //   440	444	558	finally
    //   445	449	558	finally
    //   451	455	558	finally
    //   458	462	558	finally
    //   467	471	558	finally
    //   472	476	558	finally
    //   477	481	558	finally
    //   490	493	558	finally
    //   498	502	558	finally
    //   504	509	558	finally
    //   512	517	558	finally
    //   521	525	558	finally
    //   528	534	558	finally
    //   534	538	558	finally
    //   539	543	558	finally
    //   545	549	558	finally
    //   563	566	558	finally
    //   567	570	558	finally
    //   571	575	558	finally
    //   577	581	558	finally
    //   582	586	558	finally
    //   588	592	558	finally
    //   592	594	558	finally
    //   595	598	558	finally
    //   600	604	558	finally
    //   606	610	558	finally
    //   610	612	558	finally
    //   157	161	562	java/io/IOException
    //   224	229	562	java/io/IOException
    //   242	246	562	java/io/IOException
    //   251	255	562	java/io/IOException
    //   256	260	562	java/io/IOException
    //   261	265	562	java/io/IOException
    //   274	277	562	java/io/IOException
    //   282	286	562	java/io/IOException
    //   288	293	562	java/io/IOException
    //   296	301	562	java/io/IOException
    //   305	309	562	java/io/IOException
    //   312	318	562	java/io/IOException
    //   318	322	562	java/io/IOException
    //   323	327	562	java/io/IOException
    //   329	333	562	java/io/IOException
    //   336	340	562	java/io/IOException
    //   343	348	562	java/io/IOException
    //   351	355	562	java/io/IOException
    //   357	361	562	java/io/IOException
    //   364	368	562	java/io/IOException
    //   373	377	562	java/io/IOException
    //   378	382	562	java/io/IOException
    //   383	387	562	java/io/IOException
    //   396	399	562	java/io/IOException
    //   404	408	562	java/io/IOException
    //   410	415	562	java/io/IOException
    //   418	423	562	java/io/IOException
    //   427	431	562	java/io/IOException
    //   434	440	562	java/io/IOException
    //   440	444	562	java/io/IOException
    //   445	449	562	java/io/IOException
    //   451	455	562	java/io/IOException
    //   458	462	562	java/io/IOException
    //   467	471	562	java/io/IOException
    //   472	476	562	java/io/IOException
    //   477	481	562	java/io/IOException
    //   490	493	562	java/io/IOException
    //   498	502	562	java/io/IOException
    //   504	509	562	java/io/IOException
    //   512	517	562	java/io/IOException
    //   521	525	562	java/io/IOException
    //   528	534	562	java/io/IOException
    //   534	538	562	java/io/IOException
    //   539	543	562	java/io/IOException
    //   545	549	562	java/io/IOException
    //   157	161	594	com/google/f/x
    //   224	229	594	com/google/f/x
    //   242	246	594	com/google/f/x
    //   251	255	594	com/google/f/x
    //   256	260	594	com/google/f/x
    //   261	265	594	com/google/f/x
    //   274	277	594	com/google/f/x
    //   282	286	594	com/google/f/x
    //   288	293	594	com/google/f/x
    //   296	301	594	com/google/f/x
    //   305	309	594	com/google/f/x
    //   312	318	594	com/google/f/x
    //   318	322	594	com/google/f/x
    //   323	327	594	com/google/f/x
    //   329	333	594	com/google/f/x
    //   336	340	594	com/google/f/x
    //   343	348	594	com/google/f/x
    //   351	355	594	com/google/f/x
    //   357	361	594	com/google/f/x
    //   364	368	594	com/google/f/x
    //   373	377	594	com/google/f/x
    //   378	382	594	com/google/f/x
    //   383	387	594	com/google/f/x
    //   396	399	594	com/google/f/x
    //   404	408	594	com/google/f/x
    //   410	415	594	com/google/f/x
    //   418	423	594	com/google/f/x
    //   427	431	594	com/google/f/x
    //   434	440	594	com/google/f/x
    //   440	444	594	com/google/f/x
    //   445	449	594	com/google/f/x
    //   451	455	594	com/google/f/x
    //   458	462	594	com/google/f/x
    //   467	471	594	com/google/f/x
    //   472	476	594	com/google/f/x
    //   477	481	594	com/google/f/x
    //   490	493	594	com/google/f/x
    //   498	502	594	com/google/f/x
    //   504	509	594	com/google/f/x
    //   512	517	594	com/google/f/x
    //   521	525	594	com/google/f/x
    //   528	534	594	com/google/f/x
    //   534	538	594	com/google/f/x
    //   539	543	594	com/google/f/x
    //   545	549	594	com/google/f/x
  }
  
  public final MessageContent e()
  {
    MessageContent localMessageContent = e;
    if (localMessageContent == null) {
      localMessageContent = MessageContent.h();
    }
    return localMessageContent;
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int k = -1;
    if (i != k) {
      return i;
    }
    Object localObject1 = a;
    k = 0;
    Object localObject2;
    if (localObject1 != null)
    {
      localObject2 = a();
      i = h.computeMessageSize(1, (ae)localObject2);
      k = 0 + i;
    }
    localObject1 = b;
    if (localObject1 != null)
    {
      localObject2 = b();
      i = h.computeMessageSize(2, (ae)localObject2);
      k += i;
    }
    localObject1 = c;
    boolean bool = ((String)localObject1).isEmpty();
    if (!bool)
    {
      localObject2 = c;
      j = h.computeStringSize(3, (String)localObject2);
      k += j;
    }
    int j = d;
    if (j != 0)
    {
      int m = 4;
      j = h.computeInt32Size(m, j);
      k += j;
    }
    localObject1 = e;
    if (localObject1 != null)
    {
      localObject2 = e();
      j = h.computeMessageSize(5, (ae)localObject2);
      k += j;
    }
    memoizedSerializedSize = k;
    return k;
  }
  
  public final void writeTo(h paramh)
  {
    Object localObject1 = a;
    int i;
    Object localObject2;
    if (localObject1 != null)
    {
      i = 1;
      localObject2 = a();
      paramh.writeMessage(i, (ae)localObject2);
    }
    localObject1 = b;
    if (localObject1 != null)
    {
      i = 2;
      localObject2 = b();
      paramh.writeMessage(i, (ae)localObject2);
    }
    localObject1 = c;
    boolean bool = ((String)localObject1).isEmpty();
    if (!bool)
    {
      j = 3;
      localObject2 = c;
      paramh.writeString(j, (String)localObject2);
    }
    int j = d;
    if (j != 0)
    {
      int k = 4;
      paramh.writeInt32(k, j);
    }
    localObject1 = e;
    if (localObject1 != null)
    {
      j = 5;
      localObject2 = e();
      paramh.writeMessage(j, (ae)localObject2);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.events.Event.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */