package com.truecaller.api.services.messenger.v1.events;

import com.google.f.q.a;

public final class Event$a$a
  extends q.a
  implements Event.b
{
  private Event$a$a()
  {
    super(locala);
  }
  
  public final a a(long paramLong)
  {
    copyOnWrite();
    Event.a.a((Event.a)instance, paramLong);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.events.Event.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */