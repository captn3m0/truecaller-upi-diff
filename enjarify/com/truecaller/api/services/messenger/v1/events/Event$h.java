package com.truecaller.api.services.messenger.v1.events;

import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;

public final class Event$h
  extends q
  implements Event.i
{
  private static final h b;
  private static volatile ah c;
  private boolean a;
  
  static
  {
    h localh = new com/truecaller/api/services/messenger/v1/events/Event$h;
    localh.<init>();
    b = localh;
    localh.makeImmutable();
  }
  
  public static h b()
  {
    return b;
  }
  
  public static ah c()
  {
    return b.getParserForType();
  }
  
  public final boolean a()
  {
    return a;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 33	com/truecaller/api/services/messenger/v1/events/Event$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 39	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_0
    //   19: istore 6
    //   21: aconst_null
    //   22: astore 4
    //   24: iload 5
    //   26: tableswitch	default:+46->72, 1:+306->332, 2:+302->328, 3:+300->326, 4:+289->315, 5:+243->269, 6:+108->134, 7:+239->265, 8:+56->82
    //   72: new 41	java/lang/UnsupportedOperationException
    //   75: astore_1
    //   76: aload_1
    //   77: invokespecial 42	java/lang/UnsupportedOperationException:<init>	()V
    //   80: aload_1
    //   81: athrow
    //   82: getstatic 44	com/truecaller/api/services/messenger/v1/events/Event$h:c	Lcom/google/f/ah;
    //   85: astore_1
    //   86: aload_1
    //   87: ifnonnull +43 -> 130
    //   90: ldc 2
    //   92: astore_1
    //   93: aload_1
    //   94: monitorenter
    //   95: getstatic 44	com/truecaller/api/services/messenger/v1/events/Event$h:c	Lcom/google/f/ah;
    //   98: astore_2
    //   99: aload_2
    //   100: ifnonnull +20 -> 120
    //   103: new 46	com/google/f/q$b
    //   106: astore_2
    //   107: getstatic 18	com/truecaller/api/services/messenger/v1/events/Event$h:b	Lcom/truecaller/api/services/messenger/v1/events/Event$h;
    //   110: astore_3
    //   111: aload_2
    //   112: aload_3
    //   113: invokespecial 49	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   116: aload_2
    //   117: putstatic 44	com/truecaller/api/services/messenger/v1/events/Event$h:c	Lcom/google/f/ah;
    //   120: aload_1
    //   121: monitorexit
    //   122: goto +8 -> 130
    //   125: astore_2
    //   126: aload_1
    //   127: monitorexit
    //   128: aload_2
    //   129: athrow
    //   130: getstatic 44	com/truecaller/api/services/messenger/v1/events/Event$h:c	Lcom/google/f/ah;
    //   133: areturn
    //   134: aload_2
    //   135: checkcast 51	com/google/f/g
    //   138: astore_2
    //   139: iconst_1
    //   140: istore 5
    //   142: iload 6
    //   144: ifne +121 -> 265
    //   147: aload_2
    //   148: invokevirtual 55	com/google/f/g:readTag	()I
    //   151: istore 7
    //   153: iload 7
    //   155: ifeq +48 -> 203
    //   158: bipush 8
    //   160: istore 8
    //   162: iload 7
    //   164: iload 8
    //   166: if_icmpeq +22 -> 188
    //   169: aload_2
    //   170: iload 7
    //   172: invokevirtual 60	com/google/f/g:skipField	(I)Z
    //   175: istore 7
    //   177: iload 7
    //   179: ifne -37 -> 142
    //   182: iconst_1
    //   183: istore 6
    //   185: goto -43 -> 142
    //   188: aload_2
    //   189: invokevirtual 64	com/google/f/g:readBool	()Z
    //   192: istore 7
    //   194: aload_0
    //   195: iload 7
    //   197: putfield 28	com/truecaller/api/services/messenger/v1/events/Event$h:a	Z
    //   200: goto -58 -> 142
    //   203: iconst_1
    //   204: istore 6
    //   206: goto -64 -> 142
    //   209: astore_1
    //   210: goto +53 -> 263
    //   213: astore_1
    //   214: new 66	java/lang/RuntimeException
    //   217: astore_2
    //   218: new 68	com/google/f/x
    //   221: astore_3
    //   222: aload_1
    //   223: invokevirtual 74	java/io/IOException:getMessage	()Ljava/lang/String;
    //   226: astore_1
    //   227: aload_3
    //   228: aload_1
    //   229: invokespecial 77	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   232: aload_3
    //   233: aload_0
    //   234: invokevirtual 81	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   237: astore_1
    //   238: aload_2
    //   239: aload_1
    //   240: invokespecial 84	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   243: aload_2
    //   244: athrow
    //   245: astore_1
    //   246: new 66	java/lang/RuntimeException
    //   249: astore_2
    //   250: aload_1
    //   251: aload_0
    //   252: invokevirtual 81	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   255: astore_1
    //   256: aload_2
    //   257: aload_1
    //   258: invokespecial 84	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   261: aload_2
    //   262: athrow
    //   263: aload_1
    //   264: athrow
    //   265: getstatic 18	com/truecaller/api/services/messenger/v1/events/Event$h:b	Lcom/truecaller/api/services/messenger/v1/events/Event$h;
    //   268: areturn
    //   269: aload_2
    //   270: checkcast 86	com/google/f/q$k
    //   273: astore_2
    //   274: aload_3
    //   275: checkcast 2	com/truecaller/api/services/messenger/v1/events/Event$h
    //   278: astore_3
    //   279: aload_0
    //   280: getfield 28	com/truecaller/api/services/messenger/v1/events/Event$h:a	Z
    //   283: istore 5
    //   285: aload_3
    //   286: getfield 28	com/truecaller/api/services/messenger/v1/events/Event$h:a	Z
    //   289: istore 7
    //   291: aload_2
    //   292: iload 5
    //   294: iload 5
    //   296: iload 7
    //   298: iload 7
    //   300: invokeinterface 90 5 0
    //   305: istore 5
    //   307: aload_0
    //   308: iload 5
    //   310: putfield 28	com/truecaller/api/services/messenger/v1/events/Event$h:a	Z
    //   313: aload_0
    //   314: areturn
    //   315: new 92	com/truecaller/api/services/messenger/v1/events/Event$h$a
    //   318: astore_1
    //   319: aload_1
    //   320: iconst_0
    //   321: invokespecial 95	com/truecaller/api/services/messenger/v1/events/Event$h$a:<init>	(B)V
    //   324: aload_1
    //   325: areturn
    //   326: aconst_null
    //   327: areturn
    //   328: getstatic 18	com/truecaller/api/services/messenger/v1/events/Event$h:b	Lcom/truecaller/api/services/messenger/v1/events/Event$h;
    //   331: areturn
    //   332: new 2	com/truecaller/api/services/messenger/v1/events/Event$h
    //   335: astore_1
    //   336: aload_1
    //   337: invokespecial 16	com/truecaller/api/services/messenger/v1/events/Event$h:<init>	()V
    //   340: aload_1
    //   341: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	342	0	this	h
    //   0	342	1	paramj	com.google.f.q.j
    //   0	342	2	paramObject1	Object
    //   0	342	3	paramObject2	Object
    //   3	20	4	arrayOfInt	int[]
    //   9	132	5	i	int
    //   283	26	5	bool1	boolean
    //   19	186	6	j	int
    //   151	20	7	k	int
    //   175	124	7	bool2	boolean
    //   160	7	8	m	int
    // Exception table:
    //   from	to	target	type
    //   95	98	125	finally
    //   103	106	125	finally
    //   107	110	125	finally
    //   112	116	125	finally
    //   116	120	125	finally
    //   120	122	125	finally
    //   126	128	125	finally
    //   147	151	209	finally
    //   170	175	209	finally
    //   188	192	209	finally
    //   195	200	209	finally
    //   214	217	209	finally
    //   218	221	209	finally
    //   222	226	209	finally
    //   228	232	209	finally
    //   233	237	209	finally
    //   239	243	209	finally
    //   243	245	209	finally
    //   246	249	209	finally
    //   251	255	209	finally
    //   257	261	209	finally
    //   261	263	209	finally
    //   147	151	213	java/io/IOException
    //   170	175	213	java/io/IOException
    //   188	192	213	java/io/IOException
    //   195	200	213	java/io/IOException
    //   147	151	245	com/google/f/x
    //   170	175	245	com/google/f/x
    //   188	192	245	com/google/f/x
    //   195	200	245	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int k = -1;
    if (i != k) {
      return i;
    }
    boolean bool = a;
    k = 0;
    if (bool)
    {
      int m = 1;
      int j = h.computeBoolSize(m, bool);
      k = 0 + j;
    }
    memoizedSerializedSize = k;
    return k;
  }
  
  public final void writeTo(h paramh)
  {
    boolean bool = a;
    if (bool)
    {
      int i = 1;
      paramh.writeBool(i, bool);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.events.Event.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */