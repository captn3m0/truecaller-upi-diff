package com.truecaller.api.services.messenger.v1.events;

import com.google.f.ae;
import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;
import com.truecaller.api.services.messenger.v1.models.Peer;
import com.truecaller.api.services.messenger.v1.models.ReportType;

public final class Event$v
  extends q
  implements Event.w
{
  private static final v f;
  private static volatile ah g;
  private String a = "";
  private Peer b;
  private String c = "";
  private int d;
  private int e;
  
  static
  {
    v localv = new com/truecaller/api/services/messenger/v1/events/Event$v;
    localv.<init>();
    f = localv;
    localv.makeImmutable();
  }
  
  public static v f()
  {
    return f;
  }
  
  public static ah g()
  {
    return f.getParserForType();
  }
  
  public final String a()
  {
    return a;
  }
  
  public final Peer b()
  {
    Peer localPeer = b;
    if (localPeer == null) {
      localPeer = Peer.d();
    }
    return localPeer;
  }
  
  public final String c()
  {
    return c;
  }
  
  public final int d()
  {
    return d;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 52	com/truecaller/api/services/messenger/v1/events/Event$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 58	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_0
    //   19: istore 6
    //   21: aconst_null
    //   22: astore 4
    //   24: iconst_0
    //   25: istore 7
    //   27: iconst_1
    //   28: istore 8
    //   30: iload 5
    //   32: tableswitch	default:+48->80, 1:+749->781, 2:+745->777, 3:+743->775, 4:+732->764, 5:+426->458, 6:+110->142, 7:+422->454, 8:+58->90
    //   80: new 61	java/lang/UnsupportedOperationException
    //   83: astore_1
    //   84: aload_1
    //   85: invokespecial 62	java/lang/UnsupportedOperationException:<init>	()V
    //   88: aload_1
    //   89: athrow
    //   90: getstatic 64	com/truecaller/api/services/messenger/v1/events/Event$v:g	Lcom/google/f/ah;
    //   93: astore_1
    //   94: aload_1
    //   95: ifnonnull +43 -> 138
    //   98: ldc 2
    //   100: astore_1
    //   101: aload_1
    //   102: monitorenter
    //   103: getstatic 64	com/truecaller/api/services/messenger/v1/events/Event$v:g	Lcom/google/f/ah;
    //   106: astore_2
    //   107: aload_2
    //   108: ifnonnull +20 -> 128
    //   111: new 66	com/google/f/q$b
    //   114: astore_2
    //   115: getstatic 24	com/truecaller/api/services/messenger/v1/events/Event$v:f	Lcom/truecaller/api/services/messenger/v1/events/Event$v;
    //   118: astore_3
    //   119: aload_2
    //   120: aload_3
    //   121: invokespecial 69	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   124: aload_2
    //   125: putstatic 64	com/truecaller/api/services/messenger/v1/events/Event$v:g	Lcom/google/f/ah;
    //   128: aload_1
    //   129: monitorexit
    //   130: goto +8 -> 138
    //   133: astore_2
    //   134: aload_1
    //   135: monitorexit
    //   136: aload_2
    //   137: athrow
    //   138: getstatic 64	com/truecaller/api/services/messenger/v1/events/Event$v:g	Lcom/google/f/ah;
    //   141: areturn
    //   142: aload_2
    //   143: checkcast 71	com/google/f/g
    //   146: astore_2
    //   147: aload_3
    //   148: checkcast 73	com/google/f/n
    //   151: astore_3
    //   152: iload 7
    //   154: ifne +300 -> 454
    //   157: aload_2
    //   158: invokevirtual 76	com/google/f/g:readTag	()I
    //   161: istore 5
    //   163: iload 5
    //   165: ifeq +227 -> 392
    //   168: bipush 10
    //   170: istore 9
    //   172: iload 5
    //   174: iload 9
    //   176: if_icmpeq +203 -> 379
    //   179: bipush 18
    //   181: istore 9
    //   183: iload 5
    //   185: iload 9
    //   187: if_icmpeq +98 -> 285
    //   190: bipush 26
    //   192: istore 9
    //   194: iload 5
    //   196: iload 9
    //   198: if_icmpeq +74 -> 272
    //   201: bipush 32
    //   203: istore 9
    //   205: iload 5
    //   207: iload 9
    //   209: if_icmpeq +48 -> 257
    //   212: bipush 40
    //   214: istore 9
    //   216: iload 5
    //   218: iload 9
    //   220: if_icmpeq +22 -> 242
    //   223: aload_2
    //   224: iload 5
    //   226: invokevirtual 85	com/google/f/g:skipField	(I)Z
    //   229: istore 5
    //   231: iload 5
    //   233: ifne -81 -> 152
    //   236: iconst_1
    //   237: istore 7
    //   239: goto -87 -> 152
    //   242: aload_2
    //   243: invokevirtual 88	com/google/f/g:readEnum	()I
    //   246: istore 5
    //   248: aload_0
    //   249: iload 5
    //   251: putfield 90	com/truecaller/api/services/messenger/v1/events/Event$v:e	I
    //   254: goto -102 -> 152
    //   257: aload_2
    //   258: invokevirtual 93	com/google/f/g:readInt32	()I
    //   261: istore 5
    //   263: aload_0
    //   264: iload 5
    //   266: putfield 47	com/truecaller/api/services/messenger/v1/events/Event$v:d	I
    //   269: goto -117 -> 152
    //   272: aload_2
    //   273: invokevirtual 97	com/google/f/g:readStringRequireUtf8	()Ljava/lang/String;
    //   276: astore_1
    //   277: aload_0
    //   278: aload_1
    //   279: putfield 34	com/truecaller/api/services/messenger/v1/events/Event$v:c	Ljava/lang/String;
    //   282: goto -130 -> 152
    //   285: aload_0
    //   286: getfield 40	com/truecaller/api/services/messenger/v1/events/Event$v:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   289: astore_1
    //   290: aload_1
    //   291: ifnull +21 -> 312
    //   294: aload_0
    //   295: getfield 40	com/truecaller/api/services/messenger/v1/events/Event$v:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   298: astore_1
    //   299: aload_1
    //   300: invokevirtual 101	com/truecaller/api/services/messenger/v1/models/Peer:toBuilder	()Lcom/google/f/q$a;
    //   303: astore_1
    //   304: aload_1
    //   305: checkcast 103	com/truecaller/api/services/messenger/v1/models/Peer$a
    //   308: astore_1
    //   309: goto +8 -> 317
    //   312: iconst_0
    //   313: istore 5
    //   315: aconst_null
    //   316: astore_1
    //   317: invokestatic 105	com/truecaller/api/services/messenger/v1/models/Peer:e	()Lcom/google/f/ah;
    //   320: astore 10
    //   322: aload_2
    //   323: aload 10
    //   325: aload_3
    //   326: invokevirtual 109	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   329: astore 10
    //   331: aload 10
    //   333: checkcast 42	com/truecaller/api/services/messenger/v1/models/Peer
    //   336: astore 10
    //   338: aload_0
    //   339: aload 10
    //   341: putfield 40	com/truecaller/api/services/messenger/v1/events/Event$v:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   344: aload_1
    //   345: ifnull -193 -> 152
    //   348: aload_0
    //   349: getfield 40	com/truecaller/api/services/messenger/v1/events/Event$v:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   352: astore 10
    //   354: aload_1
    //   355: aload 10
    //   357: invokevirtual 113	com/truecaller/api/services/messenger/v1/models/Peer$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   360: pop
    //   361: aload_1
    //   362: invokevirtual 117	com/truecaller/api/services/messenger/v1/models/Peer$a:buildPartial	()Lcom/google/f/q;
    //   365: astore_1
    //   366: aload_1
    //   367: checkcast 42	com/truecaller/api/services/messenger/v1/models/Peer
    //   370: astore_1
    //   371: aload_0
    //   372: aload_1
    //   373: putfield 40	com/truecaller/api/services/messenger/v1/events/Event$v:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   376: goto -224 -> 152
    //   379: aload_2
    //   380: invokevirtual 97	com/google/f/g:readStringRequireUtf8	()Ljava/lang/String;
    //   383: astore_1
    //   384: aload_0
    //   385: aload_1
    //   386: putfield 32	com/truecaller/api/services/messenger/v1/events/Event$v:a	Ljava/lang/String;
    //   389: goto -237 -> 152
    //   392: iconst_1
    //   393: istore 7
    //   395: goto -243 -> 152
    //   398: astore_1
    //   399: goto +53 -> 452
    //   402: astore_1
    //   403: new 119	java/lang/RuntimeException
    //   406: astore_2
    //   407: new 121	com/google/f/x
    //   410: astore_3
    //   411: aload_1
    //   412: invokevirtual 126	java/io/IOException:getMessage	()Ljava/lang/String;
    //   415: astore_1
    //   416: aload_3
    //   417: aload_1
    //   418: invokespecial 129	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   421: aload_3
    //   422: aload_0
    //   423: invokevirtual 133	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   426: astore_1
    //   427: aload_2
    //   428: aload_1
    //   429: invokespecial 136	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   432: aload_2
    //   433: athrow
    //   434: astore_1
    //   435: new 119	java/lang/RuntimeException
    //   438: astore_2
    //   439: aload_1
    //   440: aload_0
    //   441: invokevirtual 133	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   444: astore_1
    //   445: aload_2
    //   446: aload_1
    //   447: invokespecial 136	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   450: aload_2
    //   451: athrow
    //   452: aload_1
    //   453: athrow
    //   454: getstatic 24	com/truecaller/api/services/messenger/v1/events/Event$v:f	Lcom/truecaller/api/services/messenger/v1/events/Event$v;
    //   457: areturn
    //   458: aload_2
    //   459: checkcast 138	com/google/f/q$k
    //   462: astore_2
    //   463: aload_3
    //   464: checkcast 2	com/truecaller/api/services/messenger/v1/events/Event$v
    //   467: astore_3
    //   468: aload_0
    //   469: getfield 32	com/truecaller/api/services/messenger/v1/events/Event$v:a	Ljava/lang/String;
    //   472: invokevirtual 144	java/lang/String:isEmpty	()Z
    //   475: iload 8
    //   477: ixor
    //   478: istore 5
    //   480: aload_0
    //   481: getfield 32	com/truecaller/api/services/messenger/v1/events/Event$v:a	Ljava/lang/String;
    //   484: astore 4
    //   486: aload_3
    //   487: getfield 32	com/truecaller/api/services/messenger/v1/events/Event$v:a	Ljava/lang/String;
    //   490: invokevirtual 144	java/lang/String:isEmpty	()Z
    //   493: iload 8
    //   495: ixor
    //   496: istore 9
    //   498: aload_3
    //   499: getfield 32	com/truecaller/api/services/messenger/v1/events/Event$v:a	Ljava/lang/String;
    //   502: astore 11
    //   504: aload_2
    //   505: iload 5
    //   507: aload 4
    //   509: iload 9
    //   511: aload 11
    //   513: invokeinterface 148 5 0
    //   518: astore_1
    //   519: aload_0
    //   520: aload_1
    //   521: putfield 32	com/truecaller/api/services/messenger/v1/events/Event$v:a	Ljava/lang/String;
    //   524: aload_0
    //   525: getfield 40	com/truecaller/api/services/messenger/v1/events/Event$v:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   528: astore_1
    //   529: aload_3
    //   530: getfield 40	com/truecaller/api/services/messenger/v1/events/Event$v:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   533: astore 4
    //   535: aload_2
    //   536: aload_1
    //   537: aload 4
    //   539: invokeinterface 152 3 0
    //   544: checkcast 42	com/truecaller/api/services/messenger/v1/models/Peer
    //   547: astore_1
    //   548: aload_0
    //   549: aload_1
    //   550: putfield 40	com/truecaller/api/services/messenger/v1/events/Event$v:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   553: aload_0
    //   554: getfield 34	com/truecaller/api/services/messenger/v1/events/Event$v:c	Ljava/lang/String;
    //   557: invokevirtual 144	java/lang/String:isEmpty	()Z
    //   560: iload 8
    //   562: ixor
    //   563: istore 5
    //   565: aload_0
    //   566: getfield 34	com/truecaller/api/services/messenger/v1/events/Event$v:c	Ljava/lang/String;
    //   569: astore 4
    //   571: aload_3
    //   572: getfield 34	com/truecaller/api/services/messenger/v1/events/Event$v:c	Ljava/lang/String;
    //   575: astore 10
    //   577: aload 10
    //   579: invokevirtual 144	java/lang/String:isEmpty	()Z
    //   582: iload 8
    //   584: ixor
    //   585: istore 9
    //   587: aload_3
    //   588: getfield 34	com/truecaller/api/services/messenger/v1/events/Event$v:c	Ljava/lang/String;
    //   591: astore 11
    //   593: aload_2
    //   594: iload 5
    //   596: aload 4
    //   598: iload 9
    //   600: aload 11
    //   602: invokeinterface 148 5 0
    //   607: astore_1
    //   608: aload_0
    //   609: aload_1
    //   610: putfield 34	com/truecaller/api/services/messenger/v1/events/Event$v:c	Ljava/lang/String;
    //   613: aload_0
    //   614: getfield 47	com/truecaller/api/services/messenger/v1/events/Event$v:d	I
    //   617: istore 5
    //   619: iload 5
    //   621: ifeq +9 -> 630
    //   624: iconst_1
    //   625: istore 5
    //   627: goto +8 -> 635
    //   630: iconst_0
    //   631: istore 5
    //   633: aconst_null
    //   634: astore_1
    //   635: aload_0
    //   636: getfield 47	com/truecaller/api/services/messenger/v1/events/Event$v:d	I
    //   639: istore 6
    //   641: aload_3
    //   642: getfield 47	com/truecaller/api/services/messenger/v1/events/Event$v:d	I
    //   645: istore 9
    //   647: iload 9
    //   649: ifeq +9 -> 658
    //   652: iconst_1
    //   653: istore 9
    //   655: goto +9 -> 664
    //   658: iconst_0
    //   659: istore 9
    //   661: aconst_null
    //   662: astore 10
    //   664: aload_3
    //   665: getfield 47	com/truecaller/api/services/messenger/v1/events/Event$v:d	I
    //   668: istore 12
    //   670: aload_2
    //   671: iload 5
    //   673: iload 6
    //   675: iload 9
    //   677: iload 12
    //   679: invokeinterface 156 5 0
    //   684: istore 5
    //   686: aload_0
    //   687: iload 5
    //   689: putfield 47	com/truecaller/api/services/messenger/v1/events/Event$v:d	I
    //   692: aload_0
    //   693: getfield 90	com/truecaller/api/services/messenger/v1/events/Event$v:e	I
    //   696: istore 5
    //   698: iload 5
    //   700: ifeq +9 -> 709
    //   703: iconst_1
    //   704: istore 5
    //   706: goto +8 -> 714
    //   709: iconst_0
    //   710: istore 5
    //   712: aconst_null
    //   713: astore_1
    //   714: aload_0
    //   715: getfield 90	com/truecaller/api/services/messenger/v1/events/Event$v:e	I
    //   718: istore 6
    //   720: aload_3
    //   721: getfield 90	com/truecaller/api/services/messenger/v1/events/Event$v:e	I
    //   724: istore 9
    //   726: iload 9
    //   728: ifeq +6 -> 734
    //   731: iconst_1
    //   732: istore 7
    //   734: aload_3
    //   735: getfield 90	com/truecaller/api/services/messenger/v1/events/Event$v:e	I
    //   738: istore 13
    //   740: aload_2
    //   741: iload 5
    //   743: iload 6
    //   745: iload 7
    //   747: iload 13
    //   749: invokeinterface 156 5 0
    //   754: istore 5
    //   756: aload_0
    //   757: iload 5
    //   759: putfield 90	com/truecaller/api/services/messenger/v1/events/Event$v:e	I
    //   762: aload_0
    //   763: areturn
    //   764: new 158	com/truecaller/api/services/messenger/v1/events/Event$v$a
    //   767: astore_1
    //   768: aload_1
    //   769: iconst_0
    //   770: invokespecial 161	com/truecaller/api/services/messenger/v1/events/Event$v$a:<init>	(B)V
    //   773: aload_1
    //   774: areturn
    //   775: aconst_null
    //   776: areturn
    //   777: getstatic 24	com/truecaller/api/services/messenger/v1/events/Event$v:f	Lcom/truecaller/api/services/messenger/v1/events/Event$v;
    //   780: areturn
    //   781: new 2	com/truecaller/api/services/messenger/v1/events/Event$v
    //   784: astore_1
    //   785: aload_1
    //   786: invokespecial 22	com/truecaller/api/services/messenger/v1/events/Event$v:<init>	()V
    //   789: aload_1
    //   790: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	791	0	this	v
    //   0	791	1	paramj	com.google.f.q.j
    //   0	791	2	paramObject1	Object
    //   0	791	3	paramObject2	Object
    //   3	594	4	localObject1	Object
    //   9	216	5	i	int
    //   229	3	5	bool1	boolean
    //   246	68	5	j	int
    //   478	117	5	bool2	boolean
    //   617	55	5	k	int
    //   684	58	5	m	int
    //   754	4	5	n	int
    //   19	725	6	i1	int
    //   25	721	7	bool3	boolean
    //   28	557	8	i2	int
    //   170	51	9	i3	int
    //   496	103	9	bool4	boolean
    //   645	31	9	i4	int
    //   724	3	9	i5	int
    //   320	343	10	localObject2	Object
    //   502	99	11	str	String
    //   668	10	12	i6	int
    //   738	10	13	i7	int
    // Exception table:
    //   from	to	target	type
    //   103	106	133	finally
    //   111	114	133	finally
    //   115	118	133	finally
    //   120	124	133	finally
    //   124	128	133	finally
    //   128	130	133	finally
    //   134	136	133	finally
    //   157	161	398	finally
    //   224	229	398	finally
    //   242	246	398	finally
    //   249	254	398	finally
    //   257	261	398	finally
    //   264	269	398	finally
    //   272	276	398	finally
    //   278	282	398	finally
    //   285	289	398	finally
    //   294	298	398	finally
    //   299	303	398	finally
    //   304	308	398	finally
    //   317	320	398	finally
    //   325	329	398	finally
    //   331	336	398	finally
    //   339	344	398	finally
    //   348	352	398	finally
    //   355	361	398	finally
    //   361	365	398	finally
    //   366	370	398	finally
    //   372	376	398	finally
    //   379	383	398	finally
    //   385	389	398	finally
    //   403	406	398	finally
    //   407	410	398	finally
    //   411	415	398	finally
    //   417	421	398	finally
    //   422	426	398	finally
    //   428	432	398	finally
    //   432	434	398	finally
    //   435	438	398	finally
    //   440	444	398	finally
    //   446	450	398	finally
    //   450	452	398	finally
    //   157	161	402	java/io/IOException
    //   224	229	402	java/io/IOException
    //   242	246	402	java/io/IOException
    //   249	254	402	java/io/IOException
    //   257	261	402	java/io/IOException
    //   264	269	402	java/io/IOException
    //   272	276	402	java/io/IOException
    //   278	282	402	java/io/IOException
    //   285	289	402	java/io/IOException
    //   294	298	402	java/io/IOException
    //   299	303	402	java/io/IOException
    //   304	308	402	java/io/IOException
    //   317	320	402	java/io/IOException
    //   325	329	402	java/io/IOException
    //   331	336	402	java/io/IOException
    //   339	344	402	java/io/IOException
    //   348	352	402	java/io/IOException
    //   355	361	402	java/io/IOException
    //   361	365	402	java/io/IOException
    //   366	370	402	java/io/IOException
    //   372	376	402	java/io/IOException
    //   379	383	402	java/io/IOException
    //   385	389	402	java/io/IOException
    //   157	161	434	com/google/f/x
    //   224	229	434	com/google/f/x
    //   242	246	434	com/google/f/x
    //   249	254	434	com/google/f/x
    //   257	261	434	com/google/f/x
    //   264	269	434	com/google/f/x
    //   272	276	434	com/google/f/x
    //   278	282	434	com/google/f/x
    //   285	289	434	com/google/f/x
    //   294	298	434	com/google/f/x
    //   299	303	434	com/google/f/x
    //   304	308	434	com/google/f/x
    //   317	320	434	com/google/f/x
    //   325	329	434	com/google/f/x
    //   331	336	434	com/google/f/x
    //   339	344	434	com/google/f/x
    //   348	352	434	com/google/f/x
    //   355	361	434	com/google/f/x
    //   361	365	434	com/google/f/x
    //   366	370	434	com/google/f/x
    //   372	376	434	com/google/f/x
    //   379	383	434	com/google/f/x
    //   385	389	434	com/google/f/x
  }
  
  public final int e()
  {
    return e;
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int m = -1;
    if (i != m) {
      return i;
    }
    Object localObject1 = a;
    boolean bool1 = ((String)localObject1).isEmpty();
    m = 0;
    int j;
    if (!bool1)
    {
      localObject2 = a;
      j = h.computeStringSize(1, (String)localObject2);
      m = 0 + j;
    }
    localObject1 = b;
    if (localObject1 != null)
    {
      localObject2 = b();
      j = h.computeMessageSize(2, (ae)localObject2);
      m += j;
    }
    localObject1 = c;
    boolean bool2 = ((String)localObject1).isEmpty();
    if (!bool2)
    {
      localObject2 = c;
      k = h.computeStringSize(3, (String)localObject2);
      m += k;
    }
    int k = d;
    if (k != 0)
    {
      n = 4;
      k = h.computeInt32Size(n, k);
      m += k;
    }
    k = e;
    Object localObject2 = ReportType.UNKNOWN_REPORT_TYPE;
    int n = ((ReportType)localObject2).getNumber();
    if (k != n)
    {
      n = e;
      k = h.computeEnumSize(5, n);
      m += k;
    }
    memoizedSerializedSize = m;
    return m;
  }
  
  public final void writeTo(h paramh)
  {
    Object localObject1 = a;
    int i = ((String)localObject1).isEmpty();
    if (i == 0)
    {
      i = 1;
      localObject2 = a;
      paramh.writeString(i, (String)localObject2);
    }
    localObject1 = b;
    if (localObject1 != null)
    {
      int j = 2;
      localObject2 = b();
      paramh.writeMessage(j, (ae)localObject2);
    }
    localObject1 = c;
    boolean bool = ((String)localObject1).isEmpty();
    if (!bool)
    {
      k = 3;
      localObject2 = c;
      paramh.writeString(k, (String)localObject2);
    }
    int k = d;
    if (k != 0)
    {
      m = 4;
      paramh.writeInt32(m, k);
    }
    k = e;
    Object localObject2 = ReportType.UNKNOWN_REPORT_TYPE;
    int m = ((ReportType)localObject2).getNumber();
    if (k != m)
    {
      k = 5;
      m = e;
      paramh.writeEnum(k, m);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.events.Event.v
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */