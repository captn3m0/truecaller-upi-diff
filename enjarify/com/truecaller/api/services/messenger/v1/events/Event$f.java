package com.truecaller.api.services.messenger.v1.events;

import com.google.f.ae;
import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;
import com.truecaller.api.services.messenger.v1.models.Peer;
import com.truecaller.api.services.messenger.v1.models.c;

public final class Event$f
  extends q
  implements Event.g
{
  private static final f f;
  private static volatile ah g;
  private Peer a;
  private String b = "";
  private String c = "";
  private int d;
  private c e;
  
  static
  {
    f localf = new com/truecaller/api/services/messenger/v1/events/Event$f;
    localf.<init>();
    f = localf;
    localf.makeImmutable();
  }
  
  public static f e()
  {
    return f;
  }
  
  public static ah f()
  {
    return f.getParserForType();
  }
  
  public final Peer a()
  {
    Peer localPeer = a;
    if (localPeer == null) {
      localPeer = Peer.d();
    }
    return localPeer;
  }
  
  public final String b()
  {
    return b;
  }
  
  public final String c()
  {
    return c;
  }
  
  public final c d()
  {
    c localc = e;
    if (localc == null) {
      localc = c.c();
    }
    return localc;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 58	com/truecaller/api/services/messenger/v1/events/Event$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 64	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_0
    //   19: istore 6
    //   21: aconst_null
    //   22: astore 4
    //   24: iconst_0
    //   25: istore 7
    //   27: iconst_1
    //   28: istore 8
    //   30: iload 5
    //   32: tableswitch	default:+48->80, 1:+776->808, 2:+772->804, 3:+770->802, 4:+759->791, 5:+505->537, 6:+110->142, 7:+501->533, 8:+58->90
    //   80: new 67	java/lang/UnsupportedOperationException
    //   83: astore_1
    //   84: aload_1
    //   85: invokespecial 68	java/lang/UnsupportedOperationException:<init>	()V
    //   88: aload_1
    //   89: athrow
    //   90: getstatic 70	com/truecaller/api/services/messenger/v1/events/Event$f:g	Lcom/google/f/ah;
    //   93: astore_1
    //   94: aload_1
    //   95: ifnonnull +43 -> 138
    //   98: ldc 2
    //   100: astore_1
    //   101: aload_1
    //   102: monitorenter
    //   103: getstatic 70	com/truecaller/api/services/messenger/v1/events/Event$f:g	Lcom/google/f/ah;
    //   106: astore_2
    //   107: aload_2
    //   108: ifnonnull +20 -> 128
    //   111: new 72	com/google/f/q$b
    //   114: astore_2
    //   115: getstatic 25	com/truecaller/api/services/messenger/v1/events/Event$f:f	Lcom/truecaller/api/services/messenger/v1/events/Event$f;
    //   118: astore_3
    //   119: aload_2
    //   120: aload_3
    //   121: invokespecial 75	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   124: aload_2
    //   125: putstatic 70	com/truecaller/api/services/messenger/v1/events/Event$f:g	Lcom/google/f/ah;
    //   128: aload_1
    //   129: monitorexit
    //   130: goto +8 -> 138
    //   133: astore_2
    //   134: aload_1
    //   135: monitorexit
    //   136: aload_2
    //   137: athrow
    //   138: getstatic 70	com/truecaller/api/services/messenger/v1/events/Event$f:g	Lcom/google/f/ah;
    //   141: areturn
    //   142: aload_2
    //   143: checkcast 77	com/google/f/g
    //   146: astore_2
    //   147: aload_3
    //   148: checkcast 79	com/google/f/n
    //   151: astore_3
    //   152: iload 7
    //   154: ifne +379 -> 533
    //   157: aload_2
    //   158: invokevirtual 82	com/google/f/g:readTag	()I
    //   161: istore 5
    //   163: iload 5
    //   165: ifeq +306 -> 471
    //   168: bipush 10
    //   170: istore 9
    //   172: iload 5
    //   174: iload 9
    //   176: if_icmpeq +201 -> 377
    //   179: bipush 18
    //   181: istore 9
    //   183: iload 5
    //   185: iload 9
    //   187: if_icmpeq +177 -> 364
    //   190: bipush 26
    //   192: istore 9
    //   194: iload 5
    //   196: iload 9
    //   198: if_icmpeq +153 -> 351
    //   201: bipush 32
    //   203: istore 9
    //   205: iload 5
    //   207: iload 9
    //   209: if_icmpeq +127 -> 336
    //   212: bipush 42
    //   214: istore 9
    //   216: iload 5
    //   218: iload 9
    //   220: if_icmpeq +22 -> 242
    //   223: aload_2
    //   224: iload 5
    //   226: invokevirtual 91	com/google/f/g:skipField	(I)Z
    //   229: istore 5
    //   231: iload 5
    //   233: ifne -81 -> 152
    //   236: iconst_1
    //   237: istore 7
    //   239: goto -87 -> 152
    //   242: aload_0
    //   243: getfield 48	com/truecaller/api/services/messenger/v1/events/Event$f:e	Lcom/truecaller/api/services/messenger/v1/models/c;
    //   246: astore_1
    //   247: aload_1
    //   248: ifnull +21 -> 269
    //   251: aload_0
    //   252: getfield 48	com/truecaller/api/services/messenger/v1/events/Event$f:e	Lcom/truecaller/api/services/messenger/v1/models/c;
    //   255: astore_1
    //   256: aload_1
    //   257: invokevirtual 95	com/truecaller/api/services/messenger/v1/models/c:toBuilder	()Lcom/google/f/q$a;
    //   260: astore_1
    //   261: aload_1
    //   262: checkcast 97	com/truecaller/api/services/messenger/v1/models/c$a
    //   265: astore_1
    //   266: goto +8 -> 274
    //   269: iconst_0
    //   270: istore 5
    //   272: aconst_null
    //   273: astore_1
    //   274: invokestatic 99	com/truecaller/api/services/messenger/v1/models/c:d	()Lcom/google/f/ah;
    //   277: astore 10
    //   279: aload_2
    //   280: aload 10
    //   282: aload_3
    //   283: invokevirtual 103	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   286: astore 10
    //   288: aload 10
    //   290: checkcast 50	com/truecaller/api/services/messenger/v1/models/c
    //   293: astore 10
    //   295: aload_0
    //   296: aload 10
    //   298: putfield 48	com/truecaller/api/services/messenger/v1/events/Event$f:e	Lcom/truecaller/api/services/messenger/v1/models/c;
    //   301: aload_1
    //   302: ifnull -150 -> 152
    //   305: aload_0
    //   306: getfield 48	com/truecaller/api/services/messenger/v1/events/Event$f:e	Lcom/truecaller/api/services/messenger/v1/models/c;
    //   309: astore 10
    //   311: aload_1
    //   312: aload 10
    //   314: invokevirtual 107	com/truecaller/api/services/messenger/v1/models/c$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   317: pop
    //   318: aload_1
    //   319: invokevirtual 111	com/truecaller/api/services/messenger/v1/models/c$a:buildPartial	()Lcom/google/f/q;
    //   322: astore_1
    //   323: aload_1
    //   324: checkcast 50	com/truecaller/api/services/messenger/v1/models/c
    //   327: astore_1
    //   328: aload_0
    //   329: aload_1
    //   330: putfield 48	com/truecaller/api/services/messenger/v1/events/Event$f:e	Lcom/truecaller/api/services/messenger/v1/models/c;
    //   333: goto -181 -> 152
    //   336: aload_2
    //   337: invokevirtual 114	com/google/f/g:readInt32	()I
    //   340: istore 5
    //   342: aload_0
    //   343: iload 5
    //   345: putfield 116	com/truecaller/api/services/messenger/v1/events/Event$f:d	I
    //   348: goto -196 -> 152
    //   351: aload_2
    //   352: invokevirtual 120	com/google/f/g:readStringRequireUtf8	()Ljava/lang/String;
    //   355: astore_1
    //   356: aload_0
    //   357: aload_1
    //   358: putfield 35	com/truecaller/api/services/messenger/v1/events/Event$f:c	Ljava/lang/String;
    //   361: goto -209 -> 152
    //   364: aload_2
    //   365: invokevirtual 120	com/google/f/g:readStringRequireUtf8	()Ljava/lang/String;
    //   368: astore_1
    //   369: aload_0
    //   370: aload_1
    //   371: putfield 33	com/truecaller/api/services/messenger/v1/events/Event$f:b	Ljava/lang/String;
    //   374: goto -222 -> 152
    //   377: aload_0
    //   378: getfield 41	com/truecaller/api/services/messenger/v1/events/Event$f:a	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   381: astore_1
    //   382: aload_1
    //   383: ifnull +21 -> 404
    //   386: aload_0
    //   387: getfield 41	com/truecaller/api/services/messenger/v1/events/Event$f:a	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   390: astore_1
    //   391: aload_1
    //   392: invokevirtual 121	com/truecaller/api/services/messenger/v1/models/Peer:toBuilder	()Lcom/google/f/q$a;
    //   395: astore_1
    //   396: aload_1
    //   397: checkcast 123	com/truecaller/api/services/messenger/v1/models/Peer$a
    //   400: astore_1
    //   401: goto +8 -> 409
    //   404: iconst_0
    //   405: istore 5
    //   407: aconst_null
    //   408: astore_1
    //   409: invokestatic 125	com/truecaller/api/services/messenger/v1/models/Peer:e	()Lcom/google/f/ah;
    //   412: astore 10
    //   414: aload_2
    //   415: aload 10
    //   417: aload_3
    //   418: invokevirtual 103	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   421: astore 10
    //   423: aload 10
    //   425: checkcast 43	com/truecaller/api/services/messenger/v1/models/Peer
    //   428: astore 10
    //   430: aload_0
    //   431: aload 10
    //   433: putfield 41	com/truecaller/api/services/messenger/v1/events/Event$f:a	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   436: aload_1
    //   437: ifnull -285 -> 152
    //   440: aload_0
    //   441: getfield 41	com/truecaller/api/services/messenger/v1/events/Event$f:a	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   444: astore 10
    //   446: aload_1
    //   447: aload 10
    //   449: invokevirtual 126	com/truecaller/api/services/messenger/v1/models/Peer$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   452: pop
    //   453: aload_1
    //   454: invokevirtual 127	com/truecaller/api/services/messenger/v1/models/Peer$a:buildPartial	()Lcom/google/f/q;
    //   457: astore_1
    //   458: aload_1
    //   459: checkcast 43	com/truecaller/api/services/messenger/v1/models/Peer
    //   462: astore_1
    //   463: aload_0
    //   464: aload_1
    //   465: putfield 41	com/truecaller/api/services/messenger/v1/events/Event$f:a	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   468: goto -316 -> 152
    //   471: iconst_1
    //   472: istore 7
    //   474: goto -322 -> 152
    //   477: astore_1
    //   478: goto +53 -> 531
    //   481: astore_1
    //   482: new 129	java/lang/RuntimeException
    //   485: astore_2
    //   486: new 131	com/google/f/x
    //   489: astore_3
    //   490: aload_1
    //   491: invokevirtual 136	java/io/IOException:getMessage	()Ljava/lang/String;
    //   494: astore_1
    //   495: aload_3
    //   496: aload_1
    //   497: invokespecial 139	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   500: aload_3
    //   501: aload_0
    //   502: invokevirtual 143	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   505: astore_1
    //   506: aload_2
    //   507: aload_1
    //   508: invokespecial 146	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   511: aload_2
    //   512: athrow
    //   513: astore_1
    //   514: new 129	java/lang/RuntimeException
    //   517: astore_2
    //   518: aload_1
    //   519: aload_0
    //   520: invokevirtual 143	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   523: astore_1
    //   524: aload_2
    //   525: aload_1
    //   526: invokespecial 146	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   529: aload_2
    //   530: athrow
    //   531: aload_1
    //   532: athrow
    //   533: getstatic 25	com/truecaller/api/services/messenger/v1/events/Event$f:f	Lcom/truecaller/api/services/messenger/v1/events/Event$f;
    //   536: areturn
    //   537: aload_2
    //   538: checkcast 148	com/google/f/q$k
    //   541: astore_2
    //   542: aload_3
    //   543: checkcast 2	com/truecaller/api/services/messenger/v1/events/Event$f
    //   546: astore_3
    //   547: aload_0
    //   548: getfield 41	com/truecaller/api/services/messenger/v1/events/Event$f:a	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   551: astore_1
    //   552: aload_3
    //   553: getfield 41	com/truecaller/api/services/messenger/v1/events/Event$f:a	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   556: astore 4
    //   558: aload_2
    //   559: aload_1
    //   560: aload 4
    //   562: invokeinterface 152 3 0
    //   567: checkcast 43	com/truecaller/api/services/messenger/v1/models/Peer
    //   570: astore_1
    //   571: aload_0
    //   572: aload_1
    //   573: putfield 41	com/truecaller/api/services/messenger/v1/events/Event$f:a	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   576: aload_0
    //   577: getfield 33	com/truecaller/api/services/messenger/v1/events/Event$f:b	Ljava/lang/String;
    //   580: invokevirtual 158	java/lang/String:isEmpty	()Z
    //   583: iload 8
    //   585: ixor
    //   586: istore 5
    //   588: aload_0
    //   589: getfield 33	com/truecaller/api/services/messenger/v1/events/Event$f:b	Ljava/lang/String;
    //   592: astore 4
    //   594: aload_3
    //   595: getfield 33	com/truecaller/api/services/messenger/v1/events/Event$f:b	Ljava/lang/String;
    //   598: invokevirtual 158	java/lang/String:isEmpty	()Z
    //   601: iload 8
    //   603: ixor
    //   604: istore 9
    //   606: aload_3
    //   607: getfield 33	com/truecaller/api/services/messenger/v1/events/Event$f:b	Ljava/lang/String;
    //   610: astore 11
    //   612: aload_2
    //   613: iload 5
    //   615: aload 4
    //   617: iload 9
    //   619: aload 11
    //   621: invokeinterface 162 5 0
    //   626: astore_1
    //   627: aload_0
    //   628: aload_1
    //   629: putfield 33	com/truecaller/api/services/messenger/v1/events/Event$f:b	Ljava/lang/String;
    //   632: aload_0
    //   633: getfield 35	com/truecaller/api/services/messenger/v1/events/Event$f:c	Ljava/lang/String;
    //   636: invokevirtual 158	java/lang/String:isEmpty	()Z
    //   639: iload 8
    //   641: ixor
    //   642: istore 5
    //   644: aload_0
    //   645: getfield 35	com/truecaller/api/services/messenger/v1/events/Event$f:c	Ljava/lang/String;
    //   648: astore 4
    //   650: aload_3
    //   651: getfield 35	com/truecaller/api/services/messenger/v1/events/Event$f:c	Ljava/lang/String;
    //   654: astore 10
    //   656: aload 10
    //   658: invokevirtual 158	java/lang/String:isEmpty	()Z
    //   661: iload 8
    //   663: ixor
    //   664: istore 9
    //   666: aload_3
    //   667: getfield 35	com/truecaller/api/services/messenger/v1/events/Event$f:c	Ljava/lang/String;
    //   670: astore 11
    //   672: aload_2
    //   673: iload 5
    //   675: aload 4
    //   677: iload 9
    //   679: aload 11
    //   681: invokeinterface 162 5 0
    //   686: astore_1
    //   687: aload_0
    //   688: aload_1
    //   689: putfield 35	com/truecaller/api/services/messenger/v1/events/Event$f:c	Ljava/lang/String;
    //   692: aload_0
    //   693: getfield 116	com/truecaller/api/services/messenger/v1/events/Event$f:d	I
    //   696: istore 5
    //   698: iload 5
    //   700: ifeq +9 -> 709
    //   703: iconst_1
    //   704: istore 5
    //   706: goto +8 -> 714
    //   709: iconst_0
    //   710: istore 5
    //   712: aconst_null
    //   713: astore_1
    //   714: aload_0
    //   715: getfield 116	com/truecaller/api/services/messenger/v1/events/Event$f:d	I
    //   718: istore 6
    //   720: aload_3
    //   721: getfield 116	com/truecaller/api/services/messenger/v1/events/Event$f:d	I
    //   724: istore 9
    //   726: iload 9
    //   728: ifeq +6 -> 734
    //   731: iconst_1
    //   732: istore 7
    //   734: aload_3
    //   735: getfield 116	com/truecaller/api/services/messenger/v1/events/Event$f:d	I
    //   738: istore 8
    //   740: aload_2
    //   741: iload 5
    //   743: iload 6
    //   745: iload 7
    //   747: iload 8
    //   749: invokeinterface 166 5 0
    //   754: istore 5
    //   756: aload_0
    //   757: iload 5
    //   759: putfield 116	com/truecaller/api/services/messenger/v1/events/Event$f:d	I
    //   762: aload_0
    //   763: getfield 48	com/truecaller/api/services/messenger/v1/events/Event$f:e	Lcom/truecaller/api/services/messenger/v1/models/c;
    //   766: astore_1
    //   767: aload_3
    //   768: getfield 48	com/truecaller/api/services/messenger/v1/events/Event$f:e	Lcom/truecaller/api/services/messenger/v1/models/c;
    //   771: astore_3
    //   772: aload_2
    //   773: aload_1
    //   774: aload_3
    //   775: invokeinterface 152 3 0
    //   780: checkcast 50	com/truecaller/api/services/messenger/v1/models/c
    //   783: astore_1
    //   784: aload_0
    //   785: aload_1
    //   786: putfield 48	com/truecaller/api/services/messenger/v1/events/Event$f:e	Lcom/truecaller/api/services/messenger/v1/models/c;
    //   789: aload_0
    //   790: areturn
    //   791: new 168	com/truecaller/api/services/messenger/v1/events/Event$f$a
    //   794: astore_1
    //   795: aload_1
    //   796: iconst_0
    //   797: invokespecial 171	com/truecaller/api/services/messenger/v1/events/Event$f$a:<init>	(B)V
    //   800: aload_1
    //   801: areturn
    //   802: aconst_null
    //   803: areturn
    //   804: getstatic 25	com/truecaller/api/services/messenger/v1/events/Event$f:f	Lcom/truecaller/api/services/messenger/v1/events/Event$f;
    //   807: areturn
    //   808: new 2	com/truecaller/api/services/messenger/v1/events/Event$f
    //   811: astore_1
    //   812: aload_1
    //   813: invokespecial 23	com/truecaller/api/services/messenger/v1/events/Event$f:<init>	()V
    //   816: aload_1
    //   817: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	818	0	this	f
    //   0	818	1	paramj	com.google.f.q.j
    //   0	818	2	paramObject1	Object
    //   0	818	3	paramObject2	Object
    //   3	673	4	localObject1	Object
    //   9	216	5	i	int
    //   229	42	5	bool1	boolean
    //   340	66	5	j	int
    //   586	88	5	bool2	boolean
    //   696	46	5	k	int
    //   754	4	5	m	int
    //   19	725	6	n	int
    //   25	721	7	bool3	boolean
    //   28	720	8	i1	int
    //   170	51	9	i2	int
    //   604	74	9	bool4	boolean
    //   724	3	9	i3	int
    //   277	380	10	localObject2	Object
    //   610	70	11	str	String
    // Exception table:
    //   from	to	target	type
    //   103	106	133	finally
    //   111	114	133	finally
    //   115	118	133	finally
    //   120	124	133	finally
    //   124	128	133	finally
    //   128	130	133	finally
    //   134	136	133	finally
    //   157	161	477	finally
    //   224	229	477	finally
    //   242	246	477	finally
    //   251	255	477	finally
    //   256	260	477	finally
    //   261	265	477	finally
    //   274	277	477	finally
    //   282	286	477	finally
    //   288	293	477	finally
    //   296	301	477	finally
    //   305	309	477	finally
    //   312	318	477	finally
    //   318	322	477	finally
    //   323	327	477	finally
    //   329	333	477	finally
    //   336	340	477	finally
    //   343	348	477	finally
    //   351	355	477	finally
    //   357	361	477	finally
    //   364	368	477	finally
    //   370	374	477	finally
    //   377	381	477	finally
    //   386	390	477	finally
    //   391	395	477	finally
    //   396	400	477	finally
    //   409	412	477	finally
    //   417	421	477	finally
    //   423	428	477	finally
    //   431	436	477	finally
    //   440	444	477	finally
    //   447	453	477	finally
    //   453	457	477	finally
    //   458	462	477	finally
    //   464	468	477	finally
    //   482	485	477	finally
    //   486	489	477	finally
    //   490	494	477	finally
    //   496	500	477	finally
    //   501	505	477	finally
    //   507	511	477	finally
    //   511	513	477	finally
    //   514	517	477	finally
    //   519	523	477	finally
    //   525	529	477	finally
    //   529	531	477	finally
    //   157	161	481	java/io/IOException
    //   224	229	481	java/io/IOException
    //   242	246	481	java/io/IOException
    //   251	255	481	java/io/IOException
    //   256	260	481	java/io/IOException
    //   261	265	481	java/io/IOException
    //   274	277	481	java/io/IOException
    //   282	286	481	java/io/IOException
    //   288	293	481	java/io/IOException
    //   296	301	481	java/io/IOException
    //   305	309	481	java/io/IOException
    //   312	318	481	java/io/IOException
    //   318	322	481	java/io/IOException
    //   323	327	481	java/io/IOException
    //   329	333	481	java/io/IOException
    //   336	340	481	java/io/IOException
    //   343	348	481	java/io/IOException
    //   351	355	481	java/io/IOException
    //   357	361	481	java/io/IOException
    //   364	368	481	java/io/IOException
    //   370	374	481	java/io/IOException
    //   377	381	481	java/io/IOException
    //   386	390	481	java/io/IOException
    //   391	395	481	java/io/IOException
    //   396	400	481	java/io/IOException
    //   409	412	481	java/io/IOException
    //   417	421	481	java/io/IOException
    //   423	428	481	java/io/IOException
    //   431	436	481	java/io/IOException
    //   440	444	481	java/io/IOException
    //   447	453	481	java/io/IOException
    //   453	457	481	java/io/IOException
    //   458	462	481	java/io/IOException
    //   464	468	481	java/io/IOException
    //   157	161	513	com/google/f/x
    //   224	229	513	com/google/f/x
    //   242	246	513	com/google/f/x
    //   251	255	513	com/google/f/x
    //   256	260	513	com/google/f/x
    //   261	265	513	com/google/f/x
    //   274	277	513	com/google/f/x
    //   282	286	513	com/google/f/x
    //   288	293	513	com/google/f/x
    //   296	301	513	com/google/f/x
    //   305	309	513	com/google/f/x
    //   312	318	513	com/google/f/x
    //   318	322	513	com/google/f/x
    //   323	327	513	com/google/f/x
    //   329	333	513	com/google/f/x
    //   336	340	513	com/google/f/x
    //   343	348	513	com/google/f/x
    //   351	355	513	com/google/f/x
    //   357	361	513	com/google/f/x
    //   364	368	513	com/google/f/x
    //   370	374	513	com/google/f/x
    //   377	381	513	com/google/f/x
    //   386	390	513	com/google/f/x
    //   391	395	513	com/google/f/x
    //   396	400	513	com/google/f/x
    //   409	412	513	com/google/f/x
    //   417	421	513	com/google/f/x
    //   423	428	513	com/google/f/x
    //   431	436	513	com/google/f/x
    //   440	444	513	com/google/f/x
    //   447	453	513	com/google/f/x
    //   453	457	513	com/google/f/x
    //   458	462	513	com/google/f/x
    //   464	468	513	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int m = -1;
    if (i != m) {
      return i;
    }
    Object localObject1 = a;
    m = 0;
    Object localObject2;
    if (localObject1 != null)
    {
      localObject2 = a();
      i = h.computeMessageSize(1, (ae)localObject2);
      m = 0 + i;
    }
    localObject1 = b;
    boolean bool1 = ((String)localObject1).isEmpty();
    if (!bool1)
    {
      localObject2 = b;
      int j = h.computeStringSize(2, (String)localObject2);
      m += j;
    }
    localObject1 = c;
    boolean bool2 = ((String)localObject1).isEmpty();
    if (!bool2)
    {
      localObject2 = c;
      k = h.computeStringSize(3, (String)localObject2);
      m += k;
    }
    int k = d;
    if (k != 0)
    {
      int n = 4;
      k = h.computeInt32Size(n, k);
      m += k;
    }
    localObject1 = e;
    if (localObject1 != null)
    {
      localObject2 = d();
      k = h.computeMessageSize(5, (ae)localObject2);
      m += k;
    }
    memoizedSerializedSize = m;
    return m;
  }
  
  public final void writeTo(h paramh)
  {
    Object localObject1 = a;
    Object localObject2;
    if (localObject1 != null)
    {
      int i = 1;
      localObject2 = a();
      paramh.writeMessage(i, (ae)localObject2);
    }
    localObject1 = b;
    boolean bool1 = ((String)localObject1).isEmpty();
    if (!bool1)
    {
      int j = 2;
      localObject2 = b;
      paramh.writeString(j, (String)localObject2);
    }
    localObject1 = c;
    boolean bool2 = ((String)localObject1).isEmpty();
    if (!bool2)
    {
      k = 3;
      localObject2 = c;
      paramh.writeString(k, (String)localObject2);
    }
    int k = d;
    if (k != 0)
    {
      int m = 4;
      paramh.writeInt32(m, k);
    }
    localObject1 = e;
    if (localObject1 != null)
    {
      k = 5;
      localObject2 = d();
      paramh.writeMessage(k, (ae)localObject2);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.events.Event.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */