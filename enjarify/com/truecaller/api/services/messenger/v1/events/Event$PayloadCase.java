package com.truecaller.api.services.messenger.v1.events;

import com.google.f.w.c;

public enum Event$PayloadCase
  implements w.c
{
  private final int value;
  
  static
  {
    Object localObject = new com/truecaller/api/services/messenger/v1/events/Event$PayloadCase;
    int i = 2;
    ((PayloadCase)localObject).<init>("MESSAGE_SENT", 0, i);
    MESSAGE_SENT = (PayloadCase)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/events/Event$PayloadCase;
    int j = 1;
    int k = 3;
    ((PayloadCase)localObject).<init>("REPORT_SENT", j, k);
    REPORT_SENT = (PayloadCase)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/events/Event$PayloadCase;
    int m = 4;
    ((PayloadCase)localObject).<init>("REACTION_SENT", i, m);
    REACTION_SENT = (PayloadCase)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/events/Event$PayloadCase;
    int n = 5;
    ((PayloadCase)localObject).<init>("GROUP_CREATED", k, n);
    GROUP_CREATED = (PayloadCase)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/events/Event$PayloadCase;
    int i1 = 6;
    ((PayloadCase)localObject).<init>("PARTICIPANT_ADDED", m, i1);
    PARTICIPANT_ADDED = (PayloadCase)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/events/Event$PayloadCase;
    int i2 = 7;
    ((PayloadCase)localObject).<init>("PARTICIPANT_REMOVED", n, i2);
    PARTICIPANT_REMOVED = (PayloadCase)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/events/Event$PayloadCase;
    int i3 = 8;
    ((PayloadCase)localObject).<init>("ROLES_UPDATED", i1, i3);
    ROLES_UPDATED = (PayloadCase)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/events/Event$PayloadCase;
    int i4 = 9;
    ((PayloadCase)localObject).<init>("GROUP_INFO_UPDATED", i2, i4);
    GROUP_INFO_UPDATED = (PayloadCase)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/events/Event$PayloadCase;
    ((PayloadCase)localObject).<init>("PING", i3, 997);
    PING = (PayloadCase)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/events/Event$PayloadCase;
    ((PayloadCase)localObject).<init>("INCOMPATIBLE_EVENT", i4, 998);
    INCOMPATIBLE_EVENT = (PayloadCase)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/events/Event$PayloadCase;
    int i5 = 10;
    ((PayloadCase)localObject).<init>("PAYLOAD_NOT_SET", i5, 0);
    PAYLOAD_NOT_SET = (PayloadCase)localObject;
    localObject = new PayloadCase[11];
    PayloadCase localPayloadCase = MESSAGE_SENT;
    localObject[0] = localPayloadCase;
    localPayloadCase = REPORT_SENT;
    localObject[j] = localPayloadCase;
    localPayloadCase = REACTION_SENT;
    localObject[i] = localPayloadCase;
    localPayloadCase = GROUP_CREATED;
    localObject[k] = localPayloadCase;
    localPayloadCase = PARTICIPANT_ADDED;
    localObject[m] = localPayloadCase;
    localPayloadCase = PARTICIPANT_REMOVED;
    localObject[n] = localPayloadCase;
    localPayloadCase = ROLES_UPDATED;
    localObject[i1] = localPayloadCase;
    localPayloadCase = GROUP_INFO_UPDATED;
    localObject[i2] = localPayloadCase;
    localPayloadCase = PING;
    localObject[i3] = localPayloadCase;
    localPayloadCase = INCOMPATIBLE_EVENT;
    localObject[i4] = localPayloadCase;
    localPayloadCase = PAYLOAD_NOT_SET;
    localObject[i5] = localPayloadCase;
    $VALUES = (PayloadCase[])localObject;
  }
  
  private Event$PayloadCase(int paramInt1)
  {
    value = paramInt1;
  }
  
  public static PayloadCase forNumber(int paramInt)
  {
    if (paramInt != 0)
    {
      switch (paramInt)
      {
      default: 
        switch (paramInt)
        {
        default: 
          return null;
        case 998: 
          return INCOMPATIBLE_EVENT;
        }
        return PING;
      case 9: 
        return GROUP_INFO_UPDATED;
      case 8: 
        return ROLES_UPDATED;
      case 7: 
        return PARTICIPANT_REMOVED;
      case 6: 
        return PARTICIPANT_ADDED;
      case 5: 
        return GROUP_CREATED;
      case 4: 
        return REACTION_SENT;
      case 3: 
        return REPORT_SENT;
      }
      return MESSAGE_SENT;
    }
    return PAYLOAD_NOT_SET;
  }
  
  public static PayloadCase valueOf(int paramInt)
  {
    return forNumber(paramInt);
  }
  
  public final int getNumber()
  {
    return value;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.events.Event.PayloadCase
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */