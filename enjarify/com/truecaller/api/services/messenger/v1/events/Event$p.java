package com.truecaller.api.services.messenger.v1.events;

import com.google.f.ae;
import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;
import com.google.f.w.h;
import com.truecaller.api.services.messenger.v1.models.Peer;
import java.util.List;

public final class Event$p
  extends q
  implements Event.q
{
  private static final p g;
  private static volatile ah h;
  private int a;
  private Peer b;
  private Peer c;
  private String d = "";
  private int e;
  private w.h f;
  
  static
  {
    p localp = new com/truecaller/api/services/messenger/v1/events/Event$p;
    localp.<init>();
    g = localp;
    localp.makeImmutable();
  }
  
  private Event$p()
  {
    w.h localh = emptyProtobufList();
    f = localh;
  }
  
  public static p e()
  {
    return g;
  }
  
  public static ah f()
  {
    return g.getParserForType();
  }
  
  public final Peer a()
  {
    Peer localPeer = b;
    if (localPeer == null) {
      localPeer = Peer.d();
    }
    return localPeer;
  }
  
  public final Peer b()
  {
    Peer localPeer = c;
    if (localPeer == null) {
      localPeer = Peer.d();
    }
    return localPeer;
  }
  
  public final String c()
  {
    return d;
  }
  
  public final List d()
  {
    return f;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 58	com/truecaller/api/services/messenger/v1/events/Event$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 64	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_0
    //   19: istore 6
    //   21: aconst_null
    //   22: astore 4
    //   24: iconst_0
    //   25: istore 7
    //   27: iconst_1
    //   28: istore 8
    //   30: iload 5
    //   32: tableswitch	default:+48->80, 1:+842->874, 2:+838->870, 3:+827->859, 4:+816->848, 5:+556->588, 6:+110->142, 7:+552->584, 8:+58->90
    //   80: new 67	java/lang/UnsupportedOperationException
    //   83: astore_1
    //   84: aload_1
    //   85: invokespecial 68	java/lang/UnsupportedOperationException:<init>	()V
    //   88: aload_1
    //   89: athrow
    //   90: getstatic 70	com/truecaller/api/services/messenger/v1/events/Event$p:h	Lcom/google/f/ah;
    //   93: astore_1
    //   94: aload_1
    //   95: ifnonnull +43 -> 138
    //   98: ldc 2
    //   100: astore_1
    //   101: aload_1
    //   102: monitorenter
    //   103: getstatic 70	com/truecaller/api/services/messenger/v1/events/Event$p:h	Lcom/google/f/ah;
    //   106: astore_2
    //   107: aload_2
    //   108: ifnonnull +20 -> 128
    //   111: new 72	com/google/f/q$b
    //   114: astore_2
    //   115: getstatic 26	com/truecaller/api/services/messenger/v1/events/Event$p:g	Lcom/truecaller/api/services/messenger/v1/events/Event$p;
    //   118: astore_3
    //   119: aload_2
    //   120: aload_3
    //   121: invokespecial 75	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   124: aload_2
    //   125: putstatic 70	com/truecaller/api/services/messenger/v1/events/Event$p:h	Lcom/google/f/ah;
    //   128: aload_1
    //   129: monitorexit
    //   130: goto +8 -> 138
    //   133: astore_2
    //   134: aload_1
    //   135: monitorexit
    //   136: aload_2
    //   137: athrow
    //   138: getstatic 70	com/truecaller/api/services/messenger/v1/events/Event$p:h	Lcom/google/f/ah;
    //   141: areturn
    //   142: aload_2
    //   143: checkcast 77	com/google/f/g
    //   146: astore_2
    //   147: aload_3
    //   148: checkcast 79	com/google/f/n
    //   151: astore_3
    //   152: iload 7
    //   154: ifne +430 -> 584
    //   157: aload_2
    //   158: invokevirtual 82	com/google/f/g:readTag	()I
    //   161: istore 5
    //   163: iload 5
    //   165: ifeq +357 -> 522
    //   168: bipush 10
    //   170: istore 9
    //   172: iload 5
    //   174: iload 9
    //   176: if_icmpeq +252 -> 428
    //   179: bipush 18
    //   181: istore 9
    //   183: iload 5
    //   185: iload 9
    //   187: if_icmpeq +147 -> 334
    //   190: bipush 26
    //   192: istore 9
    //   194: iload 5
    //   196: iload 9
    //   198: if_icmpeq +123 -> 321
    //   201: bipush 32
    //   203: istore 9
    //   205: iload 5
    //   207: iload 9
    //   209: if_icmpeq +97 -> 306
    //   212: bipush 42
    //   214: istore 9
    //   216: iload 5
    //   218: iload 9
    //   220: if_icmpeq +22 -> 242
    //   223: aload_2
    //   224: iload 5
    //   226: invokevirtual 91	com/google/f/g:skipField	(I)Z
    //   229: istore 5
    //   231: iload 5
    //   233: ifne -81 -> 152
    //   236: iconst_1
    //   237: istore 7
    //   239: goto -87 -> 152
    //   242: aload_0
    //   243: getfield 40	com/truecaller/api/services/messenger/v1/events/Event$p:f	Lcom/google/f/w$h;
    //   246: astore_1
    //   247: aload_1
    //   248: invokeinterface 97 1 0
    //   253: istore 5
    //   255: iload 5
    //   257: ifne +18 -> 275
    //   260: aload_0
    //   261: getfield 40	com/truecaller/api/services/messenger/v1/events/Event$p:f	Lcom/google/f/w$h;
    //   264: astore_1
    //   265: aload_1
    //   266: invokestatic 101	com/google/f/q:mutableCopy	(Lcom/google/f/w$h;)Lcom/google/f/w$h;
    //   269: astore_1
    //   270: aload_0
    //   271: aload_1
    //   272: putfield 40	com/truecaller/api/services/messenger/v1/events/Event$p:f	Lcom/google/f/w$h;
    //   275: aload_0
    //   276: getfield 40	com/truecaller/api/services/messenger/v1/events/Event$p:f	Lcom/google/f/w$h;
    //   279: astore_1
    //   280: invokestatic 103	com/truecaller/api/services/messenger/v1/models/Peer:e	()Lcom/google/f/ah;
    //   283: astore 10
    //   285: aload_2
    //   286: aload 10
    //   288: aload_3
    //   289: invokevirtual 107	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   292: astore 10
    //   294: aload_1
    //   295: aload 10
    //   297: invokeinterface 111 2 0
    //   302: pop
    //   303: goto -151 -> 152
    //   306: aload_2
    //   307: invokevirtual 114	com/google/f/g:readInt32	()I
    //   310: istore 5
    //   312: aload_0
    //   313: iload 5
    //   315: putfield 116	com/truecaller/api/services/messenger/v1/events/Event$p:e	I
    //   318: goto -166 -> 152
    //   321: aload_2
    //   322: invokevirtual 120	com/google/f/g:readStringRequireUtf8	()Ljava/lang/String;
    //   325: astore_1
    //   326: aload_0
    //   327: aload_1
    //   328: putfield 34	com/truecaller/api/services/messenger/v1/events/Event$p:d	Ljava/lang/String;
    //   331: goto -179 -> 152
    //   334: aload_0
    //   335: getfield 53	com/truecaller/api/services/messenger/v1/events/Event$p:c	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   338: astore_1
    //   339: aload_1
    //   340: ifnull +21 -> 361
    //   343: aload_0
    //   344: getfield 53	com/truecaller/api/services/messenger/v1/events/Event$p:c	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   347: astore_1
    //   348: aload_1
    //   349: invokevirtual 124	com/truecaller/api/services/messenger/v1/models/Peer:toBuilder	()Lcom/google/f/q$a;
    //   352: astore_1
    //   353: aload_1
    //   354: checkcast 126	com/truecaller/api/services/messenger/v1/models/Peer$a
    //   357: astore_1
    //   358: goto +8 -> 366
    //   361: iconst_0
    //   362: istore 5
    //   364: aconst_null
    //   365: astore_1
    //   366: invokestatic 103	com/truecaller/api/services/messenger/v1/models/Peer:e	()Lcom/google/f/ah;
    //   369: astore 10
    //   371: aload_2
    //   372: aload 10
    //   374: aload_3
    //   375: invokevirtual 107	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   378: astore 10
    //   380: aload 10
    //   382: checkcast 48	com/truecaller/api/services/messenger/v1/models/Peer
    //   385: astore 10
    //   387: aload_0
    //   388: aload 10
    //   390: putfield 53	com/truecaller/api/services/messenger/v1/events/Event$p:c	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   393: aload_1
    //   394: ifnull -242 -> 152
    //   397: aload_0
    //   398: getfield 53	com/truecaller/api/services/messenger/v1/events/Event$p:c	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   401: astore 10
    //   403: aload_1
    //   404: aload 10
    //   406: invokevirtual 130	com/truecaller/api/services/messenger/v1/models/Peer$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   409: pop
    //   410: aload_1
    //   411: invokevirtual 134	com/truecaller/api/services/messenger/v1/models/Peer$a:buildPartial	()Lcom/google/f/q;
    //   414: astore_1
    //   415: aload_1
    //   416: checkcast 48	com/truecaller/api/services/messenger/v1/models/Peer
    //   419: astore_1
    //   420: aload_0
    //   421: aload_1
    //   422: putfield 53	com/truecaller/api/services/messenger/v1/events/Event$p:c	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   425: goto -273 -> 152
    //   428: aload_0
    //   429: getfield 46	com/truecaller/api/services/messenger/v1/events/Event$p:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   432: astore_1
    //   433: aload_1
    //   434: ifnull +21 -> 455
    //   437: aload_0
    //   438: getfield 46	com/truecaller/api/services/messenger/v1/events/Event$p:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   441: astore_1
    //   442: aload_1
    //   443: invokevirtual 124	com/truecaller/api/services/messenger/v1/models/Peer:toBuilder	()Lcom/google/f/q$a;
    //   446: astore_1
    //   447: aload_1
    //   448: checkcast 126	com/truecaller/api/services/messenger/v1/models/Peer$a
    //   451: astore_1
    //   452: goto +8 -> 460
    //   455: iconst_0
    //   456: istore 5
    //   458: aconst_null
    //   459: astore_1
    //   460: invokestatic 103	com/truecaller/api/services/messenger/v1/models/Peer:e	()Lcom/google/f/ah;
    //   463: astore 10
    //   465: aload_2
    //   466: aload 10
    //   468: aload_3
    //   469: invokevirtual 107	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   472: astore 10
    //   474: aload 10
    //   476: checkcast 48	com/truecaller/api/services/messenger/v1/models/Peer
    //   479: astore 10
    //   481: aload_0
    //   482: aload 10
    //   484: putfield 46	com/truecaller/api/services/messenger/v1/events/Event$p:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   487: aload_1
    //   488: ifnull -336 -> 152
    //   491: aload_0
    //   492: getfield 46	com/truecaller/api/services/messenger/v1/events/Event$p:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   495: astore 10
    //   497: aload_1
    //   498: aload 10
    //   500: invokevirtual 130	com/truecaller/api/services/messenger/v1/models/Peer$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   503: pop
    //   504: aload_1
    //   505: invokevirtual 134	com/truecaller/api/services/messenger/v1/models/Peer$a:buildPartial	()Lcom/google/f/q;
    //   508: astore_1
    //   509: aload_1
    //   510: checkcast 48	com/truecaller/api/services/messenger/v1/models/Peer
    //   513: astore_1
    //   514: aload_0
    //   515: aload_1
    //   516: putfield 46	com/truecaller/api/services/messenger/v1/events/Event$p:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   519: goto -367 -> 152
    //   522: iconst_1
    //   523: istore 7
    //   525: goto -373 -> 152
    //   528: astore_1
    //   529: goto +53 -> 582
    //   532: astore_1
    //   533: new 136	java/lang/RuntimeException
    //   536: astore_2
    //   537: new 138	com/google/f/x
    //   540: astore_3
    //   541: aload_1
    //   542: invokevirtual 143	java/io/IOException:getMessage	()Ljava/lang/String;
    //   545: astore_1
    //   546: aload_3
    //   547: aload_1
    //   548: invokespecial 146	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   551: aload_3
    //   552: aload_0
    //   553: invokevirtual 150	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   556: astore_1
    //   557: aload_2
    //   558: aload_1
    //   559: invokespecial 153	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   562: aload_2
    //   563: athrow
    //   564: astore_1
    //   565: new 136	java/lang/RuntimeException
    //   568: astore_2
    //   569: aload_1
    //   570: aload_0
    //   571: invokevirtual 150	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   574: astore_1
    //   575: aload_2
    //   576: aload_1
    //   577: invokespecial 153	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   580: aload_2
    //   581: athrow
    //   582: aload_1
    //   583: athrow
    //   584: getstatic 26	com/truecaller/api/services/messenger/v1/events/Event$p:g	Lcom/truecaller/api/services/messenger/v1/events/Event$p;
    //   587: areturn
    //   588: aload_2
    //   589: checkcast 155	com/google/f/q$k
    //   592: astore_2
    //   593: aload_3
    //   594: checkcast 2	com/truecaller/api/services/messenger/v1/events/Event$p
    //   597: astore_3
    //   598: aload_0
    //   599: getfield 46	com/truecaller/api/services/messenger/v1/events/Event$p:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   602: astore_1
    //   603: aload_3
    //   604: getfield 46	com/truecaller/api/services/messenger/v1/events/Event$p:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   607: astore 4
    //   609: aload_2
    //   610: aload_1
    //   611: aload 4
    //   613: invokeinterface 159 3 0
    //   618: checkcast 48	com/truecaller/api/services/messenger/v1/models/Peer
    //   621: astore_1
    //   622: aload_0
    //   623: aload_1
    //   624: putfield 46	com/truecaller/api/services/messenger/v1/events/Event$p:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   627: aload_0
    //   628: getfield 53	com/truecaller/api/services/messenger/v1/events/Event$p:c	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   631: astore_1
    //   632: aload_3
    //   633: getfield 53	com/truecaller/api/services/messenger/v1/events/Event$p:c	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   636: astore 4
    //   638: aload_2
    //   639: aload_1
    //   640: aload 4
    //   642: invokeinterface 159 3 0
    //   647: checkcast 48	com/truecaller/api/services/messenger/v1/models/Peer
    //   650: astore_1
    //   651: aload_0
    //   652: aload_1
    //   653: putfield 53	com/truecaller/api/services/messenger/v1/events/Event$p:c	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   656: aload_0
    //   657: getfield 34	com/truecaller/api/services/messenger/v1/events/Event$p:d	Ljava/lang/String;
    //   660: invokevirtual 164	java/lang/String:isEmpty	()Z
    //   663: iload 8
    //   665: ixor
    //   666: istore 5
    //   668: aload_0
    //   669: getfield 34	com/truecaller/api/services/messenger/v1/events/Event$p:d	Ljava/lang/String;
    //   672: astore 4
    //   674: aload_3
    //   675: getfield 34	com/truecaller/api/services/messenger/v1/events/Event$p:d	Ljava/lang/String;
    //   678: astore 10
    //   680: aload 10
    //   682: invokevirtual 164	java/lang/String:isEmpty	()Z
    //   685: iload 8
    //   687: ixor
    //   688: istore 9
    //   690: aload_3
    //   691: getfield 34	com/truecaller/api/services/messenger/v1/events/Event$p:d	Ljava/lang/String;
    //   694: astore 11
    //   696: aload_2
    //   697: iload 5
    //   699: aload 4
    //   701: iload 9
    //   703: aload 11
    //   705: invokeinterface 168 5 0
    //   710: astore_1
    //   711: aload_0
    //   712: aload_1
    //   713: putfield 34	com/truecaller/api/services/messenger/v1/events/Event$p:d	Ljava/lang/String;
    //   716: aload_0
    //   717: getfield 116	com/truecaller/api/services/messenger/v1/events/Event$p:e	I
    //   720: istore 5
    //   722: iload 5
    //   724: ifeq +9 -> 733
    //   727: iconst_1
    //   728: istore 5
    //   730: goto +8 -> 738
    //   733: iconst_0
    //   734: istore 5
    //   736: aconst_null
    //   737: astore_1
    //   738: aload_0
    //   739: getfield 116	com/truecaller/api/services/messenger/v1/events/Event$p:e	I
    //   742: istore 6
    //   744: aload_3
    //   745: getfield 116	com/truecaller/api/services/messenger/v1/events/Event$p:e	I
    //   748: istore 9
    //   750: iload 9
    //   752: ifeq +6 -> 758
    //   755: iconst_1
    //   756: istore 7
    //   758: aload_3
    //   759: getfield 116	com/truecaller/api/services/messenger/v1/events/Event$p:e	I
    //   762: istore 8
    //   764: aload_2
    //   765: iload 5
    //   767: iload 6
    //   769: iload 7
    //   771: iload 8
    //   773: invokeinterface 172 5 0
    //   778: istore 5
    //   780: aload_0
    //   781: iload 5
    //   783: putfield 116	com/truecaller/api/services/messenger/v1/events/Event$p:e	I
    //   786: aload_0
    //   787: getfield 40	com/truecaller/api/services/messenger/v1/events/Event$p:f	Lcom/google/f/w$h;
    //   790: astore_1
    //   791: aload_3
    //   792: getfield 40	com/truecaller/api/services/messenger/v1/events/Event$p:f	Lcom/google/f/w$h;
    //   795: astore 4
    //   797: aload_2
    //   798: aload_1
    //   799: aload 4
    //   801: invokeinterface 176 3 0
    //   806: astore_1
    //   807: aload_0
    //   808: aload_1
    //   809: putfield 40	com/truecaller/api/services/messenger/v1/events/Event$p:f	Lcom/google/f/w$h;
    //   812: getstatic 182	com/google/f/q$i:INSTANCE	Lcom/google/f/q$i;
    //   815: astore_1
    //   816: aload_2
    //   817: aload_1
    //   818: if_acmpne +28 -> 846
    //   821: aload_0
    //   822: getfield 184	com/truecaller/api/services/messenger/v1/events/Event$p:a	I
    //   825: istore 5
    //   827: aload_3
    //   828: getfield 184	com/truecaller/api/services/messenger/v1/events/Event$p:a	I
    //   831: istore 12
    //   833: iload 5
    //   835: iload 12
    //   837: ior
    //   838: istore 5
    //   840: aload_0
    //   841: iload 5
    //   843: putfield 184	com/truecaller/api/services/messenger/v1/events/Event$p:a	I
    //   846: aload_0
    //   847: areturn
    //   848: new 186	com/truecaller/api/services/messenger/v1/events/Event$p$a
    //   851: astore_1
    //   852: aload_1
    //   853: iconst_0
    //   854: invokespecial 189	com/truecaller/api/services/messenger/v1/events/Event$p$a:<init>	(B)V
    //   857: aload_1
    //   858: areturn
    //   859: aload_0
    //   860: getfield 40	com/truecaller/api/services/messenger/v1/events/Event$p:f	Lcom/google/f/w$h;
    //   863: invokeinterface 190 1 0
    //   868: aconst_null
    //   869: areturn
    //   870: getstatic 26	com/truecaller/api/services/messenger/v1/events/Event$p:g	Lcom/truecaller/api/services/messenger/v1/events/Event$p;
    //   873: areturn
    //   874: new 2	com/truecaller/api/services/messenger/v1/events/Event$p
    //   877: astore_1
    //   878: aload_1
    //   879: invokespecial 24	com/truecaller/api/services/messenger/v1/events/Event$p:<init>	()V
    //   882: aload_1
    //   883: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	884	0	this	p
    //   0	884	1	paramj	com.google.f.q.j
    //   0	884	2	paramObject1	Object
    //   0	884	3	paramObject2	Object
    //   3	797	4	localObject1	Object
    //   9	216	5	i	int
    //   229	27	5	bool1	boolean
    //   310	147	5	j	int
    //   666	32	5	bool2	boolean
    //   720	46	5	k	int
    //   778	64	5	m	int
    //   19	749	6	n	int
    //   25	745	7	bool3	boolean
    //   28	744	8	i1	int
    //   170	51	9	i2	int
    //   688	14	9	bool4	boolean
    //   748	3	9	i3	int
    //   283	398	10	localObject2	Object
    //   694	10	11	str	String
    //   831	7	12	i4	int
    // Exception table:
    //   from	to	target	type
    //   103	106	133	finally
    //   111	114	133	finally
    //   115	118	133	finally
    //   120	124	133	finally
    //   124	128	133	finally
    //   128	130	133	finally
    //   134	136	133	finally
    //   157	161	528	finally
    //   224	229	528	finally
    //   242	246	528	finally
    //   247	253	528	finally
    //   260	264	528	finally
    //   265	269	528	finally
    //   271	275	528	finally
    //   275	279	528	finally
    //   280	283	528	finally
    //   288	292	528	finally
    //   295	303	528	finally
    //   306	310	528	finally
    //   313	318	528	finally
    //   321	325	528	finally
    //   327	331	528	finally
    //   334	338	528	finally
    //   343	347	528	finally
    //   348	352	528	finally
    //   353	357	528	finally
    //   366	369	528	finally
    //   374	378	528	finally
    //   380	385	528	finally
    //   388	393	528	finally
    //   397	401	528	finally
    //   404	410	528	finally
    //   410	414	528	finally
    //   415	419	528	finally
    //   421	425	528	finally
    //   428	432	528	finally
    //   437	441	528	finally
    //   442	446	528	finally
    //   447	451	528	finally
    //   460	463	528	finally
    //   468	472	528	finally
    //   474	479	528	finally
    //   482	487	528	finally
    //   491	495	528	finally
    //   498	504	528	finally
    //   504	508	528	finally
    //   509	513	528	finally
    //   515	519	528	finally
    //   533	536	528	finally
    //   537	540	528	finally
    //   541	545	528	finally
    //   547	551	528	finally
    //   552	556	528	finally
    //   558	562	528	finally
    //   562	564	528	finally
    //   565	568	528	finally
    //   570	574	528	finally
    //   576	580	528	finally
    //   580	582	528	finally
    //   157	161	532	java/io/IOException
    //   224	229	532	java/io/IOException
    //   242	246	532	java/io/IOException
    //   247	253	532	java/io/IOException
    //   260	264	532	java/io/IOException
    //   265	269	532	java/io/IOException
    //   271	275	532	java/io/IOException
    //   275	279	532	java/io/IOException
    //   280	283	532	java/io/IOException
    //   288	292	532	java/io/IOException
    //   295	303	532	java/io/IOException
    //   306	310	532	java/io/IOException
    //   313	318	532	java/io/IOException
    //   321	325	532	java/io/IOException
    //   327	331	532	java/io/IOException
    //   334	338	532	java/io/IOException
    //   343	347	532	java/io/IOException
    //   348	352	532	java/io/IOException
    //   353	357	532	java/io/IOException
    //   366	369	532	java/io/IOException
    //   374	378	532	java/io/IOException
    //   380	385	532	java/io/IOException
    //   388	393	532	java/io/IOException
    //   397	401	532	java/io/IOException
    //   404	410	532	java/io/IOException
    //   410	414	532	java/io/IOException
    //   415	419	532	java/io/IOException
    //   421	425	532	java/io/IOException
    //   428	432	532	java/io/IOException
    //   437	441	532	java/io/IOException
    //   442	446	532	java/io/IOException
    //   447	451	532	java/io/IOException
    //   460	463	532	java/io/IOException
    //   468	472	532	java/io/IOException
    //   474	479	532	java/io/IOException
    //   482	487	532	java/io/IOException
    //   491	495	532	java/io/IOException
    //   498	504	532	java/io/IOException
    //   504	508	532	java/io/IOException
    //   509	513	532	java/io/IOException
    //   515	519	532	java/io/IOException
    //   157	161	564	com/google/f/x
    //   224	229	564	com/google/f/x
    //   242	246	564	com/google/f/x
    //   247	253	564	com/google/f/x
    //   260	264	564	com/google/f/x
    //   265	269	564	com/google/f/x
    //   271	275	564	com/google/f/x
    //   275	279	564	com/google/f/x
    //   280	283	564	com/google/f/x
    //   288	292	564	com/google/f/x
    //   295	303	564	com/google/f/x
    //   306	310	564	com/google/f/x
    //   313	318	564	com/google/f/x
    //   321	325	564	com/google/f/x
    //   327	331	564	com/google/f/x
    //   334	338	564	com/google/f/x
    //   343	347	564	com/google/f/x
    //   348	352	564	com/google/f/x
    //   353	357	564	com/google/f/x
    //   366	369	564	com/google/f/x
    //   374	378	564	com/google/f/x
    //   380	385	564	com/google/f/x
    //   388	393	564	com/google/f/x
    //   397	401	564	com/google/f/x
    //   404	410	564	com/google/f/x
    //   410	414	564	com/google/f/x
    //   415	419	564	com/google/f/x
    //   421	425	564	com/google/f/x
    //   428	432	564	com/google/f/x
    //   437	441	564	com/google/f/x
    //   442	446	564	com/google/f/x
    //   447	451	564	com/google/f/x
    //   460	463	564	com/google/f/x
    //   468	472	564	com/google/f/x
    //   474	479	564	com/google/f/x
    //   482	487	564	com/google/f/x
    //   491	495	564	com/google/f/x
    //   498	504	564	com/google/f/x
    //   504	508	564	com/google/f/x
    //   509	513	564	com/google/f/x
    //   515	519	564	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int j = -1;
    if (i != j) {
      return i;
    }
    Peer localPeer = b;
    j = 0;
    int k;
    if (localPeer != null)
    {
      localPeer = a();
      k = 1;
      i = h.computeMessageSize(k, localPeer) + 0;
    }
    else
    {
      i = 0;
      localPeer = null;
    }
    Object localObject1 = c;
    Object localObject2;
    if (localObject1 != null)
    {
      localObject2 = b();
      k = h.computeMessageSize(2, (ae)localObject2);
      i += k;
    }
    localObject1 = d;
    boolean bool = ((String)localObject1).isEmpty();
    if (!bool)
    {
      localObject2 = d;
      m = h.computeStringSize(3, (String)localObject2);
      i += m;
    }
    int m = e;
    if (m != 0)
    {
      int n = 4;
      m = h.computeInt32Size(n, m);
      i += m;
    }
    for (;;)
    {
      localObject1 = f;
      m = ((w.h)localObject1).size();
      if (j >= m) {
        break;
      }
      localObject2 = (ae)f.get(j);
      m = h.computeMessageSize(5, (ae)localObject2);
      i += m;
      j += 1;
    }
    memoizedSerializedSize = i;
    return i;
  }
  
  public final void writeTo(h paramh)
  {
    Object localObject1 = b;
    int i;
    if (localObject1 != null)
    {
      localObject1 = a();
      i = 1;
      paramh.writeMessage(i, (ae)localObject1);
    }
    localObject1 = c;
    Object localObject2;
    if (localObject1 != null)
    {
      int j = 2;
      localObject2 = b();
      paramh.writeMessage(j, (ae)localObject2);
    }
    localObject1 = d;
    boolean bool = ((String)localObject1).isEmpty();
    if (!bool)
    {
      k = 3;
      localObject2 = d;
      paramh.writeString(k, (String)localObject2);
    }
    int k = e;
    if (k != 0)
    {
      i = 4;
      paramh.writeInt32(i, k);
    }
    k = 0;
    localObject1 = null;
    for (;;)
    {
      localObject2 = f;
      i = ((w.h)localObject2).size();
      if (k >= i) {
        break;
      }
      i = 5;
      ae localae = (ae)f.get(k);
      paramh.writeMessage(i, localae);
      k += 1;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.events.Event.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */