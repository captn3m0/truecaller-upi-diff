package com.truecaller.api.services.messenger.v1.events;

import com.google.f.ae;
import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;
import com.truecaller.api.services.messenger.v1.models.Peer;
import com.truecaller.api.services.messenger.v1.models.a;

public final class Event$x
  extends q
  implements Event.y
{
  private static final x h;
  private static volatile ah i;
  private Peer a;
  private Peer b;
  private String c = "";
  private int d;
  private Peer e;
  private int f;
  private a g;
  
  static
  {
    x localx = new com/truecaller/api/services/messenger/v1/events/Event$x;
    localx.<init>();
    h = localx;
    localx.makeImmutable();
  }
  
  public static x g()
  {
    return h;
  }
  
  public static ah h()
  {
    return h.getParserForType();
  }
  
  public final Peer a()
  {
    Peer localPeer = a;
    if (localPeer == null) {
      localPeer = Peer.d();
    }
    return localPeer;
  }
  
  public final Peer b()
  {
    Peer localPeer = b;
    if (localPeer == null) {
      localPeer = Peer.d();
    }
    return localPeer;
  }
  
  public final String c()
  {
    return c;
  }
  
  public final Peer d()
  {
    Peer localPeer = e;
    if (localPeer == null) {
      localPeer = Peer.d();
    }
    return localPeer;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 55	com/truecaller/api/services/messenger/v1/events/Event$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 61	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_0
    //   19: istore 6
    //   21: aconst_null
    //   22: astore 4
    //   24: iconst_0
    //   25: istore 7
    //   27: iconst_1
    //   28: istore 8
    //   30: iload 5
    //   32: tableswitch	default:+48->80, 1:+1069->1101, 2:+1065->1097, 3:+1063->1095, 4:+1052->1084, 5:+717->749, 6:+110->142, 7:+713->745, 8:+58->90
    //   80: new 64	java/lang/UnsupportedOperationException
    //   83: astore_1
    //   84: aload_1
    //   85: invokespecial 65	java/lang/UnsupportedOperationException:<init>	()V
    //   88: aload_1
    //   89: athrow
    //   90: getstatic 67	com/truecaller/api/services/messenger/v1/events/Event$x:i	Lcom/google/f/ah;
    //   93: astore_1
    //   94: aload_1
    //   95: ifnonnull +43 -> 138
    //   98: ldc 2
    //   100: astore_1
    //   101: aload_1
    //   102: monitorenter
    //   103: getstatic 67	com/truecaller/api/services/messenger/v1/events/Event$x:i	Lcom/google/f/ah;
    //   106: astore_2
    //   107: aload_2
    //   108: ifnonnull +20 -> 128
    //   111: new 69	com/google/f/q$b
    //   114: astore_2
    //   115: getstatic 27	com/truecaller/api/services/messenger/v1/events/Event$x:h	Lcom/truecaller/api/services/messenger/v1/events/Event$x;
    //   118: astore_3
    //   119: aload_2
    //   120: aload_3
    //   121: invokespecial 72	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   124: aload_2
    //   125: putstatic 67	com/truecaller/api/services/messenger/v1/events/Event$x:i	Lcom/google/f/ah;
    //   128: aload_1
    //   129: monitorexit
    //   130: goto +8 -> 138
    //   133: astore_2
    //   134: aload_1
    //   135: monitorexit
    //   136: aload_2
    //   137: athrow
    //   138: getstatic 67	com/truecaller/api/services/messenger/v1/events/Event$x:i	Lcom/google/f/ah;
    //   141: areturn
    //   142: aload_2
    //   143: checkcast 74	com/google/f/g
    //   146: astore_2
    //   147: aload_3
    //   148: checkcast 76	com/google/f/n
    //   151: astore_3
    //   152: iload 7
    //   154: ifne +591 -> 745
    //   157: aload_2
    //   158: invokevirtual 79	com/google/f/g:readTag	()I
    //   161: istore 5
    //   163: iload 5
    //   165: ifeq +518 -> 683
    //   168: bipush 10
    //   170: istore 9
    //   172: iload 5
    //   174: iload 9
    //   176: if_icmpeq +413 -> 589
    //   179: bipush 18
    //   181: istore 9
    //   183: iload 5
    //   185: iload 9
    //   187: if_icmpeq +308 -> 495
    //   190: bipush 26
    //   192: istore 9
    //   194: iload 5
    //   196: iload 9
    //   198: if_icmpeq +284 -> 482
    //   201: bipush 32
    //   203: istore 9
    //   205: iload 5
    //   207: iload 9
    //   209: if_icmpeq +258 -> 467
    //   212: bipush 42
    //   214: istore 9
    //   216: iload 5
    //   218: iload 9
    //   220: if_icmpeq +153 -> 373
    //   223: bipush 48
    //   225: istore 9
    //   227: iload 5
    //   229: iload 9
    //   231: if_icmpeq +127 -> 358
    //   234: bipush 58
    //   236: istore 9
    //   238: iload 5
    //   240: iload 9
    //   242: if_icmpeq +22 -> 264
    //   245: aload_2
    //   246: iload 5
    //   248: invokevirtual 90	com/google/f/g:skipField	(I)Z
    //   251: istore 5
    //   253: iload 5
    //   255: ifne -103 -> 152
    //   258: iconst_1
    //   259: istore 7
    //   261: goto -109 -> 152
    //   264: aload_0
    //   265: getfield 92	com/truecaller/api/services/messenger/v1/events/Event$x:g	Lcom/truecaller/api/services/messenger/v1/models/a;
    //   268: astore_1
    //   269: aload_1
    //   270: ifnull +21 -> 291
    //   273: aload_0
    //   274: getfield 92	com/truecaller/api/services/messenger/v1/events/Event$x:g	Lcom/truecaller/api/services/messenger/v1/models/a;
    //   277: astore_1
    //   278: aload_1
    //   279: invokevirtual 98	com/truecaller/api/services/messenger/v1/models/a:toBuilder	()Lcom/google/f/q$a;
    //   282: astore_1
    //   283: aload_1
    //   284: checkcast 100	com/truecaller/api/services/messenger/v1/models/a$a
    //   287: astore_1
    //   288: goto +8 -> 296
    //   291: iconst_0
    //   292: istore 5
    //   294: aconst_null
    //   295: astore_1
    //   296: invokestatic 102	com/truecaller/api/services/messenger/v1/models/a:f	()Lcom/google/f/ah;
    //   299: astore 10
    //   301: aload_2
    //   302: aload 10
    //   304: aload_3
    //   305: invokevirtual 106	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   308: astore 10
    //   310: aload 10
    //   312: checkcast 94	com/truecaller/api/services/messenger/v1/models/a
    //   315: astore 10
    //   317: aload_0
    //   318: aload 10
    //   320: putfield 92	com/truecaller/api/services/messenger/v1/events/Event$x:g	Lcom/truecaller/api/services/messenger/v1/models/a;
    //   323: aload_1
    //   324: ifnull -172 -> 152
    //   327: aload_0
    //   328: getfield 92	com/truecaller/api/services/messenger/v1/events/Event$x:g	Lcom/truecaller/api/services/messenger/v1/models/a;
    //   331: astore 10
    //   333: aload_1
    //   334: aload 10
    //   336: invokevirtual 110	com/truecaller/api/services/messenger/v1/models/a$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   339: pop
    //   340: aload_1
    //   341: invokevirtual 114	com/truecaller/api/services/messenger/v1/models/a$a:buildPartial	()Lcom/google/f/q;
    //   344: astore_1
    //   345: aload_1
    //   346: checkcast 94	com/truecaller/api/services/messenger/v1/models/a
    //   349: astore_1
    //   350: aload_0
    //   351: aload_1
    //   352: putfield 92	com/truecaller/api/services/messenger/v1/events/Event$x:g	Lcom/truecaller/api/services/messenger/v1/models/a;
    //   355: goto -203 -> 152
    //   358: aload_2
    //   359: invokevirtual 117	com/google/f/g:readInt32	()I
    //   362: istore 5
    //   364: aload_0
    //   365: iload 5
    //   367: putfield 119	com/truecaller/api/services/messenger/v1/events/Event$x:f	I
    //   370: goto -218 -> 152
    //   373: aload_0
    //   374: getfield 50	com/truecaller/api/services/messenger/v1/events/Event$x:e	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   377: astore_1
    //   378: aload_1
    //   379: ifnull +21 -> 400
    //   382: aload_0
    //   383: getfield 50	com/truecaller/api/services/messenger/v1/events/Event$x:e	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   386: astore_1
    //   387: aload_1
    //   388: invokevirtual 120	com/truecaller/api/services/messenger/v1/models/Peer:toBuilder	()Lcom/google/f/q$a;
    //   391: astore_1
    //   392: aload_1
    //   393: checkcast 122	com/truecaller/api/services/messenger/v1/models/Peer$a
    //   396: astore_1
    //   397: goto +8 -> 405
    //   400: iconst_0
    //   401: istore 5
    //   403: aconst_null
    //   404: astore_1
    //   405: invokestatic 124	com/truecaller/api/services/messenger/v1/models/Peer:e	()Lcom/google/f/ah;
    //   408: astore 10
    //   410: aload_2
    //   411: aload 10
    //   413: aload_3
    //   414: invokevirtual 106	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   417: astore 10
    //   419: aload 10
    //   421: checkcast 43	com/truecaller/api/services/messenger/v1/models/Peer
    //   424: astore 10
    //   426: aload_0
    //   427: aload 10
    //   429: putfield 50	com/truecaller/api/services/messenger/v1/events/Event$x:e	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   432: aload_1
    //   433: ifnull -281 -> 152
    //   436: aload_0
    //   437: getfield 50	com/truecaller/api/services/messenger/v1/events/Event$x:e	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   440: astore 10
    //   442: aload_1
    //   443: aload 10
    //   445: invokevirtual 125	com/truecaller/api/services/messenger/v1/models/Peer$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   448: pop
    //   449: aload_1
    //   450: invokevirtual 126	com/truecaller/api/services/messenger/v1/models/Peer$a:buildPartial	()Lcom/google/f/q;
    //   453: astore_1
    //   454: aload_1
    //   455: checkcast 43	com/truecaller/api/services/messenger/v1/models/Peer
    //   458: astore_1
    //   459: aload_0
    //   460: aload_1
    //   461: putfield 50	com/truecaller/api/services/messenger/v1/events/Event$x:e	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   464: goto -312 -> 152
    //   467: aload_2
    //   468: invokevirtual 117	com/google/f/g:readInt32	()I
    //   471: istore 5
    //   473: aload_0
    //   474: iload 5
    //   476: putfield 128	com/truecaller/api/services/messenger/v1/events/Event$x:d	I
    //   479: goto -327 -> 152
    //   482: aload_2
    //   483: invokevirtual 132	com/google/f/g:readStringRequireUtf8	()Ljava/lang/String;
    //   486: astore_1
    //   487: aload_0
    //   488: aload_1
    //   489: putfield 35	com/truecaller/api/services/messenger/v1/events/Event$x:c	Ljava/lang/String;
    //   492: goto -340 -> 152
    //   495: aload_0
    //   496: getfield 48	com/truecaller/api/services/messenger/v1/events/Event$x:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   499: astore_1
    //   500: aload_1
    //   501: ifnull +21 -> 522
    //   504: aload_0
    //   505: getfield 48	com/truecaller/api/services/messenger/v1/events/Event$x:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   508: astore_1
    //   509: aload_1
    //   510: invokevirtual 120	com/truecaller/api/services/messenger/v1/models/Peer:toBuilder	()Lcom/google/f/q$a;
    //   513: astore_1
    //   514: aload_1
    //   515: checkcast 122	com/truecaller/api/services/messenger/v1/models/Peer$a
    //   518: astore_1
    //   519: goto +8 -> 527
    //   522: iconst_0
    //   523: istore 5
    //   525: aconst_null
    //   526: astore_1
    //   527: invokestatic 124	com/truecaller/api/services/messenger/v1/models/Peer:e	()Lcom/google/f/ah;
    //   530: astore 10
    //   532: aload_2
    //   533: aload 10
    //   535: aload_3
    //   536: invokevirtual 106	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   539: astore 10
    //   541: aload 10
    //   543: checkcast 43	com/truecaller/api/services/messenger/v1/models/Peer
    //   546: astore 10
    //   548: aload_0
    //   549: aload 10
    //   551: putfield 48	com/truecaller/api/services/messenger/v1/events/Event$x:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   554: aload_1
    //   555: ifnull -403 -> 152
    //   558: aload_0
    //   559: getfield 48	com/truecaller/api/services/messenger/v1/events/Event$x:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   562: astore 10
    //   564: aload_1
    //   565: aload 10
    //   567: invokevirtual 125	com/truecaller/api/services/messenger/v1/models/Peer$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   570: pop
    //   571: aload_1
    //   572: invokevirtual 126	com/truecaller/api/services/messenger/v1/models/Peer$a:buildPartial	()Lcom/google/f/q;
    //   575: astore_1
    //   576: aload_1
    //   577: checkcast 43	com/truecaller/api/services/messenger/v1/models/Peer
    //   580: astore_1
    //   581: aload_0
    //   582: aload_1
    //   583: putfield 48	com/truecaller/api/services/messenger/v1/events/Event$x:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   586: goto -434 -> 152
    //   589: aload_0
    //   590: getfield 41	com/truecaller/api/services/messenger/v1/events/Event$x:a	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   593: astore_1
    //   594: aload_1
    //   595: ifnull +21 -> 616
    //   598: aload_0
    //   599: getfield 41	com/truecaller/api/services/messenger/v1/events/Event$x:a	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   602: astore_1
    //   603: aload_1
    //   604: invokevirtual 120	com/truecaller/api/services/messenger/v1/models/Peer:toBuilder	()Lcom/google/f/q$a;
    //   607: astore_1
    //   608: aload_1
    //   609: checkcast 122	com/truecaller/api/services/messenger/v1/models/Peer$a
    //   612: astore_1
    //   613: goto +8 -> 621
    //   616: iconst_0
    //   617: istore 5
    //   619: aconst_null
    //   620: astore_1
    //   621: invokestatic 124	com/truecaller/api/services/messenger/v1/models/Peer:e	()Lcom/google/f/ah;
    //   624: astore 10
    //   626: aload_2
    //   627: aload 10
    //   629: aload_3
    //   630: invokevirtual 106	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   633: astore 10
    //   635: aload 10
    //   637: checkcast 43	com/truecaller/api/services/messenger/v1/models/Peer
    //   640: astore 10
    //   642: aload_0
    //   643: aload 10
    //   645: putfield 41	com/truecaller/api/services/messenger/v1/events/Event$x:a	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   648: aload_1
    //   649: ifnull -497 -> 152
    //   652: aload_0
    //   653: getfield 41	com/truecaller/api/services/messenger/v1/events/Event$x:a	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   656: astore 10
    //   658: aload_1
    //   659: aload 10
    //   661: invokevirtual 125	com/truecaller/api/services/messenger/v1/models/Peer$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   664: pop
    //   665: aload_1
    //   666: invokevirtual 126	com/truecaller/api/services/messenger/v1/models/Peer$a:buildPartial	()Lcom/google/f/q;
    //   669: astore_1
    //   670: aload_1
    //   671: checkcast 43	com/truecaller/api/services/messenger/v1/models/Peer
    //   674: astore_1
    //   675: aload_0
    //   676: aload_1
    //   677: putfield 41	com/truecaller/api/services/messenger/v1/events/Event$x:a	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   680: goto -528 -> 152
    //   683: iconst_1
    //   684: istore 7
    //   686: goto -534 -> 152
    //   689: astore_1
    //   690: goto +53 -> 743
    //   693: astore_1
    //   694: new 134	java/lang/RuntimeException
    //   697: astore_2
    //   698: new 136	com/google/f/x
    //   701: astore_3
    //   702: aload_1
    //   703: invokevirtual 141	java/io/IOException:getMessage	()Ljava/lang/String;
    //   706: astore_1
    //   707: aload_3
    //   708: aload_1
    //   709: invokespecial 144	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   712: aload_3
    //   713: aload_0
    //   714: invokevirtual 148	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   717: astore_1
    //   718: aload_2
    //   719: aload_1
    //   720: invokespecial 151	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   723: aload_2
    //   724: athrow
    //   725: astore_1
    //   726: new 134	java/lang/RuntimeException
    //   729: astore_2
    //   730: aload_1
    //   731: aload_0
    //   732: invokevirtual 148	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   735: astore_1
    //   736: aload_2
    //   737: aload_1
    //   738: invokespecial 151	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   741: aload_2
    //   742: athrow
    //   743: aload_1
    //   744: athrow
    //   745: getstatic 27	com/truecaller/api/services/messenger/v1/events/Event$x:h	Lcom/truecaller/api/services/messenger/v1/events/Event$x;
    //   748: areturn
    //   749: aload_2
    //   750: checkcast 153	com/google/f/q$k
    //   753: astore_2
    //   754: aload_3
    //   755: checkcast 2	com/truecaller/api/services/messenger/v1/events/Event$x
    //   758: astore_3
    //   759: aload_0
    //   760: getfield 41	com/truecaller/api/services/messenger/v1/events/Event$x:a	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   763: astore_1
    //   764: aload_3
    //   765: getfield 41	com/truecaller/api/services/messenger/v1/events/Event$x:a	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   768: astore 4
    //   770: aload_2
    //   771: aload_1
    //   772: aload 4
    //   774: invokeinterface 157 3 0
    //   779: checkcast 43	com/truecaller/api/services/messenger/v1/models/Peer
    //   782: astore_1
    //   783: aload_0
    //   784: aload_1
    //   785: putfield 41	com/truecaller/api/services/messenger/v1/events/Event$x:a	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   788: aload_0
    //   789: getfield 48	com/truecaller/api/services/messenger/v1/events/Event$x:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   792: astore_1
    //   793: aload_3
    //   794: getfield 48	com/truecaller/api/services/messenger/v1/events/Event$x:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   797: astore 4
    //   799: aload_2
    //   800: aload_1
    //   801: aload 4
    //   803: invokeinterface 157 3 0
    //   808: checkcast 43	com/truecaller/api/services/messenger/v1/models/Peer
    //   811: astore_1
    //   812: aload_0
    //   813: aload_1
    //   814: putfield 48	com/truecaller/api/services/messenger/v1/events/Event$x:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   817: aload_0
    //   818: getfield 35	com/truecaller/api/services/messenger/v1/events/Event$x:c	Ljava/lang/String;
    //   821: invokevirtual 163	java/lang/String:isEmpty	()Z
    //   824: iload 8
    //   826: ixor
    //   827: istore 5
    //   829: aload_0
    //   830: getfield 35	com/truecaller/api/services/messenger/v1/events/Event$x:c	Ljava/lang/String;
    //   833: astore 4
    //   835: aload_3
    //   836: getfield 35	com/truecaller/api/services/messenger/v1/events/Event$x:c	Ljava/lang/String;
    //   839: astore 10
    //   841: aload 10
    //   843: invokevirtual 163	java/lang/String:isEmpty	()Z
    //   846: iload 8
    //   848: ixor
    //   849: istore 9
    //   851: aload_3
    //   852: getfield 35	com/truecaller/api/services/messenger/v1/events/Event$x:c	Ljava/lang/String;
    //   855: astore 11
    //   857: aload_2
    //   858: iload 5
    //   860: aload 4
    //   862: iload 9
    //   864: aload 11
    //   866: invokeinterface 167 5 0
    //   871: astore_1
    //   872: aload_0
    //   873: aload_1
    //   874: putfield 35	com/truecaller/api/services/messenger/v1/events/Event$x:c	Ljava/lang/String;
    //   877: aload_0
    //   878: getfield 128	com/truecaller/api/services/messenger/v1/events/Event$x:d	I
    //   881: istore 5
    //   883: iload 5
    //   885: ifeq +9 -> 894
    //   888: iconst_1
    //   889: istore 5
    //   891: goto +8 -> 899
    //   894: iconst_0
    //   895: istore 5
    //   897: aconst_null
    //   898: astore_1
    //   899: aload_0
    //   900: getfield 128	com/truecaller/api/services/messenger/v1/events/Event$x:d	I
    //   903: istore 6
    //   905: aload_3
    //   906: getfield 128	com/truecaller/api/services/messenger/v1/events/Event$x:d	I
    //   909: istore 9
    //   911: iload 9
    //   913: ifeq +9 -> 922
    //   916: iconst_1
    //   917: istore 9
    //   919: goto +9 -> 928
    //   922: iconst_0
    //   923: istore 9
    //   925: aconst_null
    //   926: astore 10
    //   928: aload_3
    //   929: getfield 128	com/truecaller/api/services/messenger/v1/events/Event$x:d	I
    //   932: istore 12
    //   934: aload_2
    //   935: iload 5
    //   937: iload 6
    //   939: iload 9
    //   941: iload 12
    //   943: invokeinterface 171 5 0
    //   948: istore 5
    //   950: aload_0
    //   951: iload 5
    //   953: putfield 128	com/truecaller/api/services/messenger/v1/events/Event$x:d	I
    //   956: aload_0
    //   957: getfield 50	com/truecaller/api/services/messenger/v1/events/Event$x:e	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   960: astore_1
    //   961: aload_3
    //   962: getfield 50	com/truecaller/api/services/messenger/v1/events/Event$x:e	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   965: astore 4
    //   967: aload_2
    //   968: aload_1
    //   969: aload 4
    //   971: invokeinterface 157 3 0
    //   976: checkcast 43	com/truecaller/api/services/messenger/v1/models/Peer
    //   979: astore_1
    //   980: aload_0
    //   981: aload_1
    //   982: putfield 50	com/truecaller/api/services/messenger/v1/events/Event$x:e	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   985: aload_0
    //   986: getfield 119	com/truecaller/api/services/messenger/v1/events/Event$x:f	I
    //   989: istore 5
    //   991: iload 5
    //   993: ifeq +9 -> 1002
    //   996: iconst_1
    //   997: istore 5
    //   999: goto +8 -> 1007
    //   1002: iconst_0
    //   1003: istore 5
    //   1005: aconst_null
    //   1006: astore_1
    //   1007: aload_0
    //   1008: getfield 119	com/truecaller/api/services/messenger/v1/events/Event$x:f	I
    //   1011: istore 6
    //   1013: aload_3
    //   1014: getfield 119	com/truecaller/api/services/messenger/v1/events/Event$x:f	I
    //   1017: istore 9
    //   1019: iload 9
    //   1021: ifeq +6 -> 1027
    //   1024: iconst_1
    //   1025: istore 7
    //   1027: aload_3
    //   1028: getfield 119	com/truecaller/api/services/messenger/v1/events/Event$x:f	I
    //   1031: istore 8
    //   1033: aload_2
    //   1034: iload 5
    //   1036: iload 6
    //   1038: iload 7
    //   1040: iload 8
    //   1042: invokeinterface 171 5 0
    //   1047: istore 5
    //   1049: aload_0
    //   1050: iload 5
    //   1052: putfield 119	com/truecaller/api/services/messenger/v1/events/Event$x:f	I
    //   1055: aload_0
    //   1056: getfield 92	com/truecaller/api/services/messenger/v1/events/Event$x:g	Lcom/truecaller/api/services/messenger/v1/models/a;
    //   1059: astore_1
    //   1060: aload_3
    //   1061: getfield 92	com/truecaller/api/services/messenger/v1/events/Event$x:g	Lcom/truecaller/api/services/messenger/v1/models/a;
    //   1064: astore_3
    //   1065: aload_2
    //   1066: aload_1
    //   1067: aload_3
    //   1068: invokeinterface 157 3 0
    //   1073: checkcast 94	com/truecaller/api/services/messenger/v1/models/a
    //   1076: astore_1
    //   1077: aload_0
    //   1078: aload_1
    //   1079: putfield 92	com/truecaller/api/services/messenger/v1/events/Event$x:g	Lcom/truecaller/api/services/messenger/v1/models/a;
    //   1082: aload_0
    //   1083: areturn
    //   1084: new 173	com/truecaller/api/services/messenger/v1/events/Event$x$a
    //   1087: astore_1
    //   1088: aload_1
    //   1089: iconst_0
    //   1090: invokespecial 176	com/truecaller/api/services/messenger/v1/events/Event$x$a:<init>	(B)V
    //   1093: aload_1
    //   1094: areturn
    //   1095: aconst_null
    //   1096: areturn
    //   1097: getstatic 27	com/truecaller/api/services/messenger/v1/events/Event$x:h	Lcom/truecaller/api/services/messenger/v1/events/Event$x;
    //   1100: areturn
    //   1101: new 2	com/truecaller/api/services/messenger/v1/events/Event$x
    //   1104: astore_1
    //   1105: aload_1
    //   1106: invokespecial 25	com/truecaller/api/services/messenger/v1/events/Event$x:<init>	()V
    //   1109: aload_1
    //   1110: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1111	0	this	x
    //   0	1111	1	paramj	com.google.f.q.j
    //   0	1111	2	paramObject1	Object
    //   0	1111	3	paramObject2	Object
    //   3	967	4	localObject1	Object
    //   9	238	5	j	int
    //   251	42	5	bool1	boolean
    //   362	256	5	k	int
    //   827	32	5	bool2	boolean
    //   881	55	5	m	int
    //   948	87	5	n	int
    //   1047	4	5	i1	int
    //   19	1018	6	i2	int
    //   25	1014	7	bool3	boolean
    //   28	1013	8	i3	int
    //   170	73	9	i4	int
    //   849	14	9	bool4	boolean
    //   909	31	9	i5	int
    //   1017	3	9	i6	int
    //   299	628	10	localObject2	Object
    //   855	10	11	str	String
    //   932	10	12	i7	int
    // Exception table:
    //   from	to	target	type
    //   103	106	133	finally
    //   111	114	133	finally
    //   115	118	133	finally
    //   120	124	133	finally
    //   124	128	133	finally
    //   128	130	133	finally
    //   134	136	133	finally
    //   157	161	689	finally
    //   246	251	689	finally
    //   264	268	689	finally
    //   273	277	689	finally
    //   278	282	689	finally
    //   283	287	689	finally
    //   296	299	689	finally
    //   304	308	689	finally
    //   310	315	689	finally
    //   318	323	689	finally
    //   327	331	689	finally
    //   334	340	689	finally
    //   340	344	689	finally
    //   345	349	689	finally
    //   351	355	689	finally
    //   358	362	689	finally
    //   365	370	689	finally
    //   373	377	689	finally
    //   382	386	689	finally
    //   387	391	689	finally
    //   392	396	689	finally
    //   405	408	689	finally
    //   413	417	689	finally
    //   419	424	689	finally
    //   427	432	689	finally
    //   436	440	689	finally
    //   443	449	689	finally
    //   449	453	689	finally
    //   454	458	689	finally
    //   460	464	689	finally
    //   467	471	689	finally
    //   474	479	689	finally
    //   482	486	689	finally
    //   488	492	689	finally
    //   495	499	689	finally
    //   504	508	689	finally
    //   509	513	689	finally
    //   514	518	689	finally
    //   527	530	689	finally
    //   535	539	689	finally
    //   541	546	689	finally
    //   549	554	689	finally
    //   558	562	689	finally
    //   565	571	689	finally
    //   571	575	689	finally
    //   576	580	689	finally
    //   582	586	689	finally
    //   589	593	689	finally
    //   598	602	689	finally
    //   603	607	689	finally
    //   608	612	689	finally
    //   621	624	689	finally
    //   629	633	689	finally
    //   635	640	689	finally
    //   643	648	689	finally
    //   652	656	689	finally
    //   659	665	689	finally
    //   665	669	689	finally
    //   670	674	689	finally
    //   676	680	689	finally
    //   694	697	689	finally
    //   698	701	689	finally
    //   702	706	689	finally
    //   708	712	689	finally
    //   713	717	689	finally
    //   719	723	689	finally
    //   723	725	689	finally
    //   726	729	689	finally
    //   731	735	689	finally
    //   737	741	689	finally
    //   741	743	689	finally
    //   157	161	693	java/io/IOException
    //   246	251	693	java/io/IOException
    //   264	268	693	java/io/IOException
    //   273	277	693	java/io/IOException
    //   278	282	693	java/io/IOException
    //   283	287	693	java/io/IOException
    //   296	299	693	java/io/IOException
    //   304	308	693	java/io/IOException
    //   310	315	693	java/io/IOException
    //   318	323	693	java/io/IOException
    //   327	331	693	java/io/IOException
    //   334	340	693	java/io/IOException
    //   340	344	693	java/io/IOException
    //   345	349	693	java/io/IOException
    //   351	355	693	java/io/IOException
    //   358	362	693	java/io/IOException
    //   365	370	693	java/io/IOException
    //   373	377	693	java/io/IOException
    //   382	386	693	java/io/IOException
    //   387	391	693	java/io/IOException
    //   392	396	693	java/io/IOException
    //   405	408	693	java/io/IOException
    //   413	417	693	java/io/IOException
    //   419	424	693	java/io/IOException
    //   427	432	693	java/io/IOException
    //   436	440	693	java/io/IOException
    //   443	449	693	java/io/IOException
    //   449	453	693	java/io/IOException
    //   454	458	693	java/io/IOException
    //   460	464	693	java/io/IOException
    //   467	471	693	java/io/IOException
    //   474	479	693	java/io/IOException
    //   482	486	693	java/io/IOException
    //   488	492	693	java/io/IOException
    //   495	499	693	java/io/IOException
    //   504	508	693	java/io/IOException
    //   509	513	693	java/io/IOException
    //   514	518	693	java/io/IOException
    //   527	530	693	java/io/IOException
    //   535	539	693	java/io/IOException
    //   541	546	693	java/io/IOException
    //   549	554	693	java/io/IOException
    //   558	562	693	java/io/IOException
    //   565	571	693	java/io/IOException
    //   571	575	693	java/io/IOException
    //   576	580	693	java/io/IOException
    //   582	586	693	java/io/IOException
    //   589	593	693	java/io/IOException
    //   598	602	693	java/io/IOException
    //   603	607	693	java/io/IOException
    //   608	612	693	java/io/IOException
    //   621	624	693	java/io/IOException
    //   629	633	693	java/io/IOException
    //   635	640	693	java/io/IOException
    //   643	648	693	java/io/IOException
    //   652	656	693	java/io/IOException
    //   659	665	693	java/io/IOException
    //   665	669	693	java/io/IOException
    //   670	674	693	java/io/IOException
    //   676	680	693	java/io/IOException
    //   157	161	725	com/google/f/x
    //   246	251	725	com/google/f/x
    //   264	268	725	com/google/f/x
    //   273	277	725	com/google/f/x
    //   278	282	725	com/google/f/x
    //   283	287	725	com/google/f/x
    //   296	299	725	com/google/f/x
    //   304	308	725	com/google/f/x
    //   310	315	725	com/google/f/x
    //   318	323	725	com/google/f/x
    //   327	331	725	com/google/f/x
    //   334	340	725	com/google/f/x
    //   340	344	725	com/google/f/x
    //   345	349	725	com/google/f/x
    //   351	355	725	com/google/f/x
    //   358	362	725	com/google/f/x
    //   365	370	725	com/google/f/x
    //   373	377	725	com/google/f/x
    //   382	386	725	com/google/f/x
    //   387	391	725	com/google/f/x
    //   392	396	725	com/google/f/x
    //   405	408	725	com/google/f/x
    //   413	417	725	com/google/f/x
    //   419	424	725	com/google/f/x
    //   427	432	725	com/google/f/x
    //   436	440	725	com/google/f/x
    //   443	449	725	com/google/f/x
    //   449	453	725	com/google/f/x
    //   454	458	725	com/google/f/x
    //   460	464	725	com/google/f/x
    //   467	471	725	com/google/f/x
    //   474	479	725	com/google/f/x
    //   482	486	725	com/google/f/x
    //   488	492	725	com/google/f/x
    //   495	499	725	com/google/f/x
    //   504	508	725	com/google/f/x
    //   509	513	725	com/google/f/x
    //   514	518	725	com/google/f/x
    //   527	530	725	com/google/f/x
    //   535	539	725	com/google/f/x
    //   541	546	725	com/google/f/x
    //   549	554	725	com/google/f/x
    //   558	562	725	com/google/f/x
    //   565	571	725	com/google/f/x
    //   571	575	725	com/google/f/x
    //   576	580	725	com/google/f/x
    //   582	586	725	com/google/f/x
    //   589	593	725	com/google/f/x
    //   598	602	725	com/google/f/x
    //   603	607	725	com/google/f/x
    //   608	612	725	com/google/f/x
    //   621	624	725	com/google/f/x
    //   629	633	725	com/google/f/x
    //   635	640	725	com/google/f/x
    //   643	648	725	com/google/f/x
    //   652	656	725	com/google/f/x
    //   659	665	725	com/google/f/x
    //   665	669	725	com/google/f/x
    //   670	674	725	com/google/f/x
    //   676	680	725	com/google/f/x
  }
  
  public final int e()
  {
    return f;
  }
  
  public final a f()
  {
    a locala = g;
    if (locala == null) {
      locala = a.e();
    }
    return locala;
  }
  
  public final int getSerializedSize()
  {
    int j = memoizedSerializedSize;
    int m = -1;
    if (j != m) {
      return j;
    }
    Object localObject1 = a;
    m = 0;
    Object localObject2;
    if (localObject1 != null)
    {
      localObject2 = a();
      j = h.computeMessageSize(1, (ae)localObject2);
      m = 0 + j;
    }
    localObject1 = b;
    if (localObject1 != null)
    {
      localObject2 = b();
      j = h.computeMessageSize(2, (ae)localObject2);
      m += j;
    }
    localObject1 = c;
    boolean bool = ((String)localObject1).isEmpty();
    if (!bool)
    {
      localObject2 = c;
      k = h.computeStringSize(3, (String)localObject2);
      m += k;
    }
    int k = d;
    int n;
    if (k != 0)
    {
      n = 4;
      k = h.computeInt32Size(n, k);
      m += k;
    }
    localObject1 = e;
    if (localObject1 != null)
    {
      localObject2 = d();
      k = h.computeMessageSize(5, (ae)localObject2);
      m += k;
    }
    k = f;
    if (k != 0)
    {
      n = 6;
      k = h.computeInt32Size(n, k);
      m += k;
    }
    localObject1 = g;
    if (localObject1 != null)
    {
      localObject2 = f();
      k = h.computeMessageSize(7, (ae)localObject2);
      m += k;
    }
    memoizedSerializedSize = m;
    return m;
  }
  
  public final void writeTo(h paramh)
  {
    Object localObject1 = a;
    int j;
    Object localObject2;
    if (localObject1 != null)
    {
      j = 1;
      localObject2 = a();
      paramh.writeMessage(j, (ae)localObject2);
    }
    localObject1 = b;
    if (localObject1 != null)
    {
      j = 2;
      localObject2 = b();
      paramh.writeMessage(j, (ae)localObject2);
    }
    localObject1 = c;
    boolean bool = ((String)localObject1).isEmpty();
    if (!bool)
    {
      k = 3;
      localObject2 = c;
      paramh.writeString(k, (String)localObject2);
    }
    int k = d;
    int m;
    if (k != 0)
    {
      m = 4;
      paramh.writeInt32(m, k);
    }
    localObject1 = e;
    if (localObject1 != null)
    {
      k = 5;
      localObject2 = d();
      paramh.writeMessage(k, (ae)localObject2);
    }
    k = f;
    if (k != 0)
    {
      m = 6;
      paramh.writeInt32(m, k);
    }
    localObject1 = g;
    if (localObject1 != null)
    {
      k = 7;
      localObject2 = f();
      paramh.writeMessage(k, (ae)localObject2);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.events.Event.x
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */