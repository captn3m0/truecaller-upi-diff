package com.truecaller.api.services.messenger.v1.events;

import com.google.f.ae;
import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;
import com.truecaller.api.services.messenger.v1.models.Peer;
import com.truecaller.api.services.messenger.v1.models.ReactionContent;

public final class Event$t
  extends q
  implements Event.u
{
  private static final t e;
  private static volatile ah f;
  private Peer a;
  private Peer b;
  private int c;
  private ReactionContent d;
  
  static
  {
    t localt = new com/truecaller/api/services/messenger/v1/events/Event$t;
    localt.<init>();
    e = localt;
    localt.makeImmutable();
  }
  
  public static t e()
  {
    return e;
  }
  
  public static ah f()
  {
    return e.getParserForType();
  }
  
  public final Peer a()
  {
    Peer localPeer = a;
    if (localPeer == null) {
      localPeer = Peer.d();
    }
    return localPeer;
  }
  
  public final Peer b()
  {
    Peer localPeer = b;
    if (localPeer == null) {
      localPeer = Peer.d();
    }
    return localPeer;
  }
  
  public final int c()
  {
    return c;
  }
  
  public final ReactionContent d()
  {
    ReactionContent localReactionContent = d;
    if (localReactionContent == null) {
      localReactionContent = ReactionContent.c();
    }
    return localReactionContent;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 54	com/truecaller/api/services/messenger/v1/events/Event$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 60	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_1
    //   19: istore 6
    //   21: iconst_0
    //   22: istore 7
    //   24: iconst_0
    //   25: istore 8
    //   27: aconst_null
    //   28: astore 9
    //   30: iload 5
    //   32: tableswitch	default:+48->80, 1:+752->784, 2:+748->780, 3:+746->778, 4:+735->767, 5:+562->594, 6:+110->142, 7:+558->590, 8:+58->90
    //   80: new 63	java/lang/UnsupportedOperationException
    //   83: astore_1
    //   84: aload_1
    //   85: invokespecial 64	java/lang/UnsupportedOperationException:<init>	()V
    //   88: aload_1
    //   89: athrow
    //   90: getstatic 66	com/truecaller/api/services/messenger/v1/events/Event$t:f	Lcom/google/f/ah;
    //   93: astore_1
    //   94: aload_1
    //   95: ifnonnull +43 -> 138
    //   98: ldc 2
    //   100: astore_1
    //   101: aload_1
    //   102: monitorenter
    //   103: getstatic 66	com/truecaller/api/services/messenger/v1/events/Event$t:f	Lcom/google/f/ah;
    //   106: astore_2
    //   107: aload_2
    //   108: ifnonnull +20 -> 128
    //   111: new 68	com/google/f/q$b
    //   114: astore_2
    //   115: getstatic 23	com/truecaller/api/services/messenger/v1/events/Event$t:e	Lcom/truecaller/api/services/messenger/v1/events/Event$t;
    //   118: astore_3
    //   119: aload_2
    //   120: aload_3
    //   121: invokespecial 71	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   124: aload_2
    //   125: putstatic 66	com/truecaller/api/services/messenger/v1/events/Event$t:f	Lcom/google/f/ah;
    //   128: aload_1
    //   129: monitorexit
    //   130: goto +8 -> 138
    //   133: astore_2
    //   134: aload_1
    //   135: monitorexit
    //   136: aload_2
    //   137: athrow
    //   138: getstatic 66	com/truecaller/api/services/messenger/v1/events/Event$t:f	Lcom/google/f/ah;
    //   141: areturn
    //   142: aload_2
    //   143: checkcast 73	com/google/f/g
    //   146: astore_2
    //   147: aload_3
    //   148: checkcast 75	com/google/f/n
    //   151: astore_3
    //   152: iload 7
    //   154: ifne +436 -> 590
    //   157: aload_2
    //   158: invokevirtual 78	com/google/f/g:readTag	()I
    //   161: istore 5
    //   163: iload 5
    //   165: ifeq +363 -> 528
    //   168: bipush 10
    //   170: istore 10
    //   172: iload 5
    //   174: iload 10
    //   176: if_icmpeq +258 -> 434
    //   179: bipush 18
    //   181: istore 10
    //   183: iload 5
    //   185: iload 10
    //   187: if_icmpeq +153 -> 340
    //   190: bipush 24
    //   192: istore 10
    //   194: iload 5
    //   196: iload 10
    //   198: if_icmpeq +127 -> 325
    //   201: bipush 34
    //   203: istore 10
    //   205: iload 5
    //   207: iload 10
    //   209: if_icmpeq +22 -> 231
    //   212: aload_2
    //   213: iload 5
    //   215: invokevirtual 86	com/google/f/g:skipField	(I)Z
    //   218: istore 5
    //   220: iload 5
    //   222: ifne -70 -> 152
    //   225: iconst_1
    //   226: istore 7
    //   228: goto -76 -> 152
    //   231: aload_0
    //   232: getfield 44	com/truecaller/api/services/messenger/v1/events/Event$t:d	Lcom/truecaller/api/services/messenger/v1/models/ReactionContent;
    //   235: astore_1
    //   236: aload_1
    //   237: ifnull +21 -> 258
    //   240: aload_0
    //   241: getfield 44	com/truecaller/api/services/messenger/v1/events/Event$t:d	Lcom/truecaller/api/services/messenger/v1/models/ReactionContent;
    //   244: astore_1
    //   245: aload_1
    //   246: invokevirtual 90	com/truecaller/api/services/messenger/v1/models/ReactionContent:toBuilder	()Lcom/google/f/q$a;
    //   249: astore_1
    //   250: aload_1
    //   251: checkcast 92	com/truecaller/api/services/messenger/v1/models/ReactionContent$a
    //   254: astore_1
    //   255: goto +8 -> 263
    //   258: iconst_0
    //   259: istore 5
    //   261: aconst_null
    //   262: astore_1
    //   263: invokestatic 94	com/truecaller/api/services/messenger/v1/models/ReactionContent:d	()Lcom/google/f/ah;
    //   266: astore 11
    //   268: aload_2
    //   269: aload 11
    //   271: aload_3
    //   272: invokevirtual 98	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   275: astore 11
    //   277: aload 11
    //   279: checkcast 46	com/truecaller/api/services/messenger/v1/models/ReactionContent
    //   282: astore 11
    //   284: aload_0
    //   285: aload 11
    //   287: putfield 44	com/truecaller/api/services/messenger/v1/events/Event$t:d	Lcom/truecaller/api/services/messenger/v1/models/ReactionContent;
    //   290: aload_1
    //   291: ifnull -139 -> 152
    //   294: aload_0
    //   295: getfield 44	com/truecaller/api/services/messenger/v1/events/Event$t:d	Lcom/truecaller/api/services/messenger/v1/models/ReactionContent;
    //   298: astore 11
    //   300: aload_1
    //   301: aload 11
    //   303: invokevirtual 102	com/truecaller/api/services/messenger/v1/models/ReactionContent$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   306: pop
    //   307: aload_1
    //   308: invokevirtual 106	com/truecaller/api/services/messenger/v1/models/ReactionContent$a:buildPartial	()Lcom/google/f/q;
    //   311: astore_1
    //   312: aload_1
    //   313: checkcast 46	com/truecaller/api/services/messenger/v1/models/ReactionContent
    //   316: astore_1
    //   317: aload_0
    //   318: aload_1
    //   319: putfield 44	com/truecaller/api/services/messenger/v1/events/Event$t:d	Lcom/truecaller/api/services/messenger/v1/models/ReactionContent;
    //   322: goto -170 -> 152
    //   325: aload_2
    //   326: invokevirtual 109	com/google/f/g:readInt32	()I
    //   329: istore 5
    //   331: aload_0
    //   332: iload 5
    //   334: putfield 42	com/truecaller/api/services/messenger/v1/events/Event$t:c	I
    //   337: goto -185 -> 152
    //   340: aload_0
    //   341: getfield 40	com/truecaller/api/services/messenger/v1/events/Event$t:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   344: astore_1
    //   345: aload_1
    //   346: ifnull +21 -> 367
    //   349: aload_0
    //   350: getfield 40	com/truecaller/api/services/messenger/v1/events/Event$t:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   353: astore_1
    //   354: aload_1
    //   355: invokevirtual 110	com/truecaller/api/services/messenger/v1/models/Peer:toBuilder	()Lcom/google/f/q$a;
    //   358: astore_1
    //   359: aload_1
    //   360: checkcast 112	com/truecaller/api/services/messenger/v1/models/Peer$a
    //   363: astore_1
    //   364: goto +8 -> 372
    //   367: iconst_0
    //   368: istore 5
    //   370: aconst_null
    //   371: astore_1
    //   372: invokestatic 114	com/truecaller/api/services/messenger/v1/models/Peer:e	()Lcom/google/f/ah;
    //   375: astore 11
    //   377: aload_2
    //   378: aload 11
    //   380: aload_3
    //   381: invokevirtual 98	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   384: astore 11
    //   386: aload 11
    //   388: checkcast 35	com/truecaller/api/services/messenger/v1/models/Peer
    //   391: astore 11
    //   393: aload_0
    //   394: aload 11
    //   396: putfield 40	com/truecaller/api/services/messenger/v1/events/Event$t:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   399: aload_1
    //   400: ifnull -248 -> 152
    //   403: aload_0
    //   404: getfield 40	com/truecaller/api/services/messenger/v1/events/Event$t:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   407: astore 11
    //   409: aload_1
    //   410: aload 11
    //   412: invokevirtual 115	com/truecaller/api/services/messenger/v1/models/Peer$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   415: pop
    //   416: aload_1
    //   417: invokevirtual 116	com/truecaller/api/services/messenger/v1/models/Peer$a:buildPartial	()Lcom/google/f/q;
    //   420: astore_1
    //   421: aload_1
    //   422: checkcast 35	com/truecaller/api/services/messenger/v1/models/Peer
    //   425: astore_1
    //   426: aload_0
    //   427: aload_1
    //   428: putfield 40	com/truecaller/api/services/messenger/v1/events/Event$t:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   431: goto -279 -> 152
    //   434: aload_0
    //   435: getfield 33	com/truecaller/api/services/messenger/v1/events/Event$t:a	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   438: astore_1
    //   439: aload_1
    //   440: ifnull +21 -> 461
    //   443: aload_0
    //   444: getfield 33	com/truecaller/api/services/messenger/v1/events/Event$t:a	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   447: astore_1
    //   448: aload_1
    //   449: invokevirtual 110	com/truecaller/api/services/messenger/v1/models/Peer:toBuilder	()Lcom/google/f/q$a;
    //   452: astore_1
    //   453: aload_1
    //   454: checkcast 112	com/truecaller/api/services/messenger/v1/models/Peer$a
    //   457: astore_1
    //   458: goto +8 -> 466
    //   461: iconst_0
    //   462: istore 5
    //   464: aconst_null
    //   465: astore_1
    //   466: invokestatic 114	com/truecaller/api/services/messenger/v1/models/Peer:e	()Lcom/google/f/ah;
    //   469: astore 11
    //   471: aload_2
    //   472: aload 11
    //   474: aload_3
    //   475: invokevirtual 98	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   478: astore 11
    //   480: aload 11
    //   482: checkcast 35	com/truecaller/api/services/messenger/v1/models/Peer
    //   485: astore 11
    //   487: aload_0
    //   488: aload 11
    //   490: putfield 33	com/truecaller/api/services/messenger/v1/events/Event$t:a	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   493: aload_1
    //   494: ifnull -342 -> 152
    //   497: aload_0
    //   498: getfield 33	com/truecaller/api/services/messenger/v1/events/Event$t:a	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   501: astore 11
    //   503: aload_1
    //   504: aload 11
    //   506: invokevirtual 115	com/truecaller/api/services/messenger/v1/models/Peer$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   509: pop
    //   510: aload_1
    //   511: invokevirtual 116	com/truecaller/api/services/messenger/v1/models/Peer$a:buildPartial	()Lcom/google/f/q;
    //   514: astore_1
    //   515: aload_1
    //   516: checkcast 35	com/truecaller/api/services/messenger/v1/models/Peer
    //   519: astore_1
    //   520: aload_0
    //   521: aload_1
    //   522: putfield 33	com/truecaller/api/services/messenger/v1/events/Event$t:a	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   525: goto -373 -> 152
    //   528: iconst_1
    //   529: istore 7
    //   531: goto -379 -> 152
    //   534: astore_1
    //   535: goto +53 -> 588
    //   538: astore_1
    //   539: new 118	java/lang/RuntimeException
    //   542: astore_2
    //   543: new 120	com/google/f/x
    //   546: astore_3
    //   547: aload_1
    //   548: invokevirtual 126	java/io/IOException:getMessage	()Ljava/lang/String;
    //   551: astore_1
    //   552: aload_3
    //   553: aload_1
    //   554: invokespecial 129	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   557: aload_3
    //   558: aload_0
    //   559: invokevirtual 133	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   562: astore_1
    //   563: aload_2
    //   564: aload_1
    //   565: invokespecial 136	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   568: aload_2
    //   569: athrow
    //   570: astore_1
    //   571: new 118	java/lang/RuntimeException
    //   574: astore_2
    //   575: aload_1
    //   576: aload_0
    //   577: invokevirtual 133	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   580: astore_1
    //   581: aload_2
    //   582: aload_1
    //   583: invokespecial 136	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   586: aload_2
    //   587: athrow
    //   588: aload_1
    //   589: athrow
    //   590: getstatic 23	com/truecaller/api/services/messenger/v1/events/Event$t:e	Lcom/truecaller/api/services/messenger/v1/events/Event$t;
    //   593: areturn
    //   594: aload_2
    //   595: checkcast 138	com/google/f/q$k
    //   598: astore_2
    //   599: aload_3
    //   600: checkcast 2	com/truecaller/api/services/messenger/v1/events/Event$t
    //   603: astore_3
    //   604: aload_0
    //   605: getfield 33	com/truecaller/api/services/messenger/v1/events/Event$t:a	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   608: astore_1
    //   609: aload_3
    //   610: getfield 33	com/truecaller/api/services/messenger/v1/events/Event$t:a	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   613: astore 9
    //   615: aload_2
    //   616: aload_1
    //   617: aload 9
    //   619: invokeinterface 142 3 0
    //   624: checkcast 35	com/truecaller/api/services/messenger/v1/models/Peer
    //   627: astore_1
    //   628: aload_0
    //   629: aload_1
    //   630: putfield 33	com/truecaller/api/services/messenger/v1/events/Event$t:a	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   633: aload_0
    //   634: getfield 40	com/truecaller/api/services/messenger/v1/events/Event$t:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   637: astore_1
    //   638: aload_3
    //   639: getfield 40	com/truecaller/api/services/messenger/v1/events/Event$t:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   642: astore 9
    //   644: aload_2
    //   645: aload_1
    //   646: aload 9
    //   648: invokeinterface 142 3 0
    //   653: checkcast 35	com/truecaller/api/services/messenger/v1/models/Peer
    //   656: astore_1
    //   657: aload_0
    //   658: aload_1
    //   659: putfield 40	com/truecaller/api/services/messenger/v1/events/Event$t:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   662: aload_0
    //   663: getfield 42	com/truecaller/api/services/messenger/v1/events/Event$t:c	I
    //   666: istore 5
    //   668: iload 5
    //   670: ifeq +9 -> 679
    //   673: iconst_1
    //   674: istore 5
    //   676: goto +8 -> 684
    //   679: iconst_0
    //   680: istore 5
    //   682: aconst_null
    //   683: astore_1
    //   684: aload_0
    //   685: getfield 42	com/truecaller/api/services/messenger/v1/events/Event$t:c	I
    //   688: istore 8
    //   690: aload_3
    //   691: getfield 42	com/truecaller/api/services/messenger/v1/events/Event$t:c	I
    //   694: istore 10
    //   696: iload 10
    //   698: ifeq +6 -> 704
    //   701: goto +9 -> 710
    //   704: iconst_0
    //   705: istore 6
    //   707: aconst_null
    //   708: astore 4
    //   710: aload_3
    //   711: getfield 42	com/truecaller/api/services/messenger/v1/events/Event$t:c	I
    //   714: istore 7
    //   716: aload_2
    //   717: iload 5
    //   719: iload 8
    //   721: iload 6
    //   723: iload 7
    //   725: invokeinterface 146 5 0
    //   730: istore 5
    //   732: aload_0
    //   733: iload 5
    //   735: putfield 42	com/truecaller/api/services/messenger/v1/events/Event$t:c	I
    //   738: aload_0
    //   739: getfield 44	com/truecaller/api/services/messenger/v1/events/Event$t:d	Lcom/truecaller/api/services/messenger/v1/models/ReactionContent;
    //   742: astore_1
    //   743: aload_3
    //   744: getfield 44	com/truecaller/api/services/messenger/v1/events/Event$t:d	Lcom/truecaller/api/services/messenger/v1/models/ReactionContent;
    //   747: astore_3
    //   748: aload_2
    //   749: aload_1
    //   750: aload_3
    //   751: invokeinterface 142 3 0
    //   756: checkcast 46	com/truecaller/api/services/messenger/v1/models/ReactionContent
    //   759: astore_1
    //   760: aload_0
    //   761: aload_1
    //   762: putfield 44	com/truecaller/api/services/messenger/v1/events/Event$t:d	Lcom/truecaller/api/services/messenger/v1/models/ReactionContent;
    //   765: aload_0
    //   766: areturn
    //   767: new 148	com/truecaller/api/services/messenger/v1/events/Event$t$a
    //   770: astore_1
    //   771: aload_1
    //   772: iconst_0
    //   773: invokespecial 151	com/truecaller/api/services/messenger/v1/events/Event$t$a:<init>	(B)V
    //   776: aload_1
    //   777: areturn
    //   778: aconst_null
    //   779: areturn
    //   780: getstatic 23	com/truecaller/api/services/messenger/v1/events/Event$t:e	Lcom/truecaller/api/services/messenger/v1/events/Event$t;
    //   783: areturn
    //   784: new 2	com/truecaller/api/services/messenger/v1/events/Event$t
    //   787: astore_1
    //   788: aload_1
    //   789: invokespecial 21	com/truecaller/api/services/messenger/v1/events/Event$t:<init>	()V
    //   792: aload_1
    //   793: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	794	0	this	t
    //   0	794	1	paramj	com.google.f.q.j
    //   0	794	2	paramObject1	Object
    //   0	794	3	paramObject2	Object
    //   3	706	4	arrayOfInt	int[]
    //   9	205	5	i	int
    //   218	42	5	bool1	boolean
    //   329	389	5	j	int
    //   730	4	5	k	int
    //   19	703	6	bool2	boolean
    //   22	702	7	m	int
    //   25	695	8	n	int
    //   28	619	9	localPeer	Peer
    //   170	527	10	i1	int
    //   266	239	11	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   103	106	133	finally
    //   111	114	133	finally
    //   115	118	133	finally
    //   120	124	133	finally
    //   124	128	133	finally
    //   128	130	133	finally
    //   134	136	133	finally
    //   157	161	534	finally
    //   213	218	534	finally
    //   231	235	534	finally
    //   240	244	534	finally
    //   245	249	534	finally
    //   250	254	534	finally
    //   263	266	534	finally
    //   271	275	534	finally
    //   277	282	534	finally
    //   285	290	534	finally
    //   294	298	534	finally
    //   301	307	534	finally
    //   307	311	534	finally
    //   312	316	534	finally
    //   318	322	534	finally
    //   325	329	534	finally
    //   332	337	534	finally
    //   340	344	534	finally
    //   349	353	534	finally
    //   354	358	534	finally
    //   359	363	534	finally
    //   372	375	534	finally
    //   380	384	534	finally
    //   386	391	534	finally
    //   394	399	534	finally
    //   403	407	534	finally
    //   410	416	534	finally
    //   416	420	534	finally
    //   421	425	534	finally
    //   427	431	534	finally
    //   434	438	534	finally
    //   443	447	534	finally
    //   448	452	534	finally
    //   453	457	534	finally
    //   466	469	534	finally
    //   474	478	534	finally
    //   480	485	534	finally
    //   488	493	534	finally
    //   497	501	534	finally
    //   504	510	534	finally
    //   510	514	534	finally
    //   515	519	534	finally
    //   521	525	534	finally
    //   539	542	534	finally
    //   543	546	534	finally
    //   547	551	534	finally
    //   553	557	534	finally
    //   558	562	534	finally
    //   564	568	534	finally
    //   568	570	534	finally
    //   571	574	534	finally
    //   576	580	534	finally
    //   582	586	534	finally
    //   586	588	534	finally
    //   157	161	538	java/io/IOException
    //   213	218	538	java/io/IOException
    //   231	235	538	java/io/IOException
    //   240	244	538	java/io/IOException
    //   245	249	538	java/io/IOException
    //   250	254	538	java/io/IOException
    //   263	266	538	java/io/IOException
    //   271	275	538	java/io/IOException
    //   277	282	538	java/io/IOException
    //   285	290	538	java/io/IOException
    //   294	298	538	java/io/IOException
    //   301	307	538	java/io/IOException
    //   307	311	538	java/io/IOException
    //   312	316	538	java/io/IOException
    //   318	322	538	java/io/IOException
    //   325	329	538	java/io/IOException
    //   332	337	538	java/io/IOException
    //   340	344	538	java/io/IOException
    //   349	353	538	java/io/IOException
    //   354	358	538	java/io/IOException
    //   359	363	538	java/io/IOException
    //   372	375	538	java/io/IOException
    //   380	384	538	java/io/IOException
    //   386	391	538	java/io/IOException
    //   394	399	538	java/io/IOException
    //   403	407	538	java/io/IOException
    //   410	416	538	java/io/IOException
    //   416	420	538	java/io/IOException
    //   421	425	538	java/io/IOException
    //   427	431	538	java/io/IOException
    //   434	438	538	java/io/IOException
    //   443	447	538	java/io/IOException
    //   448	452	538	java/io/IOException
    //   453	457	538	java/io/IOException
    //   466	469	538	java/io/IOException
    //   474	478	538	java/io/IOException
    //   480	485	538	java/io/IOException
    //   488	493	538	java/io/IOException
    //   497	501	538	java/io/IOException
    //   504	510	538	java/io/IOException
    //   510	514	538	java/io/IOException
    //   515	519	538	java/io/IOException
    //   521	525	538	java/io/IOException
    //   157	161	570	com/google/f/x
    //   213	218	570	com/google/f/x
    //   231	235	570	com/google/f/x
    //   240	244	570	com/google/f/x
    //   245	249	570	com/google/f/x
    //   250	254	570	com/google/f/x
    //   263	266	570	com/google/f/x
    //   271	275	570	com/google/f/x
    //   277	282	570	com/google/f/x
    //   285	290	570	com/google/f/x
    //   294	298	570	com/google/f/x
    //   301	307	570	com/google/f/x
    //   307	311	570	com/google/f/x
    //   312	316	570	com/google/f/x
    //   318	322	570	com/google/f/x
    //   325	329	570	com/google/f/x
    //   332	337	570	com/google/f/x
    //   340	344	570	com/google/f/x
    //   349	353	570	com/google/f/x
    //   354	358	570	com/google/f/x
    //   359	363	570	com/google/f/x
    //   372	375	570	com/google/f/x
    //   380	384	570	com/google/f/x
    //   386	391	570	com/google/f/x
    //   394	399	570	com/google/f/x
    //   403	407	570	com/google/f/x
    //   410	416	570	com/google/f/x
    //   416	420	570	com/google/f/x
    //   421	425	570	com/google/f/x
    //   427	431	570	com/google/f/x
    //   434	438	570	com/google/f/x
    //   443	447	570	com/google/f/x
    //   448	452	570	com/google/f/x
    //   453	457	570	com/google/f/x
    //   466	469	570	com/google/f/x
    //   474	478	570	com/google/f/x
    //   480	485	570	com/google/f/x
    //   488	493	570	com/google/f/x
    //   497	501	570	com/google/f/x
    //   504	510	570	com/google/f/x
    //   510	514	570	com/google/f/x
    //   515	519	570	com/google/f/x
    //   521	525	570	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int j = -1;
    if (i != j) {
      return i;
    }
    Object localObject1 = a;
    j = 0;
    Object localObject2;
    if (localObject1 != null)
    {
      localObject2 = a();
      i = h.computeMessageSize(1, (ae)localObject2);
      j = 0 + i;
    }
    localObject1 = b;
    if (localObject1 != null)
    {
      localObject2 = b();
      i = h.computeMessageSize(2, (ae)localObject2);
      j += i;
    }
    i = c;
    if (i != 0)
    {
      int k = 3;
      i = h.computeInt32Size(k, i);
      j += i;
    }
    localObject1 = d;
    if (localObject1 != null)
    {
      localObject2 = d();
      i = h.computeMessageSize(4, (ae)localObject2);
      j += i;
    }
    memoizedSerializedSize = j;
    return j;
  }
  
  public final void writeTo(h paramh)
  {
    Object localObject1 = a;
    Object localObject2;
    if (localObject1 != null)
    {
      i = 1;
      localObject2 = a();
      paramh.writeMessage(i, (ae)localObject2);
    }
    localObject1 = b;
    if (localObject1 != null)
    {
      i = 2;
      localObject2 = b();
      paramh.writeMessage(i, (ae)localObject2);
    }
    int i = c;
    if (i != 0)
    {
      int j = 3;
      paramh.writeInt32(j, i);
    }
    localObject1 = d;
    if (localObject1 != null)
    {
      i = 4;
      localObject2 = d();
      paramh.writeMessage(i, (ae)localObject2);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.events.Event.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */