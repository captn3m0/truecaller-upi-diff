package com.truecaller.api.services.messenger.v1.events;

import com.google.f.ac;
import com.google.f.ad;
import com.google.f.ae;
import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;
import com.google.f.w.h;
import com.truecaller.api.services.messenger.v1.models.Peer;
import com.truecaller.api.services.messenger.v1.models.a;
import com.truecaller.api.services.messenger.v1.models.c;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class Event$n
  extends q
  implements Event.o
{
  private static final n n;
  private static volatile ah o;
  private int a;
  private Peer b;
  private Peer c;
  private String d;
  private int e;
  private w.h f;
  private w.h g;
  private c h;
  private ad i;
  private int j;
  private w.h k;
  private w.h l;
  private a m;
  
  static
  {
    n localn = new com/truecaller/api/services/messenger/v1/events/Event$n;
    localn.<init>();
    n = localn;
    localn.makeImmutable();
  }
  
  private Event$n()
  {
    Object localObject = ad.emptyMapField();
    i = ((ad)localObject);
    d = "";
    localObject = emptyProtobufList();
    f = ((w.h)localObject);
    localObject = emptyProtobufList();
    g = ((w.h)localObject);
    localObject = emptyProtobufList();
    k = ((w.h)localObject);
    localObject = emptyProtobufList();
    l = ((w.h)localObject);
  }
  
  public static n j()
  {
    return n;
  }
  
  public static ah k()
  {
    return n.getParserForType();
  }
  
  public final Peer a()
  {
    Peer localPeer = b;
    if (localPeer == null) {
      localPeer = Peer.d();
    }
    return localPeer;
  }
  
  public final Peer b()
  {
    Peer localPeer = c;
    if (localPeer == null) {
      localPeer = Peer.d();
    }
    return localPeer;
  }
  
  public final String c()
  {
    return d;
  }
  
  public final int d()
  {
    return e;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 84	com/truecaller/api/services/messenger/v1/events/Event$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 90	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_0
    //   19: istore 6
    //   21: aconst_null
    //   22: astore 4
    //   24: iconst_0
    //   25: istore 7
    //   27: iconst_1
    //   28: istore 8
    //   30: iload 5
    //   32: tableswitch	default:+48->80, 1:+1624->1656, 2:+1620->1652, 3:+1575->1607, 4:+1564->1596, 5:+1063->1095, 6:+110->142, 7:+1059->1091, 8:+58->90
    //   80: new 93	java/lang/UnsupportedOperationException
    //   83: astore_1
    //   84: aload_1
    //   85: invokespecial 94	java/lang/UnsupportedOperationException:<init>	()V
    //   88: aload_1
    //   89: athrow
    //   90: getstatic 96	com/truecaller/api/services/messenger/v1/events/Event$n:o	Lcom/google/f/ah;
    //   93: astore_1
    //   94: aload_1
    //   95: ifnonnull +43 -> 138
    //   98: ldc 2
    //   100: astore_1
    //   101: aload_1
    //   102: monitorenter
    //   103: getstatic 96	com/truecaller/api/services/messenger/v1/events/Event$n:o	Lcom/google/f/ah;
    //   106: astore_2
    //   107: aload_2
    //   108: ifnonnull +20 -> 128
    //   111: new 98	com/google/f/q$b
    //   114: astore_2
    //   115: getstatic 36	com/truecaller/api/services/messenger/v1/events/Event$n:n	Lcom/truecaller/api/services/messenger/v1/events/Event$n;
    //   118: astore_3
    //   119: aload_2
    //   120: aload_3
    //   121: invokespecial 101	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   124: aload_2
    //   125: putstatic 96	com/truecaller/api/services/messenger/v1/events/Event$n:o	Lcom/google/f/ah;
    //   128: aload_1
    //   129: monitorexit
    //   130: goto +8 -> 138
    //   133: astore_2
    //   134: aload_1
    //   135: monitorexit
    //   136: aload_2
    //   137: athrow
    //   138: getstatic 96	com/truecaller/api/services/messenger/v1/events/Event$n:o	Lcom/google/f/ah;
    //   141: areturn
    //   142: aload_2
    //   143: checkcast 103	com/google/f/g
    //   146: astore_2
    //   147: aload_3
    //   148: checkcast 105	com/google/f/n
    //   151: astore_3
    //   152: iload 7
    //   154: ifne +937 -> 1091
    //   157: aload_2
    //   158: invokevirtual 108	com/google/f/g:readTag	()I
    //   161: istore 5
    //   163: iload 5
    //   165: lookupswitch	default:+115->280, 0:+853->1018, 10:+759->924, 18:+665->830, 26:+652->817, 32:+637->802, 42:+573->738, 50:+509->674, 58:+415->580, 66:+363->528, 72:+348->513, 82:+284->449, 90:+220->385, 98:+126->291
    //   280: aload_2
    //   281: iload 5
    //   283: invokevirtual 112	com/google/f/g:skipField	(I)Z
    //   286: istore 5
    //   288: goto +736 -> 1024
    //   291: aload_0
    //   292: getfield 114	com/truecaller/api/services/messenger/v1/events/Event$n:m	Lcom/truecaller/api/services/messenger/v1/models/a;
    //   295: astore_1
    //   296: aload_1
    //   297: ifnull +21 -> 318
    //   300: aload_0
    //   301: getfield 114	com/truecaller/api/services/messenger/v1/events/Event$n:m	Lcom/truecaller/api/services/messenger/v1/models/a;
    //   304: astore_1
    //   305: aload_1
    //   306: invokevirtual 120	com/truecaller/api/services/messenger/v1/models/a:toBuilder	()Lcom/google/f/q$a;
    //   309: astore_1
    //   310: aload_1
    //   311: checkcast 122	com/truecaller/api/services/messenger/v1/models/a$a
    //   314: astore_1
    //   315: goto +8 -> 323
    //   318: iconst_0
    //   319: istore 5
    //   321: aconst_null
    //   322: astore_1
    //   323: invokestatic 124	com/truecaller/api/services/messenger/v1/models/a:f	()Lcom/google/f/ah;
    //   326: astore 9
    //   328: aload_2
    //   329: aload 9
    //   331: aload_3
    //   332: invokevirtual 128	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   335: astore 9
    //   337: aload 9
    //   339: checkcast 116	com/truecaller/api/services/messenger/v1/models/a
    //   342: astore 9
    //   344: aload_0
    //   345: aload 9
    //   347: putfield 114	com/truecaller/api/services/messenger/v1/events/Event$n:m	Lcom/truecaller/api/services/messenger/v1/models/a;
    //   350: aload_1
    //   351: ifnull -199 -> 152
    //   354: aload_0
    //   355: getfield 114	com/truecaller/api/services/messenger/v1/events/Event$n:m	Lcom/truecaller/api/services/messenger/v1/models/a;
    //   358: astore 9
    //   360: aload_1
    //   361: aload 9
    //   363: invokevirtual 132	com/truecaller/api/services/messenger/v1/models/a$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   366: pop
    //   367: aload_1
    //   368: invokevirtual 136	com/truecaller/api/services/messenger/v1/models/a$a:buildPartial	()Lcom/google/f/q;
    //   371: astore_1
    //   372: aload_1
    //   373: checkcast 116	com/truecaller/api/services/messenger/v1/models/a
    //   376: astore_1
    //   377: aload_0
    //   378: aload_1
    //   379: putfield 114	com/truecaller/api/services/messenger/v1/events/Event$n:m	Lcom/truecaller/api/services/messenger/v1/models/a;
    //   382: goto -230 -> 152
    //   385: aload_0
    //   386: getfield 64	com/truecaller/api/services/messenger/v1/events/Event$n:l	Lcom/google/f/w$h;
    //   389: astore_1
    //   390: aload_1
    //   391: invokeinterface 142 1 0
    //   396: istore 5
    //   398: iload 5
    //   400: ifne +18 -> 418
    //   403: aload_0
    //   404: getfield 64	com/truecaller/api/services/messenger/v1/events/Event$n:l	Lcom/google/f/w$h;
    //   407: astore_1
    //   408: aload_1
    //   409: invokestatic 146	com/google/f/q:mutableCopy	(Lcom/google/f/w$h;)Lcom/google/f/w$h;
    //   412: astore_1
    //   413: aload_0
    //   414: aload_1
    //   415: putfield 64	com/truecaller/api/services/messenger/v1/events/Event$n:l	Lcom/google/f/w$h;
    //   418: aload_0
    //   419: getfield 64	com/truecaller/api/services/messenger/v1/events/Event$n:l	Lcom/google/f/w$h;
    //   422: astore_1
    //   423: invokestatic 150	com/truecaller/api/services/messenger/v1/models/f:c	()Lcom/google/f/ah;
    //   426: astore 9
    //   428: aload_2
    //   429: aload 9
    //   431: aload_3
    //   432: invokevirtual 128	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   435: astore 9
    //   437: aload_1
    //   438: aload 9
    //   440: invokeinterface 154 2 0
    //   445: pop
    //   446: goto -294 -> 152
    //   449: aload_0
    //   450: getfield 62	com/truecaller/api/services/messenger/v1/events/Event$n:k	Lcom/google/f/w$h;
    //   453: astore_1
    //   454: aload_1
    //   455: invokeinterface 142 1 0
    //   460: istore 5
    //   462: iload 5
    //   464: ifne +18 -> 482
    //   467: aload_0
    //   468: getfield 62	com/truecaller/api/services/messenger/v1/events/Event$n:k	Lcom/google/f/w$h;
    //   471: astore_1
    //   472: aload_1
    //   473: invokestatic 146	com/google/f/q:mutableCopy	(Lcom/google/f/w$h;)Lcom/google/f/w$h;
    //   476: astore_1
    //   477: aload_0
    //   478: aload_1
    //   479: putfield 62	com/truecaller/api/services/messenger/v1/events/Event$n:k	Lcom/google/f/w$h;
    //   482: aload_0
    //   483: getfield 62	com/truecaller/api/services/messenger/v1/events/Event$n:k	Lcom/google/f/w$h;
    //   486: astore_1
    //   487: invokestatic 150	com/truecaller/api/services/messenger/v1/models/f:c	()Lcom/google/f/ah;
    //   490: astore 9
    //   492: aload_2
    //   493: aload 9
    //   495: aload_3
    //   496: invokevirtual 128	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   499: astore 9
    //   501: aload_1
    //   502: aload 9
    //   504: invokeinterface 154 2 0
    //   509: pop
    //   510: goto -358 -> 152
    //   513: aload_2
    //   514: invokevirtual 157	com/google/f/g:readInt32	()I
    //   517: istore 5
    //   519: aload_0
    //   520: iload 5
    //   522: putfield 159	com/truecaller/api/services/messenger/v1/events/Event$n:j	I
    //   525: goto -373 -> 152
    //   528: aload_0
    //   529: getfield 48	com/truecaller/api/services/messenger/v1/events/Event$n:i	Lcom/google/f/ad;
    //   532: astore_1
    //   533: aload_1
    //   534: invokevirtual 162	com/google/f/ad:isMutable	()Z
    //   537: istore 5
    //   539: iload 5
    //   541: ifne +18 -> 559
    //   544: aload_0
    //   545: getfield 48	com/truecaller/api/services/messenger/v1/events/Event$n:i	Lcom/google/f/ad;
    //   548: astore_1
    //   549: aload_1
    //   550: invokevirtual 164	com/google/f/ad:mutableCopy	()Lcom/google/f/ad;
    //   553: astore_1
    //   554: aload_0
    //   555: aload_1
    //   556: putfield 48	com/truecaller/api/services/messenger/v1/events/Event$n:i	Lcom/google/f/ad;
    //   559: getstatic 169	com/truecaller/api/services/messenger/v1/events/Event$n$b:a	Lcom/google/f/ac;
    //   562: astore_1
    //   563: aload_0
    //   564: getfield 48	com/truecaller/api/services/messenger/v1/events/Event$n:i	Lcom/google/f/ad;
    //   567: astore 9
    //   569: aload_1
    //   570: aload 9
    //   572: aload_2
    //   573: aload_3
    //   574: invokevirtual 175	com/google/f/ac:parseInto	(Lcom/google/f/ad;Lcom/google/f/g;Lcom/google/f/n;)V
    //   577: goto -425 -> 152
    //   580: aload_0
    //   581: getfield 177	com/truecaller/api/services/messenger/v1/events/Event$n:h	Lcom/truecaller/api/services/messenger/v1/models/c;
    //   584: astore_1
    //   585: aload_1
    //   586: ifnull +21 -> 607
    //   589: aload_0
    //   590: getfield 177	com/truecaller/api/services/messenger/v1/events/Event$n:h	Lcom/truecaller/api/services/messenger/v1/models/c;
    //   593: astore_1
    //   594: aload_1
    //   595: invokevirtual 180	com/truecaller/api/services/messenger/v1/models/c:toBuilder	()Lcom/google/f/q$a;
    //   598: astore_1
    //   599: aload_1
    //   600: checkcast 182	com/truecaller/api/services/messenger/v1/models/c$a
    //   603: astore_1
    //   604: goto +8 -> 612
    //   607: iconst_0
    //   608: istore 5
    //   610: aconst_null
    //   611: astore_1
    //   612: invokestatic 184	com/truecaller/api/services/messenger/v1/models/c:d	()Lcom/google/f/ah;
    //   615: astore 9
    //   617: aload_2
    //   618: aload 9
    //   620: aload_3
    //   621: invokevirtual 128	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   624: astore 9
    //   626: aload 9
    //   628: checkcast 179	com/truecaller/api/services/messenger/v1/models/c
    //   631: astore 9
    //   633: aload_0
    //   634: aload 9
    //   636: putfield 177	com/truecaller/api/services/messenger/v1/events/Event$n:h	Lcom/truecaller/api/services/messenger/v1/models/c;
    //   639: aload_1
    //   640: ifnull -488 -> 152
    //   643: aload_0
    //   644: getfield 177	com/truecaller/api/services/messenger/v1/events/Event$n:h	Lcom/truecaller/api/services/messenger/v1/models/c;
    //   647: astore 9
    //   649: aload_1
    //   650: aload 9
    //   652: invokevirtual 185	com/truecaller/api/services/messenger/v1/models/c$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   655: pop
    //   656: aload_1
    //   657: invokevirtual 186	com/truecaller/api/services/messenger/v1/models/c$a:buildPartial	()Lcom/google/f/q;
    //   660: astore_1
    //   661: aload_1
    //   662: checkcast 179	com/truecaller/api/services/messenger/v1/models/c
    //   665: astore_1
    //   666: aload_0
    //   667: aload_1
    //   668: putfield 177	com/truecaller/api/services/messenger/v1/events/Event$n:h	Lcom/truecaller/api/services/messenger/v1/models/c;
    //   671: goto -519 -> 152
    //   674: aload_0
    //   675: getfield 60	com/truecaller/api/services/messenger/v1/events/Event$n:g	Lcom/google/f/w$h;
    //   678: astore_1
    //   679: aload_1
    //   680: invokeinterface 142 1 0
    //   685: istore 5
    //   687: iload 5
    //   689: ifne +18 -> 707
    //   692: aload_0
    //   693: getfield 60	com/truecaller/api/services/messenger/v1/events/Event$n:g	Lcom/google/f/w$h;
    //   696: astore_1
    //   697: aload_1
    //   698: invokestatic 146	com/google/f/q:mutableCopy	(Lcom/google/f/w$h;)Lcom/google/f/w$h;
    //   701: astore_1
    //   702: aload_0
    //   703: aload_1
    //   704: putfield 60	com/truecaller/api/services/messenger/v1/events/Event$n:g	Lcom/google/f/w$h;
    //   707: aload_0
    //   708: getfield 60	com/truecaller/api/services/messenger/v1/events/Event$n:g	Lcom/google/f/w$h;
    //   711: astore_1
    //   712: invokestatic 188	com/truecaller/api/services/messenger/v1/models/Peer:e	()Lcom/google/f/ah;
    //   715: astore 9
    //   717: aload_2
    //   718: aload 9
    //   720: aload_3
    //   721: invokevirtual 128	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   724: astore 9
    //   726: aload_1
    //   727: aload 9
    //   729: invokeinterface 154 2 0
    //   734: pop
    //   735: goto -583 -> 152
    //   738: aload_0
    //   739: getfield 58	com/truecaller/api/services/messenger/v1/events/Event$n:f	Lcom/google/f/w$h;
    //   742: astore_1
    //   743: aload_1
    //   744: invokeinterface 142 1 0
    //   749: istore 5
    //   751: iload 5
    //   753: ifne +18 -> 771
    //   756: aload_0
    //   757: getfield 58	com/truecaller/api/services/messenger/v1/events/Event$n:f	Lcom/google/f/w$h;
    //   760: astore_1
    //   761: aload_1
    //   762: invokestatic 146	com/google/f/q:mutableCopy	(Lcom/google/f/w$h;)Lcom/google/f/w$h;
    //   765: astore_1
    //   766: aload_0
    //   767: aload_1
    //   768: putfield 58	com/truecaller/api/services/messenger/v1/events/Event$n:f	Lcom/google/f/w$h;
    //   771: aload_0
    //   772: getfield 58	com/truecaller/api/services/messenger/v1/events/Event$n:f	Lcom/google/f/w$h;
    //   775: astore_1
    //   776: invokestatic 188	com/truecaller/api/services/messenger/v1/models/Peer:e	()Lcom/google/f/ah;
    //   779: astore 9
    //   781: aload_2
    //   782: aload 9
    //   784: aload_3
    //   785: invokevirtual 128	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   788: astore 9
    //   790: aload_1
    //   791: aload 9
    //   793: invokeinterface 154 2 0
    //   798: pop
    //   799: goto -647 -> 152
    //   802: aload_2
    //   803: invokevirtual 157	com/google/f/g:readInt32	()I
    //   806: istore 5
    //   808: aload_0
    //   809: iload 5
    //   811: putfield 79	com/truecaller/api/services/messenger/v1/events/Event$n:e	I
    //   814: goto -662 -> 152
    //   817: aload_2
    //   818: invokevirtual 192	com/google/f/g:readStringRequireUtf8	()Ljava/lang/String;
    //   821: astore_1
    //   822: aload_0
    //   823: aload_1
    //   824: putfield 52	com/truecaller/api/services/messenger/v1/events/Event$n:d	Ljava/lang/String;
    //   827: goto -675 -> 152
    //   830: aload_0
    //   831: getfield 77	com/truecaller/api/services/messenger/v1/events/Event$n:c	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   834: astore_1
    //   835: aload_1
    //   836: ifnull +21 -> 857
    //   839: aload_0
    //   840: getfield 77	com/truecaller/api/services/messenger/v1/events/Event$n:c	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   843: astore_1
    //   844: aload_1
    //   845: invokevirtual 193	com/truecaller/api/services/messenger/v1/models/Peer:toBuilder	()Lcom/google/f/q$a;
    //   848: astore_1
    //   849: aload_1
    //   850: checkcast 195	com/truecaller/api/services/messenger/v1/models/Peer$a
    //   853: astore_1
    //   854: goto +8 -> 862
    //   857: iconst_0
    //   858: istore 5
    //   860: aconst_null
    //   861: astore_1
    //   862: invokestatic 188	com/truecaller/api/services/messenger/v1/models/Peer:e	()Lcom/google/f/ah;
    //   865: astore 9
    //   867: aload_2
    //   868: aload 9
    //   870: aload_3
    //   871: invokevirtual 128	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   874: astore 9
    //   876: aload 9
    //   878: checkcast 72	com/truecaller/api/services/messenger/v1/models/Peer
    //   881: astore 9
    //   883: aload_0
    //   884: aload 9
    //   886: putfield 77	com/truecaller/api/services/messenger/v1/events/Event$n:c	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   889: aload_1
    //   890: ifnull -738 -> 152
    //   893: aload_0
    //   894: getfield 77	com/truecaller/api/services/messenger/v1/events/Event$n:c	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   897: astore 9
    //   899: aload_1
    //   900: aload 9
    //   902: invokevirtual 196	com/truecaller/api/services/messenger/v1/models/Peer$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   905: pop
    //   906: aload_1
    //   907: invokevirtual 197	com/truecaller/api/services/messenger/v1/models/Peer$a:buildPartial	()Lcom/google/f/q;
    //   910: astore_1
    //   911: aload_1
    //   912: checkcast 72	com/truecaller/api/services/messenger/v1/models/Peer
    //   915: astore_1
    //   916: aload_0
    //   917: aload_1
    //   918: putfield 77	com/truecaller/api/services/messenger/v1/events/Event$n:c	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   921: goto -769 -> 152
    //   924: aload_0
    //   925: getfield 70	com/truecaller/api/services/messenger/v1/events/Event$n:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   928: astore_1
    //   929: aload_1
    //   930: ifnull +21 -> 951
    //   933: aload_0
    //   934: getfield 70	com/truecaller/api/services/messenger/v1/events/Event$n:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   937: astore_1
    //   938: aload_1
    //   939: invokevirtual 193	com/truecaller/api/services/messenger/v1/models/Peer:toBuilder	()Lcom/google/f/q$a;
    //   942: astore_1
    //   943: aload_1
    //   944: checkcast 195	com/truecaller/api/services/messenger/v1/models/Peer$a
    //   947: astore_1
    //   948: goto +8 -> 956
    //   951: iconst_0
    //   952: istore 5
    //   954: aconst_null
    //   955: astore_1
    //   956: invokestatic 188	com/truecaller/api/services/messenger/v1/models/Peer:e	()Lcom/google/f/ah;
    //   959: astore 9
    //   961: aload_2
    //   962: aload 9
    //   964: aload_3
    //   965: invokevirtual 128	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   968: astore 9
    //   970: aload 9
    //   972: checkcast 72	com/truecaller/api/services/messenger/v1/models/Peer
    //   975: astore 9
    //   977: aload_0
    //   978: aload 9
    //   980: putfield 70	com/truecaller/api/services/messenger/v1/events/Event$n:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   983: aload_1
    //   984: ifnull -832 -> 152
    //   987: aload_0
    //   988: getfield 70	com/truecaller/api/services/messenger/v1/events/Event$n:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   991: astore 9
    //   993: aload_1
    //   994: aload 9
    //   996: invokevirtual 196	com/truecaller/api/services/messenger/v1/models/Peer$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   999: pop
    //   1000: aload_1
    //   1001: invokevirtual 197	com/truecaller/api/services/messenger/v1/models/Peer$a:buildPartial	()Lcom/google/f/q;
    //   1004: astore_1
    //   1005: aload_1
    //   1006: checkcast 72	com/truecaller/api/services/messenger/v1/models/Peer
    //   1009: astore_1
    //   1010: aload_0
    //   1011: aload_1
    //   1012: putfield 70	com/truecaller/api/services/messenger/v1/events/Event$n:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   1015: goto -863 -> 152
    //   1018: iconst_1
    //   1019: istore 7
    //   1021: goto -869 -> 152
    //   1024: iload 5
    //   1026: ifne -874 -> 152
    //   1029: iconst_1
    //   1030: istore 7
    //   1032: goto -880 -> 152
    //   1035: astore_1
    //   1036: goto +53 -> 1089
    //   1039: astore_1
    //   1040: new 199	java/lang/RuntimeException
    //   1043: astore_2
    //   1044: new 201	com/google/f/x
    //   1047: astore_3
    //   1048: aload_1
    //   1049: invokevirtual 206	java/io/IOException:getMessage	()Ljava/lang/String;
    //   1052: astore_1
    //   1053: aload_3
    //   1054: aload_1
    //   1055: invokespecial 209	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   1058: aload_3
    //   1059: aload_0
    //   1060: invokevirtual 213	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   1063: astore_1
    //   1064: aload_2
    //   1065: aload_1
    //   1066: invokespecial 216	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   1069: aload_2
    //   1070: athrow
    //   1071: astore_1
    //   1072: new 199	java/lang/RuntimeException
    //   1075: astore_2
    //   1076: aload_1
    //   1077: aload_0
    //   1078: invokevirtual 213	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   1081: astore_1
    //   1082: aload_2
    //   1083: aload_1
    //   1084: invokespecial 216	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   1087: aload_2
    //   1088: athrow
    //   1089: aload_1
    //   1090: athrow
    //   1091: getstatic 36	com/truecaller/api/services/messenger/v1/events/Event$n:n	Lcom/truecaller/api/services/messenger/v1/events/Event$n;
    //   1094: areturn
    //   1095: aload_2
    //   1096: checkcast 218	com/google/f/q$k
    //   1099: astore_2
    //   1100: aload_3
    //   1101: checkcast 2	com/truecaller/api/services/messenger/v1/events/Event$n
    //   1104: astore_3
    //   1105: aload_0
    //   1106: getfield 70	com/truecaller/api/services/messenger/v1/events/Event$n:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   1109: astore_1
    //   1110: aload_3
    //   1111: getfield 70	com/truecaller/api/services/messenger/v1/events/Event$n:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   1114: astore 4
    //   1116: aload_2
    //   1117: aload_1
    //   1118: aload 4
    //   1120: invokeinterface 222 3 0
    //   1125: checkcast 72	com/truecaller/api/services/messenger/v1/models/Peer
    //   1128: astore_1
    //   1129: aload_0
    //   1130: aload_1
    //   1131: putfield 70	com/truecaller/api/services/messenger/v1/events/Event$n:b	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   1134: aload_0
    //   1135: getfield 77	com/truecaller/api/services/messenger/v1/events/Event$n:c	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   1138: astore_1
    //   1139: aload_3
    //   1140: getfield 77	com/truecaller/api/services/messenger/v1/events/Event$n:c	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   1143: astore 4
    //   1145: aload_2
    //   1146: aload_1
    //   1147: aload 4
    //   1149: invokeinterface 222 3 0
    //   1154: checkcast 72	com/truecaller/api/services/messenger/v1/models/Peer
    //   1157: astore_1
    //   1158: aload_0
    //   1159: aload_1
    //   1160: putfield 77	com/truecaller/api/services/messenger/v1/events/Event$n:c	Lcom/truecaller/api/services/messenger/v1/models/Peer;
    //   1163: aload_0
    //   1164: getfield 52	com/truecaller/api/services/messenger/v1/events/Event$n:d	Ljava/lang/String;
    //   1167: invokevirtual 227	java/lang/String:isEmpty	()Z
    //   1170: iload 8
    //   1172: ixor
    //   1173: istore 5
    //   1175: aload_0
    //   1176: getfield 52	com/truecaller/api/services/messenger/v1/events/Event$n:d	Ljava/lang/String;
    //   1179: astore 4
    //   1181: aload_3
    //   1182: getfield 52	com/truecaller/api/services/messenger/v1/events/Event$n:d	Ljava/lang/String;
    //   1185: astore 9
    //   1187: aload 9
    //   1189: invokevirtual 227	java/lang/String:isEmpty	()Z
    //   1192: iload 8
    //   1194: ixor
    //   1195: istore 10
    //   1197: aload_3
    //   1198: getfield 52	com/truecaller/api/services/messenger/v1/events/Event$n:d	Ljava/lang/String;
    //   1201: astore 11
    //   1203: aload_2
    //   1204: iload 5
    //   1206: aload 4
    //   1208: iload 10
    //   1210: aload 11
    //   1212: invokeinterface 231 5 0
    //   1217: astore_1
    //   1218: aload_0
    //   1219: aload_1
    //   1220: putfield 52	com/truecaller/api/services/messenger/v1/events/Event$n:d	Ljava/lang/String;
    //   1223: aload_0
    //   1224: getfield 79	com/truecaller/api/services/messenger/v1/events/Event$n:e	I
    //   1227: istore 5
    //   1229: iload 5
    //   1231: ifeq +9 -> 1240
    //   1234: iconst_1
    //   1235: istore 5
    //   1237: goto +8 -> 1245
    //   1240: iconst_0
    //   1241: istore 5
    //   1243: aconst_null
    //   1244: astore_1
    //   1245: aload_0
    //   1246: getfield 79	com/truecaller/api/services/messenger/v1/events/Event$n:e	I
    //   1249: istore 6
    //   1251: aload_3
    //   1252: getfield 79	com/truecaller/api/services/messenger/v1/events/Event$n:e	I
    //   1255: istore 10
    //   1257: iload 10
    //   1259: ifeq +9 -> 1268
    //   1262: iconst_1
    //   1263: istore 10
    //   1265: goto +9 -> 1274
    //   1268: iconst_0
    //   1269: istore 10
    //   1271: aconst_null
    //   1272: astore 9
    //   1274: aload_3
    //   1275: getfield 79	com/truecaller/api/services/messenger/v1/events/Event$n:e	I
    //   1278: istore 12
    //   1280: aload_2
    //   1281: iload 5
    //   1283: iload 6
    //   1285: iload 10
    //   1287: iload 12
    //   1289: invokeinterface 235 5 0
    //   1294: istore 5
    //   1296: aload_0
    //   1297: iload 5
    //   1299: putfield 79	com/truecaller/api/services/messenger/v1/events/Event$n:e	I
    //   1302: aload_0
    //   1303: getfield 58	com/truecaller/api/services/messenger/v1/events/Event$n:f	Lcom/google/f/w$h;
    //   1306: astore_1
    //   1307: aload_3
    //   1308: getfield 58	com/truecaller/api/services/messenger/v1/events/Event$n:f	Lcom/google/f/w$h;
    //   1311: astore 4
    //   1313: aload_2
    //   1314: aload_1
    //   1315: aload 4
    //   1317: invokeinterface 239 3 0
    //   1322: astore_1
    //   1323: aload_0
    //   1324: aload_1
    //   1325: putfield 58	com/truecaller/api/services/messenger/v1/events/Event$n:f	Lcom/google/f/w$h;
    //   1328: aload_0
    //   1329: getfield 60	com/truecaller/api/services/messenger/v1/events/Event$n:g	Lcom/google/f/w$h;
    //   1332: astore_1
    //   1333: aload_3
    //   1334: getfield 60	com/truecaller/api/services/messenger/v1/events/Event$n:g	Lcom/google/f/w$h;
    //   1337: astore 4
    //   1339: aload_2
    //   1340: aload_1
    //   1341: aload 4
    //   1343: invokeinterface 239 3 0
    //   1348: astore_1
    //   1349: aload_0
    //   1350: aload_1
    //   1351: putfield 60	com/truecaller/api/services/messenger/v1/events/Event$n:g	Lcom/google/f/w$h;
    //   1354: aload_0
    //   1355: getfield 177	com/truecaller/api/services/messenger/v1/events/Event$n:h	Lcom/truecaller/api/services/messenger/v1/models/c;
    //   1358: astore_1
    //   1359: aload_3
    //   1360: getfield 177	com/truecaller/api/services/messenger/v1/events/Event$n:h	Lcom/truecaller/api/services/messenger/v1/models/c;
    //   1363: astore 4
    //   1365: aload_2
    //   1366: aload_1
    //   1367: aload 4
    //   1369: invokeinterface 222 3 0
    //   1374: checkcast 179	com/truecaller/api/services/messenger/v1/models/c
    //   1377: astore_1
    //   1378: aload_0
    //   1379: aload_1
    //   1380: putfield 177	com/truecaller/api/services/messenger/v1/events/Event$n:h	Lcom/truecaller/api/services/messenger/v1/models/c;
    //   1383: aload_0
    //   1384: getfield 48	com/truecaller/api/services/messenger/v1/events/Event$n:i	Lcom/google/f/ad;
    //   1387: astore_1
    //   1388: aload_3
    //   1389: getfield 48	com/truecaller/api/services/messenger/v1/events/Event$n:i	Lcom/google/f/ad;
    //   1392: astore 4
    //   1394: aload_2
    //   1395: aload_1
    //   1396: aload 4
    //   1398: invokeinterface 243 3 0
    //   1403: astore_1
    //   1404: aload_0
    //   1405: aload_1
    //   1406: putfield 48	com/truecaller/api/services/messenger/v1/events/Event$n:i	Lcom/google/f/ad;
    //   1409: aload_0
    //   1410: getfield 159	com/truecaller/api/services/messenger/v1/events/Event$n:j	I
    //   1413: istore 5
    //   1415: iload 5
    //   1417: ifeq +9 -> 1426
    //   1420: iconst_1
    //   1421: istore 5
    //   1423: goto +8 -> 1431
    //   1426: iconst_0
    //   1427: istore 5
    //   1429: aconst_null
    //   1430: astore_1
    //   1431: aload_0
    //   1432: getfield 159	com/truecaller/api/services/messenger/v1/events/Event$n:j	I
    //   1435: istore 6
    //   1437: aload_3
    //   1438: getfield 159	com/truecaller/api/services/messenger/v1/events/Event$n:j	I
    //   1441: istore 10
    //   1443: iload 10
    //   1445: ifeq +6 -> 1451
    //   1448: iconst_1
    //   1449: istore 7
    //   1451: aload_3
    //   1452: getfield 159	com/truecaller/api/services/messenger/v1/events/Event$n:j	I
    //   1455: istore 8
    //   1457: aload_2
    //   1458: iload 5
    //   1460: iload 6
    //   1462: iload 7
    //   1464: iload 8
    //   1466: invokeinterface 235 5 0
    //   1471: istore 5
    //   1473: aload_0
    //   1474: iload 5
    //   1476: putfield 159	com/truecaller/api/services/messenger/v1/events/Event$n:j	I
    //   1479: aload_0
    //   1480: getfield 62	com/truecaller/api/services/messenger/v1/events/Event$n:k	Lcom/google/f/w$h;
    //   1483: astore_1
    //   1484: aload_3
    //   1485: getfield 62	com/truecaller/api/services/messenger/v1/events/Event$n:k	Lcom/google/f/w$h;
    //   1488: astore 4
    //   1490: aload_2
    //   1491: aload_1
    //   1492: aload 4
    //   1494: invokeinterface 239 3 0
    //   1499: astore_1
    //   1500: aload_0
    //   1501: aload_1
    //   1502: putfield 62	com/truecaller/api/services/messenger/v1/events/Event$n:k	Lcom/google/f/w$h;
    //   1505: aload_0
    //   1506: getfield 64	com/truecaller/api/services/messenger/v1/events/Event$n:l	Lcom/google/f/w$h;
    //   1509: astore_1
    //   1510: aload_3
    //   1511: getfield 64	com/truecaller/api/services/messenger/v1/events/Event$n:l	Lcom/google/f/w$h;
    //   1514: astore 4
    //   1516: aload_2
    //   1517: aload_1
    //   1518: aload 4
    //   1520: invokeinterface 239 3 0
    //   1525: astore_1
    //   1526: aload_0
    //   1527: aload_1
    //   1528: putfield 64	com/truecaller/api/services/messenger/v1/events/Event$n:l	Lcom/google/f/w$h;
    //   1531: aload_0
    //   1532: getfield 114	com/truecaller/api/services/messenger/v1/events/Event$n:m	Lcom/truecaller/api/services/messenger/v1/models/a;
    //   1535: astore_1
    //   1536: aload_3
    //   1537: getfield 114	com/truecaller/api/services/messenger/v1/events/Event$n:m	Lcom/truecaller/api/services/messenger/v1/models/a;
    //   1540: astore 4
    //   1542: aload_2
    //   1543: aload_1
    //   1544: aload 4
    //   1546: invokeinterface 222 3 0
    //   1551: checkcast 116	com/truecaller/api/services/messenger/v1/models/a
    //   1554: astore_1
    //   1555: aload_0
    //   1556: aload_1
    //   1557: putfield 114	com/truecaller/api/services/messenger/v1/events/Event$n:m	Lcom/truecaller/api/services/messenger/v1/models/a;
    //   1560: getstatic 249	com/google/f/q$i:INSTANCE	Lcom/google/f/q$i;
    //   1563: astore_1
    //   1564: aload_2
    //   1565: aload_1
    //   1566: if_acmpne +28 -> 1594
    //   1569: aload_0
    //   1570: getfield 251	com/truecaller/api/services/messenger/v1/events/Event$n:a	I
    //   1573: istore 5
    //   1575: aload_3
    //   1576: getfield 251	com/truecaller/api/services/messenger/v1/events/Event$n:a	I
    //   1579: istore 13
    //   1581: iload 5
    //   1583: iload 13
    //   1585: ior
    //   1586: istore 5
    //   1588: aload_0
    //   1589: iload 5
    //   1591: putfield 251	com/truecaller/api/services/messenger/v1/events/Event$n:a	I
    //   1594: aload_0
    //   1595: areturn
    //   1596: new 253	com/truecaller/api/services/messenger/v1/events/Event$n$a
    //   1599: astore_1
    //   1600: aload_1
    //   1601: iconst_0
    //   1602: invokespecial 256	com/truecaller/api/services/messenger/v1/events/Event$n$a:<init>	(B)V
    //   1605: aload_1
    //   1606: areturn
    //   1607: aload_0
    //   1608: getfield 58	com/truecaller/api/services/messenger/v1/events/Event$n:f	Lcom/google/f/w$h;
    //   1611: invokeinterface 257 1 0
    //   1616: aload_0
    //   1617: getfield 60	com/truecaller/api/services/messenger/v1/events/Event$n:g	Lcom/google/f/w$h;
    //   1620: invokeinterface 257 1 0
    //   1625: aload_0
    //   1626: getfield 48	com/truecaller/api/services/messenger/v1/events/Event$n:i	Lcom/google/f/ad;
    //   1629: invokevirtual 258	com/google/f/ad:makeImmutable	()V
    //   1632: aload_0
    //   1633: getfield 62	com/truecaller/api/services/messenger/v1/events/Event$n:k	Lcom/google/f/w$h;
    //   1636: invokeinterface 257 1 0
    //   1641: aload_0
    //   1642: getfield 64	com/truecaller/api/services/messenger/v1/events/Event$n:l	Lcom/google/f/w$h;
    //   1645: invokeinterface 257 1 0
    //   1650: aconst_null
    //   1651: areturn
    //   1652: getstatic 36	com/truecaller/api/services/messenger/v1/events/Event$n:n	Lcom/truecaller/api/services/messenger/v1/events/Event$n;
    //   1655: areturn
    //   1656: new 2	com/truecaller/api/services/messenger/v1/events/Event$n
    //   1659: astore_1
    //   1660: aload_1
    //   1661: invokespecial 34	com/truecaller/api/services/messenger/v1/events/Event$n:<init>	()V
    //   1664: aload_1
    //   1665: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1666	0	this	n
    //   0	1666	1	paramj	com.google.f.q.j
    //   0	1666	2	paramObject1	Object
    //   0	1666	3	paramObject2	Object
    //   3	1542	4	localObject1	Object
    //   9	273	5	i1	int
    //   286	177	5	bool1	boolean
    //   517	4	5	i2	int
    //   537	215	5	bool2	boolean
    //   806	219	5	i3	int
    //   1173	32	5	bool3	boolean
    //   1227	55	5	i4	int
    //   1294	165	5	i5	int
    //   1471	119	5	i6	int
    //   19	1442	6	i7	int
    //   25	1438	7	bool4	boolean
    //   28	1437	8	i8	int
    //   326	947	9	localObject2	Object
    //   1195	14	10	bool5	boolean
    //   1255	31	10	i9	int
    //   1441	3	10	i10	int
    //   1201	10	11	str	String
    //   1278	10	12	i11	int
    //   1579	7	13	i12	int
    // Exception table:
    //   from	to	target	type
    //   103	106	133	finally
    //   111	114	133	finally
    //   115	118	133	finally
    //   120	124	133	finally
    //   124	128	133	finally
    //   128	130	133	finally
    //   134	136	133	finally
    //   157	161	1035	finally
    //   281	286	1035	finally
    //   291	295	1035	finally
    //   300	304	1035	finally
    //   305	309	1035	finally
    //   310	314	1035	finally
    //   323	326	1035	finally
    //   331	335	1035	finally
    //   337	342	1035	finally
    //   345	350	1035	finally
    //   354	358	1035	finally
    //   361	367	1035	finally
    //   367	371	1035	finally
    //   372	376	1035	finally
    //   378	382	1035	finally
    //   385	389	1035	finally
    //   390	396	1035	finally
    //   403	407	1035	finally
    //   408	412	1035	finally
    //   414	418	1035	finally
    //   418	422	1035	finally
    //   423	426	1035	finally
    //   431	435	1035	finally
    //   438	446	1035	finally
    //   449	453	1035	finally
    //   454	460	1035	finally
    //   467	471	1035	finally
    //   472	476	1035	finally
    //   478	482	1035	finally
    //   482	486	1035	finally
    //   487	490	1035	finally
    //   495	499	1035	finally
    //   502	510	1035	finally
    //   513	517	1035	finally
    //   520	525	1035	finally
    //   528	532	1035	finally
    //   533	537	1035	finally
    //   544	548	1035	finally
    //   549	553	1035	finally
    //   555	559	1035	finally
    //   559	562	1035	finally
    //   563	567	1035	finally
    //   573	577	1035	finally
    //   580	584	1035	finally
    //   589	593	1035	finally
    //   594	598	1035	finally
    //   599	603	1035	finally
    //   612	615	1035	finally
    //   620	624	1035	finally
    //   626	631	1035	finally
    //   634	639	1035	finally
    //   643	647	1035	finally
    //   650	656	1035	finally
    //   656	660	1035	finally
    //   661	665	1035	finally
    //   667	671	1035	finally
    //   674	678	1035	finally
    //   679	685	1035	finally
    //   692	696	1035	finally
    //   697	701	1035	finally
    //   703	707	1035	finally
    //   707	711	1035	finally
    //   712	715	1035	finally
    //   720	724	1035	finally
    //   727	735	1035	finally
    //   738	742	1035	finally
    //   743	749	1035	finally
    //   756	760	1035	finally
    //   761	765	1035	finally
    //   767	771	1035	finally
    //   771	775	1035	finally
    //   776	779	1035	finally
    //   784	788	1035	finally
    //   791	799	1035	finally
    //   802	806	1035	finally
    //   809	814	1035	finally
    //   817	821	1035	finally
    //   823	827	1035	finally
    //   830	834	1035	finally
    //   839	843	1035	finally
    //   844	848	1035	finally
    //   849	853	1035	finally
    //   862	865	1035	finally
    //   870	874	1035	finally
    //   876	881	1035	finally
    //   884	889	1035	finally
    //   893	897	1035	finally
    //   900	906	1035	finally
    //   906	910	1035	finally
    //   911	915	1035	finally
    //   917	921	1035	finally
    //   924	928	1035	finally
    //   933	937	1035	finally
    //   938	942	1035	finally
    //   943	947	1035	finally
    //   956	959	1035	finally
    //   964	968	1035	finally
    //   970	975	1035	finally
    //   978	983	1035	finally
    //   987	991	1035	finally
    //   994	1000	1035	finally
    //   1000	1004	1035	finally
    //   1005	1009	1035	finally
    //   1011	1015	1035	finally
    //   1040	1043	1035	finally
    //   1044	1047	1035	finally
    //   1048	1052	1035	finally
    //   1054	1058	1035	finally
    //   1059	1063	1035	finally
    //   1065	1069	1035	finally
    //   1069	1071	1035	finally
    //   1072	1075	1035	finally
    //   1077	1081	1035	finally
    //   1083	1087	1035	finally
    //   1087	1089	1035	finally
    //   157	161	1039	java/io/IOException
    //   281	286	1039	java/io/IOException
    //   291	295	1039	java/io/IOException
    //   300	304	1039	java/io/IOException
    //   305	309	1039	java/io/IOException
    //   310	314	1039	java/io/IOException
    //   323	326	1039	java/io/IOException
    //   331	335	1039	java/io/IOException
    //   337	342	1039	java/io/IOException
    //   345	350	1039	java/io/IOException
    //   354	358	1039	java/io/IOException
    //   361	367	1039	java/io/IOException
    //   367	371	1039	java/io/IOException
    //   372	376	1039	java/io/IOException
    //   378	382	1039	java/io/IOException
    //   385	389	1039	java/io/IOException
    //   390	396	1039	java/io/IOException
    //   403	407	1039	java/io/IOException
    //   408	412	1039	java/io/IOException
    //   414	418	1039	java/io/IOException
    //   418	422	1039	java/io/IOException
    //   423	426	1039	java/io/IOException
    //   431	435	1039	java/io/IOException
    //   438	446	1039	java/io/IOException
    //   449	453	1039	java/io/IOException
    //   454	460	1039	java/io/IOException
    //   467	471	1039	java/io/IOException
    //   472	476	1039	java/io/IOException
    //   478	482	1039	java/io/IOException
    //   482	486	1039	java/io/IOException
    //   487	490	1039	java/io/IOException
    //   495	499	1039	java/io/IOException
    //   502	510	1039	java/io/IOException
    //   513	517	1039	java/io/IOException
    //   520	525	1039	java/io/IOException
    //   528	532	1039	java/io/IOException
    //   533	537	1039	java/io/IOException
    //   544	548	1039	java/io/IOException
    //   549	553	1039	java/io/IOException
    //   555	559	1039	java/io/IOException
    //   559	562	1039	java/io/IOException
    //   563	567	1039	java/io/IOException
    //   573	577	1039	java/io/IOException
    //   580	584	1039	java/io/IOException
    //   589	593	1039	java/io/IOException
    //   594	598	1039	java/io/IOException
    //   599	603	1039	java/io/IOException
    //   612	615	1039	java/io/IOException
    //   620	624	1039	java/io/IOException
    //   626	631	1039	java/io/IOException
    //   634	639	1039	java/io/IOException
    //   643	647	1039	java/io/IOException
    //   650	656	1039	java/io/IOException
    //   656	660	1039	java/io/IOException
    //   661	665	1039	java/io/IOException
    //   667	671	1039	java/io/IOException
    //   674	678	1039	java/io/IOException
    //   679	685	1039	java/io/IOException
    //   692	696	1039	java/io/IOException
    //   697	701	1039	java/io/IOException
    //   703	707	1039	java/io/IOException
    //   707	711	1039	java/io/IOException
    //   712	715	1039	java/io/IOException
    //   720	724	1039	java/io/IOException
    //   727	735	1039	java/io/IOException
    //   738	742	1039	java/io/IOException
    //   743	749	1039	java/io/IOException
    //   756	760	1039	java/io/IOException
    //   761	765	1039	java/io/IOException
    //   767	771	1039	java/io/IOException
    //   771	775	1039	java/io/IOException
    //   776	779	1039	java/io/IOException
    //   784	788	1039	java/io/IOException
    //   791	799	1039	java/io/IOException
    //   802	806	1039	java/io/IOException
    //   809	814	1039	java/io/IOException
    //   817	821	1039	java/io/IOException
    //   823	827	1039	java/io/IOException
    //   830	834	1039	java/io/IOException
    //   839	843	1039	java/io/IOException
    //   844	848	1039	java/io/IOException
    //   849	853	1039	java/io/IOException
    //   862	865	1039	java/io/IOException
    //   870	874	1039	java/io/IOException
    //   876	881	1039	java/io/IOException
    //   884	889	1039	java/io/IOException
    //   893	897	1039	java/io/IOException
    //   900	906	1039	java/io/IOException
    //   906	910	1039	java/io/IOException
    //   911	915	1039	java/io/IOException
    //   917	921	1039	java/io/IOException
    //   924	928	1039	java/io/IOException
    //   933	937	1039	java/io/IOException
    //   938	942	1039	java/io/IOException
    //   943	947	1039	java/io/IOException
    //   956	959	1039	java/io/IOException
    //   964	968	1039	java/io/IOException
    //   970	975	1039	java/io/IOException
    //   978	983	1039	java/io/IOException
    //   987	991	1039	java/io/IOException
    //   994	1000	1039	java/io/IOException
    //   1000	1004	1039	java/io/IOException
    //   1005	1009	1039	java/io/IOException
    //   1011	1015	1039	java/io/IOException
    //   157	161	1071	com/google/f/x
    //   281	286	1071	com/google/f/x
    //   291	295	1071	com/google/f/x
    //   300	304	1071	com/google/f/x
    //   305	309	1071	com/google/f/x
    //   310	314	1071	com/google/f/x
    //   323	326	1071	com/google/f/x
    //   331	335	1071	com/google/f/x
    //   337	342	1071	com/google/f/x
    //   345	350	1071	com/google/f/x
    //   354	358	1071	com/google/f/x
    //   361	367	1071	com/google/f/x
    //   367	371	1071	com/google/f/x
    //   372	376	1071	com/google/f/x
    //   378	382	1071	com/google/f/x
    //   385	389	1071	com/google/f/x
    //   390	396	1071	com/google/f/x
    //   403	407	1071	com/google/f/x
    //   408	412	1071	com/google/f/x
    //   414	418	1071	com/google/f/x
    //   418	422	1071	com/google/f/x
    //   423	426	1071	com/google/f/x
    //   431	435	1071	com/google/f/x
    //   438	446	1071	com/google/f/x
    //   449	453	1071	com/google/f/x
    //   454	460	1071	com/google/f/x
    //   467	471	1071	com/google/f/x
    //   472	476	1071	com/google/f/x
    //   478	482	1071	com/google/f/x
    //   482	486	1071	com/google/f/x
    //   487	490	1071	com/google/f/x
    //   495	499	1071	com/google/f/x
    //   502	510	1071	com/google/f/x
    //   513	517	1071	com/google/f/x
    //   520	525	1071	com/google/f/x
    //   528	532	1071	com/google/f/x
    //   533	537	1071	com/google/f/x
    //   544	548	1071	com/google/f/x
    //   549	553	1071	com/google/f/x
    //   555	559	1071	com/google/f/x
    //   559	562	1071	com/google/f/x
    //   563	567	1071	com/google/f/x
    //   573	577	1071	com/google/f/x
    //   580	584	1071	com/google/f/x
    //   589	593	1071	com/google/f/x
    //   594	598	1071	com/google/f/x
    //   599	603	1071	com/google/f/x
    //   612	615	1071	com/google/f/x
    //   620	624	1071	com/google/f/x
    //   626	631	1071	com/google/f/x
    //   634	639	1071	com/google/f/x
    //   643	647	1071	com/google/f/x
    //   650	656	1071	com/google/f/x
    //   656	660	1071	com/google/f/x
    //   661	665	1071	com/google/f/x
    //   667	671	1071	com/google/f/x
    //   674	678	1071	com/google/f/x
    //   679	685	1071	com/google/f/x
    //   692	696	1071	com/google/f/x
    //   697	701	1071	com/google/f/x
    //   703	707	1071	com/google/f/x
    //   707	711	1071	com/google/f/x
    //   712	715	1071	com/google/f/x
    //   720	724	1071	com/google/f/x
    //   727	735	1071	com/google/f/x
    //   738	742	1071	com/google/f/x
    //   743	749	1071	com/google/f/x
    //   756	760	1071	com/google/f/x
    //   761	765	1071	com/google/f/x
    //   767	771	1071	com/google/f/x
    //   771	775	1071	com/google/f/x
    //   776	779	1071	com/google/f/x
    //   784	788	1071	com/google/f/x
    //   791	799	1071	com/google/f/x
    //   802	806	1071	com/google/f/x
    //   809	814	1071	com/google/f/x
    //   817	821	1071	com/google/f/x
    //   823	827	1071	com/google/f/x
    //   830	834	1071	com/google/f/x
    //   839	843	1071	com/google/f/x
    //   844	848	1071	com/google/f/x
    //   849	853	1071	com/google/f/x
    //   862	865	1071	com/google/f/x
    //   870	874	1071	com/google/f/x
    //   876	881	1071	com/google/f/x
    //   884	889	1071	com/google/f/x
    //   893	897	1071	com/google/f/x
    //   900	906	1071	com/google/f/x
    //   906	910	1071	com/google/f/x
    //   911	915	1071	com/google/f/x
    //   917	921	1071	com/google/f/x
    //   924	928	1071	com/google/f/x
    //   933	937	1071	com/google/f/x
    //   938	942	1071	com/google/f/x
    //   943	947	1071	com/google/f/x
    //   956	959	1071	com/google/f/x
    //   964	968	1071	com/google/f/x
    //   970	975	1071	com/google/f/x
    //   978	983	1071	com/google/f/x
    //   987	991	1071	com/google/f/x
    //   994	1000	1071	com/google/f/x
    //   1000	1004	1071	com/google/f/x
    //   1005	1009	1071	com/google/f/x
    //   1011	1015	1071	com/google/f/x
  }
  
  public final c e()
  {
    c localc = h;
    if (localc == null) {
      localc = c.c();
    }
    return localc;
  }
  
  public final Map f()
  {
    return Collections.unmodifiableMap(i);
  }
  
  public final List g()
  {
    return k;
  }
  
  public final int getSerializedSize()
  {
    int i1 = memoizedSerializedSize;
    int i2 = -1;
    if (i1 != i2) {
      return i1;
    }
    Object localObject1 = b;
    i2 = 0;
    a locala = null;
    int i3;
    if (localObject1 != null)
    {
      localObject1 = a();
      i3 = 1;
      i1 = h.computeMessageSize(i3, (ae)localObject1) + 0;
    }
    else
    {
      i1 = 0;
      localObject1 = null;
    }
    Object localObject2 = c;
    Object localObject3;
    if (localObject2 != null)
    {
      localObject3 = b();
      i3 = h.computeMessageSize(2, (ae)localObject3);
      i1 += i3;
    }
    localObject2 = d;
    boolean bool1 = ((String)localObject2).isEmpty();
    if (!bool1)
    {
      localObject3 = d;
      i4 = h.computeStringSize(3, (String)localObject3);
      i1 += i4;
    }
    int i4 = e;
    int i5;
    if (i4 != 0)
    {
      i5 = 4;
      i4 = h.computeInt32Size(i5, i4);
      i1 += i4;
    }
    i4 = i1;
    i1 = 0;
    localObject1 = null;
    Object localObject4;
    for (;;)
    {
      localObject3 = f;
      i5 = ((w.h)localObject3).size();
      if (i1 >= i5) {
        break;
      }
      localObject4 = (ae)f.get(i1);
      i5 = h.computeMessageSize(5, (ae)localObject4);
      i4 += i5;
      i1 += 1;
    }
    i1 = 0;
    localObject1 = null;
    for (;;)
    {
      localObject3 = g;
      i5 = ((w.h)localObject3).size();
      if (i1 >= i5) {
        break;
      }
      localObject4 = (ae)g.get(i1);
      i5 = h.computeMessageSize(6, (ae)localObject4);
      i4 += i5;
      i1 += 1;
    }
    localObject1 = h;
    if (localObject1 != null)
    {
      localObject3 = e();
      i1 = h.computeMessageSize(7, (ae)localObject3);
      i4 += i1;
    }
    localObject1 = i.entrySet().iterator();
    int i6;
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject1).hasNext();
      if (!bool2) {
        break;
      }
      localObject3 = (Map.Entry)((Iterator)localObject1).next();
      localObject4 = Event.n.b.a;
      int i7 = 8;
      Object localObject5 = ((Map.Entry)localObject3).getKey();
      localObject3 = ((Map.Entry)localObject3).getValue();
      i6 = ((ac)localObject4).computeMessageSize(i7, localObject5, localObject3);
      i4 += i6;
    }
    i1 = j;
    if (i1 != 0)
    {
      i6 = 9;
      i1 = h.computeInt32Size(i6, i1);
      i4 += i1;
    }
    i1 = 0;
    localObject1 = null;
    for (;;)
    {
      localObject3 = k;
      i6 = ((w.h)localObject3).size();
      if (i1 >= i6) {
        break;
      }
      localObject4 = (ae)k.get(i1);
      i6 = h.computeMessageSize(10, (ae)localObject4);
      i4 += i6;
      i1 += 1;
    }
    for (;;)
    {
      localObject1 = l;
      i1 = ((w.h)localObject1).size();
      if (i2 >= i1) {
        break;
      }
      localObject3 = (ae)l.get(i2);
      i1 = h.computeMessageSize(11, (ae)localObject3);
      i4 += i1;
      i2 += 1;
    }
    localObject1 = m;
    if (localObject1 != null)
    {
      locala = i();
      i1 = h.computeMessageSize(12, locala);
      i4 += i1;
    }
    memoizedSerializedSize = i4;
    return i4;
  }
  
  public final List h()
  {
    return l;
  }
  
  public final a i()
  {
    a locala = m;
    if (locala == null) {
      locala = a.e();
    }
    return locala;
  }
  
  public final void writeTo(h paramh)
  {
    Object localObject1 = b;
    if (localObject1 != null)
    {
      localObject1 = a();
      i1 = 1;
      paramh.writeMessage(i1, (ae)localObject1);
    }
    localObject1 = c;
    if (localObject1 != null)
    {
      int i2 = 2;
      localObject2 = b();
      paramh.writeMessage(i2, (ae)localObject2);
    }
    localObject1 = d;
    boolean bool1 = ((String)localObject1).isEmpty();
    if (!bool1)
    {
      i3 = 3;
      localObject2 = d;
      paramh.writeString(i3, (String)localObject2);
    }
    int i3 = e;
    if (i3 != 0)
    {
      i1 = 4;
      paramh.writeInt32(i1, i3);
    }
    i3 = 0;
    localObject1 = null;
    int i1 = 0;
    Object localObject2 = null;
    Object localObject3;
    int i4;
    Object localObject4;
    for (;;)
    {
      localObject3 = f;
      i4 = ((w.h)localObject3).size();
      if (i1 >= i4) {
        break;
      }
      i4 = 5;
      localObject4 = (ae)f.get(i1);
      paramh.writeMessage(i4, (ae)localObject4);
      i1 += 1;
    }
    i1 = 0;
    localObject2 = null;
    for (;;)
    {
      localObject3 = g;
      i4 = ((w.h)localObject3).size();
      if (i1 >= i4) {
        break;
      }
      i4 = 6;
      localObject4 = (ae)g.get(i1);
      paramh.writeMessage(i4, (ae)localObject4);
      i1 += 1;
    }
    localObject2 = h;
    if (localObject2 != null)
    {
      i1 = 7;
      localObject3 = e();
      paramh.writeMessage(i1, (ae)localObject3);
    }
    localObject2 = i.entrySet().iterator();
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject2).hasNext();
      if (!bool2) {
        break;
      }
      localObject3 = (Map.Entry)((Iterator)localObject2).next();
      localObject4 = Event.n.b.a;
      int i6 = 8;
      Object localObject5 = ((Map.Entry)localObject3).getKey();
      localObject3 = ((Map.Entry)localObject3).getValue();
      ((ac)localObject4).serializeTo(paramh, i6, localObject5, localObject3);
    }
    i1 = j;
    int i5;
    if (i1 != 0)
    {
      i5 = 9;
      paramh.writeInt32(i5, i1);
    }
    i1 = 0;
    localObject2 = null;
    for (;;)
    {
      localObject3 = k;
      i5 = ((w.h)localObject3).size();
      if (i1 >= i5) {
        break;
      }
      i5 = 10;
      localObject4 = (ae)k.get(i1);
      paramh.writeMessage(i5, (ae)localObject4);
      i1 += 1;
    }
    for (;;)
    {
      localObject2 = l;
      i1 = ((w.h)localObject2).size();
      if (i3 >= i1) {
        break;
      }
      i1 = 11;
      localObject3 = (ae)l.get(i3);
      paramh.writeMessage(i1, (ae)localObject3);
      i3 += 1;
    }
    localObject1 = m;
    if (localObject1 != null)
    {
      i3 = 12;
      localObject2 = i();
      paramh.writeMessage(i3, (ae)localObject2);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.events.Event.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */