package com.truecaller.api.services.messenger.v1.events;

import com.google.f.ah;
import com.google.f.f;
import com.google.f.h;
import com.google.f.q;

public final class Event$l
  extends q
  implements Event.m
{
  private static final l c;
  private static volatile ah d;
  private int a;
  private f b;
  
  static
  {
    l locall = new com/truecaller/api/services/messenger/v1/events/Event$l;
    locall.<init>();
    c = locall;
    locall.makeImmutable();
  }
  
  private Event$l()
  {
    f localf = f.EMPTY;
    b = localf;
  }
  
  public static l c()
  {
    return c;
  }
  
  public static ah d()
  {
    return c.getParserForType();
  }
  
  public final int a()
  {
    return a;
  }
  
  public final f b()
  {
    return b;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 42	com/truecaller/api/services/messenger/v1/events/Event$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 48	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_1
    //   19: istore 6
    //   21: iconst_0
    //   22: istore 7
    //   24: iload 5
    //   26: tableswitch	default:+46->72, 1:+456->482, 2:+452->478, 3:+450->476, 4:+439->465, 5:+264->290, 6:+108->134, 7:+260->286, 8:+56->82
    //   72: new 51	java/lang/UnsupportedOperationException
    //   75: astore_1
    //   76: aload_1
    //   77: invokespecial 52	java/lang/UnsupportedOperationException:<init>	()V
    //   80: aload_1
    //   81: athrow
    //   82: getstatic 54	com/truecaller/api/services/messenger/v1/events/Event$l:d	Lcom/google/f/ah;
    //   85: astore_1
    //   86: aload_1
    //   87: ifnonnull +43 -> 130
    //   90: ldc 2
    //   92: astore_1
    //   93: aload_1
    //   94: monitorenter
    //   95: getstatic 54	com/truecaller/api/services/messenger/v1/events/Event$l:d	Lcom/google/f/ah;
    //   98: astore_2
    //   99: aload_2
    //   100: ifnonnull +20 -> 120
    //   103: new 56	com/google/f/q$b
    //   106: astore_2
    //   107: getstatic 20	com/truecaller/api/services/messenger/v1/events/Event$l:c	Lcom/truecaller/api/services/messenger/v1/events/Event$l;
    //   110: astore_3
    //   111: aload_2
    //   112: aload_3
    //   113: invokespecial 59	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   116: aload_2
    //   117: putstatic 54	com/truecaller/api/services/messenger/v1/events/Event$l:d	Lcom/google/f/ah;
    //   120: aload_1
    //   121: monitorexit
    //   122: goto +8 -> 130
    //   125: astore_2
    //   126: aload_1
    //   127: monitorexit
    //   128: aload_2
    //   129: athrow
    //   130: getstatic 54	com/truecaller/api/services/messenger/v1/events/Event$l:d	Lcom/google/f/ah;
    //   133: areturn
    //   134: aload_2
    //   135: checkcast 61	com/google/f/g
    //   138: astore_2
    //   139: iload 7
    //   141: ifne +145 -> 286
    //   144: aload_2
    //   145: invokevirtual 64	com/google/f/g:readTag	()I
    //   148: istore 5
    //   150: iload 5
    //   152: ifeq +72 -> 224
    //   155: bipush 8
    //   157: istore 8
    //   159: iload 5
    //   161: iload 8
    //   163: if_icmpeq +46 -> 209
    //   166: bipush 18
    //   168: istore 8
    //   170: iload 5
    //   172: iload 8
    //   174: if_icmpeq +22 -> 196
    //   177: aload_2
    //   178: iload 5
    //   180: invokevirtual 70	com/google/f/g:skipField	(I)Z
    //   183: istore 5
    //   185: iload 5
    //   187: ifne -48 -> 139
    //   190: iconst_1
    //   191: istore 7
    //   193: goto -54 -> 139
    //   196: aload_2
    //   197: invokevirtual 74	com/google/f/g:readBytes	()Lcom/google/f/f;
    //   200: astore_1
    //   201: aload_0
    //   202: aload_1
    //   203: putfield 31	com/truecaller/api/services/messenger/v1/events/Event$l:b	Lcom/google/f/f;
    //   206: goto -67 -> 139
    //   209: aload_2
    //   210: invokevirtual 77	com/google/f/g:readInt32	()I
    //   213: istore 5
    //   215: aload_0
    //   216: iload 5
    //   218: putfield 37	com/truecaller/api/services/messenger/v1/events/Event$l:a	I
    //   221: goto -82 -> 139
    //   224: iconst_1
    //   225: istore 7
    //   227: goto -88 -> 139
    //   230: astore_1
    //   231: goto +53 -> 284
    //   234: astore_1
    //   235: new 79	java/lang/RuntimeException
    //   238: astore_2
    //   239: new 81	com/google/f/x
    //   242: astore_3
    //   243: aload_1
    //   244: invokevirtual 87	java/io/IOException:getMessage	()Ljava/lang/String;
    //   247: astore_1
    //   248: aload_3
    //   249: aload_1
    //   250: invokespecial 90	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   253: aload_3
    //   254: aload_0
    //   255: invokevirtual 94	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   258: astore_1
    //   259: aload_2
    //   260: aload_1
    //   261: invokespecial 97	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   264: aload_2
    //   265: athrow
    //   266: astore_1
    //   267: new 79	java/lang/RuntimeException
    //   270: astore_2
    //   271: aload_1
    //   272: aload_0
    //   273: invokevirtual 94	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   276: astore_1
    //   277: aload_2
    //   278: aload_1
    //   279: invokespecial 97	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   282: aload_2
    //   283: athrow
    //   284: aload_1
    //   285: athrow
    //   286: getstatic 20	com/truecaller/api/services/messenger/v1/events/Event$l:c	Lcom/truecaller/api/services/messenger/v1/events/Event$l;
    //   289: areturn
    //   290: aload_2
    //   291: checkcast 99	com/google/f/q$k
    //   294: astore_2
    //   295: aload_3
    //   296: checkcast 2	com/truecaller/api/services/messenger/v1/events/Event$l
    //   299: astore_3
    //   300: aload_0
    //   301: getfield 37	com/truecaller/api/services/messenger/v1/events/Event$l:a	I
    //   304: istore 5
    //   306: iload 5
    //   308: ifeq +9 -> 317
    //   311: iconst_1
    //   312: istore 5
    //   314: goto +8 -> 322
    //   317: iconst_0
    //   318: istore 5
    //   320: aconst_null
    //   321: astore_1
    //   322: aload_0
    //   323: getfield 37	com/truecaller/api/services/messenger/v1/events/Event$l:a	I
    //   326: istore 9
    //   328: aload_3
    //   329: getfield 37	com/truecaller/api/services/messenger/v1/events/Event$l:a	I
    //   332: istore 10
    //   334: iload 10
    //   336: ifeq +9 -> 345
    //   339: iconst_1
    //   340: istore 10
    //   342: goto +9 -> 351
    //   345: iconst_0
    //   346: istore 10
    //   348: aconst_null
    //   349: astore 11
    //   351: aload_3
    //   352: getfield 37	com/truecaller/api/services/messenger/v1/events/Event$l:a	I
    //   355: istore 12
    //   357: aload_2
    //   358: iload 5
    //   360: iload 9
    //   362: iload 10
    //   364: iload 12
    //   366: invokeinterface 103 5 0
    //   371: istore 5
    //   373: aload_0
    //   374: iload 5
    //   376: putfield 37	com/truecaller/api/services/messenger/v1/events/Event$l:a	I
    //   379: aload_0
    //   380: getfield 31	com/truecaller/api/services/messenger/v1/events/Event$l:b	Lcom/google/f/f;
    //   383: astore_1
    //   384: getstatic 29	com/google/f/f:EMPTY	Lcom/google/f/f;
    //   387: astore 13
    //   389: aload_1
    //   390: aload 13
    //   392: if_acmpeq +9 -> 401
    //   395: iconst_1
    //   396: istore 5
    //   398: goto +8 -> 406
    //   401: iconst_0
    //   402: istore 5
    //   404: aconst_null
    //   405: astore_1
    //   406: aload_0
    //   407: getfield 31	com/truecaller/api/services/messenger/v1/events/Event$l:b	Lcom/google/f/f;
    //   410: astore 13
    //   412: aload_3
    //   413: getfield 31	com/truecaller/api/services/messenger/v1/events/Event$l:b	Lcom/google/f/f;
    //   416: astore 11
    //   418: getstatic 29	com/google/f/f:EMPTY	Lcom/google/f/f;
    //   421: astore 14
    //   423: aload 11
    //   425: aload 14
    //   427: if_acmpeq +6 -> 433
    //   430: goto +9 -> 439
    //   433: iconst_0
    //   434: istore 6
    //   436: aconst_null
    //   437: astore 4
    //   439: aload_3
    //   440: getfield 31	com/truecaller/api/services/messenger/v1/events/Event$l:b	Lcom/google/f/f;
    //   443: astore_3
    //   444: aload_2
    //   445: iload 5
    //   447: aload 13
    //   449: iload 6
    //   451: aload_3
    //   452: invokeinterface 107 5 0
    //   457: astore_1
    //   458: aload_0
    //   459: aload_1
    //   460: putfield 31	com/truecaller/api/services/messenger/v1/events/Event$l:b	Lcom/google/f/f;
    //   463: aload_0
    //   464: areturn
    //   465: new 109	com/truecaller/api/services/messenger/v1/events/Event$l$a
    //   468: astore_1
    //   469: aload_1
    //   470: iconst_0
    //   471: invokespecial 112	com/truecaller/api/services/messenger/v1/events/Event$l$a:<init>	(B)V
    //   474: aload_1
    //   475: areturn
    //   476: aconst_null
    //   477: areturn
    //   478: getstatic 20	com/truecaller/api/services/messenger/v1/events/Event$l:c	Lcom/truecaller/api/services/messenger/v1/events/Event$l;
    //   481: areturn
    //   482: new 2	com/truecaller/api/services/messenger/v1/events/Event$l
    //   485: astore_1
    //   486: aload_1
    //   487: invokespecial 18	com/truecaller/api/services/messenger/v1/events/Event$l:<init>	()V
    //   490: aload_1
    //   491: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	492	0	this	l
    //   0	492	1	paramj	com.google.f.q.j
    //   0	492	2	paramObject1	Object
    //   0	492	3	paramObject2	Object
    //   3	435	4	arrayOfInt	int[]
    //   9	170	5	i	int
    //   183	3	5	bool1	boolean
    //   213	146	5	j	int
    //   371	75	5	k	int
    //   19	431	6	bool2	boolean
    //   22	204	7	m	int
    //   157	18	8	n	int
    //   326	35	9	i1	int
    //   332	31	10	i2	int
    //   349	75	11	localf1	f
    //   355	10	12	i3	int
    //   387	61	13	localf2	f
    //   421	5	14	localf3	f
    // Exception table:
    //   from	to	target	type
    //   95	98	125	finally
    //   103	106	125	finally
    //   107	110	125	finally
    //   112	116	125	finally
    //   116	120	125	finally
    //   120	122	125	finally
    //   126	128	125	finally
    //   144	148	230	finally
    //   178	183	230	finally
    //   196	200	230	finally
    //   202	206	230	finally
    //   209	213	230	finally
    //   216	221	230	finally
    //   235	238	230	finally
    //   239	242	230	finally
    //   243	247	230	finally
    //   249	253	230	finally
    //   254	258	230	finally
    //   260	264	230	finally
    //   264	266	230	finally
    //   267	270	230	finally
    //   272	276	230	finally
    //   278	282	230	finally
    //   282	284	230	finally
    //   144	148	234	java/io/IOException
    //   178	183	234	java/io/IOException
    //   196	200	234	java/io/IOException
    //   202	206	234	java/io/IOException
    //   209	213	234	java/io/IOException
    //   216	221	234	java/io/IOException
    //   144	148	266	com/google/f/x
    //   178	183	266	com/google/f/x
    //   196	200	266	com/google/f/x
    //   202	206	266	com/google/f/x
    //   209	213	266	com/google/f/x
    //   216	221	266	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int k = -1;
    if (i != k) {
      return i;
    }
    i = a;
    k = 0;
    if (i != 0)
    {
      int m = 1;
      i = h.computeInt32Size(m, i);
      k = 0 + i;
    }
    f localf1 = b;
    boolean bool = localf1.isEmpty();
    if (!bool)
    {
      f localf2 = b;
      int j = h.computeBytesSize(2, localf2);
      k += j;
    }
    memoizedSerializedSize = k;
    return k;
  }
  
  public final void writeTo(h paramh)
  {
    int i = a;
    if (i != 0)
    {
      int k = 1;
      paramh.writeInt32(k, i);
    }
    f localf1 = b;
    boolean bool = localf1.isEmpty();
    if (!bool)
    {
      int j = 2;
      f localf2 = b;
      paramh.writeBytes(j, localf2);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.events.Event.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */