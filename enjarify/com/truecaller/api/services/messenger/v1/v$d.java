package com.truecaller.api.services.messenger.v1;

import com.google.f.ae;
import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;
import com.truecaller.api.services.messenger.v1.models.c;

public final class v$d
  extends q
  implements v.e
{
  private static final d d;
  private static volatile ah e;
  private String a = "";
  private int b;
  private c c;
  
  static
  {
    d locald = new com/truecaller/api/services/messenger/v1/v$d;
    locald.<init>();
    d = locald;
    locald.makeImmutable();
  }
  
  public static d c()
  {
    return d;
  }
  
  public final String a()
  {
    return a;
  }
  
  public final c b()
  {
    c localc = c;
    if (localc == null) {
      localc = c.c();
    }
    return localc;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 42	com/truecaller/api/services/messenger/v1/v$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 48	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_0
    //   19: istore 6
    //   21: aconst_null
    //   22: astore 4
    //   24: iconst_0
    //   25: istore 7
    //   27: iconst_1
    //   28: istore 8
    //   30: iload 5
    //   32: tableswitch	default:+48->80, 1:+562->594, 2:+558->590, 3:+556->588, 4:+545->577, 5:+376->408, 6:+110->142, 7:+372->404, 8:+58->90
    //   80: new 51	java/lang/UnsupportedOperationException
    //   83: astore_1
    //   84: aload_1
    //   85: invokespecial 52	java/lang/UnsupportedOperationException:<init>	()V
    //   88: aload_1
    //   89: athrow
    //   90: getstatic 54	com/truecaller/api/services/messenger/v1/v$d:e	Lcom/google/f/ah;
    //   93: astore_1
    //   94: aload_1
    //   95: ifnonnull +43 -> 138
    //   98: ldc 2
    //   100: astore_1
    //   101: aload_1
    //   102: monitorenter
    //   103: getstatic 54	com/truecaller/api/services/messenger/v1/v$d:e	Lcom/google/f/ah;
    //   106: astore_2
    //   107: aload_2
    //   108: ifnonnull +20 -> 128
    //   111: new 56	com/google/f/q$b
    //   114: astore_2
    //   115: getstatic 22	com/truecaller/api/services/messenger/v1/v$d:d	Lcom/truecaller/api/services/messenger/v1/v$d;
    //   118: astore_3
    //   119: aload_2
    //   120: aload_3
    //   121: invokespecial 59	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   124: aload_2
    //   125: putstatic 54	com/truecaller/api/services/messenger/v1/v$d:e	Lcom/google/f/ah;
    //   128: aload_1
    //   129: monitorexit
    //   130: goto +8 -> 138
    //   133: astore_2
    //   134: aload_1
    //   135: monitorexit
    //   136: aload_2
    //   137: athrow
    //   138: getstatic 54	com/truecaller/api/services/messenger/v1/v$d:e	Lcom/google/f/ah;
    //   141: areturn
    //   142: aload_2
    //   143: checkcast 61	com/google/f/g
    //   146: astore_2
    //   147: aload_3
    //   148: checkcast 63	com/google/f/n
    //   151: astore_3
    //   152: iload 7
    //   154: ifne +250 -> 404
    //   157: aload_2
    //   158: invokevirtual 66	com/google/f/g:readTag	()I
    //   161: istore 5
    //   163: iload 5
    //   165: ifeq +177 -> 342
    //   168: bipush 10
    //   170: istore 9
    //   172: iload 5
    //   174: iload 9
    //   176: if_icmpeq +153 -> 329
    //   179: bipush 16
    //   181: istore 9
    //   183: iload 5
    //   185: iload 9
    //   187: if_icmpeq +127 -> 314
    //   190: bipush 26
    //   192: istore 9
    //   194: iload 5
    //   196: iload 9
    //   198: if_icmpeq +22 -> 220
    //   201: aload_2
    //   202: iload 5
    //   204: invokevirtual 73	com/google/f/g:skipField	(I)Z
    //   207: istore 5
    //   209: iload 5
    //   211: ifne -59 -> 152
    //   214: iconst_1
    //   215: istore 7
    //   217: goto -65 -> 152
    //   220: aload_0
    //   221: getfield 32	com/truecaller/api/services/messenger/v1/v$d:c	Lcom/truecaller/api/services/messenger/v1/models/c;
    //   224: astore_1
    //   225: aload_1
    //   226: ifnull +21 -> 247
    //   229: aload_0
    //   230: getfield 32	com/truecaller/api/services/messenger/v1/v$d:c	Lcom/truecaller/api/services/messenger/v1/models/c;
    //   233: astore_1
    //   234: aload_1
    //   235: invokevirtual 77	com/truecaller/api/services/messenger/v1/models/c:toBuilder	()Lcom/google/f/q$a;
    //   238: astore_1
    //   239: aload_1
    //   240: checkcast 79	com/truecaller/api/services/messenger/v1/models/c$a
    //   243: astore_1
    //   244: goto +8 -> 252
    //   247: iconst_0
    //   248: istore 5
    //   250: aconst_null
    //   251: astore_1
    //   252: invokestatic 82	com/truecaller/api/services/messenger/v1/models/c:d	()Lcom/google/f/ah;
    //   255: astore 10
    //   257: aload_2
    //   258: aload 10
    //   260: aload_3
    //   261: invokevirtual 86	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   264: astore 10
    //   266: aload 10
    //   268: checkcast 34	com/truecaller/api/services/messenger/v1/models/c
    //   271: astore 10
    //   273: aload_0
    //   274: aload 10
    //   276: putfield 32	com/truecaller/api/services/messenger/v1/v$d:c	Lcom/truecaller/api/services/messenger/v1/models/c;
    //   279: aload_1
    //   280: ifnull -128 -> 152
    //   283: aload_0
    //   284: getfield 32	com/truecaller/api/services/messenger/v1/v$d:c	Lcom/truecaller/api/services/messenger/v1/models/c;
    //   287: astore 10
    //   289: aload_1
    //   290: aload 10
    //   292: invokevirtual 90	com/truecaller/api/services/messenger/v1/models/c$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   295: pop
    //   296: aload_1
    //   297: invokevirtual 94	com/truecaller/api/services/messenger/v1/models/c$a:buildPartial	()Lcom/google/f/q;
    //   300: astore_1
    //   301: aload_1
    //   302: checkcast 34	com/truecaller/api/services/messenger/v1/models/c
    //   305: astore_1
    //   306: aload_0
    //   307: aload_1
    //   308: putfield 32	com/truecaller/api/services/messenger/v1/v$d:c	Lcom/truecaller/api/services/messenger/v1/models/c;
    //   311: goto -159 -> 152
    //   314: aload_2
    //   315: invokevirtual 97	com/google/f/g:readInt32	()I
    //   318: istore 5
    //   320: aload_0
    //   321: iload 5
    //   323: putfield 99	com/truecaller/api/services/messenger/v1/v$d:b	I
    //   326: goto -174 -> 152
    //   329: aload_2
    //   330: invokevirtual 103	com/google/f/g:readStringRequireUtf8	()Ljava/lang/String;
    //   333: astore_1
    //   334: aload_0
    //   335: aload_1
    //   336: putfield 30	com/truecaller/api/services/messenger/v1/v$d:a	Ljava/lang/String;
    //   339: goto -187 -> 152
    //   342: iconst_1
    //   343: istore 7
    //   345: goto -193 -> 152
    //   348: astore_1
    //   349: goto +53 -> 402
    //   352: astore_1
    //   353: new 105	java/lang/RuntimeException
    //   356: astore_2
    //   357: new 107	com/google/f/x
    //   360: astore_3
    //   361: aload_1
    //   362: invokevirtual 112	java/io/IOException:getMessage	()Ljava/lang/String;
    //   365: astore_1
    //   366: aload_3
    //   367: aload_1
    //   368: invokespecial 115	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   371: aload_3
    //   372: aload_0
    //   373: invokevirtual 119	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   376: astore_1
    //   377: aload_2
    //   378: aload_1
    //   379: invokespecial 122	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   382: aload_2
    //   383: athrow
    //   384: astore_1
    //   385: new 105	java/lang/RuntimeException
    //   388: astore_2
    //   389: aload_1
    //   390: aload_0
    //   391: invokevirtual 119	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   394: astore_1
    //   395: aload_2
    //   396: aload_1
    //   397: invokespecial 122	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   400: aload_2
    //   401: athrow
    //   402: aload_1
    //   403: athrow
    //   404: getstatic 22	com/truecaller/api/services/messenger/v1/v$d:d	Lcom/truecaller/api/services/messenger/v1/v$d;
    //   407: areturn
    //   408: aload_2
    //   409: checkcast 124	com/google/f/q$k
    //   412: astore_2
    //   413: aload_3
    //   414: checkcast 2	com/truecaller/api/services/messenger/v1/v$d
    //   417: astore_3
    //   418: aload_0
    //   419: getfield 30	com/truecaller/api/services/messenger/v1/v$d:a	Ljava/lang/String;
    //   422: invokevirtual 130	java/lang/String:isEmpty	()Z
    //   425: iload 8
    //   427: ixor
    //   428: istore 5
    //   430: aload_0
    //   431: getfield 30	com/truecaller/api/services/messenger/v1/v$d:a	Ljava/lang/String;
    //   434: astore 4
    //   436: aload_3
    //   437: getfield 30	com/truecaller/api/services/messenger/v1/v$d:a	Ljava/lang/String;
    //   440: astore 10
    //   442: aload 10
    //   444: invokevirtual 130	java/lang/String:isEmpty	()Z
    //   447: iload 8
    //   449: ixor
    //   450: istore 9
    //   452: aload_3
    //   453: getfield 30	com/truecaller/api/services/messenger/v1/v$d:a	Ljava/lang/String;
    //   456: astore 11
    //   458: aload_2
    //   459: iload 5
    //   461: aload 4
    //   463: iload 9
    //   465: aload 11
    //   467: invokeinterface 134 5 0
    //   472: astore_1
    //   473: aload_0
    //   474: aload_1
    //   475: putfield 30	com/truecaller/api/services/messenger/v1/v$d:a	Ljava/lang/String;
    //   478: aload_0
    //   479: getfield 99	com/truecaller/api/services/messenger/v1/v$d:b	I
    //   482: istore 5
    //   484: iload 5
    //   486: ifeq +9 -> 495
    //   489: iconst_1
    //   490: istore 5
    //   492: goto +8 -> 500
    //   495: iconst_0
    //   496: istore 5
    //   498: aconst_null
    //   499: astore_1
    //   500: aload_0
    //   501: getfield 99	com/truecaller/api/services/messenger/v1/v$d:b	I
    //   504: istore 6
    //   506: aload_3
    //   507: getfield 99	com/truecaller/api/services/messenger/v1/v$d:b	I
    //   510: istore 9
    //   512: iload 9
    //   514: ifeq +6 -> 520
    //   517: iconst_1
    //   518: istore 7
    //   520: aload_3
    //   521: getfield 99	com/truecaller/api/services/messenger/v1/v$d:b	I
    //   524: istore 8
    //   526: aload_2
    //   527: iload 5
    //   529: iload 6
    //   531: iload 7
    //   533: iload 8
    //   535: invokeinterface 138 5 0
    //   540: istore 5
    //   542: aload_0
    //   543: iload 5
    //   545: putfield 99	com/truecaller/api/services/messenger/v1/v$d:b	I
    //   548: aload_0
    //   549: getfield 32	com/truecaller/api/services/messenger/v1/v$d:c	Lcom/truecaller/api/services/messenger/v1/models/c;
    //   552: astore_1
    //   553: aload_3
    //   554: getfield 32	com/truecaller/api/services/messenger/v1/v$d:c	Lcom/truecaller/api/services/messenger/v1/models/c;
    //   557: astore_3
    //   558: aload_2
    //   559: aload_1
    //   560: aload_3
    //   561: invokeinterface 142 3 0
    //   566: checkcast 34	com/truecaller/api/services/messenger/v1/models/c
    //   569: astore_1
    //   570: aload_0
    //   571: aload_1
    //   572: putfield 32	com/truecaller/api/services/messenger/v1/v$d:c	Lcom/truecaller/api/services/messenger/v1/models/c;
    //   575: aload_0
    //   576: areturn
    //   577: new 144	com/truecaller/api/services/messenger/v1/v$d$a
    //   580: astore_1
    //   581: aload_1
    //   582: iconst_0
    //   583: invokespecial 147	com/truecaller/api/services/messenger/v1/v$d$a:<init>	(B)V
    //   586: aload_1
    //   587: areturn
    //   588: aconst_null
    //   589: areturn
    //   590: getstatic 22	com/truecaller/api/services/messenger/v1/v$d:d	Lcom/truecaller/api/services/messenger/v1/v$d;
    //   593: areturn
    //   594: new 2	com/truecaller/api/services/messenger/v1/v$d
    //   597: astore_1
    //   598: aload_1
    //   599: invokespecial 20	com/truecaller/api/services/messenger/v1/v$d:<init>	()V
    //   602: aload_1
    //   603: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	604	0	this	d
    //   0	604	1	paramj	com.google.f.q.j
    //   0	604	2	paramObject1	Object
    //   0	604	3	paramObject2	Object
    //   3	459	4	localObject1	Object
    //   9	194	5	i	int
    //   207	42	5	bool1	boolean
    //   318	4	5	j	int
    //   428	32	5	bool2	boolean
    //   482	46	5	k	int
    //   540	4	5	m	int
    //   19	511	6	n	int
    //   25	507	7	bool3	boolean
    //   28	506	8	i1	int
    //   170	29	9	i2	int
    //   450	14	9	bool4	boolean
    //   510	3	9	i3	int
    //   255	188	10	localObject2	Object
    //   456	10	11	str	String
    // Exception table:
    //   from	to	target	type
    //   103	106	133	finally
    //   111	114	133	finally
    //   115	118	133	finally
    //   120	124	133	finally
    //   124	128	133	finally
    //   128	130	133	finally
    //   134	136	133	finally
    //   157	161	348	finally
    //   202	207	348	finally
    //   220	224	348	finally
    //   229	233	348	finally
    //   234	238	348	finally
    //   239	243	348	finally
    //   252	255	348	finally
    //   260	264	348	finally
    //   266	271	348	finally
    //   274	279	348	finally
    //   283	287	348	finally
    //   290	296	348	finally
    //   296	300	348	finally
    //   301	305	348	finally
    //   307	311	348	finally
    //   314	318	348	finally
    //   321	326	348	finally
    //   329	333	348	finally
    //   335	339	348	finally
    //   353	356	348	finally
    //   357	360	348	finally
    //   361	365	348	finally
    //   367	371	348	finally
    //   372	376	348	finally
    //   378	382	348	finally
    //   382	384	348	finally
    //   385	388	348	finally
    //   390	394	348	finally
    //   396	400	348	finally
    //   400	402	348	finally
    //   157	161	352	java/io/IOException
    //   202	207	352	java/io/IOException
    //   220	224	352	java/io/IOException
    //   229	233	352	java/io/IOException
    //   234	238	352	java/io/IOException
    //   239	243	352	java/io/IOException
    //   252	255	352	java/io/IOException
    //   260	264	352	java/io/IOException
    //   266	271	352	java/io/IOException
    //   274	279	352	java/io/IOException
    //   283	287	352	java/io/IOException
    //   290	296	352	java/io/IOException
    //   296	300	352	java/io/IOException
    //   301	305	352	java/io/IOException
    //   307	311	352	java/io/IOException
    //   314	318	352	java/io/IOException
    //   321	326	352	java/io/IOException
    //   329	333	352	java/io/IOException
    //   335	339	352	java/io/IOException
    //   157	161	384	com/google/f/x
    //   202	207	384	com/google/f/x
    //   220	224	384	com/google/f/x
    //   229	233	384	com/google/f/x
    //   234	238	384	com/google/f/x
    //   239	243	384	com/google/f/x
    //   252	255	384	com/google/f/x
    //   260	264	384	com/google/f/x
    //   266	271	384	com/google/f/x
    //   274	279	384	com/google/f/x
    //   283	287	384	com/google/f/x
    //   290	296	384	com/google/f/x
    //   296	300	384	com/google/f/x
    //   301	305	384	com/google/f/x
    //   307	311	384	com/google/f/x
    //   314	318	384	com/google/f/x
    //   321	326	384	com/google/f/x
    //   329	333	384	com/google/f/x
    //   335	339	384	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int k = -1;
    if (i != k) {
      return i;
    }
    Object localObject1 = a;
    boolean bool = ((String)localObject1).isEmpty();
    k = 0;
    Object localObject2;
    if (!bool)
    {
      localObject2 = a;
      j = h.computeStringSize(1, (String)localObject2);
      k = 0 + j;
    }
    int j = b;
    if (j != 0)
    {
      int m = 2;
      j = h.computeInt32Size(m, j);
      k += j;
    }
    localObject1 = c;
    if (localObject1 != null)
    {
      localObject2 = b();
      j = h.computeMessageSize(3, (ae)localObject2);
      k += j;
    }
    memoizedSerializedSize = k;
    return k;
  }
  
  public final void writeTo(h paramh)
  {
    Object localObject1 = a;
    int i = ((String)localObject1).isEmpty();
    Object localObject2;
    if (i == 0)
    {
      i = 1;
      localObject2 = a;
      paramh.writeString(i, (String)localObject2);
    }
    int j = b;
    if (j != 0)
    {
      int k = 2;
      paramh.writeInt32(k, j);
    }
    localObject1 = c;
    if (localObject1 != null)
    {
      j = 3;
      localObject2 = b();
      paramh.writeMessage(j, (ae)localObject2);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.v.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */