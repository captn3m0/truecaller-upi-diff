package com.truecaller.api.services.messenger.v1;

import com.google.f.q.a;

public final class MediaHandles$Request$a
  extends q.a
  implements MediaHandles.b
{
  private MediaHandles$Request$a()
  {
    super(localRequest);
  }
  
  public final a a(long paramLong)
  {
    copyOnWrite();
    MediaHandles.Request.a((MediaHandles.Request)instance, paramLong);
    return this;
  }
  
  public final a a(String paramString)
  {
    copyOnWrite();
    MediaHandles.Request.a((MediaHandles.Request)instance, paramString);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.MediaHandles.Request.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */