package com.truecaller.api.services.messenger.v1;

import com.google.f.w.c;

public enum Batched$Response$Payload$DataCase
  implements w.c
{
  private final int value;
  
  static
  {
    Object localObject = new com/truecaller/api/services/messenger/v1/Batched$Response$Payload$DataCase;
    int i = 1;
    ((DataCase)localObject).<init>("ERROR", 0, i);
    ERROR = (DataCase)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/Batched$Response$Payload$DataCase;
    int j = 2;
    ((DataCase)localObject).<init>("SENDMESSAGE", i, j);
    SENDMESSAGE = (DataCase)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/Batched$Response$Payload$DataCase;
    int k = 3;
    ((DataCase)localObject).<init>("SENDREPORT", j, k);
    SENDREPORT = (DataCase)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/Batched$Response$Payload$DataCase;
    int m = 4;
    ((DataCase)localObject).<init>("SENDREACTION", k, m);
    SENDREACTION = (DataCase)localObject;
    localObject = new com/truecaller/api/services/messenger/v1/Batched$Response$Payload$DataCase;
    ((DataCase)localObject).<init>("DATA_NOT_SET", m, 0);
    DATA_NOT_SET = (DataCase)localObject;
    localObject = new DataCase[5];
    DataCase localDataCase = ERROR;
    localObject[0] = localDataCase;
    localDataCase = SENDMESSAGE;
    localObject[i] = localDataCase;
    localDataCase = SENDREPORT;
    localObject[j] = localDataCase;
    localDataCase = SENDREACTION;
    localObject[k] = localDataCase;
    localDataCase = DATA_NOT_SET;
    localObject[m] = localDataCase;
    $VALUES = (DataCase[])localObject;
  }
  
  private Batched$Response$Payload$DataCase(int paramInt1)
  {
    value = paramInt1;
  }
  
  public static DataCase forNumber(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return null;
    case 4: 
      return SENDREACTION;
    case 3: 
      return SENDREPORT;
    case 2: 
      return SENDMESSAGE;
    case 1: 
      return ERROR;
    }
    return DATA_NOT_SET;
  }
  
  public static DataCase valueOf(int paramInt)
  {
    return forNumber(paramInt);
  }
  
  public final int getNumber()
  {
    return value;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.Batched.Response.Payload.DataCase
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */