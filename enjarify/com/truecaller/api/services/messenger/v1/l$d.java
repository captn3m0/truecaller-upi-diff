package com.truecaller.api.services.messenger.v1;

import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;

public final class l$d
  extends q
  implements l.e
{
  private static final d b;
  private static volatile ah c;
  private String a = "";
  
  static
  {
    d locald = new com/truecaller/api/services/messenger/v1/l$d;
    locald.<init>();
    b = locald;
    locald.makeImmutable();
  }
  
  public static d b()
  {
    return b;
  }
  
  public final String a()
  {
    return a;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 31	com/truecaller/api/services/messenger/v1/l$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 37	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_0
    //   19: istore 6
    //   21: aconst_null
    //   22: astore 4
    //   24: iconst_1
    //   25: istore 7
    //   27: iload 5
    //   29: tableswitch	default:+47->76, 1:+326->355, 2:+322->351, 3:+320->349, 4:+309->338, 5:+239->268, 6:+109->138, 7:+235->264, 8:+57->86
    //   76: new 40	java/lang/UnsupportedOperationException
    //   79: astore_1
    //   80: aload_1
    //   81: invokespecial 41	java/lang/UnsupportedOperationException:<init>	()V
    //   84: aload_1
    //   85: athrow
    //   86: getstatic 43	com/truecaller/api/services/messenger/v1/l$d:c	Lcom/google/f/ah;
    //   89: astore_1
    //   90: aload_1
    //   91: ifnonnull +43 -> 134
    //   94: ldc 2
    //   96: astore_1
    //   97: aload_1
    //   98: monitorenter
    //   99: getstatic 43	com/truecaller/api/services/messenger/v1/l$d:c	Lcom/google/f/ah;
    //   102: astore_2
    //   103: aload_2
    //   104: ifnonnull +20 -> 124
    //   107: new 45	com/google/f/q$b
    //   110: astore_2
    //   111: getstatic 18	com/truecaller/api/services/messenger/v1/l$d:b	Lcom/truecaller/api/services/messenger/v1/l$d;
    //   114: astore_3
    //   115: aload_2
    //   116: aload_3
    //   117: invokespecial 48	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   120: aload_2
    //   121: putstatic 43	com/truecaller/api/services/messenger/v1/l$d:c	Lcom/google/f/ah;
    //   124: aload_1
    //   125: monitorexit
    //   126: goto +8 -> 134
    //   129: astore_2
    //   130: aload_1
    //   131: monitorexit
    //   132: aload_2
    //   133: athrow
    //   134: getstatic 43	com/truecaller/api/services/messenger/v1/l$d:c	Lcom/google/f/ah;
    //   137: areturn
    //   138: aload_2
    //   139: checkcast 50	com/google/f/g
    //   142: astore_2
    //   143: iload 6
    //   145: ifne +119 -> 264
    //   148: aload_2
    //   149: invokevirtual 53	com/google/f/g:readTag	()I
    //   152: istore 5
    //   154: iload 5
    //   156: ifeq +46 -> 202
    //   159: bipush 10
    //   161: istore 8
    //   163: iload 5
    //   165: iload 8
    //   167: if_icmpeq +22 -> 189
    //   170: aload_2
    //   171: iload 5
    //   173: invokevirtual 58	com/google/f/g:skipField	(I)Z
    //   176: istore 5
    //   178: iload 5
    //   180: ifne -37 -> 143
    //   183: iconst_1
    //   184: istore 6
    //   186: goto -43 -> 143
    //   189: aload_2
    //   190: invokevirtual 62	com/google/f/g:readStringRequireUtf8	()Ljava/lang/String;
    //   193: astore_1
    //   194: aload_0
    //   195: aload_1
    //   196: putfield 26	com/truecaller/api/services/messenger/v1/l$d:a	Ljava/lang/String;
    //   199: goto -56 -> 143
    //   202: iconst_1
    //   203: istore 6
    //   205: goto -62 -> 143
    //   208: astore_1
    //   209: goto +53 -> 262
    //   212: astore_1
    //   213: new 64	java/lang/RuntimeException
    //   216: astore_2
    //   217: new 66	com/google/f/x
    //   220: astore_3
    //   221: aload_1
    //   222: invokevirtual 71	java/io/IOException:getMessage	()Ljava/lang/String;
    //   225: astore_1
    //   226: aload_3
    //   227: aload_1
    //   228: invokespecial 74	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   231: aload_3
    //   232: aload_0
    //   233: invokevirtual 78	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   236: astore_1
    //   237: aload_2
    //   238: aload_1
    //   239: invokespecial 81	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   242: aload_2
    //   243: athrow
    //   244: astore_1
    //   245: new 64	java/lang/RuntimeException
    //   248: astore_2
    //   249: aload_1
    //   250: aload_0
    //   251: invokevirtual 78	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   254: astore_1
    //   255: aload_2
    //   256: aload_1
    //   257: invokespecial 81	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   260: aload_2
    //   261: athrow
    //   262: aload_1
    //   263: athrow
    //   264: getstatic 18	com/truecaller/api/services/messenger/v1/l$d:b	Lcom/truecaller/api/services/messenger/v1/l$d;
    //   267: areturn
    //   268: aload_2
    //   269: checkcast 83	com/google/f/q$k
    //   272: astore_2
    //   273: aload_3
    //   274: checkcast 2	com/truecaller/api/services/messenger/v1/l$d
    //   277: astore_3
    //   278: aload_0
    //   279: getfield 26	com/truecaller/api/services/messenger/v1/l$d:a	Ljava/lang/String;
    //   282: invokevirtual 89	java/lang/String:isEmpty	()Z
    //   285: iload 7
    //   287: ixor
    //   288: istore 5
    //   290: aload_0
    //   291: getfield 26	com/truecaller/api/services/messenger/v1/l$d:a	Ljava/lang/String;
    //   294: astore 4
    //   296: aload_3
    //   297: getfield 26	com/truecaller/api/services/messenger/v1/l$d:a	Ljava/lang/String;
    //   300: invokevirtual 89	java/lang/String:isEmpty	()Z
    //   303: istore 9
    //   305: iload 7
    //   307: iload 9
    //   309: ixor
    //   310: istore 7
    //   312: aload_3
    //   313: getfield 26	com/truecaller/api/services/messenger/v1/l$d:a	Ljava/lang/String;
    //   316: astore_3
    //   317: aload_2
    //   318: iload 5
    //   320: aload 4
    //   322: iload 7
    //   324: aload_3
    //   325: invokeinterface 93 5 0
    //   330: astore_1
    //   331: aload_0
    //   332: aload_1
    //   333: putfield 26	com/truecaller/api/services/messenger/v1/l$d:a	Ljava/lang/String;
    //   336: aload_0
    //   337: areturn
    //   338: new 95	com/truecaller/api/services/messenger/v1/l$d$a
    //   341: astore_1
    //   342: aload_1
    //   343: iconst_0
    //   344: invokespecial 98	com/truecaller/api/services/messenger/v1/l$d$a:<init>	(B)V
    //   347: aload_1
    //   348: areturn
    //   349: aconst_null
    //   350: areturn
    //   351: getstatic 18	com/truecaller/api/services/messenger/v1/l$d:b	Lcom/truecaller/api/services/messenger/v1/l$d;
    //   354: areturn
    //   355: new 2	com/truecaller/api/services/messenger/v1/l$d
    //   358: astore_1
    //   359: aload_1
    //   360: invokespecial 16	com/truecaller/api/services/messenger/v1/l$d:<init>	()V
    //   363: aload_1
    //   364: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	365	0	this	d
    //   0	365	1	paramj	com.google.f.q.j
    //   0	365	2	paramObject1	Object
    //   0	365	3	paramObject2	Object
    //   3	318	4	localObject	Object
    //   9	163	5	i	int
    //   176	143	5	bool1	boolean
    //   19	185	6	j	int
    //   25	298	7	bool2	boolean
    //   161	7	8	k	int
    //   303	7	9	bool3	boolean
    // Exception table:
    //   from	to	target	type
    //   99	102	129	finally
    //   107	110	129	finally
    //   111	114	129	finally
    //   116	120	129	finally
    //   120	124	129	finally
    //   124	126	129	finally
    //   130	132	129	finally
    //   148	152	208	finally
    //   171	176	208	finally
    //   189	193	208	finally
    //   195	199	208	finally
    //   213	216	208	finally
    //   217	220	208	finally
    //   221	225	208	finally
    //   227	231	208	finally
    //   232	236	208	finally
    //   238	242	208	finally
    //   242	244	208	finally
    //   245	248	208	finally
    //   250	254	208	finally
    //   256	260	208	finally
    //   260	262	208	finally
    //   148	152	212	java/io/IOException
    //   171	176	212	java/io/IOException
    //   189	193	212	java/io/IOException
    //   195	199	212	java/io/IOException
    //   148	152	244	com/google/f/x
    //   171	176	244	com/google/f/x
    //   189	193	244	com/google/f/x
    //   195	199	244	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int k = -1;
    if (i != k) {
      return i;
    }
    String str1 = a;
    boolean bool = str1.isEmpty();
    k = 0;
    if (!bool)
    {
      String str2 = a;
      int j = h.computeStringSize(1, str2);
      k = 0 + j;
    }
    memoizedSerializedSize = k;
    return k;
  }
  
  public final void writeTo(h paramh)
  {
    String str1 = a;
    int i = str1.isEmpty();
    if (i == 0)
    {
      i = 1;
      String str2 = a;
      paramh.writeString(i, str2);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.l.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */