package com.truecaller.api.services.messenger.v1;

import com.google.f.q.a;
import com.truecaller.api.services.messenger.v1.models.input.a.a;

public final class d$b$a
  extends q.a
  implements d.c
{
  private d$b$a()
  {
    super(localb);
  }
  
  public final a a(long paramLong)
  {
    copyOnWrite();
    d.b.a((d.b)instance, paramLong);
    return this;
  }
  
  public final a a(a.a parama)
  {
    copyOnWrite();
    d.b.a((d.b)instance, parama);
    return this;
  }
  
  public final a a(Iterable paramIterable)
  {
    copyOnWrite();
    d.b.a((d.b)instance, paramIterable);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.d.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */