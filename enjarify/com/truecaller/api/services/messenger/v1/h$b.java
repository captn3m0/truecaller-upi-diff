package com.truecaller.api.services.messenger.v1;

import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;
import com.google.f.w.g;
import java.util.List;

public final class h$b
  extends q
  implements h.c
{
  private static final b b;
  private static volatile ah c;
  private w.g a;
  
  static
  {
    b localb = new com/truecaller/api/services/messenger/v1/h$b;
    localb.<init>();
    b = localb;
    localb.makeImmutable();
  }
  
  private h$b()
  {
    w.g localg = emptyLongList();
    a = localg;
  }
  
  public static h.b.a a()
  {
    return (h.b.a)b.toBuilder();
  }
  
  public static b b()
  {
    return b;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 55	com/truecaller/api/services/messenger/v1/h$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 61	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_0
    //   19: istore 6
    //   21: aconst_null
    //   22: astore 4
    //   24: iload 5
    //   26: tableswitch	default:+46->72, 1:+497->523, 2:+493->519, 3:+482->508, 4:+471->497, 5:+435->461, 6:+108->134, 7:+431->457, 8:+56->82
    //   72: new 63	java/lang/UnsupportedOperationException
    //   75: astore_1
    //   76: aload_1
    //   77: invokespecial 64	java/lang/UnsupportedOperationException:<init>	()V
    //   80: aload_1
    //   81: athrow
    //   82: getstatic 66	com/truecaller/api/services/messenger/v1/h$b:c	Lcom/google/f/ah;
    //   85: astore_1
    //   86: aload_1
    //   87: ifnonnull +43 -> 130
    //   90: ldc 2
    //   92: astore_1
    //   93: aload_1
    //   94: monitorenter
    //   95: getstatic 66	com/truecaller/api/services/messenger/v1/h$b:c	Lcom/google/f/ah;
    //   98: astore_2
    //   99: aload_2
    //   100: ifnonnull +20 -> 120
    //   103: new 68	com/google/f/q$b
    //   106: astore_2
    //   107: getstatic 18	com/truecaller/api/services/messenger/v1/h$b:b	Lcom/truecaller/api/services/messenger/v1/h$b;
    //   110: astore_3
    //   111: aload_2
    //   112: aload_3
    //   113: invokespecial 71	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   116: aload_2
    //   117: putstatic 66	com/truecaller/api/services/messenger/v1/h$b:c	Lcom/google/f/ah;
    //   120: aload_1
    //   121: monitorexit
    //   122: goto +8 -> 130
    //   125: astore_2
    //   126: aload_1
    //   127: monitorexit
    //   128: aload_2
    //   129: athrow
    //   130: getstatic 66	com/truecaller/api/services/messenger/v1/h$b:c	Lcom/google/f/ah;
    //   133: areturn
    //   134: aload_2
    //   135: checkcast 73	com/google/f/g
    //   138: astore_2
    //   139: iconst_1
    //   140: istore 5
    //   142: iload 6
    //   144: ifne +313 -> 457
    //   147: aload_2
    //   148: invokevirtual 77	com/google/f/g:readTag	()I
    //   151: istore 7
    //   153: iload 7
    //   155: ifeq +240 -> 395
    //   158: iload 7
    //   160: tableswitch	default:+24->184, 9:+180->340, 10:+43->203
    //   184: aload_2
    //   185: iload 7
    //   187: invokevirtual 81	com/google/f/g:skipField	(I)Z
    //   190: istore 7
    //   192: iload 7
    //   194: ifne -52 -> 142
    //   197: iconst_1
    //   198: istore 6
    //   200: goto -58 -> 142
    //   203: aload_2
    //   204: invokevirtual 84	com/google/f/g:readRawVarint32	()I
    //   207: istore 7
    //   209: aload_2
    //   210: iload 7
    //   212: invokevirtual 88	com/google/f/g:pushLimit	(I)I
    //   215: istore 8
    //   217: aload_0
    //   218: getfield 28	com/truecaller/api/services/messenger/v1/h$b:a	Lcom/google/f/w$g;
    //   221: astore 9
    //   223: aload 9
    //   225: invokeinterface 40 1 0
    //   230: istore 10
    //   232: iload 10
    //   234: ifne +64 -> 298
    //   237: aload_2
    //   238: invokevirtual 91	com/google/f/g:getBytesUntilLimit	()I
    //   241: istore 10
    //   243: iload 10
    //   245: ifle +53 -> 298
    //   248: aload_0
    //   249: getfield 28	com/truecaller/api/services/messenger/v1/h$b:a	Lcom/google/f/w$g;
    //   252: astore 9
    //   254: aload 9
    //   256: invokeinterface 94 1 0
    //   261: istore 10
    //   263: aload_0
    //   264: getfield 28	com/truecaller/api/services/messenger/v1/h$b:a	Lcom/google/f/w$g;
    //   267: astore 11
    //   269: iload 7
    //   271: bipush 8
    //   273: idiv
    //   274: istore 7
    //   276: iload 10
    //   278: iload 7
    //   280: iadd
    //   281: istore 10
    //   283: aload 11
    //   285: iload 10
    //   287: invokeinterface 98 2 0
    //   292: astore_3
    //   293: aload_0
    //   294: aload_3
    //   295: putfield 28	com/truecaller/api/services/messenger/v1/h$b:a	Lcom/google/f/w$g;
    //   298: aload_2
    //   299: invokevirtual 91	com/google/f/g:getBytesUntilLimit	()I
    //   302: istore 7
    //   304: iload 7
    //   306: ifle +25 -> 331
    //   309: aload_0
    //   310: getfield 28	com/truecaller/api/services/messenger/v1/h$b:a	Lcom/google/f/w$g;
    //   313: astore_3
    //   314: aload_2
    //   315: invokevirtual 102	com/google/f/g:readFixed64	()J
    //   318: lstore 12
    //   320: aload_3
    //   321: lload 12
    //   323: invokeinterface 106 3 0
    //   328: goto -30 -> 298
    //   331: aload_2
    //   332: iload 8
    //   334: invokevirtual 110	com/google/f/g:popLimit	(I)V
    //   337: goto -195 -> 142
    //   340: aload_0
    //   341: getfield 28	com/truecaller/api/services/messenger/v1/h$b:a	Lcom/google/f/w$g;
    //   344: astore_3
    //   345: aload_3
    //   346: invokeinterface 40 1 0
    //   351: istore 7
    //   353: iload 7
    //   355: ifne +18 -> 373
    //   358: aload_0
    //   359: getfield 28	com/truecaller/api/services/messenger/v1/h$b:a	Lcom/google/f/w$g;
    //   362: astore_3
    //   363: aload_3
    //   364: invokestatic 44	com/google/f/q:mutableCopy	(Lcom/google/f/w$g;)Lcom/google/f/w$g;
    //   367: astore_3
    //   368: aload_0
    //   369: aload_3
    //   370: putfield 28	com/truecaller/api/services/messenger/v1/h$b:a	Lcom/google/f/w$g;
    //   373: aload_0
    //   374: getfield 28	com/truecaller/api/services/messenger/v1/h$b:a	Lcom/google/f/w$g;
    //   377: astore_3
    //   378: aload_2
    //   379: invokevirtual 102	com/google/f/g:readFixed64	()J
    //   382: lstore 14
    //   384: aload_3
    //   385: lload 14
    //   387: invokeinterface 106 3 0
    //   392: goto -250 -> 142
    //   395: iconst_1
    //   396: istore 6
    //   398: goto -256 -> 142
    //   401: astore_1
    //   402: goto +53 -> 455
    //   405: astore_1
    //   406: new 112	java/lang/RuntimeException
    //   409: astore_2
    //   410: new 114	com/google/f/x
    //   413: astore_3
    //   414: aload_1
    //   415: invokevirtual 120	java/io/IOException:getMessage	()Ljava/lang/String;
    //   418: astore_1
    //   419: aload_3
    //   420: aload_1
    //   421: invokespecial 123	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   424: aload_3
    //   425: aload_0
    //   426: invokevirtual 127	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   429: astore_1
    //   430: aload_2
    //   431: aload_1
    //   432: invokespecial 130	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   435: aload_2
    //   436: athrow
    //   437: astore_1
    //   438: new 112	java/lang/RuntimeException
    //   441: astore_2
    //   442: aload_1
    //   443: aload_0
    //   444: invokevirtual 127	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   447: astore_1
    //   448: aload_2
    //   449: aload_1
    //   450: invokespecial 130	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   453: aload_2
    //   454: athrow
    //   455: aload_1
    //   456: athrow
    //   457: getstatic 18	com/truecaller/api/services/messenger/v1/h$b:b	Lcom/truecaller/api/services/messenger/v1/h$b;
    //   460: areturn
    //   461: aload_2
    //   462: checkcast 132	com/google/f/q$k
    //   465: astore_2
    //   466: aload_3
    //   467: checkcast 2	com/truecaller/api/services/messenger/v1/h$b
    //   470: astore_3
    //   471: aload_0
    //   472: getfield 28	com/truecaller/api/services/messenger/v1/h$b:a	Lcom/google/f/w$g;
    //   475: astore_1
    //   476: aload_3
    //   477: getfield 28	com/truecaller/api/services/messenger/v1/h$b:a	Lcom/google/f/w$g;
    //   480: astore_3
    //   481: aload_2
    //   482: aload_1
    //   483: aload_3
    //   484: invokeinterface 136 3 0
    //   489: astore_1
    //   490: aload_0
    //   491: aload_1
    //   492: putfield 28	com/truecaller/api/services/messenger/v1/h$b:a	Lcom/google/f/w$g;
    //   495: aload_0
    //   496: areturn
    //   497: new 34	com/truecaller/api/services/messenger/v1/h$b$a
    //   500: astore_1
    //   501: aload_1
    //   502: iconst_0
    //   503: invokespecial 139	com/truecaller/api/services/messenger/v1/h$b$a:<init>	(B)V
    //   506: aload_1
    //   507: areturn
    //   508: aload_0
    //   509: getfield 28	com/truecaller/api/services/messenger/v1/h$b:a	Lcom/google/f/w$g;
    //   512: invokeinterface 140 1 0
    //   517: aconst_null
    //   518: areturn
    //   519: getstatic 18	com/truecaller/api/services/messenger/v1/h$b:b	Lcom/truecaller/api/services/messenger/v1/h$b;
    //   522: areturn
    //   523: new 2	com/truecaller/api/services/messenger/v1/h$b
    //   526: astore_1
    //   527: aload_1
    //   528: invokespecial 16	com/truecaller/api/services/messenger/v1/h$b:<init>	()V
    //   531: aload_1
    //   532: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	533	0	this	b
    //   0	533	1	paramj	com.google.f.q.j
    //   0	533	2	paramObject1	Object
    //   0	533	3	paramObject2	Object
    //   3	20	4	arrayOfInt	int[]
    //   9	132	5	i	int
    //   19	378	6	j	int
    //   151	35	7	k	int
    //   190	3	7	bool1	boolean
    //   207	98	7	m	int
    //   351	3	7	bool2	boolean
    //   215	118	8	n	int
    //   221	34	9	localg1	w.g
    //   230	3	10	bool3	boolean
    //   241	45	10	i1	int
    //   267	17	11	localg2	w.g
    //   318	4	12	l1	long
    //   382	4	14	l2	long
    // Exception table:
    //   from	to	target	type
    //   95	98	125	finally
    //   103	106	125	finally
    //   107	110	125	finally
    //   112	116	125	finally
    //   116	120	125	finally
    //   120	122	125	finally
    //   126	128	125	finally
    //   147	151	401	finally
    //   185	190	401	finally
    //   203	207	401	finally
    //   210	215	401	finally
    //   217	221	401	finally
    //   223	230	401	finally
    //   237	241	401	finally
    //   248	252	401	finally
    //   254	261	401	finally
    //   263	267	401	finally
    //   271	274	401	finally
    //   285	292	401	finally
    //   294	298	401	finally
    //   298	302	401	finally
    //   309	313	401	finally
    //   314	318	401	finally
    //   321	328	401	finally
    //   332	337	401	finally
    //   340	344	401	finally
    //   345	351	401	finally
    //   358	362	401	finally
    //   363	367	401	finally
    //   369	373	401	finally
    //   373	377	401	finally
    //   378	382	401	finally
    //   385	392	401	finally
    //   406	409	401	finally
    //   410	413	401	finally
    //   414	418	401	finally
    //   420	424	401	finally
    //   425	429	401	finally
    //   431	435	401	finally
    //   435	437	401	finally
    //   438	441	401	finally
    //   443	447	401	finally
    //   449	453	401	finally
    //   453	455	401	finally
    //   147	151	405	java/io/IOException
    //   185	190	405	java/io/IOException
    //   203	207	405	java/io/IOException
    //   210	215	405	java/io/IOException
    //   217	221	405	java/io/IOException
    //   223	230	405	java/io/IOException
    //   237	241	405	java/io/IOException
    //   248	252	405	java/io/IOException
    //   254	261	405	java/io/IOException
    //   263	267	405	java/io/IOException
    //   271	274	405	java/io/IOException
    //   285	292	405	java/io/IOException
    //   294	298	405	java/io/IOException
    //   298	302	405	java/io/IOException
    //   309	313	405	java/io/IOException
    //   314	318	405	java/io/IOException
    //   321	328	405	java/io/IOException
    //   332	337	405	java/io/IOException
    //   340	344	405	java/io/IOException
    //   345	351	405	java/io/IOException
    //   358	362	405	java/io/IOException
    //   363	367	405	java/io/IOException
    //   369	373	405	java/io/IOException
    //   373	377	405	java/io/IOException
    //   378	382	405	java/io/IOException
    //   385	392	405	java/io/IOException
    //   147	151	437	com/google/f/x
    //   185	190	437	com/google/f/x
    //   203	207	437	com/google/f/x
    //   210	215	437	com/google/f/x
    //   217	221	437	com/google/f/x
    //   223	230	437	com/google/f/x
    //   237	241	437	com/google/f/x
    //   248	252	437	com/google/f/x
    //   254	261	437	com/google/f/x
    //   263	267	437	com/google/f/x
    //   271	274	437	com/google/f/x
    //   285	292	437	com/google/f/x
    //   294	298	437	com/google/f/x
    //   298	302	437	com/google/f/x
    //   309	313	437	com/google/f/x
    //   314	318	437	com/google/f/x
    //   321	328	437	com/google/f/x
    //   332	337	437	com/google/f/x
    //   340	344	437	com/google/f/x
    //   345	351	437	com/google/f/x
    //   358	362	437	com/google/f/x
    //   363	367	437	com/google/f/x
    //   369	373	437	com/google/f/x
    //   373	377	437	com/google/f/x
    //   378	382	437	com/google/f/x
    //   385	392	437	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int j = -1;
    if (i != j) {
      return i;
    }
    i = a.size() * 8 + 0;
    j = a.size() * 1;
    i += j;
    memoizedSerializedSize = i;
    return i;
  }
  
  public final void writeTo(h paramh)
  {
    getSerializedSize();
    int i = 0;
    for (;;)
    {
      w.g localg = a;
      int j = localg.size();
      if (i >= j) {
        break;
      }
      localg = a;
      long l = localg.getLong(i);
      int k = 1;
      paramh.writeFixed64(k, l);
      i += 1;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.h.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */