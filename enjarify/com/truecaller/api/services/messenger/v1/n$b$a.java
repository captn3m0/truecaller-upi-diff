package com.truecaller.api.services.messenger.v1;

import com.google.f.q.a;
import com.truecaller.api.services.messenger.v1.models.input.InputPeer;

public final class n$b$a
  extends q.a
  implements n.c
{
  private n$b$a()
  {
    super(localb);
  }
  
  public final a a(long paramLong)
  {
    copyOnWrite();
    n.b.a((n.b)instance, paramLong);
    return this;
  }
  
  public final a a(InputPeer paramInputPeer)
  {
    copyOnWrite();
    n.b.a((n.b)instance, paramInputPeer);
    return this;
  }
  
  public final a b(InputPeer paramInputPeer)
  {
    copyOnWrite();
    n.b.b((n.b)instance, paramInputPeer);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.n.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */