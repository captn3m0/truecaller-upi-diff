package com.truecaller.api.services.messenger.v1;

import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;

public final class MediaHandles$Request
  extends q
  implements MediaHandles.b
{
  private static final Request d;
  private static volatile ah e;
  private long a;
  private String b = "";
  private int c;
  
  static
  {
    Request localRequest = new com/truecaller/api/services/messenger/v1/MediaHandles$Request;
    localRequest.<init>();
    d = localRequest;
    localRequest.makeImmutable();
  }
  
  public static MediaHandles.Request.a a()
  {
    return (MediaHandles.Request.a)d.toBuilder();
  }
  
  public static Request b()
  {
    return d;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 46	com/truecaller/api/services/messenger/v1/MediaHandles$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 52	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_0
    //   19: istore 6
    //   21: aconst_null
    //   22: astore 4
    //   24: iconst_1
    //   25: istore 7
    //   27: iload 5
    //   29: tableswitch	default:+47->76, 1:+555->584, 2:+551->580, 3:+549->578, 4:+538->567, 5:+291->320, 6:+109->138, 7:+287->316, 8:+57->86
    //   76: new 55	java/lang/UnsupportedOperationException
    //   79: astore_1
    //   80: aload_1
    //   81: invokespecial 56	java/lang/UnsupportedOperationException:<init>	()V
    //   84: aload_1
    //   85: athrow
    //   86: getstatic 58	com/truecaller/api/services/messenger/v1/MediaHandles$Request:e	Lcom/google/f/ah;
    //   89: astore_1
    //   90: aload_1
    //   91: ifnonnull +43 -> 134
    //   94: ldc 2
    //   96: astore_1
    //   97: aload_1
    //   98: monitorenter
    //   99: getstatic 58	com/truecaller/api/services/messenger/v1/MediaHandles$Request:e	Lcom/google/f/ah;
    //   102: astore_2
    //   103: aload_2
    //   104: ifnonnull +20 -> 124
    //   107: new 60	com/google/f/q$b
    //   110: astore_2
    //   111: getstatic 22	com/truecaller/api/services/messenger/v1/MediaHandles$Request:d	Lcom/truecaller/api/services/messenger/v1/MediaHandles$Request;
    //   114: astore_3
    //   115: aload_2
    //   116: aload_3
    //   117: invokespecial 63	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   120: aload_2
    //   121: putstatic 58	com/truecaller/api/services/messenger/v1/MediaHandles$Request:e	Lcom/google/f/ah;
    //   124: aload_1
    //   125: monitorexit
    //   126: goto +8 -> 134
    //   129: astore_2
    //   130: aload_1
    //   131: monitorexit
    //   132: aload_2
    //   133: athrow
    //   134: getstatic 58	com/truecaller/api/services/messenger/v1/MediaHandles$Request:e	Lcom/google/f/ah;
    //   137: areturn
    //   138: aload_2
    //   139: checkcast 65	com/google/f/g
    //   142: astore_2
    //   143: iload 6
    //   145: ifne +171 -> 316
    //   148: aload_2
    //   149: invokevirtual 68	com/google/f/g:readTag	()I
    //   152: istore 5
    //   154: iload 5
    //   156: ifeq +98 -> 254
    //   159: bipush 8
    //   161: istore 8
    //   163: iload 5
    //   165: iload 8
    //   167: if_icmpeq +72 -> 239
    //   170: bipush 18
    //   172: istore 8
    //   174: iload 5
    //   176: iload 8
    //   178: if_icmpeq +48 -> 226
    //   181: bipush 24
    //   183: istore 8
    //   185: iload 5
    //   187: iload 8
    //   189: if_icmpeq +22 -> 211
    //   192: aload_2
    //   193: iload 5
    //   195: invokevirtual 75	com/google/f/g:skipField	(I)Z
    //   198: istore 5
    //   200: iload 5
    //   202: ifne -59 -> 143
    //   205: iconst_1
    //   206: istore 6
    //   208: goto -65 -> 143
    //   211: aload_2
    //   212: invokevirtual 78	com/google/f/g:readEnum	()I
    //   215: istore 5
    //   217: aload_0
    //   218: iload 5
    //   220: putfield 80	com/truecaller/api/services/messenger/v1/MediaHandles$Request:c	I
    //   223: goto -80 -> 143
    //   226: aload_2
    //   227: invokevirtual 84	com/google/f/g:readStringRequireUtf8	()Ljava/lang/String;
    //   230: astore_1
    //   231: aload_0
    //   232: aload_1
    //   233: putfield 30	com/truecaller/api/services/messenger/v1/MediaHandles$Request:b	Ljava/lang/String;
    //   236: goto -93 -> 143
    //   239: aload_2
    //   240: invokevirtual 88	com/google/f/g:readInt64	()J
    //   243: lstore 9
    //   245: aload_0
    //   246: lload 9
    //   248: putfield 38	com/truecaller/api/services/messenger/v1/MediaHandles$Request:a	J
    //   251: goto -108 -> 143
    //   254: iconst_1
    //   255: istore 6
    //   257: goto -114 -> 143
    //   260: astore_1
    //   261: goto +53 -> 314
    //   264: astore_1
    //   265: new 90	java/lang/RuntimeException
    //   268: astore_2
    //   269: new 92	com/google/f/x
    //   272: astore_3
    //   273: aload_1
    //   274: invokevirtual 97	java/io/IOException:getMessage	()Ljava/lang/String;
    //   277: astore_1
    //   278: aload_3
    //   279: aload_1
    //   280: invokespecial 100	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   283: aload_3
    //   284: aload_0
    //   285: invokevirtual 104	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   288: astore_1
    //   289: aload_2
    //   290: aload_1
    //   291: invokespecial 107	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   294: aload_2
    //   295: athrow
    //   296: astore_1
    //   297: new 90	java/lang/RuntimeException
    //   300: astore_2
    //   301: aload_1
    //   302: aload_0
    //   303: invokevirtual 104	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   306: astore_1
    //   307: aload_2
    //   308: aload_1
    //   309: invokespecial 107	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   312: aload_2
    //   313: athrow
    //   314: aload_1
    //   315: athrow
    //   316: getstatic 22	com/truecaller/api/services/messenger/v1/MediaHandles$Request:d	Lcom/truecaller/api/services/messenger/v1/MediaHandles$Request;
    //   319: areturn
    //   320: aload_2
    //   321: astore_1
    //   322: aload_2
    //   323: checkcast 109	com/google/f/q$k
    //   326: astore_1
    //   327: aload_3
    //   328: checkcast 2	com/truecaller/api/services/messenger/v1/MediaHandles$Request
    //   331: astore_3
    //   332: aload_0
    //   333: getfield 38	com/truecaller/api/services/messenger/v1/MediaHandles$Request:a	J
    //   336: lstore 9
    //   338: lconst_0
    //   339: lstore 11
    //   341: lload 9
    //   343: lload 11
    //   345: lcmp
    //   346: istore 13
    //   348: iload 13
    //   350: ifeq +9 -> 359
    //   353: iconst_1
    //   354: istore 14
    //   356: goto +9 -> 365
    //   359: iconst_0
    //   360: istore 14
    //   362: aconst_null
    //   363: astore 15
    //   365: aload_0
    //   366: getfield 38	com/truecaller/api/services/messenger/v1/MediaHandles$Request:a	J
    //   369: lstore 16
    //   371: aload_3
    //   372: getfield 38	com/truecaller/api/services/messenger/v1/MediaHandles$Request:a	J
    //   375: lstore 18
    //   377: lload 18
    //   379: lload 11
    //   381: lcmp
    //   382: istore 13
    //   384: iload 13
    //   386: ifeq +9 -> 395
    //   389: iconst_1
    //   390: istore 13
    //   392: goto +8 -> 400
    //   395: iconst_0
    //   396: istore 13
    //   398: aconst_null
    //   399: astore_2
    //   400: aload_3
    //   401: getfield 38	com/truecaller/api/services/messenger/v1/MediaHandles$Request:a	J
    //   404: lstore 18
    //   406: aload_1
    //   407: astore 20
    //   409: lload 16
    //   411: lstore 11
    //   413: aload_1
    //   414: iload 14
    //   416: lload 16
    //   418: iload 13
    //   420: lload 18
    //   422: invokeinterface 113 7 0
    //   427: lstore 9
    //   429: aload_0
    //   430: lload 9
    //   432: putfield 38	com/truecaller/api/services/messenger/v1/MediaHandles$Request:a	J
    //   435: aload_0
    //   436: getfield 30	com/truecaller/api/services/messenger/v1/MediaHandles$Request:b	Ljava/lang/String;
    //   439: invokevirtual 119	java/lang/String:isEmpty	()Z
    //   442: iload 7
    //   444: ixor
    //   445: istore 13
    //   447: aload_0
    //   448: getfield 30	com/truecaller/api/services/messenger/v1/MediaHandles$Request:b	Ljava/lang/String;
    //   451: astore 20
    //   453: aload_3
    //   454: getfield 30	com/truecaller/api/services/messenger/v1/MediaHandles$Request:b	Ljava/lang/String;
    //   457: astore 15
    //   459: aload 15
    //   461: invokevirtual 119	java/lang/String:isEmpty	()Z
    //   464: iload 7
    //   466: ixor
    //   467: istore 14
    //   469: aload_3
    //   470: getfield 30	com/truecaller/api/services/messenger/v1/MediaHandles$Request:b	Ljava/lang/String;
    //   473: astore 21
    //   475: aload_1
    //   476: iload 13
    //   478: aload 20
    //   480: iload 14
    //   482: aload 21
    //   484: invokeinterface 123 5 0
    //   489: astore_2
    //   490: aload_0
    //   491: aload_2
    //   492: putfield 30	com/truecaller/api/services/messenger/v1/MediaHandles$Request:b	Ljava/lang/String;
    //   495: aload_0
    //   496: getfield 80	com/truecaller/api/services/messenger/v1/MediaHandles$Request:c	I
    //   499: istore 13
    //   501: iload 13
    //   503: ifeq +9 -> 512
    //   506: iconst_1
    //   507: istore 13
    //   509: goto +8 -> 517
    //   512: iconst_0
    //   513: istore 13
    //   515: aconst_null
    //   516: astore_2
    //   517: aload_0
    //   518: getfield 80	com/truecaller/api/services/messenger/v1/MediaHandles$Request:c	I
    //   521: istore 22
    //   523: aload_3
    //   524: getfield 80	com/truecaller/api/services/messenger/v1/MediaHandles$Request:c	I
    //   527: istore 14
    //   529: iload 14
    //   531: ifeq +6 -> 537
    //   534: iconst_1
    //   535: istore 6
    //   537: aload_3
    //   538: getfield 80	com/truecaller/api/services/messenger/v1/MediaHandles$Request:c	I
    //   541: istore 8
    //   543: aload_1
    //   544: iload 13
    //   546: iload 22
    //   548: iload 6
    //   550: iload 8
    //   552: invokeinterface 127 5 0
    //   557: istore 5
    //   559: aload_0
    //   560: iload 5
    //   562: putfield 80	com/truecaller/api/services/messenger/v1/MediaHandles$Request:c	I
    //   565: aload_0
    //   566: areturn
    //   567: new 36	com/truecaller/api/services/messenger/v1/MediaHandles$Request$a
    //   570: astore_1
    //   571: aload_1
    //   572: iconst_0
    //   573: invokespecial 130	com/truecaller/api/services/messenger/v1/MediaHandles$Request$a:<init>	(B)V
    //   576: aload_1
    //   577: areturn
    //   578: aconst_null
    //   579: areturn
    //   580: getstatic 22	com/truecaller/api/services/messenger/v1/MediaHandles$Request:d	Lcom/truecaller/api/services/messenger/v1/MediaHandles$Request;
    //   583: areturn
    //   584: new 2	com/truecaller/api/services/messenger/v1/MediaHandles$Request
    //   587: astore_1
    //   588: aload_1
    //   589: invokespecial 20	com/truecaller/api/services/messenger/v1/MediaHandles$Request:<init>	()V
    //   592: aload_1
    //   593: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	594	0	this	Request
    //   0	594	1	paramj	com.google.f.q.j
    //   0	594	2	paramObject1	Object
    //   0	594	3	paramObject2	Object
    //   3	20	4	arrayOfInt	int[]
    //   9	185	5	i	int
    //   198	3	5	bool1	boolean
    //   215	346	5	j	int
    //   19	530	6	bool2	boolean
    //   25	442	7	k	int
    //   161	390	8	m	int
    //   243	188	9	l1	long
    //   339	73	11	l2	long
    //   346	131	13	bool3	boolean
    //   499	46	13	n	int
    //   354	127	14	bool4	boolean
    //   527	3	14	i1	int
    //   363	97	15	str1	String
    //   369	48	16	l3	long
    //   375	46	18	l4	long
    //   407	72	20	localObject	Object
    //   473	10	21	str2	String
    //   521	26	22	i2	int
    // Exception table:
    //   from	to	target	type
    //   99	102	129	finally
    //   107	110	129	finally
    //   111	114	129	finally
    //   116	120	129	finally
    //   120	124	129	finally
    //   124	126	129	finally
    //   130	132	129	finally
    //   148	152	260	finally
    //   193	198	260	finally
    //   211	215	260	finally
    //   218	223	260	finally
    //   226	230	260	finally
    //   232	236	260	finally
    //   239	243	260	finally
    //   246	251	260	finally
    //   265	268	260	finally
    //   269	272	260	finally
    //   273	277	260	finally
    //   279	283	260	finally
    //   284	288	260	finally
    //   290	294	260	finally
    //   294	296	260	finally
    //   297	300	260	finally
    //   302	306	260	finally
    //   308	312	260	finally
    //   312	314	260	finally
    //   148	152	264	java/io/IOException
    //   193	198	264	java/io/IOException
    //   211	215	264	java/io/IOException
    //   218	223	264	java/io/IOException
    //   226	230	264	java/io/IOException
    //   232	236	264	java/io/IOException
    //   239	243	264	java/io/IOException
    //   246	251	264	java/io/IOException
    //   148	152	296	com/google/f/x
    //   193	198	296	com/google/f/x
    //   211	215	296	com/google/f/x
    //   218	223	296	com/google/f/x
    //   226	230	296	com/google/f/x
    //   232	236	296	com/google/f/x
    //   239	243	296	com/google/f/x
    //   246	251	296	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int k = -1;
    if (i != k) {
      return i;
    }
    long l1 = a;
    long l2 = 0L;
    int m = 0;
    boolean bool2 = l1 < l2;
    if (bool2)
    {
      int n = 1;
      i = h.computeInt64Size(n, l1);
      m = 0 + i;
    }
    String str = b;
    boolean bool1 = str.isEmpty();
    if (!bool1)
    {
      localObject = b;
      j = h.computeStringSize(2, (String)localObject);
      m += j;
    }
    int j = c;
    Object localObject = MediaHandles.Request.UploadType.MEDIA;
    k = ((MediaHandles.Request.UploadType)localObject).getNumber();
    if (j != k)
    {
      k = c;
      j = h.computeEnumSize(3, k);
      m += j;
    }
    memoizedSerializedSize = m;
    return m;
  }
  
  public final void writeTo(h paramh)
  {
    long l1 = a;
    long l2 = 0L;
    boolean bool1 = l1 < l2;
    if (bool1)
    {
      int i = 1;
      paramh.writeInt64(i, l1);
    }
    String str = b;
    boolean bool2 = str.isEmpty();
    if (!bool2)
    {
      j = 2;
      localObject = b;
      paramh.writeString(j, (String)localObject);
    }
    int j = c;
    Object localObject = MediaHandles.Request.UploadType.MEDIA;
    int k = ((MediaHandles.Request.UploadType)localObject).getNumber();
    if (j != k)
    {
      j = 3;
      k = c;
      paramh.writeEnum(j, k);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.MediaHandles.Request
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */