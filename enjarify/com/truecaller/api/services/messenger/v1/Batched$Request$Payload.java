package com.truecaller.api.services.messenger.v1;

import com.google.f.ae;
import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;

public final class Batched$Request$Payload
  extends q
  implements Batched.Request.b
{
  private static final Payload d;
  private static volatile ah e;
  private int a = 0;
  private Object b;
  private long c;
  
  static
  {
    Payload localPayload = new com/truecaller/api/services/messenger/v1/Batched$Request$Payload;
    localPayload.<init>();
    d = localPayload;
    localPayload.makeImmutable();
  }
  
  public static ah a()
  {
    return d.getParserForType();
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: aload_0
    //   1: astore 4
    //   3: getstatic 37	com/truecaller/api/services/messenger/v1/Batched$1:b	[I
    //   6: astore 5
    //   8: aload_1
    //   9: invokevirtual 43	com/google/f/q$j:ordinal	()I
    //   12: istore 6
    //   14: aload 5
    //   16: iload 6
    //   18: iaload
    //   19: istore 7
    //   21: iconst_4
    //   22: istore 6
    //   24: iconst_3
    //   25: istore 8
    //   27: iconst_2
    //   28: istore 9
    //   30: aconst_null
    //   31: astore 10
    //   33: iconst_1
    //   34: istore 11
    //   36: iconst_0
    //   37: istore 12
    //   39: iload 7
    //   41: tableswitch	default:+47->88, 1:+1164->1205, 2:+1160->1201, 3:+1158->1199, 4:+1144->1185, 5:+707->748, 6:+127->168, 7:+703->744, 8:+60->101
    //   88: new 49	java/lang/UnsupportedOperationException
    //   91: astore 5
    //   93: aload 5
    //   95: invokespecial 50	java/lang/UnsupportedOperationException:<init>	()V
    //   98: aload 5
    //   100: athrow
    //   101: getstatic 52	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:e	Lcom/google/f/ah;
    //   104: astore 5
    //   106: aload 5
    //   108: ifnonnull +56 -> 164
    //   111: ldc 2
    //   113: astore 13
    //   115: aload 13
    //   117: monitorenter
    //   118: getstatic 52	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:e	Lcom/google/f/ah;
    //   121: astore 5
    //   123: aload 5
    //   125: ifnonnull +25 -> 150
    //   128: new 54	com/google/f/q$b
    //   131: astore 5
    //   133: getstatic 22	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:d	Lcom/truecaller/api/services/messenger/v1/Batched$Request$Payload;
    //   136: astore 14
    //   138: aload 5
    //   140: aload 14
    //   142: invokespecial 57	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   145: aload 5
    //   147: putstatic 52	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:e	Lcom/google/f/ah;
    //   150: aload 13
    //   152: monitorexit
    //   153: goto +11 -> 164
    //   156: astore 5
    //   158: aload 13
    //   160: monitorexit
    //   161: aload 5
    //   163: athrow
    //   164: getstatic 52	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:e	Lcom/google/f/ah;
    //   167: areturn
    //   168: aload_2
    //   169: astore 5
    //   171: aload_2
    //   172: checkcast 59	com/google/f/g
    //   175: astore 5
    //   177: aload_3
    //   178: astore 15
    //   180: aload_3
    //   181: checkcast 61	com/google/f/n
    //   184: astore 15
    //   186: iload 12
    //   188: ifne +556 -> 744
    //   191: aload 5
    //   193: invokevirtual 64	com/google/f/g:readTag	()I
    //   196: istore 16
    //   198: iload 16
    //   200: ifeq +459 -> 659
    //   203: bipush 8
    //   205: istore 17
    //   207: iload 16
    //   209: iload 17
    //   211: if_icmpeq +431 -> 642
    //   214: bipush 18
    //   216: istore 17
    //   218: iload 16
    //   220: iload 17
    //   222: if_icmpeq +295 -> 517
    //   225: bipush 26
    //   227: istore 17
    //   229: iload 16
    //   231: iload 17
    //   233: if_icmpeq +159 -> 392
    //   236: bipush 34
    //   238: istore 17
    //   240: iload 16
    //   242: iload 17
    //   244: if_icmpeq +23 -> 267
    //   247: aload 5
    //   249: iload 16
    //   251: invokevirtual 72	com/google/f/g:skipField	(I)Z
    //   254: istore 16
    //   256: iload 16
    //   258: ifne -72 -> 186
    //   261: iconst_1
    //   262: istore 12
    //   264: goto -78 -> 186
    //   267: aload 4
    //   269: getfield 28	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:a	I
    //   272: istore 16
    //   274: iload 16
    //   276: iload 6
    //   278: if_icmpne +34 -> 312
    //   281: aload 4
    //   283: getfield 74	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:b	Ljava/lang/Object;
    //   286: astore 18
    //   288: aload 18
    //   290: checkcast 76	com/truecaller/api/services/messenger/v1/r$b
    //   293: astore 18
    //   295: aload 18
    //   297: invokevirtual 80	com/truecaller/api/services/messenger/v1/r$b:toBuilder	()Lcom/google/f/q$a;
    //   300: astore 18
    //   302: aload 18
    //   304: checkcast 82	com/truecaller/api/services/messenger/v1/r$b$a
    //   307: astore 18
    //   309: goto +9 -> 318
    //   312: iconst_0
    //   313: istore 16
    //   315: aconst_null
    //   316: astore 18
    //   318: invokestatic 84	com/truecaller/api/services/messenger/v1/r$b:c	()Lcom/google/f/ah;
    //   321: astore 19
    //   323: aload 5
    //   325: aload 19
    //   327: aload 15
    //   329: invokevirtual 88	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   332: astore 19
    //   334: aload 4
    //   336: aload 19
    //   338: putfield 74	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:b	Ljava/lang/Object;
    //   341: aload 18
    //   343: ifnull +39 -> 382
    //   346: aload 4
    //   348: getfield 74	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:b	Ljava/lang/Object;
    //   351: astore 19
    //   353: aload 19
    //   355: checkcast 76	com/truecaller/api/services/messenger/v1/r$b
    //   358: astore 19
    //   360: aload 18
    //   362: aload 19
    //   364: invokevirtual 92	com/truecaller/api/services/messenger/v1/r$b$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   367: pop
    //   368: aload 18
    //   370: invokevirtual 96	com/truecaller/api/services/messenger/v1/r$b$a:buildPartial	()Lcom/google/f/q;
    //   373: astore 18
    //   375: aload 4
    //   377: aload 18
    //   379: putfield 74	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:b	Ljava/lang/Object;
    //   382: aload 4
    //   384: iload 6
    //   386: putfield 28	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:a	I
    //   389: goto -203 -> 186
    //   392: aload 4
    //   394: getfield 28	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:a	I
    //   397: istore 16
    //   399: iload 16
    //   401: iload 8
    //   403: if_icmpne +34 -> 437
    //   406: aload 4
    //   408: getfield 74	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:b	Ljava/lang/Object;
    //   411: astore 18
    //   413: aload 18
    //   415: checkcast 98	com/truecaller/api/services/messenger/v1/t$b
    //   418: astore 18
    //   420: aload 18
    //   422: invokevirtual 99	com/truecaller/api/services/messenger/v1/t$b:toBuilder	()Lcom/google/f/q$a;
    //   425: astore 18
    //   427: aload 18
    //   429: checkcast 101	com/truecaller/api/services/messenger/v1/t$b$a
    //   432: astore 18
    //   434: goto +9 -> 443
    //   437: iconst_0
    //   438: istore 16
    //   440: aconst_null
    //   441: astore 18
    //   443: invokestatic 102	com/truecaller/api/services/messenger/v1/t$b:c	()Lcom/google/f/ah;
    //   446: astore 19
    //   448: aload 5
    //   450: aload 19
    //   452: aload 15
    //   454: invokevirtual 88	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   457: astore 19
    //   459: aload 4
    //   461: aload 19
    //   463: putfield 74	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:b	Ljava/lang/Object;
    //   466: aload 18
    //   468: ifnull +39 -> 507
    //   471: aload 4
    //   473: getfield 74	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:b	Ljava/lang/Object;
    //   476: astore 19
    //   478: aload 19
    //   480: checkcast 98	com/truecaller/api/services/messenger/v1/t$b
    //   483: astore 19
    //   485: aload 18
    //   487: aload 19
    //   489: invokevirtual 103	com/truecaller/api/services/messenger/v1/t$b$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   492: pop
    //   493: aload 18
    //   495: invokevirtual 104	com/truecaller/api/services/messenger/v1/t$b$a:buildPartial	()Lcom/google/f/q;
    //   498: astore 18
    //   500: aload 4
    //   502: aload 18
    //   504: putfield 74	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:b	Ljava/lang/Object;
    //   507: aload 4
    //   509: iload 8
    //   511: putfield 28	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:a	I
    //   514: goto -328 -> 186
    //   517: aload 4
    //   519: getfield 28	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:a	I
    //   522: istore 16
    //   524: iload 16
    //   526: iload 9
    //   528: if_icmpne +34 -> 562
    //   531: aload 4
    //   533: getfield 74	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:b	Ljava/lang/Object;
    //   536: astore 18
    //   538: aload 18
    //   540: checkcast 106	com/truecaller/api/services/messenger/v1/p$b
    //   543: astore 18
    //   545: aload 18
    //   547: invokevirtual 107	com/truecaller/api/services/messenger/v1/p$b:toBuilder	()Lcom/google/f/q$a;
    //   550: astore 18
    //   552: aload 18
    //   554: checkcast 109	com/truecaller/api/services/messenger/v1/p$b$a
    //   557: astore 18
    //   559: goto +9 -> 568
    //   562: iconst_0
    //   563: istore 16
    //   565: aconst_null
    //   566: astore 18
    //   568: invokestatic 110	com/truecaller/api/services/messenger/v1/p$b:c	()Lcom/google/f/ah;
    //   571: astore 19
    //   573: aload 5
    //   575: aload 19
    //   577: aload 15
    //   579: invokevirtual 88	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   582: astore 19
    //   584: aload 4
    //   586: aload 19
    //   588: putfield 74	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:b	Ljava/lang/Object;
    //   591: aload 18
    //   593: ifnull +39 -> 632
    //   596: aload 4
    //   598: getfield 74	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:b	Ljava/lang/Object;
    //   601: astore 19
    //   603: aload 19
    //   605: checkcast 106	com/truecaller/api/services/messenger/v1/p$b
    //   608: astore 19
    //   610: aload 18
    //   612: aload 19
    //   614: invokevirtual 111	com/truecaller/api/services/messenger/v1/p$b$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   617: pop
    //   618: aload 18
    //   620: invokevirtual 112	com/truecaller/api/services/messenger/v1/p$b$a:buildPartial	()Lcom/google/f/q;
    //   623: astore 18
    //   625: aload 4
    //   627: aload 18
    //   629: putfield 74	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:b	Ljava/lang/Object;
    //   632: aload 4
    //   634: iload 9
    //   636: putfield 28	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:a	I
    //   639: goto -453 -> 186
    //   642: aload 5
    //   644: invokevirtual 116	com/google/f/g:readInt64	()J
    //   647: lstore 20
    //   649: aload 4
    //   651: lload 20
    //   653: putfield 118	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:c	J
    //   656: goto -470 -> 186
    //   659: iconst_1
    //   660: istore 12
    //   662: goto -476 -> 186
    //   665: astore 5
    //   667: goto +74 -> 741
    //   670: astore 5
    //   672: new 120	java/lang/RuntimeException
    //   675: astore 13
    //   677: new 122	com/google/f/x
    //   680: astore 14
    //   682: aload 5
    //   684: invokevirtual 128	java/io/IOException:getMessage	()Ljava/lang/String;
    //   687: astore 5
    //   689: aload 14
    //   691: aload 5
    //   693: invokespecial 131	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   696: aload 14
    //   698: aload 4
    //   700: invokevirtual 135	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   703: astore 5
    //   705: aload 13
    //   707: aload 5
    //   709: invokespecial 138	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   712: aload 13
    //   714: athrow
    //   715: astore 5
    //   717: new 120	java/lang/RuntimeException
    //   720: astore 13
    //   722: aload 5
    //   724: aload 4
    //   726: invokevirtual 135	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   729: astore 5
    //   731: aload 13
    //   733: aload 5
    //   735: invokespecial 138	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   738: aload 13
    //   740: athrow
    //   741: aload 5
    //   743: athrow
    //   744: getstatic 22	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:d	Lcom/truecaller/api/services/messenger/v1/Batched$Request$Payload;
    //   747: areturn
    //   748: aload_2
    //   749: astore 5
    //   751: aload_2
    //   752: checkcast 140	com/google/f/q$k
    //   755: astore 5
    //   757: aload_3
    //   758: astore 10
    //   760: aload_3
    //   761: checkcast 2	com/truecaller/api/services/messenger/v1/Batched$Request$Payload
    //   764: astore 10
    //   766: aload_0
    //   767: getfield 118	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:c	J
    //   770: lstore 22
    //   772: lconst_0
    //   773: lstore 24
    //   775: lload 22
    //   777: lload 24
    //   779: lcmp
    //   780: istore 26
    //   782: iload 26
    //   784: ifeq +9 -> 793
    //   787: iconst_1
    //   788: istore 16
    //   790: goto +9 -> 799
    //   793: iconst_0
    //   794: istore 16
    //   796: aconst_null
    //   797: astore 18
    //   799: aload 4
    //   801: getfield 118	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:c	J
    //   804: lstore 27
    //   806: aload 10
    //   808: getfield 118	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:c	J
    //   811: lstore 29
    //   813: lload 29
    //   815: lload 24
    //   817: lcmp
    //   818: istore 31
    //   820: iload 31
    //   822: ifeq +9 -> 831
    //   825: iconst_1
    //   826: istore 32
    //   828: goto +6 -> 834
    //   831: iconst_0
    //   832: istore 32
    //   834: aload 10
    //   836: getfield 118	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:c	J
    //   839: lstore 24
    //   841: aload 5
    //   843: astore 15
    //   845: lload 24
    //   847: lstore 33
    //   849: lload 27
    //   851: lstore 24
    //   853: iload 32
    //   855: istore 26
    //   857: aload 5
    //   859: iload 16
    //   861: lload 27
    //   863: iload 32
    //   865: lload 33
    //   867: invokeinterface 144 7 0
    //   872: lstore 22
    //   874: aload 4
    //   876: lload 22
    //   878: putfield 118	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:c	J
    //   881: getstatic 146	com/truecaller/api/services/messenger/v1/Batched$1:a	[I
    //   884: astore 15
    //   886: aload 10
    //   888: getfield 28	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:a	I
    //   891: invokestatic 152	com/truecaller/api/services/messenger/v1/Batched$Request$Payload$DataCase:forNumber	(I)Lcom/truecaller/api/services/messenger/v1/Batched$Request$Payload$DataCase;
    //   894: astore 18
    //   896: aload 18
    //   898: invokevirtual 153	com/truecaller/api/services/messenger/v1/Batched$Request$Payload$DataCase:ordinal	()I
    //   901: istore 16
    //   903: aload 15
    //   905: iload 16
    //   907: iaload
    //   908: istore 31
    //   910: iload 31
    //   912: tableswitch	default:+32->944, 1:+183->1095, 2:+124->1036, 3:+65->977, 4:+35->947
    //   944: goto +207 -> 1151
    //   947: aload 4
    //   949: getfield 28	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:a	I
    //   952: istore 6
    //   954: iload 6
    //   956: ifeq +6 -> 962
    //   959: goto +6 -> 965
    //   962: iconst_0
    //   963: istore 11
    //   965: aload 5
    //   967: iload 11
    //   969: invokeinterface 157 2 0
    //   974: goto +177 -> 1151
    //   977: aload 4
    //   979: getfield 28	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:a	I
    //   982: istore 8
    //   984: iload 8
    //   986: iload 6
    //   988: if_icmpne +6 -> 994
    //   991: goto +6 -> 997
    //   994: iconst_0
    //   995: istore 11
    //   997: aload 4
    //   999: getfield 74	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:b	Ljava/lang/Object;
    //   1002: astore 13
    //   1004: aload 10
    //   1006: getfield 74	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:b	Ljava/lang/Object;
    //   1009: astore 14
    //   1011: aload 5
    //   1013: iload 11
    //   1015: aload 13
    //   1017: aload 14
    //   1019: invokeinterface 161 4 0
    //   1024: astore 13
    //   1026: aload 4
    //   1028: aload 13
    //   1030: putfield 74	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:b	Ljava/lang/Object;
    //   1033: goto +118 -> 1151
    //   1036: aload 4
    //   1038: getfield 28	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:a	I
    //   1041: istore 6
    //   1043: iload 6
    //   1045: iload 8
    //   1047: if_icmpne +6 -> 1053
    //   1050: goto +6 -> 1056
    //   1053: iconst_0
    //   1054: istore 11
    //   1056: aload 4
    //   1058: getfield 74	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:b	Ljava/lang/Object;
    //   1061: astore 13
    //   1063: aload 10
    //   1065: getfield 74	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:b	Ljava/lang/Object;
    //   1068: astore 14
    //   1070: aload 5
    //   1072: iload 11
    //   1074: aload 13
    //   1076: aload 14
    //   1078: invokeinterface 161 4 0
    //   1083: astore 13
    //   1085: aload 4
    //   1087: aload 13
    //   1089: putfield 74	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:b	Ljava/lang/Object;
    //   1092: goto +59 -> 1151
    //   1095: aload 4
    //   1097: getfield 28	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:a	I
    //   1100: istore 6
    //   1102: iload 6
    //   1104: iload 9
    //   1106: if_icmpne +6 -> 1112
    //   1109: goto +6 -> 1115
    //   1112: iconst_0
    //   1113: istore 11
    //   1115: aload 4
    //   1117: getfield 74	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:b	Ljava/lang/Object;
    //   1120: astore 13
    //   1122: aload 10
    //   1124: getfield 74	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:b	Ljava/lang/Object;
    //   1127: astore 14
    //   1129: aload 5
    //   1131: iload 11
    //   1133: aload 13
    //   1135: aload 14
    //   1137: invokeinterface 161 4 0
    //   1142: astore 13
    //   1144: aload 4
    //   1146: aload 13
    //   1148: putfield 74	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:b	Ljava/lang/Object;
    //   1151: getstatic 167	com/google/f/q$i:INSTANCE	Lcom/google/f/q$i;
    //   1154: astore 13
    //   1156: aload 5
    //   1158: aload 13
    //   1160: if_acmpne +22 -> 1182
    //   1163: aload 10
    //   1165: getfield 28	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:a	I
    //   1168: istore 7
    //   1170: iload 7
    //   1172: ifeq +10 -> 1182
    //   1175: aload 4
    //   1177: iload 7
    //   1179: putfield 28	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:a	I
    //   1182: aload 4
    //   1184: areturn
    //   1185: new 169	com/truecaller/api/services/messenger/v1/Batched$Request$Payload$a
    //   1188: astore 5
    //   1190: aload 5
    //   1192: iconst_0
    //   1193: invokespecial 172	com/truecaller/api/services/messenger/v1/Batched$Request$Payload$a:<init>	(B)V
    //   1196: aload 5
    //   1198: areturn
    //   1199: aconst_null
    //   1200: areturn
    //   1201: getstatic 22	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:d	Lcom/truecaller/api/services/messenger/v1/Batched$Request$Payload;
    //   1204: areturn
    //   1205: new 2	com/truecaller/api/services/messenger/v1/Batched$Request$Payload
    //   1208: astore 5
    //   1210: aload 5
    //   1212: invokespecial 20	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:<init>	()V
    //   1215: aload 5
    //   1217: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1218	0	this	Payload
    //   0	1218	1	paramj	com.google.f.q.j
    //   0	1218	2	paramObject1	Object
    //   0	1218	3	paramObject2	Object
    //   1	1182	4	localPayload	Payload
    //   6	140	5	localObject1	Object
    //   156	6	5	localObject2	Object
    //   169	474	5	localObject3	Object
    //   665	1	5	localObject4	Object
    //   670	13	5	localIOException	java.io.IOException
    //   687	21	5	localObject5	Object
    //   715	8	5	localx	com.google.f.x
    //   729	487	5	localObject6	Object
    //   12	1095	6	i	int
    //   19	1159	7	j	int
    //   25	1023	8	k	int
    //   28	1079	9	m	int
    //   31	1133	10	localObject7	Object
    //   34	1098	11	bool1	boolean
    //   37	624	12	n	int
    //   113	1046	13	localObject8	Object
    //   136	1000	14	localObject9	Object
    //   178	726	15	localObject10	Object
    //   196	54	16	i1	int
    //   254	3	16	bool2	boolean
    //   272	588	16	i2	int
    //   901	5	16	i3	int
    //   205	40	17	i4	int
    //   286	611	18	localObject11	Object
    //   321	292	19	localObject12	Object
    //   647	5	20	l1	long
    //   770	107	22	l2	long
    //   773	79	24	l3	long
    //   780	76	26	bool3	boolean
    //   804	58	27	l4	long
    //   811	3	29	l5	long
    //   818	3	31	bool4	boolean
    //   908	3	31	i5	int
    //   826	38	32	bool5	boolean
    //   847	19	33	l6	long
    // Exception table:
    //   from	to	target	type
    //   118	121	156	finally
    //   128	131	156	finally
    //   133	136	156	finally
    //   140	145	156	finally
    //   145	150	156	finally
    //   150	153	156	finally
    //   158	161	156	finally
    //   191	196	665	finally
    //   249	254	665	finally
    //   267	272	665	finally
    //   281	286	665	finally
    //   288	293	665	finally
    //   295	300	665	finally
    //   302	307	665	finally
    //   318	321	665	finally
    //   327	332	665	finally
    //   336	341	665	finally
    //   346	351	665	finally
    //   353	358	665	finally
    //   362	368	665	finally
    //   368	373	665	finally
    //   377	382	665	finally
    //   384	389	665	finally
    //   392	397	665	finally
    //   406	411	665	finally
    //   413	418	665	finally
    //   420	425	665	finally
    //   427	432	665	finally
    //   443	446	665	finally
    //   452	457	665	finally
    //   461	466	665	finally
    //   471	476	665	finally
    //   478	483	665	finally
    //   487	493	665	finally
    //   493	498	665	finally
    //   502	507	665	finally
    //   509	514	665	finally
    //   517	522	665	finally
    //   531	536	665	finally
    //   538	543	665	finally
    //   545	550	665	finally
    //   552	557	665	finally
    //   568	571	665	finally
    //   577	582	665	finally
    //   586	591	665	finally
    //   596	601	665	finally
    //   603	608	665	finally
    //   612	618	665	finally
    //   618	623	665	finally
    //   627	632	665	finally
    //   634	639	665	finally
    //   642	647	665	finally
    //   651	656	665	finally
    //   672	675	665	finally
    //   677	680	665	finally
    //   682	687	665	finally
    //   691	696	665	finally
    //   698	703	665	finally
    //   707	712	665	finally
    //   712	715	665	finally
    //   717	720	665	finally
    //   724	729	665	finally
    //   733	738	665	finally
    //   738	741	665	finally
    //   191	196	670	java/io/IOException
    //   249	254	670	java/io/IOException
    //   267	272	670	java/io/IOException
    //   281	286	670	java/io/IOException
    //   288	293	670	java/io/IOException
    //   295	300	670	java/io/IOException
    //   302	307	670	java/io/IOException
    //   318	321	670	java/io/IOException
    //   327	332	670	java/io/IOException
    //   336	341	670	java/io/IOException
    //   346	351	670	java/io/IOException
    //   353	358	670	java/io/IOException
    //   362	368	670	java/io/IOException
    //   368	373	670	java/io/IOException
    //   377	382	670	java/io/IOException
    //   384	389	670	java/io/IOException
    //   392	397	670	java/io/IOException
    //   406	411	670	java/io/IOException
    //   413	418	670	java/io/IOException
    //   420	425	670	java/io/IOException
    //   427	432	670	java/io/IOException
    //   443	446	670	java/io/IOException
    //   452	457	670	java/io/IOException
    //   461	466	670	java/io/IOException
    //   471	476	670	java/io/IOException
    //   478	483	670	java/io/IOException
    //   487	493	670	java/io/IOException
    //   493	498	670	java/io/IOException
    //   502	507	670	java/io/IOException
    //   509	514	670	java/io/IOException
    //   517	522	670	java/io/IOException
    //   531	536	670	java/io/IOException
    //   538	543	670	java/io/IOException
    //   545	550	670	java/io/IOException
    //   552	557	670	java/io/IOException
    //   568	571	670	java/io/IOException
    //   577	582	670	java/io/IOException
    //   586	591	670	java/io/IOException
    //   596	601	670	java/io/IOException
    //   603	608	670	java/io/IOException
    //   612	618	670	java/io/IOException
    //   618	623	670	java/io/IOException
    //   627	632	670	java/io/IOException
    //   634	639	670	java/io/IOException
    //   642	647	670	java/io/IOException
    //   651	656	670	java/io/IOException
    //   191	196	715	com/google/f/x
    //   249	254	715	com/google/f/x
    //   267	272	715	com/google/f/x
    //   281	286	715	com/google/f/x
    //   288	293	715	com/google/f/x
    //   295	300	715	com/google/f/x
    //   302	307	715	com/google/f/x
    //   318	321	715	com/google/f/x
    //   327	332	715	com/google/f/x
    //   336	341	715	com/google/f/x
    //   346	351	715	com/google/f/x
    //   353	358	715	com/google/f/x
    //   362	368	715	com/google/f/x
    //   368	373	715	com/google/f/x
    //   377	382	715	com/google/f/x
    //   384	389	715	com/google/f/x
    //   392	397	715	com/google/f/x
    //   406	411	715	com/google/f/x
    //   413	418	715	com/google/f/x
    //   420	425	715	com/google/f/x
    //   427	432	715	com/google/f/x
    //   443	446	715	com/google/f/x
    //   452	457	715	com/google/f/x
    //   461	466	715	com/google/f/x
    //   471	476	715	com/google/f/x
    //   478	483	715	com/google/f/x
    //   487	493	715	com/google/f/x
    //   493	498	715	com/google/f/x
    //   502	507	715	com/google/f/x
    //   509	514	715	com/google/f/x
    //   517	522	715	com/google/f/x
    //   531	536	715	com/google/f/x
    //   538	543	715	com/google/f/x
    //   545	550	715	com/google/f/x
    //   552	557	715	com/google/f/x
    //   568	571	715	com/google/f/x
    //   577	582	715	com/google/f/x
    //   586	591	715	com/google/f/x
    //   596	601	715	com/google/f/x
    //   603	608	715	com/google/f/x
    //   612	618	715	com/google/f/x
    //   618	623	715	com/google/f/x
    //   627	632	715	com/google/f/x
    //   634	639	715	com/google/f/x
    //   642	647	715	com/google/f/x
    //   651	656	715	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int j = -1;
    if (i != j) {
      return i;
    }
    long l1 = c;
    long l2 = 0L;
    int k = 0;
    boolean bool = l1 < l2;
    if (bool)
    {
      int m = 1;
      i = h.computeInt64Size(m, l1);
      k = 0 + i;
    }
    i = a;
    j = 2;
    Object localObject;
    if (i == j)
    {
      localObject = (p.b)b;
      i = h.computeMessageSize(j, (ae)localObject);
      k += i;
    }
    i = a;
    j = 3;
    if (i == j)
    {
      localObject = (t.b)b;
      i = h.computeMessageSize(j, (ae)localObject);
      k += i;
    }
    i = a;
    j = 4;
    if (i == j)
    {
      localObject = (r.b)b;
      i = h.computeMessageSize(j, (ae)localObject);
      k += i;
    }
    memoizedSerializedSize = k;
    return k;
  }
  
  public final void writeTo(h paramh)
  {
    long l1 = c;
    long l2 = 0L;
    boolean bool = l1 < l2;
    if (bool)
    {
      int i = 1;
      paramh.writeInt64(i, l1);
    }
    int j = a;
    int k = 2;
    Object localObject;
    if (j == k)
    {
      localObject = (p.b)b;
      paramh.writeMessage(k, (ae)localObject);
    }
    j = a;
    k = 3;
    if (j == k)
    {
      localObject = (t.b)b;
      paramh.writeMessage(k, (ae)localObject);
    }
    j = a;
    k = 4;
    if (j == k)
    {
      localObject = (r.b)b;
      paramh.writeMessage(k, (ae)localObject);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.Batched.Request.Payload
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */