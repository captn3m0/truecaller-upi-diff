package com.truecaller.api.services.messenger.v1;

import com.google.f.ae;
import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;
import com.google.f.w.h;
import com.truecaller.api.services.messenger.v1.models.input.InputPeer;

public final class n$b
  extends q
  implements n.c
{
  private static final b e;
  private static volatile ah f;
  private int a;
  private long b;
  private InputPeer c;
  private w.h d;
  
  static
  {
    b localb = new com/truecaller/api/services/messenger/v1/n$b;
    localb.<init>();
    e = localb;
    localb.makeImmutable();
  }
  
  private n$b()
  {
    w.h localh = emptyProtobufList();
    d = localh;
  }
  
  public static n.b.a a()
  {
    return (n.b.a)e.toBuilder();
  }
  
  public static b b()
  {
    return e;
  }
  
  private InputPeer d()
  {
    InputPeer localInputPeer = c;
    if (localInputPeer == null) {
      localInputPeer = InputPeer.b();
    }
    return localInputPeer;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 71	com/truecaller/api/services/messenger/v1/n$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 77	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_0
    //   19: istore 6
    //   21: aconst_null
    //   22: astore 4
    //   24: iconst_0
    //   25: istore 7
    //   27: iload 5
    //   29: tableswitch	default:+47->76, 1:+655->684, 2:+651->680, 3:+640->669, 4:+629->658, 5:+426->455, 6:+109->138, 7:+422->451, 8:+57->86
    //   76: new 80	java/lang/UnsupportedOperationException
    //   79: astore_1
    //   80: aload_1
    //   81: invokespecial 81	java/lang/UnsupportedOperationException:<init>	()V
    //   84: aload_1
    //   85: athrow
    //   86: getstatic 83	com/truecaller/api/services/messenger/v1/n$b:f	Lcom/google/f/ah;
    //   89: astore_1
    //   90: aload_1
    //   91: ifnonnull +43 -> 134
    //   94: ldc 2
    //   96: astore_1
    //   97: aload_1
    //   98: monitorenter
    //   99: getstatic 83	com/truecaller/api/services/messenger/v1/n$b:f	Lcom/google/f/ah;
    //   102: astore_2
    //   103: aload_2
    //   104: ifnonnull +20 -> 124
    //   107: new 85	com/google/f/q$b
    //   110: astore_2
    //   111: getstatic 24	com/truecaller/api/services/messenger/v1/n$b:e	Lcom/truecaller/api/services/messenger/v1/n$b;
    //   114: astore_3
    //   115: aload_2
    //   116: aload_3
    //   117: invokespecial 88	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   120: aload_2
    //   121: putstatic 83	com/truecaller/api/services/messenger/v1/n$b:f	Lcom/google/f/ah;
    //   124: aload_1
    //   125: monitorexit
    //   126: goto +8 -> 134
    //   129: astore_2
    //   130: aload_1
    //   131: monitorexit
    //   132: aload_2
    //   133: athrow
    //   134: getstatic 83	com/truecaller/api/services/messenger/v1/n$b:f	Lcom/google/f/ah;
    //   137: areturn
    //   138: aload_2
    //   139: checkcast 90	com/google/f/g
    //   142: astore_2
    //   143: aload_3
    //   144: checkcast 92	com/google/f/n
    //   147: astore_3
    //   148: iload 7
    //   150: ifne +301 -> 451
    //   153: aload_2
    //   154: invokevirtual 95	com/google/f/g:readTag	()I
    //   157: istore 5
    //   159: iload 5
    //   161: ifeq +228 -> 389
    //   164: bipush 8
    //   166: istore 8
    //   168: iload 5
    //   170: iload 8
    //   172: if_icmpeq +202 -> 374
    //   175: bipush 18
    //   177: istore 8
    //   179: iload 5
    //   181: iload 8
    //   183: if_icmpeq +97 -> 280
    //   186: bipush 26
    //   188: istore 8
    //   190: iload 5
    //   192: iload 8
    //   194: if_icmpeq +22 -> 216
    //   197: aload_2
    //   198: iload 5
    //   200: invokevirtual 102	com/google/f/g:skipField	(I)Z
    //   203: istore 5
    //   205: iload 5
    //   207: ifne -59 -> 148
    //   210: iconst_1
    //   211: istore 7
    //   213: goto -65 -> 148
    //   216: aload_0
    //   217: getfield 34	com/truecaller/api/services/messenger/v1/n$b:d	Lcom/google/f/w$h;
    //   220: astore_1
    //   221: aload_1
    //   222: invokeinterface 53 1 0
    //   227: istore 5
    //   229: iload 5
    //   231: ifne +18 -> 249
    //   234: aload_0
    //   235: getfield 34	com/truecaller/api/services/messenger/v1/n$b:d	Lcom/google/f/w$h;
    //   238: astore_1
    //   239: aload_1
    //   240: invokestatic 57	com/google/f/q:mutableCopy	(Lcom/google/f/w$h;)Lcom/google/f/w$h;
    //   243: astore_1
    //   244: aload_0
    //   245: aload_1
    //   246: putfield 34	com/truecaller/api/services/messenger/v1/n$b:d	Lcom/google/f/w$h;
    //   249: aload_0
    //   250: getfield 34	com/truecaller/api/services/messenger/v1/n$b:d	Lcom/google/f/w$h;
    //   253: astore_1
    //   254: invokestatic 105	com/truecaller/api/services/messenger/v1/models/input/InputPeer:c	()Lcom/google/f/ah;
    //   257: astore 9
    //   259: aload_2
    //   260: aload 9
    //   262: aload_3
    //   263: invokevirtual 109	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   266: astore 9
    //   268: aload_1
    //   269: aload 9
    //   271: invokeinterface 61 2 0
    //   276: pop
    //   277: goto -129 -> 148
    //   280: aload_0
    //   281: getfield 44	com/truecaller/api/services/messenger/v1/n$b:c	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   284: astore_1
    //   285: aload_1
    //   286: ifnull +21 -> 307
    //   289: aload_0
    //   290: getfield 44	com/truecaller/api/services/messenger/v1/n$b:c	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   293: astore_1
    //   294: aload_1
    //   295: invokevirtual 110	com/truecaller/api/services/messenger/v1/models/input/InputPeer:toBuilder	()Lcom/google/f/q$a;
    //   298: astore_1
    //   299: aload_1
    //   300: checkcast 112	com/truecaller/api/services/messenger/v1/models/input/InputPeer$a
    //   303: astore_1
    //   304: goto +8 -> 312
    //   307: iconst_0
    //   308: istore 5
    //   310: aconst_null
    //   311: astore_1
    //   312: invokestatic 105	com/truecaller/api/services/messenger/v1/models/input/InputPeer:c	()Lcom/google/f/ah;
    //   315: astore 9
    //   317: aload_2
    //   318: aload 9
    //   320: aload_3
    //   321: invokevirtual 109	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   324: astore 9
    //   326: aload 9
    //   328: checkcast 63	com/truecaller/api/services/messenger/v1/models/input/InputPeer
    //   331: astore 9
    //   333: aload_0
    //   334: aload 9
    //   336: putfield 44	com/truecaller/api/services/messenger/v1/n$b:c	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   339: aload_1
    //   340: ifnull -192 -> 148
    //   343: aload_0
    //   344: getfield 44	com/truecaller/api/services/messenger/v1/n$b:c	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   347: astore 9
    //   349: aload_1
    //   350: aload 9
    //   352: invokevirtual 116	com/truecaller/api/services/messenger/v1/models/input/InputPeer$a:mergeFrom	(Lcom/google/f/q;)Lcom/google/f/q$a;
    //   355: pop
    //   356: aload_1
    //   357: invokevirtual 120	com/truecaller/api/services/messenger/v1/models/input/InputPeer$a:buildPartial	()Lcom/google/f/q;
    //   360: astore_1
    //   361: aload_1
    //   362: checkcast 63	com/truecaller/api/services/messenger/v1/models/input/InputPeer
    //   365: astore_1
    //   366: aload_0
    //   367: aload_1
    //   368: putfield 44	com/truecaller/api/services/messenger/v1/n$b:c	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   371: goto -223 -> 148
    //   374: aload_2
    //   375: invokevirtual 124	com/google/f/g:readInt64	()J
    //   378: lstore 10
    //   380: aload_0
    //   381: lload 10
    //   383: putfield 42	com/truecaller/api/services/messenger/v1/n$b:b	J
    //   386: goto -238 -> 148
    //   389: iconst_1
    //   390: istore 7
    //   392: goto -244 -> 148
    //   395: astore_1
    //   396: goto +53 -> 449
    //   399: astore_1
    //   400: new 126	java/lang/RuntimeException
    //   403: astore_2
    //   404: new 128	com/google/f/x
    //   407: astore_3
    //   408: aload_1
    //   409: invokevirtual 134	java/io/IOException:getMessage	()Ljava/lang/String;
    //   412: astore_1
    //   413: aload_3
    //   414: aload_1
    //   415: invokespecial 137	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   418: aload_3
    //   419: aload_0
    //   420: invokevirtual 141	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   423: astore_1
    //   424: aload_2
    //   425: aload_1
    //   426: invokespecial 144	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   429: aload_2
    //   430: athrow
    //   431: astore_1
    //   432: new 126	java/lang/RuntimeException
    //   435: astore_2
    //   436: aload_1
    //   437: aload_0
    //   438: invokevirtual 141	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   441: astore_1
    //   442: aload_2
    //   443: aload_1
    //   444: invokespecial 144	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   447: aload_2
    //   448: athrow
    //   449: aload_1
    //   450: athrow
    //   451: getstatic 24	com/truecaller/api/services/messenger/v1/n$b:e	Lcom/truecaller/api/services/messenger/v1/n$b;
    //   454: areturn
    //   455: aload_2
    //   456: astore_1
    //   457: aload_2
    //   458: checkcast 146	com/google/f/q$k
    //   461: astore_1
    //   462: aload_3
    //   463: checkcast 2	com/truecaller/api/services/messenger/v1/n$b
    //   466: astore_3
    //   467: aload_0
    //   468: getfield 42	com/truecaller/api/services/messenger/v1/n$b:b	J
    //   471: lstore 10
    //   473: lconst_0
    //   474: lstore 12
    //   476: lload 10
    //   478: lload 12
    //   480: lcmp
    //   481: istore 14
    //   483: iload 14
    //   485: ifeq +9 -> 494
    //   488: iconst_1
    //   489: istore 14
    //   491: goto +8 -> 499
    //   494: iconst_0
    //   495: istore 14
    //   497: aconst_null
    //   498: astore_2
    //   499: aload_0
    //   500: getfield 42	com/truecaller/api/services/messenger/v1/n$b:b	J
    //   503: lstore 10
    //   505: aload_3
    //   506: getfield 42	com/truecaller/api/services/messenger/v1/n$b:b	J
    //   509: lstore 15
    //   511: lload 15
    //   513: lload 12
    //   515: lcmp
    //   516: istore 6
    //   518: iload 6
    //   520: ifeq +9 -> 529
    //   523: iconst_1
    //   524: istore 17
    //   526: goto +6 -> 532
    //   529: iconst_0
    //   530: istore 17
    //   532: aload_3
    //   533: getfield 42	com/truecaller/api/services/messenger/v1/n$b:b	J
    //   536: lstore 18
    //   538: aload_1
    //   539: astore 4
    //   541: lload 18
    //   543: lstore 12
    //   545: aload_1
    //   546: iload 14
    //   548: lload 10
    //   550: iload 17
    //   552: lload 18
    //   554: invokeinterface 150 7 0
    //   559: lstore 20
    //   561: aload_0
    //   562: lload 20
    //   564: putfield 42	com/truecaller/api/services/messenger/v1/n$b:b	J
    //   567: aload_0
    //   568: getfield 44	com/truecaller/api/services/messenger/v1/n$b:c	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   571: astore_2
    //   572: aload_3
    //   573: getfield 44	com/truecaller/api/services/messenger/v1/n$b:c	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   576: astore 4
    //   578: aload_1
    //   579: aload_2
    //   580: aload 4
    //   582: invokeinterface 154 3 0
    //   587: checkcast 63	com/truecaller/api/services/messenger/v1/models/input/InputPeer
    //   590: astore_2
    //   591: aload_0
    //   592: aload_2
    //   593: putfield 44	com/truecaller/api/services/messenger/v1/n$b:c	Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;
    //   596: aload_0
    //   597: getfield 34	com/truecaller/api/services/messenger/v1/n$b:d	Lcom/google/f/w$h;
    //   600: astore_2
    //   601: aload_3
    //   602: getfield 34	com/truecaller/api/services/messenger/v1/n$b:d	Lcom/google/f/w$h;
    //   605: astore 4
    //   607: aload_1
    //   608: aload_2
    //   609: aload 4
    //   611: invokeinterface 158 3 0
    //   616: astore_2
    //   617: aload_0
    //   618: aload_2
    //   619: putfield 34	com/truecaller/api/services/messenger/v1/n$b:d	Lcom/google/f/w$h;
    //   622: getstatic 164	com/google/f/q$i:INSTANCE	Lcom/google/f/q$i;
    //   625: astore_2
    //   626: aload_1
    //   627: aload_2
    //   628: if_acmpne +28 -> 656
    //   631: aload_0
    //   632: getfield 166	com/truecaller/api/services/messenger/v1/n$b:a	I
    //   635: istore 5
    //   637: aload_3
    //   638: getfield 166	com/truecaller/api/services/messenger/v1/n$b:a	I
    //   641: istore 14
    //   643: iload 5
    //   645: iload 14
    //   647: ior
    //   648: istore 5
    //   650: aload_0
    //   651: iload 5
    //   653: putfield 166	com/truecaller/api/services/messenger/v1/n$b:a	I
    //   656: aload_0
    //   657: areturn
    //   658: new 40	com/truecaller/api/services/messenger/v1/n$b$a
    //   661: astore_1
    //   662: aload_1
    //   663: iconst_0
    //   664: invokespecial 169	com/truecaller/api/services/messenger/v1/n$b$a:<init>	(B)V
    //   667: aload_1
    //   668: areturn
    //   669: aload_0
    //   670: getfield 34	com/truecaller/api/services/messenger/v1/n$b:d	Lcom/google/f/w$h;
    //   673: invokeinterface 170 1 0
    //   678: aconst_null
    //   679: areturn
    //   680: getstatic 24	com/truecaller/api/services/messenger/v1/n$b:e	Lcom/truecaller/api/services/messenger/v1/n$b;
    //   683: areturn
    //   684: new 2	com/truecaller/api/services/messenger/v1/n$b
    //   687: astore_1
    //   688: aload_1
    //   689: invokespecial 22	com/truecaller/api/services/messenger/v1/n$b:<init>	()V
    //   692: aload_1
    //   693: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	694	0	this	b
    //   0	694	1	paramj	com.google.f.q.j
    //   0	694	2	paramObject1	Object
    //   0	694	3	paramObject2	Object
    //   3	607	4	localObject1	Object
    //   9	190	5	i	int
    //   203	106	5	bool1	boolean
    //   635	17	5	j	int
    //   19	500	6	bool2	boolean
    //   25	366	7	k	int
    //   166	29	8	m	int
    //   257	94	9	localObject2	Object
    //   378	171	10	l1	long
    //   474	70	12	l2	long
    //   481	66	14	bool3	boolean
    //   641	7	14	n	int
    //   509	3	15	l3	long
    //   524	27	17	bool4	boolean
    //   536	17	18	l4	long
    //   559	4	20	l5	long
    // Exception table:
    //   from	to	target	type
    //   99	102	129	finally
    //   107	110	129	finally
    //   111	114	129	finally
    //   116	120	129	finally
    //   120	124	129	finally
    //   124	126	129	finally
    //   130	132	129	finally
    //   153	157	395	finally
    //   198	203	395	finally
    //   216	220	395	finally
    //   221	227	395	finally
    //   234	238	395	finally
    //   239	243	395	finally
    //   245	249	395	finally
    //   249	253	395	finally
    //   254	257	395	finally
    //   262	266	395	finally
    //   269	277	395	finally
    //   280	284	395	finally
    //   289	293	395	finally
    //   294	298	395	finally
    //   299	303	395	finally
    //   312	315	395	finally
    //   320	324	395	finally
    //   326	331	395	finally
    //   334	339	395	finally
    //   343	347	395	finally
    //   350	356	395	finally
    //   356	360	395	finally
    //   361	365	395	finally
    //   367	371	395	finally
    //   374	378	395	finally
    //   381	386	395	finally
    //   400	403	395	finally
    //   404	407	395	finally
    //   408	412	395	finally
    //   414	418	395	finally
    //   419	423	395	finally
    //   425	429	395	finally
    //   429	431	395	finally
    //   432	435	395	finally
    //   437	441	395	finally
    //   443	447	395	finally
    //   447	449	395	finally
    //   153	157	399	java/io/IOException
    //   198	203	399	java/io/IOException
    //   216	220	399	java/io/IOException
    //   221	227	399	java/io/IOException
    //   234	238	399	java/io/IOException
    //   239	243	399	java/io/IOException
    //   245	249	399	java/io/IOException
    //   249	253	399	java/io/IOException
    //   254	257	399	java/io/IOException
    //   262	266	399	java/io/IOException
    //   269	277	399	java/io/IOException
    //   280	284	399	java/io/IOException
    //   289	293	399	java/io/IOException
    //   294	298	399	java/io/IOException
    //   299	303	399	java/io/IOException
    //   312	315	399	java/io/IOException
    //   320	324	399	java/io/IOException
    //   326	331	399	java/io/IOException
    //   334	339	399	java/io/IOException
    //   343	347	399	java/io/IOException
    //   350	356	399	java/io/IOException
    //   356	360	399	java/io/IOException
    //   361	365	399	java/io/IOException
    //   367	371	399	java/io/IOException
    //   374	378	399	java/io/IOException
    //   381	386	399	java/io/IOException
    //   153	157	431	com/google/f/x
    //   198	203	431	com/google/f/x
    //   216	220	431	com/google/f/x
    //   221	227	431	com/google/f/x
    //   234	238	431	com/google/f/x
    //   239	243	431	com/google/f/x
    //   245	249	431	com/google/f/x
    //   249	253	431	com/google/f/x
    //   254	257	431	com/google/f/x
    //   262	266	431	com/google/f/x
    //   269	277	431	com/google/f/x
    //   280	284	431	com/google/f/x
    //   289	293	431	com/google/f/x
    //   294	298	431	com/google/f/x
    //   299	303	431	com/google/f/x
    //   312	315	431	com/google/f/x
    //   320	324	431	com/google/f/x
    //   326	331	431	com/google/f/x
    //   334	339	431	com/google/f/x
    //   343	347	431	com/google/f/x
    //   350	356	431	com/google/f/x
    //   356	360	431	com/google/f/x
    //   361	365	431	com/google/f/x
    //   367	371	431	com/google/f/x
    //   374	378	431	com/google/f/x
    //   381	386	431	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int j = -1;
    if (i != j) {
      return i;
    }
    long l1 = b;
    long l2 = 0L;
    int k = 0;
    boolean bool = l1 < l2;
    if (bool)
    {
      int m = 1;
      i = h.computeInt64Size(m, l1) + 0;
    }
    else
    {
      i = 0;
    }
    Object localObject1 = c;
    Object localObject2;
    if (localObject1 != null)
    {
      localObject2 = d();
      j = h.computeMessageSize(2, (ae)localObject2);
      i += j;
    }
    for (;;)
    {
      localObject1 = d;
      j = ((w.h)localObject1).size();
      if (k >= j) {
        break;
      }
      localObject2 = (ae)d.get(k);
      j = h.computeMessageSize(3, (ae)localObject2);
      i += j;
      k += 1;
    }
    memoizedSerializedSize = i;
    return i;
  }
  
  public final void writeTo(h paramh)
  {
    long l1 = b;
    long l2 = 0L;
    boolean bool = l1 < l2;
    if (bool)
    {
      int i = 1;
      paramh.writeInt64(i, l1);
    }
    InputPeer localInputPeer = c;
    Object localObject;
    if (localInputPeer != null)
    {
      j = 2;
      localObject = d();
      paramh.writeMessage(j, (ae)localObject);
    }
    int j = 0;
    localInputPeer = null;
    for (;;)
    {
      localObject = d;
      int k = ((w.h)localObject).size();
      if (j >= k) {
        break;
      }
      k = 3;
      ae localae = (ae)d.get(j);
      paramh.writeMessage(k, localae);
      j += 1;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.n.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */