package com.truecaller.api.services.messenger.v1;

import com.google.f.q.a;
import com.truecaller.api.services.messenger.v1.models.input.InputPeer;
import com.truecaller.api.services.messenger.v1.models.input.InputReportType;

public final class t$b$a
  extends q.a
  implements t.c
{
  private t$b$a()
  {
    super(localb);
  }
  
  public final a a(InputPeer paramInputPeer)
  {
    copyOnWrite();
    t.b.a((t.b)instance, paramInputPeer);
    return this;
  }
  
  public final a a(InputReportType paramInputReportType)
  {
    copyOnWrite();
    t.b.a((t.b)instance, paramInputReportType);
    return this;
  }
  
  public final a a(String paramString)
  {
    copyOnWrite();
    t.b.a((t.b)instance, paramString);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.t.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */