package com.truecaller.api.services.messenger.v1;

import com.google.f.q.a;
import com.truecaller.api.services.messenger.v1.models.input.InputNotificationScope;
import com.truecaller.api.services.messenger.v1.models.input.InputNotificationSettings.a;

public final class x$b$a
  extends q.a
  implements x.c
{
  private x$b$a()
  {
    super(localb);
  }
  
  public final a a(InputNotificationScope paramInputNotificationScope)
  {
    copyOnWrite();
    x.b.a((x.b)instance, paramInputNotificationScope);
    return this;
  }
  
  public final a a(InputNotificationSettings.a parama)
  {
    copyOnWrite();
    x.b.a((x.b)instance, parama);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.x.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */