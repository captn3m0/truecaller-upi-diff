package com.truecaller.api.services.messenger.v1;

import com.google.f.ac;
import com.google.f.ad;
import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class h$d
  extends q
  implements h.e
{
  private static final d b;
  private static volatile ah c;
  private ad a;
  
  static
  {
    d locald = new com/truecaller/api/services/messenger/v1/h$d;
    locald.<init>();
    b = locald;
    locald.makeImmutable();
  }
  
  private h$d()
  {
    ad localad = ad.emptyMapField();
    a = localad;
  }
  
  public static d b()
  {
    return b;
  }
  
  public final Map a()
  {
    return Collections.unmodifiableMap(a);
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 41	com/truecaller/api/services/messenger/v1/h$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 47	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_0
    //   19: istore 6
    //   21: aconst_null
    //   22: astore 4
    //   24: iload 5
    //   26: tableswitch	default:+46->72, 1:+353->379, 2:+349->375, 3:+340->366, 4:+329->355, 5:+293->319, 6:+108->134, 7:+289->315, 8:+56->82
    //   72: new 49	java/lang/UnsupportedOperationException
    //   75: astore_1
    //   76: aload_1
    //   77: invokespecial 50	java/lang/UnsupportedOperationException:<init>	()V
    //   80: aload_1
    //   81: athrow
    //   82: getstatic 52	com/truecaller/api/services/messenger/v1/h$d:c	Lcom/google/f/ah;
    //   85: astore_1
    //   86: aload_1
    //   87: ifnonnull +43 -> 130
    //   90: ldc 2
    //   92: astore_1
    //   93: aload_1
    //   94: monitorenter
    //   95: getstatic 52	com/truecaller/api/services/messenger/v1/h$d:c	Lcom/google/f/ah;
    //   98: astore_2
    //   99: aload_2
    //   100: ifnonnull +20 -> 120
    //   103: new 54	com/google/f/q$b
    //   106: astore_2
    //   107: getstatic 18	com/truecaller/api/services/messenger/v1/h$d:b	Lcom/truecaller/api/services/messenger/v1/h$d;
    //   110: astore_3
    //   111: aload_2
    //   112: aload_3
    //   113: invokespecial 57	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   116: aload_2
    //   117: putstatic 52	com/truecaller/api/services/messenger/v1/h$d:c	Lcom/google/f/ah;
    //   120: aload_1
    //   121: monitorexit
    //   122: goto +8 -> 130
    //   125: astore_2
    //   126: aload_1
    //   127: monitorexit
    //   128: aload_2
    //   129: athrow
    //   130: getstatic 52	com/truecaller/api/services/messenger/v1/h$d:c	Lcom/google/f/ah;
    //   133: areturn
    //   134: aload_2
    //   135: checkcast 59	com/google/f/g
    //   138: astore_2
    //   139: aload_3
    //   140: checkcast 61	com/google/f/n
    //   143: astore_3
    //   144: iconst_1
    //   145: istore 5
    //   147: iload 6
    //   149: ifne +166 -> 315
    //   152: aload_2
    //   153: invokevirtual 65	com/google/f/g:readTag	()I
    //   156: istore 7
    //   158: iload 7
    //   160: ifeq +93 -> 253
    //   163: bipush 10
    //   165: istore 8
    //   167: iload 7
    //   169: iload 8
    //   171: if_icmpeq +22 -> 193
    //   174: aload_2
    //   175: iload 7
    //   177: invokevirtual 70	com/google/f/g:skipField	(I)Z
    //   180: istore 7
    //   182: iload 7
    //   184: ifne -37 -> 147
    //   187: iconst_1
    //   188: istore 6
    //   190: goto -43 -> 147
    //   193: aload_0
    //   194: getfield 30	com/truecaller/api/services/messenger/v1/h$d:a	Lcom/google/f/ad;
    //   197: astore 9
    //   199: aload 9
    //   201: invokevirtual 74	com/google/f/ad:isMutable	()Z
    //   204: istore 7
    //   206: iload 7
    //   208: ifne +22 -> 230
    //   211: aload_0
    //   212: getfield 30	com/truecaller/api/services/messenger/v1/h$d:a	Lcom/google/f/ad;
    //   215: astore 9
    //   217: aload 9
    //   219: invokevirtual 77	com/google/f/ad:mutableCopy	()Lcom/google/f/ad;
    //   222: astore 9
    //   224: aload_0
    //   225: aload 9
    //   227: putfield 30	com/truecaller/api/services/messenger/v1/h$d:a	Lcom/google/f/ad;
    //   230: getstatic 82	com/truecaller/api/services/messenger/v1/h$d$b:a	Lcom/google/f/ac;
    //   233: astore 9
    //   235: aload_0
    //   236: getfield 30	com/truecaller/api/services/messenger/v1/h$d:a	Lcom/google/f/ad;
    //   239: astore 10
    //   241: aload 9
    //   243: aload 10
    //   245: aload_2
    //   246: aload_3
    //   247: invokevirtual 88	com/google/f/ac:parseInto	(Lcom/google/f/ad;Lcom/google/f/g;Lcom/google/f/n;)V
    //   250: goto -103 -> 147
    //   253: iconst_1
    //   254: istore 6
    //   256: goto -109 -> 147
    //   259: astore_1
    //   260: goto +53 -> 313
    //   263: astore_1
    //   264: new 90	java/lang/RuntimeException
    //   267: astore_2
    //   268: new 92	com/google/f/x
    //   271: astore_3
    //   272: aload_1
    //   273: invokevirtual 98	java/io/IOException:getMessage	()Ljava/lang/String;
    //   276: astore_1
    //   277: aload_3
    //   278: aload_1
    //   279: invokespecial 101	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   282: aload_3
    //   283: aload_0
    //   284: invokevirtual 105	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   287: astore_1
    //   288: aload_2
    //   289: aload_1
    //   290: invokespecial 108	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   293: aload_2
    //   294: athrow
    //   295: astore_1
    //   296: new 90	java/lang/RuntimeException
    //   299: astore_2
    //   300: aload_1
    //   301: aload_0
    //   302: invokevirtual 105	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   305: astore_1
    //   306: aload_2
    //   307: aload_1
    //   308: invokespecial 108	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   311: aload_2
    //   312: athrow
    //   313: aload_1
    //   314: athrow
    //   315: getstatic 18	com/truecaller/api/services/messenger/v1/h$d:b	Lcom/truecaller/api/services/messenger/v1/h$d;
    //   318: areturn
    //   319: aload_2
    //   320: checkcast 110	com/google/f/q$k
    //   323: astore_2
    //   324: aload_3
    //   325: checkcast 2	com/truecaller/api/services/messenger/v1/h$d
    //   328: astore_3
    //   329: aload_0
    //   330: getfield 30	com/truecaller/api/services/messenger/v1/h$d:a	Lcom/google/f/ad;
    //   333: astore_1
    //   334: aload_3
    //   335: getfield 30	com/truecaller/api/services/messenger/v1/h$d:a	Lcom/google/f/ad;
    //   338: astore_3
    //   339: aload_2
    //   340: aload_1
    //   341: aload_3
    //   342: invokeinterface 114 3 0
    //   347: astore_1
    //   348: aload_0
    //   349: aload_1
    //   350: putfield 30	com/truecaller/api/services/messenger/v1/h$d:a	Lcom/google/f/ad;
    //   353: aload_0
    //   354: areturn
    //   355: new 116	com/truecaller/api/services/messenger/v1/h$d$a
    //   358: astore_1
    //   359: aload_1
    //   360: iconst_0
    //   361: invokespecial 119	com/truecaller/api/services/messenger/v1/h$d$a:<init>	(B)V
    //   364: aload_1
    //   365: areturn
    //   366: aload_0
    //   367: getfield 30	com/truecaller/api/services/messenger/v1/h$d:a	Lcom/google/f/ad;
    //   370: invokevirtual 120	com/google/f/ad:makeImmutable	()V
    //   373: aconst_null
    //   374: areturn
    //   375: getstatic 18	com/truecaller/api/services/messenger/v1/h$d:b	Lcom/truecaller/api/services/messenger/v1/h$d;
    //   378: areturn
    //   379: new 2	com/truecaller/api/services/messenger/v1/h$d
    //   382: astore_1
    //   383: aload_1
    //   384: invokespecial 16	com/truecaller/api/services/messenger/v1/h$d:<init>	()V
    //   387: aload_1
    //   388: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	389	0	this	d
    //   0	389	1	paramj	com.google.f.q.j
    //   0	389	2	paramObject1	Object
    //   0	389	3	paramObject2	Object
    //   3	20	4	arrayOfInt	int[]
    //   9	137	5	i	int
    //   19	236	6	j	int
    //   156	20	7	k	int
    //   180	27	7	bool	boolean
    //   165	7	8	m	int
    //   197	45	9	localObject	Object
    //   239	5	10	localad	ad
    // Exception table:
    //   from	to	target	type
    //   95	98	125	finally
    //   103	106	125	finally
    //   107	110	125	finally
    //   112	116	125	finally
    //   116	120	125	finally
    //   120	122	125	finally
    //   126	128	125	finally
    //   152	156	259	finally
    //   175	180	259	finally
    //   193	197	259	finally
    //   199	204	259	finally
    //   211	215	259	finally
    //   217	222	259	finally
    //   225	230	259	finally
    //   230	233	259	finally
    //   235	239	259	finally
    //   246	250	259	finally
    //   264	267	259	finally
    //   268	271	259	finally
    //   272	276	259	finally
    //   278	282	259	finally
    //   283	287	259	finally
    //   289	293	259	finally
    //   293	295	259	finally
    //   296	299	259	finally
    //   301	305	259	finally
    //   307	311	259	finally
    //   311	313	259	finally
    //   152	156	263	java/io/IOException
    //   175	180	263	java/io/IOException
    //   193	197	263	java/io/IOException
    //   199	204	263	java/io/IOException
    //   211	215	263	java/io/IOException
    //   217	222	263	java/io/IOException
    //   225	230	263	java/io/IOException
    //   230	233	263	java/io/IOException
    //   235	239	263	java/io/IOException
    //   246	250	263	java/io/IOException
    //   152	156	295	com/google/f/x
    //   175	180	295	com/google/f/x
    //   193	197	295	com/google/f/x
    //   199	204	295	com/google/f/x
    //   211	215	295	com/google/f/x
    //   217	222	295	com/google/f/x
    //   225	230	295	com/google/f/x
    //   230	233	295	com/google/f/x
    //   235	239	295	com/google/f/x
    //   246	250	295	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int j = -1;
    if (i != j) {
      return i;
    }
    i = 0;
    Iterator localIterator = a.entrySet().iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      Object localObject1 = (Map.Entry)localIterator.next();
      ac localac = h.d.b.a;
      int m = 1;
      Object localObject2 = ((Map.Entry)localObject1).getKey();
      localObject1 = ((Map.Entry)localObject1).getValue();
      int k = localac.computeMessageSize(m, localObject2, localObject1);
      i += k;
    }
    memoizedSerializedSize = i;
    return i;
  }
  
  public final void writeTo(h paramh)
  {
    Iterator localIterator = a.entrySet().iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      Object localObject1 = (Map.Entry)localIterator.next();
      ac localac = h.d.b.a;
      int i = 1;
      Object localObject2 = ((Map.Entry)localObject1).getKey();
      localObject1 = ((Map.Entry)localObject1).getValue();
      localac.serializeTo(paramh, i, localObject2, localObject1);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.h.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */