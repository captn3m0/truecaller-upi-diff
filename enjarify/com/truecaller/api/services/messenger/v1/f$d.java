package com.truecaller.api.services.messenger.v1;

import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;
import com.google.f.w.h;
import java.util.List;

public final class f$d
  extends q
  implements f.e
{
  private static final d b;
  private static volatile ah c;
  private w.h a;
  
  static
  {
    d locald = new com/truecaller/api/services/messenger/v1/f$d;
    locald.<init>();
    b = locald;
    locald.makeImmutable();
  }
  
  private f$d()
  {
    w.h localh = q.emptyProtobufList();
    a = localh;
  }
  
  public static d b()
  {
    return b;
  }
  
  public final List a()
  {
    return a;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 33	com/truecaller/api/services/messenger/v1/f$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 39	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_0
    //   19: istore 6
    //   21: aconst_null
    //   22: astore 4
    //   24: iload 5
    //   26: tableswitch	default:+46->72, 1:+352->378, 2:+348->374, 3:+337->363, 4:+326->352, 5:+290->316, 6:+108->134, 7:+286->312, 8:+56->82
    //   72: new 41	java/lang/UnsupportedOperationException
    //   75: astore_1
    //   76: aload_1
    //   77: invokespecial 42	java/lang/UnsupportedOperationException:<init>	()V
    //   80: aload_1
    //   81: athrow
    //   82: getstatic 44	com/truecaller/api/services/messenger/v1/f$d:c	Lcom/google/f/ah;
    //   85: astore_1
    //   86: aload_1
    //   87: ifnonnull +43 -> 130
    //   90: ldc 2
    //   92: astore_1
    //   93: aload_1
    //   94: monitorenter
    //   95: getstatic 44	com/truecaller/api/services/messenger/v1/f$d:c	Lcom/google/f/ah;
    //   98: astore_2
    //   99: aload_2
    //   100: ifnonnull +20 -> 120
    //   103: new 46	com/google/f/q$b
    //   106: astore_2
    //   107: getstatic 18	com/truecaller/api/services/messenger/v1/f$d:b	Lcom/truecaller/api/services/messenger/v1/f$d;
    //   110: astore_3
    //   111: aload_2
    //   112: aload_3
    //   113: invokespecial 49	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   116: aload_2
    //   117: putstatic 44	com/truecaller/api/services/messenger/v1/f$d:c	Lcom/google/f/ah;
    //   120: aload_1
    //   121: monitorexit
    //   122: goto +8 -> 130
    //   125: astore_2
    //   126: aload_1
    //   127: monitorexit
    //   128: aload_2
    //   129: athrow
    //   130: getstatic 44	com/truecaller/api/services/messenger/v1/f$d:c	Lcom/google/f/ah;
    //   133: areturn
    //   134: aload_2
    //   135: checkcast 51	com/google/f/g
    //   138: astore_2
    //   139: iconst_1
    //   140: istore 5
    //   142: iload 6
    //   144: ifne +168 -> 312
    //   147: aload_2
    //   148: invokevirtual 55	com/google/f/g:readTag	()I
    //   151: istore 7
    //   153: iload 7
    //   155: ifeq +95 -> 250
    //   158: bipush 10
    //   160: istore 8
    //   162: iload 7
    //   164: iload 8
    //   166: if_icmpeq +22 -> 188
    //   169: aload_2
    //   170: iload 7
    //   172: invokevirtual 60	com/google/f/g:skipField	(I)Z
    //   175: istore 7
    //   177: iload 7
    //   179: ifne -37 -> 142
    //   182: iconst_1
    //   183: istore 6
    //   185: goto -43 -> 142
    //   188: aload_2
    //   189: invokevirtual 64	com/google/f/g:readStringRequireUtf8	()Ljava/lang/String;
    //   192: astore_3
    //   193: aload_0
    //   194: getfield 28	com/truecaller/api/services/messenger/v1/f$d:a	Lcom/google/f/w$h;
    //   197: astore 9
    //   199: aload 9
    //   201: invokeinterface 70 1 0
    //   206: istore 8
    //   208: iload 8
    //   210: ifne +22 -> 232
    //   213: aload_0
    //   214: getfield 28	com/truecaller/api/services/messenger/v1/f$d:a	Lcom/google/f/w$h;
    //   217: astore 9
    //   219: aload 9
    //   221: invokestatic 74	com/google/f/q:mutableCopy	(Lcom/google/f/w$h;)Lcom/google/f/w$h;
    //   224: astore 9
    //   226: aload_0
    //   227: aload 9
    //   229: putfield 28	com/truecaller/api/services/messenger/v1/f$d:a	Lcom/google/f/w$h;
    //   232: aload_0
    //   233: getfield 28	com/truecaller/api/services/messenger/v1/f$d:a	Lcom/google/f/w$h;
    //   236: astore 9
    //   238: aload 9
    //   240: aload_3
    //   241: invokeinterface 78 2 0
    //   246: pop
    //   247: goto -105 -> 142
    //   250: iconst_1
    //   251: istore 6
    //   253: goto -111 -> 142
    //   256: astore_1
    //   257: goto +53 -> 310
    //   260: astore_1
    //   261: new 80	java/lang/RuntimeException
    //   264: astore_2
    //   265: new 82	com/google/f/x
    //   268: astore_3
    //   269: aload_1
    //   270: invokevirtual 87	java/io/IOException:getMessage	()Ljava/lang/String;
    //   273: astore_1
    //   274: aload_3
    //   275: aload_1
    //   276: invokespecial 90	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   279: aload_3
    //   280: aload_0
    //   281: invokevirtual 94	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   284: astore_1
    //   285: aload_2
    //   286: aload_1
    //   287: invokespecial 97	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   290: aload_2
    //   291: athrow
    //   292: astore_1
    //   293: new 80	java/lang/RuntimeException
    //   296: astore_2
    //   297: aload_1
    //   298: aload_0
    //   299: invokevirtual 94	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   302: astore_1
    //   303: aload_2
    //   304: aload_1
    //   305: invokespecial 97	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   308: aload_2
    //   309: athrow
    //   310: aload_1
    //   311: athrow
    //   312: getstatic 18	com/truecaller/api/services/messenger/v1/f$d:b	Lcom/truecaller/api/services/messenger/v1/f$d;
    //   315: areturn
    //   316: aload_2
    //   317: checkcast 99	com/google/f/q$k
    //   320: astore_2
    //   321: aload_3
    //   322: checkcast 2	com/truecaller/api/services/messenger/v1/f$d
    //   325: astore_3
    //   326: aload_0
    //   327: getfield 28	com/truecaller/api/services/messenger/v1/f$d:a	Lcom/google/f/w$h;
    //   330: astore_1
    //   331: aload_3
    //   332: getfield 28	com/truecaller/api/services/messenger/v1/f$d:a	Lcom/google/f/w$h;
    //   335: astore_3
    //   336: aload_2
    //   337: aload_1
    //   338: aload_3
    //   339: invokeinterface 103 3 0
    //   344: astore_1
    //   345: aload_0
    //   346: aload_1
    //   347: putfield 28	com/truecaller/api/services/messenger/v1/f$d:a	Lcom/google/f/w$h;
    //   350: aload_0
    //   351: areturn
    //   352: new 105	com/truecaller/api/services/messenger/v1/f$d$a
    //   355: astore_1
    //   356: aload_1
    //   357: iconst_0
    //   358: invokespecial 108	com/truecaller/api/services/messenger/v1/f$d$a:<init>	(B)V
    //   361: aload_1
    //   362: areturn
    //   363: aload_0
    //   364: getfield 28	com/truecaller/api/services/messenger/v1/f$d:a	Lcom/google/f/w$h;
    //   367: invokeinterface 109 1 0
    //   372: aconst_null
    //   373: areturn
    //   374: getstatic 18	com/truecaller/api/services/messenger/v1/f$d:b	Lcom/truecaller/api/services/messenger/v1/f$d;
    //   377: areturn
    //   378: new 2	com/truecaller/api/services/messenger/v1/f$d
    //   381: astore_1
    //   382: aload_1
    //   383: invokespecial 16	com/truecaller/api/services/messenger/v1/f$d:<init>	()V
    //   386: aload_1
    //   387: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	388	0	this	d
    //   0	388	1	paramj	com.google.f.q.j
    //   0	388	2	paramObject1	Object
    //   0	388	3	paramObject2	Object
    //   3	20	4	arrayOfInt	int[]
    //   9	132	5	i	int
    //   19	233	6	j	int
    //   151	20	7	k	int
    //   175	3	7	bool1	boolean
    //   160	7	8	m	int
    //   206	3	8	bool2	boolean
    //   197	42	9	localh	w.h
    // Exception table:
    //   from	to	target	type
    //   95	98	125	finally
    //   103	106	125	finally
    //   107	110	125	finally
    //   112	116	125	finally
    //   116	120	125	finally
    //   120	122	125	finally
    //   126	128	125	finally
    //   147	151	256	finally
    //   170	175	256	finally
    //   188	192	256	finally
    //   193	197	256	finally
    //   199	206	256	finally
    //   213	217	256	finally
    //   219	224	256	finally
    //   227	232	256	finally
    //   232	236	256	finally
    //   240	247	256	finally
    //   261	264	256	finally
    //   265	268	256	finally
    //   269	273	256	finally
    //   275	279	256	finally
    //   280	284	256	finally
    //   286	290	256	finally
    //   290	292	256	finally
    //   293	296	256	finally
    //   298	302	256	finally
    //   304	308	256	finally
    //   308	310	256	finally
    //   147	151	260	java/io/IOException
    //   170	175	260	java/io/IOException
    //   188	192	260	java/io/IOException
    //   193	197	260	java/io/IOException
    //   199	206	260	java/io/IOException
    //   213	217	260	java/io/IOException
    //   219	224	260	java/io/IOException
    //   227	232	260	java/io/IOException
    //   232	236	260	java/io/IOException
    //   240	247	260	java/io/IOException
    //   147	151	292	com/google/f/x
    //   170	175	292	com/google/f/x
    //   188	192	292	com/google/f/x
    //   193	197	292	com/google/f/x
    //   199	206	292	com/google/f/x
    //   213	217	292	com/google/f/x
    //   219	224	292	com/google/f/x
    //   227	232	292	com/google/f/x
    //   232	236	292	com/google/f/x
    //   240	247	292	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int j = -1;
    if (i != j) {
      return i;
    }
    i = 0;
    j = 0;
    int k = 0;
    for (;;)
    {
      Object localObject = a;
      int m = ((w.h)localObject).size();
      if (j >= m) {
        break;
      }
      localObject = (String)a.get(j);
      m = h.computeStringSizeNoTag((String)localObject);
      k += m;
      j += 1;
    }
    k += 0;
    i = a.size() * 1;
    k += i;
    memoizedSerializedSize = k;
    return k;
  }
  
  public final void writeTo(h paramh)
  {
    int i = 0;
    for (;;)
    {
      Object localObject = a;
      int j = ((w.h)localObject).size();
      if (i >= j) {
        break;
      }
      localObject = (String)a.get(i);
      int k = 1;
      paramh.writeString(k, (String)localObject);
      i += 1;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.f.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */