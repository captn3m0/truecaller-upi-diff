package com.truecaller.api.services.messenger.v1;

import com.google.f.ae;
import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;
import com.google.f.w.h;

public final class Batched$Request
  extends q
  implements Batched.b
{
  private static final Request b;
  private static volatile ah c;
  private w.h a;
  
  static
  {
    Request localRequest = new com/truecaller/api/services/messenger/v1/Batched$Request;
    localRequest.<init>();
    b = localRequest;
    localRequest.makeImmutable();
  }
  
  private Batched$Request()
  {
    w.h localh = emptyProtobufList();
    a = localh;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 33	com/truecaller/api/services/messenger/v1/Batched$1:b	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 39	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_0
    //   19: istore 6
    //   21: aconst_null
    //   22: astore 4
    //   24: iload 5
    //   26: tableswitch	default:+46->72, 1:+367->393, 2:+363->389, 3:+352->378, 4:+341->367, 5:+305->331, 6:+108->134, 7:+301->327, 8:+56->82
    //   72: new 41	java/lang/UnsupportedOperationException
    //   75: astore_1
    //   76: aload_1
    //   77: invokespecial 42	java/lang/UnsupportedOperationException:<init>	()V
    //   80: aload_1
    //   81: athrow
    //   82: getstatic 44	com/truecaller/api/services/messenger/v1/Batched$Request:c	Lcom/google/f/ah;
    //   85: astore_1
    //   86: aload_1
    //   87: ifnonnull +43 -> 130
    //   90: ldc 2
    //   92: astore_1
    //   93: aload_1
    //   94: monitorenter
    //   95: getstatic 44	com/truecaller/api/services/messenger/v1/Batched$Request:c	Lcom/google/f/ah;
    //   98: astore_2
    //   99: aload_2
    //   100: ifnonnull +20 -> 120
    //   103: new 46	com/google/f/q$b
    //   106: astore_2
    //   107: getstatic 18	com/truecaller/api/services/messenger/v1/Batched$Request:b	Lcom/truecaller/api/services/messenger/v1/Batched$Request;
    //   110: astore_3
    //   111: aload_2
    //   112: aload_3
    //   113: invokespecial 49	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   116: aload_2
    //   117: putstatic 44	com/truecaller/api/services/messenger/v1/Batched$Request:c	Lcom/google/f/ah;
    //   120: aload_1
    //   121: monitorexit
    //   122: goto +8 -> 130
    //   125: astore_2
    //   126: aload_1
    //   127: monitorexit
    //   128: aload_2
    //   129: athrow
    //   130: getstatic 44	com/truecaller/api/services/messenger/v1/Batched$Request:c	Lcom/google/f/ah;
    //   133: areturn
    //   134: aload_2
    //   135: checkcast 51	com/google/f/g
    //   138: astore_2
    //   139: aload_3
    //   140: checkcast 53	com/google/f/n
    //   143: astore_3
    //   144: iconst_1
    //   145: istore 5
    //   147: iload 6
    //   149: ifne +178 -> 327
    //   152: aload_2
    //   153: invokevirtual 57	com/google/f/g:readTag	()I
    //   156: istore 7
    //   158: iload 7
    //   160: ifeq +105 -> 265
    //   163: bipush 10
    //   165: istore 8
    //   167: iload 7
    //   169: iload 8
    //   171: if_icmpeq +22 -> 193
    //   174: aload_2
    //   175: iload 7
    //   177: invokevirtual 62	com/google/f/g:skipField	(I)Z
    //   180: istore 7
    //   182: iload 7
    //   184: ifne -37 -> 147
    //   187: iconst_1
    //   188: istore 6
    //   190: goto -43 -> 147
    //   193: aload_0
    //   194: getfield 28	com/truecaller/api/services/messenger/v1/Batched$Request:a	Lcom/google/f/w$h;
    //   197: astore 9
    //   199: aload 9
    //   201: invokeinterface 68 1 0
    //   206: istore 7
    //   208: iload 7
    //   210: ifne +22 -> 232
    //   213: aload_0
    //   214: getfield 28	com/truecaller/api/services/messenger/v1/Batched$Request:a	Lcom/google/f/w$h;
    //   217: astore 9
    //   219: aload 9
    //   221: invokestatic 72	com/google/f/q:mutableCopy	(Lcom/google/f/w$h;)Lcom/google/f/w$h;
    //   224: astore 9
    //   226: aload_0
    //   227: aload 9
    //   229: putfield 28	com/truecaller/api/services/messenger/v1/Batched$Request:a	Lcom/google/f/w$h;
    //   232: aload_0
    //   233: getfield 28	com/truecaller/api/services/messenger/v1/Batched$Request:a	Lcom/google/f/w$h;
    //   236: astore 9
    //   238: invokestatic 77	com/truecaller/api/services/messenger/v1/Batched$Request$Payload:a	()Lcom/google/f/ah;
    //   241: astore 10
    //   243: aload_2
    //   244: aload 10
    //   246: aload_3
    //   247: invokevirtual 81	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   250: astore 10
    //   252: aload 9
    //   254: aload 10
    //   256: invokeinterface 85 2 0
    //   261: pop
    //   262: goto -115 -> 147
    //   265: iconst_1
    //   266: istore 6
    //   268: goto -121 -> 147
    //   271: astore_1
    //   272: goto +53 -> 325
    //   275: astore_1
    //   276: new 87	java/lang/RuntimeException
    //   279: astore_2
    //   280: new 89	com/google/f/x
    //   283: astore_3
    //   284: aload_1
    //   285: invokevirtual 95	java/io/IOException:getMessage	()Ljava/lang/String;
    //   288: astore_1
    //   289: aload_3
    //   290: aload_1
    //   291: invokespecial 98	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   294: aload_3
    //   295: aload_0
    //   296: invokevirtual 102	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   299: astore_1
    //   300: aload_2
    //   301: aload_1
    //   302: invokespecial 105	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   305: aload_2
    //   306: athrow
    //   307: astore_1
    //   308: new 87	java/lang/RuntimeException
    //   311: astore_2
    //   312: aload_1
    //   313: aload_0
    //   314: invokevirtual 102	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   317: astore_1
    //   318: aload_2
    //   319: aload_1
    //   320: invokespecial 105	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   323: aload_2
    //   324: athrow
    //   325: aload_1
    //   326: athrow
    //   327: getstatic 18	com/truecaller/api/services/messenger/v1/Batched$Request:b	Lcom/truecaller/api/services/messenger/v1/Batched$Request;
    //   330: areturn
    //   331: aload_2
    //   332: checkcast 107	com/google/f/q$k
    //   335: astore_2
    //   336: aload_3
    //   337: checkcast 2	com/truecaller/api/services/messenger/v1/Batched$Request
    //   340: astore_3
    //   341: aload_0
    //   342: getfield 28	com/truecaller/api/services/messenger/v1/Batched$Request:a	Lcom/google/f/w$h;
    //   345: astore_1
    //   346: aload_3
    //   347: getfield 28	com/truecaller/api/services/messenger/v1/Batched$Request:a	Lcom/google/f/w$h;
    //   350: astore_3
    //   351: aload_2
    //   352: aload_1
    //   353: aload_3
    //   354: invokeinterface 111 3 0
    //   359: astore_1
    //   360: aload_0
    //   361: aload_1
    //   362: putfield 28	com/truecaller/api/services/messenger/v1/Batched$Request:a	Lcom/google/f/w$h;
    //   365: aload_0
    //   366: areturn
    //   367: new 113	com/truecaller/api/services/messenger/v1/Batched$Request$a
    //   370: astore_1
    //   371: aload_1
    //   372: iconst_0
    //   373: invokespecial 116	com/truecaller/api/services/messenger/v1/Batched$Request$a:<init>	(B)V
    //   376: aload_1
    //   377: areturn
    //   378: aload_0
    //   379: getfield 28	com/truecaller/api/services/messenger/v1/Batched$Request:a	Lcom/google/f/w$h;
    //   382: invokeinterface 117 1 0
    //   387: aconst_null
    //   388: areturn
    //   389: getstatic 18	com/truecaller/api/services/messenger/v1/Batched$Request:b	Lcom/truecaller/api/services/messenger/v1/Batched$Request;
    //   392: areturn
    //   393: new 2	com/truecaller/api/services/messenger/v1/Batched$Request
    //   396: astore_1
    //   397: aload_1
    //   398: invokespecial 16	com/truecaller/api/services/messenger/v1/Batched$Request:<init>	()V
    //   401: aload_1
    //   402: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	403	0	this	Request
    //   0	403	1	paramj	com.google.f.q.j
    //   0	403	2	paramObject1	Object
    //   0	403	3	paramObject2	Object
    //   3	20	4	arrayOfInt	int[]
    //   9	137	5	i	int
    //   19	248	6	j	int
    //   156	20	7	k	int
    //   180	29	7	bool	boolean
    //   165	7	8	m	int
    //   197	56	9	localh	w.h
    //   241	14	10	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   95	98	125	finally
    //   103	106	125	finally
    //   107	110	125	finally
    //   112	116	125	finally
    //   116	120	125	finally
    //   120	122	125	finally
    //   126	128	125	finally
    //   152	156	271	finally
    //   175	180	271	finally
    //   193	197	271	finally
    //   199	206	271	finally
    //   213	217	271	finally
    //   219	224	271	finally
    //   227	232	271	finally
    //   232	236	271	finally
    //   238	241	271	finally
    //   246	250	271	finally
    //   254	262	271	finally
    //   276	279	271	finally
    //   280	283	271	finally
    //   284	288	271	finally
    //   290	294	271	finally
    //   295	299	271	finally
    //   301	305	271	finally
    //   305	307	271	finally
    //   308	311	271	finally
    //   313	317	271	finally
    //   319	323	271	finally
    //   323	325	271	finally
    //   152	156	275	java/io/IOException
    //   175	180	275	java/io/IOException
    //   193	197	275	java/io/IOException
    //   199	206	275	java/io/IOException
    //   213	217	275	java/io/IOException
    //   219	224	275	java/io/IOException
    //   227	232	275	java/io/IOException
    //   232	236	275	java/io/IOException
    //   238	241	275	java/io/IOException
    //   246	250	275	java/io/IOException
    //   254	262	275	java/io/IOException
    //   152	156	307	com/google/f/x
    //   175	180	307	com/google/f/x
    //   193	197	307	com/google/f/x
    //   199	206	307	com/google/f/x
    //   213	217	307	com/google/f/x
    //   219	224	307	com/google/f/x
    //   227	232	307	com/google/f/x
    //   232	236	307	com/google/f/x
    //   238	241	307	com/google/f/x
    //   246	250	307	com/google/f/x
    //   254	262	307	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int j = -1;
    if (i != j) {
      return i;
    }
    i = 0;
    j = 0;
    for (;;)
    {
      Object localObject = a;
      int k = ((w.h)localObject).size();
      if (i >= k) {
        break;
      }
      localObject = (ae)a.get(i);
      int m = 1;
      k = h.computeMessageSize(m, (ae)localObject);
      j += k;
      i += 1;
    }
    memoizedSerializedSize = j;
    return j;
  }
  
  public final void writeTo(h paramh)
  {
    int i = 0;
    for (;;)
    {
      Object localObject = a;
      int j = ((w.h)localObject).size();
      if (i >= j) {
        break;
      }
      localObject = (ae)a.get(i);
      int k = 1;
      paramh.writeMessage(k, (ae)localObject);
      i += 1;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.Batched.Request
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */