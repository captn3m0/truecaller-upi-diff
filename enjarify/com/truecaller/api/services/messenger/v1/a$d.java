package com.truecaller.api.services.messenger.v1;

import com.google.f.ae;
import com.google.f.ah;
import com.google.f.h;
import com.google.f.q;
import com.google.f.w.h;
import java.util.List;

public final class a$d
  extends q
  implements a.e
{
  private static final d f;
  private static volatile ah g;
  private int a;
  private String b = "";
  private int c;
  private w.h d;
  private int e;
  
  static
  {
    d locald = new com/truecaller/api/services/messenger/v1/a$d;
    locald.<init>();
    f = locald;
    locald.makeImmutable();
  }
  
  private a$d()
  {
    w.h localh = emptyProtobufList();
    d = localh;
  }
  
  public static d d()
  {
    return f;
  }
  
  public final String a()
  {
    return b;
  }
  
  public final List b()
  {
    return d;
  }
  
  public final int c()
  {
    return e;
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 45	com/truecaller/api/services/messenger/v1/a$1:a	[I
    //   3: astore 4
    //   5: aload_1
    //   6: invokevirtual 51	com/google/f/q$j:ordinal	()I
    //   9: istore 5
    //   11: aload 4
    //   13: iload 5
    //   15: iaload
    //   16: istore 5
    //   18: iconst_0
    //   19: istore 6
    //   21: aconst_null
    //   22: astore 4
    //   24: iconst_1
    //   25: istore 7
    //   27: iload 5
    //   29: tableswitch	default:+47->76, 1:+678->707, 2:+674->703, 3:+663->692, 4:+652->681, 5:+371->400, 6:+109->138, 7:+367->396, 8:+57->86
    //   76: new 54	java/lang/UnsupportedOperationException
    //   79: astore_1
    //   80: aload_1
    //   81: invokespecial 55	java/lang/UnsupportedOperationException:<init>	()V
    //   84: aload_1
    //   85: athrow
    //   86: getstatic 57	com/truecaller/api/services/messenger/v1/a$d:g	Lcom/google/f/ah;
    //   89: astore_1
    //   90: aload_1
    //   91: ifnonnull +43 -> 134
    //   94: ldc 2
    //   96: astore_1
    //   97: aload_1
    //   98: monitorenter
    //   99: getstatic 57	com/truecaller/api/services/messenger/v1/a$d:g	Lcom/google/f/ah;
    //   102: astore_2
    //   103: aload_2
    //   104: ifnonnull +20 -> 124
    //   107: new 59	com/google/f/q$b
    //   110: astore_2
    //   111: getstatic 24	com/truecaller/api/services/messenger/v1/a$d:f	Lcom/truecaller/api/services/messenger/v1/a$d;
    //   114: astore_3
    //   115: aload_2
    //   116: aload_3
    //   117: invokespecial 62	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   120: aload_2
    //   121: putstatic 57	com/truecaller/api/services/messenger/v1/a$d:g	Lcom/google/f/ah;
    //   124: aload_1
    //   125: monitorexit
    //   126: goto +8 -> 134
    //   129: astore_2
    //   130: aload_1
    //   131: monitorexit
    //   132: aload_2
    //   133: athrow
    //   134: getstatic 57	com/truecaller/api/services/messenger/v1/a$d:g	Lcom/google/f/ah;
    //   137: areturn
    //   138: aload_2
    //   139: checkcast 64	com/google/f/g
    //   142: astore_2
    //   143: aload_3
    //   144: checkcast 66	com/google/f/n
    //   147: astore_3
    //   148: iload 6
    //   150: ifne +246 -> 396
    //   153: aload_2
    //   154: invokevirtual 69	com/google/f/g:readTag	()I
    //   157: istore 5
    //   159: iload 5
    //   161: ifeq +173 -> 334
    //   164: bipush 10
    //   166: istore 8
    //   168: iload 5
    //   170: iload 8
    //   172: if_icmpeq +149 -> 321
    //   175: bipush 16
    //   177: istore 8
    //   179: iload 5
    //   181: iload 8
    //   183: if_icmpeq +123 -> 306
    //   186: bipush 26
    //   188: istore 8
    //   190: iload 5
    //   192: iload 8
    //   194: if_icmpeq +48 -> 242
    //   197: bipush 32
    //   199: istore 8
    //   201: iload 5
    //   203: iload 8
    //   205: if_icmpeq +22 -> 227
    //   208: aload_2
    //   209: iload 5
    //   211: invokevirtual 77	com/google/f/g:skipField	(I)Z
    //   214: istore 5
    //   216: iload 5
    //   218: ifne -70 -> 148
    //   221: iconst_1
    //   222: istore 6
    //   224: goto -76 -> 148
    //   227: aload_2
    //   228: invokevirtual 80	com/google/f/g:readInt32	()I
    //   231: istore 5
    //   233: aload_0
    //   234: iload 5
    //   236: putfield 40	com/truecaller/api/services/messenger/v1/a$d:e	I
    //   239: goto -91 -> 148
    //   242: aload_0
    //   243: getfield 38	com/truecaller/api/services/messenger/v1/a$d:d	Lcom/google/f/w$h;
    //   246: astore_1
    //   247: aload_1
    //   248: invokeinterface 86 1 0
    //   253: istore 5
    //   255: iload 5
    //   257: ifne +18 -> 275
    //   260: aload_0
    //   261: getfield 38	com/truecaller/api/services/messenger/v1/a$d:d	Lcom/google/f/w$h;
    //   264: astore_1
    //   265: aload_1
    //   266: invokestatic 90	com/google/f/q:mutableCopy	(Lcom/google/f/w$h;)Lcom/google/f/w$h;
    //   269: astore_1
    //   270: aload_0
    //   271: aload_1
    //   272: putfield 38	com/truecaller/api/services/messenger/v1/a$d:d	Lcom/google/f/w$h;
    //   275: aload_0
    //   276: getfield 38	com/truecaller/api/services/messenger/v1/a$d:d	Lcom/google/f/w$h;
    //   279: astore_1
    //   280: invokestatic 95	com/truecaller/api/services/messenger/v1/models/Peer:e	()Lcom/google/f/ah;
    //   283: astore 9
    //   285: aload_2
    //   286: aload 9
    //   288: aload_3
    //   289: invokevirtual 99	com/google/f/g:readMessage	(Lcom/google/f/ah;Lcom/google/f/n;)Lcom/google/f/ae;
    //   292: astore 9
    //   294: aload_1
    //   295: aload 9
    //   297: invokeinterface 103 2 0
    //   302: pop
    //   303: goto -155 -> 148
    //   306: aload_2
    //   307: invokevirtual 80	com/google/f/g:readInt32	()I
    //   310: istore 5
    //   312: aload_0
    //   313: iload 5
    //   315: putfield 105	com/truecaller/api/services/messenger/v1/a$d:c	I
    //   318: goto -170 -> 148
    //   321: aload_2
    //   322: invokevirtual 109	com/google/f/g:readStringRequireUtf8	()Ljava/lang/String;
    //   325: astore_1
    //   326: aload_0
    //   327: aload_1
    //   328: putfield 32	com/truecaller/api/services/messenger/v1/a$d:b	Ljava/lang/String;
    //   331: goto -183 -> 148
    //   334: iconst_1
    //   335: istore 6
    //   337: goto -189 -> 148
    //   340: astore_1
    //   341: goto +53 -> 394
    //   344: astore_1
    //   345: new 111	java/lang/RuntimeException
    //   348: astore_2
    //   349: new 113	com/google/f/x
    //   352: astore_3
    //   353: aload_1
    //   354: invokevirtual 118	java/io/IOException:getMessage	()Ljava/lang/String;
    //   357: astore_1
    //   358: aload_3
    //   359: aload_1
    //   360: invokespecial 121	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   363: aload_3
    //   364: aload_0
    //   365: invokevirtual 125	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   368: astore_1
    //   369: aload_2
    //   370: aload_1
    //   371: invokespecial 128	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   374: aload_2
    //   375: athrow
    //   376: astore_1
    //   377: new 111	java/lang/RuntimeException
    //   380: astore_2
    //   381: aload_1
    //   382: aload_0
    //   383: invokevirtual 125	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   386: astore_1
    //   387: aload_2
    //   388: aload_1
    //   389: invokespecial 128	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   392: aload_2
    //   393: athrow
    //   394: aload_1
    //   395: athrow
    //   396: getstatic 24	com/truecaller/api/services/messenger/v1/a$d:f	Lcom/truecaller/api/services/messenger/v1/a$d;
    //   399: areturn
    //   400: aload_2
    //   401: checkcast 130	com/google/f/q$k
    //   404: astore_2
    //   405: aload_3
    //   406: checkcast 2	com/truecaller/api/services/messenger/v1/a$d
    //   409: astore_3
    //   410: aload_0
    //   411: getfield 32	com/truecaller/api/services/messenger/v1/a$d:b	Ljava/lang/String;
    //   414: invokevirtual 135	java/lang/String:isEmpty	()Z
    //   417: iload 7
    //   419: ixor
    //   420: istore 5
    //   422: aload_0
    //   423: getfield 32	com/truecaller/api/services/messenger/v1/a$d:b	Ljava/lang/String;
    //   426: astore 9
    //   428: aload_3
    //   429: getfield 32	com/truecaller/api/services/messenger/v1/a$d:b	Ljava/lang/String;
    //   432: astore 10
    //   434: aload 10
    //   436: invokevirtual 135	java/lang/String:isEmpty	()Z
    //   439: iload 7
    //   441: ixor
    //   442: istore 11
    //   444: aload_3
    //   445: getfield 32	com/truecaller/api/services/messenger/v1/a$d:b	Ljava/lang/String;
    //   448: astore 12
    //   450: aload_2
    //   451: iload 5
    //   453: aload 9
    //   455: iload 11
    //   457: aload 12
    //   459: invokeinterface 139 5 0
    //   464: astore_1
    //   465: aload_0
    //   466: aload_1
    //   467: putfield 32	com/truecaller/api/services/messenger/v1/a$d:b	Ljava/lang/String;
    //   470: aload_0
    //   471: getfield 105	com/truecaller/api/services/messenger/v1/a$d:c	I
    //   474: istore 5
    //   476: iload 5
    //   478: ifeq +9 -> 487
    //   481: iconst_1
    //   482: istore 5
    //   484: goto +8 -> 492
    //   487: iconst_0
    //   488: istore 5
    //   490: aconst_null
    //   491: astore_1
    //   492: aload_0
    //   493: getfield 105	com/truecaller/api/services/messenger/v1/a$d:c	I
    //   496: istore 8
    //   498: aload_3
    //   499: getfield 105	com/truecaller/api/services/messenger/v1/a$d:c	I
    //   502: istore 11
    //   504: iload 11
    //   506: ifeq +9 -> 515
    //   509: iconst_1
    //   510: istore 11
    //   512: goto +9 -> 521
    //   515: iconst_0
    //   516: istore 11
    //   518: aconst_null
    //   519: astore 10
    //   521: aload_3
    //   522: getfield 105	com/truecaller/api/services/messenger/v1/a$d:c	I
    //   525: istore 13
    //   527: aload_2
    //   528: iload 5
    //   530: iload 8
    //   532: iload 11
    //   534: iload 13
    //   536: invokeinterface 143 5 0
    //   541: istore 5
    //   543: aload_0
    //   544: iload 5
    //   546: putfield 105	com/truecaller/api/services/messenger/v1/a$d:c	I
    //   549: aload_0
    //   550: getfield 38	com/truecaller/api/services/messenger/v1/a$d:d	Lcom/google/f/w$h;
    //   553: astore_1
    //   554: aload_3
    //   555: getfield 38	com/truecaller/api/services/messenger/v1/a$d:d	Lcom/google/f/w$h;
    //   558: astore 9
    //   560: aload_2
    //   561: aload_1
    //   562: aload 9
    //   564: invokeinterface 147 3 0
    //   569: astore_1
    //   570: aload_0
    //   571: aload_1
    //   572: putfield 38	com/truecaller/api/services/messenger/v1/a$d:d	Lcom/google/f/w$h;
    //   575: aload_0
    //   576: getfield 40	com/truecaller/api/services/messenger/v1/a$d:e	I
    //   579: istore 5
    //   581: iload 5
    //   583: ifeq +9 -> 592
    //   586: iconst_1
    //   587: istore 5
    //   589: goto +8 -> 597
    //   592: iconst_0
    //   593: istore 5
    //   595: aconst_null
    //   596: astore_1
    //   597: aload_0
    //   598: getfield 40	com/truecaller/api/services/messenger/v1/a$d:e	I
    //   601: istore 8
    //   603: aload_3
    //   604: getfield 40	com/truecaller/api/services/messenger/v1/a$d:e	I
    //   607: istore 11
    //   609: iload 11
    //   611: ifeq +6 -> 617
    //   614: iconst_1
    //   615: istore 6
    //   617: aload_3
    //   618: getfield 40	com/truecaller/api/services/messenger/v1/a$d:e	I
    //   621: istore 7
    //   623: aload_2
    //   624: iload 5
    //   626: iload 8
    //   628: iload 6
    //   630: iload 7
    //   632: invokeinterface 143 5 0
    //   637: istore 5
    //   639: aload_0
    //   640: iload 5
    //   642: putfield 40	com/truecaller/api/services/messenger/v1/a$d:e	I
    //   645: getstatic 153	com/google/f/q$i:INSTANCE	Lcom/google/f/q$i;
    //   648: astore_1
    //   649: aload_2
    //   650: aload_1
    //   651: if_acmpne +28 -> 679
    //   654: aload_0
    //   655: getfield 155	com/truecaller/api/services/messenger/v1/a$d:a	I
    //   658: istore 5
    //   660: aload_3
    //   661: getfield 155	com/truecaller/api/services/messenger/v1/a$d:a	I
    //   664: istore 14
    //   666: iload 5
    //   668: iload 14
    //   670: ior
    //   671: istore 5
    //   673: aload_0
    //   674: iload 5
    //   676: putfield 155	com/truecaller/api/services/messenger/v1/a$d:a	I
    //   679: aload_0
    //   680: areturn
    //   681: new 157	com/truecaller/api/services/messenger/v1/a$d$a
    //   684: astore_1
    //   685: aload_1
    //   686: iconst_0
    //   687: invokespecial 160	com/truecaller/api/services/messenger/v1/a$d$a:<init>	(B)V
    //   690: aload_1
    //   691: areturn
    //   692: aload_0
    //   693: getfield 38	com/truecaller/api/services/messenger/v1/a$d:d	Lcom/google/f/w$h;
    //   696: invokeinterface 161 1 0
    //   701: aconst_null
    //   702: areturn
    //   703: getstatic 24	com/truecaller/api/services/messenger/v1/a$d:f	Lcom/truecaller/api/services/messenger/v1/a$d;
    //   706: areturn
    //   707: new 2	com/truecaller/api/services/messenger/v1/a$d
    //   710: astore_1
    //   711: aload_1
    //   712: invokespecial 22	com/truecaller/api/services/messenger/v1/a$d:<init>	()V
    //   715: aload_1
    //   716: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	717	0	this	d
    //   0	717	1	paramj	com.google.f.q.j
    //   0	717	2	paramObject1	Object
    //   0	717	3	paramObject2	Object
    //   3	20	4	arrayOfInt	int[]
    //   9	201	5	i	int
    //   214	3	5	bool1	boolean
    //   231	4	5	j	int
    //   253	3	5	bool2	boolean
    //   310	4	5	k	int
    //   420	32	5	bool3	boolean
    //   474	55	5	m	int
    //   541	84	5	n	int
    //   637	38	5	i1	int
    //   19	610	6	bool4	boolean
    //   25	606	7	i2	int
    //   166	461	8	i3	int
    //   283	280	9	localObject	Object
    //   432	88	10	str1	String
    //   442	14	11	bool5	boolean
    //   502	31	11	i4	int
    //   607	3	11	i5	int
    //   448	10	12	str2	String
    //   525	10	13	i6	int
    //   664	7	14	i7	int
    // Exception table:
    //   from	to	target	type
    //   99	102	129	finally
    //   107	110	129	finally
    //   111	114	129	finally
    //   116	120	129	finally
    //   120	124	129	finally
    //   124	126	129	finally
    //   130	132	129	finally
    //   153	157	340	finally
    //   209	214	340	finally
    //   227	231	340	finally
    //   234	239	340	finally
    //   242	246	340	finally
    //   247	253	340	finally
    //   260	264	340	finally
    //   265	269	340	finally
    //   271	275	340	finally
    //   275	279	340	finally
    //   280	283	340	finally
    //   288	292	340	finally
    //   295	303	340	finally
    //   306	310	340	finally
    //   313	318	340	finally
    //   321	325	340	finally
    //   327	331	340	finally
    //   345	348	340	finally
    //   349	352	340	finally
    //   353	357	340	finally
    //   359	363	340	finally
    //   364	368	340	finally
    //   370	374	340	finally
    //   374	376	340	finally
    //   377	380	340	finally
    //   382	386	340	finally
    //   388	392	340	finally
    //   392	394	340	finally
    //   153	157	344	java/io/IOException
    //   209	214	344	java/io/IOException
    //   227	231	344	java/io/IOException
    //   234	239	344	java/io/IOException
    //   242	246	344	java/io/IOException
    //   247	253	344	java/io/IOException
    //   260	264	344	java/io/IOException
    //   265	269	344	java/io/IOException
    //   271	275	344	java/io/IOException
    //   275	279	344	java/io/IOException
    //   280	283	344	java/io/IOException
    //   288	292	344	java/io/IOException
    //   295	303	344	java/io/IOException
    //   306	310	344	java/io/IOException
    //   313	318	344	java/io/IOException
    //   321	325	344	java/io/IOException
    //   327	331	344	java/io/IOException
    //   153	157	376	com/google/f/x
    //   209	214	376	com/google/f/x
    //   227	231	376	com/google/f/x
    //   234	239	376	com/google/f/x
    //   242	246	376	com/google/f/x
    //   247	253	376	com/google/f/x
    //   260	264	376	com/google/f/x
    //   265	269	376	com/google/f/x
    //   271	275	376	com/google/f/x
    //   275	279	376	com/google/f/x
    //   280	283	376	com/google/f/x
    //   288	292	376	com/google/f/x
    //   295	303	376	com/google/f/x
    //   306	310	376	com/google/f/x
    //   313	318	376	com/google/f/x
    //   321	325	376	com/google/f/x
    //   327	331	376	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int k = -1;
    if (i != k) {
      return i;
    }
    String str = b;
    boolean bool = str.isEmpty();
    k = 0;
    int j;
    if (!bool)
    {
      str = b;
      m = 1;
      j = h.computeStringSize(m, str) + 0;
    }
    else
    {
      j = 0;
      str = null;
    }
    int m = c;
    if (m != 0)
    {
      int n = 2;
      m = h.computeInt32Size(n, m);
      j += m;
    }
    for (;;)
    {
      w.h localh = d;
      m = localh.size();
      if (k >= m) {
        break;
      }
      ae localae = (ae)d.get(k);
      m = h.computeMessageSize(3, localae);
      j += m;
      k += 1;
    }
    k = e;
    if (k != 0)
    {
      m = 4;
      k = h.computeInt32Size(m, k);
      j += k;
    }
    memoizedSerializedSize = j;
    return j;
  }
  
  public final void writeTo(h paramh)
  {
    String str = b;
    boolean bool = str.isEmpty();
    int j;
    if (!bool)
    {
      str = b;
      j = 1;
      paramh.writeString(j, str);
    }
    int i = c;
    if (i != 0)
    {
      j = 2;
      paramh.writeInt32(j, i);
    }
    i = 0;
    str = null;
    for (;;)
    {
      w.h localh = d;
      j = localh.size();
      if (i >= j) {
        break;
      }
      j = 3;
      ae localae = (ae)d.get(i);
      paramh.writeMessage(j, localae);
      i += 1;
    }
    i = e;
    if (i != 0)
    {
      j = 4;
      paramh.writeInt32(j, i);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.a.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */