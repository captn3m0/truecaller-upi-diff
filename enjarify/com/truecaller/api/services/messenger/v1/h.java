package com.truecaller.api.services.messenger.v1;

import com.google.f.ah;
import com.google.f.q;

public final class h
  extends q
  implements i
{
  private static final h a;
  private static volatile ah b;
  
  static
  {
    h localh = new com/truecaller/api/services/messenger/v1/h;
    localh.<init>();
    a = localh;
    localh.makeImmutable();
  }
  
  /* Error */
  public final Object dynamicMethod(com.google.f.q.j paramj, Object paramObject1, Object paramObject2)
  {
    // Byte code:
    //   0: getstatic 25	com/truecaller/api/services/messenger/v1/h$1:a	[I
    //   3: astore_3
    //   4: aload_1
    //   5: invokevirtual 31	com/google/f/q$j:ordinal	()I
    //   8: istore 4
    //   10: aload_3
    //   11: iload 4
    //   13: iaload
    //   14: istore 4
    //   16: iconst_0
    //   17: istore 5
    //   19: aconst_null
    //   20: astore_3
    //   21: iload 4
    //   23: tableswitch	default:+45->68, 1:+235->258, 2:+231->254, 3:+229->252, 4:+218->241, 5:+216->239, 6:+107->130, 7:+212->235, 8:+55->78
    //   68: new 33	java/lang/UnsupportedOperationException
    //   71: astore_1
    //   72: aload_1
    //   73: invokespecial 34	java/lang/UnsupportedOperationException:<init>	()V
    //   76: aload_1
    //   77: athrow
    //   78: getstatic 36	com/truecaller/api/services/messenger/v1/h:b	Lcom/google/f/ah;
    //   81: astore_1
    //   82: aload_1
    //   83: ifnonnull +43 -> 126
    //   86: ldc 2
    //   88: astore_1
    //   89: aload_1
    //   90: monitorenter
    //   91: getstatic 36	com/truecaller/api/services/messenger/v1/h:b	Lcom/google/f/ah;
    //   94: astore_2
    //   95: aload_2
    //   96: ifnonnull +20 -> 116
    //   99: new 38	com/google/f/q$b
    //   102: astore_2
    //   103: getstatic 16	com/truecaller/api/services/messenger/v1/h:a	Lcom/truecaller/api/services/messenger/v1/h;
    //   106: astore_3
    //   107: aload_2
    //   108: aload_3
    //   109: invokespecial 41	com/google/f/q$b:<init>	(Lcom/google/f/q;)V
    //   112: aload_2
    //   113: putstatic 36	com/truecaller/api/services/messenger/v1/h:b	Lcom/google/f/ah;
    //   116: aload_1
    //   117: monitorexit
    //   118: goto +8 -> 126
    //   121: astore_2
    //   122: aload_1
    //   123: monitorexit
    //   124: aload_2
    //   125: athrow
    //   126: getstatic 36	com/truecaller/api/services/messenger/v1/h:b	Lcom/google/f/ah;
    //   129: areturn
    //   130: aload_2
    //   131: checkcast 43	com/google/f/g
    //   134: astore_2
    //   135: iconst_1
    //   136: istore 4
    //   138: iload 5
    //   140: ifne +95 -> 235
    //   143: aload_2
    //   144: invokevirtual 47	com/google/f/g:readTag	()I
    //   147: istore 6
    //   149: iload 6
    //   151: ifeq +22 -> 173
    //   154: aload_2
    //   155: iload 6
    //   157: invokevirtual 51	com/google/f/g:skipField	(I)Z
    //   160: istore 6
    //   162: iload 6
    //   164: ifne -26 -> 138
    //   167: iconst_1
    //   168: istore 5
    //   170: goto -32 -> 138
    //   173: iconst_1
    //   174: istore 5
    //   176: goto -38 -> 138
    //   179: astore_1
    //   180: goto +53 -> 233
    //   183: astore_1
    //   184: new 53	java/lang/RuntimeException
    //   187: astore_2
    //   188: new 55	com/google/f/x
    //   191: astore_3
    //   192: aload_1
    //   193: invokevirtual 61	java/io/IOException:getMessage	()Ljava/lang/String;
    //   196: astore_1
    //   197: aload_3
    //   198: aload_1
    //   199: invokespecial 64	com/google/f/x:<init>	(Ljava/lang/String;)V
    //   202: aload_3
    //   203: aload_0
    //   204: invokevirtual 68	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   207: astore_1
    //   208: aload_2
    //   209: aload_1
    //   210: invokespecial 71	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   213: aload_2
    //   214: athrow
    //   215: astore_1
    //   216: new 53	java/lang/RuntimeException
    //   219: astore_2
    //   220: aload_1
    //   221: aload_0
    //   222: invokevirtual 68	com/google/f/x:setUnfinishedMessage	(Lcom/google/f/ae;)Lcom/google/f/x;
    //   225: astore_1
    //   226: aload_2
    //   227: aload_1
    //   228: invokespecial 71	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   231: aload_2
    //   232: athrow
    //   233: aload_1
    //   234: athrow
    //   235: getstatic 16	com/truecaller/api/services/messenger/v1/h:a	Lcom/truecaller/api/services/messenger/v1/h;
    //   238: areturn
    //   239: aload_0
    //   240: areturn
    //   241: new 73	com/truecaller/api/services/messenger/v1/h$a
    //   244: astore_1
    //   245: aload_1
    //   246: iconst_0
    //   247: invokespecial 76	com/truecaller/api/services/messenger/v1/h$a:<init>	(B)V
    //   250: aload_1
    //   251: areturn
    //   252: aconst_null
    //   253: areturn
    //   254: getstatic 16	com/truecaller/api/services/messenger/v1/h:a	Lcom/truecaller/api/services/messenger/v1/h;
    //   257: areturn
    //   258: new 2	com/truecaller/api/services/messenger/v1/h
    //   261: astore_1
    //   262: aload_1
    //   263: invokespecial 14	com/truecaller/api/services/messenger/v1/h:<init>	()V
    //   266: aload_1
    //   267: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	268	0	this	h
    //   0	268	1	paramj	com.google.f.q.j
    //   0	268	2	paramObject1	Object
    //   0	268	3	paramObject2	Object
    //   8	129	4	i	int
    //   17	158	5	j	int
    //   147	9	6	k	int
    //   160	3	6	bool	boolean
    // Exception table:
    //   from	to	target	type
    //   91	94	121	finally
    //   99	102	121	finally
    //   103	106	121	finally
    //   108	112	121	finally
    //   112	116	121	finally
    //   116	118	121	finally
    //   122	124	121	finally
    //   143	147	179	finally
    //   155	160	179	finally
    //   184	187	179	finally
    //   188	191	179	finally
    //   192	196	179	finally
    //   198	202	179	finally
    //   203	207	179	finally
    //   209	213	179	finally
    //   213	215	179	finally
    //   216	219	179	finally
    //   221	225	179	finally
    //   227	231	179	finally
    //   231	233	179	finally
    //   143	147	183	java/io/IOException
    //   155	160	183	java/io/IOException
    //   143	147	215	com/google/f/x
    //   155	160	215	com/google/f/x
  }
  
  public final int getSerializedSize()
  {
    int i = memoizedSerializedSize;
    int j = -1;
    if (i != j) {
      return i;
    }
    memoizedSerializedSize = 0;
    return 0;
  }
  
  public final void writeTo(com.google.f.h paramh) {}
}

/* Location:
 * Qualified Name:     com.truecaller.api.services.messenger.v1.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */