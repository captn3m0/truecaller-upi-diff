package com.truecaller.referral;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.v4.f.a;
import com.truecaller.common.h.am;
import com.truecaller.log.AssertionUtil;
import java.util.Collection;
import java.util.List;
import org.c.a.a.a.k;

class ReferralUrl
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  static a a;
  static final a b;
  static final a c;
  ReferralUrl.a d;
  ReferralManager.ReferralLaunchContext e;
  private final String f;
  
  static
  {
    Object localObject1 = new android/support/v4/f/a;
    ((a)localObject1).<init>();
    a = (a)localObject1;
    Object localObject2 = ReferralUrl.a.d;
    ((a)localObject1).put("com.whatsapp", localObject2);
    localObject1 = a;
    localObject2 = ReferralUrl.a.g;
    ((a)localObject1).put("com.facebook.orca", localObject2);
    localObject1 = a;
    localObject2 = ReferralUrl.a.f;
    ((a)localObject1).put("com.facebook.katana", localObject2);
    localObject1 = a;
    localObject2 = ReferralUrl.a.e;
    ((a)localObject1).put("com.twitter.android", localObject2);
    localObject1 = a;
    localObject2 = ReferralUrl.a.k;
    ((a)localObject1).put("com.imo.android.imoim", localObject2);
    localObject1 = a;
    localObject2 = ReferralUrl.a.h;
    ((a)localObject1).put("com.snapchat.android", localObject2);
    localObject1 = new android/support/v4/f/a;
    ((a)localObject1).<init>();
    b = (a)localObject1;
    Object localObject3 = ReferralManager.ReferralLaunchContext.HOME_SCREEN;
    char c1 = 'a';
    Character localCharacter1 = Character.valueOf(c1);
    ((a)localObject1).put(localObject3, localCharacter1);
    localObject1 = b;
    localObject3 = ReferralManager.ReferralLaunchContext.INBOX_OVERFLOW;
    char c2 = 'b';
    Character localCharacter2 = Character.valueOf(c2);
    ((a)localObject1).put(localObject3, localCharacter2);
    localObject1 = b;
    localObject3 = ReferralManager.ReferralLaunchContext.CONTACT_DETAILS;
    char c3 = 'c';
    Character localCharacter3 = Character.valueOf(c3);
    ((a)localObject1).put(localObject3, localCharacter3);
    localObject1 = b;
    localObject3 = ReferralManager.ReferralLaunchContext.CONTACTS;
    char c4 = 'd';
    Character localCharacter4 = Character.valueOf(c4);
    ((a)localObject1).put(localObject3, localCharacter4);
    localObject1 = b;
    localObject3 = ReferralManager.ReferralLaunchContext.USER_BUSY_PROMPT;
    char c5 = 'e';
    Character localCharacter5 = Character.valueOf(c5);
    ((a)localObject1).put(localObject3, localCharacter5);
    localObject1 = b;
    localObject3 = ReferralManager.ReferralLaunchContext.AFTER_CALL;
    char c6 = 'f';
    Character localCharacter6 = Character.valueOf(c6);
    ((a)localObject1).put(localObject3, localCharacter6);
    localObject1 = b;
    localObject3 = ReferralManager.ReferralLaunchContext.AFTER_CALL_SAVE_CONTACT;
    char c7 = 'g';
    Character localCharacter7 = Character.valueOf(c7);
    ((a)localObject1).put(localObject3, localCharacter7);
    localObject1 = b;
    localObject3 = ReferralManager.ReferralLaunchContext.NAVIGATION_DRAWER;
    char c8 = 'h';
    Character localCharacter8 = Character.valueOf(c8);
    ((a)localObject1).put(localObject3, localCharacter8);
    localObject1 = b;
    localObject3 = ReferralManager.ReferralLaunchContext.PUSH_NOTIFICATION;
    char c9 = 'i';
    Character localCharacter9 = Character.valueOf(c9);
    ((a)localObject1).put(localObject3, localCharacter9);
    localObject1 = b;
    localObject3 = ReferralManager.ReferralLaunchContext.DEEP_LINK;
    char c10 = 'j';
    Character localCharacter10 = Character.valueOf(c10);
    ((a)localObject1).put(localObject3, localCharacter10);
    localObject1 = b;
    localObject3 = ReferralManager.ReferralLaunchContext.AFTER_CALL_PROMO;
    char c11 = 'k';
    Character localCharacter11 = Character.valueOf(c11);
    ((a)localObject1).put(localObject3, localCharacter11);
    localObject1 = new android/support/v4/f/a;
    ((a)localObject1).<init>();
    c = (a)localObject1;
    localObject3 = ReferralUrl.a.d;
    localObject2 = Character.valueOf(c1);
    ((a)localObject1).put(localObject3, localObject2);
    localObject1 = c;
    localObject3 = ReferralUrl.a.g;
    localObject2 = Character.valueOf(c2);
    ((a)localObject1).put(localObject3, localObject2);
    localObject1 = c;
    localObject3 = ReferralUrl.a.f;
    localObject2 = Character.valueOf(c3);
    ((a)localObject1).put(localObject3, localObject2);
    localObject1 = c;
    localObject3 = ReferralUrl.a.e;
    localObject2 = Character.valueOf(c4);
    ((a)localObject1).put(localObject3, localObject2);
    localObject1 = c;
    localObject3 = ReferralUrl.a.h;
    localObject2 = Character.valueOf(c5);
    ((a)localObject1).put(localObject3, localObject2);
    localObject1 = c;
    localObject3 = ReferralUrl.a.i;
    localObject2 = Character.valueOf(c6);
    ((a)localObject1).put(localObject3, localObject2);
    localObject1 = c;
    localObject3 = ReferralUrl.a.a;
    localObject2 = Character.valueOf(c7);
    ((a)localObject1).put(localObject3, localObject2);
    localObject1 = c;
    localObject3 = ReferralUrl.a.b;
    localObject2 = Character.valueOf(c8);
    ((a)localObject1).put(localObject3, localObject2);
    localObject1 = c;
    localObject3 = ReferralUrl.a.k;
    localObject2 = Character.valueOf(c9);
    ((a)localObject1).put(localObject3, localObject2);
    localObject1 = c;
    localObject3 = ReferralUrl.a.l;
    localObject2 = Character.valueOf(c10);
    ((a)localObject1).put(localObject3, localObject2);
    localObject1 = c;
    localObject3 = ReferralUrl.a.c;
    localObject2 = Character.valueOf(c11);
    ((a)localObject1).put(localObject3, localObject2);
    localObject1 = new com/truecaller/referral/ReferralUrl$1;
    ((ReferralUrl.1)localObject1).<init>();
    CREATOR = (Parcelable.Creator)localObject1;
  }
  
  protected ReferralUrl(Parcel paramParcel)
  {
    Object localObject = ReferralUrl.a.k;
    d = ((ReferralUrl.a)localObject);
    int i = paramParcel.readInt();
    ReferralManager.ReferralLaunchContext localReferralLaunchContext = null;
    int j = -1;
    if (i == j)
    {
      i = 0;
      localObject = null;
    }
    else
    {
      ReferralUrl.a[] arrayOfa = ReferralUrl.a.values();
      localObject = arrayOfa[i];
    }
    d = ((ReferralUrl.a)localObject);
    localObject = paramParcel.readString();
    f = ((String)localObject);
    int k = paramParcel.readInt();
    if (k != j)
    {
      localObject = ReferralManager.ReferralLaunchContext.values();
      localReferralLaunchContext = localObject[k];
    }
    e = localReferralLaunchContext;
  }
  
  private ReferralUrl(String paramString)
  {
    ReferralUrl.a locala = ReferralUrl.a.k;
    d = locala;
    f = paramString;
  }
  
  static ReferralUrl a(Uri paramUri)
  {
    Object localObject1 = paramUri.getPathSegments();
    if (localObject1 != null)
    {
      String str = "promo";
      boolean bool = ((List)localObject1).contains(str);
      if (bool)
      {
        str = "promo";
        int i = ((List)localObject1).indexOf(str);
        int j = 2;
        i += j;
        int k = ((List)localObject1).size();
        if (k > i)
        {
          localObject1 = (String)((List)localObject1).get(i);
          i = am.f((CharSequence)localObject1);
          if (i == j)
          {
            str = paramUri.toString();
            j = str.indexOf((String)localObject1);
            k = 0;
            Object localObject2 = null;
            str = str.substring(0, j);
            Object localObject3;
            try
            {
              localObject3 = b;
              k = ((String)localObject1).charAt(0);
              localObject2 = Character.valueOf(k);
              localObject3 = a((a)localObject3, localObject2);
              localObject3 = (ReferralManager.ReferralLaunchContext)localObject3;
            }
            catch (Exception localException1)
            {
              AssertionUtil.reportThrowableButNeverCrash(localException1);
              j = 0;
              localObject3 = null;
            }
            try
            {
              localObject2 = c;
              int m = 1;
              c1 = ((String)localObject1).charAt(m);
              localObject1 = Character.valueOf(c1);
              localObject1 = a((a)localObject2, localObject1);
              localObject1 = (ReferralUrl.a)localObject1;
            }
            catch (Exception localException2)
            {
              AssertionUtil.reportThrowableButNeverCrash(localException2);
              char c1 = '\000';
              localObject1 = null;
            }
            if ((localObject1 != null) && (localObject3 != null))
            {
              paramUri = a(str);
              d = ((ReferralUrl.a)localObject1);
              e = ((ReferralManager.ReferralLaunchContext)localObject3);
              return paramUri;
            }
            localObject1 = new java/lang/IllegalArgumentException;
            str = "Share medium or context is null for referral link: ";
            paramUri = String.valueOf(paramUri);
            paramUri = str.concat(paramUri);
            ((IllegalArgumentException)localObject1).<init>(paramUri);
            AssertionUtil.reportThrowableButNeverCrash((Throwable)localObject1);
          }
        }
      }
    }
    return null;
  }
  
  static ReferralUrl a(String paramString)
  {
    ReferralUrl localReferralUrl = new com/truecaller/referral/ReferralUrl;
    localReferralUrl.<init>(paramString);
    return localReferralUrl;
  }
  
  private static Object a(a parama, Object paramObject)
  {
    int i = 0;
    for (;;)
    {
      Object localObject = parama.values();
      int j = ((Collection)localObject).size();
      if (i >= j) {
        break;
      }
      localObject = parama.c(i);
      if (localObject == paramObject) {
        return parama.b(i);
      }
      i += 1;
    }
    return null;
  }
  
  static ReferralUrl.a b(String paramString)
  {
    a locala = a;
    paramString = (ReferralUrl.a)locala.get(paramString);
    if (paramString == null) {
      return ReferralUrl.a.k;
    }
    return paramString;
  }
  
  private static boolean c(String paramString)
  {
    return am.b(Uri.parse(paramString).getHost(), "truecaller.com");
  }
  
  final String a()
  {
    Object localObject1 = f;
    if (localObject1 != null)
    {
      localObject1 = e;
      if (localObject1 != null)
      {
        localObject1 = (Character)b.get(localObject1);
        localObject2 = c;
        localObject3 = d;
        localObject2 = (Character)((a)localObject2).get(localObject3);
        localObject3 = f;
        boolean bool1 = c((String)localObject3);
        if (bool1)
        {
          localObject3 = new java/lang/StringBuilder;
          ((StringBuilder)localObject3).<init>();
          String str1 = f;
          ((StringBuilder)localObject3).append(str1);
          str1 = f;
          String str2 = "/";
          boolean bool2 = k.f(str1, str2);
          if (bool2) {
            str1 = "";
          } else {
            str1 = "/";
          }
          ((StringBuilder)localObject3).append(str1);
          ((StringBuilder)localObject3).append(localObject1);
          ((StringBuilder)localObject3).append(localObject2);
          return ((StringBuilder)localObject3).toString();
        }
        return f;
      }
    }
    localObject1 = new java/lang/NullPointerException;
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("Referral url and source should not be null. Url : ");
    Object localObject3 = f;
    ((StringBuilder)localObject2).append((String)localObject3);
    ((StringBuilder)localObject2).append(" source: ");
    localObject3 = f;
    ((StringBuilder)localObject2).append((String)localObject3);
    localObject2 = ((StringBuilder)localObject2).toString();
    ((NullPointerException)localObject1).<init>((String)localObject2);
    throw ((Throwable)localObject1);
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    Object localObject = d;
    int i = -1;
    if (localObject == null) {
      paramInt = -1;
    } else {
      paramInt = ((ReferralUrl.a)localObject).ordinal();
    }
    paramParcel.writeInt(paramInt);
    localObject = f;
    paramParcel.writeString((String)localObject);
    localObject = e;
    if (localObject != null) {
      i = ((ReferralManager.ReferralLaunchContext)localObject).ordinal();
    }
    paramParcel.writeInt(i);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.referral.ReferralUrl
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */