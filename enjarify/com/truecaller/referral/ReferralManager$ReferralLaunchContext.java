package com.truecaller.referral;

public enum ReferralManager$ReferralLaunchContext
{
  static
  {
    Object localObject = new com/truecaller/referral/ReferralManager$ReferralLaunchContext;
    ((ReferralLaunchContext)localObject).<init>("HOME_SCREEN", 0);
    HOME_SCREEN = (ReferralLaunchContext)localObject;
    localObject = new com/truecaller/referral/ReferralManager$ReferralLaunchContext;
    int i = 1;
    ((ReferralLaunchContext)localObject).<init>("INBOX_OVERFLOW", i);
    INBOX_OVERFLOW = (ReferralLaunchContext)localObject;
    localObject = new com/truecaller/referral/ReferralManager$ReferralLaunchContext;
    int j = 2;
    ((ReferralLaunchContext)localObject).<init>("CONTACT_DETAILS", j);
    CONTACT_DETAILS = (ReferralLaunchContext)localObject;
    localObject = new com/truecaller/referral/ReferralManager$ReferralLaunchContext;
    int k = 3;
    ((ReferralLaunchContext)localObject).<init>("CONTACTS", k);
    CONTACTS = (ReferralLaunchContext)localObject;
    localObject = new com/truecaller/referral/ReferralManager$ReferralLaunchContext;
    int m = 4;
    ((ReferralLaunchContext)localObject).<init>("USER_BUSY_PROMPT", m);
    USER_BUSY_PROMPT = (ReferralLaunchContext)localObject;
    localObject = new com/truecaller/referral/ReferralManager$ReferralLaunchContext;
    int n = 5;
    ((ReferralLaunchContext)localObject).<init>("AFTER_CALL", n);
    AFTER_CALL = (ReferralLaunchContext)localObject;
    localObject = new com/truecaller/referral/ReferralManager$ReferralLaunchContext;
    int i1 = 6;
    ((ReferralLaunchContext)localObject).<init>("AFTER_CALL_SAVE_CONTACT", i1);
    AFTER_CALL_SAVE_CONTACT = (ReferralLaunchContext)localObject;
    localObject = new com/truecaller/referral/ReferralManager$ReferralLaunchContext;
    int i2 = 7;
    ((ReferralLaunchContext)localObject).<init>("NAVIGATION_DRAWER", i2);
    NAVIGATION_DRAWER = (ReferralLaunchContext)localObject;
    localObject = new com/truecaller/referral/ReferralManager$ReferralLaunchContext;
    int i3 = 8;
    ((ReferralLaunchContext)localObject).<init>("PUSH_NOTIFICATION", i3);
    PUSH_NOTIFICATION = (ReferralLaunchContext)localObject;
    localObject = new com/truecaller/referral/ReferralManager$ReferralLaunchContext;
    int i4 = 9;
    ((ReferralLaunchContext)localObject).<init>("DEEP_LINK", i4);
    DEEP_LINK = (ReferralLaunchContext)localObject;
    localObject = new com/truecaller/referral/ReferralManager$ReferralLaunchContext;
    int i5 = 10;
    ((ReferralLaunchContext)localObject).<init>("AFTER_CALL_PROMO", i5);
    AFTER_CALL_PROMO = (ReferralLaunchContext)localObject;
    localObject = new com/truecaller/referral/ReferralManager$ReferralLaunchContext;
    int i6 = 11;
    ((ReferralLaunchContext)localObject).<init>("SEARCH_SCREEN_PROMO", i6);
    SEARCH_SCREEN_PROMO = (ReferralLaunchContext)localObject;
    localObject = new ReferralLaunchContext[12];
    ReferralLaunchContext localReferralLaunchContext = HOME_SCREEN;
    localObject[0] = localReferralLaunchContext;
    localReferralLaunchContext = INBOX_OVERFLOW;
    localObject[i] = localReferralLaunchContext;
    localReferralLaunchContext = CONTACT_DETAILS;
    localObject[j] = localReferralLaunchContext;
    localReferralLaunchContext = CONTACTS;
    localObject[k] = localReferralLaunchContext;
    localReferralLaunchContext = USER_BUSY_PROMPT;
    localObject[m] = localReferralLaunchContext;
    localReferralLaunchContext = AFTER_CALL;
    localObject[n] = localReferralLaunchContext;
    localReferralLaunchContext = AFTER_CALL_SAVE_CONTACT;
    localObject[i1] = localReferralLaunchContext;
    localReferralLaunchContext = NAVIGATION_DRAWER;
    localObject[i2] = localReferralLaunchContext;
    localReferralLaunchContext = PUSH_NOTIFICATION;
    localObject[i3] = localReferralLaunchContext;
    localReferralLaunchContext = DEEP_LINK;
    localObject[i4] = localReferralLaunchContext;
    localReferralLaunchContext = AFTER_CALL_PROMO;
    localObject[i5] = localReferralLaunchContext;
    localReferralLaunchContext = SEARCH_SCREEN_PROMO;
    localObject[i6] = localReferralLaunchContext;
    $VALUES = (ReferralLaunchContext[])localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.referral.ReferralManager.ReferralLaunchContext
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */