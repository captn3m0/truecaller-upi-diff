package com.truecaller.referral;

final class RedeemCodeResponse
{
  public String a;
  public String b;
  public int c;
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("RedeemCodeResponse{status='");
    String str = b;
    localStringBuilder.append(str);
    localStringBuilder.append('\'');
    localStringBuilder.append(", daysOfPro=");
    int i = c;
    localStringBuilder.append(i);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.referral.RedeemCodeResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */