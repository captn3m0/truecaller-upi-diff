package com.truecaller.referral;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class BulkSmsView$PromoLayout
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  final int a;
  final int[] b;
  final String[] c;
  final int[] d;
  final int[] e;
  final int[] f;
  
  static
  {
    BulkSmsView.PromoLayout.1 local1 = new com/truecaller/referral/BulkSmsView$PromoLayout$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  BulkSmsView$PromoLayout(int paramInt, int[] paramArrayOfInt1, String[] paramArrayOfString, int[] paramArrayOfInt2, int[] paramArrayOfInt3, int[] paramArrayOfInt4)
  {
    a = paramInt;
    b = paramArrayOfInt1;
    c = paramArrayOfString;
    d = paramArrayOfInt2;
    e = paramArrayOfInt3;
    f = paramArrayOfInt4;
  }
  
  BulkSmsView$PromoLayout(Parcel paramParcel)
  {
    int i = paramParcel.readInt();
    a = i;
    Object localObject = paramParcel.createIntArray();
    b = ((int[])localObject);
    localObject = paramParcel.createStringArray();
    c = ((String[])localObject);
    localObject = paramParcel.createIntArray();
    d = ((int[])localObject);
    localObject = paramParcel.createIntArray();
    e = ((int[])localObject);
    paramParcel = paramParcel.createIntArray();
    f = paramParcel;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramInt = a;
    paramParcel.writeInt(paramInt);
    Object localObject = b;
    paramParcel.writeIntArray((int[])localObject);
    localObject = c;
    paramParcel.writeStringArray((String[])localObject);
    localObject = d;
    paramParcel.writeIntArray((int[])localObject);
    localObject = e;
    paramParcel.writeIntArray((int[])localObject);
    localObject = f;
    paramParcel.writeIntArray((int[])localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.referral.BulkSmsView.PromoLayout
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */