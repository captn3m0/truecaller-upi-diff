package com.truecaller.referral;

public enum RedeemCodeResponse$Status
{
  static
  {
    Object localObject = new com/truecaller/referral/RedeemCodeResponse$Status;
    ((Status)localObject).<init>("ALREADY_REFERRED", 0);
    ALREADY_REFERRED = (Status)localObject;
    localObject = new com/truecaller/referral/RedeemCodeResponse$Status;
    int i = 1;
    ((Status)localObject).<init>("QUOTA_OVER", i);
    QUOTA_OVER = (Status)localObject;
    localObject = new com/truecaller/referral/RedeemCodeResponse$Status;
    int j = 2;
    ((Status)localObject).<init>("SUCCESS", j);
    SUCCESS = (Status)localObject;
    localObject = new com/truecaller/referral/RedeemCodeResponse$Status;
    int k = 3;
    ((Status)localObject).<init>("OLD_PROFILE", k);
    OLD_PROFILE = (Status)localObject;
    localObject = new com/truecaller/referral/RedeemCodeResponse$Status;
    int m = 4;
    ((Status)localObject).<init>("SELF_REFERRAL", m);
    SELF_REFERRAL = (Status)localObject;
    localObject = new com/truecaller/referral/RedeemCodeResponse$Status;
    int n = 5;
    ((Status)localObject).<init>("CANNOT_GRANT_PREMIUM", n);
    CANNOT_GRANT_PREMIUM = (Status)localObject;
    localObject = new com/truecaller/referral/RedeemCodeResponse$Status;
    int i1 = 6;
    ((Status)localObject).<init>("INVALID_CODE", i1);
    INVALID_CODE = (Status)localObject;
    localObject = new com/truecaller/referral/RedeemCodeResponse$Status;
    int i2 = 7;
    ((Status)localObject).<init>("WAS_REFERRER", i2);
    WAS_REFERRER = (Status)localObject;
    localObject = new Status[8];
    Status localStatus = ALREADY_REFERRED;
    localObject[0] = localStatus;
    localStatus = QUOTA_OVER;
    localObject[i] = localStatus;
    localStatus = SUCCESS;
    localObject[j] = localStatus;
    localStatus = OLD_PROFILE;
    localObject[k] = localStatus;
    localStatus = SELF_REFERRAL;
    localObject[m] = localStatus;
    localStatus = CANNOT_GRANT_PREMIUM;
    localObject[n] = localStatus;
    localStatus = INVALID_CODE;
    localObject[i1] = localStatus;
    localStatus = WAS_REFERRER;
    localObject[i2] = localStatus;
    $VALUES = (Status[])localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.referral.RedeemCodeResponse.Status
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */