package com.truecaller.referral;

import c.g.b.k;
import java.util.List;

public final class u
{
  private final List a;
  
  public u(List paramList)
  {
    a = paramList;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof u;
      if (bool1)
      {
        paramObject = (u)paramObject;
        List localList = a;
        paramObject = a;
        boolean bool2 = k.a(localList, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final int hashCode()
  {
    List localList = a;
    if (localList != null) {
      return localList.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("ReferralInviteRequest(phoneNumbers=");
    List localList = a;
    localStringBuilder.append(localList);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.referral.u
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */