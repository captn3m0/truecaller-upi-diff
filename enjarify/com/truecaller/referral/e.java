package com.truecaller.referral;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.common.h.am;
import com.truecaller.data.entity.Contact;
import com.truecaller.log.AssertionUtil;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.ui.details.DetailsFragment;
import com.truecaller.ui.details.DetailsFragment.SourceType;
import com.truecaller.ui.view.ContactPhoto;
import com.truecaller.utils.n;
import com.truecaller.wizard.utils.i;
import dagger.a.g;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class e
  extends android.support.v4.app.e
  implements BulkSmsView
{
  j a;
  private RecyclerView b;
  private c c;
  private Button d;
  private View e;
  private View f;
  private View g;
  private ViewGroup h;
  private LinearLayoutManager i;
  private TextView j;
  private TextView k;
  private RecyclerView.OnScrollListener l;
  
  public static e a(String paramString1, Contact paramContact, BulkSmsView.PromoLayout paramPromoLayout, ReferralManager.ReferralLaunchContext paramReferralLaunchContext, String paramString2, boolean paramBoolean)
  {
    return b(paramString1, paramContact, paramPromoLayout, paramReferralLaunchContext, paramString2, paramBoolean);
  }
  
  public static e a(String paramString1, BulkSmsView.PromoLayout paramPromoLayout, ReferralManager.ReferralLaunchContext paramReferralLaunchContext, String paramString2)
  {
    return b(paramString1, null, paramPromoLayout, paramReferralLaunchContext, paramString2, false);
  }
  
  private static e a(String paramString1, ReferralManager.ReferralLaunchContext paramReferralLaunchContext, String paramString2)
  {
    e locale = new com/truecaller/referral/e;
    locale.<init>();
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putString("SHARE_TEXT", paramString1);
    localBundle.putString("CAMPAIGN_ID", paramString2);
    localBundle.putSerializable("LAUNCH_CONTEXT", paramReferralLaunchContext);
    locale.setArguments(localBundle);
    return locale;
  }
  
  private static e b(String paramString1, Contact paramContact, BulkSmsView.PromoLayout paramPromoLayout, ReferralManager.ReferralLaunchContext paramReferralLaunchContext, String paramString2, boolean paramBoolean)
  {
    paramString1 = a(paramString1, paramReferralLaunchContext, paramString2);
    paramReferralLaunchContext = paramString1.getArguments();
    paramString2 = new String[0];
    AssertionUtil.isNotNull(paramReferralLaunchContext, paramString2);
    if (paramContact != null)
    {
      paramString2 = "CONTACT";
      paramReferralLaunchContext.putParcelable(paramString2, paramContact);
    }
    if (paramPromoLayout != null)
    {
      paramContact = "LAYOUT_RES";
      paramReferralLaunchContext.putParcelable(paramContact, paramPromoLayout);
    }
    paramReferralLaunchContext.putBoolean("CONTACT_HAS_WHATSAPP_PROFILE", paramBoolean);
    return paramString1;
  }
  
  public final List a(Intent paramIntent)
  {
    return com.truecaller.messaging.newconversation.j.a(paramIntent);
  }
  
  public final void a()
  {
    c.notifyDataSetChanged();
  }
  
  public final void a(int paramInt)
  {
    i.a(this, "android.permission.SEND_SMS", paramInt, true);
  }
  
  public final void a(int paramInt, int[] paramArrayOfInt1, String[] paramArrayOfString, int[] paramArrayOfInt2, int[] paramArrayOfInt3, int[] paramArrayOfInt4)
  {
    Object localObject1 = h;
    int m = 0;
    ((ViewGroup)localObject1).setVisibility(0);
    localObject1 = LayoutInflater.from(getContext());
    Object localObject2 = h;
    boolean bool = true;
    Object localObject3 = ((LayoutInflater)localObject1).inflate(paramInt, (ViewGroup)localObject2, bool);
    int n;
    if ((paramArrayOfInt1 != null) && (paramArrayOfString != null))
    {
      n = 0;
      localObject1 = null;
      for (;;)
      {
        int i1 = paramArrayOfInt1.length;
        if (n >= i1) {
          break;
        }
        i1 = paramArrayOfInt1[n];
        localObject2 = (TextView)((View)localObject3).findViewById(i1);
        String str = paramArrayOfString[n];
        ((TextView)localObject2).setText(str);
        n += 1;
      }
    }
    int i2;
    int i3;
    if ((paramArrayOfInt2 != null) && (paramArrayOfInt3 != null))
    {
      i2 = 0;
      paramArrayOfInt1 = null;
      for (;;)
      {
        i3 = paramArrayOfInt2.length;
        if (i2 >= i3) {
          break;
        }
        i3 = paramArrayOfInt2[i2];
        paramArrayOfString = (ImageView)((View)localObject3).findViewById(i3);
        n = paramArrayOfInt3[i2];
        paramArrayOfString.setImageResource(n);
        i2 += 1;
      }
    }
    if (paramArrayOfInt4 != null)
    {
      i2 = paramArrayOfInt4.length;
      while (m < i2)
      {
        i3 = paramArrayOfInt4[m];
        paramArrayOfString = ((View)localObject3).findViewById(i3);
        int i4 = 8;
        paramArrayOfString.setVisibility(i4);
        m += 1;
      }
    }
    localObject3 = (TextView)h.findViewById(2131364884);
    k = ((TextView)localObject3);
  }
  
  public final void a(Uri paramUri, String paramString1, String paramString2)
  {
    View localView = getView();
    if (localView != null)
    {
      localView.findViewById(2131364527).setVisibility(0);
      int m = 2131362554;
      ContactPhoto localContactPhoto = (ContactPhoto)localView.findViewById(m);
      localContactPhoto.a(paramUri, null);
      ((TextView)localView.findViewById(2131363791)).setText(paramString1);
      int n = 2131363828;
      paramUri = (TextView)localView.findViewById(n);
      boolean bool = am.a(paramString1, paramString2);
      if (bool)
      {
        paramUri.setVisibility(8);
        return;
      }
      paramUri.setText(paramString2);
    }
  }
  
  public final void a(Participant paramParticipant, DetailsFragment.SourceType paramSourceType)
  {
    Context localContext = requireContext();
    String str1 = h;
    String str2 = m;
    String str3 = f;
    String str4 = e;
    String str5 = g;
    DetailsFragment.a(localContext, str1, str2, str3, str4, str5, paramSourceType, false, 14);
  }
  
  public final void a(String paramString)
  {
    Toast.makeText(getContext(), paramString, 0).show();
  }
  
  public final void a(String paramString, boolean paramBoolean)
  {
    TextView localTextView = k;
    int m = 8;
    if ((localTextView != null) && (paramBoolean))
    {
      j.setVisibility(m);
      k.setText(paramString);
      return;
    }
    localTextView = j;
    if (paramBoolean) {
      m = 0;
    }
    localTextView.setVisibility(m);
    j.setText(paramString);
  }
  
  public final void a(ArrayList paramArrayList)
  {
    paramArrayList = com.truecaller.messaging.newconversation.j.a(requireActivity(), paramArrayList);
    startActivityForResult(paramArrayList, 101);
  }
  
  public final void a(boolean paramBoolean)
  {
    d.setEnabled(paramBoolean);
  }
  
  public final void a(boolean paramBoolean, int paramInt)
  {
    LinearLayoutManager localLinearLayoutManager = (LinearLayoutManager)b.getLayoutManager();
    int m = 0;
    String[] arrayOfString = new String[0];
    AssertionUtil.isNotNull(localLinearLayoutManager, arrayOfString);
    localLinearLayoutManager.setOrientation(paramInt);
    RecyclerView localRecyclerView = b;
    if (!paramBoolean) {
      m = 8;
    }
    localRecyclerView.setVisibility(m);
  }
  
  public final void b()
  {
    dismissAllowingStateLoss();
  }
  
  public final void b(int paramInt)
  {
    c.notifyItemRemoved(paramInt);
  }
  
  public final void b(String paramString)
  {
    d.setText(paramString);
  }
  
  public final void b(boolean paramBoolean)
  {
    View localView = e;
    if (paramBoolean) {
      paramBoolean = false;
    } else {
      paramBoolean = true;
    }
    localView.setVisibility(paramBoolean);
  }
  
  public final int c()
  {
    LinearLayoutManager localLinearLayoutManager = (LinearLayoutManager)b.getLayoutManager();
    String[] arrayOfString = new String[0];
    AssertionUtil.isNotNull(localLinearLayoutManager, arrayOfString);
    return localLinearLayoutManager.findLastVisibleItemPosition();
  }
  
  public final void c(int paramInt)
  {
    b.smoothScrollToPosition(paramInt);
  }
  
  public final void c(boolean paramBoolean)
  {
    View localView = f;
    if (paramBoolean) {
      paramBoolean = false;
    } else {
      paramBoolean = true;
    }
    localView.setVisibility(paramBoolean);
  }
  
  public final void d()
  {
    d(false);
    RecyclerView localRecyclerView = b;
    RecyclerView.OnScrollListener localOnScrollListener = l;
    localRecyclerView.removeOnScrollListener(localOnScrollListener);
  }
  
  public final void d(boolean paramBoolean)
  {
    View localView = g;
    if (paramBoolean) {
      paramBoolean = false;
    } else {
      paramBoolean = true;
    }
    localView.setVisibility(paramBoolean);
  }
  
  public final int e()
  {
    return i.findFirstCompletelyVisibleItemPosition();
  }
  
  public final int f()
  {
    return i.findLastCompletelyVisibleItemPosition();
  }
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    Object localObject1 = a;
    Object localObject2 = b;
    if (localObject2 != null)
    {
      localObject2 = null;
      int m = -1;
      Object localObject3;
      Object localObject4;
      switch (paramInt1)
      {
      default: 
        break;
      case 103: 
        if (paramInt2 == m)
        {
          ((j)localObject1).e();
          return;
        }
        localObject3 = (BulkSmsView)b;
        localObject4 = c;
        int n = 2131888622;
        localObject1 = new Object[0];
        localObject4 = ((n)localObject4).a(n, (Object[])localObject1);
        ((BulkSmsView)localObject3).a((String)localObject4);
        break;
      case 102: 
        if (paramInt2 == m)
        {
          ((j)localObject1).b();
          return;
        }
        localObject3 = (BulkSmsView)b;
        localObject4 = c;
        localObject1 = new Object[0];
        localObject4 = ((n)localObject4).a(2131888621, (Object[])localObject1);
        ((BulkSmsView)localObject3).a((String)localObject4);
        return;
      case 101: 
        if (paramInt2 == m)
        {
          localObject3 = ((BulkSmsView)b).a(paramIntent);
          ((j)localObject1).a((List)localObject3);
          return;
        }
        break;
      }
    }
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setStyle(1, 0);
    Object localObject1 = getArguments().getString("SHARE_TEXT");
    Object localObject2 = (Contact)getArguments().getParcelable("CONTACT");
    Object localObject3 = new String[0];
    AssertionUtil.isNotNull(localObject1, (String[])localObject3);
    localObject3 = new com/truecaller/referral/l$a;
    ((l.a)localObject3).<init>((byte)0);
    Object localObject4 = (bp)g.a(((bk)requireContext().getApplicationContext()).a());
    c = ((bp)localObject4);
    localObject4 = new com/truecaller/referral/f;
    ((f)localObject4).<init>((String)localObject1, (Contact)localObject2);
    localObject1 = (f)g.a(localObject4);
    a = ((f)localObject1);
    localObject1 = new com/truecaller/referral/y;
    ((y)localObject1).<init>();
    localObject1 = (y)g.a(localObject1);
    b = ((y)localObject1);
    g.a(a, f.class);
    g.a(b, y.class);
    g.a(c, bp.class);
    localObject1 = new com/truecaller/referral/l;
    localObject2 = a;
    localObject4 = b;
    localObject3 = c;
    ((l)localObject1).<init>((f)localObject2, (y)localObject4, (bp)localObject3, (byte)0);
    ((b)localObject1).a(this);
    paramBundle = new com/truecaller/referral/c;
    localObject1 = a;
    paramBundle.<init>((j)localObject1);
    c = paramBundle;
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    return paramLayoutInflater.inflate(2131559084, paramViewGroup, false);
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    a.y_();
  }
  
  public void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    i.a(paramArrayOfString, paramArrayOfInt);
    j localj = a;
    int m = 102;
    int n;
    if (paramInt != m)
    {
      n = 103;
      if (paramInt != n) {}
    }
    else
    {
      n = 0;
      int i1 = 0;
      for (;;)
      {
        int i2 = paramArrayOfString.length;
        if (i1 >= i2) {
          break;
        }
        String str1 = paramArrayOfString[i1];
        String str2 = "android.permission.SEND_SMS";
        boolean bool = str1.equals(str2);
        if (bool)
        {
          int i3 = paramArrayOfInt[i1];
          if (i3 == 0)
          {
            if (paramInt == m)
            {
              localj.a(false);
              return;
            }
            localj.e();
            return;
          }
        }
        i1 += 1;
      }
    }
  }
  
  public void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    Object localObject1 = a;
    Object localObject2 = a;
    paramBundle.putParcelableArrayList("contacts", (ArrayList)localObject2);
    boolean bool = g;
    paramBundle.putBoolean("CONTACT_HAS_WHATSAPP_PROFILE", bool);
    localObject2 = e;
    paramBundle.putSerializable("LAUNCH_CONTEXT", (Serializable)localObject2);
    Object localObject3 = f;
    if (localObject3 != null)
    {
      localObject3 = "CAMPAIGN_ID";
      localObject2 = f;
      paramBundle.putString((String)localObject3, (String)localObject2);
    }
    localObject3 = d;
    if (localObject3 != null)
    {
      localObject3 = "LAYOUT_RES";
      localObject1 = d;
      paramBundle.putParcelable((String)localObject3, (Parcelable)localObject1);
    }
  }
  
  public void onStart()
  {
    super.onStart();
    Window localWindow = getDialog().getWindow();
    if (localWindow != null)
    {
      int m = -1;
      int n = -2;
      localWindow.setLayout(m, n);
    }
  }
  
  public void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    Object localObject1 = (RecyclerView)paramView.findViewById(2131363214);
    b = ((RecyclerView)localObject1);
    localObject1 = (Button)paramView.findViewById(2131363490);
    d = ((Button)localObject1);
    localObject1 = paramView.findViewById(2131362867);
    f = ((View)localObject1);
    localObject1 = paramView.findViewById(2131363682);
    e = ((View)localObject1);
    localObject1 = paramView.findViewById(2131364169);
    g = ((View)localObject1);
    localObject1 = (ViewGroup)paramView.findViewById(2131364034);
    h = ((ViewGroup)localObject1);
    localObject1 = (LinearLayoutManager)b.getLayoutManager();
    i = ((LinearLayoutManager)localObject1);
    int m = 2131361824;
    localObject1 = paramView.findViewById(m);
    int n = 2131363493;
    paramView = (TextView)paramView.findViewById(n);
    j = paramView;
    paramView = b;
    Object localObject2 = c;
    paramView.setAdapter((RecyclerView.Adapter)localObject2);
    paramView = new com/truecaller/referral/e$1;
    paramView.<init>(this);
    l = paramView;
    paramView = b;
    localObject2 = l;
    paramView.addOnScrollListener((RecyclerView.OnScrollListener)localObject2);
    paramView = d;
    localObject2 = new com/truecaller/referral/-$$Lambda$e$wGvvMC1K55dSDbHI48hg36OjOhI;
    ((-..Lambda.e.wGvvMC1K55dSDbHI48hg36OjOhI)localObject2).<init>(this);
    paramView.setOnClickListener((View.OnClickListener)localObject2);
    paramView = g;
    localObject2 = new com/truecaller/referral/-$$Lambda$e$4rA9OkMmA1eusdAn5pueNBxj9X4;
    ((-..Lambda.e.4rA9OkMmA1eusdAn5pueNBxj9X4)localObject2).<init>(this);
    paramView.setOnClickListener((View.OnClickListener)localObject2);
    paramView = new com/truecaller/referral/-$$Lambda$e$cYE2NSsY1U0J62wxlJtUNtF-f1s;
    paramView.<init>(this);
    ((View)localObject1).setOnClickListener(paramView);
    paramView = a;
    if (paramBundle == null) {
      paramBundle = getArguments();
    }
    if (paramBundle != null)
    {
      localObject1 = paramBundle.getSerializable("LAUNCH_CONTEXT");
      Object localObject3 = localObject1;
      localObject3 = (ReferralManager.ReferralLaunchContext)localObject1;
      m = 0;
      localObject1 = new String[0];
      AssertionUtil.isNotNull(localObject3, (String[])localObject1);
      localObject1 = new com/truecaller/referral/BulkSmsView$a;
      ArrayList localArrayList = paramBundle.getParcelableArrayList("contacts");
      localObject2 = paramBundle.getParcelable("LAYOUT_RES");
      Object localObject4 = localObject2;
      localObject4 = (BulkSmsView.PromoLayout)localObject2;
      String str = paramBundle.getString("CAMPAIGN_ID");
      boolean bool2 = paramBundle.getBoolean("CONTACT_HAS_WHATSAPP_PROFILE");
      localObject2 = localObject1;
      ((BulkSmsView.a)localObject1).<init>(localArrayList, (BulkSmsView.PromoLayout)localObject4, (ReferralManager.ReferralLaunchContext)localObject3, str, bool2);
    }
    else
    {
      m = 0;
      localObject1 = null;
    }
    if (localObject1 != null)
    {
      paramBundle = (ArrayList)a;
      if (paramBundle != null)
      {
        boolean bool1 = paramBundle.isEmpty();
        if (!bool1) {
          paramView.a(paramBundle);
        }
      }
      paramBundle = b;
      d = paramBundle;
      paramBundle = c;
      e = paramBundle;
      paramBundle = d;
      f = paramBundle;
      boolean bool3 = e;
      g = bool3;
    }
    a.a(this);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.referral.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */