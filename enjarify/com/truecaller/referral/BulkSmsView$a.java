package com.truecaller.referral;

import java.util.List;

public final class BulkSmsView$a
{
  final List a;
  final BulkSmsView.PromoLayout b;
  final ReferralManager.ReferralLaunchContext c;
  final String d;
  final boolean e;
  
  BulkSmsView$a(List paramList, BulkSmsView.PromoLayout paramPromoLayout, ReferralManager.ReferralLaunchContext paramReferralLaunchContext, String paramString, boolean paramBoolean)
  {
    a = paramList;
    b = paramPromoLayout;
    c = paramReferralLaunchContext;
    d = paramString;
    e = paramBoolean;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.referral.BulkSmsView.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */