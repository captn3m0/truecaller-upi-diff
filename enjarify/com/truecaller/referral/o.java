package com.truecaller.referral;

import android.os.AsyncTask;
import com.truecaller.log.AssertionUtil;
import e.b;
import e.r;
import java.io.IOException;
import java.lang.ref.WeakReference;

final class o
  extends AsyncTask
{
  private final am a;
  private final al b;
  private final WeakReference c;
  
  o(am paramam, al paramal, o.a parama)
  {
    a = paramam;
    b = paramal;
    paramam = new java/lang/ref/WeakReference;
    paramam.<init>(parama);
    c = paramam;
  }
  
  private static n a()
  {
    try
    {
      Object localObject = al.b();
      localObject = ((b)localObject).c();
      localObject = b;
      return (n)localObject;
    }
    catch (IOException localIOException)
    {
      AssertionUtil.reportThrowableButNeverCrash(localIOException;
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.referral.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */