package com.truecaller.referral;

import android.net.Uri;
import com.truecaller.abtest.c;
import com.truecaller.analytics.e.a;
import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.i;
import com.truecaller.androidactors.w;
import com.truecaller.bb;
import com.truecaller.data.entity.Contact;
import com.truecaller.log.AssertionUtil;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.util.al;
import com.truecaller.utils.l;
import com.truecaller.utils.n;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

final class j
  extends bb
  implements com.truecaller.b
{
  final ArrayList a;
  final n c;
  BulkSmsView.PromoLayout d;
  ReferralManager.ReferralLaunchContext e;
  String f;
  boolean g;
  private final String h;
  private final k i;
  private final am j;
  private final al k;
  private final Participant l;
  private final com.truecaller.analytics.b m;
  private final l n;
  private final c o;
  private final com.truecaller.analytics.a.f p;
  private final com.truecaller.androidactors.f q;
  private i r;
  private com.truecaller.androidactors.a s;
  
  j(String paramString, k paramk, am paramam, al paramal, Contact paramContact, n paramn, com.truecaller.analytics.b paramb, com.truecaller.androidactors.f paramf, i parami, l paraml, c paramc, com.truecaller.analytics.a.f paramf1)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    a = localArrayList;
    h = paramString;
    i = paramk;
    j = paramam;
    k = paramal;
    paramString = null;
    if (paramContact != null) {
      paramString = Participant.a(paramContact, null, null);
    }
    l = paramString;
    c = paramn;
    m = paramb;
    q = paramf;
    r = parami;
    n = paraml;
    o = paramc;
    p = paramf1;
  }
  
  private String a(String paramString)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    boolean bool = com.truecaller.common.h.am.b(paramString);
    if (!bool)
    {
      localStringBuilder.append(paramString);
      paramString = ",";
      localStringBuilder.append(paramString);
    }
    paramString = a.iterator();
    for (;;)
    {
      bool = paramString.hasNext();
      if (!bool) {
        break;
      }
      String str = nextf;
      localStringBuilder.append(str);
      str = ",";
      localStringBuilder.append(str);
    }
    int i1 = localStringBuilder.length();
    if (i1 > 0)
    {
      i1 = localStringBuilder.length() + -1;
      localStringBuilder.setLength(i1);
    }
    return localStringBuilder.toString();
  }
  
  private void b(List paramList)
  {
    a(paramList);
  }
  
  private void b(boolean paramBoolean)
  {
    Object localObject = b;
    if (localObject != null)
    {
      int i1 = g();
      BulkSmsView localBulkSmsView1 = (BulkSmsView)b;
      localBulkSmsView1.a(paramBoolean, i1);
      int i2 = 1;
      if ((i1 == i2) && (paramBoolean))
      {
        BulkSmsView localBulkSmsView2 = (BulkSmsView)b;
        localBulkSmsView2.d();
      }
    }
  }
  
  private boolean c(int paramInt)
  {
    ArrayList localArrayList = a;
    int i1 = localArrayList.size();
    return i1 == paramInt;
  }
  
  private int f()
  {
    return a.size() * 7;
  }
  
  private int g()
  {
    boolean bool = i();
    if (bool) {
      return 1;
    }
    return 0;
  }
  
  private boolean h()
  {
    HashSet localHashSet = new java/util/HashSet;
    localHashSet.<init>();
    ReferralManager.ReferralLaunchContext localReferralLaunchContext = ReferralManager.ReferralLaunchContext.AFTER_CALL;
    localHashSet.add(localReferralLaunchContext);
    localReferralLaunchContext = ReferralManager.ReferralLaunchContext.AFTER_CALL_SAVE_CONTACT;
    localHashSet.add(localReferralLaunchContext);
    localReferralLaunchContext = ReferralManager.ReferralLaunchContext.CONTACT_DETAILS;
    localHashSet.add(localReferralLaunchContext);
    boolean bool = j();
    if (bool)
    {
      localReferralLaunchContext = e;
      localHashSet.contains(localReferralLaunchContext);
    }
    return false;
  }
  
  private boolean i()
  {
    Participant localParticipant = l;
    return localParticipant != null;
  }
  
  private boolean j()
  {
    Object localObject = l;
    if (localObject != null)
    {
      localObject = o;
      boolean bool = ((c)localObject).b();
      if (!bool) {
        return true;
      }
    }
    return false;
  }
  
  private int k()
  {
    boolean bool = i();
    if (bool) {
      return 4;
    }
    return 3;
  }
  
  private int l()
  {
    boolean bool = i();
    if (bool) {
      return 2;
    }
    return 1;
  }
  
  public final int a()
  {
    boolean bool = j();
    if (bool) {
      return 0;
    }
    return a.size() + 1;
  }
  
  public final int a(int paramInt)
  {
    paramInt = c(paramInt);
    if (paramInt != 0) {
      return k();
    }
    return l();
  }
  
  final void a(e.a parama)
  {
    Object localObject1 = e.name();
    parama.a("source", (String)localObject1);
    Object localObject2 = e;
    localObject1 = ReferralManager.ReferralLaunchContext.DEEP_LINK;
    if (localObject2 == localObject1)
    {
      localObject2 = "Campaign";
      localObject1 = com.truecaller.common.h.am.a(f);
      parama.a((String)localObject2, (String)localObject1);
    }
    localObject2 = m;
    parama = parama.a();
    ((com.truecaller.analytics.b)localObject2).b(parama);
  }
  
  public final void a(BulkSmsView paramBulkSmsView)
  {
    super.a(paramBulkSmsView);
    Object localObject1 = d;
    Object localObject2;
    Object localObject3;
    Object localObject4;
    if (localObject1 != null)
    {
      i1 = a;
      localObject2 = d.b;
      localObject3 = d.c;
      int[] arrayOfInt1 = d.d;
      int[] arrayOfInt2 = d.e;
      localObject1 = d;
      int[] arrayOfInt3 = f;
      localObject4 = paramBulkSmsView;
      paramBulkSmsView.a(i1, (int[])localObject2, (String[])localObject3, arrayOfInt1, arrayOfInt2, arrayOfInt3);
    }
    localObject1 = l;
    boolean bool1 = true;
    int i1 = 0;
    if (localObject1 != null)
    {
      localObject2 = k;
      long l1 = p;
      localObject1 = l.n;
      localObject1 = ((al)localObject2).a(l1, (String)localObject1, bool1);
      localObject2 = l.a();
      localObject3 = l.b();
      paramBulkSmsView.a((Uri)localObject1, (String)localObject2, (String)localObject3);
      h();
      localObject1 = o.a("referralByWhatsApp_16722");
      localObject2 = "sms";
      bool2 = com.truecaller.common.h.am.b((CharSequence)localObject1, (CharSequence)localObject2);
      if (bool2)
      {
        localObject1 = p;
        localObject2 = "ab_test_referral_16722_seen";
        ((com.truecaller.analytics.a.f)localObject1).a((String)localObject2);
      }
      localObject1 = c;
      int i2 = 2131888640;
      localObject3 = new Object[0];
      localObject1 = ((n)localObject1).a(i2, (Object[])localObject3);
      paramBulkSmsView.b((String)localObject1);
    }
    boolean bool2 = j();
    if (bool2)
    {
      b(false);
      paramBulkSmsView.b(false);
      paramBulkSmsView.d(false);
      paramBulkSmsView.a(bool1);
      paramBulkSmsView.c(false);
      return;
    }
    localObject1 = l;
    if (localObject1 == null)
    {
      localObject1 = d;
      if (localObject1 == null)
      {
        bool2 = false;
        localObject1 = null;
        break label328;
      }
    }
    bool2 = true;
    label328:
    paramBulkSmsView.c(bool2);
    localObject1 = a;
    bool2 = ((ArrayList)localObject1).isEmpty();
    if (bool2)
    {
      b(false);
      paramBulkSmsView.b(bool1);
      paramBulkSmsView.d(false);
      paramBulkSmsView = ((q)q.a()).a();
      localObject1 = r;
      localObject4 = new com/truecaller/referral/-$$Lambda$j$TKs84Lzq6V2qheHFm3VobZsDAFI;
      ((-..Lambda.j.TKs84Lzq6V2qheHFm3VobZsDAFI)localObject4).<init>(this);
      paramBulkSmsView = paramBulkSmsView.a((i)localObject1, (ac)localObject4);
      s = paramBulkSmsView;
      return;
    }
    paramBulkSmsView.a();
    b(paramBulkSmsView);
  }
  
  public final void a(a parama, int paramInt)
  {
    int i1 = a(paramInt);
    switch (i1)
    {
    default: 
      break;
    case 1: 
    case 2: 
      Object localObject = (Participant)a.get(paramInt);
      String str1 = ((Participant)localObject).a();
      String str2 = ((Participant)localObject).b();
      al localal = k;
      long l1 = p;
      localObject = n;
      boolean bool = true;
      localObject = localal.a(l1, (String)localObject, bool);
      parama.a((Uri)localObject);
      parama.a(str1);
      parama.b(str2);
      paramInt = com.truecaller.common.h.am.a(str1, str2) ^ bool;
      parama.a(paramInt);
    }
  }
  
  final void a(List paramList)
  {
    a.clear();
    ArrayList localArrayList = a;
    HashSet localHashSet = new java/util/HashSet;
    localHashSet.<init>(paramList);
    localArrayList.addAll(localHashSet);
    paramList = l;
    if (paramList != null)
    {
      localArrayList = a;
      localArrayList.remove(paramList);
    }
    paramList = b;
    if (paramList != null)
    {
      ((BulkSmsView)b).a();
      paramList = (BulkSmsView)b;
      b(paramList);
    }
  }
  
  final void a(boolean paramBoolean)
  {
    Object localObject1 = b;
    int i1 = 0;
    String str = null;
    Object localObject2 = new String[0];
    AssertionUtil.isNotNull(localObject1, (String[])localObject2);
    if (paramBoolean)
    {
      localObject3 = new com/truecaller/analytics/e$a;
      ((e.a)localObject3).<init>("ANDROID_Ref_IntentToRefer");
      localObject1 = "UiType";
      boolean bool1 = j();
      if (bool1)
      {
        localObject2 = "SingleSMS";
      }
      else
      {
        localObject2 = j;
        localObject4 = "featureReferralShareApps";
        localObject2 = ((am)localObject2).a((String)localObject4);
      }
      localObject3 = ((e.a)localObject3).a((String)localObject1, (String)localObject2);
      a((e.a)localObject3);
    }
    Object localObject3 = n;
    localObject1 = new String[] { "android.permission.SEND_SMS" };
    paramBoolean = ((l)localObject3).a((String[])localObject1);
    if (!paramBoolean)
    {
      ((BulkSmsView)b).a(102);
      return;
    }
    localObject3 = new java/util/ArrayList;
    localObject1 = a;
    ((ArrayList)localObject3).<init>((Collection)localObject1);
    localObject1 = l;
    if (localObject1 != null) {
      ((ArrayList)localObject3).add(localObject1);
    }
    localObject1 = i;
    localObject2 = h;
    ((k)localObject1).a((String)localObject2, (List)localObject3);
    paramBoolean = ((ArrayList)localObject3).size();
    localObject1 = c;
    int i2 = 2131755042;
    Object localObject4 = new Object[0];
    localObject1 = ((n)localObject1).a(i2, paramBoolean, (Object[])localObject4);
    localObject2 = (BulkSmsView)b;
    localObject4 = c;
    int i3 = 2131888634;
    int i4 = 2;
    Object[] arrayOfObject = new Object[i4];
    Integer localInteger = Integer.valueOf(paramBoolean);
    arrayOfObject[0] = localInteger;
    i1 = 1;
    arrayOfObject[i1] = localObject1;
    localObject1 = ((n)localObject4).a(i3, arrayOfObject);
    ((BulkSmsView)localObject2).a((String)localObject1);
    boolean bool2 = j();
    if (!bool2)
    {
      localObject1 = j;
      str = "smsReferralPrefetchBatch";
      ((am)localObject1).f(str);
    }
    localObject1 = j;
    localObject2 = ((am)localObject1).a("smsReferralSentTo");
    localObject2 = a((String)localObject2);
    ((am)localObject1).a("smsReferralSentTo", (String)localObject2);
    localObject1 = new com/truecaller/analytics/e$a;
    ((e.a)localObject1).<init>("ANDROID_Ref_SmsSent");
    localObject3 = ((e.a)localObject1).a("count", paramBoolean);
    a((e.a)localObject3);
    ((BulkSmsView)b).b();
  }
  
  public final long b(int paramInt)
  {
    return 0L;
  }
  
  final void b()
  {
    h();
    a(true);
    Object localObject = o.a("referralByWhatsApp_16722");
    String str = "sms";
    boolean bool = com.truecaller.common.h.am.b((CharSequence)localObject, str);
    if (bool)
    {
      localObject = p;
      str = "ab_test_referral_16722_converted";
      ((com.truecaller.analytics.a.f)localObject).a(str);
    }
  }
  
  final void b(BulkSmsView paramBulkSmsView)
  {
    Object localObject1 = a;
    boolean bool1 = ((ArrayList)localObject1).isEmpty();
    boolean bool2 = true;
    if (bool1)
    {
      localObject1 = l;
      if (localObject1 == null)
      {
        bool1 = false;
        localObject1 = null;
        break label38;
      }
    }
    bool1 = true;
    label38:
    paramBulkSmsView.a(bool1);
    b(bool2);
    c();
    localObject1 = a;
    bool1 = ((ArrayList)localObject1).isEmpty();
    Object localObject2;
    if (bool1)
    {
      localObject1 = l;
      if (localObject1 != null)
      {
        localObject1 = o;
        bool1 = ((c)localObject1).b();
        if (bool1)
        {
          localObject1 = c;
          int i2 = 2131888639;
          localObject2 = new Object[0];
          localObject1 = ((n)localObject1).a(i2, (Object[])localObject2);
          break label326;
        }
      }
      bool1 = false;
      localObject1 = null;
      paramBulkSmsView.a(null, false);
      break label335;
    }
    else
    {
      localObject1 = a;
      int i1 = ((ArrayList)localObject1).size();
      Object localObject3 = c;
      int i3 = 2131755062;
      Object[] arrayOfObject1 = new Object[0];
      localObject3 = ((n)localObject3).a(i3, i1, arrayOfObject1);
      localObject2 = l;
      int i4 = 2;
      int i5 = 3;
      int i6;
      Object[] arrayOfObject2;
      if (localObject2 != null)
      {
        localObject2 = c;
        i6 = 2131888638;
        arrayOfObject2 = new Object[i5];
        localObject1 = Integer.valueOf(i1);
        arrayOfObject2[0] = localObject1;
        arrayOfObject2[bool2] = localObject3;
        i1 = f();
        localObject1 = Integer.valueOf(i1);
        arrayOfObject2[i4] = localObject1;
        localObject1 = ((n)localObject2).a(i6, arrayOfObject2);
      }
      else
      {
        localObject2 = c;
        i6 = 2131888637;
        arrayOfObject2 = new Object[i5];
        localObject1 = Integer.valueOf(i1);
        arrayOfObject2[0] = localObject1;
        arrayOfObject2[bool2] = localObject3;
        i1 = f();
        localObject1 = Integer.valueOf(i1);
        arrayOfObject2[i4] = localObject1;
        localObject1 = ((n)localObject2).a(i6, arrayOfObject2);
      }
    }
    label326:
    paramBulkSmsView.a((String)localObject1, bool2);
    label335:
    paramBulkSmsView.b(false);
  }
  
  final void c()
  {
    Object localObject = b;
    if (localObject != null)
    {
      boolean bool = i();
      if (!bool)
      {
        localObject = (BulkSmsView)b;
        int i1 = ((BulkSmsView)localObject).c();
        BulkSmsView localBulkSmsView = (BulkSmsView)b;
        int i2 = 1;
        i1 += i2;
        ArrayList localArrayList = a;
        int i3 = localArrayList.size();
        if (i1 >= i3) {
          i2 = 0;
        }
        localBulkSmsView.d(i2);
      }
    }
  }
  
  final void e()
  {
    Object localObject1 = b;
    Object localObject2 = new String[0];
    AssertionUtil.isNotNull(localObject1, (String[])localObject2);
    localObject1 = n;
    localObject2 = new String[] { "android.permission.SEND_SMS" };
    boolean bool = ((l)localObject1).a((String[])localObject2);
    if (!bool)
    {
      ((BulkSmsView)b).a(103);
      return;
    }
    localObject1 = (BulkSmsView)b;
    localObject2 = a;
    ((BulkSmsView)localObject1).a((ArrayList)localObject2);
    localObject1 = new com/truecaller/analytics/e$a;
    ((e.a)localObject1).<init>("ANDROID_Ref_BulkSmsAddMoreContactsClk");
    a((e.a)localObject1);
  }
  
  public final void y_()
  {
    super.y_();
    com.truecaller.androidactors.a locala = s;
    if (locala != null) {
      locala.a();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.referral.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */