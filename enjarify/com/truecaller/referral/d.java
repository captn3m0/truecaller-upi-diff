package com.truecaller.referral;

import android.util.TimingLogger;
import com.truecaller.analytics.e.a;
import com.truecaller.calling.dialer.suggested_contacts.f;
import com.truecaller.data.access.c;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.filters.FilterManager;
import com.truecaller.filters.FilterManager.FilterAction;
import com.truecaller.filters.g;
import com.truecaller.filters.p;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.network.search.d.b;
import e.r;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import okhttp3.ad;

final class d
{
  private final f a;
  private final int b;
  private final int c;
  private final ap d;
  private final List e;
  private final List f;
  private final c g;
  private final am h;
  private final com.truecaller.common.h.u i;
  private final com.truecaller.analytics.b j;
  private final StringBuilder k;
  private final FilterManager l;
  private final p m;
  private final al n;
  private int o;
  private TimingLogger p;
  
  d(f paramf, int paramInt, ap paramap, c paramc, am paramam, com.truecaller.common.h.u paramu, com.truecaller.analytics.b paramb, FilterManager paramFilterManager, p paramp, al paramal)
  {
    a = paramf;
    b = paramInt;
    c = 500;
    d = paramap;
    g = paramc;
    h = paramam;
    i = paramu;
    j = paramb;
    l = paramFilterManager;
    m = paramp;
    paramf = new java/util/ArrayList;
    paramf.<init>();
    e = paramf;
    paramf = new java/util/ArrayList;
    paramf.<init>();
    f = paramf;
    paramf = new java/lang/StringBuilder;
    paramf.<init>();
    k = paramf;
    n = paramal;
  }
  
  private void a(List paramList)
  {
    paramList = paramList.iterator();
    boolean bool1;
    do
    {
      Object localObject1;
      Object localObject2;
      for (;;)
      {
        bool1 = paramList.hasNext();
        if (!bool1) {
          break label279;
        }
        localObject1 = (v)paramList.next();
        localObject2 = i;
        localObject1 = a;
        localObject1 = ((com.truecaller.common.h.u)localObject2).b((String)localObject1);
        if (localObject1 != null)
        {
          localObject2 = g.b((String)localObject1);
          if (localObject2 != null) {
            break;
          }
          localObject2 = "Process invite result. Aggregate contact is null for number: ";
          localObject1 = String.valueOf(localObject1);
          localObject1 = ((String)localObject2).concat((String)localObject1);
          b((String)localObject1);
        }
      }
      Object localObject3 = i;
      int i1 = ((com.truecaller.common.h.u)localObject3).d((String)localObject1);
      int i2 = 2;
      if (i1 == i2)
      {
        localObject3 = i;
        localObject1 = Participant.a((Contact)localObject2, (String)localObject1, (com.truecaller.common.h.u)localObject3);
        localObject3 = e;
        boolean bool2 = ((List)localObject3).contains(localObject1);
        if (!bool2)
        {
          e.add(localObject1);
          localObject1 = new java/lang/StringBuilder;
          localObject3 = "Invite Result contact: ";
          ((StringBuilder)localObject1).<init>((String)localObject3);
          localObject2 = ((Contact)localObject2).t();
          ((StringBuilder)localObject1).append((String)localObject2);
          localObject2 = " Added";
          ((StringBuilder)localObject1).append((String)localObject2);
          localObject1 = ((StringBuilder)localObject1).toString();
          b((String)localObject1);
        }
        else
        {
          localObject1 = new java/lang/StringBuilder;
          localObject3 = "Invite Result contact: ";
          ((StringBuilder)localObject1).<init>((String)localObject3);
          localObject2 = ((Contact)localObject2).t();
          ((StringBuilder)localObject1).append((String)localObject2);
          localObject2 = " Not Added";
          ((StringBuilder)localObject1).append((String)localObject2);
          localObject1 = ((StringBuilder)localObject1).toString();
          b((String)localObject1);
        }
      }
      bool1 = f();
    } while (!bool1);
    e();
    return;
    label279:
    c();
  }
  
  private boolean a(String paramString)
  {
    Iterator localIterator = e.iterator();
    boolean bool;
    do
    {
      bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      String str = nextf;
      bool = com.truecaller.common.h.am.a(str, paramString);
    } while (!bool);
    return true;
    return false;
  }
  
  private boolean a(String paramString, boolean paramBoolean)
  {
    paramString = l.a(paramString).h;
    p localp = m;
    boolean bool = localp.g();
    FilterManager.FilterAction localFilterAction1 = FilterManager.FilterAction.ALLOW_WHITELISTED;
    if (paramString != localFilterAction1) {
      if ((!paramBoolean) || (!bool))
      {
        FilterManager.FilterAction localFilterAction2 = FilterManager.FilterAction.FILTER_BLACKLISTED;
        if (paramString != localFilterAction2) {}
      }
      else
      {
        return true;
      }
    }
    return false;
  }
  
  private Collection b()
  {
    Object localObject = a.a(0);
    ArrayList localArrayList = new java/util/ArrayList;
    int i1 = ((List)localObject).size();
    localArrayList.<init>(i1);
    localObject = ((List)localObject).iterator();
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject).hasNext();
      if (!bool1) {
        break;
      }
      Contact localContact = nextb;
      if (localContact != null)
      {
        boolean bool2 = localContact.Z();
        if (bool2) {
          localArrayList.add(localContact);
        }
      }
    }
    return localArrayList;
  }
  
  private void b(String paramString)
  {
    TimingLogger localTimingLogger = p;
    if (localTimingLogger == null) {
      return;
    }
    localTimingLogger.addSplit(paramString);
  }
  
  private void c()
  {
    Object localObject1 = f;
    int i1 = ((List)localObject1).size();
    int i2 = o;
    if (i1 <= i2)
    {
      e();
      return;
    }
    localObject1 = new java/lang/StringBuilder;
    Object localObject2 = "Getting next batch. Start Index: ";
    ((StringBuilder)localObject1).<init>((String)localObject2);
    i2 = o;
    ((StringBuilder)localObject1).append(i2);
    localObject1 = ((StringBuilder)localObject1).toString();
    b((String)localObject1);
    localObject1 = d();
    boolean bool2 = f();
    if (bool2)
    {
      b("Found all results locally, not triggering invite.");
      e();
      return;
    }
    localObject2 = new java/util/ArrayList;
    int i4 = ((List)localObject1).size();
    ((ArrayList)localObject2).<init>(i4);
    ArrayList localArrayList = new java/util/ArrayList;
    int i5 = ((List)localObject1).size();
    localArrayList.<init>(i5);
    localObject1 = ((List)localObject1).iterator();
    for (;;)
    {
      boolean bool4 = ((Iterator)localObject1).hasNext();
      if (!bool4) {
        break;
      }
      str1 = ((Number)((Iterator)localObject1).next()).a();
      String str2 = String.valueOf(str1);
      Object localObject3 = "Number added: number: ".concat(str2);
      b((String)localObject3);
      localObject3 = new com/truecaller/network/search/d$b;
      str2 = null;
      ((d.b)localObject3).<init>(str1, null, null);
      ((List)localObject2).add(localObject3);
      localArrayList.add(str1);
    }
    boolean bool1 = ((List)localObject2).isEmpty();
    if (bool1)
    {
      e();
      return;
    }
    localObject1 = new java/lang/StringBuilder;
    String str1 = "Triggering referral/invite for ";
    ((StringBuilder)localObject1).<init>(str1);
    int i3 = ((List)localObject2).size();
    ((StringBuilder)localObject1).append(i3);
    localObject2 = " numbers";
    ((StringBuilder)localObject1).append((String)localObject2);
    localObject1 = ((StringBuilder)localObject1).toString();
    b((String)localObject1);
    try
    {
      localObject1 = new com/truecaller/referral/u;
      ((u)localObject1).<init>(localArrayList);
      localObject1 = al.a((u)localObject1);
      localObject1 = ((e.b)localObject1).c();
      localObject2 = a;
      boolean bool3 = ((ad)localObject2).c();
      if (bool3)
      {
        localObject2 = b;
        if (localObject2 != null)
        {
          localObject1 = b;
          localObject1 = (List)localObject1;
          localObject2 = localObject1.toString();
          b((String)localObject2);
          a((List)localObject1);
        }
      }
      return;
    }
    catch (IOException localIOException)
    {
      e();
    }
  }
  
  private List d()
  {
    Object localObject1 = new java/lang/StringBuilder;
    Object localObject2 = "<- Filtering from ";
    ((StringBuilder)localObject1).<init>((String)localObject2);
    int i1 = o;
    ((StringBuilder)localObject1).append(i1);
    localObject1 = ((StringBuilder)localObject1).toString();
    b((String)localObject1);
    localObject1 = new java/util/ArrayList;
    i1 = c;
    ((ArrayList)localObject1).<init>(i1);
    for (i1 = o;; i1 = i2)
    {
      List localList = f;
      i2 = localList.size();
      if (i1 >= i2) {
        break;
      }
      i2 = i1 + 1;
      o = i2;
      Object localObject3 = f;
      localObject2 = (Contact)((List)localObject3).get(i1);
      if (localObject2 != null)
      {
        localObject3 = ((Contact)localObject2).A().iterator();
        for (;;)
        {
          boolean bool1 = ((Iterator)localObject3).hasNext();
          if (!bool1) {
            break;
          }
          Object localObject4 = (Number)((Iterator)localObject3).next();
          if (localObject4 != null)
          {
            boolean bool3 = ((List)localObject1).contains(localObject4);
            if (!bool3)
            {
              String str1 = ((Number)localObject4).a();
              Object localObject5 = new java/lang/StringBuilder;
              ((StringBuilder)localObject5).<init>("contact: ");
              String str2 = ((Contact)localObject2).t();
              ((StringBuilder)localObject5).append(str2);
              str2 = " Number: ";
              ((StringBuilder)localObject5).append(str2);
              ((StringBuilder)localObject5).append(str1);
              localObject5 = ((StringBuilder)localObject5).toString();
              b((String)localObject5);
              boolean bool4 = ((Contact)localObject2).U();
              bool4 = a(str1, bool4);
              if (bool4)
              {
                localObject4 = new java/lang/StringBuilder;
                localObject5 = "Number: ";
                ((StringBuilder)localObject4).<init>((String)localObject5);
                ((StringBuilder)localObject4).append(str1);
                str1 = " is blocked not adding.";
                ((StringBuilder)localObject4).append(str1);
                localObject4 = ((StringBuilder)localObject4).toString();
                b((String)localObject4);
              }
              else
              {
                bool4 = a(str1);
                if (!bool4)
                {
                  localObject5 = d;
                  str2 = ((ap)localObject5).a((Number)localObject4);
                  String str3 = ((Number)localObject4).a();
                  boolean bool5 = ((ap)localObject5).a(str3, str2);
                  localObject5 = a;
                  bool4 = com.truecaller.common.h.am.a(str2, (CharSequence)localObject5);
                  if ((bool5) && (bool4))
                  {
                    bool4 = true;
                  }
                  else
                  {
                    bool4 = false;
                    localObject5 = null;
                  }
                  if (bool4)
                  {
                    localObject5 = d;
                    bool3 = ((ap)localObject5).a(str1);
                    if (!bool3) {
                      ((List)localObject1).add(localObject4);
                    }
                    int i3 = ((List)localObject1).size();
                    int i4 = c;
                    if (i3 == i4) {
                      break label466;
                    }
                    boolean bool2 = f();
                    if (bool2) {
                      break label466;
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    label466:
    localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("Filtering to ");
    int i2 = o;
    ((StringBuilder)localObject2).append(i2);
    ((StringBuilder)localObject2).append(" ->");
    localObject2 = ((StringBuilder)localObject2).toString();
    b((String)localObject2);
    return (List)localObject1;
  }
  
  private void e()
  {
    Object localObject1 = e.iterator();
    Object localObject3;
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      localObject2 = (Participant)((Iterator)localObject1).next();
      localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>("Contact: ");
      String str = ((Participant)localObject2).a();
      ((StringBuilder)localObject3).append(str);
      ((StringBuilder)localObject3).append(" :: ");
      str = f;
      ((StringBuilder)localObject3).append(str);
      localObject3 = ((StringBuilder)localObject3).toString();
      b((String)localObject3);
      localObject3 = k;
      localObject2 = f;
      ((StringBuilder)localObject3).append((String)localObject2);
      localObject2 = ",";
      ((StringBuilder)localObject3).append((String)localObject2);
    }
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("Posting result contacts: ");
    Object localObject2 = e;
    int i1 = ((List)localObject2).size();
    ((StringBuilder)localObject1).append(i1);
    localObject1 = ((StringBuilder)localObject1).toString();
    b((String)localObject1);
    localObject1 = p;
    if (localObject1 != null) {
      ((TimingLogger)localObject1).dumpToLog();
    }
    localObject1 = h;
    localObject2 = "smsReferralPrefetchBatch";
    boolean bool3 = ((am)localObject1).g((String)localObject2);
    if (!bool3)
    {
      localObject1 = k;
      int i2 = ((StringBuilder)localObject1).length();
      if (i2 > 0)
      {
        localObject1 = k;
        i1 = ((StringBuilder)localObject1).length() + -1;
        ((StringBuilder)localObject1).deleteCharAt(i1);
        localObject1 = k.toString();
        boolean bool2 = com.truecaller.common.h.am.b((CharSequence)localObject1);
        if (!bool2)
        {
          localObject2 = h;
          localObject3 = "smsReferralPrefetchBatch";
          ((am)localObject2).a((String)localObject3, (String)localObject1);
        }
      }
    }
  }
  
  private boolean f()
  {
    List localList = e;
    int i1 = localList.size();
    int i2 = b;
    return i1 == i2;
  }
  
  public final List a()
  {
    Object localObject1 = p;
    if (localObject1 != null) {
      ((TimingLogger)localObject1).reset();
    }
    e.clear();
    localObject1 = h.a("smsReferralPrefetchBatch");
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("Pre-fetched contacts Present? ");
    int i1 = com.truecaller.common.h.am.b((CharSequence)localObject1);
    boolean bool1 = true;
    i1 ^= bool1;
    ((StringBuilder)localObject2).append(i1);
    localObject2 = ((StringBuilder)localObject2).toString();
    b((String)localObject2);
    boolean bool2 = com.truecaller.common.h.am.b((CharSequence)localObject1);
    i1 = 0;
    Object localObject3 = null;
    Object localObject6;
    int i2;
    if (!bool2)
    {
      localObject2 = ",";
      localObject1 = ((String)localObject1).split((String)localObject2);
      int i3 = localObject1.length;
      while (i1 < i3)
      {
        Object localObject4 = localObject1[i1];
        localObject5 = g.b((String)localObject4);
        if (localObject5 != null)
        {
          boolean bool4 = ((Contact)localObject5).U();
          bool4 = a((String)localObject4, bool4);
          if (!bool4)
          {
            localObject6 = i;
            localObject4 = Participant.a((Contact)localObject5, (String)localObject4, (com.truecaller.common.h.u)localObject6);
            localObject5 = e;
            ((List)localObject5).add(localObject4);
          }
        }
        i1 += 1;
      }
      e();
      localObject1 = new java/util/ArrayList;
      localObject2 = e;
      ((ArrayList)localObject1).<init>((Collection)localObject2);
      return Collections.unmodifiableList((List)localObject1);
    }
    localObject1 = b();
    localObject2 = h;
    Object localObject5 = "referralSuggestionCountLogged";
    boolean bool3 = ((am)localObject2).d((String)localObject5);
    if (!bool3)
    {
      i4 = ((Collection)localObject1).size();
      if (i4 > 0)
      {
        i2 = 6;
        if (i4 < i2)
        {
          i2 = 5;
        }
        else
        {
          i2 = 11;
          if (i4 < i2)
          {
            i2 = 10;
          }
          else
          {
            i2 = 51;
            if (i4 < i2) {
              i2 = 50;
            } else {
              i2 = 100;
            }
          }
        }
      }
      localObject2 = j;
      localObject5 = new com/truecaller/analytics/e$a;
      ((e.a)localObject5).<init>("ANDROID_Ref_SuggestionCount");
      localObject6 = "CountRange";
      localObject3 = ((e.a)localObject5).a((String)localObject6, i2).a();
      ((com.truecaller.analytics.b)localObject2).b((com.truecaller.analytics.e)localObject3);
      localObject2 = h;
      localObject3 = "referralSuggestionCountLogged";
      ((am)localObject2).a((String)localObject3, bool1);
    }
    f.addAll((Collection)localObject1);
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("PhoneBook contacts count: ");
    int i4 = f.size();
    ((StringBuilder)localObject1).append(i4);
    localObject1 = ((StringBuilder)localObject1).toString();
    b((String)localObject1);
    c();
    f.clear();
    localObject1 = new java/util/ArrayList;
    localObject2 = e;
    ((ArrayList)localObject1).<init>((Collection)localObject2);
    return Collections.unmodifiableList((List)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.referral.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */