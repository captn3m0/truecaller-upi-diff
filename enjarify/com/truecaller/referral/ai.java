package com.truecaller.referral;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.truecaller.log.AssertionUtil;
import com.truecaller.ui.TruecallerInit;

public final class ai
{
  static Intent a(Context paramContext)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, TruecallerInit.class);
    localIntent.setFlags(268435456);
    return localIntent;
  }
  
  public static void a(Intent paramIntent, ReferralManager paramReferralManager)
  {
    paramIntent = paramIntent.getExtras();
    if (paramIntent == null) {
      return;
    }
    Object localObject = paramIntent.getString("LAUNCH_MODE");
    int i = 1;
    String[] arrayOfString = new String[i];
    String str1 = String.valueOf(localObject);
    String str2 = "processReferralNotificationIntent:: Mode: ".concat(str1);
    str1 = null;
    arrayOfString[0] = str2;
    if (localObject == null) {
      return;
    }
    int j = ((String)localObject).hashCode();
    int k = 887528847;
    String str3;
    boolean bool;
    if (j != k)
    {
      k = 1251073302;
      if (j != k)
      {
        k = 1886261286;
        if (j != k)
        {
          i = 2003878147;
          if (j == i)
          {
            str3 = "MODE_SHOW_REFERRAL";
            bool = ((String)localObject).equals(str3);
            if (bool)
            {
              i = 3;
              break label196;
            }
          }
        }
        else
        {
          str2 = "MODE_REFER_MORE_FRIENDS";
          bool = ((String)localObject).equals(str2);
          if (bool) {
            break label196;
          }
        }
      }
      else
      {
        str3 = "MODE_REFERRAL_ON_BOARDING";
        bool = ((String)localObject).equals(str3);
        if (bool)
        {
          i = 2;
          break label196;
        }
      }
    }
    else
    {
      str3 = "MODE_REFERRAL_GRANTED_VIEW";
      bool = ((String)localObject).equals(str3);
      if (bool)
      {
        i = 0;
        str3 = null;
        break label196;
      }
    }
    i = -1;
    switch (i)
    {
    default: 
      break;
    case 3: 
      paramIntent = ReferralManager.ReferralLaunchContext.PUSH_NOTIFICATION;
      paramReferralManager.a(paramIntent);
      break;
    case 2: 
      paramReferralManager.c();
      return;
    case 1: 
      paramReferralManager.c();
      return;
    case 0: 
      label196:
      paramIntent = paramIntent.getString("REFERRAL_GRANTED_MESSAGE");
      localObject = new String[0];
      AssertionUtil.isNotNull(paramIntent, (String[])localObject);
      paramReferralManager.c(paramIntent);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.referral.ai
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */