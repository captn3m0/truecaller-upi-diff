package com.truecaller.referral;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v4.app.j;
import android.support.v4.app.o;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.data.entity.Contact;
import com.truecaller.log.AssertionUtil;
import java.io.Serializable;

public final class w
  extends Fragment
  implements ReferralManager, x
{
  aj a;
  private com.truecaller.ui.dialogs.g b;
  private AlertDialog c;
  private AlertDialog d;
  
  public static ReferralManager a(Fragment paramFragment, String paramString)
  {
    return a(paramFragment.getChildFragmentManager(), paramString);
  }
  
  public static ReferralManager a(f paramf, String paramString)
  {
    return a(paramf.getSupportFragmentManager(), paramString);
  }
  
  private static ReferralManager a(j paramj, String paramString)
  {
    w localw;
    try
    {
      localw = new com/truecaller/referral/w;
      localw.<init>();
      paramj = paramj.a();
      paramj = paramj.a(localw, paramString);
      paramj.e();
    }
    catch (Exception paramj)
    {
      AssertionUtil.reportThrowableButNeverCrash(paramj);
      localw = null;
    }
    return localw;
  }
  
  private void b(String paramString1, Contact paramContact, BulkSmsView.PromoLayout paramPromoLayout, ReferralManager.ReferralLaunchContext paramReferralLaunchContext, String paramString2, boolean paramBoolean)
  {
    if (paramContact == null) {
      paramString1 = e.a(paramString1, paramPromoLayout, paramReferralLaunchContext, paramString2);
    } else {
      paramString1 = e.a(paramString1, paramContact, paramPromoLayout, paramReferralLaunchContext, paramString2, paramBoolean);
    }
    getChildFragmentManager().a().a(paramString1, "BulkSmsDialog").a(null).d();
  }
  
  public final void a()
  {
    com.truecaller.ui.dialogs.g localg = new com/truecaller/ui/dialogs/g;
    Context localContext = requireContext();
    localg.<init>(localContext, true);
    b = localg;
    b.show();
  }
  
  public final void a(Context paramContext)
  {
    a.a(paramContext);
  }
  
  public final void a(Uri paramUri)
  {
    a.a(paramUri);
  }
  
  public final void a(ReferralManager.ReferralLaunchContext paramReferralLaunchContext)
  {
    a.a(paramReferralLaunchContext);
  }
  
  public final void a(ReferralManager.ReferralLaunchContext paramReferralLaunchContext, Contact paramContact)
  {
    a.a(paramReferralLaunchContext, paramContact);
  }
  
  public final void a(String paramString)
  {
    Toast.makeText(requireContext(), paramString, 0).show();
  }
  
  public final void a(String paramString1, Contact paramContact, BulkSmsView.PromoLayout paramPromoLayout, ReferralManager.ReferralLaunchContext paramReferralLaunchContext, String paramString2, boolean paramBoolean)
  {
    b(paramString1, paramContact, paramPromoLayout, paramReferralLaunchContext, paramString2, paramBoolean);
  }
  
  public final void a(String paramString1, BulkSmsView.PromoLayout paramPromoLayout, ReferralManager.ReferralLaunchContext paramReferralLaunchContext, String paramString2)
  {
    b(paramString1, null, paramPromoLayout, paramReferralLaunchContext, paramString2, false);
  }
  
  public final void a(String paramString, ReferralManager.ReferralLaunchContext paramReferralLaunchContext)
  {
    AlertDialog.Builder localBuilder = new android/support/v7/app/AlertDialog$Builder;
    Object localObject = requireContext();
    localBuilder.<init>((Context)localObject);
    localObject = LayoutInflater.from(requireContext()).inflate(2131558587, null);
    ((TextView)((View)localObject).findViewById(2131363752)).setText(paramString);
    localBuilder.setView((View)localObject);
    paramString = new com/truecaller/referral/-$$Lambda$w$Mq6ud2lqspn0_HTWO95YjWdA8fM;
    paramString.<init>(this, paramReferralLaunchContext);
    localBuilder.setPositiveButton(2131888625, paramString);
    paramString = -..Lambda.w.qXFG0xtOVY3QiPBUsgfos8GGA_o.INSTANCE;
    localBuilder.setNegativeButton(2131887928, paramString);
    paramString = localBuilder.show();
    d = paramString;
  }
  
  public final void a(String paramString1, ReferralUrl paramReferralUrl, ReferralManager.ReferralLaunchContext paramReferralLaunchContext, String paramString2)
  {
    Object localObject = getActivity();
    if (localObject != null)
    {
      boolean bool = isAdded();
      if (bool)
      {
        localObject = getChildFragmentManager().a().a(null);
        if (paramString2 == null) {
          paramString1 = ao.a(paramString1, paramReferralUrl, paramReferralLaunchContext);
        } else {
          paramString1 = ao.a(paramString1, paramReferralUrl, paramReferralLaunchContext, paramString2);
        }
        paramReferralUrl = ao.class.getSimpleName();
        ((o)localObject).a(paramString1, paramReferralUrl).d();
        return;
      }
    }
  }
  
  public final boolean a(Contact paramContact)
  {
    return a.a(paramContact);
  }
  
  public final void b()
  {
    a.b();
  }
  
  public final void b(ReferralManager.ReferralLaunchContext paramReferralLaunchContext)
  {
    a.b(paramReferralLaunchContext);
  }
  
  public final void b(String paramString)
  {
    a.b(paramString);
  }
  
  public final boolean b(Contact paramContact)
  {
    return a.b(paramContact);
  }
  
  public final void c()
  {
    a.c();
  }
  
  public final void c(String paramString)
  {
    a.c(paramString);
  }
  
  public final boolean c(ReferralManager.ReferralLaunchContext paramReferralLaunchContext)
  {
    return a.d(paramReferralLaunchContext);
  }
  
  public final void d()
  {
    com.truecaller.ui.dialogs.g localg = b;
    if (localg != null)
    {
      boolean bool = localg.isShowing();
      if (bool)
      {
        localg = b;
        localg.dismiss();
      }
    }
  }
  
  public final void e()
  {
    a.e();
  }
  
  public final void f()
  {
    AlertDialog localAlertDialog = c;
    if (localAlertDialog != null) {
      localAlertDialog.dismiss();
    }
  }
  
  public final void g()
  {
    AlertDialog localAlertDialog = d;
    if (localAlertDialog != null) {
      localAlertDialog.dismiss();
    }
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Object localObject1 = new com/truecaller/referral/m$a;
    boolean bool = false;
    Object localObject2 = null;
    ((m.a)localObject1).<init>((byte)0);
    Object localObject3 = (bp)dagger.a.g.a(((bk)requireContext().getApplicationContext()).a());
    b = ((bp)localObject3);
    localObject3 = new com/truecaller/referral/y;
    ((y)localObject3).<init>();
    localObject3 = (y)dagger.a.g.a(localObject3);
    a = ((y)localObject3);
    dagger.a.g.a(a, y.class);
    dagger.a.g.a(b, bp.class);
    localObject3 = new com/truecaller/referral/m;
    y localy = a;
    localObject1 = b;
    ((m)localObject3).<init>(localy, (bp)localObject1, (byte)0);
    ((t)localObject3).a(this);
    localObject1 = a;
    if (paramBundle == null) {
      paramBundle = getArguments();
    }
    if (paramBundle != null)
    {
      localObject2 = "referral_launch_context";
      bool = paramBundle.containsKey((String)localObject2);
      if (bool)
      {
        localObject2 = (ReferralManager.ReferralLaunchContext)paramBundle.getSerializable("referral_launch_context");
        f = ((ReferralManager.ReferralLaunchContext)localObject2);
      }
      localObject2 = "single_contact";
      bool = paramBundle.containsKey((String)localObject2);
      if (bool)
      {
        localObject2 = "single_contact";
        paramBundle = (Contact)paramBundle.getParcelable((String)localObject2);
        g = paramBundle;
      }
    }
    a.a(this);
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    a.y_();
  }
  
  public final void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    Object localObject = a;
    Contact localContact = g;
    paramBundle.putParcelable("single_contact", localContact);
    localObject = f;
    paramBundle.putSerializable("referral_launch_context", (Serializable)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.referral.w
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */