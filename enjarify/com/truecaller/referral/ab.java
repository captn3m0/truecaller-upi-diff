package com.truecaller.referral;

import dagger.a.d;
import javax.inject.Provider;

public final class ab
  implements d
{
  private final y a;
  private final Provider b;
  private final Provider c;
  
  private ab(y paramy, Provider paramProvider1, Provider paramProvider2)
  {
    a = paramy;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static ab a(y paramy, Provider paramProvider1, Provider paramProvider2)
  {
    ab localab = new com/truecaller/referral/ab;
    localab.<init>(paramy, paramProvider1, paramProvider2);
    return localab;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.referral.ab
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */