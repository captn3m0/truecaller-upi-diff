package com.truecaller.referral;

import dagger.a.d;
import javax.inject.Provider;

public final class h
  implements d
{
  private final f a;
  private final Provider b;
  
  private h(f paramf, Provider paramProvider)
  {
    a = paramf;
    b = paramProvider;
  }
  
  public static h a(f paramf, Provider paramProvider)
  {
    h localh = new com/truecaller/referral/h;
    localh.<init>(paramf, paramProvider);
    return localh;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.referral.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */