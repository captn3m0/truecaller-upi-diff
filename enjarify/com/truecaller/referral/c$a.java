package com.truecaller.referral;

import android.net.Uri;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import com.truecaller.ui.components.d.c;
import com.truecaller.ui.view.ContactPhoto;
import com.truecaller.utils.ui.b;

final class c$a
  extends d.c
  implements a
{
  private ContactPhoto a;
  private TextView c;
  private TextView d;
  
  public c$a(View paramView, j paramj, int paramInt)
  {
    super(paramView);
    Object localObject;
    switch (paramInt)
    {
    default: 
      break;
    case 3: 
    case 4: 
      localObject = new com/truecaller/referral/-$$Lambda$c$a$VZzlGh8fIDMtDcVXgXMpJFT1rEA;
      ((-..Lambda.c.a.VZzlGh8fIDMtDcVXgXMpJFT1rEA)localObject).<init>(paramj);
      paramView.setOnClickListener((View.OnClickListener)localObject);
      break;
    case 1: 
    case 2: 
      localObject = (ContactPhoto)paramView.findViewById(2131362554);
      a = ((ContactPhoto)localObject);
      localObject = (TextView)paramView.findViewById(2131363791);
      c = ((TextView)localObject);
      localObject = (TextView)paramView.findViewById(2131363828);
      d = ((TextView)localObject);
      localObject = new com/truecaller/referral/-$$Lambda$c$a$VdvGwi61Pwm_Ug3yRa4nr2-kRfE;
      ((-..Lambda.c.a.VdvGwi61Pwm_Ug3yRa4nr2-kRfE)localObject).<init>(this, paramj);
      paramView.setOnClickListener((View.OnClickListener)localObject);
      paramInt = 2131361826;
      localObject = (ImageView)paramView.findViewById(paramInt);
      if (localObject != null)
      {
        paramView = b.c(paramView.getContext(), 2130968844);
        ((ImageView)localObject).setImageDrawable(paramView);
        paramView = new com/truecaller/referral/-$$Lambda$c$a$SRtGwzJU6VuQQxN5Hcd0nUlUl00;
        paramView.<init>(this, paramj);
        ((ImageView)localObject).setOnClickListener(paramView);
        return;
      }
      break;
    }
  }
  
  public final void a(Uri paramUri)
  {
    a.a(paramUri, null);
  }
  
  public final void a(String paramString)
  {
    c.setText(paramString);
  }
  
  public final void a(boolean paramBoolean)
  {
    TextView localTextView = d;
    if (paramBoolean) {
      paramBoolean = false;
    } else {
      paramBoolean = true;
    }
    localTextView.setVisibility(paramBoolean);
  }
  
  public final void b(String paramString)
  {
    d.setText(paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.referral.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */