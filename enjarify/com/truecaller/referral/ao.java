package com.truecaller.referral;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.e;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.truecaller.TrueApp;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e.a;
import com.truecaller.bp;
import com.truecaller.common.h.am;
import com.truecaller.log.AssertionUtil;
import com.truecaller.profile.c;
import com.truecaller.util.at;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class ao
  extends e
{
  private static final List a;
  private bp b;
  private String c;
  private ReferralUrl d;
  private ReferralManager.ReferralLaunchContext e;
  private PackageManager f;
  private String g;
  private String h;
  private LinearLayout i;
  private b j;
  
  static
  {
    ao.a[] arrayOfa = new ao.a[5];
    ao.a locala = new com/truecaller/referral/ao$a;
    locala.<init>("com.whatsapp", "W");
    arrayOfa[0] = locala;
    locala = new com/truecaller/referral/ao$a;
    locala.<init>("com.facebook.orca", "M");
    arrayOfa[1] = locala;
    locala = new com/truecaller/referral/ao$a;
    locala.<init>("com.imo.android.imoim", "I");
    arrayOfa[2] = locala;
    locala = new com/truecaller/referral/ao$a;
    locala.<init>("com.facebook.katana", "F");
    arrayOfa[3] = locala;
    locala = new com/truecaller/referral/ao$a;
    locala.<init>("com.twitter.android", "T");
    arrayOfa[4] = locala;
    a = Collections.unmodifiableList(Arrays.asList(arrayOfa));
  }
  
  private View a(ao.a parama, List paramList)
  {
    paramList = paramList.iterator();
    boolean bool2;
    do
    {
      boolean bool1 = paramList.hasNext();
      if (!bool1) {
        break;
      }
      localObject1 = (ResolveInfo)paramList.next();
      localObject2 = activityInfo.packageName;
      String str = a;
      bool2 = ((String)localObject2).equals(str);
    } while (!bool2);
    paramList = f;
    paramList = ((ResolveInfo)localObject1).loadLabel(paramList);
    Object localObject2 = f;
    Object localObject1 = ((ResolveInfo)localObject1).loadIcon((PackageManager)localObject2);
    paramList = a(paramList, (Drawable)localObject1);
    paramList.setTag(parama);
    return paramList;
    return null;
  }
  
  private View a(CharSequence paramCharSequence, Drawable paramDrawable)
  {
    Object localObject = LayoutInflater.from(requireContext());
    LinearLayout localLinearLayout = i;
    localObject = ((LayoutInflater)localObject).inflate(2131559035, localLinearLayout, false);
    ((TextView)((View)localObject).findViewById(2131363657)).setText(paramCharSequence);
    ((ImageView)((View)localObject).findViewById(2131363654)).setImageDrawable(paramDrawable);
    return (View)localObject;
  }
  
  public static ao a(String paramString, ReferralUrl paramReferralUrl, ReferralManager.ReferralLaunchContext paramReferralLaunchContext)
  {
    return b(paramString, paramReferralUrl, paramReferralLaunchContext, null);
  }
  
  public static ao a(String paramString1, ReferralUrl paramReferralUrl, ReferralManager.ReferralLaunchContext paramReferralLaunchContext, String paramString2)
  {
    return b(paramString1, paramReferralUrl, paramReferralLaunchContext, paramString2);
  }
  
  private void a(String paramString)
  {
    Object localObject1 = new com/truecaller/analytics/e$a;
    ((e.a)localObject1).<init>("ANDROID_Ref_ShareAppSelected");
    String str = e.name();
    paramString = ((e.a)localObject1).a("Source", str).a("App", paramString);
    localObject1 = e;
    Object localObject2 = ReferralManager.ReferralLaunchContext.DEEP_LINK;
    if (localObject1 == localObject2)
    {
      localObject1 = "Campaign";
      localObject2 = am.a(g);
      paramString.a((String)localObject1, (String)localObject2);
    }
    localObject1 = j;
    paramString = paramString.a();
    ((b)localObject1).b(paramString);
  }
  
  private static void a(String paramString1, ReferralUrl paramReferralUrl, ReferralManager.ReferralLaunchContext paramReferralLaunchContext, String paramString2, Bundle paramBundle)
  {
    paramBundle.putString("EXTRA_REFERRAL_CODE", paramString1);
    paramBundle.putParcelable("EXTRA_REFERRAL_LINK", paramReferralUrl);
    paramBundle.putSerializable("EXTRA_REFERRAL_LAUNCH_CONTEXT", paramReferralLaunchContext);
    paramBundle.putSerializable("EXTRA_REFERRAL_LAUNCH_CONTEXT", paramReferralLaunchContext);
    paramBundle.putSerializable("EXTRA_DEEPLINK_CAMPAIGN_ID", paramString2);
  }
  
  private static ao b(String paramString1, ReferralUrl paramReferralUrl, ReferralManager.ReferralLaunchContext paramReferralLaunchContext, String paramString2)
  {
    ao localao = new com/truecaller/referral/ao;
    localao.<init>();
    Object localObject = new String[0];
    AssertionUtil.isNotNull(paramReferralLaunchContext, (String[])localObject);
    localObject = new android/os/Bundle;
    ((Bundle)localObject).<init>();
    a(paramString1, paramReferralUrl, paramReferralLaunchContext, paramString2, (Bundle)localObject);
    localao.setArguments((Bundle)localObject);
    return localao;
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Object localObject = null;
    int k = 1;
    setStyle(k, 0);
    if (paramBundle == null) {
      paramBundle = getArguments();
    }
    localObject = new String[0];
    AssertionUtil.isNotNull(paramBundle, (String[])localObject);
    localObject = requireContext().getPackageManager();
    f = ((PackageManager)localObject);
    localObject = paramBundle.getString("EXTRA_REFERRAL_CODE");
    c = ((String)localObject);
    localObject = (ReferralUrl)paramBundle.getParcelable("EXTRA_REFERRAL_LINK");
    d = ((ReferralUrl)localObject);
    localObject = (ReferralManager.ReferralLaunchContext)paramBundle.getSerializable("EXTRA_REFERRAL_LAUNCH_CONTEXT");
    e = ((ReferralManager.ReferralLaunchContext)localObject);
    paramBundle = paramBundle.getString("EXTRA_DEEPLINK_CAMPAIGN_ID");
    g = paramBundle;
    paramBundle = TrueApp.y().a();
    b = paramBundle;
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    paramBundle = null;
    paramLayoutInflater = paramLayoutInflater.inflate(2131559219, paramViewGroup, false);
    paramViewGroup = (LinearLayout)paramLayoutInflater.findViewById(2131362562);
    i = paramViewGroup;
    paramViewGroup = i;
    int k = 2131364884;
    paramViewGroup = (TextView)paramViewGroup.findViewById(k);
    Object localObject1 = (TextView)i.findViewById(2131364609);
    Object localObject2 = (ImageView)i.findViewById(2131363301);
    Object localObject3 = paramLayoutInflater.findViewById(2131361824);
    Object localObject4 = new com/truecaller/referral/-$$Lambda$ao$8-WmrI2qHooYwnJAzsP4Bk286GM;
    ((-..Lambda.ao.8-WmrI2qHooYwnJAzsP4Bk286GM)localObject4).<init>(this);
    ((View)localObject3).setOnClickListener((View.OnClickListener)localObject4);
    int m = 2131888627;
    paramViewGroup.setText(m);
    ((TextView)localObject1).setText(2131888626);
    int i1 = 2131234218;
    ((ImageView)localObject2).setImageResource(i1);
    paramViewGroup = i;
    localObject1 = e;
    int i2 = 2131888827;
    localObject2 = getString(i2);
    localObject3 = requireContext();
    int i3 = 2131234448;
    localObject3 = at.a((Context)localObject3, i3);
    localObject2 = a((CharSequence)localObject2, (Drawable)localObject3);
    localObject3 = new com/truecaller/referral/-$$Lambda$ao$ONTmzaFEIImhX56JCP04H100Uo8;
    ((-..Lambda.ao.ONTmzaFEIImhX56JCP04H100Uo8)localObject3).<init>(this, (ReferralManager.ReferralLaunchContext)localObject1);
    ((View)localObject2).setOnClickListener((View.OnClickListener)localObject3);
    paramViewGroup.addView((View)localObject2);
    paramViewGroup = new android/content/Intent;
    paramViewGroup.<init>("android.intent.action.SEND");
    paramViewGroup.setType("text/plain");
    localObject1 = requireContext().getPackageManager();
    localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>();
    paramViewGroup = ((PackageManager)localObject1).queryIntentActivities(paramViewGroup, 0);
    localObject1 = a.iterator();
    for (;;)
    {
      int n = ((Iterator)localObject1).hasNext();
      if (n == 0) {
        break;
      }
      localObject3 = (ao.a)((Iterator)localObject1).next();
      localObject4 = a((ao.a)localObject3, paramViewGroup);
      if (localObject4 != null)
      {
        Object localObject5 = new com/truecaller/referral/-$$Lambda$ao$IWSuJKcErtJGZ5Bkyc8gsaAJVxg;
        ((-..Lambda.ao.IWSuJKcErtJGZ5Bkyc8gsaAJVxg)localObject5).<init>(this, (ao.a)localObject3);
        ((View)localObject4).setOnClickListener((View.OnClickListener)localObject5);
        localObject5 = i;
        ((LinearLayout)localObject5).addView((View)localObject4);
        localObject3 = b;
        ((StringBuilder)localObject2).append((String)localObject3);
        n = 1;
        ((StringBuilder)localObject2).append(n);
      }
      else
      {
        localObject3 = b;
        ((StringBuilder)localObject2).append((String)localObject3);
        ((StringBuilder)localObject2).append(0);
      }
    }
    paramViewGroup = i;
    paramBundle = getString(2131888825);
    localObject1 = at.a(requireContext(), 2131234447);
    paramBundle = a(paramBundle, (Drawable)localObject1);
    localObject1 = new com/truecaller/referral/-$$Lambda$ao$xpVEtXGFKC8Bho0safdXb5sn0K8;
    ((-..Lambda.ao.xpVEtXGFKC8Bho0safdXb5sn0K8)localObject1).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject1);
    paramViewGroup.addView(paramBundle);
    paramViewGroup = c.b(b.I());
    h = paramViewGroup;
    paramViewGroup = b.c();
    j = paramViewGroup;
    paramViewGroup = j;
    paramBundle = new com/truecaller/analytics/e$a;
    paramBundle.<init>("ANDROID_Ref_ShareAppsListed");
    localObject2 = ((StringBuilder)localObject2).toString();
    paramBundle = paramBundle.a("Apps", (String)localObject2).a();
    paramViewGroup.b(paramBundle);
    return paramLayoutInflater;
  }
  
  public void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    String str1 = c;
    ReferralUrl localReferralUrl = d;
    ReferralManager.ReferralLaunchContext localReferralLaunchContext = e;
    String str2 = g;
    a(str1, localReferralUrl, localReferralLaunchContext, str2, paramBundle);
  }
  
  public void onStart()
  {
    super.onStart();
    Window localWindow = getDialog().getWindow();
    if (localWindow != null)
    {
      int k = -1;
      int m = -2;
      localWindow.setLayout(k, m);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.referral.ao
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */