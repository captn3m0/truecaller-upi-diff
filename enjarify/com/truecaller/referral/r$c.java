package com.truecaller.referral;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.data.entity.Contact;

final class r$c
  extends u
{
  private final Contact b;
  
  private r$c(e parame, Contact paramContact)
  {
    super(parame);
    b = paramContact;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".isWhatsAppProfilePresentForContact(");
    String str = a(b, 2);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.referral.r.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */