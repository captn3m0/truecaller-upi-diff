package com.truecaller.referral;

import android.content.Context;
import android.net.Uri;
import com.truecaller.analytics.e.a;
import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.i;
import com.truecaller.bb;
import com.truecaller.calling.recorder.h;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.log.AssertionUtil;
import com.truecaller.util.al;
import com.truecaller.util.p;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

final class aj
  extends bb
  implements ReferralManager, o.a, p.a
{
  String a;
  final am c;
  final ak d;
  aj.a e;
  ReferralManager.ReferralLaunchContext f;
  Contact g;
  private com.truecaller.androidactors.a h;
  private final com.truecaller.common.g.a i;
  private final com.truecaller.utils.n j;
  private final com.truecaller.f.a k;
  private final dagger.a l;
  private final dagger.a m;
  private final dagger.a n;
  private final h o;
  private final com.truecaller.abtest.c p;
  private final i q;
  private final com.truecaller.analytics.a.f r;
  private final ap s;
  private final com.truecaller.analytics.b t;
  private final al u;
  private final com.truecaller.featuretoggles.e v;
  private final HashMap w;
  private final HashMap x;
  
  aj(am paramam, com.truecaller.common.g.a parama, ak paramak, com.truecaller.utils.n paramn, ap paramap, com.truecaller.analytics.b paramb, al paramal, com.truecaller.featuretoggles.e parame, dagger.a parama1, dagger.a parama2, com.truecaller.f.a parama3, dagger.a parama4, h paramh, com.truecaller.abtest.c paramc, i parami, com.truecaller.analytics.a.f paramf)
  {
    Object localObject1 = new java/util/HashMap;
    int i1 = ReferralManager.ReferralLaunchContext.values().length;
    ((HashMap)localObject1).<init>(i1);
    w = ((HashMap)localObject1);
    localObject1 = w;
    Object localObject2 = ReferralManager.ReferralLaunchContext.HOME_SCREEN;
    ((HashMap)localObject1).put(localObject2, "featureSearchBarIcon");
    localObject1 = w;
    localObject2 = ReferralManager.ReferralLaunchContext.NAVIGATION_DRAWER;
    ((HashMap)localObject1).put(localObject2, "featureReferralNavigationDrawer");
    localObject1 = w;
    localObject2 = ReferralManager.ReferralLaunchContext.INBOX_OVERFLOW;
    ((HashMap)localObject1).put(localObject2, "featureInboxOverflow");
    localObject1 = w;
    localObject2 = ReferralManager.ReferralLaunchContext.CONTACT_DETAILS;
    ((HashMap)localObject1).put(localObject2, "featureContactDetail");
    localObject1 = w;
    localObject2 = ReferralManager.ReferralLaunchContext.CONTACTS;
    ((HashMap)localObject1).put(localObject2, "featureContacts");
    localObject1 = w;
    localObject2 = ReferralManager.ReferralLaunchContext.USER_BUSY_PROMPT;
    ((HashMap)localObject1).put(localObject2, "featureUserBusyPrompt");
    localObject1 = w;
    localObject2 = ReferralManager.ReferralLaunchContext.AFTER_CALL;
    ((HashMap)localObject1).put(localObject2, "featureAftercall");
    localObject1 = w;
    localObject2 = ReferralManager.ReferralLaunchContext.AFTER_CALL_SAVE_CONTACT;
    ((HashMap)localObject1).put(localObject2, "featureAftercallSaveContact");
    localObject1 = w;
    localObject2 = ReferralManager.ReferralLaunchContext.PUSH_NOTIFICATION;
    ((HashMap)localObject1).put(localObject2, "featurePushNotification");
    localObject1 = w;
    localObject2 = ReferralManager.ReferralLaunchContext.DEEP_LINK;
    ((HashMap)localObject1).put(localObject2, "featureLaunchReferralFromDeeplink");
    localObject1 = w;
    localObject2 = ReferralManager.ReferralLaunchContext.AFTER_CALL_PROMO;
    ((HashMap)localObject1).put(localObject2, "featureReferralAfterCallPromo");
    localObject1 = w;
    localObject2 = ReferralManager.ReferralLaunchContext.SEARCH_SCREEN_PROMO;
    ((HashMap)localObject1).put(localObject2, "featureSearchScreenPromo");
    localObject1 = new java/util/HashMap;
    i1 = ReferralManager.RedeemCodeContext.values().length;
    ((HashMap)localObject1).<init>(i1);
    x = ((HashMap)localObject1);
    localObject1 = x;
    localObject2 = ReferralManager.RedeemCodeContext.GO_PRO;
    ((HashMap)localObject1).put(localObject2, "featureGoPro");
    localObject1 = paramam;
    c = paramam;
    localObject1 = parama;
    i = parama;
    localObject1 = paramak;
    d = paramak;
    localObject1 = paramn;
    j = paramn;
    localObject1 = paramap;
    s = paramap;
    localObject1 = paramb;
    t = paramb;
    localObject1 = paramal;
    u = paramal;
    localObject1 = parame;
    v = parame;
    localObject1 = parama2;
    m = parama2;
    localObject1 = parama1;
    l = parama1;
    localObject1 = parama3;
    k = parama3;
    localObject1 = parama4;
    n = parama4;
    localObject1 = paramh;
    o = paramh;
    localObject1 = paramc;
    p = paramc;
    localObject1 = parami;
    q = parami;
    localObject1 = paramf;
    r = paramf;
  }
  
  private static BulkSmsView.PromoLayout a(boolean paramBoolean1, boolean paramBoolean2)
  {
    int[] arrayOfInt1 = new int[0];
    if (!paramBoolean1)
    {
      paramBoolean1 = 2131363044;
      arrayOfInt1 = org.c.a.a.a.a.b(arrayOfInt1, paramBoolean1);
    }
    int[] arrayOfInt2;
    if (!paramBoolean2)
    {
      paramBoolean1 = 2131363047;
      arrayOfInt1 = org.c.a.a.a.a.b(arrayOfInt1, paramBoolean1);
      arrayOfInt2 = arrayOfInt1;
    }
    else
    {
      arrayOfInt2 = arrayOfInt1;
    }
    BulkSmsView.PromoLayout localPromoLayout = new com/truecaller/referral/BulkSmsView$PromoLayout;
    localPromoLayout.<init>(2131558833, null, null, null, null, arrayOfInt2);
    return localPromoLayout;
  }
  
  private void a(String paramString, ReferralUrl paramReferralUrl)
  {
    Object localObject1 = b;
    if (localObject1 == null) {
      return;
    }
    localObject1 = o;
    boolean bool1 = ((h)localObject1).a();
    Object localObject2 = (com.truecaller.whoviewedme.w)n.get();
    boolean bool2 = ((com.truecaller.whoviewedme.w)localObject2).a();
    Object localObject3 = aj.2.b;
    Object localObject4 = e;
    int i1 = ((aj.a)localObject4).ordinal();
    int i2 = localObject3[i1];
    switch (i2)
    {
    default: 
      break;
    case 2: 
      paramString = (q)((com.truecaller.androidactors.f)m.get()).a();
      paramReferralUrl = g;
      paramString = paramString.a(paramReferralUrl);
      paramReferralUrl = q;
      localObject1 = new com/truecaller/referral/-$$Lambda$aj$SFpuCyfridf8o4qb5LFuLPbUg2U;
      ((-..Lambda.aj.SFpuCyfridf8o4qb5LFuLPbUg2U)localObject1).<init>(this);
      paramString = paramString.a(paramReferralUrl, (ac)localObject1);
      h = paramString;
      break;
    case 1: 
      localObject3 = f;
      localObject4 = ReferralManager.ReferralLaunchContext.AFTER_CALL_PROMO;
      if (localObject3 == localObject4)
      {
        paramString = c;
        long l1 = System.currentTimeMillis();
        paramString.a("referralAfterCallPromoLastShown", l1);
        paramString = (x)b;
        paramReferralUrl = k();
        localObject1 = a(bool1, bool2);
        localObject2 = f;
        localObject3 = a;
        paramString.a(paramReferralUrl, (BulkSmsView.PromoLayout)localObject1, (ReferralManager.ReferralLaunchContext)localObject2, (String)localObject3);
        return;
      }
      localObject3 = c.a("featureReferralShareApps");
      localObject4 = "Bulk Sms Single Screen";
      boolean bool3 = com.truecaller.common.h.am.b((CharSequence)localObject3, (CharSequence)localObject4);
      if (bool3)
      {
        paramString = (x)b;
        paramReferralUrl = k();
        localObject1 = a(bool1, bool2);
        localObject2 = f;
        localObject3 = a;
        paramString.a(paramReferralUrl, (BulkSmsView.PromoLayout)localObject1, (ReferralManager.ReferralLaunchContext)localObject2, (String)localObject3);
        return;
      }
      localObject1 = (x)b;
      localObject2 = f;
      localObject3 = a;
      ((x)localObject1).a(paramString, paramReferralUrl, (ReferralManager.ReferralLaunchContext)localObject2, (String)localObject3);
      return;
    }
  }
  
  private boolean a(String paramString1, String paramString2)
  {
    Object localObject = c;
    String str = "referralCode";
    localObject = ((am)localObject).a(str);
    boolean bool = com.truecaller.common.h.am.a((CharSequence)localObject, paramString1);
    if (bool) {
      return false;
    }
    c.a("redeemCode", paramString1);
    paramString1 = t;
    localObject = new com/truecaller/analytics/e$a;
    ((e.a)localObject).<init>("ANDROID_Ref_RedeemCode");
    paramString2 = ((e.a)localObject).a("Source", paramString2).a();
    paramString1.b(paramString2);
    return true;
  }
  
  private void b(Uri paramUri)
  {
    paramUri = ReferralUrl.a(paramUri);
    if (paramUri != null)
    {
      Object localObject1 = d;
      Object localObject2 = ReferralUrl.a.c;
      Object localObject3;
      if (localObject1 != localObject2)
      {
        localObject2 = ReferralUrl.a.b;
        if (localObject1 != localObject2) {}
      }
      else
      {
        localObject2 = r;
        localObject3 = "ab_test_bulk_invite_17575_conv";
        ((com.truecaller.analytics.a.f)localObject2).a((String)localObject3);
      }
      localObject2 = e;
      if (localObject2 != null)
      {
        localObject2 = t;
        localObject3 = new com/truecaller/analytics/e$a;
        ((e.a)localObject3).<init>("ANDROID_Ref_LinkRecd");
        String str = "Source";
        paramUri = e.name();
        paramUri = ((e.a)localObject3).a(str, paramUri);
        localObject3 = "Medium";
        localObject1 = ((ReferralUrl.a)localObject1).name();
        paramUri = paramUri.a((String)localObject3, (String)localObject1).a();
        ((com.truecaller.analytics.b)localObject2).b(paramUri);
      }
    }
  }
  
  private static String c(Uri paramUri)
  {
    paramUri = paramUri.getPathSegments();
    if (paramUri != null)
    {
      String str = "promo";
      boolean bool = paramUri.contains(str);
      if (bool)
      {
        str = "promo";
        int i1 = paramUri.indexOf(str) + 1;
        int i2 = paramUri.size();
        if (i2 > i1) {
          return (String)paramUri.get(i1);
        }
      }
    }
    return null;
  }
  
  private boolean i()
  {
    Object localObject = c;
    String str = "featureReferralDeeplink";
    boolean bool = ((am)localObject).d(str);
    if (bool)
    {
      localObject = c;
      str = "codeRedeemed";
      bool = ((am)localObject).d(str);
      if (!bool)
      {
        localObject = c;
        str = "redeemCode";
        localObject = ((am)localObject).a(str);
        bool = com.truecaller.common.h.am.b((CharSequence)localObject);
        if (bool) {
          return false;
        }
      }
    }
    return true;
  }
  
  private void j()
  {
    String str = c.a("referralCode");
    Object localObject1 = c.a("referralLink");
    Object localObject2 = b;
    if (localObject2 == null) {
      return;
    }
    localObject2 = u;
    boolean bool1 = ((al)localObject2).a();
    if (!bool1) {
      return;
    }
    boolean bool2 = com.truecaller.common.h.am.b((CharSequence)localObject1);
    if (!bool2)
    {
      bool2 = com.truecaller.common.h.am.b(str);
      if (!bool2)
      {
        localObject1 = h();
        a(str, (ReferralUrl)localObject1);
        return;
      }
    }
    ((x)b).a();
    d.a(this);
  }
  
  private String k()
  {
    Object localObject1 = e;
    Object localObject2 = aj.a.b;
    if (localObject1 == localObject2)
    {
      localObject1 = p;
      boolean bool = ((com.truecaller.abtest.c)localObject1).b();
      if (bool) {
        localObject1 = ReferralUrl.a.c;
      } else {
        localObject1 = ReferralUrl.a.b;
      }
    }
    else
    {
      localObject1 = ReferralUrl.a.a;
    }
    localObject2 = j;
    Object[] arrayOfObject = new Object[2];
    ReferralUrl localReferralUrl = h();
    d = ((ReferralUrl.a)localObject1);
    localObject1 = localReferralUrl.a();
    arrayOfObject[0] = localObject1;
    String str = com.truecaller.profile.c.b(i);
    arrayOfObject[1] = str;
    return ((com.truecaller.utils.n)localObject2).a(2131888623, arrayOfObject);
  }
  
  private void l()
  {
    Object localObject = b;
    if (localObject == null) {
      return;
    }
    d.a();
  }
  
  public final void a()
  {
    Object localObject1 = b;
    if (localObject1 == null) {
      return;
    }
    ((x)b).d();
    localObject1 = (x)b;
    Object localObject2 = j;
    Object[] arrayOfObject = new Object[0];
    localObject2 = ((com.truecaller.utils.n)localObject2).a(2131888630, arrayOfObject);
    ((x)localObject1).a((String)localObject2);
  }
  
  public final void a(Context paramContext)
  {
    boolean bool = i();
    if (bool) {
      return;
    }
    -..Lambda.aj.ID3mZ8t6TPaw7TgC-4gw2x-EbxY localID3mZ8t6TPaw7TgC-4gw2x-EbxY = new com/truecaller/referral/-$$Lambda$aj$ID3mZ8t6TPaw7TgC-4gw2x-EbxY;
    localID3mZ8t6TPaw7TgC-4gw2x-EbxY.<init>(this);
    ak.a(paramContext, localID3mZ8t6TPaw7TgC-4gw2x-EbxY);
  }
  
  public final void a(Uri paramUri)
  {
    boolean bool1 = i();
    if (bool1) {
      return;
    }
    Object localObject;
    if (paramUri != null)
    {
      localObject = c(paramUri);
      boolean bool2 = com.truecaller.common.h.am.b((CharSequence)localObject);
      if (!bool2)
      {
        String str = "GP";
        bool1 = a((String)localObject, str);
        if (bool1) {
          b(paramUri);
        }
      }
      return;
    }
    paramUri = b;
    if (paramUri != null)
    {
      paramUri = ((x)b).h();
      localObject = new com/truecaller/referral/-$$Lambda$aj$MwXfauW_0EcahsdpeWSacObyJBQ;
      ((-..Lambda.aj.MwXfauW_0EcahsdpeWSacObyJBQ)localObject).<init>(this);
      ak.a(paramUri, (p)localObject);
    }
  }
  
  public final void a(RedeemCodeResponse paramRedeemCodeResponse)
  {
    Object localObject = b;
    if (localObject == null) {
      return;
    }
    ((x)b).d();
    ((x)b).f();
    localObject = j;
    Object[] arrayOfObject1 = new Object[2];
    Integer localInteger = Integer.valueOf(c);
    arrayOfObject1[0] = localInteger;
    com.truecaller.utils.n localn = j;
    int i1 = c;
    Object[] arrayOfObject2 = new Object[0];
    paramRedeemCodeResponse = localn.a(2131755063, i1, arrayOfObject2);
    arrayOfObject1[1] = paramRedeemCodeResponse;
    paramRedeemCodeResponse = ((com.truecaller.utils.n)localObject).a(2131888658, arrayOfObject1);
    localObject = (x)b;
    ReferralManager.ReferralLaunchContext localReferralLaunchContext = ReferralManager.ReferralLaunchContext.PUSH_NOTIFICATION;
    ((x)localObject).a(paramRedeemCodeResponse, localReferralLaunchContext);
  }
  
  public final void a(ReferralManager.ReferralLaunchContext paramReferralLaunchContext)
  {
    f = paramReferralLaunchContext;
    boolean bool = d(paramReferralLaunchContext);
    if (!bool) {
      return;
    }
    Object localObject = aj.2.a;
    int i2 = paramReferralLaunchContext.ordinal();
    int i1 = localObject[i2];
    switch (i1)
    {
    default: 
      localObject = new java/lang/IllegalArgumentException;
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      String str = "Launch context ";
      localStringBuilder.<init>(str);
      localStringBuilder.append(paramReferralLaunchContext);
      localStringBuilder.append(" not handled");
      paramReferralLaunchContext = localStringBuilder.toString();
      ((IllegalArgumentException)localObject).<init>(paramReferralLaunchContext);
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localObject);
      break;
    case 7: 
    case 8: 
    case 9: 
    case 10: 
    case 11: 
    case 12: 
      paramReferralLaunchContext = aj.a.b;
      e = paramReferralLaunchContext;
      break;
    case 1: 
    case 2: 
    case 3: 
    case 4: 
    case 5: 
    case 6: 
      paramReferralLaunchContext = aj.a.a;
      e = paramReferralLaunchContext;
    }
    j();
  }
  
  public final void a(ReferralManager.ReferralLaunchContext paramReferralLaunchContext, Contact paramContact)
  {
    g = paramContact;
    a(paramReferralLaunchContext);
  }
  
  public final void a(n paramn)
  {
    Object localObject = b;
    if (localObject == null) {
      return;
    }
    ((x)b).d();
    paramn = a;
    localObject = h();
    a(paramn, (ReferralUrl)localObject);
  }
  
  public final void a(String paramString)
  {
    Object localObject = b;
    if (localObject == null) {
      return;
    }
    localObject = (x)b;
    ((x)localObject).d();
    boolean bool = com.truecaller.common.h.am.b(paramString);
    if (!bool)
    {
      localObject = (x)b;
      ((x)localObject).a(paramString);
    }
  }
  
  public final boolean a(Contact paramContact)
  {
    paramContact = paramContact.r();
    boolean bool;
    if (paramContact != null)
    {
      paramContact = paramContact.a();
    }
    else
    {
      bool = false;
      paramContact = null;
    }
    if (paramContact != null)
    {
      ap localap = s;
      bool = localap.a(paramContact);
      if (!bool) {
        return false;
      }
    }
    return true;
  }
  
  public final void b()
  {
    Object localObject = u;
    boolean bool = ((al)localObject).a();
    if (bool)
    {
      localObject = c;
      String str = "codeRedeemed";
      bool = ((am)localObject).d(str);
      if (!bool)
      {
        localObject = c;
        str = "redeemCode";
        localObject = ((am)localObject).a(str);
        bool = com.truecaller.common.h.am.b((CharSequence)localObject);
        if (!bool)
        {
          localObject = c;
          str = "featureReferralDeeplink";
          bool = ((am)localObject).d(str);
          if (bool) {
            break label85;
          }
        }
      }
      return;
    }
    label85:
    l();
  }
  
  public final void b(ReferralManager.ReferralLaunchContext paramReferralLaunchContext)
  {
    Object localObject1 = ReferralManager.ReferralLaunchContext.AFTER_CALL_PROMO;
    if (paramReferralLaunchContext == localObject1)
    {
      paramReferralLaunchContext = c;
      localObject1 = w;
      Object localObject2 = ReferralManager.ReferralLaunchContext.AFTER_CALL_PROMO;
      localObject1 = (String)((HashMap)localObject1).get(localObject2);
      boolean bool1 = paramReferralLaunchContext.d((String)localObject1);
      if (bool1)
      {
        paramReferralLaunchContext = c.a("referralCode");
        localObject1 = c.a("referralLink");
        int i1 = 1;
        localObject2 = new String[i1];
        Object localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>("prepareForReferralIfAvailable:: Preparing referral link and contacts to future use. Ref link: ");
        ((StringBuilder)localObject3).append((String)localObject1);
        String str = " code: ";
        ((StringBuilder)localObject3).append(str);
        ((StringBuilder)localObject3).append(paramReferralLaunchContext);
        localObject3 = ((StringBuilder)localObject3).toString();
        localObject2[0] = localObject3;
        boolean bool2 = com.truecaller.common.h.am.b((CharSequence)localObject1);
        if (!bool2)
        {
          bool1 = com.truecaller.common.h.am.b(paramReferralLaunchContext);
          if (!bool1)
          {
            f();
            return;
          }
        }
        paramReferralLaunchContext = d;
        localObject1 = new com/truecaller/referral/aj$1;
        ((aj.1)localObject1).<init>(this);
        paramReferralLaunchContext.a((o.a)localObject1);
        return;
      }
    }
    new String[1][0] = "prepareForReferralIfAvailable:: Conditions not met, returning!";
  }
  
  public final void b(String paramString)
  {
    a = paramString;
    paramString = ReferralManager.ReferralLaunchContext.DEEP_LINK;
    a(paramString);
  }
  
  public final boolean b(Contact paramContact)
  {
    ap localap = s;
    boolean bool1 = localap.a(paramContact);
    if (bool1)
    {
      boolean bool2 = a(paramContact);
      if (!bool2) {
        return true;
      }
    }
    return false;
  }
  
  public final void c()
  {
    Object localObject1 = b;
    Object localObject2 = new String[0];
    AssertionUtil.isNotNull(localObject1, (String[])localObject2);
    boolean bool = g();
    if (bool)
    {
      localObject1 = aj.a.a;
      e = ((aj.a)localObject1);
      d.a(this);
      return;
    }
    localObject1 = (x)b;
    localObject2 = c.a("referralCode");
    ReferralUrl localReferralUrl = h();
    ReferralManager.ReferralLaunchContext localReferralLaunchContext = f;
    String str = a;
    ((x)localObject1).a((String)localObject2, localReferralUrl, localReferralLaunchContext, str);
  }
  
  public final void c(String paramString)
  {
    Object localObject1 = b;
    Object localObject2 = new String[0];
    AssertionUtil.isNotNull(localObject1, (String[])localObject2);
    localObject1 = (x)b;
    localObject2 = ReferralManager.ReferralLaunchContext.PUSH_NOTIFICATION;
    ((x)localObject1).a(paramString, (ReferralManager.ReferralLaunchContext)localObject2);
  }
  
  public final boolean c(ReferralManager.ReferralLaunchContext paramReferralLaunchContext)
  {
    return d(paramReferralLaunchContext);
  }
  
  final boolean d(ReferralManager.ReferralLaunchContext paramReferralLaunchContext)
  {
    String str1 = (String)w.get(paramReferralLaunchContext);
    int i1 = 1;
    Object localObject1 = new String[i1];
    String str2 = String.valueOf(str1);
    Object localObject2 = "No feature flag defined for ReferralLaunchContext: ".concat(str2);
    str2 = null;
    localObject1[0] = localObject2;
    AssertionUtil.isNotNull(str1, (String[])localObject1);
    localObject1 = u;
    boolean bool1 = ((al)localObject1).a();
    if (bool1)
    {
      localObject1 = c;
      bool1 = ((am)localObject1).d(str1);
      if (bool1)
      {
        localObject1 = (com.truecaller.common.f.c)l.get();
        bool1 = ((com.truecaller.common.f.c)localObject1).d();
        if (!bool1)
        {
          bool1 = true;
          break label132;
        }
      }
    }
    bool1 = false;
    localObject1 = null;
    label132:
    localObject2 = ReferralManager.ReferralLaunchContext.AFTER_CALL_PROMO;
    long l1;
    Object localObject3;
    if (paramReferralLaunchContext == localObject2)
    {
      l1 = c.b("referralAfterCallPromoLastShown");
      localObject2 = c;
      String str3 = "GOOGLE_REVIEW_ASK_TIMESTAMP";
      long l2 = ((am)localObject2).b(str3);
      l1 = Math.max(l1, l2);
      localObject2 = new org/a/a/b;
      ((org.a.a.b)localObject2).<init>(l1);
      localObject3 = TimeUnit.DAYS;
      com.truecaller.featuretoggles.f localf = v.ai();
      int i2 = 5;
      int i3 = localf.a(i2);
      long l3 = i3;
      l1 = ((TimeUnit)localObject3).toMillis(l3);
      localObject2 = ((org.a.a.b)localObject2).b(l1);
      l1 = org.a.a.e.a();
      boolean bool2 = ((org.a.a.a.c)localObject2).d(l1);
      if (bool2)
      {
        localObject2 = c;
        localObject3 = "smsReferralPrefetchBatch";
        localObject2 = ((am)localObject2).a((String)localObject3);
        bool2 = com.truecaller.common.h.am.b((CharSequence)localObject2);
        if (!bool2)
        {
          localObject2 = c;
          localObject3 = "smsReferralSentTo";
          localObject2 = ((am)localObject2).a((String)localObject3);
          bool2 = com.truecaller.common.h.am.b((CharSequence)localObject2);
          if (bool2)
          {
            bool2 = true;
            break label350;
          }
        }
      }
      bool2 = false;
      localObject2 = null;
      label350:
      bool1 &= bool2;
    }
    localObject2 = ReferralManager.ReferralLaunchContext.PUSH_NOTIFICATION;
    if (paramReferralLaunchContext != localObject2)
    {
      localObject2 = ReferralManager.ReferralLaunchContext.HOME_SCREEN;
      if (paramReferralLaunchContext != localObject2)
      {
        localObject2 = ReferralManager.ReferralLaunchContext.NAVIGATION_DRAWER;
        if (paramReferralLaunchContext != localObject2)
        {
          localObject2 = ReferralManager.ReferralLaunchContext.DEEP_LINK;
          if (paramReferralLaunchContext != localObject2)
          {
            localObject2 = ReferralManager.ReferralLaunchContext.SEARCH_SCREEN_PROMO;
            if (paramReferralLaunchContext != localObject2)
            {
              paramReferralLaunchContext = new org/a/a/b;
              localObject2 = k;
              localObject3 = "KeyCallLogPromoDisabledUntil";
              l1 = ((com.truecaller.f.a)localObject2).b((String)localObject3);
              paramReferralLaunchContext.<init>(l1);
              l1 = org.a.a.e.a();
              boolean bool3 = paramReferralLaunchContext.d(l1);
              bool1 &= bool3;
            }
          }
        }
      }
    }
    paramReferralLaunchContext = new String[i1];
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(str1);
    localStringBuilder.append(" enabled?  ");
    localStringBuilder.append(bool1);
    str1 = localStringBuilder.toString();
    paramReferralLaunchContext[0] = str1;
    return bool1;
  }
  
  public final void e()
  {
    String[] arrayOfString = am.a;
    int i1 = arrayOfString.length;
    int i2 = 0;
    while (i2 < i1)
    {
      String str = arrayOfString[i2];
      am localam = c;
      localam.f(str);
      i2 += 1;
    }
  }
  
  final void f()
  {
    Object localObject = c;
    String str = "smsReferralPrefetchBatch";
    localObject = ((am)localObject).a(str);
    boolean bool = com.truecaller.common.h.am.b((CharSequence)localObject);
    if (bool)
    {
      localObject = (q)((com.truecaller.androidactors.f)m.get()).a();
      ((q)localObject).b();
    }
  }
  
  final boolean g()
  {
    return com.truecaller.common.h.am.b(c.a("referralLink"));
  }
  
  final ReferralUrl h()
  {
    ReferralUrl localReferralUrl = ReferralUrl.a(c.a("referralLink"));
    ReferralManager.ReferralLaunchContext localReferralLaunchContext = f;
    e = localReferralLaunchContext;
    return localReferralUrl;
  }
  
  public final void y_()
  {
    Object localObject = h;
    if (localObject != null)
    {
      ((com.truecaller.androidactors.a)localObject).a();
      localObject = null;
      h = null;
    }
    localObject = b;
    if (localObject != null)
    {
      ((x)b).d();
      ((x)b).f();
      localObject = (x)b;
      ((x)localObject).g();
    }
    super.y_();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.referral.aj
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */