package com.truecaller.referral;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.truecaller.log.AssertionUtil;
import java.util.concurrent.Executor;

final class ak
{
  private final am a;
  private final al b;
  
  ak(am paramam, al paramal)
  {
    a = paramam;
    b = paramal;
  }
  
  static void a(Activity paramActivity, com.truecaller.util.p paramp)
  {
    try
    {
      Object localObject1 = com.google.firebase.dynamiclinks.a.a();
      Object localObject2 = paramActivity.getIntent();
      localObject1 = ((com.google.firebase.dynamiclinks.a)localObject1).a((Intent)localObject2);
      localObject2 = new com/truecaller/referral/-$$Lambda$ak$tyC8-3TGLFzBGTKiSRFjePLCOEs;
      ((-..Lambda.ak.tyC8-3TGLFzBGTKiSRFjePLCOEs)localObject2).<init>(paramp);
      ((Task)localObject1).a(paramActivity, (OnSuccessListener)localObject2);
      return;
    }
    catch (Exception localException)
    {
      AssertionUtil.reportThrowableButNeverCrash(localException;
    }
  }
  
  static void a(Context paramContext, com.truecaller.util.p paramp)
  {
    -..Lambda.ak.Mr1vNHNOqINJ0GPi99FLGkuHIO4 localMr1vNHNOqINJ0GPi99FLGkuHIO4 = new com/truecaller/referral/-$$Lambda$ak$Mr1vNHNOqINJ0GPi99FLGkuHIO4;
    localMr1vNHNOqINJ0GPi99FLGkuHIO4.<init>(paramp);
    com.facebook.applinks.a.a(paramContext, localMr1vNHNOqINJ0GPi99FLGkuHIO4);
  }
  
  final void a()
  {
    p localp = new com/truecaller/referral/p;
    Object localObject1 = a;
    Object localObject2 = b;
    localp.<init>((am)localObject1, (al)localObject2);
    localObject1 = AsyncTask.THREAD_POOL_EXECUTOR;
    localObject2 = new Void[0];
    localp.executeOnExecutor((Executor)localObject1, (Object[])localObject2);
  }
  
  final void a(o.a parama)
  {
    o localo = new com/truecaller/referral/o;
    Object localObject = a;
    al localal = b;
    localo.<init>((am)localObject, localal, parama);
    parama = AsyncTask.THREAD_POOL_EXECUTOR;
    localObject = new Void[0];
    localo.executeOnExecutor(parama, (Object[])localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.referral.ak
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */