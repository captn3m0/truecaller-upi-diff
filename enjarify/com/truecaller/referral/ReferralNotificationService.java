package com.truecaller.referral;

import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.z.c;
import android.support.v4.app.z.d;
import android.support.v4.app.z.g;
import android.text.TextUtils;
import com.truecaller.TrueApp;
import com.truecaller.analytics.e.a;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.common.f.c;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.f;
import com.truecaller.old.data.entity.Notification;

public class ReferralNotificationService
  extends IntentService
{
  public ReferralNotificationService()
  {
    super("ReferralNotificationService");
  }
  
  private PendingIntent a(String paramString)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("com.truecaller.intent.action.REFERRAL_REFER_MORE_USERS", null, this, ReferralNotificationService.class);
    localIntent.putExtra("refer_mode", paramString);
    return PendingIntent.getService(this, 0, localIntent, 134217728);
  }
  
  private PendingIntent a(String paramString1, String paramString2, String paramString3, int paramInt)
  {
    int i = paramString1.hashCode();
    int m = -1154529449;
    int n = 2;
    int i1 = 1;
    String str1;
    boolean bool2;
    if (i != m)
    {
      m = -722568161;
      if (i != m)
      {
        m = 106940687;
        if (i == m)
        {
          str1 = "promo";
          boolean bool1 = paramString1.equals(str1);
          if (bool1)
          {
            int j = 2;
            break label123;
          }
        }
      }
      else
      {
        str1 = "referrer";
        bool2 = paramString1.equals(str1);
        if (bool2)
        {
          bool2 = false;
          str1 = null;
          break label123;
        }
      }
    }
    else
    {
      str1 = "joiner";
      bool2 = paramString1.equals(str1);
      if (bool2)
      {
        bool2 = true;
        break label123;
      }
    }
    int k = -1;
    label123:
    m = 2131755063;
    int i2 = 3;
    Object[] arrayOfObject;
    switch (k)
    {
    default: 
      paramString3 = paramString2;
      bool3 = false;
      paramString2 = null;
      break;
    case 2: 
      String str2 = "com.truecaller.intent.action.REFERRAL_REFER_MORE_USERS";
      paramString3 = paramString2;
      paramString2 = str2;
      break;
    case 1: 
      paramString2 = "com.truecaller.intent.action.REFERRAL_PREMIUM_GRANTED";
      k = 2131888650;
      arrayOfObject = new Object[i2];
      arrayOfObject[0] = paramString3;
      paramString3 = Integer.valueOf(paramInt);
      arrayOfObject[i1] = paramString3;
      paramString3 = getResources().getQuantityString(m, paramInt);
      arrayOfObject[n] = paramString3;
      paramString3 = getString(k, arrayOfObject);
      break;
    case 0: 
      paramString2 = "com.truecaller.intent.action.REFERRAL_PREMIUM_GRANTED";
      k = 2131888651;
      arrayOfObject = new Object[i2];
      arrayOfObject[0] = paramString3;
      paramString3 = Integer.valueOf(paramInt);
      arrayOfObject[i1] = paramString3;
      paramString3 = getResources().getQuantityString(m, paramInt);
      arrayOfObject[n] = paramString3;
      paramString3 = getString(k, arrayOfObject);
    }
    if (paramString2 == null)
    {
      paramString2 = new java/lang/IllegalArgumentException;
      paramString3 = new java/lang/StringBuilder;
      paramString3.<init>("ReferMode ");
      paramString3.append(paramString1);
      paramString3.append(" not handled.");
      paramString1 = paramString3.toString();
      paramString2.<init>(paramString1);
      AssertionUtil.reportThrowableButNeverCrash(paramString2);
      return null;
    }
    paramString1 = new android/content/Intent;
    paramString1.<init>(paramString2, null, this, ReferralNotificationService.class);
    String str3 = "text";
    paramString1 = paramString1.putExtra(str3, paramString3);
    paramString3 = "com.truecaller.intent.action.REFERRAL_REFER_MORE_USERS";
    boolean bool3 = com.truecaller.common.h.am.a(paramString2, paramString3);
    if (bool3)
    {
      paramString2 = "refer_mode";
      paramString3 = "promo";
      paramString1.putExtra(paramString2, paramString3);
    }
    return PendingIntent.getService(this, 0, paramString1, 134217728);
  }
  
  private String a(String paramString, Intent paramIntent)
  {
    int i = paramString.hashCode();
    int j = -1154529449;
    String str;
    boolean bool2;
    if (i != j)
    {
      j = -722568161;
      if (i != j)
      {
        j = 106940687;
        if (i == j)
        {
          str = "promo";
          boolean bool1 = paramString.equals(str);
          if (bool1)
          {
            int k = 2;
            break label112;
          }
        }
      }
      else
      {
        str = "referrer";
        bool2 = paramString.equals(str);
        if (bool2)
        {
          bool2 = false;
          paramString = null;
          break label112;
        }
      }
    }
    else
    {
      str = "joiner";
      bool2 = paramString.equals(str);
      if (bool2)
      {
        bool2 = true;
        break label112;
      }
    }
    int m = -1;
    switch (m)
    {
    default: 
      m = 0;
      paramString = null;
      break;
    case 2: 
      paramString = paramIntent.getStringExtra("title");
      break;
    case 1: 
      m = 2131888648;
      paramString = getString(m);
      break;
    case 0: 
      label112:
      m = 2131888649;
      paramString = getString(m);
    }
    return paramString;
  }
  
  private String a(String paramString1, Intent paramIntent, String paramString2, int paramInt)
  {
    int i = paramString1.hashCode();
    int j = -1154529449;
    int k = 2;
    int m = 1;
    String str;
    boolean bool2;
    if (i != j)
    {
      j = -722568161;
      if (i != j)
      {
        j = 106940687;
        if (i == j)
        {
          str = "promo";
          boolean bool1 = paramString1.equals(str);
          if (bool1)
          {
            int n = 2;
            break label122;
          }
        }
      }
      else
      {
        str = "referrer";
        bool2 = paramString1.equals(str);
        if (bool2)
        {
          bool2 = false;
          paramString1 = null;
          break label122;
        }
      }
    }
    else
    {
      str = "joiner";
      bool2 = paramString1.equals(str);
      if (bool2)
      {
        bool2 = true;
        break label122;
      }
    }
    int i1 = -1;
    switch (i1)
    {
    default: 
      i1 = 0;
      paramString1 = null;
      break;
    case 2: 
      paramString1 = paramIntent.getStringExtra("text");
      break;
    case 1: 
      i1 = 2131888646;
      int i2 = 3;
      paramIntent = new Object[i2];
      paramIntent[0] = paramString2;
      paramString2 = Integer.valueOf(paramInt);
      paramIntent[m] = paramString2;
      paramString2 = getResources();
      i = 2131755063;
      paramString2 = paramString2.getQuantityString(i, paramInt);
      paramIntent[k] = paramString2;
      paramString1 = getString(i1, paramIntent);
      break;
    case 0: 
      label122:
      i1 = 2131888647;
      paramIntent = new Object[m];
      paramIntent[0] = paramString2;
      paramString1 = getString(i1, paramIntent);
    }
    return paramString1;
  }
  
  public static void a(Context paramContext, Notification paramNotification)
  {
    int i = 1;
    Object localObject1 = new String[i];
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("onNotificationReceived() called with: context = [");
    ((StringBuilder)localObject2).append(paramContext);
    ((StringBuilder)localObject2).append("], notification = [");
    ((StringBuilder)localObject2).append(paramNotification);
    ((StringBuilder)localObject2).append("]");
    localObject2 = ((StringBuilder)localObject2).toString();
    localObject1[0] = localObject2;
    localObject1 = new android/content/Intent;
    ((Intent)localObject1).<init>(paramContext, ReferralNotificationService.class);
    localObject2 = paramNotification.a("r");
    boolean bool1 = com.truecaller.common.h.am.b((CharSequence)localObject2);
    if (!bool1)
    {
      int j = -1;
      int m = ((String)localObject2).hashCode();
      int n = -1154529449;
      String str1;
      if (m != n)
      {
        n = -759238347;
        boolean bool4;
        if (m != n)
        {
          n = -722568161;
          if (m != n)
          {
            n = 106940687;
            if (m == n)
            {
              str1 = "promo";
              boolean bool3 = ((String)localObject2).equals(str1);
              if (bool3)
              {
                int i1 = 2;
                break label251;
              }
            }
          }
          else
          {
            str1 = "referrer";
            bool4 = ((String)localObject2).equals(str1);
            if (bool4)
            {
              bool4 = false;
              localObject2 = null;
              break label251;
            }
          }
        }
        else
        {
          str1 = "clearCache";
          bool4 = ((String)localObject2).equals(str1);
          if (bool4)
          {
            int i2 = 3;
            break label251;
          }
        }
      }
      else
      {
        str1 = "joiner";
        boolean bool5 = ((String)localObject2).equals(str1);
        if (bool5)
        {
          bool5 = true;
          break label251;
        }
      }
      i3 = -1;
      switch (i3)
      {
      default: 
        break;
      case 3: 
        localObject2 = "com.truecaller.intent.action.ACTION_CLEAR_CACHE_NOTIFICATION_RECEIVED";
        break;
      case 2: 
        localObject2 = "com.truecaller.intent.action.REFERRAL_REFERRER_PROMO_NOTIFICATION_RECEIVED";
        break;
      case 1: 
        localObject2 = "com.truecaller.intent.action.REFERRAL_JOINER_NOTIFICATION_RECEIVED";
        break;
      case 0: 
        label251:
        localObject2 = "com.truecaller.intent.action.REFERRAL_REFERRER_NOTIFICATION_RECEIVED";
        break;
      }
    }
    int i3 = 0;
    localObject2 = null;
    boolean bool2 = com.truecaller.common.h.am.b((CharSequence)localObject2);
    if (bool2)
    {
      paramContext = new java/lang/IllegalArgumentException;
      paramContext.<init>("Role is not present in referral notification");
      AssertionUtil.reportThrowableButNeverCrash(paramContext);
      return;
    }
    ((Intent)localObject1).setAction((String)localObject2);
    String str2 = paramNotification.a(paramContext);
    ((Intent)localObject1).putExtra("title", str2);
    str2 = paramNotification.b(paramContext);
    ((Intent)localObject1).putExtra("text", str2);
    str2 = paramNotification.a("d");
    ((Intent)localObject1).putExtra("days", str2);
    paramNotification = paramNotification.a("f");
    ((Intent)localObject1).putExtra("name", paramNotification);
    paramNotification = ((Intent)localObject1).getExtras();
    String[] arrayOfString = new String[i];
    localObject2 = new java/lang/StringBuilder;
    str2 = "DumpIntentExtras - intent extras size: ";
    ((StringBuilder)localObject2).<init>(str2);
    int k;
    if (paramNotification == null)
    {
      bool2 = false;
      str2 = null;
    }
    else
    {
      k = paramNotification.size();
    }
    ((StringBuilder)localObject2).append(k);
    localObject2 = ((StringBuilder)localObject2).toString();
    arrayOfString[0] = localObject2;
    f.a(paramNotification);
    paramContext.startService((Intent)localObject1);
  }
  
  private void a(Intent paramIntent, String paramString)
  {
    Object localObject1 = ((bk)getApplicationContext()).a();
    Object localObject2 = paramIntent.getStringExtra("name");
    String str = paramIntent.getStringExtra("days");
    int i = 7;
    int j = org.c.a.a.a.b.a.a(str, i);
    Object localObject3 = a(paramString, paramIntent);
    paramIntent = a(paramString, paramIntent, (String)localObject2, j);
    boolean bool1 = TextUtils.isEmpty(paramIntent);
    if (!bool1)
    {
      bool1 = TextUtils.isEmpty((CharSequence)localObject3);
      if (!bool1)
      {
        Object localObject4 = b(paramString);
        boolean bool2 = com.truecaller.common.h.am.b((CharSequence)localObject4);
        if (bool2) {
          return;
        }
        Object localObject5 = ((bp)localObject1).c();
        Object localObject6 = new com/truecaller/analytics/e$a;
        ((e.a)localObject6).<init>((String)localObject4);
        localObject4 = ((e.a)localObject6).a();
        ((com.truecaller.analytics.b)localObject5).a((com.truecaller.analytics.e)localObject4);
        localObject4 = "referrer";
        bool1 = com.truecaller.common.h.am.a(paramString, (CharSequence)localObject4);
        int k;
        if (bool1) {
          k = 2131888659;
        } else {
          k = 2131888642;
        }
        localObject4 = getString(k);
        localObject5 = ((bp)localObject1).aC();
        localObject6 = new android/support/v4/app/z$d;
        localObject5 = ((com.truecaller.notificationchannels.e)localObject5).a();
        ((z.d)localObject6).<init>(this, (String)localObject5);
        localObject3 = ((z.d)localObject6).a((CharSequence)localObject3).b(paramIntent);
        localObject5 = new android/support/v4/app/z$c;
        ((z.c)localObject5).<init>();
        localObject5 = ((z.c)localObject5).b(paramIntent);
        localObject3 = ((z.d)localObject3).a((z.g)localObject5);
        int m = android.support.v4.content.b.c(this, 2131100594);
        C = m;
        localObject3 = ((z.d)localObject3).c(-1);
        m = 2131234787;
        localObject3 = ((z.d)localObject3).a(m);
        paramIntent = a(paramString, paramIntent, (String)localObject2, j);
        f = paramIntent;
        localObject2 = a(paramString);
        paramIntent = ((z.d)localObject3).a(0, (CharSequence)localObject4, (PendingIntent)localObject2);
        int n = 16;
        paramIntent.d(n);
        localObject2 = "referrer";
        boolean bool3 = com.truecaller.common.h.am.a(paramString, (CharSequence)localObject2);
        int i1;
        if (bool3) {
          i1 = 2131364096;
        } else {
          i1 = 2131364095;
        }
        localObject1 = ((bp)localObject1).W();
        paramIntent = paramIntent.h();
        ((com.truecaller.notifications.a)localObject1).a(i1, paramIntent, "notificationReferral");
        return;
      }
    }
  }
  
  private static String b(String paramString)
  {
    int i = paramString.hashCode();
    int j = -1154529449;
    String str;
    boolean bool2;
    if (i != j)
    {
      j = -722568161;
      if (i != j)
      {
        j = 106940687;
        if (i == j)
        {
          str = "promo";
          boolean bool1 = paramString.equals(str);
          if (bool1)
          {
            int k = 2;
            break label100;
          }
        }
      }
      else
      {
        str = "referrer";
        bool2 = paramString.equals(str);
        if (bool2)
        {
          bool2 = false;
          paramString = null;
          break label100;
        }
      }
    }
    else
    {
      str = "joiner";
      bool2 = paramString.equals(str);
      if (bool2)
      {
        bool2 = true;
        break label100;
      }
    }
    int m = -1;
    switch (m)
    {
    default: 
      return null;
    case 2: 
      return "ANDROID_Ref_NotifRecdPromo";
    case 1: 
      label100:
      return "ANDROID_Ref_NotifRecdJoiner";
    }
    return "ANDROID_Ref_NotifRecdReferrer";
  }
  
  protected void onHandleIntent(Intent paramIntent)
  {
    if (paramIntent == null) {
      return;
    }
    Object localObject = paramIntent.getAction();
    boolean bool1 = TextUtils.isEmpty((CharSequence)localObject);
    if (bool1) {
      return;
    }
    bp localbp = ((bk)getApplicationContext()).a();
    int i = -1;
    int j = ((String)localObject).hashCode();
    String str1;
    boolean bool2;
    String str2;
    switch (j)
    {
    default: 
      break;
    case 1987817529: 
      str1 = "com.truecaller.intent.action.REFERRAL_REFERRER_PROMO_NOTIFICATION_RECEIVED";
      bool2 = ((String)localObject).equals(str1);
      if (bool2) {
        i = 2;
      }
      break;
    case 1968287211: 
      str1 = "com.truecaller.intent.action.ACTION_CLEAR_CACHE_NOTIFICATION_RECEIVED";
      bool2 = ((String)localObject).equals(str1);
      if (bool2) {
        i = 5;
      }
      break;
    case 1204829377: 
      str1 = "com.truecaller.intent.action.REFERRAL_JOINER_NOTIFICATION_RECEIVED";
      bool2 = ((String)localObject).equals(str1);
      if (bool2)
      {
        i = 0;
        str2 = null;
      }
      break;
    case 233028233: 
      str1 = "com.truecaller.intent.action.REFERRAL_REFERRER_NOTIFICATION_RECEIVED";
      bool2 = ((String)localObject).equals(str1);
      if (bool2) {
        i = 1;
      }
      break;
    case -397777121: 
      str1 = "com.truecaller.intent.action.REFERRAL_PREMIUM_GRANTED";
      bool2 = ((String)localObject).equals(str1);
      if (bool2) {
        i = 4;
      }
      break;
    case -1908554575: 
      str1 = "com.truecaller.intent.action.REFERRAL_REFER_MORE_USERS";
      bool2 = ((String)localObject).equals(str1);
      if (bool2) {
        i = 3;
      }
      break;
    }
    switch (i)
    {
    default: 
      break;
    case 5: 
      paramIntent = new com/truecaller/referral/an;
      paramIntent.<init>();
      paramIntent.f("referralLink");
      localObject = "referralCode";
      paramIntent.f((String)localObject);
      break;
    case 4: 
      paramIntent = paramIntent.getStringExtra("text");
      localObject = ai.a(this);
      ((Intent)localObject).putExtra("REFERRAL_GRANTED_MESSAGE", paramIntent);
      ((Intent)localObject).putExtra("LAUNCH_MODE", "MODE_REFERRAL_GRANTED_VIEW");
      startActivity((Intent)localObject);
      paramIntent = TrueApp.y().a().c();
      localObject = new com/truecaller/analytics/e$a;
      ((e.a)localObject).<init>("ANDROID_Ref_NotificationClk");
      localObject = ((e.a)localObject).a();
      paramIntent.a((com.truecaller.analytics.e)localObject);
      return;
    case 3: 
      paramIntent = paramIntent.getStringExtra("refer_mode");
      localObject = "referrer";
      bool2 = com.truecaller.common.h.am.a(paramIntent, (CharSequence)localObject);
      if (bool2)
      {
        paramIntent = ai.a(this);
        localObject = "LAUNCH_MODE";
        str2 = "MODE_REFER_MORE_FRIENDS";
        paramIntent.putExtra((String)localObject, str2);
        startActivity(paramIntent);
      }
      else
      {
        localObject = "joiner";
        bool2 = com.truecaller.common.h.am.a(paramIntent, (CharSequence)localObject);
        if (bool2)
        {
          paramIntent = ai.a(this);
          localObject = "LAUNCH_MODE";
          str2 = "MODE_REFERRAL_ON_BOARDING";
          paramIntent.putExtra((String)localObject, str2);
          startActivity(paramIntent);
        }
        else
        {
          localObject = "promo";
          boolean bool3 = com.truecaller.common.h.am.a(paramIntent, (CharSequence)localObject);
          if (bool3)
          {
            paramIntent = ai.a(this);
            localObject = "LAUNCH_MODE";
            str2 = "MODE_SHOW_REFERRAL";
            paramIntent.putExtra((String)localObject, str2);
            startActivity(paramIntent);
          }
        }
      }
      localbp.W().a(2131364096);
      localbp.W().a(2131364095);
      paramIntent = new android/content/Intent;
      paramIntent.<init>("android.intent.action.CLOSE_SYSTEM_DIALOGS");
      sendBroadcast(paramIntent);
      paramIntent = TrueApp.y().a().c();
      localObject = new com/truecaller/analytics/e$a;
      ((e.a)localObject).<init>("ANDROID_Ref_NotifReferMoreClk");
      localObject = ((e.a)localObject).a();
      paramIntent.a((com.truecaller.analytics.e)localObject);
      return;
    case 2: 
      a(paramIntent, "promo");
      return;
    case 1: 
      a(paramIntent, "referrer");
      localbp.ai().c();
      return;
    case 0: 
      a(paramIntent, "joiner");
      localbp.ai().c();
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.referral.ReferralNotificationService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */