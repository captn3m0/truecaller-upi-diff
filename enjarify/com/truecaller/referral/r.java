package com.truecaller.referral;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.v;
import com.truecaller.androidactors.w;
import com.truecaller.data.entity.Contact;

public final class r
  implements q
{
  private final v a;
  
  public r(v paramv)
  {
    a = paramv;
  }
  
  public static boolean a(Class paramClass)
  {
    return q.class.equals(paramClass);
  }
  
  public final w a()
  {
    v localv = a;
    r.a locala = new com/truecaller/referral/r$a;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locala.<init>(locale, (byte)0);
    return w.a(localv, locala);
  }
  
  public final w a(Contact paramContact)
  {
    v localv = a;
    r.c localc = new com/truecaller/referral/r$c;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localc.<init>(locale, paramContact, (byte)0);
    return w.a(localv, localc);
  }
  
  public final void b()
  {
    v localv = a;
    r.b localb = new com/truecaller/referral/r$b;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localb.<init>(locale, (byte)0);
    localv.a(localb);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.referral.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */