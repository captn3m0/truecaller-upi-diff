package com.truecaller.referral;

import android.support.v7.widget.RecyclerView.Adapter;

final class c
  extends RecyclerView.Adapter
{
  private final j a;
  
  c(j paramj)
  {
    a = paramj;
  }
  
  public final int getItemCount()
  {
    return a.a();
  }
  
  public final int getItemViewType(int paramInt)
  {
    return a.a(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.referral.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */