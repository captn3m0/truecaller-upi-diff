package com.truecaller.referral;

import com.google.c.a.g;
import com.google.c.a.k;
import com.google.c.a.k.d;
import com.truecaller.common.h.ab;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.util.al;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

final class ap
{
  final String a;
  private final al b;
  private final am c;
  private final List d;
  private k e;
  
  ap(am paramam, al paramal, String paramString, k paramk)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    d = localArrayList;
    c = paramam;
    b = paramal;
    a = paramString;
    e = paramk;
    paramam = c;
    paramal = "smsReferralSentTo";
    paramam = paramam.a(paramal);
    boolean bool = com.truecaller.common.h.am.b(paramam);
    if (!bool)
    {
      paramam = paramam.split(",");
      paramal = d;
      paramam = Arrays.asList(paramam);
      paramal.addAll(paramam);
    }
  }
  
  private boolean a(String paramString1, String paramString2, String paramString3, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    Object localObject = b;
    boolean bool1 = ((al)localObject).a();
    boolean bool2 = com.truecaller.common.h.am.b(paramString2);
    boolean bool3 = a(paramString2, paramString3);
    String str = a;
    boolean bool4 = com.truecaller.common.h.am.a(paramString3, str);
    int i = 1;
    if ((bool1) && (!paramBoolean1) && (bool4) && (!paramBoolean3) && (bool2) && (bool3) && (!paramBoolean2))
    {
      bool4 = true;
    }
    else
    {
      bool4 = false;
      str = null;
    }
    String[] arrayOfString = new String[i];
    Object[] arrayOfObject = new Object[10];
    arrayOfObject[0] = paramString1;
    paramString1 = Boolean.valueOf(bool4);
    arrayOfObject[i] = paramString1;
    localObject = Boolean.valueOf(bool1);
    arrayOfObject[2] = localObject;
    localObject = Boolean.valueOf(bool2);
    arrayOfObject[3] = localObject;
    arrayOfObject[4] = paramString3;
    paramString3 = a;
    arrayOfObject[5] = paramString3;
    paramString2 = Boolean.valueOf(bool3);
    arrayOfObject[6] = paramString2;
    paramString2 = Boolean.valueOf(paramBoolean1);
    arrayOfObject[7] = paramString2;
    paramString2 = Boolean.valueOf(paramBoolean2);
    arrayOfObject[8] = paramString2;
    paramString2 = Boolean.valueOf(paramBoolean3);
    arrayOfObject[9] = paramString2;
    paramString1 = String.format("'%s' shouldShowReferral: %sHas valid account: %s\nIs numeric: %s\n Calling code: [%s - %s]\n Is mobile: %s\n Is hidden: %s\n Is Spam: %s\n Is Truecaller user: %s", arrayOfObject);
    arrayOfString[0] = paramString1;
    return bool4;
  }
  
  final String a(Number paramNumber)
  {
    paramNumber = paramNumber.l();
    int i = e.c(paramNumber);
    Locale localLocale = Locale.ENGLISH;
    Object[] arrayOfObject = new Object[1];
    paramNumber = Integer.valueOf(i);
    arrayOfObject[0] = paramNumber;
    return String.format(localLocale, "+%d", arrayOfObject);
  }
  
  final boolean a(Contact paramContact)
  {
    Object localObject = c;
    String str1 = "qaForceShowReferral";
    boolean bool1 = ((am)localObject).e(str1);
    int i = 1;
    if (bool1)
    {
      new String[1][0] = "shouldShowReferral:: QA MENU FORCE SHOW REFERRAL ENABLED";
      return i;
    }
    localObject = paramContact.r();
    if (localObject != null)
    {
      String str2 = paramContact.l();
      String str3 = ((Number)localObject).a();
      String str4 = a((Number)localObject);
      localObject = paramContact.p();
      boolean bool2 = ab.a((String)localObject);
      boolean bool3 = paramContact.U();
      boolean bool4 = paramContact.a(i);
      boolean bool5 = a(str2, str3, str4, bool2, bool3, bool4);
      if (bool5) {
        return i;
      }
    }
    return false;
  }
  
  final boolean a(String paramString)
  {
    List localList = d;
    boolean bool = localList.isEmpty();
    int i = 1;
    if (!bool)
    {
      localList = d;
      bool = localList.contains(paramString);
      if (bool)
      {
        bool = true;
        break label45;
      }
    }
    bool = false;
    localList = null;
    label45:
    String[] arrayOfString = new String[i];
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("isAlreadyInvited:: ");
    localStringBuilder.append(paramString);
    localStringBuilder.append(" invited: ");
    localStringBuilder.append(bool);
    paramString = localStringBuilder.toString();
    arrayOfString[0] = paramString;
    return bool;
  }
  
  final boolean a(String paramString1, String paramString2)
  {
    try
    {
      Object localObject = a;
      paramString2 = com.truecaller.common.h.am.e(paramString2, (CharSequence)localObject);
      paramString2 = (String)paramString2;
      localObject = e;
      paramString1 = ((k)localObject).a(paramString1, paramString2);
      paramString2 = e;
      paramString1 = paramString2.b(paramString1);
      paramString2 = k.d.b;
      if (paramString1 != paramString2)
      {
        paramString2 = k.d.c;
        if (paramString1 != paramString2) {
          return false;
        }
      }
      return true;
    }
    catch (g localg) {}
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.referral.ap
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */