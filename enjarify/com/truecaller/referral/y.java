package com.truecaller.referral;

import android.content.Context;
import com.truecaller.abtest.c;
import com.truecaller.analytics.b;
import com.truecaller.androidactors.i;
import com.truecaller.calling.recorder.h;
import com.truecaller.common.account.r;
import com.truecaller.featuretoggles.e;
import com.truecaller.utils.n;
import java.util.Locale;

class y
{
  final int a = 10;
  
  static com.truecaller.androidactors.f a(q paramq, i parami)
  {
    return parami.a(q.class, paramq);
  }
  
  static i a(com.truecaller.androidactors.k paramk)
  {
    return paramk.a("referral");
  }
  
  static aj a(am paramam, com.truecaller.common.g.a parama, ak paramak, n paramn, ap paramap, b paramb, com.truecaller.util.al paramal, e parame, dagger.a parama1, dagger.a parama2, com.truecaller.f.a parama3, dagger.a parama4, h paramh, c paramc, com.truecaller.androidactors.k paramk, com.truecaller.analytics.a.f paramf)
  {
    aj localaj = new com/truecaller/referral/aj;
    i locali = paramk.a();
    localaj.<init>(paramam, parama, paramak, paramn, paramap, paramb, paramal, parame, parama1, parama2, parama3, parama4, paramh, paramc, locali, paramf);
    return localaj;
  }
  
  static ak a(am paramam, al paramal)
  {
    ak localak = new com/truecaller/referral/ak;
    localak.<init>(paramam, paramal);
    return localak;
  }
  
  static am a()
  {
    an localan = new com/truecaller/referral/an;
    localan.<init>();
    return localan;
  }
  
  static ap a(am paramam, com.truecaller.util.al paramal, r paramr, com.google.c.a.k paramk)
  {
    paramr = paramr.a();
    int i = paramk.c(paramr);
    ap localap = new com/truecaller/referral/ap;
    Locale localLocale = Locale.ENGLISH;
    Object[] arrayOfObject = new Object[1];
    paramr = Integer.valueOf(i);
    arrayOfObject[0] = paramr;
    paramr = String.format(localLocale, "+%d", arrayOfObject);
    localap.<init>(paramam, paramal, paramr, paramk);
    return localap;
  }
  
  static q a(d paramd, Context paramContext)
  {
    s locals = new com/truecaller/referral/s;
    locals.<init>(paramd, paramContext);
    return locals;
  }
  
  static al b()
  {
    return al.a();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.referral.y
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */