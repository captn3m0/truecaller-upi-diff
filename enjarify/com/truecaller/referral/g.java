package com.truecaller.referral;

import dagger.a.d;
import javax.inject.Provider;

public final class g
  implements d
{
  private final f a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  private final Provider f;
  private final Provider g;
  private final Provider h;
  private final Provider i;
  private final Provider j;
  private final Provider k;
  
  private g(f paramf, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5, Provider paramProvider6, Provider paramProvider7, Provider paramProvider8, Provider paramProvider9, Provider paramProvider10)
  {
    a = paramf;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
    h = paramProvider7;
    i = paramProvider8;
    j = paramProvider9;
    k = paramProvider10;
  }
  
  public static g a(f paramf, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5, Provider paramProvider6, Provider paramProvider7, Provider paramProvider8, Provider paramProvider9, Provider paramProvider10)
  {
    g localg = new com/truecaller/referral/g;
    localg.<init>(paramf, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7, paramProvider8, paramProvider9, paramProvider10);
    return localg;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.referral.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */