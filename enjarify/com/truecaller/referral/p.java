package com.truecaller.referral;

import android.os.AsyncTask;
import com.truecaller.log.AssertionUtil;
import e.b;
import e.r;
import java.io.IOException;
import java.lang.ref.WeakReference;

final class p
  extends AsyncTask
{
  private final am a;
  private final WeakReference b;
  private final al c;
  
  p(am paramam, al paramal)
  {
    a = paramam;
    c = paramal;
    b = null;
  }
  
  private RedeemCodeResponse a()
  {
    try
    {
      Object localObject = a;
      String str = "redeemCode";
      localObject = ((am)localObject).a(str);
      localObject = al.a((String)localObject);
      localObject = ((b)localObject).c();
      localObject = b;
      return (RedeemCodeResponse)localObject;
    }
    catch (IOException localIOException)
    {
      AssertionUtil.reportThrowableButNeverCrash(localIOException;
    }
    return null;
  }
  
  private static boolean a(String paramString)
  {
    if (paramString == null) {
      return false;
    }
    try
    {
      paramString = RedeemCodeResponse.Status.valueOf(paramString);
      int[] arrayOfInt = p.1.a;
      int i = paramString.ordinal();
      i = arrayOfInt[i];
      switch (i)
      {
      default: 
        break;
      case 1: 
      case 2: 
      case 3: 
      case 4: 
      case 5: 
      case 6: 
      case 7: 
        return true;
      }
    }
    catch (IllegalArgumentException localIllegalArgumentException) {}
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.referral.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */