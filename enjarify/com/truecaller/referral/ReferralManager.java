package com.truecaller.referral;

import android.content.Context;
import android.net.Uri;
import com.truecaller.data.entity.Contact;

public abstract interface ReferralManager
{
  public abstract void a(Context paramContext);
  
  public abstract void a(Uri paramUri);
  
  public abstract void a(ReferralManager.ReferralLaunchContext paramReferralLaunchContext);
  
  public abstract void a(ReferralManager.ReferralLaunchContext paramReferralLaunchContext, Contact paramContact);
  
  public abstract boolean a(Contact paramContact);
  
  public abstract void b();
  
  public abstract void b(ReferralManager.ReferralLaunchContext paramReferralLaunchContext);
  
  public abstract void b(String paramString);
  
  public abstract boolean b(Contact paramContact);
  
  public abstract void c();
  
  public abstract void c(String paramString);
  
  public abstract boolean c(ReferralManager.ReferralLaunchContext paramReferralLaunchContext);
  
  public abstract void e();
}

/* Location:
 * Qualified Name:     com.truecaller.referral.ReferralManager
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */