package com.truecaller.referral;

import dagger.a.d;
import javax.inject.Provider;

public final class ah
  implements d
{
  private final y a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  
  private ah(y paramy, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    a = paramy;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
  }
  
  public static ah a(y paramy, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    ah localah = new com/truecaller/referral/ah;
    localah.<init>(paramy, paramProvider1, paramProvider2, paramProvider3, paramProvider4);
    return localah;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.referral.ah
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */