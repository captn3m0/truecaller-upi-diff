package com.truecaller.referral;

import dagger.a.d;
import javax.inject.Provider;

public final class ac
  implements d
{
  private final y a;
  private final Provider b;
  private final Provider c;
  
  private ac(y paramy, Provider paramProvider1, Provider paramProvider2)
  {
    a = paramy;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static ac a(y paramy, Provider paramProvider1, Provider paramProvider2)
  {
    ac localac = new com/truecaller/referral/ac;
    localac.<init>(paramy, paramProvider1, paramProvider2);
    return localac;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.referral.ac
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */