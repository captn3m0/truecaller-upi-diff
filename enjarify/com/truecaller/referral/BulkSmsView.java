package com.truecaller.referral;

import android.content.Intent;
import android.net.Uri;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.ui.details.DetailsFragment.SourceType;
import java.util.ArrayList;
import java.util.List;

public abstract interface BulkSmsView
  extends aq
{
  public abstract List a(Intent paramIntent);
  
  public abstract void a();
  
  public abstract void a(int paramInt);
  
  public abstract void a(int paramInt, int[] paramArrayOfInt1, String[] paramArrayOfString, int[] paramArrayOfInt2, int[] paramArrayOfInt3, int[] paramArrayOfInt4);
  
  public abstract void a(Uri paramUri, String paramString1, String paramString2);
  
  public abstract void a(Participant paramParticipant, DetailsFragment.SourceType paramSourceType);
  
  public abstract void a(String paramString);
  
  public abstract void a(String paramString, boolean paramBoolean);
  
  public abstract void a(ArrayList paramArrayList);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract void a(boolean paramBoolean, int paramInt);
  
  public abstract void b();
  
  public abstract void b(int paramInt);
  
  public abstract void b(String paramString);
  
  public abstract void b(boolean paramBoolean);
  
  public abstract void c(boolean paramBoolean);
}

/* Location:
 * Qualified Name:     com.truecaller.referral.BulkSmsView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */