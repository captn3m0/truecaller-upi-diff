package com.truecaller.scanner;

import android.os.Handler;
import android.os.Looper;
import com.truecaller.utils.n;
import java.util.List;

public final class c
  extends b
{
  private final n a;
  private final Handler c;
  private c.a d;
  
  c(n paramn)
  {
    Handler localHandler = new android/os/Handler;
    Looper localLooper = Looper.myLooper();
    localHandler.<init>(localLooper);
    c = localHandler;
    a = paramn;
  }
  
  public final void a()
  {
    Object localObject = b;
    if (localObject != null)
    {
      localObject = (e)b;
      ((e)localObject).g();
    }
  }
  
  public final void a(int paramInt, int[] paramArrayOfInt)
  {
    int i = 2;
    if (paramInt == i)
    {
      Object localObject = b;
      if (localObject != null)
      {
        paramInt = paramArrayOfInt.length;
        i = 0;
        Object[] arrayOfObject = null;
        if (paramInt != 0)
        {
          paramInt = paramArrayOfInt[0];
          if (paramInt == 0)
          {
            paramInt = 1;
            break label48;
          }
        }
        paramInt = 0;
        localObject = null;
        label48:
        if (paramInt != 0)
        {
          ((e)b).a();
          return;
        }
        localObject = (e)b;
        paramArrayOfInt = a;
        int j = 2131888732;
        arrayOfObject = new Object[0];
        paramArrayOfInt = paramArrayOfInt.a(j, arrayOfObject);
        ((e)localObject).a(paramArrayOfInt);
        localObject = (e)b;
        ((e)localObject).g();
      }
    }
  }
  
  public final void a(List paramList)
  {
    Object localObject = b;
    if (localObject != null)
    {
      localObject = new com/truecaller/scanner/c$a;
      e locale = (e)b;
      ((c.a)localObject).<init>(locale, paramList);
      d = ((c.a)localObject);
      paramList = c;
      localObject = d;
      paramList.post((Runnable)localObject);
      paramList = (e)b;
      paramList.f();
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    Object localObject = b;
    if ((localObject != null) && (!paramBoolean))
    {
      e locale = (e)b;
      localObject = new String[] { "android.permission.CAMERA" };
      locale.a((String[])localObject);
    }
  }
  
  public final void b()
  {
    Object localObject1 = b;
    if (localObject1 != null)
    {
      localObject1 = (e)b;
      Object localObject2 = a;
      int i = 2131888734;
      Object[] arrayOfObject = new Object[0];
      localObject2 = ((n)localObject2).a(i, arrayOfObject);
      ((e)localObject1).a((String)localObject2);
      localObject1 = (e)b;
      ((e)localObject1).g();
    }
  }
  
  public final void y_()
  {
    super.y_();
    c.a locala = d;
    if (locala != null)
    {
      Handler localHandler = c;
      localHandler.removeCallbacks(locala);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.scanner.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */