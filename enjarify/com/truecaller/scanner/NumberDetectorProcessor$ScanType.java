package com.truecaller.scanner;

public enum NumberDetectorProcessor$ScanType
{
  static
  {
    Object localObject = new com/truecaller/scanner/NumberDetectorProcessor$ScanType;
    ((ScanType)localObject).<init>("SCAN_PHONE", 0);
    SCAN_PHONE = (ScanType)localObject;
    localObject = new com/truecaller/scanner/NumberDetectorProcessor$ScanType;
    int i = 1;
    ((ScanType)localObject).<init>("SCAN_VPA", i);
    SCAN_VPA = (ScanType)localObject;
    localObject = new com/truecaller/scanner/NumberDetectorProcessor$ScanType;
    int j = 2;
    ((ScanType)localObject).<init>("SCAN_PAY", j);
    SCAN_PAY = (ScanType)localObject;
    localObject = new ScanType[3];
    ScanType localScanType = SCAN_PHONE;
    localObject[0] = localScanType;
    localScanType = SCAN_VPA;
    localObject[i] = localScanType;
    localScanType = SCAN_PAY;
    localObject[j] = localScanType;
    $VALUES = (ScanType[])localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.scanner.NumberDetectorProcessor.ScanType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */