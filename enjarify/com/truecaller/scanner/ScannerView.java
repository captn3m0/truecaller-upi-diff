package com.truecaller.scanner;

import android.content.Context;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.common.images.Size;
import com.google.android.gms.vision.CameraSource;
import com.truecaller.log.AssertionUtil;
import java.io.IOException;

public class ScannerView
  extends ViewGroup
{
  SurfaceView a;
  boolean b = false;
  boolean c = false;
  CameraSource d;
  ScannerView.a e;
  
  public ScannerView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    Object localObject = new android/view/SurfaceView;
    ((SurfaceView)localObject).<init>(paramContext);
    a = ((SurfaceView)localObject);
    paramContext = a.getHolder();
    localObject = new com/truecaller/scanner/ScannerView$b;
    ((ScannerView.b)localObject).<init>(this, (byte)0);
    paramContext.addCallback((SurfaceHolder.Callback)localObject);
    paramContext = a;
    addView(paramContext);
  }
  
  private void c()
  {
    ScannerView.a locala = e;
    if (locala != null) {
      locala.f();
    }
  }
  
  private void d()
  {
    ScannerView.a locala = e;
    if (locala != null) {
      locala.e();
    }
  }
  
  public final void a()
  {
    c = false;
    b = false;
    Object localObject = d;
    if (localObject != null) {}
    try
    {
      ((CameraSource)localObject).a();
      localObject = new com/truecaller/scanner/-$$Lambda$Pk7zSNOCMU89LsLDl1vL1dykh2U;
      ((-..Lambda.Pk7zSNOCMU89LsLDl1vL1dykh2U)localObject).<init>(this);
      post((Runnable)localObject);
      localObject = null;
      d = null;
      return;
    }
    catch (RuntimeException localRuntimeException)
    {
      for (;;) {}
    }
  }
  
  final void b()
  {
    try
    {
      try
      {
        boolean bool = b;
        if (bool)
        {
          bool = c;
          if (bool)
          {
            CameraSource localCameraSource = d;
            if (localCameraSource != null)
            {
              localCameraSource = d;
              Object localObject = a;
              localObject = ((SurfaceView)localObject).getHolder();
              localCameraSource.a((SurfaceHolder)localObject);
              bool = false;
              localCameraSource = null;
              b = false;
            }
          }
        }
        return;
      }
      catch (RuntimeException localRuntimeException) {}catch (IOException localIOException) {}
      d();
      AssertionUtil.reportThrowableButNeverCrash(localIOException);
      return;
    }
    catch (SecurityException localSecurityException)
    {
      c();
      AssertionUtil.reportThrowableButNeverCrash(localSecurityException);
    }
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    Object localObject = d;
    if (localObject != null)
    {
      localObject = a;
      if (localObject != null)
      {
        i = a;
        paramBoolean = b;
        break label56;
      }
    }
    int i = 320;
    float f1 = 4.48E-43F;
    paramBoolean = true;
    float f2 = 3.36E-43F;
    label56:
    paramInt3 -= paramInt1;
    paramInt4 -= paramInt2;
    float f3 = paramInt3;
    f2 = paramBoolean;
    f3 /= f2;
    float f4 = paramInt4;
    f1 = i;
    f4 /= f1;
    int j = 0;
    boolean bool = f3 < f4;
    if (bool)
    {
      f1 *= f3;
      paramBoolean = (int)f1;
      paramInt1 = (paramBoolean - paramInt4) / 2;
      paramInt4 = paramBoolean;
      paramInt2 = paramInt1;
      paramBoolean = paramInt3;
      paramInt1 = 0;
      f3 = 0.0F;
    }
    else
    {
      f2 *= f4;
      paramBoolean = (int)f2;
      paramInt1 = (paramBoolean - paramInt3) / 2;
      paramInt2 = 0;
      f4 = 0.0F;
    }
    for (;;)
    {
      paramInt3 = getChildCount();
      if (j >= paramInt3) {
        break;
      }
      View localView = getChildAt(j);
      i = paramInt1 * -1;
      int k = paramInt2 * -1;
      int m = paramBoolean - paramInt1;
      int n = paramInt4 - paramInt2;
      localView.layout(i, k, m, n);
      j += 1;
    }
    localObject = a;
    if (localObject != null) {
      b();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.scanner.ScannerView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */