package com.truecaller.scanner;

import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;

final class ScannerView$b
  implements SurfaceHolder.Callback
{
  private ScannerView$b(ScannerView paramScannerView) {}
  
  public final void surfaceChanged(SurfaceHolder paramSurfaceHolder, int paramInt1, int paramInt2, int paramInt3) {}
  
  public final void surfaceCreated(SurfaceHolder paramSurfaceHolder)
  {
    ScannerView.a(a, true);
    ScannerView.a(a);
  }
  
  public final void surfaceDestroyed(SurfaceHolder paramSurfaceHolder)
  {
    ScannerView.a(a, false);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.scanner.ScannerView.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */