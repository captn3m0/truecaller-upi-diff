package com.truecaller.scanner;

import android.os.Handler;
import android.util.SparseArray;
import com.google.android.gms.vision.Detector.Detections;
import com.google.android.gms.vision.Detector.Processor;
import com.google.android.gms.vision.text.TextBlock;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class NumberDetectorProcessor
  implements Detector.Processor
{
  private final NumberDetectorProcessor.a a;
  private final NumberDetectorProcessor.ScanType b;
  private final Handler c;
  private boolean d;
  private Runnable e;
  
  NumberDetectorProcessor(NumberDetectorProcessor.a parama, NumberDetectorProcessor.ScanType paramScanType)
  {
    Object localObject = new android/os/Handler;
    ((Handler)localObject).<init>();
    c = ((Handler)localObject);
    d = false;
    localObject = new com/truecaller/scanner/-$$Lambda$NumberDetectorProcessor$1DXI3l4LiNO9gFuCE_7e2MtYKqM;
    ((-..Lambda.NumberDetectorProcessor.1DXI3l4LiNO9gFuCE_7e2MtYKqM)localObject).<init>(this);
    e = ((Runnable)localObject);
    a = parama;
    b = paramScanType;
  }
  
  public final void a()
  {
    Handler localHandler = c;
    Runnable localRunnable = e;
    localHandler.removeCallbacks(localRunnable);
  }
  
  public final void a(Detector.Detections paramDetections)
  {
    boolean bool = d;
    if (bool) {
      return;
    }
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    paramDetections = a;
    if (paramDetections != null)
    {
      int i = paramDetections.size();
      int j = 0;
      while (j < i)
      {
        Object localObject = (TextBlock)paramDetections.valueAt(j);
        t localt = new com/truecaller/scanner/t;
        localt.<init>((TextBlock)localObject);
        localObject = b;
        localObject = localt.a((NumberDetectorProcessor.ScanType)localObject);
        if (localObject != null) {
          localArrayList.addAll((Collection)localObject);
        }
        j += 1;
      }
      int k = localArrayList.size();
      if (k > 0)
      {
        k = 1;
        d = k;
        paramDetections = c;
        Runnable localRunnable = e;
        long l = 800L;
        paramDetections.postDelayed(localRunnable, l);
        paramDetections = a;
        paramDetections.a(localArrayList);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.scanner.NumberDetectorProcessor
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */