package com.truecaller.scanner.barcode;

import android.content.Intent;
import android.os.Bundle;
import c.g.b.k;
import java.lang.ref.WeakReference;

final class b$a
  implements Runnable
{
  private final WeakReference a;
  private final com.truecaller.scanner.n b;
  private final com.truecaller.utils.n c;
  private final String d;
  
  public b$a(a.b paramb, com.truecaller.scanner.n paramn, com.truecaller.utils.n paramn1, String paramString)
  {
    b = paramn;
    c = paramn1;
    d = paramString;
    paramn = new java/lang/ref/WeakReference;
    paramn.<init>(paramb);
    a = paramn;
  }
  
  public final void run()
  {
    a.b localb = (a.b)a.get();
    if (localb != null)
    {
      Object localObject1 = b;
      Object localObject2 = d;
      boolean bool = ((com.truecaller.scanner.n)localObject1).a((String)localObject2);
      int j = 0;
      localObject2 = null;
      if (!bool)
      {
        localb.b("InvalidQR");
        localObject1 = c;
        localObject2 = new Object[0];
        localObject1 = ((com.truecaller.utils.n)localObject1).a(2131888740, (Object[])localObject2);
        k.a(localObject1, "resourceProvider.getStri…tring.scanner_invalid_qr)");
        localb.a((String)localObject1);
        return;
      }
      localb.b();
      localObject1 = (CharSequence)d;
      int i = ((CharSequence)localObject1).length();
      if (i > 0)
      {
        i = 1;
      }
      else
      {
        i = 0;
        localObject1 = null;
      }
      if (i != 0)
      {
        localObject1 = new android/os/Bundle;
        ((Bundle)localObject1).<init>();
        String str = d;
        ((Bundle)localObject1).putString("extra_barcode_value", str);
        localObject2 = new android/content/Intent;
        ((Intent)localObject2).<init>();
        localObject1 = ((Intent)localObject2).putExtras((Bundle)localObject1);
        j = -1;
        localb.a((Intent)localObject1, j);
      }
      else
      {
        i = 0;
        localObject1 = null;
        localb.a(null, 0);
      }
      localObject1 = "ValidQR";
      localb.b((String)localObject1);
      localb.c();
      localb.d();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.scanner.barcode.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */