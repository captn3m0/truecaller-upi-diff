package com.truecaller.scanner.barcode;

import android.os.Handler;
import android.os.Looper;
import c.g.b.k;
import com.google.android.gms.vision.barcode.Barcode;
import com.truecaller.analytics.e.a;
import com.truecaller.utils.l;

public final class b
  extends a.a
{
  private final Handler a;
  private b.a c;
  private final com.truecaller.utils.n d;
  private final com.truecaller.scanner.n e;
  private final l f;
  private final com.truecaller.analytics.b g;
  
  public b(com.truecaller.utils.n paramn, com.truecaller.scanner.n paramn1, l paraml, com.truecaller.analytics.b paramb)
  {
    d = paramn;
    e = paramn1;
    f = paraml;
    g = paramb;
    paramn = new android/os/Handler;
    paramn1 = Looper.getMainLooper();
    paramn.<init>(paramn1);
    a = paramn;
  }
  
  private final void f()
  {
    a.b localb = (a.b)b;
    if (localb != null)
    {
      Object localObject = d;
      Object[] arrayOfObject = new Object[0];
      localObject = ((com.truecaller.utils.n)localObject).a(2131888733, arrayOfObject);
      k.a(localObject, "resourceProvider.getStri…scanner_CameraRequiredQR)");
      localb.a((String)localObject);
      localb.e();
      return;
    }
  }
  
  public final void a()
  {
    a("CloseCamera");
    a.b localb = (a.b)b;
    if (localb != null)
    {
      localb.e();
      return;
    }
  }
  
  public final void a(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    String str = "permissions";
    k.b(paramArrayOfString, str);
    k.b(paramArrayOfInt, "grantResults");
    paramArrayOfString = (a.b)b;
    if (paramArrayOfString == null) {
      return;
    }
    int i = 2;
    if (paramInt == i)
    {
      paramInt = paramArrayOfInt.length;
      i = 1;
      if (paramInt == 0) {
        paramInt = 1;
      } else {
        paramInt = 0;
      }
      paramInt ^= i;
      if (paramInt != 0)
      {
        paramInt = paramArrayOfInt[0];
        if (paramInt == 0) {}
      }
      else
      {
        i = 0;
        str = null;
      }
      if (i != 0)
      {
        paramArrayOfString.a();
        return;
      }
      f();
    }
  }
  
  public final void a(Barcode paramBarcode)
  {
    k.b(paramBarcode, "barcode");
    Object localObject = (a.b)b;
    if (localObject == null) {
      return;
    }
    b.a locala = new com/truecaller/scanner/barcode/b$a;
    com.truecaller.scanner.n localn = e;
    com.truecaller.utils.n localn1 = d;
    paramBarcode = c;
    k.a(paramBarcode, "barcode.displayValue");
    locala.<init>((a.b)localObject, localn, localn1, paramBarcode);
    c = locala;
    paramBarcode = a;
    localObject = (Runnable)c;
    paramBarcode.post((Runnable)localObject);
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "actionType");
    com.truecaller.analytics.b localb = g;
    e.a locala = new com/truecaller/analytics/e$a;
    locala.<init>("SdkScanner");
    paramString = locala.a("Action", paramString).a("View", "Fullscreen").a();
    k.a(paramString, "AnalyticsEvent.Builder(S…\n                .build()");
    localb.b(paramString);
  }
  
  public final void b()
  {
    f();
  }
  
  public final void c()
  {
    a.b localb = (a.b)b;
    if (localb != null)
    {
      String[] arrayOfString = { "android.permission.CAMERA" };
      localb.a(arrayOfString);
      return;
    }
  }
  
  public final void e()
  {
    Object localObject = f;
    String[] arrayOfString = { "android.permission.CAMERA" };
    boolean bool = ((l)localObject).a(arrayOfString);
    if (bool)
    {
      localObject = (a.b)b;
      if (localObject != null)
      {
        ((a.b)localObject).a();
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.scanner.barcode.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */