package com.truecaller.scanner.barcode;

import com.google.android.gms.vision.barcode.Barcode;
import com.truecaller.bb;
import com.truecaller.scanner.p.b;

public abstract class a$a
  extends bb
  implements p.b
{
  public abstract void a();
  
  public abstract void a(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt);
  
  public abstract void a(Barcode paramBarcode);
  
  public abstract void a(String paramString);
  
  public abstract void e();
}

/* Location:
 * Qualified Name:     com.truecaller.scanner.barcode.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */