package com.truecaller.scanner.barcode;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.Toast;
import c.u;
import com.google.android.gms.vision.barcode.Barcode;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.scanner.NumberDetectorProcessor.ScanType;
import com.truecaller.scanner.ScannerManagerImpl;
import com.truecaller.scanner.ScannerManagerImpl.ScannerType;
import com.truecaller.scanner.p;
import com.truecaller.scanner.p.b;
import com.truecaller.scanner.r;

public final class BarcodeCaptureActivity
  extends AppCompatActivity
  implements View.OnClickListener, a.b, d.a
{
  public a.a a;
  private p b;
  private View c;
  private boolean d;
  
  public final void a()
  {
    p localp = b;
    if (localp == null)
    {
      String str = "scannerManager";
      c.g.b.k.a(str);
    }
    localp.b();
  }
  
  public final void a(Intent paramIntent, int paramInt)
  {
    setResult(paramInt, paramIntent);
    finish();
  }
  
  public final void a(Barcode paramBarcode)
  {
    c.g.b.k.b(paramBarcode, "barcode");
    a.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    locala.a(paramBarcode);
  }
  
  public final void a(String paramString)
  {
    c.g.b.k.b(paramString, "message");
    Object localObject = this;
    localObject = (Context)this;
    paramString = (CharSequence)paramString;
    Toast.makeText((Context)localObject, paramString, 0).show();
  }
  
  public final void a(String[] paramArrayOfString)
  {
    c.g.b.k.b(paramArrayOfString, "permissions");
    android.support.v4.app.a.a((Activity)this, paramArrayOfString, 2);
  }
  
  public final void b()
  {
    View localView = c;
    if (localView == null)
    {
      String str = "rootView";
      c.g.b.k.a(str);
    }
    localView.performHapticFeedback(3);
  }
  
  public final void b(String paramString)
  {
    c.g.b.k.b(paramString, "actionType");
    a.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    locala.a(paramString);
  }
  
  public final void c()
  {
    p localp = b;
    if (localp == null)
    {
      String str = "scannerManager";
      c.g.b.k.a(str);
    }
    localp.c();
  }
  
  public final void d()
  {
    boolean bool = true;
    d = bool;
    p localp = b;
    if (localp == null)
    {
      String str = "scannerManager";
      c.g.b.k.a(str);
    }
    localp.d();
  }
  
  public final void e()
  {
    finish();
  }
  
  public final void onClick(View paramView)
  {
    String str = "view";
    c.g.b.k.b(paramView, str);
    int i = paramView.getId();
    int j = 2131362511;
    if (i == j)
    {
      paramView = a;
      if (paramView == null)
      {
        str = "presenter";
        c.g.b.k.a(str);
      }
      paramView.a();
    }
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    int i = 2131558437;
    setContentView(i);
    paramBundle = getApplication();
    if (paramBundle != null)
    {
      paramBundle = ((bk)paramBundle).a();
      c.g.b.k.a(paramBundle, "(application as GraphHolder).objectsGraph");
      com.truecaller.scanner.a.a().a(paramBundle).a().a(this);
      Object localObject1 = findViewById(2131364934);
      c.g.b.k.a(localObject1, "findViewById(R.id.topLayout)");
      c = ((View)localObject1);
      int j = 2131362511;
      localObject1 = (ImageButton)findViewById(j);
      Object localObject2 = this;
      localObject2 = (View.OnClickListener)this;
      ((ImageButton)localObject1).setOnClickListener((View.OnClickListener)localObject2);
      localObject1 = new com/truecaller/scanner/ScannerManagerImpl;
      Object localObject3 = this;
      localObject3 = (Context)this;
      View localView = c;
      if (localView == null)
      {
        localObject2 = "rootView";
        c.g.b.k.a((String)localObject2);
      }
      NumberDetectorProcessor.ScanType localScanType = NumberDetectorProcessor.ScanType.SCAN_PHONE;
      localObject2 = a;
      if (localObject2 == null)
      {
        localObject4 = "presenter";
        c.g.b.k.a((String)localObject4);
      }
      Object localObject5 = localObject2;
      localObject5 = (p.b)localObject2;
      r localr = paramBundle.ba();
      ScannerManagerImpl.ScannerType localScannerType = ScannerManagerImpl.ScannerType.SCANNER_QR;
      Object localObject4 = localObject1;
      ((ScannerManagerImpl)localObject1).<init>((Context)localObject3, localView, localScanType, null, (p.b)localObject5, localr, localScannerType);
      localObject1 = (p)localObject1;
      b = ((p)localObject1);
      paramBundle = a;
      if (paramBundle == null)
      {
        localObject1 = "presenter";
        c.g.b.k.a((String)localObject1);
      }
      paramBundle.a(this);
      return;
    }
    paramBundle = new c/u;
    paramBundle.<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
    throw paramBundle;
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    a.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    locala.y_();
  }
  
  public final void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    c.g.b.k.b(paramArrayOfString, "permissions");
    c.g.b.k.b(paramArrayOfInt, "grantResults");
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    a.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    locala.a(paramInt, paramArrayOfString, paramArrayOfInt);
  }
  
  public final void onResume()
  {
    super.onResume();
    a.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    locala.e();
  }
  
  public final void onStart()
  {
    super.onStart();
    p localp = b;
    if (localp == null)
    {
      String str = "scannerManager";
      c.g.b.k.a(str);
    }
    localp.a();
  }
  
  public final void onStop()
  {
    super.onStop();
    p localp = b;
    String str;
    if (localp == null)
    {
      str = "scannerManager";
      c.g.b.k.a(str);
    }
    localp.c();
    boolean bool = d;
    if (!bool)
    {
      localp = b;
      if (localp == null)
      {
        str = "scannerManager";
        c.g.b.k.a(str);
      }
      localp.d();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.scanner.barcode.BarcodeCaptureActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */