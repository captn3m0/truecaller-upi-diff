package com.truecaller.scanner;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.view.View;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.internal.vision.zzak;
import com.google.android.gms.internal.vision.zzal;
import com.google.android.gms.internal.vision.zzm;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.CameraSource.Builder;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.Detector.Processor;
import com.google.android.gms.vision.MultiProcessor.Builder;
import com.google.android.gms.vision.MultiProcessor.Factory;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.google.android.gms.vision.barcode.BarcodeDetector.Builder;
import com.google.android.gms.vision.text.TextRecognizer;
import com.google.android.gms.vision.text.TextRecognizer.Builder;
import com.truecaller.log.AssertionUtil;
import com.truecaller.scanner.barcode.d.a;
import com.truecaller.scanner.barcode.e;
import java.util.List;
import java.util.concurrent.Executor;

public final class ScannerManagerImpl
  implements NumberDetectorProcessor.a, ScannerView.a, p
{
  private final Context a;
  private final ScannerView b;
  private final p.a c;
  private final p.b d;
  private final NumberDetectorProcessor e;
  private final BarcodeDetector f;
  private final ScannerManagerImpl.ScannerType g;
  private CameraSource h;
  private final r i;
  
  public ScannerManagerImpl(Context paramContext, View paramView, NumberDetectorProcessor.ScanType paramScanType, p.a parama, p.b paramb, r paramr)
  {
    this(paramContext, paramView, paramScanType, parama, paramb, paramr, localScannerType);
  }
  
  public ScannerManagerImpl(Context paramContext, View paramView, NumberDetectorProcessor.ScanType paramScanType, p.a parama, p.b paramb, r paramr, ScannerManagerImpl.ScannerType paramScannerType)
  {
    a = paramContext;
    paramContext = (ScannerView)paramView.findViewById(2131362419);
    b = paramContext;
    c = parama;
    d = paramb;
    paramContext = new com/truecaller/scanner/NumberDetectorProcessor;
    paramContext.<init>(this, paramScanType);
    e = paramContext;
    i = paramr;
    paramContext = new com/google/android/gms/vision/barcode/BarcodeDetector$Builder;
    paramView = a;
    paramContext.<init>(paramView);
    paramContext = paramContext.b();
    f = paramContext;
    g = paramScannerType;
  }
  
  private void g()
  {
    Object localObject1 = ScannerManagerImpl.1.a;
    Object localObject2 = g;
    int j = ((ScannerManagerImpl.ScannerType)localObject2).ordinal();
    int k = localObject1[j];
    j = 1;
    Object localObject3 = null;
    Object localObject4;
    Object localObject5;
    switch (k)
    {
    default: 
      break;
    case 2: 
      localObject1 = new com/google/android/gms/vision/text/TextRecognizer$Builder;
      localObject4 = a;
      ((TextRecognizer.Builder)localObject1).<init>((Context)localObject4);
      localObject4 = new com/google/android/gms/internal/vision/zzak;
      localObject5 = a;
      localObject1 = b;
      ((zzak)localObject4).<init>((Context)localObject5, (zzal)localObject1);
      localObject1 = new com/google/android/gms/vision/text/TextRecognizer;
      ((TextRecognizer)localObject1).<init>((zzak)localObject4, (byte)0);
      localObject4 = e;
      ((TextRecognizer)localObject1).a((Detector.Processor)localObject4);
      boolean bool2 = ((TextRecognizer)localObject1).b();
      if (!bool2)
      {
        new String[1][0] = "Detector dependencies are not yet available.";
        localObject4 = new android/content/IntentFilter;
        ((IntentFilter)localObject4).<init>("android.intent.action.DEVICE_STORAGE_LOW");
        localObject5 = a;
        localObject3 = ((Context)localObject5).registerReceiver(null, (IntentFilter)localObject4);
        if (localObject3 == null)
        {
          j = 0;
          localObject2 = null;
        }
        if (j != 0)
        {
          localObject2 = new String[] { "Low storage" };
          AssertionUtil.reportWithSummary("ScannerManager", (String[])localObject2);
          i();
          return;
        }
      }
      localObject2 = new com/google/android/gms/vision/CameraSource$Builder;
      localObject3 = a;
      ((CameraSource.Builder)localObject2).<init>((Context)localObject3, (Detector)localObject1);
      localObject1 = ((CameraSource.Builder)localObject2).a(0).a().b().c().d();
      h = ((CameraSource)localObject1);
      break;
    case 1: 
      localObject1 = new com/truecaller/scanner/barcode/e;
      localObject4 = (d.a)a;
      ((e)localObject1).<init>((d.a)localObject4);
      localObject4 = f;
      localObject5 = new com/google/android/gms/vision/MultiProcessor$Builder;
      ((MultiProcessor.Builder)localObject5).<init>((MultiProcessor.Factory)localObject1);
      localObject1 = a;
      ((BarcodeDetector)localObject4).a((Detector.Processor)localObject1);
      localObject1 = f.a;
      boolean bool1 = ((zzm)localObject1).isOperational();
      if (!bool1)
      {
        new String[1][0] = "Detector dependencies are not yet available.";
        localObject1 = new android/content/IntentFilter;
        ((IntentFilter)localObject1).<init>("android.intent.action.DEVICE_STORAGE_LOW");
        localObject4 = a;
        localObject1 = ((Context)localObject4).registerReceiver(null, (IntentFilter)localObject1);
        if (localObject1 == null)
        {
          j = 0;
          localObject2 = null;
        }
        if (j != 0)
        {
          localObject2 = new String[] { "Low storage" };
          AssertionUtil.reportWithSummary("ScannerManager", (String[])localObject2);
          i();
          return;
        }
      }
      localObject1 = new com/google/android/gms/vision/CameraSource$Builder;
      localObject2 = a;
      localObject3 = f;
      ((CameraSource.Builder)localObject1).<init>((Context)localObject2, (Detector)localObject3);
      localObject1 = ((CameraSource.Builder)localObject1).a(0).a().b().c().d();
      h = ((CameraSource)localObject1);
      return;
    }
  }
  
  private void h()
  {
    i.a = false;
    Object localObject1 = GoogleApiAvailability.a();
    Object localObject2 = a;
    int j = ((GoogleApiAvailability)localObject1).a((Context)localObject2);
    Object localObject3;
    if (j != 0)
    {
      localObject2 = GoogleApiAvailability.a();
      localObject3 = (Activity)a;
      int k = 9001;
      localObject1 = ((GoogleApiAvailability)localObject2).a((Activity)localObject3, j, k);
      ((Dialog)localObject1).show();
    }
    localObject1 = h;
    if (localObject1 != null) {
      try
      {
        localObject2 = b;
        int m = ((ScannerView)localObject2).getChildCount();
        if (m == 0)
        {
          localObject3 = a;
          ((ScannerView)localObject2).addView((View)localObject3);
        }
        e = this;
        d = ((CameraSource)localObject1);
        j = 1;
        b = j;
        ((ScannerView)localObject2).b();
        return;
      }
      catch (SecurityException localSecurityException)
      {
        AssertionUtil.reportThrowableButNeverCrash(localSecurityException);
        i();
        return;
      }
    }
    localObject2 = new String[] { "Camera source null" };
    AssertionUtil.reportWithSummary("ScannerManager", (String[])localObject2);
    i();
  }
  
  private void i()
  {
    p.b localb = d;
    if (localb != null) {
      localb.b();
    }
  }
  
  private void j()
  {
    g();
    h();
    i.b = null;
  }
  
  public final void a()
  {
    r localr = i;
    boolean bool = a;
    if (bool)
    {
      g();
      return;
    }
    localr = i;
    -..Lambda.ScannerManagerImpl.VmpMU6rnR7HDBZQam4PqvFNgmDk localVmpMU6rnR7HDBZQam4PqvFNgmDk = new com/truecaller/scanner/-$$Lambda$ScannerManagerImpl$VmpMU6rnR7HDBZQam4PqvFNgmDk;
    localVmpMU6rnR7HDBZQam4PqvFNgmDk.<init>(this);
    b = localVmpMU6rnR7HDBZQam4PqvFNgmDk;
  }
  
  public final void a(List paramList)
  {
    p.a locala = c;
    if (locala != null) {
      locala.a(paramList);
    }
  }
  
  public final void b()
  {
    r localr = i;
    boolean bool = a;
    if (bool) {
      h();
    }
  }
  
  public final void c()
  {
    ScannerView localScannerView = b;
    if (localScannerView != null) {
      c = false;
    }
    i.b = null;
  }
  
  public final void d()
  {
    Object localObject1 = b;
    if (localObject1 != null)
    {
      r.b localb = new com/truecaller/scanner/r$b;
      Object localObject2 = i;
      NumberDetectorProcessor localNumberDetectorProcessor = e;
      localb.<init>((r)localObject2, localNumberDetectorProcessor, (ScannerView)localObject1);
      localObject1 = AsyncTask.THREAD_POOL_EXECUTOR;
      localObject2 = new Void[0];
      localb.executeOnExecutor((Executor)localObject1, (Object[])localObject2);
    }
  }
  
  public final void e()
  {
    i();
  }
  
  public final void f()
  {
    p.b localb = d;
    if (localb != null) {
      localb.c();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.scanner.ScannerManagerImpl
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */