package com.truecaller.scanner;

import android.net.Uri;
import com.google.c.a.k.d;
import com.google.c.a.m.a;
import com.google.gson.o;
import com.truecaller.TrueApp;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e.a;
import com.truecaller.androidactors.f;
import com.truecaller.common.h.aa;
import com.truecaller.common.h.am;
import com.truecaller.common.network.country.CountryListDto.a;
import com.truecaller.data.entity.Contact;
import com.truecaller.network.search.j.b;
import com.truecaller.swish.g.a;
import com.truecaller.tracking.events.ao;
import com.truecaller.tracking.events.ao.a;
import com.truecaller.tracking.events.k.a;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

final class h
  extends g
  implements com.d.b.e, j.b
{
  private final com.truecaller.data.access.c a;
  private final com.truecaller.network.search.j c;
  private String d;
  private String e;
  private final f f;
  private final com.truecaller.swish.g g;
  private final b h;
  private final com.truecaller.featuretoggles.e i;
  private Contact j;
  
  h(com.truecaller.network.search.j paramj, com.truecaller.data.access.c paramc, f paramf, g.a parama, b paramb, com.truecaller.featuretoggles.e parame)
  {
    c = paramj;
    a = paramc;
    paramj = parama.a();
    g = paramj;
    h = paramb;
    i = parame;
    f = paramf;
  }
  
  private void a()
  {
    Object localObject = b;
    if (localObject != null)
    {
      localObject = (j)b;
      ((j)localObject).a();
    }
  }
  
  private void a(Contact paramContact)
  {
    Object localObject1 = b;
    boolean bool3;
    if (localObject1 != null)
    {
      localObject1 = TrueApp.y();
      boolean bool1 = ((TrueApp)localObject1).isTcPayEnabled();
      bool2 = true;
      bool3 = false;
      if (!bool1) {}
    }
    try
    {
      localObject1 = com.google.c.a.k.a();
      Object localObject2 = d;
      str = "IN";
      localObject2 = ((com.google.c.a.k)localObject1).a((CharSequence)localObject2, str);
      localObject1 = ((com.google.c.a.k)localObject1).b((m.a)localObject2);
      int m = b;
      int n;
      if (m != 0)
      {
        n = b;
        m = 91;
        if (n != m) {}
      }
      else
      {
        localObject2 = k.d.h;
        if (localObject1 != localObject2)
        {
          localObject2 = k.d.b;
          if (localObject1 != localObject2) {}
        }
        else
        {
          localObject1 = d;
          int k = ((String)localObject1).length();
          n = 10;
          if (k >= n) {
            bool3 = true;
          }
        }
      }
    }
    catch (com.google.c.a.g localg)
    {
      String str;
      boolean bool4;
      Object localObject3;
      for (;;) {}
    }
    localObject1 = paramContact.t();
    bool4 = am.b((CharSequence)localObject1);
    bool2 ^= bool4;
    bool4 = b();
    if (bool2)
    {
      localObject3 = (j)b;
      str = d;
      ((j)localObject3).a((String)localObject1, str, bool3, bool4);
      e = ((String)localObject1);
    }
    else
    {
      localObject1 = (j)b;
      localObject3 = d;
      ((j)localObject1).a((String)localObject3, bool3, bool4);
    }
    localObject1 = paramContact.v();
    boolean bool2 = am.b((CharSequence)localObject1);
    if (!bool2)
    {
      localObject3 = (j)b;
      localObject1 = Uri.parse((String)localObject1);
      ((j)localObject3).a((Uri)localObject1, this);
    }
    else
    {
      localObject1 = (j)b;
      ((j)localObject1).a();
    }
    if (bool4)
    {
      localObject1 = "Shown";
      f((String)localObject1);
    }
    j = paramContact;
  }
  
  private void a(String paramString, o paramo)
  {
    ae localae = (ae)f.a();
    paramString = com.truecaller.tracking.events.k.b().a(paramString);
    paramo = paramo.toString();
    paramString = paramString.b(paramo).a();
    localae.a(paramString);
  }
  
  private boolean b()
  {
    Object localObject = g.a();
    boolean bool = ((com.truecaller.swish.k)localObject).a();
    if (bool)
    {
      localObject = d;
      if (localObject != null)
      {
        localObject = c((String)localObject);
        if (localObject != null)
        {
          com.truecaller.swish.k localk = g.a();
          localObject = localk.a((String)localObject);
          if (localObject != null) {
            return true;
          }
        }
        return false;
      }
    }
    return false;
  }
  
  private static String c(String paramString)
  {
    String str = TrueApp.y().t();
    boolean bool = am.b(str);
    if (!bool) {
      try
      {
        return aa.a(paramString, str);
      }
      catch (com.google.c.a.g localg)
      {
        return null;
      }
    }
    return null;
  }
  
  private static String d(String paramString)
  {
    String str = c(paramString);
    if (str == null) {
      return paramString;
    }
    return str;
  }
  
  private void e(String paramString)
  {
    o localo = new com/google/gson/o;
    localo.<init>();
    localo.a("SCANDIALOG_Action", paramString);
    a("SCANDIALOG_Clicked", localo);
  }
  
  private void f(String paramString)
  {
    Object localObject1 = h;
    Object localObject2 = new com/truecaller/analytics/e$a;
    ((e.a)localObject2).<init>("Swish");
    String str1 = "scannedDialog";
    localObject2 = ((e.a)localObject2).a("Context", str1);
    String str2 = "Status";
    localObject2 = ((e.a)localObject2).a(str2, paramString).a();
    ((b)localObject1).a((com.truecaller.analytics.e)localObject2);
    localObject1 = "Clicked";
    boolean bool = paramString.equals(localObject1);
    if (bool)
    {
      paramString = new java/util/HashMap;
      paramString.<init>();
      paramString.put("Context", "scannedDialog");
      localObject1 = ao.b();
      localObject2 = com.truecaller.swish.c.a;
      localObject2 = com.truecaller.swish.c.b();
      localObject1 = ((ao.a)localObject1).b((CharSequence)localObject2);
      localObject2 = "Swish_Tapped";
      paramString = ((ao.a)localObject1).a((CharSequence)localObject2).a(paramString).a();
      localObject1 = (ae)f.a();
      ((ae)localObject1).a(paramString);
    }
  }
  
  public final void a(String paramString)
  {
    Object localObject1 = b;
    if (localObject1 != null)
    {
      int k = -1;
      int m = paramString.hashCode();
      Object localObject2;
      switch (m)
      {
      default: 
        break;
      case 1183109472: 
        localObject2 = "ScannedNumberSMS";
        bool2 = paramString.equals(localObject2);
        if (bool2) {
          k = 2;
        }
        break;
      case 1183107247: 
        localObject2 = "ScannedNumberPay";
        bool2 = paramString.equals(localObject2);
        if (bool2) {
          k = 3;
        }
        break;
      case -59593015: 
        localObject2 = "ScannedNumberDetails";
        bool2 = paramString.equals(localObject2);
        if (bool2)
        {
          k = 0;
          localObject1 = null;
        }
        break;
      case -1196854815: 
        localObject2 = "ScannedNumberSwish";
        bool2 = paramString.equals(localObject2);
        if (bool2) {
          k = 4;
        }
        break;
      case -1978768585: 
        localObject2 = "ScannedNumberCall";
        bool2 = paramString.equals(localObject2);
        if (bool2) {
          k = 1;
        }
        break;
      }
      boolean bool2 = false;
      paramString = null;
      String str;
      switch (k)
      {
      default: 
        ((j)b).b();
        paramString = "ScanAgain";
        e(paramString);
        break;
      case 4: 
        paramString = c(d);
        if (paramString != null)
        {
          localObject1 = (j)b;
          localObject2 = j;
          ((j)localObject1).a(paramString, (Contact)localObject2);
          f("Clicked");
          return;
        }
        break;
      case 3: 
        localObject1 = d(d);
        m = ((String)localObject1).length() + -10;
        localObject1 = ((String)localObject1).substring(m);
        str = "";
        localObject1 = ((String)localObject1).replace("+", str);
        localObject2 = j;
        if (localObject2 != null)
        {
          localObject2 = ((Contact)localObject2).t();
          boolean bool1 = am.b((CharSequence)localObject2);
          if (!bool1) {
            paramString = j.t();
          }
        }
        localObject2 = (j)b;
        boolean bool3 = TrueApp.y().isTcPayEnabled();
        ((j)localObject2).a(bool3, (String)localObject1, paramString);
        e("ScannedNumberPay");
        return;
      case 2: 
        paramString = (j)b;
        localObject1 = d;
        paramString.a((String)localObject1);
        e("ScannedNumberSMS");
        return;
      case 1: 
        paramString = (j)b;
        localObject1 = d;
        localObject2 = e;
        paramString.a((String)localObject1, (String)localObject2);
        e("ScannedNumberCall");
        return;
      case 0: 
        localObject1 = d(d);
        localObject2 = com.truecaller.common.h.h.c(d);
        if (localObject2 != null) {
          paramString = c;
        }
        localObject2 = (j)b;
        str = d;
        ((j)localObject2).a(str, (String)localObject1, paramString);
        e("ScannedNumberDetails");
        return;
      }
    }
  }
  
  public final void a(Throwable paramThrowable)
  {
    paramThrowable = b;
    if (paramThrowable != null)
    {
      boolean bool = b();
      j localj = (j)b;
      String str = d;
      localj.a(str, false, bool);
      localj = (j)b;
      localj.a();
      if (bool)
      {
        paramThrowable = "Shown";
        f(paramThrowable);
      }
    }
  }
  
  public final void a(List paramList, String paramString1, String paramString2)
  {
    paramList = (Contact)paramList.get(0);
    a(paramList);
  }
  
  public final void b(String paramString)
  {
    boolean bool = am.b(paramString);
    if (!bool)
    {
      d = paramString;
      Object localObject = a.b(paramString);
      if (localObject != null)
      {
        a((Contact)localObject);
      }
      else
      {
        localObject = c;
        i = paramString;
        paramString = null;
        ((com.truecaller.network.search.j)localObject).a(null, false, false, this);
      }
    }
    paramString = new com/google/gson/o;
    paramString.<init>();
    paramString.a("SCANDIALOG_Action", "ScanShow");
    a("SCANDIALOG_Shown", paramString);
  }
  
  public final void onError()
  {
    a();
  }
  
  public final void onSuccess()
  {
    a();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.scanner.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */