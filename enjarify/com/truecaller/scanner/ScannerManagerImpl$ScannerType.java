package com.truecaller.scanner;

public enum ScannerManagerImpl$ScannerType
{
  static
  {
    Object localObject = new com/truecaller/scanner/ScannerManagerImpl$ScannerType;
    ((ScannerType)localObject).<init>("SCANNER_TEXT", 0);
    SCANNER_TEXT = (ScannerType)localObject;
    localObject = new com/truecaller/scanner/ScannerManagerImpl$ScannerType;
    int i = 1;
    ((ScannerType)localObject).<init>("SCANNER_QR", i);
    SCANNER_QR = (ScannerType)localObject;
    localObject = new ScannerType[2];
    ScannerType localScannerType = SCANNER_TEXT;
    localObject[0] = localScannerType;
    localScannerType = SCANNER_QR;
    localObject[i] = localScannerType;
    $VALUES = (ScannerType[])localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.scanner.ScannerManagerImpl.ScannerType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */