package com.truecaller.scanner;

import android.util.Patterns;
import com.google.android.gms.vision.text.Text;
import com.google.android.gms.vision.text.TextBlock;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

final class t
{
  private static final Pattern a = Patterns.PHONE;
  private static final Pattern b = Pattern.compile("([\\da-zA-Z-._]+@[a-zA-Z\\d]{3,})(?![\\w\\d.])", 2);
  private static final Pattern c = Pattern.compile("[0-9]");
  private final List d;
  
  t(TextBlock paramTextBlock)
  {
    paramTextBlock = paramTextBlock.b();
    d = paramTextBlock;
  }
  
  private static List a(String paramString)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Object localObject = a;
    paramString = ((Pattern)localObject).matcher(paramString);
    for (;;)
    {
      boolean bool = paramString.find();
      if (!bool) {
        break;
      }
      localObject = paramString.group();
      int i = c((String)localObject);
      int j = 7;
      if (i > j) {
        localArrayList.add(localObject);
      }
    }
    return localArrayList;
  }
  
  private static List b(String paramString)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Object localObject = b;
    paramString = ((Pattern)localObject).matcher(paramString);
    for (;;)
    {
      boolean bool = paramString.find();
      if (!bool) {
        break;
      }
      localObject = paramString.group();
      localArrayList.add(localObject);
    }
    return localArrayList;
  }
  
  private static int c(String paramString)
  {
    paramString = c.matcher(paramString);
    int i = 0;
    for (;;)
    {
      boolean bool = paramString.find();
      if (!bool) {
        break;
      }
      i += 1;
    }
    return i;
  }
  
  final List a(NumberDetectorProcessor.ScanType paramScanType)
  {
    Object localObject1 = d;
    if (localObject1 != null)
    {
      int i = ((List)localObject1).size();
      if (i != 0)
      {
        localObject1 = new java/util/ArrayList;
        ((ArrayList)localObject1).<init>();
        Iterator localIterator = d.iterator();
        for (;;)
        {
          boolean bool = localIterator.hasNext();
          if (!bool) {
            break;
          }
          Object localObject2 = (Text)localIterator.next();
          if (localObject2 != null)
          {
            Object localObject3 = t.1.a;
            int j = paramScanType.ordinal();
            int k = localObject3[j];
            switch (k)
            {
            default: 
              break;
            case 3: 
              localObject3 = a(((Text)localObject2).a());
              ((List)localObject1).addAll((Collection)localObject3);
              localObject2 = b(((Text)localObject2).a());
              ((List)localObject1).addAll((Collection)localObject2);
              break;
            case 2: 
              localObject2 = b(((Text)localObject2).a());
              ((List)localObject1).addAll((Collection)localObject2);
              break;
            case 1: 
              localObject2 = a(((Text)localObject2).a());
              ((List)localObject1).addAll((Collection)localObject2);
            }
          }
        }
        return (List)localObject1;
      }
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.scanner.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */