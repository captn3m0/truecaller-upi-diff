package com.truecaller.scanner;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import com.truecaller.TrueApp;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.utils.l;
import java.util.List;

public class NumberScannerActivity
  extends AppCompatActivity
  implements View.OnClickListener, e, p.a, p.b
{
  b a;
  private p b;
  private View c;
  private boolean d = false;
  private boolean e = false;
  
  public static Intent a(Context paramContext, NumberDetectorProcessor.ScanType paramScanType)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, NumberScannerActivity.class);
    localIntent.putExtra("scan_type", paramScanType);
    return localIntent;
  }
  
  public final void a()
  {
    d = true;
    b.b();
  }
  
  public final void a(Intent paramIntent, int paramInt)
  {
    setResult(paramInt, paramIntent);
    finish();
  }
  
  public final void a(String paramString)
  {
    Toast.makeText(this, paramString, 0).show();
  }
  
  public final void a(List paramList)
  {
    a.a(paramList);
  }
  
  public final void a(String[] paramArrayOfString)
  {
    android.support.v4.app.a.a(this, paramArrayOfString, 2);
  }
  
  public final void b()
  {
    a.b();
  }
  
  public final void c()
  {
    a.a(false);
  }
  
  public final void d()
  {
    c.performHapticFeedback(3);
  }
  
  public final void e()
  {
    b.c();
  }
  
  public final void f()
  {
    e = true;
    b.d();
  }
  
  public final void g()
  {
    finish();
  }
  
  public void onClick(View paramView)
  {
    int i = paramView.getId();
    int j = 2131362511;
    if (i == j)
    {
      paramView = a;
      paramView.a();
    }
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    int i = 2131558454;
    setContentView(i);
    paramBundle = a.a();
    Object localObject1 = TrueApp.y().a();
    paramBundle.a((bp)localObject1).a().a(this);
    paramBundle = ((bk)getApplication()).a();
    localObject1 = NumberDetectorProcessor.ScanType.SCAN_PHONE;
    Object localObject2 = getIntent();
    Object localObject3 = "scan_type";
    boolean bool2 = ((Intent)localObject2).hasExtra((String)localObject3);
    Object localObject4;
    if (bool2)
    {
      localObject1 = getIntent();
      localObject2 = "scan_type";
      localObject1 = (NumberDetectorProcessor.ScanType)((Intent)localObject1).getSerializableExtra((String)localObject2);
      localObject4 = localObject1;
    }
    else
    {
      localObject4 = localObject1;
    }
    localObject1 = NumberDetectorProcessor.ScanType.SCAN_PHONE;
    int j = 2131362418;
    if (localObject4 == localObject1)
    {
      localObject1 = (TextView)findViewById(j);
      j = 2131888738;
      ((TextView)localObject1).setText(j);
    }
    else
    {
      localObject1 = (TextView)findViewById(j);
      j = 2131888739;
      ((TextView)localObject1).setText(j);
    }
    localObject1 = findViewById(2131364934);
    c = ((View)localObject1);
    ((ImageButton)findViewById(2131362511)).setOnClickListener(this);
    localObject1 = new com/truecaller/scanner/ScannerManagerImpl;
    View localView = c;
    r localr = paramBundle.ba();
    localObject2 = localObject1;
    localObject3 = this;
    ((ScannerManagerImpl)localObject1).<init>(this, localView, (NumberDetectorProcessor.ScanType)localObject4, this, this, localr);
    b = ((p)localObject1);
    a.a(this);
    paramBundle = paramBundle.bw();
    localObject1 = new String[] { "android.permission.CAMERA" };
    boolean bool1 = paramBundle.a((String[])localObject1);
    d = bool1;
    paramBundle = a;
    boolean bool3 = d;
    paramBundle.a(bool3);
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    a.y_();
  }
  
  public void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    a.a(paramInt, paramArrayOfInt);
  }
  
  public void onResume()
  {
    super.onResume();
    boolean bool = d;
    if (bool)
    {
      p localp = b;
      localp.b();
    }
  }
  
  public void onStart()
  {
    super.onStart();
    b.a();
  }
  
  public void onStop()
  {
    super.onStop();
    p localp = b;
    localp.c();
    boolean bool = e;
    if (!bool)
    {
      localp = b;
      localp.d();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.scanner.NumberScannerActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */