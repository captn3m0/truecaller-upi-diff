package com.truecaller.scanner;

import android.net.Uri;
import com.d.b.e;
import com.truecaller.data.entity.Contact;

abstract interface j
{
  public abstract void a();
  
  public abstract void a(Uri paramUri, e parame);
  
  public abstract void a(String paramString);
  
  public abstract void a(String paramString, Contact paramContact);
  
  public abstract void a(String paramString1, String paramString2);
  
  public abstract void a(String paramString1, String paramString2, String paramString3);
  
  public abstract void a(String paramString1, String paramString2, boolean paramBoolean1, boolean paramBoolean2);
  
  public abstract void a(String paramString, boolean paramBoolean1, boolean paramBoolean2);
  
  public abstract void a(boolean paramBoolean, String paramString1, String paramString2);
  
  public abstract void b();
}

/* Location:
 * Qualified Name:     com.truecaller.scanner.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */