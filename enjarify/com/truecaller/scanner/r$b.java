package com.truecaller.scanner;

import android.os.AsyncTask;
import java.lang.ref.WeakReference;

public final class r$b
  extends AsyncTask
{
  private final WeakReference a;
  private final WeakReference b;
  private final r c;
  
  public r$b(r paramr, NumberDetectorProcessor paramNumberDetectorProcessor, ScannerView paramScannerView)
  {
    c = paramr;
    paramr = new java/lang/ref/WeakReference;
    paramr.<init>(paramNumberDetectorProcessor);
    a = paramr;
    paramr = new java/lang/ref/WeakReference;
    paramr.<init>(paramScannerView);
    b = paramr;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.scanner.r.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */