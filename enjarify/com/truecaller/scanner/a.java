package com.truecaller.scanner;

import com.truecaller.bp;
import com.truecaller.scanner.barcode.BarcodeCaptureActivity;
import javax.inject.Provider;

public final class a
  implements k
{
  private final bp a;
  private Provider b;
  private Provider c;
  private Provider d;
  private Provider e;
  private Provider f;
  private Provider g;
  private Provider h;
  private Provider i;
  private Provider j;
  private Provider k;
  private Provider l;
  private Provider m;
  private Provider n;
  private Provider o;
  private Provider p;
  private Provider q;
  
  private a(bp parambp)
  {
    a = parambp;
    Object localObject = new com/truecaller/scanner/a$h;
    ((a.h)localObject).<init>(parambp);
    b = ((Provider)localObject);
    localObject = d.a(b);
    c = ((Provider)localObject);
    localObject = dagger.a.c.a(c);
    d = ((Provider)localObject);
    localObject = new com/truecaller/scanner/a$d;
    ((a.d)localObject).<init>(parambp);
    e = ((Provider)localObject);
    localObject = dagger.a.c.a(q.a(e));
    f = ((Provider)localObject);
    localObject = new com/truecaller/scanner/a$b;
    ((a.b)localObject).<init>(parambp);
    g = ((Provider)localObject);
    localObject = new com/truecaller/scanner/a$e;
    ((a.e)localObject).<init>(parambp);
    h = ((Provider)localObject);
    localObject = new com/truecaller/scanner/a$j;
    ((a.j)localObject).<init>(parambp);
    i = ((Provider)localObject);
    localObject = new com/truecaller/scanner/a$c;
    ((a.c)localObject).<init>(parambp);
    j = ((Provider)localObject);
    localObject = new com/truecaller/scanner/a$f;
    ((a.f)localObject).<init>(parambp);
    k = ((Provider)localObject);
    Provider localProvider1 = f;
    Provider localProvider2 = g;
    Provider localProvider3 = h;
    Provider localProvider4 = i;
    Provider localProvider5 = j;
    Provider localProvider6 = k;
    localObject = i.a(localProvider1, localProvider2, localProvider3, localProvider4, localProvider5, localProvider6);
    l = ((Provider)localObject);
    localObject = dagger.a.c.a(l);
    m = ((Provider)localObject);
    localObject = new com/truecaller/scanner/a$i;
    ((a.i)localObject).<init>(parambp);
    n = ((Provider)localObject);
    localObject = new com/truecaller/scanner/a$g;
    ((a.g)localObject).<init>(parambp);
    o = ((Provider)localObject);
    parambp = b;
    localObject = n;
    localProvider1 = o;
    localProvider2 = j;
    parambp = com.truecaller.scanner.barcode.c.a(parambp, (Provider)localObject, localProvider1, localProvider2);
    p = parambp;
    parambp = dagger.a.c.a(p);
    q = parambp;
  }
  
  public static a.a a()
  {
    a.a locala = new com/truecaller/scanner/a$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  public final void a(NumberScannerActivity paramNumberScannerActivity)
  {
    b localb = (b)d.get();
    a = localb;
  }
  
  public final void a(BarcodeCaptureActivity paramBarcodeCaptureActivity)
  {
    com.truecaller.scanner.barcode.a.a locala = (com.truecaller.scanner.barcode.a.a)q.get();
    a = locala;
  }
  
  public final void a(f paramf)
  {
    Object localObject = (g)m.get();
    b = ((g)localObject);
    localObject = (com.truecaller.calling.initiate_call.b)dagger.a.g.a(a.bM(), "Cannot return null from a non-@Nullable component method");
    c = ((com.truecaller.calling.initiate_call.b)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.scanner.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */