package com.truecaller.scanner;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.List;

public final class s
  extends BaseAdapter
{
  private final LayoutInflater a;
  private final List b;
  
  public s(Context paramContext, List paramList)
  {
    paramContext = LayoutInflater.from(paramContext);
    a = paramContext;
    b = paramList;
  }
  
  public final int getCount()
  {
    List localList = b;
    if (localList != null) {
      return localList.size();
    }
    return 0;
  }
  
  public final long getItemId(int paramInt)
  {
    return ((String)b.get(paramInt)).hashCode();
  }
  
  public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    if (paramView == null)
    {
      paramView = a;
      int i = 2131559031;
      paramView = paramView.inflate(i, paramViewGroup, false);
      paramViewGroup = new com/truecaller/scanner/s$a;
      paramViewGroup.<init>(this, paramView);
      paramView.setTag(paramViewGroup);
    }
    else
    {
      paramViewGroup = (s.a)paramView.getTag();
    }
    paramViewGroup = a;
    CharSequence localCharSequence = (CharSequence)b.get(paramInt);
    paramViewGroup.setText(localCharSequence);
    return paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.scanner.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */