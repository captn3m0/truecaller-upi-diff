package com.truecaller.scanner;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.o;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.truecaller.TrueApp;
import com.truecaller.analytics.e.a;
import com.truecaller.bp;
import com.truecaller.calling.initiate_call.b.a.a;
import com.truecaller.data.entity.Contact;
import com.truecaller.log.d;
import com.truecaller.swish.SwishInputActivity;
import com.truecaller.truepay.app.ui.transaction.views.activities.TransactionActivity;
import com.truecaller.ui.components.CyclicProgressBar;
import com.truecaller.ui.details.DetailsFragment;
import com.truecaller.ui.details.DetailsFragment.SourceType;
import com.truecaller.ui.view.ContactPhoto;
import com.truecaller.util.at;
import com.truecaller.util.bj;

public class f
  extends android.support.v4.app.e
  implements View.OnClickListener, j
{
  public static final String a = "f";
  g b;
  com.truecaller.calling.initiate_call.b c;
  private View d;
  private View e;
  private ContactPhoto f;
  private CyclicProgressBar g;
  private TextView h;
  private TextView i;
  private Button j;
  private Button k;
  private Button l;
  private Button m;
  private Button n;
  private ImageView o;
  private int p;
  private int q;
  
  private static void a(Button paramButton, int paramInt)
  {
    Object localObject = paramButton.getCompoundDrawables()[0];
    if (localObject != null)
    {
      localObject = android.support.v4.graphics.drawable.a.e((Drawable)localObject).mutate();
      android.support.v4.graphics.drawable.a.a((Drawable)localObject, paramInt);
      paramButton.setCompoundDrawablesWithIntrinsicBounds((Drawable)localObject, null, null, null);
      return;
    }
    localObject = new Object[2];
    String str = a;
    localObject[0] = str;
    paramButton = paramButton.getText();
    localObject[1] = paramButton;
    d.a(String.format("%1s's %2s has left drawable null", (Object[])localObject));
  }
  
  private static void a(ImageView paramImageView, int paramInt)
  {
    Drawable localDrawable = android.support.v4.graphics.drawable.a.e(paramImageView.getDrawable()).mutate();
    android.support.v4.graphics.drawable.a.a(localDrawable, paramInt);
    paramImageView.setImageDrawable(localDrawable);
  }
  
  public static void a(String paramString, AppCompatActivity paramAppCompatActivity)
  {
    boolean bool = paramAppCompatActivity.isFinishing();
    if (!bool)
    {
      f localf = new com/truecaller/scanner/f;
      localf.<init>();
      Bundle localBundle = new android/os/Bundle;
      localBundle.<init>();
      String str = "phone_number";
      localBundle.putString(str, paramString);
      localf.setArguments(localBundle);
      paramString = paramAppCompatActivity.getSupportFragmentManager().a();
      paramAppCompatActivity = a;
      paramString = paramString.a(localf, paramAppCompatActivity);
      paramString.d();
    }
  }
  
  private void a(boolean paramBoolean1, boolean paramBoolean2)
  {
    at.a(d, false);
    Object localObject1 = e;
    boolean bool = true;
    at.a((View)localObject1, bool);
    localObject1 = j;
    int i1 = p;
    a((Button)localObject1, i1);
    localObject1 = k;
    i1 = p;
    a((Button)localObject1, i1);
    localObject1 = n;
    i1 = p;
    a((Button)localObject1, i1);
    at.a(j, bool);
    at.a(k, bool);
    at.a(n, bool);
    localObject1 = l;
    at.a((View)localObject1, paramBoolean1);
    if (paramBoolean1)
    {
      localObject2 = l;
      int i2 = p;
      a((Button)localObject2, i2);
    }
    at.a(m, paramBoolean2);
    Object localObject2 = o;
    paramBoolean2 = q;
    a((ImageView)localObject2, paramBoolean2);
  }
  
  private static void b(String paramString)
  {
    com.truecaller.analytics.b localb = TrueApp.y().a().c();
    e.a locala = new com/truecaller/analytics/e$a;
    locala.<init>("SCANDIALOG_Clicked");
    paramString = locala.a("SCANDIALOG_Action", paramString).a();
    localb.a(paramString);
  }
  
  public final void a()
  {
    g.a(true);
  }
  
  public final void a(Uri paramUri, com.d.b.e parame)
  {
    f.setCallback(parame);
    f.a(paramUri, null);
  }
  
  public final void a(String paramString)
  {
    bj.a(getActivity(), paramString);
    b("ScannedNumberSMS");
  }
  
  public final void a(String paramString, Contact paramContact)
  {
    Object localObject1 = getFragmentManager();
    if (localObject1 == null) {
      return;
    }
    localObject1 = new android/content/Intent;
    Object localObject2 = getActivity();
    Class localClass = SwishInputActivity.class;
    ((Intent)localObject1).<init>((Context)localObject2, localClass);
    localObject2 = "payee_number";
    ((Intent)localObject1).putExtra((String)localObject2, paramString);
    if (paramContact != null)
    {
      paramString = "payee_contact";
      ((Intent)localObject1).putExtra(paramString, paramContact);
    }
    startActivity((Intent)localObject1);
  }
  
  public final void a(String paramString1, String paramString2)
  {
    Object localObject = getActivity();
    if (localObject == null) {
      return;
    }
    localObject = new com/truecaller/calling/initiate_call/b$a$a;
    ((b.a.a)localObject).<init>(paramString1, "scannedDialog");
    paramString1 = ((b.a.a)localObject).a(paramString2).a();
    c.a(paramString1);
    b("ScannedNumberCall");
  }
  
  public final void a(String paramString1, String paramString2, String paramString3)
  {
    Context localContext = getContext();
    DetailsFragment.SourceType localSourceType = DetailsFragment.SourceType.ScannedNumber;
    paramString1 = DetailsFragment.a(localContext, null, null, paramString2, paramString1, paramString3, localSourceType, true, true, 4);
    startActivity(paramString1);
  }
  
  public final void a(String paramString1, String paramString2, boolean paramBoolean1, boolean paramBoolean2)
  {
    h.setText(paramString1);
    i.setText(paramString2);
    a(paramBoolean1, paramBoolean2);
  }
  
  public final void a(String paramString, boolean paramBoolean1, boolean paramBoolean2)
  {
    h.setText(paramString);
    ((RelativeLayout.LayoutParams)h.getLayoutParams()).addRule(15);
    at.a(i, false);
    a(paramBoolean1, paramBoolean2);
  }
  
  public final void a(boolean paramBoolean, String paramString1, String paramString2)
  {
    if (paramBoolean)
    {
      Context localContext = getContext();
      TransactionActivity.startForSend(localContext, paramString1, paramString2);
    }
    b("ScannedNumberPay");
  }
  
  public final void b()
  {
    Object localObject = getActivity();
    if (localObject != null)
    {
      ((f.a)getActivity()).ah_();
      localObject = "ScanAgain";
      b((String)localObject);
    }
  }
  
  public void onClick(View paramView)
  {
    int i1 = paramView.getId();
    switch (i1)
    {
    default: 
      paramView = "ScanAgain";
      break;
    case 2131364622: 
      paramView = "ScannedNumberSwish";
      break;
    case 2131364542: 
      paramView = "ScannedNumberSMS";
      break;
    case 2131363897: 
      paramView = "ScannedNumberPay";
      break;
    case 2131363260: 
      paramView = "ScannedNumberDetails";
      break;
    case 2131362371: 
      paramView = "ScannedNumberCall";
    }
    b.a(paramView);
    dismiss();
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = a.a();
    bp localbp = TrueApp.y().a();
    paramBundle.a(localbp).a().a(this);
  }
  
  public Dialog onCreateDialog(Bundle paramBundle)
  {
    paramBundle = super.onCreateDialog(paramBundle);
    paramBundle.getWindow().requestFeature(1);
    return paramBundle;
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    return paramLayoutInflater.inflate(2131559115, paramViewGroup, false);
  }
  
  public void onDestroyView()
  {
    super.onDestroyView();
    b.y_();
  }
  
  public void onResume()
  {
    super.onResume();
    Window localWindow = getDialog().getWindow();
    if (localWindow != null)
    {
      int i1 = -1;
      int i2 = -2;
      localWindow.setLayout(i1, i2);
    }
  }
  
  public void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    paramBundle = (ContactPhoto)paramView.findViewById(2131362554);
    f = paramBundle;
    paramBundle = (CyclicProgressBar)paramView.findViewById(2131362071);
    g = paramBundle;
    paramBundle = (TextView)paramView.findViewById(2131362552);
    h = paramBundle;
    paramBundle = (TextView)paramView.findViewById(2131362553);
    i = paramBundle;
    paramBundle = paramView.findViewById(2131363260);
    e = paramBundle;
    paramBundle = paramView.findViewById(2131364018);
    d = paramBundle;
    paramBundle = (Button)paramView.findViewById(2131362371);
    j = paramBundle;
    paramBundle = (Button)paramView.findViewById(2131364542);
    k = paramBundle;
    paramBundle = (Button)paramView.findViewById(2131363897);
    l = paramBundle;
    paramBundle = (Button)paramView.findViewById(2131364622);
    m = paramBundle;
    paramBundle = (Button)paramView.findViewById(2131364233);
    n = paramBundle;
    paramView = (ImageView)paramView.findViewById(2131362550);
    o = paramView;
    j.setOnClickListener(this);
    k.setOnClickListener(this);
    l.setOnClickListener(this);
    m.setOnClickListener(this);
    n.setOnClickListener(this);
    e.setOnClickListener(this);
    b.a(this);
    paramView = b;
    paramBundle = getArguments().getString("phone_number");
    paramView.b(paramBundle);
    int i1 = com.truecaller.utils.ui.b.a(getContext(), 2130969528);
    p = i1;
    i1 = com.truecaller.utils.ui.b.a(getContext(), 2130969592);
    q = i1;
    paramView = TrueApp.y().a().c();
    paramBundle = new com/truecaller/analytics/e$a;
    paramBundle.<init>("SCANDIALOG_Shown");
    paramBundle = paramBundle.a("SCANDIALOG_Action", "ScanShow").a();
    paramView.a(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.scanner.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */