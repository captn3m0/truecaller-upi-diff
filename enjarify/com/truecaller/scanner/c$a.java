package com.truecaller.scanner;

import android.content.Intent;
import android.os.Bundle;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

final class c$a
  implements Runnable
{
  private final WeakReference a;
  private final List b;
  
  c$a(e parame, List paramList)
  {
    WeakReference localWeakReference = new java/lang/ref/WeakReference;
    localWeakReference.<init>(parame);
    a = localWeakReference;
    b = paramList;
  }
  
  public final void run()
  {
    e locale = (e)a.get();
    if (locale != null)
    {
      locale.d();
      Object localObject = b;
      int i = ((List)localObject).size();
      Intent localIntent;
      int j;
      if (i > 0)
      {
        localObject = new android/os/Bundle;
        ((Bundle)localObject).<init>();
        ArrayList localArrayList = (ArrayList)b;
        ((Bundle)localObject).putStringArrayList("extra_results", localArrayList);
        localIntent = new android/content/Intent;
        localIntent.<init>();
        localObject = localIntent.putExtras((Bundle)localObject);
        j = -1;
        locale.a((Intent)localObject, j);
      }
      else
      {
        i = 0;
        localObject = null;
        j = 0;
        localIntent = null;
        locale.a(null, 0);
      }
      locale.e();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.scanner.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */