package com.inmobi.commons.a;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Binder;
import android.os.Build.VERSION;
import android.text.TextUtils;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.inmobi.commons.core.utilities.c;
import java.io.File;
import java.util.concurrent.atomic.AtomicBoolean;

public final class a
{
  private static final String a = "a";
  private static final boolean b;
  private static Context c;
  private static String d;
  private static String e;
  private static AtomicBoolean f;
  private static boolean g = false;
  
  static
  {
    "row".contains("staging");
    b = false;
    d = "";
    e = "";
    AtomicBoolean localAtomicBoolean = new java/util/concurrent/atomic/AtomicBoolean;
    localAtomicBoolean.<init>();
    f = localAtomicBoolean;
  }
  
  public static File a(Context paramContext)
  {
    File localFile = new java/io/File;
    paramContext = paramContext.getFilesDir();
    localFile.<init>(paramContext, "im_cached_content");
    return localFile;
  }
  
  public static File a(String paramString)
  {
    h();
    File localFile1 = new java/io/File;
    File localFile2 = a(c);
    int i = paramString.length() / 2;
    int j = paramString.substring(0, i).hashCode();
    int k = -1 >>> 1;
    String str = String.valueOf(j & k);
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(str);
    paramString = String.valueOf(paramString.substring(i).hashCode() & k);
    localStringBuilder.append(paramString);
    paramString = localStringBuilder.toString();
    localFile1.<init>(localFile2, paramString);
    return localFile1;
  }
  
  public static void a(Context paramContext, Intent paramIntent)
  {
    boolean bool = paramContext instanceof Activity;
    if (!bool)
    {
      int i = 268435456;
      paramIntent.setFlags(i);
    }
    ((Context)paramContext).startActivity(paramIntent);
  }
  
  public static void a(Context paramContext, String paramString)
  {
    boolean bool = a();
    if (!bool)
    {
      Context localContext = paramContext.getApplicationContext();
      c = localContext;
      e = paramString;
      paramString = f;
      paramString.set(true);
      int j = Build.VERSION.SDK_INT;
      int i = 17;
      if (j < i) {
        try
        {
          paramString = new android/webkit/WebView;
          paramString.<init>(paramContext);
          paramContext = paramString.getSettings();
          paramContext = paramContext.getUserAgentString();
          d = paramContext;
        }
        catch (Exception paramContext)
        {
          j = 0;
          paramString = null;
          c = null;
          paramContext.getMessage();
        }
      }
      h();
    }
  }
  
  public static void a(File paramFile)
  {
    c.a(paramFile);
  }
  
  public static void a(boolean paramBoolean)
  {
    g = paramBoolean;
  }
  
  public static boolean a()
  {
    Context localContext = c;
    return localContext != null;
  }
  
  public static Context b()
  {
    return c;
  }
  
  public static void b(Context paramContext)
  {
    try
    {
      File localFile = new java/io/File;
      paramContext = paramContext.getCacheDir();
      String str = "im_cached_content";
      localFile.<init>(paramContext, str);
      boolean bool = localFile.exists();
      if (bool) {
        c.a(localFile);
      }
      return;
    }
    catch (Exception localException)
    {
      localException.getMessage();
    }
  }
  
  public static void b(boolean paramBoolean)
  {
    f.set(paramBoolean);
  }
  
  public static boolean b(Context paramContext, String paramString)
  {
    boolean bool = false;
    if (paramContext == null) {
      return false;
    }
    PackageManager localPackageManager = paramContext.getPackageManager();
    int i = Build.VERSION.SDK_INT;
    int j = 23;
    if (i < j)
    {
      int k = Binder.getCallingUid();
      paramContext = localPackageManager.getNameForUid(k);
      k = localPackageManager.checkPermission(paramString, paramContext);
      if (k == 0) {
        bool = true;
      }
    }
    else
    {
      bool = c(paramContext, paramString);
    }
    return bool;
  }
  
  private static String c(Context paramContext)
  {
    String str = "";
    try
    {
      boolean bool = b;
      if (!bool)
      {
        paramContext = paramContext.getApplicationContext();
        return WebSettings.getDefaultUserAgent(paramContext);
      }
      paramContext = new java/lang/Exception;
      localObject = "android.util.AndroidRuntimeException: android.content.pm.PackageManager$NameNotFoundException: com.google.android.webview";
      paramContext.<init>((String)localObject);
      throw paramContext;
    }
    finally
    {
      paramContext.getMessage();
      Object localObject = com.inmobi.commons.core.a.a.a();
      com.inmobi.commons.core.e.a locala = new com/inmobi/commons/core/e/a;
      locala.<init>(paramContext);
      ((com.inmobi.commons.core.a.a)localObject).a(locala);
      paramContext = "http.agent";
      try
      {
        str = System.getProperty(paramContext);
        if (str == null) {
          return "";
        }
      }
      catch (Exception paramContext)
      {
        paramContext.getMessage();
        localObject = com.inmobi.commons.core.a.a.a();
        locala = new com/inmobi/commons/core/e/a;
        locala.<init>(paramContext);
        ((com.inmobi.commons.core.a.a)localObject).a(locala);
      }
    }
    return str;
  }
  
  public static void c()
  {
    c = null;
  }
  
  private static boolean c(Context paramContext, String paramString)
  {
    if ((paramContext != null) && (paramString != null)) {
      try
      {
        Object localObject1 = paramContext.getPackageManager();
        paramContext = paramContext.getPackageName();
        int i = 4096;
        paramContext = ((PackageManager)localObject1).getPackageInfo(paramContext, i);
        localObject1 = requestedPermissions;
        if (localObject1 != null)
        {
          paramContext = requestedPermissions;
          int j = paramContext.length;
          i = 0;
          while (i < j)
          {
            Object localObject2 = paramContext[i];
            boolean bool = ((String)localObject2).equals(paramString);
            if (bool) {
              return true;
            }
            i += 1;
          }
        }
        return false;
      }
      catch (Exception paramContext)
      {
        paramContext.getLocalizedMessage();
      }
    }
    return false;
  }
  
  public static boolean d()
  {
    return g;
  }
  
  public static String e()
  {
    return e;
  }
  
  public static String f()
  {
    String str = d;
    boolean bool = TextUtils.isEmpty(str);
    if (bool)
    {
      int i = Build.VERSION.SDK_INT;
      int j = 17;
      if (i >= j)
      {
        str = c(c);
        d = str;
      }
    }
    return d;
  }
  
  public static boolean g()
  {
    return f.get();
  }
  
  private static void h()
  {
    File localFile = a(c);
    boolean bool = localFile.mkdir();
    if (!bool) {
      localFile.isDirectory();
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */