package com.inmobi.commons.core.f;

import android.content.ContentValues;
import com.inmobi.commons.core.d.c;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class a
  extends com.inmobi.commons.core.b.b
{
  private static final String a = "a";
  
  public a()
  {
    com.inmobi.commons.core.d.b localb = com.inmobi.commons.core.d.b.a();
    localb.a("trc", "(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, eventId TEXT NOT NULL, adMarkup TEXT NOT NULL, eventName TEXT NOT NULL, imPlid INTEGER NOT NULL, requestId TEXT NOT NULL, eventType TEXT NOT NULL, dNettypeRaw TEXT NOT NULL, ts TEXT NOT NULL, adtype TEXT NOT NULL, timestamp TEXT NOT NULL)");
    localb.b();
  }
  
  public static List a(int paramInt, String paramString)
  {
    com.inmobi.commons.core.d.b localb = com.inmobi.commons.core.d.b.a();
    String str1 = "trc";
    String str2 = "adtype=?";
    String[] arrayOfString = new String[1];
    boolean bool = false;
    arrayOfString[0] = paramString;
    String str3 = "timestamp ASC";
    String str4 = String.valueOf(paramInt);
    Object localObject1 = localb;
    Object localObject2 = localb.a(str1, null, str2, arrayOfString, null, null, str3, str4);
    localb.b();
    paramString = new java/util/ArrayList;
    paramString.<init>();
    localObject2 = ((List)localObject2).iterator();
    for (;;)
    {
      bool = ((Iterator)localObject2).hasNext();
      if (!bool) {
        break;
      }
      localObject1 = b.a((ContentValues)((Iterator)localObject2).next());
      paramString.add(localObject1);
    }
    return paramString;
  }
  
  public static void a(b paramb)
  {
    com.inmobi.commons.core.d.b localb = com.inmobi.commons.core.d.b.a();
    ContentValues localContentValues = new android/content/ContentValues;
    localContentValues.<init>();
    Object localObject = b;
    localContentValues.put("eventId", (String)localObject);
    localObject = c;
    localContentValues.put("adMarkup", (String)localObject);
    localObject = d;
    localContentValues.put("eventName", (String)localObject);
    localObject = Long.valueOf(e);
    localContentValues.put("imPlid", (Long)localObject);
    localObject = f;
    localContentValues.put("requestId", (String)localObject);
    localObject = g;
    localContentValues.put("eventType", (String)localObject);
    localObject = h;
    localContentValues.put("dNettypeRaw", (String)localObject);
    localObject = String.valueOf(i);
    localContentValues.put("ts", (String)localObject);
    localObject = j;
    localContentValues.put("adtype", (String)localObject);
    paramb = String.valueOf(k);
    localContentValues.put("timestamp", paramb);
    localb.a("trc", localContentValues);
    localb.b();
  }
  
  public static boolean c(String paramString)
  {
    com.inmobi.commons.core.d.b localb = com.inmobi.commons.core.d.b.a();
    String str1 = "trc";
    String str2 = "adtype=?";
    boolean bool = true;
    String[] arrayOfString = new String[bool];
    arrayOfString[0] = paramString;
    int i = localb.b(str1, str2, arrayOfString);
    localb.b();
    if (i > 0) {
      return bool;
    }
    return false;
  }
  
  public static void d(String paramString)
  {
    com.inmobi.commons.core.d.b localb = com.inmobi.commons.core.d.b.a();
    Object localObject1 = "trc";
    String str1 = "adtype=?";
    String[] arrayOfString = new String[1];
    arrayOfString[0] = paramString;
    String str2 = "timestamp ASC";
    String str3 = "1";
    String str4 = null;
    Object localObject2 = localb;
    paramString = localb.a((String)localObject1, null, str1, arrayOfString, null, null, str2, str3);
    boolean bool = paramString.isEmpty();
    if (!bool)
    {
      paramString = ((ContentValues)paramString.get(0)).getAsString("id");
      localObject2 = "trc";
      localObject1 = new java/lang/StringBuilder;
      str4 = "id IN (";
      ((StringBuilder)localObject1).<init>(str4);
      ((StringBuilder)localObject1).append(paramString);
      ((StringBuilder)localObject1).append(")");
      paramString = ((StringBuilder)localObject1).toString();
      localObject1 = null;
      localb.a((String)localObject2, paramString, null);
    }
    localb.b();
  }
  
  private static String e(String paramString)
  {
    int i = paramString.hashCode();
    int j = -1396342996;
    String str;
    if (i != j)
    {
      j = -1052618729;
      boolean bool1;
      if (i != j)
      {
        j = 104431;
        if (i == j)
        {
          str = "int";
          bool1 = paramString.equals(str);
          if (bool1)
          {
            bool1 = true;
            break label100;
          }
        }
      }
      else
      {
        str = "native";
        bool1 = paramString.equals(str);
        if (bool1)
        {
          int k = 2;
          break label100;
        }
      }
    }
    else
    {
      str = "banner";
      boolean bool2 = paramString.equals(str);
      if (bool2)
      {
        bool2 = false;
        paramString = null;
        break label100;
      }
    }
    int m = -1;
    switch (m)
    {
    default: 
      paramString = "trc_last_native_batch_process";
      break;
    case 1: 
      paramString = "trc_last_int_batch_process";
      break;
    case 0: 
      label100:
      paramString = "trc_last_banner_batch_process";
    }
    return paramString;
  }
  
  public final int a(String paramString)
  {
    com.inmobi.commons.core.d.b localb = com.inmobi.commons.core.d.b.a();
    String[] arrayOfString = new String[1];
    arrayOfString[0] = paramString;
    int i = localb.b("trc", "adtype=?", arrayOfString);
    localb.b();
    return i;
  }
  
  public final void a(List paramList)
  {
    boolean bool = paramList.isEmpty();
    if (bool) {
      return;
    }
    com.inmobi.commons.core.d.b localb = com.inmobi.commons.core.d.b.a();
    Object localObject1 = new java/lang/StringBuffer;
    ((StringBuffer)localObject1).<init>("");
    int i = 0;
    StringBuilder localStringBuilder = null;
    for (;;)
    {
      int j = paramList.size() + -1;
      if (i >= j) {
        break;
      }
      Object localObject2 = paramList.get(i);
      ((StringBuffer)localObject1).append(localObject2);
      localObject2 = ",";
      ((StringBuffer)localObject1).append((String)localObject2);
      i += 1;
    }
    i = paramList.size() + -1;
    paramList = String.valueOf(paramList.get(i));
    ((StringBuffer)localObject1).append(paramList);
    localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("id IN (");
    localStringBuilder.append(localObject1);
    localStringBuilder.append(")");
    localObject1 = localStringBuilder.toString();
    localb.a("trc", (String)localObject1, null);
    localb.b();
  }
  
  public final boolean a(long paramLong, String paramString)
  {
    int i = 1;
    paramString = a(i, paramString);
    int j = paramString.size();
    if (j > 0)
    {
      TimeUnit localTimeUnit = TimeUnit.MILLISECONDS;
      long l1 = System.currentTimeMillis();
      paramString = (b)paramString.get(0);
      long l2 = i;
      l1 -= l2;
      l1 = localTimeUnit.toSeconds(l1);
      boolean bool = l1 < paramLong;
      if (bool) {}
    }
    else
    {
      i = 0;
    }
    return i;
  }
  
  public final int b(long paramLong, String paramString)
  {
    com.inmobi.commons.core.d.b localb = com.inmobi.commons.core.d.b.a();
    long l = System.currentTimeMillis();
    paramLong *= 1000L;
    l -= paramLong;
    String[] arrayOfString = new String[2];
    String str = String.valueOf(l);
    arrayOfString[0] = str;
    arrayOfString[1] = paramString;
    int i = localb.a("trc", "ts<? and adtype=?", arrayOfString);
    localb.b();
    return i;
  }
  
  public final long b(String paramString)
  {
    paramString = e(paramString);
    boolean bool = com.inmobi.commons.a.a.a();
    long l = -1;
    if (bool) {
      return c.b("batch_processing_info").b(paramString, l);
    }
    return l;
  }
  
  public final String b(int paramInt)
  {
    com.inmobi.commons.core.d.b localb = com.inmobi.commons.core.d.b.a();
    String str1 = "trc";
    String str2 = "id=?";
    String[] arrayOfString = new String[1];
    Object localObject = String.valueOf(paramInt);
    arrayOfString[0] = localObject;
    String str3 = "timestamp ASC";
    String str4 = "1";
    localObject = localb.a(str1, null, str2, arrayOfString, null, null, str3, str4);
    localb.b();
    boolean bool = ((List)localObject).isEmpty();
    if (!bool) {
      return aget0j;
    }
    return null;
  }
  
  public final void c(long paramLong, String paramString)
  {
    paramString = e(paramString);
    boolean bool = com.inmobi.commons.a.a.a();
    if (bool)
    {
      c localc = c.b("batch_processing_info");
      localc.a(paramString, paramLong);
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.f.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */