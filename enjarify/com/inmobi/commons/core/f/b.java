package com.inmobi.commons.core.f;

import android.content.ContentValues;

public final class b
{
  public int a;
  public String b;
  public String c;
  public String d;
  public long e;
  public String f;
  public String g;
  public String h;
  public long i;
  public String j;
  long k;
  
  public b(String paramString1, String paramString2, String paramString3, long paramLong1, String paramString4, String paramString5, String paramString6, String paramString7, long paramLong2)
  {
    b = paramString1;
    c = paramString2;
    d = paramString3;
    e = paramLong1;
    f = paramString4;
    g = paramString5;
    h = paramString6;
    i = paramLong2;
    j = paramString7;
    long l = System.currentTimeMillis();
    k = l;
  }
  
  static b a(ContentValues paramContentValues)
  {
    String str1 = paramContentValues.getAsString("eventId");
    String str2 = paramContentValues.getAsString("adMarkup");
    String str3 = paramContentValues.getAsString("eventName");
    long l1 = paramContentValues.getAsLong("imPlid").longValue();
    String str4 = paramContentValues.getAsString("requestId");
    String str5 = paramContentValues.getAsString("eventType");
    String str6 = paramContentValues.getAsString("dNettypeRaw");
    long l2 = paramContentValues.getAsLong("ts").longValue();
    String str7 = paramContentValues.getAsString("adtype");
    long l3 = paramContentValues.getAsLong("timestamp").longValue();
    b localb = new com/inmobi/commons/core/f/b;
    localb.<init>(str1, str2, str3, l1, str4, str5, str6, str7, l2);
    k = l3;
    int m = paramContentValues.getAsInteger("id").intValue();
    a = m;
    return localb;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.f.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */