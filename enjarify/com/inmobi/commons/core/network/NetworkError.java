package com.inmobi.commons.core.network;

public final class NetworkError
{
  public NetworkError.ErrorCode a;
  public String b;
  
  public NetworkError(NetworkError.ErrorCode paramErrorCode, String paramString)
  {
    a = paramErrorCode;
    b = paramString;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.network.NetworkError
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */