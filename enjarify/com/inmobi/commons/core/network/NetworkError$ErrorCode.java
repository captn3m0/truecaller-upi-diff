package com.inmobi.commons.core.network;

public enum NetworkError$ErrorCode
{
  private int a;
  
  static
  {
    Object localObject = new com/inmobi/commons/core/network/NetworkError$ErrorCode;
    ((ErrorCode)localObject).<init>("NETWORK_UNAVAILABLE_ERROR", 0, 0);
    NETWORK_UNAVAILABLE_ERROR = (ErrorCode)localObject;
    localObject = new com/inmobi/commons/core/network/NetworkError$ErrorCode;
    int i = 1;
    ((ErrorCode)localObject).<init>("UNKNOWN_ERROR", i, -1);
    UNKNOWN_ERROR = (ErrorCode)localObject;
    localObject = new com/inmobi/commons/core/network/NetworkError$ErrorCode;
    int j = 2;
    ((ErrorCode)localObject).<init>("NETWORK_IO_ERROR", j, -2);
    NETWORK_IO_ERROR = (ErrorCode)localObject;
    localObject = new com/inmobi/commons/core/network/NetworkError$ErrorCode;
    int k = 3;
    ((ErrorCode)localObject).<init>("OUT_OF_MEMORY_ERROR", k, -3);
    OUT_OF_MEMORY_ERROR = (ErrorCode)localObject;
    localObject = new com/inmobi/commons/core/network/NetworkError$ErrorCode;
    int m = 4;
    ((ErrorCode)localObject).<init>("INVALID_ENCRYPTED_RESPONSE_RECEIVED", m, -4);
    INVALID_ENCRYPTED_RESPONSE_RECEIVED = (ErrorCode)localObject;
    localObject = new com/inmobi/commons/core/network/NetworkError$ErrorCode;
    int n = 5;
    ((ErrorCode)localObject).<init>("RESPONSE_EXCEEDS_SPECIFIED_SIZE_LIMIT", n, -5);
    RESPONSE_EXCEEDS_SPECIFIED_SIZE_LIMIT = (ErrorCode)localObject;
    localObject = new com/inmobi/commons/core/network/NetworkError$ErrorCode;
    int i1 = 6;
    ((ErrorCode)localObject).<init>("GZIP_DECOMPRESSION_FAILED", i1, -6);
    GZIP_DECOMPRESSION_FAILED = (ErrorCode)localObject;
    localObject = new com/inmobi/commons/core/network/NetworkError$ErrorCode;
    int i2 = 7;
    ((ErrorCode)localObject).<init>("BAD_REQUEST", i2, -7);
    BAD_REQUEST = (ErrorCode)localObject;
    localObject = new com/inmobi/commons/core/network/NetworkError$ErrorCode;
    int i3 = 8;
    ((ErrorCode)localObject).<init>("GDPR_COMPLIANCE_ENFORCED", i3, -8);
    GDPR_COMPLIANCE_ENFORCED = (ErrorCode)localObject;
    localObject = new com/inmobi/commons/core/network/NetworkError$ErrorCode;
    int i4 = 9;
    ((ErrorCode)localObject).<init>("HTTP_NO_CONTENT", i4, 204);
    HTTP_NO_CONTENT = (ErrorCode)localObject;
    localObject = new com/inmobi/commons/core/network/NetworkError$ErrorCode;
    int i5 = 10;
    ((ErrorCode)localObject).<init>("HTTP_NOT_MODIFIED", i5, 304);
    HTTP_NOT_MODIFIED = (ErrorCode)localObject;
    localObject = new com/inmobi/commons/core/network/NetworkError$ErrorCode;
    int i6 = 11;
    ((ErrorCode)localObject).<init>("HTTP_SEE_OTHER", i6, 303);
    HTTP_SEE_OTHER = (ErrorCode)localObject;
    localObject = new com/inmobi/commons/core/network/NetworkError$ErrorCode;
    ((ErrorCode)localObject).<init>("HTTP_SERVER_NOT_FOUND", 12, 404);
    HTTP_SERVER_NOT_FOUND = (ErrorCode)localObject;
    localObject = new com/inmobi/commons/core/network/NetworkError$ErrorCode;
    ((ErrorCode)localObject).<init>("HTTP_MOVED_TEMP", 13, 302);
    HTTP_MOVED_TEMP = (ErrorCode)localObject;
    localObject = new com/inmobi/commons/core/network/NetworkError$ErrorCode;
    ((ErrorCode)localObject).<init>("HTTP_INTERNAL_SERVER_ERROR", 14, 500);
    HTTP_INTERNAL_SERVER_ERROR = (ErrorCode)localObject;
    localObject = new com/inmobi/commons/core/network/NetworkError$ErrorCode;
    ((ErrorCode)localObject).<init>("HTTP_NOT_IMPLEMENTED", 15, 501);
    HTTP_NOT_IMPLEMENTED = (ErrorCode)localObject;
    localObject = new com/inmobi/commons/core/network/NetworkError$ErrorCode;
    ((ErrorCode)localObject).<init>("HTTP_BAD_GATEWAY", 16, 502);
    HTTP_BAD_GATEWAY = (ErrorCode)localObject;
    localObject = new com/inmobi/commons/core/network/NetworkError$ErrorCode;
    ((ErrorCode)localObject).<init>("HTTP_SERVER_NOT_AVAILABLE", 17, 503);
    HTTP_SERVER_NOT_AVAILABLE = (ErrorCode)localObject;
    localObject = new com/inmobi/commons/core/network/NetworkError$ErrorCode;
    ((ErrorCode)localObject).<init>("HTTP_GATEWAY_TIMEOUT", 18, 504);
    HTTP_GATEWAY_TIMEOUT = (ErrorCode)localObject;
    localObject = new com/inmobi/commons/core/network/NetworkError$ErrorCode;
    ((ErrorCode)localObject).<init>("HTTP_VERSION_NOT_SUPPORTED", 19, 505);
    HTTP_VERSION_NOT_SUPPORTED = (ErrorCode)localObject;
    localObject = new ErrorCode[20];
    ErrorCode localErrorCode = NETWORK_UNAVAILABLE_ERROR;
    localObject[0] = localErrorCode;
    localErrorCode = UNKNOWN_ERROR;
    localObject[i] = localErrorCode;
    localErrorCode = NETWORK_IO_ERROR;
    localObject[j] = localErrorCode;
    localErrorCode = OUT_OF_MEMORY_ERROR;
    localObject[k] = localErrorCode;
    localErrorCode = INVALID_ENCRYPTED_RESPONSE_RECEIVED;
    localObject[m] = localErrorCode;
    localErrorCode = RESPONSE_EXCEEDS_SPECIFIED_SIZE_LIMIT;
    localObject[n] = localErrorCode;
    localErrorCode = GZIP_DECOMPRESSION_FAILED;
    localObject[i1] = localErrorCode;
    localErrorCode = BAD_REQUEST;
    localObject[i2] = localErrorCode;
    localErrorCode = GDPR_COMPLIANCE_ENFORCED;
    localObject[i3] = localErrorCode;
    localErrorCode = HTTP_NO_CONTENT;
    localObject[i4] = localErrorCode;
    localErrorCode = HTTP_NOT_MODIFIED;
    localObject[i5] = localErrorCode;
    localErrorCode = HTTP_SEE_OTHER;
    localObject[i6] = localErrorCode;
    localErrorCode = HTTP_SERVER_NOT_FOUND;
    localObject[12] = localErrorCode;
    localErrorCode = HTTP_MOVED_TEMP;
    localObject[13] = localErrorCode;
    localErrorCode = HTTP_INTERNAL_SERVER_ERROR;
    localObject[14] = localErrorCode;
    localErrorCode = HTTP_NOT_IMPLEMENTED;
    localObject[15] = localErrorCode;
    localErrorCode = HTTP_BAD_GATEWAY;
    localObject[16] = localErrorCode;
    localErrorCode = HTTP_SERVER_NOT_AVAILABLE;
    localObject[17] = localErrorCode;
    localErrorCode = HTTP_GATEWAY_TIMEOUT;
    localObject[18] = localErrorCode;
    localErrorCode = HTTP_VERSION_NOT_SUPPORTED;
    localObject[19] = localErrorCode;
    $VALUES = (ErrorCode[])localObject;
  }
  
  private NetworkError$ErrorCode(int paramInt1)
  {
    a = paramInt1;
  }
  
  public static ErrorCode fromValue(int paramInt)
  {
    int i = 400;
    if (i <= paramInt)
    {
      i = 500;
      if (i > paramInt) {
        return BAD_REQUEST;
      }
    }
    ErrorCode[] arrayOfErrorCode = values();
    int j = arrayOfErrorCode.length;
    int k = 0;
    while (k < j)
    {
      ErrorCode localErrorCode = arrayOfErrorCode[k];
      int m = a;
      if (m == paramInt) {
        return localErrorCode;
      }
      k += 1;
    }
    return null;
  }
  
  public final int getValue()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.network.NetworkError.ErrorCode
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */