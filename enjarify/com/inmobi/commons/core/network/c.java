package com.inmobi.commons.core.network;

import android.util.Base64;
import com.inmobi.commons.core.configs.g;
import com.inmobi.commons.core.utilities.b.e;
import com.inmobi.commons.core.utilities.b.f;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import org.json.JSONObject;

public class c
{
  private static final String a = "c";
  private com.inmobi.commons.core.utilities.uid.d b;
  private byte[] c;
  private byte[] d;
  private boolean e;
  protected Map k;
  protected Map l;
  public Map m;
  String n;
  public String o;
  public int p;
  public int q;
  public boolean r;
  boolean s;
  public boolean t;
  public long u;
  boolean v;
  int w;
  public boolean x;
  
  public c(String paramString1, String paramString2)
  {
    this(paramString1, paramString2, false, null, false, 0);
    e = false;
  }
  
  public c(String paramString1, String paramString2, com.inmobi.commons.core.utilities.uid.d paramd, int paramInt)
  {
    this(paramString1, paramString2, true, paramd, false, paramInt);
  }
  
  public c(String paramString1, String paramString2, boolean paramBoolean, com.inmobi.commons.core.utilities.uid.d paramd)
  {
    this(paramString1, paramString2, paramBoolean, paramd, false, 0);
  }
  
  public c(String paramString1, String paramString2, boolean paramBoolean1, com.inmobi.commons.core.utilities.uid.d paramd, boolean paramBoolean2, int paramInt)
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    k = localHashMap;
    int i = 60000;
    p = i;
    q = i;
    i = 1;
    r = i;
    t = i;
    long l1 = -1;
    u = l1;
    w = 0;
    e = i;
    x = false;
    n = paramString1;
    o = paramString2;
    s = paramBoolean1;
    b = paramd;
    paramString2 = k;
    String str = "User-Agent";
    paramd = com.inmobi.commons.a.a.f();
    paramString2.put(str, paramd);
    v = paramBoolean2;
    w = paramInt;
    paramString2 = "GET";
    boolean bool1 = paramString2.equals(paramString1);
    if (bool1)
    {
      paramString1 = new java/util/HashMap;
      paramString1.<init>();
      l = paramString1;
      return;
    }
    paramString2 = "POST";
    boolean bool2 = paramString2.equals(paramString1);
    if (bool2)
    {
      paramString1 = new java/util/HashMap;
      paramString1.<init>();
      m = paramString1;
    }
  }
  
  private void c(Map paramMap)
  {
    Object localObject1 = ab;
    paramMap.putAll((Map)localObject1);
    boolean bool1 = x;
    localObject1 = com.inmobi.commons.core.utilities.b.b.a(bool1);
    paramMap.putAll((Map)localObject1);
    localObject1 = f.a();
    paramMap.putAll((Map)localObject1);
    localObject1 = b;
    if (localObject1 != null)
    {
      boolean bool2 = s;
      if (bool2)
      {
        localObject2 = new java/util/HashMap;
        ((HashMap)localObject2).<init>();
        localObject1 = ((com.inmobi.commons.core.utilities.uid.d)localObject1).a(null, false);
        localObject3 = new org/json/JSONObject;
        ((JSONObject)localObject3).<init>((Map)localObject1);
        localObject1 = ((JSONObject)localObject3).toString();
        ((HashMap)localObject2).put("u-id-map", localObject1);
        paramMap.putAll((Map)localObject2);
        return;
      }
      Object localObject2 = new java/util/Random;
      ((Random)localObject2).<init>();
      int i = ((Random)localObject2).nextInt();
      localObject2 = Integer.toString(i);
      boolean bool3 = true;
      localObject1 = ((com.inmobi.commons.core.utilities.uid.d)localObject1).a((String)localObject2, bool3);
      Object localObject4 = new org/json/JSONObject;
      ((JSONObject)localObject4).<init>((Map)localObject1);
      localObject1 = com.inmobi.commons.core.utilities.a.c.a(((JSONObject)localObject4).toString());
      localObject4 = new java/util/HashMap;
      ((HashMap)localObject4).<init>();
      Object localObject3 = "u-id-map";
      ((Map)localObject4).put(localObject3, localObject1);
      ((Map)localObject4).put("u-id-key", localObject2);
      localObject1 = "u-key-ver";
      com.inmobi.commons.core.utilities.uid.c.a();
      localObject2 = com.inmobi.commons.core.utilities.uid.c.d();
      ((Map)localObject4).put(localObject1, localObject2);
      paramMap.putAll((Map)localObject4);
    }
  }
  
  private String f()
  {
    com.inmobi.commons.core.utilities.d.a(l);
    return com.inmobi.commons.core.utilities.d.a(l, "&");
  }
  
  public void a()
  {
    e.c();
    int i = w;
    int j = 1;
    Object localObject1;
    if (i != j)
    {
      j = 0;
      localObject1 = null;
    }
    i = e.a(j);
    w = i;
    boolean bool1 = t;
    Object localObject2;
    if (bool1)
    {
      localObject2 = "GET";
      localObject1 = n;
      bool1 = ((String)localObject2).equals(localObject1);
      if (bool1)
      {
        localObject2 = l;
        c((Map)localObject2);
      }
      else
      {
        localObject2 = "POST";
        localObject1 = n;
        bool1 = ((String)localObject2).equals(localObject1);
        if (bool1)
        {
          localObject2 = m;
          c((Map)localObject2);
        }
      }
    }
    bool1 = e;
    if (bool1)
    {
      localObject2 = e.a();
      if (localObject2 != null)
      {
        localObject1 = "GET";
        String str = n;
        boolean bool2 = ((String)localObject1).equals(str);
        if (bool2)
        {
          localObject1 = l;
          localObject2 = ((JSONObject)localObject2).toString();
          ((Map)localObject1).put("consentObject", localObject2);
          return;
        }
        localObject1 = "POST";
        str = n;
        bool2 = ((String)localObject1).equals(str);
        if (bool2)
        {
          localObject1 = m;
          str = "consentObject";
          localObject2 = ((JSONObject)localObject2).toString();
          ((Map)localObject1).put(str, localObject2);
        }
      }
    }
  }
  
  public final void a(Map paramMap)
  {
    if (paramMap != null)
    {
      Map localMap = k;
      localMap.putAll(paramMap);
    }
  }
  
  protected final byte[] a(byte[] paramArrayOfByte)
  {
    byte[] arrayOfByte1 = null;
    try
    {
      paramArrayOfByte = Base64.decode(paramArrayOfByte, 0);
      arrayOfByte1 = d;
      byte[] arrayOfByte2 = c;
      return com.inmobi.commons.core.utilities.a.b.a(paramArrayOfByte, arrayOfByte1, arrayOfByte2);
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      localIllegalArgumentException.getMessage();
    }
    return null;
  }
  
  public final Map b()
  {
    com.inmobi.commons.core.utilities.d.a(k);
    return k;
  }
  
  public final void b(Map paramMap)
  {
    if (paramMap != null)
    {
      Map localMap = l;
      localMap.putAll(paramMap);
    }
  }
  
  public final String c()
  {
    String str = o;
    Object localObject1 = l;
    if (localObject1 != null)
    {
      localObject1 = f();
      if (localObject1 != null)
      {
        Object localObject2 = ((String)localObject1).trim();
        int i = ((String)localObject2).length();
        if (i != 0)
        {
          localObject2 = "?";
          boolean bool = str.contains((CharSequence)localObject2);
          if (!bool)
          {
            localObject2 = new java/lang/StringBuilder;
            ((StringBuilder)localObject2).<init>();
            ((StringBuilder)localObject2).append(str);
            ((StringBuilder)localObject2).append("?");
            str = ((StringBuilder)localObject2).toString();
          }
          localObject2 = "&";
          bool = str.endsWith((String)localObject2);
          if (!bool)
          {
            localObject2 = "?";
            bool = str.endsWith((String)localObject2);
            if (!bool)
            {
              localObject2 = new java/lang/StringBuilder;
              ((StringBuilder)localObject2).<init>();
              ((StringBuilder)localObject2).append(str);
              ((StringBuilder)localObject2).append("&");
              str = ((StringBuilder)localObject2).toString();
            }
          }
          localObject2 = new java/lang/StringBuilder;
          ((StringBuilder)localObject2).<init>();
          ((StringBuilder)localObject2).append(str);
          ((StringBuilder)localObject2).append((String)localObject1);
          str = ((StringBuilder)localObject2).toString();
        }
      }
    }
    return str;
  }
  
  public final String d()
  {
    com.inmobi.commons.core.utilities.d.a(m);
    Object localObject1 = m;
    Object localObject2 = "&";
    String str1 = com.inmobi.commons.core.utilities.d.a((Map)localObject1, (String)localObject2);
    boolean bool = s;
    if (bool)
    {
      byte[] arrayOfByte1 = com.inmobi.commons.core.utilities.a.b.a(8);
      int i = 16;
      localObject1 = com.inmobi.commons.core.utilities.a.b.a(i);
      c = ((byte[])localObject1);
      localObject1 = com.inmobi.commons.core.utilities.a.b.a();
      d = ((byte[])localObject1);
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      localObject2 = new com/inmobi/commons/core/configs/g;
      ((g)localObject2).<init>();
      com.inmobi.commons.core.configs.b.a().a((com.inmobi.commons.core.configs.a)localObject2, null);
      Object localObject3 = d;
      byte[] arrayOfByte2 = c;
      String str2 = b;
      String str3 = a;
      str1 = com.inmobi.commons.core.utilities.a.b.a(str1, (byte[])localObject3, arrayOfByte2, arrayOfByte1, str2, str3);
      localObject3 = "sm";
      ((Map)localObject1).put(localObject3, str1);
      localObject2 = c;
      ((Map)localObject1).put("sn", localObject2);
      localObject2 = "&";
      str1 = com.inmobi.commons.core.utilities.d.a((Map)localObject1, (String)localObject2);
    }
    return str1;
  }
  
  public final long e()
  {
    long l1 = 0L;
    String str1 = "GET";
    try
    {
      String str2 = n;
      boolean bool1 = str1.equals(str2);
      long l2;
      if (bool1)
      {
        str1 = f();
        int i = str1.length();
        l2 = i;
        l1 += l2;
      }
      else
      {
        str1 = "POST";
        str2 = n;
        boolean bool2 = str1.equals(str2);
        if (bool2)
        {
          str1 = d();
          int j = str1.length();
          l2 = j + l1;
          l1 = l2;
        }
      }
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
    return l1;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.network.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */