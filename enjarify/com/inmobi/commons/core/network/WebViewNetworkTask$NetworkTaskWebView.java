package com.inmobi.commons.core.network;

import android.content.Context;
import android.webkit.WebView;

public class WebViewNetworkTask$NetworkTaskWebView
  extends WebView
{
  public boolean a;
  
  public WebViewNetworkTask$NetworkTaskWebView(WebViewNetworkTask paramWebViewNetworkTask, Context paramContext)
  {
    super(paramContext);
  }
  
  public void destroy()
  {
    a = true;
    super.destroy();
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.network.WebViewNetworkTask.NetworkTaskWebView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */