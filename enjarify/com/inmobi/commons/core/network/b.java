package com.inmobi.commons.core.network;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

class b
{
  protected static final String a = "com.inmobi.commons.core.network.b";
  protected c b;
  protected HttpURLConnection c;
  
  public b(c paramc)
  {
    b = paramc;
  }
  
  private static String a(String paramString)
  {
    String str = null;
    if (paramString != null) {}
    try
    {
      JSONObject localJSONObject = new org/json/JSONObject;
      localJSONObject.<init>(paramString);
      paramString = "errorMessage";
      boolean bool = localJSONObject.has(paramString);
      if (bool)
      {
        paramString = "errorMessage";
        paramString = localJSONObject.getString(paramString);
        str = paramString;
      }
    }
    catch (JSONException localJSONException)
    {
      for (;;) {}
    }
    return str;
  }
  
  private void a(d paramd, boolean paramBoolean)
  {
    Object localObject1 = b;
    long l1 = u;
    long l2 = -1;
    boolean bool1 = l1 < l2;
    if (bool1)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject1 = null;
    }
    Object localObject2;
    if (i != 0)
    {
      localObject1 = c;
      i = ((HttpURLConnection)localObject1).getContentLength();
      l1 = i;
      localObject2 = b;
      l2 = u;
      bool1 = l1 < l2;
      if (bool1)
      {
        localObject3 = new com/inmobi/commons/core/network/NetworkError;
        localObject1 = NetworkError.ErrorCode.RESPONSE_EXCEEDS_SPECIFIED_SIZE_LIMIT;
        ((NetworkError)localObject3).<init>((NetworkError.ErrorCode)localObject1, "Response size greater than specified max response size");
        b = ((NetworkError)localObject3);
        return;
      }
    }
    if (paramBoolean) {
      localObject3 = c.getErrorStream();
    } else {
      localObject3 = c.getInputStream();
    }
    Object localObject3 = com.inmobi.commons.core.utilities.d.a((InputStream)localObject3);
    int i = localObject3.length;
    if (i != 0)
    {
      localObject1 = b;
      boolean bool2 = s;
      NetworkError.ErrorCode localErrorCode;
      if (bool2)
      {
        localObject1 = b;
        localObject3 = ((c)localObject1).a((byte[])localObject3);
        if (localObject3 == null)
        {
          localObject1 = new com/inmobi/commons/core/network/NetworkError;
          localErrorCode = NetworkError.ErrorCode.INVALID_ENCRYPTED_RESPONSE_RECEIVED;
          localObject2 = "Unable to decrypt the server response.";
          ((NetworkError)localObject1).<init>(localErrorCode, (String)localObject2);
          b = ((NetworkError)localObject1);
        }
      }
      if (localObject3 != null)
      {
        localObject1 = b;
        bool2 = v;
        if (bool2)
        {
          localObject3 = com.inmobi.commons.core.utilities.d.a((byte[])localObject3);
          if (localObject3 == null)
          {
            localObject1 = new com/inmobi/commons/core/network/NetworkError;
            localErrorCode = NetworkError.ErrorCode.GZIP_DECOMPRESSION_FAILED;
            localObject2 = "Failed to uncompress gzip response";
            ((NetworkError)localObject1).<init>(localErrorCode, (String)localObject2);
            b = ((NetworkError)localObject1);
          }
        }
      }
      if (localObject3 != null)
      {
        if (localObject3 != null)
        {
          int j = localObject3.length;
          if (j != 0)
          {
            j = localObject3.length;
            localObject1 = new byte[j];
            a = ((byte[])localObject1);
            localObject1 = a;
            int k = localObject3.length;
            System.arraycopy(localObject3, 0, localObject1, 0, k);
            break label341;
          }
        }
        localObject3 = new byte[0];
        a = ((byte[])localObject3);
      }
    }
    label341:
    localObject3 = c.getHeaderFields();
    d = ((Map)localObject3);
  }
  
  private void a(HttpURLConnection paramHttpURLConnection)
  {
    int i = b.p;
    paramHttpURLConnection.setConnectTimeout(i);
    i = b.q;
    paramHttpURLConnection.setReadTimeout(i);
    i = 0;
    paramHttpURLConnection.setUseCaches(false);
    Object localObject = b.b();
    if (localObject != null)
    {
      localObject = b.b().keySet().iterator();
      for (;;)
      {
        boolean bool2 = ((Iterator)localObject).hasNext();
        if (!bool2) {
          break;
        }
        str1 = (String)((Iterator)localObject).next();
        String str2 = (String)b.b().get(str1);
        paramHttpURLConnection.setRequestProperty(str1, str2);
      }
    }
    localObject = b.n;
    paramHttpURLConnection.setRequestMethod((String)localObject);
    String str1 = "GET";
    boolean bool1 = str1.equals(localObject);
    if (!bool1)
    {
      bool1 = true;
      paramHttpURLConnection.setDoOutput(bool1);
      paramHttpURLConnection.setDoInput(bool1);
    }
  }
  
  /* Error */
  public d a()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 19	com/inmobi/commons/core/network/b:b	Lcom/inmobi/commons/core/network/c;
    //   4: invokevirtual 191	com/inmobi/commons/core/network/c:a	()V
    //   7: aload_0
    //   8: getfield 19	com/inmobi/commons/core/network/b:b	Lcom/inmobi/commons/core/network/c;
    //   11: astore_1
    //   12: aload_1
    //   13: getfield 194	com/inmobi/commons/core/network/c:w	I
    //   16: istore_2
    //   17: iconst_1
    //   18: istore_3
    //   19: iload_2
    //   20: iload_3
    //   21: if_icmpeq +46 -> 67
    //   24: new 66	com/inmobi/commons/core/network/d
    //   27: astore_1
    //   28: aload_0
    //   29: getfield 19	com/inmobi/commons/core/network/b:b	Lcom/inmobi/commons/core/network/c;
    //   32: astore 4
    //   34: aload_1
    //   35: aload 4
    //   37: invokespecial 197	com/inmobi/commons/core/network/d:<init>	(Lcom/inmobi/commons/core/network/c;)V
    //   40: new 53	com/inmobi/commons/core/network/NetworkError
    //   43: astore 4
    //   45: getstatic 200	com/inmobi/commons/core/network/NetworkError$ErrorCode:GDPR_COMPLIANCE_ENFORCED	Lcom/inmobi/commons/core/network/NetworkError$ErrorCode;
    //   48: astore 5
    //   50: aload 4
    //   52: aload 5
    //   54: ldc -54
    //   56: invokespecial 64	com/inmobi/commons/core/network/NetworkError:<init>	(Lcom/inmobi/commons/core/network/NetworkError$ErrorCode;Ljava/lang/String;)V
    //   59: aload_1
    //   60: aload 4
    //   62: putfield 69	com/inmobi/commons/core/network/d:b	Lcom/inmobi/commons/core/network/NetworkError;
    //   65: aload_1
    //   66: areturn
    //   67: invokestatic 204	com/inmobi/commons/core/utilities/d:a	()Z
    //   70: istore_2
    //   71: iload_2
    //   72: ifeq +480 -> 552
    //   75: aload_0
    //   76: getfield 19	com/inmobi/commons/core/network/b:b	Lcom/inmobi/commons/core/network/c;
    //   79: astore_1
    //   80: aload_1
    //   81: invokevirtual 207	com/inmobi/commons/core/network/c:c	()Ljava/lang/String;
    //   84: astore_1
    //   85: new 209	java/net/URL
    //   88: astore 4
    //   90: aload 4
    //   92: aload_1
    //   93: invokespecial 210	java/net/URL:<init>	(Ljava/lang/String;)V
    //   96: aload 4
    //   98: invokevirtual 214	java/net/URL:openConnection	()Ljava/net/URLConnection;
    //   101: astore_1
    //   102: aload_1
    //   103: invokestatic 219	com/google/firebase/perf/network/FirebasePerfUrlConnection:instrument	(Ljava/lang/Object;)Ljava/lang/Object;
    //   106: astore_1
    //   107: aload_1
    //   108: checkcast 221	java/net/URLConnection
    //   111: astore_1
    //   112: aload_1
    //   113: checkcast 47	java/net/HttpURLConnection
    //   116: astore_1
    //   117: aload_0
    //   118: aload_1
    //   119: invokespecial 224	com/inmobi/commons/core/network/b:a	(Ljava/net/HttpURLConnection;)V
    //   122: aload_0
    //   123: aload_1
    //   124: putfield 45	com/inmobi/commons/core/network/b:c	Ljava/net/HttpURLConnection;
    //   127: aload_0
    //   128: getfield 19	com/inmobi/commons/core/network/b:b	Lcom/inmobi/commons/core/network/c;
    //   131: astore_1
    //   132: aload_1
    //   133: getfield 227	com/inmobi/commons/core/network/c:r	Z
    //   136: istore_2
    //   137: iload_2
    //   138: ifne +18 -> 156
    //   141: aload_0
    //   142: getfield 45	com/inmobi/commons/core/network/b:c	Ljava/net/HttpURLConnection;
    //   145: astore_1
    //   146: iconst_0
    //   147: istore_3
    //   148: aconst_null
    //   149: astore 4
    //   151: aload_1
    //   152: iconst_0
    //   153: invokevirtual 230	java/net/HttpURLConnection:setInstanceFollowRedirects	(Z)V
    //   156: ldc -24
    //   158: astore_1
    //   159: aload_0
    //   160: getfield 19	com/inmobi/commons/core/network/b:b	Lcom/inmobi/commons/core/network/c;
    //   163: astore 4
    //   165: aload 4
    //   167: getfield 174	com/inmobi/commons/core/network/c:n	Ljava/lang/String;
    //   170: astore 4
    //   172: aload_1
    //   173: aload 4
    //   175: invokevirtual 183	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   178: istore_2
    //   179: iload_2
    //   180: ifeq +140 -> 320
    //   183: aload_0
    //   184: getfield 19	com/inmobi/commons/core/network/b:b	Lcom/inmobi/commons/core/network/c;
    //   187: astore_1
    //   188: aload_1
    //   189: invokevirtual 234	com/inmobi/commons/core/network/c:d	()Ljava/lang/String;
    //   192: astore_1
    //   193: aload_0
    //   194: getfield 45	com/inmobi/commons/core/network/b:c	Ljava/net/HttpURLConnection;
    //   197: astore 4
    //   199: ldc -20
    //   201: astore 5
    //   203: aload_1
    //   204: invokevirtual 239	java/lang/String:length	()I
    //   207: istore 6
    //   209: iload 6
    //   211: invokestatic 245	java/lang/Integer:toString	(I)Ljava/lang/String;
    //   214: astore 7
    //   216: aload 4
    //   218: aload 5
    //   220: aload 7
    //   222: invokevirtual 171	java/net/HttpURLConnection:setRequestProperty	(Ljava/lang/String;Ljava/lang/String;)V
    //   225: aload_0
    //   226: getfield 45	com/inmobi/commons/core/network/b:c	Ljava/net/HttpURLConnection;
    //   229: astore 4
    //   231: ldc -9
    //   233: astore 5
    //   235: ldc -7
    //   237: astore 7
    //   239: aload 4
    //   241: aload 5
    //   243: aload 7
    //   245: invokevirtual 171	java/net/HttpURLConnection:setRequestProperty	(Ljava/lang/String;Ljava/lang/String;)V
    //   248: iconst_0
    //   249: istore_3
    //   250: aconst_null
    //   251: astore 4
    //   253: new 251	java/io/BufferedWriter
    //   256: astore 5
    //   258: new 253	java/io/OutputStreamWriter
    //   261: astore 7
    //   263: aload_0
    //   264: getfield 45	com/inmobi/commons/core/network/b:c	Ljava/net/HttpURLConnection;
    //   267: astore 8
    //   269: aload 8
    //   271: invokevirtual 257	java/net/HttpURLConnection:getOutputStream	()Ljava/io/OutputStream;
    //   274: astore 8
    //   276: aload 7
    //   278: aload 8
    //   280: invokespecial 260	java/io/OutputStreamWriter:<init>	(Ljava/io/OutputStream;)V
    //   283: aload 5
    //   285: aload 7
    //   287: invokespecial 263	java/io/BufferedWriter:<init>	(Ljava/io/Writer;)V
    //   290: aload 5
    //   292: aload_1
    //   293: invokevirtual 266	java/io/BufferedWriter:write	(Ljava/lang/String;)V
    //   296: aload 5
    //   298: invokestatic 269	com/inmobi/commons/core/utilities/d:a	(Ljava/io/Closeable;)V
    //   301: goto +19 -> 320
    //   304: astore_1
    //   305: aload 5
    //   307: astore 4
    //   309: goto +4 -> 313
    //   312: astore_1
    //   313: aload 4
    //   315: invokestatic 269	com/inmobi/commons/core/utilities/d:a	(Ljava/io/Closeable;)V
    //   318: aload_1
    //   319: athrow
    //   320: aload_0
    //   321: invokevirtual 272	com/inmobi/commons/core/network/b:b	()Lcom/inmobi/commons/core/network/d;
    //   324: astore_1
    //   325: goto +273 -> 598
    //   328: astore_1
    //   329: new 66	com/inmobi/commons/core/network/d
    //   332: astore 4
    //   334: aload_0
    //   335: getfield 19	com/inmobi/commons/core/network/b:b	Lcom/inmobi/commons/core/network/c;
    //   338: astore 5
    //   340: aload 4
    //   342: aload 5
    //   344: invokespecial 197	com/inmobi/commons/core/network/d:<init>	(Lcom/inmobi/commons/core/network/c;)V
    //   347: new 53	com/inmobi/commons/core/network/NetworkError
    //   350: astore 5
    //   352: getstatic 275	com/inmobi/commons/core/network/NetworkError$ErrorCode:UNKNOWN_ERROR	Lcom/inmobi/commons/core/network/NetworkError$ErrorCode;
    //   355: astore 7
    //   357: aload_1
    //   358: invokevirtual 280	java/lang/Exception:getLocalizedMessage	()Ljava/lang/String;
    //   361: astore 8
    //   363: aload 5
    //   365: aload 7
    //   367: aload 8
    //   369: invokespecial 64	com/inmobi/commons/core/network/NetworkError:<init>	(Lcom/inmobi/commons/core/network/NetworkError$ErrorCode;Ljava/lang/String;)V
    //   372: aload 4
    //   374: aload 5
    //   376: putfield 69	com/inmobi/commons/core/network/d:b	Lcom/inmobi/commons/core/network/NetworkError;
    //   379: new 282	java/util/HashMap
    //   382: astore 5
    //   384: aload 5
    //   386: invokespecial 283	java/util/HashMap:<init>	()V
    //   389: ldc_w 285
    //   392: astore 7
    //   394: ldc_w 287
    //   397: astore 8
    //   399: aload 5
    //   401: aload 7
    //   403: aload 8
    //   405: invokeinterface 291 3 0
    //   410: pop
    //   411: ldc_w 293
    //   414: astore 7
    //   416: new 295	java/lang/StringBuilder
    //   419: astore 8
    //   421: aload 8
    //   423: invokespecial 296	java/lang/StringBuilder:<init>	()V
    //   426: aload_1
    //   427: invokevirtual 299	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   430: astore 9
    //   432: aload 8
    //   434: aload 9
    //   436: invokevirtual 303	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   439: pop
    //   440: aload 8
    //   442: invokevirtual 305	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   445: astore 8
    //   447: aload 5
    //   449: aload 7
    //   451: aload 8
    //   453: invokeinterface 291 3 0
    //   458: pop
    //   459: invokestatic 310	com/inmobi/commons/core/e/b:a	()Lcom/inmobi/commons/core/e/b;
    //   462: pop
    //   463: ldc_w 312
    //   466: astore 7
    //   468: ldc_w 314
    //   471: astore 8
    //   473: aload 7
    //   475: aload 8
    //   477: aload 5
    //   479: invokestatic 317	com/inmobi/commons/core/e/b:a	(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    //   482: goto +9 -> 491
    //   485: pop
    //   486: aload_1
    //   487: invokevirtual 299	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   490: pop
    //   491: aload 4
    //   493: astore_1
    //   494: goto +104 -> 598
    //   497: astore_1
    //   498: new 66	com/inmobi/commons/core/network/d
    //   501: astore 4
    //   503: aload_0
    //   504: getfield 19	com/inmobi/commons/core/network/b:b	Lcom/inmobi/commons/core/network/c;
    //   507: astore 5
    //   509: aload 4
    //   511: aload 5
    //   513: invokespecial 197	com/inmobi/commons/core/network/d:<init>	(Lcom/inmobi/commons/core/network/c;)V
    //   516: new 53	com/inmobi/commons/core/network/NetworkError
    //   519: astore 5
    //   521: getstatic 320	com/inmobi/commons/core/network/NetworkError$ErrorCode:NETWORK_IO_ERROR	Lcom/inmobi/commons/core/network/NetworkError$ErrorCode;
    //   524: astore 7
    //   526: aload_1
    //   527: invokevirtual 323	java/io/IOException:getLocalizedMessage	()Ljava/lang/String;
    //   530: astore_1
    //   531: aload 5
    //   533: aload 7
    //   535: aload_1
    //   536: invokespecial 64	com/inmobi/commons/core/network/NetworkError:<init>	(Lcom/inmobi/commons/core/network/NetworkError$ErrorCode;Ljava/lang/String;)V
    //   539: aload 4
    //   541: aload 5
    //   543: putfield 69	com/inmobi/commons/core/network/d:b	Lcom/inmobi/commons/core/network/NetworkError;
    //   546: aload 4
    //   548: astore_1
    //   549: goto +49 -> 598
    //   552: new 66	com/inmobi/commons/core/network/d
    //   555: astore_1
    //   556: aload_0
    //   557: getfield 19	com/inmobi/commons/core/network/b:b	Lcom/inmobi/commons/core/network/c;
    //   560: astore 4
    //   562: aload_1
    //   563: aload 4
    //   565: invokespecial 197	com/inmobi/commons/core/network/d:<init>	(Lcom/inmobi/commons/core/network/c;)V
    //   568: new 53	com/inmobi/commons/core/network/NetworkError
    //   571: astore 4
    //   573: getstatic 326	com/inmobi/commons/core/network/NetworkError$ErrorCode:NETWORK_UNAVAILABLE_ERROR	Lcom/inmobi/commons/core/network/NetworkError$ErrorCode;
    //   576: astore 5
    //   578: ldc_w 328
    //   581: astore 7
    //   583: aload 4
    //   585: aload 5
    //   587: aload 7
    //   589: invokespecial 64	com/inmobi/commons/core/network/NetworkError:<init>	(Lcom/inmobi/commons/core/network/NetworkError$ErrorCode;Ljava/lang/String;)V
    //   592: aload_1
    //   593: aload 4
    //   595: putfield 69	com/inmobi/commons/core/network/d:b	Lcom/inmobi/commons/core/network/NetworkError;
    //   598: aload_1
    //   599: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	600	0	this	b
    //   11	282	1	localObject1	Object
    //   304	1	1	localObject2	Object
    //   312	7	1	localObject3	Object
    //   324	1	1	locald	d
    //   328	159	1	localException1	Exception
    //   493	1	1	localObject4	Object
    //   497	30	1	localIOException	IOException
    //   530	69	1	localObject5	Object
    //   16	6	2	i	int
    //   70	110	2	bool	boolean
    //   18	232	3	j	int
    //   32	562	4	localObject6	Object
    //   48	538	5	localObject7	Object
    //   207	3	6	k	int
    //   214	374	7	localObject8	Object
    //   267	209	8	localObject9	Object
    //   430	5	9	str	String
    //   485	1	18	localException2	Exception
    // Exception table:
    //   from	to	target	type
    //   292	296	304	finally
    //   253	256	312	finally
    //   258	261	312	finally
    //   263	267	312	finally
    //   269	274	312	finally
    //   278	283	312	finally
    //   285	290	312	finally
    //   75	79	328	java/lang/Exception
    //   80	84	328	java/lang/Exception
    //   85	88	328	java/lang/Exception
    //   92	96	328	java/lang/Exception
    //   96	101	328	java/lang/Exception
    //   102	106	328	java/lang/Exception
    //   107	111	328	java/lang/Exception
    //   112	116	328	java/lang/Exception
    //   118	122	328	java/lang/Exception
    //   123	127	328	java/lang/Exception
    //   127	131	328	java/lang/Exception
    //   132	136	328	java/lang/Exception
    //   141	145	328	java/lang/Exception
    //   152	156	328	java/lang/Exception
    //   159	163	328	java/lang/Exception
    //   165	170	328	java/lang/Exception
    //   173	178	328	java/lang/Exception
    //   183	187	328	java/lang/Exception
    //   188	192	328	java/lang/Exception
    //   193	197	328	java/lang/Exception
    //   203	207	328	java/lang/Exception
    //   209	214	328	java/lang/Exception
    //   220	225	328	java/lang/Exception
    //   225	229	328	java/lang/Exception
    //   243	248	328	java/lang/Exception
    //   296	301	328	java/lang/Exception
    //   313	318	328	java/lang/Exception
    //   318	320	328	java/lang/Exception
    //   320	324	328	java/lang/Exception
    //   379	382	485	java/lang/Exception
    //   384	389	485	java/lang/Exception
    //   403	411	485	java/lang/Exception
    //   416	419	485	java/lang/Exception
    //   421	426	485	java/lang/Exception
    //   426	430	485	java/lang/Exception
    //   434	440	485	java/lang/Exception
    //   440	445	485	java/lang/Exception
    //   451	459	485	java/lang/Exception
    //   459	463	485	java/lang/Exception
    //   477	482	485	java/lang/Exception
    //   75	79	497	java/io/IOException
    //   80	84	497	java/io/IOException
    //   85	88	497	java/io/IOException
    //   92	96	497	java/io/IOException
    //   96	101	497	java/io/IOException
    //   102	106	497	java/io/IOException
    //   107	111	497	java/io/IOException
    //   112	116	497	java/io/IOException
    //   118	122	497	java/io/IOException
    //   123	127	497	java/io/IOException
    //   127	131	497	java/io/IOException
    //   132	136	497	java/io/IOException
    //   141	145	497	java/io/IOException
    //   152	156	497	java/io/IOException
    //   159	163	497	java/io/IOException
    //   165	170	497	java/io/IOException
    //   173	178	497	java/io/IOException
    //   183	187	497	java/io/IOException
    //   188	192	497	java/io/IOException
    //   193	197	497	java/io/IOException
    //   203	207	497	java/io/IOException
    //   209	214	497	java/io/IOException
    //   220	225	497	java/io/IOException
    //   225	229	497	java/io/IOException
    //   243	248	497	java/io/IOException
    //   296	301	497	java/io/IOException
    //   313	318	497	java/io/IOException
    //   318	320	497	java/io/IOException
    //   320	324	497	java/io/IOException
  }
  
  protected d b()
  {
    locald = new com/inmobi/commons/core/network/d;
    Object localObject1 = b;
    locald.<init>((c)localObject1);
    try
    {
      localObject1 = c;
      int i = ((HttpURLConnection)localObject1).getResponseCode();
      int j = 200;
      if (i == j)
      {
        i = 0;
        localObject1 = null;
      }
      Object localObject3;
      Object localObject4;
      Object localObject5;
      try
      {
        a(locald, false);
        break label167;
        localObject3 = NetworkError.ErrorCode.fromValue(i);
        localObject4 = NetworkError.ErrorCode.BAD_REQUEST;
        if (localObject3 == localObject4)
        {
          i = 1;
          a(locald, i);
          localObject1 = locald.b();
          localObject1 = a((String)localObject1);
          localObject4 = new com/inmobi/commons/core/network/NetworkError;
          ((NetworkError)localObject4).<init>((NetworkError.ErrorCode)localObject3, (String)localObject1);
          b = ((NetworkError)localObject4);
        }
        else
        {
          if (localObject3 == null) {
            localObject3 = NetworkError.ErrorCode.UNKNOWN_ERROR;
          }
          localObject4 = new com/inmobi/commons/core/network/NetworkError;
          localObject5 = "HTTP:";
          localObject1 = String.valueOf(i);
          localObject1 = ((String)localObject5).concat((String)localObject1);
          ((NetworkError)localObject4).<init>((NetworkError.ErrorCode)localObject3, (String)localObject1);
          b = ((NetworkError)localObject4);
          localObject1 = c;
          localObject1 = ((HttpURLConnection)localObject1).getHeaderFields();
          d = ((Map)localObject1);
        }
      }
      finally
      {
        label167:
        localObject3 = c;
        ((HttpURLConnection)localObject3).disconnect();
      }
      String str;
      NetworkError localNetworkError;
      return locald;
    }
    catch (Exception localException1)
    {
      localObject3 = new com/inmobi/commons/core/network/NetworkError;
      localObject4 = NetworkError.ErrorCode.UNKNOWN_ERROR;
      localObject5 = ((NetworkError.ErrorCode)localObject4).toString();
      ((NetworkError)localObject3).<init>((NetworkError.ErrorCode)localObject4, (String)localObject5);
      b = ((NetworkError)localObject3);
      try
      {
        localObject3 = new java/util/HashMap;
        ((HashMap)localObject3).<init>();
        localObject4 = "type";
        localObject5 = "GenericException";
        ((Map)localObject3).put(localObject4, localObject5);
        localObject4 = "message";
        localObject5 = new java/lang/StringBuilder;
        ((StringBuilder)localObject5).<init>();
        str = localException1.getMessage();
        ((StringBuilder)localObject5).append(str);
        localObject5 = ((StringBuilder)localObject5).toString();
        ((Map)localObject3).put(localObject4, localObject5);
        com.inmobi.commons.core.e.b.a();
        localObject4 = "root";
        localObject5 = "ExceptionCaught";
        com.inmobi.commons.core.e.b.a((String)localObject4, (String)localObject5, (Map)localObject3);
      }
      catch (Exception localException2)
      {
        localException1.getMessage();
      }
    }
    catch (OutOfMemoryError localOutOfMemoryError)
    {
      localNetworkError = new com/inmobi/commons/core/network/NetworkError;
      localObject3 = NetworkError.ErrorCode.OUT_OF_MEMORY_ERROR;
      localObject4 = ((NetworkError.ErrorCode)localObject3).toString();
      localNetworkError.<init>((NetworkError.ErrorCode)localObject3, (String)localObject4);
      b = localNetworkError;
    }
    catch (IOException localIOException)
    {
      localNetworkError = new com/inmobi/commons/core/network/NetworkError;
      localObject3 = NetworkError.ErrorCode.NETWORK_IO_ERROR;
      localObject4 = ((NetworkError.ErrorCode)localObject3).toString();
      localNetworkError.<init>((NetworkError.ErrorCode)localObject3, (String)localObject4);
      b = localNetworkError;
    }
    catch (SocketTimeoutException localSocketTimeoutException)
    {
      localNetworkError = new com/inmobi/commons/core/network/NetworkError;
      localObject3 = NetworkError.ErrorCode.HTTP_GATEWAY_TIMEOUT;
      localObject4 = ((NetworkError.ErrorCode)localObject3).toString();
      localNetworkError.<init>((NetworkError.ErrorCode)localObject3, (String)localObject4);
      b = localNetworkError;
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.network.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */