package com.inmobi.commons.core.network;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.Map;

public final class f
  extends b
{
  public f(c paramc)
  {
    super(paramc);
  }
  
  protected final d b()
  {
    locald = new com/inmobi/commons/core/network/d;
    Object localObject1 = b;
    locald.<init>((c)localObject1);
    try
    {
      localObject1 = c;
      ((HttpURLConnection)localObject1).getResponseCode();
      Object localObject3;
      try
      {
        localObject1 = c;
        int i = ((HttpURLConnection)localObject1).getContentLength();
        c = i;
      }
      finally
      {
        localObject3 = c;
        ((HttpURLConnection)localObject3).disconnect();
      }
      Object localObject4;
      Object localObject5;
      String str;
      NetworkError localNetworkError;
      return locald;
    }
    catch (Exception localException1)
    {
      localObject3 = new com/inmobi/commons/core/network/NetworkError;
      localObject4 = NetworkError.ErrorCode.UNKNOWN_ERROR;
      localObject5 = ((NetworkError.ErrorCode)localObject4).toString();
      ((NetworkError)localObject3).<init>((NetworkError.ErrorCode)localObject4, (String)localObject5);
      b = ((NetworkError)localObject3);
      try
      {
        localObject3 = new java/util/HashMap;
        ((HashMap)localObject3).<init>();
        localObject4 = "type";
        localObject5 = "GenericException";
        ((Map)localObject3).put(localObject4, localObject5);
        localObject4 = "message";
        localObject5 = new java/lang/StringBuilder;
        ((StringBuilder)localObject5).<init>();
        str = localException1.getMessage();
        ((StringBuilder)localObject5).append(str);
        localObject5 = ((StringBuilder)localObject5).toString();
        ((Map)localObject3).put(localObject4, localObject5);
        com.inmobi.commons.core.e.b.a();
        localObject4 = "root";
        localObject5 = "ExceptionCaught";
        com.inmobi.commons.core.e.b.a((String)localObject4, (String)localObject5, (Map)localObject3);
      }
      catch (Exception localException2)
      {
        localException1.getMessage();
      }
    }
    catch (OutOfMemoryError localOutOfMemoryError)
    {
      localNetworkError = new com/inmobi/commons/core/network/NetworkError;
      localObject3 = NetworkError.ErrorCode.OUT_OF_MEMORY_ERROR;
      localObject4 = ((NetworkError.ErrorCode)localObject3).toString();
      localNetworkError.<init>((NetworkError.ErrorCode)localObject3, (String)localObject4);
      b = localNetworkError;
    }
    catch (IOException localIOException)
    {
      localNetworkError = new com/inmobi/commons/core/network/NetworkError;
      localObject3 = NetworkError.ErrorCode.NETWORK_IO_ERROR;
      localObject4 = ((NetworkError.ErrorCode)localObject3).toString();
      localNetworkError.<init>((NetworkError.ErrorCode)localObject3, (String)localObject4);
      b = localNetworkError;
    }
    catch (SocketTimeoutException localSocketTimeoutException)
    {
      localNetworkError = new com/inmobi/commons/core/network/NetworkError;
      localObject3 = NetworkError.ErrorCode.HTTP_GATEWAY_TIMEOUT;
      localObject4 = ((NetworkError.ErrorCode)localObject3).toString();
      localNetworkError.<init>((NetworkError.ErrorCode)localObject3, (String)localObject4);
      b = localNetworkError;
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.network.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */