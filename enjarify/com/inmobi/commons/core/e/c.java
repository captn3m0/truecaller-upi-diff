package com.inmobi.commons.core.e;

import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class c
{
  int a;
  boolean b;
  private String c;
  private Map d;
  
  public c()
  {
    a = 0;
    c = "telemetry";
    b = false;
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    d = localHashMap;
  }
  
  public c(String paramString, JSONObject paramJSONObject, c paramc)
  {
    int i = 0;
    a = 0;
    c = "telemetry";
    b = false;
    Object localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    d = ((Map)localObject);
    if (paramJSONObject == null)
    {
      a(paramString);
      return;
    }
    if (paramString != null) {}
    try
    {
      localObject = paramString.trim();
      int j = ((String)localObject).length();
      if (j != 0) {
        localObject = paramString;
      } else {
        localObject = c;
      }
      c = ((String)localObject);
      localObject = "enabled";
      boolean bool1 = paramJSONObject.has((String)localObject);
      if (bool1)
      {
        localObject = "enabled";
        bool1 = paramJSONObject.getBoolean((String)localObject);
        if (!bool1)
        {
          bool1 = false;
          localObject = null;
          break label141;
        }
      }
      bool1 = true;
      label141:
      b = bool1;
      localObject = "samplingFactor";
      bool1 = paramJSONObject.has((String)localObject);
      int m;
      if (bool1)
      {
        paramc = "samplingFactor";
        m = paramJSONObject.getInt(paramc);
      }
      else
      {
        m = a;
      }
      a = m;
      paramc = new java/util/HashMap;
      paramc.<init>();
      d = paramc;
      paramc = "events";
      boolean bool2 = paramJSONObject.has(paramc);
      if (bool2)
      {
        paramc = "events";
        paramJSONObject = paramJSONObject.getJSONArray(paramc);
        for (;;)
        {
          int n = paramJSONObject.length();
          if (i >= n) {
            break;
          }
          paramc = new com/inmobi/commons/core/e/c$a;
          paramc.<init>();
          localObject = paramJSONObject.getJSONObject(i);
          String str = "type";
          str = ((JSONObject)localObject).getString(str);
          a = str;
          str = "samplingFactor";
          boolean bool3 = ((JSONObject)localObject).has(str);
          int k;
          if (bool3)
          {
            str = "samplingFactor";
            k = ((JSONObject)localObject).getInt(str);
          }
          else
          {
            k = a;
          }
          b = k;
          localObject = d;
          str = a;
          ((Map)localObject).put(str, paramc);
          i += 1;
        }
      }
      return;
    }
    catch (JSONException localJSONException)
    {
      a(paramString);
    }
  }
  
  private void a(String paramString)
  {
    b = true;
    c = paramString;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.e.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */