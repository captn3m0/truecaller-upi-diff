package com.inmobi.commons.core.e;

import com.inmobi.commons.core.configs.a;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;

class d
  extends a
{
  private static final String j = "d";
  c a;
  String b = "https://telemetry.sdk.inmobi.com/metrics";
  long c = 150L;
  int d = 3;
  int e;
  long f;
  long g;
  d.a h;
  d.a i;
  
  public d()
  {
    int k = 1000;
    e = k;
    f = 259200L;
    long l1 = 86400L;
    g = l1;
    Object localObject = new com/inmobi/commons/core/e/c;
    ((c)localObject).<init>();
    a = ((c)localObject);
    try
    {
      localObject = new org/json/JSONObject;
      ((JSONObject)localObject).<init>();
      JSONObject localJSONObject = new org/json/JSONObject;
      localJSONObject.<init>();
      String str = "retryInterval";
      long l2 = 10;
      localJSONObject.put(str, l2);
      str = "maxBatchSize";
      int m = 25;
      localJSONObject.put(str, m);
      str = "wifi";
      ((JSONObject)localObject).put(str, localJSONObject);
      localJSONObject = new org/json/JSONObject;
      localJSONObject.<init>();
      str = "retryInterval";
      localJSONObject.put(str, l2);
      str = "maxBatchSize";
      localJSONObject.put(str, m);
      str = "others";
      ((JSONObject)localObject).put(str, localJSONObject);
      b((JSONObject)localObject);
      return;
    }
    catch (JSONException localJSONException) {}
  }
  
  private void b(JSONObject paramJSONObject)
  {
    Iterator localIterator = paramJSONObject.keys();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      String str1 = (String)localIterator.next();
      JSONObject localJSONObject = paramJSONObject.getJSONObject(str1);
      d.a locala = new com/inmobi/commons/core/e/d$a;
      locala.<init>(this);
      long l = localJSONObject.getLong("retryInterval");
      a = l;
      String str2 = "maxBatchSize";
      int k = localJSONObject.getInt(str2);
      b = k;
      k = -1;
      int m = str1.hashCode();
      int n = -1068855134;
      if (m != n)
      {
        n = -1006804125;
        if (m != n)
        {
          n = 3649301;
          if (m == n)
          {
            str2 = "wifi";
            bool = str1.equals(str2);
            if (bool)
            {
              k = 0;
              localJSONObject = null;
            }
          }
        }
        else
        {
          str2 = "others";
          bool = str1.equals(str2);
          if (bool) {
            k = 2;
          }
        }
      }
      else
      {
        str2 = "mobile";
        bool = str1.equals(str2);
        if (bool) {
          k = 1;
        }
      }
      if (k != 0) {
        h = locala;
      } else {
        i = locala;
      }
    }
  }
  
  public final String a()
  {
    return "telemetry";
  }
  
  public final void a(JSONObject paramJSONObject)
  {
    super.a(paramJSONObject);
    Object localObject = paramJSONObject.getJSONObject("base");
    c localc = a;
    boolean bool = ((JSONObject)localObject).getBoolean("enabled");
    b = bool;
    localc = a;
    int k = ((JSONObject)localObject).getInt("samplingFactor");
    a = k;
    localObject = paramJSONObject.getString("telemetryUrl");
    b = ((String)localObject);
    long l = paramJSONObject.getLong("processingInterval");
    c = l;
    k = paramJSONObject.getInt("maxRetryCount");
    d = k;
    k = paramJSONObject.getInt("maxEventsToPersist");
    e = k;
    l = paramJSONObject.getLong("eventTTL");
    f = l;
    l = paramJSONObject.getLong("txLatency");
    g = l;
    paramJSONObject = paramJSONObject.getJSONObject("networkType");
    b(paramJSONObject);
  }
  
  public final JSONObject b()
  {
    JSONObject localJSONObject1 = super.b();
    JSONObject localJSONObject2 = new org/json/JSONObject;
    localJSONObject2.<init>();
    boolean bool = a.b;
    localJSONObject2.put("enabled", bool);
    int k = a.a;
    localJSONObject2.put("samplingFactor", k);
    localJSONObject1.put("base", localJSONObject2);
    Object localObject = b;
    localJSONObject1.put("telemetryUrl", localObject);
    long l1 = c;
    localJSONObject1.put("processingInterval", l1);
    int m = d;
    localJSONObject1.put("maxRetryCount", m);
    m = e;
    localJSONObject1.put("maxEventsToPersist", m);
    l1 = f;
    localJSONObject1.put("eventTTL", l1);
    l1 = g;
    localJSONObject1.put("txLatency", l1);
    localJSONObject2 = new org/json/JSONObject;
    localJSONObject2.<init>();
    localObject = i;
    JSONObject localJSONObject3 = new org/json/JSONObject;
    localJSONObject3.<init>();
    long l2 = a;
    localJSONObject3.put("retryInterval", l2);
    m = b;
    localJSONObject3.put("maxBatchSize", m);
    localJSONObject2.put("wifi", localJSONObject3);
    localObject = h;
    localJSONObject3 = new org/json/JSONObject;
    localJSONObject3.<init>();
    l2 = a;
    localJSONObject3.put("retryInterval", l2);
    m = b;
    localJSONObject3.put("maxBatchSize", m);
    localJSONObject2.put("others", localJSONObject3);
    localJSONObject1.put("networkType", localJSONObject2);
    return localJSONObject1;
  }
  
  public final boolean c()
  {
    Object localObject = a;
    if (localObject == null) {
      return false;
    }
    localObject = b.trim();
    int k = ((String)localObject).length();
    if (k != 0)
    {
      localObject = b;
      String str = "http://";
      boolean bool1 = ((String)localObject).startsWith(str);
      if (!bool1)
      {
        localObject = b;
        str = "https://";
        bool1 = ((String)localObject).startsWith(str);
        if (!bool1) {}
      }
      else
      {
        long l1 = g;
        long l2 = c;
        bool1 = l1 < l2;
        if (!bool1)
        {
          long l3 = f;
          bool1 = l1 < l3;
          if (!bool1)
          {
            bool1 = l3 < l2;
            if (!bool1)
            {
              localObject = h;
              bool1 = ((d.a)localObject).a();
              if (bool1)
              {
                localObject = i;
                bool1 = ((d.a)localObject).a();
                if (bool1)
                {
                  l1 = c;
                  l2 = 0L;
                  bool1 = l1 < l2;
                  if (!bool1)
                  {
                    int m = d;
                    if (m >= 0)
                    {
                      l1 = g;
                      boolean bool2 = l1 < l2;
                      if (bool2)
                      {
                        l1 = f;
                        bool2 = l1 < l2;
                        if (bool2)
                        {
                          int n = e;
                          if (n > 0) {
                            return true;
                          }
                        }
                      }
                    }
                  }
                  return false;
                }
              }
              return false;
            }
          }
        }
        return false;
      }
    }
    return false;
  }
  
  public final a d()
  {
    d locald = new com/inmobi/commons/core/e/d;
    locald.<init>();
    return locald;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.e.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */