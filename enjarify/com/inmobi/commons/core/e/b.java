package com.inmobi.commons.core.e;

import com.inmobi.commons.core.configs.b.c;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class b
  implements com.inmobi.commons.core.b.e, b.c
{
  public static AtomicBoolean b;
  private static final String c = "b";
  private static final Object d;
  private static volatile b e;
  private static Map f;
  public ExecutorService a;
  private d g;
  private e h;
  private String i;
  private com.inmobi.commons.core.b.d j;
  
  static
  {
    Object localObject = new java/lang/Object;
    localObject.<init>();
    d = localObject;
    localObject = new java/util/concurrent/atomic/AtomicBoolean;
    ((AtomicBoolean)localObject).<init>(false);
    b = (AtomicBoolean)localObject;
  }
  
  private b()
  {
    Object localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    f = (Map)localObject;
    localObject = new com/inmobi/commons/core/e/d;
    ((d)localObject).<init>();
    g = ((d)localObject);
    c localc = g.a;
    a("telemetry", localc);
    localObject = g.b;
    i = ((String)localObject);
    localObject = new com/inmobi/commons/core/e/e;
    ((e)localObject).<init>();
    h = ((e)localObject);
    localObject = Executors.newSingleThreadExecutor();
    a = ((ExecutorService)localObject);
  }
  
  public static b a()
  {
    b localb1 = e;
    if (localb1 == null) {
      synchronized (d)
      {
        localb1 = e;
        if (localb1 == null)
        {
          localb1 = new com/inmobi/commons/core/e/b;
          localb1.<init>();
          e = localb1;
        }
      }
    }
    return localb2;
  }
  
  private static String a(List paramList)
  {
    try
    {
      Object localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      Object localObject2 = null;
      localObject2 = com.inmobi.commons.core.utilities.b.b.a(false);
      ((Map)localObject1).putAll((Map)localObject2);
      localObject2 = "im-accid";
      Object localObject3 = com.inmobi.commons.a.a.e();
      ((Map)localObject1).put(localObject2, localObject3);
      localObject2 = "version";
      localObject3 = "2.0.0";
      ((Map)localObject1).put(localObject2, localObject3);
      localObject2 = "component";
      localObject3 = "telemetry";
      ((Map)localObject1).put(localObject2, localObject3);
      localObject2 = "mk-version";
      localObject3 = com.inmobi.commons.a.b.a();
      ((Map)localObject1).put(localObject2, localObject3);
      localObject2 = com.inmobi.commons.core.utilities.b.a.a();
      localObject2 = b;
      ((Map)localObject1).putAll((Map)localObject2);
      localObject2 = new org/json/JSONObject;
      ((JSONObject)localObject2).<init>((Map)localObject1);
      localObject1 = new org/json/JSONArray;
      ((JSONArray)localObject1).<init>();
      paramList = paramList.iterator();
      for (;;)
      {
        boolean bool1 = paramList.hasNext();
        if (!bool1) {
          break;
        }
        localObject3 = paramList.next();
        localObject3 = (f)localObject3;
        JSONObject localJSONObject = new org/json/JSONObject;
        localJSONObject.<init>();
        String str1 = "eventId";
        String str2 = b;
        localJSONObject.put(str1, str2);
        str1 = "eventType";
        str2 = c;
        localJSONObject.put(str1, str2);
        str1 = ((f)localObject3).a();
        str1 = str1.trim();
        boolean bool2 = str1.isEmpty();
        if (!bool2)
        {
          str1 = "payload";
          str2 = ((f)localObject3).a();
          localJSONObject.put(str1, str2);
        }
        str1 = "componentType";
        str2 = d;
        localJSONObject.put(str1, str2);
        str1 = "ts";
        long l = e;
        localJSONObject.put(str1, l);
        ((JSONArray)localObject1).put(localJSONObject);
      }
      paramList = "telemetry";
      ((JSONObject)localObject2).put(paramList, localObject1);
      return ((JSONObject)localObject2).toString();
    }
    catch (JSONException localJSONException) {}
    return null;
  }
  
  private void a(String paramString, c paramc)
  {
    if (paramString != null)
    {
      Object localObject = paramString.trim();
      String str = "";
      boolean bool = ((String)localObject).equals(str);
      if (!bool)
      {
        if (paramc != null)
        {
          f.put(paramString, paramc);
          return;
        }
        paramc = f;
        localObject = new com/inmobi/commons/core/e/c;
        c localc = g.a;
        ((c)localObject).<init>(paramString, null, localc);
        paramc.put(paramString, localObject);
        return;
      }
    }
  }
  
  public static void a(String paramString1, String paramString2, Map paramMap)
  {
    for (;;)
    {
      try
      {
        localf = new com/inmobi/commons/core/e/f;
        localf.<init>(paramString1, paramString2);
        if (paramMap != null)
        {
          boolean bool1 = paramMap.isEmpty();
          if (bool1) {}
        }
      }
      catch (Exception localException)
      {
        f localf;
        boolean bool2;
        localException.getMessage();
        return;
      }
      try
      {
        paramString1 = new org/json/JSONObject;
        paramString1.<init>();
        paramString2 = paramMap.entrySet();
        paramString2 = paramString2.iterator();
        bool2 = paramString2.hasNext();
        if (bool2)
        {
          paramMap = paramString2.next();
          paramMap = (Map.Entry)paramMap;
          Object localObject = paramMap.getKey();
          localObject = localObject.toString();
          paramMap = paramMap.getValue();
          paramString1.put((String)localObject, paramMap);
        }
        else
        {
          paramString1 = paramString1.toString();
          f = paramString1;
        }
      }
      catch (JSONException localJSONException) {}
    }
    paramString1 = a();
    paramString1.a(localf);
  }
  
  private static c c(f paramf)
  {
    a();
    paramf = d;
    if (paramf != null)
    {
      String str1 = paramf.trim();
      String str2 = "";
      boolean bool = str1.equals(str2);
      if (!bool) {
        return (c)f.get(paramf);
      }
    }
    return null;
  }
  
  public final com.inmobi.commons.core.b.c a(String paramString)
  {
    int k = com.inmobi.commons.core.utilities.b.b.a();
    int m = 1;
    if (k != m)
    {
      k = g.h.b;
      paramString = e.a(k);
    }
    else
    {
      k = g.i.b;
      paramString = e.a(k);
    }
    boolean bool1 = paramString.isEmpty();
    if (!bool1)
    {
      ArrayList localArrayList = new java/util/ArrayList;
      localArrayList.<init>();
      localObject = paramString.iterator();
      for (;;)
      {
        boolean bool2 = ((Iterator)localObject).hasNext();
        if (!bool2) {
          break;
        }
        int n = nexta;
        Integer localInteger = Integer.valueOf(n);
        localArrayList.add(localInteger);
      }
      paramString = a(paramString);
      if (paramString != null)
      {
        localObject = new com/inmobi/commons/core/b/c;
        ((com.inmobi.commons.core.b.c)localObject).<init>(localArrayList, paramString, m);
        break label155;
      }
    }
    Object localObject = null;
    label155:
    return (com.inmobi.commons.core.b.c)localObject;
  }
  
  public final void a(com.inmobi.commons.core.configs.a parama)
  {
    parama = (d)parama;
    g = parama;
    parama = g.b;
    i = parama;
  }
  
  public final void a(f paramf)
  {
    Object localObject = c(paramf);
    if (localObject != null)
    {
      boolean bool = b;
      if (bool)
      {
        localObject = g.a;
        bool = b;
        if (bool)
        {
          localObject = a;
          b.3 local3 = new com/inmobi/commons/core/e/b$3;
          local3.<init>(this, paramf);
          ((ExecutorService)localObject).execute(local3);
          return;
        }
      }
    }
  }
  
  public final void a(String paramString, JSONObject paramJSONObject)
  {
    c localc1 = new com/inmobi/commons/core/e/c;
    c localc2 = g.a;
    localc1.<init>(paramString, paramJSONObject, localc2);
    a(paramString, localc1);
  }
  
  public final void b()
  {
    b.set(false);
    Object localObject1 = com.inmobi.commons.core.configs.b.a();
    Object localObject2 = g;
    ((com.inmobi.commons.core.configs.b)localObject1).a((com.inmobi.commons.core.configs.a)localObject2, this);
    localObject2 = g.a;
    a("telemetry", (c)localObject2);
    localObject1 = g.b;
    i = ((String)localObject1);
    localObject1 = a;
    localObject2 = new com/inmobi/commons/core/e/b$1;
    ((b.1)localObject2).<init>(this);
    ((ExecutorService)localObject1).execute((Runnable)localObject2);
  }
  
  public final void b(f paramf)
  {
    Object localObject = c(paramf);
    if (localObject != null)
    {
      boolean bool = b;
      if (bool)
      {
        localObject = g.a;
        bool = b;
        if (bool)
        {
          localObject = h;
          long l = g.f;
          String str1 = "default";
          ((e)localObject).b(l, str1);
          localObject = g;
          int k = e;
          e locale = h;
          String str2 = "default";
          int m = locale.a(str2) + 1 - k;
          if (m >= 0) {
            e.a();
          }
          e.a(paramf);
          return;
        }
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.e.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */