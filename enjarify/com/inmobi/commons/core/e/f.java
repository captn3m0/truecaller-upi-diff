package com.inmobi.commons.core.e;

import android.content.ContentValues;
import java.util.UUID;

public class f
{
  private static final String g = "f";
  int a;
  String b;
  String c;
  String d;
  long e;
  String f;
  
  public f(String paramString1, String paramString2)
  {
    String str = UUID.randomUUID().toString();
    b = str;
    d = paramString1;
    c = paramString2;
    f = null;
    long l = System.currentTimeMillis();
    e = l;
  }
  
  private f(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    b = paramString1;
    d = paramString2;
    c = paramString3;
    f = paramString4;
    long l = System.currentTimeMillis();
    e = l;
  }
  
  public static f a(ContentValues paramContentValues)
  {
    String str1 = paramContentValues.getAsString("eventId");
    String str2 = paramContentValues.getAsString("eventType");
    String str3 = paramContentValues.getAsString("componentType");
    String str4 = paramContentValues.getAsString("payload");
    long l = Long.valueOf(paramContentValues.getAsString("ts")).longValue();
    f localf = new com/inmobi/commons/core/e/f;
    localf.<init>(str1, str3, str2, str4);
    e = l;
    int i = paramContentValues.getAsInteger("id").intValue();
    a = i;
    return localf;
  }
  
  public final String a()
  {
    String str = f;
    if (str == null) {
      str = "";
    }
    return str;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    String str = c;
    localStringBuilder.append(str);
    localStringBuilder.append("@");
    str = d;
    localStringBuilder.append(str);
    localStringBuilder.append(" ");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.e.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */