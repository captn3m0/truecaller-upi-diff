package com.inmobi.commons.core.e;

import android.util.Log;
import com.inmobi.commons.core.a.d;
import org.json.JSONException;
import org.json.JSONObject;

public final class a
  extends d
{
  private static final String g = f.class.getSimpleName();
  
  public a(Throwable paramThrowable)
  {
    super("crashReporting", "catchEvent");
    JSONObject localJSONObject = new org/json/JSONObject;
    localJSONObject.<init>();
    Object localObject1 = "name";
    try
    {
      Object localObject2 = paramThrowable.getClass();
      localObject2 = ((Class)localObject2).getSimpleName();
      localJSONObject.put((String)localObject1, localObject2);
      localObject1 = "message";
      localObject2 = paramThrowable.getMessage();
      localJSONObject.put((String)localObject1, localObject2);
      localObject1 = "stack";
      paramThrowable = Log.getStackTraceString(paramThrowable);
      localJSONObject.put((String)localObject1, paramThrowable);
      paramThrowable = "thread";
      localObject1 = Thread.currentThread();
      localObject1 = ((Thread)localObject1).getName();
      localJSONObject.put(paramThrowable, localObject1);
      paramThrowable = localJSONObject.toString();
      f = paramThrowable;
      return;
    }
    catch (JSONException localJSONException) {}
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.e.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */