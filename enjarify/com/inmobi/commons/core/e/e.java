package com.inmobi.commons.core.e;

import android.content.ContentValues;
import com.inmobi.commons.a.a;
import com.inmobi.commons.core.d.c;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class e
  extends com.inmobi.commons.core.b.b
{
  private static final String a = "e";
  
  public e()
  {
    com.inmobi.commons.core.d.b localb = com.inmobi.commons.core.d.b.a();
    localb.a("telemetry", "(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, componentType TEXT NOT NULL, eventId TEXT NOT NULL, eventType TEXT NOT NULL, payload TEXT NOT NULL, ts TEXT NOT NULL)");
    localb.b();
  }
  
  public static List a(int paramInt)
  {
    com.inmobi.commons.core.d.b localb = com.inmobi.commons.core.d.b.a();
    Object localObject1 = "telemetry";
    String str1 = "ts ASC";
    String str2 = String.valueOf(paramInt);
    Object localObject2 = localb;
    Object localObject3 = localb.a((String)localObject1, null, null, null, null, null, str1, str2);
    localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>();
    localb.b();
    localObject3 = ((List)localObject3).iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject3).hasNext();
      if (!bool) {
        break;
      }
      localObject1 = f.a((ContentValues)((Iterator)localObject3).next());
      ((List)localObject2).add(localObject1);
    }
    return (List)localObject2;
  }
  
  public static void a()
  {
    com.inmobi.commons.core.d.b localb = com.inmobi.commons.core.d.b.a();
    String str1 = "telemetry";
    String str2 = "ts ASC";
    String str3 = "1";
    StringBuilder localStringBuilder = null;
    String str4 = null;
    Object localObject = localb;
    localObject = localb.a(str1, null, null, null, null, null, str2, str3);
    boolean bool = ((List)localObject).isEmpty();
    if (!bool)
    {
      bool = false;
      localObject = ((ContentValues)((List)localObject).get(0)).getAsString("id");
      str1 = "telemetry";
      localStringBuilder = new java/lang/StringBuilder;
      str4 = "id IN (";
      localStringBuilder.<init>(str4);
      localStringBuilder.append((String)localObject);
      localStringBuilder.append(")");
      localObject = localStringBuilder.toString();
      localStringBuilder = null;
      localb.a(str1, (String)localObject, null);
    }
    localb.b();
  }
  
  public static void a(f paramf)
  {
    com.inmobi.commons.core.d.b localb = com.inmobi.commons.core.d.b.a();
    ContentValues localContentValues = new android/content/ContentValues;
    localContentValues.<init>();
    String str = b;
    localContentValues.put("eventId", str);
    str = d;
    localContentValues.put("componentType", str);
    str = c;
    localContentValues.put("eventType", str);
    str = paramf.a();
    localContentValues.put("payload", str);
    paramf = String.valueOf(e);
    localContentValues.put("ts", paramf);
    localb.a("telemetry", localContentValues);
    localb.b();
  }
  
  public final int a(String paramString)
  {
    paramString = com.inmobi.commons.core.d.b.a();
    int i = paramString.a("telemetry");
    paramString.b();
    return i;
  }
  
  public final void a(List paramList)
  {
    boolean bool = paramList.isEmpty();
    if (bool) {
      return;
    }
    com.inmobi.commons.core.d.b localb = com.inmobi.commons.core.d.b.a();
    Object localObject1 = new java/lang/StringBuffer;
    ((StringBuffer)localObject1).<init>("");
    int i = 0;
    StringBuilder localStringBuilder = null;
    for (;;)
    {
      int j = paramList.size() + -1;
      if (i >= j) {
        break;
      }
      Object localObject2 = paramList.get(i);
      ((StringBuffer)localObject1).append(localObject2);
      localObject2 = ",";
      ((StringBuffer)localObject1).append((String)localObject2);
      i += 1;
    }
    i = paramList.size() + -1;
    paramList = String.valueOf(paramList.get(i));
    ((StringBuffer)localObject1).append(paramList);
    localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("id IN (");
    localStringBuilder.append(localObject1);
    localStringBuilder.append(")");
    localObject1 = localStringBuilder.toString();
    localb.a("telemetry", (String)localObject1, null);
    localb.b();
  }
  
  public final boolean a(long paramLong, String paramString)
  {
    int i = 1;
    Object localObject = a(i);
    int j = ((List)localObject).size();
    if (j > 0)
    {
      TimeUnit localTimeUnit = TimeUnit.MILLISECONDS;
      long l1 = System.currentTimeMillis();
      localObject = (f)((List)localObject).get(0);
      long l2 = e;
      l1 -= l2;
      long l3 = localTimeUnit.toSeconds(l1);
      boolean bool = l3 < paramLong;
      if (bool) {}
    }
    else
    {
      i = 0;
    }
    return i;
  }
  
  public final int b(long paramLong, String paramString)
  {
    paramString = com.inmobi.commons.core.d.b.a();
    long l = System.currentTimeMillis();
    paramLong *= 1000L;
    l -= paramLong;
    String[] arrayOfString = new String[1];
    String str = String.valueOf(l);
    arrayOfString[0] = str;
    int i = paramString.a("telemetry", "ts<?", arrayOfString);
    paramString.b();
    return i;
  }
  
  public final long b(String paramString)
  {
    boolean bool = a.a();
    long l = -1;
    if (bool) {
      return c.b("batch_processing_info").b("telemetry_last_batch_process", l);
    }
    return l;
  }
  
  public final void c(long paramLong, String paramString)
  {
    boolean bool = a.a();
    if (bool)
    {
      paramString = c.b("batch_processing_info");
      String str = "telemetry_last_batch_process";
      paramString.a(str, paramLong);
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.e.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */