package com.inmobi.commons.core.a;

import android.content.ContentValues;
import android.util.Log;
import java.util.UUID;
import org.json.JSONException;
import org.json.JSONObject;

public class d
{
  private static final String g = "d";
  int a;
  String b;
  String c;
  String d;
  long e;
  public String f;
  
  public d(String paramString1, String paramString2)
  {
    String str = UUID.randomUUID().toString();
    b = str;
    d = paramString1;
    c = paramString2;
    f = null;
    long l = System.currentTimeMillis();
    e = l;
  }
  
  private d(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    b = paramString1;
    d = paramString2;
    c = paramString3;
    f = paramString4;
    long l = System.currentTimeMillis();
    e = l;
  }
  
  public d(Thread paramThread, Throwable paramThrowable)
  {
    this("crashReporting", "CrashEvent");
    JSONObject localJSONObject = new org/json/JSONObject;
    localJSONObject.<init>();
    String str = "name";
    try
    {
      Object localObject = paramThrowable.getClass();
      localObject = ((Class)localObject).getSimpleName();
      localJSONObject.put(str, localObject);
      str = "message";
      localObject = paramThrowable.getMessage();
      localJSONObject.put(str, localObject);
      str = "stack";
      paramThrowable = Log.getStackTraceString(paramThrowable);
      localJSONObject.put(str, paramThrowable);
      paramThrowable = "thread";
      paramThread = paramThread.getName();
      localJSONObject.put(paramThrowable, paramThread);
      paramThread = localJSONObject.toString();
      f = paramThread;
      return;
    }
    catch (JSONException localJSONException) {}
  }
  
  public static d a(ContentValues paramContentValues)
  {
    String str1 = paramContentValues.getAsString("eventId");
    String str2 = paramContentValues.getAsString("eventType");
    String str3 = paramContentValues.getAsString("componentType");
    String str4 = paramContentValues.getAsString("payload");
    long l = Long.valueOf(paramContentValues.getAsString("ts")).longValue();
    d locald = new com/inmobi/commons/core/a/d;
    locald.<init>(str1, str3, str2, str4);
    e = l;
    int i = paramContentValues.getAsInteger("id").intValue();
    a = i;
    return locald;
  }
  
  public final String a()
  {
    String str = f;
    if (str == null) {
      str = "";
    }
    return str;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    String str = c;
    localStringBuilder.append(str);
    localStringBuilder.append("@");
    str = d;
    localStringBuilder.append(str);
    localStringBuilder.append(" ");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.a.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */