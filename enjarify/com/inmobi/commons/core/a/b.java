package com.inmobi.commons.core.a;

import com.inmobi.commons.core.configs.a;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;

public class b
  extends a
{
  private static final String m = "b";
  public String a = "https://crash-metrics.sdk.inmobi.com/trace";
  long b = 0L;
  int c = 3;
  int d = 50;
  long e = 2592000L;
  long f;
  boolean g;
  boolean h;
  JSONObject i;
  JSONObject j;
  b.a k;
  b.a l;
  
  public b()
  {
    long l1 = 86400L;
    f = l1;
    String str1 = null;
    g = false;
    h = false;
    try
    {
      JSONObject localJSONObject1 = new org/json/JSONObject;
      localJSONObject1.<init>();
      JSONObject localJSONObject2 = new org/json/JSONObject;
      localJSONObject2.<init>();
      String str2 = "retryInterval";
      long l2 = 10;
      localJSONObject2.put(str2, l2);
      str2 = "maxBatchSize";
      int n = 1;
      localJSONObject2.put(str2, n);
      str2 = "wifi";
      localJSONObject1.put(str2, localJSONObject2);
      localJSONObject2 = new org/json/JSONObject;
      localJSONObject2.<init>();
      str2 = "retryInterval";
      localJSONObject2.put(str2, l2);
      str2 = "maxBatchSize";
      localJSONObject2.put(str2, n);
      str2 = "others";
      localJSONObject1.put(str2, localJSONObject2);
      b(localJSONObject1);
      localJSONObject1 = new org/json/JSONObject;
      localJSONObject1.<init>();
      localJSONObject2 = new org/json/JSONObject;
      localJSONObject2.<init>();
      str2 = "enabled";
      localJSONObject2.put(str2, false);
      str2 = "samplingFactor";
      localJSONObject2.put(str2, 0);
      str2 = "catchEvent";
      localJSONObject1.put(str2, localJSONObject2);
      localJSONObject2 = new org/json/JSONObject;
      localJSONObject2.<init>();
      str2 = "enabled";
      localJSONObject2.put(str2, false);
      str2 = "samplingFactor";
      localJSONObject2.put(str2, 0);
      str1 = "crashEvent";
      localJSONObject1.put(str1, localJSONObject2);
      c(localJSONObject1);
      return;
    }
    catch (JSONException localJSONException) {}
  }
  
  private void b(JSONObject paramJSONObject)
  {
    Iterator localIterator = paramJSONObject.keys();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      String str1 = (String)localIterator.next();
      JSONObject localJSONObject = paramJSONObject.getJSONObject(str1);
      b.a locala = new com/inmobi/commons/core/a/b$a;
      locala.<init>(this);
      long l1 = localJSONObject.getLong("retryInterval");
      a = l1;
      String str2 = "maxBatchSize";
      int n = localJSONObject.getInt(str2);
      b = n;
      n = -1;
      int i1 = str1.hashCode();
      int i2 = -1068855134;
      if (i1 != i2)
      {
        i2 = -1006804125;
        if (i1 != i2)
        {
          i2 = 3649301;
          if (i1 == i2)
          {
            str2 = "wifi";
            bool = str1.equals(str2);
            if (bool)
            {
              n = 0;
              localJSONObject = null;
            }
          }
        }
        else
        {
          str2 = "others";
          bool = str1.equals(str2);
          if (bool) {
            n = 2;
          }
        }
      }
      else
      {
        str2 = "mobile";
        bool = str1.equals(str2);
        if (bool) {
          n = 1;
        }
      }
      if (n != 0) {
        k = locala;
      } else {
        l = locala;
      }
    }
  }
  
  private void c(JSONObject paramJSONObject)
  {
    Iterator localIterator = paramJSONObject.keys();
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      Object localObject = (String)localIterator.next();
      int n = -1;
      int i1 = ((String)localObject).hashCode();
      int i2 = -488533857;
      String str;
      boolean bool2;
      if (i1 != i2)
      {
        i2 = 1411010355;
        if (i1 == i2)
        {
          str = "crashEvent";
          bool2 = ((String)localObject).equals(str);
          if (bool2) {
            n = 1;
          }
        }
      }
      else
      {
        str = "catchEvent";
        bool2 = ((String)localObject).equals(str);
        if (bool2) {
          n = 0;
        }
      }
      switch (n)
      {
      default: 
        break;
      case 1: 
        localObject = paramJSONObject.getJSONObject((String)localObject);
        i = ((JSONObject)localObject);
        break;
      case 0: 
        localObject = paramJSONObject.getJSONObject((String)localObject);
        j = ((JSONObject)localObject);
      }
    }
  }
  
  public final String a()
  {
    return "crashReporting";
  }
  
  public final void a(JSONObject paramJSONObject)
  {
    super.a(paramJSONObject);
    Object localObject = paramJSONObject.getString("url");
    a = ((String)localObject);
    long l1 = paramJSONObject.getLong("processingInterval");
    b = l1;
    int n = paramJSONObject.getInt("maxRetryCount");
    c = n;
    n = paramJSONObject.getInt("maxEventsToPersist");
    d = n;
    l1 = paramJSONObject.getLong("eventTTL");
    e = l1;
    l1 = paramJSONObject.getLong("txLatency");
    f = l1;
    boolean bool = paramJSONObject.getBoolean("crashEnabled");
    g = bool;
    bool = paramJSONObject.getBoolean("catchEnabled");
    h = bool;
    localObject = paramJSONObject.getJSONObject("networkType");
    b((JSONObject)localObject);
    paramJSONObject = paramJSONObject.getJSONObject("telemetry");
    c(paramJSONObject);
  }
  
  public final JSONObject b()
  {
    JSONObject localJSONObject1 = super.b();
    new JSONObject();
    Object localObject = a;
    localJSONObject1.put("url", localObject);
    long l1 = b;
    localJSONObject1.put("processingInterval", l1);
    int n = c;
    localJSONObject1.put("maxRetryCount", n);
    n = d;
    localJSONObject1.put("maxEventsToPersist", n);
    l1 = e;
    localJSONObject1.put("eventTTL", l1);
    l1 = f;
    localJSONObject1.put("txLatency", l1);
    boolean bool = g;
    localJSONObject1.put("crashEnabled", bool);
    bool = h;
    localJSONObject1.put("catchEnabled", bool);
    JSONObject localJSONObject2 = new org/json/JSONObject;
    localJSONObject2.<init>();
    localObject = l;
    JSONObject localJSONObject3 = new org/json/JSONObject;
    localJSONObject3.<init>();
    long l2 = a;
    localJSONObject3.put("retryInterval", l2);
    int i1 = b;
    localJSONObject3.put("maxBatchSize", i1);
    localJSONObject2.put("wifi", localJSONObject3);
    localObject = k;
    localJSONObject3 = new org/json/JSONObject;
    localJSONObject3.<init>();
    l2 = a;
    localJSONObject3.put("retryInterval", l2);
    i1 = b;
    localJSONObject3.put("maxBatchSize", i1);
    localJSONObject2.put("others", localJSONObject3);
    localJSONObject1.put("networkType", localJSONObject2);
    localJSONObject2 = new org/json/JSONObject;
    localJSONObject2.<init>();
    localObject = i;
    localJSONObject2.put("crashEvent", localObject);
    localObject = j;
    localJSONObject2.put("catchEvent", localObject);
    localJSONObject1.put("telemetry", localJSONObject2);
    return localJSONObject1;
  }
  
  public final boolean c()
  {
    Object localObject = a.trim();
    int n = ((String)localObject).length();
    if (n != 0)
    {
      localObject = a;
      String str = "http://";
      boolean bool1 = ((String)localObject).startsWith(str);
      if (!bool1)
      {
        localObject = a;
        str = "https://";
        bool1 = ((String)localObject).startsWith(str);
        if (!bool1) {}
      }
      else
      {
        long l1 = f;
        long l2 = b;
        bool1 = l1 < l2;
        if (!bool1)
        {
          long l3 = e;
          bool1 = l1 < l3;
          if (!bool1)
          {
            bool1 = l3 < l2;
            if (!bool1)
            {
              localObject = k;
              bool1 = ((b.a)localObject).a();
              if (bool1)
              {
                localObject = l;
                bool1 = ((b.a)localObject).a();
                if (bool1)
                {
                  l1 = b;
                  l2 = 0L;
                  bool1 = l1 < l2;
                  if (!bool1)
                  {
                    int i1 = c;
                    if (i1 >= 0)
                    {
                      l1 = f;
                      boolean bool2 = l1 < l2;
                      if (bool2)
                      {
                        l1 = e;
                        bool2 = l1 < l2;
                        if (bool2)
                        {
                          int i2 = d;
                          if (i2 > 0) {
                            return true;
                          }
                        }
                      }
                    }
                  }
                  return false;
                }
              }
              return false;
            }
          }
        }
        return false;
      }
    }
    return false;
  }
  
  public final a d()
  {
    b localb = new com/inmobi/commons/core/a/b;
    localb.<init>();
    return localb;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */