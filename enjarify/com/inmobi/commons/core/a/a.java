package com.inmobi.commons.core.a;

import com.inmobi.commons.core.configs.b.c;
import com.inmobi.commons.core.e.f;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class a
  implements com.inmobi.commons.core.b.e, b.c
{
  public static AtomicBoolean b;
  private static final String e = "a";
  private static final Object f;
  private static volatile a g;
  public ExecutorService a;
  public b c;
  public String d;
  private c h;
  private com.inmobi.commons.core.b.d i;
  
  static
  {
    Object localObject = new java/lang/Object;
    localObject.<init>();
    f = localObject;
    localObject = new java/util/concurrent/atomic/AtomicBoolean;
    ((AtomicBoolean)localObject).<init>(false);
    b = (AtomicBoolean)localObject;
  }
  
  private a()
  {
    Object localObject = new com/inmobi/commons/core/a/e;
    Thread.UncaughtExceptionHandler localUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
    ((e)localObject).<init>(localUncaughtExceptionHandler);
    Thread.setDefaultUncaughtExceptionHandler((Thread.UncaughtExceptionHandler)localObject);
    localObject = new com/inmobi/commons/core/a/b;
    ((b)localObject).<init>();
    c = ((b)localObject);
    localObject = com.inmobi.commons.core.e.b.a();
    JSONObject localJSONObject = c.i;
    ((com.inmobi.commons.core.e.b)localObject).a("crashReporting", localJSONObject);
    localObject = com.inmobi.commons.core.e.b.a();
    localJSONObject = c.j;
    ((com.inmobi.commons.core.e.b)localObject).a("catchReporting", localJSONObject);
    localObject = c.a;
    d = ((String)localObject);
    localObject = new com/inmobi/commons/core/a/c;
    ((c)localObject).<init>();
    h = ((c)localObject);
    localObject = Executors.newSingleThreadExecutor();
    a = ((ExecutorService)localObject);
  }
  
  public static a a()
  {
    a locala1 = g;
    if (locala1 == null) {
      synchronized (f)
      {
        locala1 = g;
        if (locala1 == null)
        {
          locala1 = new com/inmobi/commons/core/a/a;
          locala1.<init>();
          g = locala1;
        }
      }
    }
    return locala2;
  }
  
  private static String a(List paramList)
  {
    try
    {
      Object localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      Object localObject2 = null;
      localObject2 = com.inmobi.commons.core.utilities.b.b.a(false);
      ((Map)localObject1).putAll((Map)localObject2);
      localObject2 = "im-accid";
      Object localObject3 = com.inmobi.commons.a.a.e();
      ((Map)localObject1).put(localObject2, localObject3);
      localObject2 = "version";
      localObject3 = "2.0.0";
      ((Map)localObject1).put(localObject2, localObject3);
      localObject2 = "component";
      localObject3 = "crash";
      ((Map)localObject1).put(localObject2, localObject3);
      localObject2 = "mk-version";
      localObject3 = com.inmobi.commons.a.b.a();
      ((Map)localObject1).put(localObject2, localObject3);
      localObject2 = com.inmobi.commons.core.utilities.b.a.a();
      localObject2 = b;
      ((Map)localObject1).putAll((Map)localObject2);
      localObject2 = new org/json/JSONObject;
      ((JSONObject)localObject2).<init>((Map)localObject1);
      localObject1 = new org/json/JSONArray;
      ((JSONArray)localObject1).<init>();
      paramList = paramList.iterator();
      for (;;)
      {
        boolean bool1 = paramList.hasNext();
        if (!bool1) {
          break;
        }
        localObject3 = paramList.next();
        localObject3 = (d)localObject3;
        JSONObject localJSONObject = new org/json/JSONObject;
        localJSONObject.<init>();
        String str1 = "eventId";
        String str2 = b;
        localJSONObject.put(str1, str2);
        str1 = "eventType";
        str2 = c;
        localJSONObject.put(str1, str2);
        str1 = ((d)localObject3).a();
        str1 = str1.trim();
        boolean bool2 = str1.isEmpty();
        if (!bool2)
        {
          str1 = "crash_report";
          str2 = ((d)localObject3).a();
          localJSONObject.put(str1, str2);
        }
        str1 = "ts";
        long l = e;
        localJSONObject.put(str1, l);
        ((JSONArray)localObject1).put(localJSONObject);
      }
      paramList = "crash";
      ((JSONObject)localObject2).put(paramList, localObject1);
      return ((JSONObject)localObject2).toString();
    }
    catch (JSONException localJSONException) {}
    return null;
  }
  
  public final com.inmobi.commons.core.b.c a(String paramString)
  {
    int j = com.inmobi.commons.core.utilities.b.b.a();
    int k = 1;
    if (j != k)
    {
      j = c.k.b;
      paramString = c.a(j);
    }
    else
    {
      j = c.l.b;
      paramString = c.a(j);
    }
    boolean bool1 = paramString.isEmpty();
    if (!bool1)
    {
      ArrayList localArrayList = new java/util/ArrayList;
      localArrayList.<init>();
      localObject = paramString.iterator();
      int m;
      Integer localInteger;
      for (;;)
      {
        boolean bool2 = ((Iterator)localObject).hasNext();
        if (!bool2) {
          break;
        }
        m = nexta;
        localInteger = Integer.valueOf(m);
        localArrayList.add(localInteger);
      }
      paramString = a(paramString);
      if (paramString != null)
      {
        localObject = new com/inmobi/commons/core/b/c;
        m = 0;
        localInteger = null;
        ((com.inmobi.commons.core.b.c)localObject).<init>(localArrayList, paramString, false);
        break label159;
      }
    }
    Object localObject = null;
    label159:
    return (com.inmobi.commons.core.b.c)localObject;
  }
  
  public final void a(d paramd)
  {
    boolean bool = paramd instanceof com.inmobi.commons.core.e.a;
    if (!bool)
    {
      localObject1 = c;
      bool = g;
      if (!bool) {
        return;
      }
      localObject1 = new com/inmobi/commons/core/e/f;
      str1 = "CrashEventOccurred";
      ((f)localObject1).<init>("crashReporting", str1);
      localObject2 = com.inmobi.commons.core.e.b.a();
      ((com.inmobi.commons.core.e.b)localObject2).b((f)localObject1);
    }
    Object localObject1 = h;
    long l = c.e;
    String str2 = "default";
    ((c)localObject1).b(l, str2);
    localObject1 = c;
    int j = d;
    Object localObject2 = h;
    String str1 = "default";
    int k = ((c)localObject2).a(str1) + 1 - j;
    if (k >= 0) {
      c.a();
    }
    c.a((d)paramd);
  }
  
  public final void a(com.inmobi.commons.core.configs.a parama)
  {
    parama = (b)parama;
    c = parama;
    parama = c.a;
    d = parama;
    parama = com.inmobi.commons.core.e.b.a();
    JSONObject localJSONObject = c.i;
    parama.a("crashReporting", localJSONObject);
    parama = com.inmobi.commons.core.e.b.a();
    localJSONObject = c.j;
    parama.a("catchReporting", localJSONObject);
  }
  
  public final void a(com.inmobi.commons.core.e.a parama)
  {
    Object localObject = c;
    boolean bool = h;
    if (!bool) {
      return;
    }
    localObject = new com/inmobi/commons/core/e/f;
    ((f)localObject).<init>("catchReporting", "CatchEventOccurred");
    com.inmobi.commons.core.e.b.a().a((f)localObject);
    localObject = a;
    a.1 local1 = new com/inmobi/commons/core/a/a$1;
    local1.<init>(this, parama);
    ((ExecutorService)localObject).execute(local1);
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */