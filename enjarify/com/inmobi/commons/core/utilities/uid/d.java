package com.inmobi.commons.core.utilities.uid;

import android.util.Base64;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class d
{
  private Map a;
  
  public d(Map paramMap)
  {
    a = paramMap;
  }
  
  private static String a(String paramString1, String paramString2)
  {
    String str1 = "";
    Object localObject = "UTF-8";
    try
    {
      paramString1 = paramString1.getBytes((String)localObject);
      int i = paramString1.length;
      localObject = new byte[i];
      String str2 = "UTF-8";
      paramString2 = paramString2.getBytes(str2);
      int j = 0;
      str2 = null;
      for (;;)
      {
        int k = paramString1.length;
        if (j >= k) {
          break;
        }
        k = paramString1[j];
        int m = paramString2.length;
        m = j % m;
        m = paramString2[m];
        k = (byte)(k ^ m);
        localObject[j] = k;
        j += 1;
      }
      int n = 2;
      paramString1 = Base64.encode((byte[])localObject, n);
      paramString2 = new java/lang/String;
      localObject = "UTF-8";
      paramString2.<init>(paramString1, (String)localObject);
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      paramString2 = str1;
    }
    return paramString2;
  }
  
  private void a(String paramString1, boolean paramBoolean, Map paramMap, String paramString2)
  {
    try
    {
      Object localObject = a;
      String str = "UM5";
      localObject = ((Map)localObject).get(str);
      localObject = (Boolean)localObject;
      boolean bool = ((Boolean)localObject).booleanValue();
      if ((bool) && (paramString2 == null))
      {
        c.a();
        c.a();
        localObject = c.e();
        str = "MD5";
        localObject = c.a((String)localObject, str);
        if (paramBoolean) {
          localObject = a((String)localObject, paramString1);
        }
        str = "UM5";
        paramMap.put(str, localObject);
      }
      localObject = a;
      str = "O1";
      localObject = ((Map)localObject).get(str);
      localObject = (Boolean)localObject;
      bool = ((Boolean)localObject).booleanValue();
      if ((bool) && (paramString2 == null))
      {
        c.a();
        c.a();
        paramString2 = c.e();
        localObject = "SHA-1";
        paramString2 = c.a(paramString2, (String)localObject);
        if (paramBoolean) {
          paramString2 = a(paramString2, paramString1);
        }
        paramString1 = "O1";
        paramMap.put(paramString1, paramString2);
      }
      return;
    }
    catch (Exception localException)
    {
      d.class.getSimpleName();
    }
  }
  
  public final Map a(String paramString, boolean paramBoolean)
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    String str1 = null;
    try
    {
      localObject = a;
      String str2 = "GPID";
      localObject = ((Map)localObject).get(str2);
      localObject = (Boolean)localObject;
      boolean bool = ((Boolean)localObject).booleanValue();
      if (bool)
      {
        c.a();
        localObject = c.f();
        if (localObject != null)
        {
          str1 = a;
          if (str1 != null)
          {
            if (paramBoolean) {
              str1 = a(str1, paramString);
            }
            localObject = "GPID";
            localHashMap.put(localObject, str1);
          }
        }
      }
      a(paramString, paramBoolean, localHashMap, str1);
    }
    catch (Exception localException)
    {
      Object localObject = d.class;
      ((Class)localObject).getSimpleName();
      a(paramString, paramBoolean, localHashMap, str1);
    }
    return localHashMap;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.utilities.uid.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */