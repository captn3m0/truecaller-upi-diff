package com.inmobi.commons.core.utilities.uid;

import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings.Secure;
import android.provider.Settings.System;
import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;
import com.inmobi.commons.core.utilities.f;
import java.security.MessageDigest;

public class c
{
  private static final String a = "c";
  private static final Object b;
  private static c c;
  private static a d;
  
  static
  {
    Object localObject = new java/lang/Object;
    localObject.<init>();
    b = localObject;
  }
  
  public static c a()
  {
    c localc1 = c;
    if (localc1 == null) {
      synchronized (b)
      {
        localc1 = c;
        if (localc1 == null)
        {
          localc1 = new com/inmobi/commons/core/utilities/uid/c;
          localc1.<init>();
          c = localc1;
        }
      }
    }
    return localc2;
  }
  
  static String a(String paramString1, String paramString2)
  {
    String str1;
    if (paramString1 != null) {
      str1 = "";
    }
    try
    {
      String str2 = paramString1.trim();
      int i = str1.equals(str2);
      if (i == 0)
      {
        paramString2 = MessageDigest.getInstance(paramString2);
        paramString1 = paramString1.getBytes();
        paramString2.update(paramString1);
        paramString1 = paramString2.digest();
        paramString2 = new java/lang/StringBuffer;
        paramString2.<init>();
        i = 0;
        str1 = null;
        for (;;)
        {
          int k = paramString1.length;
          if (i >= k) {
            break;
          }
          int m = (paramString1[i] & 0xFF) + 256;
          int n = 16;
          str2 = Integer.toString(m, n);
          n = 1;
          str2 = str2.substring(n);
          paramString2.append(str2);
          int j;
          i += 1;
        }
        return paramString2.toString();
      }
      return "TEST_EMULATOR";
    }
    catch (Exception localException)
    {
      localException.getMessage();
    }
    return null;
  }
  
  public static void c()
  {
    Object localObject1 = "root";
    try
    {
      boolean bool = f.a((String)localObject1);
      if (bool)
      {
        localObject1 = d;
        if (localObject1 != null)
        {
          localObject1 = a;
          if (localObject1 != null)
          {
            localInternalLogLevel = Logger.InternalLogLevel.DEBUG;
            str1 = a;
            localObject2 = "Publisher device Id is ";
            localObject1 = String.valueOf(localObject1);
            localObject1 = ((String)localObject2).concat((String)localObject1);
            Logger.a(localInternalLogLevel, str1, (String)localObject1);
          }
        }
        return;
      }
      localObject1 = e();
      Logger.InternalLogLevel localInternalLogLevel = Logger.InternalLogLevel.DEBUG;
      String str1 = a;
      Object localObject2 = new java/lang/StringBuilder;
      String str2 = "Publisher device Id is ";
      ((StringBuilder)localObject2).<init>(str2);
      str2 = "SHA-1";
      localObject1 = a((String)localObject1, str2);
      ((StringBuilder)localObject2).append((String)localObject1);
      localObject1 = ((StringBuilder)localObject2).toString();
      Logger.a(localInternalLogLevel, str1, (String)localObject1);
      return;
    }
    catch (Exception localException)
    {
      localException.getMessage();
    }
  }
  
  public static String d()
  {
    return "1";
  }
  
  static String e()
  {
    Object localObject1 = "";
    Object localObject2 = com.inmobi.commons.a.a.b();
    if (localObject2 != null) {
      try
      {
        localObject1 = ((Context)localObject2).getContentResolver();
        String str = "android_id";
        localObject1 = Settings.Secure.getString((ContentResolver)localObject1, str);
        if (localObject1 == null)
        {
          localObject1 = ((Context)localObject2).getContentResolver();
          localObject2 = "android_id";
          localObject1 = Settings.System.getString((ContentResolver)localObject1, (String)localObject2);
        }
      }
      catch (Exception localException)
      {
        localObject1 = "";
      }
    }
    return (String)localObject1;
  }
  
  static a f()
  {
    return d;
  }
  
  public static Boolean g()
  {
    a();
    a locala = d;
    if (locala == null) {
      return null;
    }
    return b;
  }
  
  public final void b()
  {
    try
    {
      b localb = new com/inmobi/commons/core/utilities/uid/b;
      localb.<init>();
      Object localObject1 = new com/inmobi/commons/core/utilities/uid/a;
      ((a)localObject1).<init>();
      d = (a)localObject1;
      Object localObject2 = a;
      String str = "adv_id";
      localObject2 = ((com.inmobi.commons.core.d.c)localObject2).c(str);
      a = ((String)localObject2);
      localObject1 = d;
      localObject2 = a;
      str = "limit_ad_tracking";
      localObject2 = a;
      boolean bool1 = ((SharedPreferences)localObject2).contains(str);
      if (bool1)
      {
        localObject2 = a;
        str = "limit_ad_tracking";
        boolean bool2 = true;
        bool1 = ((com.inmobi.commons.core.d.c)localObject2).b(str, bool2);
        localObject2 = Boolean.valueOf(bool1);
      }
      else
      {
        bool1 = false;
        localObject2 = null;
      }
      b = ((Boolean)localObject2);
      localObject1 = new java/lang/Thread;
      localObject2 = new com/inmobi/commons/core/utilities/uid/c$1;
      ((c.1)localObject2).<init>(this, localb);
      ((Thread)localObject1).<init>((Runnable)localObject2);
      ((Thread)localObject1).start();
      return;
    }
    catch (Exception localException)
    {
      localException.getMessage();
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.utilities.uid.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */