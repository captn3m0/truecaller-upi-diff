package com.inmobi.commons.core.utilities;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.text.TextUtils;
import java.net.URISyntaxException;
import java.net.URLDecoder;

public class b
{
  private static final String a = "b";
  
  public static String a(Context paramContext, String paramString1, String paramString2)
  {
    if (paramContext == null) {
      return null;
    }
    boolean bool1 = false;
    Object localObject1 = null;
    try
    {
      localObject1 = Intent.parseUri(paramString1, 0);
      Object localObject2 = paramContext.getPackageManager();
      localObject2 = ((Intent)localObject1).resolveActivity((PackageManager)localObject2);
      if (localObject2 != null)
      {
        int i = 268435456;
        ((Intent)localObject1).setFlags(i);
        paramContext.startActivity((Intent)localObject1);
        return paramString1;
      }
      bool1 = TextUtils.isEmpty(paramString2);
      if (!bool1) {
        return a(paramContext, paramString2, null);
      }
      paramString2 = Uri.parse(paramString1);
      localObject1 = "intent";
      paramString2 = paramString2.getScheme();
      boolean bool2 = ((String)localObject1).equals(paramString2);
      if (bool2)
      {
        paramString1 = b(paramString1);
        bool2 = TextUtils.isEmpty(paramString1);
        if (!bool2)
        {
          paramString2 = "UTF-8";
          paramString1 = URLDecoder.decode(paramString1, paramString2);
          return a(paramContext, paramString1, null);
        }
      }
    }
    catch (Exception paramContext)
    {
      paramContext.getMessage();
    }
    return null;
  }
  
  public static boolean a(Context paramContext, String paramString)
  {
    if (paramString == null) {
      return false;
    }
    if (paramContext != null) {
      try
      {
        Intent localIntent = new android/content/Intent;
        String str = "android.intent.action.VIEW";
        paramString = Uri.parse(paramString);
        localIntent.<init>(str, paramString);
        paramContext = paramContext.getPackageManager();
        paramContext = localIntent.resolveActivity(paramContext);
        return paramContext != null;
      }
      catch (Exception localException)
      {
        return false;
      }
    }
    return a(Uri.parse(paramString));
  }
  
  private static boolean a(Uri paramUri)
  {
    String str1 = "http";
    String str2 = paramUri.getScheme();
    boolean bool1 = str1.equals(str2);
    if (!bool1)
    {
      str1 = "https";
      paramUri = paramUri.getScheme();
      boolean bool2 = str1.equals(paramUri);
      if (!bool2) {
        return false;
      }
    }
    return true;
  }
  
  public static boolean a(String paramString)
  {
    paramString = Uri.parse(paramString);
    boolean bool1 = a(paramString);
    if (bool1)
    {
      String str1 = "play.google.com";
      String str2 = paramString.getHost();
      bool1 = str1.equals(str2);
      if (!bool1)
      {
        str1 = "market.android.com";
        str2 = paramString.getHost();
        bool1 = str1.equals(str2);
        if (!bool1)
        {
          str1 = "market";
          paramString = paramString.getScheme();
          boolean bool2 = str1.equals(paramString);
          if (!bool2) {
            return true;
          }
        }
      }
    }
    return false;
  }
  
  private static String b(String paramString)
  {
    int i = 1;
    try
    {
      paramString = Intent.parseUri(paramString, i);
      String str = "browser_fallback_url";
      paramString = paramString.getStringExtra(str);
    }
    catch (URISyntaxException localURISyntaxException)
    {
      localURISyntaxException.getMessage();
      paramString = null;
    }
    return paramString;
  }
  
  public static void b(Context paramContext, String paramString)
  {
    for (;;)
    {
      if (paramContext == null) {
        return;
      }
      Intent localIntent = null;
      try
      {
        localIntent = Intent.parseUri(paramString, 0);
        int i = 268435456;
        localIntent.setFlags(i);
        paramContext.startActivity(localIntent);
        return;
      }
      catch (ActivityNotFoundException localActivityNotFoundException)
      {
        Object localObject = Uri.parse(paramString);
        String str = "intent";
        localObject = ((Uri)localObject).getScheme();
        boolean bool = str.equals(localObject);
        if (bool)
        {
          paramString = b(paramString);
          bool = TextUtils.isEmpty(paramString);
          if (!bool) {
            continue;
          }
        }
        throw localActivityNotFoundException;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.utilities.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */