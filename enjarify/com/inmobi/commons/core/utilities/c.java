package com.inmobi.commons.core.utilities;

import android.net.Uri;
import java.io.File;

public class c
{
  private static final String a = "c";
  
  public static long a(String paramString)
  {
    long l = 0L;
    try
    {
      File localFile = new java/io/File;
      paramString = Uri.parse(paramString);
      paramString = paramString.getPath();
      localFile.<init>(paramString);
      boolean bool = localFile.exists();
      if (bool) {
        l = localFile.length();
      }
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
    return l;
  }
  
  public static void a(File paramFile)
  {
    try
    {
      boolean bool1 = paramFile.exists();
      if (bool1)
      {
        File[] arrayOfFile = paramFile.listFiles();
        if (arrayOfFile != null)
        {
          int i = arrayOfFile.length;
          int j = 0;
          while (j < i)
          {
            File localFile = arrayOfFile[j];
            boolean bool2 = localFile.isDirectory();
            if (bool2) {
              a(localFile);
            } else {
              localFile.delete();
            }
            j += 1;
          }
        }
        paramFile.delete();
      }
      return;
    }
    catch (Exception localException)
    {
      localException.getMessage();
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.utilities.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */