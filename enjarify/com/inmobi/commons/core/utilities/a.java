package com.inmobi.commons.core.utilities;

import android.app.Application;
import android.os.HandlerThread;
import com.inmobi.commons.core.e.b;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class a
{
  private static final String a = "a";
  private static List b;
  private static Object c;
  private static boolean d;
  private static HandlerThread e;
  private static final Object f;
  private static volatile a g;
  
  static
  {
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    b = (List)localObject;
    d = false;
    e = null;
    localObject = new java/lang/Object;
    localObject.<init>();
    f = localObject;
  }
  
  public static a a()
  {
    a locala1 = g;
    if (locala1 == null) {
      synchronized (f)
      {
        locala1 = g;
        if (locala1 == null)
        {
          locala1 = new com/inmobi/commons/core/utilities/a;
          locala1.<init>();
          g = locala1;
        }
      }
    }
    return locala2;
  }
  
  public static void b()
  {
    d = true;
  }
  
  public static void c()
  {
    d = false;
  }
  
  public final void a(a.b paramb)
  {
    Object localObject1 = b;
    ((List)localObject1).add(paramb);
    paramb = b;
    int i = paramb.size();
    int j = 1;
    if (i == j)
    {
      boolean bool1 = com.inmobi.commons.a.a.a();
      if (bool1)
      {
        paramb = new android/os/HandlerThread;
        paramb.<init>("ApplicationFocusChangeObserverHandler");
        e = paramb;
        paramb.start();
        paramb = Application.class.getDeclaredClasses();
        int k = paramb.length;
        String str1 = null;
        Object localObject2 = null;
        int m = 0;
        Object localObject3 = null;
        Class[] arrayOfClass;
        while (m < k)
        {
          arrayOfClass = paramb[m];
          String str2 = arrayOfClass.getSimpleName();
          String str3 = "ActivityLifecycleCallbacks";
          boolean bool2 = str2.equalsIgnoreCase(str3);
          if (bool2)
          {
            new Class[j][0] = arrayOfClass;
            localObject2 = arrayOfClass;
          }
          m += 1;
        }
        paramb = ((Class)localObject2).getClassLoader();
        localObject3 = new Class[j];
        localObject3[0] = localObject2;
        Object localObject4 = new com/inmobi/commons/core/utilities/a$1;
        ((a.1)localObject4).<init>(this);
        c = Proxy.newProxyInstance(paramb, (Class[])localObject3, (InvocationHandler)localObject4);
        paramb = (Application)com.inmobi.commons.a.a.b();
        localObject3 = c;
        if (localObject3 != null)
        {
          localObject3 = Application.class;
          localObject4 = "registerActivityLifecycleCallbacks";
          try
          {
            arrayOfClass = new Class[j];
            arrayOfClass[0] = localObject2;
            localObject3 = ((Class)localObject3).getMethod((String)localObject4, arrayOfClass);
            localObject1 = new Object[j];
            localObject4 = c;
            localObject1[0] = localObject4;
            ((Method)localObject3).invoke(paramb, (Object[])localObject1);
            return;
          }
          catch (Exception paramb)
          {
            try
            {
              localObject1 = new java/util/HashMap;
              ((HashMap)localObject1).<init>();
              localObject3 = "type";
              localObject4 = "GenericException";
              ((Map)localObject1).put(localObject3, localObject4);
              localObject3 = "message";
              localObject4 = new java/lang/StringBuilder;
              ((StringBuilder)localObject4).<init>();
              str1 = paramb.getMessage();
              ((StringBuilder)localObject4).append(str1);
              localObject4 = ((StringBuilder)localObject4).toString();
              ((Map)localObject1).put(localObject3, localObject4);
              b.a();
              localObject3 = "root";
              localObject4 = "ExceptionCaught";
              b.a((String)localObject3, (String)localObject4, (Map)localObject1);
              return;
            }
            catch (Exception localException)
            {
              paramb.getMessage();
            }
          }
          catch (NoSuchMethodException|InvocationTargetException|IllegalAccessException localNoSuchMethodException) {}
        }
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.utilities.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */