package com.inmobi.commons.core.utilities;

import android.app.Activity;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

final class a$1
  implements InvocationHandler
{
  private final Handler b;
  private WeakReference c;
  
  a$1(a parama)
  {
    parama = new com/inmobi/commons/core/utilities/a$a;
    Looper localLooper = a.d().getLooper();
    parama.<init>(localLooper);
    b = parama;
  }
  
  public final Object invoke(Object paramObject, Method paramMethod, Object[] paramArrayOfObject)
  {
    if (paramArrayOfObject != null)
    {
      paramObject = paramMethod.getName();
      int i = -1;
      int j = ((String)paramObject).hashCode();
      int k = 195654633;
      String str;
      boolean bool;
      if (j != k)
      {
        k = 1495889555;
        if (j != k)
        {
          k = 1508755423;
          if (j == k)
          {
            str = "onActivityStopped";
            bool = ((String)paramObject).equals(str);
            if (bool) {
              i = 2;
            }
          }
        }
        else
        {
          str = "onActivityStarted";
          bool = ((String)paramObject).equals(str);
          if (bool)
          {
            i = 0;
            paramMethod = null;
          }
        }
      }
      else
      {
        str = "onActivityResumed";
        bool = ((String)paramObject).equals(str);
        if (bool) {
          i = 1;
        }
      }
      int m = 1001;
      switch (i)
      {
      default: 
        break;
      case 2: 
        paramMethod = (Activity)paramArrayOfObject[0];
        paramArrayOfObject = c;
        if (paramArrayOfObject != null)
        {
          paramArrayOfObject = paramArrayOfObject.get();
          if (paramArrayOfObject == paramMethod)
          {
            paramMethod = b;
            long l = 3000L;
            paramMethod.sendEmptyMessageDelayed(m, l);
          }
        }
        break;
      case 0: 
      case 1: 
        paramMethod = (Activity)paramArrayOfObject[0];
        paramArrayOfObject = c;
        if (paramArrayOfObject != null)
        {
          paramArrayOfObject = paramArrayOfObject.get();
          if (paramArrayOfObject == paramMethod) {}
        }
        else
        {
          paramArrayOfObject = new java/lang/ref/WeakReference;
          paramArrayOfObject.<init>(paramMethod);
          c = paramArrayOfObject;
        }
        paramMethod = b;
        paramMethod.removeMessages(m);
        paramObject = b;
        i = 1002;
        ((Handler)paramObject).sendEmptyMessage(i);
      }
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.utilities.a.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */