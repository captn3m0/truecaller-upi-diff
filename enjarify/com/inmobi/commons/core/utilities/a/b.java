package com.inmobi.commons.core.utilities.a;

import android.util.Base64;
import com.inmobi.commons.core.d.c;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.RSAPublicKeySpec;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class b
{
  private static final String a = "b";
  private static String b = "AES";
  private static String c = "AES/CBC/PKCS7Padding";
  private static byte[] d;
  
  public static String a(String paramString1, byte[] paramArrayOfByte1, byte[] paramArrayOfByte2, byte[] paramArrayOfByte3, String paramString2, String paramString3)
  {
    Object localObject = "UTF-8";
    try
    {
      paramString1 = paramString1.getBytes((String)localObject);
      paramString1 = b(paramString1, paramArrayOfByte1, paramArrayOfByte2);
      localObject = a(paramString1, paramArrayOfByte3);
      paramString1 = a(paramString1);
      localObject = a((byte[])localObject);
      paramArrayOfByte2 = a(paramArrayOfByte2);
      paramArrayOfByte1 = a(paramArrayOfByte1);
      paramArrayOfByte3 = a(paramArrayOfByte3);
      paramArrayOfByte1 = b(paramArrayOfByte1, paramArrayOfByte3);
      paramArrayOfByte1 = b(paramArrayOfByte1, paramArrayOfByte2);
      paramArrayOfByte1 = a(paramArrayOfByte1, paramString3, paramString2);
      paramArrayOfByte1 = a(paramArrayOfByte1);
      paramString1 = b(paramString1, (byte[])localObject);
      paramString1 = b(paramArrayOfByte1, paramString1);
      int i = 8;
      paramString1 = Base64.encode(paramString1, i);
      paramArrayOfByte1 = new java/lang/String;
      paramArrayOfByte1.<init>(paramString1);
      return paramArrayOfByte1;
    }
    catch (Exception localException)
    {
      localException.getMessage();
    }
    return null;
  }
  
  public static byte[] a()
  {
    synchronized (b.class)
    {
      Object localObject1 = new com/inmobi/commons/core/utilities/a/a;
      ((a)localObject1).<init>();
      long l1 = System.currentTimeMillis();
      long l2 = 1000L;
      l1 /= l2;
      c localc = a;
      String str1 = "last_generated_ts";
      long l3 = 0L;
      l2 = localc.b(str1, l3);
      l1 -= l2;
      l2 = 86400L;
      boolean bool = l1 < l2;
      if (bool) {}
      try
      {
        localObject3 = b();
        d = (byte[])localObject3;
        localObject3 = Base64.encodeToString((byte[])localObject3, 0);
        ((a)localObject1).a((String)localObject3);
      }
      catch (Exception localException)
      {
        Object localObject3;
        String str2;
        for (;;) {}
      }
      localObject3 = d;
      if (localObject3 == null)
      {
        localObject3 = a;
        str2 = "aes_public_key";
        localObject3 = ((c)localObject3).c(str2);
        try
        {
          localObject3 = Base64.decode((String)localObject3, 0);
          d = (byte[])localObject3;
        }
        catch (IllegalArgumentException localIllegalArgumentException)
        {
          localObject3 = b();
          d = (byte[])localObject3;
          localObject3 = Base64.encodeToString((byte[])localObject3, 0);
          ((a)localObject1).a((String)localObject3);
        }
      }
      localObject1 = d;
      return (byte[])localObject1;
    }
  }
  
  public static byte[] a(int paramInt)
  {
    byte[] arrayOfByte = new byte[paramInt];
    try
    {
      SecureRandom localSecureRandom = new java/security/SecureRandom;
      localSecureRandom.<init>();
      localSecureRandom.nextBytes(arrayOfByte);
    }
    catch (Exception localException)
    {
      localException.getMessage();
    }
    return arrayOfByte;
  }
  
  private static byte[] a(byte[] paramArrayOfByte)
  {
    long l = paramArrayOfByte.length;
    ByteBuffer localByteBuffer = ByteBuffer.allocate(8);
    ByteOrder localByteOrder = ByteOrder.LITTLE_ENDIAN;
    localByteBuffer.order(localByteOrder);
    localByteBuffer.putLong(l);
    byte[] arrayOfByte1 = localByteBuffer.array();
    int i = arrayOfByte1.length;
    int j = paramArrayOfByte.length;
    byte[] arrayOfByte2 = new byte[i + j];
    j = arrayOfByte1.length;
    System.arraycopy(arrayOfByte1, 0, arrayOfByte2, 0, j);
    int k = arrayOfByte1.length;
    j = paramArrayOfByte.length;
    System.arraycopy(paramArrayOfByte, 0, arrayOfByte2, k, j);
    return arrayOfByte2;
  }
  
  private static byte[] a(byte[] paramArrayOfByte, String paramString1, String paramString2)
  {
    BigInteger localBigInteger = new java/math/BigInteger;
    int i = 16;
    localBigInteger.<init>(paramString2, i);
    paramString2 = new java/math/BigInteger;
    paramString2.<init>(paramString1, i);
    paramString1 = "RSA";
    try
    {
      paramString1 = KeyFactory.getInstance(paramString1);
      RSAPublicKeySpec localRSAPublicKeySpec = new java/security/spec/RSAPublicKeySpec;
      localRSAPublicKeySpec.<init>(localBigInteger, paramString2);
      paramString1 = paramString1.generatePublic(localRSAPublicKeySpec);
      paramString1 = (RSAPublicKey)paramString1;
      paramString2 = "RSA/ECB/nopadding";
      paramString2 = Cipher.getInstance(paramString2);
      int j = 1;
      paramString2.init(j, paramString1);
      paramArrayOfByte = paramString2.doFinal(paramArrayOfByte);
    }
    finally
    {
      paramArrayOfByte = null;
    }
    return paramArrayOfByte;
  }
  
  private static byte[] a(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2)
  {
    SecretKeySpec localSecretKeySpec = new javax/crypto/spec/SecretKeySpec;
    int i = paramArrayOfByte2.length;
    String str = "HmacSHA1";
    localSecretKeySpec.<init>(paramArrayOfByte2, 0, i, str);
    paramArrayOfByte2 = "HmacSHA1";
    try
    {
      paramArrayOfByte2 = Mac.getInstance(paramArrayOfByte2);
      paramArrayOfByte2.init(localSecretKeySpec);
      paramArrayOfByte1 = paramArrayOfByte2.doFinal(paramArrayOfByte1);
    }
    catch (NoSuchAlgorithmException|InvalidKeyException localNoSuchAlgorithmException)
    {
      paramArrayOfByte1 = null;
    }
    return paramArrayOfByte1;
  }
  
  public static byte[] a(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2, byte[] paramArrayOfByte3)
  {
    SecretKeySpec localSecretKeySpec = new javax/crypto/spec/SecretKeySpec;
    String str = b;
    localSecretKeySpec.<init>(paramArrayOfByte2, str);
    try
    {
      paramArrayOfByte2 = c;
      paramArrayOfByte2 = Cipher.getInstance(paramArrayOfByte2);
      int i = 2;
      IvParameterSpec localIvParameterSpec = new javax/crypto/spec/IvParameterSpec;
      localIvParameterSpec.<init>(paramArrayOfByte3);
      paramArrayOfByte2.init(i, localSecretKeySpec, localIvParameterSpec);
      paramArrayOfByte1 = paramArrayOfByte2.doFinal(paramArrayOfByte1);
    }
    finally
    {
      paramArrayOfByte1 = null;
    }
    return paramArrayOfByte1;
  }
  
  private static byte[] b()
  {
    Object localObject = "AES";
    try
    {
      localObject = KeyGenerator.getInstance((String)localObject);
      int i = 128;
      SecureRandom localSecureRandom = new java/security/SecureRandom;
      localSecureRandom.<init>();
      ((KeyGenerator)localObject).init(i, localSecureRandom);
      localObject = ((KeyGenerator)localObject).generateKey();
      if (localObject != null) {
        return ((SecretKey)localObject).getEncoded();
      }
    }
    catch (Exception localException)
    {
      localException.getMessage();
    }
    return null;
  }
  
  private static byte[] b(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2)
  {
    int i = paramArrayOfByte1.length;
    int j = paramArrayOfByte2.length;
    byte[] arrayOfByte = new byte[i + j];
    j = paramArrayOfByte1.length;
    System.arraycopy(paramArrayOfByte1, 0, arrayOfByte, 0, j);
    int k = paramArrayOfByte1.length;
    j = paramArrayOfByte2.length;
    System.arraycopy(paramArrayOfByte2, 0, arrayOfByte, k, j);
    return arrayOfByte;
  }
  
  private static byte[] b(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2, byte[] paramArrayOfByte3)
  {
    SecretKeySpec localSecretKeySpec = new javax/crypto/spec/SecretKeySpec;
    String str = b;
    localSecretKeySpec.<init>(paramArrayOfByte2, str);
    paramArrayOfByte2 = new javax/crypto/spec/IvParameterSpec;
    paramArrayOfByte2.<init>(paramArrayOfByte3);
    try
    {
      paramArrayOfByte3 = c;
      paramArrayOfByte3 = Cipher.getInstance(paramArrayOfByte3);
      int i = 1;
      paramArrayOfByte3.init(i, localSecretKeySpec, paramArrayOfByte2);
      paramArrayOfByte3.init(i, localSecretKeySpec, paramArrayOfByte2);
      paramArrayOfByte3.init(i, localSecretKeySpec, paramArrayOfByte2);
      paramArrayOfByte1 = paramArrayOfByte3.doFinal(paramArrayOfByte1);
    }
    finally
    {
      paramArrayOfByte1 = null;
    }
    return paramArrayOfByte1;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.utilities.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */