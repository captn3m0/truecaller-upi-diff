package com.inmobi.commons.core.utilities.a;

import android.util.Base64;
import java.math.BigInteger;
import java.security.Key;
import java.security.KeyFactory;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.KeySpec;
import java.security.spec.RSAPublicKeySpec;
import javax.crypto.Cipher;

public class c
{
  private static final String a = "c";
  
  public static String a(String paramString)
  {
    Object localObject1 = null;
    if (paramString != null)
    {
      Object localObject2 = "";
      boolean bool = ((String)localObject2).equals(paramString);
      if (!bool)
      {
        try
        {
          localObject2 = new java/math/BigInteger;
          Object localObject3 = "C10F7968CFE2C76AC6F0650C877806D4514DE58FC239592D2385BCE5609A84B2A0FBDAF29B05505EAD1FDFEF3D7209ACBF34B5D0A806DF18147EA9C0337D6B5B";
          int i = 16;
          ((BigInteger)localObject2).<init>((String)localObject3, i);
          localObject3 = new java/math/BigInteger;
          Object localObject4 = "010001";
          ((BigInteger)localObject3).<init>((String)localObject4, i);
          Object localObject5 = "RSA";
          localObject5 = KeyFactory.getInstance((String)localObject5);
          localObject4 = new java/security/spec/RSAPublicKeySpec;
          ((RSAPublicKeySpec)localObject4).<init>((BigInteger)localObject2, (BigInteger)localObject3);
          localObject2 = ((KeyFactory)localObject5).generatePublic((KeySpec)localObject4);
          localObject2 = (RSAPublicKey)localObject2;
          localObject3 = "RSA/ECB/nopadding";
          localObject3 = Cipher.getInstance((String)localObject3);
          i = 1;
          ((Cipher)localObject3).init(i, (Key)localObject2);
          localObject2 = "UTF-8";
          paramString = paramString.getBytes((String)localObject2);
          paramString = a(paramString, (Cipher)localObject3);
          bool = false;
          localObject2 = null;
          paramString = Base64.encode(paramString, 0);
          localObject2 = new java/lang/String;
          ((String)localObject2).<init>(paramString);
          localObject1 = localObject2;
        }
        catch (Exception paramString)
        {
          paramString.getMessage();
        }
        return (String)localObject1;
      }
    }
    return null;
  }
  
  private static byte[] a(byte[] paramArrayOfByte, Cipher paramCipher)
  {
    int i = 0;
    byte[] arrayOfByte1 = new byte[0];
    int j = paramArrayOfByte.length;
    int k = 64;
    byte[] arrayOfByte2 = new byte[k];
    while (i < j)
    {
      if (i > 0)
      {
        m = i % 64;
        if (m == 0)
        {
          arrayOfByte2 = paramCipher.doFinal(arrayOfByte2);
          arrayOfByte1 = a(arrayOfByte1, arrayOfByte2);
          int n = i + 64;
          if (n > j) {
            n = j - i;
          } else {
            n = 64;
          }
          arrayOfByte2 = new byte[n];
        }
      }
      int m = i % 64;
      int i1 = paramArrayOfByte[i];
      arrayOfByte2[m] = i1;
      i += 1;
    }
    paramArrayOfByte = paramCipher.doFinal(arrayOfByte2);
    return a(arrayOfByte1, paramArrayOfByte);
  }
  
  private static byte[] a(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2)
  {
    int i = paramArrayOfByte1.length;
    int j = paramArrayOfByte2.length;
    byte[] arrayOfByte = new byte[i + j];
    j = paramArrayOfByte1.length;
    System.arraycopy(paramArrayOfByte1, 0, arrayOfByte, 0, j);
    int k = paramArrayOfByte1.length;
    j = paramArrayOfByte2.length;
    System.arraycopy(paramArrayOfByte2, 0, arrayOfByte, k, j);
    return arrayOfByte;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.utilities.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */