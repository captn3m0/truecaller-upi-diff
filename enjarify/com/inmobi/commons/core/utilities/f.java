package com.inmobi.commons.core.utilities;

import android.content.Context;
import com.inmobi.commons.a.a;
import com.inmobi.commons.core.e.b;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public final class f
{
  private static final String a = "f";
  
  public static boolean a(String paramString)
  {
    Context localContext = a.b();
    if (localContext == null) {
      return false;
    }
    Object localObject1 = "com.google.android.gms.common.GoogleApiAvailability";
    try
    {
      localObject1 = Class.forName((String)localObject1);
      localObject2 = null;
      boolean bool = true;
      if (localObject1 != null)
      {
        localObject3 = "getInstance";
        Object localObject4 = new Class[0];
        localObject3 = ((Class)localObject1).getMethod((String)localObject3, (Class[])localObject4);
        localObject4 = "isGooglePlayServicesAvailable";
        Class[] arrayOfClass = new Class[bool];
        Class localClass = Context.class;
        arrayOfClass[0] = localClass;
        localObject1 = ((Class)localObject1).getMethod((String)localObject4, arrayOfClass);
        if ((localObject3 != null) && (localObject1 != null))
        {
          localObject4 = new Object[0];
          localObject3 = ((Method)localObject3).invoke(null, (Object[])localObject4);
          if (localObject3 != null)
          {
            localObject2 = new Object[bool];
            localObject2[0] = localContext;
            localObject2 = ((Method)localObject1).invoke(localObject3, (Object[])localObject2);
          }
        }
      }
      localObject2 = (Integer)localObject2;
      int i = ((Integer)localObject2).intValue();
      if (i == 0) {
        return bool;
      }
      return false;
    }
    catch (Exception localException1)
    {
      try
      {
        localObject1 = new java/util/HashMap;
        ((HashMap)localObject1).<init>();
        Object localObject2 = "type";
        Object localObject5 = "RuntimeException";
        ((Map)localObject1).put(localObject2, localObject5);
        localObject2 = "message";
        localObject5 = new java/lang/StringBuilder;
        ((StringBuilder)localObject5).<init>();
        Object localObject3 = localException1.getMessage();
        ((StringBuilder)localObject5).append((String)localObject3);
        localObject5 = ((StringBuilder)localObject5).toString();
        ((Map)localObject1).put(localObject2, localObject5);
        b.a();
        localObject2 = "ExceptionCaught";
        b.a(paramString, (String)localObject2, (Map)localObject1);
      }
      catch (Exception localException2)
      {
        localException1.getMessage();
      }
      return false;
    }
    catch (Error localError)
    {
      for (;;) {}
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.utilities.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */