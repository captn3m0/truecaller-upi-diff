package com.inmobi.commons.core.utilities;

public enum Logger$InternalLogLevel
{
  static
  {
    Object localObject = new com/inmobi/commons/core/utilities/Logger$InternalLogLevel;
    ((InternalLogLevel)localObject).<init>("NONE", 0);
    NONE = (InternalLogLevel)localObject;
    localObject = new com/inmobi/commons/core/utilities/Logger$InternalLogLevel;
    int i = 1;
    ((InternalLogLevel)localObject).<init>("ERROR", i);
    ERROR = (InternalLogLevel)localObject;
    localObject = new com/inmobi/commons/core/utilities/Logger$InternalLogLevel;
    int j = 2;
    ((InternalLogLevel)localObject).<init>("DEBUG", j);
    DEBUG = (InternalLogLevel)localObject;
    localObject = new com/inmobi/commons/core/utilities/Logger$InternalLogLevel;
    int k = 3;
    ((InternalLogLevel)localObject).<init>("INTERNAL", k);
    INTERNAL = (InternalLogLevel)localObject;
    localObject = new InternalLogLevel[4];
    InternalLogLevel localInternalLogLevel = NONE;
    localObject[0] = localInternalLogLevel;
    localInternalLogLevel = ERROR;
    localObject[i] = localInternalLogLevel;
    localInternalLogLevel = DEBUG;
    localObject[j] = localInternalLogLevel;
    localInternalLogLevel = INTERNAL;
    localObject[k] = localInternalLogLevel;
    $VALUES = (InternalLogLevel[])localObject;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.utilities.Logger.InternalLogLevel
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */