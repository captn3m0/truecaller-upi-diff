package com.inmobi.commons.core.utilities;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build.VERSION;
import android.os.PowerManager;
import com.inmobi.commons.a.a;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class d
{
  private static final String a = "d";
  
  private static String a(String paramString)
  {
    String str1 = "";
    String str2 = "UTF-8";
    try
    {
      str1 = URLEncoder.encode(paramString, str2);
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      for (;;) {}
    }
    return str1;
  }
  
  public static String a(String paramString, Map paramMap)
  {
    if (paramMap != null)
    {
      int i = paramMap.size();
      if (i > 0)
      {
        paramMap = paramMap.entrySet().iterator();
        for (;;)
        {
          boolean bool = paramMap.hasNext();
          if (!bool) {
            break;
          }
          Object localObject = (Map.Entry)paramMap.next();
          CharSequence localCharSequence = (CharSequence)((Map.Entry)localObject).getKey();
          localObject = (CharSequence)((Map.Entry)localObject).getValue();
          paramString = paramString.replace(localCharSequence, (CharSequence)localObject);
        }
      }
    }
    return paramString;
  }
  
  public static String a(Map paramMap, String paramString)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    paramMap = paramMap.entrySet().iterator();
    for (;;)
    {
      boolean bool = paramMap.hasNext();
      if (!bool) {
        break;
      }
      Object localObject = (Map.Entry)paramMap.next();
      int i = localStringBuilder.length();
      if (i > 0) {
        localStringBuilder.append(paramString);
      }
      Locale localLocale = Locale.US;
      String str1 = "%s=%s";
      int j = 2;
      Object[] arrayOfObject = new Object[j];
      String str2 = a((String)((Map.Entry)localObject).getKey());
      arrayOfObject[0] = str2;
      int k = 1;
      localObject = a((String)((Map.Entry)localObject).getValue());
      arrayOfObject[k] = localObject;
      localObject = String.format(localLocale, str1, arrayOfObject);
      localStringBuilder.append((String)localObject);
    }
    return localStringBuilder.toString();
  }
  
  public static void a(Closeable paramCloseable)
  {
    if (paramCloseable != null) {
      try
      {
        paramCloseable.close();
      }
      catch (IOException localIOException) {}
    }
  }
  
  public static void a(Map paramMap)
  {
    if (paramMap != null)
    {
      Iterator localIterator = paramMap.entrySet().iterator();
      HashMap localHashMap = new java/util/HashMap;
      localHashMap.<init>();
      for (;;)
      {
        boolean bool1 = localIterator.hasNext();
        if (!bool1) {
          break;
        }
        Object localObject1 = (Map.Entry)localIterator.next();
        Object localObject2 = ((Map.Entry)localObject1).getValue();
        if (localObject2 != null)
        {
          localObject2 = ((String)((Map.Entry)localObject1).getValue()).trim();
          int i = ((String)localObject2).length();
          if (i != 0)
          {
            localObject2 = ((Map.Entry)localObject1).getKey();
            if (localObject2 != null)
            {
              localObject2 = ((String)((Map.Entry)localObject1).getKey()).trim();
              i = ((String)localObject2).length();
              if (i != 0)
              {
                localObject2 = (String)((Map.Entry)localObject1).getKey();
                String str = ((String)((Map.Entry)localObject1).getKey()).trim();
                boolean bool2 = ((String)localObject2).equals(str);
                if (!bool2)
                {
                  localIterator.remove();
                  localObject2 = ((String)((Map.Entry)localObject1).getKey()).trim();
                  localObject1 = ((String)((Map.Entry)localObject1).getValue()).trim();
                  localHashMap.put(localObject2, localObject1);
                  continue;
                }
                localObject2 = ((Map.Entry)localObject1).getKey();
                localObject1 = ((String)((Map.Entry)localObject1).getValue()).trim();
                localHashMap.put(localObject2, localObject1);
                continue;
              }
            }
          }
        }
        localIterator.remove();
      }
      paramMap.putAll(localHashMap);
    }
  }
  
  public static boolean a()
  {
    boolean bool1 = false;
    try
    {
      Object localObject = a.b();
      String str = "connectivity";
      localObject = ((Context)localObject).getSystemService(str);
      localObject = (ConnectivityManager)localObject;
      localObject = ((ConnectivityManager)localObject).getActiveNetworkInfo();
      if (localObject != null)
      {
        boolean bool2 = ((NetworkInfo)localObject).isConnected();
        if (bool2)
        {
          bool2 = b();
          if (!bool2) {
            bool1 = true;
          }
        }
      }
      return bool1;
    }
    catch (Exception localException)
    {
      localException.getMessage();
    }
    return false;
  }
  
  public static byte[] a(InputStream paramInputStream)
  {
    ByteArrayOutputStream localByteArrayOutputStream = new java/io/ByteArrayOutputStream;
    localByteArrayOutputStream.<init>();
    int i = 4096;
    byte[] arrayOfByte = new byte[i];
    for (;;)
    {
      int j = -1;
      try
      {
        int k = paramInputStream.read(arrayOfByte);
        if (j != k)
        {
          j = 0;
          localByteArrayOutputStream.write(arrayOfByte, 0, k);
          continue;
        }
        paramInputStream = localByteArrayOutputStream.toByteArray();
        return paramInputStream;
      }
      finally
      {
        localByteArrayOutputStream.close();
      }
    }
  }
  
  /* Error */
  public static byte[] a(byte[] paramArrayOfByte)
  {
    // Byte code:
    //   0: new 180	java/io/ByteArrayInputStream
    //   3: astore_1
    //   4: aload_1
    //   5: aload_0
    //   6: invokespecial 183	java/io/ByteArrayInputStream:<init>	([B)V
    //   9: aconst_null
    //   10: astore_0
    //   11: new 185	java/util/zip/GZIPInputStream
    //   14: astore_2
    //   15: aload_2
    //   16: aload_1
    //   17: invokespecial 188	java/util/zip/GZIPInputStream:<init>	(Ljava/io/InputStream;)V
    //   20: aload_2
    //   21: invokestatic 191	com/inmobi/commons/core/utilities/d:a	(Ljava/io/InputStream;)[B
    //   24: astore_0
    //   25: aload_1
    //   26: invokestatic 194	com/inmobi/commons/core/utilities/d:a	(Ljava/io/Closeable;)V
    //   29: aload_2
    //   30: invokestatic 194	com/inmobi/commons/core/utilities/d:a	(Ljava/io/Closeable;)V
    //   33: aload_0
    //   34: areturn
    //   35: astore_3
    //   36: goto +16 -> 52
    //   39: astore 4
    //   41: aconst_null
    //   42: astore_2
    //   43: aload 4
    //   45: astore_0
    //   46: goto +41 -> 87
    //   49: astore_3
    //   50: aconst_null
    //   51: astore_2
    //   52: getstatic 200	com/inmobi/commons/core/utilities/Logger$InternalLogLevel:DEBUG	Lcom/inmobi/commons/core/utilities/Logger$InternalLogLevel;
    //   55: astore 5
    //   57: getstatic 202	com/inmobi/commons/core/utilities/d:a	Ljava/lang/String;
    //   60: astore 6
    //   62: ldc -52
    //   64: astore 7
    //   66: aload 5
    //   68: aload 6
    //   70: aload 7
    //   72: aload_3
    //   73: invokestatic 209	com/inmobi/commons/core/utilities/Logger:a	(Lcom/inmobi/commons/core/utilities/Logger$InternalLogLevel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   76: aload_1
    //   77: invokestatic 194	com/inmobi/commons/core/utilities/d:a	(Ljava/io/Closeable;)V
    //   80: aload_2
    //   81: invokestatic 194	com/inmobi/commons/core/utilities/d:a	(Ljava/io/Closeable;)V
    //   84: aconst_null
    //   85: areturn
    //   86: astore_0
    //   87: aload_1
    //   88: invokestatic 194	com/inmobi/commons/core/utilities/d:a	(Ljava/io/Closeable;)V
    //   91: aload_2
    //   92: invokestatic 194	com/inmobi/commons/core/utilities/d:a	(Ljava/io/Closeable;)V
    //   95: aload_0
    //   96: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	97	0	paramArrayOfByte	byte[]
    //   3	85	1	localByteArrayInputStream	java.io.ByteArrayInputStream
    //   14	78	2	localGZIPInputStream	java.util.zip.GZIPInputStream
    //   35	1	3	localIOException1	IOException
    //   49	24	3	localIOException2	IOException
    //   39	5	4	localObject	Object
    //   55	12	5	localInternalLogLevel	Logger.InternalLogLevel
    //   60	9	6	str1	String
    //   64	7	7	str2	String
    // Exception table:
    //   from	to	target	type
    //   20	24	35	java/io/IOException
    //   11	14	39	finally
    //   16	20	39	finally
    //   11	14	49	java/io/IOException
    //   16	20	49	java/io/IOException
    //   20	24	86	finally
    //   52	55	86	finally
    //   57	60	86	finally
    //   72	76	86	finally
  }
  
  private static boolean b()
  {
    boolean bool = false;
    try
    {
      Object localObject = a.b();
      String str = "power";
      localObject = ((Context)localObject).getSystemService(str);
      localObject = (PowerManager)localObject;
      int i = Build.VERSION.SDK_INT;
      int j = 22;
      if (i > j) {
        bool = ((PowerManager)localObject).isDeviceIdleMode();
      }
    }
    catch (Exception localException)
    {
      localException.getMessage();
    }
    return bool;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.utilities.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */