package com.inmobi.commons.core.utilities;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

final class a$a
  extends Handler
{
  boolean a = true;
  
  a$a(Looper paramLooper)
  {
    super(paramLooper);
  }
  
  public final void handleMessage(Message paramMessage)
  {
    boolean bool1 = a.g();
    if (bool1) {
      return;
    }
    int i = what;
    int k = 1001;
    if (i == k)
    {
      boolean bool2 = a;
      if (bool2)
      {
        a = false;
        a.a(false);
        a.f();
        return;
      }
    }
    int m = what;
    int j = 1002;
    if (m == j)
    {
      boolean bool3 = a;
      if (!bool3)
      {
        bool3 = true;
        a = bool3;
        a.a(bool3);
        a.f();
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.utilities.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */