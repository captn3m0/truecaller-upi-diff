package com.inmobi.commons.core.utilities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build.VERSION;
import android.os.PowerManager;

final class g$a
  extends BroadcastReceiver
{
  private static final String a = "g$a";
  
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    if (paramIntent != null) {
      try
      {
        String str1 = paramIntent.getAction();
        if (str1 != null)
        {
          str1 = "android.net.conn.CONNECTIVITY_CHANGE";
          String str2 = paramIntent.getAction();
          boolean bool1 = str1.equals(str2);
          boolean bool2 = false;
          str2 = null;
          boolean bool3;
          if (bool1)
          {
            str1 = "connectivity";
            paramContext = paramContext.getSystemService(str1);
            paramContext = (ConnectivityManager)paramContext;
            if (paramContext != null)
            {
              paramContext = paramContext.getActiveNetworkInfo();
              if (paramContext != null)
              {
                bool3 = paramContext.isConnected();
                if (bool3) {
                  bool2 = true;
                }
              }
            }
          }
          else
          {
            str1 = "android.os.action.DEVICE_IDLE_MODE_CHANGED";
            String str3 = paramIntent.getAction();
            bool1 = str1.equalsIgnoreCase(str3);
            if (bool1)
            {
              str1 = "power";
              paramContext = paramContext.getSystemService(str1);
              paramContext = (PowerManager)paramContext;
              if (paramContext != null)
              {
                int i = Build.VERSION.SDK_INT;
                int j = 23;
                if (i >= j)
                {
                  bool3 = paramContext.isDeviceIdleMode();
                  if (bool3) {
                    bool2 = true;
                  }
                }
              }
            }
            else
            {
              paramContext = "android.intent.action.USER_PRESENT";
              str1 = paramIntent.getAction();
              bool3 = paramContext.equals(str1);
              if (bool3) {
                bool2 = true;
              }
            }
          }
          paramContext = paramIntent.getAction();
          g.a(bool2, paramContext);
          paramIntent.getAction();
        }
      }
      catch (Exception localException)
      {
        localException.getMessage();
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.utilities.g.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */