package com.inmobi.commons.core.utilities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import com.inmobi.commons.a.a;
import java.util.HashMap;
import java.util.concurrent.CopyOnWriteArrayList;

public class g
{
  private static final String a = "g";
  private static HashMap b;
  private static HashMap c;
  private static final Object d;
  private static volatile g e;
  
  static
  {
    Object localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    b = (HashMap)localObject;
    localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    c = (HashMap)localObject;
    localObject = new java/lang/Object;
    localObject.<init>();
    d = localObject;
  }
  
  public static g a()
  {
    g localg1 = e;
    if (localg1 == null) {
      synchronized (d)
      {
        localg1 = e;
        if (localg1 == null)
        {
          localg1 = new com/inmobi/commons/core/utilities/g;
          localg1.<init>();
          e = localg1;
        }
      }
    }
    return localg2;
  }
  
  public static void a(g.b paramb, String paramString)
  {
    Object localObject = (CopyOnWriteArrayList)b.get(paramString);
    if (localObject != null)
    {
      ((CopyOnWriteArrayList)localObject).remove(paramb);
      int i = ((CopyOnWriteArrayList)localObject).size();
      if (i == 0)
      {
        paramb = a.b();
        if (paramb != null)
        {
          localObject = c.get(paramString);
          if (localObject != null)
          {
            localObject = (BroadcastReceiver)c.get(paramString);
            paramb.unregisterReceiver((BroadcastReceiver)localObject);
            paramb = c;
            paramb.remove(paramString);
          }
        }
      }
    }
  }
  
  public static void a(String paramString, g.b paramb)
  {
    Object localObject = (CopyOnWriteArrayList)b.get(paramString);
    if (localObject != null)
    {
      ((CopyOnWriteArrayList)localObject).add(paramb);
    }
    else
    {
      localObject = new java/util/concurrent/CopyOnWriteArrayList;
      ((CopyOnWriteArrayList)localObject).<init>();
      ((CopyOnWriteArrayList)localObject).add(paramb);
    }
    paramb = b;
    paramb.put(paramString, localObject);
    int i = ((CopyOnWriteArrayList)localObject).size();
    int j = 1;
    if (i == j)
    {
      paramb = a.b();
      if (paramb != null)
      {
        localObject = new com/inmobi/commons/core/utilities/g$a;
        ((g.a)localObject).<init>();
        c.put(paramString, localObject);
        IntentFilter localIntentFilter = new android/content/IntentFilter;
        localIntentFilter.<init>(paramString);
        paramb.registerReceiver((BroadcastReceiver)localObject, localIntentFilter);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.utilities.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */