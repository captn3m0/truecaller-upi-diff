package com.inmobi.commons.core.utilities;

import android.content.Context;
import com.inmobi.commons.core.e.b;
import java.util.HashMap;
import java.util.Map;

public final class e
{
  public static boolean a(Context paramContext, String paramString1, String paramString2)
  {
    try
    {
      int i = paramContext.checkCallingOrSelfPermission(paramString2);
      return i == 0;
    }
    catch (Exception paramContext)
    {
      try
      {
        paramString2 = new java/util/HashMap;
        paramString2.<init>();
        String str1 = "type";
        Object localObject = "RuntimeException";
        paramString2.put(str1, localObject);
        str1 = "message";
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>();
        String str2 = paramContext.getMessage();
        ((StringBuilder)localObject).append(str2);
        localObject = ((StringBuilder)localObject).toString();
        paramString2.put(str1, localObject);
        b.a();
        str1 = "ExceptionCaught";
        b.a(paramString1, str1, paramString2);
      }
      catch (Exception localException)
      {
        paramContext.getMessage();
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.utilities.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */