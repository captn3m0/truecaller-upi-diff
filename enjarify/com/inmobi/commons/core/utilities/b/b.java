package com.inmobi.commons.core.utilities.b;

import android.content.Context;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import com.inmobi.commons.a.a;
import com.inmobi.commons.core.utilities.e;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class b
{
  public static int a()
  {
    String str1 = b();
    String str2 = "0";
    boolean bool1 = str1.startsWith(str2);
    if (bool1) {
      return 0;
    }
    str2 = "1";
    boolean bool2 = str1.startsWith(str2);
    if (bool2) {
      return 1;
    }
    return 2;
  }
  
  public static int a(Context paramContext)
  {
    return ((AudioManager)paramContext.getSystemService("audio")).getStreamVolume(3);
  }
  
  public static Map a(boolean paramBoolean)
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    Object localObject1 = "d-brand-name";
    try
    {
      Object localObject2 = Build.BRAND;
      localHashMap.put(localObject1, localObject2);
      localObject1 = "d-manufacturer-name";
      localObject2 = Build.MANUFACTURER;
      localHashMap.put(localObject1, localObject2);
      localObject1 = "d-model-name";
      localObject2 = Build.MODEL;
      localHashMap.put(localObject1, localObject2);
      localObject1 = "d-nettype-raw";
      localObject2 = b();
      localHashMap.put(localObject1, localObject2);
      localObject1 = "d-localization";
      localObject2 = Locale.getDefault();
      localObject2 = ((Locale)localObject2).toString();
      localHashMap.put(localObject1, localObject2);
      localObject1 = "d-media-volume";
      localObject2 = a.b();
      int j;
      if ((localObject2 != null) && (!paramBoolean))
      {
        localObject3 = "audio";
        localObject3 = ((Context)localObject2).getSystemService((String)localObject3);
        localObject3 = (AudioManager)localObject3;
        int i = 3;
        boolean bool = ((AudioManager)localObject3).getStreamVolume(i);
        paramBoolean = ((AudioManager)localObject3).getStreamMaxVolume(i);
        bool *= true;
        bool /= paramBoolean;
      }
      else
      {
        j = 0;
      }
      Object localObject3 = String.valueOf(j);
      localHashMap.put(localObject1, localObject3);
    }
    catch (Exception localException)
    {
      localObject1 = b.class;
      ((Class)localObject1).getSimpleName();
      localException.getMessage();
    }
    return localHashMap;
  }
  
  private static String b()
  {
    Object localObject1 = a.b();
    if (localObject1 == null) {
      return "";
    }
    String str1 = "";
    Object localObject2 = "root";
    String str2 = "android.permission.ACCESS_NETWORK_STATE";
    boolean bool = e.a((Context)localObject1, (String)localObject2, str2);
    if (bool)
    {
      localObject2 = "connectivity";
      localObject1 = (ConnectivityManager)((Context)localObject1).getSystemService((String)localObject2);
      if (localObject1 != null)
      {
        localObject1 = ((ConnectivityManager)localObject1).getActiveNetworkInfo();
        if (localObject1 != null)
        {
          int i = ((NetworkInfo)localObject1).getType();
          int j = ((NetworkInfo)localObject1).getSubtype();
          if (i == 0)
          {
            localObject2 = new java/lang/StringBuilder;
            ((StringBuilder)localObject2).<init>();
            ((StringBuilder)localObject2).append(i);
            ((StringBuilder)localObject2).append("|");
            ((StringBuilder)localObject2).append(j);
            str1 = ((StringBuilder)localObject2).toString();
          }
          else
          {
            j = 1;
            if (i == j) {
              str1 = "1";
            } else {
              str1 = Integer.toString(i);
            }
          }
        }
      }
    }
    return str1;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.utilities.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */