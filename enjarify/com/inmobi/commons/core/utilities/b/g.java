package com.inmobi.commons.core.utilities.b;

import android.location.Location;
import com.inmobi.commons.a.a;
import com.inmobi.commons.core.d.c;
import java.util.HashMap;
import java.util.Locale;

public final class g
{
  private static int a = Integer.MIN_VALUE;
  private static String b;
  private static String c;
  private static String d;
  private static String e;
  private static String f;
  private static String g;
  private static int h = Integer.MIN_VALUE;
  private static String i;
  private static String j;
  private static String k;
  private static String l;
  private static Location m;
  
  public static String a()
  {
    return c.a("user_info_store");
  }
  
  public static void a(int paramInt)
  {
    boolean bool = a.a();
    if (bool)
    {
      int n = -1 << -1;
      if (paramInt != n)
      {
        c.b("user_info_store").a("user_age", paramInt);
        return;
      }
    }
    a = paramInt;
  }
  
  public static void a(Location paramLocation)
  {
    boolean bool = a.a();
    if ((bool) && (paramLocation != null))
    {
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      double d1 = paramLocation.getLatitude();
      localStringBuilder.append(d1);
      localStringBuilder.append(",");
      d1 = paramLocation.getLongitude();
      localStringBuilder.append(d1);
      localStringBuilder.append(",");
      int n = (int)paramLocation.getAccuracy();
      localStringBuilder.append(n);
      localStringBuilder.append(",");
      long l1 = paramLocation.getTime();
      localStringBuilder.append(l1);
      paramLocation = localStringBuilder.toString();
      c.b("user_info_store").a("user_location", paramLocation);
      return;
    }
    m = paramLocation;
  }
  
  public static void a(String paramString)
  {
    boolean bool = a.a();
    if ((bool) && (paramString != null))
    {
      c.b("user_info_store").a("user_age_group", paramString);
      return;
    }
    b = paramString;
  }
  
  public static void b()
  {
    a(a);
    a(b);
    b(c);
    c(d);
    d(e);
    e(f);
    f(g);
    b(h);
    g(i);
    h(j);
    i(k);
    j(l);
    a(m);
  }
  
  public static void b(int paramInt)
  {
    boolean bool = a.a();
    if (bool)
    {
      int n = -1 << -1;
      if (paramInt != n)
      {
        c.b("user_info_store").a("user_yob", paramInt);
        return;
      }
    }
    h = paramInt;
  }
  
  public static void b(String paramString)
  {
    boolean bool = a.a();
    if ((bool) && (paramString != null))
    {
      c.b("user_info_store").a("user_area_code", paramString);
      return;
    }
    c = paramString;
  }
  
  public static Location c()
  {
    Object localObject1 = m;
    if (localObject1 != null) {
      return (Location)localObject1;
    }
    localObject1 = c.b("user_info_store").c("user_location");
    Object localObject2 = null;
    if (localObject1 == null) {
      return null;
    }
    Location localLocation = new android/location/Location;
    localLocation.<init>("");
    String str = ",";
    try
    {
      localObject1 = ((String)localObject1).split(str);
      int n = 0;
      float f1 = 0.0F;
      str = null;
      str = localObject1[0];
      double d1 = Double.parseDouble(str);
      localLocation.setLatitude(d1);
      n = 1;
      f1 = Float.MIN_VALUE;
      str = localObject1[n];
      d1 = Double.parseDouble(str);
      localLocation.setLongitude(d1);
      n = 2;
      f1 = 2.8E-45F;
      str = localObject1[n];
      f1 = Float.parseFloat(str);
      localLocation.setAccuracy(f1);
      n = 3;
      f1 = 4.2E-45F;
      localObject1 = localObject1[n];
      long l1 = Long.parseLong((String)localObject1);
      localLocation.setTime(l1);
      localObject2 = localLocation;
    }
    catch (NumberFormatException|ArrayIndexOutOfBoundsException localNumberFormatException)
    {
      for (;;) {}
    }
    return (Location)localObject2;
  }
  
  public static void c(String paramString)
  {
    boolean bool = a.a();
    if ((bool) && (paramString != null))
    {
      c.b("user_info_store").a("user_post_code", paramString);
      return;
    }
    d = paramString;
  }
  
  public static HashMap d()
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    int n = a;
    int i1 = -1 << -1;
    if (n == i1)
    {
      localObject1 = c.b("user_info_store");
      localObject2 = "user_age";
      n = ((c)localObject1).d((String)localObject2);
    }
    if ((n != i1) && (n > 0))
    {
      localObject2 = "u-age";
      localObject1 = String.valueOf(n);
      localHashMap.put(localObject2, localObject1);
    }
    n = h;
    if (n == i1)
    {
      localObject1 = c.b("user_info_store");
      localObject2 = "user_yob";
      n = ((c)localObject1).d((String)localObject2);
    }
    if ((n != i1) && (n > 0))
    {
      localObject3 = "u-yearofbirth";
      localObject1 = String.valueOf(n);
      localHashMap.put(localObject3, localObject1);
    }
    Object localObject1 = e;
    if (localObject1 == null)
    {
      localObject1 = c.b("user_info_store");
      localObject3 = "user_city_code";
      localObject1 = ((c)localObject1).c((String)localObject3);
    }
    Object localObject3 = f;
    if (localObject3 == null)
    {
      localObject3 = c.b("user_info_store");
      localObject2 = "user_state_code";
      localObject3 = ((c)localObject3).c((String)localObject2);
    }
    Object localObject2 = g;
    if (localObject2 == null)
    {
      localObject2 = c.b("user_info_store");
      str1 = "user_country_code";
      localObject2 = ((c)localObject2).c(str1);
    }
    String str1 = "";
    if (localObject1 != null)
    {
      String str2 = ((String)localObject1).trim();
      int i2 = str2.length();
      if (i2 != 0) {
        str1 = ((String)localObject1).trim();
      }
    }
    if (localObject3 != null)
    {
      localObject1 = ((String)localObject3).trim();
      n = ((String)localObject1).length();
      if (n != 0)
      {
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>();
        ((StringBuilder)localObject1).append(str1);
        ((StringBuilder)localObject1).append("-");
        localObject3 = ((String)localObject3).trim();
        ((StringBuilder)localObject1).append((String)localObject3);
        str1 = ((StringBuilder)localObject1).toString();
      }
    }
    if (localObject2 != null)
    {
      localObject1 = ((String)localObject2).trim();
      n = ((String)localObject1).length();
      if (n != 0)
      {
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>();
        ((StringBuilder)localObject1).append(str1);
        ((StringBuilder)localObject1).append("-");
        localObject3 = ((String)localObject2).trim();
        ((StringBuilder)localObject1).append((String)localObject3);
        str1 = ((StringBuilder)localObject1).toString();
      }
    }
    if (str1 != null)
    {
      localObject1 = str1.trim();
      n = ((String)localObject1).length();
      if (n != 0)
      {
        localObject1 = "u-location";
        localHashMap.put(localObject1, str1);
      }
    }
    localObject1 = b;
    if (localObject1 == null)
    {
      localObject1 = c.b("user_info_store");
      localObject3 = "user_age_group";
      localObject1 = ((c)localObject1).c((String)localObject3);
    }
    if (localObject1 != null)
    {
      localObject3 = "u-agegroup";
      localObject1 = ((String)localObject1).toString();
      localObject2 = Locale.ENGLISH;
      localObject1 = ((String)localObject1).toLowerCase((Locale)localObject2);
      localHashMap.put(localObject3, localObject1);
    }
    localObject1 = c;
    if (localObject1 == null)
    {
      localObject1 = c.b("user_info_store");
      localObject3 = "user_area_code";
      localObject1 = ((c)localObject1).c((String)localObject3);
    }
    if (localObject1 != null)
    {
      localObject3 = "u-areacode";
      localHashMap.put(localObject3, localObject1);
    }
    localObject1 = d;
    if (localObject1 == null)
    {
      localObject1 = c.b("user_info_store");
      localObject3 = "user_post_code";
      localObject1 = ((c)localObject1).c((String)localObject3);
    }
    if (localObject1 != null)
    {
      localObject3 = "u-postalcode";
      localHashMap.put(localObject3, localObject1);
    }
    localObject1 = i;
    if (localObject1 == null)
    {
      localObject1 = c.b("user_info_store");
      localObject3 = "user_gender";
      localObject1 = ((c)localObject1).c((String)localObject3);
    }
    if (localObject1 != null)
    {
      localObject3 = "u-gender";
      localHashMap.put(localObject3, localObject1);
    }
    localObject1 = j;
    if (localObject1 == null)
    {
      localObject1 = c.b("user_info_store");
      localObject3 = "user_education";
      localObject1 = ((c)localObject1).c((String)localObject3);
    }
    if (localObject1 != null)
    {
      localObject3 = "u-education";
      localHashMap.put(localObject3, localObject1);
    }
    localObject1 = k;
    if (localObject1 == null)
    {
      localObject1 = c.b("user_info_store");
      localObject3 = "user_language";
      localObject1 = ((c)localObject1).c((String)localObject3);
    }
    if (localObject1 != null)
    {
      localObject3 = "u-language";
      localHashMap.put(localObject3, localObject1);
    }
    localObject1 = l;
    if (localObject1 == null)
    {
      localObject1 = c.b("user_info_store");
      localObject3 = "user_interest";
      localObject1 = ((c)localObject1).c((String)localObject3);
    }
    if (localObject1 != null)
    {
      localObject3 = "u-interests";
      localHashMap.put(localObject3, localObject1);
    }
    return localHashMap;
  }
  
  public static void d(String paramString)
  {
    boolean bool = a.a();
    if ((bool) && (paramString != null))
    {
      c.b("user_info_store").a("user_city_code", paramString);
      return;
    }
    e = paramString;
  }
  
  public static void e(String paramString)
  {
    boolean bool = a.a();
    if ((bool) && (paramString != null))
    {
      c.b("user_info_store").a("user_state_code", paramString);
      return;
    }
    f = paramString;
  }
  
  public static void f(String paramString)
  {
    boolean bool = a.a();
    if ((bool) && (paramString != null))
    {
      c.b("user_info_store").a("user_country_code", paramString);
      return;
    }
    g = paramString;
  }
  
  public static void g(String paramString)
  {
    boolean bool = a.a();
    if ((bool) && (paramString != null))
    {
      c.b("user_info_store").a("user_gender", paramString);
      return;
    }
    i = paramString;
  }
  
  public static void h(String paramString)
  {
    boolean bool = a.a();
    if ((bool) && (paramString != null))
    {
      c.b("user_info_store").a("user_education", paramString);
      return;
    }
    j = paramString;
  }
  
  public static void i(String paramString)
  {
    boolean bool = a.a();
    if ((bool) && (paramString != null))
    {
      c.b("user_info_store").a("user_language", paramString);
      return;
    }
    k = paramString;
  }
  
  public static void j(String paramString)
  {
    boolean bool = a.a();
    if ((bool) && (paramString != null))
    {
      c.b("user_info_store").a("user_interest", paramString);
      return;
    }
    l = paramString;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.utilities.b.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */