package com.inmobi.commons.core.utilities.b;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import android.widget.TextView;
import com.inmobi.commons.a.a;
import java.util.HashMap;
import java.util.Map;

public class c
{
  private static final String a = "c";
  
  public static int a(int paramInt)
  {
    float f = ac;
    return Math.round(paramInt * f);
  }
  
  public static d a()
  {
    Object localObject = a.b();
    if (localObject == null)
    {
      localObject = new com/inmobi/commons/core/utilities/b/d;
      ((d)localObject).<init>(0, 0, 2.0F);
      return (d)localObject;
    }
    DisplayMetrics localDisplayMetrics = new android/util/DisplayMetrics;
    localDisplayMetrics.<init>();
    ((WindowManager)((Context)localObject).getSystemService("window")).getDefaultDisplay().getMetrics(localDisplayMetrics);
    float f = density;
    int i = Math.round(widthPixels / f);
    int j = Math.round(heightPixels / f);
    d locald = new com/inmobi/commons/core/utilities/b/d;
    locald.<init>(i, j, f);
    return locald;
  }
  
  public static int b()
  {
    Object localObject = a.b();
    int i = 1;
    if (localObject == null) {
      return i;
    }
    Display localDisplay = ((WindowManager)((Context)localObject).getSystemService("window")).getDefaultDisplay();
    int j = localDisplay.getRotation();
    localObject = ((Context)localObject).getResources().getConfiguration();
    int k = orientation;
    switch (k)
    {
    default: 
      return i;
    case 2: 
      if ((j != 0) && (j != i)) {
        return 4;
      }
      return 3;
    }
    k = 2;
    if ((j != i) && (j != k)) {
      return i;
    }
    return k;
  }
  
  public static int b(int paramInt)
  {
    float f = ac;
    return Math.round(paramInt / f);
  }
  
  public static Map c()
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    String str = "d-device-screen-density";
    try
    {
      Object localObject1 = a();
      float f = c;
      localObject1 = String.valueOf(f);
      localHashMap.put(str, localObject1);
      str = "d-device-screen-size";
      localObject1 = a();
      Object localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      int i = a;
      ((StringBuilder)localObject2).append(i);
      Object localObject3 = "X";
      ((StringBuilder)localObject2).append((String)localObject3);
      int j = b;
      ((StringBuilder)localObject2).append(j);
      localObject1 = ((StringBuilder)localObject2).toString();
      localHashMap.put(str, localObject1);
      str = "d-density-dependent-screen-size";
      localObject1 = a.b();
      if (localObject1 == null)
      {
        localObject1 = "0x0";
      }
      else
      {
        localObject2 = new android/util/DisplayMetrics;
        ((DisplayMetrics)localObject2).<init>();
        localObject3 = "window";
        localObject1 = ((Context)localObject1).getSystemService((String)localObject3);
        localObject1 = (WindowManager)localObject1;
        localObject1 = ((WindowManager)localObject1).getDefaultDisplay();
        ((Display)localObject1).getMetrics((DisplayMetrics)localObject2);
        j = widthPixels;
        int k = heightPixels;
        localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>();
        ((StringBuilder)localObject3).append(j);
        localObject1 = "x";
        ((StringBuilder)localObject3).append((String)localObject1);
        ((StringBuilder)localObject3).append(k);
        localObject1 = ((StringBuilder)localObject3).toString();
      }
      localHashMap.put(str, localObject1);
      str = "d-orientation";
      j = b();
      localObject1 = String.valueOf(j);
      localHashMap.put(str, localObject1);
      str = "d-textsize";
      localObject1 = a.b();
      localObject2 = new android/widget/TextView;
      ((TextView)localObject2).<init>((Context)localObject1);
      f = ((TextView)localObject2).getTextSize();
      localObject1 = String.valueOf(f);
      localHashMap.put(str, localObject1);
    }
    catch (Exception localException)
    {
      localException.getMessage();
    }
    return localHashMap;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.utilities.b.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */