package com.inmobi.commons.core.utilities.b;

import com.inmobi.commons.core.configs.b;
import com.inmobi.commons.core.configs.b.c;
import com.inmobi.commons.core.configs.h;
import org.json.JSONException;
import org.json.JSONObject;

public final class e
{
  private static final String a = "com.inmobi.commons.core.utilities.b.e";
  private static JSONObject b;
  private static h c;
  private static b.c d;
  
  static
  {
    Object localObject = new com/inmobi/commons/core/configs/h;
    ((h)localObject).<init>();
    c = (h)localObject;
    localObject = new com/inmobi/commons/core/utilities/b/e$1;
    ((e.1)localObject).<init>();
    d = (b.c)localObject;
  }
  
  public static int a(boolean paramBoolean)
  {
    h localh = c;
    boolean bool1 = d;
    int i = f();
    int j = 0;
    boolean bool2 = true;
    if ((i != bool2) && (bool1 != bool2) && (!paramBoolean))
    {
      if (bool1)
      {
        paramBoolean = true;
        if ((bool1 == paramBoolean) && (i != 0)) {
          j = 1;
        }
      }
    }
    else {
      j = 1;
    }
    return j;
  }
  
  public static JSONObject a()
  {
    return b;
  }
  
  public static void a(JSONObject paramJSONObject)
  {
    b = paramJSONObject;
  }
  
  public static boolean b()
  {
    JSONObject localJSONObject = b;
    if (localJSONObject != null)
    {
      String str = "gdpr_consent_available";
      try
      {
        boolean bool1 = localJSONObject.has(str);
        if (bool1)
        {
          str = "gdpr_consent_available";
          boolean bool2 = localJSONObject.getBoolean(str);
          if (!bool2) {
            return true;
          }
        }
      }
      catch (JSONException localJSONException)
      {
        return false;
      }
    }
    return false;
  }
  
  public static void c()
  {
    b localb = b.a();
    h localh = c;
    b.c localc = d;
    localb.a(localh, localc);
  }
  
  public static int d()
  {
    return a(false);
  }
  
  public static boolean e()
  {
    boolean bool = false;
    int i = a(false);
    if (i != 0) {
      bool = true;
    }
    return bool;
  }
  
  private static int f()
  {
    JSONObject localJSONObject = b;
    int i = -1;
    if (localJSONObject != null)
    {
      String str = "gdpr_consent_available";
      boolean bool1 = localJSONObject.has(str);
      if (bool1)
      {
        str = "gdpr_consent_available";
        try
        {
          boolean bool2 = localJSONObject.getBoolean(str);
          if (bool2) {
            return 1;
          }
          return 0;
        }
        catch (JSONException localJSONException)
        {
          return i;
        }
      }
    }
    return i;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.utilities.b.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */