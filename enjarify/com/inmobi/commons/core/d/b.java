package com.inmobi.commons.core.d;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public final class b
{
  private static final String a = "b";
  private static volatile b b;
  private static final Object c;
  private static final Object d;
  private static int e = 0;
  private SQLiteDatabase f;
  
  static
  {
    Object localObject = new java/lang/Object;
    localObject.<init>();
    c = localObject;
    localObject = new java/lang/Object;
    localObject.<init>();
    d = localObject;
  }
  
  private b()
  {
    Object localObject = new com/inmobi/commons/core/d/a;
    Context localContext = com.inmobi.commons.a.a.b();
    ((a)localObject).<init>(localContext);
    try
    {
      localObject = ((a)localObject).getWritableDatabase();
      f = ((SQLiteDatabase)localObject);
      b = this;
      return;
    }
    catch (Exception localException) {}
  }
  
  public static b a()
  {
    synchronized (b.class)
    {
      synchronized (d)
      {
        int i = e + 1;
        e = i;
        ??? = b;
        if (??? == null) {
          synchronized (c)
          {
            b localb = b;
            if (localb == null)
            {
              localb = new com/inmobi/commons/core/d/b;
              localb.<init>();
              b = localb;
            }
            ??? = localb;
          }
        }
        return (b)???;
      }
    }
  }
  
  /* Error */
  public final int a(String paramString)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: new 50	java/lang/StringBuilder
    //   5: astore_2
    //   6: ldc 52
    //   8: astore_3
    //   9: aload_2
    //   10: aload_3
    //   11: invokespecial 55	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   14: aload_2
    //   15: aload_1
    //   16: invokevirtual 59	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   19: pop
    //   20: ldc 61
    //   22: astore_1
    //   23: aload_2
    //   24: aload_1
    //   25: invokevirtual 59	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   28: pop
    //   29: aload_2
    //   30: invokevirtual 65	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   33: astore_1
    //   34: aload_0
    //   35: getfield 43	com/inmobi/commons/core/d/b:f	Landroid/database/sqlite/SQLiteDatabase;
    //   38: astore_2
    //   39: aconst_null
    //   40: astore_3
    //   41: aload_2
    //   42: aload_1
    //   43: aconst_null
    //   44: invokevirtual 71	android/database/sqlite/SQLiteDatabase:rawQuery	(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    //   47: astore_1
    //   48: aload_1
    //   49: invokeinterface 77 1 0
    //   54: pop
    //   55: iconst_0
    //   56: istore 4
    //   58: aconst_null
    //   59: astore_2
    //   60: aload_1
    //   61: iconst_0
    //   62: invokeinterface 81 2 0
    //   67: istore 4
    //   69: aload_1
    //   70: invokeinterface 84 1 0
    //   75: aload_0
    //   76: monitorexit
    //   77: iload 4
    //   79: ireturn
    //   80: astore_1
    //   81: goto +13 -> 94
    //   84: astore_1
    //   85: aload_1
    //   86: invokevirtual 87	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   89: pop
    //   90: aload_0
    //   91: monitorexit
    //   92: iconst_m1
    //   93: ireturn
    //   94: aload_0
    //   95: monitorexit
    //   96: aload_1
    //   97: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	98	0	this	b
    //   0	98	1	paramString	String
    //   5	55	2	localObject	Object
    //   8	33	3	str	String
    //   56	22	4	i	int
    // Exception table:
    //   from	to	target	type
    //   2	5	80	finally
    //   10	14	80	finally
    //   15	20	80	finally
    //   24	29	80	finally
    //   29	33	80	finally
    //   34	38	80	finally
    //   43	47	80	finally
    //   48	55	80	finally
    //   61	67	80	finally
    //   69	75	80	finally
    //   85	90	80	finally
    //   2	5	84	java/lang/Exception
    //   10	14	84	java/lang/Exception
    //   15	20	84	java/lang/Exception
    //   24	29	84	java/lang/Exception
    //   29	33	84	java/lang/Exception
    //   34	38	84	java/lang/Exception
    //   43	47	84	java/lang/Exception
    //   48	55	84	java/lang/Exception
    //   61	67	84	java/lang/Exception
    //   69	75	84	java/lang/Exception
  }
  
  /* Error */
  public final int a(String paramString1, String paramString2, String[] paramArrayOfString)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 43	com/inmobi/commons/core/d/b:f	Landroid/database/sqlite/SQLiteDatabase;
    //   6: astore 4
    //   8: aload 4
    //   10: aload_1
    //   11: aload_2
    //   12: aload_3
    //   13: invokevirtual 91	android/database/sqlite/SQLiteDatabase:delete	(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    //   16: istore 5
    //   18: aload_0
    //   19: monitorexit
    //   20: iload 5
    //   22: ireturn
    //   23: astore_1
    //   24: goto +13 -> 37
    //   27: astore_1
    //   28: aload_1
    //   29: invokevirtual 87	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   32: pop
    //   33: aload_0
    //   34: monitorexit
    //   35: iconst_m1
    //   36: ireturn
    //   37: aload_0
    //   38: monitorexit
    //   39: aload_1
    //   40: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	41	0	this	b
    //   0	41	1	paramString1	String
    //   0	41	2	paramString2	String
    //   0	41	3	paramArrayOfString	String[]
    //   6	3	4	localSQLiteDatabase	SQLiteDatabase
    //   16	5	5	i	int
    // Exception table:
    //   from	to	target	type
    //   2	6	23	finally
    //   12	16	23	finally
    //   28	33	23	finally
    //   2	6	27	java/lang/Exception
    //   12	16	27	java/lang/Exception
  }
  
  /* Error */
  public final java.util.List a(String paramString1, String[] paramArrayOfString1, String paramString2, String[] paramArrayOfString2, String paramString3, String paramString4, String paramString5, String paramString6)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: new 93	java/util/ArrayList
    //   5: astore 9
    //   7: aload 9
    //   9: invokespecial 94	java/util/ArrayList:<init>	()V
    //   12: aconst_null
    //   13: astore 10
    //   15: aload_0
    //   16: getfield 43	com/inmobi/commons/core/d/b:f	Landroid/database/sqlite/SQLiteDatabase;
    //   19: astore 11
    //   21: aload 11
    //   23: aload_1
    //   24: aload_2
    //   25: aload_3
    //   26: aload 4
    //   28: aload 5
    //   30: aload 6
    //   32: aload 7
    //   34: aload 8
    //   36: invokevirtual 98	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   39: astore 10
    //   41: aload 10
    //   43: invokeinterface 77 1 0
    //   48: istore 12
    //   50: iload 12
    //   52: ifeq +44 -> 96
    //   55: new 100	android/content/ContentValues
    //   58: astore 13
    //   60: aload 13
    //   62: invokespecial 101	android/content/ContentValues:<init>	()V
    //   65: aload 10
    //   67: aload 13
    //   69: invokestatic 107	android/database/DatabaseUtils:cursorRowToContentValues	(Landroid/database/Cursor;Landroid/content/ContentValues;)V
    //   72: aload 9
    //   74: aload 13
    //   76: invokeinterface 113 2 0
    //   81: pop
    //   82: aload 10
    //   84: invokeinterface 116 1 0
    //   89: istore 12
    //   91: iload 12
    //   93: ifne -38 -> 55
    //   96: aload 10
    //   98: invokeinterface 84 1 0
    //   103: aload 10
    //   105: ifnull +34 -> 139
    //   108: aload 10
    //   110: invokeinterface 84 1 0
    //   115: goto +24 -> 139
    //   118: astore 13
    //   120: goto +24 -> 144
    //   123: astore 13
    //   125: aload 13
    //   127: invokevirtual 87	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   130: pop
    //   131: aload 10
    //   133: ifnull +6 -> 139
    //   136: goto -28 -> 108
    //   139: aload_0
    //   140: monitorexit
    //   141: aload 9
    //   143: areturn
    //   144: aload 10
    //   146: ifnull +10 -> 156
    //   149: aload 10
    //   151: invokeinterface 84 1 0
    //   156: aload 13
    //   158: athrow
    //   159: astore 13
    //   161: aload_0
    //   162: monitorexit
    //   163: aload 13
    //   165: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	166	0	this	b
    //   0	166	1	paramString1	String
    //   0	166	2	paramArrayOfString1	String[]
    //   0	166	3	paramString2	String
    //   0	166	4	paramArrayOfString2	String[]
    //   0	166	5	paramString3	String
    //   0	166	6	paramString4	String
    //   0	166	7	paramString5	String
    //   0	166	8	paramString6	String
    //   5	137	9	localArrayList	java.util.ArrayList
    //   13	137	10	localCursor	android.database.Cursor
    //   19	3	11	localSQLiteDatabase	SQLiteDatabase
    //   48	44	12	bool	boolean
    //   58	17	13	localContentValues	android.content.ContentValues
    //   118	1	13	localObject1	Object
    //   123	34	13	localException	Exception
    //   159	5	13	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   15	19	118	finally
    //   34	39	118	finally
    //   41	48	118	finally
    //   55	58	118	finally
    //   60	65	118	finally
    //   67	72	118	finally
    //   74	82	118	finally
    //   82	89	118	finally
    //   96	103	118	finally
    //   125	131	118	finally
    //   15	19	123	java/lang/Exception
    //   34	39	123	java/lang/Exception
    //   41	48	123	java/lang/Exception
    //   55	58	123	java/lang/Exception
    //   60	65	123	java/lang/Exception
    //   67	72	123	java/lang/Exception
    //   74	82	123	java/lang/Exception
    //   82	89	123	java/lang/Exception
    //   96	103	123	java/lang/Exception
    //   2	5	159	finally
    //   7	12	159	finally
    //   108	115	159	finally
    //   149	156	159	finally
    //   156	159	159	finally
  }
  
  /* Error */
  public final void a(String paramString1, android.content.ContentValues paramContentValues, String paramString2, String[] paramArrayOfString)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_1
    //   4: aload_2
    //   5: invokevirtual 119	com/inmobi/commons/core/d/b:a	(Ljava/lang/String;Landroid/content/ContentValues;)Z
    //   8: istore 5
    //   10: iload 5
    //   12: ifne +13 -> 25
    //   15: aload_0
    //   16: aload_1
    //   17: aload_2
    //   18: aload_3
    //   19: aload 4
    //   21: invokevirtual 122	com/inmobi/commons/core/d/b:b	(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    //   24: pop
    //   25: aload_0
    //   26: monitorexit
    //   27: return
    //   28: astore_1
    //   29: goto +12 -> 41
    //   32: astore_1
    //   33: aload_1
    //   34: invokevirtual 87	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   37: pop
    //   38: aload_0
    //   39: monitorexit
    //   40: return
    //   41: aload_0
    //   42: monitorexit
    //   43: aload_1
    //   44: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	45	0	this	b
    //   0	45	1	paramString1	String
    //   0	45	2	paramContentValues	android.content.ContentValues
    //   0	45	3	paramString2	String
    //   0	45	4	paramArrayOfString	String[]
    //   8	3	5	bool	boolean
    // Exception table:
    //   from	to	target	type
    //   4	8	28	finally
    //   19	25	28	finally
    //   33	38	28	finally
    //   4	8	32	java/lang/Exception
    //   19	25	32	java/lang/Exception
  }
  
  /* Error */
  public final void a(String paramString1, String paramString2)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: new 50	java/lang/StringBuilder
    //   5: astore_3
    //   6: ldc 124
    //   8: astore 4
    //   10: aload_3
    //   11: aload 4
    //   13: invokespecial 55	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   16: aload_3
    //   17: aload_1
    //   18: invokevirtual 59	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   21: pop
    //   22: aload_3
    //   23: aload_2
    //   24: invokevirtual 59	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   27: pop
    //   28: ldc 126
    //   30: astore_1
    //   31: aload_3
    //   32: aload_1
    //   33: invokevirtual 59	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   36: pop
    //   37: aload_3
    //   38: invokevirtual 65	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   41: astore_1
    //   42: aload_0
    //   43: getfield 43	com/inmobi/commons/core/d/b:f	Landroid/database/sqlite/SQLiteDatabase;
    //   46: astore_2
    //   47: aload_2
    //   48: aload_1
    //   49: invokevirtual 129	android/database/sqlite/SQLiteDatabase:execSQL	(Ljava/lang/String;)V
    //   52: aload_0
    //   53: monitorexit
    //   54: return
    //   55: astore_1
    //   56: goto +12 -> 68
    //   59: astore_1
    //   60: aload_1
    //   61: invokevirtual 87	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   64: pop
    //   65: aload_0
    //   66: monitorexit
    //   67: return
    //   68: aload_0
    //   69: monitorexit
    //   70: aload_1
    //   71: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	72	0	this	b
    //   0	72	1	paramString1	String
    //   0	72	2	paramString2	String
    //   5	33	3	localStringBuilder	StringBuilder
    //   8	4	4	str	String
    // Exception table:
    //   from	to	target	type
    //   2	5	55	finally
    //   11	16	55	finally
    //   17	22	55	finally
    //   23	28	55	finally
    //   32	37	55	finally
    //   37	41	55	finally
    //   42	46	55	finally
    //   48	52	55	finally
    //   60	65	55	finally
    //   2	5	59	java/lang/Exception
    //   11	16	59	java/lang/Exception
    //   17	22	59	java/lang/Exception
    //   23	28	59	java/lang/Exception
    //   32	37	59	java/lang/Exception
    //   37	41	59	java/lang/Exception
    //   42	46	59	java/lang/Exception
    //   48	52	59	java/lang/Exception
  }
  
  /* Error */
  public final boolean a(String paramString, android.content.ContentValues paramContentValues)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 43	com/inmobi/commons/core/d/b:f	Landroid/database/sqlite/SQLiteDatabase;
    //   6: astore_3
    //   7: iconst_4
    //   8: istore 4
    //   10: aload_3
    //   11: aload_1
    //   12: aconst_null
    //   13: aload_2
    //   14: iload 4
    //   16: invokevirtual 134	android/database/sqlite/SQLiteDatabase:insertWithOnConflict	(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J
    //   19: lstore 5
    //   21: iconst_m1
    //   22: i2l
    //   23: lstore 7
    //   25: lload 5
    //   27: lload 7
    //   29: lcmp
    //   30: istore 4
    //   32: iload 4
    //   34: ifeq +7 -> 41
    //   37: aload_0
    //   38: monitorexit
    //   39: iconst_1
    //   40: ireturn
    //   41: aload_0
    //   42: monitorexit
    //   43: iconst_0
    //   44: ireturn
    //   45: astore_1
    //   46: goto +13 -> 59
    //   49: astore_1
    //   50: aload_1
    //   51: invokevirtual 87	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   54: pop
    //   55: aload_0
    //   56: monitorexit
    //   57: iconst_0
    //   58: ireturn
    //   59: aload_0
    //   60: monitorexit
    //   61: aload_1
    //   62: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	63	0	this	b
    //   0	63	1	paramString	String
    //   0	63	2	paramContentValues	android.content.ContentValues
    //   6	5	3	localSQLiteDatabase	SQLiteDatabase
    //   8	7	4	i	int
    //   30	3	4	bool	boolean
    //   19	7	5	l1	long
    //   23	5	7	l2	long
    // Exception table:
    //   from	to	target	type
    //   2	6	45	finally
    //   14	19	45	finally
    //   50	55	45	finally
    //   2	6	49	java/lang/Exception
    //   14	19	49	java/lang/Exception
  }
  
  /* Error */
  public final int b(String paramString1, android.content.ContentValues paramContentValues, String paramString2, String[] paramArrayOfString)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 43	com/inmobi/commons/core/d/b:f	Landroid/database/sqlite/SQLiteDatabase;
    //   6: astore 5
    //   8: iconst_4
    //   9: istore 6
    //   11: aload 5
    //   13: aload_1
    //   14: aload_2
    //   15: aload_3
    //   16: aload 4
    //   18: iload 6
    //   20: invokevirtual 139	android/database/sqlite/SQLiteDatabase:updateWithOnConflict	(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;I)I
    //   23: istore 7
    //   25: aload_0
    //   26: monitorexit
    //   27: iload 7
    //   29: ireturn
    //   30: astore_1
    //   31: goto +13 -> 44
    //   34: astore_1
    //   35: aload_1
    //   36: invokevirtual 87	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   39: pop
    //   40: aload_0
    //   41: monitorexit
    //   42: iconst_m1
    //   43: ireturn
    //   44: aload_0
    //   45: monitorexit
    //   46: aload_1
    //   47: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	48	0	this	b
    //   0	48	1	paramString1	String
    //   0	48	2	paramContentValues	android.content.ContentValues
    //   0	48	3	paramString2	String
    //   0	48	4	paramArrayOfString	String[]
    //   6	6	5	localSQLiteDatabase	SQLiteDatabase
    //   9	10	6	i	int
    //   23	5	7	j	int
    // Exception table:
    //   from	to	target	type
    //   2	6	30	finally
    //   18	23	30	finally
    //   35	40	30	finally
    //   2	6	34	java/lang/Exception
    //   18	23	34	java/lang/Exception
  }
  
  /* Error */
  public final int b(String paramString1, String paramString2, String[] paramArrayOfString)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: new 50	java/lang/StringBuilder
    //   5: astore 4
    //   7: ldc 52
    //   9: astore 5
    //   11: aload 4
    //   13: aload 5
    //   15: invokespecial 55	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   18: aload 4
    //   20: aload_1
    //   21: invokevirtual 59	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   24: pop
    //   25: ldc -115
    //   27: astore_1
    //   28: aload 4
    //   30: aload_1
    //   31: invokevirtual 59	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   34: pop
    //   35: aload 4
    //   37: aload_2
    //   38: invokevirtual 59	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   41: pop
    //   42: ldc 61
    //   44: astore_1
    //   45: aload 4
    //   47: aload_1
    //   48: invokevirtual 59	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   51: pop
    //   52: aload 4
    //   54: invokevirtual 65	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   57: astore_1
    //   58: aload_0
    //   59: getfield 43	com/inmobi/commons/core/d/b:f	Landroid/database/sqlite/SQLiteDatabase;
    //   62: astore_2
    //   63: aload_2
    //   64: aload_1
    //   65: aload_3
    //   66: invokevirtual 71	android/database/sqlite/SQLiteDatabase:rawQuery	(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    //   69: astore_1
    //   70: aload_1
    //   71: invokeinterface 77 1 0
    //   76: pop
    //   77: iconst_0
    //   78: istore 6
    //   80: aconst_null
    //   81: astore_2
    //   82: aload_1
    //   83: iconst_0
    //   84: invokeinterface 81 2 0
    //   89: istore 6
    //   91: aload_1
    //   92: invokeinterface 84 1 0
    //   97: aload_0
    //   98: monitorexit
    //   99: iload 6
    //   101: ireturn
    //   102: astore_1
    //   103: goto +13 -> 116
    //   106: astore_1
    //   107: aload_1
    //   108: invokevirtual 87	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   111: pop
    //   112: aload_0
    //   113: monitorexit
    //   114: iconst_m1
    //   115: ireturn
    //   116: aload_0
    //   117: monitorexit
    //   118: aload_1
    //   119: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	120	0	this	b
    //   0	120	1	paramString1	String
    //   0	120	2	paramString2	String
    //   0	120	3	paramArrayOfString	String[]
    //   5	48	4	localStringBuilder	StringBuilder
    //   9	5	5	str	String
    //   78	22	6	i	int
    // Exception table:
    //   from	to	target	type
    //   2	5	102	finally
    //   13	18	102	finally
    //   20	25	102	finally
    //   30	35	102	finally
    //   37	42	102	finally
    //   47	52	102	finally
    //   52	57	102	finally
    //   58	62	102	finally
    //   65	69	102	finally
    //   70	77	102	finally
    //   83	89	102	finally
    //   91	97	102	finally
    //   107	112	102	finally
    //   2	5	106	java/lang/Exception
    //   13	18	106	java/lang/Exception
    //   20	25	106	java/lang/Exception
    //   30	35	106	java/lang/Exception
    //   37	42	106	java/lang/Exception
    //   47	52	106	java/lang/Exception
    //   52	57	106	java/lang/Exception
    //   58	62	106	java/lang/Exception
    //   65	69	106	java/lang/Exception
    //   70	77	106	java/lang/Exception
    //   83	89	106	java/lang/Exception
    //   91	97	106	java/lang/Exception
  }
  
  /* Error */
  public final void b()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: getstatic 25	com/inmobi/commons/core/d/b:d	Ljava/lang/Object;
    //   5: astore_1
    //   6: aload_1
    //   7: monitorenter
    //   8: getstatic 27	com/inmobi/commons/core/d/b:e	I
    //   11: iconst_m1
    //   12: iadd
    //   13: istore_2
    //   14: iload_2
    //   15: putstatic 27	com/inmobi/commons/core/d/b:e	I
    //   18: iload_2
    //   19: ifne +20 -> 39
    //   22: aload_0
    //   23: getfield 43	com/inmobi/commons/core/d/b:f	Landroid/database/sqlite/SQLiteDatabase;
    //   26: astore_3
    //   27: aload_3
    //   28: invokevirtual 142	android/database/sqlite/SQLiteDatabase:close	()V
    //   31: iconst_0
    //   32: istore_2
    //   33: aconst_null
    //   34: astore_3
    //   35: aconst_null
    //   36: putstatic 45	com/inmobi/commons/core/d/b:b	Lcom/inmobi/commons/core/d/b;
    //   39: aload_1
    //   40: monitorexit
    //   41: aload_0
    //   42: monitorexit
    //   43: return
    //   44: astore_3
    //   45: aload_1
    //   46: monitorexit
    //   47: aload_3
    //   48: athrow
    //   49: astore_1
    //   50: goto +12 -> 62
    //   53: astore_1
    //   54: aload_1
    //   55: invokevirtual 87	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   58: pop
    //   59: aload_0
    //   60: monitorexit
    //   61: return
    //   62: aload_0
    //   63: monitorexit
    //   64: aload_1
    //   65: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	66	0	this	b
    //   49	1	1	localObject2	Object
    //   53	12	1	localException	Exception
    //   13	20	2	i	int
    //   26	9	3	localSQLiteDatabase	SQLiteDatabase
    //   44	4	3	localObject3	Object
    // Exception table:
    //   from	to	target	type
    //   8	11	44	finally
    //   14	18	44	finally
    //   22	26	44	finally
    //   27	31	44	finally
    //   35	39	44	finally
    //   39	41	44	finally
    //   45	47	44	finally
    //   2	5	49	finally
    //   6	8	49	finally
    //   47	49	49	finally
    //   54	59	49	finally
    //   2	5	53	java/lang/Exception
    //   6	8	53	java/lang/Exception
    //   47	49	53	java/lang/Exception
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.d.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */