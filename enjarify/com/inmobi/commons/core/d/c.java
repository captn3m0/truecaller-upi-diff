package com.inmobi.commons.core.d;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.inmobi.commons.a.a;
import java.util.HashMap;

public final class c
{
  private static HashMap b;
  private static final Object c;
  public SharedPreferences a;
  
  static
  {
    Object localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    b = (HashMap)localObject;
    localObject = new java/lang/Object;
    localObject.<init>();
    c = localObject;
  }
  
  private c(Context paramContext, String paramString)
  {
    paramContext = paramContext.getSharedPreferences(paramString, 0);
    a = paramContext;
  }
  
  public static c a(Context paramContext, String paramString)
  {
    paramString = a(paramString);
    ??? = (c)b.get(paramString);
    if (??? != null) {
      return (c)???;
    }
    synchronized (c)
    {
      Object localObject2 = b;
      localObject2 = ((HashMap)localObject2).get(paramString);
      localObject2 = (c)localObject2;
      if (localObject2 != null) {
        return (c)localObject2;
      }
      localObject2 = new com/inmobi/commons/core/d/c;
      ((c)localObject2).<init>(paramContext, paramString);
      paramContext = b;
      paramContext.put(paramString, localObject2);
      return (c)localObject2;
    }
  }
  
  public static String a(String paramString)
  {
    paramString = String.valueOf(paramString);
    return "com.im.keyValueStore.".concat(paramString);
  }
  
  public static c b(String paramString)
  {
    return a(a.b(), paramString);
  }
  
  public final void a(String paramString, int paramInt)
  {
    SharedPreferences.Editor localEditor = a.edit();
    localEditor.putInt(paramString, paramInt);
    localEditor.apply();
  }
  
  public final void a(String paramString, long paramLong)
  {
    SharedPreferences.Editor localEditor = a.edit();
    localEditor.putLong(paramString, paramLong);
    localEditor.apply();
  }
  
  public final void a(String paramString1, String paramString2)
  {
    SharedPreferences.Editor localEditor = a.edit();
    localEditor.putString(paramString1, paramString2);
    localEditor.apply();
  }
  
  public final void a(String paramString, boolean paramBoolean)
  {
    SharedPreferences.Editor localEditor = a.edit();
    localEditor.putBoolean(paramString, paramBoolean);
    localEditor.apply();
  }
  
  public final long b(String paramString, long paramLong)
  {
    return a.getLong(paramString, paramLong);
  }
  
  public final boolean b(String paramString, boolean paramBoolean)
  {
    return a.getBoolean(paramString, paramBoolean);
  }
  
  public final String c(String paramString)
  {
    return a.getString(paramString, null);
  }
  
  public final int d(String paramString)
  {
    return a.getInt(paramString, -1 << -1);
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.d.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */