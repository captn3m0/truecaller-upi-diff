package com.inmobi.commons.core.configs;

import android.os.SystemClock;
import com.inmobi.commons.core.network.d;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

class e
  implements Runnable
{
  private static final String a = "com.inmobi.commons.core.configs.e";
  private f b;
  private e.a c;
  private final f d;
  
  e(e.a parama, f paramf1, f paramf2)
  {
    c = parama;
    b = paramf1;
    d = paramf2;
  }
  
  private static ConfigNetworkResponse a(f paramf)
  {
    Object localObject = new com/inmobi/commons/core/network/e;
    ((com.inmobi.commons.core.network.e)localObject).<init>(paramf);
    long l1 = SystemClock.elapsedRealtime();
    localObject = ((com.inmobi.commons.core.network.e)localObject).a();
    ConfigNetworkResponse localConfigNetworkResponse = new com/inmobi/commons/core/configs/ConfigNetworkResponse;
    paramf = c;
    long l2 = SystemClock.elapsedRealtime() - l1;
    localConfigNetworkResponse.<init>(paramf, (d)localObject, l2);
    return localConfigNetworkResponse;
  }
  
  private void a(f paramf, Map paramMap)
  {
    paramMap = paramMap.entrySet().iterator();
    for (;;)
    {
      boolean bool1 = paramMap.hasNext();
      if (!bool1) {
        break;
      }
      Object localObject1 = (Map.Entry)paramMap.next();
      Object localObject2 = (ConfigNetworkResponse.ConfigResponse)((Map.Entry)localObject1).getValue();
      localObject1 = (String)((Map.Entry)localObject1).getKey();
      boolean bool2 = ((ConfigNetworkResponse.ConfigResponse)localObject2).a();
      if (!bool2)
      {
        e.a locala = c;
        locala.a((ConfigNetworkResponse.ConfigResponse)localObject2);
        localObject2 = c;
        ((Map)localObject2).remove(localObject1);
      }
    }
  }
  
  private boolean a(f paramf, int paramInt, Map paramMap)
  {
    int i = a;
    if (paramInt > i)
    {
      paramf = c.entrySet().iterator();
      for (;;)
      {
        paramInt = paramf.hasNext();
        if (paramInt == 0) {
          break;
        }
        Object localObject = (String)((Map.Entry)paramf.next()).getKey();
        boolean bool = paramMap.containsKey(localObject);
        if (bool)
        {
          e.a locala = c;
          localObject = (ConfigNetworkResponse.ConfigResponse)paramMap.get(localObject);
          locala.a((ConfigNetworkResponse.ConfigResponse)localObject);
        }
      }
      return true;
    }
    Thread.sleep(b * 1000);
    return false;
  }
  
  public void run()
  {
    int i = 0;
    e.a locala = null;
    int j = 0;
    Object localObject1 = null;
    try
    {
      boolean bool2;
      label83:
      do
      {
        Object localObject2 = b;
        int m = a;
        if (j > m) {
          break;
        }
        localObject2 = b;
        localObject2 = a((f)localObject2);
        Map localMap = a;
        bool2 = ((ConfigNetworkResponse)localObject2).a();
        if (bool2)
        {
          localObject2 = d;
          if (localObject2 != null)
          {
            bool2 = true;
            break label83;
          }
        }
        bool2 = false;
        localObject2 = null;
        boolean bool1;
        if (bool2)
        {
          do
          {
            localObject1 = d;
            j = a;
            if (i > j) {
              break;
            }
            localObject1 = d;
            localObject1 = a((f)localObject1);
            localObject2 = a;
            bool1 = ((ConfigNetworkResponse)localObject1).a();
            if (bool1) {
              break;
            }
            localObject1 = d;
            a((f)localObject1, (Map)localObject2);
            localObject1 = d;
            localObject1 = c;
            bool1 = ((Map)localObject1).isEmpty();
            if (bool1) {
              break;
            }
            i += 1;
            localObject1 = d;
            bool1 = a((f)localObject1, i, (Map)localObject2);
          } while (!bool1);
          locala = c;
          locala.a();
          return;
        }
        localObject2 = b;
        a((f)localObject2, localMap);
        localObject2 = b;
        localObject2 = c;
        bool2 = ((Map)localObject2).isEmpty();
        if (bool2) {
          break;
        }
        int k;
        bool1 += true;
        localObject2 = b;
        bool2 = a((f)localObject2, k, localMap);
      } while (!bool2);
      locala = c;
      locala.a();
      return;
    }
    catch (InterruptedException localInterruptedException) {}
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.configs.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */