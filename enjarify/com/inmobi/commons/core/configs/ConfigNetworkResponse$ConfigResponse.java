package com.inmobi.commons.core.configs;

import org.json.JSONException;
import org.json.JSONObject;

public final class ConfigNetworkResponse$ConfigResponse
{
  ConfigNetworkResponse.ConfigResponse.ConfigResponseStatus a;
  a b;
  d c;
  
  public ConfigNetworkResponse$ConfigResponse(JSONObject paramJSONObject, a parama)
  {
    b = parama;
    if (paramJSONObject != null)
    {
      int i = 2;
      Object localObject1 = "status";
      try
      {
        int j = paramJSONObject.getInt((String)localObject1);
        localObject1 = ConfigNetworkResponse.ConfigResponse.ConfigResponseStatus.fromValue(j);
        a = ((ConfigNetworkResponse.ConfigResponse.ConfigResponseStatus)localObject1);
        localObject1 = a;
        Object localObject2 = ConfigNetworkResponse.ConfigResponse.ConfigResponseStatus.SUCCESS;
        if (localObject1 == localObject2)
        {
          localObject1 = "content";
          paramJSONObject = paramJSONObject.getJSONObject((String)localObject1);
          localObject1 = b;
          ((a)localObject1).a(paramJSONObject);
          paramJSONObject = b;
          boolean bool = paramJSONObject.c();
          if (!bool)
          {
            paramJSONObject = new com/inmobi/commons/core/configs/d;
            localObject1 = "The received config has failed validation.";
            paramJSONObject.<init>(i, (String)localObject1);
            c = paramJSONObject;
            ConfigNetworkResponse.b();
            paramJSONObject = b;
            paramJSONObject.a();
          }
        }
        else
        {
          paramJSONObject = a;
          localObject1 = ConfigNetworkResponse.ConfigResponse.ConfigResponseStatus.NOT_MODIFIED;
          if (paramJSONObject == localObject1)
          {
            ConfigNetworkResponse.b();
            paramJSONObject = b;
            paramJSONObject.a();
            return;
          }
          paramJSONObject = new com/inmobi/commons/core/configs/d;
          j = 1;
          localObject2 = a;
          localObject2 = ((ConfigNetworkResponse.ConfigResponse.ConfigResponseStatus)localObject2).toString();
          paramJSONObject.<init>(j, (String)localObject2);
          c = paramJSONObject;
          ConfigNetworkResponse.b();
          paramJSONObject = b;
          paramJSONObject.a();
        }
        return;
      }
      catch (JSONException paramJSONObject)
      {
        localObject1 = new com/inmobi/commons/core/configs/d;
        paramJSONObject = paramJSONObject.getLocalizedMessage();
        ((d)localObject1).<init>(i, paramJSONObject);
        c = ((d)localObject1);
        ConfigNetworkResponse.b();
        paramJSONObject = b;
        paramJSONObject.a();
      }
    }
  }
  
  public final boolean a()
  {
    d locald = c;
    return locald != null;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.configs.ConfigNetworkResponse.ConfigResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */