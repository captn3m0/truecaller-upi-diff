package com.inmobi.commons.core.configs;

public enum ConfigNetworkResponse$ConfigResponse$ConfigResponseStatus
{
  private int a;
  
  static
  {
    Object localObject = new com/inmobi/commons/core/configs/ConfigNetworkResponse$ConfigResponse$ConfigResponseStatus;
    ((ConfigResponseStatus)localObject).<init>("SUCCESS", 0, 200);
    SUCCESS = (ConfigResponseStatus)localObject;
    localObject = new com/inmobi/commons/core/configs/ConfigNetworkResponse$ConfigResponse$ConfigResponseStatus;
    int i = 1;
    ((ConfigResponseStatus)localObject).<init>("NOT_MODIFIED", i, 304);
    NOT_MODIFIED = (ConfigResponseStatus)localObject;
    localObject = new com/inmobi/commons/core/configs/ConfigNetworkResponse$ConfigResponse$ConfigResponseStatus;
    int j = 2;
    ((ConfigResponseStatus)localObject).<init>("PRODUCT_NOT_FOUND", j, 404);
    PRODUCT_NOT_FOUND = (ConfigResponseStatus)localObject;
    localObject = new com/inmobi/commons/core/configs/ConfigNetworkResponse$ConfigResponse$ConfigResponseStatus;
    int k = 3;
    ((ConfigResponseStatus)localObject).<init>("INTERNAL_ERROR", k, 500);
    INTERNAL_ERROR = (ConfigResponseStatus)localObject;
    localObject = new com/inmobi/commons/core/configs/ConfigNetworkResponse$ConfigResponse$ConfigResponseStatus;
    int m = 4;
    ((ConfigResponseStatus)localObject).<init>("UNKNOWN", m, -1);
    UNKNOWN = (ConfigResponseStatus)localObject;
    localObject = new ConfigResponseStatus[5];
    ConfigResponseStatus localConfigResponseStatus = SUCCESS;
    localObject[0] = localConfigResponseStatus;
    localConfigResponseStatus = NOT_MODIFIED;
    localObject[i] = localConfigResponseStatus;
    localConfigResponseStatus = PRODUCT_NOT_FOUND;
    localObject[j] = localConfigResponseStatus;
    localConfigResponseStatus = INTERNAL_ERROR;
    localObject[k] = localConfigResponseStatus;
    localConfigResponseStatus = UNKNOWN;
    localObject[m] = localConfigResponseStatus;
    $VALUES = (ConfigResponseStatus[])localObject;
  }
  
  private ConfigNetworkResponse$ConfigResponse$ConfigResponseStatus(int paramInt1)
  {
    a = paramInt1;
  }
  
  public static ConfigResponseStatus fromValue(int paramInt)
  {
    ConfigResponseStatus[] arrayOfConfigResponseStatus = values();
    int i = arrayOfConfigResponseStatus.length;
    int j = 0;
    while (j < i)
    {
      ConfigResponseStatus localConfigResponseStatus = arrayOfConfigResponseStatus[j];
      int k = a;
      if (k == paramInt) {
        return localConfigResponseStatus;
      }
      j += 1;
    }
    return UNKNOWN;
  }
  
  public final int getValue()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.configs.ConfigNetworkResponse.ConfigResponse.ConfigResponseStatus
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */