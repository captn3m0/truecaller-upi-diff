package com.inmobi.commons.core.configs;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.inmobi.commons.core.utilities.uid.d;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.json.JSONException;
import org.json.JSONObject;

final class b$b
  extends Handler
  implements e.a
{
  private List a;
  private Map b;
  private Map c;
  private ExecutorService d;
  
  b$b(Looper paramLooper)
  {
    super(paramLooper);
    paramLooper = new java/util/ArrayList;
    paramLooper.<init>();
    a = paramLooper;
    paramLooper = new java/util/HashMap;
    paramLooper.<init>();
    b = paramLooper;
    paramLooper = new java/util/HashMap;
    paramLooper.<init>();
    c = paramLooper;
  }
  
  private void a(List paramList)
  {
    int i = 0;
    for (;;)
    {
      int j = paramList.size();
      if (i >= j) {
        break;
      }
      a locala = (a)paramList.get(i);
      Object localObject1 = b;
      Object localObject2 = b.f();
      Object localObject3 = locala.a();
      localObject2 = ((h)localObject2).b((String)localObject3);
      localObject1 = (HashMap)((Map)localObject1).get(localObject2);
      if (localObject1 == null)
      {
        localObject1 = new java/util/HashMap;
        ((HashMap)localObject1).<init>();
        localObject2 = b;
        localObject3 = b.f();
        String str = locala.a();
        localObject3 = ((h)localObject3).b(str);
        ((Map)localObject2).put(localObject3, localObject1);
      }
      localObject2 = locala.a();
      ((HashMap)localObject1).put(localObject2, locala);
      i += 1;
    }
  }
  
  private boolean a(String paramString)
  {
    Object localObject1 = b;
    String str = b.f().b(paramString);
    localObject1 = ((Map)localObject1).get(str);
    if (localObject1 != null)
    {
      localObject1 = b;
      localObject2 = b.f().b(paramString);
      localObject1 = (Map)((Map)localObject1).get(localObject2);
      bool1 = ((Map)localObject1).containsKey(paramString);
      if (bool1)
      {
        bool1 = true;
        break label76;
      }
    }
    boolean bool1 = false;
    localObject1 = null;
    label76:
    Object localObject2 = c;
    if (localObject2 != null)
    {
      boolean bool2 = ((Map)localObject2).containsKey(paramString);
      if (bool2) {
        bool1 = true;
      }
    }
    return bool1;
  }
  
  public final void a()
  {
    sendEmptyMessage(4);
  }
  
  public final void a(ConfigNetworkResponse.ConfigResponse paramConfigResponse)
  {
    Object localObject1 = new com/inmobi/commons/core/configs/c;
    ((c)localObject1).<init>();
    boolean bool = paramConfigResponse.a();
    if (!bool)
    {
      Object localObject2 = a;
      Object localObject3 = ConfigNetworkResponse.ConfigResponse.ConfigResponseStatus.NOT_MODIFIED;
      if (localObject2 == localObject3)
      {
        b.e();
        b.a();
        paramConfigResponse = b.a();
        long l1 = System.currentTimeMillis();
        ((c)localObject1).a(paramConfigResponse, l1);
        return;
      }
      localObject2 = b;
      for (;;)
      {
        try
        {
          localObject3 = a;
          Object localObject4 = new java/lang/StringBuilder;
          ((StringBuilder)localObject4).<init>();
          Object localObject5 = ((a)localObject2).a();
          ((StringBuilder)localObject4).append((String)localObject5);
          localObject5 = "_config";
          ((StringBuilder)localObject4).append((String)localObject5);
          localObject4 = ((StringBuilder)localObject4).toString();
          localObject5 = ((a)localObject2).b();
          localObject5 = ((JSONObject)localObject5).toString();
          ((com.inmobi.commons.core.d.c)localObject3).a((String)localObject4, (String)localObject5);
          localObject2 = ((a)localObject2).a();
          long l2 = System.currentTimeMillis();
          ((c)localObject1).a((String)localObject2, l2);
        }
        catch (JSONException localJSONException1)
        {
          a locala;
          continue;
        }
        try
        {
          localObject1 = new java/util/HashMap;
          ((HashMap)localObject1).<init>();
          localObject2 = "configName";
          localObject3 = b;
          localObject3 = ((a)localObject3).a();
          ((Map)localObject1).put(localObject2, localObject3);
          localObject2 = "latency";
          localObject3 = "2147483647";
          ((Map)localObject1).put(localObject2, localObject3);
          com.inmobi.commons.core.e.b.a();
          localObject2 = "root";
          localObject3 = "ConfigFetched";
          com.inmobi.commons.core.e.b.a((String)localObject2, (String)localObject3, (Map)localObject1);
        }
        catch (Exception localException)
        {
          b.e();
          localException.getMessage();
        }
      }
    }
    try
    {
      b.e();
      locala = b;
      locala.a();
      b.e();
      locala = b;
      locala.b();
      paramConfigResponse = b;
      b.a(paramConfigResponse);
      return;
    }
    catch (JSONException localJSONException2)
    {
      for (;;) {}
    }
    b.e();
    b.a();
  }
  
  public final void handleMessage(Message paramMessage)
  {
    b localb = this;
    Object localObject1 = paramMessage;
    int i = what;
    f localf1 = null;
    int j = 1;
    int k = 3;
    boolean bool2;
    Object localObject2;
    label302:
    int m;
    switch (i)
    {
    default: 
      break;
    case 5: 
      localObject1 = d;
      if (localObject1 != null)
      {
        bool2 = ((ExecutorService)localObject1).isShutdown();
        if (!bool2)
        {
          c = null;
          b.clear();
          removeMessages(k);
          localObject1 = d;
          ((ExecutorService)localObject1).shutdownNow();
        }
      }
      break;
    case 4: 
      localObject1 = b;
      bool2 = ((Map)localObject1).isEmpty();
      if (!bool2)
      {
        localObject1 = (Map.Entry)b.entrySet().iterator().next();
        localObject2 = (Map)((Map.Entry)localObject1).getValue();
        c = ((Map)localObject2);
        localObject2 = b;
        Object localObject3 = ((Map.Entry)localObject1).getKey();
        ((Map)localObject2).remove(localObject3);
        localObject1 = ((Map.Entry)localObject1).getKey();
        Object localObject4 = localObject1;
        localObject4 = (String)localObject1;
        localObject1 = c;
        int i1 = fb;
        int i2 = fa;
        localObject2 = new com/inmobi/commons/core/utilities/uid/d;
        localObject3 = fs.a;
        ((d)localObject2).<init>((Map)localObject3);
        k = com.inmobi.commons.core.utilities.b.e.d();
        if (k == 0)
        {
          localObject5 = "root";
          boolean bool3 = ((Map)localObject1).containsKey(localObject5);
          if (bool3)
          {
            localObject1 = b.a((Map)localObject1);
            i3 = 1;
            break label302;
          }
        }
        int i3 = k;
        f localf2 = new com/inmobi/commons/core/configs/f;
        localObject3 = localf2;
        Object localObject5 = localObject1;
        localf2.<init>((Map)localObject1, (d)localObject2, (String)localObject4, i2, i1, i3);
        localObject3 = "root";
        m = ((Map)localObject1).containsKey(localObject3);
        if (m != 0)
        {
          String str = b.f().e();
          localf1 = new com/inmobi/commons/core/configs/f;
          Map localMap = b.a((Map)localObject1);
          boolean bool4 = true;
          localf1.<init>(localMap, (d)localObject2, str, i2, i1, bool4, i3);
        }
        localObject1 = new com/inmobi/commons/core/configs/e;
        ((e)localObject1).<init>(localb, localf2, localf1);
        d.execute((Runnable)localObject1);
        return;
      }
      b.e();
      sendEmptyMessage(5);
      return;
    case 3: 
      localObject1 = a;
      a((List)localObject1);
      a.clear();
      localObject1 = d;
      if (localObject1 != null)
      {
        bool2 = ((ExecutorService)localObject1).isShutdown();
        if (!bool2) {
          break;
        }
      }
      else
      {
        localObject1 = Executors.newFixedThreadPool(j);
        d = ((ExecutorService)localObject1);
        localb.sendEmptyMessage(4);
        return;
      }
      break;
    case 2: 
      long l = fc * 1000;
      sendEmptyMessageDelayed(m, l);
      return;
    case 1: 
      localObject1 = (a)obj;
      b.e();
      ((a)localObject1).a();
      localObject2 = ((a)localObject1).a();
      a((String)localObject2);
      localObject2 = ((a)localObject1).a();
      boolean bool1 = a((String)localObject2);
      if (!bool1)
      {
        localObject2 = a;
        ((List)localObject2).add(localObject1);
        int n = 2;
        bool1 = hasMessages(n);
        if (!bool1) {
          sendEmptyMessage(n);
        }
      }
      else
      {
        b.e();
        ((a)localObject1).a();
        return;
      }
      break;
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.configs.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */