package com.inmobi.commons.core.configs;

import java.util.HashMap;
import org.json.JSONObject;

public abstract class a
{
  public a.a s;
  
  public a()
  {
    a.a locala = new com/inmobi/commons/core/configs/a$a;
    locala.<init>();
    s = locala;
  }
  
  public abstract String a();
  
  public void a(JSONObject paramJSONObject)
  {
    paramJSONObject = paramJSONObject.getJSONObject("includeIds");
    int i = 0;
    for (;;)
    {
      int j = paramJSONObject.length();
      if (i >= j) {
        break;
      }
      HashMap localHashMap = s.a;
      Boolean localBoolean = Boolean.valueOf(paramJSONObject.getBoolean("O1"));
      localHashMap.put("O1", localBoolean);
      localHashMap = s.a;
      localBoolean = Boolean.valueOf(paramJSONObject.getBoolean("UM5"));
      localHashMap.put("UM5", localBoolean);
      localHashMap = s.a;
      localBoolean = Boolean.valueOf(paramJSONObject.getBoolean("GPID"));
      localHashMap.put("GPID", localBoolean);
      localHashMap = s.a;
      localBoolean = Boolean.valueOf(paramJSONObject.optBoolean("SHA1_IMEI", false));
      localHashMap.put("SHA1_IMEI", localBoolean);
      localHashMap = s.a;
      String str = "MD5_IMEI";
      boolean bool = paramJSONObject.optBoolean("MD5_IMEI", false);
      localBoolean = Boolean.valueOf(bool);
      localHashMap.put(str, localBoolean);
      i += 1;
    }
  }
  
  public JSONObject b()
  {
    JSONObject localJSONObject1 = new org/json/JSONObject;
    localJSONObject1.<init>();
    JSONObject localJSONObject2 = new org/json/JSONObject;
    localJSONObject2.<init>();
    Object localObject = s.a.get("O1");
    localJSONObject2.put("O1", localObject);
    localObject = s.a.get("UM5");
    localJSONObject2.put("UM5", localObject);
    localObject = s.a.get("GPID");
    localJSONObject2.put("GPID", localObject);
    localObject = s.a.get("SHA1_IMEI");
    localJSONObject2.put("SHA1_IMEI", localObject);
    localObject = s.a.get("MD5_IMEI");
    localJSONObject2.put("MD5_IMEI", localObject);
    localJSONObject1.put("includeIds", localJSONObject2);
    return localJSONObject1;
  }
  
  public abstract boolean c();
  
  public abstract a d();
  
  public boolean equals(Object paramObject)
  {
    boolean bool1 = false;
    if (paramObject != null)
    {
      Object localObject = paramObject.getClass();
      Class localClass = getClass();
      if (localObject == localClass)
      {
        localObject = a();
        paramObject = ((a)paramObject).a();
        boolean bool2 = ((String)localObject).equals(paramObject);
        if (bool2) {
          bool1 = true;
        }
      }
    }
    return bool1;
  }
  
  public int hashCode()
  {
    return a().hashCode();
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.configs.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */