package com.inmobi.commons.core.configs;

import com.inmobi.commons.core.e.b;
import com.inmobi.commons.core.network.NetworkError;
import com.inmobi.commons.core.network.NetworkError.ErrorCode;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

final class ConfigNetworkResponse
{
  private static final String b = "com.inmobi.commons.core.configs.ConfigNetworkResponse";
  Map a;
  private Map c;
  private com.inmobi.commons.core.network.d d;
  private d e;
  private long f;
  
  ConfigNetworkResponse(Map paramMap, com.inmobi.commons.core.network.d paramd, long paramLong)
  {
    c = paramMap;
    d = paramd;
    paramMap = new java/util/HashMap;
    paramMap.<init>();
    a = paramMap;
    f = paramLong;
    c();
  }
  
  private static String a(Map paramMap)
  {
    String str1 = "";
    paramMap = paramMap.keySet().iterator();
    for (;;)
    {
      boolean bool = paramMap.hasNext();
      if (!bool) {
        break;
      }
      String str2 = (String)paramMap.next();
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localStringBuilder.append(str1);
      localStringBuilder.append(str2);
      localStringBuilder.append(",");
      str1 = localStringBuilder.toString();
    }
    int i = str1.length() + -1;
    return str1.substring(0, i);
  }
  
  private void c()
  {
    Object localObject1 = d;
    boolean bool1 = ((com.inmobi.commons.core.network.d)localObject1).a();
    Object localObject4;
    Object localObject5;
    Object localObject6;
    Object localObject7;
    int i;
    long l;
    if (!bool1) {
      try
      {
        localObject1 = new org/json/JSONObject;
        localObject3 = d;
        localObject3 = ((com.inmobi.commons.core.network.d)localObject3).b();
        ((JSONObject)localObject1).<init>((String)localObject3);
        localObject3 = ((JSONObject)localObject1).keys();
        for (;;)
        {
          boolean bool2 = ((Iterator)localObject3).hasNext();
          if (!bool2) {
            break;
          }
          localObject4 = ((Iterator)localObject3).next();
          localObject4 = (String)localObject4;
          localObject5 = ((JSONObject)localObject1).getJSONObject((String)localObject4);
          localObject6 = c;
          localObject6 = ((Map)localObject6).get(localObject4);
          if (localObject6 != null)
          {
            localObject6 = new com/inmobi/commons/core/configs/ConfigNetworkResponse$ConfigResponse;
            localObject7 = c;
            localObject7 = ((Map)localObject7).get(localObject4);
            localObject7 = (a)localObject7;
            ((ConfigNetworkResponse.ConfigResponse)localObject6).<init>((JSONObject)localObject5, (a)localObject7);
            localObject5 = a;
            ((Map)localObject5).put(localObject4, localObject6);
          }
        }
        return;
      }
      catch (JSONException localJSONException)
      {
        localObject3 = new com/inmobi/commons/core/configs/d;
        i = 2;
        localObject5 = localJSONException.getLocalizedMessage();
        ((d)localObject3).<init>(i, (String)localObject5);
        e = ((d)localObject3);
        try
        {
          localObject3 = new java/util/HashMap;
          ((HashMap)localObject3).<init>();
          localObject4 = "name";
          localObject5 = c;
          localObject5 = a((Map)localObject5);
          ((Map)localObject3).put(localObject4, localObject5);
          localObject4 = "errorCode";
          localObject5 = "ParsingError";
          ((Map)localObject3).put(localObject4, localObject5);
          localObject4 = "reason";
          localObject2 = localJSONException.getLocalizedMessage();
          ((Map)localObject3).put(localObject4, localObject2);
          localObject2 = "latency";
          l = f;
          localObject4 = Long.valueOf(l);
          ((Map)localObject3).put(localObject2, localObject4);
          b.a();
          localObject2 = "root";
          localObject4 = "InvalidConfig";
          b.a((String)localObject2, (String)localObject4, (Map)localObject3);
          return;
        }
        catch (Exception localException1)
        {
          localException1.getMessage();
          return;
        }
      }
    }
    Object localObject2 = c.entrySet().iterator();
    for (;;)
    {
      boolean bool3 = ((Iterator)localObject2).hasNext();
      i = 0;
      localObject4 = null;
      if (!bool3) {
        break;
      }
      localObject3 = (Map.Entry)((Iterator)localObject2).next();
      localObject5 = new com/inmobi/commons/core/configs/ConfigNetworkResponse$ConfigResponse;
      localObject7 = (a)((Map.Entry)localObject3).getValue();
      ((ConfigNetworkResponse.ConfigResponse)localObject5).<init>(null, (a)localObject7);
      localObject6 = new com/inmobi/commons/core/configs/d;
      localObject7 = "Network error in fetching config.";
      ((d)localObject6).<init>(0, (String)localObject7);
      c = ((d)localObject6);
      localObject4 = a;
      localObject3 = ((Map.Entry)localObject3).getKey();
      ((Map)localObject4).put(localObject3, localObject5);
    }
    localObject2 = new com/inmobi/commons/core/configs/d;
    Object localObject3 = d.b.b;
    ((d)localObject2).<init>(0, (String)localObject3);
    e = ((d)localObject2);
    try
    {
      localObject2 = new java/util/HashMap;
      ((HashMap)localObject2).<init>();
      localObject3 = "name";
      localObject4 = c;
      localObject4 = a((Map)localObject4);
      ((Map)localObject2).put(localObject3, localObject4);
      localObject3 = "errorCode";
      localObject4 = d;
      localObject4 = b;
      localObject4 = a;
      i = ((NetworkError.ErrorCode)localObject4).getValue();
      localObject4 = String.valueOf(i);
      ((Map)localObject2).put(localObject3, localObject4);
      localObject3 = "reason";
      localObject4 = d;
      localObject4 = b;
      localObject4 = b;
      ((Map)localObject2).put(localObject3, localObject4);
      localObject3 = "latency";
      l = f;
      localObject4 = Long.valueOf(l);
      ((Map)localObject2).put(localObject3, localObject4);
      b.a();
      localObject3 = "root";
      localObject4 = "InvalidConfig";
      b.a((String)localObject3, (String)localObject4, (Map)localObject2);
      return;
    }
    catch (Exception localException2)
    {
      localException2.getMessage();
    }
  }
  
  public final boolean a()
  {
    Object localObject = d;
    if (localObject != null)
    {
      localObject = b;
      if (localObject != null)
      {
        localObject = d.b.a;
        NetworkError.ErrorCode localErrorCode = NetworkError.ErrorCode.BAD_REQUEST;
        boolean bool = true;
        if (localObject != localErrorCode)
        {
          localObject = d.b.a;
          int i = ((NetworkError.ErrorCode)localObject).getValue();
          int j = 500;
          if (j <= i)
          {
            j = 600;
            if (i < j)
            {
              i = 1;
              break label92;
            }
          }
          i = 0;
          localObject = null;
          label92:
          if (i == 0) {}
        }
        else
        {
          return bool;
        }
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.configs.ConfigNetworkResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */