package com.inmobi.commons.core.configs;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public final class h
  extends a
{
  private static final Object i;
  int a;
  int b;
  int c;
  public int d;
  h.b e;
  JSONObject f;
  public boolean g;
  private List h;
  
  static
  {
    Object localObject = new java/lang/Object;
    localObject.<init>();
    i = localObject;
  }
  
  public h()
  {
    int j = 3;
    a = j;
    b = 60;
    c = j;
    d = -1;
    g = false;
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    h = ((List)localObject);
    localObject = new com/inmobi/commons/core/configs/h$b;
    ((h.b)localObject).<init>();
    e = ((h.b)localObject);
    localObject = new org/json/JSONObject;
    ((JSONObject)localObject).<init>();
    f = ((JSONObject)localObject);
  }
  
  private static boolean c(String paramString)
  {
    if (paramString != null)
    {
      String str = paramString.trim();
      int j = str.length();
      if (j != 0)
      {
        str = "http://";
        boolean bool1 = paramString.startsWith(str);
        if (!bool1)
        {
          str = "https://";
          boolean bool2 = paramString.startsWith(str);
          if (!bool2) {}
        }
        else
        {
          return false;
        }
      }
    }
    return true;
  }
  
  public final long a(String paramString)
  {
    Object localObject1 = i;
    int j = 0;
    try
    {
      for (;;)
      {
        Object localObject2 = h;
        int k = ((List)localObject2).size();
        if (j >= k) {
          break;
        }
        localObject2 = h;
        localObject2 = ((List)localObject2).get(j);
        localObject2 = (h.a)localObject2;
        String str = a;
        boolean bool = paramString.equals(str);
        if (bool)
        {
          l = b;
          return l;
        }
        j += 1;
      }
      long l = 86400L;
      return l;
    }
    finally {}
  }
  
  public final String a()
  {
    return "root";
  }
  
  public final void a(JSONObject paramJSONObject)
  {
    super.a(paramJSONObject);
    int j = paramJSONObject.getInt("maxRetries");
    a = j;
    j = paramJSONObject.getInt("retryInterval");
    b = j;
    j = paramJSONObject.getInt("waitTime");
    c = j;
    Object localObject1 = paramJSONObject.getJSONObject("latestSdkInfo");
    ??? = e;
    Object localObject3 = ((JSONObject)localObject1).getString("version");
    a = ((String)localObject3);
    ??? = e;
    localObject3 = "url";
    localObject1 = ((JSONObject)localObject1).getString((String)localObject3);
    b = ((String)localObject1);
    localObject1 = paramJSONObject.getJSONArray("components");
    synchronized (i)
    {
      localObject3 = h;
      ((List)localObject3).clear();
      int k = 0;
      localObject3 = null;
      for (;;)
      {
        int m = ((JSONArray)localObject1).length();
        if (k >= m) {
          break;
        }
        Object localObject4 = ((JSONArray)localObject1).getJSONObject(k);
        h.a locala = new com/inmobi/commons/core/configs/h$a;
        locala.<init>();
        String str1 = "type";
        str1 = ((JSONObject)localObject4).getString(str1);
        a = str1;
        str1 = "expiry";
        long l = ((JSONObject)localObject4).getLong(str1);
        b = l;
        str1 = "protocol";
        str1 = ((JSONObject)localObject4).getString(str1);
        c = str1;
        str1 = "url";
        str1 = ((JSONObject)localObject4).getString(str1);
        d = str1;
        str1 = "root";
        String str2 = a;
        boolean bool2 = str1.equals(str2);
        if (bool2)
        {
          str1 = "fallbackUrl";
          localObject4 = ((JSONObject)localObject4).getString(str1);
          e = ((String)localObject4);
        }
        localObject4 = h;
        ((List)localObject4).add(locala);
        k += 1;
      }
      boolean bool1 = paramJSONObject.getBoolean("monetizationDisabled");
      g = bool1;
      int n = paramJSONObject.getJSONObject("gdpr").getBoolean("transmitRequest");
      d = n;
      return;
    }
  }
  
  public final String b(String paramString)
  {
    Object localObject1 = i;
    int j = 0;
    try
    {
      for (;;)
      {
        Object localObject2 = h;
        int k = ((List)localObject2).size();
        if (j >= k) {
          break;
        }
        localObject2 = h;
        localObject2 = ((List)localObject2).get(j);
        localObject2 = (h.a)localObject2;
        String str = a;
        boolean bool = paramString.equals(str);
        if (bool)
        {
          paramString = d;
          return paramString;
        }
        j += 1;
      }
      paramString = "";
      return paramString;
    }
    finally {}
  }
  
  public final JSONObject b()
  {
    JSONObject localJSONObject1 = super.b();
    Object localObject2 = new org/json/JSONArray;
    ((JSONArray)localObject2).<init>();
    int j = a;
    localJSONObject1.put("maxRetries", j);
    j = b;
    localJSONObject1.put("retryInterval", j);
    j = c;
    localJSONObject1.put("waitTime", j);
    Object localObject3 = new org/json/JSONObject;
    ((JSONObject)localObject3).<init>();
    String str1 = e.a;
    ((JSONObject)localObject3).put("version", str1);
    str1 = e.b;
    ((JSONObject)localObject3).put("url", str1);
    localJSONObject1.put("latestSdkInfo", localObject3);
    localObject3 = i;
    j = 0;
    int k = 0;
    str1 = null;
    try
    {
      for (;;)
      {
        Object localObject4 = h;
        m = ((List)localObject4).size();
        if (k >= m) {
          break;
        }
        localObject4 = h;
        localObject4 = ((List)localObject4).get(k);
        localObject4 = (h.a)localObject4;
        JSONObject localJSONObject2 = new org/json/JSONObject;
        localJSONObject2.<init>();
        String str2 = "type";
        String str3 = a;
        localJSONObject2.put(str2, str3);
        str2 = "expiry";
        long l = b;
        localJSONObject2.put(str2, l);
        str2 = "protocol";
        str3 = c;
        localJSONObject2.put(str2, str3);
        str2 = "url";
        str3 = d;
        localJSONObject2.put(str2, str3);
        str2 = "root";
        str3 = a;
        boolean bool1 = str2.equals(str3);
        if (bool1)
        {
          str2 = "fallbackUrl";
          localObject4 = e;
          localJSONObject2.put(str2, localObject4);
        }
        ((JSONArray)localObject2).put(localJSONObject2);
        k += 1;
      }
      localJSONObject1.put("components", localObject2);
      boolean bool2 = g;
      localJSONObject1.put("monetizationDisabled", bool2);
      localObject2 = new org/json/JSONObject;
      ((JSONObject)localObject2).<init>();
      localObject3 = "transmitRequest";
      k = d;
      int m = 1;
      if (k == m) {
        j = 1;
      }
      ((JSONObject)localObject2).put((String)localObject3, j);
      localJSONObject1.put("gdpr", localObject2);
      return localJSONObject1;
    }
    finally {}
  }
  
  public final boolean c()
  {
    Object localObject1 = h;
    Object localObject2 = null;
    if (localObject1 == null) {
      return false;
    }
    int j = a;
    if (j >= 0)
    {
      j = b;
      if (j >= 0)
      {
        j = c;
        if (j >= 0)
        {
          localObject1 = e.a.trim();
          j = ((String)localObject1).length();
          if (j != 0)
          {
            localObject1 = e.b;
            String str1 = "http://";
            boolean bool1 = ((String)localObject1).startsWith(str1);
            if (!bool1)
            {
              localObject1 = e.b;
              str1 = "https://";
              bool1 = ((String)localObject1).startsWith(str1);
              if (!bool1) {}
            }
            else
            {
              localObject1 = i;
              int m = 0;
              str1 = null;
              try
              {
                for (;;)
                {
                  Object localObject4 = h;
                  int n = ((List)localObject4).size();
                  if (m >= n) {
                    break label363;
                  }
                  localObject4 = h;
                  localObject4 = ((List)localObject4).get(m);
                  localObject4 = (h.a)localObject4;
                  String str2 = a;
                  str2 = str2.trim();
                  int i1 = str2.length();
                  if (i1 == 0) {
                    return false;
                  }
                  long l1 = b;
                  long l2 = 0L;
                  boolean bool4 = l1 < l2;
                  if (bool4) {
                    break;
                  }
                  l1 = b;
                  l2 = 864000L;
                  bool4 = l1 < l2;
                  if (bool4) {
                    break;
                  }
                  str2 = c;
                  str2 = str2.trim();
                  i1 = str2.length();
                  if (i1 == 0) {
                    return false;
                  }
                  str2 = d;
                  boolean bool3 = c(str2);
                  if (bool3) {
                    return false;
                  }
                  str2 = "root";
                  String str3 = a;
                  bool3 = str2.equals(str3);
                  if (bool3)
                  {
                    localObject4 = e;
                    boolean bool2 = c((String)localObject4);
                    if (bool2) {
                      return false;
                    }
                  }
                  m += 1;
                }
                return false;
                label363:
                int k = d;
                m = -1;
                return k != m;
              }
              finally {}
            }
          }
          return false;
        }
      }
    }
    return false;
  }
  
  public final a d()
  {
    h localh = new com/inmobi/commons/core/configs/h;
    localh.<init>();
    return localh;
  }
  
  public final String e()
  {
    synchronized (i)
    {
      Object localObject2 = h;
      localObject2 = ((List)localObject2).iterator();
      Object localObject4;
      boolean bool2;
      do
      {
        boolean bool1 = ((Iterator)localObject2).hasNext();
        if (!bool1) {
          break;
        }
        localObject4 = ((Iterator)localObject2).next();
        localObject4 = (h.a)localObject4;
        String str1 = "root";
        String str2 = a;
        bool2 = str1.equals(str2);
      } while (!bool2);
      localObject2 = e;
      return (String)localObject2;
      localObject2 = "https://config.inmobi.cn/config-server/v1/config/secure.cfg";
      return (String)localObject2;
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.configs.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */