package com.inmobi.commons.core.configs;

import org.json.JSONException;
import org.json.JSONObject;

public final class c
{
  com.inmobi.commons.core.d.c a;
  
  public c()
  {
    com.inmobi.commons.core.d.c localc = com.inmobi.commons.core.d.c.b("config_store");
    a = localc;
  }
  
  public final void a(a parama)
  {
    Object localObject1 = a;
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>();
    String str = parama.a();
    ((StringBuilder)localObject2).append(str);
    str = "_config";
    ((StringBuilder)localObject2).append(str);
    localObject2 = ((StringBuilder)localObject2).toString();
    localObject1 = ((com.inmobi.commons.core.d.c)localObject1).c((String)localObject2);
    if (localObject1 == null) {
      return;
    }
    try
    {
      localObject2 = new org/json/JSONObject;
      ((JSONObject)localObject2).<init>((String)localObject1);
      parama.a((JSONObject)localObject2);
      return;
    }
    catch (JSONException localJSONException) {}
  }
  
  public final void a(String paramString, long paramLong)
  {
    com.inmobi.commons.core.d.c localc = a;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(paramString);
    localStringBuilder.append("_config_update_ts");
    paramString = localStringBuilder.toString();
    localc.a(paramString, paramLong);
  }
  
  public final boolean a(String paramString)
  {
    com.inmobi.commons.core.d.c localc = a;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(paramString);
    localStringBuilder.append("_config");
    paramString = localStringBuilder.toString();
    paramString = localc.c(paramString);
    return paramString != null;
  }
  
  public final long b(String paramString)
  {
    com.inmobi.commons.core.d.c localc = a;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(paramString);
    localStringBuilder.append("_config_update_ts");
    paramString = localStringBuilder.toString();
    return localc.b(paramString, 0L);
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.configs.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */