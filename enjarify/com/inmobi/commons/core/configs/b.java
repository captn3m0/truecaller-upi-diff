package com.inmobi.commons.core.configs;

import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.json.JSONObject;

public class b
{
  private static final String a = "b";
  private static Map b;
  private static h c;
  private static b.d d;
  private HandlerThread e;
  private b.b f;
  private boolean g = false;
  
  private b()
  {
    Object localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    b = (Map)localObject;
    localObject = new android/os/HandlerThread;
    ((HandlerThread)localObject).<init>("ConfigBootstrapHandler");
    e = ((HandlerThread)localObject);
    e.start();
    localObject = new com/inmobi/commons/core/configs/b$b;
    Looper localLooper = e.getLooper();
    ((b.b)localObject).<init>(localLooper);
    f = ((b.b)localObject);
    localObject = new com/inmobi/commons/core/configs/h;
    ((h)localObject).<init>();
    c = (h)localObject;
  }
  
  public static b a()
  {
    return b.a.a();
  }
  
  private static boolean a(long paramLong1, long paramLong2)
  {
    long l = System.currentTimeMillis() - paramLong1;
    paramLong1 = 1000L;
    paramLong2 *= paramLong1;
    boolean bool = l < paramLong2;
    return bool;
  }
  
  private static boolean a(String paramString1, String paramString2)
  {
    paramString1 = paramString1.split("\\.");
    paramString2 = paramString2.split("\\.");
    try
    {
      int i = paramString1.length;
      int j = 0;
      Object localObject1;
      int k;
      while (j < i)
      {
        localObject1 = paramString1[j];
        localObject1 = Integer.valueOf((String)localObject1);
        k = ((Integer)localObject1).intValue();
        if (k < 0) {
          return false;
        }
        j += 1;
      }
      i = paramString2.length;
      j = 0;
      while (j < i)
      {
        localObject1 = paramString2[j];
        localObject1 = Integer.valueOf((String)localObject1);
        k = ((Integer)localObject1).intValue();
        if (k < 0) {
          return false;
        }
        j += 1;
      }
      i = paramString1.length;
      j = paramString2.length;
      if (i < j) {
        i = paramString1.length;
      } else {
        i = paramString2.length;
      }
      j = 0;
      for (;;)
      {
        k = 1;
        if (j >= i) {
          break;
        }
        Object localObject2 = paramString1[j];
        Object localObject3 = paramString2[j];
        boolean bool = ((String)localObject2).equals(localObject3);
        if (!bool)
        {
          paramString1 = Integer.valueOf(paramString1[j]);
          m = paramString1.intValue();
          paramString2 = Integer.valueOf(paramString2[j]);
          n = paramString2.intValue();
          if (m < n) {
            return k;
          }
          return false;
        }
        j += 1;
      }
      int m = paramString1.length;
      int n = paramString2.length;
      if (m < n) {
        return k;
      }
      return false;
    }
    catch (NumberFormatException localNumberFormatException) {}
    return false;
  }
  
  private static void b(a parama)
  {
    ArrayList localArrayList = (ArrayList)b.get(parama);
    if (localArrayList != null)
    {
      int i = 0;
      for (;;)
      {
        int j = localArrayList.size();
        if (i >= j) {
          break;
        }
        Object localObject = localArrayList.get(i);
        if (localObject != null)
        {
          localObject = ((WeakReference)localArrayList.get(i)).get();
          if (localObject != null)
          {
            localObject = (b.c)((WeakReference)localArrayList.get(i)).get();
            ((b.c)localObject).a(parama);
          }
        }
        i += 1;
      }
    }
  }
  
  private void c(a parama)
  {
    try
    {
      c localc = new com/inmobi/commons/core/configs/c;
      localc.<init>();
      Object localObject1 = "root";
      boolean bool1 = localc.a((String)localObject1);
      if (!bool1)
      {
        parama.a();
        parama = new com/inmobi/commons/core/configs/h;
        parama.<init>();
        d(parama);
        return;
      }
      localObject1 = c;
      localc.a((a)localObject1);
      localObject1 = "root";
      long l1 = localc.b((String)localObject1);
      Object localObject2 = c;
      String str = "root";
      long l2 = ((h)localObject2).a(str);
      bool1 = a(l1, l2);
      if (bool1)
      {
        localObject1 = new com/inmobi/commons/core/configs/h;
        ((h)localObject1).<init>();
        d((a)localObject1);
      }
      localObject1 = parama.a();
      bool1 = localc.a((String)localObject1);
      if (!bool1)
      {
        parama.a();
        parama = parama.d();
        d(parama);
        return;
      }
      localc.a(parama);
      localObject1 = parama.a();
      long l3 = localc.b((String)localObject1);
      h localh = c;
      localObject2 = parama.a();
      long l4 = localh.a((String)localObject2);
      boolean bool2 = a(l3, l4);
      if (bool2)
      {
        parama.a();
        parama = parama.d();
        d(parama);
        return;
      }
      parama.a();
      return;
    }
    finally {}
  }
  
  public static void d()
  {
    String str1 = ce.a;
    String str2 = ce.b;
    Object localObject = str1.trim();
    int i = ((String)localObject).length();
    if (i != 0)
    {
      localObject = "7.1.1";
      String str3 = str1.trim();
      boolean bool = a((String)localObject, str3);
      if (bool)
      {
        localObject = Logger.InternalLogLevel.DEBUG;
        str3 = a;
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        String str4 = "A newer version (version ";
        localStringBuilder.<init>(str4);
        localStringBuilder.append(str1);
        localStringBuilder.append(") of the InMobi SDK is available! You are currently on an older version (Version 7.1.1). Please download the latest InMobi SDK from ");
        localStringBuilder.append(str2);
        str1 = localStringBuilder.toString();
        Logger.a((Logger.InternalLogLevel)localObject, str3, str1);
      }
    }
  }
  
  private void d(a parama)
  {
    Message localMessage = f.obtainMessage();
    what = 1;
    obj = parama;
    f.sendMessage(localMessage);
  }
  
  public final void a(a parama, b.c paramc)
  {
    try
    {
      boolean bool = g;
      if (!bool)
      {
        parama.a();
        return;
      }
      Object localObject = b;
      localObject = ((Map)localObject).get(parama);
      localObject = (ArrayList)localObject;
      if (localObject == null)
      {
        localObject = new java/util/ArrayList;
        ((ArrayList)localObject).<init>();
      }
      if (paramc == null)
      {
        paramc = null;
      }
      else
      {
        WeakReference localWeakReference = new java/lang/ref/WeakReference;
        localWeakReference.<init>(paramc);
        paramc = localWeakReference;
      }
      ((ArrayList)localObject).add(paramc);
      paramc = b;
      paramc.put(parama, localObject);
      c(parama);
      return;
    }
    finally {}
  }
  
  public final void b()
  {
    try
    {
      boolean bool1 = g;
      if (!bool1)
      {
        bool1 = true;
        g = bool1;
        Object localObject1 = com.inmobi.commons.core.e.b.a();
        Object localObject3 = "root";
        Object localObject4 = c;
        localObject4 = f;
        ((com.inmobi.commons.core.e.b)localObject1).a((String)localObject3, (JSONObject)localObject4);
        localObject1 = d;
        if (localObject1 == null)
        {
          localObject1 = new com/inmobi/commons/core/configs/b$d;
          ((b.d)localObject1).<init>();
          d = (b.d)localObject1;
          localObject1 = c;
          localObject3 = d;
          a((a)localObject1, (b.c)localObject3);
        }
        localObject1 = b;
        localObject1 = ((Map)localObject1).entrySet();
        localObject1 = ((Set)localObject1).iterator();
        for (;;)
        {
          boolean bool2 = ((Iterator)localObject1).hasNext();
          if (!bool2) {
            break;
          }
          localObject3 = ((Iterator)localObject1).next();
          localObject3 = (Map.Entry)localObject3;
          localObject3 = ((Map.Entry)localObject3).getKey();
          localObject3 = (a)localObject3;
          c((a)localObject3);
          b((a)localObject3);
        }
      }
      return;
    }
    finally {}
  }
  
  public final void c()
  {
    try
    {
      boolean bool = g;
      if (bool)
      {
        bool = false;
        b.b localb = null;
        g = false;
        localb = f;
        int i = 5;
        localb.sendEmptyMessage(i);
      }
      return;
    }
    finally {}
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.configs.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */