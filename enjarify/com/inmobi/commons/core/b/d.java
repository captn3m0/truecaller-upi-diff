package com.inmobi.commons.core.b;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class d
  implements com.inmobi.commons.core.c.b
{
  private static final String a = "d";
  private AtomicBoolean b;
  private AtomicBoolean c;
  private b d;
  private e e;
  private HashMap f;
  private List g;
  private ScheduledExecutorService h;
  
  public d(b paramb, e parame, a parama)
  {
    d = paramb;
    e = parame;
    paramb = new java/util/concurrent/atomic/AtomicBoolean;
    paramb.<init>(false);
    b = paramb;
    paramb = new java/util/concurrent/atomic/AtomicBoolean;
    paramb.<init>(false);
    c = paramb;
    paramb = new java/util/LinkedList;
    paramb.<init>();
    g = paramb;
    paramb = new java/util/HashMap;
    paramb.<init>(1);
    f = paramb;
    a(parama);
  }
  
  private void a(String paramString, com.inmobi.commons.core.utilities.uid.d paramd)
  {
    Object localObject1 = c;
    boolean bool1 = ((AtomicBoolean)localObject1).get();
    if (!bool1)
    {
      localObject1 = b;
      bool1 = ((AtomicBoolean)localObject1).get();
      if (!bool1)
      {
        localObject1 = d;
        Object localObject2 = b(paramString);
        long l1 = a;
        ((b)localObject1).b(l1, paramString);
        localObject1 = d;
        int i = ((b)localObject1).a(paramString);
        int j = com.inmobi.commons.core.utilities.b.b.a();
        int k = 1;
        Object localObject3;
        int m;
        if (j != k)
        {
          localObject3 = b(paramString);
          m = i;
        }
        else
        {
          localObject3 = b(paramString);
          m = g;
        }
        long l2;
        if (j != k)
        {
          localObject2 = b(paramString);
          l2 = j;
        }
        else
        {
          localObject2 = b(paramString);
          l2 = h;
        }
        if (m > i)
        {
          localObject1 = d;
          localObject2 = b(paramString);
          long l3 = c;
          boolean bool2 = ((b)localObject1).a(l3, paramString);
          if (!bool2) {}
        }
        else
        {
          localObject1 = e.a(paramString);
          if (localObject1 != null)
          {
            b.set(k);
            Object localObject4 = b(paramString);
            localObject2 = com.inmobi.commons.core.c.a.a();
            localObject3 = e;
            int n = d;
            int i1 = n + 1;
            localObject4 = localObject2;
            localObject2 = localObject3;
            k = i1;
            m = i1;
            ((com.inmobi.commons.core.c.a)localObject4).a((c)localObject1, (String)localObject3, i1, i1, l2, paramd, this);
          }
        }
        return;
      }
    }
  }
  
  private a b(String paramString)
  {
    return (a)f.get(paramString);
  }
  
  public final void a()
  {
    ScheduledExecutorService localScheduledExecutorService = h;
    if (localScheduledExecutorService != null)
    {
      localScheduledExecutorService.shutdownNow();
      localScheduledExecutorService = null;
      h = null;
    }
    b.set(false);
    c.set(true);
    g.clear();
    f.clear();
  }
  
  public final void a(a parama)
  {
    String str = b;
    if (str == null) {
      str = "default";
    }
    f.put(str, parama);
  }
  
  public final void a(c paramc)
  {
    Object localObject = d;
    int i = ((Integer)a.get(0)).intValue();
    localObject = ((b)localObject).b(i);
    b localb = d;
    paramc = a;
    localb.a(paramc);
    if (localObject != null)
    {
      paramc = d;
      long l = System.currentTimeMillis();
      paramc.c(l, (String)localObject);
      paramc = b;
      paramc.set(false);
    }
  }
  
  public final void a(c paramc, boolean paramBoolean)
  {
    Object localObject = d;
    Integer localInteger = (Integer)a.get(0);
    int i = localInteger.intValue();
    localObject = ((b)localObject).b(i);
    boolean bool = c;
    if ((bool) && (paramBoolean))
    {
      b localb = d;
      paramc = a;
      localb.a(paramc);
    }
    if (localObject != null)
    {
      paramc = d;
      long l = System.currentTimeMillis();
      paramc.c(l, (String)localObject);
      paramc = b;
      paramc.set(false);
    }
  }
  
  public final void a(String paramString)
  {
    Object localObject1 = c;
    boolean bool1 = ((AtomicBoolean)localObject1).get();
    if (!bool1)
    {
      if (paramString == null) {
        paramString = "default";
      }
      localObject1 = b(paramString);
      long l1 = f;
      long l2 = 0L;
      boolean bool2 = l1 < l2;
      if (!bool2)
      {
        a(paramString, null);
        return;
      }
      Object localObject2 = g;
      bool2 = ((List)localObject2).contains(paramString);
      if (!bool2)
      {
        g.add(paramString);
        localObject2 = h;
        if (localObject2 == null)
        {
          localObject2 = Executors.newSingleThreadScheduledExecutor();
          h = ((ScheduledExecutorService)localObject2);
        }
        localObject2 = h;
        d.1 local1 = new com/inmobi/commons/core/b/d$1;
        local1.<init>(this, paramString);
        a locala = b(paramString);
        Object localObject3 = d;
        long l3 = ((b)localObject3).b(paramString);
        long l4 = -1;
        boolean bool3 = l3 < l4;
        if (!bool3)
        {
          b localb = d;
          long l5 = System.currentTimeMillis();
          localb.c(l5, paramString);
        }
        l3 = TimeUnit.MILLISECONDS.toSeconds(l3);
        l4 = f;
        l3 += l4;
        paramString = TimeUnit.MILLISECONDS;
        l4 = System.currentTimeMillis();
        l4 = paramString.toSeconds(l4);
        l4 = l3 - l4;
        boolean bool4 = l4 < l2;
        if (bool4)
        {
          paramString = TimeUnit.MILLISECONDS;
          l2 = System.currentTimeMillis();
          l2 = paramString.toSeconds(l2);
          l3 -= l2;
        }
        else
        {
          l3 = l2;
        }
        paramString = TimeUnit.SECONDS;
        Object localObject4 = localObject2;
        localObject2 = local1;
        localObject3 = paramString;
        ((ScheduledExecutorService)localObject4).scheduleAtFixedRate(local1, l3, l1, paramString);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.commons.core.b.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */