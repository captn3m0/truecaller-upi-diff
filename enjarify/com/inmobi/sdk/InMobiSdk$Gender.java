package com.inmobi.sdk;

public enum InMobiSdk$Gender
{
  private String a;
  
  static
  {
    Object localObject = new com/inmobi/sdk/InMobiSdk$Gender;
    ((Gender)localObject).<init>("FEMALE", 0, "f");
    FEMALE = (Gender)localObject;
    localObject = new com/inmobi/sdk/InMobiSdk$Gender;
    int i = 1;
    ((Gender)localObject).<init>("MALE", i, "m");
    MALE = (Gender)localObject;
    localObject = new Gender[2];
    Gender localGender = FEMALE;
    localObject[0] = localGender;
    localGender = MALE;
    localObject[i] = localGender;
    $VALUES = (Gender[])localObject;
  }
  
  private InMobiSdk$Gender(String paramString1)
  {
    a = paramString1;
  }
  
  public final String toString()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.sdk.InMobiSdk.Gender
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */