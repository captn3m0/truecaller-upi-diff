package com.inmobi.sdk;

public enum InMobiSdk$AgeGroup
{
  private String a;
  
  static
  {
    Object localObject = new com/inmobi/sdk/InMobiSdk$AgeGroup;
    ((AgeGroup)localObject).<init>("BELOW_18", 0, "below18");
    BELOW_18 = (AgeGroup)localObject;
    localObject = new com/inmobi/sdk/InMobiSdk$AgeGroup;
    int i = 1;
    ((AgeGroup)localObject).<init>("BETWEEN_18_AND_24", i, "between18and24");
    BETWEEN_18_AND_24 = (AgeGroup)localObject;
    localObject = new com/inmobi/sdk/InMobiSdk$AgeGroup;
    int j = 2;
    ((AgeGroup)localObject).<init>("BETWEEN_25_AND_29", j, "between25and29");
    BETWEEN_25_AND_29 = (AgeGroup)localObject;
    localObject = new com/inmobi/sdk/InMobiSdk$AgeGroup;
    int k = 3;
    ((AgeGroup)localObject).<init>("BETWEEN_30_AND_34", k, "between30and34");
    BETWEEN_30_AND_34 = (AgeGroup)localObject;
    localObject = new com/inmobi/sdk/InMobiSdk$AgeGroup;
    int m = 4;
    ((AgeGroup)localObject).<init>("BETWEEN_35_AND_44", m, "between35and44");
    BETWEEN_35_AND_44 = (AgeGroup)localObject;
    localObject = new com/inmobi/sdk/InMobiSdk$AgeGroup;
    int n = 5;
    ((AgeGroup)localObject).<init>("BETWEEN_45_AND_54", n, "between45and54");
    BETWEEN_45_AND_54 = (AgeGroup)localObject;
    localObject = new com/inmobi/sdk/InMobiSdk$AgeGroup;
    int i1 = 6;
    ((AgeGroup)localObject).<init>("BETWEEN_55_AND_65", i1, "between55and65");
    BETWEEN_55_AND_65 = (AgeGroup)localObject;
    localObject = new com/inmobi/sdk/InMobiSdk$AgeGroup;
    int i2 = 7;
    ((AgeGroup)localObject).<init>("ABOVE_65", i2, "above65");
    ABOVE_65 = (AgeGroup)localObject;
    localObject = new AgeGroup[8];
    AgeGroup localAgeGroup = BELOW_18;
    localObject[0] = localAgeGroup;
    localAgeGroup = BETWEEN_18_AND_24;
    localObject[i] = localAgeGroup;
    localAgeGroup = BETWEEN_25_AND_29;
    localObject[j] = localAgeGroup;
    localAgeGroup = BETWEEN_30_AND_34;
    localObject[k] = localAgeGroup;
    localAgeGroup = BETWEEN_35_AND_44;
    localObject[m] = localAgeGroup;
    localAgeGroup = BETWEEN_45_AND_54;
    localObject[n] = localAgeGroup;
    localAgeGroup = BETWEEN_55_AND_65;
    localObject[i1] = localAgeGroup;
    localAgeGroup = ABOVE_65;
    localObject[i2] = localAgeGroup;
    $VALUES = (AgeGroup[])localObject;
  }
  
  private InMobiSdk$AgeGroup(String paramString1)
  {
    a = paramString1;
  }
  
  public final String toString()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.sdk.InMobiSdk.AgeGroup
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */