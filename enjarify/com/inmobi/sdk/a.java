package com.inmobi.sdk;

import android.content.Context;
import com.inmobi.commons.core.d.c;
import com.inmobi.commons.core.utilities.b.g;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

final class a
{
  public static boolean a(Context paramContext)
  {
    Object localObject1 = "aeskeygenerate";
    Object localObject2 = "impref";
    Object localObject3 = "IMAdTrackerStatusUpload";
    String str1 = "IMAdMMediationCache";
    String str2 = "inmobiAppAnalyticsAppId";
    String str3 = "inmobiAppAnalyticsSession";
    String str4 = "inmobisdkaid";
    String str5 = "IMAdTrackerStatusUpload";
    String str6 = "testAppPref";
    String[] tmp38_35 = new String[12];
    String[] tmp39_38 = tmp38_35;
    String[] tmp39_38 = tmp38_35;
    tmp39_38[0] = "carbpreference";
    tmp39_38[1] = "IMAdMLtvpRuleCache";
    String[] tmp48_39 = tmp39_38;
    String[] tmp48_39 = tmp39_38;
    tmp48_39[2] = "inmobiAppAnalyticsSession";
    tmp48_39[3] = localObject1;
    String[] tmp56_48 = tmp48_39;
    String[] tmp56_48 = tmp48_39;
    tmp56_48[4] = localObject2;
    tmp56_48[5] = localObject3;
    String[] tmp63_56 = tmp56_48;
    String[] tmp63_56 = tmp56_48;
    tmp63_56[6] = str1;
    tmp63_56[7] = str2;
    String[] tmp74_63 = tmp63_56;
    String[] tmp74_63 = tmp63_56;
    tmp74_63[8] = str3;
    tmp74_63[9] = str4;
    tmp74_63[10] = str5;
    String[] tmp90_74 = tmp74_63;
    tmp90_74[11] = str6;
    Object localObject4 = Arrays.asList(tmp90_74);
    int i = 0;
    String str7 = null;
    for (;;)
    {
      j = ((List)localObject4).size();
      if (i >= j) {
        break;
      }
      localObject1 = new java/io/File;
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("/data/data/");
      localObject3 = paramContext.getPackageName();
      ((StringBuilder)localObject2).append((String)localObject3);
      ((StringBuilder)localObject2).append("/shared_prefs/");
      localObject3 = (String)((List)localObject4).get(i);
      ((StringBuilder)localObject2).append((String)localObject3);
      localObject3 = ".xml";
      ((StringBuilder)localObject2).append((String)localObject3);
      localObject2 = ((StringBuilder)localObject2).toString();
      ((File)localObject1).<init>((String)localObject2);
      boolean bool1 = ((File)localObject1).exists();
      if (bool1) {
        ((File)localObject1).delete();
      }
      i += 1;
    }
    int m = 5;
    localObject4 = new String[m];
    str7 = c.a("carb_store");
    localObject4[0] = str7;
    str7 = c.a("config_store");
    int j = 1;
    localObject4[j] = str7;
    localObject2 = c.a("aes_key_store");
    localObject4[2] = localObject2;
    localObject2 = c.a("mraid_js_store");
    localObject4[3] = localObject2;
    localObject2 = g.a();
    localObject4[4] = localObject2;
    localObject4 = Arrays.asList((Object[])localObject4);
    i = 0;
    str7 = null;
    int k;
    boolean bool2;
    for (;;)
    {
      k = ((List)localObject4).size();
      if (i >= k) {
        break;
      }
      localObject2 = new java/io/File;
      localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>("/data/data/");
      str1 = paramContext.getPackageName();
      ((StringBuilder)localObject3).append(str1);
      ((StringBuilder)localObject3).append("/shared_prefs/");
      str1 = (String)((List)localObject4).get(i);
      ((StringBuilder)localObject3).append(str1);
      str1 = ".xml";
      ((StringBuilder)localObject3).append(str1);
      localObject3 = ((StringBuilder)localObject3).toString();
      ((File)localObject2).<init>((String)localObject3);
      bool2 = ((File)localObject2).exists();
      if (bool2) {
        ((File)localObject2).delete();
      }
      i += 1;
    }
    localObject2 = "inmobi.cache.data.events.number";
    localObject3 = "inmobi.cache.data.events.timestamp";
    String[] tmp429_426 = new String[4];
    String[] tmp430_429 = tmp429_426;
    String[] tmp430_429 = tmp429_426;
    tmp430_429[0] = "inmobi.cache";
    tmp430_429[1] = "inmobi.cache.data";
    tmp430_429[2] = localObject2;
    String[] tmp442_430 = tmp430_429;
    tmp442_430[3] = localObject3;
    localObject4 = Arrays.asList(tmp442_430);
    i = 0;
    str7 = null;
    for (;;)
    {
      k = ((List)localObject4).size();
      if (i >= k) {
        break;
      }
      localObject2 = paramContext.getCacheDir();
      if (localObject2 != null)
      {
        localObject2 = new java/io/File;
        localObject3 = paramContext.getCacheDir();
        str1 = (String)((List)localObject4).get(i);
        ((File)localObject2).<init>((File)localObject3, str1);
        bool2 = ((File)localObject2).exists();
        if (bool2) {
          ((File)localObject2).delete();
        }
      }
      i += 1;
    }
    localObject4 = Arrays.asList(new String[] { "eventlog", "imai_click_events" });
    i = 0;
    str7 = null;
    for (;;)
    {
      k = ((List)localObject4).size();
      if (i >= k) {
        break;
      }
      localObject2 = paramContext.getDir("data", 0);
      if (localObject2 != null)
      {
        localObject2 = new java/io/File;
        localObject3 = paramContext.getDir("data", 0);
        str1 = (String)((List)localObject4).get(i);
        ((File)localObject2).<init>((File)localObject3, str1);
        bool2 = ((File)localObject2).exists();
        if (bool2) {
          ((File)localObject2).delete();
        }
      }
      i += 1;
    }
    paramContext = b(paramContext);
    int n = paramContext.size();
    if (n != 0) {
      return j;
    }
    return false;
  }
  
  private static boolean a(Context paramContext, String paramString)
  {
    File localFile = paramContext.getDatabasePath(paramString);
    if (localFile != null)
    {
      boolean bool1 = localFile.exists();
      if (bool1)
      {
        boolean bool2 = paramContext.deleteDatabase(paramString);
        if (!bool2) {
          return false;
        }
      }
    }
    return true;
  }
  
  public static List b(Context paramContext)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    HashSet localHashSet = new java/util/HashSet;
    localHashSet.<init>();
    localHashSet.add("adcache.db");
    localHashSet.add("appengage.db");
    localHashSet.add("im.db");
    localHashSet.add("ltvp.db");
    localHashSet.add("analytics.db");
    localHashSet.add("com.im.db");
    String[] arrayOfString = paramContext.databaseList();
    if (arrayOfString != null)
    {
      int i = arrayOfString.length;
      if (i > 0)
      {
        i = arrayOfString.length;
        int j = 0;
        while (j < i)
        {
          String str1 = arrayOfString[j];
          boolean bool = localHashSet.contains(str1);
          if (bool)
          {
            bool = a(paramContext, str1);
            if (!bool)
            {
              localArrayList.add(str1);
              break label207;
            }
          }
          String str2 = "com\\.im_([0-9]+\\.){3}db";
          bool = str1.matches(str2);
          if (bool)
          {
            str2 = com.inmobi.commons.core.d.a.a;
            bool = str1.equals(str2);
            if (!bool)
            {
              bool = a(paramContext, str1);
              if (!bool) {
                localArrayList.add(str1);
              }
            }
          }
          label207:
          j += 1;
        }
      }
    }
    return localArrayList;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.sdk.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */