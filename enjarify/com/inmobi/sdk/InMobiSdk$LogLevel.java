package com.inmobi.sdk;

public enum InMobiSdk$LogLevel
{
  static
  {
    Object localObject = new com/inmobi/sdk/InMobiSdk$LogLevel;
    ((LogLevel)localObject).<init>("NONE", 0);
    NONE = (LogLevel)localObject;
    localObject = new com/inmobi/sdk/InMobiSdk$LogLevel;
    int i = 1;
    ((LogLevel)localObject).<init>("ERROR", i);
    ERROR = (LogLevel)localObject;
    localObject = new com/inmobi/sdk/InMobiSdk$LogLevel;
    int j = 2;
    ((LogLevel)localObject).<init>("DEBUG", j);
    DEBUG = (LogLevel)localObject;
    localObject = new LogLevel[3];
    LogLevel localLogLevel = NONE;
    localObject[0] = localLogLevel;
    localLogLevel = ERROR;
    localObject[i] = localLogLevel;
    localLogLevel = DEBUG;
    localObject[j] = localLogLevel;
    $VALUES = (LogLevel[])localObject;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.sdk.InMobiSdk.LogLevel
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */