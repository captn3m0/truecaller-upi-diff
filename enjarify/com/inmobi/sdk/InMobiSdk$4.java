package com.inmobi.sdk;

import com.inmobi.ads.as;
import com.inmobi.ads.cache.AssetStore;
import com.inmobi.ads.y;
import com.inmobi.commons.core.configs.b.c;
import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;
import com.inmobi.commons.core.utilities.b.e;
import com.inmobi.signals.p;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;

final class InMobiSdk$4
  implements Runnable
{
  public final void run()
  {
    try
    {
      com.inmobi.commons.core.utilities.uid.c localc = com.inmobi.commons.core.utilities.uid.c.a();
      try
      {
        com.inmobi.commons.core.utilities.uid.c.c();
        localc.b();
      }
      catch (Exception localException1)
      {
        localException1.getMessage();
      }
      Object localObject1 = com.inmobi.commons.core.utilities.uid.c.a();
      ((com.inmobi.commons.core.utilities.uid.c)localObject1).b();
      localObject1 = com.inmobi.commons.core.configs.b.a();
      ((com.inmobi.commons.core.configs.b)localObject1).b();
      localObject1 = com.inmobi.rendering.a.c.a();
      ((com.inmobi.rendering.a.c)localObject1).b();
      localObject1 = com.inmobi.commons.core.a.a.a();
      localObject2 = com.inmobi.commons.core.a.a.b;
      Object localObject3 = null;
      ((AtomicBoolean)localObject2).set(false);
      localObject2 = com.inmobi.commons.core.configs.b.a();
      Object localObject4 = c;
      ((com.inmobi.commons.core.configs.b)localObject2).a((com.inmobi.commons.core.configs.a)localObject4, (b.c)localObject1);
      localObject2 = c;
      localObject2 = a;
      d = ((String)localObject2);
      localObject2 = a;
      localObject4 = new com/inmobi/commons/core/a/a$2;
      ((com.inmobi.commons.core.a.a.2)localObject4).<init>((com.inmobi.commons.core.a.a)localObject1);
      ((ExecutorService)localObject2).execute((Runnable)localObject4);
      localObject1 = com.inmobi.commons.core.e.b.a();
      ((com.inmobi.commons.core.e.b)localObject1).b();
      localObject1 = com.inmobi.a.a.a();
      localObject2 = com.inmobi.a.a.b;
      ((AtomicBoolean)localObject2).set(false);
      e.c();
      localObject2 = com.inmobi.commons.core.configs.b.a();
      localObject3 = c;
      ((com.inmobi.commons.core.configs.b)localObject2).a((com.inmobi.commons.core.configs.a)localObject3, (b.c)localObject1);
      localObject2 = c;
      localObject2 = f;
      d = ((String)localObject2);
      localObject2 = a;
      localObject3 = new com/inmobi/a/a$2;
      ((com.inmobi.a.a.2)localObject3).<init>((com.inmobi.a.a)localObject1);
      ((ExecutorService)localObject2).execute((Runnable)localObject3);
      localObject1 = p.a();
      ((p)localObject1).b();
      localObject1 = y.d();
      ((y)localObject1).a();
      localObject1 = as.d();
      ((as)localObject1).a();
      localObject1 = AssetStore.a();
      ((AssetStore)localObject1).b();
      return;
    }
    catch (Exception localException2)
    {
      InMobiSdk.access$000();
      localException2.getMessage();
      Logger.InternalLogLevel localInternalLogLevel = Logger.InternalLogLevel.DEBUG;
      Object localObject2 = InMobiSdk.access$000();
      Logger.a(localInternalLogLevel, (String)localObject2, "SDK encountered unexpected error while starting internal components");
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.sdk.InMobiSdk.4
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */