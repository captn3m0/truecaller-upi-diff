package com.inmobi.sdk;

import com.inmobi.commons.a.a;
import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;
import com.inmobi.commons.core.utilities.a.b;

final class InMobiSdk$2
  implements a.b
{
  public final void a(boolean paramBoolean)
  {
    a.b(paramBoolean);
    if (paramBoolean) {}
    try
    {
      InMobiSdk.access$100();
      return;
    }
    catch (Exception localException)
    {
      InMobiSdk.access$000();
      localException.getMessage();
      Logger.InternalLogLevel localInternalLogLevel = Logger.InternalLogLevel.DEBUG;
      String str = InMobiSdk.access$000();
      Logger.a(localInternalLogLevel, str, "SDK encountered an unexpected error; some components may not work as advertised");
    }
    InMobiSdk.access$200();
    return;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.sdk.InMobiSdk.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */