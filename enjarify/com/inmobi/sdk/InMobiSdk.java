package com.inmobi.sdk;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build.VERSION;
import android.os.SystemClock;
import com.inmobi.commons.core.d.c;
import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;
import com.inmobi.commons.core.utilities.b.g;
import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.json.JSONObject;

public final class InMobiSdk
{
  private static final ExecutorService COMPONENT_SERVICE = ;
  public static final String IM_GDPR_CONSENT_AVAILABLE = "gdpr_consent_available";
  private static final String TAG = "InMobiSdk";
  
  private static void deInitComponents()
  {
    try
    {
      localObject1 = COMPONENT_SERVICE;
      localObject2 = new com/inmobi/sdk/InMobiSdk$5;
      ((InMobiSdk.5)localObject2).<init>();
      ((ExecutorService)localObject1).execute((Runnable)localObject2);
      return;
    }
    catch (Exception localException)
    {
      localException.getMessage();
      Object localObject1 = Logger.InternalLogLevel.ERROR;
      Object localObject2 = TAG;
      Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "SDK encountered unexpected error while stopping internal components");
    }
  }
  
  public static String getVersion()
  {
    return "7.1.1";
  }
  
  private static boolean hasSdkVersionChanged(Context paramContext)
  {
    String str = com.inmobi.commons.a.b.a(paramContext);
    if (str != null)
    {
      paramContext = com.inmobi.commons.a.b.a(paramContext);
      str = "7.1.1";
      boolean bool = paramContext.equals(str);
      if (bool) {
        return false;
      }
    }
    return true;
  }
  
  public static void init(Context paramContext, String paramString)
  {
    init(paramContext, paramString, null);
  }
  
  public static void init(Context paramContext, String paramString, JSONObject paramJSONObject)
  {
    long l1 = SystemClock.elapsedRealtime();
    try
    {
      com.inmobi.commons.core.utilities.b.e.a(paramJSONObject);
      int i = Build.VERSION.SDK_INT;
      int k = 14;
      if (i < k)
      {
        paramContext = Logger.InternalLogLevel.ERROR;
        paramJSONObject = TAG;
        localObject1 = "The minimum supported Android API level is 14, SDK could not be initialized.";
        Logger.a(paramContext, paramJSONObject, (String)localObject1);
        return;
      }
      if (paramContext == null)
      {
        paramContext = Logger.InternalLogLevel.ERROR;
        paramJSONObject = TAG;
        localObject1 = "Context supplied as null, SDK could not be initialized.";
        Logger.a(paramContext, paramJSONObject, (String)localObject1);
        return;
      }
      if (paramString != null)
      {
        paramJSONObject = paramString.trim();
        i = paramJSONObject.length();
        if (i != 0)
        {
          paramJSONObject = new android/content/Intent;
          paramJSONObject.<init>();
          localObject1 = paramContext.getPackageName();
          localObject2 = "com.inmobi.rendering.InMobiAdActivity";
          paramJSONObject.setClassName((String)localObject1, (String)localObject2);
          localObject1 = paramContext.getPackageManager();
          int m = 65536;
          paramJSONObject = ((PackageManager)localObject1).resolveActivity(paramJSONObject, m);
          if (paramJSONObject == null)
          {
            paramContext = Logger.InternalLogLevel.ERROR;
            paramJSONObject = TAG;
            localObject1 = "The activity com.inmobi.rendering.InMobiAdActivity not present in AndroidManifest. SDK could not be initialized.";
            Logger.a(paramContext, paramJSONObject, (String)localObject1);
            return;
          }
          paramJSONObject = "ads";
          localObject1 = "android.permission.INTERNET";
          boolean bool1 = com.inmobi.commons.core.utilities.e.a(paramContext, paramJSONObject, (String)localObject1);
          if (bool1)
          {
            paramJSONObject = "ads";
            localObject1 = "android.permission.ACCESS_NETWORK_STATE";
            bool1 = com.inmobi.commons.core.utilities.e.a(paramContext, paramJSONObject, (String)localObject1);
            if (bool1)
            {
              paramJSONObject = "ads";
              localObject1 = "android.permission.ACCESS_COARSE_LOCATION";
              bool1 = com.inmobi.commons.core.utilities.e.a(paramContext, paramJSONObject, (String)localObject1);
              if (!bool1)
              {
                paramJSONObject = "ads";
                localObject1 = "android.permission.ACCESS_FINE_LOCATION";
                bool1 = com.inmobi.commons.core.utilities.e.a(paramContext, paramJSONObject, (String)localObject1);
                if (!bool1)
                {
                  paramJSONObject = Logger.InternalLogLevel.ERROR;
                  localObject1 = TAG;
                  localObject2 = "Please grant the location permissions (ACCESS_COARSE_LOCATION or ACCESS_FINE_LOCATION, or both) for better ad targeting.";
                  Logger.a(paramJSONObject, (String)localObject1, (String)localObject2);
                }
              }
              paramString = paramString.trim();
              int j = paramString.length();
              k = 32;
              if (j != k)
              {
                j = paramString.length();
                k = 36;
                if (j != k)
                {
                  paramJSONObject = Logger.InternalLogLevel.DEBUG;
                  localObject1 = TAG;
                  localObject2 = "Invalid account id passed to init. Please provide a valid account id.";
                  Logger.a(paramJSONObject, (String)localObject1, (String)localObject2);
                }
              }
              boolean bool2 = com.inmobi.commons.a.a.a();
              k = 0;
              localObject1 = null;
              if (bool2) {
                try
                {
                  com.inmobi.commons.core.e.b.a();
                  paramContext = "root";
                  paramJSONObject = "InitRequested";
                  com.inmobi.commons.core.e.b.a(paramContext, paramJSONObject, null);
                  return;
                }
                catch (Exception paramContext)
                {
                  paramContext.getMessage();
                  return;
                }
              }
              bool2 = hasSdkVersionChanged(paramContext);
              if (bool2)
              {
                bool2 = a.a(paramContext);
                com.inmobi.commons.a.b.a(paramContext, bool2);
                paramJSONObject = "7.1.1";
                localObject2 = "sdk_version_store";
                localObject2 = c.a(paramContext, (String)localObject2);
                str = "sdk_version";
                ((c)localObject2).a(str, paramJSONObject);
                paramJSONObject = paramContext.getApplicationContext();
                resetMediaCache(paramJSONObject);
              }
              com.inmobi.commons.a.a.a(paramContext, paramString);
              paramJSONObject = com.inmobi.commons.core.configs.b.a();
              paramJSONObject.b();
              paramJSONObject = com.inmobi.commons.core.e.b.a();
              paramJSONObject.b();
              paramJSONObject = "sdk_version_store";
              paramJSONObject = c.a(paramContext, paramJSONObject);
              localObject2 = "db_deletion_failed";
              String str = null;
              bool2 = paramJSONObject.b((String)localObject2, false);
              if (bool2)
              {
                paramJSONObject = a.b(paramContext);
                localObject2 = paramJSONObject.iterator();
                for (;;)
                {
                  boolean bool3 = ((Iterator)localObject2).hasNext();
                  if (!bool3) {
                    break;
                  }
                  Object localObject3 = ((Iterator)localObject2).next();
                  localObject3 = (String)localObject3;
                  sendDbDeletionTelemetryEvent((String)localObject3);
                }
                bool2 = paramJSONObject.isEmpty();
                if (bool2) {
                  com.inmobi.commons.a.b.a(paramContext, false);
                }
              }
              g.b();
              initComponents();
              com.inmobi.commons.core.configs.b.a();
              com.inmobi.commons.core.configs.b.d();
              paramJSONObject = COMPONENT_SERVICE;
              localObject2 = new com/inmobi/sdk/InMobiSdk$1;
              ((InMobiSdk.1)localObject2).<init>();
              paramJSONObject.execute((Runnable)localObject2);
              boolean bool4 = paramContext instanceof Activity;
              if (bool4)
              {
                paramContext = com.inmobi.commons.core.utilities.a.a();
                if (paramContext != null)
                {
                  paramJSONObject = new com/inmobi/sdk/InMobiSdk$2;
                  paramJSONObject.<init>();
                  paramContext.a(paramJSONObject);
                }
              }
              try
              {
                com.inmobi.commons.core.e.b.a();
                paramContext = "root";
                paramJSONObject = "InitRequested";
                com.inmobi.commons.core.e.b.a(paramContext, paramJSONObject, null);
              }
              catch (Exception paramContext)
              {
                paramContext.getMessage();
              }
            }
          }
          paramContext = Logger.InternalLogLevel.ERROR;
          paramJSONObject = TAG;
          localObject1 = "Please grant the mandatory permissions : INTERNET and ACCESS_NETWORK_STATE, SDK could not be initialized.";
          Logger.a(paramContext, paramJSONObject, (String)localObject1);
          return;
        }
      }
      paramContext = Logger.InternalLogLevel.ERROR;
      paramJSONObject = TAG;
      localObject1 = "Account id cannot be null or empty. Please provide a valid account id.";
      Logger.a(paramContext, paramJSONObject, (String)localObject1);
      return;
    }
    catch (Exception paramContext)
    {
      com.inmobi.commons.a.a.c();
      paramJSONObject = Logger.InternalLogLevel.ERROR;
      Object localObject1 = TAG;
      Object localObject2 = "SDK could not be initialized; an unexpected error was encountered";
      Logger.a(paramJSONObject, (String)localObject1, (String)localObject2);
      paramContext.getMessage();
      paramContext = Logger.InternalLogLevel.DEBUG;
      paramJSONObject = TAG;
      localObject1 = "InMobi SDK initialized with account id: ";
      paramString = String.valueOf(paramString);
      paramString = ((String)localObject1).concat(paramString);
      Logger.a(paramContext, paramJSONObject, paramString);
      try
      {
        paramContext = new java/util/HashMap;
        paramContext.<init>();
        paramString = "initTime";
        long l2 = SystemClock.elapsedRealtime() - l1;
        paramJSONObject = Long.valueOf(l2);
        paramContext.put(paramString, paramJSONObject);
        com.inmobi.commons.core.e.b.a();
        paramString = "root";
        paramJSONObject = "SdkInitialized";
        com.inmobi.commons.core.e.b.a(paramString, paramJSONObject, paramContext);
      }
      catch (Exception paramContext)
      {
        paramContext.getMessage();
      }
      printGrantedPermissions();
    }
  }
  
  private static void initComponents()
  {
    try
    {
      localObject1 = COMPONENT_SERVICE;
      localObject2 = new com/inmobi/sdk/InMobiSdk$4;
      ((InMobiSdk.4)localObject2).<init>();
      ((ExecutorService)localObject1).execute((Runnable)localObject2);
      return;
    }
    catch (Exception localException)
    {
      localException.getMessage();
      Object localObject1 = Logger.InternalLogLevel.DEBUG;
      Object localObject2 = TAG;
      Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "SDK encountered unexpected error while starting internal components");
    }
  }
  
  private static void printGrantedPermissions()
  {
    ExecutorService localExecutorService = COMPONENT_SERVICE;
    InMobiSdk.3 local3 = new com/inmobi/sdk/InMobiSdk$3;
    local3.<init>();
    localExecutorService.execute(local3);
  }
  
  private static void resetMediaCache(Context paramContext)
  {
    File localFile = com.inmobi.commons.a.a.a(paramContext);
    ExecutorService localExecutorService = COMPONENT_SERVICE;
    InMobiSdk.6 local6 = new com/inmobi/sdk/InMobiSdk$6;
    local6.<init>(localFile, paramContext);
    localExecutorService.execute(local6);
    boolean bool = localFile.mkdir();
    if (!bool) {
      localFile.isDirectory();
    }
  }
  
  private static void sendDbDeletionTelemetryEvent(String paramString)
  {
    boolean bool = com.inmobi.commons.a.a.a();
    if (bool) {
      try
      {
        HashMap localHashMap = new java/util/HashMap;
        localHashMap.<init>();
        String str1 = "filename";
        localHashMap.put(str1, paramString);
        str1 = "description";
        String str2 = "DB Deleted : ";
        paramString = String.valueOf(paramString);
        paramString = str2.concat(paramString);
        localHashMap.put(str1, paramString);
        com.inmobi.commons.core.e.b.a();
        paramString = "ads";
        str1 = "PersistentDataCleanFail";
        com.inmobi.commons.core.e.b.a(paramString, str1, localHashMap);
        return;
      }
      catch (Exception paramString)
      {
        paramString.getMessage();
      }
    }
  }
  
  public static void setAge(int paramInt)
  {
    g.a(paramInt);
  }
  
  public static void setAgeGroup(InMobiSdk.AgeGroup paramAgeGroup)
  {
    paramAgeGroup = paramAgeGroup.toString();
    Locale localLocale = Locale.ENGLISH;
    g.a(paramAgeGroup.toLowerCase(localLocale));
  }
  
  public static void setApplicationMuted(boolean paramBoolean)
  {
    com.inmobi.commons.a.a.a(paramBoolean);
  }
  
  public static void setAreaCode(String paramString)
  {
    g.b(paramString);
  }
  
  public static void setEducation(InMobiSdk.Education paramEducation)
  {
    paramEducation = paramEducation.toString();
    Locale localLocale = Locale.ENGLISH;
    g.h(paramEducation.toLowerCase(localLocale));
  }
  
  public static void setGender(InMobiSdk.Gender paramGender)
  {
    paramGender = paramGender.toString();
    Locale localLocale = Locale.ENGLISH;
    g.g(paramGender.toLowerCase(localLocale));
  }
  
  public static void setInterests(String paramString)
  {
    g.j(paramString);
  }
  
  public static void setLanguage(String paramString)
  {
    g.i(paramString);
  }
  
  public static void setLocation(Location paramLocation)
  {
    g.a(paramLocation);
  }
  
  public static void setLocationWithCityStateCountry(String paramString1, String paramString2, String paramString3)
  {
    g.d(paramString1);
    g.e(paramString2);
    g.f(paramString3);
  }
  
  public static void setLogLevel(InMobiSdk.LogLevel paramLogLevel)
  {
    int[] arrayOfInt = InMobiSdk.7.a;
    int i = paramLogLevel.ordinal();
    i = arrayOfInt[i];
    switch (i)
    {
    default: 
      break;
    case 3: 
      paramLogLevel = Logger.InternalLogLevel.DEBUG;
      Logger.a(paramLogLevel);
      break;
    case 2: 
      Logger.a(Logger.InternalLogLevel.ERROR);
      return;
    case 1: 
      Logger.a(Logger.InternalLogLevel.NONE);
      return;
    }
  }
  
  public static void setPostalCode(String paramString)
  {
    g.c(paramString);
  }
  
  public static void setYearOfBirth(int paramInt)
  {
    g.b(paramInt);
  }
  
  public static void updateGDPRConsent(JSONObject paramJSONObject)
  {
    com.inmobi.commons.core.utilities.b.e.a(paramJSONObject);
  }
}

/* Location:
 * Qualified Name:     com.inmobi.sdk.InMobiSdk
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */