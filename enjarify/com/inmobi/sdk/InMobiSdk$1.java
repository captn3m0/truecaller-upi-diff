package com.inmobi.sdk;

import android.content.ContentValues;
import android.content.Context;
import com.inmobi.ads.b.b;
import com.inmobi.ads.cache.AssetStore;
import com.inmobi.ads.cache.d;
import com.inmobi.commons.core.d.b;
import java.io.File;
import java.util.Iterator;
import java.util.List;

final class InMobiSdk$1
  implements Runnable
{
  public final void run()
  {
    try
    {
      Object localObject1 = AssetStore.a();
      ((AssetStore)localObject1).b();
      localObject1 = AssetStore.a();
      synchronized (AssetStore.e)
      {
        List localList = d.b();
        boolean bool1 = localList.isEmpty();
        if (bool1) {
          return;
        }
        Object localObject4 = localList.iterator();
        boolean bool2;
        Object localObject5;
        long l1;
        long l2;
        for (;;)
        {
          bool2 = ((Iterator)localObject4).hasNext();
          int k = 1;
          if (!bool2) {
            break;
          }
          localObject5 = ((Iterator)localObject4).next();
          localObject5 = (com.inmobi.ads.cache.a)localObject5;
          l1 = System.currentTimeMillis();
          l2 = h;
          boolean bool3 = l1 < l2;
          if (!bool3) {
            k = 0;
          }
          if (k != 0) {
            AssetStore.a((com.inmobi.ads.cache.a)localObject5);
          }
        }
        Object localObject6;
        boolean bool4;
        Object localObject7;
        int j;
        for (;;)
        {
          localObject4 = d.b();
          l1 = 0L;
          localObject4 = ((List)localObject4).iterator();
          for (;;)
          {
            bool2 = ((Iterator)localObject4).hasNext();
            if (!bool2) {
              break;
            }
            localObject5 = ((Iterator)localObject4).next();
            localObject5 = (com.inmobi.ads.cache.a)localObject5;
            localObject6 = new java/io/File;
            localObject5 = e;
            ((File)localObject6).<init>((String)localObject5);
            l2 = ((File)localObject6).length();
            l1 += l2;
          }
          localObject4 = b;
          long l3 = d;
          bool4 = l1 < l3;
          if (!bool4) {
            break;
          }
          localObject7 = b.a();
          String str1 = "asset";
          String[] arrayOfString = d.a;
          String str2 = "ts ASC ";
          localObject4 = ((b)localObject7).a(str1, arrayOfString, null, null, null, null, str2, null);
          j = ((List)localObject4).size();
          if (j == 0)
          {
            bool1 = false;
            localObject4 = null;
          }
          else
          {
            localObject4 = ((List)localObject4).get(0);
            localObject4 = (ContentValues)localObject4;
            localObject4 = d.a((ContentValues)localObject4);
          }
          if (localObject4 == null) {
            break;
          }
          AssetStore.a((com.inmobi.ads.cache.a)localObject4);
        }
        localObject1 = com.inmobi.commons.a.a.b();
        localObject1 = com.inmobi.commons.a.a.a((Context)localObject1);
        bool1 = ((File)localObject1).exists();
        if (bool1)
        {
          localObject1 = ((File)localObject1).listFiles();
          if (localObject1 != null)
          {
            int i = localObject1.length;
            j = 0;
            localObject5 = null;
            while (j < i)
            {
              Object localObject8 = localObject1[j];
              Iterator localIterator = localList.iterator();
              do
              {
                bool4 = localIterator.hasNext();
                if (!bool4) {
                  break;
                }
                localObject6 = localIterator.next();
                localObject6 = (com.inmobi.ads.cache.a)localObject6;
                localObject7 = ((File)localObject8).getAbsolutePath();
                localObject6 = e;
                bool4 = ((String)localObject7).equals(localObject6);
              } while (!bool4);
              int m = 1;
              break label456;
              m = 0;
              localIterator = null;
              label456:
              if (m == 0)
              {
                ((File)localObject8).getAbsolutePath();
                ((File)localObject8).delete();
              }
              j += 1;
            }
          }
        }
        return;
      }
      return;
    }
    catch (Exception localException)
    {
      InMobiSdk.access$000();
      localException.getMessage();
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.sdk.InMobiSdk.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */