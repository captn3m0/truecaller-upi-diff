package com.inmobi.sdk;

import android.content.Context;
import com.inmobi.commons.a.a;
import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;
import com.inmobi.commons.core.utilities.e;

final class InMobiSdk$3
  implements Runnable
{
  public final void run()
  {
    String str1 = "android.permission.CHANGE_WIFI_STATE";
    Object localObject1 = "android.permission.VIBRATE";
    String str2 = "android.permission.READ_CALENDAR";
    String str3 = "android.permission.WRITE_CALENDAR";
    String str4 = "com.google.android.gms.permission.ACTIVITY_RECOGNITION";
    String[] tmp22_19 = new String[8];
    String[] tmp23_22 = tmp22_19;
    String[] tmp23_22 = tmp22_19;
    tmp23_22[0] = "android.permission.ACCESS_COARSE_LOCATION";
    tmp23_22[1] = "android.permission.ACCESS_FINE_LOCATION";
    String[] tmp32_23 = tmp23_22;
    String[] tmp32_23 = tmp23_22;
    tmp32_23[2] = "android.permission.ACCESS_WIFI_STATE";
    tmp32_23[3] = str1;
    String[] tmp40_32 = tmp32_23;
    String[] tmp40_32 = tmp32_23;
    tmp40_32[4] = localObject1;
    tmp40_32[5] = str2;
    tmp40_32[6] = str3;
    String[] tmp52_40 = tmp40_32;
    tmp52_40[7] = str4;
    Object localObject2 = tmp52_40;
    Object localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>("Permissions granted to SDK are :\nandroid.permission.INTERNET\nandroid.permission.ACCESS_NETWORK_STATE");
    int i = 0;
    String str5 = null;
    for (;;)
    {
      int j = 8;
      if (i >= j) {
        break;
      }
      str1 = localObject2[i];
      localObject1 = a.b();
      str2 = "ads";
      boolean bool = e.a((Context)localObject1, str2, str1);
      if (bool)
      {
        localObject1 = "\n";
        ((StringBuilder)localObject3).append((String)localObject1);
        ((StringBuilder)localObject3).append(str1);
      }
      i += 1;
    }
    localObject2 = Logger.InternalLogLevel.DEBUG;
    str5 = InMobiSdk.access$000();
    localObject3 = ((StringBuilder)localObject3).toString();
    Logger.a((Logger.InternalLogLevel)localObject2, str5, (String)localObject3);
  }
}

/* Location:
 * Qualified Name:     com.inmobi.sdk.InMobiSdk.3
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */