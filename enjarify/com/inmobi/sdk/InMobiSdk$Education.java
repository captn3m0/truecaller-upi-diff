package com.inmobi.sdk;

public enum InMobiSdk$Education
{
  private String a;
  
  static
  {
    Object localObject = new com/inmobi/sdk/InMobiSdk$Education;
    ((Education)localObject).<init>("HIGH_SCHOOL_OR_LESS", 0, "highschoolorless");
    HIGH_SCHOOL_OR_LESS = (Education)localObject;
    localObject = new com/inmobi/sdk/InMobiSdk$Education;
    int i = 1;
    ((Education)localObject).<init>("COLLEGE_OR_GRADUATE", i, "collegeorgraduate");
    COLLEGE_OR_GRADUATE = (Education)localObject;
    localObject = new com/inmobi/sdk/InMobiSdk$Education;
    int j = 2;
    ((Education)localObject).<init>("POST_GRADUATE_OR_ABOVE", j, "postgraduateorabove");
    POST_GRADUATE_OR_ABOVE = (Education)localObject;
    localObject = new Education[3];
    Education localEducation = HIGH_SCHOOL_OR_LESS;
    localObject[0] = localEducation;
    localEducation = COLLEGE_OR_GRADUATE;
    localObject[i] = localEducation;
    localEducation = POST_GRADUATE_OR_ABOVE;
    localObject[j] = localEducation;
    $VALUES = (Education[])localObject;
  }
  
  private InMobiSdk$Education(String paramString1)
  {
    a = paramString1;
  }
  
  public final String toString()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.sdk.InMobiSdk.Education
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */