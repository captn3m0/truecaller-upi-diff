package com.inmobi.sdk;

import com.inmobi.a.a;
import com.inmobi.a.a.3;
import com.inmobi.ads.as;
import com.inmobi.ads.cache.AssetStore;
import com.inmobi.ads.y;
import com.inmobi.commons.core.e.b.2;
import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;
import com.inmobi.signals.p;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;

final class InMobiSdk$5
  implements Runnable
{
  public final void run()
  {
    try
    {
      Object localObject1 = com.inmobi.commons.core.configs.b.a();
      ((com.inmobi.commons.core.configs.b)localObject1).c();
      localObject1 = com.inmobi.commons.core.e.b.a();
      localObject2 = com.inmobi.commons.core.e.b.b;
      boolean bool = true;
      ((AtomicBoolean)localObject2).set(bool);
      localObject2 = a;
      Object localObject3 = new com/inmobi/commons/core/e/b$2;
      ((b.2)localObject3).<init>((com.inmobi.commons.core.e.b)localObject1);
      ((ExecutorService)localObject2).execute((Runnable)localObject3);
      localObject1 = a.a();
      localObject2 = a.b;
      ((AtomicBoolean)localObject2).set(bool);
      localObject2 = a;
      localObject3 = new com/inmobi/a/a$3;
      ((a.3)localObject3).<init>((a)localObject1);
      ((ExecutorService)localObject2).execute((Runnable)localObject3);
      localObject1 = p.a();
      ((p)localObject1).c();
      localObject1 = y.d();
      ((y)localObject1).b();
      localObject1 = as.d();
      ((as)localObject1).b();
      localObject1 = AssetStore.a();
      localObject2 = d;
      ((AtomicBoolean)localObject2).set(bool);
      ((AssetStore)localObject1).c();
      return;
    }
    catch (Exception localException)
    {
      InMobiSdk.access$000();
      localException.getMessage();
      Logger.InternalLogLevel localInternalLogLevel = Logger.InternalLogLevel.ERROR;
      Object localObject2 = InMobiSdk.access$000();
      Logger.a(localInternalLogLevel, (String)localObject2, "SDK encountered unexpected error while stopping internal components");
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.sdk.InMobiSdk.5
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */