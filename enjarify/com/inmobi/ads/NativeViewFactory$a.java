package com.inmobi.ads;

import android.content.Context;
import android.widget.ImageView;
import java.lang.ref.WeakReference;

final class NativeViewFactory$a
  implements Runnable
{
  private WeakReference a;
  private WeakReference b;
  
  NativeViewFactory$a(Context paramContext, ImageView paramImageView)
  {
    WeakReference localWeakReference = new java/lang/ref/WeakReference;
    localWeakReference.<init>(paramContext);
    a = localWeakReference;
    paramContext = new java/lang/ref/WeakReference;
    paramContext.<init>(paramImageView);
    b = paramContext;
  }
  
  public final void run()
  {
    Context localContext = (Context)a.get();
    ImageView localImageView = (ImageView)b.get();
    if ((localContext != null) && (localImageView != null)) {
      NativeViewFactory.a(localContext, localImageView);
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.NativeViewFactory.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */