package com.inmobi.ads;

import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Xfermode;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.LinearInterpolator;
import com.inmobi.commons.core.utilities.b.c;
import java.util.concurrent.TimeUnit;

public class NativeTimerView
  extends View
{
  long a = 0L;
  long b;
  ValueAnimator c;
  private Bitmap d;
  private Canvas e;
  private RectF f;
  private RectF g;
  private Rect h;
  private Paint i;
  private Paint j;
  private Paint k;
  private Paint l;
  private Paint m;
  private float n;
  private NativeTimerView.b o;
  
  public NativeTimerView(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public NativeTimerView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public NativeTimerView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    paramContext = new android/graphics/Paint;
    paramContext.<init>();
    i = paramContext;
    paramContext = i;
    boolean bool = true;
    paramContext.setAntiAlias(bool);
    i.setColor(-723724);
    paramContext = new android/graphics/Paint;
    paramContext.<init>();
    m = paramContext;
    m.setAntiAlias(bool);
    paramContext = m;
    paramInt = -16777216;
    paramContext.setColor(paramInt);
    paramContext = m;
    Object localObject = Paint.Align.CENTER;
    paramContext.setTextAlign((Paint.Align)localObject);
    m.setAntiAlias(bool);
    paramContext = new android/graphics/Rect;
    paramContext.<init>();
    h = paramContext;
    paramContext = new android/graphics/Paint;
    paramContext.<init>();
    j = paramContext;
    j.setAntiAlias(bool);
    j.setColor(paramInt);
    paramContext = new android/graphics/Paint;
    paramContext.<init>();
    k = paramContext;
    k.setAntiAlias(bool);
    k.setColor(0);
    paramContext = k;
    localObject = new android/graphics/PorterDuffXfermode;
    PorterDuff.Mode localMode = PorterDuff.Mode.CLEAR;
    ((PorterDuffXfermode)localObject).<init>(localMode);
    paramContext.setXfermode((Xfermode)localObject);
    paramContext = new android/graphics/Paint;
    paramContext.<init>();
    l = paramContext;
    paramContext = l;
    localObject = Paint.Style.STROKE;
    paramContext.setStyle((Paint.Style)localObject);
    l.setAntiAlias(bool);
    l.setColor(paramInt);
  }
  
  private void b()
  {
    NativeTimerView.b localb = o;
    if (localb != null)
    {
      localb.a();
      c.cancel();
      localb = null;
      c = null;
    }
  }
  
  public final void a()
  {
    Object localObject1 = new float[2];
    Object tmp5_4 = localObject1;
    tmp5_4[0] = 0.0F;
    tmp5_4[1] = 1.0F;
    localObject1 = ValueAnimator.ofFloat((float[])localObject1);
    c = ((ValueAnimator)localObject1);
    localObject1 = c;
    Object localObject2 = TimeUnit.SECONDS;
    long l1 = a;
    long l2 = ((TimeUnit)localObject2).toMillis(l1);
    ((ValueAnimator)localObject1).setDuration(l2);
    localObject1 = c;
    localObject2 = new android/view/animation/LinearInterpolator;
    ((LinearInterpolator)localObject2).<init>();
    ((ValueAnimator)localObject1).setInterpolator((TimeInterpolator)localObject2);
    localObject1 = c;
    localObject2 = new com/inmobi/ads/NativeTimerView$a;
    ((NativeTimerView.a)localObject2).<init>(this);
    ((ValueAnimator)localObject1).addUpdateListener((ValueAnimator.AnimatorUpdateListener)localObject2);
    c.start();
  }
  
  final void a(float paramFloat)
  {
    paramFloat *= 360.0F;
    n = paramFloat;
    invalidate();
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    Object localObject1 = e;
    Object localObject2 = PorterDuff.Mode.CLEAR;
    boolean bool1 = false;
    float f1 = 0.0F;
    RectF localRectF = null;
    ((Canvas)localObject1).drawColor(0, (PorterDuff.Mode)localObject2);
    int i1 = getWidth() / 2;
    int i2 = getHeight() / 2;
    int i3 = Math.min(i1, i2);
    float f2 = getWidth() * 7.0F * 0.007F;
    int i4 = c.a((int)f2);
    float f3 = i1;
    float f4 = i2;
    float f5 = i3;
    Paint localPaint1 = i;
    paramCanvas.drawCircle(f3, f4, f5, localPaint1);
    i3 -= i4;
    float f6 = i3;
    Object localObject3 = l;
    paramCanvas.drawCircle(f3, f4, f6, (Paint)localObject3);
    localObject1 = c;
    Object localObject4;
    if (localObject1 != null)
    {
      long l1 = a;
      long l2 = ((ValueAnimator)localObject1).getCurrentPlayTime() / 1000L;
      l1 -= l2;
      i1 = (int)l1;
      localObject2 = (Float)c.getAnimatedValue();
      f4 = ((Float)localObject2).floatValue();
      double d1 = f4;
      double d2 = 1.0D;
      bool2 = d1 < d2;
      if (!bool2)
      {
        i1 = 0;
        f3 = 0.0F;
        localObject1 = null;
      }
      localObject2 = m;
      localObject4 = h;
      localObject1 = String.valueOf(i1);
      i4 = ((String)localObject1).length();
      ((Paint)localObject2).getTextBounds((String)localObject1, 0, i4, (Rect)localObject4);
      f1 = ((Paint)localObject2).descent();
      f6 = ((Paint)localObject2).ascent();
      f1 = (f1 - f6) / 2.0F;
      f6 = ((Paint)localObject2).descent();
      f1 -= f6;
      i3 = getWidth() / 2;
      f6 = i3;
      i4 = getHeight() / 2;
      f2 = i4 + f1;
      paramCanvas.drawText((String)localObject1, f6, f2, (Paint)localObject2);
      localObject1 = (Float)c.getAnimatedValue();
      f3 = ((Float)localObject1).floatValue();
      double d3 = f3;
      bool1 = d3 < d2;
      if (!bool1) {
        b();
      }
    }
    float f7 = n;
    i1 = 0;
    f3 = 0.0F;
    localObject1 = null;
    boolean bool2 = f7 < 0.0F;
    if (bool2)
    {
      localObject4 = e;
      localObject3 = f;
      f5 = 270.0F;
      boolean bool3 = true;
      Paint localPaint2 = j;
      ((Canvas)localObject4).drawArc((RectF)localObject3, f5, f7, bool3, localPaint2);
      localObject2 = e;
      localRectF = g;
      localObject4 = k;
      ((Canvas)localObject2).drawOval(localRectF, (Paint)localObject4);
    }
    localObject2 = d;
    paramCanvas.drawBitmap((Bitmap)localObject2, 0.0F, 0.0F, null);
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    super.onMeasure(paramInt1, paramInt1);
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    if ((paramInt1 != paramInt3) || (paramInt2 != paramInt4))
    {
      localObject = Bitmap.Config.ARGB_8888;
      localObject = Bitmap.createBitmap(paramInt1, paramInt2, (Bitmap.Config)localObject);
      d = ((Bitmap)localObject);
      localObject = d;
      f1 = 0.0F;
      ((Bitmap)localObject).eraseColor(0);
      localObject = new android/graphics/Canvas;
      Bitmap localBitmap = d;
      ((Canvas)localObject).<init>(localBitmap);
      e = ((Canvas)localObject);
    }
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    float f2 = getWidth() * 4.0F;
    float f3 = 0.007F;
    f2 = c.a((int)(f2 * f3));
    float f4 = c.a((int)(getWidth() * 14.0F * f3));
    float f5 = c.a((int)(getWidth() * 5.0F * f3));
    f3 = c.a((int)(getWidth() * 1.5F * f3));
    Object localObject = new android/graphics/RectF;
    float f1 = getWidth() - f5;
    float f6 = getHeight() - f5;
    ((RectF)localObject).<init>(f5, f5, f1, f6);
    f = ((RectF)localObject);
    RectF localRectF = new android/graphics/RectF;
    float f7 = f.left + f2;
    f1 = f.top + f2;
    f6 = f.right - f2;
    float f8 = f.bottom - f2;
    localRectF.<init>(f7, f1, f6, f8);
    g = localRectF;
    l.setStrokeWidth(f3);
    m.setTextSize(f4);
    invalidate();
  }
  
  public void setTimerEventsListener(NativeTimerView.b paramb)
  {
    o = paramb;
  }
  
  public void setTimerValue(long paramLong)
  {
    a = paramLong;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.NativeTimerView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */