package com.inmobi.ads;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

final class j$c
{
  static HashMap a(String paramString1, String paramString2, JSONArray paramJSONArray1, JSONArray paramJSONArray2, JSONObject paramJSONObject)
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    int i = 0;
    String str1 = null;
    int j;
    if (paramJSONArray1 != null) {
      try
      {
        j = paramJSONArray1.length();
        int k = 0;
        String str2 = null;
        while (k < j)
        {
          Object localObject = new java/lang/StringBuilder;
          ((StringBuilder)localObject).<init>();
          ((StringBuilder)localObject).append(paramString1);
          int m = k + 1;
          ((StringBuilder)localObject).append(m);
          localObject = ((StringBuilder)localObject).toString();
          str2 = paramJSONArray1.getString(k);
          localHashMap.put(localObject, str2);
          k = m;
        }
        if (paramJSONArray2 == null) {
          break label179;
        }
      }
      catch (Exception paramString1) {}
    }
    int n = paramJSONArray2.length();
    while (i < n)
    {
      paramJSONArray1 = new java/lang/StringBuilder;
      paramJSONArray1.<init>();
      paramJSONArray1.append(paramString2);
      j = i + 1;
      paramJSONArray1.append(j);
      paramJSONArray1 = paramJSONArray1.toString();
      str1 = paramJSONArray2.getString(i);
      localHashMap.put(paramJSONArray1, str1);
      i = j;
    }
    label179:
    if (paramJSONObject != null)
    {
      paramString1 = paramJSONObject.keys();
      for (;;)
      {
        boolean bool = paramString1.hasNext();
        if (!bool) {
          break;
        }
        paramString2 = paramString1.next();
        paramString2 = (String)paramString2;
        paramJSONArray1 = paramJSONObject.optString(paramString2);
        localHashMap.put(paramString2, paramJSONArray1);
      }
      j.H();
      paramString1.getMessage();
      paramString2 = com.inmobi.commons.core.a.a.a();
      paramJSONArray1 = new com/inmobi/commons/core/e/a;
      paramJSONArray1.<init>(paramString1);
      paramString2.a(paramJSONArray1);
    }
    return localHashMap;
  }
  
  static Map a(JSONArray paramJSONArray)
  {
    try
    {
      int i = paramJSONArray.length();
      boolean bool1 = false;
      localObject1 = null;
      int j = 0;
      Object localObject2 = null;
      Object localObject3;
      while (j < i)
      {
        localObject3 = paramJSONArray.getJSONObject(j);
        String str = "moat";
        boolean bool2 = ((JSONObject)localObject3).has(str);
        if (bool2)
        {
          paramJSONArray = "moat";
          paramJSONArray = ((JSONObject)localObject3).getJSONObject(paramJSONArray);
          break label71;
        }
        j += 1;
      }
      paramJSONArray = null;
      label71:
      if (paramJSONArray != null)
      {
        localObject4 = new java/util/HashMap;
        ((HashMap)localObject4).<init>();
        localObject2 = "enabled";
        localObject3 = "enabled";
        boolean bool3 = paramJSONArray.getBoolean((String)localObject3);
        localObject3 = Boolean.valueOf(bool3);
        ((Map)localObject4).put(localObject2, localObject3);
        localObject2 = "instrumentVideo";
        localObject3 = "instrumentVideo";
        bool1 = paramJSONArray.optBoolean((String)localObject3, false);
        localObject1 = Boolean.valueOf(bool1);
        ((Map)localObject4).put(localObject2, localObject1);
        localObject1 = "partnerCode";
        localObject2 = "partnerCode";
        localObject2 = paramJSONArray.optString((String)localObject2, null);
        ((Map)localObject4).put(localObject1, localObject2);
        localObject1 = "clientLevels";
        localObject2 = "clientLevels";
        localObject2 = paramJSONArray.optJSONArray((String)localObject2);
        ((Map)localObject4).put(localObject1, localObject2);
        localObject1 = "clientSlicers";
        localObject2 = "clientSlicers";
        localObject2 = paramJSONArray.optJSONArray((String)localObject2);
        ((Map)localObject4).put(localObject1, localObject2);
        localObject1 = "zMoatExtras";
        localObject2 = "zMoatExtras";
        paramJSONArray = paramJSONArray.optJSONObject((String)localObject2);
        ((Map)localObject4).put(localObject1, paramJSONArray);
        return (Map)localObject4;
      }
      return null;
    }
    catch (JSONException paramJSONArray)
    {
      j.H();
      paramJSONArray.getMessage();
      Object localObject4 = com.inmobi.commons.core.a.a.a();
      Object localObject1 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject1).<init>(paramJSONArray);
      ((com.inmobi.commons.core.a.a)localObject4).a((com.inmobi.commons.core.e.a)localObject1);
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.j.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */