package com.inmobi.ads;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings.System;
import android.view.View;
import android.view.ViewGroup;
import com.b.a.a.a.f.a.g;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.Set;

class r
  extends bv
{
  private static final String d = "r";
  private final WeakReference e;
  private final bw f;
  private final com.b.a.a.a.f.d g;
  private r.a h;
  private WeakReference i;
  
  r(Activity paramActivity, bw parambw, ba paramba, com.b.a.a.a.f.d paramd)
  {
    super(paramba);
    paramba = new java/lang/ref/WeakReference;
    paramba.<init>(paramActivity);
    e = paramba;
    f = parambw;
    g = paramd;
  }
  
  static com.b.a.a.a.f.d a(Context paramContext, Set paramSet)
  {
    Object localObject = new com/b/a/a/a/f/f;
    ((com.b.a.a.a.f.f)localObject).<init>("7.1.1", true);
    com.b.a.a.a.d.b().a(paramContext);
    com.b.a.a.a.f.d locald = new com/b/a/a/a/f/d;
    locald.<init>();
    g localg = new com/b/a/a/a/f/a/g;
    String str = a;
    localg.<init>(paramContext, str, (com.b.a.a.a.f.f)localObject);
    localg.d();
    com.b.a.a.a.d.b();
    com.b.a.a.a.d.a(locald, localg);
    boolean bool1 = paramContext instanceof Activity;
    localg = null;
    if (bool1)
    {
      paramContext = (Activity)paramContext;
      locald.a(null, paramContext);
    }
    else
    {
      locald.a(null, null);
    }
    paramContext = paramSet.iterator();
    for (;;)
    {
      boolean bool2 = paramContext.hasNext();
      if (!bool2) {
        break;
      }
      paramSet = (String)paramContext.next();
      com.b.a.a.a.d.b();
      localObject = (com.b.a.a.a.f.a.f)com.b.a.a.a.d.a(a);
      if (localObject != null)
      {
        localObject = j;
        ((com.b.a.a.a.f.a.b.a)localObject).a(paramSet);
      }
    }
    return locald;
  }
  
  private void a(ViewGroup paramViewGroup, NativeVideoWrapper paramNativeVideoWrapper)
  {
    int j = paramViewGroup.getChildCount();
    int k = 0;
    while (k < j)
    {
      Object localObject = paramViewGroup.getChildAt(k);
      boolean bool = localObject.equals(paramNativeVideoWrapper);
      if (!bool)
      {
        com.b.a.a.a.f.d locald = g;
        locald.b((View)localObject);
        bool = localObject instanceof ViewGroup;
        if (bool)
        {
          localObject = (ViewGroup)localObject;
          int m = ((ViewGroup)localObject).getChildCount();
          if (m > 0) {
            a((ViewGroup)localObject, paramNativeVideoWrapper);
          }
        }
      }
      k += 1;
    }
  }
  
  private void g()
  {
    Object localObject1 = (Activity)e.get();
    if (localObject1 != null)
    {
      Object localObject2 = a;
      boolean bool1 = localObject2 instanceof ba;
      if (bool1)
      {
        localObject2 = (NativeVideoWrapper)a.getVideoContainerView();
        if (localObject2 != null)
        {
          Object localObject3 = new java/lang/ref/WeakReference;
          ((WeakReference)localObject3).<init>(localObject2);
          i = ((WeakReference)localObject3);
          localObject3 = f.b();
          Object localObject4;
          if ((localObject2 != null) && (localObject3 != null))
          {
            boolean bool2 = localObject3 instanceof ViewGroup;
            if (bool2)
            {
              localObject4 = new android/os/Handler;
              Object localObject5 = Looper.getMainLooper();
              ((Handler)localObject4).<init>((Looper)localObject5);
              localObject5 = new com/inmobi/ads/r$1;
              ((r.1)localObject5).<init>(this, (View)localObject3, (NativeVideoWrapper)localObject2);
              ((Handler)localObject4).post((Runnable)localObject5);
            }
          }
          localObject2 = g;
          localObject3 = (View)i.get();
          ((com.b.a.a.a.f.d)localObject2).a((View)localObject3, (Activity)localObject1);
          localObject2 = h;
          if (localObject2 == null)
          {
            localObject2 = new com/inmobi/ads/r$a;
            localObject3 = ((Activity)localObject1).getApplicationContext();
            ((r.a)localObject2).<init>((Context)localObject3, this);
            h = ((r.a)localObject2);
            localObject1 = ((Activity)localObject1).getContentResolver();
            localObject2 = Settings.System.CONTENT_URI;
            boolean bool3 = true;
            localObject4 = h;
            ((ContentResolver)localObject1).registerContentObserver((Uri)localObject2, bool3, (ContentObserver)localObject4);
          }
          localObject1 = g;
          localObject1.hashCode();
        }
      }
    }
  }
  
  public final View a()
  {
    return f.a();
  }
  
  public final View a(View paramView, ViewGroup paramViewGroup, boolean paramBoolean)
  {
    return f.a(paramView, paramViewGroup, paramBoolean);
  }
  
  /* Error */
  public final void a(int paramInt)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 33	com/inmobi/ads/r:g	Lcom/b/a/a/a/f/d;
    //   4: astore_2
    //   5: aload_2
    //   6: invokevirtual 133	com/b/a/a/a/f/d:c	()Lcom/b/a/a/a/h/a;
    //   9: astore_2
    //   10: aload_2
    //   11: ifnull +616 -> 627
    //   14: aload_0
    //   15: getfield 33	com/inmobi/ads/r:g	Lcom/b/a/a/a/f/d;
    //   18: astore_2
    //   19: aload_2
    //   20: invokevirtual 136	java/lang/Object:hashCode	()I
    //   23: pop
    //   24: iload_1
    //   25: tableswitch	default:+87->112, 0:+586->611, 1:+551->576, 2:+516->541, 3:+497->522, 4:+478->503, 5:+87->112, 6:+443->468, 7:+382->407, 8:+363->388, 9:+344->369, 10:+325->350, 11:+306->331, 12:+271->296, 13:+139->164, 14:+139->164, 15:+120->145, 16:+113->138, 17:+90->115
    //   112: goto +515 -> 627
    //   115: aload_0
    //   116: getfield 33	com/inmobi/ads/r:g	Lcom/b/a/a/a/f/d;
    //   119: astore_2
    //   120: aload_2
    //   121: invokevirtual 133	com/b/a/a/a/f/d:c	()Lcom/b/a/a/a/h/a;
    //   124: astore_2
    //   125: ldc -12
    //   127: astore_3
    //   128: aload_2
    //   129: aload_3
    //   130: invokeinterface 245 2 0
    //   135: goto +492 -> 627
    //   138: aload_0
    //   139: invokespecial 247	com/inmobi/ads/r:g	()V
    //   142: goto +485 -> 627
    //   145: aload_0
    //   146: getfield 33	com/inmobi/ads/r:g	Lcom/b/a/a/a/f/d;
    //   149: astore_2
    //   150: aload_2
    //   151: invokevirtual 133	com/b/a/a/a/f/d:c	()Lcom/b/a/a/a/h/a;
    //   154: astore_2
    //   155: aload_2
    //   156: invokeinterface 250 1 0
    //   161: goto +466 -> 627
    //   164: aload_0
    //   165: getfield 33	com/inmobi/ads/r:g	Lcom/b/a/a/a/f/d;
    //   168: astore_2
    //   169: aload_2
    //   170: invokevirtual 133	com/b/a/a/a/f/d:c	()Lcom/b/a/a/a/h/a;
    //   173: astore_2
    //   174: iconst_1
    //   175: istore 4
    //   177: aconst_null
    //   178: astore 5
    //   180: bipush 13
    //   182: istore 6
    //   184: iload 6
    //   186: iload_1
    //   187: if_icmpne +12 -> 199
    //   190: iconst_0
    //   191: istore 7
    //   193: aconst_null
    //   194: astore 8
    //   196: goto +47 -> 243
    //   199: aload_0
    //   200: getfield 212	com/inmobi/ads/r:h	Lcom/inmobi/ads/r$a;
    //   203: astore 8
    //   205: aload 8
    //   207: ifnull +33 -> 240
    //   210: aload_0
    //   211: getfield 29	com/inmobi/ads/r:e	Ljava/lang/ref/WeakReference;
    //   214: astore 8
    //   216: aload 8
    //   218: invokevirtual 169	java/lang/ref/WeakReference:get	()Ljava/lang/Object;
    //   221: astore 8
    //   223: aload 8
    //   225: checkcast 253	android/content/Context
    //   228: astore 8
    //   230: aload 8
    //   232: invokestatic 258	com/inmobi/commons/core/utilities/b/b:a	(Landroid/content/Context;)I
    //   235: istore 7
    //   237: goto +6 -> 243
    //   240: iconst_1
    //   241: istore 7
    //   243: iload 7
    //   245: invokestatic 142	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   248: astore 8
    //   250: aload_2
    //   251: aload 8
    //   253: invokeinterface 147 2 0
    //   258: aload_0
    //   259: getfield 212	com/inmobi/ads/r:h	Lcom/inmobi/ads/r$a;
    //   262: astore_2
    //   263: aload_2
    //   264: ifnull +363 -> 627
    //   267: aload_0
    //   268: getfield 212	com/inmobi/ads/r:h	Lcom/inmobi/ads/r$a;
    //   271: astore_2
    //   272: iload 6
    //   274: iload_1
    //   275: if_icmpne +6 -> 281
    //   278: goto +8 -> 286
    //   281: iconst_0
    //   282: istore 4
    //   284: aconst_null
    //   285: astore_3
    //   286: aload_2
    //   287: iload 4
    //   289: invokestatic 261	com/inmobi/ads/r$a:a	(Lcom/inmobi/ads/r$a;Z)Z
    //   292: pop
    //   293: goto +334 -> 627
    //   296: aload_0
    //   297: getfield 33	com/inmobi/ads/r:g	Lcom/b/a/a/a/f/d;
    //   300: astore_2
    //   301: aload_2
    //   302: invokevirtual 133	com/b/a/a/a/f/d:c	()Lcom/b/a/a/a/h/a;
    //   305: astore_2
    //   306: aload_2
    //   307: invokeinterface 263 1 0
    //   312: aload_0
    //   313: getfield 33	com/inmobi/ads/r:g	Lcom/b/a/a/a/f/d;
    //   316: astore_2
    //   317: aload_2
    //   318: invokevirtual 133	com/b/a/a/a/f/d:c	()Lcom/b/a/a/a/h/a;
    //   321: astore_2
    //   322: aload_2
    //   323: invokeinterface 265 1 0
    //   328: goto +299 -> 627
    //   331: aload_0
    //   332: getfield 33	com/inmobi/ads/r:g	Lcom/b/a/a/a/f/d;
    //   335: astore_2
    //   336: aload_2
    //   337: invokevirtual 133	com/b/a/a/a/f/d:c	()Lcom/b/a/a/a/h/a;
    //   340: astore_2
    //   341: aload_2
    //   342: invokeinterface 267 1 0
    //   347: goto +280 -> 627
    //   350: aload_0
    //   351: getfield 33	com/inmobi/ads/r:g	Lcom/b/a/a/a/f/d;
    //   354: astore_2
    //   355: aload_2
    //   356: invokevirtual 133	com/b/a/a/a/f/d:c	()Lcom/b/a/a/a/h/a;
    //   359: astore_2
    //   360: aload_2
    //   361: invokeinterface 269 1 0
    //   366: goto +261 -> 627
    //   369: aload_0
    //   370: getfield 33	com/inmobi/ads/r:g	Lcom/b/a/a/a/f/d;
    //   373: astore_2
    //   374: aload_2
    //   375: invokevirtual 133	com/b/a/a/a/f/d:c	()Lcom/b/a/a/a/h/a;
    //   378: astore_2
    //   379: aload_2
    //   380: invokeinterface 271 1 0
    //   385: goto +242 -> 627
    //   388: aload_0
    //   389: getfield 33	com/inmobi/ads/r:g	Lcom/b/a/a/a/f/d;
    //   392: astore_2
    //   393: aload_2
    //   394: invokevirtual 133	com/b/a/a/a/f/d:c	()Lcom/b/a/a/a/h/a;
    //   397: astore_2
    //   398: aload_2
    //   399: invokeinterface 274 1 0
    //   404: goto +223 -> 627
    //   407: aload_0
    //   408: getfield 33	com/inmobi/ads/r:g	Lcom/b/a/a/a/f/d;
    //   411: astore_2
    //   412: aload_2
    //   413: invokevirtual 133	com/b/a/a/a/f/d:c	()Lcom/b/a/a/a/h/a;
    //   416: astore_2
    //   417: aload_2
    //   418: invokeinterface 277 1 0
    //   423: aload_0
    //   424: getfield 33	com/inmobi/ads/r:g	Lcom/b/a/a/a/f/d;
    //   427: astore_2
    //   428: aload_0
    //   429: getfield 186	com/inmobi/ads/r:i	Ljava/lang/ref/WeakReference;
    //   432: astore_3
    //   433: aload_3
    //   434: ifnonnull +11 -> 445
    //   437: iconst_0
    //   438: istore 4
    //   440: aconst_null
    //   441: astore_3
    //   442: goto +18 -> 460
    //   445: aload_0
    //   446: getfield 186	com/inmobi/ads/r:i	Ljava/lang/ref/WeakReference;
    //   449: astore_3
    //   450: aload_3
    //   451: invokevirtual 169	java/lang/ref/WeakReference:get	()Ljava/lang/Object;
    //   454: astore_3
    //   455: aload_3
    //   456: checkcast 203	android/view/View
    //   459: astore_3
    //   460: aload_2
    //   461: aload_3
    //   462: invokevirtual 279	com/b/a/a/a/f/d:a	(Landroid/view/View;)V
    //   465: goto +162 -> 627
    //   468: aload_0
    //   469: getfield 33	com/inmobi/ads/r:g	Lcom/b/a/a/a/f/d;
    //   472: astore_2
    //   473: aload_2
    //   474: invokevirtual 133	com/b/a/a/a/f/d:c	()Lcom/b/a/a/a/h/a;
    //   477: astore_2
    //   478: aload_2
    //   479: invokeinterface 282 1 0
    //   484: aload_0
    //   485: getfield 33	com/inmobi/ads/r:g	Lcom/b/a/a/a/f/d;
    //   488: astore_2
    //   489: aload_2
    //   490: invokevirtual 133	com/b/a/a/a/f/d:c	()Lcom/b/a/a/a/h/a;
    //   493: astore_2
    //   494: aload_2
    //   495: invokeinterface 283 1 0
    //   500: goto +127 -> 627
    //   503: aload_0
    //   504: getfield 33	com/inmobi/ads/r:g	Lcom/b/a/a/a/f/d;
    //   507: astore_2
    //   508: aload_2
    //   509: invokevirtual 133	com/b/a/a/a/f/d:c	()Lcom/b/a/a/a/h/a;
    //   512: astore_2
    //   513: aload_2
    //   514: invokeinterface 284 1 0
    //   519: goto +108 -> 627
    //   522: aload_0
    //   523: getfield 33	com/inmobi/ads/r:g	Lcom/b/a/a/a/f/d;
    //   526: astore_2
    //   527: aload_2
    //   528: invokevirtual 133	com/b/a/a/a/f/d:c	()Lcom/b/a/a/a/h/a;
    //   531: astore_2
    //   532: aload_2
    //   533: invokeinterface 287 1 0
    //   538: goto +89 -> 627
    //   541: aload_0
    //   542: getfield 33	com/inmobi/ads/r:g	Lcom/b/a/a/a/f/d;
    //   545: astore_2
    //   546: aload_2
    //   547: invokevirtual 133	com/b/a/a/a/f/d:c	()Lcom/b/a/a/a/h/a;
    //   550: astore_2
    //   551: aload_2
    //   552: invokeinterface 289 1 0
    //   557: aload_0
    //   558: getfield 33	com/inmobi/ads/r:g	Lcom/b/a/a/a/f/d;
    //   561: astore_2
    //   562: aload_2
    //   563: invokevirtual 133	com/b/a/a/a/f/d:c	()Lcom/b/a/a/a/h/a;
    //   566: astore_2
    //   567: aload_2
    //   568: invokeinterface 292 1 0
    //   573: goto +54 -> 627
    //   576: aload_0
    //   577: getfield 33	com/inmobi/ads/r:g	Lcom/b/a/a/a/f/d;
    //   580: astore_2
    //   581: aload_2
    //   582: invokevirtual 133	com/b/a/a/a/f/d:c	()Lcom/b/a/a/a/h/a;
    //   585: astore_2
    //   586: aload_2
    //   587: invokeinterface 295 1 0
    //   592: aload_0
    //   593: getfield 33	com/inmobi/ads/r:g	Lcom/b/a/a/a/f/d;
    //   596: astore_2
    //   597: aload_2
    //   598: invokevirtual 133	com/b/a/a/a/f/d:c	()Lcom/b/a/a/a/h/a;
    //   601: astore_2
    //   602: aload_2
    //   603: invokeinterface 298 1 0
    //   608: goto +19 -> 627
    //   611: aload_0
    //   612: getfield 33	com/inmobi/ads/r:g	Lcom/b/a/a/a/f/d;
    //   615: astore_2
    //   616: aload_2
    //   617: invokevirtual 133	com/b/a/a/a/f/d:c	()Lcom/b/a/a/a/h/a;
    //   620: astore_2
    //   621: aload_2
    //   622: invokeinterface 301 1 0
    //   627: aload_0
    //   628: getfield 31	com/inmobi/ads/r:f	Lcom/inmobi/ads/bw;
    //   631: iload_1
    //   632: invokevirtual 304	com/inmobi/ads/bw:a	(I)V
    //   635: return
    //   636: astore_2
    //   637: goto +33 -> 670
    //   640: astore_2
    //   641: aload_2
    //   642: invokevirtual 153	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   645: pop
    //   646: invokestatic 158	com/inmobi/commons/core/a/a:a	()Lcom/inmobi/commons/core/a/a;
    //   649: astore_3
    //   650: new 160	com/inmobi/commons/core/e/a
    //   653: astore 5
    //   655: aload 5
    //   657: aload_2
    //   658: invokespecial 163	com/inmobi/commons/core/e/a:<init>	(Ljava/lang/Throwable;)V
    //   661: aload_3
    //   662: aload 5
    //   664: invokevirtual 166	com/inmobi/commons/core/a/a:a	(Lcom/inmobi/commons/core/e/a;)V
    //   667: goto -40 -> 627
    //   670: aload_0
    //   671: getfield 31	com/inmobi/ads/r:f	Lcom/inmobi/ads/bw;
    //   674: iload_1
    //   675: invokevirtual 304	com/inmobi/ads/bw:a	(I)V
    //   678: aload_2
    //   679: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	680	0	this	r
    //   0	680	1	paramInt	int
    //   4	618	2	localObject1	Object
    //   636	1	2	localObject2	Object
    //   640	39	2	localException	Exception
    //   127	535	3	localObject3	Object
    //   175	264	4	bool	boolean
    //   178	485	5	locala	com.inmobi.commons.core.e.a
    //   182	94	6	j	int
    //   191	53	7	k	int
    //   194	58	8	localObject4	Object
    // Exception table:
    //   from	to	target	type
    //   0	4	636	finally
    //   5	9	636	finally
    //   14	18	636	finally
    //   19	24	636	finally
    //   115	119	636	finally
    //   120	124	636	finally
    //   129	135	636	finally
    //   138	142	636	finally
    //   145	149	636	finally
    //   150	154	636	finally
    //   155	161	636	finally
    //   164	168	636	finally
    //   169	173	636	finally
    //   199	203	636	finally
    //   210	214	636	finally
    //   216	221	636	finally
    //   223	228	636	finally
    //   230	235	636	finally
    //   243	248	636	finally
    //   251	258	636	finally
    //   258	262	636	finally
    //   267	271	636	finally
    //   287	293	636	finally
    //   296	300	636	finally
    //   301	305	636	finally
    //   306	312	636	finally
    //   312	316	636	finally
    //   317	321	636	finally
    //   322	328	636	finally
    //   331	335	636	finally
    //   336	340	636	finally
    //   341	347	636	finally
    //   350	354	636	finally
    //   355	359	636	finally
    //   360	366	636	finally
    //   369	373	636	finally
    //   374	378	636	finally
    //   379	385	636	finally
    //   388	392	636	finally
    //   393	397	636	finally
    //   398	404	636	finally
    //   407	411	636	finally
    //   412	416	636	finally
    //   417	423	636	finally
    //   423	427	636	finally
    //   428	432	636	finally
    //   445	449	636	finally
    //   450	454	636	finally
    //   455	459	636	finally
    //   461	465	636	finally
    //   468	472	636	finally
    //   473	477	636	finally
    //   478	484	636	finally
    //   484	488	636	finally
    //   489	493	636	finally
    //   494	500	636	finally
    //   503	507	636	finally
    //   508	512	636	finally
    //   513	519	636	finally
    //   522	526	636	finally
    //   527	531	636	finally
    //   532	538	636	finally
    //   541	545	636	finally
    //   546	550	636	finally
    //   551	557	636	finally
    //   557	561	636	finally
    //   562	566	636	finally
    //   567	573	636	finally
    //   576	580	636	finally
    //   581	585	636	finally
    //   586	592	636	finally
    //   592	596	636	finally
    //   597	601	636	finally
    //   602	608	636	finally
    //   611	615	636	finally
    //   616	620	636	finally
    //   621	627	636	finally
    //   641	646	636	finally
    //   646	649	636	finally
    //   650	653	636	finally
    //   657	661	636	finally
    //   662	667	636	finally
    //   0	4	640	java/lang/Exception
    //   5	9	640	java/lang/Exception
    //   14	18	640	java/lang/Exception
    //   19	24	640	java/lang/Exception
    //   115	119	640	java/lang/Exception
    //   120	124	640	java/lang/Exception
    //   129	135	640	java/lang/Exception
    //   138	142	640	java/lang/Exception
    //   145	149	640	java/lang/Exception
    //   150	154	640	java/lang/Exception
    //   155	161	640	java/lang/Exception
    //   164	168	640	java/lang/Exception
    //   169	173	640	java/lang/Exception
    //   199	203	640	java/lang/Exception
    //   210	214	640	java/lang/Exception
    //   216	221	640	java/lang/Exception
    //   223	228	640	java/lang/Exception
    //   230	235	640	java/lang/Exception
    //   243	248	640	java/lang/Exception
    //   251	258	640	java/lang/Exception
    //   258	262	640	java/lang/Exception
    //   267	271	640	java/lang/Exception
    //   287	293	640	java/lang/Exception
    //   296	300	640	java/lang/Exception
    //   301	305	640	java/lang/Exception
    //   306	312	640	java/lang/Exception
    //   312	316	640	java/lang/Exception
    //   317	321	640	java/lang/Exception
    //   322	328	640	java/lang/Exception
    //   331	335	640	java/lang/Exception
    //   336	340	640	java/lang/Exception
    //   341	347	640	java/lang/Exception
    //   350	354	640	java/lang/Exception
    //   355	359	640	java/lang/Exception
    //   360	366	640	java/lang/Exception
    //   369	373	640	java/lang/Exception
    //   374	378	640	java/lang/Exception
    //   379	385	640	java/lang/Exception
    //   388	392	640	java/lang/Exception
    //   393	397	640	java/lang/Exception
    //   398	404	640	java/lang/Exception
    //   407	411	640	java/lang/Exception
    //   412	416	640	java/lang/Exception
    //   417	423	640	java/lang/Exception
    //   423	427	640	java/lang/Exception
    //   428	432	640	java/lang/Exception
    //   445	449	640	java/lang/Exception
    //   450	454	640	java/lang/Exception
    //   455	459	640	java/lang/Exception
    //   461	465	640	java/lang/Exception
    //   468	472	640	java/lang/Exception
    //   473	477	640	java/lang/Exception
    //   478	484	640	java/lang/Exception
    //   484	488	640	java/lang/Exception
    //   489	493	640	java/lang/Exception
    //   494	500	640	java/lang/Exception
    //   503	507	640	java/lang/Exception
    //   508	512	640	java/lang/Exception
    //   513	519	640	java/lang/Exception
    //   522	526	640	java/lang/Exception
    //   527	531	640	java/lang/Exception
    //   532	538	640	java/lang/Exception
    //   541	545	640	java/lang/Exception
    //   546	550	640	java/lang/Exception
    //   551	557	640	java/lang/Exception
    //   557	561	640	java/lang/Exception
    //   562	566	640	java/lang/Exception
    //   567	573	640	java/lang/Exception
    //   576	580	640	java/lang/Exception
    //   581	585	640	java/lang/Exception
    //   586	592	640	java/lang/Exception
    //   592	596	640	java/lang/Exception
    //   597	601	640	java/lang/Exception
    //   602	608	640	java/lang/Exception
    //   611	615	640	java/lang/Exception
    //   616	620	640	java/lang/Exception
    //   621	627	640	java/lang/Exception
  }
  
  public final void a(Context paramContext, int paramInt)
  {
    f.a(paramContext, paramInt);
  }
  
  /* Error */
  public final void a(View... paramVarArgs)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 31	com/inmobi/ads/r:f	Lcom/inmobi/ads/bw;
    //   4: astore_2
    //   5: aload_2
    //   6: invokevirtual 310	com/inmobi/ads/bw:c	()Lcom/inmobi/ads/b;
    //   9: astore_2
    //   10: aload_2
    //   11: getfield 315	com/inmobi/ads/b:o	Lcom/inmobi/ads/b$k;
    //   14: astore_2
    //   15: aload_2
    //   16: getfield 320	com/inmobi/ads/b$k:j	Z
    //   19: istore_3
    //   20: iload_3
    //   21: ifeq +67 -> 88
    //   24: aload_0
    //   25: invokespecial 247	com/inmobi/ads/r:g	()V
    //   28: aload_0
    //   29: getfield 33	com/inmobi/ads/r:g	Lcom/b/a/a/a/f/d;
    //   32: astore_2
    //   33: aload_2
    //   34: invokevirtual 323	com/b/a/a/a/f/d:b	()Lcom/b/a/a/a/c/a;
    //   37: astore_2
    //   38: aload_2
    //   39: ifnull +19 -> 58
    //   42: aload_0
    //   43: getfield 33	com/inmobi/ads/r:g	Lcom/b/a/a/a/f/d;
    //   46: astore_2
    //   47: aload_2
    //   48: invokevirtual 323	com/b/a/a/a/f/d:b	()Lcom/b/a/a/a/c/a;
    //   51: astore_2
    //   52: aload_2
    //   53: invokeinterface 328 1 0
    //   58: aload_0
    //   59: getfield 33	com/inmobi/ads/r:g	Lcom/b/a/a/a/f/d;
    //   62: astore_2
    //   63: aload_2
    //   64: invokevirtual 133	com/b/a/a/a/f/d:c	()Lcom/b/a/a/a/h/a;
    //   67: astore_2
    //   68: aload_2
    //   69: ifnull +19 -> 88
    //   72: aload_0
    //   73: getfield 33	com/inmobi/ads/r:g	Lcom/b/a/a/a/f/d;
    //   76: astore_2
    //   77: aload_2
    //   78: invokevirtual 133	com/b/a/a/a/f/d:c	()Lcom/b/a/a/a/h/a;
    //   81: astore_2
    //   82: aload_2
    //   83: invokeinterface 330 1 0
    //   88: aload_0
    //   89: getfield 31	com/inmobi/ads/r:f	Lcom/inmobi/ads/bw;
    //   92: aload_1
    //   93: invokevirtual 333	com/inmobi/ads/bw:a	([Landroid/view/View;)V
    //   96: return
    //   97: astore_2
    //   98: goto +35 -> 133
    //   101: astore_2
    //   102: aload_2
    //   103: invokevirtual 153	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   106: pop
    //   107: invokestatic 158	com/inmobi/commons/core/a/a:a	()Lcom/inmobi/commons/core/a/a;
    //   110: astore 4
    //   112: new 160	com/inmobi/commons/core/e/a
    //   115: astore 5
    //   117: aload 5
    //   119: aload_2
    //   120: invokespecial 163	com/inmobi/commons/core/e/a:<init>	(Ljava/lang/Throwable;)V
    //   123: aload 4
    //   125: aload 5
    //   127: invokevirtual 166	com/inmobi/commons/core/a/a:a	(Lcom/inmobi/commons/core/e/a;)V
    //   130: goto -42 -> 88
    //   133: aload_0
    //   134: getfield 31	com/inmobi/ads/r:f	Lcom/inmobi/ads/bw;
    //   137: aload_1
    //   138: invokevirtual 333	com/inmobi/ads/bw:a	([Landroid/view/View;)V
    //   141: aload_2
    //   142: athrow
    //   143: pop
    //   144: goto -86 -> 58
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	147	0	this	r
    //   0	147	1	paramVarArgs	View[]
    //   4	79	2	localObject1	Object
    //   97	1	2	localObject2	Object
    //   101	41	2	localException1	Exception
    //   19	2	3	bool	boolean
    //   110	14	4	locala	com.inmobi.commons.core.a.a
    //   115	11	5	locala1	com.inmobi.commons.core.e.a
    //   143	1	8	localException2	Exception
    // Exception table:
    //   from	to	target	type
    //   0	4	97	finally
    //   5	9	97	finally
    //   10	14	97	finally
    //   15	19	97	finally
    //   24	28	97	finally
    //   28	32	97	finally
    //   33	37	97	finally
    //   42	46	97	finally
    //   47	51	97	finally
    //   52	58	97	finally
    //   58	62	97	finally
    //   63	67	97	finally
    //   72	76	97	finally
    //   77	81	97	finally
    //   82	88	97	finally
    //   102	107	97	finally
    //   107	110	97	finally
    //   112	115	97	finally
    //   119	123	97	finally
    //   125	130	97	finally
    //   0	4	101	java/lang/Exception
    //   5	9	101	java/lang/Exception
    //   10	14	101	java/lang/Exception
    //   15	19	101	java/lang/Exception
    //   24	28	101	java/lang/Exception
    //   58	62	101	java/lang/Exception
    //   63	67	101	java/lang/Exception
    //   72	76	101	java/lang/Exception
    //   77	81	101	java/lang/Exception
    //   82	88	101	java/lang/Exception
    //   28	32	143	java/lang/Exception
    //   33	37	143	java/lang/Exception
    //   42	46	143	java/lang/Exception
    //   47	51	143	java/lang/Exception
    //   52	58	143	java/lang/Exception
  }
  
  public final View b()
  {
    return f.b();
  }
  
  final b c()
  {
    return f.c();
  }
  
  /* Error */
  public final void d()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 174	com/inmobi/ads/bw:a	Lcom/inmobi/ads/AdContainer;
    //   4: astore_1
    //   5: aload_1
    //   6: checkcast 176	com/inmobi/ads/ba
    //   9: astore_1
    //   10: aload_1
    //   11: invokevirtual 335	com/inmobi/ads/ba:i	()Z
    //   14: istore_2
    //   15: iload_2
    //   16: ifne +61 -> 77
    //   19: aload_0
    //   20: getfield 33	com/inmobi/ads/r:g	Lcom/b/a/a/a/f/d;
    //   23: astore_1
    //   24: aload_0
    //   25: getfield 186	com/inmobi/ads/r:i	Ljava/lang/ref/WeakReference;
    //   28: astore_3
    //   29: aload_3
    //   30: ifnonnull +8 -> 38
    //   33: aconst_null
    //   34: astore_3
    //   35: goto +18 -> 53
    //   38: aload_0
    //   39: getfield 186	com/inmobi/ads/r:i	Ljava/lang/ref/WeakReference;
    //   42: astore_3
    //   43: aload_3
    //   44: invokevirtual 169	java/lang/ref/WeakReference:get	()Ljava/lang/Object;
    //   47: astore_3
    //   48: aload_3
    //   49: checkcast 203	android/view/View
    //   52: astore_3
    //   53: aload_1
    //   54: aload_3
    //   55: invokevirtual 279	com/b/a/a/a/f/d:a	(Landroid/view/View;)V
    //   58: aload_0
    //   59: getfield 33	com/inmobi/ads/r:g	Lcom/b/a/a/a/f/d;
    //   62: astore_1
    //   63: aload_1
    //   64: invokevirtual 337	com/b/a/a/a/f/d:a	()V
    //   67: aload_0
    //   68: getfield 33	com/inmobi/ads/r:g	Lcom/b/a/a/a/f/d;
    //   71: astore_1
    //   72: aload_1
    //   73: invokevirtual 136	java/lang/Object:hashCode	()I
    //   76: pop
    //   77: aload_0
    //   78: getfield 29	com/inmobi/ads/r:e	Ljava/lang/ref/WeakReference;
    //   81: astore_1
    //   82: aload_1
    //   83: invokevirtual 169	java/lang/ref/WeakReference:get	()Ljava/lang/Object;
    //   86: astore_1
    //   87: aload_1
    //   88: checkcast 72	android/app/Activity
    //   91: astore_1
    //   92: aload_1
    //   93: ifnull +27 -> 120
    //   96: aload_0
    //   97: getfield 212	com/inmobi/ads/r:h	Lcom/inmobi/ads/r$a;
    //   100: astore_3
    //   101: aload_3
    //   102: ifnull +18 -> 120
    //   105: aload_1
    //   106: invokevirtual 225	android/app/Activity:getContentResolver	()Landroid/content/ContentResolver;
    //   109: astore_1
    //   110: aload_0
    //   111: getfield 212	com/inmobi/ads/r:h	Lcom/inmobi/ads/r$a;
    //   114: astore_3
    //   115: aload_1
    //   116: aload_3
    //   117: invokevirtual 341	android/content/ContentResolver:unregisterContentObserver	(Landroid/database/ContentObserver;)V
    //   120: aload_0
    //   121: getfield 31	com/inmobi/ads/r:f	Lcom/inmobi/ads/bw;
    //   124: invokevirtual 342	com/inmobi/ads/bw:d	()V
    //   127: return
    //   128: astore_1
    //   129: goto +33 -> 162
    //   132: astore_1
    //   133: aload_1
    //   134: invokevirtual 153	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   137: pop
    //   138: invokestatic 158	com/inmobi/commons/core/a/a:a	()Lcom/inmobi/commons/core/a/a;
    //   141: astore_3
    //   142: new 160	com/inmobi/commons/core/e/a
    //   145: astore 4
    //   147: aload 4
    //   149: aload_1
    //   150: invokespecial 163	com/inmobi/commons/core/e/a:<init>	(Ljava/lang/Throwable;)V
    //   153: aload_3
    //   154: aload 4
    //   156: invokevirtual 166	com/inmobi/commons/core/a/a:a	(Lcom/inmobi/commons/core/e/a;)V
    //   159: goto -39 -> 120
    //   162: aload_0
    //   163: getfield 31	com/inmobi/ads/r:f	Lcom/inmobi/ads/bw;
    //   166: invokevirtual 342	com/inmobi/ads/bw:d	()V
    //   169: aload_1
    //   170: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	171	0	this	r
    //   4	112	1	localObject1	Object
    //   128	1	1	localObject2	Object
    //   132	38	1	localException	Exception
    //   14	2	2	bool	boolean
    //   28	126	3	localObject3	Object
    //   145	10	4	locala	com.inmobi.commons.core.e.a
    // Exception table:
    //   from	to	target	type
    //   0	4	128	finally
    //   5	9	128	finally
    //   10	14	128	finally
    //   19	23	128	finally
    //   24	28	128	finally
    //   38	42	128	finally
    //   43	47	128	finally
    //   48	52	128	finally
    //   54	58	128	finally
    //   58	62	128	finally
    //   63	67	128	finally
    //   67	71	128	finally
    //   72	77	128	finally
    //   77	81	128	finally
    //   82	86	128	finally
    //   87	91	128	finally
    //   96	100	128	finally
    //   105	109	128	finally
    //   110	114	128	finally
    //   116	120	128	finally
    //   133	138	128	finally
    //   138	141	128	finally
    //   142	145	128	finally
    //   149	153	128	finally
    //   154	159	128	finally
    //   0	4	132	java/lang/Exception
    //   5	9	132	java/lang/Exception
    //   10	14	132	java/lang/Exception
    //   19	23	132	java/lang/Exception
    //   24	28	132	java/lang/Exception
    //   38	42	132	java/lang/Exception
    //   43	47	132	java/lang/Exception
    //   48	52	132	java/lang/Exception
    //   54	58	132	java/lang/Exception
    //   58	62	132	java/lang/Exception
    //   63	67	132	java/lang/Exception
    //   67	71	132	java/lang/Exception
    //   72	77	132	java/lang/Exception
    //   77	81	132	java/lang/Exception
    //   82	86	132	java/lang/Exception
    //   87	91	132	java/lang/Exception
    //   96	100	132	java/lang/Exception
    //   105	109	132	java/lang/Exception
    //   110	114	132	java/lang/Exception
    //   116	120	132	java/lang/Exception
  }
  
  /* Error */
  public final void e()
  {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial 343	com/inmobi/ads/bv:e	()V
    //   4: aload_0
    //   5: getfield 29	com/inmobi/ads/r:e	Ljava/lang/ref/WeakReference;
    //   8: astore_1
    //   9: aload_1
    //   10: invokevirtual 346	java/lang/ref/WeakReference:clear	()V
    //   13: aload_0
    //   14: getfield 186	com/inmobi/ads/r:i	Ljava/lang/ref/WeakReference;
    //   17: astore_1
    //   18: aload_1
    //   19: ifnull +12 -> 31
    //   22: aload_0
    //   23: getfield 186	com/inmobi/ads/r:i	Ljava/lang/ref/WeakReference;
    //   26: astore_1
    //   27: aload_1
    //   28: invokevirtual 346	java/lang/ref/WeakReference:clear	()V
    //   31: aconst_null
    //   32: astore_1
    //   33: aload_0
    //   34: aconst_null
    //   35: putfield 212	com/inmobi/ads/r:h	Lcom/inmobi/ads/r$a;
    //   38: aload_0
    //   39: getfield 31	com/inmobi/ads/r:f	Lcom/inmobi/ads/bw;
    //   42: invokevirtual 347	com/inmobi/ads/bw:e	()V
    //   45: return
    //   46: astore_1
    //   47: goto +30 -> 77
    //   50: astore_1
    //   51: aload_1
    //   52: invokevirtual 153	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   55: pop
    //   56: invokestatic 158	com/inmobi/commons/core/a/a:a	()Lcom/inmobi/commons/core/a/a;
    //   59: astore_2
    //   60: new 160	com/inmobi/commons/core/e/a
    //   63: astore_3
    //   64: aload_3
    //   65: aload_1
    //   66: invokespecial 163	com/inmobi/commons/core/e/a:<init>	(Ljava/lang/Throwable;)V
    //   69: aload_2
    //   70: aload_3
    //   71: invokevirtual 166	com/inmobi/commons/core/a/a:a	(Lcom/inmobi/commons/core/e/a;)V
    //   74: goto -36 -> 38
    //   77: aload_0
    //   78: getfield 31	com/inmobi/ads/r:f	Lcom/inmobi/ads/bw;
    //   81: invokevirtual 347	com/inmobi/ads/bw:e	()V
    //   84: aload_1
    //   85: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	86	0	this	r
    //   8	25	1	localWeakReference	WeakReference
    //   46	1	1	localObject	Object
    //   50	35	1	localException	Exception
    //   59	11	2	locala	com.inmobi.commons.core.a.a
    //   63	8	3	locala1	com.inmobi.commons.core.e.a
    // Exception table:
    //   from	to	target	type
    //   4	8	46	finally
    //   9	13	46	finally
    //   13	17	46	finally
    //   22	26	46	finally
    //   27	31	46	finally
    //   34	38	46	finally
    //   51	56	46	finally
    //   56	59	46	finally
    //   60	63	46	finally
    //   65	69	46	finally
    //   70	74	46	finally
    //   4	8	50	java/lang/Exception
    //   9	13	50	java/lang/Exception
    //   13	17	50	java/lang/Exception
    //   22	26	50	java/lang/Exception
    //   27	31	50	java/lang/Exception
    //   34	38	50	java/lang/Exception
  }
  
  public final bw.a f()
  {
    return f.f();
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */