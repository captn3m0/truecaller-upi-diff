package com.inmobi.ads;

import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;

final class j$1
  implements Runnable
{
  j$1(j paramj, long paramLong, InMobiAdRequestStatus paramInMobiAdRequestStatus) {}
  
  public final void run()
  {
    try
    {
      long l1 = a;
      localObject1 = c;
      long l2 = b;
      boolean bool1 = l1 < l2;
      if (!bool1)
      {
        Object localObject2 = c;
        localObject3 = c;
        localObject3 = ((j)localObject3).f();
        localObject1 = "ARN";
        Object localObject4 = "";
        ((j)localObject2).a((j.b)localObject3, (String)localObject1, (String)localObject4);
        localObject2 = Logger.InternalLogLevel.DEBUG;
        localObject3 = "InMobi";
        localObject1 = new java/lang/StringBuilder;
        localObject4 = "Failed to fetch ad for placement id: ";
        ((StringBuilder)localObject1).<init>((String)localObject4);
        localObject4 = c;
        long l3 = j.a((j)localObject4);
        ((StringBuilder)localObject1).append(l3);
        localObject4 = ", reason phrase available in onAdLoadFailed callback.";
        ((StringBuilder)localObject1).append((String)localObject4);
        localObject1 = ((StringBuilder)localObject1).toString();
        Logger.a((Logger.InternalLogLevel)localObject2, (String)localObject3, (String)localObject1);
        localObject2 = c;
        localObject3 = b;
        boolean bool2 = true;
        ((j)localObject2).a((InMobiAdRequestStatus)localObject3, bool2);
      }
      return;
    }
    catch (Exception localException)
    {
      Logger.a(Logger.InternalLogLevel.ERROR, "[InMobi]", "Unable to load Ad; SDK encountered an unexpected error");
      j.H();
      localException.getMessage();
      Object localObject3 = com.inmobi.commons.core.a.a.a();
      Object localObject1 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject1).<init>(localException);
      ((com.inmobi.commons.core.a.a)localObject3).a((com.inmobi.commons.core.e.a)localObject1);
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.j.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */