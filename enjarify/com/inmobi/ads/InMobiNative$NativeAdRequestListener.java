package com.inmobi.ads;

public abstract interface InMobiNative$NativeAdRequestListener
{
  public abstract void onAdRequestCompleted(InMobiAdRequestStatus paramInMobiAdRequestStatus, InMobiNative paramInMobiNative);
}

/* Location:
 * Qualified Name:     com.inmobi.ads.InMobiNative.NativeAdRequestListener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */