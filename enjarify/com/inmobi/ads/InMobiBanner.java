package com.inmobi.ads;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.inmobi.commons.a.a;
import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;
import com.inmobi.commons.core.utilities.b.c;
import com.inmobi.rendering.RenderView;
import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONObject;

public final class InMobiBanner
  extends RelativeLayout
{
  private static final String DEBUG_LOG_TAG = "InMobi";
  private static final String TAG = "InMobiBanner";
  private static ConcurrentHashMap prefetchAdUnitMap;
  private WeakReference mActivityRef;
  private long mAdLoadCalledTimestamp;
  private InMobiBanner.AnimationType mAnimationType;
  private n mBackgroundBannerAdUnit;
  private final j.b mBannerAdListener;
  private n mBannerAdUnit1;
  private n mBannerAdUnit2;
  private int mBannerHeightInDp;
  private int mBannerWidthInDp;
  private InMobiBanner.b mClientCallbackHandler;
  private InMobiBanner.BannerAdListener mClientListener;
  private n mForegroundBannerAdUnit;
  private boolean mIsAutoRefreshEnabled;
  private boolean mIsInitialized = false;
  private o mRefreshHandler;
  private int mRefreshInterval;
  
  static
  {
    ConcurrentHashMap localConcurrentHashMap = new java/util/concurrent/ConcurrentHashMap;
    localConcurrentHashMap.<init>(5, 0.9F, 3);
    prefetchAdUnitMap = localConcurrentHashMap;
  }
  
  public InMobiBanner(Activity paramActivity, long paramLong)
  {
    super(paramActivity);
    boolean bool1 = true;
    mIsAutoRefreshEnabled = bool1;
    mBannerWidthInDp = 0;
    mBannerHeightInDp = 0;
    Object localObject = InMobiBanner.AnimationType.ROTATE_HORIZONTAL_AXIS;
    mAnimationType = ((InMobiBanner.AnimationType)localObject);
    long l = 0L;
    mAdLoadCalledTimestamp = l;
    localObject = new com/inmobi/ads/InMobiBanner$4;
    ((InMobiBanner.4)localObject).<init>(this);
    mBannerAdListener = ((j.b)localObject);
    boolean bool2 = a.a();
    String str;
    if (!bool2)
    {
      paramActivity = Logger.InternalLogLevel.ERROR;
      str = TAG;
      Logger.a(paramActivity, str, "Please initialize the SDK before creating a Banner ad");
      return;
    }
    if (paramActivity == null)
    {
      paramActivity = Logger.InternalLogLevel.ERROR;
      str = TAG;
      Logger.a(paramActivity, str, "Unable to create Banner ad with null Activity object.");
      return;
    }
    localObject = new java/lang/ref/WeakReference;
    ((WeakReference)localObject).<init>(paramActivity);
    mActivityRef = ((WeakReference)localObject);
    localObject = new com/inmobi/ads/InMobiBanner$b;
    ((InMobiBanner.b)localObject).<init>(this);
    mClientCallbackHandler = ((InMobiBanner.b)localObject);
    initializeAdUnit(paramActivity, paramLong);
  }
  
  public InMobiBanner(Activity paramActivity, AttributeSet paramAttributeSet)
  {
    super(paramActivity, paramAttributeSet);
    boolean bool1 = true;
    mIsAutoRefreshEnabled = bool1;
    mBannerWidthInDp = 0;
    mBannerHeightInDp = 0;
    Object localObject = InMobiBanner.AnimationType.ROTATE_HORIZONTAL_AXIS;
    mAnimationType = ((InMobiBanner.AnimationType)localObject);
    long l = 0L;
    mAdLoadCalledTimestamp = l;
    localObject = new com/inmobi/ads/InMobiBanner$4;
    ((InMobiBanner.4)localObject).<init>(this);
    mBannerAdListener = ((j.b)localObject);
    boolean bool2 = a.a();
    if (!bool2)
    {
      paramActivity = Logger.InternalLogLevel.ERROR;
      paramAttributeSet = TAG;
      Logger.a(paramActivity, paramAttributeSet, "Please initialize the SDK before creating a Banner ad");
      return;
    }
    if (paramActivity == null)
    {
      paramActivity = Logger.InternalLogLevel.ERROR;
      paramAttributeSet = TAG;
      Logger.a(paramActivity, paramAttributeSet, "Unable to create Banner ad with null Activity object.");
      return;
    }
    localObject = new java/lang/ref/WeakReference;
    ((WeakReference)localObject).<init>(paramActivity);
    mActivityRef = ((WeakReference)localObject);
    localObject = new com/inmobi/ads/InMobiBanner$b;
    ((InMobiBanner.b)localObject).<init>(this);
    mClientCallbackHandler = ((InMobiBanner.b)localObject);
    localObject = paramAttributeSet.getAttributeValue("http://schemas.android.com/apk/lib/com.inmobi.ads", "placementId");
    String str1 = "http://schemas.android.com/apk/lib/com.inmobi.ads";
    String str2 = "refreshInterval";
    paramAttributeSet = paramAttributeSet.getAttributeValue(str1, str2);
    if (localObject != null)
    {
      try
      {
        localObject = ((String)localObject).trim();
        l = Long.parseLong((String)localObject);
        initializeAdUnit(paramActivity, l);
      }
      catch (NumberFormatException localNumberFormatException1)
      {
        paramActivity = Logger.InternalLogLevel.ERROR;
        localObject = TAG;
        str1 = "Placement id value supplied in XML layout is not valid. Banner creation failed.";
        Logger.a(paramActivity, (String)localObject, str1);
      }
    }
    else
    {
      paramActivity = Logger.InternalLogLevel.ERROR;
      localObject = TAG;
      str1 = "Placement id value is not supplied in XML layout. Banner creation failed.";
      Logger.a(paramActivity, (String)localObject, str1);
    }
    if (paramAttributeSet != null) {
      try
      {
        paramActivity = paramAttributeSet.trim();
        int i = Integer.parseInt(paramActivity);
        setRefreshInterval(i);
        return;
      }
      catch (NumberFormatException localNumberFormatException2)
      {
        paramActivity = Logger.InternalLogLevel.ERROR;
        paramAttributeSet = TAG;
        localObject = "Refresh interval value supplied in XML layout is not valid. Falling back to default value.";
        Logger.a(paramActivity, paramAttributeSet, (String)localObject);
      }
    }
  }
  
  public InMobiBanner(Context paramContext, long paramLong)
  {
    super(paramContext);
    boolean bool1 = true;
    mIsAutoRefreshEnabled = bool1;
    mBannerWidthInDp = 0;
    mBannerHeightInDp = 0;
    Object localObject = InMobiBanner.AnimationType.ROTATE_HORIZONTAL_AXIS;
    mAnimationType = ((InMobiBanner.AnimationType)localObject);
    long l = 0L;
    mAdLoadCalledTimestamp = l;
    localObject = new com/inmobi/ads/InMobiBanner$4;
    ((InMobiBanner.4)localObject).<init>(this);
    mBannerAdListener = ((j.b)localObject);
    boolean bool2 = a.a();
    String str;
    if (!bool2)
    {
      paramContext = Logger.InternalLogLevel.ERROR;
      str = TAG;
      Logger.a(paramContext, str, "Please initialize the SDK before creating a Banner ad");
      return;
    }
    if (paramContext == null)
    {
      paramContext = Logger.InternalLogLevel.ERROR;
      str = TAG;
      Logger.a(paramContext, str, "Unable to create InMobiBanner ad with null context object.");
      return;
    }
    localObject = new com/inmobi/ads/InMobiBanner$b;
    ((InMobiBanner.b)localObject).<init>(this);
    mClientCallbackHandler = ((InMobiBanner.b)localObject);
    initializeAdUnit(paramContext, paramLong);
  }
  
  public InMobiBanner(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    boolean bool1 = true;
    mIsAutoRefreshEnabled = bool1;
    mBannerWidthInDp = 0;
    mBannerHeightInDp = 0;
    Object localObject = InMobiBanner.AnimationType.ROTATE_HORIZONTAL_AXIS;
    mAnimationType = ((InMobiBanner.AnimationType)localObject);
    long l = 0L;
    mAdLoadCalledTimestamp = l;
    localObject = new com/inmobi/ads/InMobiBanner$4;
    ((InMobiBanner.4)localObject).<init>(this);
    mBannerAdListener = ((j.b)localObject);
    boolean bool2 = a.a();
    if (!bool2)
    {
      paramContext = Logger.InternalLogLevel.ERROR;
      paramAttributeSet = TAG;
      Logger.a(paramContext, paramAttributeSet, "Please initialize the SDK before creating a Banner ad");
      return;
    }
    localObject = new com/inmobi/ads/InMobiBanner$b;
    ((InMobiBanner.b)localObject).<init>(this);
    mClientCallbackHandler = ((InMobiBanner.b)localObject);
    localObject = paramAttributeSet.getAttributeValue("http://schemas.android.com/apk/lib/com.inmobi.ads", "placementId");
    String str1 = "http://schemas.android.com/apk/lib/com.inmobi.ads";
    String str2 = "refreshInterval";
    paramAttributeSet = paramAttributeSet.getAttributeValue(str1, str2);
    if (localObject != null)
    {
      try
      {
        localObject = ((String)localObject).trim();
        l = Long.parseLong((String)localObject);
        initializeAdUnit(paramContext, l);
      }
      catch (NumberFormatException localNumberFormatException1)
      {
        paramContext = Logger.InternalLogLevel.ERROR;
        localObject = TAG;
        str1 = "Placement id value supplied in XML layout is not valid. Banner creation failed.";
        Logger.a(paramContext, (String)localObject, str1);
      }
    }
    else
    {
      paramContext = Logger.InternalLogLevel.ERROR;
      localObject = TAG;
      str1 = "Placement id value is not supplied in XML layout. Banner creation failed.";
      Logger.a(paramContext, (String)localObject, str1);
    }
    if (paramAttributeSet != null) {
      try
      {
        paramContext = paramAttributeSet.trim();
        int i = Integer.parseInt(paramContext);
        setRefreshInterval(i);
        return;
      }
      catch (NumberFormatException localNumberFormatException2)
      {
        paramContext = Logger.InternalLogLevel.ERROR;
        paramAttributeSet = TAG;
        localObject = "Refresh interval value supplied in XML layout is not valid. Falling back to default value.";
        Logger.a(paramContext, paramAttributeSet, (String)localObject);
      }
    }
  }
  
  private void cancelScheduledRefresh()
  {
    mRefreshHandler.removeMessages(1);
  }
  
  private boolean checkForRefreshRate()
  {
    long l1 = mAdLoadCalledTimestamp;
    long l2 = 0L;
    boolean bool1 = l1 < l2;
    if (bool1)
    {
      Object localObject1 = mBackgroundBannerAdUnit.e;
      int i = g;
      long l3 = SystemClock.elapsedRealtime();
      long l4 = mAdLoadCalledTimestamp;
      l3 -= l4;
      int j = i * 1000;
      l4 = j;
      boolean bool2 = l3 < l4;
      if (bool2)
      {
        Object localObject2 = mBackgroundBannerAdUnit;
        Object localObject3 = new com/inmobi/ads/InMobiAdRequestStatus;
        Object localObject4 = InMobiAdRequestStatus.StatusCode.EARLY_REFRESH_REQUEST;
        ((InMobiAdRequestStatus)localObject3).<init>((InMobiAdRequestStatus.StatusCode)localObject4);
        localObject4 = new java/lang/StringBuilder;
        ((StringBuilder)localObject4).<init>("Ad cannot be refreshed before ");
        ((StringBuilder)localObject4).append(i);
        ((StringBuilder)localObject4).append(" seconds");
        localObject4 = ((StringBuilder)localObject4).toString();
        localObject3 = ((InMobiAdRequestStatus)localObject3).setCustomMessage((String)localObject4);
        ((n)localObject2).a((InMobiAdRequestStatus)localObject3, false);
        localObject2 = Logger.InternalLogLevel.ERROR;
        localObject3 = TAG;
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>("Ad cannot be refreshed before ");
        localStringBuilder.append(i);
        localStringBuilder.append(" seconds (Placement Id = ");
        long l5 = mBackgroundBannerAdUnit.b;
        localStringBuilder.append(l5);
        localStringBuilder.append(")");
        localObject1 = localStringBuilder.toString();
        Logger.a((Logger.InternalLogLevel)localObject2, (String)localObject3, (String)localObject1);
        return false;
      }
    }
    l1 = SystemClock.elapsedRealtime();
    mAdLoadCalledTimestamp = l1;
    return true;
  }
  
  private void displayAd()
  {
    Object localObject1 = getContext();
    if (localObject1 == null) {
      return;
    }
    localObject1 = (RenderView)mForegroundBannerAdUnit.i();
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = ((RenderView)localObject1).getViewableAd();
    Object localObject3 = mForegroundBannerAdUnit;
    boolean bool = x;
    if (bool) {
      ((RenderView)localObject1).a();
    }
    localObject1 = (ViewGroup)((RenderView)localObject1).getParent();
    localObject3 = new android/widget/RelativeLayout$LayoutParams;
    int i = -1;
    ((RelativeLayout.LayoutParams)localObject3).<init>(i, i);
    View localView = ((bw)localObject2).a();
    View[] arrayOfView = new View[0];
    ((bw)localObject2).a(arrayOfView);
    localObject2 = mBackgroundBannerAdUnit;
    if (localObject2 != null) {
      ((n)localObject2).L();
    }
    if (localObject1 == null)
    {
      addView(localView, (ViewGroup.LayoutParams)localObject3);
    }
    else
    {
      ((ViewGroup)localObject1).removeAllViews();
      ((ViewGroup)localObject1).addView(localView, (ViewGroup.LayoutParams)localObject3);
    }
    mBackgroundBannerAdUnit.r();
  }
  
  private void fireTRC(String paramString1, String paramString2)
  {
    n localn = mBackgroundBannerAdUnit;
    if (localn != null)
    {
      j.b localb = mBannerAdListener;
      localn.a(localb, paramString1, paramString2);
    }
  }
  
  private void initializeAdUnit(Activity paramActivity, long paramLong)
  {
    n localn = new com/inmobi/ads/n;
    j.b localb = mBannerAdListener;
    localn.<init>(paramActivity, paramLong, localb);
    mBannerAdUnit1 = localn;
    localn = new com/inmobi/ads/n;
    localb = mBannerAdListener;
    localn.<init>(paramActivity, paramLong, localb);
    mBannerAdUnit2 = localn;
    paramActivity = mBannerAdUnit1;
    mBackgroundBannerAdUnit = paramActivity;
    int i = mBackgroundBannerAdUnit.e.h;
    mRefreshInterval = i;
    paramActivity = new com/inmobi/ads/o;
    paramActivity.<init>(this);
    mRefreshHandler = paramActivity;
    paramActivity = InMobiAdRequest.MonetizationContext.MONETIZATION_CONTEXT_ACTIVITY;
    setMonetizationContext(paramActivity);
    mIsInitialized = true;
  }
  
  private void initializeAdUnit(Context paramContext, long paramLong)
  {
    n localn = new com/inmobi/ads/n;
    j.b localb = mBannerAdListener;
    localn.<init>(paramContext, paramLong, localb);
    mBannerAdUnit1 = localn;
    localn = new com/inmobi/ads/n;
    localb = mBannerAdListener;
    localn.<init>(paramContext, paramLong, localb);
    mBannerAdUnit2 = localn;
    paramContext = mBannerAdUnit1;
    mBackgroundBannerAdUnit = paramContext;
    int i = mBackgroundBannerAdUnit.e.h;
    mRefreshInterval = i;
    paramContext = new com/inmobi/ads/o;
    paramContext.<init>(this);
    mRefreshHandler = paramContext;
    paramContext = InMobiAdRequest.MonetizationContext.MONETIZATION_CONTEXT_OTHER;
    setMonetizationContext(paramContext);
    mIsInitialized = true;
  }
  
  public static void requestAd(Context paramContext, InMobiAdRequest paramInMobiAdRequest, InMobiBanner.BannerAdRequestListener paramBannerAdRequestListener)
  {
    boolean bool = a.a();
    if (!bool)
    {
      paramContext = Logger.InternalLogLevel.ERROR;
      paramInMobiAdRequest = TAG;
      Logger.a(paramContext, paramInMobiAdRequest, "Please initialize the SDK before calling requestAd. Ignoring request");
      return;
    }
    if (paramContext == null)
    {
      paramContext = Logger.InternalLogLevel.ERROR;
      paramInMobiAdRequest = TAG;
      Logger.a(paramContext, paramInMobiAdRequest, "Please supply a non null Context. Aborting request");
      return;
    }
    if (paramInMobiAdRequest == null)
    {
      paramContext = Logger.InternalLogLevel.ERROR;
      paramInMobiAdRequest = TAG;
      Logger.a(paramContext, paramInMobiAdRequest, "Please supply a non  null InMobiAdRequest. Ignoring request");
      return;
    }
    if (paramBannerAdRequestListener == null)
    {
      paramContext = Logger.InternalLogLevel.ERROR;
      paramInMobiAdRequest = TAG;
      Logger.a(paramContext, paramInMobiAdRequest, "Please supply a non null BannerAdRequestListener. Ignoring request");
      return;
    }
    int i = paramInMobiAdRequest.getWidth();
    if (i <= 0)
    {
      i = paramInMobiAdRequest.getHeight();
      if (i <= 0)
      {
        paramContext = Logger.InternalLogLevel.ERROR;
        paramInMobiAdRequest = TAG;
        Logger.a(paramContext, paramInMobiAdRequest, "Please provide positive width and height for banner. Ignoring request");
        return;
      }
    }
    Object localObject = paramInMobiAdRequest.getMonetizationContext();
    if (localObject == null)
    {
      paramContext = Logger.InternalLogLevel.ERROR;
      paramInMobiAdRequest = TAG;
      Logger.a(paramContext, paramInMobiAdRequest, "Please supply a MonetizationContext type. Ignoring request");
      return;
    }
    localObject = new com/inmobi/ads/InMobiBanner$2;
    ((InMobiBanner.2)localObject).<init>();
    try
    {
      n localn = new com/inmobi/ads/n;
      paramContext = paramContext.getApplicationContext();
      long l = paramInMobiAdRequest.getPlacementId();
      localn.<init>(paramContext, l, null);
      paramContext = paramInMobiAdRequest.getExtras();
      d = paramContext;
      paramContext = paramInMobiAdRequest.getMonetizationContext();
      localn.a(paramContext);
      paramContext = paramInMobiAdRequest.getKeywords();
      c = paramContext;
      paramContext = new java/lang/StringBuilder;
      paramContext.<init>();
      int j = paramInMobiAdRequest.getWidth();
      paramContext.append(j);
      String str = "x";
      paramContext.append(str);
      int k = paramInMobiAdRequest.getHeight();
      paramContext.append(k);
      paramContext = paramContext.toString();
      y = paramContext;
      p = ((j.d)localObject);
      paramContext = prefetchAdUnitMap;
      paramInMobiAdRequest = new java/lang/ref/WeakReference;
      paramInMobiAdRequest.<init>(paramBannerAdRequestListener);
      paramContext.put(localn, paramInMobiAdRequest);
      localn.n();
      return;
    }
    catch (Exception localException)
    {
      localException.getMessage();
    }
  }
  
  private void scheduleRefresh()
  {
    boolean bool1 = isShown();
    if (bool1)
    {
      bool1 = hasWindowFocus();
      if (bool1)
      {
        Object localObject = mRefreshHandler;
        int j = 1;
        ((o)localObject).removeMessages(j);
        localObject = mBackgroundBannerAdUnit;
        int i = a;
        if (i != j)
        {
          localObject = mBackgroundBannerAdUnit;
          i = a;
          int k = 2;
          if (i != k)
          {
            localObject = mForegroundBannerAdUnit;
            if (localObject != null)
            {
              i = a;
              k = 8;
              if (i == k) {}
            }
            else
            {
              boolean bool2 = mIsAutoRefreshEnabled;
              if (bool2)
              {
                localObject = mRefreshHandler;
                k = mRefreshInterval * 1000;
                long l = k;
                ((o)localObject).sendEmptyMessageDelayed(j, l);
              }
              return;
            }
          }
        }
        return;
      }
    }
  }
  
  private void setMonetizationContext(InMobiAdRequest.MonetizationContext paramMonetizationContext)
  {
    n localn1 = mBannerAdUnit1;
    if (localn1 != null)
    {
      n localn2 = mBannerAdUnit2;
      if (localn2 != null)
      {
        localn1.a(paramMonetizationContext);
        localn1 = mBannerAdUnit2;
        localn1.a(paramMonetizationContext);
      }
    }
  }
  
  private void setSizeFromLayoutParams()
  {
    ViewGroup.LayoutParams localLayoutParams = getLayoutParams();
    if (localLayoutParams != null)
    {
      int i = c.b(getLayoutParamswidth);
      mBannerWidthInDp = i;
      localLayoutParams = getLayoutParams();
      i = c.b(height);
      mBannerHeightInDp = i;
    }
  }
  
  private void swapAdUnitsAndDisplayAd(InMobiBanner.a parama)
  {
    Object localObject1 = mForegroundBannerAdUnit;
    Object localObject2;
    boolean bool;
    if (localObject1 == null)
    {
      localObject1 = mBannerAdUnit1;
      mForegroundBannerAdUnit = ((n)localObject1);
      localObject1 = mBannerAdUnit2;
      mBackgroundBannerAdUnit = ((n)localObject1);
    }
    else
    {
      localObject2 = mBannerAdUnit1;
      bool = localObject1.equals(localObject2);
      if (bool)
      {
        localObject1 = mBannerAdUnit2;
        mForegroundBannerAdUnit = ((n)localObject1);
        localObject1 = mBannerAdUnit1;
        mBackgroundBannerAdUnit = ((n)localObject1);
      }
      else
      {
        localObject1 = mForegroundBannerAdUnit;
        localObject2 = mBannerAdUnit2;
        bool = localObject1.equals(localObject2);
        if (bool)
        {
          localObject1 = mBannerAdUnit1;
          mForegroundBannerAdUnit = ((n)localObject1);
          localObject1 = mBannerAdUnit2;
          mBackgroundBannerAdUnit = ((n)localObject1);
        }
      }
    }
    try
    {
      localObject1 = mAnimationType;
      int i = getWidth();
      float f1 = i;
      int j = getHeight();
      float f2 = j;
      Object localObject3 = null;
      InMobiBanner.AnimationType localAnimationType = InMobiBanner.AnimationType.ANIMATION_ALPHA;
      if (localObject1 == localAnimationType)
      {
        localObject3 = new android/view/animation/AlphaAnimation;
        bool = false;
        localObject1 = null;
        i = 1056964608;
        f1 = 0.5F;
        ((AlphaAnimation)localObject3).<init>(0.0F, f1);
        long l1 = 1000L;
        ((AlphaAnimation)localObject3).setDuration(l1);
        ((AlphaAnimation)localObject3).setFillAfter(false);
        localObject1 = new android/view/animation/DecelerateInterpolator;
        ((DecelerateInterpolator)localObject1).<init>();
        ((AlphaAnimation)localObject3).setInterpolator((Interpolator)localObject1);
      }
      else
      {
        localAnimationType = InMobiBanner.AnimationType.ROTATE_HORIZONTAL_AXIS;
        long l2 = 500L;
        float f3 = 2.0F;
        if (localObject1 == localAnimationType)
        {
          localObject3 = new com/inmobi/ads/k$a;
          f1 /= f3;
          f2 /= f3;
          ((k.a)localObject3).<init>(f1, f2);
          ((k.a)localObject3).setDuration(l2);
          ((k.a)localObject3).setFillAfter(false);
          localObject1 = new android/view/animation/AccelerateInterpolator;
          ((AccelerateInterpolator)localObject1).<init>();
          ((k.a)localObject3).setInterpolator((Interpolator)localObject1);
        }
        else
        {
          localAnimationType = InMobiBanner.AnimationType.ROTATE_VERTICAL_AXIS;
          if (localObject1 == localAnimationType)
          {
            localObject3 = new com/inmobi/ads/k$b;
            f1 /= f3;
            f2 /= f3;
            ((k.b)localObject3).<init>(f1, f2);
            ((k.b)localObject3).setDuration(l2);
            ((k.b)localObject3).setFillAfter(false);
            localObject1 = new android/view/animation/AccelerateInterpolator;
            ((AccelerateInterpolator)localObject1).<init>();
            ((k.b)localObject3).setInterpolator((Interpolator)localObject1);
          }
        }
      }
      displayAd();
      if (localObject3 != null) {
        startAnimation((Animation)localObject3);
      }
      parama.a();
      return;
    }
    catch (Exception parama)
    {
      localObject1 = Logger.InternalLogLevel.ERROR;
      localObject2 = TAG;
      Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "Unexpected error while displaying Banner Ad.");
      parama.getMessage();
    }
  }
  
  public final void disableHardwareAcceleration()
  {
    boolean bool = mIsInitialized;
    if (bool)
    {
      mBannerAdUnit1.J();
      n localn = mBannerAdUnit2;
      localn.J();
    }
  }
  
  public final JSONObject getAdMetaInfo()
  {
    boolean bool = mIsInitialized;
    if (bool)
    {
      localObject = mForegroundBannerAdUnit;
      if (localObject != null) {
        return g;
      }
    }
    Object localObject = new org/json/JSONObject;
    ((JSONObject)localObject).<init>();
    return (JSONObject)localObject;
  }
  
  public final String getCreativeId()
  {
    boolean bool = mIsInitialized;
    if (bool)
    {
      n localn = mForegroundBannerAdUnit;
      if (localn != null) {
        return w;
      }
    }
    return "";
  }
  
  final String getFrameSizeString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    int i = mBannerWidthInDp;
    localStringBuilder.append(i);
    localStringBuilder.append("x");
    i = mBannerHeightInDp;
    localStringBuilder.append(i);
    return localStringBuilder.toString();
  }
  
  final boolean hasValidSize()
  {
    int i = mBannerWidthInDp;
    if (i > 0)
    {
      i = mBannerHeightInDp;
      if (i > 0) {
        return true;
      }
    }
    return false;
  }
  
  public final void load()
  {
    load(false);
  }
  
  public final void load(Context paramContext)
  {
    boolean bool = a.a();
    if (!bool)
    {
      paramContext = Logger.InternalLogLevel.ERROR;
      localObject = TAG;
      Logger.a(paramContext, (String)localObject, "InMobiBanner is not initialized. Ignoring InMobiBanner.load()");
      return;
    }
    if (paramContext == null)
    {
      paramContext = Logger.InternalLogLevel.ERROR;
      localObject = TAG;
      Logger.a(paramContext, (String)localObject, "Context is null, InMobiBanner cannot be loaded.");
      return;
    }
    bool = mIsInitialized;
    if (!bool)
    {
      paramContext = Logger.InternalLogLevel.ERROR;
      localObject = TAG;
      Logger.a(paramContext, (String)localObject, "InMobiBanner is not initialized. Ignoring InMobiBanner.load()");
      return;
    }
    mBannerAdUnit1.a(paramContext);
    Object localObject = mBannerAdUnit2;
    ((n)localObject).a(paramContext);
    bool = paramContext instanceof Activity;
    if (bool)
    {
      localObject = new java/lang/ref/WeakReference;
      paramContext = (Activity)paramContext;
      ((WeakReference)localObject).<init>(paramContext);
      mActivityRef = ((WeakReference)localObject);
      paramContext = InMobiAdRequest.MonetizationContext.MONETIZATION_CONTEXT_ACTIVITY;
      setMonetizationContext(paramContext);
    }
    else
    {
      paramContext = InMobiAdRequest.MonetizationContext.MONETIZATION_CONTEXT_OTHER;
      setMonetizationContext(paramContext);
    }
    load();
  }
  
  final void load(boolean paramBoolean)
  {
    try
    {
      boolean bool1 = a.a();
      Object localObject1;
      if (!bool1)
      {
        localObject1 = Logger.InternalLogLevel.ERROR;
        localObject2 = TAG;
        localObject3 = "InMobiBanner is not initialized. Ignoring InMobiBanner.load()";
        Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, (String)localObject3);
        return;
      }
      bool1 = mIsInitialized;
      if (bool1)
      {
        localObject2 = "ARR";
        localObject3 = "";
        fireTRC((String)localObject2, (String)localObject3);
        localObject2 = mForegroundBannerAdUnit;
        if (localObject2 != null)
        {
          localObject2 = mForegroundBannerAdUnit;
          bool1 = ((n)localObject2).K();
          if (bool1)
          {
            localObject1 = Message.obtain();
            int i = 2;
            what = i;
            localObject2 = new com/inmobi/ads/InMobiAdRequestStatus;
            localObject3 = InMobiAdRequestStatus.StatusCode.AD_ACTIVE;
            ((InMobiAdRequestStatus)localObject2).<init>((InMobiAdRequestStatus.StatusCode)localObject3);
            obj = localObject2;
            localObject2 = "ART";
            localObject3 = "LoadInProgress";
            fireTRC((String)localObject2, (String)localObject3);
            localObject2 = mClientCallbackHandler;
            ((InMobiBanner.b)localObject2).sendMessage((Message)localObject1);
            localObject1 = mForegroundBannerAdUnit;
            localObject2 = "AdActive";
            ((n)localObject1).b((String)localObject2);
            localObject1 = Logger.InternalLogLevel.ERROR;
            localObject2 = TAG;
            localObject3 = "An ad is currently being viewed by the user. Please wait for the user to close the ad before requesting for another ad.";
            Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, (String)localObject3);
            return;
          }
        }
        boolean bool2 = hasValidSize();
        if (!bool2)
        {
          localObject2 = getLayoutParams();
          if (localObject2 == null)
          {
            localObject1 = Logger.InternalLogLevel.ERROR;
            localObject2 = TAG;
            localObject3 = "The layout params of the banner must be set before calling load";
            Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, (String)localObject3);
            localObject1 = Logger.InternalLogLevel.ERROR;
            localObject2 = TAG;
            localObject3 = "or call setBannerSize(int widthInDp, int heightInDp) before load";
            Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, (String)localObject3);
            localObject1 = mBannerAdListener;
            localObject2 = new com/inmobi/ads/InMobiAdRequestStatus;
            localObject3 = InMobiAdRequestStatus.StatusCode.REQUEST_INVALID;
            ((InMobiAdRequestStatus)localObject2).<init>((InMobiAdRequestStatus.StatusCode)localObject3);
            ((j.b)localObject1).a((InMobiAdRequestStatus)localObject2);
            return;
          }
          localObject2 = getLayoutParams();
          int j = width;
          int k = -2;
          if (j != k)
          {
            localObject2 = getLayoutParams();
            j = height;
            if (j != k)
            {
              setSizeFromLayoutParams();
              break label409;
            }
          }
          localObject1 = Logger.InternalLogLevel.ERROR;
          localObject2 = TAG;
          localObject3 = "The height or width of a Banner ad can't be WRAP_CONTENT";
          Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, (String)localObject3);
          localObject1 = Logger.InternalLogLevel.ERROR;
          localObject2 = TAG;
          localObject3 = "or call setBannerSize(int widthInDp, int heightInDp) before load";
          Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, (String)localObject3);
          localObject1 = mBannerAdListener;
          localObject2 = new com/inmobi/ads/InMobiAdRequestStatus;
          localObject3 = InMobiAdRequestStatus.StatusCode.REQUEST_INVALID;
          ((InMobiAdRequestStatus)localObject2).<init>((InMobiAdRequestStatus.StatusCode)localObject3);
          ((j.b)localObject1).a((InMobiAdRequestStatus)localObject2);
          return;
        }
        label409:
        boolean bool3 = hasValidSize();
        if (!bool3)
        {
          localObject2 = new android/os/Handler;
          ((Handler)localObject2).<init>();
          localObject3 = new com/inmobi/ads/InMobiBanner$1;
          ((InMobiBanner.1)localObject3).<init>(this, paramBoolean);
          long l = 200L;
          ((Handler)localObject2).postDelayed((Runnable)localObject3, l);
          return;
        }
        cancelScheduledRefresh();
        bool3 = checkForRefreshRate();
        if (bool3)
        {
          localObject2 = mBackgroundBannerAdUnit;
          localObject3 = getFrameSizeString();
          y = ((String)localObject3);
          localObject2 = mBackgroundBannerAdUnit;
          ((n)localObject2).b(paramBoolean);
        }
      }
      else
      {
        localObject1 = Logger.InternalLogLevel.ERROR;
        localObject2 = TAG;
        localObject3 = "InMobiBanner is not initialized. Ignoring InMobiBanner.load()";
        Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, (String)localObject3);
      }
      return;
    }
    catch (Exception localException)
    {
      Object localObject2 = Logger.InternalLogLevel.ERROR;
      Object localObject3 = TAG;
      Logger.a((Logger.InternalLogLevel)localObject2, (String)localObject3, "Unable to load ad; SDK encountered an unexpected error");
      localException.getMessage();
    }
  }
  
  protected final void onAttachedToWindow()
  {
    try
    {
      super.onAttachedToWindow();
      boolean bool = mIsInitialized;
      if (bool)
      {
        setSizeFromLayoutParams();
        bool = hasValidSize();
        if (!bool) {
          setupBannerSizeObserver();
        }
        scheduleRefresh();
      }
      return;
    }
    catch (Exception localException)
    {
      Logger.InternalLogLevel localInternalLogLevel = Logger.InternalLogLevel.ERROR;
      String str = TAG;
      Logger.a(localInternalLogLevel, str, "InMobiBanner#onAttachedToWindow() handler threw unexpected error");
      localException.getMessage();
    }
  }
  
  protected final void onDetachedFromWindow()
  {
    try
    {
      super.onDetachedFromWindow();
      boolean bool = mIsInitialized;
      if (bool) {
        cancelScheduledRefresh();
      }
      n localn = mBannerAdUnit1;
      if (localn != null)
      {
        localn = mBannerAdUnit1;
        localn.N();
      }
      localn = mBannerAdUnit2;
      if (localn != null)
      {
        localn = mBannerAdUnit2;
        localn.N();
      }
      return;
    }
    catch (Exception localException)
    {
      Logger.InternalLogLevel localInternalLogLevel = Logger.InternalLogLevel.ERROR;
      String str = TAG;
      Logger.a(localInternalLogLevel, str, "InMobiBanner.onDetachedFromWindow() handler threw unexpected error");
      localException.getMessage();
    }
  }
  
  protected final void onVisibilityChanged(View paramView, int paramInt)
  {
    try
    {
      super.onVisibilityChanged(paramView, paramInt);
      boolean bool = mIsInitialized;
      if (bool)
      {
        if (paramInt == 0)
        {
          scheduleRefresh();
          return;
        }
        cancelScheduledRefresh();
      }
      return;
    }
    catch (Exception paramView)
    {
      Logger.InternalLogLevel localInternalLogLevel = Logger.InternalLogLevel.ERROR;
      String str = TAG;
      Logger.a(localInternalLogLevel, str, "InMobiBanner$1.onVisibilityChanged() handler threw unexpected error");
      paramView.getMessage();
    }
  }
  
  public final void onWindowFocusChanged(boolean paramBoolean)
  {
    try
    {
      super.onWindowFocusChanged(paramBoolean);
      boolean bool = mIsInitialized;
      if (bool)
      {
        if (paramBoolean)
        {
          scheduleRefresh();
          return;
        }
        cancelScheduledRefresh();
      }
      return;
    }
    catch (Exception localException)
    {
      Logger.InternalLogLevel localInternalLogLevel = Logger.InternalLogLevel.ERROR;
      String str = TAG;
      Logger.a(localInternalLogLevel, str, "InMobiBanner$1.onWindowFocusChanged() handler threw unexpected error");
      localException.getMessage();
    }
  }
  
  public final void pause()
  {
    try
    {
      Object localObject = mForegroundBannerAdUnit;
      if (localObject != null)
      {
        localObject = mActivityRef;
        if (localObject == null)
        {
          localObject = mForegroundBannerAdUnit;
          ((n)localObject).L();
        }
      }
      return;
    }
    catch (Exception localException)
    {
      Logger.a(Logger.InternalLogLevel.ERROR, "InMobi", "Could not pause ad; SDK encountered an unexpected error");
      localException.getMessage();
    }
  }
  
  public final void resume()
  {
    try
    {
      Object localObject = mForegroundBannerAdUnit;
      if (localObject != null)
      {
        localObject = mActivityRef;
        if (localObject == null)
        {
          localObject = mForegroundBannerAdUnit;
          ((n)localObject).M();
        }
      }
      return;
    }
    catch (Exception localException)
    {
      Logger.a(Logger.InternalLogLevel.ERROR, "InMobi", "Could not resume ad; SDK encountered an unexpected error");
      localException.getMessage();
    }
  }
  
  public final void setAnimationType(InMobiBanner.AnimationType paramAnimationType)
  {
    boolean bool = mIsInitialized;
    if (bool) {
      mAnimationType = paramAnimationType;
    }
  }
  
  public final void setBannerSize(int paramInt1, int paramInt2)
  {
    boolean bool = mIsInitialized;
    if (bool)
    {
      mBannerWidthInDp = paramInt1;
      mBannerHeightInDp = paramInt2;
    }
  }
  
  public final void setEnableAutoRefresh(boolean paramBoolean)
  {
    try
    {
      boolean bool = mIsInitialized;
      if (bool)
      {
        bool = mIsAutoRefreshEnabled;
        if (bool == paramBoolean) {
          return;
        }
        mIsAutoRefreshEnabled = paramBoolean;
        paramBoolean = mIsAutoRefreshEnabled;
        if (paramBoolean)
        {
          scheduleRefresh();
          return;
        }
        cancelScheduledRefresh();
      }
      return;
    }
    catch (Exception localException)
    {
      Logger.InternalLogLevel localInternalLogLevel = Logger.InternalLogLevel.ERROR;
      String str = TAG;
      Logger.a(localInternalLogLevel, str, "Unable to setup auto-refresh on the ad; SDK encountered an unexpected error");
      localException.getMessage();
    }
  }
  
  public final void setExtras(Map paramMap)
  {
    boolean bool = mIsInitialized;
    if (bool)
    {
      mBannerAdUnit1.d = paramMap;
      n localn = mBannerAdUnit2;
      d = paramMap;
    }
  }
  
  public final void setKeywords(String paramString)
  {
    boolean bool = mIsInitialized;
    if (bool)
    {
      mBannerAdUnit1.c = paramString;
      n localn = mBannerAdUnit2;
      c = paramString;
    }
  }
  
  public final void setListener(InMobiBanner.BannerAdListener paramBannerAdListener)
  {
    if (paramBannerAdListener == null)
    {
      paramBannerAdListener = Logger.InternalLogLevel.ERROR;
      localObject = TAG;
      Logger.a(paramBannerAdListener, (String)localObject, "Please pass a non-null listener to the banner.");
      return;
    }
    mClientListener = paramBannerAdListener;
    Object localObject = mClientCallbackHandler;
    if (localObject != null)
    {
      WeakReference localWeakReference = new java/lang/ref/WeakReference;
      localWeakReference.<init>(paramBannerAdListener);
      a = localWeakReference;
    }
  }
  
  public final void setRefreshInterval(int paramInt)
  {
    try
    {
      boolean bool = mIsInitialized;
      if (bool)
      {
        localObject1 = mBackgroundBannerAdUnit;
        localObject1 = e;
        int i = g;
        if (paramInt < i)
        {
          Object localObject2 = mBackgroundBannerAdUnit;
          localObject2 = e;
          paramInt = g;
        }
        mRefreshInterval = paramInt;
      }
      return;
    }
    catch (Exception localException)
    {
      Object localObject1 = Logger.InternalLogLevel.ERROR;
      String str = TAG;
      Logger.a((Logger.InternalLogLevel)localObject1, str, "Unable to set refresh interval for the ad; SDK encountered an unexpected error");
      localException.getMessage();
    }
  }
  
  final void setupBannerSizeObserver()
  {
    ViewTreeObserver localViewTreeObserver = getViewTreeObserver();
    InMobiBanner.3 local3 = new com/inmobi/ads/InMobiBanner$3;
    local3.<init>(this);
    localViewTreeObserver.addOnGlobalLayoutListener(local3);
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.InMobiBanner
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */