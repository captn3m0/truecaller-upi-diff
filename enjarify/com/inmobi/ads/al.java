package com.inmobi.ads;

import android.os.Handler;
import android.os.Looper;
import android.support.v4.view.o;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;

final class al
  extends o
  implements au
{
  private static final String a = "al";
  private static Handler e;
  private boolean b;
  private final ak c;
  private aq d;
  private SparseArray f;
  
  static
  {
    Handler localHandler = new android/os/Handler;
    Looper localLooper = Looper.getMainLooper();
    localHandler.<init>(localLooper);
    e = localHandler;
  }
  
  al(ak paramak, aq paramaq)
  {
    SparseArray localSparseArray = new android/util/SparseArray;
    localSparseArray.<init>();
    f = localSparseArray;
    c = paramak;
    d = paramaq;
  }
  
  public final void destroy()
  {
    b = true;
    SparseArray localSparseArray1 = f;
    int i = localSparseArray1.size();
    int j = 0;
    while (j < i)
    {
      int k = f.keyAt(j);
      Handler localHandler = e;
      SparseArray localSparseArray2 = f;
      Runnable localRunnable = (Runnable)localSparseArray2.get(k);
      localHandler.removeCallbacks(localRunnable);
      j += 1;
    }
    f.clear();
  }
  
  public final void destroyItem(ViewGroup paramViewGroup, int paramInt, Object paramObject)
  {
    Object localObject1 = paramObject;
    localObject1 = (View)paramObject;
    paramViewGroup.removeView((View)localObject1);
    paramViewGroup = (Runnable)f.get(paramInt);
    if (paramViewGroup != null)
    {
      localObject2 = e;
      ((Handler)localObject2).removeCallbacks(paramViewGroup);
      paramViewGroup = NativeViewFactory.class;
      paramViewGroup.getSimpleName();
    }
    paramViewGroup = e;
    Object localObject2 = new com/inmobi/ads/al$1;
    ((al.1)localObject2).<init>(this, paramObject);
    paramViewGroup.post((Runnable)localObject2);
  }
  
  public final int getCount()
  {
    return c.b();
  }
  
  public final int getItemPosition(Object paramObject)
  {
    if (paramObject == null) {
      paramObject = null;
    } else {
      paramObject = ((View)paramObject).getTag();
    }
    if (paramObject != null)
    {
      boolean bool = paramObject instanceof Integer;
      if (bool) {
        return ((Integer)paramObject).intValue();
      }
    }
    return -2;
  }
  
  public final Object instantiateItem(ViewGroup paramViewGroup, int paramInt)
  {
    Object localObject1 = c.a(paramInt);
    if (localObject1 == null) {
      return null;
    }
    ViewGroup localViewGroup = d.a(paramViewGroup, (ai)localObject1);
    int i = Math.abs(d.b - paramInt);
    al.2 local2 = new com/inmobi/ads/al$2;
    Object localObject2 = local2;
    local2.<init>(this, paramInt, localViewGroup, paramViewGroup, (ai)localObject1);
    f.put(paramInt, local2);
    localObject2 = e;
    long l = i * 50;
    ((Handler)localObject2).postDelayed(local2, l);
    localObject1 = NativeViewFactory.a((ag)localObject1, paramViewGroup);
    localViewGroup.setLayoutParams((ViewGroup.LayoutParams)localObject1);
    Integer localInteger = Integer.valueOf(paramInt);
    localViewGroup.setTag(localInteger);
    paramViewGroup.addView(localViewGroup);
    return localViewGroup;
  }
  
  public final boolean isViewFromObject(View paramView, Object paramObject)
  {
    return paramView.equals(paramObject);
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.al
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */