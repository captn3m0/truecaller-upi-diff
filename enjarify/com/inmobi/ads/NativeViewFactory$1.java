package com.inmobi.ads;

import android.content.Context;
import android.view.View;

final class NativeViewFactory$1
  extends NativeViewFactory.c
{
  NativeViewFactory$1(NativeViewFactory paramNativeViewFactory)
  {
    super(paramNativeViewFactory);
  }
  
  protected final View a(Context paramContext)
  {
    at localat = new com/inmobi/ads/at;
    paramContext = paramContext.getApplicationContext();
    localat.<init>(paramContext);
    return localat;
  }
  
  protected final void a(View paramView, ag paramag, b paramb)
  {
    super.a(paramView, paramag, paramb);
    paramag = c;
    NativeViewFactory.a(paramView, paramag);
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.NativeViewFactory.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */