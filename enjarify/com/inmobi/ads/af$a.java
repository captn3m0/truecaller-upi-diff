package com.inmobi.ads;

import android.content.Context;
import java.util.concurrent.ConcurrentHashMap;

final class af$a
{
  static af a(Context paramContext, bf parambf, j.b paramb, int paramInt)
  {
    Object localObject = (j)g.a.get(parambf);
    int i = localObject instanceof af;
    if (i != 0) {
      localObject = (af)localObject;
    } else {
      localObject = null;
    }
    if (localObject != null)
    {
      i = 1;
      if (i == paramInt) {
        return null;
      }
    }
    if (localObject == null)
    {
      af.L();
      localObject = new com/inmobi/ads/af;
      long l = a;
      ((af)localObject).<init>(paramContext, l, paramb, (byte)0);
      if (paramInt != 0)
      {
        paramContext = g.a;
        paramContext.put(parambf, localObject);
      }
    }
    else
    {
      af.L();
      ((af)localObject).a(paramContext);
      paramContext = g.a;
      paramContext.remove(parambf);
    }
    ((af)localObject).a(paramb);
    paramContext = f;
    ((af)localObject).a(paramContext);
    return (af)localObject;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.af.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */