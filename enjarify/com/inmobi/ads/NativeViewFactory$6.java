package com.inmobi.ads;

import android.content.Context;
import android.view.View;

final class NativeViewFactory$6
  extends NativeViewFactory.c
{
  NativeViewFactory$6(NativeViewFactory paramNativeViewFactory)
  {
    super(paramNativeViewFactory);
  }
  
  protected final View a(Context paramContext)
  {
    NativeContainerLayout localNativeContainerLayout = new com/inmobi/ads/NativeContainerLayout;
    paramContext = paramContext.getApplicationContext();
    localNativeContainerLayout.<init>(paramContext);
    return localNativeContainerLayout;
  }
  
  protected final void a(View paramView, ag paramag, b paramb)
  {
    super.a(paramView, paramag, paramb);
    paramag = c;
    NativeViewFactory.a(paramView, paramag);
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.NativeViewFactory.6
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */