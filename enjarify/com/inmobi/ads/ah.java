package com.inmobi.ads;

import android.graphics.Point;
import java.util.Locale;

public class ah
{
  public Point a;
  Point b;
  Point c;
  Point d;
  protected String e;
  protected String f;
  protected String g;
  protected float h;
  protected String i;
  protected String j;
  protected ax k;
  
  ah()
  {
    Point localPoint = new android/graphics/Point;
    localPoint.<init>(0, 0);
    a = localPoint;
    localPoint = new android/graphics/Point;
    localPoint.<init>(0, 0);
    c = localPoint;
    localPoint = new android/graphics/Point;
    localPoint.<init>(0, 0);
    b = localPoint;
    localPoint = new android/graphics/Point;
    localPoint.<init>(0, 0);
    d = localPoint;
    e = "none";
    f = "straight";
    h = 10.0F;
    i = "#ff000000";
    j = "#00000000";
    g = "fill";
    k = null;
  }
  
  public ah(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, String paramString1, String paramString2, String paramString3, String paramString4, ax paramax)
  {
    this(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7, paramInt8, "fill", paramString1, paramString2, paramString3, paramString4, paramax);
  }
  
  public ah(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, ax paramax)
  {
    Point localPoint1 = new android/graphics/Point;
    localPoint1.<init>(paramInt3, paramInt4);
    a = localPoint1;
    Point localPoint2 = new android/graphics/Point;
    localPoint2.<init>(paramInt7, paramInt8);
    b = localPoint2;
    localPoint2 = new android/graphics/Point;
    localPoint2.<init>(paramInt1, paramInt2);
    c = localPoint2;
    Point localPoint3 = new android/graphics/Point;
    localPoint3.<init>(paramInt5, paramInt6);
    d = localPoint3;
    e = paramString2;
    f = paramString3;
    float f1 = 10.0F;
    h = f1;
    g = paramString1;
    paramInt1 = paramString4.length();
    if (paramInt1 == 0) {
      paramString4 = "#ff000000";
    }
    i = paramString4;
    paramInt1 = paramString5.length();
    if (paramInt1 == 0) {
      paramString5 = "#00000000";
    }
    j = paramString5;
    k = paramax;
  }
  
  public final String a()
  {
    return e;
  }
  
  public final String b()
  {
    return f;
  }
  
  public final float c()
  {
    return h;
  }
  
  public final String d()
  {
    String str = i;
    Locale localLocale = Locale.US;
    return str.toLowerCase(localLocale);
  }
  
  public String e()
  {
    String str = j;
    Locale localLocale = Locale.US;
    return str.toLowerCase(localLocale);
  }
  
  public final String f()
  {
    return g;
  }
  
  public final ax g()
  {
    return k;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.ah
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */