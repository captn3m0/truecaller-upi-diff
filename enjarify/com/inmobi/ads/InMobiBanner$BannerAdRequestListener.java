package com.inmobi.ads;

public abstract interface InMobiBanner$BannerAdRequestListener
{
  public abstract void onAdRequestCompleted(InMobiAdRequestStatus paramInMobiAdRequestStatus, InMobiBanner paramInMobiBanner);
}

/* Location:
 * Qualified Name:     com.inmobi.ads.InMobiBanner.BannerAdRequestListener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */