package com.inmobi.ads;

import com.inmobi.commons.core.configs.b;
import com.inmobi.commons.core.configs.h;
import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;
import com.inmobi.commons.core.utilities.b.e;
import com.inmobi.commons.core.utilities.d;
import com.inmobi.signals.p;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

final class j$6
  implements Runnable
{
  j$6(j paramj, boolean paramBoolean) {}
  
  public final void run()
  {
    try
    {
      Object localObject1 = b;
      localObject1 = j.b((j)localObject1);
      if (localObject1 == null)
      {
        localObject1 = b;
        localObject3 = new com/inmobi/ads/j$a;
        localObject4 = b;
        ((j.a)localObject3).<init>((j)localObject4);
        j.a((j)localObject1, (j.a)localObject3);
      }
      boolean bool1 = d.a();
      long l1;
      Object localObject5;
      Object localObject6;
      if (!bool1)
      {
        localObject1 = b;
        localObject3 = b;
        l1 = b;
        localObject5 = new com/inmobi/ads/InMobiAdRequestStatus;
        localObject6 = InMobiAdRequestStatus.StatusCode.NETWORK_UNREACHABLE;
        ((InMobiAdRequestStatus)localObject5).<init>((InMobiAdRequestStatus.StatusCode)localObject6);
        ((j)localObject1).b(l1, (InMobiAdRequestStatus)localObject5);
        return;
      }
      localObject1 = p.a();
      ((p)localObject1).e();
      j.I();
      localObject1 = new com/inmobi/commons/core/configs/h;
      ((h)localObject1).<init>();
      localObject3 = b.a();
      int i = 0;
      localObject4 = null;
      ((b)localObject3).a((com.inmobi.commons.core.configs.a)localObject1, null);
      bool1 = g;
      if (!bool1)
      {
        localObject1 = b;
        l1 = System.currentTimeMillis();
        j.a((j)localObject1, l1);
        try
        {
          localObject1 = b;
          localObject1 = j.c((j)localObject1);
          if (localObject1 == null)
          {
            localObject1 = b;
            localObject3 = new com/inmobi/ads/bi;
            localObject4 = b;
            ((bi)localObject3).<init>((bi.a)localObject4);
            j.a((j)localObject1, (bi)localObject3);
          }
          localObject1 = b;
          localObject3 = b;
          localObject3 = j.c((j)localObject3);
          localObject4 = b;
          localObject4 = j.d((j)localObject4);
          boolean bool4 = a;
          boolean bool5 = e.b();
          if (bool5) {
            c.b();
          }
          bool5 = false;
          localObject6 = null;
          a = false;
          d = ((i)localObject4);
          e = bool4;
          localObject4 = d;
          localObject4 = h;
          Object localObject7 = d;
          localObject7 = j;
          long l2 = d;
          i = c.a((String)localObject4, l2);
          Object localObject8;
          if (i > 0)
          {
            localObject7 = new java/util/HashMap;
            ((HashMap)localObject7).<init>();
            localObject8 = "count";
            localObject4 = Integer.valueOf(i);
            ((Map)localObject7).put(localObject8, localObject4);
            localObject4 = "isPreloaded";
            localObject8 = "1";
            ((Map)localObject7).put(localObject4, localObject8);
            localObject4 = "im-accid";
            localObject8 = com.inmobi.commons.a.a.e();
            ((Map)localObject7).put(localObject4, localObject8);
            localObject4 = b;
            localObject8 = "ads";
            String str1 = "AdCacheAdExpired";
            ((bi.a)localObject4).a((String)localObject8, str1, (Map)localObject7);
          }
          localObject7 = c;
          localObject4 = d;
          long l3 = d;
          localObject4 = d;
          String str2 = f;
          localObject4 = d;
          InMobiAdRequest.MonetizationContext localMonetizationContext = m;
          localObject4 = d;
          localObject4 = g;
          String str3 = g.a((Map)localObject4);
          localObject4 = ((c)localObject7).c(l3, str2, localMonetizationContext, str3);
          int k = ((List)localObject4).size();
          if (k == 0)
          {
            a = false;
            localObject4 = d;
            i = b;
            boolean bool2 = ((bi)localObject3).a(i);
            if (!bool2)
            {
              localObject4 = d;
              localObject3 = ((bi)localObject3).a((i)localObject4);
            }
            else
            {
              localObject1 = new com/inmobi/ads/a/a;
              localObject3 = "Ignoring request to fetch an ad from the network sooner than the minimum request interval";
              ((com.inmobi.ads.a.a)localObject1).<init>((String)localObject3);
              throw ((Throwable)localObject1);
            }
          }
          else
          {
            localObject8 = d;
            localObject8 = j;
            int m = c;
            boolean bool6 = true;
            if (k < m)
            {
              a = bool6;
              if (!bool4)
              {
                localObject5 = b;
                localObject6 = d;
                long l4 = d;
                ((bi.a)localObject5).a(l4);
              }
              ((bi)localObject3).a((List)localObject4);
              localObject4 = d;
              int j = b;
              boolean bool3 = ((bi)localObject3).a(j);
              if (!bool3)
              {
                localObject4 = d;
                localObject4 = ((bi)localObject3).a((i)localObject4);
              }
              else
              {
                localObject1 = new com/inmobi/ads/a/a;
                localObject3 = "Ignoring request to fetch an ad from the network sooner than the minimum request interval";
                ((com.inmobi.ads.a.a)localObject1).<init>((String)localObject3);
                throw ((Throwable)localObject1);
              }
            }
            else
            {
              a = bool6;
              localObject6 = ((List)localObject4).get(0);
              localObject6 = (a)localObject6;
              localObject6 = f;
              if (!bool4)
              {
                localObject5 = b;
                localObject7 = d;
                l2 = d;
                ((bi.a)localObject5).a(l2);
              }
              ((bi)localObject3).a((List)localObject4);
              localObject4 = localObject6;
            }
            localObject5 = new java/util/HashMap;
            ((HashMap)localObject5).<init>();
            localObject6 = "im-accid";
            localObject7 = com.inmobi.commons.a.a.e();
            ((Map)localObject5).put(localObject6, localObject7);
            localObject6 = "isPreloaded";
            localObject7 = "1";
            ((Map)localObject5).put(localObject6, localObject7);
            localObject3 = b;
            localObject6 = "ads";
            localObject7 = "AdCacheAdRequested";
            ((bi.a)localObject3).a((String)localObject6, (String)localObject7, (Map)localObject5);
            localObject3 = localObject4;
          }
          j.a((j)localObject1, (String)localObject3);
          return;
        }
        catch (com.inmobi.ads.a.a locala)
        {
          j.H();
          locala.getMessage();
          Object localObject2 = b;
          localObject2 = j.c((j)localObject2);
          bool1 = a;
          if (!bool1)
          {
            localObject2 = b;
            localObject3 = b;
            l1 = j.a((j)localObject3);
            localObject5 = new com/inmobi/ads/InMobiAdRequestStatus;
            localObject6 = InMobiAdRequestStatus.StatusCode.EARLY_REFRESH_REQUEST;
            ((InMobiAdRequestStatus)localObject5).<init>((InMobiAdRequestStatus.StatusCode)localObject6);
            ((j)localObject2).b(l1, (InMobiAdRequestStatus)localObject5);
          }
        }
      }
      return;
    }
    catch (Exception localException)
    {
      Logger.a(Logger.InternalLogLevel.ERROR, "InMobi", "Unable to Prefetch ad; SDK encountered an unexpected error");
      j.H();
      localException.getMessage();
      Object localObject3 = com.inmobi.commons.core.a.a.a();
      Object localObject4 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject4).<init>(localException);
      ((com.inmobi.commons.core.a.a)localObject3).a((com.inmobi.commons.core.e.a)localObject4);
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.j.6
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */