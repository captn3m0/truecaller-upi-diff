package com.inmobi.ads;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import com.moat.analytics.mobile.inm.NativeDisplayTracker;
import java.lang.ref.WeakReference;
import java.util.Map;

class aa
  extends bv
{
  private final String d;
  private final WeakReference e;
  private NativeDisplayTracker f;
  private Map g;
  private bw h;
  
  aa(Activity paramActivity, bw parambw, Map paramMap)
  {
    Object localObject = aa.class.getSimpleName();
    d = ((String)localObject);
    localObject = new java/lang/ref/WeakReference;
    ((WeakReference)localObject).<init>(paramActivity);
    e = ((WeakReference)localObject);
    h = parambw;
    g = paramMap;
  }
  
  public final View a()
  {
    return h.a();
  }
  
  public final View a(View paramView, ViewGroup paramViewGroup, boolean paramBoolean)
  {
    return h.a(paramView, paramViewGroup, paramBoolean);
  }
  
  /* Error */
  public final void a(int paramInt)
  {
    // Byte code:
    //   0: iconst_4
    //   1: istore_2
    //   2: iload_2
    //   3: iload_1
    //   4: if_icmpne +67 -> 71
    //   7: aload_0
    //   8: getfield 39	com/inmobi/ads/aa:f	Lcom/moat/analytics/mobile/inm/NativeDisplayTracker;
    //   11: astore_3
    //   12: getstatic 55	com/moat/analytics/mobile/inm/NativeDisplayTracker$MoatUserInteractionType:CLICK	Lcom/moat/analytics/mobile/inm/NativeDisplayTracker$MoatUserInteractionType;
    //   15: astore 4
    //   17: aload_3
    //   18: aload 4
    //   20: invokeinterface 61 2 0
    //   25: aload_0
    //   26: getfield 39	com/inmobi/ads/aa:f	Lcom/moat/analytics/mobile/inm/NativeDisplayTracker;
    //   29: astore_3
    //   30: aload_3
    //   31: invokevirtual 67	java/lang/Object:hashCode	()I
    //   34: pop
    //   35: goto +36 -> 71
    //   38: astore_3
    //   39: goto +41 -> 80
    //   42: astore_3
    //   43: aload_3
    //   44: invokevirtual 72	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   47: pop
    //   48: invokestatic 77	com/inmobi/commons/core/a/a:a	()Lcom/inmobi/commons/core/a/a;
    //   51: astore 4
    //   53: new 79	com/inmobi/commons/core/e/a
    //   56: astore 5
    //   58: aload 5
    //   60: aload_3
    //   61: invokespecial 82	com/inmobi/commons/core/e/a:<init>	(Ljava/lang/Throwable;)V
    //   64: aload 4
    //   66: aload 5
    //   68: invokevirtual 85	com/inmobi/commons/core/a/a:a	(Lcom/inmobi/commons/core/e/a;)V
    //   71: aload_0
    //   72: getfield 35	com/inmobi/ads/aa:h	Lcom/inmobi/ads/bw;
    //   75: iload_1
    //   76: invokevirtual 88	com/inmobi/ads/bw:a	(I)V
    //   79: return
    //   80: aload_0
    //   81: getfield 35	com/inmobi/ads/aa:h	Lcom/inmobi/ads/bw;
    //   84: iload_1
    //   85: invokevirtual 88	com/inmobi/ads/bw:a	(I)V
    //   88: aload_3
    //   89: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	90	0	this	aa
    //   0	90	1	paramInt	int
    //   1	4	2	i	int
    //   11	20	3	localNativeDisplayTracker	NativeDisplayTracker
    //   38	1	3	localObject1	Object
    //   42	47	3	localException	Exception
    //   15	50	4	localObject2	Object
    //   56	11	5	locala	com.inmobi.commons.core.e.a
    // Exception table:
    //   from	to	target	type
    //   7	11	38	finally
    //   12	15	38	finally
    //   18	25	38	finally
    //   25	29	38	finally
    //   30	35	38	finally
    //   43	48	38	finally
    //   48	51	38	finally
    //   53	56	38	finally
    //   60	64	38	finally
    //   66	71	38	finally
    //   7	11	42	java/lang/Exception
    //   12	15	42	java/lang/Exception
    //   18	25	42	java/lang/Exception
    //   25	29	42	java/lang/Exception
    //   30	35	42	java/lang/Exception
  }
  
  public final void a(Context paramContext, int paramInt)
  {
    h.a(paramContext, paramInt);
  }
  
  /* Error */
  public final void a(View... paramVarArgs)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 35	com/inmobi/ads/aa:h	Lcom/inmobi/ads/bw;
    //   4: astore_2
    //   5: aload_2
    //   6: invokevirtual 94	com/inmobi/ads/bw:b	()Landroid/view/View;
    //   9: astore_2
    //   10: aload_2
    //   11: ifnonnull +12 -> 23
    //   14: aload_0
    //   15: getfield 35	com/inmobi/ads/aa:h	Lcom/inmobi/ads/bw;
    //   18: aload_1
    //   19: invokevirtual 97	com/inmobi/ads/bw:a	([Landroid/view/View;)V
    //   22: return
    //   23: aload_0
    //   24: getfield 33	com/inmobi/ads/aa:e	Ljava/lang/ref/WeakReference;
    //   27: astore_3
    //   28: aload_3
    //   29: invokevirtual 101	java/lang/ref/WeakReference:get	()Ljava/lang/Object;
    //   32: astore_3
    //   33: aload_3
    //   34: checkcast 103	android/app/Activity
    //   37: astore_3
    //   38: aload_0
    //   39: getfield 35	com/inmobi/ads/aa:h	Lcom/inmobi/ads/bw;
    //   42: astore 4
    //   44: aload 4
    //   46: invokevirtual 107	com/inmobi/ads/bw:c	()Lcom/inmobi/ads/b;
    //   49: astore 4
    //   51: aload 4
    //   53: getfield 113	com/inmobi/ads/b:o	Lcom/inmobi/ads/b$k;
    //   56: astore 4
    //   58: aload 4
    //   60: getfield 119	com/inmobi/ads/b$k:i	Z
    //   63: istore 5
    //   65: iload 5
    //   67: ifeq +298 -> 365
    //   70: aload_3
    //   71: ifnull +294 -> 365
    //   74: aload_0
    //   75: getfield 37	com/inmobi/ads/aa:g	Ljava/util/Map;
    //   78: astore 4
    //   80: ldc 121
    //   82: astore 6
    //   84: aload 4
    //   86: aload 6
    //   88: invokeinterface 126 2 0
    //   93: astore 4
    //   95: aload 4
    //   97: checkcast 128	java/lang/Boolean
    //   100: astore 4
    //   102: aload 4
    //   104: invokevirtual 132	java/lang/Boolean:booleanValue	()Z
    //   107: istore 5
    //   109: iload 5
    //   111: ifeq +254 -> 365
    //   114: aload_0
    //   115: getfield 39	com/inmobi/ads/aa:f	Lcom/moat/analytics/mobile/inm/NativeDisplayTracker;
    //   118: astore 4
    //   120: aload 4
    //   122: ifnonnull +202 -> 324
    //   125: aload_3
    //   126: invokevirtual 136	android/app/Activity:getApplication	()Landroid/app/Application;
    //   129: astore_3
    //   130: aload_0
    //   131: getfield 37	com/inmobi/ads/aa:g	Ljava/util/Map;
    //   134: astore 4
    //   136: ldc -118
    //   138: astore 6
    //   140: aload 4
    //   142: aload 6
    //   144: invokeinterface 126 2 0
    //   149: astore 4
    //   151: aload 4
    //   153: checkcast 140	java/lang/String
    //   156: astore 4
    //   158: ldc -114
    //   160: astore 6
    //   162: ldc -112
    //   164: astore 7
    //   166: aload_0
    //   167: getfield 37	com/inmobi/ads/aa:g	Ljava/util/Map;
    //   170: astore 8
    //   172: ldc -110
    //   174: astore 9
    //   176: aload 8
    //   178: aload 9
    //   180: invokeinterface 126 2 0
    //   185: astore 8
    //   187: aload 8
    //   189: checkcast 148	org/json/JSONArray
    //   192: astore 8
    //   194: aload_0
    //   195: getfield 37	com/inmobi/ads/aa:g	Ljava/util/Map;
    //   198: astore 9
    //   200: ldc -106
    //   202: astore 10
    //   204: aload 9
    //   206: aload 10
    //   208: invokeinterface 126 2 0
    //   213: astore 9
    //   215: aload 9
    //   217: checkcast 148	org/json/JSONArray
    //   220: astore 9
    //   222: aload_0
    //   223: getfield 37	com/inmobi/ads/aa:g	Ljava/util/Map;
    //   226: astore 10
    //   228: ldc -104
    //   230: astore 11
    //   232: aload 10
    //   234: aload 11
    //   236: invokeinterface 126 2 0
    //   241: astore 10
    //   243: aload 10
    //   245: checkcast 154	org/json/JSONObject
    //   248: astore 10
    //   250: aload 6
    //   252: aload 7
    //   254: aload 8
    //   256: aload 9
    //   258: aload 10
    //   260: invokestatic 159	com/inmobi/ads/j$c:a	(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONArray;Lorg/json/JSONArray;Lorg/json/JSONObject;)Ljava/util/HashMap;
    //   263: astore 6
    //   265: ldc -95
    //   267: astore 7
    //   269: aload_0
    //   270: getfield 37	com/inmobi/ads/aa:g	Ljava/util/Map;
    //   273: astore 8
    //   275: ldc -95
    //   277: astore 9
    //   279: aload 8
    //   281: aload 9
    //   283: invokeinterface 126 2 0
    //   288: astore 8
    //   290: aload 8
    //   292: checkcast 140	java/lang/String
    //   295: astore 8
    //   297: aload 6
    //   299: aload 7
    //   301: aload 8
    //   303: invokeinterface 165 3 0
    //   308: pop
    //   309: aload_3
    //   310: aload 4
    //   312: aload_2
    //   313: aload 6
    //   315: invokestatic 170	com/inmobi/ads/u:a	(Landroid/app/Application;Ljava/lang/String;Landroid/view/View;Ljava/util/Map;)Lcom/moat/analytics/mobile/inm/NativeDisplayTracker;
    //   318: astore_3
    //   319: aload_0
    //   320: aload_3
    //   321: putfield 39	com/inmobi/ads/aa:f	Lcom/moat/analytics/mobile/inm/NativeDisplayTracker;
    //   324: new 172	com/inmobi/ads/aa$1
    //   327: astore_3
    //   328: aload_3
    //   329: aload_0
    //   330: invokespecial 175	com/inmobi/ads/aa$1:<init>	(Lcom/inmobi/ads/aa;)V
    //   333: aload_2
    //   334: aload_3
    //   335: invokevirtual 181	android/view/View:setOnTouchListener	(Landroid/view/View$OnTouchListener;)V
    //   338: aload_0
    //   339: getfield 39	com/inmobi/ads/aa:f	Lcom/moat/analytics/mobile/inm/NativeDisplayTracker;
    //   342: astore_2
    //   343: aload_2
    //   344: invokeinterface 184 1 0
    //   349: aload_0
    //   350: getfield 37	com/inmobi/ads/aa:g	Ljava/util/Map;
    //   353: astore_2
    //   354: ldc -95
    //   356: astore_3
    //   357: aload_2
    //   358: aload_3
    //   359: invokeinterface 126 2 0
    //   364: pop
    //   365: aload_0
    //   366: getfield 35	com/inmobi/ads/aa:h	Lcom/inmobi/ads/bw;
    //   369: aload_1
    //   370: invokevirtual 97	com/inmobi/ads/bw:a	([Landroid/view/View;)V
    //   373: return
    //   374: astore_2
    //   375: goto +33 -> 408
    //   378: astore_2
    //   379: aload_2
    //   380: invokevirtual 72	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   383: pop
    //   384: invokestatic 77	com/inmobi/commons/core/a/a:a	()Lcom/inmobi/commons/core/a/a;
    //   387: astore_3
    //   388: new 79	com/inmobi/commons/core/e/a
    //   391: astore 4
    //   393: aload 4
    //   395: aload_2
    //   396: invokespecial 82	com/inmobi/commons/core/e/a:<init>	(Ljava/lang/Throwable;)V
    //   399: aload_3
    //   400: aload 4
    //   402: invokevirtual 85	com/inmobi/commons/core/a/a:a	(Lcom/inmobi/commons/core/e/a;)V
    //   405: goto -40 -> 365
    //   408: aload_0
    //   409: getfield 35	com/inmobi/ads/aa:h	Lcom/inmobi/ads/bw;
    //   412: aload_1
    //   413: invokevirtual 97	com/inmobi/ads/bw:a	([Landroid/view/View;)V
    //   416: aload_2
    //   417: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	418	0	this	aa
    //   0	418	1	paramVarArgs	View[]
    //   4	354	2	localObject1	Object
    //   374	1	2	localObject2	Object
    //   378	39	2	localException	Exception
    //   27	373	3	localObject3	Object
    //   42	359	4	localObject4	Object
    //   63	47	5	bool	boolean
    //   82	232	6	localObject5	Object
    //   164	136	7	str1	String
    //   170	132	8	localObject6	Object
    //   174	108	9	localObject7	Object
    //   202	57	10	localObject8	Object
    //   230	5	11	str2	String
    // Exception table:
    //   from	to	target	type
    //   0	4	374	finally
    //   5	9	374	finally
    //   23	27	374	finally
    //   28	32	374	finally
    //   33	37	374	finally
    //   38	42	374	finally
    //   44	49	374	finally
    //   51	56	374	finally
    //   58	63	374	finally
    //   74	78	374	finally
    //   86	93	374	finally
    //   95	100	374	finally
    //   102	107	374	finally
    //   114	118	374	finally
    //   125	129	374	finally
    //   130	134	374	finally
    //   142	149	374	finally
    //   151	156	374	finally
    //   166	170	374	finally
    //   178	185	374	finally
    //   187	192	374	finally
    //   194	198	374	finally
    //   206	213	374	finally
    //   215	220	374	finally
    //   222	226	374	finally
    //   234	241	374	finally
    //   243	248	374	finally
    //   258	263	374	finally
    //   269	273	374	finally
    //   281	288	374	finally
    //   290	295	374	finally
    //   301	309	374	finally
    //   313	318	374	finally
    //   320	324	374	finally
    //   324	327	374	finally
    //   329	333	374	finally
    //   334	338	374	finally
    //   338	342	374	finally
    //   343	349	374	finally
    //   349	353	374	finally
    //   358	365	374	finally
    //   379	384	374	finally
    //   384	387	374	finally
    //   388	391	374	finally
    //   395	399	374	finally
    //   400	405	374	finally
    //   0	4	378	java/lang/Exception
    //   5	9	378	java/lang/Exception
    //   23	27	378	java/lang/Exception
    //   28	32	378	java/lang/Exception
    //   33	37	378	java/lang/Exception
    //   38	42	378	java/lang/Exception
    //   44	49	378	java/lang/Exception
    //   51	56	378	java/lang/Exception
    //   58	63	378	java/lang/Exception
    //   74	78	378	java/lang/Exception
    //   86	93	378	java/lang/Exception
    //   95	100	378	java/lang/Exception
    //   102	107	378	java/lang/Exception
    //   114	118	378	java/lang/Exception
    //   125	129	378	java/lang/Exception
    //   130	134	378	java/lang/Exception
    //   142	149	378	java/lang/Exception
    //   151	156	378	java/lang/Exception
    //   166	170	378	java/lang/Exception
    //   178	185	378	java/lang/Exception
    //   187	192	378	java/lang/Exception
    //   194	198	378	java/lang/Exception
    //   206	213	378	java/lang/Exception
    //   215	220	378	java/lang/Exception
    //   222	226	378	java/lang/Exception
    //   234	241	378	java/lang/Exception
    //   243	248	378	java/lang/Exception
    //   258	263	378	java/lang/Exception
    //   269	273	378	java/lang/Exception
    //   281	288	378	java/lang/Exception
    //   290	295	378	java/lang/Exception
    //   301	309	378	java/lang/Exception
    //   313	318	378	java/lang/Exception
    //   320	324	378	java/lang/Exception
    //   324	327	378	java/lang/Exception
    //   329	333	378	java/lang/Exception
    //   334	338	378	java/lang/Exception
    //   338	342	378	java/lang/Exception
    //   343	349	378	java/lang/Exception
    //   349	353	378	java/lang/Exception
    //   358	365	378	java/lang/Exception
  }
  
  public final View b()
  {
    return h.b();
  }
  
  final b c()
  {
    return h.c();
  }
  
  /* Error */
  public final void d()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 39	com/inmobi/ads/aa:f	Lcom/moat/analytics/mobile/inm/NativeDisplayTracker;
    //   4: astore_1
    //   5: aload_1
    //   6: ifnull +30 -> 36
    //   9: aload_0
    //   10: getfield 39	com/inmobi/ads/aa:f	Lcom/moat/analytics/mobile/inm/NativeDisplayTracker;
    //   13: astore_1
    //   14: aload_1
    //   15: invokeinterface 187 1 0
    //   20: aload_0
    //   21: getfield 37	com/inmobi/ads/aa:g	Ljava/util/Map;
    //   24: astore_1
    //   25: ldc -95
    //   27: astore_2
    //   28: aload_1
    //   29: aload_2
    //   30: invokeinterface 126 2 0
    //   35: pop
    //   36: aload_0
    //   37: getfield 35	com/inmobi/ads/aa:h	Lcom/inmobi/ads/bw;
    //   40: invokevirtual 189	com/inmobi/ads/bw:d	()V
    //   43: return
    //   44: astore_1
    //   45: goto +30 -> 75
    //   48: astore_1
    //   49: aload_1
    //   50: invokevirtual 72	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   53: pop
    //   54: invokestatic 77	com/inmobi/commons/core/a/a:a	()Lcom/inmobi/commons/core/a/a;
    //   57: astore_2
    //   58: new 79	com/inmobi/commons/core/e/a
    //   61: astore_3
    //   62: aload_3
    //   63: aload_1
    //   64: invokespecial 82	com/inmobi/commons/core/e/a:<init>	(Ljava/lang/Throwable;)V
    //   67: aload_2
    //   68: aload_3
    //   69: invokevirtual 85	com/inmobi/commons/core/a/a:a	(Lcom/inmobi/commons/core/e/a;)V
    //   72: goto -36 -> 36
    //   75: aload_0
    //   76: getfield 35	com/inmobi/ads/aa:h	Lcom/inmobi/ads/bw;
    //   79: invokevirtual 189	com/inmobi/ads/bw:d	()V
    //   82: aload_1
    //   83: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	84	0	this	aa
    //   4	25	1	localObject1	Object
    //   44	1	1	localObject2	Object
    //   48	35	1	localException	Exception
    //   27	41	2	localObject3	Object
    //   61	8	3	locala	com.inmobi.commons.core.e.a
    // Exception table:
    //   from	to	target	type
    //   0	4	44	finally
    //   9	13	44	finally
    //   14	20	44	finally
    //   20	24	44	finally
    //   29	36	44	finally
    //   49	54	44	finally
    //   54	57	44	finally
    //   58	61	44	finally
    //   63	67	44	finally
    //   68	72	44	finally
    //   0	4	48	java/lang/Exception
    //   9	13	48	java/lang/Exception
    //   14	20	48	java/lang/Exception
    //   20	24	48	java/lang/Exception
    //   29	36	48	java/lang/Exception
  }
  
  public final void e()
  {
    f = null;
    e.clear();
    super.e();
    h.e();
  }
  
  public final bw.a f()
  {
    return h.f();
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.aa
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */