package com.inmobi.ads;

import android.content.Context;
import com.inmobi.rendering.RenderView;
import com.inmobi.rendering.RenderView.a;
import java.util.Set;

final class j$4$1
  implements Runnable
{
  j$4$1(j.4 param4, ak paramak) {}
  
  public final void run()
  {
    try
    {
      Object localObject1 = a;
      localObject1 = k;
      if (localObject1 != null)
      {
        localObject2 = b;
        localObject2 = a;
        localObject3 = new com/inmobi/rendering/RenderView;
        localObject4 = b;
        localObject4 = a;
        localObject4 = ((j)localObject4).a();
        AdContainer.RenderingProperties localRenderingProperties = new com/inmobi/ads/AdContainer$RenderingProperties;
        Object localObject5 = b;
        localObject5 = a;
        localObject5 = ((j)localObject5).d();
        localRenderingProperties.<init>((AdContainer.RenderingProperties.PlacementType)localObject5);
        localObject5 = b;
        localObject5 = a;
        localObject5 = j.f((j)localObject5);
        Object localObject6 = b;
        localObject6 = a;
        localObject6 = j;
        ((RenderView)localObject3).<init>((Context)localObject4, localRenderingProperties, (Set)localObject5, (String)localObject6);
        j.a((j)localObject2, (RenderView)localObject3);
        localObject2 = b;
        localObject2 = a;
        localObject2 = j.h((j)localObject2);
        localObject3 = b;
        localObject3 = a;
        localObject3 = j.g((j)localObject3);
        localObject4 = b;
        localObject4 = a;
        localObject4 = e;
        ((RenderView)localObject2).a((RenderView.a)localObject3, (b)localObject4);
        localObject2 = b;
        localObject2 = a;
        localObject2 = j.h((j)localObject2);
        boolean bool1 = true;
        j = bool1;
        localObject2 = b;
        localObject2 = a;
        localObject2 = j.h((j)localObject2);
        localObject4 = b;
        localObject4 = a;
        ((RenderView)localObject2).setBlobProvider((com.inmobi.rendering.a)localObject4);
        localObject2 = b;
        localObject2 = a;
        localObject2 = j.h((j)localObject2);
        ((RenderView)localObject2).setIsPreload(bool1);
        localObject2 = b;
        localObject2 = a;
        int i = q;
        if (i == 0)
        {
          localObject2 = b;
          localObject2 = a;
          localObject4 = b;
          localObject4 = a;
          localObject4 = j.h((j)localObject4);
          ((j)localObject2).a(bool1, (RenderView)localObject4);
        }
        localObject2 = "URL";
        localObject3 = z;
        boolean bool2 = ((String)localObject2).equals(localObject3);
        if (bool2)
        {
          localObject2 = b;
          localObject2 = a;
          localObject2 = j.h((j)localObject2);
          localObject1 = e;
          localObject1 = (String)localObject1;
          ((RenderView)localObject2).b((String)localObject1);
        }
        else
        {
          localObject2 = b;
          localObject2 = a;
          localObject2 = j.h((j)localObject2);
          localObject1 = e;
          localObject1 = (String)localObject1;
          ((RenderView)localObject2).a((String)localObject1);
        }
      }
      localObject1 = b;
      localObject1 = a;
      j.e((j)localObject1);
      return;
    }
    catch (Exception localException)
    {
      j.H();
      b.a.a = 3;
      Object localObject2 = b.a;
      Object localObject3 = new com/inmobi/ads/InMobiAdRequestStatus;
      Object localObject4 = InMobiAdRequestStatus.StatusCode.INTERNAL_ERROR;
      ((InMobiAdRequestStatus)localObject3).<init>((InMobiAdRequestStatus.StatusCode)localObject4);
      ((j)localObject2).a((InMobiAdRequestStatus)localObject3, false);
      localObject2 = com.inmobi.commons.core.a.a.a();
      localObject3 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject3).<init>(localException);
      ((com.inmobi.commons.core.a.a)localObject2).a((com.inmobi.commons.core.e.a)localObject3);
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.j.4.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */