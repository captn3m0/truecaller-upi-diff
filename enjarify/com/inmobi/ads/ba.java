package com.inmobi.ads;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import com.b.a.a.a.f.d;
import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;
import com.inmobi.rendering.RenderView;
import java.lang.ref.WeakReference;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class ba
  extends ad
{
  private static final String A = "ba";
  private final AdContainer.a B;
  WeakReference y;
  ae.a z;
  
  ba(Context paramContext, AdContainer.RenderingProperties paramRenderingProperties, ak paramak, String paramString1, String paramString2, Set paramSet, b paramb)
  {
    super(paramContext, paramRenderingProperties, paramak, paramString1, paramString2, paramSet, paramb);
    paramContext = new com/inmobi/ads/ba$1;
    paramContext.<init>(this);
    B = paramContext;
    paramContext = new com/inmobi/ads/ba$2;
    paramContext.<init>(this);
    z = paramContext;
    a = paramak;
  }
  
  private void f(bb parambb)
  {
    Object localObject1 = v;
    Object localObject2 = "didImpressionFire";
    localObject1 = (Boolean)((Map)localObject1).get(localObject2);
    boolean bool1 = ((Boolean)localObject1).booleanValue();
    if (bool1) {
      return;
    }
    localObject1 = u;
    localObject2 = g(parambb);
    Object localObject3 = new java/util/ArrayList;
    ((ArrayList)localObject3).<init>();
    localObject1 = ((List)localObject1).iterator();
    NativeTracker.TrackerEventType localTrackerEventType;
    do
    {
      boolean bool2 = ((Iterator)localObject1).hasNext();
      if (!bool2) {
        break;
      }
      localObject4 = (NativeTracker)((Iterator)localObject1).next();
      localObject5 = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_VIDEO_RENDER;
      localTrackerEventType = b;
    } while (localObject5 != localTrackerEventType);
    localObject3 = a;
    Object localObject5 = "http";
    boolean bool3 = ((String)localObject3).startsWith((String)localObject5);
    if (bool3) {
      bb.a((NativeTracker)localObject4, (Map)localObject2);
    }
    localObject3 = (List)d.get("referencedEvents");
    Object localObject4 = ((List)localObject3).iterator();
    for (;;)
    {
      boolean bool4 = ((Iterator)localObject4).hasNext();
      if (!bool4) {
        break;
      }
      localObject5 = (NativeTracker.TrackerEventType)((Iterator)localObject4).next();
      parambb.a((NativeTracker.TrackerEventType)localObject5, (Map)localObject2);
    }
    bool1 = ((List)localObject3).isEmpty();
    if (bool1)
    {
      localObject1 = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_PLAY;
      parambb.a((NativeTracker.TrackerEventType)localObject1, (Map)localObject2);
      localObject1 = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_RENDER;
      parambb.a((NativeTracker.TrackerEventType)localObject1, (Map)localObject2);
    }
    localObject1 = a.d;
    localObject2 = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_RENDER;
    localObject3 = g(parambb);
    ((ai)localObject1).a((NativeTracker.TrackerEventType)localObject2, (Map)localObject3);
    parambb = v;
    localObject2 = Boolean.TRUE;
    parambb.put("didImpressionFire", localObject2);
    parambb = g;
    bool1 = false;
    parambb.a(0);
    parambb = b.a;
    localObject1 = AdContainer.RenderingProperties.PlacementType.PLACEMENT_TYPE_INLINE;
    if (parambb == localObject1)
    {
      parambb = new java/util/HashMap;
      parambb.<init>();
      parambb.put("type", "native");
      localObject2 = e;
      parambb.put("clientRequestId", localObject2);
      localObject2 = d;
      parambb.put("impId", localObject2);
      localObject1 = "AdRendered";
      a((String)localObject1, parambb);
    }
    parambb = e();
    if (parambb != null)
    {
      parambb = e();
      parambb.d();
    }
  }
  
  private Map g(bb parambb)
  {
    Object localObject1 = (ai)t;
    HashMap localHashMap = new java/util/HashMap;
    int i = 4;
    localHashMap.<init>(i);
    Object localObject2 = (NativeVideoWrapper)y.get();
    if (localObject2 != null)
    {
      localObject2 = ((NativeVideoWrapper)localObject2).getVideoView();
      localObject3 = "$MD";
      double d1 = ((NativeVideoView)localObject2).getDuration();
      Double.isNaN(d1);
      d1 *= 1.0D;
      l1 = 4652007308841189376L;
      double d2 = 1000.0D;
      d1 /= d2;
      long l2 = Math.round(d1);
      int j = (int)l2;
      localObject2 = String.valueOf(j);
      localHashMap.put(localObject3, localObject2);
    }
    localHashMap.put("[ERRORCODE]", "405");
    int k = ((Integer)v.get("seekPosition")).intValue();
    Locale localLocale = Locale.US;
    String str = "%02d:%02d:%02d.%03d";
    Object localObject4 = new Object[i];
    TimeUnit localTimeUnit1 = TimeUnit.MILLISECONDS;
    long l3 = k;
    Object localObject3 = Long.valueOf(localTimeUnit1.toHours(l3));
    localObject4[0] = localObject3;
    long l1 = TimeUnit.MILLISECONDS.toMinutes(l3);
    TimeUnit localTimeUnit2 = TimeUnit.HOURS;
    long l4 = TimeUnit.MILLISECONDS.toHours(l3);
    long l5 = localTimeUnit2.toMinutes(l4);
    Long localLong = Long.valueOf(l1 - l5);
    localObject4[1] = localLong;
    l1 = TimeUnit.MILLISECONDS.toSeconds(l3);
    localTimeUnit2 = TimeUnit.MINUTES;
    TimeUnit localTimeUnit3 = TimeUnit.MILLISECONDS;
    l4 = localTimeUnit3.toMinutes(l3);
    l5 = localTimeUnit2.toSeconds(l4);
    localLong = Long.valueOf(l1 - l5);
    localObject4[2] = localLong;
    k = 3;
    l1 = TimeUnit.MILLISECONDS.toSeconds(l3);
    l5 = 1000L;
    l1 *= l5;
    l3 -= l1;
    localLong = Long.valueOf(l3);
    localObject4[k] = localLong;
    localObject4 = String.format(localLocale, str, (Object[])localObject4);
    localHashMap.put("[CONTENTPLAYHEAD]", localObject4);
    localObject2 = y();
    localHashMap.put("[CACHEBUSTING]", localObject2);
    parambb = parambb.b().b();
    localHashMap.put("[ASSETURI]", parambb);
    localObject4 = String.valueOf(System.currentTimeMillis());
    localHashMap.put("$TS", localObject4);
    parambb = "$LTS";
    long l6 = a.d.z;
    localObject4 = String.valueOf(l6);
    localHashMap.put(parambb, localObject4);
    if (localObject1 != null)
    {
      parambb = "$STS";
      l6 = z;
      localObject1 = String.valueOf(l6);
      localHashMap.put(parambb, localObject1);
    }
    return localHashMap;
  }
  
  private static String y()
  {
    SecureRandom localSecureRandom = new java/security/SecureRandom;
    localSecureRandom.<init>();
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    int j;
    for (int i = 0;; i = (localSecureRandom.nextInt() & j) % 10)
    {
      j = -1 >>> 1;
      if (i != 0) {
        break;
      }
    }
    localStringBuilder.append(i);
    i = 1;
    for (;;)
    {
      int k = 8;
      if (i >= k) {
        break;
      }
      k = (localSecureRandom.nextInt() & j) % 10;
      localStringBuilder.append(k);
      i += 1;
    }
    return localStringBuilder.toString();
  }
  
  final void a(View paramView)
  {
    boolean bool = k();
    if (!bool)
    {
      bool = i;
      if (!bool)
      {
        bool = paramView instanceof NativeVideoView;
        if (bool)
        {
          paramView = (NativeVideoView)paramView;
          bool = true;
          h = bool;
          HashMap localHashMap = new java/util/HashMap;
          localHashMap.<init>();
          String str = "type";
          Object localObject = AdContainer.RenderingProperties.PlacementType.PLACEMENT_TYPE_FULLSCREEN;
          AdContainer.RenderingProperties.PlacementType localPlacementType = getRenderingPropertiesa;
          if (localObject == localPlacementType) {
            localObject = "int";
          } else {
            localObject = "native";
          }
          localHashMap.put(str, localObject);
          localObject = e;
          localHashMap.put("clientRequestId", localObject);
          localObject = d;
          localHashMap.put("impId", localObject);
          com.inmobi.commons.core.e.b.a();
          com.inmobi.commons.core.e.b.a("ads", "ViewableBeaconFired", localHashMap);
          paramView = (bb)paramView.getTag();
          f(paramView);
          return;
        }
      }
    }
  }
  
  final void a(bb parambb)
  {
    boolean bool = i;
    if (bool) {
      return;
    }
    c(f());
    NativeTracker.TrackerEventType localTrackerEventType = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_PAUSE;
    Map localMap = g(parambb);
    parambb.a(localTrackerEventType, localMap);
    g.a(7);
  }
  
  final void a(bb parambb, int paramInt)
  {
    boolean bool = i;
    if (bool) {
      return;
    }
    Object localObject1 = new java/util/HashMap;
    ((HashMap)localObject1).<init>();
    Object localObject2 = String.valueOf(paramInt);
    ((Map)localObject1).put("errorCode", localObject2);
    ((Map)localObject1).put("reason", "Video Player Error");
    String str = parambb.b().b();
    ((Map)localObject1).put("url", str);
    a("VideoError", (Map)localObject1);
    localObject2 = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_ERROR;
    localObject1 = g(parambb);
    parambb.a((NativeTracker.TrackerEventType)localObject2, (Map)localObject1);
    g.a(17);
  }
  
  protected final void b(ag paramag)
  {
    int i = l;
    int j = 4;
    switch (i)
    {
    }
    try
    {
      localObject1 = AdContainer.RenderingProperties.PlacementType.PLACEMENT_TYPE_FULLSCREEN;
      break label852;
      Object localObject4;
      try
      {
        paramag = getVideoContainerView();
        paramag = (NativeVideoWrapper)paramag;
        if (paramag != null)
        {
          localObject1 = paramag.getVideoView();
          localObject1 = ((NativeVideoView)localObject1).getTag();
          localObject1 = (bb)localObject1;
          localObject2 = v;
          localObject3 = "shouldAutoPlay";
          localObject4 = Boolean.TRUE;
          ((Map)localObject2).put(localObject3, localObject4);
          localObject2 = y;
          if (localObject2 != null)
          {
            localObject1 = y;
            localObject1 = v;
            localObject2 = "shouldAutoPlay";
            localObject3 = Boolean.TRUE;
            ((Map)localObject1).put(localObject2, localObject3);
          }
          paramag = paramag.getVideoView();
          paramag.start();
        }
        return;
      }
      catch (Exception paramag)
      {
        paramag.getMessage();
        Logger.a(Logger.InternalLogLevel.DEBUG, "InMobi", "SDK encountered unexpected error in playing video");
        localObject1 = com.inmobi.commons.core.a.a.a();
        localObject2 = new com/inmobi/commons/core/e/a;
        ((com.inmobi.commons.core.e.a)localObject2).<init>(paramag);
        ((com.inmobi.commons.core.a.a)localObject1).a((com.inmobi.commons.core.e.a)localObject2);
        return;
      }
      boolean bool2;
      Object localObject5;
      try
      {
        paramag = AdContainer.RenderingProperties.PlacementType.PLACEMENT_TYPE_INLINE;
        localObject1 = b;
        localObject1 = a;
        if (paramag == localObject1)
        {
          paramag = getVideoContainerView();
          paramag = (NativeVideoWrapper)paramag;
          if (paramag != null)
          {
            paramag = paramag.getVideoView();
            localObject1 = paramag.getTag();
            localObject1 = (bb)localObject1;
            int k = paramag.getState();
            int m = 1;
            if (k != m) {
              try
              {
                bool2 = this.i;
                if (!bool2)
                {
                  localObject3 = this.m;
                  localObject3 = ((WeakReference)localObject3).get();
                  if (localObject3 != null)
                  {
                    localObject3 = v;
                    localObject4 = "didRequestFullScreen";
                    localObject3 = ((Map)localObject3).get(localObject4);
                    localObject3 = (Boolean)localObject3;
                    bool2 = ((Boolean)localObject3).booleanValue();
                    if (!bool2)
                    {
                      localObject3 = v;
                      localObject4 = "didRequestFullScreen";
                      localObject5 = Boolean.TRUE;
                      ((Map)localObject3).put(localObject4, localObject5);
                      localObject3 = v;
                      localObject4 = "seekPosition";
                      int n = paramag.getCurrentPosition();
                      localObject5 = Integer.valueOf(n);
                      ((Map)localObject3).put(localObject4, localObject5);
                      localObject3 = v;
                      localObject4 = "lastMediaVolume";
                      n = paramag.getVolume();
                      localObject5 = Integer.valueOf(n);
                      ((Map)localObject3).put(localObject4, localObject5);
                      localObject3 = paramag.getMediaPlayer();
                      bool2 = ((ar)localObject3).isPlaying();
                      if (bool2)
                      {
                        localObject3 = paramag.getMediaPlayer();
                        ((ar)localObject3).pause();
                      }
                      localObject3 = paramag.getMediaPlayer();
                      a = j;
                      localObject2 = v;
                      localObject3 = "isFullScreen";
                      localObject4 = Boolean.TRUE;
                      ((Map)localObject2).put(localObject3, localObject4);
                      localObject1 = v;
                      localObject2 = "seekPosition";
                      paramag = paramag.getMediaPlayer();
                      int i1 = paramag.getCurrentPosition();
                      paramag = Integer.valueOf(i1);
                      ((Map)localObject1).put(localObject2, paramag);
                      m();
                    }
                    return;
                  }
                }
                return;
              }
              catch (Exception paramag)
              {
                paramag.getMessage();
                localObject1 = com.inmobi.commons.core.a.a.a();
                localObject2 = new com/inmobi/commons/core/e/a;
                ((com.inmobi.commons.core.e.a)localObject2).<init>(paramag);
                ((com.inmobi.commons.core.a.a)localObject1).a((com.inmobi.commons.core.e.a)localObject2);
              }
            }
          }
        }
        return;
      }
      catch (Exception paramag)
      {
        paramag.getMessage();
        Logger.a(Logger.InternalLogLevel.DEBUG, "InMobi", "SDK encountered unexpected error in expanding video to fullscreen");
        localObject1 = com.inmobi.commons.core.a.a.a();
        localObject2 = new com/inmobi/commons/core/e/a;
        ((com.inmobi.commons.core.e.a)localObject2).<init>(paramag);
        ((com.inmobi.commons.core.a.a)localObject1).a((com.inmobi.commons.core.e.a)localObject2);
        return;
      }
      boolean bool3;
      try
      {
        localObject1 = t;
        if (localObject1 != null)
        {
          localObject1 = t;
          localObject2 = "window.imraid.broadcastEvent('replay');";
          ((RenderView)localObject1).c((String)localObject2);
        }
        localObject1 = f();
        if (localObject1 != null)
        {
          localObject1 = f();
          localObject2 = ad.b((View)localObject1);
          if (localObject2 != null) {
            ((NativeTimerView)localObject2).a();
          }
          localObject2 = ((View)localObject1).getParent();
          localObject2 = (ViewGroup)localObject2;
          if (localObject2 != null) {
            ((ViewGroup)localObject2).removeView((View)localObject1);
          }
        }
        localObject1 = "VIDEO";
        paramag = b;
        bool3 = ((String)localObject1).equals(paramag);
        if (!bool3) {
          return;
        }
        paramag = getVideoContainerView();
        paramag = (NativeVideoWrapper)paramag;
        if (paramag != null)
        {
          localObject1 = paramag.getVideoView();
          ((NativeVideoView)localObject1).e();
          paramag = paramag.getVideoView();
          paramag.start();
        }
        return;
      }
      catch (Exception paramag)
      {
        paramag.getMessage();
        Logger.a(Logger.InternalLogLevel.DEBUG, "InMobi", "SDK encountered unexpected error in replaying video");
        localObject1 = com.inmobi.commons.core.a.a.a();
        localObject2 = new com/inmobi/commons/core/e/a;
        ((com.inmobi.commons.core.e.a)localObject2).<init>(paramag);
        ((com.inmobi.commons.core.a.a)localObject1).a((com.inmobi.commons.core.e.a)localObject2);
        return;
      }
      super.b(paramag);
      return;
      return;
      label852:
      Object localObject3 = b;
      localObject3 = a;
      if (localObject1 == localObject3)
      {
        super.b(paramag);
        localObject1 = "VIDEO";
        paramag = b;
        bool3 = ((String)localObject1).equals(paramag);
        if (!bool3) {
          return;
        }
        paramag = getVideoContainerView();
        paramag = (NativeVideoWrapper)paramag;
        if (paramag != null)
        {
          localObject1 = paramag.getVideoView();
          ((NativeVideoView)localObject1).d();
          paramag = paramag.getVideoView();
          boolean bool1 = paramag.b();
          if (bool1)
          {
            localObject1 = c;
            bool1 = ((ar)localObject1).isPlaying();
            if (bool1)
            {
              localObject1 = c;
              ((ar)localObject1).pause();
              localObject1 = c;
              bool2 = false;
              localObject3 = null;
              ((ar)localObject1).seekTo(0);
              localObject1 = paramag.getTag();
              if (localObject1 != null)
              {
                localObject1 = paramag.getTag();
                localObject1 = (bb)localObject1;
                localObject4 = v;
                localObject5 = "didPause";
                Boolean localBoolean = Boolean.TRUE;
                ((Map)localObject4).put(localObject5, localBoolean);
                localObject4 = v;
                localObject5 = "seekPosition";
                localObject3 = Integer.valueOf(0);
                ((Map)localObject4).put(localObject5, localObject3);
                localObject1 = v;
                localObject3 = "didCompleteQ4";
                localObject4 = Boolean.TRUE;
                ((Map)localObject1).put(localObject3, localObject4);
              }
              localObject1 = c;
              a = j;
              localObject1 = paramag.getPlaybackEventListener();
              ((NativeVideoView.b)localObject1).a(j);
            }
          }
          localObject1 = c;
          if (localObject1 != null)
          {
            paramag = c;
            b = j;
          }
        }
        return;
      }
      paramag = e();
      if (paramag != null) {
        paramag.i();
      }
      return;
    }
    catch (Exception paramag)
    {
      Object localObject1 = com.inmobi.commons.core.a.a.a();
      Object localObject2 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject2).<init>(paramag);
      ((com.inmobi.commons.core.a.a)localObject1).a((com.inmobi.commons.core.e.a)localObject2);
    }
  }
  
  final void b(bb parambb)
  {
    boolean bool = i;
    if (bool) {
      return;
    }
    d(f());
    NativeTracker.TrackerEventType localTrackerEventType = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_RESUME;
    Map localMap = g(parambb);
    parambb.a(localTrackerEventType, localMap);
    g.a(8);
  }
  
  final void b(bb parambb, int paramInt)
  {
    boolean bool = i;
    if (bool) {
      return;
    }
    Object localObject1;
    Object localObject2;
    switch (paramInt)
    {
    default: 
      break;
    case 3: 
      localObject1 = v;
      localObject2 = "didQ4Fire";
      localObject1 = (Boolean)((Map)localObject1).get(localObject2);
      paramInt = ((Boolean)localObject1).booleanValue();
      if (paramInt == 0) {
        e(parambb);
      }
      break;
    case 2: 
      localObject1 = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_Q3;
      localObject2 = g(parambb);
      parambb.a((NativeTracker.TrackerEventType)localObject1, (Map)localObject2);
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      parambb = parambb.b().b();
      ((Map)localObject1).put("url", parambb);
      ((Map)localObject1).put("isCached", "1");
      a("VideoQ3Completed", (Map)localObject1);
      g.a(11);
      return;
    case 1: 
      localObject1 = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_Q2;
      localObject2 = g(parambb);
      parambb.a((NativeTracker.TrackerEventType)localObject1, (Map)localObject2);
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      parambb = parambb.b().b();
      ((Map)localObject1).put("url", parambb);
      ((Map)localObject1).put("isCached", "1");
      a("VideoQ2Completed", (Map)localObject1);
      g.a(10);
      return;
    case 0: 
      localObject1 = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_Q1;
      localObject2 = g(parambb);
      parambb.a((NativeTracker.TrackerEventType)localObject1, (Map)localObject2);
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      parambb = parambb.b().b();
      ((Map)localObject1).put("url", parambb);
      ((Map)localObject1).put("isCached", "1");
      a("VideoQ1Completed", (Map)localObject1);
      g.a(9);
      return;
    }
  }
  
  final void c(bb parambb)
  {
    boolean bool = i;
    if (bool) {
      return;
    }
    Object localObject = v;
    Integer localInteger = Integer.valueOf(0);
    ((Map)localObject).put("lastMediaVolume", localInteger);
    localObject = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_MUTE;
    Map localMap = g(parambb);
    parambb.a((NativeTracker.TrackerEventType)localObject, localMap);
    g.a(13);
  }
  
  final void d(bb parambb)
  {
    boolean bool = i;
    if (bool) {
      return;
    }
    Object localObject = v;
    Integer localInteger = Integer.valueOf(15);
    ((Map)localObject).put("lastMediaVolume", localInteger);
    localObject = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_UNMUTE;
    Map localMap = g(parambb);
    parambb.a((NativeTracker.TrackerEventType)localObject, localMap);
    g.a(14);
  }
  
  public void destroy()
  {
    boolean bool = i;
    if (bool) {
      return;
    }
    Object localObject = getVideoContainerView();
    if (localObject != null)
    {
      localObject = (NativeVideoWrapper)getVideoContainerView();
      if (localObject != null)
      {
        localObject = ((NativeVideoWrapper)localObject).getVideoView();
        ((NativeVideoView)localObject).c();
      }
    }
    super.destroy();
  }
  
  final void e(bb parambb)
  {
    Object localObject1 = v;
    Object localObject2 = Boolean.TRUE;
    ((Map)localObject1).put("didQ4Fire", localObject2);
    localObject1 = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_Q4;
    Map localMap = g(parambb);
    parambb.a((NativeTracker.TrackerEventType)localObject1, localMap);
    g.a(12);
    localObject1 = new java/util/HashMap;
    ((HashMap)localObject1).<init>();
    localObject2 = parambb.b().b();
    ((Map)localObject1).put("url", localObject2);
    ((Map)localObject1).put("isCached", "1");
    parambb = Integer.valueOf(E);
    ((Map)localObject1).put("completeAfter", parambb);
    a("VideoQ4Completed", (Map)localObject1);
  }
  
  public AdContainer.a getFullScreenEventsListener()
  {
    return B;
  }
  
  public View getVideoContainerView()
  {
    WeakReference localWeakReference = y;
    if (localWeakReference == null) {
      return null;
    }
    return (View)localWeakReference.get();
  }
  
  public bw getViewableAd()
  {
    Object localObject1 = j();
    Object localObject2 = g;
    if ((localObject2 == null) && (localObject1 != null))
    {
      g();
      localObject2 = new com/inmobi/ads/w;
      Object localObject3 = new com/inmobi/ads/bz;
      ((bz)localObject3).<init>(this);
      ((w)localObject2).<init>(this, (bw)localObject3);
      g = ((bw)localObject2);
      localObject2 = f;
      if (localObject2 != null)
      {
        boolean bool1 = localObject1 instanceof Activity;
        if (bool1) {
          try
          {
            localObject1 = (Activity)localObject1;
            localObject2 = f;
            localObject2 = ((Set)localObject2).iterator();
            for (;;)
            {
              boolean bool2 = ((Iterator)localObject2).hasNext();
              if (!bool2) {
                break;
              }
              localObject3 = ((Iterator)localObject2).next();
              localObject3 = (bm)localObject3;
              int i = a;
              int j = 1;
              Object localObject4;
              Object localObject5;
              if (i != j)
              {
                j = 3;
                if (i == j)
                {
                  localObject3 = b;
                  localObject4 = "avidAdSession";
                  localObject3 = ((Map)localObject3).get(localObject4);
                  localObject3 = (d)localObject3;
                  if (localObject3 != null)
                  {
                    localObject4 = new com/inmobi/ads/r;
                    localObject5 = g;
                    ((r)localObject4).<init>((Activity)localObject1, (bw)localObject5, this, (d)localObject3);
                    g = ((bw)localObject4);
                  }
                }
              }
              else
              {
                localObject4 = g;
                localObject3 = b;
                localObject5 = a;
                Object localObject6 = "VIDEO";
                localObject5 = ((ak)localObject5).c((String)localObject6);
                localObject6 = null;
                localObject5 = ((List)localObject5).get(0);
                localObject5 = (bb)localObject5;
                localObject6 = new java/lang/StringBuilder;
                ((StringBuilder)localObject6).<init>();
                localObject5 = u;
                localObject5 = ((List)localObject5).iterator();
                for (;;)
                {
                  boolean bool3 = ((Iterator)localObject5).hasNext();
                  if (!bool3) {
                    break;
                  }
                  Object localObject7 = ((Iterator)localObject5).next();
                  localObject7 = (NativeTracker)localObject7;
                  NativeTracker.TrackerEventType localTrackerEventType1 = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_MOAT;
                  NativeTracker.TrackerEventType localTrackerEventType2 = b;
                  if (localTrackerEventType1 == localTrackerEventType2)
                  {
                    localObject7 = a;
                    ((StringBuilder)localObject6).append((String)localObject7);
                  }
                }
                j = ((StringBuilder)localObject6).length();
                if (j > 0)
                {
                  localObject5 = "zMoatVASTIDs";
                  localObject6 = ((StringBuilder)localObject6).toString();
                  ((Map)localObject3).put(localObject5, localObject6);
                }
                localObject5 = new com/inmobi/ads/ab;
                ((ab)localObject5).<init>((Activity)localObject1, (bw)localObject4, this, (Map)localObject3);
                g = ((bw)localObject5);
              }
            }
            localHashMap = new java/util/HashMap;
          }
          catch (Exception localException)
          {
            localException.getMessage();
            localObject2 = com.inmobi.commons.core.a.a.a();
            localObject3 = new com/inmobi/commons/core/e/a;
            ((com.inmobi.commons.core.e.a)localObject3).<init>(localException);
            ((com.inmobi.commons.core.a.a)localObject2).a((com.inmobi.commons.core.e.a)localObject3);
          }
        }
        HashMap localHashMap;
        localHashMap.<init>();
        localHashMap.put("type", "native");
        localObject3 = d;
        localHashMap.put("impId", localObject3);
        com.inmobi.commons.core.e.b.a();
        localObject2 = "ads";
        localObject3 = "TrackersForService";
        com.inmobi.commons.core.e.b.a((String)localObject2, (String)localObject3, localHashMap);
      }
    }
    return g;
  }
  
  final boolean i()
  {
    Object localObject = AdContainer.RenderingProperties.PlacementType.PLACEMENT_TYPE_INLINE;
    AdContainer.RenderingProperties.PlacementType localPlacementType = b.a;
    if (localObject == localPlacementType)
    {
      localObject = l();
      if (localObject != null) {
        return true;
      }
    }
    return false;
  }
  
  final boolean n()
  {
    boolean bool = o;
    return !bool;
  }
  
  final void q()
  {
    super.q();
    Object localObject = (NativeVideoWrapper)getVideoContainerView();
    if (localObject != null)
    {
      localObject = ((NativeVideoWrapper)localObject).getVideoView();
      ((NativeVideoView)localObject).pause();
    }
  }
  
  final void w()
  {
    g.a(5);
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.ba
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */