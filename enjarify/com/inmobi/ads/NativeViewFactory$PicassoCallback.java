package com.inmobi.ads;

import android.content.Context;
import android.widget.ImageView;
import com.d.b.e;
import java.lang.ref.WeakReference;

class NativeViewFactory$PicassoCallback
  implements e
{
  private WeakReference a;
  private WeakReference b;
  private ag c;
  
  NativeViewFactory$PicassoCallback(Context paramContext, ImageView paramImageView, ag paramag)
  {
    WeakReference localWeakReference = new java/lang/ref/WeakReference;
    localWeakReference.<init>(paramContext);
    a = localWeakReference;
    paramContext = new java/lang/ref/WeakReference;
    paramContext.<init>(paramImageView);
    b = paramContext;
    c = paramag;
  }
  
  public void onError()
  {
    Context localContext = (Context)a.get();
    ImageView localImageView = (ImageView)b.get();
    ag localag = c;
    NativeViewFactory.a(localContext, localImageView, localag);
  }
  
  public void onSuccess() {}
}

/* Location:
 * Qualified Name:     com.inmobi.ads.NativeViewFactory.PicassoCallback
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */