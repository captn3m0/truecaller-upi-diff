package com.inmobi.ads;

import android.app.Activity;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.view.Window;
import java.lang.ref.WeakReference;

final class p
  extends ca
{
  private static final String d = "p";
  private ViewTreeObserver.OnPreDrawListener e;
  private final WeakReference f;
  
  p(ca.a parama, Activity paramActivity)
  {
    super(parama);
    parama = paramActivity.getWindow().getDecorView();
    paramActivity = new java/lang/ref/WeakReference;
    paramActivity.<init>(parama);
    f = paramActivity;
    parama = parama.getViewTreeObserver();
    boolean bool = parama.isAlive();
    if (bool)
    {
      paramActivity = new com/inmobi/ads/p$1;
      paramActivity.<init>(this);
      e = paramActivity;
      paramActivity = e;
      parama.addOnPreDrawListener(paramActivity);
    }
  }
  
  private void h()
  {
    Object localObject = (View)f.get();
    if (localObject != null)
    {
      localObject = ((View)localObject).getViewTreeObserver();
      boolean bool = ((ViewTreeObserver)localObject).isAlive();
      if (bool)
      {
        ViewTreeObserver.OnPreDrawListener localOnPreDrawListener = e;
        ((ViewTreeObserver)localObject).removeOnPreDrawListener(localOnPreDrawListener);
      }
    }
  }
  
  protected final int a()
  {
    return 100;
  }
  
  protected final void b() {}
  
  public final void c()
  {
    boolean bool = a;
    if (!bool)
    {
      h();
      super.c();
    }
  }
  
  public final void d()
  {
    boolean bool1 = a;
    if (bool1)
    {
      Object localObject = (View)f.get();
      if (localObject != null)
      {
        localObject = ((View)localObject).getViewTreeObserver();
        boolean bool2 = ((ViewTreeObserver)localObject).isAlive();
        if (bool2)
        {
          ViewTreeObserver.OnPreDrawListener localOnPreDrawListener = e;
          ((ViewTreeObserver)localObject).addOnPreDrawListener(localOnPreDrawListener);
        }
      }
      super.d();
    }
  }
  
  protected final void e()
  {
    h();
    super.e();
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */