package com.inmobi.ads;

import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;

final class g$2
  implements Runnable
{
  g$2(g paramg, j paramj) {}
  
  public final void run()
  {
    try
    {
      j localj = a;
      localj.r();
      return;
    }
    catch (Exception localException)
    {
      g.c();
      localException.getMessage();
      Logger.a(Logger.InternalLogLevel.DEBUG, "InMobi", "SDK encountered an unexpected error clearing an old ad");
      com.inmobi.commons.core.a.a locala = com.inmobi.commons.core.a.a.a();
      com.inmobi.commons.core.e.a locala1 = new com/inmobi/commons/core/e/a;
      locala1.<init>(localException);
      locala.a(locala1);
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.g.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */