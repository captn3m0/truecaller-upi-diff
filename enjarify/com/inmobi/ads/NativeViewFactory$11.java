package com.inmobi.ads;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

final class NativeViewFactory$11
  extends NativeViewFactory.c
{
  NativeViewFactory$11(NativeViewFactory paramNativeViewFactory)
  {
    super(paramNativeViewFactory);
  }
  
  protected final View a(Context paramContext)
  {
    NativeVideoWrapper localNativeVideoWrapper = new com/inmobi/ads/NativeVideoWrapper;
    paramContext = paramContext.getApplicationContext();
    localNativeVideoWrapper.<init>(paramContext);
    return localNativeVideoWrapper;
  }
  
  protected final void a(View paramView, ag paramag, b paramb)
  {
    super.a(paramView, paramag, paramb);
    NativeViewFactory.a((NativeVideoWrapper)paramView, paramag);
  }
  
  public final boolean a(View paramView)
  {
    boolean bool = paramView instanceof NativeVideoWrapper;
    if (!bool) {
      return false;
    }
    Object localObject = paramView;
    localObject = (NativeVideoWrapper)paramView;
    ((NativeVideoWrapper)localObject).getProgressBar().setVisibility(8);
    ((NativeVideoWrapper)localObject).getPoster().setImageBitmap(null);
    ((NativeVideoWrapper)localObject).getVideoView().a();
    return super.a((View)paramView);
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.NativeViewFactory.11
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */