package com.inmobi.ads;

import android.os.Handler;
import android.os.Message;
import java.lang.ref.WeakReference;

final class NativeVideoController$a
  extends Handler
{
  private final WeakReference a;
  
  NativeVideoController$a(NativeVideoController paramNativeVideoController)
  {
    WeakReference localWeakReference = new java/lang/ref/WeakReference;
    localWeakReference.<init>(paramNativeVideoController);
    a = localWeakReference;
  }
  
  public final void handleMessage(Message paramMessage)
  {
    int i = what;
    int j = 2;
    if (i != j)
    {
      super.handleMessage(paramMessage);
      return;
    }
    paramMessage = (NativeVideoController)a.get();
    if (paramMessage != null)
    {
      NativeVideoController.a(paramMessage);
      boolean bool1 = a;
      if (bool1)
      {
        paramMessage = NativeVideoController.b(paramMessage);
        boolean bool2 = paramMessage.isPlaying();
        if (bool2)
        {
          paramMessage = obtainMessage(j);
          long l = 200L;
          sendMessageDelayed(paramMessage, l);
        }
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.NativeVideoController.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */