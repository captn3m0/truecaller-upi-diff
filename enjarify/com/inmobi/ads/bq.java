package com.inmobi.ads;

import android.os.SystemClock;
import android.webkit.URLUtil;
import com.inmobi.commons.core.network.e;
import com.inmobi.signals.o;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class bq
{
  private static final String a = "bq";
  private static final Map d;
  private b.j b;
  private final String c = "Progressive";
  private int e = 0;
  private bt f;
  
  static
  {
    Object localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    d = (Map)localObject;
    NativeTracker.TrackerEventType localTrackerEventType = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_ERROR;
    ((Map)localObject).put("Error", localTrackerEventType);
    localObject = d;
    localTrackerEventType = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_RENDER;
    ((Map)localObject).put("Impression", localTrackerEventType);
    localObject = d;
    localTrackerEventType = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_CLICK;
    ((Map)localObject).put("ClickTracking", localTrackerEventType);
    localObject = d;
    localTrackerEventType = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_CREATIVE_VIEW;
    ((Map)localObject).put("creativeView", localTrackerEventType);
    localObject = d;
    localTrackerEventType = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_PLAY;
    ((Map)localObject).put("start", localTrackerEventType);
    localObject = d;
    localTrackerEventType = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_Q1;
    ((Map)localObject).put("firstQuartile", localTrackerEventType);
    localObject = d;
    localTrackerEventType = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_Q2;
    ((Map)localObject).put("midpoint", localTrackerEventType);
    localObject = d;
    localTrackerEventType = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_Q3;
    ((Map)localObject).put("thirdQuartile", localTrackerEventType);
    localObject = d;
    localTrackerEventType = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_Q4;
    ((Map)localObject).put("complete", localTrackerEventType);
    localObject = d;
    localTrackerEventType = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_MUTE;
    ((Map)localObject).put("mute", localTrackerEventType);
    localObject = d;
    localTrackerEventType = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_UNMUTE;
    ((Map)localObject).put("unmute", localTrackerEventType);
    localObject = d;
    localTrackerEventType = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_PAUSE;
    ((Map)localObject).put("pause", localTrackerEventType);
    localObject = d;
    localTrackerEventType = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_RESUME;
    ((Map)localObject).put("resume", localTrackerEventType);
    localObject = d;
    localTrackerEventType = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_FULLSCREEN;
    ((Map)localObject).put("fullscreen", localTrackerEventType);
    localObject = d;
    localTrackerEventType = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_EXIT_FULLSCREEN;
    ((Map)localObject).put("exitFullscreen", localTrackerEventType);
  }
  
  public bq(b.j paramj)
  {
    b = paramj;
    paramj = new com/inmobi/ads/bt;
    b.j localj = b;
    paramj.<init>(localj);
    f = paramj;
  }
  
  static String a(Node paramNode)
  {
    if (paramNode == null) {
      return null;
    }
    try
    {
      paramNode = paramNode.getTextContent();
    }
    catch (DOMException paramNode)
    {
      paramNode.getMessage();
      com.inmobi.commons.core.a.a locala = com.inmobi.commons.core.a.a.a();
      com.inmobi.commons.core.e.a locala1 = new com/inmobi/commons/core/e/a;
      locala1.<init>(paramNode);
      locala.a(locala1);
      paramNode = null;
    }
    if (paramNode != null) {
      return paramNode.trim();
    }
    return null;
  }
  
  static List a(Document paramDocument, String paramString)
  {
    if (paramDocument == null) {
      return null;
    }
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    paramDocument = paramDocument.getElementsByTagName(paramString);
    int i = 0;
    paramString = null;
    for (;;)
    {
      int j = paramDocument.getLength();
      if (i >= j) {
        break;
      }
      Node localNode = paramDocument.item(i);
      localArrayList.add(localNode);
      i += 1;
    }
    int k = paramDocument.getLength();
    if (k == 0) {
      return null;
    }
    return localArrayList;
  }
  
  private static Node a(List paramList, String paramString)
  {
    Node localNode = null;
    if (paramList == null) {
      return null;
    }
    int i = 0;
    for (;;)
    {
      int j = paramList.size();
      if (i >= j) {
        break;
      }
      localNode = a((Node)paramList.get(i), paramString);
      if (localNode != null) {
        break;
      }
      i += 1;
    }
    return localNode;
  }
  
  static Node a(Node paramNode, String paramString)
  {
    paramNode = b(paramNode, paramString);
    int i = paramNode.size();
    if (i > 0)
    {
      i = 0;
      paramString = null;
      paramNode = (Node)paramNode.get(0);
    }
    else
    {
      paramNode = null;
    }
    return paramNode;
  }
  
  private void a(int paramInt)
  {
    Object localObject1 = f;
    f = paramInt;
    localObject1 = d.iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject1).hasNext();
      if (!bool) {
        break;
      }
      Object localObject2 = (NativeTracker)((Iterator)localObject1).next();
      Object localObject3 = b;
      Object localObject4 = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_ERROR;
      if (localObject3 == localObject4)
      {
        localObject3 = com.inmobi.rendering.a.c.a();
        localObject4 = a;
        HashMap localHashMap = new java/util/HashMap;
        localHashMap.<init>();
        String str1 = "[ERRORCODE]";
        String str2 = String.valueOf(paramInt);
        localHashMap.put(str1, str2);
        localObject4 = com.inmobi.commons.core.utilities.d.a((String)localObject4, localHashMap);
        localObject2 = c;
        ((com.inmobi.rendering.a.c)localObject3).a((String)localObject4, (Map)localObject2);
      }
    }
  }
  
  private void a(List paramList)
  {
    if (paramList != null)
    {
      int i = paramList.size();
      if (i != 0)
      {
        paramList = paramList.iterator();
        for (;;)
        {
          boolean bool1 = paramList.hasNext();
          if (!bool1) {
            break;
          }
          Node localNode = (Node)paramList.next();
          Object localObject = localNode;
          localObject = ((Element)localNode).getAttribute("event");
          Map localMap = d;
          boolean bool2 = localMap.containsKey(localObject);
          if (bool2)
          {
            localMap = d;
            localObject = (NativeTracker.TrackerEventType)localMap.get(localObject);
            a((NativeTracker.TrackerEventType)localObject, localNode);
          }
        }
        return;
      }
    }
  }
  
  private static void a(List paramList, bp parambp)
  {
    if (paramList != null)
    {
      int i = 0;
      for (;;)
      {
        int j = paramList.size();
        if (i >= j) {
          break;
        }
        Object localObject = (Node)paramList.get(i);
        int k = ((Node)localObject).getNodeType();
        int m = 1;
        if (m == k)
        {
          localObject = a((Node)localObject);
          boolean bool = URLUtil.isValidUrl((String)localObject);
          if (bool)
          {
            NativeTracker localNativeTracker = new com/inmobi/ads/NativeTracker;
            NativeTracker.TrackerEventType localTrackerEventType = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_CLICK;
            localNativeTracker.<init>((String)localObject, 0, localTrackerEventType, null);
            parambp.a(localNativeTracker);
          }
        }
        i += 1;
      }
    }
  }
  
  private boolean a(NativeTracker.TrackerEventType paramTrackerEventType, List paramList)
  {
    int i = 1;
    if (paramList != null)
    {
      int j = 0;
      for (;;)
      {
        int k = paramList.size();
        if (j >= k) {
          break;
        }
        Node localNode = (Node)paramList.get(j);
        int m = localNode.getNodeType();
        if (m == i)
        {
          boolean bool = a(paramTrackerEventType, localNode);
          if (!bool) {
            return false;
          }
        }
        j += 1;
      }
    }
    return i;
  }
  
  private boolean a(NativeTracker.TrackerEventType paramTrackerEventType, Node paramNode)
  {
    paramNode = a(paramNode);
    boolean bool1 = URLUtil.isValidUrl(paramNode);
    boolean bool2 = true;
    if (!bool1)
    {
      paramNode = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_RENDER;
      if (paramTrackerEventType != paramNode) {
        return bool2;
      }
      return false;
    }
    NativeTracker localNativeTracker = new com/inmobi/ads/NativeTracker;
    localNativeTracker.<init>(paramNode, 0, paramTrackerEventType, null);
    f.a(localNativeTracker);
    return bool2;
  }
  
  private static com.inmobi.commons.core.network.d b(String paramString)
  {
    Object localObject1 = new com/inmobi/commons/core/network/c;
    Object localObject2 = "GET";
    ((com.inmobi.commons.core.network.c)localObject1).<init>((String)localObject2, paramString);
    t = false;
    boolean bool = true;
    r = bool;
    long l1 = SystemClock.elapsedRealtime();
    paramString = new com/inmobi/commons/core/network/e;
    paramString.<init>((com.inmobi.commons.core.network.c)localObject1);
    paramString = paramString.a();
    try
    {
      o localo = o.a();
      long l2 = ((com.inmobi.commons.core.network.c)localObject1).e();
      localo.a(l2);
      localObject1 = o.a();
      long l3 = paramString.c();
      ((o)localObject1).b(l3);
      localObject1 = o.a();
      l3 = SystemClock.elapsedRealtime() - l1;
      ((o)localObject1).c(l3);
    }
    catch (Exception localException)
    {
      localException.getMessage();
      localObject2 = com.inmobi.commons.core.a.a.a();
      com.inmobi.commons.core.e.a locala = new com/inmobi/commons/core/e/a;
      locala.<init>(localException);
      ((com.inmobi.commons.core.a.a)localObject2).a(locala);
    }
    return paramString;
  }
  
  private static List b(Node paramNode, String paramString)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    if ((paramNode != null) && (paramString != null))
    {
      paramNode = paramNode.getChildNodes();
      int i = 0;
      for (;;)
      {
        int j = paramNode.getLength();
        if (i >= j) {
          break;
        }
        Node localNode = paramNode.item(i);
        int k = localNode.getNodeType();
        int m = 1;
        if (k == m)
        {
          String str = localNode.getNodeName();
          boolean bool = paramString.equals(str);
          if (bool) {
            localArrayList.add(localNode);
          }
        }
        i += 1;
      }
      return localArrayList;
    }
    return localArrayList;
  }
  
  private boolean b(Node paramNode)
  {
    NativeTracker.TrackerEventType localTrackerEventType1 = null;
    if (paramNode == null) {
      return false;
    }
    Object localObject = b(paramNode, "Error");
    NativeTracker.TrackerEventType localTrackerEventType2 = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_ERROR;
    a(localTrackerEventType2, (List)localObject);
    localObject = "Impression";
    paramNode = b(paramNode, (String)localObject);
    boolean bool = paramNode.isEmpty();
    if (bool) {
      return false;
    }
    localTrackerEventType1 = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_RENDER;
    return a(localTrackerEventType1, paramNode);
  }
  
  private static int c(String paramString)
  {
    int i;
    try
    {
      i = Integer.parseInt(paramString);
    }
    catch (NumberFormatException paramString)
    {
      com.inmobi.commons.core.a.a locala = com.inmobi.commons.core.a.a.a();
      com.inmobi.commons.core.e.a locala1 = new com/inmobi/commons/core/e/a;
      locala1.<init>(paramString);
      locala.a(locala1);
      i = 0;
      paramString = null;
    }
    return i;
  }
  
  private static String c(Node paramNode, String paramString)
  {
    return a(a(paramNode, paramString));
  }
  
  private bt d(String paramString)
  {
    if (paramString != null)
    {
      boolean bool = paramString.isEmpty();
      if (!bool)
      {
        paramString = b(paramString);
        bool = paramString.a();
        if (bool)
        {
          int i = 301;
          a(i);
        }
        else
        {
          paramString = paramString.b();
          a(paramString);
        }
        return f;
      }
    }
    a(300);
    return f;
  }
  
  private static boolean e(String paramString)
  {
    ArrayList localArrayList = bp.f;
    int i = localArrayList.size();
    int j = 0;
    while (j < i)
    {
      String str = (String)bp.f.get(j);
      boolean bool = paramString.equalsIgnoreCase(str);
      if (bool) {
        return true;
      }
      j += 1;
    }
    return false;
  }
  
  public final bt a(String paramString)
  {
    int i = 303;
    if (paramString != null)
    {
      boolean bool1 = paramString.isEmpty();
      if (!bool1)
      {
        try
        {
          localObject1 = new java/io/StringReader;
          ((StringReader)localObject1).<init>(paramString);
          paramString = new org/xml/sax/InputSource;
          paramString.<init>((Reader)localObject1);
          localObject1 = DocumentBuilderFactory.newInstance();
          localObject1 = ((DocumentBuilderFactory)localObject1).newDocumentBuilder();
          paramString = ((DocumentBuilder)localObject1).parse(paramString);
          localObject1 = a(paramString, "VAST");
          int m = 101;
          if (localObject1 == null)
          {
            a(m);
            return f;
          }
          Object localObject2 = "Ad";
          localObject1 = a((Node)localObject1, (String)localObject2);
          if (localObject1 == null)
          {
            a(i);
            return f;
          }
          localObject3 = a((Node)localObject1, "Wrapper");
          int n = 1;
          boolean bool2;
          if (localObject3 != null)
          {
            int j = e + n;
            e = j;
            j = e;
            localObject2 = b;
            n = a;
            if (j > n)
            {
              a(302);
              return f;
            }
            bool2 = b((Node)localObject3);
            if (!bool2)
            {
              a(m);
              return f;
            }
            localObject1 = a(paramString, "TrackingEvents");
            if (localObject1 != null)
            {
              localObject1 = (Node)((List)localObject1).get(0);
              localObject2 = "Tracking";
              localObject1 = b((Node)localObject1, (String)localObject2);
              a((List)localObject1);
            }
            localObject1 = f;
            s.a(paramString, (bt)localObject1);
            localObject1 = f;
            ac.a(paramString, (bt)localObject1);
            paramString = a(paramString, "ClickTracking");
            localObject1 = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_CLICK;
            a((NativeTracker.TrackerEventType)localObject1, paramString);
            paramString = c((Node)localObject3, "VASTAdTagURI");
            if (paramString == null)
            {
              a(m);
              return f;
            }
            d(paramString);
          }
          else
          {
            localObject3 = a((Node)localObject1, "InLine");
            if (localObject3 == null)
            {
              a(m);
              return f;
            }
            bool2 = b((Node)localObject3);
            if (!bool2)
            {
              a(m);
              return f;
            }
            localObject1 = "Creatives";
            localObject3 = a((Node)localObject3, (String)localObject1);
            if (localObject3 == null)
            {
              a(m);
              return f;
            }
            localObject1 = b((Node)localObject3, "Creative");
            boolean bool4 = ((List)localObject1).isEmpty();
            if (bool4)
            {
              a(m);
              return f;
            }
            Object localObject4 = "Linear";
            localObject1 = a((List)localObject1, (String)localObject4);
            if (localObject1 == null)
            {
              a(201);
              return f;
            }
            localObject4 = b((Node)localObject1, "Duration");
            int i1 = ((List)localObject4).isEmpty();
            if (i1 != 0)
            {
              a(m);
              return f;
            }
            localObject4 = a((Node)((List)localObject4).get(0));
            if (localObject4 == null) {
              break label2123;
            }
            i1 = ((String)localObject4).isEmpty();
            if (i1 != 0) {
              break label2123;
            }
            Object localObject5 = "\\d*:[0-5][0-9]:[0-5][0-9](:[0-9][0-9][0-9])?";
            i1 = ((String)localObject4).matches((String)localObject5);
            if (i1 == 0) {
              break label2123;
            }
            localObject5 = f;
            b = ((String)localObject4);
            localObject4 = a((Node)localObject1, "MediaFiles");
            if (localObject4 == null)
            {
              a(m);
              return f;
            }
            localObject5 = a((Node)localObject1, "VideoClicks");
            Object localObject6 = c((Node)localObject5, "ClickThrough");
            Object localObject7 = f;
            c = ((String)localObject6);
            localObject5 = b((Node)localObject5, "ClickTracking");
            localObject6 = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_CLICK;
            a((NativeTracker.TrackerEventType)localObject6, (List)localObject5);
            localObject1 = a((Node)localObject1, "TrackingEvents");
            localObject5 = "Tracking";
            localObject1 = b((Node)localObject1, (String)localObject5);
            a((List)localObject1);
            localObject1 = f;
            s.a(paramString, (bt)localObject1);
            localObject1 = f;
            ac.a(paramString, (bt)localObject1);
            localObject1 = b((Node)localObject4, "MediaFile");
            bool4 = ((List)localObject1).isEmpty();
            if (bool4)
            {
              a(401);
              return f;
            }
            localObject4 = b.d;
            i1 = 0;
            localObject5 = null;
            Object localObject8;
            boolean bool9;
            String str1;
            Object localObject9;
            int i4;
            Object localObject10;
            int i2;
            for (;;)
            {
              int i3 = ((List)localObject1).size();
              if (i1 >= i3) {
                break;
              }
              localObject6 = (Element)((List)localObject1).get(i1);
              localObject7 = a((Node)localObject6);
              if (localObject7 != null)
              {
                localObject8 = ((String)localObject7).trim();
                bool9 = ((String)localObject8).isEmpty();
                if (!bool9)
                {
                  localObject8 = ((Element)localObject6).getAttribute("delivery");
                  str1 = ((Element)localObject6).getAttribute("type");
                  localObject9 = "bitrate";
                  localObject6 = ((Element)localObject6).getAttribute((String)localObject9);
                  i4 = c((String)localObject6);
                  boolean bool11 = a;
                  if (((!bool11) || (i4 > 0)) && (localObject8 != null))
                  {
                    localObject9 = ((String)localObject8).trim();
                    localObject10 = "Progressive";
                    bool11 = ((String)localObject9).equalsIgnoreCase((String)localObject10);
                    if (bool11)
                    {
                      localObject9 = b.e;
                      if (str1 != null)
                      {
                        int i8 = 0;
                        localObject10 = null;
                        for (;;)
                        {
                          int i9 = ((List)localObject9).size();
                          if (i8 >= i9) {
                            break;
                          }
                          String str2 = (String)((List)localObject9).get(i8);
                          boolean bool12 = str1.equalsIgnoreCase(str2);
                          if (bool12)
                          {
                            localObject9 = f;
                            localObject10 = new com/inmobi/ads/br;
                            ((br)localObject10).<init>((String)localObject7, (String)localObject8, str1, i4);
                            localObject6 = a;
                            ((List)localObject6).add(localObject10);
                            break;
                          }
                          i8 += 1;
                        }
                      }
                    }
                  }
                }
              }
              i1 += 1;
            }
            localObject1 = f.a;
            bool2 = ((List)localObject1).isEmpty();
            if (bool2)
            {
              int k = 403;
              a(k);
            }
            localObject1 = "Creative";
            localObject3 = b((Node)localObject3, (String)localObject1);
            boolean bool3 = ((List)localObject3).isEmpty();
            if (bool3)
            {
              a(m);
              return f;
            }
            localObject1 = "CompanionAds";
            localObject3 = a((List)localObject3, (String)localObject1);
            if (localObject3 == null) {
              return f;
            }
            paramString = a(paramString, "CompanionAdTracking");
            localObject1 = new java/util/HashMap;
            ((HashMap)localObject1).<init>();
            m = 0;
            if (paramString != null)
            {
              paramString = paramString.iterator();
              do
              {
                bool4 = paramString.hasNext();
                if (!bool4) {
                  break;
                }
                localObject4 = a((Node)paramString.next(), "TrackingEvents");
                localObject5 = ((Node)localObject4).getAttributes();
                if (localObject5 != null)
                {
                  i4 = ((NamedNodeMap)localObject5).getLength();
                  if (i4 > 0)
                  {
                    localObject6 = "id";
                    localObject5 = ((NamedNodeMap)localObject5).getNamedItem((String)localObject6);
                    continue;
                  }
                }
                i2 = 0;
                localObject5 = null;
              } while (localObject5 == null);
              localObject5 = ((Node)localObject5).getNodeValue();
              localObject6 = "Tracking";
              localObject4 = b((Node)localObject4, (String)localObject6).iterator();
              for (;;)
              {
                boolean bool6 = ((Iterator)localObject4).hasNext();
                if (!bool6) {
                  break;
                }
                localObject6 = (Node)((Iterator)localObject4).next();
                localObject7 = localObject6;
                localObject7 = ((Element)localObject6).getAttribute("event");
                localObject8 = "closeEndCard";
                boolean bool13 = ((String)localObject8).equals(localObject7);
                if (bool13)
                {
                  localObject6 = a((Node)localObject6);
                  bool13 = URLUtil.isValidUrl((String)localObject6);
                  if (bool13)
                  {
                    localObject7 = new com/inmobi/ads/NativeTracker;
                    localObject8 = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_END_CARD_CLOSE;
                    ((NativeTracker)localObject7).<init>((String)localObject6, 0, (NativeTracker.TrackerEventType)localObject8, null);
                    ((Map)localObject1).put(localObject5, localObject7);
                  }
                }
              }
            }
            paramString = b((Node)localObject3, "Companion");
            i = 0;
            localObject3 = null;
            bool4 = false;
            localObject4 = null;
            for (;;)
            {
              i2 = paramString.size();
              if (i >= i2) {
                break;
              }
              localObject5 = (Element)paramString.get(i);
              localObject6 = ((Element)localObject5).getAttribute("width");
              int i5 = c((String)localObject6);
              localObject7 = ((Element)localObject5).getAttribute("height");
              int i10 = c((String)localObject7);
              if ((i5 != 0) && (i10 != 0))
              {
                localObject8 = c((Node)localObject5, "CompanionClickThrough");
                boolean bool14 = URLUtil.isValidUrl((String)localObject8);
                if (!bool14)
                {
                  bool9 = false;
                  localObject8 = null;
                }
                str1 = "id";
                bool14 = ((Element)localObject5).hasAttribute(str1);
                if (bool14)
                {
                  str1 = ((Element)localObject5).getAttribute("id");
                }
                else
                {
                  bool14 = false;
                  str1 = null;
                }
                localObject9 = new com/inmobi/ads/bp;
                ((bp)localObject9).<init>(i5, i10, (String)localObject8, str1);
                localObject6 = a((Node)localObject5, "StaticResource");
                if (localObject6 != null)
                {
                  localObject7 = a((Node)localObject6);
                  localObject6 = (Element)localObject6;
                  localObject8 = "creativeType";
                  localObject6 = ((Element)localObject6).getAttribute((String)localObject8);
                  if (localObject6 != null)
                  {
                    localObject8 = ((String)localObject6).trim();
                    bool9 = ((String)localObject8).isEmpty();
                    if (!bool9)
                    {
                      boolean bool7 = e((String)localObject6);
                      if (bool7)
                      {
                        localObject6 = new com/inmobi/ads/bp$a;
                        ((bp.a)localObject6).<init>(n, (String)localObject7);
                        ((bp)localObject9).a((bp.a)localObject6);
                      }
                      else
                      {
                        bool4 = true;
                      }
                    }
                  }
                }
                localObject6 = a((Node)localObject5, "HTMLResource");
                int i7;
                if (localObject6 != null)
                {
                  localObject6 = a((Node)localObject6);
                  localObject7 = new com/inmobi/ads/bp$a;
                  i7 = 2;
                  ((bp.a)localObject7).<init>(i7, (String)localObject6);
                  ((bp)localObject9).a((bp.a)localObject7);
                }
                localObject6 = a((Node)localObject5, "IFrameResource");
                if (localObject6 != null)
                {
                  localObject6 = a((Node)localObject6);
                  localObject7 = new com/inmobi/ads/bp$a;
                  i7 = 3;
                  ((bp.a)localObject7).<init>(i7, (String)localObject6);
                  ((bp)localObject9).a((bp.a)localObject7);
                }
                localObject6 = c;
                if (localObject6 != null)
                {
                  int i6 = ((List)localObject6).size();
                  if (i6 != 0)
                  {
                    a(b((Node)localObject5, "CompanionClickTracking"), (bp)localObject9);
                    localObject5 = a((Node)localObject5, "TrackingEvents");
                    localObject6 = "Tracking";
                    localObject5 = b((Node)localObject5, (String)localObject6).iterator();
                    for (;;)
                    {
                      boolean bool8 = ((Iterator)localObject5).hasNext();
                      if (!bool8) {
                        break;
                      }
                      localObject6 = (Node)((Iterator)localObject5).next();
                      localObject7 = localObject6;
                      localObject7 = ((Element)localObject6).getAttribute("event");
                      localObject8 = d;
                      boolean bool10 = ((Map)localObject8).containsKey(localObject7);
                      if (bool10)
                      {
                        localObject6 = a((Node)localObject6);
                        bool10 = URLUtil.isValidUrl((String)localObject6);
                        if (bool10)
                        {
                          localObject8 = new com/inmobi/ads/NativeTracker;
                          localObject10 = d;
                          localObject7 = (NativeTracker.TrackerEventType)((Map)localObject10).get(localObject7);
                          ((NativeTracker)localObject8).<init>((String)localObject6, 0, (NativeTracker.TrackerEventType)localObject7, null);
                          ((bp)localObject9).a((NativeTracker)localObject8);
                        }
                      }
                    }
                    if (str1 != null)
                    {
                      boolean bool5 = ((Map)localObject1).containsKey(str1);
                      if (bool5)
                      {
                        localObject5 = (NativeTracker)((Map)localObject1).get(str1);
                        ((bp)localObject9).a((NativeTracker)localObject5);
                      }
                    }
                    localObject5 = f.e;
                    ((List)localObject5).add(localObject9);
                  }
                }
              }
              i += 1;
            }
            localObject3 = f.e;
            i = ((List)localObject3).size();
            int i11;
            if ((i == 0) && (bool4))
            {
              i11 = 604;
              a(i11);
            }
            else
            {
              i11 = paramString.size();
              if ((i11 > 0) && (i == 0))
              {
                i11 = 600;
                a(i11);
              }
            }
          }
          return f;
          label2123:
          a(m);
          return f;
        }
        catch (DOMException paramString) {}catch (IOException paramString) {}catch (SAXException paramString) {}catch (ParserConfigurationException paramString) {}
        a(100);
        Object localObject3 = com.inmobi.commons.core.a.a.a();
        Object localObject1 = new com/inmobi/commons/core/e/a;
        ((com.inmobi.commons.core.e.a)localObject1).<init>(paramString);
        ((com.inmobi.commons.core.a.a)localObject3).a((com.inmobi.commons.core.e.a)localObject1);
        return f;
      }
    }
    a(i);
    return f;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.bq
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */