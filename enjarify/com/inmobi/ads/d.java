package com.inmobi.ads;

import android.os.SystemClock;
import com.inmobi.commons.core.network.a;
import com.inmobi.commons.core.network.a.a;
import com.inmobi.signals.o;

final class d
  implements a.a
{
  private static final String a = "d";
  private e b;
  private d.a c;
  private long d = 0L;
  
  public d(e parame, d.a parama)
  {
    b = parame;
    c = parama;
  }
  
  public final void a()
  {
    long l = SystemClock.elapsedRealtime();
    d = l;
    a locala = new com/inmobi/commons/core/network/a;
    e locale = b;
    locala.<init>(locale, this);
    locala.a();
  }
  
  public final void a(com.inmobi.commons.core.network.d paramd)
  {
    f localf = new com/inmobi/ads/f;
    Object localObject = b;
    localf.<init>((e)localObject, paramd);
    try
    {
      localObject = o.a();
      e locale = b;
      long l1 = locale.e();
      ((o)localObject).a(l1);
      localObject = o.a();
      l1 = paramd.c();
      ((o)localObject).b(l1);
      paramd = o.a();
      long l2 = SystemClock.elapsedRealtime();
      long l3 = d;
      l2 -= l3;
      paramd.c(l2);
      paramd = c;
      paramd.a(localf);
      return;
    }
    catch (Exception localException)
    {
      localException.getMessage();
    }
  }
  
  public final void b(com.inmobi.commons.core.network.d paramd)
  {
    f localf = new com/inmobi/ads/f;
    Object localObject = b;
    localf.<init>((e)localObject, paramd);
    try
    {
      localObject = o.a();
      e locale = b;
      long l = locale.e();
      ((o)localObject).a(l);
      localObject = o.a();
      l = paramd.c();
      ((o)localObject).b(l);
      paramd = c;
      paramd.b(localf);
      return;
    }
    catch (Exception localException)
    {
      localException.getMessage();
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */