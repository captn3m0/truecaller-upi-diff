package com.inmobi.ads;

import android.content.Context;
import android.view.View;

final class NativeViewFactory$10
  extends NativeViewFactory.c
{
  NativeViewFactory$10(NativeViewFactory paramNativeViewFactory)
  {
    super(paramNativeViewFactory);
  }
  
  protected final View a(Context paramContext)
  {
    GifView localGifView = new com/inmobi/ads/GifView;
    paramContext = paramContext.getApplicationContext();
    localGifView.<init>(paramContext);
    return localGifView;
  }
  
  protected final void a(View paramView, ag paramag, b paramb)
  {
    super.a(paramView, paramag, paramb);
    paramb = a;
    paramView = (GifView)paramView;
    NativeViewFactory.a(paramb, paramView, paramag);
  }
  
  public final boolean a(View paramView)
  {
    boolean bool = paramView instanceof GifView;
    if (!bool) {
      return false;
    }
    ((GifView)paramView).setMovie(null);
    return super.a((View)paramView);
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.NativeViewFactory.10
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */