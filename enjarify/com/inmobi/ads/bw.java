package com.inmobi.ads;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import java.lang.ref.WeakReference;

public abstract class bw
{
  AdContainer a;
  bw.a b;
  protected WeakReference c;
  
  public bw() {}
  
  public bw(AdContainer paramAdContainer)
  {
    a = paramAdContainer;
  }
  
  public View a()
  {
    return null;
  }
  
  public abstract View a(View paramView, ViewGroup paramViewGroup, boolean paramBoolean);
  
  public abstract void a(int paramInt);
  
  public abstract void a(Context paramContext, int paramInt);
  
  protected final void a(View paramView)
  {
    WeakReference localWeakReference = new java/lang/ref/WeakReference;
    localWeakReference.<init>(paramView);
    c = localWeakReference;
  }
  
  public abstract void a(View... paramVarArgs);
  
  public View b()
  {
    WeakReference localWeakReference = c;
    if (localWeakReference == null) {
      return null;
    }
    return (View)localWeakReference.get();
  }
  
  b c()
  {
    b localb = new com/inmobi/ads/b;
    localb.<init>();
    return localb;
  }
  
  public abstract void d();
  
  public void e()
  {
    WeakReference localWeakReference = c;
    if (localWeakReference != null) {
      localWeakReference.clear();
    }
  }
  
  public bw.a f()
  {
    return b;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.bw
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */