package com.inmobi.ads;

import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;

final class InMobiBanner$1
  implements Runnable
{
  InMobiBanner$1(InMobiBanner paramInMobiBanner, boolean paramBoolean) {}
  
  public final void run()
  {
    try
    {
      Object localObject1 = b;
      boolean bool1 = ((InMobiBanner)localObject1).hasValidSize();
      if (bool1)
      {
        localObject1 = b;
        InMobiBanner.access$000((InMobiBanner)localObject1);
        localObject1 = b;
        bool1 = InMobiBanner.access$100((InMobiBanner)localObject1);
        if (bool1)
        {
          localObject1 = b;
          localObject1 = InMobiBanner.access$200((InMobiBanner)localObject1);
          localObject2 = b;
          localObject2 = ((InMobiBanner)localObject2).getFrameSizeString();
          y = ((String)localObject2);
          localObject1 = b;
          localObject1 = InMobiBanner.access$200((InMobiBanner)localObject1);
          boolean bool2 = a;
          ((n)localObject1).b(bool2);
        }
      }
      else
      {
        localObject1 = Logger.InternalLogLevel.ERROR;
        localObject2 = InMobiBanner.access$300();
        localObject3 = "The height or width of the banner can not be determined";
        Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, (String)localObject3);
        localObject1 = b;
        localObject1 = InMobiBanner.access$400((InMobiBanner)localObject1);
        localObject2 = b;
        InMobiBanner.access$200((InMobiBanner)localObject2);
        localObject2 = new com/inmobi/ads/InMobiAdRequestStatus;
        localObject3 = InMobiAdRequestStatus.StatusCode.INTERNAL_ERROR;
        ((InMobiAdRequestStatus)localObject2).<init>((InMobiAdRequestStatus.StatusCode)localObject3);
        ((j.b)localObject1).a((InMobiAdRequestStatus)localObject2);
      }
      return;
    }
    catch (Exception localException)
    {
      Object localObject2 = Logger.InternalLogLevel.ERROR;
      Object localObject3 = InMobiBanner.access$300();
      Logger.a((Logger.InternalLogLevel)localObject2, (String)localObject3, "SDK encountered unexpected error while loading an ad");
      InMobiBanner.access$300();
      localException.getMessage();
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.InMobiBanner.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */