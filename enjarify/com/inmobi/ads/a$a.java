package com.inmobi.ads;

import android.content.ContentValues;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

final class a$a
{
  static a a(ContentValues paramContentValues)
  {
    Object localObject = "video_url";
    boolean bool = paramContentValues.containsKey((String)localObject);
    if (bool)
    {
      localObject = paramContentValues.getAsString("video_url");
      if (localObject != null)
      {
        localObject = paramContentValues.getAsString("video_url");
        bool = ((String)localObject).isEmpty();
        if (!bool)
        {
          localObject = new com/inmobi/ads/az;
          ((az)localObject).<init>(paramContentValues);
          return (a)localObject;
        }
      }
    }
    localObject = new com/inmobi/ads/a;
    ((a)localObject).<init>(paramContentValues);
    return (a)localObject;
  }
  
  static a a(JSONObject paramJSONObject, long paramLong, String paramString1, String paramString2, String paramString3, String paramString4, InMobiAdRequest.MonetizationContext paramMonetizationContext)
  {
    Object localObject1 = paramJSONObject;
    Object localObject2 = "markupType";
    try
    {
      localObject2 = paramJSONObject.getString((String)localObject2);
      localObject3 = "expiry";
      long l1 = -1;
      long l2 = paramJSONObject.optLong((String)localObject3, l1);
      boolean bool1 = l1 < l2;
      if (bool1)
      {
        long l3 = 0L;
        bool1 = l2 < l3;
        if (bool1)
        {
          localObject3 = TimeUnit.SECONDS;
          long l4 = ((TimeUnit)localObject3).toMillis(l2);
          l5 = l4;
          break label88;
        }
      }
      long l5 = l1;
      label88:
      int i = -1;
      int m = ((String)localObject2).hashCode();
      int n = -1084172778;
      Object localObject4;
      boolean bool4;
      if (m != n)
      {
        n = 3213227;
        if (m == n)
        {
          localObject4 = "html";
          bool4 = ((String)localObject2).equals(localObject4);
          if (bool4) {
            i = 1;
          }
        }
      }
      else
      {
        localObject4 = "inmobiJson";
        bool4 = ((String)localObject2).equals(localObject4);
        if (bool4) {
          i = 2;
        }
      }
      Object localObject5;
      boolean bool7;
      Object localObject7;
      Object localObject11;
      az localaz;
      String str1;
      switch (i)
      {
      default: 
        return null;
      case 2: 
        localObject2 = new org/json/JSONObject;
        localObject3 = "pubContent";
        localObject3 = ((JSONObject)localObject1).getString((String)localObject3);
        ((JSONObject)localObject2).<init>((String)localObject3);
        a.e();
        localObject3 = "rootContainer";
        boolean bool2 = ((JSONObject)localObject2).isNull((String)localObject3);
        if (bool2)
        {
          a.e();
          try
          {
            localObject1 = new java/util/HashMap;
            ((HashMap)localObject1).<init>();
            localObject2 = "errorCode";
            localObject3 = "MISSING rootContainer";
            ((Map)localObject1).put(localObject2, localObject3);
            localObject2 = "reason";
            localObject3 = "Missing rootContainer ad markup";
            ((Map)localObject1).put(localObject2, localObject3);
            com.inmobi.commons.core.e.b.a();
            localObject2 = "ads";
            localObject3 = "ServerError";
            com.inmobi.commons.core.e.b.a((String)localObject2, (String)localObject3, (Map)localObject1);
          }
          catch (Exception localException)
          {
            a.e();
            localException.getMessage();
          }
          return null;
        }
        localObject3 = "rootContainer";
        localObject3 = ((JSONObject)localObject2).getJSONObject((String)localObject3);
        localObject4 = new org/json/JSONArray;
        ((JSONArray)localObject4).<init>();
        localObject5 = c((JSONObject)localObject3);
        localObject5 = ((List)localObject5).iterator();
        boolean bool5;
        for (;;)
        {
          bool5 = ((Iterator)localObject5).hasNext();
          if (!bool5) {
            break;
          }
          localObject6 = ((Iterator)localObject5).next();
          localObject6 = (String)localObject6;
          ((JSONArray)localObject4).put(localObject6);
        }
        localObject5 = d((JSONObject)localObject3);
        localObject5 = ((List)localObject5).iterator();
        for (;;)
        {
          bool5 = ((Iterator)localObject5).hasNext();
          if (!bool5) {
            break;
          }
          localObject6 = ((Iterator)localObject5).next();
          localObject6 = (String)localObject6;
          ((JSONArray)localObject4).put(localObject6);
        }
        localObject5 = a((JSONObject)localObject3);
        bool7 = b((JSONObject)localObject3);
        localObject3 = ((String)localObject5).trim();
        int j = ((String)localObject3).length();
        if (j == 0)
        {
          a.e();
          localObject7 = new com/inmobi/ads/a;
          localObject4 = ((JSONArray)localObject4).toString();
          localObject2 = localObject7;
          localObject3 = paramJSONObject;
          localObject8 = paramString1;
          localObject9 = paramString2;
          localObject10 = paramString3;
          localObject11 = paramMonetizationContext;
          ((a)localObject7).<init>(paramJSONObject, (String)localObject4, paramLong, paramString1, paramString2, paramString3, paramString4, paramMonetizationContext, bool7, l5);
          return (a)localObject7;
        }
        localObject3 = new com/inmobi/ads/b;
        ((b)localObject3).<init>();
        localObject6 = com.inmobi.commons.core.configs.b.a();
        ((com.inmobi.commons.core.configs.b)localObject6).a((com.inmobi.commons.core.configs.a)localObject3, null);
        localObject6 = new com/inmobi/ads/bq;
        localObject8 = q;
        ((bq)localObject6).<init>((b.j)localObject8);
        localObject5 = ((bq)localObject6).a((String)localObject5);
        if (localObject5 != null)
        {
          int i1 = f;
          if (i1 == 0)
          {
            localObject6 = new java/util/HashMap;
            ((HashMap)localObject6).<init>();
            localObject8 = "message";
            localObject9 = "VAST PROCESSING SUCCESS";
            ((Map)localObject6).put(localObject8, localObject9);
            com.inmobi.commons.core.e.b.a();
            localObject8 = "ads";
            localObject9 = "VastProcessingSuccess";
            com.inmobi.commons.core.e.b.a((String)localObject8, (String)localObject9, (Map)localObject6);
            localObject6 = d;
            localObject8 = new org/json/JSONArray;
            ((JSONArray)localObject8).<init>();
            localObject6 = ((List)localObject6).iterator();
            for (;;)
            {
              boolean bool8 = ((Iterator)localObject6).hasNext();
              if (!bool8) {
                break;
              }
              localObject9 = ((Iterator)localObject6).next();
              localObject9 = (NativeTracker)localObject9;
              localObject9 = ((NativeTracker)localObject9).toString();
              ((JSONArray)localObject8).put(localObject9);
            }
            localObject6 = e;
            localObject9 = new org/json/JSONArray;
            ((JSONArray)localObject9).<init>();
            localObject6 = ((List)localObject6).iterator();
            boolean bool9;
            for (;;)
            {
              bool9 = ((Iterator)localObject6).hasNext();
              if (!bool9) {
                break;
              }
              localObject10 = ((Iterator)localObject6).next();
              localObject10 = (bp)localObject10;
              localObject10 = ((bp)localObject10).toString();
              ((JSONArray)localObject9).put(localObject10);
            }
            localObject6 = ((bt)localObject5).b();
            if (localObject6 != null)
            {
              bool9 = ((String)localObject6).isEmpty();
              if (!bool9)
              {
                ((JSONArray)localObject4).put(localObject6);
                localObject3 = q;
                localObject3 = a((JSONObject)localObject2, (b.j)localObject3);
                a.e();
                ((List)localObject3).size();
                localObject3 = ((List)localObject3).iterator();
                boolean bool6;
                for (;;)
                {
                  bool6 = ((Iterator)localObject3).hasNext();
                  if (!bool6) {
                    break;
                  }
                  localObject6 = ((Iterator)localObject3).next();
                  localObject6 = (String)localObject6;
                  ((JSONArray)localObject4).put(localObject6);
                }
                localObject3 = "pages";
                localObject3 = a((JSONObject)localObject2, (String)localObject3);
                localObject3 = ((List)localObject3).iterator();
                for (;;)
                {
                  bool6 = ((Iterator)localObject3).hasNext();
                  if (!bool6) {
                    break;
                  }
                  localObject6 = ((Iterator)localObject3).next();
                  localObject6 = (String)localObject6;
                  ((JSONArray)localObject4).put(localObject6);
                }
                localObject3 = "pages";
                localObject2 = b((JSONObject)localObject2, (String)localObject3);
                localObject2 = ((List)localObject2).iterator();
                for (;;)
                {
                  boolean bool3 = ((Iterator)localObject2).hasNext();
                  if (!bool3) {
                    break;
                  }
                  localObject3 = ((Iterator)localObject2).next();
                  localObject3 = (String)localObject3;
                  ((JSONArray)localObject4).put(localObject3);
                }
                localaz = new com/inmobi/ads/az;
                localObject4 = ((JSONArray)localObject4).toString();
                localObject11 = ((bt)localObject5).b();
                str1 = b;
                str2 = c;
                localObject12 = ((JSONArray)localObject8).toString();
                localObject7 = ((JSONArray)localObject9).toString();
                localObject2 = localaz;
                localObject3 = paramJSONObject;
                localObject8 = paramString1;
                localObject9 = paramString2;
                localObject10 = paramString3;
              }
            }
          }
        }
        break;
      }
      try
      {
        localaz.<init>(paramJSONObject, (String)localObject4, paramLong, paramString1, paramString2, paramString3, paramString4, (String)localObject11, str1, str2, (String)localObject12, (String)localObject7, paramMonetizationContext, bool7, l5);
        return localaz;
      }
      catch (JSONException localJSONException1) {}
      HashMap localHashMap = new java/util/HashMap;
      localHashMap.<init>();
      localObject2 = "errorCode";
      localObject3 = "ZERO LENGTH ASSET";
      localHashMap.put(localObject2, localObject3);
      localObject2 = "reason";
      localObject3 = "Asset length is 0";
      localHashMap.put(localObject2, localObject3);
      com.inmobi.commons.core.e.b.a();
      localObject2 = "ads";
      localObject3 = "ServerError";
      com.inmobi.commons.core.e.b.a((String)localObject2, (String)localObject3, localHashMap);
      a.e();
      return null;
      a.e();
      localHashMap = new java/util/HashMap;
      localHashMap.<init>();
      localObject2 = "errorCode";
      int k = f;
      localObject3 = String.valueOf(k);
      localHashMap.put(localObject2, localObject3);
      localObject2 = "reason";
      localObject3 = "Processing VAST XML to build a video descriptor failed";
      localHashMap.put(localObject2, localObject3);
      localObject2 = "latency";
      localObject3 = "0";
      localHashMap.put(localObject2, localObject3);
      com.inmobi.commons.core.e.b.a();
      localObject2 = "ads";
      localObject3 = "VastProcessingError";
      com.inmobi.commons.core.e.b.a((String)localObject2, (String)localObject3, localHashMap);
      return null;
      Object localObject12 = new com/inmobi/ads/a;
      String str2 = null;
      localObject2 = localObject12;
      localObject3 = paramJSONObject;
      l1 = paramLong;
      Object localObject6 = paramString1;
      Object localObject8 = paramString2;
      Object localObject9 = paramString3;
      Object localObject10 = paramString4;
      ((a)localObject12).<init>(paramJSONObject, paramLong, paramString1, paramString2, paramString3, paramString4, paramMonetizationContext, l5, (byte)0);
      return (a)localObject12;
    }
    catch (JSONException localJSONException2) {}
    a.e();
    localJSONException2.getMessage();
    localObject2 = com.inmobi.commons.core.a.a.a();
    Object localObject3 = new com/inmobi/commons/core/e/a;
    ((com.inmobi.commons.core.e.a)localObject3).<init>(localJSONException2);
    ((com.inmobi.commons.core.a.a)localObject2).a((com.inmobi.commons.core.e.a)localObject3);
    return null;
  }
  
  private static String a(JSONObject paramJSONObject)
  {
    Object localObject1 = "assetValue";
    try
    {
      localObject1 = paramJSONObject.getJSONArray((String)localObject1);
      int i = ((JSONArray)localObject1).length();
      if (i == 0) {
        return "";
      }
      localObject2 = "assetType";
      paramJSONObject = paramJSONObject.getString((String)localObject2);
      localObject2 = "video";
      boolean bool1 = paramJSONObject.equalsIgnoreCase((String)localObject2);
      int k = 0;
      if (bool1) {
        return ((JSONArray)localObject1).getString(0);
      }
      localObject2 = "container";
      boolean bool2 = paramJSONObject.equalsIgnoreCase((String)localObject2);
      if (bool2)
      {
        paramJSONObject = "";
        for (;;)
        {
          int j = ((JSONArray)localObject1).length();
          if (k >= j) {
            break;
          }
          paramJSONObject = ((JSONArray)localObject1).getJSONObject(k);
          paramJSONObject = a(paramJSONObject);
          localObject2 = paramJSONObject.trim();
          j = ((String)localObject2).length();
          if (j != 0) {
            break;
          }
          k += 1;
        }
        return paramJSONObject;
      }
      return "";
    }
    catch (JSONException paramJSONObject)
    {
      a.e();
      paramJSONObject.getMessage();
      localObject1 = com.inmobi.commons.core.a.a.a();
      Object localObject2 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject2).<init>(paramJSONObject);
      ((com.inmobi.commons.core.a.a)localObject1).a((com.inmobi.commons.core.e.a)localObject2);
    }
    return "";
  }
  
  private static List a(JSONObject paramJSONObject, b.j paramj)
  {
    localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    String str = "pages";
    try
    {
      paramJSONObject = paramJSONObject.getJSONArray(str);
      int i = 0;
      str = null;
      for (;;)
      {
        int j = paramJSONObject.length();
        if (i >= j) {
          break;
        }
        Object localObject1 = paramJSONObject.getJSONObject(i);
        Object localObject2 = "rootContainer";
        boolean bool1 = ((JSONObject)localObject1).isNull((String)localObject2);
        if (!bool1)
        {
          localObject1 = paramJSONObject.getJSONObject(i);
          localObject2 = "rootContainer";
          localObject1 = ((JSONObject)localObject1).getJSONObject((String)localObject2);
          localObject1 = a((JSONObject)localObject1);
          localObject2 = ((String)localObject1).trim();
          int k = ((String)localObject2).length();
          if (k == 0)
          {
            a.e();
          }
          else
          {
            localObject2 = new com/inmobi/ads/bq;
            ((bq)localObject2).<init>(paramj);
            localObject1 = ((bq)localObject2).a((String)localObject1);
            if (localObject1 != null)
            {
              k = f;
              if (k == 0)
              {
                localObject1 = ((bt)localObject1).b();
                if (localObject1 == null) {
                  break label195;
                }
                boolean bool2 = ((String)localObject1).isEmpty();
                if (bool2) {
                  break label195;
                }
                localArrayList.add(localObject1);
                break label195;
              }
            }
            a.e();
          }
        }
        label195:
        i += 1;
      }
      return localArrayList;
    }
    catch (JSONException localJSONException)
    {
      a.e();
    }
  }
  
  private static List a(JSONObject paramJSONObject, String paramString)
  {
    localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    try
    {
      paramJSONObject = paramJSONObject.getJSONArray(paramString);
      int i = 0;
      paramString = null;
      for (;;)
      {
        int j = paramJSONObject.length();
        if (i >= j) {
          break;
        }
        Object localObject = paramJSONObject.getJSONObject(i);
        String str = "rootContainer";
        boolean bool = ((JSONObject)localObject).isNull(str);
        if (!bool)
        {
          localObject = paramJSONObject.getJSONObject(i);
          str = "rootContainer";
          localObject = ((JSONObject)localObject).getJSONObject(str);
          localObject = c((JSONObject)localObject);
          localArrayList.addAll((Collection)localObject);
        }
        i += 1;
      }
      return localArrayList;
    }
    catch (JSONException localJSONException)
    {
      a.e();
    }
  }
  
  private static List b(JSONObject paramJSONObject, String paramString)
  {
    localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    try
    {
      paramJSONObject = paramJSONObject.getJSONArray(paramString);
      int i = 0;
      paramString = null;
      for (;;)
      {
        int j = paramJSONObject.length();
        if (i >= j) {
          break;
        }
        Object localObject = paramJSONObject.getJSONObject(i);
        String str = "rootContainer";
        boolean bool = ((JSONObject)localObject).isNull(str);
        if (!bool)
        {
          localObject = paramJSONObject.getJSONObject(i);
          str = "rootContainer";
          localObject = ((JSONObject)localObject).getJSONObject(str);
          localObject = d((JSONObject)localObject);
          localArrayList.addAll((Collection)localObject);
        }
        i += 1;
      }
      return localArrayList;
    }
    catch (JSONException localJSONException)
    {
      a.e();
    }
  }
  
  private static boolean b(JSONObject paramJSONObject)
  {
    Object localObject1 = "assetValue";
    try
    {
      localObject1 = paramJSONObject.getJSONArray((String)localObject1);
      int i = ((JSONArray)localObject1).length();
      if (i == 0) {
        return false;
      }
      localObject2 = "assetType";
      localObject2 = paramJSONObject.getString((String)localObject2);
      String str = "webview";
      boolean bool2 = ((String)localObject2).equalsIgnoreCase(str);
      if (bool2)
      {
        localObject1 = "preload";
        boolean bool3 = paramJSONObject.isNull((String)localObject1);
        if (!bool3)
        {
          localObject1 = "preload";
          k = paramJSONObject.getBoolean((String)localObject1);
          if (k != 0) {
            return true;
          }
        }
        return false;
      }
      paramJSONObject = "container";
      int k = ((String)localObject2).equalsIgnoreCase(paramJSONObject);
      if (k != 0)
      {
        k = 0;
        paramJSONObject = null;
        i = 0;
        localObject2 = null;
        boolean bool1;
        for (;;)
        {
          int j = ((JSONArray)localObject1).length();
          if (k >= j) {
            break;
          }
          localObject2 = ((JSONArray)localObject1).getJSONObject(k);
          bool1 = b((JSONObject)localObject2);
          if (bool1) {
            break;
          }
          int m;
          k += 1;
        }
        return bool1;
      }
      return false;
    }
    catch (JSONException paramJSONObject)
    {
      a.e();
      paramJSONObject.getMessage();
      localObject1 = com.inmobi.commons.core.a.a.a();
      Object localObject2 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject2).<init>(paramJSONObject);
      ((com.inmobi.commons.core.a.a)localObject1).a((com.inmobi.commons.core.e.a)localObject2);
    }
    return false;
  }
  
  private static List c(JSONObject paramJSONObject)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Object localObject = "assetValue";
    try
    {
      localObject = paramJSONObject.getJSONArray((String)localObject);
      int i = ((JSONArray)localObject).length();
      if (i == 0) {
        return localArrayList;
      }
      String str1 = "assetType";
      str1 = paramJSONObject.getString(str1);
      String str2 = "image";
      boolean bool2 = str1.equalsIgnoreCase(str2);
      int j = 0;
      if (bool2)
      {
        str1 = "preload";
        boolean bool1 = paramJSONObject.isNull(str1);
        if (!bool1)
        {
          str1 = "preload";
          bool3 = paramJSONObject.getBoolean(str1);
          if (bool3)
          {
            paramJSONObject = ((JSONArray)localObject).getString(0);
            localArrayList.add(paramJSONObject);
          }
        }
        return localArrayList;
      }
      paramJSONObject = "container";
      boolean bool3 = str1.equalsIgnoreCase(paramJSONObject);
      if (bool3)
      {
        for (;;)
        {
          int k = ((JSONArray)localObject).length();
          if (j >= k) {
            break;
          }
          paramJSONObject = ((JSONArray)localObject).getJSONObject(j);
          paramJSONObject = c(paramJSONObject);
          localArrayList.addAll(paramJSONObject);
          j += 1;
        }
        return localArrayList;
      }
      return localArrayList;
    }
    catch (JSONException paramJSONObject)
    {
      a.e();
      paramJSONObject.getMessage();
    }
    return localArrayList;
  }
  
  private static List d(JSONObject paramJSONObject)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Object localObject = "assetValue";
    try
    {
      localObject = paramJSONObject.getJSONArray((String)localObject);
      int i = ((JSONArray)localObject).length();
      if (i == 0) {
        return localArrayList;
      }
      String str = "assetType";
      paramJSONObject = paramJSONObject.getString(str);
      str = "gif";
      boolean bool1 = paramJSONObject.equalsIgnoreCase(str);
      int j = 0;
      if (bool1)
      {
        paramJSONObject = ((JSONArray)localObject).getString(0);
        localArrayList.add(paramJSONObject);
        return localArrayList;
      }
      str = "container";
      boolean bool2 = paramJSONObject.equalsIgnoreCase(str);
      if (bool2)
      {
        for (;;)
        {
          int k = ((JSONArray)localObject).length();
          if (j >= k) {
            break;
          }
          paramJSONObject = ((JSONArray)localObject).getJSONObject(j);
          paramJSONObject = d(paramJSONObject);
          localArrayList.addAll(paramJSONObject);
          j += 1;
        }
        return localArrayList;
      }
      return localArrayList;
    }
    catch (JSONException paramJSONObject)
    {
      a.e();
      paramJSONObject.getMessage();
    }
    return localArrayList;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */