package com.inmobi.ads;

import java.util.Map;

public class InMobiAdRequest$Builder
{
  Map mExtras;
  int mHeightInDp;
  String mKeywords;
  private InMobiAdRequest.MonetizationContext mMonetizationContext;
  private long mPlacementId;
  int mWidthInDp;
  
  public InMobiAdRequest$Builder(long paramLong)
  {
    mPlacementId = paramLong;
  }
  
  public InMobiAdRequest build()
  {
    InMobiAdRequest localInMobiAdRequest = new com/inmobi/ads/InMobiAdRequest;
    long l = mPlacementId;
    InMobiAdRequest.MonetizationContext localMonetizationContext = mMonetizationContext;
    int i = mWidthInDp;
    int j = mHeightInDp;
    String str = mKeywords;
    Map localMap = mExtras;
    localInMobiAdRequest.<init>(l, localMonetizationContext, i, j, str, localMap, null);
    return localInMobiAdRequest;
  }
  
  public Builder setExtras(Map paramMap)
  {
    mExtras = paramMap;
    return this;
  }
  
  public Builder setKeywords(String paramString)
  {
    mKeywords = paramString;
    return this;
  }
  
  public Builder setMonetizationContext(InMobiAdRequest.MonetizationContext paramMonetizationContext)
  {
    mMonetizationContext = paramMonetizationContext;
    return this;
  }
  
  public Builder setSlotSize(int paramInt1, int paramInt2)
  {
    mWidthInDp = paramInt1;
    mHeightInDp = paramInt2;
    return this;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.InMobiAdRequest.Builder
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */