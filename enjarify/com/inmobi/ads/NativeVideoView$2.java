package com.inmobi.ads;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import java.util.Map;

final class NativeVideoView$2
  implements MediaPlayer.OnPreparedListener
{
  NativeVideoView$2(NativeVideoView paramNativeVideoView) {}
  
  public final void onPrepared(MediaPlayer paramMediaPlayer)
  {
    Object localObject1 = NativeVideoView.c(a);
    if (localObject1 == null) {
      return;
    }
    ca).a = 2;
    localObject1 = a;
    boolean bool1 = NativeVideoView.d((NativeVideoView)localObject1);
    bool1 = NativeVideoView.b((NativeVideoView)localObject1, bool1);
    NativeVideoView.a((NativeVideoView)localObject1, bool1);
    localObject1 = NativeVideoView.e(a);
    if (localObject1 != null)
    {
      localObject1 = NativeVideoView.e(a);
      bool1 = true;
      ((NativeVideoController)localObject1).setEnabled(bool1);
    }
    localObject1 = a;
    int i = paramMediaPlayer.getVideoWidth();
    NativeVideoView.a((NativeVideoView)localObject1, i);
    localObject1 = a;
    int k = paramMediaPlayer.getVideoHeight();
    NativeVideoView.b((NativeVideoView)localObject1, k);
    paramMediaPlayer = (bb)a.getTag();
    int n = 0;
    localObject1 = null;
    Object localObject3;
    boolean bool2;
    if (paramMediaPlayer != null)
    {
      localObject2 = v;
      localObject3 = "didCompleteQ4";
      localObject2 = (Boolean)((Map)localObject2).get(localObject3);
      bool2 = ((Boolean)localObject2).booleanValue();
      if (bool2)
      {
        localObject2 = a;
        i1 = 8;
        ((NativeVideoView)localObject2).a(i1, 0);
        localObject2 = (AdContainer.RenderingProperties.PlacementType)v.get("placementType");
        localObject3 = AdContainer.RenderingProperties.PlacementType.PLACEMENT_TYPE_FULLSCREEN;
        if (localObject2 == localObject3) {
          return;
        }
      }
    }
    Object localObject2 = a.getPlaybackEventListener();
    if (localObject2 != null)
    {
      localObject2 = a.getPlaybackEventListener();
      ((NativeVideoView.b)localObject2).a(0);
    }
    if (paramMediaPlayer != null)
    {
      localObject2 = v;
      localObject3 = "didCompleteQ4";
      localObject2 = (Boolean)((Map)localObject2).get(localObject3);
      bool2 = ((Boolean)localObject2).booleanValue();
      if (!bool2)
      {
        localObject1 = v;
        localObject2 = "seekPosition";
        localObject1 = (Integer)((Map)localObject1).get(localObject2);
        n = ((Integer)localObject1).intValue();
      }
    }
    localObject2 = a;
    int j = NativeVideoView.a((NativeVideoView)localObject2);
    int i1 = 3;
    if (j != 0)
    {
      localObject2 = a;
      j = NativeVideoView.b((NativeVideoView)localObject2);
      if (j != 0)
      {
        localObject2 = NativeVideoView.c(a);
        j = b;
        if (i1 == j)
        {
          if (paramMediaPlayer != null)
          {
            paramMediaPlayer = v;
            localObject1 = "isFullScreen";
            paramMediaPlayer = (Boolean)paramMediaPlayer.get(localObject1);
            bool3 = paramMediaPlayer.booleanValue();
            if (bool3)
            {
              paramMediaPlayer = a;
              paramMediaPlayer.start();
            }
          }
          paramMediaPlayer = NativeVideoView.e(a);
          if (paramMediaPlayer == null) {
            return;
          }
          NativeVideoView.e(a).a();
          return;
        }
        paramMediaPlayer = a;
        boolean bool3 = paramMediaPlayer.isPlaying();
        if (bool3) {
          return;
        }
        if (n == 0)
        {
          paramMediaPlayer = a;
          int m = paramMediaPlayer.getCurrentPosition();
          if (m <= 0) {
            return;
          }
        }
        paramMediaPlayer = NativeVideoView.e(a);
        if (paramMediaPlayer == null) {
          return;
        }
        NativeVideoView.e(a).a();
        return;
      }
    }
    localObject1 = NativeVideoView.c(a);
    n = b;
    if ((i1 == n) && (paramMediaPlayer != null))
    {
      paramMediaPlayer = v;
      localObject1 = "isFullScreen";
      paramMediaPlayer = (Boolean)paramMediaPlayer.get(localObject1);
      boolean bool4 = paramMediaPlayer.booleanValue();
      if (bool4)
      {
        paramMediaPlayer = a;
        paramMediaPlayer.start();
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.NativeVideoView.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */