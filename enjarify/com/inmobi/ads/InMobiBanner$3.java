package com.inmobi.ads;

import android.os.Build.VERSION;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;
import com.inmobi.commons.core.utilities.b.c;

final class InMobiBanner$3
  implements ViewTreeObserver.OnGlobalLayoutListener
{
  InMobiBanner$3(InMobiBanner paramInMobiBanner) {}
  
  public final void onGlobalLayout()
  {
    try
    {
      Object localObject1 = a;
      localObject2 = a;
      int i = ((InMobiBanner)localObject2).getMeasuredWidth();
      i = c.b(i);
      InMobiBanner.access$702((InMobiBanner)localObject1, i);
      localObject1 = a;
      localObject2 = a;
      i = ((InMobiBanner)localObject2).getMeasuredHeight();
      i = c.b(i);
      InMobiBanner.access$802((InMobiBanner)localObject1, i);
      localObject1 = a;
      boolean bool = ((InMobiBanner)localObject1).hasValidSize();
      if (bool)
      {
        int j = Build.VERSION.SDK_INT;
        i = 16;
        if (j >= i)
        {
          localObject1 = a;
          localObject1 = ((InMobiBanner)localObject1).getViewTreeObserver();
          ((ViewTreeObserver)localObject1).removeOnGlobalLayoutListener(this);
          return;
        }
        localObject1 = a;
        localObject1 = ((InMobiBanner)localObject1).getViewTreeObserver();
        ((ViewTreeObserver)localObject1).removeGlobalOnLayoutListener(this);
      }
      return;
    }
    catch (Exception localException)
    {
      Object localObject2 = Logger.InternalLogLevel.ERROR;
      String str = InMobiBanner.access$300();
      Logger.a((Logger.InternalLogLevel)localObject2, str, "InMobiBanner$1.onGlobalLayout() handler threw unexpected error");
      InMobiBanner.access$300();
      localException.getMessage();
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.InMobiBanner.3
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */