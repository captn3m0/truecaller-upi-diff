package com.inmobi.ads;

import android.os.Handler;
import android.os.Message;
import java.lang.ref.WeakReference;
import java.util.Map;

final class NativeVideoView$d
  extends Handler
{
  private final WeakReference a;
  
  NativeVideoView$d(NativeVideoView paramNativeVideoView)
  {
    WeakReference localWeakReference = new java/lang/ref/WeakReference;
    localWeakReference.<init>(paramNativeVideoView);
    a = localWeakReference;
  }
  
  public final void handleMessage(Message paramMessage)
  {
    Object localObject1 = (NativeVideoView)a.get();
    if (localObject1 != null)
    {
      int i = what;
      int k = 1;
      if (i == k)
      {
        i = ((NativeVideoView)localObject1).getDuration();
        int m = ((NativeVideoView)localObject1).getCurrentPosition();
        int n = -1;
        if ((i != n) && (m != 0))
        {
          bb localbb = (bb)((NativeVideoView)localObject1).getTag();
          Object localObject2 = v;
          String str = "didCompleteQ1";
          localObject2 = (Boolean)((Map)localObject2).get(str);
          boolean bool2 = ((Boolean)localObject2).booleanValue();
          Boolean localBoolean;
          int i4;
          if (!bool2)
          {
            int i1 = m * 4 - i;
            if (i1 >= 0)
            {
              localObject2 = v;
              localBoolean = Boolean.TRUE;
              ((Map)localObject2).put("didCompleteQ1", localBoolean);
              localObject2 = ((NativeVideoView)localObject1).getQuartileCompletedListener();
              i4 = 0;
              str = null;
              ((NativeVideoView.c)localObject2).a(0);
            }
          }
          localObject2 = v;
          str = "didCompleteQ2";
          localObject2 = (Boolean)((Map)localObject2).get(str);
          boolean bool3 = ((Boolean)localObject2).booleanValue();
          if (!bool3)
          {
            int i2 = m * 2 - i;
            if (i2 >= 0)
            {
              localObject2 = v;
              str = "didCompleteQ2";
              localBoolean = Boolean.TRUE;
              ((Map)localObject2).put(str, localBoolean);
              localObject2 = ((NativeVideoView)localObject1).getQuartileCompletedListener();
              ((NativeVideoView.c)localObject2).a(k);
            }
          }
          localObject2 = v;
          str = "didCompleteQ3";
          localObject2 = (Boolean)((Map)localObject2).get(str);
          boolean bool4 = ((Boolean)localObject2).booleanValue();
          if (!bool4)
          {
            int i3 = m * 4;
            i4 = i * 3;
            i3 -= i4;
            if (i3 >= 0)
            {
              localObject2 = v;
              str = "didCompleteQ3";
              localBoolean = Boolean.TRUE;
              ((Map)localObject2).put(str, localBoolean);
              localObject2 = ((NativeVideoView)localObject1).getQuartileCompletedListener();
              i4 = 2;
              ((NativeVideoView.c)localObject2).a(i4);
            }
          }
          localObject2 = v;
          str = "didQ4Fire";
          localObject2 = (Boolean)((Map)localObject2).get(str);
          boolean bool5 = ((Boolean)localObject2).booleanValue();
          float f1 = m;
          float f2 = i;
          f1 = f1 / f2 * 100.0F;
          f2 = E;
          boolean bool1 = f1 < f2;
          if ((bool1) && (!bool5))
          {
            localObject1 = ((NativeVideoView)localObject1).getPlaybackEventListener();
            int j = 5;
            f2 = 7.0E-45F;
            ((NativeVideoView.b)localObject1).a(j);
          }
        }
        long l = 1000L;
        sendEmptyMessageDelayed(k, l);
      }
    }
    super.handleMessage(paramMessage);
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.NativeVideoView.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */