package com.inmobi.ads;

import android.graphics.Camera;
import android.graphics.Matrix;
import android.view.animation.Animation;
import android.view.animation.Transformation;

final class k$b
  extends Animation
{
  private final float a = 0.0F;
  private final float b = 90.0F;
  private final float c;
  private final float d;
  private final float e;
  private final boolean f;
  private Camera g;
  
  public k$b(float paramFloat1, float paramFloat2)
  {
    c = paramFloat1;
    d = paramFloat2;
    e = 0.0F;
    f = true;
  }
  
  protected final void applyTransformation(float paramFloat, Transformation paramTransformation)
  {
    float f1 = a;
    float f2 = (b - f1) * paramFloat;
    f1 += f2;
    f2 = c;
    float f3 = d;
    Camera localCamera = g;
    paramTransformation = paramTransformation.getMatrix();
    localCamera.save();
    boolean bool = f;
    float f4;
    if (bool)
    {
      f4 = e * paramFloat;
      localCamera.translate(0.0F, 0.0F, f4);
    }
    else
    {
      f4 = e;
      float f5 = 1.0F - paramFloat;
      f4 *= f5;
      localCamera.translate(0.0F, 0.0F, f4);
    }
    localCamera.rotateY(f1);
    localCamera.getMatrix(paramTransformation);
    localCamera.restore();
    paramFloat = -f2;
    f1 = -f3;
    paramTransformation.preTranslate(paramFloat, f1);
    paramTransformation.postTranslate(f2, f3);
  }
  
  public final void initialize(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.initialize(paramInt1, paramInt2, paramInt3, paramInt4);
    Camera localCamera = new android/graphics/Camera;
    localCamera.<init>();
    g = localCamera;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.k.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */