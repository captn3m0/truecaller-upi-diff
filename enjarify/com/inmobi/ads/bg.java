package com.inmobi.ads;

import android.content.ContentValues;
import com.inmobi.commons.core.d.b;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class bg
{
  public static final String[] a = tmp40_27;
  private static final String b = "bg";
  private static bg c;
  private static Object d;
  
  static
  {
    Object localObject = new java/lang/Object;
    localObject.<init>();
    d = localObject;
    String[] tmp17_14 = new String[6];
    String[] tmp18_17 = tmp17_14;
    String[] tmp18_17 = tmp17_14;
    tmp18_17[0] = "id";
    tmp18_17[1] = "placement_id";
    String[] tmp27_18 = tmp18_17;
    String[] tmp27_18 = tmp18_17;
    tmp27_18[2] = "tp_key";
    tmp27_18[3] = "last_accessed_ts";
    tmp27_18[4] = "ad_type";
    String[] tmp40_27 = tmp27_18;
    tmp40_27[5] = "m10_context";
  }
  
  private bg()
  {
    b localb = b.a();
    localb.a("placement", "(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, placement_id INTEGER NOT NULL,tp_key TEXT,last_accessed_ts INTEGER NOT NULL,ad_type TEXT NOT NULL,m10_context TEXT NOT NULL,UNIQUE(placement_id,m10_context,tp_key))");
    localb.b();
  }
  
  public static int a(long paramLong, String paramString)
  {
    b localb = b.a();
    long l = System.currentTimeMillis();
    paramLong *= 1000L;
    l -= paramLong;
    String[] arrayOfString = new String[2];
    arrayOfString[0] = paramString;
    paramString = String.valueOf(l);
    arrayOfString[1] = paramString;
    int i = localb.a("placement", "ad_type=? AND last_accessed_ts<?", arrayOfString);
    localb.b();
    return i;
  }
  
  public static bg a()
  {
    bg localbg1 = c;
    if (localbg1 == null) {
      synchronized (d)
      {
        localbg1 = c;
        if (localbg1 == null)
        {
          localbg1 = new com/inmobi/ads/bg;
          localbg1.<init>();
          c = localbg1;
        }
      }
    }
    return localbg2;
  }
  
  public static List a(String paramString)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    b localb = b.a();
    Object localObject1 = "placement";
    String[] arrayOfString1 = a;
    String str = "ad_type=? ";
    String[] arrayOfString2 = new String[1];
    boolean bool = false;
    arrayOfString2[0] = paramString;
    Object localObject2 = localb;
    paramString = localb.a((String)localObject1, arrayOfString1, str, arrayOfString2, null, null, null, null);
    localb.b();
    paramString = paramString.iterator();
    for (;;)
    {
      bool = paramString.hasNext();
      if (!bool) {
        break;
      }
      localObject2 = (ContentValues)paramString.next();
      localObject1 = new com/inmobi/ads/bf;
      ((bf)localObject1).<init>((ContentValues)localObject2);
      localArrayList.add(localObject1);
    }
    return localArrayList;
  }
  
  public final int a(List paramList, int paramInt)
  {
    try
    {
      int i = paramList.size();
      int j = 0;
      String str1 = null;
      if (i == 0) {
        return 0;
      }
      b localb = b.a();
      int k = 0;
      Object localObject1 = null;
      int m;
      Object localObject2;
      Object localObject3;
      int n;
      String[] arrayOfString;
      Object localObject4;
      int i1;
      int i2;
      Object localObject5;
      String str2;
      Object localObject6;
      for (;;)
      {
        m = paramList.size();
        if (k >= m) {
          break;
        }
        localObject2 = "placement_id = ? AND m10_context = ? AND tp_key=?";
        localObject3 = paramList.get(k);
        localObject3 = (bf)localObject3;
        n = 3;
        arrayOfString = new String[n];
        long l1 = a;
        localObject4 = String.valueOf(l1);
        arrayOfString[0] = localObject4;
        localObject4 = f;
        localObject4 = ((InMobiAdRequest.MonetizationContext)localObject4).toString();
        i1 = 1;
        arrayOfString[i1] = localObject4;
        i2 = 2;
        localObject5 = b;
        arrayOfString[i2] = localObject5;
        localObject4 = "placement";
        localObject5 = new android/content/ContentValues;
        ((ContentValues)localObject5).<init>();
        str2 = "placement_id";
        long l2 = a;
        localObject6 = Long.valueOf(l2);
        ((ContentValues)localObject5).put(str2, (Long)localObject6);
        str2 = "last_accessed_ts";
        l2 = System.currentTimeMillis();
        localObject6 = Long.valueOf(l2);
        ((ContentValues)localObject5).put(str2, (Long)localObject6);
        str2 = "tp_key";
        localObject6 = b;
        ((ContentValues)localObject5).put(str2, (String)localObject6);
        str2 = "ad_type";
        localObject6 = e;
        ((ContentValues)localObject5).put(str2, (String)localObject6);
        str2 = "m10_context";
        localObject3 = f;
        localObject3 = ((InMobiAdRequest.MonetizationContext)localObject3).toString();
        ((ContentValues)localObject5).put(str2, (String)localObject3);
        localb.a((String)localObject4, (ContentValues)localObject5, (String)localObject2, arrayOfString);
        k += 1;
      }
      paramList = "placement";
      int i3 = localb.a(paramList) - paramInt;
      if (i3 > 0)
      {
        localObject2 = "placement";
        Object localObject7 = "id";
        localObject3 = new String[] { localObject7 };
        n = 0;
        arrayOfString = null;
        i2 = 0;
        localObject4 = null;
        i1 = 0;
        localObject5 = null;
        str2 = null;
        localObject6 = "last_accessed_ts ASC";
        String str3 = String.valueOf(i3);
        localObject1 = localb;
        localObject7 = localb.a((String)localObject2, (String[])localObject3, null, null, null, null, (String)localObject6, str3);
        k = ((List)localObject7).size();
        localObject1 = new String[k];
        for (;;)
        {
          m = ((List)localObject7).size();
          if (j >= m) {
            break;
          }
          localObject2 = ((List)localObject7).get(j);
          localObject2 = (ContentValues)localObject2;
          localObject3 = "id";
          localObject2 = ((ContentValues)localObject2).getAsInteger((String)localObject3);
          localObject2 = String.valueOf(localObject2);
          localObject1[j] = localObject2;
          j += 1;
        }
        localObject7 = Arrays.toString((Object[])localObject1);
        str1 = "[";
        localObject1 = "(";
        localObject7 = ((String)localObject7).replace(str1, (CharSequence)localObject1);
        str1 = "]";
        localObject1 = ")";
        localObject7 = ((String)localObject7).replace(str1, (CharSequence)localObject1);
        str1 = "placement";
        localObject1 = "id IN ";
        localObject7 = String.valueOf(localObject7);
        localObject7 = ((String)localObject1).concat((String)localObject7);
        k = 0;
        localObject1 = null;
        localb.a(str1, (String)localObject7, null);
      }
      localb.b();
      return i3;
    }
    finally {}
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.bg
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */