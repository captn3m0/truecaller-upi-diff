package com.inmobi.ads;

import android.view.View;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

final class ca$b
  implements Runnable
{
  private final ArrayList a;
  private final ArrayList b;
  private WeakReference c;
  
  ca$b(ca paramca)
  {
    WeakReference localWeakReference = new java/lang/ref/WeakReference;
    localWeakReference.<init>(paramca);
    c = localWeakReference;
    paramca = new java/util/ArrayList;
    paramca.<init>();
    b = paramca;
    paramca = new java/util/ArrayList;
    paramca.<init>();
    a = paramca;
  }
  
  public final void run()
  {
    ca localca = (ca)c.get();
    Object localObject2;
    Object localObject3;
    if (localca != null)
    {
      ca.a(localca);
      localObject1 = ca.b(localca).entrySet().iterator();
      for (;;)
      {
        boolean bool = ((Iterator)localObject1).hasNext();
        if (!bool) {
          break;
        }
        localObject2 = (Map.Entry)((Iterator)localObject1).next();
        localObject3 = (View)((Map.Entry)localObject2).getKey();
        ca.d locald = (ca.d)((Map.Entry)localObject2).getValue();
        int i = a;
        View localView = getValuec;
        localObject2 = getValued;
        ca.a locala = ca.c(localca);
        bool = locala.a(localView, (View)localObject3, i, localObject2);
        if (bool)
        {
          localObject2 = a;
          ((ArrayList)localObject2).add(localObject3);
        }
        else
        {
          localObject2 = b;
          ((ArrayList)localObject2).add(localObject3);
        }
      }
    }
    if (localca != null)
    {
      localObject1 = ca.d(localca);
      if (localObject1 != null)
      {
        localObject2 = a;
        localObject3 = b;
        ((ca.c)localObject1).a((List)localObject2, (List)localObject3);
      }
    }
    a.clear();
    Object localObject1 = b;
    ((ArrayList)localObject1).clear();
    if (localca != null) {
      localca.b();
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.ca.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */