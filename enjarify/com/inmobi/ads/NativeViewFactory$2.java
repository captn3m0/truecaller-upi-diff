package com.inmobi.ads;

import android.content.Context;
import android.view.View;

final class NativeViewFactory$2
  extends NativeViewFactory.c
{
  NativeViewFactory$2(NativeViewFactory paramNativeViewFactory)
  {
    super(paramNativeViewFactory);
  }
  
  protected final View a(Context paramContext)
  {
    NativeTimerView localNativeTimerView = new com/inmobi/ads/NativeTimerView;
    paramContext = paramContext.getApplicationContext();
    localNativeTimerView.<init>(paramContext);
    return localNativeTimerView;
  }
  
  protected final void a(View paramView, ag paramag, b paramb)
  {
    super.a(paramView, paramag, paramb);
    paramb = a;
    paramView = (NativeTimerView)paramView;
    NativeViewFactory.a(paramb, paramView, paramag);
  }
  
  public final boolean a(View paramView)
  {
    boolean bool1 = paramView instanceof NativeTimerView;
    if (bool1)
    {
      boolean bool2 = super.a((View)paramView);
      if (bool2) {
        return true;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.NativeViewFactory.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */