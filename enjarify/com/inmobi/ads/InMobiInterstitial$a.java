package com.inmobi.ads;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;
import java.lang.ref.WeakReference;
import java.util.Map;

final class InMobiInterstitial$a
  extends Handler
{
  private WeakReference a;
  private WeakReference b;
  
  public InMobiInterstitial$a(InMobiInterstitial paramInMobiInterstitial, InMobiInterstitial.InterstitialAdListener2 paramInterstitialAdListener2)
  {
    super((Looper)localObject);
    localObject = new java/lang/ref/WeakReference;
    ((WeakReference)localObject).<init>(paramInMobiInterstitial);
    b = ((WeakReference)localObject);
    paramInMobiInterstitial = new java/lang/ref/WeakReference;
    paramInMobiInterstitial.<init>(paramInterstitialAdListener2);
    a = paramInMobiInterstitial;
  }
  
  public final void handleMessage(Message paramMessage)
  {
    Object localObject1 = (InMobiInterstitial)b.get();
    Object localObject2 = a;
    if (localObject2 != null)
    {
      localObject2 = (InMobiInterstitial.InterstitialAdListener2)((WeakReference)localObject2).get();
      if ((localObject1 != null) && (localObject2 != null))
      {
        int i = what;
        Object localObject3 = null;
        Object localObject4;
        switch (i)
        {
        default: 
          InMobiInterstitial.access$200();
          break;
        case 11: 
          try
          {
            ((InMobiInterstitial.InterstitialAdListener2)localObject2).onUserLeftApplication((InMobiInterstitial)localObject1);
            return;
          }
          catch (Exception paramMessage)
          {
            localObject1 = Logger.InternalLogLevel.ERROR;
            localObject2 = InMobiInterstitial.access$200();
            Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "Publisher handler caused unexpected error");
            InMobiInterstitial.access$200();
            paramMessage.getMessage();
            return;
          }
        case 10: 
          try
          {
            ((InMobiInterstitial.InterstitialAdListener2)localObject2).onAdDismissed((InMobiInterstitial)localObject1);
            return;
          }
          catch (Exception paramMessage)
          {
            localObject1 = Logger.InternalLogLevel.ERROR;
            localObject2 = InMobiInterstitial.access$200();
            Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "Publisher handler caused unexpected error");
            InMobiInterstitial.access$200();
            paramMessage.getMessage();
            return;
          }
        case 9: 
          localObject4 = obj;
          if (localObject4 != null)
          {
            paramMessage = obj;
            localObject3 = paramMessage;
            localObject3 = (Map)paramMessage;
          }
          try
          {
            ((InMobiInterstitial.InterstitialAdListener2)localObject2).onAdInteraction((InMobiInterstitial)localObject1, (Map)localObject3);
            return;
          }
          catch (Exception paramMessage)
          {
            localObject1 = Logger.InternalLogLevel.ERROR;
            localObject2 = InMobiInterstitial.access$200();
            Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "Publisher handler caused unexpected error");
            InMobiInterstitial.access$200();
            paramMessage.getMessage();
            return;
          }
        case 8: 
          return;
        case 7: 
          try
          {
            ((InMobiInterstitial.InterstitialAdListener2)localObject2).onAdDisplayed((InMobiInterstitial)localObject1);
            return;
          }
          catch (Exception paramMessage)
          {
            localObject1 = Logger.InternalLogLevel.ERROR;
            localObject2 = InMobiInterstitial.access$200();
            Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "Publisher handler caused unexpected error");
            InMobiInterstitial.access$200();
            paramMessage.getMessage();
            return;
          }
        case 6: 
          try
          {
            ((InMobiInterstitial.InterstitialAdListener2)localObject2).onAdWillDisplay((InMobiInterstitial)localObject1);
            return;
          }
          catch (Exception paramMessage)
          {
            localObject1 = Logger.InternalLogLevel.ERROR;
            localObject2 = InMobiInterstitial.access$200();
            Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "Publisher handler caused unexpected error");
            InMobiInterstitial.access$200();
            paramMessage.getMessage();
            return;
          }
        case 5: 
          try
          {
            ((InMobiInterstitial.InterstitialAdListener2)localObject2).onAdDisplayFailed((InMobiInterstitial)localObject1);
            return;
          }
          catch (Exception paramMessage)
          {
            localObject1 = Logger.InternalLogLevel.ERROR;
            localObject2 = InMobiInterstitial.access$200();
            Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "Publisher handler caused unexpected error");
            InMobiInterstitial.access$200();
            paramMessage.getMessage();
            return;
          }
        case 4: 
          localObject4 = obj;
          if (localObject4 != null)
          {
            paramMessage = obj;
            localObject3 = paramMessage;
            localObject3 = (Map)paramMessage;
          }
          try
          {
            ((InMobiInterstitial.InterstitialAdListener2)localObject2).onAdRewardActionCompleted((InMobiInterstitial)localObject1, (Map)localObject3);
            return;
          }
          catch (Exception paramMessage)
          {
            localObject1 = Logger.InternalLogLevel.ERROR;
            localObject2 = InMobiInterstitial.access$200();
            Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "Publisher handler caused unexpected error");
            InMobiInterstitial.access$200();
            paramMessage.getMessage();
            return;
          }
        case 3: 
          try
          {
            ((InMobiInterstitial.InterstitialAdListener2)localObject2).onAdLoadSucceeded((InMobiInterstitial)localObject1);
            return;
          }
          catch (Exception paramMessage)
          {
            localObject1 = Logger.InternalLogLevel.ERROR;
            localObject2 = InMobiInterstitial.access$200();
            Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "Publisher handler caused unexpected error");
            InMobiInterstitial.access$200();
            paramMessage.getMessage();
            return;
          }
        case 2: 
          paramMessage = paramMessage.getData();
          localObject4 = "available";
          boolean bool = paramMessage.getBoolean((String)localObject4);
          if (!bool) {
            return;
          }
          try
          {
            ((InMobiInterstitial.InterstitialAdListener2)localObject2).onAdReceived((InMobiInterstitial)localObject1);
            return;
          }
          catch (Exception paramMessage)
          {
            localObject1 = Logger.InternalLogLevel.ERROR;
            localObject2 = InMobiInterstitial.access$200();
            Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "Publisher handler caused unexpected error");
            InMobiInterstitial.access$200();
            paramMessage.getMessage();
            return;
          }
        }
        paramMessage = (InMobiAdRequestStatus)obj;
        try
        {
          ((InMobiInterstitial.InterstitialAdListener2)localObject2).onAdLoadFailed((InMobiInterstitial)localObject1, paramMessage);
          return;
        }
        catch (Exception paramMessage)
        {
          localObject1 = Logger.InternalLogLevel.ERROR;
          localObject2 = InMobiInterstitial.access$200();
          Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "Publisher handler caused unexpected error");
          InMobiInterstitial.access$200();
          paramMessage.getMessage();
          return;
        }
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.InMobiInterstitial.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */