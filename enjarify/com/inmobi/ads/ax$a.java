package com.inmobi.ads;

import android.media.MediaMetadataRetriever;

public final class ax$a
{
  private long a;
  private long b;
  private String c;
  private ak d;
  
  public ax$a(long paramLong1, long paramLong2, String paramString, ak paramak)
  {
    a = paramLong1;
    b = paramLong2;
    c = paramString;
    d = paramak;
  }
  
  public final long a()
  {
    long l1 = a;
    Object localObject1 = d;
    Object localObject2 = c;
    localObject1 = ((ak)localObject1).b((String)localObject2);
    if (localObject1 != null)
    {
      boolean bool = localObject1 instanceof bb;
      if (bool)
      {
        localObject1 = (bb)localObject1;
        if (localObject1 != null)
        {
          localObject1 = ((bb)localObject1).b().b();
          if (localObject1 != null)
          {
            localObject2 = new android/media/MediaMetadataRetriever;
            ((MediaMetadataRetriever)localObject2).<init>();
            ((MediaMetadataRetriever)localObject2).setDataSource((String)localObject1);
            localObject1 = Integer.valueOf(((MediaMetadataRetriever)localObject2).extractMetadata(9));
            int i = ((Integer)localObject1).intValue();
            long l2 = i;
            double d1 = l1;
            long l3 = b;
            double d2 = l3;
            Double.isNaN(d2);
            d2 = d2 * 1.0D / 100.0D;
            long l4 = 1000L;
            l2 /= l4;
            double d3 = l2;
            Double.isNaN(d3);
            d2 *= d3;
            Double.isNaN(d1);
            d1 += d2;
            l1 = d1;
            ((MediaMetadataRetriever)localObject2).release();
          }
        }
      }
    }
    return l1;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.ax.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */