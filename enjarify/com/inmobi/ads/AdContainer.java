package com.inmobi.ads;

import android.app.Activity;
import android.view.View;
import java.util.Map;

public abstract interface AdContainer
{
  public abstract void a();
  
  public abstract void a(int paramInt, Map paramMap);
  
  public abstract void b();
  
  public abstract boolean c();
  
  public abstract void destroy();
  
  public abstract m getApkDownloader();
  
  public abstract Object getDataModel();
  
  public abstract AdContainer.a getFullScreenEventsListener();
  
  public abstract String getMarkupType();
  
  public abstract AdContainer.RenderingProperties getRenderingProperties();
  
  public abstract View getVideoContainerView();
  
  public abstract bw getViewableAd();
  
  public abstract void setFullScreenActivityContext(Activity paramActivity);
  
  public abstract void setRequestedScreenOrientation();
}

/* Location:
 * Qualified Name:     com.inmobi.ads.AdContainer
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */