package com.inmobi.ads;

import android.app.KeyguardManager;
import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioAttributes.Builder;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnInfoListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnVideoSizeChangedListener;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.PowerManager;
import android.view.Surface;
import android.view.TextureView;
import android.view.TextureView.SurfaceTextureListener;
import android.view.View.MeasureSpec;
import android.widget.ImageView;
import android.widget.MediaController.MediaPlayerControl;
import android.widget.ProgressBar;
import java.io.IOException;
import java.util.Map;

public class NativeVideoView
  extends TextureView
  implements MediaController.MediaPlayerControl
{
  private static final String m = "NativeVideoView";
  private MediaPlayer.OnInfoListener A;
  private MediaPlayer.OnBufferingUpdateListener B;
  private MediaPlayer.OnErrorListener C;
  Uri a;
  Map b;
  ar c = null;
  int d;
  int e;
  int f;
  NativeVideoView.d g;
  Handler h;
  boolean i;
  MediaPlayer.OnVideoSizeChangedListener j;
  MediaPlayer.OnPreparedListener k;
  final TextureView.SurfaceTextureListener l;
  private Surface n = null;
  private int o;
  private int p = 0;
  private NativeVideoView.c q;
  private NativeVideoView.b r;
  private NativeVideoView.a s;
  private boolean t;
  private NativeVideoController u;
  private int v;
  private boolean w;
  private boolean x;
  private boolean y;
  private MediaPlayer.OnCompletionListener z;
  
  public NativeVideoView(Context paramContext)
  {
    super(paramContext);
    paramContext = new com/inmobi/ads/NativeVideoView$1;
    paramContext.<init>(this);
    j = paramContext;
    paramContext = new com/inmobi/ads/NativeVideoView$2;
    paramContext.<init>(this);
    k = paramContext;
    paramContext = new com/inmobi/ads/NativeVideoView$3;
    paramContext.<init>(this);
    z = paramContext;
    paramContext = new com/inmobi/ads/NativeVideoView$4;
    paramContext.<init>(this);
    A = paramContext;
    paramContext = new com/inmobi/ads/NativeVideoView$5;
    paramContext.<init>(this);
    B = paramContext;
    paramContext = new com/inmobi/ads/NativeVideoView$6;
    paramContext.<init>(this);
    C = paramContext;
    paramContext = new com/inmobi/ads/NativeVideoView$7;
    paramContext.<init>(this);
    l = paramContext;
    requestLayout();
    invalidate();
  }
  
  private void g()
  {
    Object localObject1 = a;
    if (localObject1 != null)
    {
      localObject1 = n;
      if (localObject1 != null)
      {
        localObject1 = c;
        int i1 = -1;
        Object localObject3;
        Object localObject4;
        if (localObject1 == null)
        {
          localObject1 = (bb)getTag();
          localObject2 = AdContainer.RenderingProperties.PlacementType.PLACEMENT_TYPE_FULLSCREEN;
          if (localObject1 != null)
          {
            localObject1 = v.get("placementType");
            localObject2 = localObject1;
            localObject2 = (AdContainer.RenderingProperties.PlacementType)localObject1;
          }
          localObject1 = AdContainer.RenderingProperties.PlacementType.PLACEMENT_TYPE_FULLSCREEN;
          if (localObject1 == localObject2)
          {
            localObject1 = new com/inmobi/ads/ar;
            ((ar)localObject1).<init>();
          }
          else
          {
            localObject1 = ar.a();
          }
          c = ((ar)localObject1);
          i2 = d;
          if (i2 != 0)
          {
            localObject2 = c;
            ((ar)localObject2).setAudioSessionId(i2);
          }
          else
          {
            localObject1 = c;
            i2 = ((ar)localObject1).getAudioSessionId();
            d = i2;
          }
          try
          {
            localObject1 = c;
            localObject2 = getContext();
            localObject2 = ((Context)localObject2).getApplicationContext();
            localObject3 = a;
            localObject4 = b;
            ((ar)localObject1).setDataSource((Context)localObject2, (Uri)localObject3, (Map)localObject4);
          }
          catch (IOException localIOException)
          {
            localObject1 = c;
            a = i1;
            b = i1;
            return;
          }
        }
        int i2 = 1;
        Object localObject2 = null;
        try
        {
          localObject3 = getTag();
          localObject3 = (bb)localObject3;
          localObject4 = c;
          Object localObject5 = k;
          ((ar)localObject4).setOnPreparedListener((MediaPlayer.OnPreparedListener)localObject5);
          localObject4 = c;
          localObject5 = j;
          ((ar)localObject4).setOnVideoSizeChangedListener((MediaPlayer.OnVideoSizeChangedListener)localObject5);
          localObject4 = c;
          localObject5 = z;
          ((ar)localObject4).setOnCompletionListener((MediaPlayer.OnCompletionListener)localObject5);
          localObject4 = c;
          localObject5 = C;
          ((ar)localObject4).setOnErrorListener((MediaPlayer.OnErrorListener)localObject5);
          localObject4 = c;
          localObject5 = A;
          ((ar)localObject4).setOnInfoListener((MediaPlayer.OnInfoListener)localObject5);
          localObject4 = c;
          localObject5 = B;
          ((ar)localObject4).setOnBufferingUpdateListener((MediaPlayer.OnBufferingUpdateListener)localObject5);
          localObject4 = c;
          localObject5 = n;
          ((ar)localObject4).setSurface((Surface)localObject5);
          int i3 = Build.VERSION.SDK_INT;
          int i4 = 26;
          int i5 = 3;
          if (i3 >= i4)
          {
            localObject4 = c;
            localObject5 = new android/media/AudioAttributes$Builder;
            ((AudioAttributes.Builder)localObject5).<init>();
            localObject5 = ((AudioAttributes.Builder)localObject5).setUsage(i2);
            int i6 = 2;
            localObject5 = ((AudioAttributes.Builder)localObject5).setContentType(i6);
            localObject5 = ((AudioAttributes.Builder)localObject5).setLegacyStreamType(i5);
            localObject5 = ((AudioAttributes.Builder)localObject5).build();
            ((ar)localObject4).setAudioAttributes((AudioAttributes)localObject5);
          }
          else
          {
            localObject4 = c;
            ((ar)localObject4).setAudioStreamType(i5);
          }
          localObject4 = c;
          ((ar)localObject4).prepareAsync();
          v = 0;
          localObject4 = c;
          a = i2;
          h();
          if (localObject3 != null)
          {
            localObject4 = v;
            localObject5 = "shouldAutoPlay";
            localObject4 = ((Map)localObject4).get(localObject5);
            localObject4 = (Boolean)localObject4;
            boolean bool1 = ((Boolean)localObject4).booleanValue();
            if (bool1)
            {
              localObject4 = c;
              b = i5;
            }
            localObject3 = v;
            localObject4 = "didCompleteQ4";
            localObject3 = ((Map)localObject3).get(localObject4);
            localObject3 = (Boolean)localObject3;
            boolean bool2 = ((Boolean)localObject3).booleanValue();
            if (bool2)
            {
              int i7 = 8;
              a(i7, 0);
              return;
            }
          }
          a(0, 0);
          return;
        }
        catch (Exception localException)
        {
          localObject4 = c;
          a = i1;
          b = i1;
          C.onError((MediaPlayer)localObject4, i2, 0);
          localObject1 = com.inmobi.commons.core.a.a.a();
          com.inmobi.commons.core.e.a locala = new com/inmobi/commons/core/e/a;
          locala.<init>(localException);
          ((com.inmobi.commons.core.a.a)localObject1).a(locala);
          return;
        }
      }
    }
  }
  
  private void h()
  {
    Object localObject = c;
    if (localObject != null)
    {
      localObject = u;
      if (localObject != null)
      {
        ((NativeVideoController)localObject).setMediaPlayer(this);
        localObject = u;
        boolean bool = b();
        ((NativeVideoController)localObject).setEnabled(bool);
        localObject = u;
        ((NativeVideoController)localObject).a();
      }
    }
  }
  
  public final void a()
  {
    Surface localSurface = n;
    if (localSurface != null)
    {
      localSurface.release();
      localSurface = null;
      n = null;
    }
    c();
  }
  
  final void a(int paramInt)
  {
    boolean bool = b();
    if (bool)
    {
      ar localar = c;
      localar.seekTo(paramInt);
    }
  }
  
  final void a(int paramInt1, int paramInt2)
  {
    Object localObject = c;
    if (localObject != null)
    {
      localObject = ((NativeVideoWrapper)getParent()).getProgressBar();
      ImageView localImageView = ((NativeVideoWrapper)getParent()).getPoster();
      ((ProgressBar)localObject).setVisibility(paramInt1);
      localImageView.setVisibility(paramInt2);
    }
  }
  
  final boolean b()
  {
    ar localar = c;
    if (localar != null)
    {
      int i1 = a;
      int i2 = -1;
      if (i1 != i2)
      {
        localar = c;
        i1 = a;
        if (i1 != 0)
        {
          localar = c;
          i1 = a;
          i2 = 1;
          if (i1 != i2) {
            return i2;
          }
        }
      }
    }
    return false;
  }
  
  final void c()
  {
    Object localObject1 = c;
    if (localObject1 != null)
    {
      localObject1 = g;
      if (localObject1 != null)
      {
        i1 = 1;
        ((NativeVideoView.d)localObject1).removeMessages(i1);
      }
      localObject1 = getTag();
      if (localObject1 != null)
      {
        localObject1 = getTagv;
        str1 = "seekPosition";
        int i2 = getCurrentPosition();
        localObject2 = Integer.valueOf(i2);
        ((Map)localObject1).put(str1, localObject2);
      }
      localObject1 = c;
      a = 0;
      b = 0;
      ((ar)localObject1).reset();
      localObject1 = c;
      int i1 = 0;
      String str1 = null;
      ((ar)localObject1).setOnPreparedListener(null);
      c.setOnVideoSizeChangedListener(null);
      c.setOnCompletionListener(null);
      c.setOnErrorListener(null);
      c.setOnInfoListener(null);
      c.setOnBufferingUpdateListener(null);
      localObject1 = getTag();
      if (localObject1 != null)
      {
        localObject1 = (bb)getTag();
        localObject2 = AdContainer.RenderingProperties.PlacementType.PLACEMENT_TYPE_INLINE;
        localObject1 = v;
        String str2 = "placementType";
        localObject1 = ((Map)localObject1).get(str2);
        if (localObject2 == localObject1)
        {
          localObject1 = c;
          ((ar)localObject1).b();
        }
      }
      else
      {
        localObject1 = c;
        ((ar)localObject1).b();
      }
      localObject1 = getContext();
      Object localObject2 = "audio";
      localObject1 = (AudioManager)((Context)localObject1).getSystemService((String)localObject2);
      if (localObject1 != null) {
        ((AudioManager)localObject1).abandonAudioFocus(null);
      }
      c = null;
    }
  }
  
  public boolean canPause()
  {
    return w;
  }
  
  public boolean canSeekBackward()
  {
    return x;
  }
  
  public boolean canSeekForward()
  {
    return y;
  }
  
  public final void d()
  {
    Object localObject = c;
    if (localObject != null)
    {
      Integer localInteger = null;
      o = 0;
      String str = null;
      ((ar)localObject).setVolume(0.0F, 0.0F);
      localObject = getTag();
      if (localObject != null)
      {
        localObject = getTagv;
        str = "currentMediaVolume";
        localInteger = Integer.valueOf(0);
        ((Map)localObject).put(str, localInteger);
      }
    }
  }
  
  public final void e()
  {
    Object localObject = c;
    if (localObject != null)
    {
      o = 1;
      float f1 = 1.0F;
      ((ar)localObject).setVolume(f1, f1);
      localObject = getTag();
      if (localObject != null)
      {
        localObject = getTagv;
        String str = "currentMediaVolume";
        int i1 = 15;
        Integer localInteger = Integer.valueOf(i1);
        ((Map)localObject).put(str, localInteger);
      }
    }
  }
  
  public int getAudioSessionId()
  {
    int i1 = d;
    if (i1 == 0)
    {
      MediaPlayer localMediaPlayer = new android/media/MediaPlayer;
      localMediaPlayer.<init>();
      int i2 = localMediaPlayer.getAudioSessionId();
      d = i2;
      localMediaPlayer.release();
    }
    return d;
  }
  
  public int getBufferPercentage()
  {
    ar localar = c;
    if (localar != null) {
      return v;
    }
    return 0;
  }
  
  public int getCurrentPosition()
  {
    boolean bool = b();
    if (bool) {
      return c.getCurrentPosition();
    }
    return 0;
  }
  
  public int getDuration()
  {
    boolean bool = b();
    if (bool) {
      return c.getDuration();
    }
    return -1;
  }
  
  NativeVideoController getMediaController()
  {
    return u;
  }
  
  public ar getMediaPlayer()
  {
    return c;
  }
  
  public NativeVideoView.b getPlaybackEventListener()
  {
    return r;
  }
  
  public NativeVideoView.c getQuartileCompletedListener()
  {
    return q;
  }
  
  public int getState()
  {
    ar localar = c;
    if (localar != null) {
      return a;
    }
    return 0;
  }
  
  public int getVolume()
  {
    boolean bool = b();
    if (bool) {
      return o;
    }
    return -1;
  }
  
  public boolean isPlaying()
  {
    boolean bool = b();
    if (bool)
    {
      ar localar = c;
      bool = localar.isPlaying();
      if (bool) {
        return true;
      }
    }
    return false;
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    try
    {
      int i1 = e;
      i1 = getDefaultSize(i1, paramInt1);
      int i2 = f;
      i2 = getDefaultSize(i2, paramInt2);
      int i3 = e;
      if (i3 > 0)
      {
        i3 = f;
        if (i3 > 0)
        {
          i1 = View.MeasureSpec.getMode(paramInt1);
          paramInt1 = View.MeasureSpec.getSize(paramInt1);
          i2 = View.MeasureSpec.getMode(paramInt2);
          paramInt2 = View.MeasureSpec.getSize(paramInt2);
          i3 = 1073741824;
          if ((i1 == i3) && (i2 == i3))
          {
            i1 = e * paramInt2;
            i2 = f * paramInt1;
            if (i1 < i2)
            {
              paramInt2 = f * paramInt1;
              i1 = e;
              i2 = paramInt2 / i1;
              paramInt2 = i2;
              break label366;
            }
            i1 = e * paramInt2;
            i2 = f * paramInt1;
            if (i1 <= i2) {
              break label366;
            }
            paramInt1 = e * paramInt2;
            i1 = f;
            i1 = paramInt1 / i1;
            paramInt1 = i1;
            break label366;
          }
          int i4 = -1 << -1;
          if (i1 == i3)
          {
            i1 = f * paramInt1;
            i3 = e;
            i1 /= i3;
            if ((i2 == i4) && (i1 > paramInt2)) {
              break label366;
            }
            paramInt2 = i1;
            break label366;
          }
          if (i2 == i3)
          {
            i2 = e * paramInt2;
            i3 = f;
            i2 /= i3;
            if ((i1 == i4) && (i2 > paramInt1)) {
              break label366;
            }
          }
          else
          {
            i3 = e;
            int i5 = f;
            if ((i2 == i4) && (i5 > paramInt2))
            {
              i2 = e * paramInt2;
              i3 = f;
              i2 /= i3;
            }
            else
            {
              i2 = i3;
              paramInt2 = i5;
            }
            if ((i1 == i4) && (i2 > paramInt1))
            {
              paramInt2 = f * paramInt1;
              i1 = e;
              i2 = paramInt2 / i1;
              paramInt2 = i2;
              break label366;
            }
          }
          paramInt1 = i2;
          break label366;
        }
      }
      paramInt1 = i1;
      paramInt2 = i2;
      label366:
      setMeasuredDimension(paramInt1, paramInt2);
      return;
    }
    catch (Exception localException)
    {
      localException.getMessage();
    }
  }
  
  public void pause()
  {
    boolean bool = b();
    if (bool)
    {
      localObject1 = c;
      bool = ((ar)localObject1).isPlaying();
      if (bool)
      {
        bool = true;
        break label32;
      }
    }
    bool = false;
    Object localObject1 = null;
    label32:
    int i1 = 4;
    if (bool)
    {
      c.pause();
      c.a = i1;
      localObject1 = getTag();
      if (localObject1 != null)
      {
        localObject1 = (bb)getTag();
        Object localObject2 = v;
        Boolean localBoolean = Boolean.TRUE;
        ((Map)localObject2).put("didPause", localBoolean);
        localObject1 = v;
        localObject2 = "seekPosition";
        int i2 = getCurrentPosition();
        Integer localInteger = Integer.valueOf(i2);
        ((Map)localObject1).put(localObject2, localInteger);
      }
      localObject1 = getPlaybackEventListener();
      int i3 = 2;
      ((NativeVideoView.b)localObject1).a(i3);
    }
    localObject1 = c;
    if (localObject1 != null) {
      b = i1;
    }
    i = false;
  }
  
  public void seekTo(int paramInt) {}
  
  public void setIsLockScreen(boolean paramBoolean)
  {
    t = paramBoolean;
  }
  
  public void setMediaController(NativeVideoController paramNativeVideoController)
  {
    if (paramNativeVideoController != null)
    {
      u = paramNativeVideoController;
      h();
    }
  }
  
  public void setMediaErrorListener(NativeVideoView.a parama)
  {
    s = parama;
  }
  
  public void setPlaybackEventListener(NativeVideoView.b paramb)
  {
    r = paramb;
  }
  
  public void setQuartileCompletedListener(NativeVideoView.c paramc)
  {
    q = paramc;
  }
  
  public void setVideoPath(String paramString)
  {
    paramString = Uri.parse(paramString);
    setVideoURI(paramString);
  }
  
  public void setVideoURI(Uri paramUri)
  {
    setVideoURI(paramUri, null);
  }
  
  public void setVideoURI(Uri paramUri, Map paramMap)
  {
    a = paramUri;
    b = paramMap;
    g();
    requestLayout();
    invalidate();
  }
  
  public void start()
  {
    Object localObject1 = (PowerManager)getContext().getSystemService("power");
    Object localObject2 = getContext();
    Object localObject3 = "keyguard";
    localObject2 = (KeyguardManager)((Context)localObject2).getSystemService((String)localObject3);
    boolean bool1 = ((KeyguardManager)localObject2).inKeyguardRestrictedInputMode();
    int i1 = Build.VERSION.SDK_INT;
    int i2 = 20;
    boolean bool3;
    if (i1 < i2) {
      bool3 = ((PowerManager)localObject1).isScreenOn();
    } else {
      bool3 = ((PowerManager)localObject1).isInteractive();
    }
    boolean bool2 = b();
    bb localbb = (bb)getTag();
    int i3 = 0;
    int i4 = 1;
    if (localbb != null)
    {
      Object localObject4 = v;
      String str = "shouldAutoPlay";
      localObject4 = (Boolean)((Map)localObject4).get(str);
      bool4 = ((Boolean)localObject4).booleanValue();
      if (!bool4)
      {
        bool4 = false;
        localObject4 = null;
        break label148;
      }
    }
    boolean bool4 = true;
    label148:
    int i5 = 8;
    if ((bool2) && (!bool4)) {
      a(i5, 0);
    }
    int i6 = 3;
    if ((bool2) && (bool3))
    {
      localObject1 = c;
      bool3 = ((ar)localObject1).isPlaying();
      if ((!bool3) && (bool4))
      {
        bool3 = t;
        if ((bool3) || (!bool1))
        {
          if (localbb != null)
          {
            localObject1 = v;
            localObject2 = "didCompleteQ4";
            localObject1 = (Boolean)((Map)localObject1).get(localObject2);
            bool3 = ((Boolean)localObject1).booleanValue();
            if (!bool3)
            {
              localObject1 = v;
              localObject2 = "seekPosition";
              localObject1 = (Integer)((Map)localObject1).get(localObject2);
              i3 = ((Integer)localObject1).intValue();
            }
          }
          d();
          a(i3);
          c.start();
          localObject1 = c;
          a = i6;
          a(i5, i5);
          if (localbb != null)
          {
            localObject1 = v;
            localObject2 = "didCompleteQ4";
            localObject3 = Boolean.FALSE;
            ((Map)localObject1).put(localObject2, localObject3);
            bool3 = localbb.a();
            if (bool3) {
              e();
            }
            localObject1 = v;
            localObject2 = "didPause";
            localObject1 = (Boolean)((Map)localObject1).get(localObject2);
            bool3 = ((Boolean)localObject1).booleanValue();
            if (bool3)
            {
              getPlaybackEventListener().a(i6);
              localObject1 = v;
              localObject2 = "didPause";
              localObject3 = Boolean.FALSE;
              ((Map)localObject1).put(localObject2, localObject3);
            }
            else
            {
              localObject1 = getPlaybackEventListener();
              ((NativeVideoView.b)localObject1).a(i4);
            }
            localObject1 = g;
            if (localObject1 != null)
            {
              bool3 = ((NativeVideoView.d)localObject1).hasMessages(i4);
              if (!bool3)
              {
                localObject1 = g;
                ((NativeVideoView.d)localObject1).sendEmptyMessage(i4);
              }
            }
          }
          localObject1 = u;
          if (localObject1 != null) {
            ((NativeVideoController)localObject1).a();
          }
        }
      }
    }
    localObject1 = c;
    if (localObject1 != null) {
      b = i6;
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.NativeVideoView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */