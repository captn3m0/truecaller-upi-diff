package com.inmobi.ads;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;
import java.lang.ref.WeakReference;

final class InMobiNative$a
  extends Handler
{
  boolean a = true;
  private WeakReference b;
  
  public InMobiNative$a(InMobiNative paramInMobiNative)
  {
    super((Looper)localObject);
    localObject = new java/lang/ref/WeakReference;
    ((WeakReference)localObject).<init>(paramInMobiNative);
    b = ((WeakReference)localObject);
  }
  
  public final void handleMessage(Message paramMessage)
  {
    Object localObject1 = (InMobiNative)b.get();
    if (localObject1 == null)
    {
      paramMessage = Logger.InternalLogLevel.ERROR;
      localObject1 = InMobiNative.access$200();
      Logger.a(paramMessage, (String)localObject1, "Lost reference to InMobiNative! callback cannot be given");
      return;
    }
    Object localObject2 = InMobiNative.access$600((InMobiNative)localObject1);
    if (localObject2 == null)
    {
      paramMessage = Logger.InternalLogLevel.ERROR;
      localObject1 = InMobiNative.access$200();
      Logger.a(paramMessage, (String)localObject1, "InMobiNative is already destroyed! Callback cannot be given");
      return;
    }
    int i = what;
    boolean bool2 = true;
    switch (i)
    {
    default: 
      InMobiNative.access$200();
      return;
    case 11: 
      try
      {
        ((InMobiNative.NativeAdListener)localObject2).onUserSkippedMedia((InMobiNative)localObject1);
        return;
      }
      catch (Exception paramMessage)
      {
        localObject1 = Logger.InternalLogLevel.ERROR;
        localObject2 = InMobiNative.access$200();
        Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "Publisher handler caused unexpected error");
        InMobiNative.access$200();
        paramMessage.getMessage();
        return;
      }
    case 10: 
      try
      {
        ((InMobiNative.NativeAdListener)localObject2).onAdStatusChanged((InMobiNative)localObject1);
        return;
      }
      catch (Exception paramMessage)
      {
        localObject1 = Logger.InternalLogLevel.ERROR;
        localObject2 = InMobiNative.access$200();
        Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "Publisher handler caused unexpected error");
        InMobiNative.access$200();
        paramMessage.getMessage();
        return;
      }
    case 9: 
      try
      {
        ((InMobiNative.NativeAdListener)localObject2).onMediaPlaybackComplete((InMobiNative)localObject1);
        return;
      }
      catch (Exception paramMessage)
      {
        localObject1 = Logger.InternalLogLevel.ERROR;
        localObject2 = InMobiNative.access$200();
        Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "Publisher handler caused unexpected error");
        InMobiNative.access$200();
        paramMessage.getMessage();
        return;
      }
    case 8: 
      try
      {
        paramMessage = InMobiNative.access$700((InMobiNative)localObject1);
        if (paramMessage != null)
        {
          paramMessage = InMobiNative.access$700((InMobiNative)localObject1);
          paramMessage.onActionRequired((InMobiNative)localObject1);
        }
        ((InMobiNative.NativeAdListener)localObject2).onUserWillLeaveApplication((InMobiNative)localObject1);
        return;
      }
      catch (Exception paramMessage)
      {
        localObject1 = Logger.InternalLogLevel.ERROR;
        localObject2 = InMobiNative.access$200();
        Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "Publisher handler caused unexpected error");
        InMobiNative.access$200();
        paramMessage.getMessage();
        return;
      }
    case 7: 
      try
      {
        ((InMobiNative.NativeAdListener)localObject2).onAdClicked((InMobiNative)localObject1);
        return;
      }
      catch (Exception paramMessage)
      {
        localObject1 = Logger.InternalLogLevel.ERROR;
        localObject2 = InMobiNative.access$200();
        Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "Publisher handler caused unexpected error");
        InMobiNative.access$200();
        paramMessage.getMessage();
        return;
      }
    case 6: 
      try
      {
        ((InMobiNative.NativeAdListener)localObject2).onAdImpressed((InMobiNative)localObject1);
        return;
      }
      catch (Exception paramMessage)
      {
        localObject1 = Logger.InternalLogLevel.ERROR;
        localObject2 = InMobiNative.access$200();
        Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "Publisher handler caused unexpected error");
        InMobiNative.access$200();
        paramMessage.getMessage();
        return;
      }
    case 5: 
      try
      {
        ((InMobiNative.NativeAdListener)localObject2).onAdFullScreenDismissed((InMobiNative)localObject1);
        return;
      }
      catch (Exception paramMessage)
      {
        localObject1 = Logger.InternalLogLevel.ERROR;
        localObject2 = InMobiNative.access$200();
        Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "Publisher handler caused unexpected error");
        InMobiNative.access$200();
        paramMessage.getMessage();
        return;
      }
    case 4: 
      try
      {
        ((InMobiNative.NativeAdListener)localObject2).onAdFullScreenDisplayed((InMobiNative)localObject1);
        return;
      }
      catch (Exception paramMessage)
      {
        localObject1 = Logger.InternalLogLevel.ERROR;
        localObject2 = InMobiNative.access$200();
        Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "Publisher handler caused unexpected error");
        InMobiNative.access$200();
        paramMessage.getMessage();
        return;
      }
    case 3: 
      try
      {
        paramMessage = InMobiNative.access$700((InMobiNative)localObject1);
        if (paramMessage != null)
        {
          paramMessage = InMobiNative.access$700((InMobiNative)localObject1);
          paramMessage.onActionRequired((InMobiNative)localObject1);
        }
        ((InMobiNative.NativeAdListener)localObject2).onAdFullScreenWillDisplay((InMobiNative)localObject1);
        return;
      }
      catch (Exception paramMessage)
      {
        localObject1 = Logger.InternalLogLevel.ERROR;
        localObject2 = InMobiNative.access$200();
        Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "Publisher handler caused unexpected error");
        InMobiNative.access$200();
        paramMessage.getMessage();
        return;
      }
    case 2: 
      try
      {
        boolean bool1 = a;
        if (!bool1)
        {
          a = bool2;
          paramMessage = obj;
          paramMessage = (InMobiAdRequestStatus)paramMessage;
          ((InMobiNative.NativeAdListener)localObject2).onAdLoadFailed((InMobiNative)localObject1, paramMessage);
        }
        return;
      }
      catch (Exception paramMessage)
      {
        localObject1 = Logger.InternalLogLevel.ERROR;
        localObject2 = InMobiNative.access$200();
        Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "Publisher handler caused unexpected error");
        InMobiNative.access$200();
        paramMessage.getMessage();
        return;
      }
    }
    try
    {
      boolean bool3 = a;
      if (!bool3)
      {
        a = bool2;
        ((InMobiNative.NativeAdListener)localObject2).onAdLoadSucceeded((InMobiNative)localObject1);
      }
      return;
    }
    catch (Exception paramMessage)
    {
      localObject1 = Logger.InternalLogLevel.ERROR;
      localObject2 = InMobiNative.access$200();
      Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "Publisher handler caused unexpected error");
      InMobiNative.access$200();
      paramMessage.getMessage();
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.InMobiNative.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */