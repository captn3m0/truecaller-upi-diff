package com.inmobi.ads;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import com.inmobi.rendering.RenderView;
import java.lang.ref.WeakReference;

public class ap
  extends bw.a
  implements aq.b
{
  private static final String b = "ap";
  final aq a;
  private final ad c;
  private final aq.c d;
  private final aq.a e;
  private final bd f;
  
  public ap(Context paramContext, b paramb, ad paramad, ak paramak)
  {
    Object localObject = new com/inmobi/ads/ap$1;
    ((ap.1)localObject).<init>(this);
    d = ((aq.c)localObject);
    localObject = new com/inmobi/ads/ap$2;
    ((ap.2)localObject).<init>(this);
    e = ((aq.a)localObject);
    localObject = new com/inmobi/ads/ap$3;
    ((ap.3)localObject).<init>(this);
    f = ((bd)localObject);
    c = paramad;
    localObject = new com/inmobi/ads/aq;
    ad localad = c;
    aq.c localc = d;
    aq.a locala = e;
    ((aq)localObject).<init>(paramContext, paramb, localad, paramak, localc, locala, this);
    a = ((aq)localObject);
    NativeViewFactory.a(p);
    paramContext = a;
    paramb = f;
    a = paramb;
  }
  
  public final View a(View paramView, ViewGroup paramViewGroup, boolean paramBoolean, RenderView paramRenderView)
  {
    if (paramView == null)
    {
      if (paramBoolean) {
        paramView = a.b(null, paramViewGroup, paramRenderView);
      } else {
        paramView = a.a(null, paramViewGroup, paramRenderView);
      }
    }
    else
    {
      String str = "InMobiAdView";
      paramView = paramView.findViewWithTag(str);
      if (paramView != null)
      {
        paramView = (at)paramView;
        if (paramBoolean)
        {
          localObject = a;
          paramView = ((aq)localObject).b(paramView, paramViewGroup, paramRenderView);
        }
        else
        {
          localObject = a;
          paramView = ((aq)localObject).a(paramView, paramViewGroup, paramRenderView);
        }
      }
      else if (paramBoolean)
      {
        paramView = a.b(null, paramViewGroup, paramRenderView);
      }
      else
      {
        paramView = a.a(null, paramViewGroup, paramRenderView);
      }
    }
    paramViewGroup = c;
    Object localObject = new java/lang/ref/WeakReference;
    ((WeakReference)localObject).<init>(paramViewGroup);
    a = ((WeakReference)localObject);
    paramView.setTag("InMobiAdView");
    return paramView;
  }
  
  public final void a()
  {
    a.b();
    super.a();
  }
  
  public final void a(ay paramay)
  {
    int i = k;
    int j = 1;
    if (i == j)
    {
      paramay = c;
      paramay.b();
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.ap
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */