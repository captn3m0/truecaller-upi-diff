package com.inmobi.ads;

import android.os.Looper;
import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;
import java.lang.ref.WeakReference;

final class x$2
  implements Runnable
{
  x$2(x paramx, WeakReference paramWeakReference) {}
  
  public final void run()
  {
    j.b localb = (j.b)a.get();
    if (localb != null) {
      try
      {
        Object localObject1 = b;
        boolean bool = x.b((x)localObject1);
        if (bool)
        {
          localObject1 = b;
          localObject3 = b;
          localObject3 = f;
          localObject4 = new com/inmobi/ads/x$2$1;
          ((x.2.1)localObject4).<init>(this, localb);
          Looper localLooper = Looper.getMainLooper();
          ((x)localObject1).a(localb, (String)localObject3, (Runnable)localObject4, localLooper);
          return;
        }
        localObject1 = Logger.InternalLogLevel.ERROR;
        localObject3 = InMobiInterstitial.class;
        localObject3 = ((Class)localObject3).getSimpleName();
        localObject4 = "Unable to Show Ad, canShowAd Failed";
        Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject3, (String)localObject4);
        localObject1 = b;
        x.a((x)localObject1, localb);
      }
      catch (x.c localc)
      {
        localObject3 = Logger.InternalLogLevel.ERROR;
        localObject4 = InMobiInterstitial.class.getSimpleName();
        String str = localc.getMessage();
        Logger.a((Logger.InternalLogLevel)localObject3, (String)localObject4, str);
        x.a(b, localb);
        return;
      }
      catch (x.b localb1)
      {
        Object localObject3 = Logger.InternalLogLevel.ERROR;
        Object localObject4 = InMobiInterstitial.class.getSimpleName();
        Object localObject2 = localb1.getMessage();
        Logger.a((Logger.InternalLogLevel)localObject3, (String)localObject4, (String)localObject2);
        localObject2 = b;
        x.a((x)localObject2, localb);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.x.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */