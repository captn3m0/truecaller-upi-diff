package com.inmobi.ads;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

final class NativeViewFactory$9
  extends NativeViewFactory.c
{
  NativeViewFactory$9(NativeViewFactory paramNativeViewFactory)
  {
    super(paramNativeViewFactory);
  }
  
  protected final View a(Context paramContext)
  {
    ImageView localImageView = new android/widget/ImageView;
    paramContext = paramContext.getApplicationContext();
    localImageView.<init>(paramContext);
    return localImageView;
  }
  
  protected final void a(View paramView, ag paramag, b paramb)
  {
    super.a(paramView, paramag, paramb);
    NativeViewFactory.a((ImageView)paramView, paramag);
  }
  
  public final boolean a(View paramView)
  {
    boolean bool = paramView instanceof ImageView;
    if (!bool) {
      return false;
    }
    ((ImageView)paramView).setImageDrawable(null);
    return super.a((View)paramView);
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.NativeViewFactory.9
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */