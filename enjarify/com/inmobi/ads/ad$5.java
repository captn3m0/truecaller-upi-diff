package com.inmobi.ads;

import android.content.Context;
import android.content.Intent;
import com.inmobi.commons.a.a;
import com.inmobi.rendering.InMobiAdActivity;
import java.lang.ref.WeakReference;

final class ad$5
  implements Runnable
{
  ad$5(ad paramad) {}
  
  public final void run()
  {
    ad localad = ad.b(a);
    if (localad == null)
    {
      localad = a;
      ad.a(localad);
    }
    int i = InMobiAdActivity.a(ad.b(a));
    Intent localIntent = new android/content/Intent;
    Object localObject = (Context)a.m.get();
    Class localClass = InMobiAdActivity.class;
    localIntent.<init>((Context)localObject, localClass);
    localObject = "com.inmobi.rendering.InMobiAdActivity.EXTRA_AD_CONTAINER_INDEX";
    localIntent.putExtra((String)localObject, i);
    localIntent.putExtra("com.inmobi.rendering.InMobiAdActivity.EXTRA_AD_ACTIVITY_TYPE", 102);
    localIntent.putExtra("com.inmobi.rendering.InMobiAdActivity.EXTRA_AD_ACTIVITY_IS_FULL_SCREEN", true);
    int j = 201;
    localIntent.putExtra("com.inmobi.rendering.InMobiAdActivity.EXTRA_AD_CONTAINER_TYPE", j);
    localad = a;
    boolean bool = r;
    if (bool)
    {
      a.s = localIntent;
      return;
    }
    a.a((Context)a.m.get(), localIntent);
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.ad.5
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */