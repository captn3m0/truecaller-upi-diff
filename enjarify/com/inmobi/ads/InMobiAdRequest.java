package com.inmobi.ads;

import java.util.Map;

public class InMobiAdRequest
{
  private final Map mExtras;
  private final int mHeightInDp;
  private final String mKeywords;
  private final InMobiAdRequest.MonetizationContext mMonetizationContext;
  private final long mPlacementId;
  private final int mWidthInDp;
  
  private InMobiAdRequest(long paramLong, InMobiAdRequest.MonetizationContext paramMonetizationContext, int paramInt1, int paramInt2, String paramString, Map paramMap)
  {
    mPlacementId = paramLong;
    mMonetizationContext = paramMonetizationContext;
    mWidthInDp = paramInt1;
    mHeightInDp = paramInt2;
    mKeywords = paramString;
    mExtras = paramMap;
  }
  
  Map getExtras()
  {
    return mExtras;
  }
  
  int getHeight()
  {
    return mHeightInDp;
  }
  
  String getKeywords()
  {
    return mKeywords;
  }
  
  InMobiAdRequest.MonetizationContext getMonetizationContext()
  {
    return mMonetizationContext;
  }
  
  long getPlacementId()
  {
    return mPlacementId;
  }
  
  int getWidth()
  {
    return mWidthInDp;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.InMobiAdRequest
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */