package com.inmobi.ads;

import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;
import com.inmobi.rendering.RenderView;
import java.util.HashMap;
import java.util.Map;

class n
  extends j
  implements Application.ActivityLifecycleCallbacks
{
  private static final String A = InMobiBanner.class.getSimpleName();
  private static final String z = "n";
  private boolean B = true;
  private int C = 0;
  boolean x = false;
  String y;
  
  n(Context paramContext, long paramLong, j.b paramb)
  {
    super(paramContext, paramLong, paramb);
    boolean bool = paramContext instanceof Activity;
    if (bool)
    {
      paramContext = ((Activity)paramContext).getApplication();
      paramContext.registerActivityLifecycleCallbacks(this);
    }
  }
  
  final void J()
  {
    RenderView localRenderView = (RenderView)i();
    if (localRenderView == null) {
      return;
    }
    x = true;
    localRenderView.a();
  }
  
  final boolean K()
  {
    int i = a;
    int j = 8;
    return i == j;
  }
  
  public final void L()
  {
    int i = a;
    int j = 4;
    if (i != j)
    {
      j = 7;
      if (i != j)
      {
        j = 8;
        if (i != j) {
          return;
        }
      }
    }
    Object localObject = i();
    if (localObject != null)
    {
      localObject = ((AdContainer)localObject).getViewableAd();
      if (localObject != null) {
        ((bw)localObject).d();
      }
    }
  }
  
  public final void M()
  {
    int i = a;
    int j = 4;
    if (i != j)
    {
      j = 7;
      if (i != j)
      {
        j = 8;
        if (i != j) {
          return;
        }
      }
    }
    Object localObject = i();
    if (localObject != null)
    {
      localObject = ((AdContainer)localObject).getViewableAd();
      if (localObject != null)
      {
        j = 0;
        View[] arrayOfView = new View[0];
        ((bw)localObject).a(arrayOfView);
      }
    }
  }
  
  public final void N()
  {
    Object localObject = a();
    boolean bool = localObject instanceof Activity;
    if (bool)
    {
      localObject = ((Activity)a()).getApplication();
      ((Application)localObject).unregisterActivityLifecycleCallbacks(this);
    }
  }
  
  public final void a(Context paramContext)
  {
    super.a(paramContext);
    boolean bool = paramContext instanceof Activity;
    if (bool)
    {
      paramContext = ((Activity)paramContext).getApplication();
      paramContext.registerActivityLifecycleCallbacks(this);
    }
  }
  
  public final void a(RenderView paramRenderView)
  {
    try
    {
      super.a(paramRenderView);
      int i = a;
      int j = 2;
      if (i == j)
      {
        y();
        i = 4;
        a = i;
        B();
        paramRenderView = Logger.InternalLogLevel.DEBUG;
        localObject1 = A;
        localObject2 = new java/lang/StringBuilder;
        String str = "Successfully loaded Banner ad markup in the WebView for placement id: ";
        ((StringBuilder)localObject2).<init>(str);
        long l = b;
        ((StringBuilder)localObject2).append(l);
        localObject2 = ((StringBuilder)localObject2).toString();
        Logger.a(paramRenderView, (String)localObject1, (String)localObject2);
        paramRenderView = f();
        if (paramRenderView != null)
        {
          paramRenderView = f();
          paramRenderView.a();
        }
        q();
      }
      return;
    }
    catch (Exception paramRenderView)
    {
      Object localObject1 = Logger.InternalLogLevel.ERROR;
      Object localObject2 = A;
      Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "Unable to load ad; SDK encountered an internal error");
      paramRenderView.getMessage();
    }
  }
  
  protected final String b()
  {
    return "banner";
  }
  
  protected final void b(a parama) {}
  
  public final void b(RenderView paramRenderView)
  {
    try
    {
      super.b(paramRenderView);
      int i = a;
      int j = 4;
      if (i == j)
      {
        i = 7;
        a = i;
        paramRenderView = "AdRendered";
        d(paramRenderView);
      }
      return;
    }
    catch (Exception paramRenderView)
    {
      Logger.InternalLogLevel localInternalLogLevel = Logger.InternalLogLevel.ERROR;
      String str = A;
      Logger.a(localInternalLogLevel, str, "Unable to load ad; SDK encountered an internal error");
      paramRenderView.getMessage();
    }
  }
  
  public final void b(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      localInternalLogLevel = Logger.InternalLogLevel.DEBUG;
      str1 = A;
      localObject = new java/lang/StringBuilder;
      String str2 = "Initiating Banner refresh for placement id: ";
      ((StringBuilder)localObject).<init>(str2);
      l = b;
      ((StringBuilder)localObject).append(l);
      localObject = ((StringBuilder)localObject).toString();
      Logger.a(localInternalLogLevel, str1, (String)localObject);
    }
    Logger.InternalLogLevel localInternalLogLevel = Logger.InternalLogLevel.DEBUG;
    String str1 = A;
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("Fetching a Banner ad for placement id: ");
    long l = b;
    ((StringBuilder)localObject).append(l);
    localObject = ((StringBuilder)localObject).toString();
    Logger.a(localInternalLogLevel, str1, (String)localObject);
    v = false;
    B = paramBoolean;
    super.l();
  }
  
  protected final String c()
  {
    return y;
  }
  
  public final void c(long paramLong, a parama)
  {
    try
    {
      super.c(paramLong, parama);
      parama = Logger.InternalLogLevel.DEBUG;
      Object localObject1 = A;
      Object localObject2 = new java/lang/StringBuilder;
      String str = "Banner ad fetch successful for placement id: ";
      ((StringBuilder)localObject2).<init>(str);
      long l1 = b;
      ((StringBuilder)localObject2).append(l1);
      localObject2 = ((StringBuilder)localObject2).toString();
      Logger.a(parama, (String)localObject1, (String)localObject2);
      long l2 = b;
      boolean bool = paramLong < l2;
      if (!bool)
      {
        int i = a;
        int j = 2;
        if (i == j)
        {
          i = 0;
          Object localObject3 = null;
          localObject4 = j();
          a(false, (RenderView)localObject4);
          try
          {
            localObject3 = Logger.InternalLogLevel.DEBUG;
            localObject4 = A;
            parama = new java/lang/StringBuilder;
            localObject1 = "Started loading banner ad markup in WebView for placement id: ";
            parama.<init>((String)localObject1);
            l2 = b;
            parama.append(l2);
            parama = parama.toString();
            Logger.a((Logger.InternalLogLevel)localObject3, (String)localObject4, parama);
            localObject3 = f;
            j = 0;
            localObject4 = null;
            a(null, (String)localObject3, null, null);
            return;
          }
          catch (Exception localException1)
          {
            y();
            localObject4 = f();
            if (localObject4 != null)
            {
              localObject4 = f();
              parama = new com/inmobi/ads/InMobiAdRequestStatus;
              localObject1 = InMobiAdRequestStatus.StatusCode.INTERNAL_ERROR;
              parama.<init>((InMobiAdRequestStatus.StatusCode)localObject1);
              ((j.b)localObject4).a(parama);
            }
            localObject4 = Logger.InternalLogLevel.ERROR;
            parama = A;
            localObject1 = "Unable to load ad; SDK encountered an internal error";
            Logger.a((Logger.InternalLogLevel)localObject4, parama, (String)localObject1);
            localException1.getMessage();
          }
        }
      }
      return;
    }
    catch (Exception localException2)
    {
      Object localObject4 = Logger.InternalLogLevel.ERROR;
      parama = A;
      Logger.a((Logger.InternalLogLevel)localObject4, parama, "Unable to load ad; SDK encountered an internal error");
      localException2.getMessage();
    }
  }
  
  /* Error */
  public final void c(RenderView paramRenderView)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_1
    //   4: invokespecial 204	com/inmobi/ads/j:c	(Lcom/inmobi/rendering/RenderView;)V
    //   7: aload_0
    //   8: getfield 63	com/inmobi/ads/j:a	I
    //   11: istore_2
    //   12: bipush 7
    //   14: istore_3
    //   15: bipush 8
    //   17: istore 4
    //   19: iload_2
    //   20: iload_3
    //   21: if_icmpne +98 -> 119
    //   24: aload_0
    //   25: getfield 39	com/inmobi/ads/n:C	I
    //   28: iconst_1
    //   29: iadd
    //   30: istore_2
    //   31: aload_0
    //   32: iload_2
    //   33: putfield 39	com/inmobi/ads/n:C	I
    //   36: aload_0
    //   37: iload 4
    //   39: putfield 63	com/inmobi/ads/j:a	I
    //   42: getstatic 105	com/inmobi/commons/core/utilities/Logger$InternalLogLevel:DEBUG	Lcom/inmobi/commons/core/utilities/Logger$InternalLogLevel;
    //   45: astore_1
    //   46: getstatic 28	com/inmobi/ads/n:A	Ljava/lang/String;
    //   49: astore 5
    //   51: new 107	java/lang/StringBuilder
    //   54: astore 6
    //   56: ldc -50
    //   58: astore 7
    //   60: aload 6
    //   62: aload 7
    //   64: invokespecial 112	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   67: aload_0
    //   68: getfield 116	com/inmobi/ads/j:b	J
    //   71: lstore 8
    //   73: aload 6
    //   75: lload 8
    //   77: invokevirtual 120	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   80: pop
    //   81: aload 6
    //   83: invokevirtual 123	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   86: astore 6
    //   88: aload_1
    //   89: aload 5
    //   91: aload 6
    //   93: invokestatic 128	com/inmobi/commons/core/utilities/Logger:a	(Lcom/inmobi/commons/core/utilities/Logger$InternalLogLevel;Ljava/lang/String;Ljava/lang/String;)V
    //   96: aload_0
    //   97: invokevirtual 132	com/inmobi/ads/n:f	()Lcom/inmobi/ads/j$b;
    //   100: astore_1
    //   101: aload_1
    //   102: ifnull +40 -> 142
    //   105: aload_0
    //   106: invokevirtual 132	com/inmobi/ads/n:f	()Lcom/inmobi/ads/j$b;
    //   109: astore_1
    //   110: aload_1
    //   111: invokeinterface 207 1 0
    //   116: aload_0
    //   117: monitorexit
    //   118: return
    //   119: aload_0
    //   120: getfield 63	com/inmobi/ads/j:a	I
    //   123: istore_2
    //   124: iload_2
    //   125: iload 4
    //   127: if_icmpne +15 -> 142
    //   130: aload_0
    //   131: getfield 39	com/inmobi/ads/n:C	I
    //   134: iconst_1
    //   135: iadd
    //   136: istore_2
    //   137: aload_0
    //   138: iload_2
    //   139: putfield 39	com/inmobi/ads/n:C	I
    //   142: aload_0
    //   143: monitorexit
    //   144: return
    //   145: astore_1
    //   146: goto +35 -> 181
    //   149: astore_1
    //   150: getstatic 141	com/inmobi/commons/core/utilities/Logger$InternalLogLevel:ERROR	Lcom/inmobi/commons/core/utilities/Logger$InternalLogLevel;
    //   153: astore 5
    //   155: getstatic 28	com/inmobi/ads/n:A	Ljava/lang/String;
    //   158: astore 6
    //   160: ldc -47
    //   162: astore 7
    //   164: aload 5
    //   166: aload 6
    //   168: aload 7
    //   170: invokestatic 128	com/inmobi/commons/core/utilities/Logger:a	(Lcom/inmobi/commons/core/utilities/Logger$InternalLogLevel;Ljava/lang/String;Ljava/lang/String;)V
    //   173: aload_1
    //   174: invokevirtual 148	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   177: pop
    //   178: aload_0
    //   179: monitorexit
    //   180: return
    //   181: aload_0
    //   182: monitorexit
    //   183: aload_1
    //   184: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	185	0	this	n
    //   0	185	1	paramRenderView	RenderView
    //   11	128	2	i	int
    //   14	8	3	j	int
    //   17	111	4	k	int
    //   49	116	5	localObject1	Object
    //   54	113	6	localObject2	Object
    //   58	111	7	str	String
    //   71	5	8	l	long
    // Exception table:
    //   from	to	target	type
    //   3	7	145	finally
    //   7	11	145	finally
    //   24	28	145	finally
    //   32	36	145	finally
    //   37	42	145	finally
    //   42	45	145	finally
    //   46	49	145	finally
    //   51	54	145	finally
    //   62	67	145	finally
    //   67	71	145	finally
    //   75	81	145	finally
    //   81	86	145	finally
    //   91	96	145	finally
    //   96	100	145	finally
    //   105	109	145	finally
    //   110	116	145	finally
    //   119	123	145	finally
    //   130	134	145	finally
    //   138	142	145	finally
    //   150	153	145	finally
    //   155	158	145	finally
    //   168	173	145	finally
    //   173	178	145	finally
    //   3	7	149	java/lang/Exception
    //   7	11	149	java/lang/Exception
    //   24	28	149	java/lang/Exception
    //   32	36	149	java/lang/Exception
    //   37	42	149	java/lang/Exception
    //   42	45	149	java/lang/Exception
    //   46	49	149	java/lang/Exception
    //   51	54	149	java/lang/Exception
    //   62	67	149	java/lang/Exception
    //   67	71	149	java/lang/Exception
    //   75	81	149	java/lang/Exception
    //   81	86	149	java/lang/Exception
    //   91	96	149	java/lang/Exception
    //   96	100	149	java/lang/Exception
    //   105	109	149	java/lang/Exception
    //   110	116	149	java/lang/Exception
    //   119	123	149	java/lang/Exception
    //   130	134	149	java/lang/Exception
    //   138	142	149	java/lang/Exception
  }
  
  protected final AdContainer.RenderingProperties.PlacementType d()
  {
    return AdContainer.RenderingProperties.PlacementType.PLACEMENT_TYPE_INLINE;
  }
  
  /* Error */
  public final void d(RenderView paramRenderView)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_1
    //   4: invokespecial 217	com/inmobi/ads/j:d	(Lcom/inmobi/rendering/RenderView;)V
    //   7: aload_0
    //   8: getfield 63	com/inmobi/ads/j:a	I
    //   11: istore_2
    //   12: bipush 8
    //   14: istore_3
    //   15: iload_2
    //   16: iload_3
    //   17: if_icmpne +47 -> 64
    //   20: aload_0
    //   21: getfield 39	com/inmobi/ads/n:C	I
    //   24: iconst_m1
    //   25: iadd
    //   26: istore_2
    //   27: aload_0
    //   28: iload_2
    //   29: putfield 39	com/inmobi/ads/n:C	I
    //   32: iload_2
    //   33: ifne +31 -> 64
    //   36: bipush 7
    //   38: istore_2
    //   39: aload_0
    //   40: iload_2
    //   41: putfield 63	com/inmobi/ads/j:a	I
    //   44: aload_0
    //   45: invokevirtual 132	com/inmobi/ads/n:f	()Lcom/inmobi/ads/j$b;
    //   48: astore_1
    //   49: aload_1
    //   50: ifnull +14 -> 64
    //   53: aload_0
    //   54: invokevirtual 132	com/inmobi/ads/n:f	()Lcom/inmobi/ads/j$b;
    //   57: astore_1
    //   58: aload_1
    //   59: invokeinterface 220 1 0
    //   64: aload_0
    //   65: monitorexit
    //   66: return
    //   67: astore_1
    //   68: goto +35 -> 103
    //   71: astore_1
    //   72: getstatic 141	com/inmobi/commons/core/utilities/Logger$InternalLogLevel:ERROR	Lcom/inmobi/commons/core/utilities/Logger$InternalLogLevel;
    //   75: astore 4
    //   77: getstatic 28	com/inmobi/ads/n:A	Ljava/lang/String;
    //   80: astore 5
    //   82: ldc -34
    //   84: astore 6
    //   86: aload 4
    //   88: aload 5
    //   90: aload 6
    //   92: invokestatic 128	com/inmobi/commons/core/utilities/Logger:a	(Lcom/inmobi/commons/core/utilities/Logger$InternalLogLevel;Ljava/lang/String;Ljava/lang/String;)V
    //   95: aload_1
    //   96: invokevirtual 148	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   99: pop
    //   100: aload_0
    //   101: monitorexit
    //   102: return
    //   103: aload_0
    //   104: monitorexit
    //   105: aload_1
    //   106: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	107	0	this	n
    //   0	107	1	paramRenderView	RenderView
    //   11	30	2	i	int
    //   14	4	3	j	int
    //   75	12	4	localInternalLogLevel	Logger.InternalLogLevel
    //   80	9	5	str1	String
    //   84	7	6	str2	String
    // Exception table:
    //   from	to	target	type
    //   3	7	67	finally
    //   7	11	67	finally
    //   20	24	67	finally
    //   28	32	67	finally
    //   40	44	67	finally
    //   44	48	67	finally
    //   53	57	67	finally
    //   58	64	67	finally
    //   72	75	67	finally
    //   77	80	67	finally
    //   90	95	67	finally
    //   95	100	67	finally
    //   3	7	71	java/lang/Exception
    //   7	11	71	java/lang/Exception
    //   20	24	71	java/lang/Exception
    //   28	32	71	java/lang/Exception
    //   40	44	71	java/lang/Exception
    //   44	48	71	java/lang/Exception
    //   53	57	71	java/lang/Exception
    //   58	64	71	java/lang/Exception
  }
  
  protected final Map e()
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    String str1 = "u-rt";
    boolean bool = B;
    if (bool) {
      str2 = "1";
    } else {
      str2 = "0";
    }
    localHashMap.put(str1, str2);
    String str2 = y;
    localHashMap.put("mk-ad-slot", str2);
    return localHashMap;
  }
  
  protected final RenderView j()
  {
    RenderView localRenderView = super.j();
    boolean bool = x;
    if (bool) {
      localRenderView.a();
    }
    return localRenderView;
  }
  
  public final void n()
  {
    a = 1;
    super.n();
  }
  
  public void onActivityCreated(Activity paramActivity, Bundle paramBundle) {}
  
  public void onActivityDestroyed(Activity paramActivity)
  {
    Object localObject = a();
    if (localObject != null)
    {
      boolean bool = localObject.equals(paramActivity);
      if (bool)
      {
        localObject = (Activity)localObject;
        paramActivity = ((Activity)localObject).getApplication();
        paramActivity.unregisterActivityLifecycleCallbacks(this);
        r();
      }
    }
  }
  
  public void onActivityPaused(Activity paramActivity) {}
  
  public void onActivityResumed(Activity paramActivity) {}
  
  public void onActivitySaveInstanceState(Activity paramActivity, Bundle paramBundle) {}
  
  public void onActivityStarted(Activity paramActivity)
  {
    Context localContext = a();
    if (localContext != null)
    {
      boolean bool = localContext.equals(paramActivity);
      if (bool) {
        M();
      }
    }
  }
  
  public void onActivityStopped(Activity paramActivity)
  {
    Context localContext = a();
    if (localContext != null)
    {
      boolean bool = localContext.equals(paramActivity);
      if (bool) {
        L();
      }
    }
  }
  
  protected final boolean p()
  {
    int i = a;
    String str = null;
    int j = 1;
    if (j != i)
    {
      i = 2;
      int k = a;
      if (i != k)
      {
        i = a;
        k = 8;
        if (i == k)
        {
          localObject1 = new com/inmobi/ads/InMobiAdRequestStatus;
          localObject2 = InMobiAdRequestStatus.StatusCode.AD_ACTIVE;
          ((InMobiAdRequestStatus)localObject1).<init>((InMobiAdRequestStatus.StatusCode)localObject2);
          a((InMobiAdRequestStatus)localObject1, false);
          localObject1 = Logger.InternalLogLevel.ERROR;
          str = A;
          localObject2 = new java/lang/StringBuilder;
          ((StringBuilder)localObject2).<init>("An ad is currently being viewed by the user. Please wait for the user to close the ad before requesting for another ad for placement id: ");
          l = b;
          ((StringBuilder)localObject2).append(l);
          localObject2 = ((StringBuilder)localObject2).toString();
          Logger.a((Logger.InternalLogLevel)localObject1, str, (String)localObject2);
          return j;
        }
        return false;
      }
    }
    Object localObject1 = new com/inmobi/ads/InMobiAdRequestStatus;
    Object localObject2 = InMobiAdRequestStatus.StatusCode.REQUEST_PENDING;
    ((InMobiAdRequestStatus)localObject1).<init>((InMobiAdRequestStatus.StatusCode)localObject2);
    a((InMobiAdRequestStatus)localObject1, false);
    localObject1 = Logger.InternalLogLevel.ERROR;
    str = A;
    localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("An ad load is already in progress. Please wait for the load to complete before requesting for another ad for placement id: ");
    long l = b;
    ((StringBuilder)localObject2).append(l);
    localObject2 = ((StringBuilder)localObject2).toString();
    Logger.a((Logger.InternalLogLevel)localObject1, str, (String)localObject2);
    return j;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */