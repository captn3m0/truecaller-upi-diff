package com.inmobi.ads;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

public class NativeVideoWrapper
  extends RelativeLayout
{
  private static final String b = "NativeVideoWrapper";
  NativeVideoView a;
  private ImageView c;
  private ProgressBar d;
  private NativeVideoController e;
  private bd f;
  
  public NativeVideoWrapper(Context paramContext)
  {
    super(paramContext);
    paramContext = new com/inmobi/ads/NativeVideoView;
    Object localObject1 = getContext();
    paramContext.<init>((Context)localObject1);
    a = paramContext;
    paramContext = new android/widget/RelativeLayout$LayoutParams;
    int i = -1;
    paramContext.<init>(i, i);
    int j = 13;
    paramContext.addRule(j);
    Object localObject2 = a;
    addView((View)localObject2, paramContext);
    localObject2 = new android/widget/ImageView;
    Object localObject3 = getContext();
    ((ImageView)localObject2).<init>((Context)localObject3);
    c = ((ImageView)localObject2);
    localObject2 = c;
    localObject3 = ImageView.ScaleType.FIT_XY;
    ((ImageView)localObject2).setScaleType((ImageView.ScaleType)localObject3);
    localObject2 = c;
    int k = 8;
    ((ImageView)localObject2).setVisibility(k);
    localObject2 = c;
    addView((View)localObject2, paramContext);
    paramContext = new android/widget/ProgressBar;
    localObject2 = getContext();
    paramContext.<init>((Context)localObject2);
    d = paramContext;
    d.setVisibility(k);
    paramContext = new android/widget/RelativeLayout$LayoutParams;
    int m = -2;
    paramContext.<init>(m, m);
    paramContext.addRule(j);
    localObject2 = d;
    addView((View)localObject2, paramContext);
    paramContext = new com/inmobi/ads/NativeVideoController;
    localObject2 = getContext();
    paramContext.<init>((Context)localObject2);
    e = paramContext;
    paramContext = new android/widget/RelativeLayout$LayoutParams;
    paramContext.<init>(i, i);
    paramContext.addRule(j);
    localObject1 = a;
    NativeVideoController localNativeVideoController = e;
    ((NativeVideoView)localObject1).setMediaController(localNativeVideoController);
    localObject1 = e;
    addView((View)localObject1, paramContext);
  }
  
  public ImageView getPoster()
  {
    return c;
  }
  
  public ProgressBar getProgressBar()
  {
    return d;
  }
  
  public NativeVideoController getVideoController()
  {
    return e;
  }
  
  public NativeVideoView getVideoView()
  {
    return a;
  }
  
  public void setPosterImage(Bitmap paramBitmap)
  {
    c.setImageBitmap(paramBitmap);
  }
  
  public void setVideoEventListener(bd parambd)
  {
    f = parambd;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.NativeVideoWrapper
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */