package com.inmobi.ads;

public enum InMobiAdRequest$MonetizationContext
{
  private final String mValue;
  
  static
  {
    Object localObject = new com/inmobi/ads/InMobiAdRequest$MonetizationContext;
    ((MonetizationContext)localObject).<init>("MONETIZATION_CONTEXT_ACTIVITY", 0, "activity");
    MONETIZATION_CONTEXT_ACTIVITY = (MonetizationContext)localObject;
    localObject = new com/inmobi/ads/InMobiAdRequest$MonetizationContext;
    int i = 1;
    ((MonetizationContext)localObject).<init>("MONETIZATION_CONTEXT_OTHER", i, "others");
    MONETIZATION_CONTEXT_OTHER = (MonetizationContext)localObject;
    localObject = new MonetizationContext[2];
    MonetizationContext localMonetizationContext = MONETIZATION_CONTEXT_ACTIVITY;
    localObject[0] = localMonetizationContext;
    localMonetizationContext = MONETIZATION_CONTEXT_OTHER;
    localObject[i] = localMonetizationContext;
    $VALUES = (MonetizationContext[])localObject;
  }
  
  private InMobiAdRequest$MonetizationContext(String paramString1)
  {
    mValue = paramString1;
  }
  
  static MonetizationContext fromValue(String paramString)
  {
    MonetizationContext[] arrayOfMonetizationContext = values();
    int i = arrayOfMonetizationContext.length;
    int j = 0;
    while (j < i)
    {
      MonetizationContext localMonetizationContext = arrayOfMonetizationContext[j];
      String str = localMonetizationContext.getValue();
      boolean bool = str.equalsIgnoreCase(paramString);
      if (bool) {
        return localMonetizationContext;
      }
      j += 1;
    }
    return null;
  }
  
  final String getValue()
  {
    return mValue;
  }
  
  public final String toString()
  {
    return mValue;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.InMobiAdRequest.MonetizationContext
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */