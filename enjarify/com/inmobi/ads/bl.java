package com.inmobi.ads;

import android.content.Context;
import android.graphics.Point;
import android.os.Build.VERSION;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.f;
import android.support.v4.view.o;
import android.view.MotionEvent;
import android.widget.FrameLayout.LayoutParams;

class bl
  extends NativeScrollableContainer
  implements ViewPager.f
{
  private static final String b = "bl";
  NativeScrollableContainer.a a;
  private ViewPager c;
  private Point d;
  private Point e;
  private boolean f;
  private boolean g;
  
  public bl(Context paramContext)
  {
    super(paramContext, 0);
    paramContext = new android/graphics/Point;
    paramContext.<init>();
    d = paramContext;
    paramContext = new android/graphics/Point;
    paramContext.<init>();
    e = paramContext;
    g = false;
    setClipChildren(false);
    setLayerType(1, null);
    paramContext = new android/support/v4/view/ViewPager;
    Context localContext = getContext();
    paramContext.<init>(localContext);
    c = paramContext;
    c.a(this);
    paramContext = c;
    addView(paramContext);
  }
  
  public final void a(ai paramai, au paramau, int paramInt1, int paramInt2, NativeScrollableContainer.a parama)
  {
    paramai = (FrameLayout.LayoutParams)NativeViewFactory.a(paramai.a(0), this);
    int i = Build.VERSION.SDK_INT;
    int j = 20;
    int k = 17;
    if (i >= k)
    {
      paramai.setMarginStart(j);
      paramai.setMarginEnd(j);
    }
    else
    {
      paramai.setMargins(j, 0, j, 0);
    }
    gravity = paramInt2;
    c.setLayoutParams(paramai);
    paramai = c;
    paramau = (al)paramau;
    paramai.setAdapter(paramau);
    c.setOffscreenPageLimit(2);
    c.setPageMargin(16);
    c.setCurrentItem(paramInt1);
    a = parama;
  }
  
  public void onPageScrollStateChanged(int paramInt)
  {
    if (paramInt != 0) {
      paramInt = 1;
    } else {
      paramInt = 0;
    }
    f = paramInt;
  }
  
  public void onPageScrolled(int paramInt1, float paramFloat, int paramInt2)
  {
    paramInt1 = f;
    if (paramInt1 != 0) {
      invalidate();
    }
  }
  
  public void onPageSelected(int paramInt)
  {
    FrameLayout.LayoutParams localLayoutParams = (FrameLayout.LayoutParams)c.getLayoutParams();
    NativeScrollableContainer.a locala = a;
    if (locala != null)
    {
      paramInt = locala.a(paramInt);
      gravity = paramInt;
      ViewPager localViewPager = c;
      localViewPager.requestLayout();
    }
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    Point localPoint = d;
    paramInt1 /= 2;
    x = paramInt1;
    paramInt2 /= 2;
    y = paramInt2;
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    int i = paramMotionEvent.getAction();
    switch (i)
    {
    default: 
      break;
    case 1: 
      localObject1 = e;
      i = x;
      f1 = i;
      f2 = paramMotionEvent.getX();
      localObject2 = c;
      k = ((ViewPager)localObject2).getCurrentItem();
      o localo = c.getAdapter();
      int m = localo.getCount();
      ViewPager localViewPager = c;
      int n = localViewPager.getWidth();
      int i1 = getWidth();
      int i2 = 0;
      float f3;
      double d1;
      boolean bool1;
      if (k != 0)
      {
        m += -1;
        if (m != k)
        {
          k = (i1 - n) / 2;
          f3 = k;
          boolean bool2 = f1 < f3;
          if (bool2)
          {
            bool2 = f2 < f3;
            if (bool2)
            {
              f3 -= f2;
              f1 = n;
              f3 /= f1;
              d1 = Math.ceil(f3);
              i = (int)d1;
              i2 = -i;
              break label400;
            }
          }
          i1 = (i1 + n) / 2;
          f3 = i1;
          bool1 = f1 < f3;
          if (!bool1) {
            break label400;
          }
          bool1 = f2 < f3;
          if (!bool1) {
            break label400;
          }
          f2 -= f3;
          f1 = n;
          f2 /= f1;
          d1 = Math.ceil(f2);
          i2 = (int)d1;
          break label400;
        }
      }
      i1 -= n;
      if (k == 0)
      {
        f3 = i1;
        bool1 = f1 < f3;
        if (bool1)
        {
          bool1 = f2 < f3;
          if (bool1)
          {
            f2 -= f3;
            f1 = n;
            f2 /= f1;
            d1 = Math.ceil(f2);
            i2 = (int)d1;
          }
        }
      }
      else
      {
        f3 = i1;
        bool1 = f1 < f3;
        if (bool1)
        {
          bool1 = f2 < f3;
          if (bool1)
          {
            f3 -= f2;
            f1 = n;
            f3 /= f1;
            d1 = Math.ceil(f3);
            j = (int)d1;
            i2 = -j;
          }
        }
      }
      if (i2 != 0)
      {
        j = 3;
        f1 = 4.2E-45F;
        paramMotionEvent.setAction(j);
        localObject1 = c;
        i3 = ((ViewPager)localObject1).getCurrentItem() + i2;
        ((ViewPager)localObject1).setCurrentItem(i3);
      }
      localObject1 = d;
      j = x;
      i3 = e.x;
      j -= i3;
      f1 = j;
      localPoint = d;
      i3 = y;
      localObject2 = e;
      k = y;
      i3 -= k;
      f2 = i3;
      paramMotionEvent.offsetLocation(f1, f2);
      break;
    case 0: 
      label400:
      localObject1 = e;
      i3 = (int)paramMotionEvent.getX();
      x = i3;
      localObject1 = e;
      f2 = paramMotionEvent.getY();
      i3 = (int)f2;
      y = i3;
    }
    Object localObject1 = d;
    int j = x;
    int i3 = e.x;
    j -= i3;
    float f1 = j;
    Point localPoint = d;
    i3 = y;
    Object localObject2 = e;
    int k = y;
    i3 -= k;
    float f2 = i3;
    paramMotionEvent.offsetLocation(f1, f2);
    return c.dispatchTouchEvent(paramMotionEvent);
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.bl
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */