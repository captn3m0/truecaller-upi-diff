package com.inmobi.ads;

import android.content.Context;
import android.view.View;

final class NativeViewFactory$7
  extends NativeViewFactory.c
{
  NativeViewFactory$7(NativeViewFactory paramNativeViewFactory)
  {
    super(paramNativeViewFactory);
  }
  
  protected final View a(Context paramContext)
  {
    bl localbl = new com/inmobi/ads/bl;
    paramContext = paramContext.getApplicationContext();
    localbl.<init>(paramContext);
    return localbl;
  }
  
  protected final void a(View paramView, ag paramag, b paramb)
  {
    super.a(paramView, paramag, paramb);
    paramag = c;
    NativeViewFactory.a(paramView, paramag);
  }
  
  public final boolean a(View paramView)
  {
    a = null;
    return super.a(paramView);
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.NativeViewFactory.7
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */