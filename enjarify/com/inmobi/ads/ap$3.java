package com.inmobi.ads;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

final class ap$3
  implements bd
{
  ap$3(ap paramap) {}
  
  public final void a()
  {
    Object localObject = a;
    boolean bool = ((ap)localObject).b();
    if (!bool)
    {
      localObject = ap.a(a);
      bool = localObject instanceof ba;
      if (bool)
      {
        localObject = (ba)ap.a(a);
        ((ba)localObject).w();
      }
    }
  }
  
  public final void a(NativeVideoView paramNativeVideoView)
  {
    Object localObject = a;
    boolean bool1 = ((ap)localObject).b();
    if (!bool1)
    {
      localObject = ap.a(a);
      bool1 = localObject instanceof ba;
      if (bool1)
      {
        localObject = (ba)ap.a(a);
        boolean bool2 = r;
        paramNativeVideoView.setIsLockScreen(bool2);
        paramNativeVideoView = (NativeVideoWrapper)paramNativeVideoView.getParent();
        WeakReference localWeakReference = new java/lang/ref/WeakReference;
        localWeakReference.<init>(paramNativeVideoView);
        y = localWeakReference;
        paramNativeVideoView = paramNativeVideoView.getVideoView().getMediaController();
        if (paramNativeVideoView != null) {
          paramNativeVideoView.setVideoAd((ba)localObject);
        }
      }
    }
  }
  
  public final void a(bb parambb)
  {
    Object localObject1 = a;
    boolean bool1 = ((ap)localObject1).b();
    if (!bool1)
    {
      localObject1 = ap.a(a);
      bool1 = localObject1 instanceof ba;
      if (bool1)
      {
        localObject1 = (ba)ap.a(a);
        boolean bool2 = i;
        if (!bool2)
        {
          Object localObject2 = AdContainer.RenderingProperties.PlacementType.PLACEMENT_TYPE_INLINE;
          Object localObject3 = b.a;
          if (localObject2 == localObject3)
          {
            localObject2 = v;
            localObject3 = "currentMediaVolume";
            localObject2 = (Integer)((Map)localObject2).get(localObject3);
            int i = ((Integer)localObject2).intValue();
            if (i > 0)
            {
              localObject2 = v;
              localObject3 = "lastMediaVolume";
              localObject2 = (Integer)((Map)localObject2).get(localObject3);
              i = ((Integer)localObject2).intValue();
              if (i == 0) {
                ((ba)localObject1).d(parambb);
              }
            }
            localObject2 = v;
            localObject3 = "currentMediaVolume";
            localObject2 = (Integer)((Map)localObject2).get(localObject3);
            i = ((Integer)localObject2).intValue();
            if (i == 0)
            {
              localObject2 = v;
              localObject3 = "lastMediaVolume";
              localObject2 = (Integer)((Map)localObject2).get(localObject3);
              i = ((Integer)localObject2).intValue();
              if (i > 0) {
                ((ba)localObject1).c(parambb);
              }
            }
          }
          localObject2 = v;
          localObject3 = "didStartPlaying";
          localObject2 = (Boolean)((Map)localObject2).get(localObject3);
          boolean bool3 = ((Boolean)localObject2).booleanValue();
          if (!bool3)
          {
            parambb = v;
            localObject2 = "didStartPlaying";
            localObject3 = Boolean.TRUE;
            parambb.put(localObject2, localObject3);
            parambb = ((ba)localObject1).getViewableAd();
            int j = 6;
            parambb.a(j);
            try
            {
              parambb = new java/util/HashMap;
              parambb.<init>();
              localObject2 = "isCached";
              localObject3 = "1";
              parambb.put(localObject2, localObject3);
              localObject2 = "VideoPlayed";
              ((ba)localObject1).a((String)localObject2, parambb);
              return;
            }
            catch (Exception parambb)
            {
              parambb.getMessage();
            }
          }
        }
      }
    }
  }
  
  public final void a(bb parambb, int paramInt)
  {
    Object localObject = a;
    boolean bool = ((ap)localObject).b();
    if (!bool)
    {
      localObject = ap.a(a);
      bool = localObject instanceof ba;
      if (bool)
      {
        localObject = (ba)ap.a(a);
        ((ba)localObject).a(parambb, paramInt);
      }
    }
  }
  
  public final void b(bb parambb)
  {
    Object localObject = a;
    boolean bool = ((ap)localObject).b();
    if (!bool)
    {
      localObject = ap.a(a);
      bool = localObject instanceof ba;
      if (bool)
      {
        localObject = (ba)ap.a(a);
        ((ba)localObject).a(parambb);
      }
    }
  }
  
  public final void b(bb parambb, int paramInt)
  {
    Object localObject = a;
    boolean bool = ((ap)localObject).b();
    if (!bool)
    {
      localObject = ap.a(a);
      bool = localObject instanceof ba;
      if (bool)
      {
        localObject = (ba)ap.a(a);
        ((ba)localObject).b(parambb, paramInt);
      }
    }
  }
  
  public final void c(bb parambb)
  {
    Object localObject = a;
    boolean bool = ((ap)localObject).b();
    if (!bool)
    {
      localObject = ap.a(a);
      bool = localObject instanceof ba;
      if (bool)
      {
        localObject = (ba)ap.a(a);
        ((ba)localObject).b(parambb);
      }
    }
  }
  
  public final void d(bb parambb)
  {
    Object localObject1 = a;
    boolean bool1 = ((ap)localObject1).b();
    if (!bool1)
    {
      localObject1 = ap.a(a);
      bool1 = localObject1 instanceof ba;
      if (bool1)
      {
        localObject1 = (ba)ap.a(a);
        Object localObject2 = v;
        Object localObject3 = "didSignalVideoCompleted";
        localObject2 = (Boolean)((Map)localObject2).get(localObject3);
        boolean bool2 = ((Boolean)localObject2).booleanValue();
        if (!bool2)
        {
          ((ba)localObject1).o();
          localObject2 = ((ba)localObject1).e();
          if (localObject2 != null) {
            ((ad.c)localObject2).h();
          }
        }
        localObject2 = AdContainer.RenderingProperties.PlacementType.PLACEMENT_TYPE_FULLSCREEN;
        localObject3 = b.a;
        if (localObject2 == localObject3) {
          ((ba)localObject1).c(parambb);
        }
      }
    }
  }
  
  public final void e(bb parambb)
  {
    Object localObject = a;
    boolean bool = ((ap)localObject).b();
    if (!bool)
    {
      localObject = ap.a(a);
      bool = localObject instanceof ba;
      if (bool)
      {
        localObject = (ba)ap.a(a);
        ((ba)localObject).e(parambb);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.ap.3
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */