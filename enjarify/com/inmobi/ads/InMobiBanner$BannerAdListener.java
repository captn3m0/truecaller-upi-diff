package com.inmobi.ads;

import java.util.Map;

public abstract interface InMobiBanner$BannerAdListener
{
  public abstract void onAdDismissed(InMobiBanner paramInMobiBanner);
  
  public abstract void onAdDisplayed(InMobiBanner paramInMobiBanner);
  
  public abstract void onAdInteraction(InMobiBanner paramInMobiBanner, Map paramMap);
  
  public abstract void onAdLoadFailed(InMobiBanner paramInMobiBanner, InMobiAdRequestStatus paramInMobiAdRequestStatus);
  
  public abstract void onAdLoadSucceeded(InMobiBanner paramInMobiBanner);
  
  public abstract void onAdRewardActionCompleted(InMobiBanner paramInMobiBanner, Map paramMap);
  
  public abstract void onUserLeftApplication(InMobiBanner paramInMobiBanner);
}

/* Location:
 * Qualified Name:     com.inmobi.ads.InMobiBanner.BannerAdListener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */