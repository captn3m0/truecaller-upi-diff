package com.inmobi.ads;

import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import java.util.Map;

final class ba$3
  implements Runnable
{
  ba$3(ba paramba, bb parambb, boolean paramBoolean, NativeVideoView paramNativeVideoView) {}
  
  public final void run()
  {
    Object localObject1 = a.v;
    Object localObject2 = "visible";
    Object localObject3 = Boolean.valueOf(b);
    ((Map)localObject1).put(localObject2, localObject3);
    boolean bool1 = b;
    int j = 1;
    int k = 4;
    Object localObject5;
    if (bool1)
    {
      localObject1 = d;
      bool1 = j;
      if (!bool1)
      {
        localObject1 = a.v;
        localObject4 = "lastVisibleTimestamp";
        long l1 = SystemClock.uptimeMillis();
        localObject5 = Long.valueOf(l1);
        ((Map)localObject1).put(localObject4, localObject5);
        localObject1 = c;
        bool1 = i;
        if (bool1)
        {
          localObject1 = c.getMediaPlayer();
          if (localObject1 != null)
          {
            localObject1 = a;
            bool1 = ((bb)localObject1).a();
            if (bool1)
            {
              localObject1 = c;
              ((NativeVideoView)localObject1).e();
            }
            else
            {
              localObject1 = c;
              ((NativeVideoView)localObject1).d();
            }
          }
        }
        localObject1 = c;
        localObject4 = h;
        bool3 = false;
        localObject5 = null;
        if (localObject4 != null)
        {
          localObject4 = h;
          ((Handler)localObject4).removeMessages(0);
        }
        i = false;
        localObject1 = c;
        int i = ((NativeVideoView)localObject1).getState();
        if (j == i)
        {
          c.getMediaPlayer().b = 3;
          return;
        }
        i = 2;
        localObject2 = c;
        j = ((NativeVideoView)localObject2).getState();
        if (i != j)
        {
          localObject1 = c;
          i = ((NativeVideoView)localObject1).getState();
          if (k != i)
          {
            i = 5;
            localObject2 = c;
            j = ((NativeVideoView)localObject2).getState();
            if (i != j) {
              break label439;
            }
            localObject1 = a;
            boolean bool2 = C;
            if (!bool2) {
              break label439;
            }
          }
        }
        c.start();
        return;
      }
    }
    localObject1 = c;
    Object localObject4 = a;
    int n = F;
    boolean bool3 = i;
    if (!bool3)
    {
      int m = ((NativeVideoView)localObject1).getState();
      if (k != m)
      {
        localObject3 = h;
        if (localObject3 == null)
        {
          localObject3 = new android/os/Handler;
          localObject5 = Looper.getMainLooper();
          ((Handler)localObject3).<init>((Looper)localObject5);
          h = ((Handler)localObject3);
        }
        if (n > 0)
        {
          i = j;
          ((NativeVideoView)localObject1).d();
          localObject2 = h;
          localObject3 = new com/inmobi/ads/NativeVideoView$8;
          ((NativeVideoView.8)localObject3).<init>((NativeVideoView)localObject1);
          long l2 = n * 1000;
          ((Handler)localObject2).postDelayed((Runnable)localObject3, l2);
          return;
        }
        ((NativeVideoView)localObject1).pause();
        label439:
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.ba.3
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */