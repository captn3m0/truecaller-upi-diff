package com.inmobi.ads;

import android.content.Context;
import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

final class InMobiNative$1
  implements j.d
{
  InMobiNative$1(bf parambf) {}
  
  public final void a(j paramj)
  {
    boolean bool = paramj instanceof af;
    if (!bool) {
      return;
    }
    try
    {
      Object localObject1 = g.a;
      Object localObject2 = a;
      ((ConcurrentHashMap)localObject1).remove(localObject2);
      localObject1 = InMobiNative.access$000();
      localObject1 = ((ConcurrentHashMap)localObject1).get(paramj);
      localObject1 = (WeakReference)localObject1;
      if (localObject1 != null)
      {
        localObject2 = InMobiNative.access$000();
        ((ConcurrentHashMap)localObject2).remove(paramj);
        localObject1 = ((WeakReference)localObject1).get();
        localObject1 = (InMobiNative.NativeAdRequestListener)localObject1;
        if (localObject1 != null)
        {
          long l = b;
          Object localObject3 = d;
          String str1 = "native";
          String str2 = c;
          localObject2 = bf.a(l, (Map)localObject3, str1, str2);
          Object localObject4 = ((j)paramj).k();
          f = ((InMobiAdRequest.MonetizationContext)localObject4);
          localObject4 = new com/inmobi/ads/InMobiNative;
          localObject3 = ((j)paramj).a();
          str1 = null;
          ((InMobiNative)localObject4).<init>((Context)localObject3, (bf)localObject2, null);
          localObject2 = c;
          ((InMobiNative)localObject4).setKeywords((String)localObject2);
          paramj = d;
          ((InMobiNative)localObject4).setExtras(paramj);
          paramj = new com/inmobi/ads/InMobiAdRequestStatus;
          localObject2 = InMobiAdRequestStatus.StatusCode.NO_ERROR;
          paramj.<init>((InMobiAdRequestStatus.StatusCode)localObject2);
          ((InMobiNative.NativeAdRequestListener)localObject1).onAdRequestCompleted(paramj, (InMobiNative)localObject4);
        }
      }
      return;
    }
    catch (Exception paramj)
    {
      InMobiNative.access$200();
      paramj.getMessage();
    }
  }
  
  public final void a(j paramj, InMobiAdRequestStatus paramInMobiAdRequestStatus)
  {
    boolean bool = paramj instanceof af;
    if (!bool) {
      return;
    }
    try
    {
      Object localObject1 = g.a;
      Object localObject2 = a;
      ((ConcurrentHashMap)localObject1).remove(localObject2);
      localObject1 = InMobiNative.access$000();
      localObject1 = ((ConcurrentHashMap)localObject1).get(paramj);
      localObject1 = (WeakReference)localObject1;
      if (localObject1 != null)
      {
        localObject2 = InMobiNative.access$000();
        ((ConcurrentHashMap)localObject2).remove(paramj);
        paramj = ((WeakReference)localObject1).get();
        paramj = (InMobiNative.NativeAdRequestListener)paramj;
        if (paramj != null)
        {
          bool = false;
          localObject1 = null;
          paramj.onAdRequestCompleted(paramInMobiAdRequestStatus, null);
        }
      }
      return;
    }
    catch (Exception paramj)
    {
      InMobiNative.access$200();
      paramj.getMessage();
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.InMobiNative.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */