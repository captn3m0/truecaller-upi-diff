package com.inmobi.ads;

import android.content.Context;
import com.inmobi.rendering.RenderView;
import java.lang.ref.WeakReference;
import org.json.JSONArray;
import org.json.JSONObject;

final class ad$a
  extends Thread
{
  private WeakReference b;
  
  ad$a(ad paramad1, ad paramad2)
  {
    paramad1 = new java/lang/ref/WeakReference;
    paramad1.<init>(paramad2);
    b = paramad1;
  }
  
  public final void run()
  {
    Object localObject1 = a.l();
    if (localObject1 == null)
    {
      ad.v();
      return;
    }
    localObject1 = (ad)b.get();
    if (localObject1 != null)
    {
      boolean bool1 = i;
      if (!bool1) {
        try
        {
          ak localak1 = ((ad)localObject1).h();
          localObject2 = a;
          localObject2 = ((ad)localObject2).l();
          if (localObject2 != null)
          {
            localObject2 = e;
            int i = ((JSONArray)localObject2).length();
            if (i != 0)
            {
              ad.v();
              JSONObject localJSONObject = localak1.a();
              if (localJSONObject == null) {
                return;
              }
              localObject2 = a;
              localObject2 = b;
              localObject2 = a;
              localObject3 = AdContainer.RenderingProperties.PlacementType.PLACEMENT_TYPE_INLINE;
              boolean bool3;
              if (localObject2 == localObject3)
              {
                i = 1;
                bool3 = true;
              }
              else
              {
                i = 0;
                localObject2 = null;
                bool3 = false;
              }
              ak localak2 = new com/inmobi/ads/ak;
              localObject2 = a;
              localObject2 = b;
              AdContainer.RenderingProperties.PlacementType localPlacementType = a;
              localObject2 = a;
              Object localObject4 = c;
              AdContainer.RenderingProperties localRenderingProperties = null;
              localObject3 = localak2;
              localak2.<init>(localPlacementType, localJSONObject, localak1, bool3, (b)localObject4, null);
              boolean bool2 = localak2.c();
              if (bool2)
              {
                localObject2 = a;
                localObject4 = ((ad)localObject2).l();
                localRenderingProperties = new com/inmobi/ads/AdContainer$RenderingProperties;
                localObject2 = AdContainer.RenderingProperties.PlacementType.PLACEMENT_TYPE_INLINE;
                localRenderingProperties.<init>((AdContainer.RenderingProperties.PlacementType)localObject2);
                localObject2 = a;
                String str1 = d;
                localObject2 = a;
                String str2 = e;
                localObject2 = a;
                b localb = c;
                localObject2 = ad.b.a((Context)localObject4, localRenderingProperties, localak2, str1, str2, null, localb);
                ad.v();
                ((ad)localObject2).a((AdContainer)localObject1);
                localObject3 = t;
                t = ((RenderView)localObject3);
                ad.a((ad)localObject1, (ad)localObject2);
                return;
              }
              ad.v();
              return;
            }
          }
          ad.v();
          return;
        }
        catch (Exception localException)
        {
          ad.v();
          localException.getMessage();
          Object localObject2 = com.inmobi.commons.core.a.a.a();
          Object localObject3 = new com/inmobi/commons/core/e/a;
          ((com.inmobi.commons.core.e.a)localObject3).<init>(localException);
          ((com.inmobi.commons.core.a.a)localObject2).a((com.inmobi.commons.core.e.a)localObject3);
          return;
        }
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.ad.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */