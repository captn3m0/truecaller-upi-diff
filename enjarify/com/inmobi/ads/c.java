package com.inmobi.ads;

import android.content.ContentValues;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class c
{
  private static final String a = "c";
  private static c b;
  private static Object c;
  private static final String[] d = tmp100_89;
  
  static
  {
    Object localObject = new java/lang/Object;
    localObject.<init>();
    c = localObject;
    String[] tmp17_14 = new String[19];
    String[] tmp18_17 = tmp17_14;
    String[] tmp18_17 = tmp17_14;
    tmp18_17[0] = "id";
    tmp18_17[1] = "ad_content";
    String[] tmp27_18 = tmp18_17;
    String[] tmp27_18 = tmp18_17;
    tmp27_18[2] = "video_url";
    tmp27_18[3] = "video_track_duration";
    String[] tmp36_27 = tmp27_18;
    String[] tmp36_27 = tmp27_18;
    tmp36_27[4] = "click_url";
    tmp36_27[5] = "video_trackers";
    String[] tmp45_36 = tmp36_27;
    String[] tmp45_36 = tmp36_27;
    tmp45_36[6] = "companion_ads";
    tmp45_36[7] = "web_vast";
    String[] tmp56_45 = tmp45_36;
    String[] tmp56_45 = tmp45_36;
    tmp56_45[8] = "preload_webView";
    tmp56_45[9] = "asset_urls";
    String[] tmp67_56 = tmp56_45;
    String[] tmp67_56 = tmp56_45;
    tmp67_56[10] = "ad_type";
    tmp67_56[11] = "ad_size";
    String[] tmp78_67 = tmp67_56;
    String[] tmp78_67 = tmp67_56;
    tmp78_67[12] = "placement_id";
    tmp78_67[13] = "tp_key";
    String[] tmp89_78 = tmp78_67;
    String[] tmp89_78 = tmp78_67;
    tmp89_78[14] = "insertion_ts";
    tmp89_78[15] = "expiry_duration";
    String[] tmp100_89 = tmp89_78;
    String[] tmp100_89 = tmp89_78;
    tmp100_89[16] = "imp_id";
    tmp100_89[17] = "m10_context";
    tmp100_89[18] = "client_request_id";
  }
  
  private c()
  {
    com.inmobi.commons.core.d.b localb = com.inmobi.commons.core.d.b.a();
    localb.a("ad", "(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, placement_id INTEGER NOT NULL, ad_content TEXT NOT NULL, ad_type TEXT NOT NULL, ad_size TEXT, asset_urls TEXT, video_url TEXT, video_track_duration TEXT, click_url TEXT, video_trackers TEXT, companion_ads TEXT, web_vast TEXT, preload_webView INTEGER DEFAULT 0, insertion_ts INTEGER NOT NULL, imp_id TEXT NOT NULL, m10_context TEXT NOT NULL, tp_key TEXT, expiry_duration INTEGER NOT NULL, client_request_id TEXT NOT NULL)");
    localb.b();
  }
  
  static int a(long paramLong, String paramString1, InMobiAdRequest.MonetizationContext paramMonetizationContext, String paramString2)
  {
    com.inmobi.commons.core.d.b localb = com.inmobi.commons.core.d.b.a();
    int i = 2;
    int j = 1;
    int k = 3;
    if (paramString1 != null)
    {
      str1 = paramString1.trim();
      int m = str1.length();
      if (m != 0)
      {
        str1 = "ad";
        String str2 = "placement_id=? AND ad_size=? AND m10_context=? AND tp_key=?";
        int n = 4;
        String[] arrayOfString1 = new String[n];
        str3 = String.valueOf(paramLong);
        arrayOfString1[0] = str3;
        arrayOfString1[j] = paramString1;
        str3 = paramMonetizationContext.getValue();
        arrayOfString1[i] = str3;
        arrayOfString1[k] = paramString2;
        i1 = localb.b(str1, str2, arrayOfString1);
        break label166;
      }
    }
    paramString1 = "ad";
    String str1 = "placement_id=? AND m10_context=? AND tp_key=?";
    String[] arrayOfString2 = new String[k];
    String str3 = String.valueOf(paramLong);
    arrayOfString2[0] = str3;
    str3 = paramMonetizationContext.getValue();
    arrayOfString2[j] = str3;
    arrayOfString2[i] = paramString2;
    int i1 = localb.b(paramString1, str1, arrayOfString2);
    label166:
    localb.b();
    return i1;
  }
  
  public static int a(a parama)
  {
    return a(e);
  }
  
  public static int a(String paramString)
  {
    com.inmobi.commons.core.d.b localb = com.inmobi.commons.core.d.b.a();
    String[] arrayOfString = new String[1];
    paramString = String.valueOf(paramString);
    arrayOfString[0] = paramString;
    int i = localb.a("ad", "imp_id = ?", arrayOfString);
    localb.b();
    return i;
  }
  
  static int a(String paramString, long paramLong)
  {
    com.inmobi.commons.core.d.b localb = com.inmobi.commons.core.d.b.a();
    String str = "ad";
    String[] arrayOfString1 = d;
    Object localObject1 = "ad_type=?";
    String[] arrayOfString2 = new String[1];
    int i = 0;
    arrayOfString2[0] = paramString;
    boolean bool1 = false;
    Object localObject2 = localb;
    paramString = localb.a(str, arrayOfString1, (String)localObject1, arrayOfString2, null, null, null, null);
    int j = paramString.size();
    if (j == 0)
    {
      localb.b();
      return 0;
    }
    paramString = paramString.iterator();
    for (;;)
    {
      boolean bool2 = paramString.hasNext();
      if (!bool2) {
        break;
      }
      localObject2 = a.a.a((ContentValues)paramString.next());
      long l1 = ((a)localObject2).b();
      long l2 = -1;
      bool1 = l1 < l2;
      if (!bool1)
      {
        l1 = d;
        localObject1 = TimeUnit.SECONDS;
        l2 = ((TimeUnit)localObject1).toMillis(paramLong);
        l1 += l2;
        l2 = System.currentTimeMillis();
        l1 -= l2;
      }
      else
      {
        l1 = ((a)localObject2).b();
        l2 = System.currentTimeMillis();
        l1 -= l2;
      }
      l2 = 0L;
      bool1 = l1 < l2;
      if (bool1)
      {
        localObject2 = e;
        int k = a((String)localObject2);
        i += k;
      }
    }
    localb.b();
    return i;
  }
  
  public static c a()
  {
    c localc1 = b;
    if (localc1 == null) {
      synchronized (c)
      {
        localc1 = b;
        if (localc1 == null)
        {
          localc1 = new com/inmobi/ads/c;
          localc1.<init>();
          b = localc1;
        }
      }
    }
    return localc2;
  }
  
  public static a b(String paramString)
  {
    com.inmobi.commons.core.d.b localb = com.inmobi.commons.core.d.b.a();
    String str1 = "ad";
    String[] arrayOfString1 = d;
    String str2 = "imp_id=?";
    int i = 1;
    String[] arrayOfString2 = new String[i];
    arrayOfString2[0] = paramString;
    String str3 = "1";
    paramString = localb.a(str1, arrayOfString1, str2, arrayOfString2, null, null, null, str3);
    int j = paramString.size();
    if (j == 0) {
      return null;
    }
    return a.a.a((ContentValues)paramString.get(0));
  }
  
  public static void b()
  {
    com.inmobi.commons.core.d.b localb = com.inmobi.commons.core.d.b.a();
    localb.a("ad", null, null);
    localb.b();
  }
  
  private a d(long paramLong, String paramString1, InMobiAdRequest.MonetizationContext paramMonetizationContext, String paramString2)
  {
    try
    {
      com.inmobi.commons.core.d.b localb = com.inmobi.commons.core.d.b.a();
      int i = 2;
      int j = 1;
      int k = 3;
      if (paramString1 != null)
      {
        localObject1 = paramString1.trim();
        int m = ((String)localObject1).length();
        if (m != 0)
        {
          localObject1 = "ad";
          localObject2 = d;
          localObject3 = "placement_id=? AND ad_size=? AND m10_context=? AND tp_key=?";
          int n = 4;
          localObject4 = new String[n];
          localObject5 = String.valueOf(paramLong);
          localObject4[0] = localObject5;
          localObject4[j] = paramString1;
          localObject5 = paramMonetizationContext.getValue();
          localObject4[i] = localObject5;
          localObject4[k] = paramString2;
          localObject5 = null;
          i1 = 0;
          paramString1 = "insertion_ts";
          str = "1";
          localObject7 = localObject1;
          localObject8 = localObject2;
          localObject9 = localObject3;
          localObject1 = localObject4;
          localObject2 = null;
          localObject3 = null;
          localObject4 = paramString1;
          localObject5 = localb.a((String)localObject7, (String[])localObject8, (String)localObject9, (String[])localObject1, null, null, paramString1, str);
          break label268;
        }
      }
      paramString1 = "ad";
      Object localObject1 = d;
      Object localObject2 = "placement_id=? AND m10_context=? AND tp_key=?";
      Object localObject3 = new String[k];
      Object localObject5 = String.valueOf(paramLong);
      localObject3[0] = localObject5;
      localObject5 = paramMonetizationContext.getValue();
      localObject3[j] = localObject5;
      localObject3[i] = paramString2;
      localObject5 = null;
      int i1 = 0;
      Object localObject4 = "insertion_ts";
      String str = "1";
      Object localObject7 = paramString1;
      Object localObject8 = localObject1;
      Object localObject9 = localObject2;
      localObject1 = localObject3;
      localObject2 = null;
      localObject3 = null;
      localObject5 = localb.a(paramString1, (String[])localObject8, (String)localObject9, (String[])localObject1, null, null, (String)localObject4, str);
      label268:
      i1 = ((List)localObject5).size();
      if (i1 == 0) {
        return null;
      }
      localObject5 = ((List)localObject5).get(0);
      localObject5 = (ContentValues)localObject5;
      localObject5 = a.a.a((ContentValues)localObject5);
      return (a)localObject5;
    }
    finally {}
  }
  
  final List a(String paramString1, String paramString2)
  {
    try
    {
      ArrayList localArrayList = new java/util/ArrayList;
      localArrayList.<init>();
      com.inmobi.commons.core.d.b localb = com.inmobi.commons.core.d.b.a();
      Object localObject1 = null;
      int i = 1;
      boolean bool;
      if (paramString2 != null)
      {
        localObject2 = paramString2.trim();
        int j = ((String)localObject2).length();
        if (j != 0)
        {
          localObject2 = "ad";
          localObject3 = d;
          localObject4 = "video_url=? AND ad_size=?";
          k = 2;
          arrayOfString = new String[k];
          arrayOfString[0] = paramString1;
          arrayOfString[i] = paramString2;
          paramString1 = null;
          bool = false;
          paramString2 = null;
          str = "insertion_ts";
          localObject1 = localObject2;
          localObject5 = localObject3;
          localObject2 = localObject4;
          localObject3 = arrayOfString;
          localObject4 = null;
          k = 0;
          arrayOfString = null;
          paramString1 = localb.a((String)localObject1, (String[])localObject5, (String)localObject2, (String[])localObject3, null, null, str, null);
          break label207;
        }
      }
      paramString2 = "ad";
      Object localObject2 = d;
      Object localObject3 = "video_url=?";
      Object localObject4 = new String[i];
      localObject4[0] = paramString1;
      paramString1 = null;
      int k = 0;
      String[] arrayOfString = null;
      String str = "insertion_ts";
      localObject1 = paramString2;
      Object localObject5 = localObject2;
      localObject2 = localObject3;
      localObject3 = localObject4;
      localObject4 = null;
      paramString1 = localb.a(paramString2, (String[])localObject5, (String)localObject2, (String[])localObject3, null, null, str, null);
      label207:
      paramString1 = paramString1.iterator();
      for (;;)
      {
        bool = paramString1.hasNext();
        if (!bool) {
          break;
        }
        paramString2 = paramString1.next();
        paramString2 = (ContentValues)paramString2;
        paramString2 = a.a.a(paramString2);
        localArrayList.add(paramString2);
      }
      return localArrayList;
    }
    finally {}
  }
  
  public final void a(List paramList, long paramLong, int paramInt, String paramString1, InMobiAdRequest.MonetizationContext paramMonetizationContext, String paramString2)
  {
    Object localObject1 = paramString2;
    if (paramInt != 0) {
      try
      {
        int i = paramList.size();
        if (i != 0)
        {
          com.inmobi.commons.core.d.b localb = com.inmobi.commons.core.d.b.a();
          Object localObject3 = paramList.iterator();
          boolean bool;
          Object localObject4;
          for (;;)
          {
            bool = ((Iterator)localObject3).hasNext();
            if (!bool) {
              break;
            }
            localObject4 = ((Iterator)localObject3).next();
            localObject4 = (a)localObject4;
            long l = System.currentTimeMillis();
            d = l;
            localObject4 = ((a)localObject4).a();
            localObject5 = "tp_key";
            ((ContentValues)localObject4).put((String)localObject5, (String)localObject1);
            localObject5 = "ad";
            localb.a((String)localObject5, (ContentValues)localObject4);
          }
          Object localObject5 = paramMonetizationContext;
          int k = a(paramLong, null, paramMonetizationContext, (String)localObject1) - paramInt;
          if (k > 0)
          {
            Object localObject6 = new java/util/HashMap;
            ((HashMap)localObject6).<init>();
            Object localObject7 = "type";
            Object localObject8 = paramString1;
            ((Map)localObject6).put(localObject7, paramString1);
            localObject7 = "count";
            localObject8 = Integer.valueOf(k);
            ((Map)localObject6).put(localObject7, localObject8);
            com.inmobi.commons.core.e.b.a();
            localObject7 = "ads";
            localObject8 = "DbSpaceOverflow";
            com.inmobi.commons.core.e.b.a((String)localObject7, (String)localObject8, (Map)localObject6);
            localObject6 = "ad";
            localObject7 = "id";
            localObject7 = new String[] { localObject7 };
            localObject8 = "placement_id=? AND m10_context=? AND tp_key=?";
            int m = 3;
            Object localObject9 = new String[m];
            localObject3 = String.valueOf(paramLong);
            int n = 0;
            localObject9[0] = localObject3;
            localObject3 = paramMonetizationContext.getValue();
            bool = true;
            localObject9[bool] = localObject3;
            int i1 = 2;
            localObject9[i1] = localObject1;
            localObject1 = null;
            String str1 = "insertion_ts ASC";
            String str2 = String.valueOf(k);
            localObject3 = localb;
            localObject4 = localObject6;
            localObject5 = localObject7;
            Object localObject10 = localObject8;
            localObject6 = localObject9;
            localObject7 = null;
            localObject8 = null;
            localObject9 = str1;
            localObject1 = localb.a((String)localObject4, (String[])localObject5, (String)localObject10, (String[])localObject6, null, null, str1, str2);
            i1 = ((List)localObject1).size();
            localObject3 = new String[i1];
            for (;;)
            {
              int j = ((List)localObject1).size();
              if (n >= j) {
                break;
              }
              localObject4 = ((List)localObject1).get(n);
              localObject4 = (ContentValues)localObject4;
              localObject5 = "id";
              localObject4 = ((ContentValues)localObject4).getAsInteger((String)localObject5);
              localObject4 = String.valueOf(localObject4);
              localObject3[n] = localObject4;
              n += 1;
            }
            localObject1 = Arrays.toString((Object[])localObject3);
            localObject3 = "[";
            localObject4 = "(";
            localObject1 = ((String)localObject1).replace((CharSequence)localObject3, (CharSequence)localObject4);
            localObject3 = "]";
            localObject4 = ")";
            localObject1 = ((String)localObject1).replace((CharSequence)localObject3, (CharSequence)localObject4);
            localObject3 = "ad";
            localObject4 = "id IN ";
            localObject1 = String.valueOf(localObject1);
            localObject1 = ((String)localObject4).concat((String)localObject1);
            localb.a((String)localObject3, (String)localObject1, null);
          }
          localb.b();
          return;
        }
      }
      finally {}
    }
  }
  
  final a b(long paramLong, String paramString1, InMobiAdRequest.MonetizationContext paramMonetizationContext, String paramString2)
  {
    try
    {
      a locala = d(paramLong, paramString1, paramMonetizationContext, paramString2);
      if (locala != null)
      {
        com.inmobi.commons.core.d.b localb = com.inmobi.commons.core.d.b.a();
        paramString1 = "ad";
        paramMonetizationContext = "id=?";
        int i = 1;
        paramString2 = new String[i];
        int j = a;
        String str = String.valueOf(j);
        paramString2[0] = str;
        localb.a(paramString1, paramMonetizationContext, paramString2);
      }
      return locala;
    }
    finally {}
  }
  
  final List b(String paramString1, String paramString2)
  {
    try
    {
      ArrayList localArrayList = new java/util/ArrayList;
      localArrayList.<init>();
      com.inmobi.commons.core.d.b localb = com.inmobi.commons.core.d.b.a();
      int i = 1;
      int j;
      if (paramString2 != null)
      {
        localObject1 = paramString2.trim();
        j = ((String)localObject1).length();
        if (j != 0)
        {
          str1 = "ad";
          localObject2 = d;
          localObject3 = "video_url=? AND ad_size=?";
          j = 2;
          arrayOfString = new String[j];
          arrayOfString[0] = paramString1;
          arrayOfString[i] = paramString2;
          str2 = "insertion_ts";
          localObject1 = localb;
          paramString1 = localb.a(str1, (String[])localObject2, (String)localObject3, arrayOfString, null, null, str2, null);
          break label156;
        }
      }
      String str1 = "ad";
      Object localObject2 = d;
      Object localObject3 = "video_url=?";
      String[] arrayOfString = new String[i];
      arrayOfString[0] = paramString1;
      String str2 = "insertion_ts";
      Object localObject1 = localb;
      paramString1 = localb.a(str1, (String[])localObject2, (String)localObject3, arrayOfString, null, null, str2, null);
      label156:
      paramString1 = paramString1.iterator();
      for (;;)
      {
        boolean bool = paramString1.hasNext();
        if (!bool) {
          break;
        }
        paramString2 = paramString1.next();
        paramString2 = (ContentValues)paramString2;
        localObject1 = "id";
        localObject1 = paramString2.getAsInteger((String)localObject1);
        j = ((Integer)localObject1).intValue();
        str1 = "ad";
        localObject2 = "id=?";
        localObject3 = new String[i];
        localObject1 = String.valueOf(j);
        localObject3[0] = localObject1;
        localb.a(str1, (String)localObject2, (String[])localObject3);
        paramString2 = a.a.a(paramString2);
        localArrayList.add(paramString2);
      }
      return localArrayList;
    }
    finally {}
  }
  
  public final List c(long paramLong, String paramString1, InMobiAdRequest.MonetizationContext paramMonetizationContext, String paramString2)
  {
    try
    {
      ArrayList localArrayList = new java/util/ArrayList;
      localArrayList.<init>();
      com.inmobi.commons.core.d.b localb = com.inmobi.commons.core.d.b.a();
      int i = 2;
      int j = 1;
      Object localObject1 = null;
      int k = 3;
      if (paramString1 != null)
      {
        localObject2 = paramString1.trim();
        m = ((String)localObject2).length();
        if (m != 0)
        {
          localObject2 = "ad";
          localObject3 = d;
          str = "placement_id=? AND ad_size=? AND m10_context=? AND tp_key=?";
          n = 4;
          arrayOfString1 = new String[n];
          localObject4 = String.valueOf(paramLong);
          arrayOfString1[0] = localObject4;
          arrayOfString1[j] = paramString1;
          localObject4 = paramMonetizationContext.getValue();
          arrayOfString1[i] = localObject4;
          arrayOfString1[k] = paramString2;
          localObject4 = null;
          bool = false;
          localObject6 = null;
          paramString1 = "insertion_ts";
          paramMonetizationContext = null;
          localObject7 = localObject2;
          localObject8 = localObject3;
          localObject1 = str;
          arrayOfString2 = arrayOfString1;
          m = 0;
          localObject2 = null;
          localObject3 = null;
          str = paramString1;
          n = 0;
          arrayOfString1 = null;
          localObject4 = localb.a((String)localObject7, (String[])localObject8, (String)localObject1, arrayOfString2, null, null, paramString1, null);
          break label294;
        }
      }
      paramString1 = "ad";
      Object localObject2 = d;
      Object localObject3 = "placement_id=? AND m10_context=? AND tp_key=?";
      String[] arrayOfString2 = new String[k];
      Object localObject4 = String.valueOf(paramLong);
      arrayOfString2[0] = localObject4;
      localObject4 = paramMonetizationContext.getValue();
      arrayOfString2[j] = localObject4;
      arrayOfString2[i] = paramString2;
      localObject4 = null;
      boolean bool = false;
      Object localObject6 = null;
      String str = "insertion_ts";
      int n = 0;
      String[] arrayOfString1 = null;
      Object localObject7 = paramString1;
      Object localObject8 = localObject2;
      localObject1 = localObject3;
      int m = 0;
      localObject2 = null;
      localObject3 = null;
      localObject4 = localb.a(paramString1, (String[])localObject8, (String)localObject1, arrayOfString2, null, null, str, null);
      label294:
      localObject4 = ((List)localObject4).iterator();
      for (;;)
      {
        bool = ((Iterator)localObject4).hasNext();
        if (!bool) {
          break;
        }
        localObject6 = ((Iterator)localObject4).next();
        localObject6 = (ContentValues)localObject6;
        localObject6 = a.a.a((ContentValues)localObject6);
        localArrayList.add(localObject6);
      }
      return localArrayList;
    }
    finally {}
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */