package com.inmobi.ads;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;

public class NativeContainerLayout
  extends ViewGroup
{
  private static final String a = "NativeContainerLayout";
  
  public NativeContainerLayout(Context paramContext)
  {
    super(paramContext);
  }
  
  protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    return paramLayoutParams instanceof NativeContainerLayout.a;
  }
  
  protected ViewGroup.LayoutParams generateDefaultLayoutParams()
  {
    NativeContainerLayout.a locala = new com/inmobi/ads/NativeContainerLayout$a;
    int i = -2;
    locala.<init>(i, i);
    return locala;
  }
  
  protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    NativeContainerLayout.a locala = new com/inmobi/ads/NativeContainerLayout$a;
    locala.<init>(paramLayoutParams);
    return locala;
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    paramBoolean = getChildCount();
    paramInt1 = 0;
    while (paramInt1 < paramBoolean)
    {
      View localView = getChildAt(paramInt1);
      paramInt3 = localView.getVisibility();
      paramInt4 = 8;
      if (paramInt3 != paramInt4)
      {
        NativeContainerLayout.a locala = (NativeContainerLayout.a)localView.getLayoutParams();
        paramInt4 = a;
        int i = b;
        int j = a;
        int k = localView.getMeasuredWidth();
        j += k;
        paramInt3 = b;
        k = localView.getMeasuredHeight();
        paramInt3 += k;
        localView.layout(paramInt4, i, j, paramInt3);
      }
      paramInt1 += 1;
    }
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    measureChildren(paramInt1, paramInt2);
    int i = getChildCount();
    int j = 0;
    int k = 0;
    int m = 0;
    while (j < i)
    {
      View localView = getChildAt(j);
      int n = localView.getVisibility();
      int i1 = 8;
      if (n != i1)
      {
        NativeContainerLayout.a locala = (NativeContainerLayout.a)localView.getLayoutParams();
        i1 = a;
        int i2 = localView.getMeasuredWidth();
        i1 += i2;
        n = b;
        int i3 = localView.getMeasuredHeight();
        n += i3;
        m = Math.max(m, i1);
        k = Math.max(k, n);
      }
      j += 1;
    }
    i = getSuggestedMinimumHeight();
    i = Math.max(k, i);
    j = getSuggestedMinimumWidth();
    paramInt1 = resolveSize(Math.max(m, j), paramInt1);
    paramInt2 = resolveSize(i, paramInt2);
    setMeasuredDimension(paramInt1, paramInt2);
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.NativeContainerLayout
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */