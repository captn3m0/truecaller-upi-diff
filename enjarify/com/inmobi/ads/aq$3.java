package com.inmobi.ads;

import android.animation.Animator;
import android.view.View;
import android.view.View.OnAttachStateChangeListener;
import java.util.Iterator;
import java.util.List;

final class aq$3
  implements View.OnAttachStateChangeListener
{
  aq$3(aq paramaq, List paramList, ag paramag) {}
  
  public final void onViewAttachedToWindow(View paramView)
  {
    paramView = aq.d(c);
    Object localObject = a;
    paramView.a((List)localObject);
    aq.e(c);
    paramView = aq.e(c).h();
    localObject = b;
    paramView = ad.a(paramView, (ag)localObject);
    localObject = b;
    NativeTracker.TrackerEventType localTrackerEventType = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_CREATIVE_VIEW;
    ad localad = aq.e(c);
    if (paramView == null) {
      paramView = b;
    }
    paramView = localad.a(paramView);
    ((ag)localObject).a(localTrackerEventType, paramView);
  }
  
  public final void onViewDetachedFromWindow(View paramView)
  {
    paramView = aq.d(c);
    List localList = a;
    if (localList == null) {
      return;
    }
    Iterator localIterator = localList.iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      Animator localAnimator = nexta;
      localAnimator.cancel();
    }
    a.removeAll(localList);
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.aq.3
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */