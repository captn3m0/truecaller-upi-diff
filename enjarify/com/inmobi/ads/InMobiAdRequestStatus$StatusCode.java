package com.inmobi.ads;

public enum InMobiAdRequestStatus$StatusCode
{
  static
  {
    Object localObject = new com/inmobi/ads/InMobiAdRequestStatus$StatusCode;
    ((StatusCode)localObject).<init>("NO_ERROR", 0);
    NO_ERROR = (StatusCode)localObject;
    localObject = new com/inmobi/ads/InMobiAdRequestStatus$StatusCode;
    int i = 1;
    ((StatusCode)localObject).<init>("NETWORK_UNREACHABLE", i);
    NETWORK_UNREACHABLE = (StatusCode)localObject;
    localObject = new com/inmobi/ads/InMobiAdRequestStatus$StatusCode;
    int j = 2;
    ((StatusCode)localObject).<init>("NO_FILL", j);
    NO_FILL = (StatusCode)localObject;
    localObject = new com/inmobi/ads/InMobiAdRequestStatus$StatusCode;
    int k = 3;
    ((StatusCode)localObject).<init>("REQUEST_INVALID", k);
    REQUEST_INVALID = (StatusCode)localObject;
    localObject = new com/inmobi/ads/InMobiAdRequestStatus$StatusCode;
    int m = 4;
    ((StatusCode)localObject).<init>("REQUEST_PENDING", m);
    REQUEST_PENDING = (StatusCode)localObject;
    localObject = new com/inmobi/ads/InMobiAdRequestStatus$StatusCode;
    int n = 5;
    ((StatusCode)localObject).<init>("REQUEST_TIMED_OUT", n);
    REQUEST_TIMED_OUT = (StatusCode)localObject;
    localObject = new com/inmobi/ads/InMobiAdRequestStatus$StatusCode;
    int i1 = 6;
    ((StatusCode)localObject).<init>("INTERNAL_ERROR", i1);
    INTERNAL_ERROR = (StatusCode)localObject;
    localObject = new com/inmobi/ads/InMobiAdRequestStatus$StatusCode;
    int i2 = 7;
    ((StatusCode)localObject).<init>("SERVER_ERROR", i2);
    SERVER_ERROR = (StatusCode)localObject;
    localObject = new com/inmobi/ads/InMobiAdRequestStatus$StatusCode;
    int i3 = 8;
    ((StatusCode)localObject).<init>("AD_ACTIVE", i3);
    AD_ACTIVE = (StatusCode)localObject;
    localObject = new com/inmobi/ads/InMobiAdRequestStatus$StatusCode;
    int i4 = 9;
    ((StatusCode)localObject).<init>("EARLY_REFRESH_REQUEST", i4);
    EARLY_REFRESH_REQUEST = (StatusCode)localObject;
    localObject = new com/inmobi/ads/InMobiAdRequestStatus$StatusCode;
    int i5 = 10;
    ((StatusCode)localObject).<init>("AD_NO_LONGER_AVAILABLE", i5);
    AD_NO_LONGER_AVAILABLE = (StatusCode)localObject;
    localObject = new com/inmobi/ads/InMobiAdRequestStatus$StatusCode;
    int i6 = 11;
    ((StatusCode)localObject).<init>("MISSING_REQUIRED_DEPENDENCIES", i6);
    MISSING_REQUIRED_DEPENDENCIES = (StatusCode)localObject;
    localObject = new com/inmobi/ads/InMobiAdRequestStatus$StatusCode;
    int i7 = 12;
    ((StatusCode)localObject).<init>("REPETITIVE_LOAD", i7);
    REPETITIVE_LOAD = (StatusCode)localObject;
    localObject = new com/inmobi/ads/InMobiAdRequestStatus$StatusCode;
    int i8 = 13;
    ((StatusCode)localObject).<init>("GDPR_COMPLIANCE_ENFORCED", i8);
    GDPR_COMPLIANCE_ENFORCED = (StatusCode)localObject;
    localObject = new StatusCode[14];
    StatusCode localStatusCode = NO_ERROR;
    localObject[0] = localStatusCode;
    localStatusCode = NETWORK_UNREACHABLE;
    localObject[i] = localStatusCode;
    localStatusCode = NO_FILL;
    localObject[j] = localStatusCode;
    localStatusCode = REQUEST_INVALID;
    localObject[k] = localStatusCode;
    localStatusCode = REQUEST_PENDING;
    localObject[m] = localStatusCode;
    localStatusCode = REQUEST_TIMED_OUT;
    localObject[n] = localStatusCode;
    localStatusCode = INTERNAL_ERROR;
    localObject[i1] = localStatusCode;
    localStatusCode = SERVER_ERROR;
    localObject[i2] = localStatusCode;
    localStatusCode = AD_ACTIVE;
    localObject[i3] = localStatusCode;
    localStatusCode = EARLY_REFRESH_REQUEST;
    localObject[i4] = localStatusCode;
    localStatusCode = AD_NO_LONGER_AVAILABLE;
    localObject[i5] = localStatusCode;
    localStatusCode = MISSING_REQUIRED_DEPENDENCIES;
    localObject[i6] = localStatusCode;
    localStatusCode = REPETITIVE_LOAD;
    localObject[i7] = localStatusCode;
    localStatusCode = GDPR_COMPLIANCE_ENFORCED;
    localObject[i8] = localStatusCode;
    $VALUES = (StatusCode[])localObject;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.InMobiAdRequestStatus.StatusCode
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */