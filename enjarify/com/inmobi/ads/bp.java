package com.inmobi.ads;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class bp
{
  static final ArrayList f;
  private static final String h = "bp";
  int a;
  int b;
  List c;
  List d;
  String e;
  boolean g;
  private String i;
  
  static
  {
    ArrayList localArrayList = new java/util/ArrayList;
    List localList = Arrays.asList(new String[] { "image/jpeg", "image/png" });
    localArrayList.<init>(localList);
    f = localArrayList;
  }
  
  public bp(int paramInt1, int paramInt2, String paramString1, String paramString2)
  {
    i = paramString2;
    a = paramInt1;
    b = paramInt2;
    e = paramString1;
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    c = localArrayList;
    localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    d = localArrayList;
  }
  
  /* Error */
  static bp a(JSONObject paramJSONObject)
  {
    // Byte code:
    //   0: ldc 59
    //   2: astore_1
    //   3: aload_0
    //   4: aload_1
    //   5: invokevirtual 65	org/json/JSONObject:getInt	(Ljava/lang/String;)I
    //   8: istore_2
    //   9: ldc 67
    //   11: astore_3
    //   12: aload_0
    //   13: aload_3
    //   14: invokevirtual 65	org/json/JSONObject:getInt	(Ljava/lang/String;)I
    //   17: istore 4
    //   19: ldc 69
    //   21: astore 5
    //   23: aload_0
    //   24: aload 5
    //   26: invokevirtual 73	org/json/JSONObject:has	(Ljava/lang/String;)Z
    //   29: istore 6
    //   31: iload 6
    //   33: ifeq +18 -> 51
    //   36: ldc 69
    //   38: astore 5
    //   40: aload_0
    //   41: aload 5
    //   43: invokevirtual 77	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   46: astore 5
    //   48: goto +9 -> 57
    //   51: iconst_0
    //   52: istore 6
    //   54: aconst_null
    //   55: astore 5
    //   57: ldc 79
    //   59: astore 7
    //   61: aload_0
    //   62: aload 7
    //   64: aconst_null
    //   65: invokevirtual 83	org/json/JSONObject:optString	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   68: astore 7
    //   70: new 2	com/inmobi/ads/bp
    //   73: astore 8
    //   75: aload 8
    //   77: iload_2
    //   78: iload 4
    //   80: aload 5
    //   82: aload 7
    //   84: invokespecial 86	com/inmobi/ads/bp:<init>	(IILjava/lang/String;Ljava/lang/String;)V
    //   87: ldc 88
    //   89: astore_1
    //   90: aload_0
    //   91: aload_1
    //   92: invokevirtual 77	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   95: astore_1
    //   96: iconst_0
    //   97: istore 4
    //   99: aconst_null
    //   100: astore_3
    //   101: new 90	org/json/JSONArray
    //   104: astore 5
    //   106: aload 5
    //   108: aload_1
    //   109: invokespecial 93	org/json/JSONArray:<init>	(Ljava/lang/String;)V
    //   112: iconst_0
    //   113: istore_2
    //   114: aconst_null
    //   115: astore_1
    //   116: aload 5
    //   118: invokevirtual 97	org/json/JSONArray:length	()I
    //   121: istore 9
    //   123: iload_2
    //   124: iload 9
    //   126: if_icmpge +73 -> 199
    //   129: new 61	org/json/JSONObject
    //   132: astore 7
    //   134: aload 5
    //   136: iload_2
    //   137: invokevirtual 100	org/json/JSONArray:getString	(I)Ljava/lang/String;
    //   140: astore 10
    //   142: aload 7
    //   144: aload 10
    //   146: invokespecial 101	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   149: aload 7
    //   151: invokestatic 106	com/inmobi/ads/NativeTracker:a	(Lorg/json/JSONObject;)Lcom/inmobi/ads/NativeTracker;
    //   154: astore 7
    //   156: aload 7
    //   158: ifnull +10 -> 168
    //   161: aload 8
    //   163: aload 7
    //   165: invokevirtual 109	com/inmobi/ads/bp:a	(Lcom/inmobi/ads/NativeTracker;)V
    //   168: iload_2
    //   169: iconst_1
    //   170: iadd
    //   171: istore_2
    //   172: goto -56 -> 116
    //   175: astore_1
    //   176: invokestatic 114	com/inmobi/commons/core/a/a:a	()Lcom/inmobi/commons/core/a/a;
    //   179: astore 5
    //   181: new 116	com/inmobi/commons/core/e/a
    //   184: astore 7
    //   186: aload 7
    //   188: aload_1
    //   189: invokespecial 119	com/inmobi/commons/core/e/a:<init>	(Ljava/lang/Throwable;)V
    //   192: aload 5
    //   194: aload 7
    //   196: invokevirtual 122	com/inmobi/commons/core/a/a:a	(Lcom/inmobi/commons/core/e/a;)V
    //   199: ldc 124
    //   201: astore_1
    //   202: aload_0
    //   203: aload_1
    //   204: invokevirtual 77	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   207: astore_0
    //   208: new 90	org/json/JSONArray
    //   211: astore_1
    //   212: aload_1
    //   213: aload_0
    //   214: invokespecial 93	org/json/JSONArray:<init>	(Ljava/lang/String;)V
    //   217: aload_1
    //   218: invokevirtual 97	org/json/JSONArray:length	()I
    //   221: istore 11
    //   223: iload 4
    //   225: iload 11
    //   227: if_icmpge +64 -> 291
    //   230: new 61	org/json/JSONObject
    //   233: astore_0
    //   234: aload_1
    //   235: iload 4
    //   237: invokevirtual 100	org/json/JSONArray:getString	(I)Ljava/lang/String;
    //   240: astore 5
    //   242: aload_0
    //   243: aload 5
    //   245: invokespecial 101	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   248: aload_0
    //   249: invokestatic 129	com/inmobi/ads/bp$a:a	(Lorg/json/JSONObject;)Lcom/inmobi/ads/bp$a;
    //   252: astore_0
    //   253: aload_0
    //   254: ifnull +9 -> 263
    //   257: aload 8
    //   259: aload_0
    //   260: invokevirtual 132	com/inmobi/ads/bp:a	(Lcom/inmobi/ads/bp$a;)V
    //   263: iload 4
    //   265: iconst_1
    //   266: iadd
    //   267: istore 4
    //   269: goto -52 -> 217
    //   272: astore_0
    //   273: invokestatic 114	com/inmobi/commons/core/a/a:a	()Lcom/inmobi/commons/core/a/a;
    //   276: astore_1
    //   277: new 116	com/inmobi/commons/core/e/a
    //   280: astore_3
    //   281: aload_3
    //   282: aload_0
    //   283: invokespecial 119	com/inmobi/commons/core/e/a:<init>	(Ljava/lang/Throwable;)V
    //   286: aload_1
    //   287: aload_3
    //   288: invokevirtual 122	com/inmobi/commons/core/a/a:a	(Lcom/inmobi/commons/core/e/a;)V
    //   291: aload 8
    //   293: areturn
    //   294: astore_0
    //   295: aload_0
    //   296: invokevirtual 138	org/json/JSONException:getMessage	()Ljava/lang/String;
    //   299: pop
    //   300: invokestatic 114	com/inmobi/commons/core/a/a:a	()Lcom/inmobi/commons/core/a/a;
    //   303: astore_1
    //   304: new 116	com/inmobi/commons/core/e/a
    //   307: astore_3
    //   308: aload_3
    //   309: aload_0
    //   310: invokespecial 119	com/inmobi/commons/core/e/a:<init>	(Ljava/lang/Throwable;)V
    //   313: aload_1
    //   314: aload_3
    //   315: invokevirtual 122	com/inmobi/commons/core/a/a:a	(Lcom/inmobi/commons/core/e/a;)V
    //   318: aconst_null
    //   319: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	320	0	paramJSONObject	JSONObject
    //   2	114	1	str1	String
    //   175	14	1	localJSONException	JSONException
    //   201	113	1	localObject1	Object
    //   8	164	2	j	int
    //   11	304	3	localObject2	Object
    //   17	251	4	k	int
    //   21	223	5	localObject3	Object
    //   29	24	6	bool	boolean
    //   59	136	7	localObject4	Object
    //   73	219	8	localbp	bp
    //   121	6	9	m	int
    //   140	5	10	str2	String
    //   221	7	11	n	int
    // Exception table:
    //   from	to	target	type
    //   101	104	175	org/json/JSONException
    //   108	112	175	org/json/JSONException
    //   116	121	175	org/json/JSONException
    //   129	132	175	org/json/JSONException
    //   136	140	175	org/json/JSONException
    //   144	149	175	org/json/JSONException
    //   149	154	175	org/json/JSONException
    //   163	168	175	org/json/JSONException
    //   208	211	272	org/json/JSONException
    //   213	217	272	org/json/JSONException
    //   217	221	272	org/json/JSONException
    //   230	233	272	org/json/JSONException
    //   235	240	272	org/json/JSONException
    //   243	248	272	org/json/JSONException
    //   248	252	272	org/json/JSONException
    //   259	263	272	org/json/JSONException
    //   4	8	294	org/json/JSONException
    //   13	17	294	org/json/JSONException
    //   24	29	294	org/json/JSONException
    //   41	46	294	org/json/JSONException
    //   64	68	294	org/json/JSONException
    //   70	73	294	org/json/JSONException
    //   82	87	294	org/json/JSONException
    //   91	95	294	org/json/JSONException
    //   176	179	294	org/json/JSONException
    //   181	184	294	org/json/JSONException
    //   188	192	294	org/json/JSONException
    //   194	199	294	org/json/JSONException
    //   203	207	294	org/json/JSONException
    //   273	276	294	org/json/JSONException
    //   277	280	294	org/json/JSONException
    //   282	286	294	org/json/JSONException
    //   287	291	294	org/json/JSONException
  }
  
  final List a(int paramInt)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Iterator localIterator = c.iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      bp.a locala = (bp.a)localIterator.next();
      int j = a;
      if (j == paramInt) {
        localArrayList.add(locala);
      }
    }
    return localArrayList;
  }
  
  final List a(NativeTracker.TrackerEventType paramTrackerEventType)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Iterator localIterator = d.iterator();
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      NativeTracker localNativeTracker = (NativeTracker)localIterator.next();
      NativeTracker.TrackerEventType localTrackerEventType = b;
      boolean bool2 = localTrackerEventType.equals(paramTrackerEventType);
      if (bool2) {
        localArrayList.add(localNativeTracker);
      }
    }
    return localArrayList;
  }
  
  public final void a(NativeTracker paramNativeTracker)
  {
    d.add(paramNativeTracker);
  }
  
  final void a(bp.a parama)
  {
    c.add(parama);
  }
  
  public String toString()
  {
    JSONObject localJSONObject = new org/json/JSONObject;
    localJSONObject.<init>();
    try
    {
      localObject1 = i;
      if (localObject1 != null)
      {
        localObject1 = "id";
        localObject2 = i;
        localJSONObject.put((String)localObject1, localObject2);
      }
      localObject1 = "width";
      int j = a;
      localJSONObject.put((String)localObject1, j);
      localObject1 = "height";
      j = b;
      localJSONObject.put((String)localObject1, j);
      localObject1 = "clickThroughUrl";
      localObject2 = e;
      localJSONObject.put((String)localObject1, localObject2);
      localObject1 = new org/json/JSONArray;
      ((JSONArray)localObject1).<init>();
      localObject2 = c;
      localObject2 = ((List)localObject2).iterator();
      boolean bool;
      Object localObject3;
      for (;;)
      {
        bool = ((Iterator)localObject2).hasNext();
        if (!bool) {
          break;
        }
        localObject3 = ((Iterator)localObject2).next();
        localObject3 = (bp.a)localObject3;
        localObject3 = ((bp.a)localObject3).toString();
        ((JSONArray)localObject1).put(localObject3);
      }
      localObject2 = "resources";
      localJSONObject.put((String)localObject2, localObject1);
      localObject1 = new org/json/JSONArray;
      ((JSONArray)localObject1).<init>();
      localObject2 = d;
      localObject2 = ((List)localObject2).iterator();
      for (;;)
      {
        bool = ((Iterator)localObject2).hasNext();
        if (!bool) {
          break;
        }
        localObject3 = ((Iterator)localObject2).next();
        localObject3 = (NativeTracker)localObject3;
        localObject3 = ((NativeTracker)localObject3).toString();
        ((JSONArray)localObject1).put(localObject3);
      }
      localObject2 = "trackers";
      localJSONObject.put((String)localObject2, localObject1);
      return localJSONObject.toString();
    }
    catch (JSONException localJSONException)
    {
      localJSONException.getMessage();
      Object localObject1 = com.inmobi.commons.core.a.a.a();
      Object localObject2 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject2).<init>(localJSONException);
      ((com.inmobi.commons.core.a.a)localObject1).a((com.inmobi.commons.core.e.a)localObject2);
    }
    return "";
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.bp
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */