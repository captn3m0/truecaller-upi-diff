package com.inmobi.ads;

import android.content.Context;
import android.view.View;

final class NativeViewFactory$8
  extends NativeViewFactory.c
{
  NativeViewFactory$8(NativeViewFactory paramNativeViewFactory)
  {
    super(paramNativeViewFactory);
  }
  
  protected final View a(Context paramContext)
  {
    bk localbk = new com/inmobi/ads/bk;
    paramContext = paramContext.getApplicationContext();
    localbk.<init>(paramContext);
    return localbk;
  }
  
  protected final void a(View paramView, ag paramag, b paramb)
  {
    super.a(paramView, paramag, paramb);
    paramag = c;
    NativeViewFactory.a(paramView, paramag);
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.NativeViewFactory.8
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */