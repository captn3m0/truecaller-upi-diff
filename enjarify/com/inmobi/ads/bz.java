package com.inmobi.ads;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

final class bz
  extends bw
{
  private final ba d;
  private boolean e = false;
  
  bz(ba paramba)
  {
    super(paramba);
    d = paramba;
  }
  
  public final View a(View paramView, ViewGroup paramViewGroup, boolean paramBoolean)
  {
    paramBoolean = e;
    if (paramBoolean) {
      return null;
    }
    Context localContext = d.j();
    if (localContext == null) {
      return null;
    }
    ap localap = new com/inmobi/ads/ap;
    b localb = d.c;
    ba localba = d;
    ak localak = localba.h();
    localap.<init>(localContext, localb, localba, localak);
    b = localap;
    paramView = b.a(paramView, paramViewGroup, false, null);
    a(paramView);
    d.t();
    return paramView;
  }
  
  public final void a(int paramInt) {}
  
  public final void a(Context paramContext, int paramInt) {}
  
  public final void a(View... paramVarArgs) {}
  
  final b c()
  {
    return d.c;
  }
  
  public final void d() {}
  
  public final void e()
  {
    boolean bool = e;
    if (bool) {
      return;
    }
    bool = true;
    e = bool;
    bw.a locala = b;
    if (locala != null)
    {
      locala = b;
      locala.a();
    }
    super.e();
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.bz
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */