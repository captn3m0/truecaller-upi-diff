package com.inmobi.ads;

public enum InMobiBanner$AnimationType
{
  static
  {
    Object localObject = new com/inmobi/ads/InMobiBanner$AnimationType;
    ((AnimationType)localObject).<init>("ANIMATION_OFF", 0);
    ANIMATION_OFF = (AnimationType)localObject;
    localObject = new com/inmobi/ads/InMobiBanner$AnimationType;
    int i = 1;
    ((AnimationType)localObject).<init>("ROTATE_HORIZONTAL_AXIS", i);
    ROTATE_HORIZONTAL_AXIS = (AnimationType)localObject;
    localObject = new com/inmobi/ads/InMobiBanner$AnimationType;
    int j = 2;
    ((AnimationType)localObject).<init>("ANIMATION_ALPHA", j);
    ANIMATION_ALPHA = (AnimationType)localObject;
    localObject = new com/inmobi/ads/InMobiBanner$AnimationType;
    int k = 3;
    ((AnimationType)localObject).<init>("ROTATE_VERTICAL_AXIS", k);
    ROTATE_VERTICAL_AXIS = (AnimationType)localObject;
    localObject = new AnimationType[4];
    AnimationType localAnimationType = ANIMATION_OFF;
    localObject[0] = localAnimationType;
    localAnimationType = ROTATE_HORIZONTAL_AXIS;
    localObject[i] = localAnimationType;
    localAnimationType = ANIMATION_ALPHA;
    localObject[j] = localAnimationType;
    localAnimationType = ROTATE_VERTICAL_AXIS;
    localObject[k] = localAnimationType;
    $VALUES = (AnimationType[])localObject;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.InMobiBanner.AnimationType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */