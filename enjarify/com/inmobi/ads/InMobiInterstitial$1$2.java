package com.inmobi.ads;

import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;

final class InMobiInterstitial$1$2
  implements Runnable
{
  InMobiInterstitial$1$2(InMobiInterstitial.1 param1, InMobiInterstitial.InterstitialAdRequestListener paramInterstitialAdRequestListener, InMobiAdRequestStatus paramInMobiAdRequestStatus) {}
  
  public final void run()
  {
    try
    {
      localObject1 = a;
      localObject2 = b;
      ((InMobiInterstitial.InterstitialAdRequestListener)localObject1).onAdRequestCompleted((InMobiAdRequestStatus)localObject2, null);
      return;
    }
    catch (Exception localException)
    {
      Object localObject1 = Logger.InternalLogLevel.ERROR;
      Object localObject2 = InMobiInterstitial.access$200();
      Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "Publisher handler caused unexpected error");
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.InMobiInterstitial.1.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */