package com.inmobi.ads;

import android.os.Bundle;
import android.os.Message;
import java.util.Map;

final class InMobiInterstitial$2
  implements j.b
{
  InMobiInterstitial$2(InMobiInterstitial paramInMobiInterstitial) {}
  
  public final void a() {}
  
  public final void a(InMobiAdRequestStatus paramInMobiAdRequestStatus)
  {
    Object localObject1 = InMobiInterstitial.3.a;
    Object localObject2 = paramInMobiAdRequestStatus.getStatusCode();
    int i = ((InMobiAdRequestStatus.StatusCode)localObject2).ordinal();
    int j = localObject1[i];
    String str;
    switch (j)
    {
    default: 
      localObject1 = a;
      localObject2 = "AF";
      str = "";
      InMobiInterstitial.access$400((InMobiInterstitial)localObject1, (String)localObject2, str);
      break;
    case 5: 
      localObject1 = a;
      localObject2 = "ART";
      str = "MissingRequiredDependencies";
      InMobiInterstitial.access$400((InMobiInterstitial)localObject1, (String)localObject2, str);
      break;
    case 4: 
      localObject1 = a;
      localObject2 = "ART";
      str = "FrequentRequests";
      InMobiInterstitial.access$400((InMobiInterstitial)localObject1, (String)localObject2, str);
      break;
    case 2: 
    case 3: 
      localObject1 = a;
      localObject2 = "ART";
      str = "LoadInProgress";
      InMobiInterstitial.access$400((InMobiInterstitial)localObject1, (String)localObject2, str);
      break;
    case 1: 
      localObject1 = a;
      localObject2 = "ART";
      str = "NetworkNotAvailable";
      InMobiInterstitial.access$400((InMobiInterstitial)localObject1, (String)localObject2, str);
    }
    localObject1 = Message.obtain();
    what = 1;
    obj = paramInMobiAdRequestStatus;
    InMobiInterstitial.access$300(a).sendMessage((Message)localObject1);
  }
  
  public final void a(j paramj)
  {
    InMobiInterstitial.access$400(a, "AR", "");
    InMobiInterstitial localInMobiInterstitial = a;
    paramj = w;
    InMobiInterstitial.access$500(localInMobiInterstitial, paramj);
    InMobiInterstitial.access$300(a).sendEmptyMessage(3);
  }
  
  public final void a(Map paramMap)
  {
    InMobiInterstitial.access$400(a, "AVCL", "");
    Message localMessage = Message.obtain();
    what = 9;
    obj = paramMap;
    InMobiInterstitial.access$300(a).sendMessage(localMessage);
  }
  
  public final void a(boolean paramBoolean)
  {
    int i = 2;
    if (paramBoolean)
    {
      localMessage = Message.obtain();
      what = i;
      localBundle = new android/os/Bundle;
      localBundle.<init>();
      localBundle.putBoolean("available", true);
      localMessage.setData(localBundle);
      InMobiInterstitial.access$300(a).sendMessage(localMessage);
      return;
    }
    Message localMessage = Message.obtain();
    what = i;
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putBoolean("available", false);
    localMessage.setData(localBundle);
    InMobiInterstitial.access$300(a).sendMessage(localMessage);
  }
  
  public final void b()
  {
    InMobiInterstitial.access$300(a).sendEmptyMessage(5);
  }
  
  public final void b(Map paramMap)
  {
    Message localMessage = Message.obtain();
    what = 4;
    obj = paramMap;
    InMobiInterstitial.access$300(a).sendMessage(localMessage);
  }
  
  public final void c()
  {
    InMobiInterstitial.access$300(a).sendEmptyMessage(6);
  }
  
  public final void d()
  {
    InMobiInterstitial.access$400(a, "AVD", "");
    InMobiInterstitial.access$300(a).sendEmptyMessage(7);
  }
  
  public final void e()
  {
    InMobiInterstitial.access$400(a, "AVCD", "");
    InMobiInterstitial.access$300(a).sendEmptyMessage(10);
    y localy = y.d();
    long l = InMobiInterstitial.access$600(a);
    Map localMap = InMobiInterstitial.access$700(a);
    String str = InMobiInterstitial.access$800(a);
    bf localbf = bf.a(l, localMap, "int", str);
    localy.b(localbf);
  }
  
  public final void f()
  {
    InMobiInterstitial.access$300(a).sendEmptyMessage(11);
  }
  
  public final void g() {}
  
  public final void h() {}
  
  public final boolean i()
  {
    return true;
  }
  
  public final void j() {}
}

/* Location:
 * Qualified Name:     com.inmobi.ads.InMobiInterstitial.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */