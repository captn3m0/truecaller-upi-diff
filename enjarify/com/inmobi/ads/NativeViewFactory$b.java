package com.inmobi.ads;

import android.content.Context;
import android.view.MotionEvent;
import android.widget.TextView;

final class NativeViewFactory$b
  extends TextView
{
  public NativeViewFactory$b(Context paramContext)
  {
    super(paramContext);
  }
  
  protected final void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    paramInt1 = getLineHeight();
    paramInt3 = 0;
    if (paramInt1 > 0)
    {
      paramInt1 = getLineHeight();
      paramInt1 = paramInt2 / paramInt1;
    }
    else
    {
      paramInt1 = 0;
    }
    if (paramInt1 > 0)
    {
      setSingleLine(false);
      setLines(paramInt1);
    }
    paramInt2 = 1;
    if (paramInt1 == paramInt2) {
      setSingleLine();
    }
  }
  
  public final boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    return false;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.NativeViewFactory.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */