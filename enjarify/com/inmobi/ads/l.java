package com.inmobi.ads;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Point;
import android.view.View;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class l
{
  private static final String c = "l";
  List a;
  boolean b;
  
  public l()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    a = localArrayList;
    b = false;
  }
  
  private static Animator a(View paramView, String paramString, float paramFloat1, float paramFloat2)
  {
    paramFloat2 /= paramFloat1;
    paramView.setPivotX(0.0F);
    paramView.setPivotY(0.0F);
    float[] arrayOfFloat = new float[1];
    arrayOfFloat[0] = paramFloat2;
    return ObjectAnimator.ofFloat(paramView, paramString, arrayOfFloat);
  }
  
  private l.a a(Animator paramAnimator, ag paramag)
  {
    long l1 = 0L;
    paramAnimator.setDuration(l1);
    paramAnimator.setStartDelay(l1);
    paramag = c.g();
    if (paramag != null)
    {
      ax.a locala = a;
      paramag = b;
      long l2 = 1000L;
      long l3;
      if (paramag != null)
      {
        l3 = paramag.a() * l2;
        paramAnimator.setDuration(l3);
      }
      if (locala != null)
      {
        l3 = locala.a() * l2;
        paramAnimator.setStartDelay(l3);
      }
    }
    paramag = new com/inmobi/ads/l$a;
    paramag.<init>(this, paramAnimator);
    return paramag;
  }
  
  final List a(View paramView, ag paramag)
  {
    LinkedList localLinkedList = new java/util/LinkedList;
    localLinkedList.<init>();
    try
    {
      Object localObject1 = c;
      localObject1 = c;
      int i = x;
      i = NativeViewFactory.c(i);
      float f1 = i;
      Object localObject2 = c;
      localObject2 = d;
      int j = x;
      j = NativeViewFactory.c(j);
      float f2 = j;
      boolean bool1 = true;
      int k = 2;
      boolean bool2 = f1 < f2;
      if (bool2)
      {
        Object localObject3 = new float[k];
        i = (int)f1;
        f1 = i;
        localObject3[0] = f1;
        i = (int)f2;
        f1 = i;
        localObject3[bool1] = f1;
        localObject1 = ValueAnimator.ofFloat((float[])localObject3);
        localObject2 = paramView.getLayoutParams();
        localObject2 = (NativeContainerLayout.a)localObject2;
        localObject3 = new com/inmobi/ads/l$1;
        ((l.1)localObject3).<init>(this, (NativeContainerLayout.a)localObject2, paramView);
        ((ValueAnimator)localObject1).addUpdateListener((ValueAnimator.AnimatorUpdateListener)localObject3);
        localObject1 = a((Animator)localObject1, paramag);
        localLinkedList.add(localObject1);
      }
      localObject1 = c;
      localObject1 = c;
      i = y;
      i = NativeViewFactory.c(i);
      f1 = i;
      localObject2 = c;
      localObject2 = d;
      j = y;
      j = NativeViewFactory.c(j);
      f2 = j;
      bool2 = f1 < f2;
      Object localObject4;
      if (bool2)
      {
        float[] arrayOfFloat = new float[k];
        i = (int)f1;
        f1 = i;
        arrayOfFloat[0] = f1;
        i = (int)f2;
        f1 = i;
        arrayOfFloat[bool1] = f1;
        localObject1 = ValueAnimator.ofFloat(arrayOfFloat);
        localObject2 = paramView.getLayoutParams();
        localObject2 = (NativeContainerLayout.a)localObject2;
        localObject4 = new com/inmobi/ads/l$2;
        ((l.2)localObject4).<init>(this, (NativeContainerLayout.a)localObject2, paramView);
        ((ValueAnimator)localObject1).addUpdateListener((ValueAnimator.AnimatorUpdateListener)localObject4);
        localObject1 = a((Animator)localObject1, paramag);
        localLinkedList.add(localObject1);
      }
      localObject1 = c;
      localObject1 = a;
      i = x;
      i = NativeViewFactory.c(i);
      f1 = i;
      localObject2 = c;
      localObject2 = b;
      j = x;
      j = NativeViewFactory.c(j);
      f2 = j;
      bool1 = f1 < f2;
      if (bool1)
      {
        localObject4 = "scaleX";
        localObject1 = a(paramView, (String)localObject4, f1, f2);
        localObject1 = a((Animator)localObject1, paramag);
        localLinkedList.add(localObject1);
      }
      localObject1 = c;
      localObject1 = a;
      i = y;
      i = NativeViewFactory.c(i);
      f1 = i;
      localObject2 = c;
      localObject2 = b;
      j = y;
      j = NativeViewFactory.c(j);
      f2 = j;
      bool1 = f1 < f2;
      if (bool1)
      {
        localObject4 = "scaleY";
        paramView = a(paramView, (String)localObject4, f1, f2);
        paramView = a(paramView, paramag);
        localLinkedList.add(paramView);
      }
    }
    catch (Exception localException)
    {
      boolean bool3;
      for (;;) {}
    }
    bool3 = localLinkedList.isEmpty();
    if (bool3) {
      return null;
    }
    return localLinkedList;
  }
  
  final void a(List paramList)
  {
    if (paramList == null) {
      return;
    }
    paramList = paramList.iterator();
    for (;;)
    {
      boolean bool1 = paramList.hasNext();
      if (!bool1) {
        break;
      }
      l.a locala = (l.a)paramList.next();
      boolean bool2 = c;
      if (!bool2)
      {
        localObject = (ValueAnimator)a;
        long l = b;
        ((ValueAnimator)localObject).setCurrentPlayTime(l);
        ((ValueAnimator)localObject).start();
      }
      Object localObject = a;
      bool2 = ((List)localObject).contains(locala);
      if (!bool2)
      {
        localObject = a;
        ((List)localObject).add(locala);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */