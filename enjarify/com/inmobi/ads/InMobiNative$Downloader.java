package com.inmobi.ads;

import com.inmobi.commons.a.a;
import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;

public final class InMobiNative$Downloader
{
  public static final int STATE_DOWNLOADED = 1;
  public static final int STATE_DOWNLOADING = 0;
  public static final int STATE_ERROR = 2;
  public static final int STATE_INITIALIZING = 255;
  public static final int STATE_UNINITIALIZED = 254;
  
  public InMobiNative$Downloader(InMobiNative paramInMobiNative) {}
  
  public final int getDownloadProgress()
  {
    boolean bool = a.a();
    Object localObject;
    String str1;
    if (!bool)
    {
      localObject = Logger.InternalLogLevel.ERROR;
      str1 = InMobiNative.access$200();
      Logger.a((Logger.InternalLogLevel)localObject, str1, "InMobiNative is not initialized.Ignoring getDownloadProgress()");
      return 0;
    }
    try
    {
      localObject = this$0;
      localObject = InMobiNative.access$300((InMobiNative)localObject);
      if (localObject != null)
      {
        localObject = this$0;
        localObject = InMobiNative.access$300((InMobiNative)localObject);
        localObject = ((af)localObject).i();
        if (localObject != null)
        {
          localObject = (ad)localObject;
          localObject = ((ad)localObject).getApkDownloader();
          if (localObject != null) {
            return 0;
          }
        }
        return 0;
      }
    }
    catch (Exception localException)
    {
      localObject = Logger.InternalLogLevel.ERROR;
      str1 = InMobiNative.access$200();
      String str2 = "Encountered unexpected error in getting download progress";
      Logger.a((Logger.InternalLogLevel)localObject, str1, str2);
    }
    return 0;
  }
  
  public final int getDownloadStatus()
  {
    boolean bool = a.a();
    int i = -2;
    Object localObject;
    String str1;
    if (!bool)
    {
      localObject = Logger.InternalLogLevel.ERROR;
      str1 = InMobiNative.access$200();
      Logger.a((Logger.InternalLogLevel)localObject, str1, "InMobiNative is not initialized.Ignoring getDownloadStatus()");
      return i;
    }
    try
    {
      localObject = this$0;
      localObject = InMobiNative.access$300((InMobiNative)localObject);
      if (localObject != null)
      {
        localObject = this$0;
        localObject = InMobiNative.access$300((InMobiNative)localObject);
        localObject = ((af)localObject).i();
        if (localObject != null)
        {
          localObject = (ad)localObject;
          localObject = ((ad)localObject).getApkDownloader();
          if (localObject != null) {
            return i;
          }
        }
        return i;
      }
    }
    catch (Exception localException)
    {
      localObject = Logger.InternalLogLevel.ERROR;
      str1 = InMobiNative.access$200();
      String str2 = "Encountered unexpected error in getting download progress";
      Logger.a((Logger.InternalLogLevel)localObject, str1, str2);
    }
    return i;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.InMobiNative.Downloader
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */