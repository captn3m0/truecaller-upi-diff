package com.inmobi.ads;

import com.inmobi.commons.core.utilities.d;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public final class NativeTracker
{
  private static final String e = "NativeTracker";
  String a;
  NativeTracker.TrackerEventType b;
  Map c;
  Map d;
  private String f;
  private int g;
  
  public NativeTracker(String paramString, int paramInt, NativeTracker.TrackerEventType paramTrackerEventType, Map paramMap)
  {
    this("url_ping", paramString, paramInt, paramTrackerEventType, paramMap);
  }
  
  private NativeTracker(String paramString1, String paramString2, int paramInt, NativeTracker.TrackerEventType paramTrackerEventType, Map paramMap)
  {
    f = paramString1;
    paramString1 = paramString2.trim();
    a = paramString1;
    g = paramInt;
    b = paramTrackerEventType;
    c = paramMap;
  }
  
  static NativeTracker.TrackerEventType a(String paramString)
  {
    if (paramString != null)
    {
      int i = paramString.length();
      if (i != 0)
      {
        i = -1;
        int j = paramString.hashCode();
        String str;
        boolean bool;
        switch (j)
        {
        default: 
          break;
        case 2114088489: 
          str = "Impression";
          bool = paramString.equals(str);
          if (bool) {
            i = 4;
          }
          break;
        case 1778167540: 
          str = "creativeView";
          bool = paramString.equals(str);
          if (bool) {
            i = 13;
          }
          break;
        case 1342121331: 
          str = "closeEndCard";
          bool = paramString.equals(str);
          if (bool) {
            i = 23;
          }
          break;
        case 883937877: 
          str = "page_view";
          bool = paramString.equals(str);
          if (bool) {
            i = 6;
          }
          break;
        case 560220243: 
          str = "firstQuartile";
          bool = paramString.equals(str);
          if (bool) {
            i = 9;
          }
          break;
        case 354294980: 
          str = "VideoImpression";
          bool = paramString.equals(str);
          if (bool) {
            i = 5;
          }
          break;
        case 113951609: 
          str = "exitFullscreen";
          bool = paramString.equals(str);
          if (bool) {
            i = 15;
          }
          break;
        case 110066619: 
          str = "fullscreen";
          bool = paramString.equals(str);
          if (bool) {
            i = 14;
          }
          break;
        case 109757538: 
          str = "start";
          bool = paramString.equals(str);
          if (bool) {
            i = 8;
          }
          break;
        case 106440182: 
          str = "pause";
          bool = paramString.equals(str);
          if (bool) {
            i = 18;
          }
          break;
        case 96784904: 
          str = "error";
          bool = paramString.equals(str);
          if (bool) {
            i = 20;
          }
          break;
        case 94750088: 
          str = "click";
          bool = paramString.equals(str);
          if (bool) {
            i = 7;
          }
          break;
        case 3363353: 
          str = "mute";
          bool = paramString.equals(str);
          if (bool) {
            i = 16;
          }
          break;
        case 3327206: 
          str = "load";
          bool = paramString.equals(str);
          if (bool) {
            i = 2;
          }
          break;
        case -45894975: 
          str = "IAS_VIEWABILITY";
          bool = paramString.equals(str);
          if (bool) {
            i = 21;
          }
          break;
        case -174104201: 
          str = "client_fill";
          bool = paramString.equals(str);
          if (bool) {
            i = 3;
          }
          break;
        case -284840886: 
          str = "unknown";
          bool = paramString.equals(str);
          if (bool) {
            i = 1;
          }
          break;
        case -599445191: 
          str = "complete";
          bool = paramString.equals(str);
          if (bool) {
            i = 12;
          }
          break;
        case -667101923: 
          str = "zMoatVASTIDs";
          bool = paramString.equals(str);
          if (bool) {
            i = 22;
          }
          break;
        case -840405966: 
          str = "unmute";
          bool = paramString.equals(str);
          if (bool) {
            i = 17;
          }
          break;
        case -934426579: 
          str = "resume";
          bool = paramString.equals(str);
          if (bool) {
            i = 19;
          }
          break;
        case -1337830390: 
          str = "thirdQuartile";
          bool = paramString.equals(str);
          if (bool) {
            i = 11;
          }
          break;
        case -1638835128: 
          str = "midpoint";
          bool = paramString.equals(str);
          if (bool) {
            i = 10;
          }
          break;
        }
        switch (i)
        {
        default: 
          return NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_UNKNOWN;
        case 23: 
          return NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_END_CARD_CLOSE;
        case 22: 
          return NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_MOAT;
        case 21: 
          return NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_IAS;
        case 20: 
          return NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_ERROR;
        case 19: 
          return NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_RESUME;
        case 18: 
          return NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_PAUSE;
        case 17: 
          return NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_UNMUTE;
        case 16: 
          return NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_MUTE;
        case 15: 
          return NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_EXIT_FULLSCREEN;
        case 14: 
          return NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_FULLSCREEN;
        case 13: 
          return NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_CREATIVE_VIEW;
        case 12: 
          return NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_Q4;
        case 11: 
          return NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_Q3;
        case 10: 
          return NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_Q2;
        case 9: 
          return NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_Q1;
        case 8: 
          return NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_PLAY;
        case 7: 
          return NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_CLICK;
        case 6: 
          return NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_PAGE_VIEW;
        case 5: 
          return NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_VIDEO_RENDER;
        case 4: 
          return NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_RENDER;
        case 3: 
          return NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_CLIENT_FILL;
        }
        return NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_LOAD;
      }
    }
    return NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_UNKNOWN;
  }
  
  static NativeTracker a(JSONObject paramJSONObject)
  {
    Object localObject1 = "type";
    try
    {
      localObject1 = paramJSONObject.getString((String)localObject1);
      if (localObject1 != null)
      {
        int i = ((String)localObject1).length();
        if (i != 0)
        {
          localObject2 = Locale.US;
          localObject1 = ((String)localObject1).toLowerCase((Locale)localObject2);
          i = ((String)localObject1).hashCode();
          int j = -1918378017;
          if (i != j)
          {
            j = -970292670;
            if (i != j)
            {
              j = -284840886;
              if (i != j)
              {
                j = 2015859192;
                if (i != j) {
                  break label119;
                }
                localObject2 = "webview_ping";
                ((String)localObject1).equals(localObject2);
                break label119;
              }
              localObject2 = "unknown";
            }
          }
          for (;;)
          {
            ((String)localObject1).equals(localObject2);
            break;
            localObject2 = "url_ping";
            continue;
            localObject2 = "html_script";
          }
        }
      }
      label119:
      localObject1 = "eventType";
      localObject1 = paramJSONObject.getString((String)localObject1);
      localObject1 = a((String)localObject1);
      localObject2 = "url";
      localObject2 = paramJSONObject.getString((String)localObject2);
      Object localObject3 = "eventId";
      int k = -1;
      int m = paramJSONObject.optInt((String)localObject3, k);
      localObject3 = new java/util/HashMap;
      ((HashMap)localObject3).<init>();
      NativeTracker localNativeTracker = new com/inmobi/ads/NativeTracker;
      localNativeTracker.<init>((String)localObject2, m, (NativeTracker.TrackerEventType)localObject1, (Map)localObject3);
      return localNativeTracker;
    }
    catch (JSONException paramJSONObject)
    {
      paramJSONObject.getMessage();
      localObject1 = com.inmobi.commons.core.a.a.a();
      Object localObject2 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject2).<init>(paramJSONObject);
      ((com.inmobi.commons.core.a.a)localObject1).a((com.inmobi.commons.core.e.a)localObject2);
    }
    return null;
  }
  
  public final String toString()
  {
    JSONObject localJSONObject = new org/json/JSONObject;
    localJSONObject.<init>();
    Object localObject1 = "type";
    try
    {
      localObject2 = f;
      localJSONObject.put((String)localObject1, localObject2);
      localObject1 = "url";
      localObject2 = a;
      localJSONObject.put((String)localObject1, localObject2);
      localObject1 = "eventType";
      localObject2 = b;
      Object localObject3 = NativeTracker.1.a;
      int i = ((NativeTracker.TrackerEventType)localObject2).ordinal();
      i = localObject3[i];
      switch (i)
      {
      default: 
        localObject2 = "unknown";
        break;
      case 23: 
        localObject2 = "closeEndCard";
        break;
      case 22: 
        localObject2 = "zMoatVASTIDs";
        break;
      case 21: 
        localObject2 = "IAS_VIEWABILITY";
        break;
      case 20: 
        localObject2 = "error";
        break;
      case 19: 
        localObject2 = "resume";
        break;
      case 18: 
        localObject2 = "pause";
        break;
      case 17: 
        localObject2 = "unmute";
        break;
      case 16: 
        localObject2 = "mute";
        break;
      case 15: 
        localObject2 = "exitFullscreen";
        break;
      case 14: 
        localObject2 = "fullscreen";
        break;
      case 13: 
        localObject2 = "creativeView";
        break;
      case 12: 
        localObject2 = "complete";
        break;
      case 11: 
        localObject2 = "thirdQuartile";
        break;
      case 10: 
        localObject2 = "midpoint";
        break;
      case 9: 
        localObject2 = "firstQuartile";
        break;
      case 8: 
        localObject2 = "start";
        break;
      case 7: 
        localObject2 = "click";
        break;
      case 6: 
        localObject2 = "page_view";
        break;
      case 5: 
        localObject2 = "VideoImpression";
        break;
      case 4: 
        localObject2 = "Impression";
        break;
      case 3: 
        localObject2 = "client_fill";
        break;
      case 2: 
        localObject2 = "load";
      }
      localJSONObject.put((String)localObject1, localObject2);
      localObject1 = "eventId";
      i = g;
      localJSONObject.put((String)localObject1, i);
      localObject1 = "extras";
      localObject2 = c;
      if (localObject2 == null)
      {
        localObject2 = new java/util/HashMap;
        ((HashMap)localObject2).<init>();
      }
      else
      {
        localObject2 = c;
      }
      localObject3 = ",";
      localObject2 = d.a((Map)localObject2, (String)localObject3);
      localJSONObject.put((String)localObject1, localObject2);
      return localJSONObject.toString();
    }
    catch (JSONException localJSONException)
    {
      localJSONException.getMessage();
      localObject1 = com.inmobi.commons.core.a.a.a();
      Object localObject2 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject2).<init>(localJSONException);
      ((com.inmobi.commons.core.a.a)localObject1).a((com.inmobi.commons.core.e.a)localObject2);
    }
    return "";
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.NativeTracker
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */