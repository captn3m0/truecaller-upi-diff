package com.inmobi.ads;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import java.lang.ref.WeakReference;

final class j$a
  extends Handler
{
  private WeakReference a;
  
  j$a(j paramj)
  {
    super((Looper)localObject);
    localObject = new java/lang/ref/WeakReference;
    ((WeakReference)localObject).<init>(paramj);
    a = ((WeakReference)localObject);
  }
  
  public final void handleMessage(Message paramMessage)
  {
    Object localObject = a;
    if (localObject == null) {
      localObject = null;
    } else {
      localObject = (j)((WeakReference)localObject).get();
    }
    if (localObject == null) {
      return;
    }
    Bundle localBundle = paramMessage.getData();
    String str = "placementId";
    long l = localBundle.getLong(str);
    int i = what;
    switch (i)
    {
    default: 
      switch (i)
      {
      default: 
        return;
      case 14: 
        ((j)localObject).C();
        return;
      case 13: 
        paramMessage = (InMobiAdRequestStatus)obj;
        ((j)localObject).b(paramMessage);
        return;
      case 12: 
        ((j)localObject).v();
        return;
      }
      ((j)localObject).t();
      return;
    case 4: 
      boolean bool1 = localBundle.getBoolean("assetAvailable");
      ((j)localObject).b(l, bool1);
      return;
    case 3: 
      return;
    case 2: 
      paramMessage = (a)obj;
      ((j)localObject).c(l, paramMessage);
      return;
    }
    paramMessage = (a)obj;
    boolean bool2 = localBundle.getBoolean("adAvailable");
    ((j)localObject).a(l, bool2, paramMessage);
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.j.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */