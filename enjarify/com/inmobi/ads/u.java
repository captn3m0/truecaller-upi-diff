package com.inmobi.ads;

import android.app.Application;
import android.view.View;
import android.webkit.WebView;
import com.inmobi.commons.core.utilities.uid.c;
import com.inmobi.signals.p;
import com.inmobi.signals.q;
import com.inmobi.signals.q.b;
import com.moat.analytics.mobile.inm.MoatAnalytics;
import com.moat.analytics.mobile.inm.MoatFactory;
import com.moat.analytics.mobile.inm.MoatOptions;
import com.moat.analytics.mobile.inm.NativeDisplayTracker;
import com.moat.analytics.mobile.inm.ReactiveVideoTracker;
import com.moat.analytics.mobile.inm.ReactiveVideoTrackerPlugin;
import com.moat.analytics.mobile.inm.WebAdTracker;
import java.util.Map;

class u
{
  private static final String a = "u";
  private static final boolean b = false;
  private static boolean c = false;
  
  static
  {
    "row".contains("staging");
  }
  
  static NativeDisplayTracker a(Application paramApplication, String paramString, View paramView, Map paramMap)
  {
    boolean bool = c;
    if (!bool) {
      a(paramApplication);
    }
    MoatAnalytics.getInstance().prepareNativeDisplayTracking(paramString);
    return MoatFactory.create().createNativeDisplayTracker(paramView, paramMap);
  }
  
  static ReactiveVideoTracker a(Application paramApplication, String paramString)
  {
    boolean bool = c;
    if (!bool) {
      a(paramApplication);
    }
    paramApplication = MoatFactory.create();
    ReactiveVideoTrackerPlugin localReactiveVideoTrackerPlugin = new com/moat/analytics/mobile/inm/ReactiveVideoTrackerPlugin;
    localReactiveVideoTrackerPlugin.<init>(paramString);
    return (ReactiveVideoTracker)paramApplication.createCustomTracker(localReactiveVideoTrackerPlugin);
  }
  
  static WebAdTracker a(Application paramApplication, WebView paramWebView)
  {
    boolean bool = c;
    if (!bool) {
      a(paramApplication);
    }
    return MoatFactory.create().createWebAdTracker(paramWebView);
  }
  
  static void a(Application paramApplication)
  {
    boolean bool1 = c;
    if (bool1) {
      return;
    }
    try
    {
      localObject1 = new com/moat/analytics/mobile/inm/MoatOptions;
      ((MoatOptions)localObject1).<init>();
      boolean bool2 = b;
      loggingEnabled = bool2;
      localObject2 = p.a();
      localObject2 = a;
      localObject2 = a;
      bool2 = ((q.b)localObject2).a();
      boolean bool3 = true;
      if (!bool2)
      {
        bool2 = true;
      }
      else
      {
        bool2 = false;
        localObject2 = null;
      }
      disableLocationServices = bool2;
      c.a();
      localObject2 = c.g();
      if (localObject2 != null)
      {
        bool2 = ((Boolean)localObject2).booleanValue();
        if (!bool2) {}
      }
      else
      {
        disableAdIdCollection = bool3;
      }
      localObject2 = MoatAnalytics.getInstance();
      ((MoatAnalytics)localObject2).start((MoatOptions)localObject1, paramApplication);
      c = bool3;
      return;
    }
    catch (Exception paramApplication)
    {
      paramApplication.getMessage();
      Object localObject1 = com.inmobi.commons.core.a.a.a();
      Object localObject2 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject2).<init>(paramApplication);
      ((com.inmobi.commons.core.a.a)localObject1).a((com.inmobi.commons.core.e.a)localObject2);
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.u
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */