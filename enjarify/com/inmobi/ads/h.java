package com.inmobi.ads;

import android.os.SystemClock;
import com.inmobi.ads.cache.AssetStore;
import com.inmobi.ads.cache.b;
import com.inmobi.commons.core.network.NetworkError;
import com.inmobi.commons.core.network.NetworkError.ErrorCode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class h
  implements d.a
{
  private static final String f = "h";
  final h.a a;
  final c b;
  i c;
  boolean d = false;
  long e = 0L;
  private final com.inmobi.ads.cache.f g;
  
  public h(h.a parama)
  {
    h.1 local1 = new com/inmobi/ads/h$1;
    local1.<init>(this);
    g = local1;
    a = parama;
    parama = c.a();
    b = parama;
  }
  
  private List c(f paramf)
  {
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    try
    {
      localObject2 = new org/json/JSONObject;
      localObject3 = a;
      localObject3 = ((com.inmobi.commons.core.network.d)localObject3).b();
      ((JSONObject)localObject2).<init>((String)localObject3);
      localObject3 = "requestId";
      localObject3 = ((JSONObject)localObject2).getString((String)localObject3);
      localObject3 = ((String)localObject3).trim();
      Object localObject4 = "ads";
      localObject2 = ((JSONObject)localObject2).getJSONArray((String)localObject4);
      if (localObject2 != null)
      {
        localObject4 = c;
        int i = d;
        int j = ((JSONArray)localObject2).length();
        i = Math.min(i, j);
        j = 0;
        while (j < i)
        {
          Object localObject5 = ((JSONArray)localObject2).getJSONObject(j);
          e locale = c;
          long l1 = a;
          Object localObject6 = c;
          localObject6 = e;
          Object localObject7 = c;
          localObject7 = c;
          Object localObject8 = new java/lang/StringBuilder;
          ((StringBuilder)localObject8).<init>();
          ((StringBuilder)localObject8).append((String)localObject3);
          Object localObject9 = "_";
          ((StringBuilder)localObject8).append((String)localObject9);
          ((StringBuilder)localObject8).append(j);
          localObject8 = ((StringBuilder)localObject8).toString();
          localObject9 = c;
          localObject9 = i;
          Object localObject10 = c;
          localObject10 = m;
          localObject5 = a.a.a((JSONObject)localObject5, l1, (String)localObject6, (String)localObject7, (String)localObject8, (String)localObject9, (InMobiAdRequest.MonetizationContext)localObject10);
          if (localObject5 != null) {
            ((List)localObject1).add(localObject5);
          }
          j += 1;
        }
        if (i > 0)
        {
          boolean bool = ((List)localObject1).isEmpty();
          if (bool) {
            return null;
          }
        }
      }
    }
    catch (JSONException paramf)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      ((Map)localObject1).put("errorCode", "ParsingError");
      paramf = paramf.getLocalizedMessage();
      ((Map)localObject1).put("reason", paramf);
      long l2 = SystemClock.elapsedRealtime();
      long l3 = e;
      l2 -= l3;
      Object localObject2 = Long.valueOf(l2);
      ((Map)localObject1).put("latency", localObject2);
      localObject2 = com.inmobi.commons.a.a.e();
      ((Map)localObject1).put("im-accid", localObject2);
      paramf = a;
      localObject2 = "ads";
      Object localObject3 = "ServerError";
      paramf.a((String)localObject2, (String)localObject3, (Map)localObject1);
      localObject1 = null;
    }
    return (List)localObject1;
  }
  
  private void d()
  {
    Object localObject1 = c;
    if (localObject1 == null) {
      return;
    }
    b();
    localObject1 = c;
    long l = d;
    String str1 = c.f;
    InMobiAdRequest.MonetizationContext localMonetizationContext = c.m;
    String str2 = g.a(c.g);
    int i = c.a(l, str1, localMonetizationContext, str2);
    Object localObject2 = c.j;
    int j = c;
    if (i < j)
    {
      localObject1 = "int";
      localObject2 = c.h;
      boolean bool = ((String)localObject1).equals(localObject2);
      if (bool)
      {
        bool = true;
        d = bool;
        localObject2 = c;
        a((i)localObject2, bool);
        return;
      }
      e();
    }
  }
  
  private void e()
  {
    Object localObject = c;
    if (localObject == null) {
      return;
    }
    localObject = g.a(h);
    if (localObject != null)
    {
      long l = c.d;
      Map localMap = c.g;
      String str1 = c.h;
      String str2 = c.e;
      bf localbf = bf.a(l, localMap, str1, str2);
      InMobiAdRequest.MonetizationContext localMonetizationContext = c.m;
      f = localMonetizationContext;
      ((g)localObject).b(localbf);
    }
  }
  
  final a a(long paramLong, String paramString1, int paramInt, InMobiAdRequest.MonetizationContext paramMonetizationContext, String paramString2)
  {
    b();
    int i = c.a(paramLong, paramString1, paramMonetizationContext, paramString2);
    boolean bool1 = false;
    if (i == 0) {
      return null;
    }
    c localc = b;
    a locala = localc.b(paramLong, paramString1, paramMonetizationContext, paramString2);
    if (locala == null) {
      return null;
    }
    bool1 = true;
    d = bool1;
    int j = c.a(paramLong, paramString1, paramMonetizationContext, paramString2);
    Object localObject1 = new java/util/HashMap;
    ((HashMap)localObject1).<init>();
    paramMonetizationContext = f;
    ((Map)localObject1).put("clientRequestId", paramMonetizationContext);
    paramMonetizationContext = com.inmobi.commons.a.a.e();
    ((Map)localObject1).put("im-accid", paramMonetizationContext);
    paramMonetizationContext = c.a();
    ((Map)localObject1).put("isPreloaded", paramMonetizationContext);
    paramString1 = a;
    paramMonetizationContext = "ads";
    paramString2 = "AdCacheHit";
    paramString1.a(paramMonetizationContext, paramString2, (Map)localObject1);
    if (j < paramInt)
    {
      Object localObject2 = c;
      if (localObject2 != null)
      {
        localObject1 = "native";
        localObject2 = h;
        boolean bool2 = ((String)localObject1).equals(localObject2);
        if (bool2)
        {
          e();
        }
        else
        {
          localObject2 = c;
          a((i)localObject2, bool1);
        }
      }
    }
    return locala;
  }
  
  final String a()
  {
    long l1 = c.d;
    Object localObject1 = c.f;
    int i = c.j.c;
    InMobiAdRequest.MonetizationContext localMonetizationContext = c.m;
    String str1 = g.a(c.g);
    Object localObject2 = a(l1, (String)localObject1, i, localMonetizationContext, str1);
    boolean bool1 = true;
    String str2;
    if (localObject2 == null)
    {
      localObject2 = c.a();
      str2 = "1";
      boolean bool2 = ((String)localObject2).equals(str2);
      if (bool2)
      {
        localObject2 = c;
        localObject2 = a((i)localObject2, bool1);
      }
      else
      {
        localObject2 = c;
        localObject2 = a((i)localObject2, false);
      }
    }
    else
    {
      str2 = f;
      localObject1 = a;
      long l2 = c.d;
      ((h.a)localObject1).a(l2, (a)localObject2);
      localObject1 = "INMOBIJSON";
      String str3 = ((a)localObject2).d();
      boolean bool3 = ((String)localObject1).equalsIgnoreCase(str3);
      if (bool3)
      {
        localObject1 = new java/util/ArrayList;
        a[] arrayOfa = new a[bool1];
        arrayOfa[0] = localObject2;
        localObject2 = Arrays.asList(arrayOfa);
        ((ArrayList)localObject1).<init>((Collection)localObject2);
        a((List)localObject1);
      }
      localObject2 = str2;
    }
    return (String)localObject2;
  }
  
  final String a(i parami, boolean paramBoolean)
  {
    if (parami != null)
    {
      localObject1 = k;
      if (localObject1 == null)
      {
        localObject1 = new java/util/HashMap;
        ((HashMap)localObject1).<init>();
      }
      str1 = "preload-request";
      localObject2 = String.valueOf(paramBoolean);
      ((Map)localObject1).put(str1, localObject2);
      k = ((Map)localObject1);
    }
    Object localObject2 = new com/inmobi/ads/e;
    String str2 = a;
    long l1 = d;
    com.inmobi.commons.core.utilities.uid.d locald = l;
    com.inmobi.ads.cache.d.a();
    String str3 = com.inmobi.ads.cache.d.c();
    ((e)localObject2).<init>(str2, l1, locald, str3);
    Object localObject1 = e;
    f = ((String)localObject1);
    localObject1 = g;
    g = ((Map)localObject1);
    localObject1 = h;
    e = ((String)localObject1);
    localObject1 = i;
    b = ((String)localObject1);
    int i = j.b;
    d = i;
    localObject1 = k;
    h = ((Map)localObject1);
    localObject1 = i;
    b = ((String)localObject1);
    localObject1 = f;
    c = ((String)localObject1);
    i = c * 1000;
    p = i;
    i = c * 1000;
    q = i;
    localObject1 = m;
    j = ((InMobiAdRequest.MonetizationContext)localObject1);
    boolean bool = n;
    x = bool;
    long l2 = SystemClock.elapsedRealtime();
    e = l2;
    localObject1 = new com/inmobi/ads/d;
    ((d)localObject1).<init>((e)localObject2, this);
    ((d)localObject1).a();
    localObject1 = new java/util/HashMap;
    ((HashMap)localObject1).<init>();
    parami = parami.a();
    ((Map)localObject1).put("isPreloaded", parami);
    String str1 = i;
    ((Map)localObject1).put("clientRequestId", str1);
    str1 = com.inmobi.commons.a.a.e();
    ((Map)localObject1).put("im-accid", str1);
    a.a("ads", "ServerCallInitiated", (Map)localObject1);
    return i;
  }
  
  final void a(a parama)
  {
    h.2 local2 = new com/inmobi/ads/h$2;
    local2.<init>(this, parama);
    local2.start();
  }
  
  public final void a(f paramf)
  {
    h localh = this;
    Object localObject1 = paramf;
    List localList1 = c(paramf);
    boolean bool1;
    long l1;
    InMobiAdRequestStatus.StatusCode localStatusCode;
    if (localList1 == null)
    {
      localObject1 = a;
      ((com.inmobi.commons.core.network.d)localObject1).b();
      bool1 = d;
      if (!bool1)
      {
        localObject1 = a;
        localObject2 = c;
        l1 = d;
        localObject3 = new com/inmobi/ads/InMobiAdRequestStatus;
        localStatusCode = InMobiAdRequestStatus.StatusCode.INTERNAL_ERROR;
        ((InMobiAdRequestStatus)localObject3).<init>(localStatusCode);
        ((h.a)localObject1).a(l1, (InMobiAdRequestStatus)localObject3);
      }
      return;
    }
    int m = localList1.size();
    if (m == 0)
    {
      a.b();
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      l2 = SystemClock.elapsedRealtime();
      l3 = e;
      l2 -= l3;
      localObject4 = Long.valueOf(l2);
      ((Map)localObject1).put("latency", localObject4);
      localObject4 = c.a();
      ((Map)localObject1).put("isPreloaded", localObject4);
      localObject4 = com.inmobi.commons.a.a.e();
      ((Map)localObject1).put("im-accid", localObject4);
      localObject2 = a;
      localObject4 = "ads";
      localObject3 = "ServerNoFill";
      ((h.a)localObject2).a((String)localObject4, (String)localObject3, (Map)localObject1);
      bool1 = d;
      if (!bool1)
      {
        localObject1 = a;
        localObject2 = c;
        l1 = d;
        localObject3 = new com/inmobi/ads/InMobiAdRequestStatus;
        localStatusCode = InMobiAdRequestStatus.StatusCode.NO_FILL;
        ((InMobiAdRequestStatus)localObject3).<init>(localStatusCode);
        ((h.a)localObject1).a(l1, (InMobiAdRequestStatus)localObject3);
      }
      return;
    }
    localObject1 = new java/util/HashMap;
    ((HashMap)localObject1).<init>();
    int n = localList1.size();
    Object localObject4 = Integer.valueOf(n);
    ((Map)localObject1).put("numberOfAdsReturned", localObject4);
    long l2 = SystemClock.elapsedRealtime();
    long l3 = e;
    l2 -= l3;
    localObject4 = Long.valueOf(l2);
    ((Map)localObject1).put("latency", localObject4);
    localObject4 = c.a();
    ((Map)localObject1).put("isPreloaded", localObject4);
    localObject4 = com.inmobi.commons.a.a.e();
    ((Map)localObject1).put("im-accid", localObject4);
    Object localObject2 = a;
    localObject4 = "ads";
    Object localObject3 = "ServerFill";
    ((h.a)localObject2).a((String)localObject4, (String)localObject3, (Map)localObject1);
    localObject1 = ((a)localList1.get(0)).d();
    String str1 = g.a(c.g);
    localObject2 = localList1.get(0);
    Object localObject5 = localObject2;
    localObject5 = (a)localObject2;
    if (localObject1 != null)
    {
      localObject2 = Locale.ENGLISH;
      localObject1 = ((String)localObject1).toUpperCase((Locale)localObject2);
      m = -1;
      n = ((String)localObject1).hashCode();
      int i1 = -598127114;
      int i2 = 1;
      if (n != i1)
      {
        i1 = 2228139;
        if (n == i1)
        {
          localObject4 = "HTML";
          bool1 = ((String)localObject1).equals(localObject4);
          if (bool1)
          {
            m = 0;
            localObject2 = null;
          }
        }
      }
      else
      {
        localObject4 = "INMOBIJSON";
        bool1 = ((String)localObject1).equals(localObject4);
        if (bool1) {
          m = 1;
        }
      }
      c localc;
      List localList2;
      long l4;
      int i3;
      int j;
      Object localObject6;
      switch (m)
      {
      default: 
        break;
      case 1: 
        localObject1 = "int";
        localObject2 = c.h;
        bool1 = ((String)localObject1).equals(localObject2);
        if (bool1)
        {
          localc = b;
          int i = localList1.size();
          localList2 = localList1.subList(0, i);
          l4 = c.d;
          i3 = c.j.a;
          localObject1 = c.h;
          localObject2 = c.m;
          localc.a(localList2, l4, i3, (String)localObject1, (InMobiAdRequest.MonetizationContext)localObject2, str1);
          j = d;
          if (j == 0)
          {
            localObject1 = a;
            localObject2 = c;
            l1 = d;
            ((h.a)localObject1).b(l1, (a)localObject5);
          }
        }
        else
        {
          localObject1 = "native";
          localObject2 = c.h;
          j = ((String)localObject1).equals(localObject2);
          if (j != 0)
          {
            localObject1 = b;
            l2 = c.d;
            i2 = c.j.a;
            String str2 = c.h;
            InMobiAdRequest.MonetizationContext localMonetizationContext = c.m;
            localObject2 = localList1;
            localObject6 = localObject5;
            localObject5 = str1;
            ((c)localObject1).a(localList1, l2, i2, str2, localMonetizationContext, str1);
            j = d;
            if (j == 0)
            {
              localObject4 = b;
              long l5 = c.d;
              str2 = c.f;
              localObject1 = c;
              localMonetizationContext = m;
              localObject5 = ((c)localObject4).b(l5, str2, localMonetizationContext, str1);
              if (localObject5 != null)
              {
                localObject1 = ((a)localObject6).c();
                localObject2 = ((a)localObject5).c();
                j = ((Set)localObject1).equals(localObject2);
                if (j == 0) {
                  localList1.add(0, localObject5);
                }
                localObject6 = localObject5;
              }
              localObject1 = a;
              localObject2 = c;
              l1 = d;
              ((h.a)localObject1).a(l1, (a)localObject6);
              d();
            }
          }
        }
        localh.a(localList1);
        return;
      case 0: 
        localObject6 = localObject5;
        localObject1 = "native";
        localObject2 = c.h;
        j = ((String)localObject1).equals(localObject2);
        if (j != 0)
        {
          j = d;
          if (j == 0)
          {
            localObject1 = a;
            l1 = c.d;
            localObject3 = new com/inmobi/ads/InMobiAdRequestStatus;
            localStatusCode = InMobiAdRequestStatus.StatusCode.INTERNAL_ERROR;
            ((InMobiAdRequestStatus)localObject3).<init>(localStatusCode);
            ((h.a)localObject1).a(l1, (InMobiAdRequestStatus)localObject3);
          }
        }
        else
        {
          j = d ^ i2;
          localc = b;
          m = localList1.size();
          localList2 = localList1.subList(j, m);
          l4 = c.d;
          localObject1 = c.j;
          int k = a;
          localObject2 = c.h;
          localObject4 = c.m;
          localObject3 = localObject5;
          i3 = k;
          localc.a(localList2, l4, k, (String)localObject2, (InMobiAdRequest.MonetizationContext)localObject4, str1);
          boolean bool2 = d;
          if (!bool2)
          {
            localObject1 = a;
            l1 = c.d;
            ((h.a)localObject1).a(l1, (a)localObject5);
            d();
            return;
          }
        }
        break;
      }
    }
  }
  
  final void a(String paramString)
  {
    h.3 local3 = new com/inmobi/ads/h$3;
    local3.<init>(this, paramString);
    local3.start();
  }
  
  final void a(List paramList)
  {
    if (paramList != null)
    {
      int i = paramList.size();
      if (i > 0)
      {
        i = 0;
        Object localObject1 = (a)paramList.get(0);
        int j = 1;
        Object localObject2;
        Object localObject3;
        String str;
        if (localObject1 != null)
        {
          localObject2 = ((a)localObject1).d();
          localObject3 = "inmobiJson";
          boolean bool3 = ((String)localObject2).equalsIgnoreCase((String)localObject3);
          if (bool3)
          {
            localObject2 = ((a)localObject1).c();
            int m = ((Set)localObject2).size();
            if (m == 0)
            {
              paramList = a;
              long l = c.d;
              paramList.a(l, j);
              return;
            }
            localObject3 = new com/inmobi/ads/cache/b;
            str = UUID.randomUUID().toString();
            localObject1 = f;
            com.inmobi.ads.cache.f localf = g;
            ((b)localObject3).<init>(str, (String)localObject1, (Set)localObject2, localf);
            localObject1 = AssetStore.a();
            ((AssetStore)localObject1).a((b)localObject3);
          }
        }
        i = paramList.size();
        paramList = paramList.subList(j, i).iterator();
        for (;;)
        {
          boolean bool1 = paramList.hasNext();
          if (!bool1) {
            break;
          }
          localObject1 = (a)paramList.next();
          if (localObject1 != null)
          {
            Object localObject4 = ((a)localObject1).d();
            localObject2 = "inmobiJson";
            boolean bool2 = ((String)localObject4).equalsIgnoreCase((String)localObject2);
            if (bool2)
            {
              localObject4 = ((a)localObject1).c();
              int k = ((Set)localObject4).size();
              if (k != 0)
              {
                localObject2 = new com/inmobi/ads/cache/b;
                localObject3 = UUID.randomUUID().toString();
                localObject1 = f;
                str = null;
                ((b)localObject2).<init>((String)localObject3, (String)localObject1, (Set)localObject4, null);
                localObject1 = AssetStore.a();
                ((AssetStore)localObject1).a((b)localObject2);
              }
            }
          }
        }
      }
    }
  }
  
  final void b()
  {
    Object localObject1 = c;
    if (localObject1 == null) {
      return;
    }
    localObject1 = h;
    Object localObject2 = c.j;
    long l = d;
    int i = c.a((String)localObject1, l);
    if (i > 0)
    {
      localObject2 = new java/util/HashMap;
      ((HashMap)localObject2).<init>();
      localObject1 = Integer.valueOf(i);
      ((Map)localObject2).put("count", localObject1);
      String str1 = com.inmobi.commons.a.a.e();
      ((Map)localObject2).put("im-accid", str1);
      localObject1 = a;
      str1 = "ads";
      String str2 = "AdCacheAdExpired";
      ((h.a)localObject1).a(str1, str2, (Map)localObject2);
    }
  }
  
  public final void b(f paramf)
  {
    boolean bool = d;
    if (!bool)
    {
      Object localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      int i = a.b.a.getValue();
      Object localObject2 = String.valueOf(i);
      ((Map)localObject1).put("errorCode", localObject2);
      localObject2 = a.b.b;
      ((Map)localObject1).put("reason", localObject2);
      long l1 = SystemClock.elapsedRealtime();
      long l2 = e;
      l1 -= l2;
      localObject2 = Long.valueOf(l1);
      ((Map)localObject1).put("latency", localObject2);
      localObject2 = com.inmobi.commons.a.a.e();
      ((Map)localObject1).put("im-accid", localObject2);
      Object localObject3 = a;
      localObject2 = "ads";
      String str = "ServerError";
      ((h.a)localObject3).a((String)localObject2, str, (Map)localObject1);
      localObject1 = a;
      localObject3 = c;
      long l3 = d;
      paramf = b;
      ((h.a)localObject1).a(l3, paramf);
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */