package com.inmobi.ads;

import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

final class x$3
  implements Runnable
{
  x$3(x paramx, long paramLong, InMobiAdRequestStatus paramInMobiAdRequestStatus) {}
  
  public final void run()
  {
    try
    {
      long l1 = a;
      localObject1 = c;
      long l2 = b;
      boolean bool = l1 < l2;
      if (!bool)
      {
        Object localObject2 = Logger.InternalLogLevel.DEBUG;
        localObject3 = x.L();
        localObject1 = new java/lang/StringBuilder;
        Object localObject4 = "Failed to fetch ad for placement id: ";
        ((StringBuilder)localObject1).<init>((String)localObject4);
        long l3 = a;
        ((StringBuilder)localObject1).append(l3);
        localObject4 = ", reason phrase available in onAdLoadFailed callback.";
        ((StringBuilder)localObject1).append((String)localObject4);
        localObject1 = ((StringBuilder)localObject1).toString();
        Logger.a((Logger.InternalLogLevel)localObject2, (String)localObject3, (String)localObject1);
        int i = 0;
        localObject2 = null;
        int k;
        for (;;)
        {
          localObject3 = c;
          localObject3 = x.a((x)localObject3);
          int j = ((ArrayList)localObject3).size();
          k = 1;
          if (i >= j) {
            break;
          }
          localObject3 = c;
          localObject3 = x.a((x)localObject3);
          localObject3 = ((ArrayList)localObject3).get(i);
          localObject3 = (WeakReference)localObject3;
          localObject3 = ((WeakReference)localObject3).get();
          localObject3 = (j.b)localObject3;
          if (localObject3 == null)
          {
            localObject3 = c;
            ((x)localObject3).g();
          }
          else
          {
            localObject4 = c;
            localObject4 = x.a((x)localObject4);
            int m = ((ArrayList)localObject4).size() - k;
            if (i < m)
            {
              localObject1 = c;
              localObject4 = "VAR";
              str = "";
              ((x)localObject1).a((j.b)localObject3, (String)localObject4, str);
            }
            localObject1 = c;
            localObject4 = "ARN";
            String str = "";
            ((x)localObject1).a((j.b)localObject3, (String)localObject4, str);
          }
          i += 1;
        }
        localObject2 = c;
        localObject3 = b;
        ((x)localObject2).a((InMobiAdRequestStatus)localObject3, k);
      }
      return;
    }
    catch (Exception localException)
    {
      Logger.a(Logger.InternalLogLevel.ERROR, "[InMobi]", "Unable to load Ad; SDK encountered an unexpected error");
      x.K();
      localException.getMessage();
      Object localObject3 = com.inmobi.commons.core.a.a.a();
      Object localObject1 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject1).<init>(localException);
      ((com.inmobi.commons.core.a.a)localObject3).a((com.inmobi.commons.core.e.a)localObject1);
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.x.3
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */