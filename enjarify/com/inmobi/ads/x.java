package com.inmobi.ads;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.RecyclerView;
import com.d.b.w;
import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;
import com.inmobi.commons.core.utilities.b.e;
import com.inmobi.rendering.InMobiAdActivity;
import com.inmobi.rendering.RenderView;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;

class x
  extends j
{
  private static final String y = "x";
  private static final String z = InMobiInterstitial.class.getSimpleName();
  private int A = 0;
  private ArrayList B;
  boolean x = false;
  
  private x(Context paramContext, long paramLong, j.b paramb)
  {
    super(paramContext, paramLong, paramb);
    paramContext = new java/util/ArrayList;
    paramContext.<init>(1);
    B = paramContext;
  }
  
  private boolean M()
  {
    boolean bool1 = true;
    String str1 = "html";
    try
    {
      String str2 = l;
      boolean bool2 = str1.equals(str2);
      str2 = null;
      if (bool2)
      {
        bool2 = h();
        if (bool2)
        {
          super.r();
          return bool1;
        }
        N();
        return false;
      }
      b(bool1);
      N();
      return false;
    }
    catch (x.c localc)
    {
      return bool1;
    }
    catch (x.b localb) {}
    return bool1;
  }
  
  private void N()
  {
    Handler localHandler = r;
    x.1 local1 = new com/inmobi/ads/x$1;
    local1.<init>(this);
    localHandler.post(local1);
  }
  
  private boolean O()
  {
    Object localObject1 = InMobiAdActivity.class;
    try
    {
      ((Class)localObject1).getSimpleName();
      localObject1 = i();
      if (localObject1 != null)
      {
        localObject2 = "unknown";
        localObject3 = ((AdContainer)localObject1).getMarkupType();
        boolean bool1 = ((String)localObject2).equals(localObject3);
        if (!bool1)
        {
          int i = InMobiAdActivity.a((AdContainer)localObject1);
          localObject2 = new android/content/Intent;
          localObject3 = a();
          Object localObject4 = InMobiAdActivity.class;
          ((Intent)localObject2).<init>((Context)localObject3, (Class)localObject4);
          localObject3 = "com.inmobi.rendering.InMobiAdActivity.EXTRA_AD_CONTAINER_INDEX";
          ((Intent)localObject2).putExtra((String)localObject3, i);
          localObject1 = "com.inmobi.rendering.InMobiAdActivity.EXTRA_AD_ACTIVITY_TYPE";
          int j = 102;
          ((Intent)localObject2).putExtra((String)localObject1, j);
          localObject1 = "com.inmobi.rendering.InMobiAdActivity.EXTRA_AD_CONTAINER_TYPE";
          localObject3 = "html";
          localObject4 = l;
          boolean bool2 = ((String)localObject3).equals(localObject4);
          if (bool2) {
            k = 200;
          } else {
            k = 201;
          }
          ((Intent)localObject2).putExtra((String)localObject1, k);
          localObject1 = "com.inmobi.rendering.InMobiAdActivity.EXTRA_AD_ACTIVITY_IS_FULL_SCREEN";
          int k = 1;
          ((Intent)localObject2).putExtra((String)localObject1, k);
          localObject1 = a();
          com.inmobi.commons.a.a.a((Context)localObject1, (Intent)localObject2);
          return k;
        }
      }
      return false;
    }
    catch (Exception localException)
    {
      Object localObject2 = Logger.InternalLogLevel.ERROR;
      Object localObject3 = InMobiInterstitial.class.getSimpleName();
      Logger.a((Logger.InternalLogLevel)localObject2, (String)localObject3, "Cannot show ad; SDK encountered an unexpected error");
      localException.getMessage();
      localObject2 = com.inmobi.commons.core.a.a.a();
      localObject3 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject3).<init>(localException);
      ((com.inmobi.commons.core.a.a)localObject2).a((com.inmobi.commons.core.e.a)localObject3);
    }
    return false;
  }
  
  private boolean b(boolean paramBoolean)
  {
    Object localObject1 = d;
    String str = g.a((Map)localObject1);
    long l;
    Object localObject3;
    Object localObject4;
    if (paramBoolean)
    {
      localObject2 = h;
      l = b;
      localObject1 = e;
      ((b)localObject1).a("int");
      InMobiAdRequest.MonetizationContext localMonetizationContext = InMobiAdRequest.MonetizationContext.MONETIZATION_CONTEXT_ACTIVITY;
      ((h)localObject2).b();
      localObject3 = b;
      localObject4 = str;
      localObject2 = ((c)localObject3).c(l, null, localMonetizationContext, str);
      int i = ((List)localObject2).size();
      if (i <= 0)
      {
        paramBoolean = false;
        localObject2 = null;
      }
      else
      {
        i = 0;
        localObject1 = null;
        localObject2 = (a)((List)localObject2).get(0);
      }
    }
    else
    {
      localObject3 = h;
      l = b;
      localObject2 = e;
      localObject1 = "int";
      int j = ac;
      localObject4 = InMobiAdRequest.MonetizationContext.MONETIZATION_CONTEXT_ACTIVITY;
      localObject2 = ((h)localObject3).a(l, null, j, (InMobiAdRequest.MonetizationContext)localObject4, str);
    }
    if (localObject2 != null)
    {
      paramBoolean = a((a)localObject2);
      if (paramBoolean) {
        return true;
      }
      localObject2 = new com/inmobi/ads/x$c;
      ((x.c)localObject2).<init>(this, "No Cached Asset for AdUnit");
      throw ((Throwable)localObject2);
    }
    Object localObject2 = new com/inmobi/ads/x$b;
    ((x.b)localObject2).<init>(this, "No Cached Ad found for AdUnit");
    throw ((Throwable)localObject2);
  }
  
  private int f(j.b paramb)
  {
    int i = -1;
    int j = 0;
    for (;;)
    {
      Object localObject = B;
      int k = ((ArrayList)localObject).size();
      if (j >= k) {
        break;
      }
      localObject = (WeakReference)B.get(j);
      if (localObject != null)
      {
        localObject = (j.b)((WeakReference)localObject).get();
        if (localObject != null)
        {
          boolean bool = localObject.equals(paramb);
          if (bool) {
            i = j;
          }
        }
      }
      j += 1;
    }
    return i;
  }
  
  private void g(j.b paramb)
  {
    String str = "ShowInt";
    d(str);
    boolean bool = O();
    if (paramb == null) {
      g();
    }
    if (!bool)
    {
      a = 3;
      a(paramb, "AVRR", "");
      paramb.b();
      return;
    }
    paramb.c();
  }
  
  private void h(j.b paramb)
  {
    a(paramb, "AVFB", "");
    Handler localHandler = r;
    x.4 local4 = new com/inmobi/ads/x$4;
    local4.<init>(this, paramb);
    localHandler.post(local4);
  }
  
  public final void A()
  {
    b("RenderTimeOut");
    Object localObject1 = this.j;
    Object localObject2;
    if (localObject1 != null)
    {
      localObject1 = h;
      localObject2 = this.j;
      ((h)localObject1).a((String)localObject2);
    }
    int i = 4;
    int j = a;
    if (i != j)
    {
      i = 2;
      j = a;
      if (i != j) {}
    }
    else
    {
      i = 3;
      a = i;
      localObject1 = Logger.InternalLogLevel.DEBUG;
      localObject2 = y;
      Object localObject3 = new java/lang/StringBuilder;
      String str = "Failed to load the Interstitial markup in the webview due to time out for placement id: ";
      ((StringBuilder)localObject3).<init>(str);
      long l = b;
      ((StringBuilder)localObject3).append(l);
      localObject3 = ((StringBuilder)localObject3).toString();
      Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, (String)localObject3);
      localObject1 = new com/inmobi/ads/InMobiAdRequestStatus;
      localObject2 = InMobiAdRequestStatus.StatusCode.INTERNAL_ERROR;
      ((InMobiAdRequestStatus)localObject1).<init>((InMobiAdRequestStatus.StatusCode)localObject2);
      j = 0;
      localObject2 = null;
      a((InMobiAdRequestStatus)localObject1, false);
    }
  }
  
  public final void C()
  {
    int i = a;
    int j = 1;
    if (j == i)
    {
      i = 9;
      a = i;
      Object localObject = p;
      if (localObject != null)
      {
        localObject = p;
        ((j.d)localObject).a(this);
      }
      localObject = B.iterator();
      for (;;)
      {
        boolean bool = ((Iterator)localObject).hasNext();
        if (!bool) {
          break;
        }
        j.b localb = (j.b)((WeakReference)((Iterator)localObject).next()).get();
        if (localb != null)
        {
          d(localb);
          return;
        }
        g();
      }
    }
  }
  
  final void F()
  {
    B();
    a = 5;
    int i = 0;
    for (;;)
    {
      Object localObject1 = B;
      int j = ((ArrayList)localObject1).size();
      if (i >= j) {
        break;
      }
      localObject1 = (j.b)((WeakReference)B.get(i)).get();
      if (localObject1 == null)
      {
        g();
      }
      else
      {
        Object localObject2 = B;
        int k = ((ArrayList)localObject2).size() + -1;
        if (i < k)
        {
          a((j.b)localObject1, "VAR", "");
          localObject2 = "ARF";
          String str = "";
          a((j.b)localObject1, (String)localObject2, str);
        }
        ((j.b)localObject1).a(this);
      }
      i += 1;
    }
    B.clear();
  }
  
  final void G()
  {
    Iterator localIterator = B.iterator();
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      j.b localb = (j.b)((WeakReference)localIterator.next()).get();
      if (localb != null)
      {
        boolean bool2 = true;
        localb.a(bool2);
      }
      else
      {
        g();
      }
    }
  }
  
  public final boolean J()
  {
    int i = a;
    int j = 5;
    return i == j;
  }
  
  public final void a(long paramLong, InMobiAdRequestStatus paramInMobiAdRequestStatus)
  {
    Handler localHandler = r;
    x.3 local3 = new com/inmobi/ads/x$3;
    local3.<init>(this, paramLong, paramInMobiAdRequestStatus);
    localHandler.post(local3);
  }
  
  public final void a(long paramLong, boolean paramBoolean, a parama)
  {
    try
    {
      super.a(paramLong, paramBoolean, parama);
      long l = b;
      boolean bool1 = paramLong < l;
      if (!bool1)
      {
        int i = a;
        boolean bool5 = false;
        localObject1 = null;
        int k = 2;
        int m = 1;
        if ((m == i) && (paramBoolean))
        {
          a = k;
          boolean bool2 = super.a(parama);
          if (bool2)
          {
            localObject2 = f();
            localObject1 = "ARF";
            localObject3 = "";
            a((j.b)localObject2, (String)localObject1, (String)localObject3);
            c(parama);
            bool2 = h;
            if (bool2)
            {
              s = m;
              D();
              return;
            }
            localObject2 = B;
            localObject2 = ((ArrayList)localObject2).iterator();
            for (;;)
            {
              bool5 = ((Iterator)localObject2).hasNext();
              if (!bool5) {
                break;
              }
              localObject1 = ((Iterator)localObject2).next();
              localObject1 = (WeakReference)localObject1;
              localObject1 = ((WeakReference)localObject1).get();
              localObject1 = (j.b)localObject1;
              if (localObject1 != null) {
                ((j.b)localObject1).a(m);
              } else {
                g();
              }
            }
            return;
          }
          localObject2 = B;
          localObject2 = ((ArrayList)localObject2).iterator();
          for (;;)
          {
            paramBoolean = ((Iterator)localObject2).hasNext();
            if (!paramBoolean) {
              break;
            }
            localObject3 = ((Iterator)localObject2).next();
            localObject3 = (WeakReference)localObject3;
            localObject3 = ((WeakReference)localObject3).get();
            localObject3 = (j.b)localObject3;
            if (localObject3 != null) {
              ((j.b)localObject3).a(false);
            } else {
              g();
            }
          }
          return;
        }
        boolean bool3 = true;
        paramBoolean = a;
        if (bool3 != paramBoolean)
        {
          boolean bool4 = true;
          paramBoolean = a;
          if (bool4 != paramBoolean)
          {
            int j = a;
            if (k != j) {
              break label444;
            }
          }
        }
        a = 0;
        Object localObject2 = B;
        localObject2 = ((ArrayList)localObject2).iterator();
        for (;;)
        {
          bool5 = ((Iterator)localObject2).hasNext();
          if (!bool5) {
            break;
          }
          localObject1 = ((Iterator)localObject2).next();
          localObject1 = (WeakReference)localObject1;
          localObject1 = ((WeakReference)localObject1).get();
          localObject1 = (j.b)localObject1;
          if (localObject1 != null)
          {
            localObject3 = new com/inmobi/ads/InMobiAdRequestStatus;
            parama = InMobiAdRequestStatus.StatusCode.AD_NO_LONGER_AVAILABLE;
            ((InMobiAdRequestStatus)localObject3).<init>(parama);
            ((j.b)localObject1).a((InMobiAdRequestStatus)localObject3);
          }
          else
          {
            g();
          }
          localObject1 = B;
          ((ArrayList)localObject1).clear();
        }
      }
      label444:
      return;
    }
    catch (Exception localException)
    {
      Object localObject1 = Logger.InternalLogLevel.ERROR;
      Object localObject3 = InMobiInterstitial.class.getSimpleName();
      Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject3, "Unable to load ad; SDK encountered an internal error");
      localException.getMessage();
    }
  }
  
  public final void a(InMobiAdRequest.MonetizationContext paramMonetizationContext)
  {
    paramMonetizationContext = InMobiAdRequest.MonetizationContext.MONETIZATION_CONTEXT_ACTIVITY;
    super.a(paramMonetizationContext);
  }
  
  final void a(InMobiAdRequestStatus paramInMobiAdRequestStatus, boolean paramBoolean)
  {
    int i = a;
    int j = 1;
    if ((i == j) && (paramBoolean))
    {
      paramBoolean = true;
      a = paramBoolean;
    }
    Iterator localIterator = B.iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      j.b localb = (j.b)((WeakReference)localIterator.next()).get();
      if (localb != null) {
        localb.a(paramInMobiAdRequestStatus);
      } else {
        g();
      }
    }
    B.clear();
    a(paramInMobiAdRequestStatus);
    super.r();
  }
  
  public final void a(RenderView paramRenderView)
  {
    super.a(paramRenderView);
    int i = a;
    int j = 2;
    if (i == j)
    {
      i = 4;
      a = i;
      G();
    }
  }
  
  public final boolean a(a parama)
  {
    boolean bool1 = super.a(parama);
    bt localbt = null;
    if (!bool1)
    {
      b(parama);
      return false;
    }
    bool1 = parama instanceof az;
    if (bool1)
    {
      parama = (az)parama;
      com.inmobi.ads.cache.d.a();
      com.inmobi.ads.cache.a locala = com.inmobi.ads.cache.d.b(i);
      if (locala != null)
      {
        boolean bool2 = locala.a();
        if (bool2)
        {
          localbt = new com/inmobi/ads/bt;
          String str1 = e;
          String str2 = j;
          String str3 = k;
          List localList1 = parama.f();
          List localList2 = parama.g();
          parama = e;
          b.j localj = q;
          localbt.<init>(str1, str2, str3, localList1, localList2, localj);
          i = localbt;
          break label138;
        }
      }
      return false;
    }
    label138:
    return true;
  }
  
  protected final String b()
  {
    return "int";
  }
  
  public final void b(long paramLong, boolean paramBoolean)
  {
    super.b(paramLong, paramBoolean);
    int i = 2;
    long l;
    int j;
    if (!paramBoolean)
    {
      l = b;
      paramBoolean = paramLong < l;
      if (!paramBoolean)
      {
        j = a;
        if (i != j)
        {
          j = 5;
          int k = a;
          if (j != k) {}
        }
        else
        {
          a = 0;
          InMobiAdRequestStatus localInMobiAdRequestStatus = new com/inmobi/ads/InMobiAdRequestStatus;
          InMobiAdRequestStatus.StatusCode localStatusCode = InMobiAdRequestStatus.StatusCode.AD_NO_LONGER_AVAILABLE;
          localInMobiAdRequestStatus.<init>(localStatusCode);
          a(localInMobiAdRequestStatus, false);
        }
      }
    }
    else
    {
      l = b;
      paramBoolean = paramLong < l;
      if (!paramBoolean)
      {
        j = a;
        if (i == j)
        {
          boolean bool = s;
          if (bool)
          {
            u = true;
            E();
            return;
          }
          F();
        }
      }
    }
  }
  
  public final void b(InMobiAdRequestStatus paramInMobiAdRequestStatus)
  {
    int i = a;
    int j = 1;
    if (j == i)
    {
      i = 3;
      a = i;
      Object localObject = p;
      if (localObject != null)
      {
        localObject = p;
        ((j.d)localObject).a(this, paramInMobiAdRequestStatus);
      }
      localObject = B;
      i = ((ArrayList)localObject).size();
      if (i > 0)
      {
        i = 0;
        localObject = null;
        a(paramInMobiAdRequestStatus, false);
      }
    }
  }
  
  protected final void b(a parama)
  {
    h.a(parama);
  }
  
  final void b(j.b paramb)
  {
    int i = a;
    int j = 8;
    int k = 1;
    int m = 7;
    if (i == m)
    {
      i = A + k;
      A = i;
      i = A;
      if (i == k)
      {
        d("AdRendered");
        Logger.InternalLogLevel localInternalLogLevel = Logger.InternalLogLevel.DEBUG;
        String str1 = z;
        Object localObject = new java/lang/StringBuilder;
        String str2 = "Successfully displayed Interstitial for placement id: ";
        ((StringBuilder)localObject).<init>(str2);
        long l = b;
        ((StringBuilder)localObject).append(l);
        localObject = ((StringBuilder)localObject).toString();
        Logger.a(localInternalLogLevel, str1, (String)localObject);
        if (paramb != null)
        {
          paramb.d();
          return;
        }
        g();
        return;
      }
      a = j;
      return;
    }
    int n = a;
    if (n == j)
    {
      n = A + k;
      A = n;
    }
  }
  
  protected final String c()
  {
    return null;
  }
  
  public final void c(long paramLong, a parama)
  {
    try
    {
      super.c(paramLong, parama);
      parama = Logger.InternalLogLevel.DEBUG;
      String str1 = z;
      Object localObject1 = new java/lang/StringBuilder;
      String str2 = "Interstitial ad successfully fetched for placement id: ";
      ((StringBuilder)localObject1).<init>(str2);
      long l1 = b;
      ((StringBuilder)localObject1).append(l1);
      localObject1 = ((StringBuilder)localObject1).toString();
      Logger.a(parama, str1, (String)localObject1);
      long l2 = b;
      boolean bool = paramLong < l2;
      if (!bool)
      {
        int i = a;
        int j = 2;
        if (i == j)
        {
          i = 1;
          localObject2 = j();
          a(i, (RenderView)localObject2);
          try
          {
            String str3 = f;
            int k = 0;
            localObject2 = null;
            a(null, str3, null, null);
            return;
          }
          catch (Exception localException1)
          {
            y();
            localObject2 = new com/inmobi/ads/InMobiAdRequestStatus;
            parama = InMobiAdRequestStatus.StatusCode.INTERNAL_ERROR;
            ((InMobiAdRequestStatus)localObject2).<init>(parama);
            bool = false;
            parama = null;
            a((InMobiAdRequestStatus)localObject2, false);
            localObject2 = Logger.InternalLogLevel.ERROR;
            parama = InMobiInterstitial.class;
            parama = parama.getSimpleName();
            str1 = "Unable to load ad; SDK encountered an internal error";
            Logger.a((Logger.InternalLogLevel)localObject2, parama, str1);
            localException1.getMessage();
            localObject2 = com.inmobi.commons.core.a.a.a();
            parama = new com/inmobi/commons/core/e/a;
            parama.<init>(localException1);
            ((com.inmobi.commons.core.a.a)localObject2).a(parama);
          }
        }
      }
      return;
    }
    catch (Exception localException2)
    {
      Object localObject2 = Logger.InternalLogLevel.ERROR;
      parama = z;
      Logger.a((Logger.InternalLogLevel)localObject2, parama, "Unable to load ad; SDK encountered an internal error");
      localException2.getMessage();
      localObject2 = com.inmobi.commons.core.a.a.a();
      parama = new com/inmobi/commons/core/e/a;
      parama.<init>(localException2);
      ((com.inmobi.commons.core.a.a)localObject2).a(parama);
    }
  }
  
  final void c(j.b paramb)
  {
    int i = a;
    int j = 7;
    int k = 1;
    int m = 8;
    if (i == m)
    {
      int n = A - k;
      A = n;
      n = A;
      if (n == k) {
        a = j;
      }
    }
    else
    {
      i = a;
      if (i == j)
      {
        i = A - k;
        A = i;
        d("IntClosed");
        super.r();
        Logger.InternalLogLevel localInternalLogLevel = Logger.InternalLogLevel.DEBUG;
        String str1 = z;
        Object localObject = new java/lang/StringBuilder;
        String str2 = "Interstitial ad dismissed for placement id: ";
        ((StringBuilder)localObject).<init>(str2);
        long l = b;
        ((StringBuilder)localObject).append(l);
        localObject = ((StringBuilder)localObject).toString();
        Logger.a(localInternalLogLevel, str1, (String)localObject);
        if (paramb != null)
        {
          paramb.e();
          return;
        }
        g();
      }
    }
  }
  
  public final void c(RenderView paramRenderView)
  {
    try
    {
      super.c(paramRenderView);
      paramRenderView = f();
      b(paramRenderView);
      return;
    }
    finally {}
  }
  
  protected final AdContainer.RenderingProperties.PlacementType d()
  {
    return AdContainer.RenderingProperties.PlacementType.PLACEMENT_TYPE_FULLSCREEN;
  }
  
  public final void d(j.b paramb)
  {
    boolean bool1 = true;
    Object localObject1 = RecyclerView.class;
    try
    {
      ((Class)localObject1).getName();
      localObject1 = w.class;
      ((Class)localObject1).getName();
      boolean bool2 = false;
      localObject1 = null;
      v = false;
      if (paramb == null)
      {
        g();
        return;
      }
      int i = f(paramb);
      int k = -1;
      if (k == i)
      {
        Object localObject2 = B;
        Object localObject3 = new java/lang/ref/WeakReference;
        ((WeakReference)localObject3).<init>(paramb);
        ((ArrayList)localObject2).add(localObject3);
        boolean bool3 = com.inmobi.commons.core.utilities.d.a();
        if (!bool3)
        {
          paramb = new com/inmobi/ads/InMobiAdRequestStatus;
          localObject1 = InMobiAdRequestStatus.StatusCode.NETWORK_UNREACHABLE;
          paramb.<init>((InMobiAdRequestStatus.StatusCode)localObject1);
          a(paramb, bool1);
          return;
        }
        int j = a;
        Object localObject4;
        long l2;
        switch (j)
        {
        case 3: 
        case 5: 
        case 6: 
        default: 
          bool1 = false;
          break;
        case 7: 
        case 8: 
          localObject1 = Logger.InternalLogLevel.ERROR;
          localObject2 = z;
          localObject3 = new java/lang/StringBuilder;
          String str = "An ad is currently being viewed by the user. Please wait for the user to close the ad before requesting for another ad for placement id: ";
          ((StringBuilder)localObject3).<init>(str);
          long l1 = b;
          ((StringBuilder)localObject3).append(l1);
          localObject3 = ((StringBuilder)localObject3).toString();
          Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, (String)localObject3);
          localObject1 = new com/inmobi/ads/InMobiAdRequestStatus;
          localObject2 = InMobiAdRequestStatus.StatusCode.AD_ACTIVE;
          ((InMobiAdRequestStatus)localObject1).<init>((InMobiAdRequestStatus.StatusCode)localObject2);
          a((InMobiAdRequestStatus)localObject1);
          j = f(paramb);
          if (j != k)
          {
            localObject4 = B;
            ((ArrayList)localObject4).remove(j);
          }
          if (paramb != null) {
            paramb.a((InMobiAdRequestStatus)localObject1);
          }
          break;
        case 4: 
          if (paramb != null) {
            paramb.a(bool1);
          }
          break;
        case 2: 
          localObject1 = "html";
          localObject2 = l;
          bool2 = ((String)localObject1).equals(localObject2);
          if (bool2)
          {
            paramb = Logger.InternalLogLevel.ERROR;
            localObject1 = z;
            localObject2 = new java/lang/StringBuilder;
            localObject4 = "An ad load is already in progress. Please wait for the load to complete before requesting for another ad for placement id: ";
            ((StringBuilder)localObject2).<init>((String)localObject4);
            l2 = b;
            ((StringBuilder)localObject2).append(l2);
            localObject2 = ((StringBuilder)localObject2).toString();
            Logger.a(paramb, (String)localObject1, (String)localObject2);
          }
          else if (paramb != null)
          {
            paramb.a(bool1);
          }
          break;
        case 1: 
          paramb = Logger.InternalLogLevel.ERROR;
          localObject1 = z;
          localObject2 = new java/lang/StringBuilder;
          localObject4 = "An ad load is already in progress. Please wait for the load to complete before requesting for another ad for placement id: ";
          ((StringBuilder)localObject2).<init>((String)localObject4);
          l2 = b;
          ((StringBuilder)localObject2).append(l2);
          localObject2 = ((StringBuilder)localObject2).toString();
          Logger.a(paramb, (String)localObject1, (String)localObject2);
        }
        if (bool1)
        {
          d("AdLoadRequested");
          return;
        }
        super.l();
        return;
      }
      a(paramb, "ART", "LoadInProgress");
      return;
    }
    catch (NoClassDefFoundError localNoClassDefFoundError)
    {
      a("MissingDependency");
      paramb = new com/inmobi/ads/InMobiAdRequestStatus;
      localObject1 = InMobiAdRequestStatus.StatusCode.MISSING_REQUIRED_DEPENDENCIES;
      paramb.<init>((InMobiAdRequestStatus.StatusCode)localObject1);
      a(paramb, bool1);
    }
  }
  
  public final void d(RenderView paramRenderView)
  {
    try
    {
      super.d(paramRenderView);
      paramRenderView = f();
      c(paramRenderView);
      return;
    }
    finally {}
  }
  
  protected final Map e()
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    String str1 = "preload-request";
    boolean bool = m;
    String str2;
    if (bool) {
      str2 = "1";
    } else {
      str2 = "0";
    }
    localHashMap.put(str1, str2);
    return localHashMap;
  }
  
  final void e(j.b paramb)
  {
    Object localObject1 = Looper.myLooper();
    Object localObject2 = Looper.getMainLooper();
    if (localObject1 == localObject2)
    {
      if (paramb == null)
      {
        g();
        return;
      }
      boolean bool1 = J();
      if (!bool1)
      {
        a(paramb, "AVRR", "");
        localObject1 = Logger.InternalLogLevel.ERROR;
        localObject2 = y;
        Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "Ad Load is not complete. Please wait for the Ad to be in a ready state before calling show.");
        localObject2 = new java/util/HashMap;
        ((HashMap)localObject2).<init>();
        ((Map)localObject2).put("errorCode", "ShowIntBeforeReady");
        c("ads", "AdShowFailed", (Map)localObject2);
        paramb.b();
        return;
      }
      bool1 = e.e();
      if (!bool1)
      {
        super.r();
        paramb.b();
        return;
      }
      a(paramb);
      a = 7;
      localObject1 = "html";
      localObject2 = l;
      bool1 = ((String)localObject1).equals(localObject2);
      if (bool1)
      {
        localObject1 = i();
        boolean bool2 = h();
        if (bool2)
        {
          h(paramb);
          if (localObject1 != null) {
            ((AdContainer)localObject1).destroy();
          }
        }
        else
        {
          g(paramb);
        }
        return;
      }
      localObject1 = new java/lang/ref/WeakReference;
      ((WeakReference)localObject1).<init>(paramb);
      paramb = o;
      localObject2 = new com/inmobi/ads/x$2;
      ((x.2)localObject2).<init>(this, (WeakReference)localObject1);
      paramb.execute((Runnable)localObject2);
      return;
    }
    a(paramb, "AVRR", "");
    paramb = Logger.InternalLogLevel.ERROR;
    localObject1 = InMobiInterstitial.class.getSimpleName();
    Logger.a(paramb, (String)localObject1, "Please ensure that you call show() on the UI thread");
  }
  
  protected final RenderView j()
  {
    RenderView localRenderView = super.j();
    boolean bool = x;
    if (bool) {
      localRenderView.a();
    }
    return localRenderView;
  }
  
  public final InMobiAdRequest.MonetizationContext k()
  {
    return InMobiAdRequest.MonetizationContext.MONETIZATION_CONTEXT_ACTIVITY;
  }
  
  public final void l() {}
  
  final void n()
  {
    Handler localHandler = r;
    x.5 local5 = new com/inmobi/ads/x$5;
    local5.<init>(this);
    localHandler.post(local5);
  }
  
  protected final int o()
  {
    int i = a;
    int j = 1;
    if (j == i)
    {
      Logger.InternalLogLevel localInternalLogLevel = Logger.InternalLogLevel.ERROR;
      String str = z;
      Object localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>("An ad load is already in progress. Please wait for the load to complete before requesting for another ad for placement id: ");
      long l = b;
      ((StringBuilder)localObject).append(l);
      localObject = ((StringBuilder)localObject).toString();
      Logger.a(localInternalLogLevel, str, (String)localObject);
      return 2;
    }
    i = 5;
    int k = a;
    if (i == k)
    {
      boolean bool = M();
      if (bool) {
        return super.o();
      }
      return j;
    }
    return super.o();
  }
  
  protected final boolean p()
  {
    return false;
  }
  
  protected final void r()
  {
    super.r();
  }
  
  public final void t()
  {
    super.t();
    int i = a;
    int j = 4;
    if (i == j)
    {
      y();
      Logger.InternalLogLevel localInternalLogLevel = Logger.InternalLogLevel.DEBUG;
      String str1 = z;
      Object localObject = new java/lang/StringBuilder;
      String str2 = "Successfully loaded Interstitial ad markup in the WebView for placement id: ";
      ((StringBuilder)localObject).<init>(str2);
      long l = b;
      ((StringBuilder)localObject).append(l);
      localObject = ((StringBuilder)localObject).toString();
      Logger.a(localInternalLogLevel, str1, (String)localObject);
      q();
      F();
    }
  }
  
  public final void v()
  {
    super.v();
    int i = a;
    int j = 4;
    if (i == j)
    {
      y();
      i = 3;
      a = i;
      Object localObject1 = Logger.InternalLogLevel.DEBUG;
      Object localObject2 = z;
      Object localObject3 = new java/lang/StringBuilder;
      String str = "Failed to load the Interstitial markup in the WebView for placement id: ";
      ((StringBuilder)localObject3).<init>(str);
      long l = b;
      ((StringBuilder)localObject3).append(l);
      localObject3 = ((StringBuilder)localObject3).toString();
      Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, (String)localObject3);
      localObject1 = new com/inmobi/ads/InMobiAdRequestStatus;
      localObject2 = InMobiAdRequestStatus.StatusCode.INTERNAL_ERROR;
      ((InMobiAdRequestStatus)localObject1).<init>((InMobiAdRequestStatus.StatusCode)localObject2);
      j = 0;
      localObject2 = null;
      a((InMobiAdRequestStatus)localObject1, false);
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.x
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */