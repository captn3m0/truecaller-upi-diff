package com.inmobi.ads;

import android.content.Context;
import android.support.v7.widget.RecyclerView.Adapter;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import java.lang.ref.WeakReference;

public class NativeRecyclerViewAdapter
  extends RecyclerView.Adapter
  implements au
{
  private static final String a = "NativeRecyclerViewAdapter";
  private final ak b;
  private aq c;
  private SparseArray d;
  private boolean e = false;
  
  NativeRecyclerViewAdapter(ak paramak, aq paramaq)
  {
    b = paramak;
    c = paramaq;
    paramak = new android/util/SparseArray;
    paramak.<init>();
    d = paramak;
  }
  
  public ViewGroup buildScrollableView(int paramInt, ViewGroup paramViewGroup, ai paramai)
  {
    ViewGroup localViewGroup = c.a(paramViewGroup, paramai);
    c.b(localViewGroup, paramai);
    paramViewGroup = NativeViewFactory.a(paramai, paramViewGroup);
    localViewGroup.setLayoutParams(paramViewGroup);
    return localViewGroup;
  }
  
  public void destroy()
  {
    e = true;
  }
  
  public int getItemCount()
  {
    return b.b();
  }
  
  public void onBindViewHolder(NativeRecyclerViewAdapter.a parama, int paramInt)
  {
    Object localObject1 = b.a(paramInt);
    Object localObject2 = (WeakReference)d.get(paramInt);
    if (localObject2 != null)
    {
      localObject2 = (View)((WeakReference)localObject2).get();
      if (localObject2 != null) {}
    }
    else
    {
      localObject2 = NativeRecyclerViewAdapter.a.a(parama);
      localObject2 = buildScrollableView(paramInt, (ViewGroup)localObject2, (ai)localObject1);
    }
    if (localObject2 != null)
    {
      int i = getItemCount() + -1;
      if (paramInt != i)
      {
        localObject1 = NativeRecyclerViewAdapter.a.a(parama);
        int j = 16;
        ((ViewGroup)localObject1).setPadding(0, 0, j, 0);
      }
      NativeRecyclerViewAdapter.a.a(parama).addView((View)localObject2);
      parama = d;
      localObject1 = new java/lang/ref/WeakReference;
      ((WeakReference)localObject1).<init>(localObject2);
      parama.put(paramInt, localObject1);
    }
  }
  
  public NativeRecyclerViewAdapter.a onCreateViewHolder(ViewGroup paramViewGroup, int paramInt)
  {
    paramViewGroup = new android/widget/FrameLayout;
    Object localObject = c.a();
    paramViewGroup.<init>((Context)localObject);
    localObject = new com/inmobi/ads/NativeRecyclerViewAdapter$a;
    ((NativeRecyclerViewAdapter.a)localObject).<init>(this, paramViewGroup);
    return (NativeRecyclerViewAdapter.a)localObject;
  }
  
  public void onViewRecycled(NativeRecyclerViewAdapter.a parama)
  {
    NativeRecyclerViewAdapter.a.a(parama).removeAllViews();
    super.onViewRecycled(parama);
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.NativeRecyclerViewAdapter
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */