package com.inmobi.ads;

public abstract interface InMobiNative$NativeAdListener
{
  public abstract void onAdClicked(InMobiNative paramInMobiNative);
  
  public abstract void onAdFullScreenDismissed(InMobiNative paramInMobiNative);
  
  public abstract void onAdFullScreenDisplayed(InMobiNative paramInMobiNative);
  
  public abstract void onAdFullScreenWillDisplay(InMobiNative paramInMobiNative);
  
  public abstract void onAdImpressed(InMobiNative paramInMobiNative);
  
  public abstract void onAdLoadFailed(InMobiNative paramInMobiNative, InMobiAdRequestStatus paramInMobiAdRequestStatus);
  
  public abstract void onAdLoadSucceeded(InMobiNative paramInMobiNative);
  
  public abstract void onAdStatusChanged(InMobiNative paramInMobiNative);
  
  public abstract void onMediaPlaybackComplete(InMobiNative paramInMobiNative);
  
  public abstract void onUserSkippedMedia(InMobiNative paramInMobiNative);
  
  public abstract void onUserWillLeaveApplication(InMobiNative paramInMobiNative);
}

/* Location:
 * Qualified Name:     com.inmobi.ads.InMobiNative.NativeAdListener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */