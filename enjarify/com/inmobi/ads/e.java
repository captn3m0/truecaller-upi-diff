package com.inmobi.ads;

import android.net.Uri;
import com.inmobi.commons.core.utilities.b.g;
import com.inmobi.commons.core.utilities.uid.d;
import com.inmobi.signals.b.b;
import com.inmobi.signals.n;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;

public final class e
  extends com.inmobi.commons.core.network.c
{
  private static final String y = "e";
  long a;
  String b;
  String c;
  int d;
  String e;
  String f;
  Map g;
  Map h;
  final String i;
  InMobiAdRequest.MonetizationContext j;
  
  public e(String paramString1, long paramLong, d paramd, String paramString2)
  {
    super(str, paramString1, bool1, paramd, bool2, 0);
    paramString1 = "json";
    b = paramString1;
    int k = 1;
    d = k;
    a = paramLong;
    Object localObject2 = this.m;
    long l1 = a;
    paramd = String.valueOf(l1);
    ((Map)localObject2).put("im-plid", paramd);
    localObject2 = this.m;
    Object localObject3 = g.d();
    ((Map)localObject2).putAll((Map)localObject3);
    localObject2 = this.m;
    localObject3 = com.inmobi.commons.core.utilities.b.c.c();
    ((Map)localObject2).putAll((Map)localObject3);
    localObject2 = this.m;
    paramd = aa;
    ((Map)localObject2).put("u-appIS", paramd);
    localObject2 = this.m;
    localObject3 = n.a().e();
    ((Map)localObject2).putAll((Map)localObject3);
    localObject2 = this.m;
    localObject3 = n.a().d();
    ((Map)localObject2).putAll((Map)localObject3);
    localObject2 = this.m;
    localObject3 = b.a();
    paramd = new java/util/HashMap;
    paramd.<init>();
    long l2;
    if (localObject3 != null)
    {
      localObject1 = "c-ap-bssid";
      l2 = a;
      localObject3 = String.valueOf(l2);
      paramd.put(localObject1, localObject3);
    }
    ((Map)localObject2).putAll(paramd);
    localObject2 = this.m;
    localObject3 = (ArrayList)com.inmobi.signals.b.c.a();
    paramd = new java/util/HashMap;
    paramd.<init>();
    if (localObject3 != null)
    {
      int m = ((ArrayList)localObject3).size();
      if (m > 0)
      {
        localObject1 = "v-ap-bssid";
        int n = ((ArrayList)localObject3).size() - k;
        l2 = geta;
        paramString1 = String.valueOf(l2);
        paramd.put(localObject1, paramString1);
      }
    }
    ((Map)localObject2).putAll(paramd);
    paramString1 = this.m;
    localObject2 = com.inmobi.signals.a.c.b();
    paramString1.putAll((Map)localObject2);
    paramString1 = this.m;
    localObject2 = com.inmobi.signals.a.c.c();
    paramString1.putAll((Map)localObject2);
    paramString1 = this.m;
    localObject2 = com.inmobi.signals.a.c.a();
    paramString1.putAll((Map)localObject2);
    paramString1 = UUID.randomUUID().toString();
    i = paramString1;
    paramString1 = this.m;
    localObject2 = "client-request-id";
    localObject3 = i;
    paramString1.put(localObject2, localObject3);
    if (paramString2 != null)
    {
      paramString1 = this.m;
      localObject2 = "u-appcache";
      paramString1.put(localObject2, paramString2);
    }
    this.m.put("sdk-flavor", "row");
  }
  
  private static boolean a(String paramString)
  {
    boolean bool1 = true;
    if (paramString == null) {
      return bool1;
    }
    paramString = Uri.parse(paramString);
    String str1 = "http";
    String str2 = paramString.getScheme();
    boolean bool2 = str1.equals(str2);
    if (!bool2)
    {
      str1 = "https";
      paramString = paramString.getScheme();
      boolean bool3 = str1.equals(paramString);
      if (bool3) {
        return false;
      }
    }
    return bool1;
  }
  
  public final void a()
  {
    super.a();
    Object localObject1 = m;
    Object localObject2 = b;
    ((Map)localObject1).put("format", localObject2);
    localObject1 = m;
    int k = d;
    localObject2 = String.valueOf(k);
    ((Map)localObject1).put("mk-ads", localObject2);
    localObject1 = m;
    Object localObject3 = "adtype";
    localObject2 = e;
    ((Map)localObject1).put(localObject3, localObject2);
    localObject1 = f;
    if (localObject1 != null)
    {
      localObject1 = m;
      localObject3 = "p-keywords";
      localObject2 = f;
      ((Map)localObject1).put(localObject3, localObject2);
    }
    localObject1 = j;
    if (localObject1 != null)
    {
      localObject3 = InMobiAdRequest.MonetizationContext.MONETIZATION_CONTEXT_OTHER;
      if (localObject1 == localObject3) {
        localObject1 = "M10N_CONTEXT_OTHER";
      } else {
        localObject1 = "M10N_CONTEXT_ACTIVITY";
      }
    }
    else
    {
      localObject1 = "M10N_CONTEXT_ACTIVITY";
    }
    localObject3 = m;
    localObject2 = "m10n_context";
    ((Map)localObject3).put(localObject2, localObject1);
    localObject1 = g;
    if (localObject1 != null)
    {
      localObject1 = ((Map)localObject1).entrySet().iterator();
      for (;;)
      {
        boolean bool2 = ((Iterator)localObject1).hasNext();
        if (!bool2) {
          break;
        }
        localObject3 = (Map.Entry)((Iterator)localObject1).next();
        localObject2 = m;
        Object localObject4 = ((Map.Entry)localObject3).getKey();
        boolean bool1 = ((Map)localObject2).containsKey(localObject4);
        if (!bool1)
        {
          localObject2 = m;
          localObject4 = ((Map.Entry)localObject3).getKey();
          localObject3 = ((Map.Entry)localObject3).getValue();
          ((Map)localObject2).put(localObject4, localObject3);
        }
      }
    }
    localObject1 = h;
    if (localObject1 != null)
    {
      localObject1 = m;
      localObject3 = h;
      ((Map)localObject1).putAll((Map)localObject3);
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */