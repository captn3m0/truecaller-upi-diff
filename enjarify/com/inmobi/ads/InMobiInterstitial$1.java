package com.inmobi.ads;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

final class InMobiInterstitial$1
  implements j.d
{
  public final void a(j paramj)
  {
    if (paramj != null) {
      try
      {
        Object localObject1 = InMobiInterstitial.access$000();
        localObject1 = ((ConcurrentHashMap)localObject1).get(paramj);
        localObject1 = (ArrayList)localObject1;
        if (localObject1 != null)
        {
          Object localObject2 = InMobiInterstitial.access$000();
          ((ConcurrentHashMap)localObject2).remove(paramj);
          localObject2 = new android/os/Handler;
          Object localObject3 = Looper.getMainLooper();
          ((Handler)localObject2).<init>((Looper)localObject3);
          localObject1 = ((ArrayList)localObject1).iterator();
          for (;;)
          {
            boolean bool = ((Iterator)localObject1).hasNext();
            if (!bool) {
              break;
            }
            localObject3 = ((Iterator)localObject1).next();
            localObject3 = (WeakReference)localObject3;
            if (localObject3 != null)
            {
              localObject3 = ((WeakReference)localObject3).get();
              localObject3 = (InMobiInterstitial.InterstitialAdRequestListener)localObject3;
              if (localObject3 != null)
              {
                InMobiInterstitial localInMobiInterstitial = new com/inmobi/ads/InMobiInterstitial;
                Object localObject4 = paramj.a();
                long l = b;
                localInMobiInterstitial.<init>((Context)localObject4, l, null);
                localObject4 = c;
                localInMobiInterstitial.setKeywords((String)localObject4);
                localObject4 = d;
                localInMobiInterstitial.setExtras((Map)localObject4);
                localObject4 = new com/inmobi/ads/InMobiInterstitial$1$1;
                ((InMobiInterstitial.1.1)localObject4).<init>(this, (InMobiInterstitial.InterstitialAdRequestListener)localObject3, localInMobiInterstitial);
                ((Handler)localObject2).post((Runnable)localObject4);
              }
            }
          }
        }
        return;
      }
      catch (Exception paramj)
      {
        InMobiInterstitial.access$200();
        paramj.getMessage();
        return;
      }
    }
  }
  
  public final void a(j paramj, InMobiAdRequestStatus paramInMobiAdRequestStatus)
  {
    if (paramj != null) {
      try
      {
        Object localObject1 = InMobiInterstitial.access$000();
        localObject1 = ((ConcurrentHashMap)localObject1).get(paramj);
        localObject1 = (ArrayList)localObject1;
        if (localObject1 != null)
        {
          int i = ((ArrayList)localObject1).size();
          if (i > 0)
          {
            i = ((ArrayList)localObject1).size() + -1;
            Object localObject2 = ((ArrayList)localObject1).get(i);
            localObject2 = (WeakReference)localObject2;
            if (localObject2 != null)
            {
              ((ArrayList)localObject1).remove(localObject2);
              int j = ((ArrayList)localObject1).size();
              if (j == 0)
              {
                localObject1 = InMobiInterstitial.access$000();
                ((ConcurrentHashMap)localObject1).remove(paramj);
              }
              paramj = ((WeakReference)localObject2).get();
              paramj = (InMobiInterstitial.InterstitialAdRequestListener)paramj;
              if (paramj != null)
              {
                localObject1 = new android/os/Handler;
                localObject2 = Looper.getMainLooper();
                ((Handler)localObject1).<init>((Looper)localObject2);
                localObject2 = new com/inmobi/ads/InMobiInterstitial$1$2;
                ((InMobiInterstitial.1.2)localObject2).<init>(this, paramj, paramInMobiAdRequestStatus);
                ((Handler)localObject1).post((Runnable)localObject2);
              }
            }
          }
        }
      }
      catch (Exception paramj)
      {
        InMobiInterstitial.access$200();
        paramj.getMessage();
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.InMobiInterstitial.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */