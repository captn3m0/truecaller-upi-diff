package com.inmobi.ads;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.b.a.a.a.f.a.h;
import com.b.a.a.a.f.c;
import com.b.a.a.a.f.e;
import com.b.a.a.a.f.f;
import com.inmobi.rendering.RenderView;
import java.lang.ref.WeakReference;

public class q
  extends bv
{
  private static final String d = "q";
  private final WeakReference e;
  private final bw f;
  private final com.b.a.a.a.f.a g;
  private final boolean h;
  
  public q(AdContainer paramAdContainer, Activity paramActivity, bw parambw, com.b.a.a.a.f.a parama, boolean paramBoolean)
  {
    super(paramAdContainer);
    paramAdContainer = new java/lang/ref/WeakReference;
    paramAdContainer.<init>(paramActivity);
    e = paramAdContainer;
    f = parambw;
    g = parama;
    h = paramBoolean;
  }
  
  static com.b.a.a.a.f.a a(Context paramContext, boolean paramBoolean, String paramString, RenderView paramRenderView)
  {
    f localf = new com/b/a/a/a/f/f;
    Object localObject1 = "7.1.1";
    localf.<init>((String)localObject1, paramBoolean);
    paramBoolean = paramString.hashCode();
    boolean bool1 = -284840886;
    Object localObject2;
    if (paramBoolean != bool1)
    {
      boolean bool2 = 112202875;
      if (paramBoolean != bool2)
      {
        boolean bool3 = 1425678798;
        if (paramBoolean == bool3)
        {
          localObject2 = "nonvideo";
          paramBoolean = paramString.equals(localObject2);
          if (paramBoolean)
          {
            paramBoolean = true;
            break label117;
          }
        }
      }
      else
      {
        localObject2 = "video";
        paramBoolean = paramString.equals(localObject2);
        if (paramBoolean)
        {
          paramBoolean = true;
          break label117;
        }
      }
    }
    else
    {
      localObject2 = "unknown";
      paramBoolean = paramString.equals(localObject2);
      if (paramBoolean)
      {
        paramBoolean = true;
        break label117;
      }
    }
    paramBoolean = true;
    label117:
    paramString = null;
    String str;
    switch (paramBoolean)
    {
    default: 
      paramBoolean = false;
      localObject2 = null;
      break;
    case 3: 
      com.b.a.a.a.d.b().a(paramContext);
      localObject2 = new com/b/a/a/a/f/e;
      ((e)localObject2).<init>();
      localObject1 = new com/b/a/a/a/f/a/h;
      str = a;
      ((h)localObject1).<init>(paramContext, str, localf);
      ((h)localObject1).d();
      com.b.a.a.a.d.b();
      com.b.a.a.a.d.a((com.b.a.a.a.f.a)localObject2, (com.b.a.a.a.f.a.a)localObject1);
      break;
    case 2: 
      com.b.a.a.a.d.b().a(paramContext);
      localObject2 = new com/b/a/a/a/f/c;
      ((c)localObject2).<init>();
      localObject1 = new com/b/a/a/a/f/a/d;
      str = a;
      ((com.b.a.a.a.f.a.d)localObject1).<init>(paramContext, str, localf);
      ((com.b.a.a.a.f.a.d)localObject1).d();
      com.b.a.a.a.d.b();
      com.b.a.a.a.d.a((com.b.a.a.a.f.a)localObject2, (com.b.a.a.a.f.a.a)localObject1);
    }
    if (localObject2 != null)
    {
      boolean bool4 = paramContext instanceof Activity;
      if (bool4)
      {
        paramContext = (Activity)paramContext;
        ((com.b.a.a.a.f.a)localObject2).a(paramRenderView, paramContext);
      }
      else
      {
        ((com.b.a.a.a.f.a)localObject2).a(paramRenderView, null);
      }
    }
    return (com.b.a.a.a.f.a)localObject2;
  }
  
  private void a(Activity paramActivity, WebView paramWebView, View[] paramArrayOfView)
  {
    if (paramArrayOfView != null)
    {
      int i = paramArrayOfView.length;
      int j = 0;
      while (j < i)
      {
        View localView = paramArrayOfView[j];
        com.b.a.a.a.f.a locala = g;
        locala.b(localView);
        j += 1;
      }
    }
    paramArrayOfView = g;
    paramArrayOfView.a(paramWebView, paramActivity);
    boolean bool = h;
    if (bool)
    {
      paramActivity = g.b();
      if (paramActivity != null)
      {
        paramActivity = g.b();
        paramActivity.i_();
      }
    }
  }
  
  public final View a()
  {
    return f.a();
  }
  
  public final View a(View paramView, ViewGroup paramViewGroup, boolean paramBoolean)
  {
    return f.a(paramView, paramViewGroup, paramBoolean);
  }
  
  public final void a(int paramInt)
  {
    f.a(paramInt);
  }
  
  public final void a(Context paramContext, int paramInt)
  {
    f.a(paramContext, paramInt);
  }
  
  /* Error */
  public final void a(View... paramVarArgs)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 28	com/inmobi/ads/q:e	Ljava/lang/ref/WeakReference;
    //   4: astore_2
    //   5: aload_2
    //   6: invokevirtual 136	java/lang/ref/WeakReference:get	()Ljava/lang/Object;
    //   9: astore_2
    //   10: aload_2
    //   11: checkcast 104	android/app/Activity
    //   14: astore_2
    //   15: aload_0
    //   16: getfield 30	com/inmobi/ads/q:f	Lcom/inmobi/ads/bw;
    //   19: astore_3
    //   20: aload_3
    //   21: invokevirtual 140	com/inmobi/ads/bw:c	()Lcom/inmobi/ads/b;
    //   24: astore_3
    //   25: aload_3
    //   26: getfield 146	com/inmobi/ads/b:o	Lcom/inmobi/ads/b$k;
    //   29: astore_3
    //   30: aload_2
    //   31: ifnull +92 -> 123
    //   34: aload_3
    //   35: getfield 151	com/inmobi/ads/b$k:j	Z
    //   38: istore 4
    //   40: iload 4
    //   42: ifeq +81 -> 123
    //   45: aload_0
    //   46: getfield 154	com/inmobi/ads/bw:a	Lcom/inmobi/ads/AdContainer;
    //   49: astore_3
    //   50: aload_3
    //   51: instanceof 156
    //   54: istore 4
    //   56: iload 4
    //   58: ifeq +39 -> 97
    //   61: aload_0
    //   62: getfield 154	com/inmobi/ads/bw:a	Lcom/inmobi/ads/AdContainer;
    //   65: astore_3
    //   66: aload_3
    //   67: checkcast 156	com/inmobi/ads/ad
    //   70: astore_3
    //   71: aload_3
    //   72: invokevirtual 160	com/inmobi/ads/ad:s	()Lcom/inmobi/rendering/RenderView;
    //   75: astore 5
    //   77: aload 5
    //   79: ifnull +44 -> 123
    //   82: aload_3
    //   83: invokevirtual 160	com/inmobi/ads/ad:s	()Lcom/inmobi/rendering/RenderView;
    //   86: astore_3
    //   87: aload_0
    //   88: aload_2
    //   89: aload_3
    //   90: aload_1
    //   91: invokespecial 163	com/inmobi/ads/q:a	(Landroid/app/Activity;Landroid/webkit/WebView;[Landroid/view/View;)V
    //   94: goto +29 -> 123
    //   97: aload_0
    //   98: getfield 30	com/inmobi/ads/q:f	Lcom/inmobi/ads/bw;
    //   101: astore_3
    //   102: aload_3
    //   103: invokevirtual 165	com/inmobi/ads/bw:b	()Landroid/view/View;
    //   106: astore_3
    //   107: aload_3
    //   108: ifnull +15 -> 123
    //   111: aload_3
    //   112: checkcast 167	android/webkit/WebView
    //   115: astore_3
    //   116: aload_0
    //   117: aload_2
    //   118: aload_3
    //   119: aload_1
    //   120: invokespecial 163	com/inmobi/ads/q:a	(Landroid/app/Activity;Landroid/webkit/WebView;[Landroid/view/View;)V
    //   123: aload_0
    //   124: getfield 30	com/inmobi/ads/q:f	Lcom/inmobi/ads/bw;
    //   127: aload_1
    //   128: invokevirtual 170	com/inmobi/ads/bw:a	([Landroid/view/View;)V
    //   131: return
    //   132: astore_2
    //   133: goto +12 -> 145
    //   136: astore_2
    //   137: aload_2
    //   138: invokevirtual 176	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   141: pop
    //   142: goto -19 -> 123
    //   145: aload_0
    //   146: getfield 30	com/inmobi/ads/q:f	Lcom/inmobi/ads/bw;
    //   149: aload_1
    //   150: invokevirtual 170	com/inmobi/ads/bw:a	([Landroid/view/View;)V
    //   153: aload_2
    //   154: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	155	0	this	q
    //   0	155	1	paramVarArgs	View[]
    //   4	114	2	localObject1	Object
    //   132	1	2	localObject2	Object
    //   136	18	2	localException	Exception
    //   19	100	3	localObject3	Object
    //   38	19	4	bool	boolean
    //   75	3	5	localRenderView	RenderView
    // Exception table:
    //   from	to	target	type
    //   0	4	132	finally
    //   5	9	132	finally
    //   10	14	132	finally
    //   15	19	132	finally
    //   20	24	132	finally
    //   25	29	132	finally
    //   34	38	132	finally
    //   45	49	132	finally
    //   61	65	132	finally
    //   66	70	132	finally
    //   71	75	132	finally
    //   82	86	132	finally
    //   90	94	132	finally
    //   97	101	132	finally
    //   102	106	132	finally
    //   111	115	132	finally
    //   119	123	132	finally
    //   137	142	132	finally
    //   0	4	136	java/lang/Exception
    //   5	9	136	java/lang/Exception
    //   10	14	136	java/lang/Exception
    //   15	19	136	java/lang/Exception
    //   20	24	136	java/lang/Exception
    //   25	29	136	java/lang/Exception
    //   34	38	136	java/lang/Exception
    //   45	49	136	java/lang/Exception
    //   61	65	136	java/lang/Exception
    //   66	70	136	java/lang/Exception
    //   71	75	136	java/lang/Exception
    //   82	86	136	java/lang/Exception
    //   90	94	136	java/lang/Exception
    //   97	101	136	java/lang/Exception
    //   102	106	136	java/lang/Exception
    //   111	115	136	java/lang/Exception
    //   119	123	136	java/lang/Exception
  }
  
  public final View b()
  {
    return f.b();
  }
  
  final b c()
  {
    return f.c();
  }
  
  /* Error */
  public final void d()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 154	com/inmobi/ads/bw:a	Lcom/inmobi/ads/AdContainer;
    //   4: astore_1
    //   5: aload_1
    //   6: instanceof 156
    //   9: istore_2
    //   10: iload_2
    //   11: ifeq +21 -> 32
    //   14: aload_0
    //   15: getfield 154	com/inmobi/ads/bw:a	Lcom/inmobi/ads/AdContainer;
    //   18: astore_1
    //   19: aload_1
    //   20: checkcast 156	com/inmobi/ads/ad
    //   23: astore_1
    //   24: aload_1
    //   25: invokevirtual 160	com/inmobi/ads/ad:s	()Lcom/inmobi/rendering/RenderView;
    //   28: astore_1
    //   29: goto +18 -> 47
    //   32: aload_0
    //   33: getfield 30	com/inmobi/ads/q:f	Lcom/inmobi/ads/bw;
    //   36: astore_1
    //   37: aload_1
    //   38: invokevirtual 165	com/inmobi/ads/bw:b	()Landroid/view/View;
    //   41: astore_1
    //   42: aload_1
    //   43: checkcast 167	android/webkit/WebView
    //   46: astore_1
    //   47: aload_0
    //   48: getfield 32	com/inmobi/ads/q:g	Lcom/b/a/a/a/f/a;
    //   51: astore_3
    //   52: aload_3
    //   53: aload_1
    //   54: invokevirtual 178	com/b/a/a/a/f/a:a	(Landroid/view/View;)V
    //   57: aload_0
    //   58: getfield 32	com/inmobi/ads/q:g	Lcom/b/a/a/a/f/a;
    //   61: astore_1
    //   62: aload_1
    //   63: invokevirtual 180	com/b/a/a/a/f/a:a	()V
    //   66: aload_0
    //   67: getfield 30	com/inmobi/ads/q:f	Lcom/inmobi/ads/bw;
    //   70: invokevirtual 181	com/inmobi/ads/bw:d	()V
    //   73: return
    //   74: astore_1
    //   75: goto +12 -> 87
    //   78: astore_1
    //   79: aload_1
    //   80: invokevirtual 176	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   83: pop
    //   84: goto -18 -> 66
    //   87: aload_0
    //   88: getfield 30	com/inmobi/ads/q:f	Lcom/inmobi/ads/bw;
    //   91: invokevirtual 181	com/inmobi/ads/bw:d	()V
    //   94: aload_1
    //   95: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	96	0	this	q
    //   4	59	1	localObject1	Object
    //   74	1	1	localObject2	Object
    //   78	17	1	localException	Exception
    //   9	2	2	bool	boolean
    //   51	2	3	locala	com.b.a.a.a.f.a
    // Exception table:
    //   from	to	target	type
    //   0	4	74	finally
    //   14	18	74	finally
    //   19	23	74	finally
    //   24	28	74	finally
    //   32	36	74	finally
    //   37	41	74	finally
    //   42	46	74	finally
    //   47	51	74	finally
    //   53	57	74	finally
    //   57	61	74	finally
    //   62	66	74	finally
    //   79	84	74	finally
    //   0	4	78	java/lang/Exception
    //   14	18	78	java/lang/Exception
    //   19	23	78	java/lang/Exception
    //   24	28	78	java/lang/Exception
    //   32	36	78	java/lang/Exception
    //   37	41	78	java/lang/Exception
    //   42	46	78	java/lang/Exception
    //   47	51	78	java/lang/Exception
    //   53	57	78	java/lang/Exception
    //   57	61	78	java/lang/Exception
    //   62	66	78	java/lang/Exception
  }
  
  /* Error */
  public final void e()
  {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial 183	com/inmobi/ads/bv:e	()V
    //   4: aload_0
    //   5: getfield 28	com/inmobi/ads/q:e	Ljava/lang/ref/WeakReference;
    //   8: astore_1
    //   9: aload_1
    //   10: invokevirtual 186	java/lang/ref/WeakReference:clear	()V
    //   13: aload_0
    //   14: getfield 30	com/inmobi/ads/q:f	Lcom/inmobi/ads/bw;
    //   17: invokevirtual 187	com/inmobi/ads/bw:e	()V
    //   20: return
    //   21: astore_1
    //   22: goto +12 -> 34
    //   25: astore_1
    //   26: aload_1
    //   27: invokevirtual 176	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   30: pop
    //   31: goto -18 -> 13
    //   34: aload_0
    //   35: getfield 30	com/inmobi/ads/q:f	Lcom/inmobi/ads/bw;
    //   38: invokevirtual 187	com/inmobi/ads/bw:e	()V
    //   41: aload_1
    //   42: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	43	0	this	q
    //   8	2	1	localWeakReference	WeakReference
    //   21	1	1	localObject	Object
    //   25	17	1	localException	Exception
    // Exception table:
    //   from	to	target	type
    //   4	8	21	finally
    //   9	13	21	finally
    //   26	31	21	finally
    //   4	8	25	java/lang/Exception
    //   9	13	25	java/lang/Exception
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */