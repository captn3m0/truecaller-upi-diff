package com.inmobi.ads;

import android.content.ContentValues;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class az
  extends a
{
  private static final String l = "az";
  final String i;
  final String j;
  final String k;
  private final String m;
  private final String n;
  
  az(ContentValues paramContentValues)
  {
    super(paramContentValues);
    String str = paramContentValues.getAsString("video_url");
    i = str;
    str = paramContentValues.getAsString("video_track_duration");
    j = str;
    str = paramContentValues.getAsString("click_url");
    k = str;
    str = paramContentValues.getAsString("video_trackers");
    m = str;
    paramContentValues = paramContentValues.getAsString("companion_ads");
    n = paramContentValues;
  }
  
  public az(JSONObject paramJSONObject, String paramString1, long paramLong1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, String paramString9, String paramString10, InMobiAdRequest.MonetizationContext paramMonetizationContext, boolean paramBoolean, long paramLong2)
  {
    super(paramJSONObject, paramString1, paramLong1, paramString2, paramString3, paramString4, paramString5, paramMonetizationContext, paramBoolean, paramLong2);
    i = paramString6;
    j = paramString7;
    k = paramString8;
    m = paramString9;
    n = paramString10;
  }
  
  public final ContentValues a()
  {
    ContentValues localContentValues = super.a();
    String str = i;
    localContentValues.put("video_url", str);
    str = j;
    localContentValues.put("video_track_duration", str);
    str = k;
    localContentValues.put("click_url", str);
    str = m;
    localContentValues.put("video_trackers", str);
    str = n;
    localContentValues.put("companion_ads", str);
    return localContentValues;
  }
  
  public final List f()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Object localObject1 = m;
    if (localObject1 != null)
    {
      int i1 = ((String)localObject1).length();
      if (i1 != 0) {
        try
        {
          localObject1 = new org/json/JSONArray;
          localObject2 = m;
          ((JSONArray)localObject1).<init>((String)localObject2);
          int i2 = ((JSONArray)localObject1).length();
          if (i2 == 0) {
            return localArrayList;
          }
          i2 = 0;
          localObject2 = null;
          for (;;)
          {
            int i3 = ((JSONArray)localObject1).length();
            if (i2 >= i3) {
              break;
            }
            localObject3 = new org/json/JSONObject;
            String str = ((JSONArray)localObject1).getString(i2);
            ((JSONObject)localObject3).<init>(str);
            localObject3 = NativeTracker.a((JSONObject)localObject3);
            if (localObject3 != null) {
              localArrayList.add(localObject3);
            }
            i2 += 1;
          }
          return localArrayList;
        }
        catch (JSONException localJSONException)
        {
          Object localObject2 = com.inmobi.commons.core.a.a.a();
          Object localObject3 = new com/inmobi/commons/core/e/a;
          ((com.inmobi.commons.core.e.a)localObject3).<init>(localJSONException);
          ((com.inmobi.commons.core.a.a)localObject2).a((com.inmobi.commons.core.e.a)localObject3);
          return localArrayList;
        }
      }
    }
    return localArrayList;
  }
  
  final List g()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Object localObject1 = n;
    if (localObject1 != null)
    {
      int i1 = ((String)localObject1).length();
      if (i1 != 0) {
        try
        {
          localObject1 = new org/json/JSONArray;
          localObject2 = n;
          ((JSONArray)localObject1).<init>((String)localObject2);
          int i2 = ((JSONArray)localObject1).length();
          if (i2 == 0) {
            return localArrayList;
          }
          i2 = 0;
          localObject2 = null;
          for (;;)
          {
            int i3 = ((JSONArray)localObject1).length();
            if (i2 >= i3) {
              break;
            }
            localObject3 = new org/json/JSONObject;
            String str = ((JSONArray)localObject1).getString(i2);
            ((JSONObject)localObject3).<init>(str);
            localObject3 = bp.a((JSONObject)localObject3);
            if (localObject3 != null) {
              localArrayList.add(localObject3);
            }
            i2 += 1;
          }
          return localArrayList;
        }
        catch (JSONException localJSONException)
        {
          Object localObject2 = com.inmobi.commons.core.a.a.a();
          Object localObject3 = new com/inmobi/commons/core/e/a;
          ((com.inmobi.commons.core.e.a)localObject3).<init>(localJSONException);
          ((com.inmobi.commons.core.a.a)localObject2).a((com.inmobi.commons.core.e.a)localObject3);
          return localArrayList;
        }
      }
    }
    return localArrayList;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.az
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */