package com.inmobi.ads;

import android.app.Activity;
import android.content.Context;
import android.os.Build.VERSION;
import android.view.View;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;

public class ae
{
  static final Map a;
  private static final String b = "ae";
  private static final Map c;
  private static final Map d;
  private static final t.a e;
  private static final ca.a f;
  private boolean g;
  private int h;
  
  static
  {
    Object localObject = new java/util/WeakHashMap;
    ((WeakHashMap)localObject).<init>();
    a = (Map)localObject;
    localObject = new java/util/WeakHashMap;
    ((WeakHashMap)localObject).<init>();
    c = (Map)localObject;
    localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    d = (Map)localObject;
    localObject = new com/inmobi/ads/ae$1;
    ((ae.1)localObject).<init>();
    e = (t.a)localObject;
    localObject = new com/inmobi/ads/ae$2;
    ((ae.2)localObject).<init>();
    f = (ca.a)localObject;
  }
  
  ae(int paramInt)
  {
    h = paramInt;
  }
  
  static void b(Context paramContext)
  {
    Map localMap = a;
    paramContext = (t)localMap.get(paramContext);
    if (paramContext != null) {
      paramContext.b();
    }
  }
  
  static void c(Context paramContext)
  {
    Map localMap = a;
    paramContext = (t)localMap.get(paramContext);
    if (paramContext != null) {
      paramContext.a();
    }
  }
  
  final void a(Context paramContext)
  {
    t localt = (t)a.remove(paramContext);
    if (localt != null) {
      localt.c();
    }
    boolean bool1 = paramContext instanceof Activity;
    if (bool1)
    {
      int i = Build.VERSION.SDK_INT;
      int j = 15;
      if (i >= j)
      {
        paramContext = a;
        boolean bool2 = paramContext.isEmpty();
        if (bool2)
        {
          bool2 = g;
          if (bool2)
          {
            bool2 = false;
            paramContext = null;
            g = false;
          }
        }
      }
    }
  }
  
  final void a(Context paramContext, View paramView, ad paramad)
  {
    ca localca = (ca)c.get(paramContext);
    if (localca != null)
    {
      localca.a(paramad);
      paramad = b;
      boolean bool1 = paramad.isEmpty() ^ true;
      if (!bool1)
      {
        paramad = (ca)c.remove(paramContext);
        if (paramad != null) {
          paramad.e();
        }
        boolean bool2 = paramContext instanceof Activity;
        if (bool2)
        {
          int j = Build.VERSION.SDK_INT;
          int i = 15;
          if (j >= i)
          {
            paramContext = c;
            boolean bool3 = paramContext.isEmpty();
            if (bool3)
            {
              bool3 = g;
              if (bool3)
              {
                bool3 = false;
                paramContext = null;
                g = false;
              }
            }
          }
        }
      }
    }
    d.remove(paramView);
  }
  
  final void a(Context paramContext, View paramView, ad paramad, ae.a parama, b.k paramk)
  {
    Object localObject1 = (ca)c.get(paramContext);
    if (localObject1 == null)
    {
      boolean bool1 = paramContext instanceof Activity;
      Object localObject2;
      if (bool1)
      {
        localObject2 = new com/inmobi/ads/p;
        localObject3 = f;
        Object localObject4 = paramContext;
        localObject4 = (Activity)paramContext;
        ((p)localObject2).<init>((ca.a)localObject3, (Activity)localObject4);
      }
      else
      {
        localObject2 = new com/inmobi/ads/bh;
        localObject3 = f;
        ((bh)localObject2).<init>((ca.a)localObject3, paramk);
      }
      Object localObject3 = new com/inmobi/ads/ae$3;
      ((ae.3)localObject3).<init>(this);
      c = ((ca.c)localObject3);
      localObject3 = c;
      ((Map)localObject3).put(paramContext, localObject2);
      if (bool1)
      {
        int j = Build.VERSION.SDK_INT;
        int i = 15;
        if (j >= i)
        {
          boolean bool2 = g;
          if (!bool2)
          {
            bool2 = true;
            g = bool2;
          }
        }
      }
      localObject1 = localObject2;
    }
    paramContext = d;
    paramContext.put(paramView, parama);
    int k = h;
    if (k != 0)
    {
      k = e;
      ((ca)localObject1).a(paramView, paramad, k);
      return;
    }
    k = h;
    ((ca)localObject1).a(paramView, paramad, k);
  }
  
  final void a(Context paramContext, View paramView, ad paramad, b.k paramk)
  {
    t localt = (t)a.get(paramContext);
    if (localt == null)
    {
      boolean bool1 = paramContext instanceof Activity;
      Object localObject2;
      if (bool1)
      {
        localt = new com/inmobi/ads/t;
        localObject1 = new com/inmobi/ads/p;
        localObject2 = f;
        Object localObject3 = paramContext;
        localObject3 = (Activity)paramContext;
        ((p)localObject1).<init>((ca.a)localObject2, (Activity)localObject3);
        localObject2 = e;
        localt.<init>(paramk, (ca)localObject1, (t.a)localObject2);
        int i = Build.VERSION.SDK_INT;
        int j = 15;
        if (i >= j)
        {
          boolean bool2 = g;
          if (!bool2)
          {
            bool2 = true;
            g = bool2;
          }
        }
      }
      else
      {
        localt = new com/inmobi/ads/t;
        localObject1 = new com/inmobi/ads/bh;
        localObject2 = f;
        ((bh)localObject1).<init>((ca.a)localObject2, paramk);
        localObject2 = e;
        localt.<init>(paramk, (ca)localObject1, (t.a)localObject2);
      }
      Object localObject1 = a;
      ((Map)localObject1).put(paramContext, localt);
    }
    int k = h;
    if (k != 0)
    {
      k = a;
      m = b;
      localt.a(paramView, paramad, k, m);
      return;
    }
    k = f;
    int m = g;
    localt.a(paramView, paramad, k, m);
  }
  
  final void a(Context paramContext, ad paramad)
  {
    t localt = (t)a.get(paramContext);
    if (localt != null)
    {
      localt.a(paramad);
      paramad = a;
      boolean bool = paramad.isEmpty() ^ true;
      if (!bool) {
        a(paramContext);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.ae
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */