package com.inmobi.ads.cache;

import com.d.b.e;
import java.util.concurrent.CountDownLatch;

class AssetStore$PicassoCallback
  implements e
{
  private CountDownLatch b;
  private String c;
  
  AssetStore$PicassoCallback(AssetStore paramAssetStore, CountDownLatch paramCountDownLatch, String paramString)
  {
    b = paramCountDownLatch;
    c = paramString;
  }
  
  public void onError()
  {
    AssetStore localAssetStore = a;
    String str = c;
    AssetStore.d(localAssetStore, str);
    b.countDown();
  }
  
  public void onSuccess()
  {
    AssetStore localAssetStore = a;
    String str = c;
    AssetStore.c(localAssetStore, str);
    b.countDown();
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.cache.AssetStore.PicassoCallback
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */