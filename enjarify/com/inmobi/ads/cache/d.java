package com.inmobi.ads.cache;

import android.content.ContentValues;
import com.inmobi.commons.core.d.b;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;

public class d
{
  public static final String[] a = tmp50_36;
  private static final String b = "d";
  private static d c;
  private static final Object d;
  
  static
  {
    Object localObject = new java/lang/Object;
    localObject.<init>();
    d = localObject;
    String[] tmp17_14 = new String[8];
    String[] tmp18_17 = tmp17_14;
    String[] tmp18_17 = tmp17_14;
    tmp18_17[0] = "id";
    tmp18_17[1] = "pending_attempts";
    String[] tmp27_18 = tmp18_17;
    String[] tmp27_18 = tmp18_17;
    tmp27_18[2] = "url";
    tmp27_18[3] = "disk_uri";
    String[] tmp36_27 = tmp27_18;
    String[] tmp36_27 = tmp27_18;
    tmp36_27[4] = "ts";
    tmp36_27[5] = "created_ts";
    tmp36_27[6] = "ttl";
    String[] tmp50_36 = tmp36_27;
    tmp50_36[7] = "soft_ttl";
  }
  
  private d()
  {
    b localb = b.a();
    localb.a("asset", "(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, pending_attempts INTEGER NOT NULL, url TEXT NOT NULL, disk_uri TEXT, ts TEXT NOT NULL, created_ts TEXT NOT NULL, ttl TEXT NOT NULL, soft_ttl TEXT NOT NULL)");
    localb.b();
  }
  
  public static a a(ContentValues paramContentValues)
  {
    int i = paramContentValues.getAsInteger("id").intValue();
    int j = paramContentValues.getAsInteger("pending_attempts").intValue();
    String str1 = paramContentValues.getAsString("url");
    String str2 = paramContentValues.getAsString("disk_uri");
    long l1 = Long.valueOf(paramContentValues.getAsString("ts")).longValue();
    long l2 = Long.valueOf(paramContentValues.getAsString("created_ts")).longValue();
    long l3 = Long.valueOf(paramContentValues.getAsString("ttl")).longValue();
    long l4 = Long.valueOf(paramContentValues.getAsString("soft_ttl")).longValue();
    paramContentValues = new com/inmobi/ads/cache/a;
    paramContentValues.<init>(i, str1, str2, j, l1, l2, l3, l4);
    return paramContentValues;
  }
  
  static a a(String paramString)
  {
    b localb = b.a();
    String str1 = "asset";
    String[] arrayOfString1 = a;
    String str2 = "url=? ";
    String[] arrayOfString2 = new String[1];
    arrayOfString2[0] = paramString;
    String str3 = "created_ts DESC ";
    String str4 = "1";
    paramString = localb.a(str1, arrayOfString1, str2, arrayOfString2, null, null, str3, str4);
    localb.b();
    boolean bool = paramString.isEmpty();
    if (bool) {
      return null;
    }
    return a((ContentValues)paramString.get(0));
  }
  
  public static d a()
  {
    d locald1 = c;
    if (locald1 == null) {
      synchronized (d)
      {
        locald1 = c;
        if (locald1 == null)
        {
          locald1 = new com/inmobi/ads/cache/d;
          locald1.<init>();
          c = locald1;
        }
      }
    }
    return locald2;
  }
  
  static List a(int paramInt)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    b localb = b.a();
    Object localObject1 = "asset";
    int i = localb.a((String)localObject1);
    if (i == 0) {
      return localArrayList;
    }
    String str1 = "asset";
    String[] arrayOfString = a;
    String str2 = "ts";
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("ts < ");
    long l1 = System.currentTimeMillis();
    long l2 = paramInt;
    l1 -= l2;
    ((StringBuilder)localObject1).append(l1);
    String str3 = ((StringBuilder)localObject1).toString();
    String str4 = "ts ASC ";
    localObject1 = localb;
    Object localObject2 = localb.a(str1, arrayOfString, null, null, str2, str3, str4, null);
    localb.b();
    localObject2 = ((List)localObject2).iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject2).hasNext();
      if (!bool) {
        break;
      }
      localObject1 = a((ContentValues)((Iterator)localObject2).next());
      localArrayList.add(localObject1);
    }
    return localArrayList;
  }
  
  public static int b(a parama)
  {
    b localb = b.a();
    String[] arrayOfString = new String[1];
    String str = String.valueOf(d);
    arrayOfString[0] = str;
    parama = d(parama);
    int i = localb.b("asset", parama, "url = ?", arrayOfString);
    localb.b();
    return i;
  }
  
  public static a b(String paramString)
  {
    b localb = b.a();
    String str1 = "asset";
    String[] arrayOfString1 = a;
    String str2 = "url=? ";
    String[] arrayOfString2 = new String[1];
    arrayOfString2[0] = paramString;
    String str3 = "created_ts DESC ";
    String str4 = "1";
    paramString = localb.a(str1, arrayOfString1, str2, arrayOfString2, null, null, str3, str4);
    localb.b();
    boolean bool = paramString.isEmpty();
    if (bool) {
      return null;
    }
    return a((ContentValues)paramString.get(0));
  }
  
  public static List b()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    b localb = b.a();
    Object localObject1 = "asset";
    int i = localb.a((String)localObject1);
    if (i == 0) {
      return localArrayList;
    }
    Object localObject2 = "asset";
    String[] arrayOfString = a;
    String str1 = "disk_uri IS NOT NULL";
    String str2 = "created_ts DESC ";
    localObject1 = localb;
    localObject1 = localb.a((String)localObject2, arrayOfString, str1, null, null, null, str2, null);
    localb.b();
    localObject1 = ((List)localObject1).iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject1).hasNext();
      if (!bool) {
        break;
      }
      localObject2 = a((ContentValues)((Iterator)localObject1).next());
      localArrayList.add(localObject2);
    }
    return localArrayList;
  }
  
  public static String c()
  {
    Object localObject1 = b();
    int i = ((List)localObject1).size();
    if (i == 0) {
      return null;
    }
    JSONArray localJSONArray = new org/json/JSONArray;
    localJSONArray.<init>();
    localObject1 = ((List)localObject1).iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject1).hasNext();
      if (!bool) {
        break;
      }
      Object localObject2 = (a)((Iterator)localObject1).next();
      try
      {
        localObject2 = d;
        String str = "UTF-8";
        localObject2 = URLEncoder.encode((String)localObject2, str);
        localJSONArray.put(localObject2);
      }
      catch (UnsupportedEncodingException localUnsupportedEncodingException) {}
    }
    return localJSONArray.toString();
  }
  
  public static void c(a parama)
  {
    b localb = b.a();
    String[] arrayOfString = new String[1];
    parama = String.valueOf(b);
    arrayOfString[0] = parama;
    localb.a("asset", "id = ?", arrayOfString);
    localb.b();
  }
  
  private static ContentValues d(a parama)
  {
    ContentValues localContentValues = new android/content/ContentValues;
    localContentValues.<init>();
    Object localObject = Integer.valueOf(b);
    localContentValues.put("id", (Integer)localObject);
    localObject = d;
    localContentValues.put("url", (String)localObject);
    localObject = e;
    localContentValues.put("disk_uri", (String)localObject);
    localObject = Integer.valueOf(c);
    localContentValues.put("pending_attempts", (Integer)localObject);
    localObject = Long.toString(f);
    localContentValues.put("ts", (String)localObject);
    localObject = Long.toString(g);
    localContentValues.put("created_ts", (String)localObject);
    localObject = Long.toString(h);
    localContentValues.put("ttl", (String)localObject);
    parama = Long.toString(i);
    localContentValues.put("soft_ttl", parama);
    return localContentValues;
  }
  
  public static List d()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    b localb = b.a();
    Object localObject1 = "asset";
    int i = localb.a((String)localObject1);
    if (i == 0) {
      return localArrayList;
    }
    Object localObject2 = "asset";
    Object localObject3 = { "url" };
    String str = "created_ts DESC ";
    localObject1 = localb;
    localObject1 = localb.a((String)localObject2, (String[])localObject3, null, null, null, null, str, null);
    localb.b();
    localObject1 = ((List)localObject1).iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject1).hasNext();
      if (!bool) {
        break;
      }
      localObject2 = (ContentValues)((Iterator)localObject1).next();
      localObject3 = "url";
      localObject2 = ((ContentValues)localObject2).getAsString((String)localObject3);
      localArrayList.add(localObject2);
    }
    return localArrayList;
  }
  
  public final void a(a parama)
  {
    try
    {
      int i = b(parama);
      if (i <= 0)
      {
        parama = d(parama);
        b localb = b.a();
        String str = "asset";
        localb.a(str, parama);
        localb.b();
      }
      return;
    }
    finally {}
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.cache.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */