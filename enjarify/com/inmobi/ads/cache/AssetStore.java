package com.inmobi.ads.cache;

import android.os.Build.VERSION;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.SystemClock;
import android.webkit.URLUtil;
import com.google.firebase.perf.network.FirebasePerfUrlConnection;
import com.inmobi.ads.b.b;
import com.inmobi.ads.b.j;
import com.inmobi.commons.core.configs.b.c;
import com.inmobi.commons.core.utilities.g;
import com.inmobi.commons.core.utilities.g.b;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

public final class AssetStore
  implements b.c
{
  public static final Object e;
  private static final String f = "AssetStore";
  private static AssetStore o;
  private static final Object p;
  public d a;
  public b.b b;
  public ExecutorService c;
  public AtomicBoolean d;
  private b.j g;
  private ExecutorService h;
  private AssetStore.a i;
  private HandlerThread j;
  private AtomicBoolean k;
  private ConcurrentHashMap l;
  private g.b m;
  private g.b n;
  private List q;
  private final e r;
  
  static
  {
    Object localObject = new java/lang/Object;
    localObject.<init>();
    p = localObject;
    localObject = new java/lang/Object;
    localObject.<init>();
    e = localObject;
  }
  
  private AssetStore()
  {
    Object localObject1 = new java/util/concurrent/atomic/AtomicBoolean;
    ((AtomicBoolean)localObject1).<init>(false);
    k = ((AtomicBoolean)localObject1);
    localObject1 = new java/util/concurrent/atomic/AtomicBoolean;
    ((AtomicBoolean)localObject1).<init>(false);
    d = ((AtomicBoolean)localObject1);
    localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    q = ((List)localObject1);
    localObject1 = new com/inmobi/ads/cache/AssetStore$1;
    ((AssetStore.1)localObject1).<init>(this);
    r = ((e)localObject1);
    localObject1 = new com/inmobi/ads/b;
    ((com.inmobi.ads.b)localObject1).<init>();
    com.inmobi.commons.core.configs.b.a().a((com.inmobi.commons.core.configs.a)localObject1, this);
    Object localObject2 = r;
    b = ((b.b)localObject2);
    localObject1 = q;
    g = ((b.j)localObject1);
    localObject1 = d.a();
    a = ((d)localObject1);
    localObject1 = Executors.newCachedThreadPool();
    c = ((ExecutorService)localObject1);
    localObject1 = Executors.newFixedThreadPool(1);
    h = ((ExecutorService)localObject1);
    localObject1 = new android/os/HandlerThread;
    ((HandlerThread)localObject1).<init>("assetFetcher");
    j = ((HandlerThread)localObject1);
    j.start();
    localObject1 = new com/inmobi/ads/cache/AssetStore$a;
    localObject2 = j.getLooper();
    ((AssetStore.a)localObject1).<init>((Looper)localObject2, this);
    i = ((AssetStore.a)localObject1);
    localObject1 = new com/inmobi/ads/cache/AssetStore$2;
    ((AssetStore.2)localObject1).<init>(this);
    m = ((g.b)localObject1);
    int i1 = Build.VERSION.SDK_INT;
    int i2 = 23;
    if (i1 >= i2)
    {
      localObject1 = new com/inmobi/ads/cache/AssetStore$3;
      ((AssetStore.3)localObject1).<init>(this);
      n = ((g.b)localObject1);
    }
    localObject1 = new java/util/concurrent/ConcurrentHashMap;
    int i3 = 2;
    ((ConcurrentHashMap)localObject1).<init>(i3, 0.9F, i3);
    l = ((ConcurrentHashMap)localObject1);
  }
  
  public static AssetStore a()
  {
    AssetStore localAssetStore1 = o;
    if (localAssetStore1 == null) {
      synchronized (p)
      {
        localAssetStore1 = o;
        if (localAssetStore1 == null)
        {
          localAssetStore1 = new com/inmobi/ads/cache/AssetStore;
          localAssetStore1.<init>();
          o = localAssetStore1;
        }
      }
    }
    return localAssetStore2;
  }
  
  public static void a(a parama)
  {
    d.c(parama);
    File localFile = new java/io/File;
    parama = e;
    localFile.<init>(parama);
    boolean bool = localFile.exists();
    if (bool) {
      localFile.delete();
    }
  }
  
  private void a(a parama, boolean paramBoolean)
  {
    try
    {
      b(parama);
      String str = d;
      c(str);
      if (paramBoolean)
      {
        parama = d;
        a(parama);
        e();
        return;
      }
      parama = d;
      b(parama);
      f();
      return;
    }
    finally {}
  }
  
  private void a(String paramString)
  {
    int i1 = 0;
    try
    {
      for (;;)
      {
        Object localObject = q;
        int i2 = ((List)localObject).size();
        if (i1 >= i2) {
          break;
        }
        localObject = q;
        localObject = ((List)localObject).get(i1);
        localObject = (b)localObject;
        Set localSet1 = b;
        Set localSet2 = c;
        boolean bool = localSet1.contains(paramString);
        if (bool)
        {
          bool = localSet2.contains(paramString);
          if (!bool)
          {
            localSet1 = c;
            localSet1.add(paramString);
            int i3 = d + 1;
            d = i3;
          }
        }
        i1 += 1;
      }
      return;
    }
    finally {}
  }
  
  private void a(List paramList)
  {
    try
    {
      int i1 = paramList.size();
      int i2 = 0;
      while (i2 < i1)
      {
        List localList = q;
        Object localObject = paramList.get(i2);
        localList.remove(localObject);
        i2 += 1;
      }
      return;
    }
    finally {}
  }
  
  private boolean a(a parama, e parame)
  {
    a locala = parama;
    Object localObject1 = l;
    Object localObject3 = d;
    localObject1 = (a)((ConcurrentHashMap)localObject1).putIfAbsent(localObject3, parama);
    if (localObject1 == null)
    {
      c localc = new com/inmobi/ads/cache/c;
      localObject1 = parame;
      localc.<init>(parame);
      long l1 = g.c;
      localObject1 = g.e;
      boolean bool1 = com.inmobi.commons.core.utilities.d.a();
      int i2 = 8;
      int i3;
      if (!bool1)
      {
        l = i2;
        localObject1 = a;
        ((e)localObject1).a(parama);
        i3 = 1;
      }
      else
      {
        Object localObject4 = d;
        Object localObject5 = "";
        bool1 = ((String)localObject4).equals(localObject5);
        if (!bool1)
        {
          localObject4 = d;
          bool1 = URLUtil.isValidUrl((String)localObject4);
          if (bool1)
          {
            int i1 = ((List)localObject1).size();
            localObject4 = new String[i1];
            localObject1 = (String[])((List)localObject1).toArray((Object[])localObject4);
            try
            {
              long l2 = SystemClock.elapsedRealtime();
              localObject4 = new java/net/URL;
              localObject5 = d;
              ((URL)localObject4).<init>((String)localObject5);
              localObject4 = ((URL)localObject4).openConnection();
              localObject4 = FirebasePerfUrlConnection.instrument(localObject4);
              localObject4 = (URLConnection)localObject4;
              localObject4 = (HttpURLConnection)localObject4;
              localObject5 = "GET";
              ((HttpURLConnection)localObject4).setRequestMethod((String)localObject5);
              int i4 = 60000;
              ((HttpURLConnection)localObject4).setConnectTimeout(i4);
              ((HttpURLConnection)localObject4).setReadTimeout(i4);
              i4 = ((HttpURLConnection)localObject4).getResponseCode();
              int i5 = 400;
              if (i4 < i5)
              {
                localObject5 = ((HttpURLConnection)localObject4).getContentType();
                i5 = localObject1.length;
                int i6 = 0;
                localObject6 = null;
                while (i6 < i5)
                {
                  Object localObject7 = localObject1[i6];
                  if (localObject5 != null)
                  {
                    localObject8 = Locale.ENGLISH;
                    localObject8 = ((String)localObject7).toLowerCase((Locale)localObject8);
                    localObject7 = Locale.ENGLISH;
                    localObject7 = ((String)localObject5).toLowerCase((Locale)localObject7);
                    boolean bool4 = ((String)localObject8).equals(localObject7);
                    if (bool4)
                    {
                      i8 = 1;
                      break label382;
                    }
                  }
                  i6 += 1;
                }
                i8 = 0;
                localObject1 = null;
                label382:
                if (i8 == 0)
                {
                  i8 = 6;
                  l = i8;
                  c = 0;
                  localObject1 = a;
                  ((e)localObject1).a(locala);
                  i3 = 1;
                  break label1049;
                }
              }
              long l3 = ((HttpURLConnection)localObject4).getContentLength();
              int i8 = 7;
              long l4 = 0L;
              boolean bool3 = l3 < l4;
              if (!bool3)
              {
                bool3 = l3 < l1;
                if (bool3)
                {
                  l = i8;
                  c = 0;
                  localObject1 = a;
                  ((e)localObject1).a(locala);
                  i3 = 1;
                  break label1049;
                }
              }
              ((HttpURLConnection)localObject4).connect();
              localObject5 = d;
              Object localObject8 = com.inmobi.commons.a.a.a((String)localObject5);
              boolean bool2 = ((File)localObject8).exists();
              if (bool2) {
                ((File)localObject8).delete();
              }
              localObject5 = ((HttpURLConnection)localObject4).getInputStream();
              BufferedOutputStream localBufferedOutputStream = new java/io/BufferedOutputStream;
              Object localObject6 = new java/io/FileOutputStream;
              ((FileOutputStream)localObject6).<init>((File)localObject8);
              localBufferedOutputStream.<init>((OutputStream)localObject6);
              int i7 = 1024;
              localObject6 = new byte[i7];
              long l5 = l4;
              for (;;)
              {
                int i9 = ((InputStream)localObject5).read((byte[])localObject6);
                if (i9 <= 0) {
                  break;
                }
                l6 = i9;
                l5 += l6;
                boolean bool6 = l5 < l1;
                if (bool6)
                {
                  l = i8;
                  c = 0;
                  try
                  {
                    boolean bool5 = ((File)localObject8).exists();
                    if (bool5) {
                      ((File)localObject8).delete();
                    }
                    ((HttpURLConnection)localObject4).disconnect();
                    com.inmobi.commons.core.utilities.d.a(localBufferedOutputStream);
                  }
                  catch (Exception localException1)
                  {
                    localObject3 = com.inmobi.commons.core.a.a.a();
                    localObject9 = new com/inmobi/commons/core/e/a;
                    ((com.inmobi.commons.core.e.a)localObject9).<init>(localException1);
                    ((com.inmobi.commons.core.a.a)localObject3).a((com.inmobi.commons.core.e.a)localObject9);
                  }
                  l7 = SystemClock.elapsedRealtime();
                  l4 = l2;
                  c.a(l2, l5, l7);
                  localObject2 = a;
                  ((e)localObject2).a(locala);
                  i3 = 1;
                  break label1049;
                }
                localBufferedOutputStream.write((byte[])localObject6, 0, i9);
              }
              localBufferedOutputStream.flush();
              ((HttpURLConnection)localObject4).disconnect();
              com.inmobi.commons.core.utilities.d.a(localBufferedOutputStream);
              long l6 = SystemClock.elapsedRealtime();
              l4 = l2;
              long l7 = l6;
              c.a(l2, l5, l6);
              localObject2 = new com/inmobi/commons/core/network/d;
              ((com.inmobi.commons.core.network.d)localObject2).<init>();
              localObject3 = ((HttpURLConnection)localObject4).getHeaderFields();
              d = ((Map)localObject3);
              localObject3 = parama;
              Object localObject9 = localObject8;
              localObject3 = c.a(parama, (File)localObject8, l2, l6);
              k = ((String)localObject3);
              l6 -= l2;
              a = l6;
              localObject3 = a;
              localObject9 = ((File)localObject8).getAbsolutePath();
              ((e)localObject3).a((com.inmobi.commons.core.network.d)localObject2, (String)localObject9, locala);
            }
            catch (Exception localException2)
            {
              l = 0;
              localObject2 = a;
              ((e)localObject2).a(locala);
              i3 = 1;
              break label1049;
            }
            catch (IOException localIOException)
            {
              l = i2;
              localObject2 = a;
              ((e)localObject2).a(locala);
            }
            catch (ProtocolException localProtocolException)
            {
              l = i2;
              localObject2 = a;
              ((e)localObject2).a(locala);
            }
            catch (MalformedURLException localMalformedURLException)
            {
              i3 = 3;
              l = i3;
              localObject2 = a;
              ((e)localObject2).a(locala);
            }
            catch (FileNotFoundException localFileNotFoundException)
            {
              i3 = 4;
              l = i3;
              localObject2 = a;
              ((e)localObject2).a(locala);
            }
            catch (SocketTimeoutException localSocketTimeoutException)
            {
              i3 = 4;
              l = i3;
              localObject2 = a;
              ((e)localObject2).a(locala);
            }
            i3 = 1;
            break label1049;
          }
        }
        l = 3;
        Object localObject2 = a;
        ((e)localObject2).a(locala);
        i3 = 1;
      }
      label1049:
      return i3;
    }
    return false;
  }
  
  private void b(a parama)
  {
    int i1 = 0;
    try
    {
      for (;;)
      {
        Object localObject1 = q;
        int i2 = ((List)localObject1).size();
        if (i1 >= i2) {
          break;
        }
        localObject1 = q;
        localObject1 = ((List)localObject1).get(i1);
        localObject1 = (b)localObject1;
        Object localObject2 = b;
        String str = d;
        boolean bool = ((Set)localObject2).contains(str);
        if (bool)
        {
          localObject2 = a;
          bool = ((List)localObject2).contains(parama);
          if (!bool)
          {
            localObject1 = a;
            ((List)localObject1).add(parama);
          }
        }
        i1 += 1;
      }
      return;
    }
    finally {}
  }
  
  private void b(b paramb)
  {
    try
    {
      List localList = q;
      boolean bool = localList.contains(paramb);
      if (!bool)
      {
        localList = q;
        localList.add(paramb);
      }
      return;
    }
    finally {}
  }
  
  private void b(String paramString)
  {
    int i1 = 0;
    try
    {
      for (;;)
      {
        Object localObject = q;
        int i2 = ((List)localObject).size();
        if (i1 >= i2) {
          break;
        }
        localObject = q;
        localObject = ((List)localObject).get(i1);
        localObject = (b)localObject;
        Set localSet = b;
        boolean bool = localSet.contains(paramString);
        if (bool)
        {
          int i3 = e + 1;
          e = i3;
        }
        i1 += 1;
      }
      return;
    }
    finally {}
  }
  
  private void c(a parama)
  {
    File localFile = new java/io/File;
    Object localObject = e;
    localFile.<init>((String)localObject);
    long l1 = System.currentTimeMillis();
    long l2 = h;
    long l3 = f;
    l2 -= l3;
    l1 += l2;
    l2 = System.currentTimeMillis();
    l3 = b.e * 1000L;
    l2 += l3;
    l1 = Math.min(l1, l2);
    localObject = new com/inmobi/ads/cache/a$a;
    ((a.a)localObject).<init>();
    String str1 = d;
    String str2 = e;
    int i1 = b.a;
    long l4 = i;
    c = str1;
    d = str2;
    b = i1;
    g = l1;
    h = l4;
    a locala = ((a.a)localObject).a();
    l1 = System.currentTimeMillis();
    f = l1;
    d.b(locala);
    l1 = f;
    l2 = f;
    localObject = parama;
    parama = c.a(parama, localFile, l1, l2);
    k = parama;
    boolean bool = true;
    j = bool;
    a(locala, bool);
  }
  
  private void c(String paramString)
  {
    l.remove(paramString);
  }
  
  private void d(String paramString)
  {
    Object localObject1 = d.a(paramString);
    if (localObject1 != null)
    {
      boolean bool = ((a)localObject1).a();
      if (bool)
      {
        c((a)localObject1);
        return;
      }
    }
    localObject1 = new com/inmobi/ads/cache/a$a;
    ((a.a)localObject1).<init>();
    int i1 = b.a;
    b.b localb = b;
    long l1 = e;
    localObject1 = ((a.a)localObject1).a(paramString, i1, l1).a();
    Object localObject2 = d.a(paramString);
    if (localObject2 == null)
    {
      localObject2 = a;
      ((d)localObject2).a((a)localObject1);
    }
    localObject1 = h;
    localObject2 = new com/inmobi/ads/cache/AssetStore$6;
    ((AssetStore.6)localObject2).<init>(this, paramString);
    ((ExecutorService)localObject1).execute((Runnable)localObject2);
  }
  
  private void e()
  {
    try
    {
      ArrayList localArrayList = new java/util/ArrayList;
      localArrayList.<init>();
      int i1 = 0;
      for (;;)
      {
        Object localObject2 = q;
        int i2 = ((List)localObject2).size();
        if (i1 >= i2) {
          break;
        }
        localObject2 = q;
        localObject2 = ((List)localObject2).get(i1);
        localObject2 = (b)localObject2;
        int i3 = d;
        Object localObject3 = b;
        int i4 = ((Set)localObject3).size();
        if (i3 == i4) {
          try
          {
            localObject4 = ((b)localObject2).a();
            if (localObject4 != null) {
              ((f)localObject4).b((b)localObject2);
            }
            localArrayList.add(localObject2);
          }
          catch (Exception localException)
          {
            localException.getMessage();
            Object localObject4 = com.inmobi.commons.core.a.a.a();
            localObject3 = new com/inmobi/commons/core/e/a;
            ((com.inmobi.commons.core.e.a)localObject3).<init>(localException);
            ((com.inmobi.commons.core.a.a)localObject4).a((com.inmobi.commons.core.e.a)localObject3);
          }
        }
        i1 += 1;
      }
      a(localArrayList);
      return;
    }
    finally {}
  }
  
  private void f()
  {
    try
    {
      ArrayList localArrayList = new java/util/ArrayList;
      localArrayList.<init>();
      int i1 = 0;
      for (;;)
      {
        Object localObject2 = q;
        int i2 = ((List)localObject2).size();
        if (i1 >= i2) {
          break;
        }
        localObject2 = q;
        localObject2 = ((List)localObject2).get(i1);
        localObject2 = (b)localObject2;
        int i3 = e;
        if (i3 > 0) {
          try
          {
            localObject3 = ((b)localObject2).a();
            if (localObject3 != null) {
              ((f)localObject3).a((b)localObject2);
            }
            localArrayList.add(localObject2);
          }
          catch (Exception localException)
          {
            localException.getMessage();
            Object localObject3 = com.inmobi.commons.core.a.a.a();
            com.inmobi.commons.core.e.a locala = new com/inmobi/commons/core/e/a;
            locala.<init>(localException);
            ((com.inmobi.commons.core.a.a)localObject3).a(locala);
          }
        }
        i1 += 1;
      }
      a(localArrayList);
      return;
    }
    finally {}
  }
  
  private void g()
  {
    g.a();
    g.b localb = m;
    String str = "android.net.conn.CONNECTIVITY_CHANGE";
    g.a(localb, str);
    int i1 = Build.VERSION.SDK_INT;
    int i2 = 23;
    if (i1 >= i2)
    {
      g.a();
      localb = n;
      str = "android.os.action.DEVICE_IDLE_MODE_CHANGED";
      g.a(localb, str);
    }
  }
  
  private void h()
  {
    g.a();
    String str = "android.net.conn.CONNECTIVITY_CHANGE";
    g.b localb = m;
    g.a(str, localb);
    int i1 = Build.VERSION.SDK_INT;
    int i2 = 23;
    if (i1 >= i2)
    {
      g.a();
      str = "android.os.action.DEVICE_IDLE_MODE_CHANGED";
      localb = n;
      g.a(str, localb);
    }
  }
  
  public final void a(b paramb)
  {
    ExecutorService localExecutorService = c;
    AssetStore.5 local5 = new com/inmobi/ads/cache/AssetStore$5;
    local5.<init>(this, paramb);
    localExecutorService.execute(local5);
  }
  
  public final void a(com.inmobi.commons.core.configs.a parama)
  {
    parama = (com.inmobi.ads.b)parama;
    b.b localb = r;
    b = localb;
    parama = q;
    g = parama;
  }
  
  public final void b()
  {
    ??? = d;
    boolean bool1 = false;
    Object localObject2 = null;
    ((AtomicBoolean)???).set(false);
    boolean bool2 = com.inmobi.commons.core.utilities.d.a();
    if (!bool2)
    {
      g();
      h();
      return;
    }
    synchronized (e)
    {
      Object localObject4 = k;
      int i1 = 1;
      bool1 = ((AtomicBoolean)localObject4).compareAndSet(false, i1);
      if (bool1)
      {
        localObject2 = j;
        if (localObject2 == null)
        {
          localObject2 = new android/os/HandlerThread;
          localObject4 = "assetFetcher";
          ((HandlerThread)localObject2).<init>((String)localObject4);
          j = ((HandlerThread)localObject2);
          localObject2 = j;
          ((HandlerThread)localObject2).start();
        }
        localObject2 = i;
        if (localObject2 == null)
        {
          localObject2 = new com/inmobi/ads/cache/AssetStore$a;
          localObject4 = j;
          localObject4 = ((HandlerThread)localObject4).getLooper();
          ((AssetStore.a)localObject2).<init>((Looper)localObject4, this);
          i = ((AssetStore.a)localObject2);
        }
        localObject2 = new java/util/ArrayList;
        ((ArrayList)localObject2).<init>();
        localObject4 = b;
        int i2 = b;
        localObject4 = d.a(i2);
        boolean bool3 = ((List)localObject4).isEmpty();
        if (bool3)
        {
          c();
          return;
        }
        localObject4 = ((List)localObject4).iterator();
        Object localObject5;
        boolean bool4;
        do
        {
          int i3;
          int i4;
          do
          {
            bool3 = ((Iterator)localObject4).hasNext();
            if (!bool3) {
              break;
            }
            localObject5 = ((Iterator)localObject4).next();
            localObject5 = (a)localObject5;
            i3 = ((List)localObject2).indexOf(localObject5);
            i4 = -1;
          } while (i3 != i4);
          bool4 = ((a)localObject5).a();
        } while (bool4);
        ((List)localObject2).add(localObject5);
        bool1 = ((List)localObject2).isEmpty();
        if (bool1)
        {
          c();
        }
        else
        {
          g();
          h();
          localObject2 = i;
          ((AssetStore.a)localObject2).sendEmptyMessage(i1);
        }
      }
      return;
    }
  }
  
  public final void c()
  {
    synchronized (e)
    {
      Object localObject2 = k;
      ((AtomicBoolean)localObject2).set(false);
      localObject2 = l;
      ((ConcurrentHashMap)localObject2).clear();
      localObject2 = j;
      if (localObject2 != null)
      {
        localObject2 = j;
        localObject2 = ((HandlerThread)localObject2).getLooper();
        ((Looper)localObject2).quit();
        localObject2 = j;
        ((Thread)localObject2).interrupt();
        localObject2 = null;
        j = null;
        i = null;
      }
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.cache.AssetStore
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */