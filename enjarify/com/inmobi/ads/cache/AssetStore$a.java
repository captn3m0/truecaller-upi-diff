package com.inmobi.ads.cache;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.inmobi.ads.b.b;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.List;

final class AssetStore$a
  extends Handler
{
  private WeakReference a;
  private final e b;
  
  AssetStore$a(Looper paramLooper, AssetStore paramAssetStore)
  {
    super(paramLooper);
    paramLooper = new java/lang/ref/WeakReference;
    paramLooper.<init>(paramAssetStore);
    a = paramLooper;
    paramLooper = new com/inmobi/ads/cache/AssetStore$a$1;
    paramLooper.<init>(this);
    b = paramLooper;
  }
  
  private void a()
  {
    int i = 3;
    try
    {
      sendEmptyMessage(i);
      return;
    }
    catch (Exception localException)
    {
      AssetStore.d();
      localException.getMessage();
    }
  }
  
  private void a(a parama)
  {
    try
    {
      Message localMessage = Message.obtain();
      int i = 4;
      what = i;
      obj = parama;
      sendMessage(localMessage);
      return;
    }
    catch (Exception parama)
    {
      AssetStore.d();
      parama.getMessage();
    }
  }
  
  private void b()
  {
    int i = 1;
    try
    {
      sendEmptyMessage(i);
      return;
    }
    catch (Exception localException)
    {
      AssetStore.d();
      localException.getMessage();
    }
  }
  
  public final void handleMessage(Message paramMessage)
  {
    try
    {
      int i = what;
      int k;
      Object localObject2;
      boolean bool2;
      switch (i)
      {
      default: 
        return;
      case 4: 
        paramMessage = obj;
        paramMessage = (a)paramMessage;
        localObject1 = a;
        localObject1 = ((WeakReference)localObject1).get();
        localObject1 = (AssetStore)localObject1;
        if (localObject1 != null)
        {
          AssetStore.i((AssetStore)localObject1);
          d.c(paramMessage);
        }
        b();
      case 3: 
        b();
        break;
      case 2: 
        localObject1 = a;
        localObject1 = ((WeakReference)localObject1).get();
        if (localObject1 != null)
        {
          localObject1 = a;
          localObject1 = ((WeakReference)localObject1).get();
          localObject1 = (AssetStore)localObject1;
          paramMessage = obj;
          paramMessage = (String)paramMessage;
          AssetStore.i((AssetStore)localObject1);
          paramMessage = d.b(paramMessage);
          if (paramMessage == null)
          {
            b();
            return;
          }
          boolean bool1 = paramMessage.a();
          if (!bool1)
          {
            AssetStore.h((AssetStore)localObject1);
            int j = c;
            k = 0;
            localObject2 = null;
            if (j == 0)
            {
              j = 11;
              l = j;
              AssetStore.a((AssetStore)localObject1, paramMessage, false);
              a(paramMessage);
              return;
            }
            bool2 = com.inmobi.commons.core.utilities.d.a();
            if (!bool2)
            {
              AssetStore.a((AssetStore)localObject1, paramMessage, false);
              AssetStore.d((AssetStore)localObject1);
              return;
            }
            localObject3 = b;
            boolean bool3 = AssetStore.a((AssetStore)localObject1, paramMessage, (e)localObject3);
            if (bool3)
            {
              AssetStore.d();
              AssetStore.d();
              return;
            }
            AssetStore.d();
            b();
            return;
          }
          AssetStore.d();
          a();
          bool2 = true;
          AssetStore.a((AssetStore)localObject1, paramMessage, bool2);
          return;
        }
        break;
      case 1: 
        paramMessage = a;
        paramMessage = paramMessage.get();
        if (paramMessage != null)
        {
          paramMessage = a;
          paramMessage = paramMessage.get();
          paramMessage = (AssetStore)paramMessage;
          localObject1 = AssetStore.h(paramMessage);
          bool2 = false;
          localObject3 = null;
          if (localObject1 == null)
          {
            localObject1 = new com/inmobi/ads/b;
            ((com.inmobi.ads.b)localObject1).<init>();
            localObject2 = com.inmobi.commons.core.configs.b.a();
            ((com.inmobi.commons.core.configs.b)localObject2).a((com.inmobi.commons.core.configs.a)localObject1, null);
            localObject1 = r;
          }
          AssetStore.i(paramMessage);
          k = b;
          localObject2 = d.a(k);
          localObject2 = ((List)localObject2).iterator();
          Object localObject4;
          boolean bool5;
          do
          {
            boolean bool4 = ((Iterator)localObject2).hasNext();
            if (!bool4) {
              break;
            }
            localObject4 = ((Iterator)localObject2).next();
            localObject4 = (a)localObject4;
            bool5 = ((a)localObject4).a();
          } while (bool5);
          localObject3 = localObject4;
          if (localObject3 == null)
          {
            AssetStore.d();
            AssetStore.d(paramMessage);
            return;
          }
          AssetStore.d();
          paramMessage = Message.obtain();
          k = 2;
          what = k;
          localObject2 = d;
          obj = localObject2;
          long l1 = System.currentTimeMillis();
          long l2 = f;
          l1 -= l2;
          try
          {
            l2 = b * 1000;
            bool2 = l1 < l2;
            if (bool2)
            {
              i = b * 1000;
              long l3 = i - l1;
              sendMessageDelayed(paramMessage, l3);
              return;
            }
            sendMessage(paramMessage);
            return;
          }
          catch (Exception paramMessage)
          {
            AssetStore.d();
            paramMessage.getMessage();
            return;
          }
        }
        break;
      }
      return;
    }
    catch (Exception paramMessage)
    {
      AssetStore.d();
      Object localObject1 = com.inmobi.commons.core.a.a.a();
      Object localObject3 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject3).<init>(paramMessage);
      ((com.inmobi.commons.core.a.a)localObject1).a((com.inmobi.commons.core.e.a)localObject3);
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.cache.AssetStore.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */