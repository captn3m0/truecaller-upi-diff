package com.inmobi.ads.cache;

import android.net.Uri;
import com.inmobi.signals.o;
import java.io.File;
import org.json.JSONException;
import org.json.JSONObject;

public final class c
{
  private static final String b = "c";
  e a;
  
  c(e parame)
  {
    a = parame;
  }
  
  static String a(a parama, File paramFile, long paramLong1, long paramLong2)
  {
    JSONObject localJSONObject = new org/json/JSONObject;
    localJSONObject.<init>();
    Object localObject = "url";
    try
    {
      parama = d;
      localJSONObject.put((String)localObject, parama);
      parama = "saved_url";
      localObject = Uri.fromFile(paramFile);
      localJSONObject.put(parama, localObject);
      parama = "size_in_bytes";
      long l = paramFile.length();
      localJSONObject.put(parama, l);
      parama = "download_started_at";
      localJSONObject.put(parama, paramLong1);
      parama = "download_ended_at";
      localJSONObject.put(parama, paramLong2);
    }
    catch (JSONException parama)
    {
      paramFile = com.inmobi.commons.core.a.a.a();
      com.inmobi.commons.core.e.a locala = new com/inmobi/commons/core/e/a;
      locala.<init>(parama);
      paramFile.a(locala);
    }
    return localJSONObject.toString().replace("\"", "\\\"");
  }
  
  static void a(long paramLong1, long paramLong2, long paramLong3)
  {
    try
    {
      o localo = o.a();
      long l = 0L;
      localo.a(l);
      localo = o.a();
      localo.b(paramLong2);
      localObject = o.a();
      paramLong3 -= paramLong1;
      ((o)localObject).c(paramLong3);
      return;
    }
    catch (Exception localException)
    {
      localException.getMessage();
      com.inmobi.commons.core.a.a locala = com.inmobi.commons.core.a.a.a();
      Object localObject = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject).<init>(localException);
      locala.a((com.inmobi.commons.core.e.a)localObject);
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.cache.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */