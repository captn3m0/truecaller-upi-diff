package com.inmobi.ads.cache;

import com.inmobi.commons.core.network.d;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

public final class a$a
{
  int a;
  int b;
  String c;
  String d;
  long e;
  long f;
  long g;
  long h;
  
  public a$a()
  {
    Random localRandom = new java/util/Random;
    localRandom.<init>();
    int i = localRandom.nextInt() & -1 >>> 1;
    a = i;
    long l = System.currentTimeMillis();
    e = l;
    l = System.currentTimeMillis();
    f = l;
  }
  
  private static long a(String paramString)
  {
    Object localObject1 = new java/text/SimpleDateFormat;
    Object localObject2 = "EEE,dd MMM yyyy HH:mm:ss z";
    Locale localLocale = Locale.ENGLISH;
    ((SimpleDateFormat)localObject1).<init>((String)localObject2, localLocale);
    try
    {
      paramString = ((DateFormat)localObject1).parse(paramString);
      return paramString.getTime();
    }
    catch (ParseException paramString)
    {
      localObject1 = com.inmobi.commons.core.a.a.a();
      localObject2 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject2).<init>(paramString);
      ((com.inmobi.commons.core.a.a)localObject1).a((com.inmobi.commons.core.e.a)localObject2);
    }
    return 0L;
  }
  
  public final a a(String paramString, int paramInt, long paramLong)
  {
    c = paramString;
    b = paramInt;
    long l = System.currentTimeMillis() + paramLong;
    g = l;
    return this;
  }
  
  public final a a(String paramString1, String paramString2, d paramd, int paramInt, long paramLong)
  {
    a locala = this;
    Object localObject1 = paramd;
    Object localObject3 = d;
    long l1 = System.currentTimeMillis();
    new ArrayList();
    localObject1 = (List)((Map)localObject3).get("Date");
    long l2 = 0L;
    int i;
    if (localObject1 != null)
    {
      i = ((List)localObject1).size();
      if (i > 0)
      {
        localObject1 = (String)((List)((Map)localObject3).get("Date")).get(0);
        if (localObject1 != null)
        {
          l3 = a((String)localObject1);
          break label101;
        }
      }
    }
    long l3 = l2;
    label101:
    localObject1 = (List)((Map)localObject3).get("Cache-Control");
    int j;
    if (localObject1 != null)
    {
      i = ((List)localObject1).size();
      if (i > 0)
      {
        localObject1 = (String)((List)((Map)localObject3).get("Cache-Control")).get(0);
        if (localObject1 != null)
        {
          String[] arrayOfString = ((String)localObject1).split(",");
          l4 = l2;
          l5 = l2;
          j = 0;
          localObject4 = null;
          k = 0;
          for (;;)
          {
            i = arrayOfString.length;
            if (j >= i) {
              break;
            }
            localObject1 = arrayOfString[j].trim();
            localObject5 = "no-cache";
            boolean bool2 = ((String)localObject1).equals(localObject5);
            if (!bool2)
            {
              localObject5 = "no-store";
              bool2 = ((String)localObject1).equals(localObject5);
              if (!bool2)
              {
                localObject5 = "max-age=";
                bool2 = ((String)localObject1).startsWith((String)localObject5);
                if (bool2)
                {
                  int m = 8;
                  try
                  {
                    localObject1 = ((String)localObject1).substring(m);
                    l4 = Long.parseLong((String)localObject1);
                  }
                  catch (Exception localException1)
                  {
                    a.b();
                    localException1.getMessage();
                  }
                }
                else
                {
                  localObject5 = "stale-while-revalidate=";
                  boolean bool3 = localException1.startsWith((String)localObject5);
                  if (bool3)
                  {
                    int n = 23;
                    try
                    {
                      String str = localException1.substring(n);
                      l5 = Long.parseLong(str);
                    }
                    catch (Exception localException2)
                    {
                      a.b();
                      localException2.getMessage();
                    }
                  }
                  else
                  {
                    localObject5 = "must-revalidate";
                    boolean bool4 = localException2.equals(localObject5);
                    if (!bool4)
                    {
                      localObject5 = "proxy-revalidate";
                      bool1 = localException2.equals(localObject5);
                      if (!bool1) {}
                    }
                    else
                    {
                      k = 1;
                    }
                  }
                }
              }
            }
            j += 1;
          }
          bool1 = true;
          break label429;
        }
      }
    }
    long l4 = l2;
    long l5 = l2;
    boolean bool1 = false;
    Object localObject2 = null;
    int k = 0;
    label429:
    Object localObject5 = (List)((Map)localObject3).get("Expires");
    if (localObject5 != null)
    {
      int i1 = ((List)localObject5).size();
      if (i1 > 0)
      {
        localObject5 = (String)((List)((Map)localObject3).get("Expires")).get(0);
        if (localObject5 != null)
        {
          l6 = a((String)localObject5);
          break label504;
        }
      }
    }
    long l6 = l2;
    label504:
    Object localObject4 = (List)((Map)localObject3).get("Last-Modified");
    if (localObject4 != null)
    {
      j = ((List)localObject4).size();
      if (j > 0)
      {
        localObject4 = (String)((List)((Map)localObject3).get("Last-Modified")).get(0);
        if (localObject4 != null) {
          a((String)localObject4);
        }
      }
    }
    localObject4 = (List)((Map)localObject3).get("ETag");
    if (localObject4 != null)
    {
      j = ((List)localObject4).size();
      if (j > 0)
      {
        localObject4 = "ETag";
        localObject3 = (List)((Map)localObject3).get(localObject4);
        ((List)localObject3).get(0);
      }
    }
    long l7 = 1000L;
    long l8;
    if (bool1)
    {
      l4 *= l7;
      l2 = l1 + l4;
      if (k != 0)
      {
        l5 = l2;
      }
      else
      {
        Long.signum(l5);
        l5 *= l7;
        l5 = l2 + l5;
      }
      localObject3 = paramString1;
      l8 = l5;
    }
    else
    {
      bool1 = l3 < l2;
      if (bool1)
      {
        bool1 = l6 < l3;
        if (!bool1)
        {
          l6 -= l3;
          l2 = l1 + l6;
          localObject3 = paramString1;
          l8 = l2;
          break label752;
        }
      }
      localObject3 = paramString1;
      l8 = l2;
    }
    label752:
    c = ((String)localObject3);
    localObject3 = paramString2;
    d = paramString2;
    b = paramInt;
    long l9 = paramLong * l7;
    l1 += l9;
    g = l1;
    h = l2;
    long l10 = Math.min(g, l8);
    g = l10;
    return locala;
  }
  
  public final a a()
  {
    a locala = new com/inmobi/ads/cache/a;
    int i = a;
    String str1 = c;
    String str2 = d;
    int j = b;
    long l1 = e;
    long l2 = f;
    long l3 = g;
    long l4 = h;
    locala.<init>(i, str1, str2, j, l1, l2, l3, l4);
    return locala;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.cache.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */