package com.inmobi.ads.cache;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class b
{
  public List a;
  Set b;
  Set c;
  int d;
  int e;
  public String f;
  public String g;
  public String h;
  private String i;
  private final WeakReference j;
  
  public b(String paramString1, String paramString2, Set paramSet, f paramf)
  {
    i = paramString1;
    f = paramString2;
    b = paramSet;
    paramString1 = new java/lang/ref/WeakReference;
    paramString1.<init>(paramf);
    j = paramString1;
    paramString1 = new java/util/ArrayList;
    paramString1.<init>();
    a = paramString1;
    paramString1 = new java/util/HashSet;
    paramString1.<init>();
    c = paramString1;
  }
  
  public b(String paramString1, Set paramSet, f paramf, String paramString2)
  {
    i = paramString1;
    h = paramString2;
    b = paramSet;
    paramString1 = new java/lang/ref/WeakReference;
    paramString1.<init>(paramf);
    j = paramString1;
    paramString1 = new java/util/ArrayList;
    paramString1.<init>();
    a = paramString1;
    paramString1 = new java/util/HashSet;
    paramString1.<init>();
    c = paramString1;
  }
  
  public final f a()
  {
    return (f)j.get();
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("AdAssetBatch{mAssetUrls=");
    Set localSet = b;
    localStringBuilder.append(localSet);
    localStringBuilder.append(", mBatchDownloadSuccessCount=");
    int k = d;
    localStringBuilder.append(k);
    localStringBuilder.append(", mBatchDownloadFailureCount=");
    k = e;
    localStringBuilder.append(k);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.cache.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */