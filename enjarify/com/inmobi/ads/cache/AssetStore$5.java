package com.inmobi.ads.cache;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

final class AssetStore$5
  implements Runnable
{
  AssetStore$5(AssetStore paramAssetStore, b paramb) {}
  
  public final void run()
  {
    Object localObject1 = b;
    Object localObject2 = a;
    AssetStore.a((AssetStore)localObject1, (b)localObject2);
    AssetStore.d();
    a.b.size();
    localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>();
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Object localObject3 = a.b.iterator();
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject3).hasNext();
      if (!bool1) {
        break;
      }
      String str1 = (String)((Iterator)localObject3).next();
      String str2 = str1.trim();
      int i = str2.length();
      if (i > 0)
      {
        String str3 = "mp4";
        boolean bool3 = str1.contains(str3);
        if (bool3)
        {
          ((List)localObject2).add(str1);
          continue;
        }
      }
      if (i > 0)
      {
        str2 = "gif";
        boolean bool2 = str1.endsWith(str2);
        if (bool2)
        {
          localArrayList.add(str1);
          continue;
        }
      }
      ((List)localObject1).add(str1);
    }
    localObject3 = b;
    AssetStore.a((AssetStore)localObject3, (List)localObject1);
    AssetStore.e(b);
    AssetStore.f(b);
    localObject1 = ((List)localObject2).iterator();
    for (;;)
    {
      boolean bool4 = ((Iterator)localObject1).hasNext();
      if (!bool4) {
        break;
      }
      localObject2 = (String)((Iterator)localObject1).next();
      localObject3 = b;
      AssetStore.b((AssetStore)localObject3, (String)localObject2);
    }
    AssetStore.b(b, localArrayList);
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.cache.AssetStore.5
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */