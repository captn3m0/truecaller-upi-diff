package com.inmobi.ads;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

final class NativeViewFactory$12
  extends NativeViewFactory.c
{
  NativeViewFactory$12(NativeViewFactory paramNativeViewFactory)
  {
    super(paramNativeViewFactory);
  }
  
  protected final View a(Context paramContext)
  {
    NativeViewFactory.b localb = new com/inmobi/ads/NativeViewFactory$b;
    paramContext = paramContext.getApplicationContext();
    localb.<init>(paramContext);
    return localb;
  }
  
  protected final void a(View paramView, ag paramag, b paramb)
  {
    super.a(paramView, paramag, paramb);
    NativeViewFactory.a((TextView)paramView, paramag);
  }
  
  public final boolean a(View paramView)
  {
    boolean bool = paramView instanceof TextView;
    if (!bool) {
      return false;
    }
    NativeViewFactory.a((TextView)paramView);
    return super.a((View)paramView);
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.NativeViewFactory.12
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */