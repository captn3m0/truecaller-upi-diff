package com.inmobi.ads;

import android.content.ContentValues;
import java.util.Map;

public class bf
{
  private static final String g = "bf";
  long a;
  String b;
  Map c;
  String d;
  String e;
  InMobiAdRequest.MonetizationContext f;
  
  private bf(long paramLong, String paramString1, String paramString2)
  {
    InMobiAdRequest.MonetizationContext localMonetizationContext = InMobiAdRequest.MonetizationContext.MONETIZATION_CONTEXT_ACTIVITY;
    f = localMonetizationContext;
    a = paramLong;
    b = paramString1;
    e = paramString2;
    String str = b;
    if (str == null)
    {
      str = "";
      b = str;
    }
  }
  
  public bf(ContentValues paramContentValues)
  {
    Object localObject = InMobiAdRequest.MonetizationContext.MONETIZATION_CONTEXT_ACTIVITY;
    f = ((InMobiAdRequest.MonetizationContext)localObject);
    long l = paramContentValues.getAsLong("placement_id").longValue();
    a = l;
    localObject = paramContentValues.getAsString("tp_key");
    b = ((String)localObject);
    localObject = paramContentValues.getAsString("ad_type");
    e = ((String)localObject);
    paramContentValues = InMobiAdRequest.MonetizationContext.fromValue(paramContentValues.getAsString("m10_context"));
    f = paramContentValues;
  }
  
  public static bf a(long paramLong, Map paramMap, String paramString1, String paramString2)
  {
    bf localbf = new com/inmobi/ads/bf;
    String str = g.a(paramMap);
    localbf.<init>(paramLong, str, paramString1);
    d = paramString2;
    c = paramMap;
    return localbf;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this == paramObject) {
      return bool1;
    }
    if (paramObject != null)
    {
      Object localObject1 = getClass();
      Object localObject2 = paramObject.getClass();
      if (localObject1 == localObject2)
      {
        paramObject = (bf)paramObject;
        long l1 = a;
        long l2 = a;
        boolean bool2 = l1 < l2;
        if (!bool2)
        {
          localObject1 = f;
          localObject2 = f;
          if (localObject1 == localObject2)
          {
            localObject1 = b;
            localObject2 = b;
            boolean bool3 = ((String)localObject1).equals(localObject2);
            if (bool3)
            {
              localObject1 = e;
              paramObject = e;
              boolean bool4 = ((String)localObject1).equals(paramObject);
              if (bool4) {
                return bool1;
              }
            }
          }
        }
        return false;
      }
    }
    return false;
  }
  
  public int hashCode()
  {
    long l1 = a;
    long l2 = l1 >>> 32;
    int i = (int)(l1 ^ l2) * 31;
    int j = e.hashCode();
    i = (i + j) * 30;
    j = f.hashCode();
    return i + j;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.bf
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */