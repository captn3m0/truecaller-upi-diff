package com.inmobi.ads;

import android.content.Context;
import android.view.View;
import com.inmobi.rendering.RenderView;

final class NativeViewFactory$3
  extends NativeViewFactory.c
{
  NativeViewFactory$3(NativeViewFactory paramNativeViewFactory)
  {
    super(paramNativeViewFactory);
  }
  
  protected final View a(Context paramContext)
  {
    RenderView localRenderView = new com/inmobi/rendering/RenderView;
    paramContext = paramContext.getApplicationContext();
    AdContainer.RenderingProperties localRenderingProperties = new com/inmobi/ads/AdContainer$RenderingProperties;
    AdContainer.RenderingProperties.PlacementType localPlacementType = AdContainer.RenderingProperties.PlacementType.PLACEMENT_TYPE_INLINE;
    localRenderingProperties.<init>(localPlacementType);
    localRenderView.<init>(paramContext, localRenderingProperties, null, null);
    return localRenderView;
  }
  
  protected final void a(View paramView, ag paramag, b paramb)
  {
    super.a(paramView, paramag, paramb);
    NativeViewFactory.a((RenderView)paramView, paramag, paramb);
  }
  
  public final boolean a(View paramView)
  {
    boolean bool1 = paramView instanceof RenderView;
    if (bool1)
    {
      Object localObject = paramView;
      localObject = (RenderView)paramView;
      bool1 = v;
      if (!bool1)
      {
        boolean bool2 = super.a((View)paramView);
        if (bool2) {
          return true;
        }
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.NativeViewFactory.3
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */