package com.inmobi.ads;

import android.content.Context;
import android.graphics.Point;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Looper;
import android.view.TextureView.SurfaceTextureListener;
import android.view.View;
import android.view.View.OnAttachStateChangeListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.RelativeLayout.LayoutParams;
import com.inmobi.rendering.RenderView;
import com.inmobi.rendering.RenderView.a;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

final class aq
  implements NativeScrollableContainer.a
{
  private static final String e = "aq";
  private static Handler n;
  bd a;
  int b = 0;
  final l c;
  NativeViewFactory d;
  private final WeakReference f;
  private final ak g;
  private final ad h;
  private final b i;
  private aq.c j;
  private aq.a k;
  private aq.b l;
  private au m;
  private boolean o = false;
  private RenderView p;
  
  static
  {
    Handler localHandler = new android/os/Handler;
    Looper localLooper = Looper.getMainLooper();
    localHandler.<init>(localLooper);
    n = localHandler;
  }
  
  aq(Context paramContext, b paramb, ad paramad, ak paramak, aq.c paramc, aq.a parama, aq.b paramb1)
  {
    WeakReference localWeakReference = new java/lang/ref/WeakReference;
    localWeakReference.<init>(paramContext);
    f = localWeakReference;
    h = paramad;
    g = paramak;
    j = paramc;
    k = parama;
    l = paramb1;
    paramad = new com/inmobi/ads/l;
    paramad.<init>();
    c = paramad;
    i = paramb;
    paramContext = NativeViewFactory.a(paramContext);
    d = paramContext;
  }
  
  private at a(at paramat, ViewGroup paramViewGroup)
  {
    Object localObject1;
    Object localObject2;
    if (paramat == null)
    {
      localObject1 = d;
      Context localContext = a();
      localObject2 = g.d;
      b localb = i;
      localObject1 = (at)((NativeViewFactory)localObject1).a(localContext, (ag)localObject2, localb);
    }
    else
    {
      localObject1 = paramat;
    }
    if ((localObject1 != null) && (paramat != null))
    {
      paramat = ((at)localObject1).getParent();
      boolean bool = paramat instanceof ViewGroup;
      if (bool)
      {
        paramat = (ViewGroup)paramat;
        paramat.removeView((View)localObject1);
      }
      paramat = d;
      int i1 = ((ViewGroup)localObject1).getChildCount() + -1;
      while (i1 >= 0)
      {
        localObject2 = ((ViewGroup)localObject1).getChildAt(i1);
        ((ViewGroup)localObject1).removeViewAt(i1);
        paramat.a((View)localObject2);
        i1 += -1;
      }
      paramat = g.d.c;
      NativeViewFactory.a((View)localObject1, paramat);
    }
    NativeViewFactory.b(g.d.c.a.x);
    paramat = NativeViewFactory.a(g.d, paramViewGroup);
    ((at)localObject1).setLayoutParams(paramat);
    return (at)localObject1;
  }
  
  private void a(View paramView, ag paramag)
  {
    List localList = c.a(paramView, paramag);
    Object localObject;
    if (localList == null)
    {
      localObject = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_CREATIVE_VIEW;
      Iterator localIterator = u.iterator();
      NativeTracker.TrackerEventType localTrackerEventType;
      do
      {
        boolean bool = localIterator.hasNext();
        if (!bool) {
          break;
        }
        localTrackerEventType = nextb;
      } while (localObject != localTrackerEventType);
      int i1 = 1;
      break label78;
      i1 = 0;
      localObject = null;
      label78:
      if (i1 == 0) {}
    }
    else
    {
      localObject = new com/inmobi/ads/aq$3;
      ((aq.3)localObject).<init>(this, localList, paramag);
      paramView.addOnAttachStateChangeListener((View.OnAttachStateChangeListener)localObject);
    }
  }
  
  private void a(ag paramag, View paramView)
  {
    boolean bool = h;
    if (bool)
    {
      aq.6 local6 = new com/inmobi/ads/aq$6;
      local6.<init>(this, paramag);
      paramView.setOnClickListener(local6);
    }
  }
  
  private int d()
  {
    int i1 = b;
    if (i1 == 0) {
      return 8388611;
    }
    ak localak = g;
    i1 = localak.b();
    int i2 = 1;
    i1 -= i2;
    int i3 = b;
    if (i1 == i3) {
      return 8388613;
    }
    return i2;
  }
  
  public final int a(int paramInt)
  {
    b = paramInt;
    aq.c localc = j;
    ai localai = g.a(paramInt);
    localc.a(paramInt, localai);
    return d();
  }
  
  public final Context a()
  {
    return (Context)f.get();
  }
  
  public final ViewGroup a(ViewGroup paramViewGroup, ai paramai)
  {
    Object localObject = d;
    Context localContext = a();
    b localb = i;
    localObject = (ViewGroup)((NativeViewFactory)localObject).a(localContext, paramai, localb);
    if (localObject != null)
    {
      paramViewGroup = NativeViewFactory.a(paramai, paramViewGroup);
      ((ViewGroup)localObject).setLayoutParams(paramViewGroup);
    }
    return (ViewGroup)localObject;
  }
  
  public final at a(at paramat, ViewGroup paramViewGroup, RenderView paramRenderView)
  {
    p = paramRenderView;
    paramat = a(paramat, paramViewGroup);
    boolean bool = o;
    if (!bool)
    {
      paramViewGroup = g.d;
      b(paramat, paramViewGroup);
    }
    return paramat;
  }
  
  final ViewGroup b(ViewGroup paramViewGroup, ai paramai)
  {
    aq localaq = this;
    ViewGroup localViewGroup = paramViewGroup;
    Object localObject1 = paramai;
    a(paramai, paramViewGroup);
    Iterator localIterator = paramai.iterator();
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      localObject1 = localIterator.next();
      Object localObject4 = localObject1;
      localObject4 = (ag)localObject1;
      localObject1 = "CONTAINER";
      Object localObject5 = b;
      Object localObject6;
      int i2;
      Object localObject7;
      int i3;
      int i4;
      if (localObject1 == localObject5)
      {
        localObject1 = d;
        localObject5 = "card_scrollable";
        bool1 = ((String)localObject1).equalsIgnoreCase((String)localObject5);
        if (bool1)
        {
          localObject1 = d;
          localObject5 = a();
          localObject6 = i;
          localObject1 = (NativeScrollableContainer)((NativeViewFactory)localObject1).a((Context)localObject5, (ag)localObject4, (b)localObject6);
          if (localObject1 != null)
          {
            i2 = ((NativeScrollableContainer)localObject1).getType();
            localObject6 = g;
            localObject5 = av.a(i2, (ak)localObject6, localaq);
            m = ((au)localObject5);
            localObject7 = m;
            if (localObject7 != null)
            {
              localObject6 = localObject4;
              localObject6 = (ai)localObject4;
              i3 = b;
              i4 = d();
              localObject5 = localObject1;
              ((NativeScrollableContainer)localObject1).a((ai)localObject6, (au)localObject7, i3, i4, this);
              localObject5 = NativeViewFactory.a((ag)localObject4, localViewGroup);
              ((NativeScrollableContainer)localObject1).setLayoutParams((ViewGroup.LayoutParams)localObject5);
              localaq.a((View)localObject1, (ag)localObject4);
              localViewGroup.addView((View)localObject1);
            }
          }
        }
        else
        {
          localObject1 = d;
          localObject5 = a();
          localObject6 = i;
          localObject1 = (ViewGroup)((NativeViewFactory)localObject1).a((Context)localObject5, (ag)localObject4, (b)localObject6);
          if (localObject1 != null)
          {
            localObject5 = localObject4;
            localObject5 = (ai)localObject4;
            localObject1 = localaq.b((ViewGroup)localObject1, (ai)localObject5);
            localObject5 = NativeViewFactory.a((ag)localObject4, localViewGroup);
            ((ViewGroup)localObject1).setLayoutParams((ViewGroup.LayoutParams)localObject5);
            localaq.a((View)localObject1, (ag)localObject4);
            localViewGroup.addView((View)localObject1);
          }
        }
      }
      else
      {
        localObject1 = "WEBVIEW";
        localObject5 = b;
        bool1 = ((String)localObject1).equals(localObject5);
        i2 = 0;
        localObject5 = null;
        if (bool1)
        {
          localObject1 = localObject4;
          localObject1 = (bc)localObject4;
          boolean bool3 = A;
          if (bool3)
          {
            localObject6 = p;
            if (localObject6 != null)
            {
              localObject1 = ((View)localObject6).getParent();
              if (localObject1 != null)
              {
                localObject1 = (ViewGroup)((View)localObject6).getParent();
                ((ViewGroup)localObject1).removeView((View)localObject6);
              }
              p = null;
              localObject5 = localObject6;
              break label520;
            }
          }
          localObject6 = "UNKNOWN";
          localObject1 = z;
          bool1 = ((String)localObject6).equals(localObject1);
          if (!bool1) {}
        }
        else
        {
          localObject1 = "IMAGE";
          localObject6 = b;
          bool1 = ((String)localObject1).equals(localObject6);
          if (bool1)
          {
            localObject1 = e;
            if (localObject1 == null) {
              continue;
            }
          }
        }
        label520:
        if (localObject5 == null)
        {
          localObject1 = d;
          localObject5 = a();
          localObject6 = i;
          localObject5 = ((NativeViewFactory)localObject1).a((Context)localObject5, (ag)localObject4, (b)localObject6);
        }
        if (localObject5 != null)
        {
          localObject1 = new java/lang/ref/WeakReference;
          ((WeakReference)localObject1).<init>(localObject5);
          int i5 = o;
          int i7 = -1;
          Object localObject8;
          long l1;
          if (i5 != i7)
          {
            i5 = 4;
            ((View)localObject5).setVisibility(i5);
            localObject6 = n;
            localObject8 = new com/inmobi/ads/aq$4;
            ((aq.4)localObject8).<init>(localaq, (WeakReference)localObject1);
            i1 = o * 1000;
            l1 = i1;
            ((Handler)localObject6).postDelayed((Runnable)localObject8, l1);
          }
          else
          {
            i5 = p;
            if (i5 != i7)
            {
              localObject6 = n;
              localObject8 = new com/inmobi/ads/aq$5;
              ((aq.5)localObject8).<init>(localaq, (WeakReference)localObject1);
              i1 = p * 1000;
              l1 = i1;
              ((Handler)localObject6).postDelayed((Runnable)localObject8, l1);
            }
          }
          localObject1 = NativeViewFactory.a((ag)localObject4, localViewGroup);
          ((View)localObject5).setLayoutParams((ViewGroup.LayoutParams)localObject1);
          localaq.a((View)localObject5, (ag)localObject4);
          localViewGroup.addView((View)localObject5);
          int i1 = Build.VERSION.SDK_INT;
          i5 = 15;
          Object localObject9;
          double d1;
          int i8;
          if (i1 >= i5)
          {
            localObject1 = "VIDEO";
            localObject8 = b;
            if (localObject1 == localObject8)
            {
              localObject1 = localObject4;
              localObject1 = (bb)localObject4;
              localObject8 = localObject5;
              localObject8 = ((NativeVideoWrapper)localObject5).getVideoView();
              i4 = Build.VERSION.SDK_INT;
              if (i4 >= i5)
              {
                localObject9 = (ai)t;
                long l2 = System.currentTimeMillis();
                if (localObject9 != null)
                {
                  long l3 = 0L;
                  d1 = 0.0D;
                  long l4 = z;
                  boolean bool4 = l3 < l4;
                  if (bool4) {
                    l2 = z;
                  }
                }
                if (localObject9 != null) {
                  z = l2;
                }
                i5 = 0;
                ((NativeVideoView)localObject8).setClickable(false);
                i8 = -1 >>> 1;
                ((NativeVideoView)localObject8).setId(i8);
                e = 0;
                f = 0;
                localObject6 = Uri.parse(((bu)e).b());
                a = ((Uri)localObject6);
                localObject6 = (AdContainer.RenderingProperties.PlacementType)v.get("placementType");
                localObject7 = AdContainer.RenderingProperties.PlacementType.PLACEMENT_TYPE_FULLSCREEN;
                if (localObject7 == localObject6)
                {
                  localObject6 = new com/inmobi/ads/ar;
                  ((ar)localObject6).<init>();
                }
                else
                {
                  localObject6 = ar.a();
                }
                c = ((ar)localObject6);
                i5 = d;
                if (i5 != 0)
                {
                  localObject6 = c;
                  i8 = d;
                  ((ar)localObject6).setAudioSessionId(i8);
                }
                else
                {
                  localObject6 = c;
                  i5 = ((ar)localObject6).getAudioSessionId();
                  d = i5;
                }
                try
                {
                  localObject6 = c;
                  localObject7 = ((NativeVideoView)localObject8).getContext();
                  localObject7 = ((Context)localObject7).getApplicationContext();
                  localObject9 = a;
                  Map localMap = b;
                  ((ar)localObject6).setDataSource((Context)localObject7, (Uri)localObject9, localMap);
                  ((NativeVideoView)localObject8).setTag(localObject1);
                  localObject6 = new com/inmobi/ads/NativeVideoView$d;
                  ((NativeVideoView.d)localObject6).<init>((NativeVideoView)localObject8);
                  g = ((NativeVideoView.d)localObject6);
                  localObject6 = l;
                  ((NativeVideoView)localObject8).setSurfaceTextureListener((TextureView.SurfaceTextureListener)localObject6);
                  i5 = 1;
                  ((NativeVideoView)localObject8).setFocusable(i5);
                  ((NativeVideoView)localObject8).setFocusableInTouchMode(i5);
                  ((NativeVideoView)localObject8).requestFocus();
                }
                catch (IOException localIOException)
                {
                  localObject6 = c;
                  i8 = -1;
                  a = i8;
                  localObject6 = c;
                  b = i8;
                }
                localObject6 = y;
                if (localObject6 != null)
                {
                  localObject6 = (bb)y;
                  ((bb)localObject1).a((bb)localObject6);
                }
                localObject6 = new com/inmobi/ads/aq$7;
                ((aq.7)localObject6).<init>(localaq, (bb)localObject1);
                ((NativeVideoView)localObject8).setQuartileCompletedListener((NativeVideoView.c)localObject6);
                localObject6 = new com/inmobi/ads/aq$8;
                ((aq.8)localObject6).<init>(localaq, (bb)localObject1);
                ((NativeVideoView)localObject8).setPlaybackEventListener((NativeVideoView.b)localObject6);
                localObject6 = new com/inmobi/ads/aq$9;
                ((aq.9)localObject6).<init>(localaq, (bb)localObject1);
                ((NativeVideoView)localObject8).setMediaErrorListener((NativeVideoView.a)localObject6);
                localObject1 = a;
                if (localObject1 != null) {
                  try
                  {
                    ((bd)localObject1).a((NativeVideoView)localObject8);
                  }
                  catch (Exception localException1)
                  {
                    localObject6 = localException1;
                    localException1.getMessage();
                  }
                }
              }
            }
          }
          localaq.a((ag)localObject4, (View)localObject5);
          Object localObject2 = "TIMER";
          localObject6 = b;
          if (localObject2 == localObject6)
          {
            ((View)localObject5).setTag("timerView");
            localObject2 = localObject4;
            localObject2 = (ay)localObject4;
            localObject6 = localObject5;
            localObject6 = (NativeTimerView)localObject5;
            localObject7 = new com/inmobi/ads/aq$2;
            ((aq.2)localObject7).<init>(localaq, (ay)localObject2);
            ((NativeTimerView)localObject6).setTimerEventsListener((NativeTimerView.b)localObject7);
          }
          i1 = Build.VERSION.SDK_INT;
          int i6 = 15;
          if (i1 >= i6)
          {
            localObject2 = "VIDEO";
            localObject6 = b;
            if (localObject2 == localObject6)
            {
              localObject6 = localObject5;
              localObject6 = (NativeVideoWrapper)localObject5;
              localObject2 = a;
              ((NativeVideoWrapper)localObject6).setVideoEventListener((bd)localObject2);
              localObject2 = (bb)a.getTag();
              if (localObject2 != null)
              {
                try
                {
                  localObject7 = ((bb)localObject2).b();
                  localObject7 = ((bu)localObject7).b();
                  localObject8 = new android/media/MediaMetadataRetriever;
                  ((MediaMetadataRetriever)localObject8).<init>();
                  ((MediaMetadataRetriever)localObject8).setDataSource((String)localObject7);
                  i8 = 18;
                  localObject7 = ((MediaMetadataRetriever)localObject8).extractMetadata(i8);
                  localObject7 = Integer.valueOf((String)localObject7);
                  i8 = ((Integer)localObject7).intValue();
                  i4 = 19;
                  localObject9 = ((MediaMetadataRetriever)localObject8).extractMetadata(i4);
                  localObject9 = Integer.valueOf((String)localObject9);
                  i4 = ((Integer)localObject9).intValue();
                  ((MediaMetadataRetriever)localObject8).release();
                  localObject2 = c;
                  localObject2 = a;
                  i3 = x;
                  i3 = NativeViewFactory.c(i3);
                  double d2 = i3;
                  i3 = y;
                  i3 = NativeViewFactory.c(i3);
                  d1 = i3;
                  Double.isNaN(d2);
                  Double.isNaN(d1);
                  d2 /= d1;
                  double d3 = i8;
                  double d4 = i4;
                  Double.isNaN(d3);
                  Double.isNaN(d4);
                  d1 = d3 / d4;
                  double d5 = 1.0D;
                  boolean bool5 = d2 < d1;
                  int i9;
                  if (bool5)
                  {
                    i9 = y;
                    i9 = NativeViewFactory.c(i9);
                    d2 = i9;
                    Double.isNaN(d2);
                    d2 *= d5;
                    Double.isNaN(d4);
                    d2 /= d4;
                    Double.isNaN(d3);
                    d3 *= d2;
                    i1 = y;
                    i1 = NativeViewFactory.c(i1);
                    d4 = i1;
                  }
                  else
                  {
                    i9 = x;
                    i9 = NativeViewFactory.c(i9);
                    d2 = i9;
                    i1 = x;
                    i1 = NativeViewFactory.c(i1);
                    d1 = i1;
                    Double.isNaN(d1);
                    d1 *= d5;
                    Double.isNaN(d3);
                    d1 /= d3;
                    Double.isNaN(d4);
                    d4 *= d1;
                    d3 = d2;
                  }
                  localObject2 = new android/widget/RelativeLayout$LayoutParams;
                  i8 = (int)d3;
                  i3 = (int)d4;
                  ((RelativeLayout.LayoutParams)localObject2).<init>(i8, i3);
                }
                catch (Exception localException2)
                {
                  localObject7 = new android/widget/RelativeLayout$LayoutParams;
                  i3 = -1;
                  ((RelativeLayout.LayoutParams)localObject7).<init>(i3, i3);
                  localObject8 = com.inmobi.commons.core.a.a.a();
                  localObject9 = new com/inmobi/commons/core/e/a;
                  ((com.inmobi.commons.core.e.a)localObject9).<init>(localException2);
                  ((com.inmobi.commons.core.a.a)localObject8).a((com.inmobi.commons.core.e.a)localObject9);
                  localObject3 = localObject7;
                }
                i8 = 13;
                ((RelativeLayout.LayoutParams)localObject3).addRule(i8);
                localObject6 = a;
                ((NativeVideoView)localObject6).setLayoutParams((ViewGroup.LayoutParams)localObject3);
              }
            }
          }
          Object localObject3 = "WEBVIEW";
          localObject6 = b;
          if (localObject3 == localObject6)
          {
            boolean bool2 = localObject5 instanceof RenderView;
            if (bool2)
            {
              localObject5 = (RenderView)localObject5;
              localObject4 = (bc)localObject4;
              bool2 = B;
              ((RenderView)localObject5).setScrollable(bool2);
              localObject3 = h.k;
              ((RenderView)localObject5).setReferenceContainer((AdContainer)localObject3);
              localObject3 = h.u();
              ((RenderView)localObject5).setRenderViewEventListener((RenderView.a)localObject3);
              bool2 = A;
              if (!bool2)
              {
                localObject3 = h;
                i6 = v;
                if (i6 == 0)
                {
                  localObject6 = u;
                  if (localObject6 == null)
                  {
                    localObject6 = t;
                    if (localObject6 == null) {
                      u = ((RenderView)localObject5);
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    return localViewGroup;
  }
  
  final at b(at paramat, ViewGroup paramViewGroup, RenderView paramRenderView)
  {
    p = paramRenderView;
    paramat = a(paramat, paramViewGroup);
    paramRenderView = n;
    aq.1 local1 = new com/inmobi/ads/aq$1;
    local1.<init>(this, paramat, paramViewGroup);
    paramRenderView.post(local1);
    return paramat;
  }
  
  final void b()
  {
    boolean bool = true;
    o = bool;
    f.clear();
    au localau = m;
    if (localau != null) {
      localau.destroy();
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.aq
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */