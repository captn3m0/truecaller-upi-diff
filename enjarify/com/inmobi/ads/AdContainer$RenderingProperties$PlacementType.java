package com.inmobi.ads;

public enum AdContainer$RenderingProperties$PlacementType
{
  static
  {
    Object localObject = new com/inmobi/ads/AdContainer$RenderingProperties$PlacementType;
    ((PlacementType)localObject).<init>("PLACEMENT_TYPE_INLINE", 0);
    PLACEMENT_TYPE_INLINE = (PlacementType)localObject;
    localObject = new com/inmobi/ads/AdContainer$RenderingProperties$PlacementType;
    int i = 1;
    ((PlacementType)localObject).<init>("PLACEMENT_TYPE_FULLSCREEN", i);
    PLACEMENT_TYPE_FULLSCREEN = (PlacementType)localObject;
    localObject = new PlacementType[2];
    PlacementType localPlacementType = PLACEMENT_TYPE_INLINE;
    localObject[0] = localPlacementType;
    localPlacementType = PLACEMENT_TYPE_FULLSCREEN;
    localObject[i] = localPlacementType;
    $VALUES = (PlacementType[])localObject;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.AdContainer.RenderingProperties.PlacementType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */