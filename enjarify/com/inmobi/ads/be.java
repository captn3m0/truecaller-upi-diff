package com.inmobi.ads;

import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.content.Context;
import com.d.b.w;
import com.d.b.w.a;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class be
{
  private static final String a = "be";
  private static volatile w b;
  private static final Object c;
  private static List d;
  private static Application.ActivityLifecycleCallbacks e;
  
  static
  {
    Object localObject = new java/lang/Object;
    localObject.<init>();
    c = localObject;
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    d = (List)localObject;
    localObject = new com/inmobi/ads/be$1;
    ((be.1)localObject).<init>();
    e = (Application.ActivityLifecycleCallbacks)localObject;
  }
  
  public static w a(Context paramContext)
  {
    synchronized (c)
    {
      boolean bool = c(paramContext);
      if (!bool)
      {
        localObject2 = d;
        WeakReference localWeakReference = new java/lang/ref/WeakReference;
        localWeakReference.<init>(paramContext);
        ((List)localObject2).add(localWeakReference);
      }
      Object localObject2 = b;
      if (localObject2 == null)
      {
        localObject2 = new com/d/b/w$a;
        ((w.a)localObject2).<init>(paramContext);
        localObject2 = ((w.a)localObject2).a();
        b = (w)localObject2;
        bool = paramContext instanceof Activity;
        if (bool)
        {
          paramContext = (Activity)paramContext;
          paramContext = paramContext.getApplication();
          localObject2 = e;
          paramContext.registerActivityLifecycleCallbacks((Application.ActivityLifecycleCallbacks)localObject2);
        }
      }
      return b;
    }
  }
  
  private static boolean c(Context paramContext)
  {
    int i = 0;
    for (;;)
    {
      Object localObject = d;
      int j = ((List)localObject).size();
      if (i >= j) {
        break;
      }
      localObject = (Context)((WeakReference)d.get(i)).get();
      if (localObject != null)
      {
        boolean bool = localObject.equals(paramContext);
        if (bool) {
          return true;
        }
      }
      i += 1;
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.be
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */