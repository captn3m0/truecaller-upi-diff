package com.inmobi.ads;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Movie;
import android.os.Build.VERSION;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class GifView
  extends View
{
  ExecutorService a;
  Runnable b;
  GifView.a c;
  private Movie d;
  private long e;
  private int f = 0;
  private float g;
  private float h;
  private float i;
  private int j;
  private int k;
  private volatile boolean l = false;
  private boolean m;
  
  public GifView(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public GifView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    int n = 1;
    m = n;
    setLayerType(n, null);
    a();
  }
  
  public GifView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    int n = 1;
    m = n;
    setLayerType(n, null);
    a();
  }
  
  private void a()
  {
    Object localObject = Executors.newSingleThreadExecutor();
    a = ((ExecutorService)localObject);
    localObject = new com/inmobi/ads/GifView$1;
    ((GifView.1)localObject).<init>(this);
    b = ((Runnable)localObject);
  }
  
  private void a(Canvas paramCanvas)
  {
    Object localObject = d;
    int n = f;
    ((Movie)localObject).setTime(n);
    paramCanvas.save();
    float f1 = i;
    paramCanvas.scale(f1, f1);
    localObject = d;
    float f2 = g;
    float f3 = i;
    f2 /= f3;
    float f4 = h / f3;
    ((Movie)localObject).draw(paramCanvas, f2, f4);
    paramCanvas.restore();
    paramCanvas = a;
    localObject = b;
    paramCanvas.execute((Runnable)localObject);
  }
  
  private void b()
  {
    boolean bool = m;
    if (bool)
    {
      int n = Build.VERSION.SDK_INT;
      int i1 = 16;
      if (n >= i1)
      {
        postInvalidateOnAnimation();
        return;
      }
      invalidate();
    }
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    Movie localMovie1 = d;
    if (localMovie1 != null)
    {
      boolean bool1 = l;
      if (!bool1)
      {
        long l1 = SystemClock.uptimeMillis();
        long l2 = e;
        long l3 = 0L;
        boolean bool2 = l2 < l3;
        if (!bool2) {
          e = l1;
        }
        Movie localMovie2 = d;
        int n = localMovie2.duration();
        if (n == 0) {
          n = 1000;
        }
        long l4 = e;
        l1 -= l4;
        l2 = n;
        int i1 = (int)(l1 % l2);
        f = i1;
        a(paramCanvas);
        b();
        return;
      }
      a(paramCanvas);
    }
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
    paramBoolean = getWidth();
    paramInt1 = j;
    float f1 = paramBoolean - paramInt1;
    paramInt1 = 1073741824;
    float f2 = 2.0F;
    f1 /= f2;
    g = f1;
    paramBoolean = getHeight();
    paramInt2 = k;
    f1 = (paramBoolean - paramInt2) / f2;
    h = f1;
    paramBoolean = getVisibility();
    if (!paramBoolean)
    {
      paramBoolean = true;
      f1 = Float.MIN_VALUE;
    }
    else
    {
      paramBoolean = false;
      f1 = 0.0F;
    }
    m = paramBoolean;
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    Movie localMovie1 = d;
    if (localMovie1 != null)
    {
      int n = localMovie1.width();
      Movie localMovie2 = d;
      int i1 = localMovie2.height();
      int i2 = View.MeasureSpec.getMode(paramInt1);
      float f1 = 1.0F;
      float f2;
      if (i2 != 0)
      {
        paramInt1 = View.MeasureSpec.getSize(paramInt1);
        if (n > paramInt1)
        {
          f2 = n;
          f3 = paramInt1;
          f3 = f2 / f3;
          break label80;
        }
      }
      paramInt1 = 1065353216;
      float f3 = 1.0F;
      label80:
      i2 = View.MeasureSpec.getMode(paramInt2);
      if (i2 != 0)
      {
        paramInt2 = View.MeasureSpec.getSize(paramInt2);
        if (i1 > paramInt2)
        {
          f2 = i1;
          f4 = paramInt2;
          f4 = f2 / f4;
          break label128;
        }
      }
      paramInt2 = 1065353216;
      float f4 = 1.0F;
      label128:
      f3 = Math.max(f3, f4);
      f1 /= f3;
      i = f1;
      f3 = n;
      f4 = i;
      paramInt1 = (int)(f3 * f4);
      j = paramInt1;
      paramInt1 = (int)(i1 * f4);
      k = paramInt1;
      paramInt1 = j;
      paramInt2 = k;
      setMeasuredDimension(paramInt1, paramInt2);
      return;
    }
    paramInt1 = getSuggestedMinimumWidth();
    paramInt2 = getSuggestedMinimumHeight();
    setMeasuredDimension(paramInt1, paramInt2);
  }
  
  public void onScreenStateChanged(int paramInt)
  {
    super.onScreenStateChanged(paramInt);
    int n = 1;
    if (paramInt != n) {
      n = 0;
    }
    m = n;
    b();
  }
  
  protected void onVisibilityChanged(View paramView, int paramInt)
  {
    super.onVisibilityChanged(paramView, paramInt);
    boolean bool;
    if (paramInt == 0)
    {
      bool = true;
    }
    else
    {
      bool = false;
      paramView = null;
    }
    m = bool;
    b();
  }
  
  protected void onWindowVisibilityChanged(int paramInt)
  {
    super.onWindowVisibilityChanged(paramInt);
    if (paramInt == 0) {
      paramInt = 1;
    } else {
      paramInt = 0;
    }
    m = paramInt;
    b();
  }
  
  public void setGifCallbacks(GifView.a parama)
  {
    c = parama;
  }
  
  public void setMovie(Movie paramMovie)
  {
    d = paramMovie;
    requestLayout();
  }
  
  public void setMovieTime(int paramInt)
  {
    f = paramInt;
    invalidate();
  }
  
  public void setPaused(boolean paramBoolean)
  {
    l = paramBoolean;
    if (!paramBoolean)
    {
      long l1 = SystemClock.uptimeMillis();
      paramBoolean = f;
      long l2 = paramBoolean;
      l1 -= l2;
      e = l1;
    }
    invalidate();
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.GifView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */