package com.inmobi.ads;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import com.moat.analytics.mobile.inm.WebAdTracker;
import java.lang.ref.WeakReference;
import java.util.Map;

public class z
  extends bv
{
  private static final String d = "z";
  private final WeakReference e;
  private final bw f;
  private final Map g;
  private WebAdTracker h;
  
  public z(AdContainer paramAdContainer, Activity paramActivity, bw parambw, Map paramMap)
  {
    super(paramAdContainer);
    paramAdContainer = new java/lang/ref/WeakReference;
    paramAdContainer.<init>(paramActivity);
    e = paramAdContainer;
    f = parambw;
    g = paramMap;
  }
  
  public final View a()
  {
    return f.a();
  }
  
  public final View a(View paramView, ViewGroup paramViewGroup, boolean paramBoolean)
  {
    return f.a(paramView, paramViewGroup, paramBoolean);
  }
  
  public final void a(int paramInt)
  {
    f.a(paramInt);
  }
  
  public final void a(Context paramContext, int paramInt)
  {
    f.a(paramContext, paramInt);
  }
  
  /* Error */
  public final void a(View... paramVarArgs)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 28	com/inmobi/ads/z:e	Ljava/lang/ref/WeakReference;
    //   4: astore_2
    //   5: aload_2
    //   6: invokevirtual 51	java/lang/ref/WeakReference:get	()Ljava/lang/Object;
    //   9: astore_2
    //   10: aload_2
    //   11: checkcast 53	android/app/Activity
    //   14: astore_2
    //   15: aload_0
    //   16: getfield 30	com/inmobi/ads/z:f	Lcom/inmobi/ads/bw;
    //   19: astore_3
    //   20: aload_3
    //   21: invokevirtual 57	com/inmobi/ads/bw:c	()Lcom/inmobi/ads/b;
    //   24: astore_3
    //   25: aload_3
    //   26: getfield 63	com/inmobi/ads/b:o	Lcom/inmobi/ads/b$k;
    //   29: astore_3
    //   30: aload_3
    //   31: getfield 69	com/inmobi/ads/b$k:i	Z
    //   34: istore 4
    //   36: iload 4
    //   38: ifeq +157 -> 195
    //   41: aload_2
    //   42: ifnull +153 -> 195
    //   45: aload_0
    //   46: getfield 32	com/inmobi/ads/z:g	Ljava/util/Map;
    //   49: astore_3
    //   50: ldc 71
    //   52: astore 5
    //   54: aload_3
    //   55: aload 5
    //   57: invokeinterface 76 2 0
    //   62: astore_3
    //   63: aload_3
    //   64: checkcast 78	java/lang/Boolean
    //   67: astore_3
    //   68: aload_3
    //   69: invokevirtual 82	java/lang/Boolean:booleanValue	()Z
    //   72: istore 4
    //   74: iload 4
    //   76: ifeq +119 -> 195
    //   79: aload_0
    //   80: getfield 84	com/inmobi/ads/z:h	Lcom/moat/analytics/mobile/inm/WebAdTracker;
    //   83: astore_3
    //   84: aload_3
    //   85: ifnonnull +99 -> 184
    //   88: aload_0
    //   89: getfield 87	com/inmobi/ads/bw:a	Lcom/inmobi/ads/AdContainer;
    //   92: astore_3
    //   93: aload_3
    //   94: instanceof 89
    //   97: istore 4
    //   99: iload 4
    //   101: ifeq +48 -> 149
    //   104: aload_0
    //   105: getfield 87	com/inmobi/ads/bw:a	Lcom/inmobi/ads/AdContainer;
    //   108: astore_3
    //   109: aload_3
    //   110: checkcast 89	com/inmobi/ads/ad
    //   113: astore_3
    //   114: aload_3
    //   115: invokevirtual 93	com/inmobi/ads/ad:s	()Lcom/inmobi/rendering/RenderView;
    //   118: astore 5
    //   120: aload 5
    //   122: ifnull +62 -> 184
    //   125: aload_2
    //   126: invokevirtual 97	android/app/Activity:getApplication	()Landroid/app/Application;
    //   129: astore_2
    //   130: aload_3
    //   131: invokevirtual 93	com/inmobi/ads/ad:s	()Lcom/inmobi/rendering/RenderView;
    //   134: astore_3
    //   135: aload_2
    //   136: aload_3
    //   137: invokestatic 102	com/inmobi/ads/u:a	(Landroid/app/Application;Landroid/webkit/WebView;)Lcom/moat/analytics/mobile/inm/WebAdTracker;
    //   140: astore_2
    //   141: aload_0
    //   142: aload_2
    //   143: putfield 84	com/inmobi/ads/z:h	Lcom/moat/analytics/mobile/inm/WebAdTracker;
    //   146: goto +38 -> 184
    //   149: aload_0
    //   150: getfield 30	com/inmobi/ads/z:f	Lcom/inmobi/ads/bw;
    //   153: astore_3
    //   154: aload_3
    //   155: invokevirtual 105	com/inmobi/ads/bw:b	()Landroid/view/View;
    //   158: astore_3
    //   159: aload_3
    //   160: ifnull +24 -> 184
    //   163: aload_2
    //   164: invokevirtual 97	android/app/Activity:getApplication	()Landroid/app/Application;
    //   167: astore_2
    //   168: aload_3
    //   169: checkcast 107	android/webkit/WebView
    //   172: astore_3
    //   173: aload_2
    //   174: aload_3
    //   175: invokestatic 102	com/inmobi/ads/u:a	(Landroid/app/Application;Landroid/webkit/WebView;)Lcom/moat/analytics/mobile/inm/WebAdTracker;
    //   178: astore_2
    //   179: aload_0
    //   180: aload_2
    //   181: putfield 84	com/inmobi/ads/z:h	Lcom/moat/analytics/mobile/inm/WebAdTracker;
    //   184: aload_0
    //   185: getfield 84	com/inmobi/ads/z:h	Lcom/moat/analytics/mobile/inm/WebAdTracker;
    //   188: astore_2
    //   189: aload_2
    //   190: invokeinterface 113 1 0
    //   195: aload_0
    //   196: getfield 30	com/inmobi/ads/z:f	Lcom/inmobi/ads/bw;
    //   199: aload_1
    //   200: invokevirtual 116	com/inmobi/ads/bw:a	([Landroid/view/View;)V
    //   203: return
    //   204: astore_2
    //   205: goto +33 -> 238
    //   208: astore_2
    //   209: aload_2
    //   210: invokevirtual 122	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   213: pop
    //   214: invokestatic 127	com/inmobi/commons/core/a/a:a	()Lcom/inmobi/commons/core/a/a;
    //   217: astore_3
    //   218: new 129	com/inmobi/commons/core/e/a
    //   221: astore 5
    //   223: aload 5
    //   225: aload_2
    //   226: invokespecial 132	com/inmobi/commons/core/e/a:<init>	(Ljava/lang/Throwable;)V
    //   229: aload_3
    //   230: aload 5
    //   232: invokevirtual 135	com/inmobi/commons/core/a/a:a	(Lcom/inmobi/commons/core/e/a;)V
    //   235: goto -40 -> 195
    //   238: aload_0
    //   239: getfield 30	com/inmobi/ads/z:f	Lcom/inmobi/ads/bw;
    //   242: aload_1
    //   243: invokevirtual 116	com/inmobi/ads/bw:a	([Landroid/view/View;)V
    //   246: aload_2
    //   247: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	248	0	this	z
    //   0	248	1	paramVarArgs	View[]
    //   4	186	2	localObject1	Object
    //   204	1	2	localObject2	Object
    //   208	39	2	localException	Exception
    //   19	211	3	localObject3	Object
    //   34	66	4	bool	boolean
    //   52	179	5	localObject4	Object
    // Exception table:
    //   from	to	target	type
    //   0	4	204	finally
    //   5	9	204	finally
    //   10	14	204	finally
    //   15	19	204	finally
    //   20	24	204	finally
    //   25	29	204	finally
    //   30	34	204	finally
    //   45	49	204	finally
    //   55	62	204	finally
    //   63	67	204	finally
    //   68	72	204	finally
    //   79	83	204	finally
    //   88	92	204	finally
    //   104	108	204	finally
    //   109	113	204	finally
    //   114	118	204	finally
    //   125	129	204	finally
    //   130	134	204	finally
    //   136	140	204	finally
    //   142	146	204	finally
    //   149	153	204	finally
    //   154	158	204	finally
    //   163	167	204	finally
    //   168	172	204	finally
    //   174	178	204	finally
    //   180	184	204	finally
    //   184	188	204	finally
    //   189	195	204	finally
    //   209	214	204	finally
    //   214	217	204	finally
    //   218	221	204	finally
    //   225	229	204	finally
    //   230	235	204	finally
    //   0	4	208	java/lang/Exception
    //   5	9	208	java/lang/Exception
    //   10	14	208	java/lang/Exception
    //   15	19	208	java/lang/Exception
    //   20	24	208	java/lang/Exception
    //   25	29	208	java/lang/Exception
    //   30	34	208	java/lang/Exception
    //   45	49	208	java/lang/Exception
    //   55	62	208	java/lang/Exception
    //   63	67	208	java/lang/Exception
    //   68	72	208	java/lang/Exception
    //   79	83	208	java/lang/Exception
    //   88	92	208	java/lang/Exception
    //   104	108	208	java/lang/Exception
    //   109	113	208	java/lang/Exception
    //   114	118	208	java/lang/Exception
    //   125	129	208	java/lang/Exception
    //   130	134	208	java/lang/Exception
    //   136	140	208	java/lang/Exception
    //   142	146	208	java/lang/Exception
    //   149	153	208	java/lang/Exception
    //   154	158	208	java/lang/Exception
    //   163	167	208	java/lang/Exception
    //   168	172	208	java/lang/Exception
    //   174	178	208	java/lang/Exception
    //   180	184	208	java/lang/Exception
    //   184	188	208	java/lang/Exception
    //   189	195	208	java/lang/Exception
  }
  
  public final View b()
  {
    return f.b();
  }
  
  final b c()
  {
    return f.c();
  }
  
  /* Error */
  public final void d()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 84	com/inmobi/ads/z:h	Lcom/moat/analytics/mobile/inm/WebAdTracker;
    //   4: astore_1
    //   5: aload_1
    //   6: ifnull +14 -> 20
    //   9: aload_0
    //   10: getfield 84	com/inmobi/ads/z:h	Lcom/moat/analytics/mobile/inm/WebAdTracker;
    //   13: astore_1
    //   14: aload_1
    //   15: invokeinterface 138 1 0
    //   20: aload_0
    //   21: getfield 30	com/inmobi/ads/z:f	Lcom/inmobi/ads/bw;
    //   24: invokevirtual 140	com/inmobi/ads/bw:d	()V
    //   27: return
    //   28: astore_1
    //   29: goto +30 -> 59
    //   32: astore_1
    //   33: aload_1
    //   34: invokevirtual 122	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   37: pop
    //   38: invokestatic 127	com/inmobi/commons/core/a/a:a	()Lcom/inmobi/commons/core/a/a;
    //   41: astore_2
    //   42: new 129	com/inmobi/commons/core/e/a
    //   45: astore_3
    //   46: aload_3
    //   47: aload_1
    //   48: invokespecial 132	com/inmobi/commons/core/e/a:<init>	(Ljava/lang/Throwable;)V
    //   51: aload_2
    //   52: aload_3
    //   53: invokevirtual 135	com/inmobi/commons/core/a/a:a	(Lcom/inmobi/commons/core/e/a;)V
    //   56: goto -36 -> 20
    //   59: aload_0
    //   60: getfield 30	com/inmobi/ads/z:f	Lcom/inmobi/ads/bw;
    //   63: invokevirtual 140	com/inmobi/ads/bw:d	()V
    //   66: aload_1
    //   67: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	68	0	this	z
    //   4	11	1	localWebAdTracker	WebAdTracker
    //   28	1	1	localObject	Object
    //   32	35	1	localException	Exception
    //   41	11	2	locala	com.inmobi.commons.core.a.a
    //   45	8	3	locala1	com.inmobi.commons.core.e.a
    // Exception table:
    //   from	to	target	type
    //   0	4	28	finally
    //   9	13	28	finally
    //   14	20	28	finally
    //   33	38	28	finally
    //   38	41	28	finally
    //   42	45	28	finally
    //   47	51	28	finally
    //   52	56	28	finally
    //   0	4	32	java/lang/Exception
    //   9	13	32	java/lang/Exception
    //   14	20	32	java/lang/Exception
  }
  
  public final void e()
  {
    h = null;
    e.clear();
    super.e();
    f.e();
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.z
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */