package com.inmobi.ads;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import java.lang.ref.WeakReference;

public final class NativeTimerView$a
  implements ValueAnimator.AnimatorUpdateListener
{
  public WeakReference a;
  
  public NativeTimerView$a(NativeTimerView paramNativeTimerView)
  {
    WeakReference localWeakReference = new java/lang/ref/WeakReference;
    localWeakReference.<init>(paramNativeTimerView);
    a = localWeakReference;
  }
  
  public final void onAnimationUpdate(ValueAnimator paramValueAnimator)
  {
    NativeTimerView localNativeTimerView = (NativeTimerView)a.get();
    if (localNativeTimerView == null) {
      return;
    }
    int i = localNativeTimerView.getVisibility();
    int j = 4;
    if (i != j)
    {
      j = 8;
      if (i != j)
      {
        paramValueAnimator = (Float)paramValueAnimator.getAnimatedValue();
        f = paramValueAnimator.floatValue();
        NativeTimerView.a(localNativeTimerView, f);
        return;
      }
    }
    paramValueAnimator = (Float)paramValueAnimator.getAnimatedValue();
    float f = paramValueAnimator.floatValue();
    double d1 = f;
    double d2 = 1.0D;
    boolean bool = d1 < d2;
    if (!bool)
    {
      NativeTimerView.a(localNativeTimerView);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.NativeTimerView.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */