package com.inmobi.ads;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.json.JSONObject;

public final class ai
  extends ag
  implements Iterable
{
  int A;
  ag[] B;
  int C;
  long z = 0L;
  
  public ai(String paramString1, String paramString2, ah paramah, int paramInt1, JSONObject paramJSONObject, int paramInt2)
  {
    this(paramString1, paramString2, paramah, localLinkedList, paramInt1, paramJSONObject, paramInt2);
  }
  
  public ai(String paramString1, String paramString2, ah paramah, List paramList, int paramInt1, JSONObject paramJSONObject, int paramInt2)
  {
    super(paramString1, paramString2, "CONTAINER", paramah, paramList);
    f = paramJSONObject;
    paramString1 = new ag[1];
    B = paramString1;
    i = paramInt1;
    C = 0;
    A = paramInt2;
  }
  
  public final ag a(int paramInt)
  {
    if (paramInt >= 0)
    {
      int i = C;
      if (paramInt < i) {
        return B[paramInt];
      }
    }
    return null;
  }
  
  public final Iterator iterator()
  {
    ai.a locala = new com/inmobi/ads/ai$a;
    locala.<init>(this);
    return locala;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.ai
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */