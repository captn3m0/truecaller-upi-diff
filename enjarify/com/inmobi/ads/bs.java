package com.inmobi.ads;

import com.inmobi.commons.core.network.c;
import java.lang.ref.WeakReference;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

final class bs
{
  public static final Executor d;
  private static final String e = "bs";
  private static final int g = Runtime.getRuntime().availableProcessors();
  private static final int h;
  private static final int i;
  private static final ThreadFactory j;
  private static final BlockingQueue k;
  c a;
  WeakReference b;
  long c = 0L;
  private final CountDownLatch f;
  
  static
  {
    int m = g;
    int n = 1;
    m = Math.min(m - n, 4);
    h = Math.max(2, m);
    i = g * 2 + n;
    Object localObject = new com/inmobi/ads/bs$1;
    ((bs.1)localObject).<init>();
    j = (ThreadFactory)localObject;
    localObject = new java/util/concurrent/LinkedBlockingQueue;
    ((LinkedBlockingQueue)localObject).<init>(128);
    k = (BlockingQueue)localObject;
    localObject = new java/util/concurrent/ThreadPoolExecutor;
    int i1 = h;
    int i2 = i;
    TimeUnit localTimeUnit = TimeUnit.SECONDS;
    BlockingQueue localBlockingQueue = k;
    ThreadFactory localThreadFactory = j;
    ((ThreadPoolExecutor)localObject).<init>(i1, i2, 30, localTimeUnit, localBlockingQueue, localThreadFactory);
    ((ThreadPoolExecutor)localObject).allowCoreThreadTimeOut(n);
    d = (Executor)localObject;
  }
  
  public bs(br parambr, int paramInt, CountDownLatch paramCountDownLatch)
  {
    c localc = new com/inmobi/commons/core/network/c;
    String str = a;
    localc.<init>("GET", str);
    a = localc;
    a.p = paramInt;
    WeakReference localWeakReference = new java/lang/ref/WeakReference;
    localWeakReference.<init>(parambr);
    b = localWeakReference;
    f = paramCountDownLatch;
  }
  
  final void a()
  {
    CountDownLatch localCountDownLatch = f;
    if (localCountDownLatch != null) {
      localCountDownLatch.countDown();
    }
  }
  
  /* Error */
  public final void a(com.inmobi.commons.core.network.d paramd)
  {
    // Byte code:
    //   0: invokestatic 130	com/inmobi/signals/o:a	()Lcom/inmobi/signals/o;
    //   3: astore_2
    //   4: aload_0
    //   5: getfield 106	com/inmobi/ads/bs:a	Lcom/inmobi/commons/core/network/c;
    //   8: astore_3
    //   9: aload_3
    //   10: invokevirtual 133	com/inmobi/commons/core/network/c:e	()J
    //   13: lstore 4
    //   15: aload_2
    //   16: lload 4
    //   18: invokevirtual 136	com/inmobi/signals/o:a	(J)V
    //   21: invokestatic 130	com/inmobi/signals/o:a	()Lcom/inmobi/signals/o;
    //   24: astore_2
    //   25: aload_1
    //   26: invokevirtual 140	com/inmobi/commons/core/network/d:c	()J
    //   29: lstore 4
    //   31: aload_2
    //   32: lload 4
    //   34: invokevirtual 142	com/inmobi/signals/o:b	(J)V
    //   37: aload_0
    //   38: invokevirtual 144	com/inmobi/ads/bs:a	()V
    //   41: return
    //   42: astore_1
    //   43: goto +14 -> 57
    //   46: astore_1
    //   47: aload_1
    //   48: invokevirtual 150	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   51: pop
    //   52: aload_0
    //   53: invokevirtual 144	com/inmobi/ads/bs:a	()V
    //   56: return
    //   57: aload_0
    //   58: invokevirtual 144	com/inmobi/ads/bs:a	()V
    //   61: aload_1
    //   62: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	63	0	this	bs
    //   0	63	1	paramd	com.inmobi.commons.core.network.d
    //   3	29	2	localo	com.inmobi.signals.o
    //   8	2	3	localc	c
    //   13	20	4	l	long
    // Exception table:
    //   from	to	target	type
    //   0	3	42	finally
    //   4	8	42	finally
    //   9	13	42	finally
    //   16	21	42	finally
    //   21	24	42	finally
    //   25	29	42	finally
    //   32	37	42	finally
    //   47	52	42	finally
    //   0	3	46	java/lang/Exception
    //   4	8	46	java/lang/Exception
    //   9	13	46	java/lang/Exception
    //   16	21	46	java/lang/Exception
    //   21	24	46	java/lang/Exception
    //   25	29	46	java/lang/Exception
    //   32	37	46	java/lang/Exception
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.bs
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */