package com.inmobi.ads;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import java.lang.ref.WeakReference;

class v
  extends bv
{
  private static final String d = "v";
  private final WeakReference e;
  private final bw f;
  private final ae g;
  private final ad h;
  
  v(Context paramContext, ad paramad, bw parambw)
  {
    super(paramad);
    WeakReference localWeakReference = new java/lang/ref/WeakReference;
    localWeakReference.<init>(paramContext);
    e = localWeakReference;
    f = parambw;
    h = paramad;
    paramContext = new com/inmobi/ads/ae;
    paramContext.<init>(1);
    g = paramContext;
  }
  
  public final View a(View paramView, ViewGroup paramViewGroup, boolean paramBoolean)
  {
    View localView = f.b();
    if (localView != null)
    {
      ae localae = g;
      Context localContext = h.d();
      ad localad = h;
      localae.a(localContext, localView, localad);
    }
    return f.a(paramView, paramViewGroup, paramBoolean);
  }
  
  public final void a(int paramInt)
  {
    f.a(paramInt);
  }
  
  /* Error */
  public final void a(Context paramContext, int paramInt)
  {
    // Byte code:
    //   0: iload_2
    //   1: tableswitch	default:+27->28, 0:+50->51, 1:+43->44, 2:+30->31
    //   28: goto +63 -> 91
    //   31: aload_0
    //   32: getfield 40	com/inmobi/ads/v:g	Lcom/inmobi/ads/ae;
    //   35: astore_3
    //   36: aload_3
    //   37: aload_1
    //   38: invokevirtual 63	com/inmobi/ads/ae:a	(Landroid/content/Context;)V
    //   41: goto +50 -> 91
    //   44: aload_1
    //   45: invokestatic 66	com/inmobi/ads/ae:c	(Landroid/content/Context;)V
    //   48: goto +43 -> 91
    //   51: aload_1
    //   52: invokestatic 68	com/inmobi/ads/ae:b	(Landroid/content/Context;)V
    //   55: goto +36 -> 91
    //   58: astore_3
    //   59: goto +42 -> 101
    //   62: astore_3
    //   63: aload_3
    //   64: invokevirtual 74	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   67: pop
    //   68: invokestatic 79	com/inmobi/commons/core/a/a:a	()Lcom/inmobi/commons/core/a/a;
    //   71: astore 4
    //   73: new 81	com/inmobi/commons/core/e/a
    //   76: astore 5
    //   78: aload 5
    //   80: aload_3
    //   81: invokespecial 84	com/inmobi/commons/core/e/a:<init>	(Ljava/lang/Throwable;)V
    //   84: aload 4
    //   86: aload 5
    //   88: invokevirtual 87	com/inmobi/commons/core/a/a:a	(Lcom/inmobi/commons/core/e/a;)V
    //   91: aload_0
    //   92: getfield 30	com/inmobi/ads/v:f	Lcom/inmobi/ads/bw;
    //   95: aload_1
    //   96: iload_2
    //   97: invokevirtual 90	com/inmobi/ads/bw:a	(Landroid/content/Context;I)V
    //   100: return
    //   101: aload_0
    //   102: getfield 30	com/inmobi/ads/v:f	Lcom/inmobi/ads/bw;
    //   105: aload_1
    //   106: iload_2
    //   107: invokevirtual 90	com/inmobi/ads/bw:a	(Landroid/content/Context;I)V
    //   110: aload_3
    //   111: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	112	0	this	v
    //   0	112	1	paramContext	Context
    //   0	112	2	paramInt	int
    //   35	2	3	localae	ae
    //   58	1	3	localObject	Object
    //   62	49	3	localException	Exception
    //   71	14	4	locala	com.inmobi.commons.core.a.a
    //   76	11	5	locala1	com.inmobi.commons.core.e.a
    // Exception table:
    //   from	to	target	type
    //   31	35	58	finally
    //   37	41	58	finally
    //   44	48	58	finally
    //   51	55	58	finally
    //   63	68	58	finally
    //   68	71	58	finally
    //   73	76	58	finally
    //   80	84	58	finally
    //   86	91	58	finally
    //   31	35	62	java/lang/Exception
    //   37	41	62	java/lang/Exception
    //   44	48	62	java/lang/Exception
    //   51	55	62	java/lang/Exception
  }
  
  /* Error */
  public final void a(View... paramVarArgs)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 28	com/inmobi/ads/v:e	Ljava/lang/ref/WeakReference;
    //   4: astore_2
    //   5: aload_2
    //   6: invokevirtual 94	java/lang/ref/WeakReference:get	()Ljava/lang/Object;
    //   9: astore_2
    //   10: aload_2
    //   11: astore_3
    //   12: aload_2
    //   13: checkcast 96	android/content/Context
    //   16: astore_3
    //   17: aload_0
    //   18: getfield 30	com/inmobi/ads/v:f	Lcom/inmobi/ads/bw;
    //   21: astore_2
    //   22: aload_2
    //   23: invokevirtual 46	com/inmobi/ads/bw:b	()Landroid/view/View;
    //   26: astore 4
    //   28: aload_0
    //   29: getfield 30	com/inmobi/ads/v:f	Lcom/inmobi/ads/bw;
    //   32: astore_2
    //   33: aload_2
    //   34: invokevirtual 99	com/inmobi/ads/bw:c	()Lcom/inmobi/ads/b;
    //   37: astore_2
    //   38: aload_2
    //   39: getfield 105	com/inmobi/ads/b:o	Lcom/inmobi/ads/b$k;
    //   42: astore 5
    //   44: aload_0
    //   45: getfield 108	com/inmobi/ads/bw:a	Lcom/inmobi/ads/AdContainer;
    //   48: astore_2
    //   49: aload_2
    //   50: checkcast 48	com/inmobi/ads/ad
    //   53: astore_2
    //   54: aload_3
    //   55: ifnull +73 -> 128
    //   58: aload 4
    //   60: ifnull +68 -> 128
    //   63: aload_2
    //   64: getfield 112	com/inmobi/ads/ad:i	Z
    //   67: istore 6
    //   69: iload 6
    //   71: ifne +57 -> 128
    //   74: aload_0
    //   75: getfield 40	com/inmobi/ads/v:g	Lcom/inmobi/ads/ae;
    //   78: astore 7
    //   80: aload 7
    //   82: aload_3
    //   83: aload 4
    //   85: aload_2
    //   86: aload 5
    //   88: invokevirtual 115	com/inmobi/ads/ae:a	(Landroid/content/Context;Landroid/view/View;Lcom/inmobi/ads/ad;Lcom/inmobi/ads/b$k;)V
    //   91: aload_0
    //   92: getfield 40	com/inmobi/ads/v:g	Lcom/inmobi/ads/ae;
    //   95: astore 7
    //   97: aload_0
    //   98: getfield 32	com/inmobi/ads/v:h	Lcom/inmobi/ads/ad;
    //   101: astore 8
    //   103: aload_0
    //   104: getfield 32	com/inmobi/ads/v:h	Lcom/inmobi/ads/ad;
    //   107: astore_2
    //   108: aload_2
    //   109: getfield 119	com/inmobi/ads/ad:x	Lcom/inmobi/ads/ae$a;
    //   112: astore 9
    //   114: aload 7
    //   116: aload_3
    //   117: aload 4
    //   119: aload 8
    //   121: aload 9
    //   123: aload 5
    //   125: invokevirtual 122	com/inmobi/ads/ae:a	(Landroid/content/Context;Landroid/view/View;Lcom/inmobi/ads/ad;Lcom/inmobi/ads/ae$a;Lcom/inmobi/ads/b$k;)V
    //   128: aload_0
    //   129: getfield 30	com/inmobi/ads/v:f	Lcom/inmobi/ads/bw;
    //   132: aload_1
    //   133: invokevirtual 125	com/inmobi/ads/bw:a	([Landroid/view/View;)V
    //   136: return
    //   137: astore_2
    //   138: goto +32 -> 170
    //   141: astore_2
    //   142: aload_2
    //   143: invokevirtual 74	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   146: pop
    //   147: invokestatic 79	com/inmobi/commons/core/a/a:a	()Lcom/inmobi/commons/core/a/a;
    //   150: astore 7
    //   152: new 81	com/inmobi/commons/core/e/a
    //   155: astore_3
    //   156: aload_3
    //   157: aload_2
    //   158: invokespecial 84	com/inmobi/commons/core/e/a:<init>	(Ljava/lang/Throwable;)V
    //   161: aload 7
    //   163: aload_3
    //   164: invokevirtual 87	com/inmobi/commons/core/a/a:a	(Lcom/inmobi/commons/core/e/a;)V
    //   167: goto -39 -> 128
    //   170: aload_0
    //   171: getfield 30	com/inmobi/ads/v:f	Lcom/inmobi/ads/bw;
    //   174: aload_1
    //   175: invokevirtual 125	com/inmobi/ads/bw:a	([Landroid/view/View;)V
    //   178: aload_2
    //   179: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	180	0	this	v
    //   0	180	1	paramVarArgs	View[]
    //   4	105	2	localObject1	Object
    //   137	1	2	localObject2	Object
    //   141	38	2	localException	Exception
    //   11	153	3	localObject3	Object
    //   26	92	4	localView	View
    //   42	82	5	localk	b.k
    //   67	3	6	bool	boolean
    //   78	84	7	localObject4	Object
    //   101	19	8	localad	ad
    //   112	10	9	locala	ae.a
    // Exception table:
    //   from	to	target	type
    //   0	4	137	finally
    //   5	9	137	finally
    //   12	16	137	finally
    //   17	21	137	finally
    //   22	26	137	finally
    //   28	32	137	finally
    //   33	37	137	finally
    //   38	42	137	finally
    //   44	48	137	finally
    //   49	53	137	finally
    //   63	67	137	finally
    //   74	78	137	finally
    //   86	91	137	finally
    //   91	95	137	finally
    //   97	101	137	finally
    //   103	107	137	finally
    //   108	112	137	finally
    //   123	128	137	finally
    //   142	147	137	finally
    //   147	150	137	finally
    //   152	155	137	finally
    //   157	161	137	finally
    //   163	167	137	finally
    //   0	4	141	java/lang/Exception
    //   5	9	141	java/lang/Exception
    //   12	16	141	java/lang/Exception
    //   17	21	141	java/lang/Exception
    //   22	26	141	java/lang/Exception
    //   28	32	141	java/lang/Exception
    //   33	37	141	java/lang/Exception
    //   38	42	141	java/lang/Exception
    //   44	48	141	java/lang/Exception
    //   49	53	141	java/lang/Exception
    //   63	67	141	java/lang/Exception
    //   74	78	141	java/lang/Exception
    //   86	91	141	java/lang/Exception
    //   91	95	141	java/lang/Exception
    //   97	101	141	java/lang/Exception
    //   103	107	141	java/lang/Exception
    //   108	112	141	java/lang/Exception
    //   123	128	141	java/lang/Exception
  }
  
  public final View b()
  {
    return f.b();
  }
  
  final b c()
  {
    return f.c();
  }
  
  /* Error */
  public final void d()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 108	com/inmobi/ads/bw:a	Lcom/inmobi/ads/AdContainer;
    //   4: astore_1
    //   5: aload_1
    //   6: checkcast 48	com/inmobi/ads/ad
    //   9: astore_1
    //   10: aload_1
    //   11: getfield 112	com/inmobi/ads/ad:i	Z
    //   14: istore_2
    //   15: iload_2
    //   16: ifne +35 -> 51
    //   19: aload_0
    //   20: getfield 40	com/inmobi/ads/v:g	Lcom/inmobi/ads/ae;
    //   23: astore_3
    //   24: aload_0
    //   25: getfield 28	com/inmobi/ads/v:e	Ljava/lang/ref/WeakReference;
    //   28: astore 4
    //   30: aload 4
    //   32: invokevirtual 94	java/lang/ref/WeakReference:get	()Ljava/lang/Object;
    //   35: astore 4
    //   37: aload 4
    //   39: checkcast 96	android/content/Context
    //   42: astore 4
    //   44: aload_3
    //   45: aload 4
    //   47: aload_1
    //   48: invokevirtual 128	com/inmobi/ads/ae:a	(Landroid/content/Context;Lcom/inmobi/ads/ad;)V
    //   51: aload_0
    //   52: getfield 30	com/inmobi/ads/v:f	Lcom/inmobi/ads/bw;
    //   55: invokevirtual 131	com/inmobi/ads/bw:d	()V
    //   58: return
    //   59: astore_1
    //   60: goto +33 -> 93
    //   63: astore_1
    //   64: aload_1
    //   65: invokevirtual 74	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   68: pop
    //   69: invokestatic 79	com/inmobi/commons/core/a/a:a	()Lcom/inmobi/commons/core/a/a;
    //   72: astore_3
    //   73: new 81	com/inmobi/commons/core/e/a
    //   76: astore 4
    //   78: aload 4
    //   80: aload_1
    //   81: invokespecial 84	com/inmobi/commons/core/e/a:<init>	(Ljava/lang/Throwable;)V
    //   84: aload_3
    //   85: aload 4
    //   87: invokevirtual 87	com/inmobi/commons/core/a/a:a	(Lcom/inmobi/commons/core/e/a;)V
    //   90: goto -39 -> 51
    //   93: aload_0
    //   94: getfield 30	com/inmobi/ads/v:f	Lcom/inmobi/ads/bw;
    //   97: invokevirtual 131	com/inmobi/ads/bw:d	()V
    //   100: aload_1
    //   101: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	102	0	this	v
    //   4	44	1	localObject1	Object
    //   59	1	1	localObject2	Object
    //   63	38	1	localException	Exception
    //   14	2	2	bool	boolean
    //   23	62	3	localObject3	Object
    //   28	58	4	localObject4	Object
    // Exception table:
    //   from	to	target	type
    //   0	4	59	finally
    //   5	9	59	finally
    //   10	14	59	finally
    //   19	23	59	finally
    //   24	28	59	finally
    //   30	35	59	finally
    //   37	42	59	finally
    //   47	51	59	finally
    //   64	69	59	finally
    //   69	72	59	finally
    //   73	76	59	finally
    //   80	84	59	finally
    //   85	90	59	finally
    //   0	4	63	java/lang/Exception
    //   5	9	63	java/lang/Exception
    //   10	14	63	java/lang/Exception
    //   19	23	63	java/lang/Exception
    //   24	28	63	java/lang/Exception
    //   30	35	63	java/lang/Exception
    //   37	42	63	java/lang/Exception
    //   47	51	63	java/lang/Exception
  }
  
  public final void e()
  {
    ae localae = g;
    Context localContext = h.d();
    View localView = f.b();
    ad localad = h;
    localae.a(localContext, localView, localad);
    super.e();
    e.clear();
    f.e();
  }
  
  public final bw.a f()
  {
    return f.f();
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.v
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */