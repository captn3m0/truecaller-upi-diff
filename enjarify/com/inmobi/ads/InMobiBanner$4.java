package com.inmobi.ads;

import android.os.Message;
import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;
import java.util.Map;

final class InMobiBanner$4
  implements j.b
{
  InMobiBanner$4(InMobiBanner paramInMobiBanner) {}
  
  public final void a()
  {
    try
    {
      Object localObject1 = a;
      localObject1 = InMobiBanner.access$900((InMobiBanner)localObject1);
      if (localObject1 != null)
      {
        localObject1 = a;
        localObject1 = InMobiBanner.access$900((InMobiBanner)localObject1);
        boolean bool = ((n)localObject1).K();
        if (bool) {}
      }
      else
      {
        localObject1 = a;
        localObject2 = new com/inmobi/ads/InMobiBanner$4$1;
        ((InMobiBanner.4.1)localObject2).<init>(this);
        InMobiBanner.access$1300((InMobiBanner)localObject1, (InMobiBanner.a)localObject2);
      }
      return;
    }
    catch (Exception localException)
    {
      Object localObject2 = Logger.InternalLogLevel.ERROR;
      String str = InMobiBanner.access$300();
      Logger.a((Logger.InternalLogLevel)localObject2, str, "Encountered unexpected error in loading banner ad");
      InMobiBanner.access$300();
      localException.getMessage();
    }
  }
  
  public final void a(InMobiAdRequestStatus paramInMobiAdRequestStatus)
  {
    try
    {
      localObject1 = InMobiBanner.5.a;
      localObject2 = paramInMobiAdRequestStatus.getStatusCode();
      int i = ((InMobiAdRequestStatus.StatusCode)localObject2).ordinal();
      int j = localObject1[i];
      switch (j)
      {
      default: 
        localObject1 = a;
        break;
      case 4: 
        localObject1 = a;
        localObject2 = "ART";
        str = "FrequentRequests";
        InMobiBanner.access$1000((InMobiBanner)localObject1, (String)localObject2, str);
        break;
      case 2: 
      case 3: 
        localObject1 = a;
        localObject2 = "ART";
        str = "LoadInProgress";
        InMobiBanner.access$1000((InMobiBanner)localObject1, (String)localObject2, str);
        break;
      case 1: 
        localObject1 = a;
        localObject2 = "ART";
        str = "NetworkNotAvailable";
        InMobiBanner.access$1000((InMobiBanner)localObject1, (String)localObject2, str);
        break;
      }
      localObject2 = "AF";
      String str = "";
      InMobiBanner.access$1000((InMobiBanner)localObject1, (String)localObject2, str);
      localObject1 = Message.obtain();
      i = 2;
      what = i;
      obj = paramInMobiAdRequestStatus;
      paramInMobiAdRequestStatus = a;
      paramInMobiAdRequestStatus = InMobiBanner.access$1100(paramInMobiAdRequestStatus);
      paramInMobiAdRequestStatus.sendMessage((Message)localObject1);
      paramInMobiAdRequestStatus = a;
      InMobiBanner.access$1200(paramInMobiAdRequestStatus);
      return;
    }
    catch (Exception paramInMobiAdRequestStatus)
    {
      Object localObject1 = Logger.InternalLogLevel.ERROR;
      Object localObject2 = InMobiBanner.access$300();
      Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "Encountered unexpected error in loading banner ad");
      InMobiBanner.access$300();
      paramInMobiAdRequestStatus.getMessage();
    }
  }
  
  public final void a(j paramj) {}
  
  public final void a(Map paramMap)
  {
    InMobiBanner.access$1000(a, "AVCL", "");
    Message localMessage = Message.obtain();
    what = 5;
    obj = paramMap;
    InMobiBanner.access$1100(a).sendMessage(localMessage);
  }
  
  public final void a(boolean paramBoolean) {}
  
  public final void b() {}
  
  public final void b(Map paramMap)
  {
    Message localMessage = Message.obtain();
    what = 7;
    obj = paramMap;
    InMobiBanner.access$1100(a).sendMessage(localMessage);
  }
  
  public final void c() {}
  
  public final void d()
  {
    InMobiBanner.access$1100(a).sendEmptyMessage(3);
  }
  
  public final void e()
  {
    try
    {
      Object localObject = a;
      InMobiBanner.access$1200((InMobiBanner)localObject);
      localObject = a;
      localObject = InMobiBanner.access$1100((InMobiBanner)localObject);
      int i = 4;
      ((InMobiBanner.b)localObject).sendEmptyMessage(i);
      return;
    }
    catch (Exception localException)
    {
      Logger.InternalLogLevel localInternalLogLevel = Logger.InternalLogLevel.ERROR;
      String str = InMobiBanner.access$300();
      Logger.a(localInternalLogLevel, str, "Encountered unexpected error in closing banner ad");
      InMobiBanner.access$300();
      localException.getMessage();
    }
  }
  
  public final void f()
  {
    InMobiBanner.access$1100(a).sendEmptyMessage(6);
  }
  
  public final void g() {}
  
  public final void h() {}
  
  public final boolean i()
  {
    return true;
  }
  
  public final void j() {}
}

/* Location:
 * Qualified Name:     com.inmobi.ads.InMobiBanner.4
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */