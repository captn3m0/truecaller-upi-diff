package com.inmobi.ads;

import android.graphics.SurfaceTexture;
import android.view.Surface;
import android.view.TextureView.SurfaceTextureListener;
import java.util.Map;

final class NativeVideoView$7
  implements TextureView.SurfaceTextureListener
{
  NativeVideoView$7(NativeVideoView paramNativeVideoView) {}
  
  public final void onSurfaceTextureAvailable(SurfaceTexture paramSurfaceTexture, int paramInt1, int paramInt2)
  {
    NativeVideoView localNativeVideoView = a;
    Surface localSurface = new android/view/Surface;
    localSurface.<init>(paramSurfaceTexture);
    NativeVideoView.a(localNativeVideoView, localSurface);
    NativeVideoView.i(a);
  }
  
  public final boolean onSurfaceTextureDestroyed(SurfaceTexture paramSurfaceTexture)
  {
    paramSurfaceTexture = NativeVideoView.j(a);
    if (paramSurfaceTexture != null)
    {
      NativeVideoView.j(a).release();
      paramSurfaceTexture = a;
      NativeVideoView.a(paramSurfaceTexture, null);
    }
    paramSurfaceTexture = NativeVideoView.e(a);
    if (paramSurfaceTexture != null)
    {
      paramSurfaceTexture = NativeVideoView.e(a);
      paramSurfaceTexture.b();
    }
    a.c();
    return true;
  }
  
  public final void onSurfaceTextureSizeChanged(SurfaceTexture paramSurfaceTexture, int paramInt1, int paramInt2)
  {
    paramSurfaceTexture = NativeVideoView.c(a);
    int i = 1;
    if (paramSurfaceTexture != null)
    {
      paramSurfaceTexture = NativeVideoView.c(a);
      j = b;
      int k = 3;
      if (j == k)
      {
        j = 1;
        break label50;
      }
    }
    int j = 0;
    paramSurfaceTexture = null;
    label50:
    if ((paramInt1 <= 0) || (paramInt2 <= 0)) {
      i = 0;
    }
    Object localObject = NativeVideoView.c(a);
    if ((localObject != null) && (j != 0) && (i != 0))
    {
      paramSurfaceTexture = a.getTag();
      if (paramSurfaceTexture != null)
      {
        paramSurfaceTexture = a.getTag()).v;
        localObject = "seekPosition";
        paramSurfaceTexture = (Integer)paramSurfaceTexture.get(localObject);
        j = paramSurfaceTexture.intValue();
        if (j != 0)
        {
          localObject = a;
          ((NativeVideoView)localObject).a(j);
        }
      }
      paramSurfaceTexture = a;
      paramSurfaceTexture.start();
    }
  }
  
  public final void onSurfaceTextureUpdated(SurfaceTexture paramSurfaceTexture) {}
}

/* Location:
 * Qualified Name:     com.inmobi.ads.NativeVideoView.7
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */