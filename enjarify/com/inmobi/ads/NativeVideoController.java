package com.inmobi.ads;

import android.content.Context;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build.VERSION;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.inmobi.commons.core.utilities.b.c;
import com.inmobi.commons.core.utilities.b.d;
import com.inmobi.rendering.CustomView;

public class NativeVideoController
  extends FrameLayout
{
  private static final String b = "NativeVideoController";
  boolean a;
  private ba c;
  private NativeVideoController.a d;
  private NativeVideoView e;
  private CustomView f;
  private CustomView g;
  private ProgressBar h;
  private RelativeLayout i;
  private boolean j;
  private float k;
  private final View.OnClickListener l;
  
  public NativeVideoController(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public NativeVideoController(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public NativeVideoController(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    int m = 0;
    paramContext = null;
    j = false;
    paramAttributeSet = new com/inmobi/ads/NativeVideoController$1;
    paramAttributeSet.<init>(this);
    l = paramAttributeSet;
    paramAttributeSet = new android/widget/RelativeLayout;
    Object localObject1 = getContext();
    paramAttributeSet.<init>((Context)localObject1);
    i = paramAttributeSet;
    paramAttributeSet = new android/widget/RelativeLayout$LayoutParams;
    paramInt = -1;
    paramAttributeSet.<init>(paramInt, paramInt);
    Object localObject2 = i;
    addView((View)localObject2, paramAttributeSet);
    i.setPadding(0, 0, 0, 0);
    paramAttributeSet = i;
    if (paramAttributeSet != null)
    {
      float f1 = ac;
      k = f1;
      paramAttributeSet = new com/inmobi/rendering/CustomView;
      localObject2 = getContext();
      float f2 = k;
      paramAttributeSet.<init>((Context)localObject2, f2, 9);
      f = paramAttributeSet;
      paramAttributeSet = new com/inmobi/rendering/CustomView;
      localObject2 = getContext();
      f2 = k;
      paramAttributeSet.<init>((Context)localObject2, f2, 11);
      g = paramAttributeSet;
      paramAttributeSet = new android/widget/ProgressBar;
      localObject2 = getContext();
      PorterDuff.Mode localMode = null;
      paramAttributeSet.<init>((Context)localObject2, null, 16842872);
      h = paramAttributeSet;
      h.setScaleY(0.8F);
      c();
      paramAttributeSet = new android/widget/RelativeLayout$LayoutParams;
      paramAttributeSet.<init>(paramInt, -2);
      paramAttributeSet.addRule(12, paramInt);
      float f3 = ac;
      f2 = -6.0F * f3;
      int n = (int)f2;
      float f4 = -8.0F;
      f3 *= f4;
      int i1 = (int)f3;
      paramAttributeSet.setMargins(0, n, 0, i1);
      localObject2 = (LayerDrawable)h.getProgressDrawable();
      if (localObject2 != null)
      {
        paramContext = ((LayerDrawable)localObject2).getDrawable(0);
        localMode = PorterDuff.Mode.SRC_IN;
        paramContext.setColorFilter(paramInt, localMode);
        m = 2;
        paramContext = ((LayerDrawable)localObject2).getDrawable(m);
        paramInt = -327674;
        localObject2 = PorterDuff.Mode.SRC_IN;
        paramContext.setColorFilter(paramInt, (PorterDuff.Mode)localObject2);
      }
      paramContext = i;
      localObject1 = h;
      paramContext.addView((View)localObject1, paramAttributeSet);
    }
    paramContext = new com/inmobi/ads/NativeVideoController$a;
    paramContext.<init>(this);
    d = paramContext;
  }
  
  private void c()
  {
    Object localObject1 = new android/widget/RelativeLayout$LayoutParams;
    float f1 = k;
    float f2 = 30.0F;
    int m = (int)(f1 * f2);
    int n = (int)(f1 * f2);
    ((RelativeLayout.LayoutParams)localObject1).<init>(m, n);
    n = -1;
    ((RelativeLayout.LayoutParams)localObject1).addRule(9, n);
    ((RelativeLayout.LayoutParams)localObject1).addRule(12, n);
    Object localObject2 = i;
    CustomView localCustomView = f;
    ((RelativeLayout)localObject2).addView(localCustomView, (ViewGroup.LayoutParams)localObject1);
    localObject1 = f;
    localObject2 = l;
    ((CustomView)localObject1).setOnClickListener((View.OnClickListener)localObject2);
  }
  
  private void d()
  {
    Object localObject1 = new android/widget/RelativeLayout$LayoutParams;
    float f1 = k;
    float f2 = 30.0F;
    int m = (int)(f1 * f2);
    int n = (int)(f1 * f2);
    ((RelativeLayout.LayoutParams)localObject1).<init>(m, n);
    n = -1;
    ((RelativeLayout.LayoutParams)localObject1).addRule(9, n);
    ((RelativeLayout.LayoutParams)localObject1).addRule(12, n);
    Object localObject2 = i;
    CustomView localCustomView = g;
    ((RelativeLayout)localObject2).addView(localCustomView, (ViewGroup.LayoutParams)localObject1);
    localObject1 = g;
    localObject2 = l;
    ((CustomView)localObject1).setOnClickListener((View.OnClickListener)localObject2);
  }
  
  private int e()
  {
    NativeVideoView localNativeVideoView1 = e;
    if (localNativeVideoView1 == null) {
      return 0;
    }
    int m = localNativeVideoView1.getCurrentPosition();
    NativeVideoView localNativeVideoView2 = e;
    int n = localNativeVideoView2.getDuration();
    ProgressBar localProgressBar = h;
    if ((localProgressBar != null) && (n != 0))
    {
      int i1 = m * 100 / n;
      localProgressBar.setProgress(i1);
    }
    return m;
  }
  
  public final void a()
  {
    boolean bool1 = a;
    if (!bool1)
    {
      e();
      bool1 = true;
      a = bool1;
      bb localbb = (bb)e.getTag();
      if (localbb != null)
      {
        Object localObject = f;
        boolean bool2 = B;
        int n = 4;
        int m;
        if (bool2) {
          bool2 = false;
        } else {
          m = 4;
        }
        ((CustomView)localObject).setVisibility(m);
        localObject = h;
        bool1 = D;
        if (bool1) {
          n = 0;
        }
        ((ProgressBar)localObject).setVisibility(n);
      }
      setVisibility(0);
    }
    d.sendEmptyMessage(2);
  }
  
  public final void b()
  {
    boolean bool = a;
    if (bool)
    {
      try
      {
        NativeVideoController.a locala = d;
        int n = 2;
        locala.removeMessages(n);
        m = 8;
        setVisibility(m);
      }
      catch (IllegalArgumentException localIllegalArgumentException)
      {
        com.inmobi.commons.core.a.a locala1 = com.inmobi.commons.core.a.a.a();
        com.inmobi.commons.core.e.a locala2 = new com/inmobi/commons/core/e/a;
        locala2.<init>(localIllegalArgumentException);
        locala1.a(locala2);
      }
      int m = 0;
      Object localObject = null;
      a = false;
    }
  }
  
  public boolean dispatchKeyEvent(KeyEvent paramKeyEvent)
  {
    int m = paramKeyEvent.getKeyCode();
    int n = paramKeyEvent.getRepeatCount();
    boolean bool1 = true;
    if (n == 0)
    {
      n = paramKeyEvent.getAction();
      if (n == 0)
      {
        n = 1;
        break label33;
      }
    }
    n = 0;
    label33:
    int i1 = Build.VERSION.SDK_INT;
    int i2 = 15;
    if (i1 >= i2)
    {
      i1 = 79;
      boolean bool2;
      if (m != i1)
      {
        i1 = 85;
        if (m != i1)
        {
          i1 = 62;
          if (m != i1)
          {
            i1 = 126;
            if (m == i1)
            {
              if (n != 0)
              {
                paramKeyEvent = e;
                bool2 = paramKeyEvent.isPlaying();
                if (!bool2)
                {
                  paramKeyEvent = e;
                  paramKeyEvent.start();
                  a();
                }
              }
              return bool1;
            }
            i1 = 86;
            if (m != i1)
            {
              i1 = 127;
              if (m != i1)
              {
                n = 25;
                if (m != n)
                {
                  n = 24;
                  if (m != n)
                  {
                    n = 164;
                    if (m != n)
                    {
                      n = 27;
                      if (m != n)
                      {
                        a();
                        break label284;
                      }
                    }
                  }
                }
                return super.dispatchKeyEvent(paramKeyEvent);
              }
            }
            if (n != 0)
            {
              paramKeyEvent = e;
              bool2 = paramKeyEvent.isPlaying();
              if (bool2)
              {
                paramKeyEvent = e;
                paramKeyEvent.pause();
                a();
              }
            }
            return bool1;
          }
        }
      }
      if (n != 0)
      {
        paramKeyEvent = e;
        bool2 = paramKeyEvent.isPlaying();
        if (bool2)
        {
          paramKeyEvent = e;
          paramKeyEvent.pause();
        }
        else
        {
          paramKeyEvent = e;
          paramKeyEvent.start();
        }
        a();
      }
      return bool1;
    }
    label284:
    return super.dispatchKeyEvent(paramKeyEvent);
  }
  
  public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
  {
    super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
    String str = NativeVideoController.class.getName();
    paramAccessibilityEvent.setClassName(str);
  }
  
  public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
  {
    super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
    int m = Build.VERSION.SDK_INT;
    int n = 15;
    if (m >= n)
    {
      String str = NativeVideoController.class.getName();
      paramAccessibilityNodeInfo.setClassName(str);
    }
  }
  
  public boolean onTrackballEvent(MotionEvent paramMotionEvent)
  {
    int m = Build.VERSION.SDK_INT;
    int n = 15;
    if (m >= n)
    {
      paramMotionEvent = e;
      if (paramMotionEvent != null)
      {
        boolean bool = paramMotionEvent.b();
        if (bool)
        {
          bool = a;
          if (bool) {
            b();
          } else {
            a();
          }
        }
      }
    }
    return false;
  }
  
  public void setMediaPlayer(NativeVideoView paramNativeVideoView)
  {
    e = paramNativeVideoView;
    paramNativeVideoView = (bb)e.getTag();
    if (paramNativeVideoView != null)
    {
      boolean bool1 = B;
      if (bool1)
      {
        boolean bool2 = paramNativeVideoView.a();
        if (!bool2)
        {
          bool2 = true;
          j = bool2;
          paramNativeVideoView = i;
          CustomView localCustomView = g;
          paramNativeVideoView.removeView(localCustomView);
          paramNativeVideoView = i;
          localCustomView = f;
          paramNativeVideoView.removeView(localCustomView);
          d();
        }
      }
    }
  }
  
  public void setVideoAd(ba paramba)
  {
    c = paramba;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.NativeVideoController
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */