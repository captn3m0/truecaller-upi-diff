package com.inmobi.ads;

import com.inmobi.ads.cache.b;
import com.inmobi.ads.cache.f;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

final class h$1
  implements f
{
  h$1(h paramh) {}
  
  public final void a(b paramb)
  {
    h.c();
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    Object localObject3;
    Object localObject5;
    boolean bool2;
    if (paramb != null)
    {
      paramb = a.iterator();
      boolean bool1 = paramb.hasNext();
      if (bool1)
      {
        Object localObject2 = (com.inmobi.ads.cache.a)paramb.next();
        localObject3 = new java/util/HashMap;
        ((HashMap)localObject3).<init>();
        Object localObject4 = d;
        ((Map)localObject3).put("url", localObject4);
        localObject4 = Long.valueOf(a);
        ((Map)localObject3).put("latency", localObject4);
        long l1 = com.inmobi.commons.core.utilities.c.a(e);
        localObject4 = Long.valueOf(l1);
        ((Map)localObject3).put("size", localObject4);
        localObject5 = h.a(a);
        localObject4 = "ads";
        String str = "VideoAssetDownloadFailed";
        ((h.a)localObject5).a((String)localObject4, str, (Map)localObject3);
        localObject3 = h.c(a);
        localObject2 = d;
        localObject5 = h.b(a);
        if (localObject5 == null)
        {
          bool2 = false;
          localObject5 = null;
        }
        else
        {
          localObject5 = ba).f;
        }
        localObject2 = ((c)localObject3).b((String)localObject2, (String)localObject5).iterator();
        for (;;)
        {
          boolean bool3 = ((Iterator)localObject2).hasNext();
          if (!bool3) {
            break;
          }
          localObject3 = (a)((Iterator)localObject2).next();
          long l2 = c;
          localObject5 = Long.valueOf(l2);
          bool2 = ((List)localObject1).contains(localObject5);
          if (!bool2)
          {
            long l3 = c;
            localObject3 = Long.valueOf(l3);
            ((List)localObject1).add(localObject3);
          }
        }
      }
    }
    long l4 = ba).d;
    paramb = Long.valueOf(l4);
    boolean bool4 = ((List)localObject1).contains(paramb);
    if (!bool4)
    {
      l4 = ba).d;
      paramb = Long.valueOf(l4);
      ((List)localObject1).add(paramb);
    }
    paramb = ((List)localObject1).iterator();
    for (;;)
    {
      boolean bool5 = paramb.hasNext();
      if (!bool5) {
        break;
      }
      localObject1 = (Long)paramb.next();
      long l5 = ((Long)localObject1).longValue();
      localObject3 = h.a(a);
      bool2 = false;
      localObject5 = null;
      ((h.a)localObject3).a(l5, false);
    }
  }
  
  public final void b(b paramb)
  {
    h.c();
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    Object localObject2;
    boolean bool3;
    if (paramb != null)
    {
      Iterator localIterator = a.iterator();
      boolean bool1 = localIterator.hasNext();
      if (bool1)
      {
        localObject2 = (com.inmobi.ads.cache.a)localIterator.next();
        Object localObject3 = new java/util/HashMap;
        ((HashMap)localObject3).<init>();
        Object localObject4 = d;
        ((Map)localObject3).put("url", localObject4);
        localObject4 = Long.valueOf(a);
        ((Map)localObject3).put("latency", localObject4);
        long l1 = com.inmobi.commons.core.utilities.c.a(e);
        localObject4 = Long.valueOf(l1);
        ((Map)localObject3).put("size", localObject4);
        Object localObject5 = "clientRequestId";
        localObject4 = f;
        ((Map)localObject3).put(localObject5, localObject4);
        boolean bool2 = j;
        String str;
        if (bool2)
        {
          localObject5 = h.a(a);
          localObject4 = "ads";
          str = "GotCachedVideoAsset";
          ((h.a)localObject5).a((String)localObject4, str, (Map)localObject3);
        }
        else
        {
          localObject5 = h.a(a);
          localObject4 = "ads";
          str = "VideoAssetDownloaded";
          ((h.a)localObject5).a((String)localObject4, str, (Map)localObject3);
        }
        localObject3 = h.c(a);
        localObject2 = d;
        localObject5 = h.b(a);
        if (localObject5 == null)
        {
          bool2 = false;
          localObject5 = null;
        }
        else
        {
          localObject5 = ba).f;
        }
        localObject2 = ((c)localObject3).a((String)localObject2, (String)localObject5);
        h.c();
        ((List)localObject2).size();
        localObject2 = ((List)localObject2).iterator();
        for (;;)
        {
          bool3 = ((Iterator)localObject2).hasNext();
          if (!bool3) {
            break;
          }
          localObject3 = (a)((Iterator)localObject2).next();
          long l2 = c;
          localObject5 = Long.valueOf(l2);
          bool2 = ((List)localObject1).contains(localObject5);
          if (!bool2)
          {
            long l3 = c;
            localObject3 = Long.valueOf(l3);
            ((List)localObject1).add(localObject3);
          }
        }
      }
    }
    long l4 = ba).d;
    paramb = Long.valueOf(l4);
    boolean bool4 = ((List)localObject1).contains(paramb);
    if (!bool4)
    {
      l4 = ba).d;
      paramb = Long.valueOf(l4);
      ((List)localObject1).add(paramb);
    }
    paramb = ((List)localObject1).iterator();
    for (;;)
    {
      boolean bool5 = paramb.hasNext();
      if (!bool5) {
        break;
      }
      localObject1 = (Long)paramb.next();
      long l5 = ((Long)localObject1).longValue();
      h.c();
      localObject2 = h.a(a);
      bool3 = true;
      ((h.a)localObject2).a(l5, bool3);
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.h.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */