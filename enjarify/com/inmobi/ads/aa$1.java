package com.inmobi.ads;

import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import com.moat.analytics.mobile.inm.NativeDisplayTracker;
import com.moat.analytics.mobile.inm.NativeDisplayTracker.MoatUserInteractionType;

final class aa$1
  implements View.OnTouchListener
{
  aa$1(aa paramaa) {}
  
  public final boolean onTouch(View paramView, MotionEvent paramMotionEvent)
  {
    paramView = aa.a(a);
    paramMotionEvent = NativeDisplayTracker.MoatUserInteractionType.TOUCH;
    paramView.reportUserInteractionEvent(paramMotionEvent);
    aa.a(a).hashCode();
    return true;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.aa.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */