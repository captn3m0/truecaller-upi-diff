package com.inmobi.ads;

import android.text.TextUtils;
import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;
import java.io.StringWriter;
import java.util.Iterator;
import java.util.List;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

class ac
{
  private static final String a = "ac";
  
  private static String a(Node paramNode)
  {
    StringWriter localStringWriter = new java/io/StringWriter;
    localStringWriter.<init>();
    try
    {
      localObject1 = TransformerFactory.newInstance();
      localObject1 = ((TransformerFactory)localObject1).newTransformer();
      localObject2 = "omit-xml-declaration";
      localObject3 = "yes";
      ((Transformer)localObject1).setOutputProperty((String)localObject2, (String)localObject3);
      localObject2 = new javax/xml/transform/dom/DOMSource;
      ((DOMSource)localObject2).<init>(paramNode);
      paramNode = new javax/xml/transform/stream/StreamResult;
      paramNode.<init>(localStringWriter);
      ((Transformer)localObject1).transform((Source)localObject2, paramNode);
    }
    catch (TransformerException paramNode)
    {
      Object localObject1 = Logger.InternalLogLevel.INTERNAL;
      Object localObject2 = a;
      Object localObject3 = new java/lang/StringBuilder;
      String str = "Exception while converting Moat node to string : ";
      ((StringBuilder)localObject3).<init>(str);
      paramNode = paramNode.getMessage();
      ((StringBuilder)localObject3).append(paramNode);
      paramNode = ((StringBuilder)localObject3).toString();
      Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, paramNode);
    }
    return localStringWriter.toString();
  }
  
  static void a(Document paramDocument, bt parambt)
  {
    Object localObject1 = "Verification";
    paramDocument = bq.a(paramDocument, (String)localObject1);
    if (paramDocument != null)
    {
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      paramDocument = paramDocument.iterator();
      int j;
      Object localObject2;
      Object localObject3;
      Object localObject4;
      String str1;
      int i;
      do
      {
        boolean bool1;
        do
        {
          bool1 = paramDocument.hasNext();
          j = 0;
          if (!bool1) {
            break;
          }
          localObject2 = (Element)paramDocument.next();
          localObject3 = ((Element)localObject2).getChildNodes();
          localObject4 = "Moat";
          str1 = "vendor";
          localObject2 = ((Element)localObject2).getAttribute(str1);
          bool1 = ((String)localObject4).equals(localObject2);
        } while (!bool1);
        i = ((NodeList)localObject3).getLength();
      } while (i <= 0);
      for (;;)
      {
        i = ((NodeList)localObject3).getLength();
        if (j >= i) {
          break;
        }
        localObject2 = ((NodeList)localObject3).item(j);
        localObject4 = "ViewableImpression";
        str1 = ((Node)localObject2).getNodeName();
        boolean bool2 = ((String)localObject4).equals(str1);
        if (bool2)
        {
          localObject2 = a((Node)localObject2);
          bool2 = TextUtils.isEmpty((CharSequence)localObject2);
          if (!bool2)
          {
            ((StringBuilder)localObject1).append((String)localObject2);
            ((StringBuilder)localObject1).append(";");
            localObject4 = Logger.InternalLogLevel.INTERNAL;
            str1 = a;
            String str2 = "Found Moat Verification tag in VAST with value : ";
            localObject2 = String.valueOf(localObject2);
            localObject2 = str2.concat((String)localObject2);
            Logger.a((Logger.InternalLogLevel)localObject4, str1, (String)localObject2);
          }
        }
        j += 1;
      }
      int k = ((StringBuilder)localObject1).length();
      if (k != 0)
      {
        paramDocument = Logger.InternalLogLevel.INTERNAL;
        localObject2 = a;
        localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>("Moat VastIDs in VAST : ");
        localObject4 = ((StringBuilder)localObject1).toString();
        ((StringBuilder)localObject3).append((String)localObject4);
        localObject3 = ((StringBuilder)localObject3).toString();
        Logger.a(paramDocument, (String)localObject2, (String)localObject3);
        paramDocument = new com/inmobi/ads/NativeTracker;
        localObject1 = ((StringBuilder)localObject1).toString();
        localObject2 = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_MOAT;
        localObject3 = null;
        paramDocument.<init>((String)localObject1, 0, (NativeTracker.TrackerEventType)localObject2, null);
        parambt.a(paramDocument);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.ac
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */