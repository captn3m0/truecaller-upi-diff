package com.inmobi.ads;

import android.content.Context;
import android.database.ContentObserver;
import android.os.Handler;
import com.inmobi.commons.core.utilities.b.b;
import java.lang.ref.WeakReference;

final class r$a
  extends ContentObserver
{
  private Context a;
  private int b;
  private WeakReference c;
  private boolean d;
  
  r$a(Context paramContext, r paramr)
  {
    super(localHandler);
    a = paramContext;
    b = -1;
    d = false;
    paramContext = new java/lang/ref/WeakReference;
    paramContext.<init>(paramr);
    c = paramContext;
  }
  
  public final void onChange(boolean paramBoolean)
  {
    super.onChange(paramBoolean);
    Context localContext = a;
    if (localContext != null)
    {
      paramBoolean = b.a(localContext);
      boolean bool1 = b;
      if (paramBoolean != bool1)
      {
        b = paramBoolean;
        r localr = (r)c.get();
        boolean bool2 = d;
        if ((!bool2) && (localr != null)) {
          r.a(localr, paramBoolean);
        }
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.r.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */