package com.inmobi.ads;

import android.os.Handler;
import android.os.Looper;
import android.view.View;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.WeakHashMap;

abstract class ca
{
  private static final String d = "ca";
  boolean a = true;
  final Map b;
  ca.c c;
  private final ArrayList e;
  private long f = 0L;
  private final ca.a g;
  private final ca.b h;
  private final Handler i;
  private boolean j;
  
  ca(ca.a parama)
  {
    this(localWeakHashMap, parama, localHandler);
  }
  
  private ca(Map paramMap, ca.a parama, Handler paramHandler)
  {
    b = paramMap;
    g = parama;
    i = paramHandler;
    paramMap = new com/inmobi/ads/ca$b;
    paramMap.<init>(this);
    h = paramMap;
    paramMap = new java/util/ArrayList;
    paramMap.<init>(50);
    e = paramMap;
  }
  
  protected abstract int a();
  
  final View a(Object paramObject)
  {
    Object localObject1 = null;
    if (paramObject == null) {
      return null;
    }
    Iterator localIterator = b.entrySet().iterator();
    Map.Entry localEntry;
    boolean bool2;
    do
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      localEntry = (Map.Entry)localIterator.next();
      Object localObject2 = getValued;
      bool2 = localObject2.equals(paramObject);
    } while (!bool2);
    paramObject = localEntry.getKey();
    localObject1 = paramObject;
    localObject1 = (View)paramObject;
    if (localObject1 != null) {
      a((View)localObject1);
    }
    return (View)localObject1;
  }
  
  protected final void a(View paramView)
  {
    Map localMap = b;
    paramView = (ca.d)localMap.remove(paramView);
    if (paramView != null)
    {
      long l1 = f;
      long l2 = 1L;
      l1 -= l2;
      f = l1;
      paramView = b;
      int k = paramView.size();
      if (k == 0) {
        c();
      }
    }
  }
  
  protected final void a(View paramView, Object paramObject, int paramInt)
  {
    ca.d locald = (ca.d)b.get(paramView);
    if (locald == null)
    {
      locald = new com/inmobi/ads/ca$d;
      locald.<init>();
      Map localMap = b;
      localMap.put(paramView, locald);
      l1 = f;
      l2 = 1L;
      l1 += l2;
      f = l1;
    }
    a = paramInt;
    long l1 = f;
    b = l1;
    c = paramView;
    d = paramObject;
    long l3 = 50;
    long l2 = l1 % l3;
    long l4 = 0L;
    paramInt = l2 < l4;
    if (paramInt == 0)
    {
      l1 -= l3;
      paramView = b.entrySet().iterator();
      boolean bool;
      for (;;)
      {
        bool = paramView.hasNext();
        if (!bool) {
          break;
        }
        paramObject = (Map.Entry)paramView.next();
        Object localObject = (ca.d)((Map.Entry)paramObject).getValue();
        l2 = b;
        paramInt = l2 < l1;
        if (paramInt < 0)
        {
          localObject = e;
          paramObject = ((Map.Entry)paramObject).getKey();
          ((ArrayList)localObject).add(paramObject);
        }
      }
      paramView = e.iterator();
      for (;;)
      {
        bool = paramView.hasNext();
        if (!bool) {
          break;
        }
        paramObject = (View)paramView.next();
        a((View)paramObject);
      }
      paramView = e;
      paramView.clear();
    }
    int m = 1;
    paramObject = b;
    int k = ((Map)paramObject).size();
    if (m == k) {
      d();
    }
  }
  
  protected abstract void b();
  
  public void c()
  {
    h.run();
    i.removeCallbacksAndMessages(null);
    j = false;
    a = true;
  }
  
  public void d()
  {
    a = false;
    g();
  }
  
  protected void e()
  {
    f();
    c = null;
    a = true;
  }
  
  protected final void f()
  {
    b.clear();
    i.removeMessages(0);
    j = false;
  }
  
  final void g()
  {
    boolean bool = j;
    if (!bool)
    {
      bool = a;
      if (!bool)
      {
        j = true;
        Handler localHandler = i;
        ca.b localb = h;
        long l = a();
        localHandler.postDelayed(localb, l);
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.ca
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */