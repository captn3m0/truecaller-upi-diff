package com.inmobi.ads;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnVideoSizeChangedListener;

final class NativeVideoView$1
  implements MediaPlayer.OnVideoSizeChangedListener
{
  NativeVideoView$1(NativeVideoView paramNativeVideoView) {}
  
  public final void onVideoSizeChanged(MediaPlayer paramMediaPlayer, int paramInt1, int paramInt2)
  {
    NativeVideoView localNativeVideoView = a;
    paramInt2 = paramMediaPlayer.getVideoWidth();
    NativeVideoView.a(localNativeVideoView, paramInt2);
    localNativeVideoView = a;
    int i = paramMediaPlayer.getVideoHeight();
    NativeVideoView.b(localNativeVideoView, i);
    paramMediaPlayer = a;
    i = NativeVideoView.a(paramMediaPlayer);
    if (i != 0)
    {
      paramMediaPlayer = a;
      i = NativeVideoView.b(paramMediaPlayer);
      if (i != 0)
      {
        paramMediaPlayer = a;
        paramMediaPlayer.requestLayout();
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.NativeVideoView.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */