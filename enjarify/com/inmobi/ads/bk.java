package com.inmobi.ads;

import android.content.Context;
import android.os.Build.VERSION;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout.LayoutParams;

class bk
  extends NativeScrollableContainer
{
  private RecyclerView a;
  private boolean b = false;
  
  public bk(Context paramContext)
  {
    super(paramContext, 1);
  }
  
  public final void a(ai paramai, au paramau, int paramInt1, int paramInt2, NativeScrollableContainer.a parama)
  {
    paramai = (FrameLayout.LayoutParams)NativeViewFactory.a(paramai.a(0), this);
    int i = Build.VERSION.SDK_INT;
    int j = 20;
    int k = 17;
    if (i >= k)
    {
      paramai.setMarginStart(j);
      paramai.setMarginEnd(j);
    }
    else
    {
      paramai.setMargins(j, 0, j, 0);
    }
    gravity = paramInt2;
    paramai = new android/support/v7/widget/RecyclerView;
    Object localObject = getContext();
    paramai.<init>((Context)localObject);
    a = paramai;
    paramai = a;
    localObject = new android/widget/FrameLayout$LayoutParams;
    i = -1;
    ((FrameLayout.LayoutParams)localObject).<init>(i, i);
    paramai.setLayoutParams((ViewGroup.LayoutParams)localObject);
    paramai = new android/support/v7/widget/LinearLayoutManager;
    localObject = getContext();
    paramai.<init>((Context)localObject, 0, false);
    a.setLayoutManager(paramai);
    paramai = a;
    addView(paramai);
    paramai = a;
    paramau = (NativeRecyclerViewAdapter)paramau;
    paramai.setAdapter(paramau);
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.bk
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */