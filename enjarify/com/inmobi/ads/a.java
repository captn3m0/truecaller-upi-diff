package com.inmobi.ads;

import android.content.ContentValues;
import java.util.HashSet;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class a
{
  private static final String i = "a";
  final int a;
  final String b;
  final long c;
  long d;
  final String e;
  String f;
  String g;
  boolean h;
  private final String j;
  private final String k;
  private long l;
  private String m;
  private InMobiAdRequest.MonetizationContext n;
  
  a(ContentValues paramContentValues)
  {
    int i1 = paramContentValues.getAsInteger("id").intValue();
    a = i1;
    Object localObject = paramContentValues.getAsString("ad_type");
    j = ((String)localObject);
    localObject = paramContentValues.getAsString("ad_size");
    k = ((String)localObject);
    localObject = paramContentValues.getAsString("asset_urls");
    m = ((String)localObject);
    localObject = paramContentValues.getAsString("ad_content");
    b = ((String)localObject);
    long l1 = paramContentValues.getAsLong("placement_id").longValue();
    c = l1;
    l1 = paramContentValues.getAsLong("insertion_ts").longValue();
    d = l1;
    l1 = paramContentValues.getAsLong("expiry_duration").longValue();
    l = l1;
    localObject = paramContentValues.getAsString("imp_id");
    e = ((String)localObject);
    localObject = paramContentValues.getAsString("client_request_id");
    f = ((String)localObject);
    localObject = InMobiAdRequest.MonetizationContext.fromValue(paramContentValues.getAsString("m10_context"));
    n = ((InMobiAdRequest.MonetizationContext)localObject);
    localObject = n;
    if (localObject == null)
    {
      localObject = InMobiAdRequest.MonetizationContext.MONETIZATION_CONTEXT_ACTIVITY;
      n = ((InMobiAdRequest.MonetizationContext)localObject);
    }
    localObject = paramContentValues.getAsString("web_vast");
    g = ((String)localObject);
    localObject = "preload_webView";
    paramContentValues = paramContentValues.getAsInteger((String)localObject);
    int i2 = paramContentValues.intValue();
    if (i2 != 0)
    {
      i2 = 1;
    }
    else
    {
      i2 = 0;
      paramContentValues = null;
    }
    h = i2;
  }
  
  private a(JSONObject paramJSONObject, long paramLong1, String paramString1, String paramString2, String paramString3, String paramString4, InMobiAdRequest.MonetizationContext paramMonetizationContext, long paramLong2)
  {
    this(paramJSONObject, null, paramLong1, paramString1, paramString2, paramString3, paramString4, paramMonetizationContext, false, paramLong2);
  }
  
  a(JSONObject paramJSONObject, String paramString1, long paramLong1, String paramString2, String paramString3, String paramString4, String paramString5, InMobiAdRequest.MonetizationContext paramMonetizationContext, boolean paramBoolean, long paramLong2)
  {
    a = -1;
    paramJSONObject = paramJSONObject.toString();
    b = paramJSONObject;
    m = paramString1;
    c = paramLong1;
    j = paramString2;
    k = paramString3;
    long l1 = System.currentTimeMillis();
    d = l1;
    e = paramString4;
    f = paramString5;
    n = paramMonetizationContext;
    g = "";
    h = paramBoolean;
    l = paramLong2;
  }
  
  public ContentValues a()
  {
    ContentValues localContentValues = new android/content/ContentValues;
    localContentValues.<init>();
    Object localObject = j;
    localContentValues.put("ad_type", (String)localObject);
    localObject = k;
    localContentValues.put("ad_size", (String)localObject);
    localObject = m;
    localContentValues.put("asset_urls", (String)localObject);
    localObject = b;
    localContentValues.put("ad_content", (String)localObject);
    localObject = Long.valueOf(c);
    localContentValues.put("placement_id", (Long)localObject);
    localObject = Long.valueOf(d);
    localContentValues.put("insertion_ts", (Long)localObject);
    long l1 = l;
    localObject = Long.valueOf(l1);
    localContentValues.put("expiry_duration", (Long)localObject);
    localObject = e;
    localContentValues.put("imp_id", (String)localObject);
    localObject = f;
    localContentValues.put("client_request_id", (String)localObject);
    localObject = n.getValue();
    localContentValues.put("m10_context", (String)localObject);
    String str = g;
    if (str != null)
    {
      localObject = "web_vast";
      localContentValues.put((String)localObject, str);
    }
    localObject = Integer.valueOf(h);
    localContentValues.put("preload_webView", (Integer)localObject);
    return localContentValues;
  }
  
  final long b()
  {
    long l1 = l;
    long l2 = -1;
    boolean bool = l1 < l2;
    if (!bool) {
      return l2;
    }
    return d + l1;
  }
  
  public final Set c()
  {
    HashSet localHashSet = new java/util/HashSet;
    localHashSet.<init>();
    Object localObject1 = m;
    if (localObject1 != null)
    {
      int i1 = ((String)localObject1).length();
      if (i1 != 0) {
        try
        {
          localObject1 = new org/json/JSONArray;
          localObject2 = m;
          ((JSONArray)localObject1).<init>((String)localObject2);
          int i2 = ((JSONArray)localObject1).length();
          if (i2 == 0) {
            return localHashSet;
          }
          i2 = 0;
          localObject2 = null;
          for (;;)
          {
            int i3 = ((JSONArray)localObject1).length();
            if (i2 >= i3) {
              break;
            }
            localObject3 = ((JSONArray)localObject1).getString(i2);
            localHashSet.add(localObject3);
            i2 += 1;
          }
          return localHashSet;
        }
        catch (JSONException localJSONException)
        {
          Object localObject2 = com.inmobi.commons.core.a.a.a();
          Object localObject3 = new com/inmobi/commons/core/e/a;
          ((com.inmobi.commons.core.e.a)localObject3).<init>(localJSONException);
          ((com.inmobi.commons.core.a.a)localObject2).a((com.inmobi.commons.core.e.a)localObject3);
          return localHashSet;
        }
      }
    }
    return localHashSet;
  }
  
  final String d()
  {
    try
    {
      JSONObject localJSONObject = new org/json/JSONObject;
      localObject = b;
      localJSONObject.<init>((String)localObject);
      localObject = "markupType";
      boolean bool = localJSONObject.isNull((String)localObject);
      if (bool) {
        return "";
      }
      localObject = "markupType";
      return localJSONObject.getString((String)localObject);
    }
    catch (JSONException localJSONException)
    {
      Object localObject = com.inmobi.commons.core.a.a.a();
      com.inmobi.commons.core.e.a locala = new com/inmobi/commons/core/e/a;
      locala.<init>(localJSONException);
      ((com.inmobi.commons.core.a.a)localObject).a(locala);
    }
    return "";
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */