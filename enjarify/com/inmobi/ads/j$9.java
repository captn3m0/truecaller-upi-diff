package com.inmobi.ads;

import android.os.Handler;
import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

final class j$9
  implements ad.c
{
  j$9(j paramj, WeakReference paramWeakReference) {}
  
  public final void a()
  {
    Object localObject = b;
    boolean bool = v;
    if (bool) {
      return;
    }
    localObject = (j.b)a.get();
    if (localObject != null)
    {
      b.a((j.b)localObject, "AVFB", "");
      ((j.b)localObject).b();
      return;
    }
    b.g();
  }
  
  public final void a(String paramString1, String paramString2, Map paramMap)
  {
    b.c(paramString1, paramString2, paramMap);
  }
  
  public final void a(Map paramMap)
  {
    Object localObject = b;
    boolean bool = v;
    if (bool) {
      return;
    }
    localObject = (j.b)a.get();
    if (localObject != null)
    {
      HashMap localHashMap = new java/util/HashMap;
      localHashMap.<init>(paramMap);
      ((j.b)localObject).b(localHashMap);
      return;
    }
    b.g();
  }
  
  public final void b()
  {
    Object localObject1 = b;
    Object localObject2 = "AdRendered";
    ((j)localObject1).d((String)localObject2);
    localObject1 = b;
    boolean bool = v;
    if (bool) {
      return;
    }
    localObject1 = b.r;
    localObject2 = new com/inmobi/ads/j$9$1;
    ((j.9.1)localObject2).<init>(this);
    ((Handler)localObject1).post((Runnable)localObject2);
  }
  
  public final void c()
  {
    Object localObject = b;
    boolean bool = v;
    if (bool) {
      return;
    }
    localObject = (j.b)a.get();
    if (localObject != null)
    {
      ((j.b)localObject).c();
      return;
    }
    b.g();
  }
  
  public final void d()
  {
    Object localObject1 = Logger.InternalLogLevel.DEBUG;
    String str = "InMobi";
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("Successfully impressed ad for placement id: ");
    j localj = b;
    long l = b;
    ((StringBuilder)localObject2).append(l);
    localObject2 = ((StringBuilder)localObject2).toString();
    Logger.a((Logger.InternalLogLevel)localObject1, str, (String)localObject2);
    localObject1 = b;
    boolean bool = v;
    if (bool) {
      return;
    }
    localObject1 = (j.b)a.get();
    if (localObject1 != null)
    {
      ((j.b)localObject1).g();
      return;
    }
    b.g();
  }
  
  public final void e()
  {
    Object localObject1 = Logger.InternalLogLevel.DEBUG;
    Object localObject2 = "InMobi";
    Object localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>("Ad interaction for placement id: ");
    j localj = b;
    long l = b;
    ((StringBuilder)localObject3).append(l);
    localObject3 = ((StringBuilder)localObject3).toString();
    Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, (String)localObject3);
    localObject1 = b;
    boolean bool = v;
    if (bool) {
      return;
    }
    localObject1 = (j.b)a.get();
    if (localObject1 != null)
    {
      localObject2 = new java/util/HashMap;
      ((HashMap)localObject2).<init>();
      ((j.b)localObject1).a((Map)localObject2);
      return;
    }
    b.g();
  }
  
  public final void f()
  {
    Object localObject1 = b;
    boolean bool = v;
    if (bool) {
      return;
    }
    localObject1 = Logger.InternalLogLevel.DEBUG;
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("Ad dismissed for placement id: ");
    long l = b.b;
    ((StringBuilder)localObject2).append(l);
    localObject2 = ((StringBuilder)localObject2).toString();
    Logger.a((Logger.InternalLogLevel)localObject1, "InMobi", (String)localObject2);
    localObject1 = b.r;
    j.9.2 local2 = new com/inmobi/ads/j$9$2;
    local2.<init>(this);
    ((Handler)localObject1).post(local2);
  }
  
  public final void g()
  {
    Object localObject = b;
    boolean bool = v;
    if (bool) {
      return;
    }
    localObject = (j.b)a.get();
    if (localObject != null)
    {
      ((j.b)localObject).f();
      return;
    }
    b.g();
  }
  
  public final void h()
  {
    Object localObject = b;
    boolean bool = v;
    if (bool) {
      return;
    }
    localObject = (j.b)a.get();
    if (localObject != null)
    {
      ((j.b)localObject).h();
      return;
    }
    b.g();
  }
  
  public final void i()
  {
    Object localObject = b;
    boolean bool = v;
    if (bool) {
      return;
    }
    localObject = (j.b)a.get();
    if (localObject != null)
    {
      ((j.b)localObject).j();
      return;
    }
    b.g();
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.j.9
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */