package com.inmobi.ads;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

final class bs$1
  implements ThreadFactory
{
  private final AtomicInteger a;
  
  bs$1()
  {
    AtomicInteger localAtomicInteger = new java/util/concurrent/atomic/AtomicInteger;
    localAtomicInteger.<init>(1);
    a = localAtomicInteger;
  }
  
  public final Thread newThread(Runnable paramRunnable)
  {
    Thread localThread = new java/lang/Thread;
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("VastNetworkTask #");
    int i = a.getAndIncrement();
    ((StringBuilder)localObject).append(i);
    localObject = ((StringBuilder)localObject).toString();
    localThread.<init>(paramRunnable, (String)localObject);
    return localThread;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.bs.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */