package com.inmobi.ads;

import android.content.Context;
import android.view.View;
import android.widget.Button;

final class NativeViewFactory$13
  extends NativeViewFactory.c
{
  NativeViewFactory$13(NativeViewFactory paramNativeViewFactory)
  {
    super(paramNativeViewFactory);
  }
  
  protected final View a(Context paramContext)
  {
    Button localButton = new android/widget/Button;
    paramContext = paramContext.getApplicationContext();
    localButton.<init>(paramContext);
    return localButton;
  }
  
  protected final void a(View paramView, ag paramag, b paramb)
  {
    super.a(paramView, paramag, paramb);
    NativeViewFactory.a((Button)paramView, paramag);
  }
  
  public final boolean a(View paramView)
  {
    boolean bool = paramView instanceof Button;
    if (!bool) {
      return false;
    }
    NativeViewFactory.a((Button)paramView);
    return super.a((View)paramView);
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.NativeViewFactory.13
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */