package com.inmobi.ads;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import java.lang.ref.WeakReference;

class w
  extends bv
{
  private static final String d = "w";
  private final WeakReference e;
  private final bw f;
  private final ae g;
  private final ad h;
  
  w(ba paramba, bw parambw)
  {
    super(paramba);
    WeakReference localWeakReference = new java/lang/ref/WeakReference;
    Context localContext = paramba.j();
    localWeakReference.<init>(localContext);
    e = localWeakReference;
    f = parambw;
    h = paramba;
    paramba = new com/inmobi/ads/ae;
    paramba.<init>(0);
    g = paramba;
  }
  
  public final View a(View paramView, ViewGroup paramViewGroup, boolean paramBoolean)
  {
    View localView = f.b();
    if (localView != null)
    {
      ae localae = g;
      Context localContext = (Context)e.get();
      ad localad = h;
      localae.a(localContext, localView, localad);
    }
    return f.a(paramView, paramViewGroup, paramBoolean);
  }
  
  public final void a(int paramInt)
  {
    f.a(paramInt);
  }
  
  /* Error */
  public final void a(Context paramContext, int paramInt)
  {
    // Byte code:
    //   0: iload_2
    //   1: tableswitch	default:+27->28, 0:+50->51, 1:+43->44, 2:+30->31
    //   28: goto +63 -> 91
    //   31: aload_0
    //   32: getfield 45	com/inmobi/ads/w:g	Lcom/inmobi/ads/ae;
    //   35: astore_3
    //   36: aload_3
    //   37: aload_1
    //   38: invokevirtual 69	com/inmobi/ads/ae:a	(Landroid/content/Context;)V
    //   41: goto +50 -> 91
    //   44: aload_1
    //   45: invokestatic 72	com/inmobi/ads/ae:c	(Landroid/content/Context;)V
    //   48: goto +43 -> 91
    //   51: aload_1
    //   52: invokestatic 74	com/inmobi/ads/ae:b	(Landroid/content/Context;)V
    //   55: goto +36 -> 91
    //   58: astore_3
    //   59: goto +42 -> 101
    //   62: astore_3
    //   63: aload_3
    //   64: invokevirtual 80	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   67: pop
    //   68: invokestatic 85	com/inmobi/commons/core/a/a:a	()Lcom/inmobi/commons/core/a/a;
    //   71: astore 4
    //   73: new 87	com/inmobi/commons/core/e/a
    //   76: astore 5
    //   78: aload 5
    //   80: aload_3
    //   81: invokespecial 90	com/inmobi/commons/core/e/a:<init>	(Ljava/lang/Throwable;)V
    //   84: aload 4
    //   86: aload 5
    //   88: invokevirtual 93	com/inmobi/commons/core/a/a:a	(Lcom/inmobi/commons/core/e/a;)V
    //   91: aload_0
    //   92: getfield 36	com/inmobi/ads/w:f	Lcom/inmobi/ads/bw;
    //   95: aload_1
    //   96: iload_2
    //   97: invokevirtual 96	com/inmobi/ads/bw:a	(Landroid/content/Context;I)V
    //   100: return
    //   101: aload_0
    //   102: getfield 36	com/inmobi/ads/w:f	Lcom/inmobi/ads/bw;
    //   105: aload_1
    //   106: iload_2
    //   107: invokevirtual 96	com/inmobi/ads/bw:a	(Landroid/content/Context;I)V
    //   110: aload_3
    //   111: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	112	0	this	w
    //   0	112	1	paramContext	Context
    //   0	112	2	paramInt	int
    //   35	2	3	localae	ae
    //   58	1	3	localObject	Object
    //   62	49	3	localException	Exception
    //   71	14	4	locala	com.inmobi.commons.core.a.a
    //   76	11	5	locala1	com.inmobi.commons.core.e.a
    // Exception table:
    //   from	to	target	type
    //   31	35	58	finally
    //   37	41	58	finally
    //   44	48	58	finally
    //   51	55	58	finally
    //   63	68	58	finally
    //   68	71	58	finally
    //   73	76	58	finally
    //   80	84	58	finally
    //   86	91	58	finally
    //   31	35	62	java/lang/Exception
    //   37	41	62	java/lang/Exception
    //   44	48	62	java/lang/Exception
    //   51	55	62	java/lang/Exception
  }
  
  /* Error */
  public final void a(View... paramVarArgs)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 99	com/inmobi/ads/bw:a	Lcom/inmobi/ads/AdContainer;
    //   4: astore_2
    //   5: aload_2
    //   6: checkcast 25	com/inmobi/ads/ba
    //   9: astore_2
    //   10: aload_2
    //   11: invokevirtual 102	com/inmobi/ads/ba:getVideoContainerView	()Landroid/view/View;
    //   14: astore_3
    //   15: aload_3
    //   16: checkcast 104	com/inmobi/ads/NativeVideoWrapper
    //   19: astore_3
    //   20: aload_0
    //   21: getfield 34	com/inmobi/ads/w:e	Ljava/lang/ref/WeakReference;
    //   24: astore 4
    //   26: aload 4
    //   28: invokevirtual 55	java/lang/ref/WeakReference:get	()Ljava/lang/Object;
    //   31: astore 4
    //   33: aload 4
    //   35: astore 5
    //   37: aload 4
    //   39: checkcast 57	android/content/Context
    //   42: astore 5
    //   44: aload_0
    //   45: getfield 36	com/inmobi/ads/w:f	Lcom/inmobi/ads/bw;
    //   48: astore 4
    //   50: aload 4
    //   52: invokevirtual 107	com/inmobi/ads/bw:c	()Lcom/inmobi/ads/b;
    //   55: astore 4
    //   57: aload 4
    //   59: getfield 113	com/inmobi/ads/b:o	Lcom/inmobi/ads/b$k;
    //   62: astore 6
    //   64: aload 5
    //   66: ifnull +175 -> 241
    //   69: aload_3
    //   70: ifnull +171 -> 241
    //   73: aload_2
    //   74: getfield 119	com/inmobi/ads/ad:i	Z
    //   77: istore 7
    //   79: iload 7
    //   81: ifne +160 -> 241
    //   84: aload_3
    //   85: invokevirtual 123	com/inmobi/ads/NativeVideoWrapper:getVideoView	()Lcom/inmobi/ads/NativeVideoView;
    //   88: astore_3
    //   89: aload_0
    //   90: getfield 45	com/inmobi/ads/w:g	Lcom/inmobi/ads/ae;
    //   93: astore 4
    //   95: aload 4
    //   97: aload 5
    //   99: aload_3
    //   100: aload_2
    //   101: aload 6
    //   103: invokevirtual 126	com/inmobi/ads/ae:a	(Landroid/content/Context;Landroid/view/View;Lcom/inmobi/ads/ad;Lcom/inmobi/ads/b$k;)V
    //   106: aload_0
    //   107: getfield 36	com/inmobi/ads/w:f	Lcom/inmobi/ads/bw;
    //   110: astore 4
    //   112: aload 4
    //   114: invokevirtual 51	com/inmobi/ads/bw:b	()Landroid/view/View;
    //   117: astore 8
    //   119: aload_3
    //   120: invokevirtual 131	com/inmobi/ads/NativeVideoView:getTag	()Ljava/lang/Object;
    //   123: astore 4
    //   125: aload 4
    //   127: ifnull +114 -> 241
    //   130: aload 8
    //   132: ifnull +109 -> 241
    //   135: aload_3
    //   136: invokevirtual 131	com/inmobi/ads/NativeVideoView:getTag	()Ljava/lang/Object;
    //   139: astore_3
    //   140: aload_3
    //   141: checkcast 133	com/inmobi/ads/bb
    //   144: astore_3
    //   145: getstatic 139	com/inmobi/ads/AdContainer$RenderingProperties$PlacementType:PLACEMENT_TYPE_INLINE	Lcom/inmobi/ads/AdContainer$RenderingProperties$PlacementType;
    //   148: astore 4
    //   150: aload_2
    //   151: getfield 142	com/inmobi/ads/ad:b	Lcom/inmobi/ads/AdContainer$RenderingProperties;
    //   154: astore_2
    //   155: aload_2
    //   156: getfield 146	com/inmobi/ads/AdContainer$RenderingProperties:a	Lcom/inmobi/ads/AdContainer$RenderingProperties$PlacementType;
    //   159: astore_2
    //   160: aload 4
    //   162: aload_2
    //   163: if_acmpne +78 -> 241
    //   166: aload_3
    //   167: getfield 152	com/inmobi/ads/ag:v	Ljava/util/Map;
    //   170: astore_2
    //   171: ldc -102
    //   173: astore_3
    //   174: aload_2
    //   175: aload_3
    //   176: invokeinterface 159 2 0
    //   181: astore_2
    //   182: aload_2
    //   183: checkcast 161	java/lang/Boolean
    //   186: astore_2
    //   187: aload_2
    //   188: invokevirtual 165	java/lang/Boolean:booleanValue	()Z
    //   191: istore 9
    //   193: iload 9
    //   195: ifne +46 -> 241
    //   198: aload_0
    //   199: getfield 45	com/inmobi/ads/w:g	Lcom/inmobi/ads/ae;
    //   202: astore 10
    //   204: aload_0
    //   205: getfield 38	com/inmobi/ads/w:h	Lcom/inmobi/ads/ad;
    //   208: astore 11
    //   210: aload_0
    //   211: getfield 38	com/inmobi/ads/w:h	Lcom/inmobi/ads/ad;
    //   214: astore_2
    //   215: aload_2
    //   216: checkcast 25	com/inmobi/ads/ba
    //   219: astore_2
    //   220: aload_2
    //   221: getfield 169	com/inmobi/ads/ba:z	Lcom/inmobi/ads/ae$a;
    //   224: astore 12
    //   226: aload 10
    //   228: aload 5
    //   230: aload 8
    //   232: aload 11
    //   234: aload 12
    //   236: aload 6
    //   238: invokevirtual 172	com/inmobi/ads/ae:a	(Landroid/content/Context;Landroid/view/View;Lcom/inmobi/ads/ad;Lcom/inmobi/ads/ae$a;Lcom/inmobi/ads/b$k;)V
    //   241: aload_0
    //   242: getfield 36	com/inmobi/ads/w:f	Lcom/inmobi/ads/bw;
    //   245: aload_1
    //   246: invokevirtual 175	com/inmobi/ads/bw:a	([Landroid/view/View;)V
    //   249: return
    //   250: astore_2
    //   251: goto +33 -> 284
    //   254: astore_2
    //   255: aload_2
    //   256: invokevirtual 80	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   259: pop
    //   260: invokestatic 85	com/inmobi/commons/core/a/a:a	()Lcom/inmobi/commons/core/a/a;
    //   263: astore_3
    //   264: new 87	com/inmobi/commons/core/e/a
    //   267: astore 4
    //   269: aload 4
    //   271: aload_2
    //   272: invokespecial 90	com/inmobi/commons/core/e/a:<init>	(Ljava/lang/Throwable;)V
    //   275: aload_3
    //   276: aload 4
    //   278: invokevirtual 93	com/inmobi/commons/core/a/a:a	(Lcom/inmobi/commons/core/e/a;)V
    //   281: goto -40 -> 241
    //   284: aload_0
    //   285: getfield 36	com/inmobi/ads/w:f	Lcom/inmobi/ads/bw;
    //   288: aload_1
    //   289: invokevirtual 175	com/inmobi/ads/bw:a	([Landroid/view/View;)V
    //   292: aload_2
    //   293: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	294	0	this	w
    //   0	294	1	paramVarArgs	View[]
    //   4	217	2	localObject1	Object
    //   250	1	2	localObject2	Object
    //   254	39	2	localException	Exception
    //   14	262	3	localObject3	Object
    //   24	253	4	localObject4	Object
    //   35	194	5	localObject5	Object
    //   62	175	6	localk	b.k
    //   77	3	7	bool1	boolean
    //   117	114	8	localView	View
    //   191	3	9	bool2	boolean
    //   202	25	10	localae	ae
    //   208	25	11	localad	ad
    //   224	11	12	locala	ae.a
    // Exception table:
    //   from	to	target	type
    //   0	4	250	finally
    //   5	9	250	finally
    //   10	14	250	finally
    //   15	19	250	finally
    //   20	24	250	finally
    //   26	31	250	finally
    //   37	42	250	finally
    //   44	48	250	finally
    //   50	55	250	finally
    //   57	62	250	finally
    //   73	77	250	finally
    //   84	88	250	finally
    //   89	93	250	finally
    //   101	106	250	finally
    //   106	110	250	finally
    //   112	117	250	finally
    //   119	123	250	finally
    //   135	139	250	finally
    //   140	144	250	finally
    //   145	148	250	finally
    //   150	154	250	finally
    //   155	159	250	finally
    //   166	170	250	finally
    //   175	181	250	finally
    //   182	186	250	finally
    //   187	191	250	finally
    //   198	202	250	finally
    //   204	208	250	finally
    //   210	214	250	finally
    //   215	219	250	finally
    //   220	224	250	finally
    //   236	241	250	finally
    //   255	260	250	finally
    //   260	263	250	finally
    //   264	267	250	finally
    //   271	275	250	finally
    //   276	281	250	finally
    //   0	4	254	java/lang/Exception
    //   5	9	254	java/lang/Exception
    //   10	14	254	java/lang/Exception
    //   15	19	254	java/lang/Exception
    //   20	24	254	java/lang/Exception
    //   26	31	254	java/lang/Exception
    //   37	42	254	java/lang/Exception
    //   44	48	254	java/lang/Exception
    //   50	55	254	java/lang/Exception
    //   57	62	254	java/lang/Exception
    //   73	77	254	java/lang/Exception
    //   84	88	254	java/lang/Exception
    //   89	93	254	java/lang/Exception
    //   101	106	254	java/lang/Exception
    //   106	110	254	java/lang/Exception
    //   112	117	254	java/lang/Exception
    //   119	123	254	java/lang/Exception
    //   135	139	254	java/lang/Exception
    //   140	144	254	java/lang/Exception
    //   145	148	254	java/lang/Exception
    //   150	154	254	java/lang/Exception
    //   155	159	254	java/lang/Exception
    //   166	170	254	java/lang/Exception
    //   175	181	254	java/lang/Exception
    //   182	186	254	java/lang/Exception
    //   187	191	254	java/lang/Exception
    //   198	202	254	java/lang/Exception
    //   204	208	254	java/lang/Exception
    //   210	214	254	java/lang/Exception
    //   215	219	254	java/lang/Exception
    //   220	224	254	java/lang/Exception
    //   236	241	254	java/lang/Exception
  }
  
  public final View b()
  {
    return f.b();
  }
  
  final b c()
  {
    return f.c();
  }
  
  /* Error */
  public final void d()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 34	com/inmobi/ads/w:e	Ljava/lang/ref/WeakReference;
    //   4: astore_1
    //   5: aload_1
    //   6: invokevirtual 55	java/lang/ref/WeakReference:get	()Ljava/lang/Object;
    //   9: astore_1
    //   10: aload_1
    //   11: checkcast 57	android/content/Context
    //   14: astore_1
    //   15: aload_0
    //   16: getfield 99	com/inmobi/ads/bw:a	Lcom/inmobi/ads/AdContainer;
    //   19: astore_2
    //   20: aload_2
    //   21: checkcast 25	com/inmobi/ads/ba
    //   24: astore_2
    //   25: aload_2
    //   26: getfield 119	com/inmobi/ads/ad:i	Z
    //   29: istore_3
    //   30: iload_3
    //   31: ifne +20 -> 51
    //   34: aload_1
    //   35: ifnull +16 -> 51
    //   38: aload_0
    //   39: getfield 45	com/inmobi/ads/w:g	Lcom/inmobi/ads/ae;
    //   42: astore 4
    //   44: aload 4
    //   46: aload_1
    //   47: aload_2
    //   48: invokevirtual 178	com/inmobi/ads/ae:a	(Landroid/content/Context;Lcom/inmobi/ads/ad;)V
    //   51: aload_0
    //   52: getfield 36	com/inmobi/ads/w:f	Lcom/inmobi/ads/bw;
    //   55: invokevirtual 181	com/inmobi/ads/bw:d	()V
    //   58: return
    //   59: astore_1
    //   60: goto +33 -> 93
    //   63: astore_1
    //   64: aload_1
    //   65: invokevirtual 80	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   68: pop
    //   69: invokestatic 85	com/inmobi/commons/core/a/a:a	()Lcom/inmobi/commons/core/a/a;
    //   72: astore_2
    //   73: new 87	com/inmobi/commons/core/e/a
    //   76: astore 4
    //   78: aload 4
    //   80: aload_1
    //   81: invokespecial 90	com/inmobi/commons/core/e/a:<init>	(Ljava/lang/Throwable;)V
    //   84: aload_2
    //   85: aload 4
    //   87: invokevirtual 93	com/inmobi/commons/core/a/a:a	(Lcom/inmobi/commons/core/e/a;)V
    //   90: goto -39 -> 51
    //   93: aload_0
    //   94: getfield 36	com/inmobi/ads/w:f	Lcom/inmobi/ads/bw;
    //   97: invokevirtual 181	com/inmobi/ads/bw:d	()V
    //   100: aload_1
    //   101: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	102	0	this	w
    //   4	43	1	localObject1	Object
    //   59	1	1	localObject2	Object
    //   63	38	1	localException	Exception
    //   19	66	2	localObject3	Object
    //   29	2	3	bool	boolean
    //   42	44	4	localObject4	Object
    // Exception table:
    //   from	to	target	type
    //   0	4	59	finally
    //   5	9	59	finally
    //   10	14	59	finally
    //   15	19	59	finally
    //   20	24	59	finally
    //   25	29	59	finally
    //   38	42	59	finally
    //   47	51	59	finally
    //   64	69	59	finally
    //   69	72	59	finally
    //   73	76	59	finally
    //   80	84	59	finally
    //   85	90	59	finally
    //   0	4	63	java/lang/Exception
    //   5	9	63	java/lang/Exception
    //   10	14	63	java/lang/Exception
    //   15	19	63	java/lang/Exception
    //   20	24	63	java/lang/Exception
    //   25	29	63	java/lang/Exception
    //   38	42	63	java/lang/Exception
    //   47	51	63	java/lang/Exception
  }
  
  public final void e()
  {
    ae localae = g;
    Context localContext = (Context)e.get();
    View localView = f.b();
    ad localad = h;
    localae.a(localContext, localView, localad);
    super.e();
    e.clear();
    f.e();
  }
  
  public final bw.a f()
  {
    return f.f();
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.w
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */