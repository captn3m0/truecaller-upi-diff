package com.inmobi.ads;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import com.inmobi.rendering.RenderView;

public final class bx
  extends bw
{
  private final RenderView d;
  
  public bx(RenderView paramRenderView)
  {
    super(paramRenderView);
    d = paramRenderView;
  }
  
  public final View a()
  {
    RenderView localRenderView = d;
    a(localRenderView);
    return d;
  }
  
  public final View a(View paramView, ViewGroup paramViewGroup, boolean paramBoolean)
  {
    return a();
  }
  
  public final void a(int paramInt) {}
  
  public final void a(Context paramContext, int paramInt) {}
  
  public final void a(View... paramVarArgs) {}
  
  final b c()
  {
    return d.getAdConfig();
  }
  
  public final void d() {}
}

/* Location:
 * Qualified Name:     com.inmobi.ads.bx
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */