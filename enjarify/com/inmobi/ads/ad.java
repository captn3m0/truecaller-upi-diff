package com.inmobi.ads;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;
import com.inmobi.commons.core.utilities.d;
import com.inmobi.rendering.InMobiAdActivity;
import com.inmobi.rendering.RenderView;
import com.inmobi.rendering.RenderView.a;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.json.JSONException;
import org.json.JSONObject;

public class ad
  implements Application.ActivityLifecycleCallbacks, AdContainer
{
  private static final String y = "ad";
  private List A;
  private aq B;
  private int C;
  private m D;
  private ad E;
  private ag F;
  private String G;
  private ad H;
  private RenderView.a I;
  private final AdContainer.a J;
  private ExecutorService K;
  private Runnable L;
  protected ak a;
  public AdContainer.RenderingProperties b;
  b c;
  final String d;
  final String e;
  protected Set f;
  protected bw g;
  protected boolean h;
  public boolean i;
  protected boolean j;
  ad k;
  protected ad.c l;
  protected WeakReference m;
  WeakReference n;
  boolean o;
  int p;
  boolean q;
  boolean r;
  Intent s;
  RenderView t;
  RenderView u;
  int v;
  public List w;
  ae.a x;
  private Set z;
  
  ad(Context paramContext, AdContainer.RenderingProperties paramRenderingProperties, ak paramak, String paramString1, String paramString2, Set paramSet, b paramb)
  {
    Object localObject1 = new java/util/HashSet;
    ((HashSet)localObject1).<init>();
    z = ((Set)localObject1);
    localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    A = ((List)localObject1);
    localObject1 = null;
    i = false;
    WeakReference localWeakReference = new java/lang/ref/WeakReference;
    localWeakReference.<init>(null);
    m = localWeakReference;
    int i1 = -1;
    C = i1;
    o = false;
    p = 0;
    boolean bool = true;
    q = bool;
    r = false;
    F = null;
    G = null;
    s = null;
    Object localObject2 = new com/inmobi/ads/ad$1;
    ((ad.1)localObject2).<init>(this);
    J = ((AdContainer.a)localObject2);
    localObject2 = new com/inmobi/ads/ad$2;
    ((ad.2)localObject2).<init>(this);
    L = ((Runnable)localObject2);
    localObject2 = new com/inmobi/ads/ad$3;
    ((ad.3)localObject2).<init>(this);
    x = ((ae.a)localObject2);
    b = paramRenderingProperties;
    a = paramak;
    d = paramString1;
    e = paramString2;
    a(this);
    h = false;
    i = false;
    c = paramb;
    paramRenderingProperties = new com/inmobi/ads/m;
    paramRenderingProperties.<init>();
    D = paramRenderingProperties;
    if (paramSet != null)
    {
      paramRenderingProperties = new java/util/HashSet;
      paramRenderingProperties.<init>(paramSet);
      f = paramRenderingProperties;
    }
    paramRenderingProperties = a.d;
    long l1 = System.currentTimeMillis();
    z = l1;
    a(paramContext);
    v = i1;
    paramContext = Executors.newSingleThreadExecutor();
    K = paramContext;
    paramContext = K;
    paramRenderingProperties = L;
    paramContext.submit(paramRenderingProperties);
  }
  
  private aq A()
  {
    Object localObject = g;
    if (localObject == null) {
      localObject = null;
    } else {
      localObject = (ap)((bw)localObject).f();
    }
    if (localObject != null)
    {
      localObject = a;
      B = ((aq)localObject);
    }
    return B;
  }
  
  private void B()
  {
    Object localObject = (Context)m.get();
    boolean bool = localObject instanceof Activity;
    if (bool)
    {
      localObject = ((Activity)localObject).getApplication();
      ((Application)localObject).unregisterActivityLifecycleCallbacks(this);
    }
  }
  
  private Context C()
  {
    Object localObject = l();
    if (localObject == null) {
      localObject = (Context)m.get();
    }
    return (Context)localObject;
  }
  
  private static int a(String paramString)
  {
    Object localObject = Locale.US;
    paramString = paramString.toLowerCase((Locale)localObject).trim();
    int i1 = paramString.hashCode();
    int i2 = -934641255;
    int i3 = 5;
    int i4 = 4;
    int i5 = 1;
    int i6 = 3;
    int i7 = 2;
    if (i1 != i2)
    {
      i2 = -934524953;
      boolean bool5;
      if (i1 != i2)
      {
        if (i1 != 0)
        {
          i2 = 3127582;
          if (i1 != i2)
          {
            i2 = 3443508;
            if (i1 != i2)
            {
              i2 = 3532159;
              if (i1 != i2)
              {
                i2 = 110066619;
                if (i1 == i2)
                {
                  localObject = "fullscreen";
                  boolean bool1 = paramString.equals(localObject);
                  if (bool1)
                  {
                    int i8 = 6;
                    break label247;
                  }
                }
              }
              else
              {
                localObject = "skip";
                boolean bool2 = paramString.equals(localObject);
                if (bool2)
                {
                  int i9 = 2;
                  break label247;
                }
              }
            }
            else
            {
              localObject = "play";
              boolean bool3 = paramString.equals(localObject);
              if (bool3)
              {
                int i10 = 7;
                break label247;
              }
            }
          }
          else
          {
            localObject = "exit";
            boolean bool4 = paramString.equals(localObject);
            if (bool4)
            {
              int i11 = 5;
              break label247;
            }
          }
        }
        else
        {
          localObject = "";
          bool5 = paramString.equals(localObject);
          if (bool5)
          {
            bool5 = true;
            break label247;
          }
        }
      }
      else
      {
        localObject = "replay";
        bool5 = paramString.equals(localObject);
        if (bool5)
        {
          int i12 = 4;
          break label247;
        }
      }
    }
    else
    {
      localObject = "reload";
      boolean bool6 = paramString.equals(localObject);
      if (bool6)
      {
        i13 = 3;
        break label247;
      }
    }
    int i13 = -1;
    switch (i13)
    {
    default: 
      return 0;
    case 7: 
      return i3;
    case 6: 
      return i4;
    case 5: 
      return i5;
    case 3: 
    case 4: 
      label247:
      return i6;
    }
    return i7;
  }
  
  private ag a(ag paramag, ak paramak, String paramString)
  {
    Object localObject = (Context)m.get();
    boolean bool1 = com.inmobi.commons.core.utilities.b.a((Context)localObject, paramString);
    if (bool1) {
      return paramag;
    }
    paramString = paramString.split("\\|");
    bool1 = false;
    localObject = paramString[0];
    localObject = paramak.b((String)localObject);
    if (localObject == null)
    {
      paramak = f;
      return b(paramak, paramag);
    }
    boolean bool2 = localObject.equals(paramag);
    if (bool2) {
      return null;
    }
    int i1 = paramString.length;
    int i2 = 1;
    if (i2 != i1)
    {
      i1 = paramString.length;
      int i3 = 2;
      if (i3 != i1)
      {
        i1 = paramString.length;
        if (i1 > i3)
        {
          paramag = paramString[i3];
          i1 = ak.a(paramag);
          m = i1;
        }
        return (ag)localObject;
      }
    }
    m = i2;
    return (ag)localObject;
  }
  
  protected static ag a(ak paramak, ag paramag)
  {
    Object localObject1;
    Object localObject2;
    int i2;
    for (;;)
    {
      if (paramak == null) {
        return null;
      }
      localObject1 = j;
      localObject2 = null;
      if (localObject1 == null) {
        break label126;
      }
      int i1 = ((String)localObject1).length();
      if (i1 == 0) {
        break label126;
      }
      String str = "\\|";
      localObject1 = ((String)localObject1).split(str);
      i1 = localObject1.length;
      i2 = 1;
      if (i2 == i1)
      {
        int i3 = a(localObject1[0]);
        l = i3;
        return paramag;
      }
      localObject2 = localObject1[0];
      localObject2 = paramak.b((String)localObject2);
      if (localObject2 != null) {
        break;
      }
      paramak = f;
    }
    boolean bool = localObject2.equals(paramag);
    if (bool) {
      return null;
    }
    int i4 = a(localObject1[i2]);
    l = i4;
    return (ag)localObject2;
    label126:
    l = 0;
    return paramag;
  }
  
  private void a(int paramInt, ai paramai)
  {
    boolean bool = i;
    if (bool) {
      return;
    }
    Set localSet = z;
    Object localObject = Integer.valueOf(paramInt);
    localSet.add(localObject);
    long l1 = System.currentTimeMillis();
    z = l1;
    paramInt = h;
    if (paramInt != 0)
    {
      localObject = a(paramai);
      b(paramai, (Map)localObject);
      return;
    }
    A.add(paramai);
  }
  
  private void a(ag paramag, int paramInt, String paramString)
  {
    int i1 = 1;
    Object localObject;
    if (i1 == paramInt)
    {
      paramInt = com.inmobi.commons.core.utilities.b.a(paramString);
      i1 = 0;
      if (paramInt != 0)
      {
        paramag = (Context)m.get();
        if (paramag != null)
        {
          localObject = l();
          if (localObject == null)
          {
            localObject = l;
            if (localObject != null) {
              ((ad.c)localObject).c();
            }
          }
          InMobiAdActivity.a(null);
          InMobiAdActivity.a(u());
          localObject = new android/content/Intent;
          ((Intent)localObject).<init>(paramag, InMobiAdActivity.class);
          ((Intent)localObject).putExtra("com.inmobi.rendering.InMobiAdActivity.EXTRA_AD_ACTIVITY_TYPE", 100);
          ((Intent)localObject).putExtra("com.inmobi.rendering.InMobiAdActivity.IN_APP_BROWSER_URL", paramString);
          com.inmobi.commons.a.a.a(paramag, (Intent)localObject);
        }
      }
      else
      {
        a(paramString, null, paramag);
      }
    }
    else
    {
      localObject = s;
      a(paramString, (String)localObject, paramag);
    }
  }
  
  private void a(ag paramag, Map paramMap)
  {
    Object localObject = "ads";
    String str = "ReportClick";
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    a((String)localObject, str, localHashMap);
    int i1 = m;
    int i2 = 2;
    if (i2 == i1)
    {
      localObject = paramag;
      localObject = ((bb)paramag).b().f();
      if (localObject != null)
      {
        str = e;
        if (str == null)
        {
          str = r;
          if (str != null) {}
        }
        else
        {
          paramag = d;
          int i3 = paramag.size();
          if (i3 <= 0) {
            break label160;
          }
          paramag = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_CLICK;
          paramag = ((bp)localObject).a(paramag).iterator();
          for (;;)
          {
            boolean bool = paramag.hasNext();
            if (!bool) {
              break;
            }
            localObject = (NativeTracker)paramag.next();
            ag.a((NativeTracker)localObject, paramMap);
          }
          return;
        }
      }
      localObject = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_CLICK;
      paramag.a((NativeTracker.TrackerEventType)localObject, paramMap);
      label160:
      return;
    }
    localObject = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_CLICK;
    paramag.a((NativeTracker.TrackerEventType)localObject, paramMap);
  }
  
  private void a(bb parambb, ad paramad)
  {
    Object localObject1 = parambb.b().f();
    if (localObject1 != null)
    {
      boolean bool1 = g;
      if (bool1)
      {
        Object localObject2 = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_END_CARD_CLOSE;
        localObject2 = ((bp)localObject1).a((NativeTracker.TrackerEventType)localObject2).iterator();
        for (;;)
        {
          boolean bool2 = ((Iterator)localObject2).hasNext();
          if (!bool2) {
            break;
          }
          NativeTracker localNativeTracker = (NativeTracker)((Iterator)localObject2).next();
          Map localMap = a(parambb);
          bb.a(localNativeTracker, localMap);
        }
        g = false;
        parambb = "EndCardClosed";
        localObject1 = paramad.z();
        paramad.a(parambb, (Map)localObject1);
      }
    }
  }
  
  private void a(String paramString1, String paramString2, ag paramag)
  {
    Object localObject = m.get();
    if (localObject == null) {
      return;
    }
    localObject = (Context)m.get();
    paramString1 = com.inmobi.commons.core.utilities.b.a((Context)localObject, paramString1, paramString2);
    if (paramString1 != null)
    {
      localObject = c(this);
      if (localObject == null) {
        return;
      }
      localObject = l;
      if (localObject != null)
      {
        boolean bool1 = r;
        if (!bool1) {
          ((ad.c)localObject).g();
        }
      }
      boolean bool2 = paramString1.equals(paramString2);
      if (bool2)
      {
        paramString1 = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_FALLBACK_URL;
        paramString2 = a(paramag);
        paramag.a(paramString1, paramString2);
      }
    }
  }
  
  private void a(String paramString1, String paramString2, Map paramMap)
  {
    Object localObject = c(this);
    if (localObject != null)
    {
      localObject = l;
      if (localObject != null)
      {
        ((ad.c)localObject).a(paramString1, paramString2, paramMap);
        return;
      }
      return;
    }
  }
  
  static NativeTimerView b(View paramView)
  {
    if (paramView != null) {
      return (NativeTimerView)paramView.findViewWithTag("timerView");
    }
    return null;
  }
  
  private ag b(ak paramak, ag paramag)
  {
    ag localag = null;
    if (paramak == null) {
      return null;
    }
    String str1 = r;
    String str2 = s;
    if (str1 != null) {
      localag = a(paramag, paramak, str1);
    }
    if ((localag == null) && (str2 != null)) {
      localag = a(paramag, paramak, str2);
    }
    return localag;
  }
  
  private void b(ag paramag, Map paramMap)
  {
    if (paramag == null) {
      return;
    }
    Object localObject1 = new org/json/JSONObject;
    ((JSONObject)localObject1).<init>();
    String str = "id";
    try
    {
      localObject2 = g;
      ((JSONObject)localObject1).put(str, localObject2);
      str = "asset";
      localObject2 = f;
      ((JSONObject)localObject1).put(str, localObject2);
    }
    catch (JSONException localJSONException)
    {
      Object localObject2 = com.inmobi.commons.core.a.a.a();
      localObject3 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject3).<init>(localJSONException);
      ((com.inmobi.commons.core.a.a)localObject2).a((com.inmobi.commons.core.e.a)localObject3);
    }
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    localHashMap.put("type", "native");
    Object localObject3 = d;
    localHashMap.put("impId", localObject3);
    localHashMap.put("pageJson", localObject1);
    com.inmobi.commons.core.e.b.a();
    com.inmobi.commons.core.e.b.a("ads", "PageRendered", localHashMap);
    localObject1 = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_PAGE_VIEW;
    paramag.a((NativeTracker.TrackerEventType)localObject1, paramMap);
  }
  
  private static ad c(ad paramad)
  {
    for (;;)
    {
      if (paramad == null) {
        return null;
      }
      Object localObject = paramad.l();
      if (localObject != null) {
        break;
      }
      localObject = k;
      if (paramad == localObject) {
        break;
      }
      paramad = (ad)localObject;
    }
    return paramad;
  }
  
  protected static void c(View paramView)
  {
    paramView = b(paramView);
    if (paramView != null)
    {
      ValueAnimator localValueAnimator = c;
      if (localValueAnimator != null)
      {
        localValueAnimator = c;
        boolean bool = localValueAnimator.isRunning();
        if (bool)
        {
          localValueAnimator = c;
          long l1 = localValueAnimator.getCurrentPlayTime();
          b = l1;
          paramView = c;
          paramView.cancel();
        }
      }
    }
  }
  
  protected static void d(View paramView)
  {
    paramView = b(paramView);
    if (paramView != null)
    {
      ValueAnimator localValueAnimator = c;
      if (localValueAnimator != null)
      {
        localValueAnimator = c;
        boolean bool = localValueAnimator.isRunning();
        if (!bool)
        {
          localValueAnimator = c;
          long l1 = b;
          localValueAnimator.setCurrentPlayTime(l1);
          paramView = c;
          paramView.start();
        }
      }
    }
  }
  
  private void w()
  {
    ai localai = a.a(0);
    Set localSet = z;
    Integer localInteger = Integer.valueOf(0);
    boolean bool = localSet.contains(localInteger);
    if ((!bool) && (localai != null)) {
      a(0, localai);
    }
  }
  
  private void x()
  {
    Object localObject = A();
    if (localObject != null)
    {
      localObject = c;
      boolean bool = b;
      if (!bool)
      {
        bool = true;
        b = bool;
        List localList = a;
        ((l)localObject).a(localList);
      }
    }
  }
  
  private void y()
  {
    Object localObject = A();
    if (localObject != null)
    {
      localObject = c;
      boolean bool1 = b;
      if (bool1)
      {
        bool1 = false;
        l.a locala = null;
        b = false;
        localObject = a.iterator();
        for (;;)
        {
          bool1 = ((Iterator)localObject).hasNext();
          if (!bool1) {
            break;
          }
          locala = (l.a)((Iterator)localObject).next();
          ValueAnimator localValueAnimator = (ValueAnimator)a;
          long l1 = localValueAnimator.getCurrentPlayTime();
          b = l1;
          float f1 = localValueAnimator.getAnimatedFraction();
          double d1 = f1;
          double d2 = 1.0D;
          boolean bool2 = d1 < d2;
          if (!bool2)
          {
            boolean bool3 = true;
            f1 = Float.MIN_VALUE;
            c = bool3;
          }
          localValueAnimator.cancel();
        }
      }
    }
  }
  
  private Map z()
  {
    Object localObject = H.a;
    String str1 = "WEBVIEW";
    localObject = ((ak)localObject).c(str1);
    int i1 = ((List)localObject).size();
    if (i1 > 0)
    {
      i1 = 0;
      str1 = null;
      localObject = (bc)((List)localObject).get(0);
    }
    else
    {
      localObject = null;
    }
    if (localObject == null) {
      str1 = "Static";
    } else {
      str1 = "Rich";
    }
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    String str2 = "type";
    localHashMap.put(str2, str1);
    str1 = "dataType";
    if (localObject == null) {
      localObject = "URL";
    } else {
      localObject = z;
    }
    localHashMap.put(str1, localObject);
    return localHashMap;
  }
  
  final Map a(ag paramag)
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>(3);
    boolean bool1 = i;
    if (!bool1)
    {
      Object localObject = a;
      if (localObject != null)
      {
        String str = "$LTS";
        long l1 = d.z;
        localObject = String.valueOf(l1);
        localHashMap.put(str, localObject);
        paramag = ak.a(paramag);
        long l2 = System.currentTimeMillis();
        if (paramag != null)
        {
          l1 = 0L;
          long l3 = z;
          boolean bool2 = l1 < l3;
          if (bool2) {
            l2 = z;
          }
        }
        localObject = String.valueOf(l2);
        localHashMap.put("$STS", localObject);
        localObject = String.valueOf(System.currentTimeMillis());
        localHashMap.put("$TS", localObject);
        return localHashMap;
      }
    }
    return localHashMap;
  }
  
  public final void a() {}
  
  final void a(int paramInt, ag paramag)
  {
    Set localSet = z;
    Integer localInteger = Integer.valueOf(paramInt);
    boolean bool = localSet.contains(localInteger);
    if (!bool)
    {
      bool = i;
      if (!bool)
      {
        w();
        paramag = (ai)paramag;
        a(paramInt, paramag);
        return;
      }
    }
  }
  
  public final void a(int paramInt, Map paramMap)
  {
    boolean bool = i;
    if (bool) {
      return;
    }
    switch (paramInt)
    {
    default: 
      return;
    case 3: 
      return;
    case 2: 
      localai = a.d;
      localTrackerEventType = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_CLIENT_FILL;
      localai.a(localTrackerEventType, paramMap);
      return;
    }
    ai localai = a.d;
    NativeTracker.TrackerEventType localTrackerEventType = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_LOAD;
    localai.a(localTrackerEventType, paramMap);
  }
  
  final void a(Context paramContext)
  {
    B();
    WeakReference localWeakReference = new java/lang/ref/WeakReference;
    localWeakReference.<init>(paramContext);
    m = localWeakReference;
    boolean bool = paramContext instanceof Activity;
    if (bool)
    {
      paramContext = ((Activity)paramContext).getApplication();
      paramContext.registerActivityLifecycleCallbacks(this);
    }
  }
  
  void a(View paramView)
  {
    boolean bool1 = h;
    if (!bool1)
    {
      bool1 = i;
      if (!bool1)
      {
        bool1 = true;
        h = bool1;
        paramView = a.d;
        Object localObject1 = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_RENDER;
        Object localObject2 = a.d;
        localObject2 = a((ag)localObject2);
        paramView.a((NativeTracker.TrackerEventType)localObject1, (Map)localObject2);
        paramView = new java/util/HashMap;
        paramView.<init>();
        localObject1 = "type";
        localObject2 = AdContainer.RenderingProperties.PlacementType.PLACEMENT_TYPE_FULLSCREEN;
        AdContainer.RenderingProperties.PlacementType localPlacementType = getRenderingPropertiesa;
        if (localObject2 == localPlacementType) {
          localObject2 = "int";
        } else {
          localObject2 = "native";
        }
        paramView.put(localObject1, localObject2);
        localObject2 = e;
        paramView.put("clientRequestId", localObject2);
        localObject2 = d;
        paramView.put("impId", localObject2);
        com.inmobi.commons.core.e.b.a();
        com.inmobi.commons.core.e.b.a("ads", "AdRendered", paramView);
        com.inmobi.commons.core.e.b.a();
        localObject1 = "ads";
        localObject2 = "ViewableBeaconFired";
        com.inmobi.commons.core.e.b.a((String)localObject1, (String)localObject2, paramView);
        w();
        paramView = A.iterator();
        for (;;)
        {
          boolean bool2 = paramView.hasNext();
          if (!bool2) {
            break;
          }
          localObject1 = (ag)paramView.next();
          localObject2 = a((ag)localObject1);
          b((ag)localObject1, (Map)localObject2);
        }
        A.clear();
        paramView = c(this);
        if (paramView == null) {
          return;
        }
        paramView = l;
        if (paramView != null) {
          paramView.d();
        }
        return;
      }
    }
  }
  
  final void a(View paramView, ag paramag)
  {
    boolean bool1 = i;
    if (bool1) {
      return;
    }
    w();
    Object localObject1 = a;
    localObject1 = b((ak)localObject1, paramag);
    if (localObject1 != null)
    {
      localObject2 = a((ag)localObject1);
      a((ag)localObject1, (Map)localObject2);
      bool1 = localObject1.equals(paramag);
      if (!bool1) {
        a(paramag, (Map)localObject2);
      }
    }
    else
    {
      localObject1 = a(paramag);
      a(paramag, (Map)localObject1);
    }
    localObject1 = c(this);
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = r.trim();
    boolean bool2 = ((String)localObject2).isEmpty();
    if (!bool2)
    {
      localObject1 = l;
      if (localObject1 != null) {
        ((ad.c)localObject1).e();
      }
    }
    localObject1 = a(a, paramag);
    if (localObject1 != null)
    {
      if (paramView != null)
      {
        localObject2 = "VIDEO";
        String str = b;
        bool2 = ((String)localObject2).equals(str);
        if (bool2)
        {
          int i1 = 5;
          int i2 = l;
          if (i1 == i2)
          {
            i1 = 4;
            paramView.setVisibility(i1);
            x = i1;
          }
        }
      }
      b((ag)localObject1);
    }
  }
  
  public final void a(AdContainer paramAdContainer)
  {
    boolean bool = paramAdContainer instanceof ad;
    if (bool)
    {
      paramAdContainer = (ad)paramAdContainer;
      k = paramAdContainer;
    }
  }
  
  public final void a(ad.c paramc)
  {
    l = paramc;
  }
  
  final void a(ag paramag, boolean paramBoolean)
  {
    Object localObject1 = a;
    boolean bool1 = j;
    if (bool1)
    {
      bool1 = i;
      if (bool1) {
        return;
      }
      localObject1 = a;
      localObject1 = b((ak)localObject1, paramag);
      if (localObject1 != null)
      {
        Object localObject2 = a((ag)localObject1);
        int i1 = i;
        i = i1;
        paramag = "VIDEO";
        Object localObject3 = b;
        boolean bool2 = paramag.equals(localObject3);
        if (!bool2)
        {
          bool2 = h;
          if (!bool2) {}
        }
        else
        {
          paramag = g;
          int i4 = 4;
          if (paramag != null) {
            paramag.a(i4);
          }
          int i2 = i;
          if (i2 == 0) {
            return;
          }
          String str1 = r;
          boolean bool3 = q;
          if ((!bool3) || (i4 != i2))
          {
            i4 = 2;
            int i6 = m;
            if (i4 == i6)
            {
              localObject3 = localObject1;
              localObject3 = ((bb)localObject1).b().f();
              if (localObject3 != null)
              {
                String str2 = e;
                if (str2 != null)
                {
                  str2 = e.trim();
                  boolean bool4 = str2.isEmpty();
                  if (!bool4) {
                    str1 = e;
                  }
                }
              }
            }
            localObject3 = C();
            int i5 = com.inmobi.commons.core.utilities.b.a((Context)localObject3, str1);
            if (i5 == 0)
            {
              str1 = s;
              localObject3 = C();
              i5 = com.inmobi.commons.core.utilities.b.a((Context)localObject3, str1);
              if (i5 == 0) {
                return;
              }
            }
            localObject2 = d.a(str1, (Map)localObject2);
            i5 = r;
            int i3;
            if ((i5 != 0) && (!paramBoolean))
            {
              Object localObject4 = c(this);
              if (localObject4 == null) {
                return;
              }
              localObject4 = l;
              if (localObject4 != null)
              {
                i5 = 1;
                if (i5 == i2)
                {
                  i3 = com.inmobi.commons.core.utilities.b.a((String)localObject2);
                  if (i3 != 0)
                  {
                    ((ad.c)localObject4).c();
                    break label364;
                  }
                }
                ((ad.c)localObject4).g();
              }
              label364:
              F = ((ag)localObject1);
              G = ((String)localObject2);
              return;
            }
            a((ag)localObject1, i3, (String)localObject2);
          }
        }
      }
    }
  }
  
  public final void a(RenderView paramRenderView)
  {
    Object localObject = w;
    if (localObject == null)
    {
      localObject = new java/util/LinkedList;
      ((LinkedList)localObject).<init>();
      w = ((List)localObject);
    }
    localObject = w;
    boolean bool = ((List)localObject).contains(paramRenderView);
    if (!bool)
    {
      localObject = w;
      ((List)localObject).add(paramRenderView);
    }
  }
  
  final void a(String paramString, Map paramMap)
  {
    String str1 = "clientRequestId";
    try
    {
      String str2 = e;
      paramMap.put(str1, str2);
      str1 = "impId";
      str2 = d;
      paramMap.put(str1, str2);
      com.inmobi.commons.core.e.b.a();
      str1 = "ads";
      com.inmobi.commons.core.e.b.a(str1, paramString, paramMap);
      return;
    }
    catch (Exception localException)
    {
      localException.getMessage();
    }
  }
  
  final void a(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      x();
      return;
    }
    y();
  }
  
  public final void b()
  {
    try
    {
      boolean bool1 = i;
      if (bool1) {
        return;
      }
      Object localObject1 = c(this);
      if (localObject1 == null) {
        return;
      }
      ((ad)localObject1).o();
      InMobiAdActivity.a(localObject1);
      boolean bool2 = localObject1 instanceof ba;
      if (bool2)
      {
        localObject2 = localObject1;
        localObject2 = (ba)localObject1;
        localObject3 = ((ba)localObject2).getVideoContainerView();
        localObject3 = (NativeVideoWrapper)localObject3;
        if (localObject3 != null)
        {
          localObject3 = ((NativeVideoWrapper)localObject3).getVideoView();
          Object localObject4 = ((NativeVideoView)localObject3).getTag();
          localObject4 = (bb)localObject4;
          Map localMap = v;
          String str = "seekPosition";
          int i2 = ((NativeVideoView)localObject3).getCurrentPosition();
          Integer localInteger = Integer.valueOf(i2);
          localMap.put(str, localInteger);
          localMap = v;
          str = "lastMediaVolume";
          i3 = ((NativeVideoView)localObject3).getVolume();
          localObject3 = Integer.valueOf(i3);
          localMap.put(str, localObject3);
          localObject3 = y;
          if (localObject3 != null)
          {
            localObject3 = y;
            localObject3 = (bb)localObject3;
            ((bb)localObject3).a((bb)localObject4);
          }
          a((bb)localObject4, (ad)localObject2);
        }
      }
      localObject2 = n;
      int i3 = 0;
      localObject3 = null;
      if (localObject2 == null)
      {
        bool1 = false;
        localObject1 = null;
      }
      else
      {
        localObject1 = n;
        localObject1 = ((WeakReference)localObject1).get();
        localObject1 = (Activity)localObject1;
      }
      if (localObject1 != null)
      {
        bool2 = localObject1 instanceof InMobiAdActivity;
        if (bool2)
        {
          localObject2 = localObject1;
          localObject2 = (InMobiAdActivity)localObject1;
          int i4 = 1;
          a = i4;
          ((Activity)localObject1).finish();
          int i1 = C;
          i4 = -1;
          if (i1 != i4)
          {
            i1 = 0;
            localObject2 = null;
            i4 = C;
            ((Activity)localObject1).overridePendingTransition(0, i4);
          }
        }
      }
      localObject1 = k;
      E = null;
      localObject1 = k;
      localObject1 = K;
      localObject2 = L;
      ((ExecutorService)localObject1).submit((Runnable)localObject2);
      return;
    }
    catch (Exception localException)
    {
      localException.getMessage();
      Logger.a(Logger.InternalLogLevel.DEBUG, "InMobi", "SDK encountered unexpected error in exiting video");
      Object localObject2 = com.inmobi.commons.core.a.a.a();
      Object localObject3 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject3).<init>(localException);
      ((com.inmobi.commons.core.a.a)localObject2).a((com.inmobi.commons.core.e.a)localObject3);
    }
  }
  
  protected void b(ag paramag)
  {
    int i1 = l;
    Object localObject1;
    Object localObject2;
    switch (i1)
    {
    case 2: 
    default: 
      i1 = 1;
      o = i1;
      localObject1 = t;
      if ((localObject1 != null) && (localObject1 != null))
      {
        localObject2 = "window.imraid.broadcastEvent('skip');";
        ((RenderView)localObject1).c((String)localObject2);
      }
      break;
    case 5: 
      return;
    case 4: 
      try
      {
        paramag = AdContainer.RenderingProperties.PlacementType.PLACEMENT_TYPE_INLINE;
        localObject1 = b;
        localObject1 = a;
        if (paramag == localObject1) {
          m();
        }
        return;
      }
      catch (Exception paramag)
      {
        paramag.getMessage();
        Logger.a(Logger.InternalLogLevel.DEBUG, "InMobi", "SDK encountered unexpected error in launching fullscreen ad");
        localObject1 = com.inmobi.commons.core.a.a.a();
        localObject2 = new com/inmobi/commons/core/e/a;
        ((com.inmobi.commons.core.e.a)localObject2).<init>(paramag);
        ((com.inmobi.commons.core.a.a)localObject1).a((com.inmobi.commons.core.e.a)localObject2);
        return;
      }
    case 3: 
      try
      {
        localObject1 = t;
        if (localObject1 != null)
        {
          localObject1 = t;
          localObject2 = "window.imraid.broadcastEvent('replay');";
          ((RenderView)localObject1).c((String)localObject2);
        }
        localObject1 = f();
        if (localObject1 != null)
        {
          localObject1 = f();
          localObject2 = ((View)localObject1).getParent();
          localObject2 = (ViewGroup)localObject2;
          if (localObject2 != null) {
            ((ViewGroup)localObject2).removeView((View)localObject1);
          }
        }
        localObject1 = k;
        localObject2 = ((ad)localObject1).f();
        localObject2 = b((View)localObject2);
        Object localObject3;
        if (localObject2 != null)
        {
          localObject3 = c;
          if (localObject3 != null)
          {
            localObject3 = c;
            boolean bool1 = ((ValueAnimator)localObject3).isRunning();
            if (bool1)
            {
              localObject3 = c;
              long l1 = a;
              long l2 = 1000L;
              l1 *= l2;
              ((ValueAnimator)localObject3).setCurrentPlayTime(l1);
              int i2 = 1065353216;
              float f1 = 1.0F;
              ((NativeTimerView)localObject2).a(f1);
            }
          }
        }
        localObject2 = "VIDEO";
        paramag = b;
        boolean bool3 = ((String)localObject2).equals(paramag);
        if (!bool3) {
          return;
        }
        bool3 = localObject1 instanceof ba;
        if (bool3)
        {
          paramag = ((ad)localObject1).getVideoContainerView();
          paramag = (NativeVideoWrapper)paramag;
          if (paramag != null)
          {
            paramag = paramag.getVideoView();
            localObject2 = paramag.getTag();
            localObject2 = (bb)localObject2;
            if (localObject2 != null)
            {
              boolean bool2 = ((bb)localObject2).a();
              if (bool2) {
                paramag.e();
              } else {
                paramag.d();
              }
            }
            else
            {
              localObject3 = AdContainer.RenderingProperties.PlacementType.PLACEMENT_TYPE_FULLSCREEN;
              Object localObject4 = b;
              localObject4 = a;
              if (localObject3 == localObject4) {
                paramag.e();
              } else {
                paramag.d();
              }
            }
            a((bb)localObject2, (ad)localObject1);
            paramag.start();
          }
        }
        return;
      }
      catch (Exception paramag)
      {
        paramag.getMessage();
        Logger.a(Logger.InternalLogLevel.DEBUG, "InMobi", "SDK encountered unexpected error in replaying video");
        localObject1 = com.inmobi.commons.core.a.a.a();
        localObject2 = new com/inmobi/commons/core/e/a;
        ((com.inmobi.commons.core.e.a)localObject2).<init>(paramag);
        ((com.inmobi.commons.core.a.a)localObject1).a((com.inmobi.commons.core.e.a)localObject2);
        return;
      }
    case 1: 
      try
      {
        paramag = t;
        if (paramag != null)
        {
          paramag = t;
          localObject1 = "window.imraid.broadcastEvent('close');";
          paramag.c((String)localObject1);
        }
        b();
        return;
      }
      catch (Exception paramag)
      {
        paramag.getMessage();
        Logger.a(Logger.InternalLogLevel.DEBUG, "InMobi", "SDK encountered unexpected error in exiting video");
        localObject1 = com.inmobi.commons.core.a.a.a();
        localObject2 = new com/inmobi/commons/core/e/a;
        ((com.inmobi.commons.core.e.a)localObject2).<init>(paramag);
        ((com.inmobi.commons.core.a.a)localObject1).a((com.inmobi.commons.core.e.a)localObject2);
        return;
      }
    case 0: 
      return;
    }
    c(f());
    c(paramag);
  }
  
  public final void c(ag paramag)
  {
    Object localObject1 = z();
    a("EndCardRequested", (Map)localObject1);
    Object localObject2 = H;
    if (localObject2 != null)
    {
      localObject1 = f();
      if (localObject1 != null) {
        try
        {
          localObject1 = f();
          localObject1 = (ViewGroup)localObject1;
          Object localObject3 = ((ad)localObject2).getViewableAd();
          localObject3 = ((bw)localObject3).a(null, (ViewGroup)localObject1, false);
          if (localObject3 != null)
          {
            ((ViewGroup)localObject1).addView((View)localObject3);
            boolean bool1 = true;
            ((View)localObject3).setClickable(bool1);
            ((ad)localObject2).x();
            localObject2 = "EndCardDisplayed";
            localObject3 = z();
            a((String)localObject2, (Map)localObject3);
            boolean bool2 = paramag instanceof bb;
            if (!bool2) {
              return;
            }
            paramag = ((bb)paramag).b().f();
            if (paramag != null) {
              g = bool1;
            }
            return;
          }
          b();
          return;
        }
        catch (Exception paramag)
        {
          b();
          localObject2 = com.inmobi.commons.core.a.a.a();
          localObject1 = new com/inmobi/commons/core/e/a;
          ((com.inmobi.commons.core.e.a)localObject1).<init>(paramag);
          ((com.inmobi.commons.core.a.a)localObject2).a((com.inmobi.commons.core.e.a)localObject1);
          return;
        }
      }
    }
    paramag = Logger.InternalLogLevel.DEBUG;
    localObject2 = "InMobi";
    localObject1 = "Failed to show end card";
    Logger.a(paramag, (String)localObject2, (String)localObject1);
    b();
  }
  
  public final boolean c()
  {
    return i;
  }
  
  public final Context d()
  {
    return (Context)m.get();
  }
  
  public void destroy()
  {
    boolean bool1 = i;
    if (bool1) {
      return;
    }
    bool1 = true;
    i = bool1;
    int i1 = -1;
    C = i1;
    Object localObject1 = E;
    if (localObject1 != null) {
      ((ad)localObject1).b();
    }
    i = bool1;
    bool1 = false;
    l = null;
    localObject1 = A();
    if (localObject1 != null)
    {
      Object localObject2 = c;
      Iterator localIterator = a.iterator();
      for (;;)
      {
        boolean bool2 = localIterator.hasNext();
        if (!bool2) {
          break;
        }
        Animator localAnimator = nexta;
        localAnimator.cancel();
      }
      localObject2 = a;
      ((List)localObject2).clear();
      ((aq)localObject1).b();
    }
    A.clear();
    localObject1 = g;
    if (localObject1 != null)
    {
      ((bw)localObject1).d();
      localObject1 = g;
      ((bw)localObject1).e();
    }
    B();
    m.clear();
    localObject1 = n;
    if (localObject1 != null) {
      ((WeakReference)localObject1).clear();
    }
    localObject1 = w;
    if (localObject1 != null) {
      ((List)localObject1).clear();
    }
    a = null;
    t = null;
    localObject1 = H;
    if (localObject1 != null)
    {
      ((ad)localObject1).destroy();
      H = null;
    }
  }
  
  public final ad.c e()
  {
    return l;
  }
  
  public final View f()
  {
    bw localbw = g;
    if (localbw == null) {
      return null;
    }
    return localbw.b();
  }
  
  final void g()
  {
    Object localObject = a.d;
    localObject = a((ag)localObject);
    a(1, (Map)localObject);
    a(2, (Map)localObject);
  }
  
  public m getApkDownloader()
  {
    return D;
  }
  
  public AdContainer.a getFullScreenEventsListener()
  {
    return J;
  }
  
  public String getMarkupType()
  {
    return "inmobiJson";
  }
  
  public AdContainer.RenderingProperties getRenderingProperties()
  {
    return b;
  }
  
  public View getVideoContainerView()
  {
    return null;
  }
  
  public bw getViewableAd()
  {
    Object localObject1 = j();
    Object localObject2 = g;
    if ((localObject2 == null) && (localObject1 != null))
    {
      g();
      localObject2 = new com/inmobi/ads/v;
      Object localObject3 = new com/inmobi/ads/by;
      Object localObject4 = t;
      ((by)localObject3).<init>(this, (RenderView)localObject4);
      ((v)localObject2).<init>((Context)localObject1, this, (bw)localObject3);
      g = ((bw)localObject2);
      localObject2 = f;
      if (localObject2 != null)
      {
        boolean bool1 = localObject1 instanceof Activity;
        if (bool1) {
          try
          {
            localObject1 = (Activity)localObject1;
            localObject2 = ((Set)localObject2).iterator();
            for (;;)
            {
              bool1 = ((Iterator)localObject2).hasNext();
              if (!bool1) {
                break;
              }
              localObject3 = ((Iterator)localObject2).next();
              localObject3 = (bm)localObject3;
              int i1 = a;
              int i3 = 1;
              Object localObject5;
              label261:
              Object localObject7;
              if (i1 != i3)
              {
                int i4 = 3;
                if (i1 == i4)
                {
                  i1 = v;
                  if (i1 == 0)
                  {
                    localObject4 = b;
                    localObject5 = "avidAdSession";
                    localObject4 = ((Map)localObject4).get(localObject5);
                    Object localObject6 = localObject4;
                    localObject6 = (com.b.a.a.a.f.a)localObject4;
                    localObject4 = b;
                    localObject5 = "deferred";
                    boolean bool2 = ((Map)localObject4).containsKey(localObject5);
                    if (bool2)
                    {
                      localObject3 = b;
                      localObject4 = "deferred";
                      localObject3 = ((Map)localObject3).get(localObject4);
                      localObject3 = (Boolean)localObject3;
                      bool1 = ((Boolean)localObject3).booleanValue();
                      if (bool1)
                      {
                        bool3 = true;
                        break label261;
                      }
                    }
                    bool1 = false;
                    localObject3 = null;
                    boolean bool3 = false;
                    if (localObject6 != null)
                    {
                      localObject3 = new com/inmobi/ads/q;
                      bw localbw = g;
                      localObject4 = localObject3;
                      localObject7 = this;
                      localObject5 = localObject1;
                      ((q)localObject3).<init>(this, (Activity)localObject1, localbw, (com.b.a.a.a.f.a)localObject6, bool3);
                      g = ((bw)localObject3);
                    }
                  }
                }
              }
              else
              {
                int i2 = v;
                if (i2 == 0)
                {
                  localObject4 = new com/inmobi/ads/z;
                  localObject7 = g;
                  localObject3 = b;
                  ((z)localObject4).<init>(this, (Activity)localObject1, (bw)localObject7, (Map)localObject3);
                  g = ((bw)localObject4);
                }
                else
                {
                  localObject4 = b;
                  localObject7 = "zMoatIID";
                  localObject5 = UUID.randomUUID();
                  localObject5 = ((UUID)localObject5).toString();
                  ((Map)localObject4).put(localObject7, localObject5);
                  localObject4 = new com/inmobi/ads/aa;
                  localObject7 = g;
                  localObject3 = b;
                  ((aa)localObject4).<init>((Activity)localObject1, (bw)localObject7, (Map)localObject3);
                  g = ((bw)localObject4);
                }
              }
            }
            localHashMap = new java/util/HashMap;
          }
          catch (Exception localException)
          {
            localException.getMessage();
            localObject2 = com.inmobi.commons.core.a.a.a();
            localObject3 = new com/inmobi/commons/core/e/a;
            ((com.inmobi.commons.core.e.a)localObject3).<init>(localException);
            ((com.inmobi.commons.core.a.a)localObject2).a((com.inmobi.commons.core.e.a)localObject3);
          }
        }
        HashMap localHashMap;
        localHashMap.<init>();
        localHashMap.put("type", "native");
        localObject3 = d;
        localHashMap.put("impId", localObject3);
        com.inmobi.commons.core.e.b.a();
        localObject2 = "ads";
        localObject3 = "TrackersForService";
        com.inmobi.commons.core.e.b.a((String)localObject2, (String)localObject3, localHashMap);
      }
    }
    return g;
  }
  
  public final ak h()
  {
    return a;
  }
  
  boolean i()
  {
    Object localObject = AdContainer.RenderingProperties.PlacementType.PLACEMENT_TYPE_INLINE;
    AdContainer.RenderingProperties.PlacementType localPlacementType = b.a;
    if (localObject == localPlacementType)
    {
      localObject = l();
      if (localObject != null) {
        return true;
      }
    }
    return false;
  }
  
  protected final Context j()
  {
    AdContainer.RenderingProperties.PlacementType localPlacementType1 = AdContainer.RenderingProperties.PlacementType.PLACEMENT_TYPE_FULLSCREEN;
    AdContainer.RenderingProperties.PlacementType localPlacementType2 = b.a;
    if (localPlacementType1 != localPlacementType2)
    {
      boolean bool = i();
      if (!bool) {
        return (Context)m.get();
      }
    }
    return l();
  }
  
  protected final boolean k()
  {
    return h;
  }
  
  public final Activity l()
  {
    WeakReference localWeakReference = n;
    if (localWeakReference == null) {
      return null;
    }
    return (Activity)localWeakReference.get();
  }
  
  final void m()
  {
    Object localObject = c(this);
    if (localObject == null) {
      return;
    }
    localObject = l;
    if (localObject != null) {
      ((ad.c)localObject).c();
    }
    localObject = K;
    ad.5 local5 = new com/inmobi/ads/ad$5;
    local5.<init>(this);
    ((ExecutorService)localObject).submit(local5);
  }
  
  boolean n()
  {
    return false;
  }
  
  final void o()
  {
    boolean bool = n();
    if (bool)
    {
      bool = true;
      o = bool;
      ad.c localc = l;
      if (localc != null)
      {
        Map localMap = a.g;
        if (localMap != null)
        {
          localMap = a.g;
          localc.a(localMap);
        }
      }
    }
  }
  
  public void onActivityCreated(Activity paramActivity, Bundle paramBundle) {}
  
  public void onActivityDestroyed(Activity paramActivity)
  {
    bw localbw = g;
    if (localbw != null)
    {
      int i1 = 2;
      localbw.a(paramActivity, i1);
    }
  }
  
  public void onActivityPaused(Activity paramActivity) {}
  
  public void onActivityResumed(Activity paramActivity) {}
  
  public void onActivitySaveInstanceState(Activity paramActivity, Bundle paramBundle) {}
  
  public void onActivityStarted(Activity paramActivity)
  {
    Context localContext = C();
    if (localContext != null)
    {
      boolean bool = localContext.equals(paramActivity);
      if (bool) {
        p();
      }
    }
  }
  
  public void onActivityStopped(Activity paramActivity)
  {
    Context localContext = C();
    if (localContext != null)
    {
      boolean bool = localContext.equals(paramActivity);
      if (bool) {
        q();
      }
    }
  }
  
  final void p()
  {
    j = false;
    d(f());
    x();
    bw localbw = g;
    if (localbw != null)
    {
      Context localContext = C();
      localbw.a(localContext, 0);
    }
  }
  
  void q()
  {
    int i1 = 1;
    j = i1;
    c(f());
    y();
    bw localbw = g;
    if (localbw != null)
    {
      Context localContext = C();
      localbw.a(localContext, i1);
    }
  }
  
  final void r()
  {
    Object localObject1 = F;
    Object localObject2;
    if (localObject1 != null)
    {
      localObject2 = G;
      if (localObject2 != null)
      {
        int i1 = i;
        String str = G;
        a((ag)localObject1, i1, str);
        return;
      }
    }
    localObject1 = s;
    if (localObject1 != null)
    {
      localObject1 = m.get();
      if (localObject1 != null)
      {
        localObject1 = (Context)m.get();
        localObject2 = s;
        com.inmobi.commons.a.a.a((Context)localObject1, (Intent)localObject2);
      }
    }
  }
  
  final RenderView s()
  {
    RenderView localRenderView = t;
    if (localRenderView == null) {
      return u;
    }
    return localRenderView;
  }
  
  public void setFullScreenActivityContext(Activity paramActivity)
  {
    WeakReference localWeakReference = new java/lang/ref/WeakReference;
    localWeakReference.<init>(paramActivity);
    n = localWeakReference;
  }
  
  public void setRequestedScreenOrientation()
  {
    Activity localActivity = l();
    if (localActivity != null)
    {
      boolean bool = i;
      if (!bool)
      {
        ak localak = a;
        int i1 = a;
        switch (i1)
        {
        default: 
          i1 = localActivity.getRequestedOrientation();
          localActivity.setRequestedOrientation(i1);
          break;
        case 2: 
          localActivity.setRequestedOrientation(0);
          return;
        case 1: 
          localActivity.setRequestedOrientation(1);
          return;
        }
      }
    }
  }
  
  final void t()
  {
    ad.a locala = new com/inmobi/ads/ad$a;
    locala.<init>(this, this);
    locala.start();
  }
  
  final RenderView.a u()
  {
    Object localObject = I;
    if (localObject == null)
    {
      localObject = new com/inmobi/ads/ad$7;
      ((ad.7)localObject).<init>(this);
      I = ((RenderView.a)localObject);
    }
    return I;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.ad
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */