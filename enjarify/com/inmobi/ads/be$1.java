package com.inmobi.ads;

import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import com.d.b.ae;
import com.d.b.d;
import com.d.b.h;
import com.d.b.i;
import com.d.b.i.1;
import com.d.b.i.b;
import com.d.b.j;
import com.d.b.w;
import com.d.b.w.b;
import com.d.b.y;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;

final class be$1
  implements Application.ActivityLifecycleCallbacks
{
  public final void onActivityCreated(Activity paramActivity, Bundle paramBundle) {}
  
  public final void onActivityDestroyed(Activity paramActivity)
  {
    synchronized ()
    {
      Object localObject2 = be.b();
      if (localObject2 != null)
      {
        boolean bool1 = be.b(paramActivity);
        if (bool1)
        {
          localObject2 = paramActivity.getApplication();
          Object localObject3 = be.c();
          ((Application)localObject2).unregisterActivityLifecycleCallbacks((Application.ActivityLifecycleCallbacks)localObject3);
          localObject2 = be.d();
          ((List)localObject2).remove(paramActivity);
          paramActivity = be.d();
          boolean bool2 = paramActivity.isEmpty();
          if (bool2)
          {
            be.e();
            be.b();
            paramActivity = be.b();
            localObject2 = w.b;
            if (paramActivity != localObject2)
            {
              bool1 = o;
              if (!bool1)
              {
                localObject2 = g;
                ((d)localObject2).c();
                localObject2 = c;
                ((w.b)localObject2).interrupt();
                localObject2 = h;
                localObject2 = a;
                ((HandlerThread)localObject2).quit();
                localObject2 = f;
                localObject3 = c;
                boolean bool3 = localObject3 instanceof y;
                if (bool3)
                {
                  localObject3 = c;
                  ((ExecutorService)localObject3).shutdown();
                }
                localObject3 = d;
                ((j)localObject3).a();
                localObject3 = a;
                ((i.b)localObject3).quit();
                localObject3 = w.a;
                i.1 local1 = new com/d/b/i$1;
                local1.<init>((i)localObject2);
                ((Handler)localObject3).post(local1);
                localObject2 = j;
                localObject2 = ((Map)localObject2).values();
                localObject2 = ((Collection)localObject2).iterator();
                for (;;)
                {
                  bool3 = ((Iterator)localObject2).hasNext();
                  if (!bool3) {
                    break;
                  }
                  localObject3 = ((Iterator)localObject2).next();
                  localObject3 = (h)localObject3;
                  ((h)localObject3).a();
                }
                localObject2 = j;
                ((Map)localObject2).clear();
                bool1 = true;
                o = bool1;
              }
              be.f();
            }
            else
            {
              paramActivity = new java/lang/UnsupportedOperationException;
              localObject2 = "Default singleton instance cannot be shutdown.";
              paramActivity.<init>((String)localObject2);
              throw paramActivity;
            }
          }
        }
      }
      return;
    }
  }
  
  public final void onActivityPaused(Activity paramActivity) {}
  
  public final void onActivityResumed(Activity paramActivity) {}
  
  public final void onActivitySaveInstanceState(Activity paramActivity, Bundle paramBundle) {}
  
  public final void onActivityStarted(Activity paramActivity) {}
  
  public final void onActivityStopped(Activity paramActivity) {}
}

/* Location:
 * Qualified Name:     com.inmobi.ads.be.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */