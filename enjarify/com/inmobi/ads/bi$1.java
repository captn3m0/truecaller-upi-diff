package com.inmobi.ads;

import com.inmobi.ads.cache.b;
import com.inmobi.ads.cache.f;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

final class bi$1
  implements f
{
  bi$1(bi parambi) {}
  
  public final void a(b paramb)
  {
    bi.a();
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    Object localObject3;
    Object localObject4;
    Object localObject5;
    if (paramb != null)
    {
      paramb = a.iterator();
      boolean bool1 = paramb.hasNext();
      if (bool1)
      {
        Object localObject2 = (com.inmobi.ads.cache.a)paramb.next();
        localObject3 = new java/util/HashMap;
        ((HashMap)localObject3).<init>();
        localObject4 = d;
        ((Map)localObject3).put("url", localObject4);
        localObject4 = Long.valueOf(a);
        ((Map)localObject3).put("latency", localObject4);
        long l1 = com.inmobi.commons.core.utilities.c.a(e);
        localObject4 = Long.valueOf(l1);
        ((Map)localObject3).put("size", localObject4);
        localObject5 = bi.a(a);
        localObject4 = "ads";
        String str = "VideoAssetDownloadFailed";
        ((bi.a)localObject5).a((String)localObject4, str, (Map)localObject3);
        localObject3 = bi.c(a);
        localObject2 = d;
        localObject5 = bi.b(a);
        boolean bool2;
        if (localObject5 == null)
        {
          bool2 = false;
          localObject5 = null;
        }
        else
        {
          localObject5 = ba).f;
        }
        localObject2 = ((c)localObject3).b((String)localObject2, (String)localObject5).iterator();
        for (;;)
        {
          boolean bool3 = ((Iterator)localObject2).hasNext();
          if (!bool3) {
            break;
          }
          localObject3 = (a)((Iterator)localObject2).next();
          long l2 = c;
          localObject5 = Long.valueOf(l2);
          bool2 = ((List)localObject1).contains(localObject5);
          if (!bool2)
          {
            long l3 = c;
            localObject3 = Long.valueOf(l3);
            ((List)localObject1).add(localObject3);
          }
        }
      }
    }
    long l4 = ba).d;
    paramb = Long.valueOf(l4);
    boolean bool4 = ((List)localObject1).contains(paramb);
    if (!bool4)
    {
      l4 = ba).d;
      paramb = Long.valueOf(l4);
      ((List)localObject1).add(paramb);
    }
    paramb = ((List)localObject1).iterator();
    for (;;)
    {
      boolean bool5 = paramb.hasNext();
      if (!bool5) {
        break;
      }
      localObject1 = (Long)paramb.next();
      long l5 = ((Long)localObject1).longValue();
      localObject3 = bi.a(a);
      localObject5 = new com/inmobi/ads/InMobiAdRequestStatus;
      localObject4 = InMobiAdRequestStatus.StatusCode.AD_NO_LONGER_AVAILABLE;
      ((InMobiAdRequestStatus)localObject5).<init>((InMobiAdRequestStatus.StatusCode)localObject4);
      ((bi.a)localObject3).b(l5, (InMobiAdRequestStatus)localObject5);
    }
  }
  
  public final void b(b paramb)
  {
    bi.a();
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    Object localObject2;
    if (paramb != null)
    {
      Iterator localIterator = a.iterator();
      boolean bool1 = localIterator.hasNext();
      if (bool1)
      {
        localObject2 = (com.inmobi.ads.cache.a)localIterator.next();
        Object localObject3 = new java/util/HashMap;
        ((HashMap)localObject3).<init>();
        Object localObject4 = d;
        ((Map)localObject3).put("url", localObject4);
        localObject4 = Long.valueOf(a);
        ((Map)localObject3).put("latency", localObject4);
        long l1 = com.inmobi.commons.core.utilities.c.a(e);
        localObject4 = Long.valueOf(l1);
        ((Map)localObject3).put("size", localObject4);
        Object localObject5 = "clientRequestId";
        localObject4 = f;
        ((Map)localObject3).put(localObject5, localObject4);
        boolean bool2 = j;
        String str;
        if (bool2)
        {
          localObject5 = bi.a(a);
          localObject4 = "ads";
          str = "GotCachedVideoAsset";
          ((bi.a)localObject5).a((String)localObject4, str, (Map)localObject3);
        }
        else
        {
          localObject5 = bi.a(a);
          localObject4 = "ads";
          str = "VideoAssetDownloaded";
          ((bi.a)localObject5).a((String)localObject4, str, (Map)localObject3);
        }
        localObject3 = bi.c(a);
        localObject2 = d;
        localObject5 = bi.b(a);
        if (localObject5 == null)
        {
          bool2 = false;
          localObject5 = null;
        }
        else
        {
          localObject5 = ba).f;
        }
        localObject2 = ((c)localObject3).a((String)localObject2, (String)localObject5);
        bi.a();
        ((List)localObject2).size();
        localObject2 = ((List)localObject2).iterator();
        for (;;)
        {
          boolean bool3 = ((Iterator)localObject2).hasNext();
          if (!bool3) {
            break;
          }
          localObject3 = (a)((Iterator)localObject2).next();
          long l2 = c;
          localObject5 = Long.valueOf(l2);
          bool2 = ((List)localObject1).contains(localObject5);
          if (!bool2)
          {
            long l3 = c;
            localObject3 = Long.valueOf(l3);
            ((List)localObject1).add(localObject3);
          }
        }
      }
    }
    long l4 = ba).d;
    paramb = Long.valueOf(l4);
    boolean bool4 = ((List)localObject1).contains(paramb);
    if (!bool4)
    {
      l4 = ba).d;
      paramb = Long.valueOf(l4);
      ((List)localObject1).add(paramb);
    }
    paramb = ((List)localObject1).iterator();
    for (;;)
    {
      boolean bool5 = paramb.hasNext();
      if (!bool5) {
        break;
      }
      localObject1 = (Long)paramb.next();
      long l5 = ((Long)localObject1).longValue();
      bi.a();
      localObject2 = bi.a(a);
      ((bi.a)localObject2).a(l5);
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.bi.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */