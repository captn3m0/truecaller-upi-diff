package com.inmobi.ads;

import android.os.Handler;
import android.view.View;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.WeakHashMap;

class t
{
  private static final String b = "t";
  final Map a;
  private final ca c;
  private final Map d;
  private final Handler e;
  private final t.c f;
  private final long g;
  private ca.c h;
  private t.a i;
  
  t(b.k paramk, ca paramca, t.a parama)
  {
    this(localWeakHashMap1, localWeakHashMap2, paramca, localHandler, paramk, parama);
  }
  
  private t(Map paramMap1, Map paramMap2, ca paramca, Handler paramHandler, b.k paramk, t.a parama)
  {
    a = paramMap1;
    d = paramMap2;
    c = paramca;
    long l = d;
    g = l;
    paramMap1 = new com/inmobi/ads/t$1;
    paramMap1.<init>(this);
    h = paramMap1;
    paramMap1 = c;
    paramMap2 = h;
    c = paramMap2;
    e = paramHandler;
    paramMap1 = new com/inmobi/ads/t$c;
    paramMap1.<init>(this);
    f = paramMap1;
    i = parama;
  }
  
  private void a(View paramView)
  {
    a.remove(paramView);
    d.remove(paramView);
    c.a(paramView);
  }
  
  private void d()
  {
    Handler localHandler = e;
    t.c localc = null;
    boolean bool = localHandler.hasMessages(0);
    if (bool) {
      return;
    }
    localHandler = e;
    localc = f;
    long l = g;
    localHandler.postDelayed(localc, l);
  }
  
  final View a(Object paramObject)
  {
    Iterator localIterator = a.entrySet().iterator();
    Map.Entry localEntry;
    boolean bool2;
    do
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      localEntry = (Map.Entry)localIterator.next();
      Object localObject = getValuea;
      bool2 = localObject.equals(paramObject);
    } while (!bool2);
    paramObject = (View)localEntry.getKey();
    break label81;
    paramObject = null;
    label81:
    if (paramObject != null) {
      a((View)paramObject);
    }
    return (View)paramObject;
  }
  
  final void a()
  {
    c.f();
    e.removeCallbacksAndMessages(null);
    d.clear();
  }
  
  final void a(View paramView, Object paramObject, int paramInt1, int paramInt2)
  {
    Object localObject = (t.b)a.get(paramView);
    if (localObject != null)
    {
      localObject = a;
      boolean bool = localObject.equals(paramObject);
      if (bool) {
        return;
      }
    }
    a(paramView);
    localObject = new com/inmobi/ads/t$b;
    ((t.b)localObject).<init>(paramObject, paramInt1, paramInt2);
    a.put(paramView, localObject);
    ca localca = c;
    paramInt2 = b;
    localca.a(paramView, paramObject, paramInt2);
  }
  
  final void b()
  {
    Iterator localIterator = a.entrySet().iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      Object localObject1 = (Map.Entry)localIterator.next();
      ca localca = c;
      View localView = (View)((Map.Entry)localObject1).getKey();
      Object localObject2 = getValuea;
      localObject1 = (t.b)((Map.Entry)localObject1).getValue();
      int j = b;
      localca.a(localView, localObject2, j);
    }
    d();
    c.d();
  }
  
  final void c()
  {
    a.clear();
    d.clear();
    c.f();
    e.removeMessages(0);
    c.e();
    h = null;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */