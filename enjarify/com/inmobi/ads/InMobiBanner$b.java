package com.inmobi.ads;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;
import java.lang.ref.WeakReference;
import java.util.Map;

final class InMobiBanner$b
  extends Handler
{
  WeakReference a;
  private WeakReference b;
  
  public InMobiBanner$b(InMobiBanner paramInMobiBanner)
  {
    super((Looper)localObject);
    localObject = new java/lang/ref/WeakReference;
    ((WeakReference)localObject).<init>(paramInMobiBanner);
    b = ((WeakReference)localObject);
    paramInMobiBanner = new java/lang/ref/WeakReference;
    paramInMobiBanner.<init>(null);
    a = paramInMobiBanner;
  }
  
  public final void handleMessage(Message paramMessage)
  {
    Object localObject1 = (InMobiBanner)b.get();
    Object localObject2 = (InMobiBanner.BannerAdListener)a.get();
    if ((localObject1 != null) && (localObject2 != null))
    {
      int i = what;
      Object localObject3 = null;
      Object localObject4;
      switch (i)
      {
      default: 
        InMobiBanner.access$300();
        break;
      case 7: 
        try
        {
          localObject4 = obj;
          if (localObject4 != null)
          {
            paramMessage = obj;
            localObject3 = paramMessage;
            localObject3 = (Map)paramMessage;
          }
          ((InMobiBanner.BannerAdListener)localObject2).onAdRewardActionCompleted((InMobiBanner)localObject1, (Map)localObject3);
          return;
        }
        catch (Exception paramMessage)
        {
          localObject1 = Logger.InternalLogLevel.ERROR;
          localObject2 = InMobiBanner.access$300();
          Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "Publisher handler caused unexpected error");
          InMobiBanner.access$300();
          paramMessage.getMessage();
          return;
        }
      case 6: 
        try
        {
          ((InMobiBanner.BannerAdListener)localObject2).onUserLeftApplication((InMobiBanner)localObject1);
          return;
        }
        catch (Exception paramMessage)
        {
          localObject1 = Logger.InternalLogLevel.ERROR;
          localObject2 = InMobiBanner.access$300();
          Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "Publisher handler caused unexpected error");
          InMobiBanner.access$300();
          paramMessage.getMessage();
          return;
        }
      case 5: 
        try
        {
          localObject4 = obj;
          if (localObject4 != null)
          {
            paramMessage = obj;
            localObject3 = paramMessage;
            localObject3 = (Map)paramMessage;
          }
          ((InMobiBanner.BannerAdListener)localObject2).onAdInteraction((InMobiBanner)localObject1, (Map)localObject3);
          return;
        }
        catch (Exception paramMessage)
        {
          localObject1 = Logger.InternalLogLevel.ERROR;
          localObject2 = InMobiBanner.access$300();
          Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "Publisher handler caused unexpected error");
          InMobiBanner.access$300();
          paramMessage.getMessage();
          return;
        }
      case 4: 
        try
        {
          ((InMobiBanner.BannerAdListener)localObject2).onAdDismissed((InMobiBanner)localObject1);
          return;
        }
        catch (Exception paramMessage)
        {
          localObject1 = Logger.InternalLogLevel.ERROR;
          localObject2 = InMobiBanner.access$300();
          Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "Publisher handler caused unexpected error");
          InMobiBanner.access$300();
          paramMessage.getMessage();
          return;
        }
      case 3: 
        try
        {
          ((InMobiBanner.BannerAdListener)localObject2).onAdDisplayed((InMobiBanner)localObject1);
          return;
        }
        catch (Exception paramMessage)
        {
          localObject1 = Logger.InternalLogLevel.ERROR;
          localObject2 = InMobiBanner.access$300();
          Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "Publisher handler caused unexpected error");
          InMobiBanner.access$300();
          paramMessage.getMessage();
          return;
        }
      case 2: 
        try
        {
          paramMessage = obj;
          paramMessage = (InMobiAdRequestStatus)paramMessage;
          ((InMobiBanner.BannerAdListener)localObject2).onAdLoadFailed((InMobiBanner)localObject1, paramMessage);
          return;
        }
        catch (Exception paramMessage)
        {
          localObject1 = Logger.InternalLogLevel.ERROR;
          localObject2 = InMobiBanner.access$300();
          Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "Publisher handler caused unexpected error");
          InMobiBanner.access$300();
          paramMessage.getMessage();
          return;
        }
      }
      try
      {
        ((InMobiBanner.BannerAdListener)localObject2).onAdLoadSucceeded((InMobiBanner)localObject1);
        return;
      }
      catch (Exception paramMessage)
      {
        localObject1 = Logger.InternalLogLevel.ERROR;
        localObject2 = InMobiBanner.access$300();
        Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "Publisher handler caused unexpected error");
        InMobiBanner.access$300();
        paramMessage.getMessage();
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.InMobiBanner.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */