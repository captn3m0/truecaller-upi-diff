package com.inmobi.ads;

import android.os.Message;
import java.util.Map;

final class InMobiNative$2
  implements j.b
{
  InMobiNative$2(InMobiNative paramInMobiNative) {}
  
  public final void a()
  {
    InMobiNative.access$400(a, "AR", "");
    InMobiNative.access$500(a).sendEmptyMessage(1);
  }
  
  public final void a(InMobiAdRequestStatus paramInMobiAdRequestStatus)
  {
    Object localObject1 = InMobiNative.3.a;
    Object localObject2 = paramInMobiAdRequestStatus.getStatusCode();
    int i = ((InMobiAdRequestStatus.StatusCode)localObject2).ordinal();
    int j = localObject1[i];
    String str;
    switch (j)
    {
    default: 
      localObject1 = a;
      localObject2 = "AF";
      str = "";
      InMobiNative.access$400((InMobiNative)localObject1, (String)localObject2, str);
      break;
    case 6: 
      localObject1 = a;
      localObject2 = "ART";
      str = "ReloadNotPermitted";
      InMobiNative.access$400((InMobiNative)localObject1, (String)localObject2, str);
      break;
    case 5: 
      localObject1 = a;
      localObject2 = "ART";
      str = "MissingRequiredDependencies";
      InMobiNative.access$400((InMobiNative)localObject1, (String)localObject2, str);
      break;
    case 4: 
      localObject1 = a;
      localObject2 = "ART";
      str = "FrequentRequests";
      InMobiNative.access$400((InMobiNative)localObject1, (String)localObject2, str);
      break;
    case 2: 
    case 3: 
      localObject1 = a;
      localObject2 = "ART";
      str = "LoadInProgress";
      InMobiNative.access$400((InMobiNative)localObject1, (String)localObject2, str);
      break;
    case 1: 
      localObject1 = a;
      localObject2 = "ART";
      str = "NetworkNotAvailable";
      InMobiNative.access$400((InMobiNative)localObject1, (String)localObject2, str);
    }
    localObject1 = Message.obtain();
    what = 2;
    obj = paramInMobiAdRequestStatus;
    InMobiNative.access$500(a).sendMessage((Message)localObject1);
  }
  
  public final void a(j paramj) {}
  
  public final void a(Map paramMap)
  {
    InMobiNative.access$400(a, "AVCL", "");
    InMobiNative.access$500(a).sendEmptyMessage(7);
  }
  
  public final void a(boolean paramBoolean) {}
  
  public final void b()
  {
    InMobiNative.access$200();
  }
  
  public final void b(Map paramMap) {}
  
  public final void c()
  {
    InMobiNative.access$500(a).sendEmptyMessage(3);
  }
  
  public final void d()
  {
    InMobiNative.access$400(a, "AVE", "");
    InMobiNative.access$500(a).sendEmptyMessage(4);
  }
  
  public final void e()
  {
    InMobiNative.access$400(a, "AVCO", "");
    InMobiNative.access$500(a).sendEmptyMessage(5);
  }
  
  public final void f()
  {
    InMobiNative.access$500(a).sendEmptyMessage(8);
  }
  
  public final void g()
  {
    InMobiNative.access$500(a).sendEmptyMessage(6);
  }
  
  public final void h()
  {
    InMobiNative.access$500(a).sendEmptyMessage(9);
  }
  
  public final boolean i()
  {
    return true;
  }
  
  public final void j()
  {
    InMobiNative.access$500(a).sendEmptyMessage(11);
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.InMobiNative.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */