package com.inmobi.ads;

import java.util.Map;

public abstract interface InMobiInterstitial$InterstitialAdListener2
{
  public abstract void onAdDismissed(InMobiInterstitial paramInMobiInterstitial);
  
  public abstract void onAdDisplayFailed(InMobiInterstitial paramInMobiInterstitial);
  
  public abstract void onAdDisplayed(InMobiInterstitial paramInMobiInterstitial);
  
  public abstract void onAdInteraction(InMobiInterstitial paramInMobiInterstitial, Map paramMap);
  
  public abstract void onAdLoadFailed(InMobiInterstitial paramInMobiInterstitial, InMobiAdRequestStatus paramInMobiAdRequestStatus);
  
  public abstract void onAdLoadSucceeded(InMobiInterstitial paramInMobiInterstitial);
  
  public abstract void onAdReceived(InMobiInterstitial paramInMobiInterstitial);
  
  public abstract void onAdRewardActionCompleted(InMobiInterstitial paramInMobiInterstitial, Map paramMap);
  
  public abstract void onAdWillDisplay(InMobiInterstitial paramInMobiInterstitial);
  
  public abstract void onUserLeftApplication(InMobiInterstitial paramInMobiInterstitial);
}

/* Location:
 * Qualified Name:     com.inmobi.ads.InMobiInterstitial.InterstitialAdListener2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */