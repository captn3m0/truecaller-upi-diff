package com.inmobi.ads;

import android.os.SystemClock;
import com.inmobi.ads.cache.AssetStore;
import com.inmobi.ads.cache.b;
import com.inmobi.commons.core.network.NetworkError;
import com.inmobi.commons.core.network.NetworkError.ErrorCode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class bi
  implements d.a
{
  private static final String f = "bi";
  boolean a = false;
  final bi.a b;
  final c c;
  i d;
  boolean e;
  private long g = 0L;
  private final com.inmobi.ads.cache.f h;
  
  public bi(bi.a parama)
  {
    bi.1 local1 = new com/inmobi/ads/bi$1;
    local1.<init>(this);
    h = local1;
    b = parama;
    parama = c.a();
    c = parama;
  }
  
  private List c(f paramf)
  {
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    try
    {
      localObject2 = new org/json/JSONObject;
      localObject3 = a;
      localObject3 = ((com.inmobi.commons.core.network.d)localObject3).b();
      ((JSONObject)localObject2).<init>((String)localObject3);
      localObject3 = "requestId";
      localObject3 = ((JSONObject)localObject2).getString((String)localObject3);
      localObject3 = ((String)localObject3).trim();
      Object localObject4 = "ads";
      localObject2 = ((JSONObject)localObject2).getJSONArray((String)localObject4);
      if (localObject2 != null)
      {
        localObject4 = c;
        int i = d;
        int j = ((JSONArray)localObject2).length();
        i = Math.min(i, j);
        j = 0;
        while (j < i)
        {
          Object localObject5 = ((JSONArray)localObject2).getJSONObject(j);
          e locale = c;
          long l1 = a;
          Object localObject6 = c;
          localObject6 = e;
          Object localObject7 = c;
          localObject7 = c;
          Object localObject8 = new java/lang/StringBuilder;
          ((StringBuilder)localObject8).<init>();
          ((StringBuilder)localObject8).append((String)localObject3);
          Object localObject9 = "_";
          ((StringBuilder)localObject8).append((String)localObject9);
          ((StringBuilder)localObject8).append(j);
          localObject8 = ((StringBuilder)localObject8).toString();
          localObject9 = c;
          localObject9 = i;
          Object localObject10 = d;
          localObject10 = m;
          localObject5 = a.a.a((JSONObject)localObject5, l1, (String)localObject6, (String)localObject7, (String)localObject8, (String)localObject9, (InMobiAdRequest.MonetizationContext)localObject10);
          if (localObject5 != null) {
            ((List)localObject1).add(localObject5);
          }
          j += 1;
        }
        if (i > 0)
        {
          boolean bool = ((List)localObject1).isEmpty();
          if (bool) {
            return null;
          }
        }
      }
    }
    catch (JSONException paramf)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      ((Map)localObject1).put("errorCode", "ParsingError");
      paramf = paramf.getLocalizedMessage();
      ((Map)localObject1).put("reason", paramf);
      long l2 = SystemClock.elapsedRealtime();
      long l3 = g;
      l2 -= l3;
      Object localObject2 = Long.valueOf(l2);
      ((Map)localObject1).put("latency", localObject2);
      ((Map)localObject1).put("isPreloaded", "1");
      localObject2 = com.inmobi.commons.a.a.e();
      ((Map)localObject1).put("im-accid", localObject2);
      paramf = b;
      localObject2 = "ads";
      Object localObject3 = "ServerError";
      paramf.a((String)localObject2, (String)localObject3, (Map)localObject1);
      localObject1 = null;
    }
    return (List)localObject1;
  }
  
  final String a(i parami)
  {
    if (parami != null)
    {
      localObject1 = k;
      if (localObject1 == null)
      {
        localObject1 = new java/util/HashMap;
        ((HashMap)localObject1).<init>();
      }
      localObject2 = "preload-request";
      boolean bool1 = ((Map)localObject1).containsKey(localObject2);
      if (!bool1)
      {
        localObject2 = "preload-request";
        str1 = "1";
        ((Map)localObject1).put(localObject2, str1);
        k = ((Map)localObject1);
      }
    }
    Object localObject1 = new com/inmobi/ads/e;
    String str2 = a;
    long l1 = d;
    com.inmobi.commons.core.utilities.uid.d locald = l;
    com.inmobi.ads.cache.d.a();
    String str3 = com.inmobi.ads.cache.d.c();
    ((e)localObject1).<init>(str2, l1, locald, str3);
    Object localObject2 = e;
    f = ((String)localObject2);
    localObject2 = g;
    g = ((Map)localObject2);
    localObject2 = h;
    e = ((String)localObject2);
    localObject2 = i;
    b = ((String)localObject2);
    int i = j.b;
    d = i;
    localObject2 = k;
    h = ((Map)localObject2);
    localObject2 = i;
    b = ((String)localObject2);
    localObject2 = f;
    c = ((String)localObject2);
    i = c * 1000;
    p = i;
    i = c * 1000;
    q = i;
    localObject2 = m;
    j = ((InMobiAdRequest.MonetizationContext)localObject2);
    boolean bool2 = n;
    x = bool2;
    long l2 = SystemClock.elapsedRealtime();
    g = l2;
    parami = new com/inmobi/ads/d;
    parami.<init>((e)localObject1, this);
    parami.a();
    parami = new java/util/HashMap;
    parami.<init>();
    parami.put("isPreloaded", "1");
    String str1 = i;
    parami.put("clientRequestId", str1);
    str1 = com.inmobi.commons.a.a.e();
    parami.put("im-accid", str1);
    b.a("ads", "ServerCallInitiated", parami);
    return i;
  }
  
  public final void a(f paramf)
  {
    List localList = c(paramf);
    long l1;
    InMobiAdRequestStatus.StatusCode localStatusCode;
    if (localList == null)
    {
      paramf = a;
      paramf.b();
      bool = a;
      if (!bool)
      {
        paramf = b;
        localObject1 = d;
        l1 = d;
        localObject2 = new com/inmobi/ads/InMobiAdRequestStatus;
        localStatusCode = InMobiAdRequestStatus.StatusCode.INTERNAL_ERROR;
        ((InMobiAdRequestStatus)localObject2).<init>(localStatusCode);
        paramf.b(l1, (InMobiAdRequestStatus)localObject2);
      }
      return;
    }
    int i = localList.size();
    if (i == 0)
    {
      a.b();
      paramf = new java/util/HashMap;
      paramf.<init>();
      l2 = SystemClock.elapsedRealtime();
      l3 = g;
      l2 -= l3;
      localObject3 = Long.valueOf(l2);
      paramf.put("latency", localObject3);
      paramf.put("isPreloaded", "1");
      localObject3 = com.inmobi.commons.a.a.e();
      paramf.put("im-accid", localObject3);
      localObject1 = b;
      localObject3 = "ads";
      localObject2 = "ServerNoFill";
      ((bi.a)localObject1).a((String)localObject3, (String)localObject2, paramf);
      bool = a;
      if (!bool)
      {
        paramf = b;
        localObject1 = d;
        l1 = d;
        localObject2 = new com/inmobi/ads/InMobiAdRequestStatus;
        localStatusCode = InMobiAdRequestStatus.StatusCode.NO_FILL;
        ((InMobiAdRequestStatus)localObject2).<init>(localStatusCode);
        paramf.b(l1, (InMobiAdRequestStatus)localObject2);
      }
      return;
    }
    paramf = new java/util/HashMap;
    paramf.<init>();
    int j = localList.size();
    Object localObject3 = Integer.valueOf(j);
    paramf.put("numberOfAdsReturned", localObject3);
    long l2 = SystemClock.elapsedRealtime();
    long l3 = g;
    l2 -= l3;
    localObject3 = Long.valueOf(l2);
    paramf.put("latency", localObject3);
    paramf.put("isPreloaded", "1");
    localObject3 = com.inmobi.commons.a.a.e();
    paramf.put("im-accid", localObject3);
    Object localObject1 = b;
    localObject3 = "ads";
    Object localObject2 = "ServerFill";
    ((bi.a)localObject1).a((String)localObject3, (String)localObject2, paramf);
    paramf = ((a)localList.get(0)).d();
    localObject1 = "HTML";
    boolean bool = ((String)localObject1).equalsIgnoreCase(paramf);
    if (bool)
    {
      paramf = "native";
      localObject1 = d.h;
      bool = paramf.equals(localObject1);
      if (bool)
      {
        bool = a;
        if (!bool)
        {
          paramf = b;
          localObject1 = d;
          l1 = d;
          localObject2 = new com/inmobi/ads/InMobiAdRequestStatus;
          localStatusCode = InMobiAdRequestStatus.StatusCode.INTERNAL_ERROR;
          ((InMobiAdRequestStatus)localObject2).<init>(localStatusCode);
          paramf.b(l1, (InMobiAdRequestStatus)localObject2);
        }
        return;
      }
    }
    localObject1 = c;
    long l4 = d.d;
    int k = d.j.a;
    String str1 = d.h;
    InMobiAdRequest.MonetizationContext localMonetizationContext = d.m;
    paramf = d.g;
    String str2 = g.a(paramf);
    localObject3 = localList;
    ((c)localObject1).a(localList, l4, k, str1, localMonetizationContext, str2);
    a(localList);
    bool = a;
    if (!bool)
    {
      bool = e;
      if (!bool)
      {
        paramf = b;
        localObject1 = d;
        l1 = d;
        paramf.a(l1);
      }
    }
  }
  
  final void a(List paramList)
  {
    if (paramList != null)
    {
      int i = paramList.size();
      if (i > 0)
      {
        i = 0;
        Object localObject1 = (a)paramList.get(0);
        Object localObject2;
        Object localObject3;
        int k;
        String str;
        if (localObject1 != null)
        {
          localObject2 = ((a)localObject1).d();
          localObject3 = "inmobiJson";
          boolean bool2 = ((String)localObject2).equalsIgnoreCase((String)localObject3);
          if (bool2)
          {
            localObject2 = ((a)localObject1).c();
            k = ((Set)localObject2).size();
            if (k == 0)
            {
              paramList = b;
              long l = d.d;
              paramList.a(l);
              return;
            }
            localObject3 = new com/inmobi/ads/cache/b;
            str = UUID.randomUUID().toString();
            localObject1 = f;
            boolean bool4 = e;
            com.inmobi.ads.cache.f localf;
            if (bool4)
            {
              localf = h;
            }
            else
            {
              bool4 = false;
              localf = null;
            }
            ((b)localObject3).<init>(str, (String)localObject1, (Set)localObject2, localf);
            localObject1 = AssetStore.a();
            ((AssetStore)localObject1).a((b)localObject3);
          }
        }
        i = 1;
        int j = paramList.size();
        paramList = paramList.subList(i, j).iterator();
        for (;;)
        {
          boolean bool1 = paramList.hasNext();
          if (!bool1) {
            break;
          }
          localObject1 = (a)paramList.next();
          if (localObject1 != null)
          {
            localObject2 = ((a)localObject1).d();
            localObject3 = "inmobiJson";
            boolean bool3 = ((String)localObject2).equalsIgnoreCase((String)localObject3);
            if (bool3)
            {
              localObject2 = ((a)localObject1).c();
              k = ((Set)localObject2).size();
              if (k != 0)
              {
                localObject3 = new com/inmobi/ads/cache/b;
                str = UUID.randomUUID().toString();
                localObject1 = f;
                ((b)localObject3).<init>(str, (String)localObject1, (Set)localObject2, null);
                localObject1 = AssetStore.a();
                ((AssetStore)localObject1).a((b)localObject3);
              }
            }
          }
        }
      }
    }
  }
  
  final boolean a(int paramInt)
  {
    long l1 = SystemClock.elapsedRealtime();
    long l2 = g;
    l1 -= l2;
    l2 = paramInt * 1000;
    paramInt = l1 < l2;
    return paramInt < 0;
  }
  
  public final void b(f paramf)
  {
    boolean bool = a;
    if (!bool)
    {
      Object localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      int i = a.b.a.getValue();
      Object localObject2 = String.valueOf(i);
      ((Map)localObject1).put("errorCode", localObject2);
      localObject2 = a.b.b;
      ((Map)localObject1).put("reason", localObject2);
      long l1 = SystemClock.elapsedRealtime();
      long l2 = g;
      l1 -= l2;
      localObject2 = Long.valueOf(l1);
      ((Map)localObject1).put("latency", localObject2);
      ((Map)localObject1).put("isPreloaded", "1");
      localObject2 = com.inmobi.commons.a.a.e();
      ((Map)localObject1).put("im-accid", localObject2);
      Object localObject3 = b;
      localObject2 = "ads";
      String str = "ServerError";
      ((bi.a)localObject3).a((String)localObject2, str, (Map)localObject1);
      localObject1 = b;
      localObject3 = d;
      long l3 = d;
      paramf = b;
      ((bi.a)localObject1).b(l3, paramf);
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.bi
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */