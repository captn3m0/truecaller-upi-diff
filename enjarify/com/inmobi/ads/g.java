package com.inmobi.ads;

import android.app.Application;
import android.content.ComponentCallbacks;
import android.os.Handler;
import android.os.Looper;
import com.inmobi.commons.core.configs.b.c;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONObject;

public abstract class g
  implements b.c
{
  static ConcurrentHashMap a;
  static b b;
  private static final String d = "g";
  String c;
  
  static
  {
    ConcurrentHashMap localConcurrentHashMap = new java/util/concurrent/ConcurrentHashMap;
    localConcurrentHashMap.<init>(8, 0.9F, 3);
    a = localConcurrentHashMap;
  }
  
  g(String paramString)
  {
    c = paramString;
    paramString = new com/inmobi/ads/b;
    paramString.<init>();
    b = paramString;
    paramString = com.inmobi.commons.core.configs.b.a();
    b localb = b;
    paramString.a(localb, this);
    paramString = com.inmobi.commons.core.e.b.a();
    JSONObject localJSONObject = bp;
    paramString.a("ads", localJSONObject);
  }
  
  public static g a(String paramString)
  {
    int i = paramString.hashCode();
    int j = -1052618729;
    String str;
    boolean bool;
    if (i != j)
    {
      j = 104431;
      if (i == j)
      {
        str = "int";
        bool = paramString.equals(str);
        if (bool)
        {
          bool = true;
          break label71;
        }
      }
    }
    else
    {
      str = "native";
      bool = paramString.equals(str);
      if (bool)
      {
        bool = false;
        paramString = null;
        break label71;
      }
    }
    int k = -1;
    switch (k)
    {
    default: 
      return null;
    case 1: 
      label71:
      return y.d();
    }
    return as.d();
  }
  
  static String a(Map paramMap)
  {
    if (paramMap == null) {
      return "";
    }
    String str = (String)paramMap.get("tp");
    if (str == null) {
      return "";
    }
    return (String)paramMap.get("tp");
  }
  
  private void d()
  {
    Iterator localIterator = a.entrySet().iterator();
    try
    {
      for (;;)
      {
        boolean bool1 = localIterator.hasNext();
        if (!bool1) {
          break;
        }
        localObject1 = localIterator.next();
        localObject1 = (Map.Entry)localObject1;
        localObject2 = ((Map.Entry)localObject1).getValue();
        localObject2 = (j)localObject2;
        boolean bool2 = ((j)localObject2).h();
        if (bool2)
        {
          ((Map.Entry)localObject1).getKey();
          ((Map.Entry)localObject1).getKey();
          localObject1 = new android/os/Handler;
          Object localObject3 = Looper.getMainLooper();
          ((Handler)localObject1).<init>((Looper)localObject3);
          localObject3 = new com/inmobi/ads/g$2;
          ((g.2)localObject3).<init>(this, (j)localObject2);
          ((Handler)localObject1).post((Runnable)localObject3);
          localIterator.remove();
        }
      }
      return;
    }
    catch (Exception localException)
    {
      localException.getMessage();
      Object localObject1 = com.inmobi.commons.core.a.a.a();
      Object localObject2 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject2).<init>(localException);
      ((com.inmobi.commons.core.a.a)localObject1).a((com.inmobi.commons.core.e.a)localObject2);
    }
  }
  
  private void e()
  {
    Object localObject1 = b;
    Object localObject2 = c;
    localObject1 = ((b)localObject1).c((String)localObject2);
    boolean bool = a;
    if (!bool) {
      return;
    }
    bg.a();
    localObject1 = b;
    localObject2 = c;
    localObject1 = ((b)localObject1).c((String)localObject2);
    long l = b;
    String str1 = c;
    int i = bg.a(l, str1);
    if (i > 0) {
      try
      {
        localObject2 = new java/util/HashMap;
        ((HashMap)localObject2).<init>();
        str1 = "type";
        String str2 = c;
        ((Map)localObject2).put(str1, str2);
        str1 = "count";
        localObject1 = Integer.valueOf(i);
        ((Map)localObject2).put(str1, localObject1);
        com.inmobi.commons.core.e.b.a();
        localObject1 = "ads";
        str1 = "PreLoadPidExpiry";
        com.inmobi.commons.core.e.b.a((String)localObject1, str1, (Map)localObject2);
        return;
      }
      catch (Exception localException)
      {
        localException.getMessage();
      }
    }
  }
  
  abstract j a(bf parambf);
  
  public final void a()
  {
    Object localObject1 = (Application)com.inmobi.commons.a.a.b();
    if (localObject1 != null)
    {
      localObject2 = new com/inmobi/ads/g$1;
      ((g.1)localObject2).<init>(this);
      ((Application)localObject1).registerComponentCallbacks((ComponentCallbacks)localObject2);
    }
    e();
    d();
    localObject1 = b;
    Object localObject2 = c;
    localObject1 = ((b)localObject1).c((String)localObject2);
    boolean bool = a;
    if (bool)
    {
      bg.a();
      localObject1 = (ArrayList)bg.a(c);
      int i = 0;
      localObject2 = null;
      for (;;)
      {
        int j = ((ArrayList)localObject1).size();
        if (i >= j) {
          break;
        }
        bf localbf = (bf)((ArrayList)localObject1).get(i);
        b(localbf);
        i += 1;
      }
    }
  }
  
  public final void a(com.inmobi.commons.core.configs.a parama)
  {
    b = (b)parama;
    parama = com.inmobi.commons.core.e.b.a();
    JSONObject localJSONObject = bp;
    parama.a("ads", localJSONObject);
  }
  
  public final void b()
  {
    e();
    d();
  }
  
  abstract void b(bf parambf);
  
  void c(bf parambf)
  {
    g.4 local4 = new com/inmobi/ads/g$4;
    local4.<init>(this, parambf);
    local4.start();
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */