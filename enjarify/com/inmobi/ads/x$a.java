package com.inmobi.ads;

import android.content.Context;
import java.util.HashMap;
import java.util.Map;

final class x$a
{
  static final Map a;
  
  static
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    a = localHashMap;
  }
  
  static x a(Context paramContext, bf parambf, j.b paramb)
  {
    long l = a;
    Object localObject = (x)a.get(parambf);
    if (localObject != null)
    {
      boolean bool = ((x)localObject).h();
      if (bool)
      {
        x.K();
        ((x)localObject).r();
      }
      ((x)localObject).a(paramContext);
      if (paramb != null) {
        ((x)localObject).a(paramb);
      }
      return (x)localObject;
    }
    x localx = new com/inmobi/ads/x;
    localObject = localx;
    localx.<init>(paramContext, l, paramb, (byte)0);
    a.put(parambf, localx);
    return localx;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.x.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */