package com.inmobi.ads;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnInfoListener;
import android.os.Build.VERSION;

final class NativeVideoView$4
  implements MediaPlayer.OnInfoListener
{
  NativeVideoView$4(NativeVideoView paramNativeVideoView) {}
  
  public final boolean onInfo(MediaPlayer paramMediaPlayer, int paramInt1, int paramInt2)
  {
    int i = Build.VERSION.SDK_INT;
    paramInt2 = 17;
    if (i >= paramInt2)
    {
      i = 3;
      if (i == paramInt1)
      {
        paramMediaPlayer = a;
        paramInt1 = 8;
        paramMediaPlayer.a(paramInt1, paramInt1);
      }
    }
    return true;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.NativeVideoView.4
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */