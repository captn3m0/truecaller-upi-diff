package com.inmobi.ads;

import android.graphics.Color;
import com.inmobi.commons.core.configs.a;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class b
  extends a
{
  public static boolean a = true;
  public static boolean b = false;
  public static boolean c = true;
  public static boolean d = true;
  private static final String t = "b";
  private static final Object u;
  String e = "https://i.w.inmobi.com/showad.asm";
  public String f = "https://sdktm.w.inmobi.com/sdkpubreq/v2";
  int g = 20;
  int h;
  int i;
  b.a j;
  Map k;
  public b.e l;
  public b.h m;
  public b.f n;
  public b.k o;
  JSONObject p;
  public b.j q;
  public b.b r;
  private List v;
  private b.d w;
  private Map x;
  private Map y;
  private b.g z;
  
  static
  {
    Object localObject = new java/lang/Object;
    localObject.<init>();
    u = localObject;
  }
  
  public b()
  {
    int i1 = 60;
    h = i1;
    i = i1;
    Object localObject1 = new com/inmobi/ads/b$e;
    ((b.e)localObject1).<init>();
    l = ((b.e)localObject1);
    localObject1 = new com/inmobi/ads/b$h;
    ((b.h)localObject1).<init>();
    m = ((b.h)localObject1);
    localObject1 = new com/inmobi/ads/b$f;
    ((b.f)localObject1).<init>();
    n = ((b.f)localObject1);
    localObject1 = new com/inmobi/ads/b$k;
    ((b.k)localObject1).<init>();
    o = ((b.k)localObject1);
    localObject1 = new com/inmobi/ads/b$j;
    ((b.j)localObject1).<init>();
    q = ((b.j)localObject1);
    localObject1 = new com/inmobi/ads/b$b;
    ((b.b)localObject1).<init>();
    r = ((b.b)localObject1);
    localObject1 = new com/inmobi/ads/b$g;
    ((b.g)localObject1).<init>();
    z = ((b.g)localObject1);
    localObject1 = new java/util/HashMap;
    ((HashMap)localObject1).<init>();
    y = ((Map)localObject1);
    localObject1 = new java/util/LinkedList;
    ((LinkedList)localObject1).<init>();
    v = ((List)localObject1);
    v.add("bannerDict");
    v.add("intDict");
    localObject1 = v;
    Object localObject2 = "nativeDict";
    ((List)localObject1).add(localObject2);
    try
    {
      localObject1 = new org/json/JSONObject;
      ((JSONObject)localObject1).<init>();
      localObject2 = new org/json/JSONObject;
      ((JSONObject)localObject2).<init>();
      Object localObject3 = "maxCacheSize";
      int i2 = 1;
      ((JSONObject)localObject2).put((String)localObject3, i2);
      localObject3 = "fetchLimit";
      ((JSONObject)localObject2).put((String)localObject3, i2);
      localObject3 = "minThreshold";
      boolean bool1 = false;
      String str1 = null;
      ((JSONObject)localObject2).put((String)localObject3, 0);
      localObject3 = "timeToLive";
      int i3 = 3300;
      ((JSONObject)localObject2).put((String)localObject3, i3);
      localObject3 = "base";
      ((JSONObject)localObject1).put((String)localObject3, localObject2);
      localObject2 = new org/json/JSONObject;
      ((JSONObject)localObject2).<init>();
      localObject3 = "maxCacheSize";
      ((JSONObject)localObject2).put((String)localObject3, i2);
      localObject3 = "fetchLimit";
      ((JSONObject)localObject2).put((String)localObject3, i2);
      localObject3 = "minThreshold";
      ((JSONObject)localObject2).put((String)localObject3, i2);
      localObject3 = "timeToLive";
      ((JSONObject)localObject2).put((String)localObject3, i3);
      localObject3 = "int";
      ((JSONObject)localObject1).put((String)localObject3, localObject2);
      localObject2 = new org/json/JSONObject;
      ((JSONObject)localObject2).<init>();
      localObject3 = "maxCacheSize";
      int i4 = 100;
      ((JSONObject)localObject2).put((String)localObject3, i4);
      localObject3 = "fetchLimit";
      ((JSONObject)localObject2).put((String)localObject3, i2);
      localObject3 = "minThreshold";
      ((JSONObject)localObject2).put((String)localObject3, i2);
      localObject3 = "timeToLive";
      ((JSONObject)localObject2).put((String)localObject3, i3);
      localObject3 = "native";
      ((JSONObject)localObject1).put((String)localObject3, localObject2);
      c((JSONObject)localObject1);
      localObject1 = new org/json/JSONObject;
      ((JSONObject)localObject1).<init>();
      localObject2 = "enabled";
      ((JSONObject)localObject1).put((String)localObject2, i2);
      localObject2 = "samplingFactor";
      ((JSONObject)localObject1).put((String)localObject2, 0);
      p = ((JSONObject)localObject1);
      localObject1 = new org/json/JSONObject;
      ((JSONObject)localObject1).<init>();
      localObject2 = new org/json/JSONObject;
      ((JSONObject)localObject2).<init>();
      localObject3 = "enabled";
      bool1 = a;
      ((JSONObject)localObject2).put((String)localObject3, bool1);
      localObject3 = "maxRetryCount";
      ((JSONObject)localObject2).put((String)localObject3, i2);
      localObject3 = "eventTTL";
      long l1 = 14400L;
      ((JSONObject)localObject2).put((String)localObject3, l1);
      localObject3 = "maxEventsToPersist";
      i4 = 1000;
      ((JSONObject)localObject2).put((String)localObject3, i4);
      localObject3 = "txLatency";
      long l2 = 60;
      ((JSONObject)localObject2).put((String)localObject3, l2);
      localObject3 = "processingInterval";
      long l3 = 0L;
      ((JSONObject)localObject2).put((String)localObject3, l3);
      localObject3 = e();
      Object localObject4 = "networkType";
      ((JSONObject)localObject2).put((String)localObject4, localObject3);
      localObject3 = new org/json/JSONObject;
      ((JSONObject)localObject3).<init>();
      localObject4 = "enabled";
      boolean bool2 = b;
      ((JSONObject)localObject3).put((String)localObject4, bool2);
      localObject4 = "maxRetryCount";
      ((JSONObject)localObject3).put((String)localObject4, i2);
      localObject4 = "eventTTL";
      ((JSONObject)localObject3).put((String)localObject4, l1);
      localObject4 = "maxEventsToPersist";
      ((JSONObject)localObject3).put((String)localObject4, i4);
      localObject4 = "txLatency";
      ((JSONObject)localObject3).put((String)localObject4, l2);
      localObject4 = "processingInterval";
      ((JSONObject)localObject3).put((String)localObject4, l3);
      localObject4 = e();
      Object localObject5 = "networkType";
      ((JSONObject)localObject3).put((String)localObject5, localObject4);
      localObject4 = new org/json/JSONObject;
      ((JSONObject)localObject4).<init>();
      localObject5 = "enabled";
      boolean bool3 = c;
      ((JSONObject)localObject4).put((String)localObject5, bool3);
      localObject5 = "maxRetryCount";
      ((JSONObject)localObject4).put((String)localObject5, i2);
      localObject5 = "eventTTL";
      ((JSONObject)localObject4).put((String)localObject5, l1);
      localObject5 = "maxEventsToPersist";
      ((JSONObject)localObject4).put((String)localObject5, i4);
      localObject5 = "txLatency";
      ((JSONObject)localObject4).put((String)localObject5, l2);
      localObject5 = "processingInterval";
      ((JSONObject)localObject4).put((String)localObject5, l3);
      localObject5 = e();
      String str2 = "networkType";
      ((JSONObject)localObject4).put(str2, localObject5);
      localObject5 = new org/json/JSONObject;
      ((JSONObject)localObject5).<init>();
      str2 = "enabled";
      boolean bool4 = d;
      ((JSONObject)localObject5).put(str2, bool4);
      str2 = "maxRetryCount";
      ((JSONObject)localObject5).put(str2, i2);
      Object localObject6 = "eventTTL";
      ((JSONObject)localObject5).put((String)localObject6, l1);
      localObject6 = "maxEventsToPersist";
      ((JSONObject)localObject5).put((String)localObject6, i4);
      localObject6 = "txLatency";
      ((JSONObject)localObject5).put((String)localObject6, l2);
      localObject6 = "processingInterval";
      ((JSONObject)localObject5).put((String)localObject6, l3);
      localObject6 = e();
      str1 = "networkType";
      ((JSONObject)localObject5).put(str1, localObject6);
      localObject6 = "baseDict";
      ((JSONObject)localObject1).put((String)localObject6, localObject2);
      localObject2 = "bannerDict";
      ((JSONObject)localObject1).put((String)localObject2, localObject3);
      localObject2 = "intDict";
      ((JSONObject)localObject1).put((String)localObject2, localObject4);
      localObject2 = "nativeDict";
      ((JSONObject)localObject1).put((String)localObject2, localObject5);
      b((JSONObject)localObject1);
      return;
    }
    catch (JSONException localJSONException) {}
  }
  
  private static JSONObject a(b.a parama)
  {
    JSONObject localJSONObject1 = new org/json/JSONObject;
    localJSONObject1.<init>();
    Object localObject = g;
    JSONObject localJSONObject2 = new org/json/JSONObject;
    localJSONObject2.<init>();
    long l1 = a;
    localJSONObject2.put("retryInterval", l1);
    int i1 = b;
    localJSONObject2.put("maxBatchSize", i1);
    localJSONObject1.put("wifi", localJSONObject2);
    parama = f;
    localObject = new org/json/JSONObject;
    ((JSONObject)localObject).<init>();
    long l2 = a;
    ((JSONObject)localObject).put("retryInterval", l2);
    int i2 = b;
    ((JSONObject)localObject).put("maxBatchSize", i2);
    localJSONObject1.put("others", localObject);
    return localJSONObject1;
  }
  
  private static void a(JSONObject paramJSONObject, b.a parama)
  {
    Iterator localIterator = paramJSONObject.keys();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      String str1 = (String)localIterator.next();
      JSONObject localJSONObject = paramJSONObject.getJSONObject(str1);
      b.i locali = new com/inmobi/ads/b$i;
      locali.<init>();
      long l1 = localJSONObject.getLong("retryInterval");
      a = l1;
      String str2 = "maxBatchSize";
      int i1 = localJSONObject.getInt(str2);
      b = i1;
      i1 = -1;
      int i2 = str1.hashCode();
      int i3 = -1068855134;
      if (i2 != i3)
      {
        i3 = -1006804125;
        if (i2 != i3)
        {
          i3 = 3649301;
          if (i2 == i3)
          {
            str2 = "wifi";
            bool = str1.equals(str2);
            if (bool)
            {
              i1 = 0;
              localJSONObject = null;
            }
          }
        }
        else
        {
          str2 = "others";
          bool = str1.equals(str2);
          if (bool) {
            i1 = 2;
          }
        }
      }
      else
      {
        str2 = "mobile";
        bool = str1.equals(str2);
        if (bool) {
          i1 = 1;
        }
      }
      if (i1 != 0) {
        f = locali;
      } else {
        g = locali;
      }
    }
  }
  
  private void b(JSONObject paramJSONObject)
  {
    Object localObject1 = paramJSONObject.getJSONObject("baseDict");
    Object localObject2 = new com/inmobi/ads/b$a;
    ((b.a)localObject2).<init>();
    j = ((b.a)localObject2);
    localObject2 = j;
    boolean bool1 = ((JSONObject)localObject1).getBoolean("enabled");
    h = bool1;
    localObject2 = j;
    int i1 = ((JSONObject)localObject1).getInt("maxRetryCount");
    a = i1;
    localObject2 = j;
    long l1 = ((JSONObject)localObject1).getLong("eventTTL");
    b = l1;
    localObject2 = j;
    i1 = ((JSONObject)localObject1).getInt("maxEventsToPersist");
    c = i1;
    localObject2 = j;
    l1 = ((JSONObject)localObject1).getLong("processingInterval");
    d = l1;
    localObject2 = j;
    Object localObject3 = "txLatency";
    l1 = ((JSONObject)localObject1).getLong((String)localObject3);
    e = l1;
    localObject1 = ((JSONObject)localObject1).getJSONObject("networkType");
    localObject2 = j;
    a((JSONObject)localObject1, (b.a)localObject2);
    paramJSONObject.remove("baseDict");
    localObject1 = new java/util/HashMap;
    ((HashMap)localObject1).<init>();
    k = ((Map)localObject1);
    localObject1 = paramJSONObject.keys();
    for (;;)
    {
      boolean bool3 = ((Iterator)localObject1).hasNext();
      if (!bool3) {
        break;
      }
      localObject2 = (String)((Iterator)localObject1).next();
      if (localObject2 != null)
      {
        localObject3 = v;
        if (localObject3 != null)
        {
          boolean bool2 = ((List)localObject3).contains(localObject2);
          if (bool2)
          {
            localObject3 = paramJSONObject.getJSONObject((String)localObject2);
            b.a locala = new com/inmobi/ads/b$a;
            locala.<init>();
            Object localObject4 = "enabled";
            boolean bool4 = ((JSONObject)localObject3).has((String)localObject4);
            if (bool4)
            {
              localObject4 = "enabled";
              bool4 = ((JSONObject)localObject3).getBoolean((String)localObject4);
            }
            else
            {
              localObject4 = j;
              bool4 = h;
            }
            h = bool4;
            localObject4 = "maxRetryCount";
            bool4 = ((JSONObject)localObject3).has((String)localObject4);
            int i2;
            if (bool4)
            {
              localObject4 = "maxRetryCount";
              i2 = ((JSONObject)localObject3).getInt((String)localObject4);
            }
            else
            {
              localObject4 = j;
              i2 = a;
            }
            a = i2;
            localObject4 = "eventTTL";
            boolean bool5 = ((JSONObject)localObject3).has((String)localObject4);
            long l2;
            if (bool5)
            {
              localObject4 = "eventTTL";
              l2 = ((JSONObject)localObject3).getLong((String)localObject4);
            }
            else
            {
              localObject4 = j;
              l2 = b;
            }
            b = l2;
            localObject4 = "maxEventsToPersist";
            bool5 = ((JSONObject)localObject3).has((String)localObject4);
            int i3;
            if (bool5)
            {
              localObject4 = "maxEventsToPersist";
              i3 = ((JSONObject)localObject3).getInt((String)localObject4);
            }
            else
            {
              localObject4 = j;
              i3 = c;
            }
            c = i3;
            localObject4 = "processingInterval";
            boolean bool6 = ((JSONObject)localObject3).has((String)localObject4);
            if (bool6)
            {
              localObject4 = "processingInterval";
              l2 = ((JSONObject)localObject3).getLong((String)localObject4);
            }
            else
            {
              localObject4 = j;
              l2 = d;
            }
            d = l2;
            localObject4 = "txLatency";
            bool6 = ((JSONObject)localObject3).has((String)localObject4);
            if (bool6)
            {
              localObject4 = "txLatency";
              l2 = ((JSONObject)localObject3).getLong((String)localObject4);
            }
            else
            {
              localObject4 = j;
              l2 = e;
            }
            e = l2;
            localObject4 = "networkType";
            a(((JSONObject)localObject3).getJSONObject((String)localObject4), locala);
            localObject3 = k;
            ((Map)localObject3).put(localObject2, locala);
          }
        }
      }
    }
  }
  
  private void c(JSONObject paramJSONObject)
  {
    Object localObject1 = paramJSONObject.getJSONObject("base");
    Object localObject2 = new com/inmobi/ads/b$d;
    ((b.d)localObject2).<init>();
    w = ((b.d)localObject2);
    localObject2 = w;
    int i1 = ((JSONObject)localObject1).getInt("maxCacheSize");
    a = i1;
    localObject2 = w;
    i1 = ((JSONObject)localObject1).getInt("fetchLimit");
    b = i1;
    localObject2 = w;
    i1 = ((JSONObject)localObject1).getInt("minThreshold");
    c = i1;
    localObject2 = w;
    Object localObject3 = "timeToLive";
    long l1 = ((JSONObject)localObject1).getLong((String)localObject3);
    d = l1;
    paramJSONObject.remove("base");
    localObject1 = new java/util/HashMap;
    ((HashMap)localObject1).<init>();
    x = ((Map)localObject1);
    localObject1 = paramJSONObject.keys();
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      localObject2 = (String)((Iterator)localObject1).next();
      localObject3 = paramJSONObject.getJSONObject((String)localObject2);
      b.d locald = new com/inmobi/ads/b$d;
      locald.<init>();
      Object localObject4 = "maxCacheSize";
      boolean bool2 = ((JSONObject)localObject3).has((String)localObject4);
      int i2;
      if (bool2)
      {
        localObject4 = "maxCacheSize";
        i2 = ((JSONObject)localObject3).getInt((String)localObject4);
      }
      else
      {
        localObject4 = w;
        i2 = a;
      }
      a = i2;
      localObject4 = "fetchLimit";
      boolean bool3 = ((JSONObject)localObject3).has((String)localObject4);
      int i3;
      if (bool3)
      {
        localObject4 = "fetchLimit";
        i3 = ((JSONObject)localObject3).getInt((String)localObject4);
      }
      else
      {
        localObject4 = w;
        i3 = b;
      }
      b = i3;
      localObject4 = "minThreshold";
      boolean bool4 = ((JSONObject)localObject3).has((String)localObject4);
      int i4;
      if (bool4)
      {
        localObject4 = "minThreshold";
        i4 = ((JSONObject)localObject3).getInt((String)localObject4);
      }
      else
      {
        localObject4 = w;
        i4 = c;
      }
      c = i4;
      localObject4 = "timeToLive";
      boolean bool5 = ((JSONObject)localObject3).has((String)localObject4);
      long l2;
      if (bool5)
      {
        localObject4 = "timeToLive";
        i1 = ((JSONObject)localObject3).getInt((String)localObject4);
        l2 = i1;
      }
      else
      {
        localObject3 = w;
        l2 = d;
      }
      d = l2;
      localObject3 = x;
      ((Map)localObject3).put(localObject2, locald);
    }
  }
  
  private static JSONObject e()
  {
    JSONObject localJSONObject1 = new org/json/JSONObject;
    localJSONObject1.<init>();
    JSONObject localJSONObject2 = new org/json/JSONObject;
    localJSONObject2.<init>();
    long l1 = 3;
    localJSONObject2.put("retryInterval", l1);
    localJSONObject2.put("maxBatchSize", 10);
    localJSONObject1.put("wifi", localJSONObject2);
    localJSONObject2 = new org/json/JSONObject;
    localJSONObject2.<init>();
    localJSONObject2.put("retryInterval", l1);
    localJSONObject2.put("maxBatchSize", 5);
    localJSONObject1.put("others", localJSONObject2);
    return localJSONObject1;
  }
  
  private JSONObject f()
  {
    JSONObject localJSONObject = new org/json/JSONObject;
    localJSONObject.<init>();
    Object localObject1 = new org/json/JSONObject;
    ((JSONObject)localObject1).<init>();
    boolean bool1 = z.a;
    ((JSONObject)localObject1).put("enabled", bool1);
    long l1 = z.b;
    ((JSONObject)localObject1).put("placementExpiry", l1);
    Object localObject2 = z;
    int i1 = c;
    ((JSONObject)localObject1).put("maxPreloadedAds", i1);
    Object localObject3 = "base";
    localJSONObject.put((String)localObject3, localObject1);
    localObject1 = y.entrySet().iterator();
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject1).hasNext();
      if (!bool2) {
        break;
      }
      localObject3 = (Map.Entry)((Iterator)localObject1).next();
      localObject2 = new org/json/JSONObject;
      ((JSONObject)localObject2).<init>();
      b.g localg = (b.g)((Map.Entry)localObject3).getValue();
      boolean bool3 = a;
      ((JSONObject)localObject2).put("enabled", bool3);
      long l2 = b;
      ((JSONObject)localObject2).put("placementExpiry", l2);
      String str = "maxPreloadedAds";
      int i2 = c;
      ((JSONObject)localObject2).put(str, i2);
      localObject3 = (String)((Map.Entry)localObject3).getKey();
      localJSONObject.put((String)localObject3, localObject2);
    }
    return localJSONObject;
  }
  
  private JSONObject g()
  {
    JSONObject localJSONObject = new org/json/JSONObject;
    localJSONObject.<init>();
    Object localObject1 = new org/json/JSONObject;
    ((JSONObject)localObject1).<init>();
    boolean bool1 = j.h;
    ((JSONObject)localObject1).put("enabled", bool1);
    int i1 = j.a;
    ((JSONObject)localObject1).put("maxRetryCount", i1);
    long l1 = j.b;
    ((JSONObject)localObject1).put("eventTTL", l1);
    i1 = j.c;
    ((JSONObject)localObject1).put("maxEventsToPersist", i1);
    l1 = j.d;
    ((JSONObject)localObject1).put("processingInterval", l1);
    l1 = j.e;
    ((JSONObject)localObject1).put("txLatency", l1);
    Object localObject2 = a(j);
    Object localObject3 = "networkType";
    ((JSONObject)localObject1).put((String)localObject3, localObject2);
    localObject2 = "baseDict";
    localJSONObject.put((String)localObject2, localObject1);
    localObject1 = k.entrySet().iterator();
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject1).hasNext();
      if (!bool2) {
        break;
      }
      localObject2 = (Map.Entry)((Iterator)localObject1).next();
      localObject3 = new org/json/JSONObject;
      ((JSONObject)localObject3).<init>();
      Object localObject4 = (b.a)((Map.Entry)localObject2).getValue();
      boolean bool3 = h;
      ((JSONObject)localObject3).put("enabled", bool3);
      int i2 = a;
      ((JSONObject)localObject3).put("maxRetryCount", i2);
      long l2 = b;
      ((JSONObject)localObject3).put("eventTTL", l2);
      i2 = c;
      ((JSONObject)localObject3).put("maxEventsToPersist", i2);
      l2 = d;
      ((JSONObject)localObject3).put("processingInterval", l2);
      l2 = e;
      ((JSONObject)localObject3).put("txLatency", l2);
      localObject4 = a((b.a)localObject4);
      String str = "networkType";
      ((JSONObject)localObject3).put(str, localObject4);
      localObject2 = (String)((Map.Entry)localObject2).getKey();
      localJSONObject.put((String)localObject2, localObject3);
    }
    return localJSONObject;
  }
  
  final b.d a(String paramString)
  {
    Map localMap = x;
    paramString = (b.d)localMap.get(paramString);
    if (paramString == null) {
      paramString = w;
    }
    return paramString;
  }
  
  public final String a()
  {
    return "ads";
  }
  
  public final void a(JSONObject paramJSONObject)
  {
    super.a(paramJSONObject);
    Object localObject1 = "url";
    boolean bool1 = paramJSONObject.has((String)localObject1);
    if (bool1)
    {
      localObject1 = paramJSONObject.getString("url");
      e = ((String)localObject1);
    }
    localObject1 = "trueRequestUrl";
    bool1 = paramJSONObject.has((String)localObject1);
    if (bool1)
    {
      localObject1 = paramJSONObject.getString("trueRequestUrl");
      f = ((String)localObject1);
    }
    int i1 = paramJSONObject.getInt("minimumRefreshInterval");
    g = i1;
    i1 = paramJSONObject.getInt("defaultRefreshInterval");
    h = i1;
    i1 = paramJSONObject.getInt("fetchTimeout");
    i = i1;
    localObject1 = paramJSONObject.getJSONObject("cache");
    c((JSONObject)localObject1);
    localObject1 = paramJSONObject.getJSONObject("trcFlagDict");
    b((JSONObject)localObject1);
    localObject1 = paramJSONObject.getJSONObject("preload");
    ??? = ((JSONObject)localObject1).getJSONObject("base");
    ??? = new com/inmobi/ads/b$g;
    ((b.g)???).<init>();
    z = ((b.g)???);
    ??? = z;
    boolean bool3 = ((JSONObject)???).getBoolean("enabled");
    a = bool3;
    ??? = z;
    long l1 = ((JSONObject)???).getLong("placementExpiry");
    b = l1;
    ??? = z;
    Object localObject4 = "maxPreloadedAds";
    int i4 = ((JSONObject)???).getInt((String)localObject4);
    c = i4;
    ((JSONObject)localObject1).remove("base");
    ??? = ((JSONObject)localObject1).keys();
    Object localObject6;
    for (;;)
    {
      boolean bool4 = ((Iterator)???).hasNext();
      if (!bool4) {
        break;
      }
      ??? = (String)((Iterator)???).next();
      localObject4 = ((JSONObject)localObject1).getJSONObject((String)???);
      localObject5 = new com/inmobi/ads/b$g;
      ((b.g)localObject5).<init>();
      localObject6 = "enabled";
      boolean bool8 = ((JSONObject)localObject4).has((String)localObject6);
      if (bool8)
      {
        localObject6 = "enabled";
        bool8 = ((JSONObject)localObject4).getBoolean((String)localObject6);
      }
      else
      {
        localObject6 = z;
        bool8 = a;
      }
      a = bool8;
      localObject6 = "placementExpiry";
      bool8 = ((JSONObject)localObject4).has((String)localObject6);
      long l2;
      if (bool8)
      {
        localObject6 = "placementExpiry";
        int i8 = ((JSONObject)localObject4).getInt((String)localObject6);
        l2 = i8;
      }
      else
      {
        localObject6 = z;
        l2 = b;
      }
      b = l2;
      localObject6 = "maxPreloadedAds";
      boolean bool9 = ((JSONObject)localObject4).has((String)localObject6);
      if (bool9)
      {
        localObject6 = "maxPreloadedAds";
        i3 = ((JSONObject)localObject4).getInt((String)localObject6);
      }
      else
      {
        localObject4 = z;
        i3 = c;
      }
      c = i3;
      localObject4 = y;
      ((Map)localObject4).put(???, localObject5);
    }
    localObject1 = paramJSONObject.getJSONObject("imai");
    ??? = l;
    int i5 = ((JSONObject)localObject1).getInt("maxRetries");
    a = i5;
    ??? = l;
    i5 = ((JSONObject)localObject1).getInt("pingInterval");
    b = i5;
    ??? = l;
    i5 = ((JSONObject)localObject1).getInt("pingTimeout");
    c = i5;
    ??? = l;
    i5 = ((JSONObject)localObject1).getInt("maxDbEvents");
    d = i5;
    ??? = l;
    i5 = ((JSONObject)localObject1).getInt("maxEventBatch");
    e = i5;
    ??? = l;
    long l3 = ((JSONObject)localObject1).getLong("pingCacheExpiry");
    f = l3;
    localObject1 = paramJSONObject.getJSONObject("rendering");
    ??? = m;
    i5 = ((JSONObject)localObject1).getInt("renderTimeout");
    a = i5;
    ??? = m;
    i5 = ((JSONObject)localObject1).getInt("picHeight");
    c = i5;
    ??? = m;
    i5 = ((JSONObject)localObject1).getInt("picWidth");
    b = i5;
    ??? = m;
    i5 = ((JSONObject)localObject1).getInt("picQuality");
    d = i5;
    ??? = m;
    ??? = ((JSONObject)localObject1).getString("webviewBackground");
    e = ((String)???);
    ??? = m;
    i5 = ((JSONObject)localObject1).getInt("maxVibrationDuration");
    g = i5;
    ??? = m;
    i5 = ((JSONObject)localObject1).getInt("maxVibrationPatternLength");
    h = i5;
    ??? = m;
    int i3 = 0;
    localObject4 = null;
    boolean bool5 = ((JSONObject)localObject1).optBoolean("enablePubMuteControl", false);
    l = bool5;
    ??? = m;
    ??? = ((JSONObject)localObject1).getJSONObject("savecontent");
    Object localObject5 = "maxSaveSize";
    int i6 = ((JSONObject)???).getInt((String)localObject5);
    long l4 = i6;
    i = l4;
    synchronized (u)
    {
      ??? = m;
      ??? = j;
      ((ArrayList)???).clear();
      ??? = "savecontent";
      ??? = ((JSONObject)localObject1).getJSONObject((String)???);
      localObject5 = "allowedContentType";
      ??? = ((JSONObject)???).getJSONArray((String)localObject5);
      int i10 = 0;
      localObject5 = null;
      for (;;)
      {
        int i9 = ((JSONArray)???).length();
        if (i10 >= i9) {
          break;
        }
        localObject6 = m;
        localObject6 = j;
        String str = ((JSONArray)???).getString(i10);
        ((ArrayList)localObject6).add(str);
        i10 += 1;
      }
      ??? = m;
      boolean bool2 = ((JSONObject)localObject1).getBoolean("shouldRenderPopup");
      k = bool2;
      localObject1 = paramJSONObject.getJSONObject("mraid");
      ??? = n;
      l4 = ((JSONObject)localObject1).getLong("expiry");
      a = l4;
      ??? = n;
      i6 = ((JSONObject)localObject1).getInt("maxRetries");
      b = i6;
      ??? = n;
      i6 = ((JSONObject)localObject1).getInt("retryInterval");
      c = i6;
      ??? = n;
      ??? = "url";
      localObject1 = ((JSONObject)localObject1).getString((String)???);
      d = ((String)localObject1);
      localObject1 = "telemetry";
      bool2 = paramJSONObject.has((String)localObject1);
      if (bool2)
      {
        localObject1 = paramJSONObject.getJSONObject("telemetry");
        p = ((JSONObject)localObject1);
      }
      localObject1 = paramJSONObject.getJSONObject("viewability");
      ??? = o;
      i6 = ((JSONObject)localObject1).getInt("impressionMinPercentageViewed");
      a = i6;
      ??? = o;
      i6 = ((JSONObject)localObject1).getInt("impressionMinTimeViewed");
      b = i6;
      ??? = o;
      i6 = ((JSONObject)localObject1).optInt("displayMinPercentageAnimate", 67);
      e = i6;
      ??? = o;
      i6 = ((JSONObject)localObject1).optInt("visibilityThrottleMillis", 100);
      c = i6;
      ??? = o;
      i6 = ((JSONObject)localObject1).optInt("impressionPollIntervalMillis", 250);
      d = i6;
      ??? = o;
      boolean bool6 = ((JSONObject)localObject1).optBoolean("moatEnabled", false);
      i = bool6;
      ??? = o;
      bool6 = ((JSONObject)localObject1).optBoolean("iasEnabled", false);
      j = bool6;
      localObject1 = ((JSONObject)localObject1).getJSONObject("video");
      ??? = o;
      int i7 = ((JSONObject)localObject1).getInt("impressionMinPercentageViewed");
      f = i7;
      ??? = o;
      i7 = ((JSONObject)localObject1).getInt("impressionMinTimeViewed");
      g = i7;
      ??? = o;
      i10 = 50;
      int i2 = ((JSONObject)localObject1).optInt("videoMinPercentagePlay", i10);
      h = i2;
      localObject1 = paramJSONObject.getJSONObject("vastVideo");
      ??? = q;
      i7 = ((JSONObject)localObject1).getInt("maxWrapperLimit");
      a = i7;
      ??? = q;
      l4 = ((JSONObject)localObject1).getLong("optimalVastVideoSize");
      b = l4;
      ??? = q;
      l4 = ((JSONObject)localObject1).getLong("vastMaxAssetSize");
      c = l4;
      synchronized (u)
      {
        ??? = q;
        ??? = e;
        ((ArrayList)???).clear();
        ??? = "allowedContentType";
        ??? = ((JSONObject)localObject1).getJSONArray((String)???);
        for (;;)
        {
          i10 = ((JSONArray)???).length();
          if (i3 >= i10) {
            break;
          }
          localObject5 = q;
          localObject5 = e;
          localObject6 = ((JSONArray)???).getString(i3);
          ((ArrayList)localObject5).add(localObject6);
          i3 += 1;
        }
        ??? = q.d;
        localObject1 = ((JSONObject)localObject1).getJSONObject("bitRate");
        boolean bool7 = ((JSONObject)localObject1).getBoolean("bitrate_mandatory");
        a = bool7;
        i2 = ((JSONObject)localObject1).getInt("headerTimeout");
        b = i2;
        paramJSONObject = paramJSONObject.getJSONObject("assetCache");
        localObject1 = r;
        i4 = paramJSONObject.getInt("retryInterval");
        b = i4;
        localObject1 = r;
        i4 = paramJSONObject.getInt("maxRetries");
        a = i4;
        localObject1 = r;
        i4 = paramJSONObject.getInt("maxCachedAssets");
        c = i4;
        localObject1 = r;
        long l5 = paramJSONObject.getInt("maxCacheSize");
        d = l5;
        localObject1 = r;
        l5 = paramJSONObject.getLong("timeToLive");
        e = l5;
        return;
      }
    }
  }
  
  public final b.a b(String paramString)
  {
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    ((StringBuilder)localObject).append(paramString);
    ((StringBuilder)localObject).append("Dict");
    paramString = ((StringBuilder)localObject).toString();
    localObject = k;
    paramString = (b.a)((Map)localObject).get(paramString);
    if (paramString == null) {
      paramString = j;
    }
    return paramString;
  }
  
  public final JSONObject b()
  {
    JSONObject localJSONObject1 = super.b();
    Object localObject1 = e;
    localJSONObject1.put("url", localObject1);
    localObject1 = f;
    localJSONObject1.put("trueRequestUrl", localObject1);
    int i1 = g;
    localJSONObject1.put("minimumRefreshInterval", i1);
    i1 = h;
    localJSONObject1.put("defaultRefreshInterval", i1);
    i1 = i;
    localJSONObject1.put("fetchTimeout", i1);
    JSONObject localJSONObject2 = new org/json/JSONObject;
    localJSONObject2.<init>();
    localObject1 = new org/json/JSONObject;
    ((JSONObject)localObject1).<init>();
    int i2 = w.a;
    ((JSONObject)localObject1).put("maxCacheSize", i2);
    i2 = w.b;
    ((JSONObject)localObject1).put("fetchLimit", i2);
    i2 = w.c;
    ((JSONObject)localObject1).put("minThreshold", i2);
    Object localObject2 = w;
    long l1 = d;
    ((JSONObject)localObject1).put("timeToLive", l1);
    Object localObject3 = "base";
    localJSONObject2.put((String)localObject3, localObject1);
    localObject1 = x.entrySet().iterator();
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject1).hasNext();
      if (!bool2) {
        break;
      }
      localObject3 = (Map.Entry)((Iterator)localObject1).next();
      localObject2 = new org/json/JSONObject;
      ((JSONObject)localObject2).<init>();
      localObject4 = (b.d)((Map.Entry)localObject3).getValue();
      int i6 = a;
      ((JSONObject)localObject2).put("maxCacheSize", i6);
      i6 = b;
      ((JSONObject)localObject2).put("fetchLimit", i6);
      i6 = c;
      ((JSONObject)localObject2).put("minThreshold", i6);
      String str = "timeToLive";
      long l2 = d;
      ((JSONObject)localObject2).put(str, l2);
      localObject3 = (String)((Map.Entry)localObject3).getKey();
      localJSONObject2.put((String)localObject3, localObject2);
    }
    localJSONObject1.put("cache", localJSONObject2);
    localJSONObject2 = g();
    localJSONObject1.put("trcFlagDict", localJSONObject2);
    localJSONObject2 = new org/json/JSONObject;
    localJSONObject2.<init>();
    int i3 = l.a;
    localJSONObject2.put("maxRetries", i3);
    i3 = l.b;
    localJSONObject2.put("pingInterval", i3);
    i3 = l.c;
    localJSONObject2.put("pingTimeout", i3);
    i3 = l.d;
    localJSONObject2.put("maxDbEvents", i3);
    i3 = l.e;
    localJSONObject2.put("maxEventBatch", i3);
    long l3 = l.f;
    localJSONObject2.put("pingCacheExpiry", l3);
    localJSONObject1.put("imai", localJSONObject2);
    localJSONObject2 = new org/json/JSONObject;
    localJSONObject2.<init>();
    i3 = m.a;
    localJSONObject2.put("renderTimeout", i3);
    i3 = m.b;
    localJSONObject2.put("picWidth", i3);
    i3 = m.c;
    localJSONObject2.put("picHeight", i3);
    i3 = m.d;
    localJSONObject2.put("picQuality", i3);
    localObject3 = m.e;
    localJSONObject2.put("webviewBackground", localObject3);
    i3 = m.g;
    localJSONObject2.put("maxVibrationDuration", i3);
    i3 = m.h;
    localJSONObject2.put("maxVibrationPatternLength", i3);
    boolean bool3 = m.l;
    localJSONObject2.put("enablePubMuteControl", bool3);
    localObject1 = new org/json/JSONObject;
    ((JSONObject)localObject1).<init>();
    l1 = m.i;
    ((JSONObject)localObject1).put("maxSaveSize", l1);
    localObject2 = new org/json/JSONArray;
    Object localObject4 = m.j;
    ((JSONArray)localObject2).<init>((Collection)localObject4);
    ((JSONObject)localObject1).put("allowedContentType", localObject2);
    localJSONObject2.put("savecontent", localObject1);
    bool3 = m.k;
    localJSONObject2.put("shouldRenderPopup", bool3);
    localJSONObject1.put("rendering", localJSONObject2);
    localJSONObject2 = new org/json/JSONObject;
    localJSONObject2.<init>();
    l3 = n.a;
    localJSONObject2.put("expiry", l3);
    int i4 = n.b;
    localJSONObject2.put("maxRetries", i4);
    i4 = n.c;
    localJSONObject2.put("retryInterval", i4);
    localObject3 = n.d;
    localJSONObject2.put("url", localObject3);
    localJSONObject1.put("mraid", localJSONObject2);
    localJSONObject2 = new org/json/JSONObject;
    localJSONObject2.<init>();
    i4 = o.a;
    localJSONObject2.put("impressionMinPercentageViewed", i4);
    i4 = o.b;
    localJSONObject2.put("impressionMinTimeViewed", i4);
    i4 = o.e;
    localJSONObject2.put("displayMinPercentageAnimate", i4);
    i4 = o.c;
    localJSONObject2.put("visibilityThrottleMillis", i4);
    i4 = o.d;
    localJSONObject2.put("impressionPollIntervalMillis", i4);
    boolean bool4 = o.i;
    localJSONObject2.put("moatEnabled", bool4);
    bool4 = o.j;
    localJSONObject2.put("iasEnabled", bool4);
    localObject1 = new org/json/JSONObject;
    ((JSONObject)localObject1).<init>();
    i2 = o.f;
    ((JSONObject)localObject1).put("impressionMinPercentageViewed", i2);
    i2 = o.g;
    ((JSONObject)localObject1).put("impressionMinTimeViewed", i2);
    i2 = o.h;
    ((JSONObject)localObject1).put("videoMinPercentagePlay", i2);
    localJSONObject2.put("video", localObject1);
    localJSONObject1.put("viewability", localJSONObject2);
    localJSONObject2 = f();
    localJSONObject1.put("preload", localJSONObject2);
    localJSONObject2 = new org/json/JSONObject;
    localJSONObject2.<init>();
    int i5 = q.a;
    localJSONObject2.put("maxWrapperLimit", i5);
    l3 = q.b;
    localJSONObject2.put("optimalVastVideoSize", l3);
    l3 = q.c;
    localJSONObject2.put("vastMaxAssetSize", l3);
    localObject3 = new org/json/JSONArray;
    localObject2 = q.e;
    ((JSONArray)localObject3).<init>((Collection)localObject2);
    localJSONObject2.put("allowedContentType", localObject3);
    localObject1 = q.d;
    localObject3 = new org/json/JSONObject;
    ((JSONObject)localObject3).<init>();
    int i7 = b;
    ((JSONObject)localObject3).put("headerTimeout", i7);
    localObject2 = "bitrate_mandatory";
    boolean bool1 = a;
    ((JSONObject)localObject3).put((String)localObject2, bool1);
    localJSONObject2.put("bitRate", localObject3);
    localJSONObject1.put("vastVideo", localJSONObject2);
    localJSONObject2 = new org/json/JSONObject;
    localJSONObject2.<init>();
    i5 = r.b;
    localJSONObject2.put("retryInterval", i5);
    i5 = r.a;
    localJSONObject2.put("maxRetries", i5);
    i5 = r.c;
    localJSONObject2.put("maxCachedAssets", i5);
    l3 = r.d;
    localJSONObject2.put("maxCacheSize", l3);
    localObject3 = r;
    l3 = e;
    localJSONObject2.put("timeToLive", l3);
    localObject1 = "assetCache";
    localJSONObject1.put((String)localObject1, localJSONObject2);
    localJSONObject2 = p;
    if (localJSONObject2 != null)
    {
      localObject1 = "telemetry";
      localJSONObject1.put((String)localObject1, localJSONObject2);
    }
    return localJSONObject1;
  }
  
  public final b.g c(String paramString)
  {
    Map localMap = y;
    paramString = (b.g)localMap.get(paramString);
    if (paramString == null) {
      paramString = z;
    }
    return paramString;
  }
  
  public final boolean c()
  {
    Object localObject1 = e;
    boolean bool1 = ((String)localObject1).startsWith("http://");
    Object localObject2;
    if (!bool1)
    {
      localObject1 = e;
      localObject2 = "https://";
      bool1 = ((String)localObject1).startsWith((String)localObject2);
      if (!bool1) {}
    }
    else
    {
      localObject1 = f;
      localObject2 = "http://";
      bool1 = ((String)localObject1).startsWith((String)localObject2);
      if (!bool1)
      {
        localObject1 = f;
        localObject2 = "https://";
        bool1 = ((String)localObject1).startsWith((String)localObject2);
        if (!bool1) {}
      }
      else
      {
        int i1 = g;
        if (i1 >= 0)
        {
          i1 = h;
          if (i1 >= 0)
          {
            i1 = i;
            if (i1 > 0)
            {
              localObject1 = w;
              if (localObject1 != null)
              {
                boolean bool2 = ((b.d)localObject1).a();
                if (bool2)
                {
                  localObject1 = x.entrySet().iterator();
                  boolean bool9;
                  do
                  {
                    bool9 = ((Iterator)localObject1).hasNext();
                    if (!bool9) {
                      break;
                    }
                    localObject2 = (b.d)((Map.Entry)((Iterator)localObject1).next()).getValue();
                    bool9 = ((b.d)localObject2).a();
                  } while (bool9);
                  return false;
                  localObject1 = j;
                  if (localObject1 != null)
                  {
                    bool2 = ((b.a)localObject1).a();
                    if (bool2)
                    {
                      localObject1 = k.entrySet().iterator();
                      do
                      {
                        bool9 = ((Iterator)localObject1).hasNext();
                        if (!bool9) {
                          break;
                        }
                        localObject2 = (b.a)((Map.Entry)((Iterator)localObject1).next()).getValue();
                        bool9 = ((b.a)localObject2).a();
                      } while (bool9);
                      return false;
                      localObject1 = l;
                      int i2 = d;
                      if (i2 >= 0)
                      {
                        localObject1 = l;
                        i2 = e;
                        if (i2 >= 0)
                        {
                          localObject1 = l;
                          i2 = a;
                          if (i2 >= 0)
                          {
                            localObject1 = l;
                            i2 = b;
                            if (i2 >= 0)
                            {
                              localObject1 = l;
                              i2 = c;
                              if (i2 > 0)
                              {
                                localObject1 = l;
                                long l1 = f;
                                long l2 = 0L;
                                boolean bool3 = l1 < l2;
                                if (bool3)
                                {
                                  localObject1 = n;
                                  l1 = a;
                                  bool3 = l1 < l2;
                                  if (!bool3)
                                  {
                                    localObject1 = n;
                                    int i3 = c;
                                    if (i3 >= 0)
                                    {
                                      localObject1 = n;
                                      i3 = b;
                                      if (i3 >= 0)
                                      {
                                        localObject1 = n.d;
                                        localObject2 = "http://";
                                        boolean bool4 = ((String)localObject1).startsWith((String)localObject2);
                                        if (!bool4)
                                        {
                                          localObject1 = n.d;
                                          localObject2 = "https://";
                                          bool4 = ((String)localObject1).startsWith((String)localObject2);
                                          if (!bool4) {}
                                        }
                                        else
                                        {
                                          localObject1 = m;
                                          int i4 = a;
                                          if (i4 >= 0)
                                          {
                                            localObject1 = m;
                                            i4 = c;
                                            if (i4 >= 0)
                                            {
                                              localObject1 = m;
                                              i4 = b;
                                              if (i4 >= 0)
                                              {
                                                localObject1 = m;
                                                i4 = d;
                                                if (i4 >= 0)
                                                {
                                                  localObject1 = m;
                                                  i4 = g;
                                                  if (i4 >= 0)
                                                  {
                                                    localObject1 = m;
                                                    i4 = h;
                                                    if (i4 >= 0)
                                                    {
                                                      localObject1 = m;
                                                      l1 = i;
                                                      boolean bool5 = l1 < l2;
                                                      if (!bool5)
                                                      {
                                                        localObject1 = m.e;
                                                        if (localObject1 != null)
                                                        {
                                                          localObject1 = m.e.trim();
                                                          int i5 = ((String)localObject1).length();
                                                          if (i5 != 0) {
                                                            try
                                                            {
                                                              localObject1 = m;
                                                              localObject2 = m;
                                                              localObject2 = e;
                                                              int i9 = Color.parseColor((String)localObject2);
                                                              f = i9;
                                                              localObject1 = n;
                                                              i5 = b;
                                                              if (i5 >= 0)
                                                              {
                                                                localObject1 = n;
                                                                i5 = c;
                                                                if (i5 >= 0)
                                                                {
                                                                  localObject1 = n.d;
                                                                  if (localObject1 != null)
                                                                  {
                                                                    localObject1 = n.d.trim();
                                                                    i5 = ((String)localObject1).length();
                                                                    if (i5 != 0)
                                                                    {
                                                                      localObject1 = o;
                                                                      i5 = a;
                                                                      if (i5 > 0)
                                                                      {
                                                                        localObject1 = o;
                                                                        i5 = a;
                                                                        i9 = 100;
                                                                        if (i5 <= i9)
                                                                        {
                                                                          localObject1 = o;
                                                                          i5 = b;
                                                                          if (i5 >= 0)
                                                                          {
                                                                            localObject1 = o;
                                                                            i5 = e;
                                                                            if (i5 > 0)
                                                                            {
                                                                              localObject1 = o;
                                                                              i5 = e;
                                                                              if (i5 <= i9)
                                                                              {
                                                                                localObject1 = o;
                                                                                i5 = f;
                                                                                if (i5 > 0)
                                                                                {
                                                                                  localObject1 = o;
                                                                                  i5 = f;
                                                                                  if (i5 <= i9)
                                                                                  {
                                                                                    localObject1 = o;
                                                                                    i5 = g;
                                                                                    if (i5 >= 0)
                                                                                    {
                                                                                      localObject1 = o;
                                                                                      i5 = h;
                                                                                      if (i5 > 0)
                                                                                      {
                                                                                        localObject1 = o;
                                                                                        i5 = h;
                                                                                        if (i5 <= i9)
                                                                                        {
                                                                                          localObject1 = o;
                                                                                          i5 = c;
                                                                                          i9 = 50;
                                                                                          if (i5 >= i9)
                                                                                          {
                                                                                            localObject1 = o;
                                                                                            i5 = c * 5;
                                                                                            b.k localk = o;
                                                                                            int i11 = b;
                                                                                            if (i5 <= i11)
                                                                                            {
                                                                                              localObject1 = o;
                                                                                              i5 = d;
                                                                                              if (i5 >= i9)
                                                                                              {
                                                                                                localObject1 = o;
                                                                                                i5 = d * 4;
                                                                                                localObject2 = o;
                                                                                                i9 = b;
                                                                                                if (i5 <= i9)
                                                                                                {
                                                                                                  localObject1 = z;
                                                                                                  if (localObject1 != null)
                                                                                                  {
                                                                                                    boolean bool6 = ((b.g)localObject1).a();
                                                                                                    if (bool6)
                                                                                                    {
                                                                                                      localObject1 = y.entrySet().iterator();
                                                                                                      boolean bool10;
                                                                                                      do
                                                                                                      {
                                                                                                        bool10 = ((Iterator)localObject1).hasNext();
                                                                                                        if (!bool10) {
                                                                                                          break;
                                                                                                        }
                                                                                                        localObject2 = (b.g)((Map.Entry)((Iterator)localObject1).next()).getValue();
                                                                                                        bool10 = ((b.g)localObject2).a();
                                                                                                      } while (bool10);
                                                                                                      return false;
                                                                                                      localObject1 = q;
                                                                                                      l1 = b;
                                                                                                      long l3 = 31457280L;
                                                                                                      bool6 = l1 < l3;
                                                                                                      if (!bool6)
                                                                                                      {
                                                                                                        localObject1 = q;
                                                                                                        l1 = b;
                                                                                                        bool6 = l1 < l2;
                                                                                                        if (bool6)
                                                                                                        {
                                                                                                          localObject1 = q;
                                                                                                          int i6 = a;
                                                                                                          if (i6 >= 0)
                                                                                                          {
                                                                                                            localObject1 = q;
                                                                                                            l1 = c;
                                                                                                            boolean bool7 = l1 < l2;
                                                                                                            if (bool7)
                                                                                                            {
                                                                                                              localObject1 = q;
                                                                                                              l1 = c;
                                                                                                              bool7 = l1 < l3;
                                                                                                              if (!bool7)
                                                                                                              {
                                                                                                                localObject1 = r;
                                                                                                                int i7 = b;
                                                                                                                if (i7 >= 0)
                                                                                                                {
                                                                                                                  localObject1 = r;
                                                                                                                  i7 = c;
                                                                                                                  int i10 = 20;
                                                                                                                  if (i7 <= i10)
                                                                                                                  {
                                                                                                                    localObject1 = r;
                                                                                                                    i7 = c;
                                                                                                                    if (i7 >= 0)
                                                                                                                    {
                                                                                                                      localObject1 = r;
                                                                                                                      l1 = e;
                                                                                                                      boolean bool8 = l1 < l2;
                                                                                                                      if (!bool8)
                                                                                                                      {
                                                                                                                        localObject1 = r;
                                                                                                                        l1 = d;
                                                                                                                        bool8 = l1 < l2;
                                                                                                                        if (!bool8)
                                                                                                                        {
                                                                                                                          localObject1 = r;
                                                                                                                          int i8 = a;
                                                                                                                          if (i8 >= 0) {
                                                                                                                            return true;
                                                                                                                          }
                                                                                                                        }
                                                                                                                      }
                                                                                                                    }
                                                                                                                  }
                                                                                                                }
                                                                                                                return false;
                                                                                                              }
                                                                                                            }
                                                                                                          }
                                                                                                        }
                                                                                                      }
                                                                                                      return false;
                                                                                                    }
                                                                                                  }
                                                                                                  return false;
                                                                                                }
                                                                                              }
                                                                                            }
                                                                                          }
                                                                                        }
                                                                                      }
                                                                                    }
                                                                                  }
                                                                                }
                                                                              }
                                                                            }
                                                                          }
                                                                        }
                                                                      }
                                                                      return false;
                                                                    }
                                                                  }
                                                                }
                                                              }
                                                              return false;
                                                            }
                                                            catch (IllegalArgumentException localIllegalArgumentException)
                                                            {
                                                              return false;
                                                            }
                                                          }
                                                        }
                                                      }
                                                    }
                                                  }
                                                }
                                              }
                                            }
                                          }
                                          return false;
                                        }
                                      }
                                    }
                                  }
                                  return false;
                                }
                              }
                            }
                          }
                        }
                      }
                      return false;
                    }
                  }
                  return false;
                }
              }
              return false;
            }
          }
        }
      }
    }
    return false;
  }
  
  public final a d()
  {
    b localb = new com/inmobi/ads/b;
    localb.<init>();
    return localb;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */