package com.inmobi.ads;

import java.util.Locale;
import org.json.JSONException;
import org.json.JSONObject;

final class bp$a
{
  int a;
  String b;
  
  bp$a(int paramInt, String paramString)
  {
    a = paramInt;
    b = paramString;
  }
  
  static a a(JSONObject paramJSONObject)
  {
    Object localObject1 = "type";
    try
    {
      localObject1 = paramJSONObject.getString((String)localObject1);
      int i = 0;
      if (localObject1 != null)
      {
        Object localObject2 = ((String)localObject1).trim();
        int j = ((String)localObject2).length();
        if (j != 0)
        {
          localObject2 = Locale.US;
          localObject1 = ((String)localObject1).toLowerCase((Locale)localObject2);
          j = -1;
          int k = ((String)localObject1).hashCode();
          int m = -1191214428;
          String str;
          if (k != m)
          {
            m = -892481938;
            boolean bool2;
            if (k != m)
            {
              m = -284840886;
              if (k != m)
              {
                m = 3213227;
                if (k == m)
                {
                  str = "html";
                  boolean bool1 = ((String)localObject1).equals(str);
                  if (bool1)
                  {
                    int n = 3;
                    break label195;
                  }
                }
              }
              else
              {
                str = "unknown";
                bool2 = ((String)localObject1).equals(str);
                if (bool2)
                {
                  bool2 = true;
                  break label195;
                }
              }
            }
            else
            {
              str = "static";
              bool2 = ((String)localObject1).equals(str);
              if (bool2)
              {
                int i1 = 2;
                break label195;
              }
            }
          }
          else
          {
            str = "iframe";
            boolean bool3 = ((String)localObject1).equals(str);
            if (bool3)
            {
              i2 = 4;
              break label195;
            }
          }
          int i2 = -1;
          switch (i2)
          {
          default: 
            break;
          case 4: 
            i = 3;
            break;
          case 3: 
            i = 2;
            break;
          case 2: 
            label195:
            i = 1;
          }
        }
      }
      localObject1 = "content";
      paramJSONObject = paramJSONObject.getString((String)localObject1);
      localObject1 = new com/inmobi/ads/bp$a;
      ((a)localObject1).<init>(i, paramJSONObject);
      return (a)localObject1;
    }
    catch (JSONException paramJSONObject)
    {
      bp.a();
      paramJSONObject.getMessage();
      localObject1 = com.inmobi.commons.core.a.a.a();
      com.inmobi.commons.core.e.a locala = new com/inmobi/commons/core/e/a;
      locala.<init>(paramJSONObject);
      ((com.inmobi.commons.core.a.a)localObject1).a(locala);
    }
    return null;
  }
  
  public final String toString()
  {
    JSONObject localJSONObject = new org/json/JSONObject;
    localJSONObject.<init>();
    Object localObject1 = "type";
    try
    {
      int i = a;
      switch (i)
      {
      default: 
        localObject2 = "unknown";
        break;
      case 3: 
        localObject2 = "iframe";
        break;
      case 2: 
        localObject2 = "html";
        break;
      case 1: 
        localObject2 = "static";
      }
      localJSONObject.put((String)localObject1, localObject2);
      localObject1 = "content";
      localObject2 = b;
      localJSONObject.put((String)localObject1, localObject2);
      return localJSONObject.toString();
    }
    catch (JSONException localJSONException)
    {
      bp.a();
      localJSONException.getMessage();
      localObject1 = com.inmobi.commons.core.a.a.a();
      Object localObject2 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject2).<init>(localJSONException);
      ((com.inmobi.commons.core.a.a)localObject1).a((com.inmobi.commons.core.e.a)localObject2);
    }
    return "";
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.bp.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */