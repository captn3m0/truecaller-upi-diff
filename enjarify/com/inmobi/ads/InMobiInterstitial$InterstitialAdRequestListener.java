package com.inmobi.ads;

public abstract interface InMobiInterstitial$InterstitialAdRequestListener
{
  public abstract void onAdRequestCompleted(InMobiAdRequestStatus paramInMobiAdRequestStatus, InMobiInterstitial paramInMobiInterstitial);
}

/* Location:
 * Qualified Name:     com.inmobi.ads.InMobiInterstitial.InterstitialAdRequestListener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */