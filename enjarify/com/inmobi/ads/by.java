package com.inmobi.ads;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;
import com.inmobi.rendering.RenderView;

final class by
  extends bw
{
  private final ad d;
  private boolean e = false;
  private RenderView f;
  
  by(ad paramad, RenderView paramRenderView)
  {
    super(paramad);
    d = paramad;
    f = paramRenderView;
  }
  
  public final View a(View paramView, ViewGroup paramViewGroup, boolean paramBoolean)
  {
    boolean bool = e;
    Object localObject1 = null;
    if (bool) {
      return null;
    }
    Object localObject2 = d.j();
    if (localObject2 == null) {
      return null;
    }
    localObject1 = new com/inmobi/ads/ap;
    b localb = d.c;
    ad localad = d;
    ak localak = localad.h();
    ((ap)localObject1).<init>((Context)localObject2, localb, localad, localak);
    b = ((bw.a)localObject1);
    Logger.a(Logger.InternalLogLevel.DEBUG, "InMobi", "Ad markup loaded into the container will be inflated into a View.");
    localObject2 = b;
    localObject1 = f;
    paramView = ((bw.a)localObject2).a(paramView, paramViewGroup, paramBoolean, (RenderView)localObject1);
    a(paramView);
    d.t();
    return paramView;
  }
  
  public final void a(int paramInt) {}
  
  public final void a(Context paramContext, int paramInt) {}
  
  public final void a(View... paramVarArgs) {}
  
  final b c()
  {
    return d.c;
  }
  
  public final void d() {}
  
  public final void e()
  {
    boolean bool = e;
    if (bool) {
      return;
    }
    bool = true;
    e = bool;
    Object localObject = b;
    if (localObject != null)
    {
      localObject = b;
      ((bw.a)localObject).a();
    }
    localObject = f;
    if (localObject != null)
    {
      ((RenderView)localObject).destroy();
      bool = false;
      localObject = null;
      f = null;
    }
    super.e();
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.by
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */