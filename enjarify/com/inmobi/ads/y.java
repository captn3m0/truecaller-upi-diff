package com.inmobi.ads;

import android.os.Handler;
import android.os.Looper;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class y
  extends g
{
  private static final String d = "y";
  private static volatile y e;
  private static final Object f;
  private static List g;
  
  static
  {
    Object localObject = new java/lang/Object;
    localObject.<init>();
    f = localObject;
    localObject = new java/util/LinkedList;
    ((LinkedList)localObject).<init>();
    g = (List)localObject;
  }
  
  private y()
  {
    super("int");
  }
  
  public static y d()
  {
    y localy1 = e;
    if (localy1 == null) {
      synchronized (f)
      {
        localy1 = e;
        if (localy1 == null)
        {
          localy1 = new com/inmobi/ads/y;
          localy1.<init>();
          e = localy1;
        }
      }
    }
    return localy2;
  }
  
  final j a(bf parambf)
  {
    Object localObject1 = b;
    Object localObject2 = c;
    localObject1 = ((b)localObject1).c((String)localObject2);
    boolean bool1 = a;
    localObject2 = null;
    if (!bool1) {
      return null;
    }
    c(parambf);
    localObject1 = (j)a.get(parambf);
    if (localObject1 == null) {
      return null;
    }
    boolean bool2 = ((j)localObject1).h();
    if (bool2)
    {
      ((j)localObject1).r();
      a.remove(parambf);
      HashMap localHashMap = new java/util/HashMap;
      localHashMap.<init>();
      localHashMap.put("errorCode", "AdUnitExpired");
      localObject3 = ((j)localObject1).b();
      localHashMap.put("type", localObject3);
      localObject3 = Long.valueOf(b);
      localHashMap.put("plId", localObject3);
      localObject1 = k;
      localHashMap.put("clientRequestId", localObject1);
      return null;
    }
    parambf = (j)a.remove(parambf);
    localObject2 = new java/util/HashMap;
    ((HashMap)localObject2).<init>();
    Object localObject3 = parambf.b();
    ((Map)localObject2).put("type", localObject3);
    localObject3 = Long.valueOf(b);
    ((Map)localObject2).put("plId", localObject3);
    parambf = k;
    ((Map)localObject2).put("clientRequestId", parambf);
    return (j)localObject1;
  }
  
  final void b(bf parambf)
  {
    Object localObject1 = b;
    Object localObject2 = c;
    localObject1 = ((b)localObject1).c((String)localObject2);
    boolean bool = a;
    if (!bool) {
      return;
    }
    localObject1 = new android/os/Handler;
    localObject2 = Looper.getMainLooper();
    ((Handler)localObject1).<init>((Looper)localObject2);
    localObject2 = new com/inmobi/ads/y$1;
    ((y.1)localObject2).<init>(this, parambf);
    ((Handler)localObject1).post((Runnable)localObject2);
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.y
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */