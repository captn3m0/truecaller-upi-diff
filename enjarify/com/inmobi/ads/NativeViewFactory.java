package com.inmobi.ads;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build.VERSION;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AbsListView;
import android.widget.AbsListView.LayoutParams;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import com.inmobi.commons.core.utilities.b.c;
import com.inmobi.commons.core.utilities.b.d;
import com.inmobi.rendering.CustomView;
import com.inmobi.rendering.InMobiAdActivity;
import com.inmobi.rendering.RenderView;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Stack;

class NativeViewFactory
{
  private static final String a = "NativeViewFactory";
  private static final Map c;
  private static volatile WeakReference e;
  private static WeakReference f;
  private static int g = 1;
  private static int h = 1;
  private int b;
  private Map d;
  
  static
  {
    Object localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    c = (Map)localObject;
    Integer localInteger = Integer.valueOf(0);
    ((Map)localObject).put(at.class, localInteger);
    localObject = c;
    localInteger = Integer.valueOf(1);
    ((Map)localObject).put(bl.class, localInteger);
    localObject = c;
    localInteger = Integer.valueOf(2);
    ((Map)localObject).put(bk.class, localInteger);
    localObject = c;
    localInteger = Integer.valueOf(3);
    ((Map)localObject).put(NativeContainerLayout.class, localInteger);
    localObject = c;
    localInteger = Integer.valueOf(6);
    ((Map)localObject).put(ImageView.class, localInteger);
    localObject = c;
    localInteger = Integer.valueOf(7);
    ((Map)localObject).put(NativeVideoWrapper.class, localInteger);
    localObject = c;
    localInteger = Integer.valueOf(4);
    ((Map)localObject).put(NativeViewFactory.b.class, localInteger);
    localObject = c;
    localInteger = Integer.valueOf(5);
    ((Map)localObject).put(Button.class, localInteger);
    localObject = c;
    localInteger = Integer.valueOf(8);
    ((Map)localObject).put(NativeTimerView.class, localInteger);
    localObject = c;
    localInteger = Integer.valueOf(9);
    ((Map)localObject).put(RenderView.class, localInteger);
    localObject = c;
    localInteger = Integer.valueOf(10);
    ((Map)localObject).put(GifView.class, localInteger);
  }
  
  private NativeViewFactory(Context paramContext)
  {
    Object localObject = new java/lang/ref/WeakReference;
    ((WeakReference)localObject).<init>(paramContext);
    f = (WeakReference)localObject;
    paramContext = new java/util/HashMap;
    paramContext.<init>();
    d = paramContext;
    paramContext = new com/inmobi/ads/NativeViewFactory$1;
    paramContext.<init>(this);
    localObject = d;
    Integer localInteger = Integer.valueOf(0);
    ((Map)localObject).put(localInteger, paramContext);
    paramContext = new com/inmobi/ads/NativeViewFactory$6;
    paramContext.<init>(this);
    localObject = d;
    localInteger = Integer.valueOf(3);
    ((Map)localObject).put(localInteger, paramContext);
    paramContext = new com/inmobi/ads/NativeViewFactory$7;
    paramContext.<init>(this);
    localObject = d;
    localInteger = Integer.valueOf(1);
    ((Map)localObject).put(localInteger, paramContext);
    paramContext = new com/inmobi/ads/NativeViewFactory$8;
    paramContext.<init>(this);
    localObject = d;
    localInteger = Integer.valueOf(2);
    ((Map)localObject).put(localInteger, paramContext);
    paramContext = new com/inmobi/ads/NativeViewFactory$9;
    paramContext.<init>(this);
    localObject = d;
    localInteger = Integer.valueOf(6);
    ((Map)localObject).put(localInteger, paramContext);
    paramContext = new com/inmobi/ads/NativeViewFactory$10;
    paramContext.<init>(this);
    localObject = d;
    localInteger = Integer.valueOf(10);
    ((Map)localObject).put(localInteger, paramContext);
    paramContext = new com/inmobi/ads/NativeViewFactory$11;
    paramContext.<init>(this);
    localObject = d;
    localInteger = Integer.valueOf(7);
    ((Map)localObject).put(localInteger, paramContext);
    paramContext = new com/inmobi/ads/NativeViewFactory$12;
    paramContext.<init>(this);
    localObject = d;
    localInteger = Integer.valueOf(4);
    ((Map)localObject).put(localInteger, paramContext);
    paramContext = new com/inmobi/ads/NativeViewFactory$13;
    paramContext.<init>(this);
    localObject = d;
    localInteger = Integer.valueOf(5);
    ((Map)localObject).put(localInteger, paramContext);
    paramContext = new com/inmobi/ads/NativeViewFactory$2;
    paramContext.<init>(this);
    localObject = d;
    localInteger = Integer.valueOf(8);
    ((Map)localObject).put(localInteger, paramContext);
    paramContext = new com/inmobi/ads/NativeViewFactory$3;
    paramContext.<init>(this);
    localObject = d;
    localInteger = Integer.valueOf(9);
    ((Map)localObject).put(localInteger, paramContext);
  }
  
  public static ViewGroup.LayoutParams a(ag paramag, ViewGroup paramViewGroup)
  {
    Point localPoint = c.a;
    paramag = c.c;
    Object localObject = new android/view/ViewGroup$LayoutParams;
    int i = c(x);
    int j = c(y);
    ((ViewGroup.LayoutParams)localObject).<init>(i, j);
    boolean bool = paramViewGroup instanceof NativeContainerLayout;
    int k;
    int m;
    int n;
    if (bool)
    {
      localObject = new com/inmobi/ads/NativeContainerLayout$a;
      k = c(x);
      m = c(y);
      ((NativeContainerLayout.a)localObject).<init>(k, m);
      paramViewGroup = (ViewGroup)localObject;
      paramViewGroup = (NativeContainerLayout.a)localObject;
      m = c(x);
      n = c(y);
      a = m;
      b = n;
    }
    else
    {
      bool = paramViewGroup instanceof LinearLayout;
      j = 0;
      if (bool)
      {
        localObject = new android/widget/LinearLayout$LayoutParams;
        k = c(x);
        m = c(y);
        ((LinearLayout.LayoutParams)localObject).<init>(k, m);
        paramViewGroup = (ViewGroup)localObject;
        paramViewGroup = (LinearLayout.LayoutParams)localObject;
        m = c(x);
        n = c(y);
        paramViewGroup.setMargins(m, n, 0, 0);
      }
      else
      {
        bool = paramViewGroup instanceof AbsListView;
        if (bool)
        {
          paramag = new android/widget/AbsListView$LayoutParams;
          k = c(x);
          m = c(y);
          paramag.<init>(k, m);
          return paramag;
        }
        bool = paramViewGroup instanceof FrameLayout;
        if (bool)
        {
          localObject = new android/widget/FrameLayout$LayoutParams;
          k = c(x);
          m = c(y);
          ((FrameLayout.LayoutParams)localObject).<init>(k, m);
          paramViewGroup = (ViewGroup)localObject;
          paramViewGroup = (FrameLayout.LayoutParams)localObject;
          m = c(x);
          n = c(y);
          paramViewGroup.setMargins(m, n, 0, 0);
        }
        else
        {
          paramag = paramViewGroup.getClass();
          paramag.getSimpleName();
        }
      }
    }
    return (ViewGroup.LayoutParams)localObject;
  }
  
  public static NativeViewFactory a(Context paramContext)
  {
    Object localObject1 = e;
    Object localObject2 = null;
    if (localObject1 == null) {
      localObject1 = null;
    } else {
      localObject1 = (NativeViewFactory)e.get();
    }
    if (localObject1 == null) {
      synchronized (NativeViewFactory.class)
      {
        localObject1 = e;
        if (localObject1 != null)
        {
          localObject1 = e;
          localObject1 = ((WeakReference)localObject1).get();
          localObject2 = localObject1;
          localObject2 = (NativeViewFactory)localObject1;
        }
        if (localObject2 == null)
        {
          localObject1 = new com/inmobi/ads/NativeViewFactory;
          ((NativeViewFactory)localObject1).<init>(paramContext);
          paramContext = new java/lang/ref/WeakReference;
          paramContext.<init>(localObject1);
          e = paramContext;
        }
        else
        {
          localObject1 = localObject2;
        }
      }
    }
    return (NativeViewFactory)localObject1;
  }
  
  static void a(int paramInt)
  {
    g = paramInt;
  }
  
  static void a(View paramView, ah paramah)
  {
    String str1 = "#00000000";
    int i = Color.parseColor(str1);
    com.inmobi.commons.core.e.a locala;
    try
    {
      String str2 = paramah.e();
      i = Color.parseColor(str2);
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      localObject2 = com.inmobi.commons.core.a.a.a();
      locala = new com/inmobi/commons/core/e/a;
      locala.<init>(localIllegalArgumentException);
      ((com.inmobi.commons.core.a.a)localObject2).a(locala);
    }
    paramView.setBackgroundColor(i);
    Object localObject1 = "line";
    Object localObject2 = paramah.a();
    if (localObject1 == localObject2)
    {
      localObject1 = new android/graphics/drawable/GradientDrawable;
      ((GradientDrawable)localObject1).<init>();
      ((GradientDrawable)localObject1).setColor(i);
      str1 = "curved";
      localObject2 = paramah.b();
      if (str1 == localObject2)
      {
        f1 = paramah.c();
        ((GradientDrawable)localObject1).setCornerRadius(f1);
      }
      str1 = "#ff000000";
      i = Color.parseColor(str1);
      try
      {
        paramah = paramah.d();
        i = Color.parseColor(paramah);
      }
      catch (IllegalArgumentException paramah)
      {
        localObject2 = com.inmobi.commons.core.a.a.a();
        locala = new com/inmobi/commons/core/e/a;
        locala.<init>(paramah);
        ((com.inmobi.commons.core.a.a)localObject2).a(locala);
      }
      ((GradientDrawable)localObject1).setStroke(1, i);
      int j = Build.VERSION.SDK_INT;
      i = 16;
      float f1 = 2.24E-44F;
      if (j < i)
      {
        paramView.setBackgroundDrawable((Drawable)localObject1);
        return;
      }
      paramView.setBackground((Drawable)localObject1);
    }
  }
  
  private static void a(TextView paramTextView, String[] paramArrayOfString)
  {
    int i = paramTextView.getPaintFlags();
    int j = paramArrayOfString.length;
    int k = i;
    i = 0;
    int m = 0;
    while (i < j)
    {
      String str1 = paramArrayOfString[i];
      int n = str1.hashCode();
      int i1 = -1178781136;
      String str2;
      if (n != i1)
      {
        i1 = -1026963764;
        if (n != i1)
        {
          i1 = -891985998;
          boolean bool1;
          if (n != i1)
          {
            i1 = 3029637;
            if (n == i1)
            {
              str2 = "bold";
              bool1 = str1.equals(str2);
              if (bool1)
              {
                bool1 = false;
                str1 = null;
                break label190;
              }
            }
          }
          else
          {
            str2 = "strike";
            bool1 = str1.equals(str2);
            if (bool1)
            {
              int i2 = 2;
              break label190;
            }
          }
        }
        else
        {
          str2 = "underline";
          boolean bool2 = str1.equals(str2);
          if (bool2)
          {
            int i3 = 3;
            break label190;
          }
        }
      }
      else
      {
        str2 = "italic";
        boolean bool3 = str1.equals(str2);
        if (bool3)
        {
          bool3 = true;
          break label190;
        }
      }
      int i4 = -1;
      switch (i4)
      {
      default: 
        break;
      case 3: 
        k |= 0x8;
        break;
      case 2: 
        k |= 0x10;
        break;
      case 1: 
        m |= 0x2;
        break;
      case 0: 
        label190:
        m |= 0x1;
      }
      i += 1;
    }
    paramArrayOfString = Typeface.DEFAULT;
    paramTextView.setTypeface(paramArrayOfString, m);
    paramTextView.setPaintFlags(k);
  }
  
  private static Button b(Button paramButton, ag paramag)
  {
    aj.a locala = (aj.a)c;
    Object localObject1 = new android/view/ViewGroup$LayoutParams;
    Object localObject2 = a;
    int i = c(x);
    Object localObject3 = a;
    int j = c(y);
    ((ViewGroup.LayoutParams)localObject1).<init>(i, j);
    paramButton.setLayoutParams((ViewGroup.LayoutParams)localObject1);
    paramag = (CharSequence)e;
    paramButton.setText(paramag);
    float f1 = c(locala.h());
    int k = 1;
    paramButton.setTextSize(k, f1);
    paramag = "#ff000000";
    int m = Color.parseColor(paramag);
    try
    {
      localObject1 = locala.i();
      m = Color.parseColor((String)localObject1);
    }
    catch (IllegalArgumentException localIllegalArgumentException1)
    {
      localObject2 = com.inmobi.commons.core.a.a.a();
      localObject3 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject3).<init>(localIllegalArgumentException1);
      ((com.inmobi.commons.core.a.a)localObject2).a((com.inmobi.commons.core.e.a)localObject3);
    }
    paramButton.setTextColor(m);
    paramag = "#00000000";
    m = Color.parseColor(paramag);
    try
    {
      String str = locala.e();
      m = Color.parseColor(str);
    }
    catch (IllegalArgumentException localIllegalArgumentException2)
    {
      localObject2 = com.inmobi.commons.core.a.a.a();
      localObject3 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject3).<init>(localIllegalArgumentException2);
      ((com.inmobi.commons.core.a.a)localObject2).a((com.inmobi.commons.core.e.a)localObject3);
    }
    paramButton.setBackgroundColor(m);
    m = Build.VERSION.SDK_INT;
    k = 17;
    if (m >= k)
    {
      m = 4;
      f1 = 5.6E-45F;
      paramButton.setTextAlignment(m);
    }
    paramButton.setGravity(k);
    paramag = locala.j();
    a(paramButton, paramag);
    a(paramButton, locala);
    return paramButton;
  }
  
  private NativeViewFactory.c b()
  {
    Iterator localIterator = d.entrySet().iterator();
    int i = 0;
    NativeViewFactory.c localc1 = null;
    int j = 0;
    NativeViewFactory.c localc2 = null;
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      LinkedList localLinkedList = getValueb;
      int k = localLinkedList.size();
      if (k > i)
      {
        localc1 = (NativeViewFactory.c)localEntry.getValue();
        j = b.size();
        localc2 = localc1;
        i = j;
      }
    }
    return localc2;
  }
  
  static void b(int paramInt)
  {
    h = paramInt;
  }
  
  private static void b(Context paramContext, ImageView paramImageView)
  {
    Object localObject = paramImageView.getDrawable();
    if (localObject == null)
    {
      localObject = c.a();
      float f1 = c;
      CustomView localCustomView = new com/inmobi/rendering/CustomView;
      localCustomView.<init>(paramContext, f1, 0);
      int i = 40;
      float f2 = c(i) * f1;
      int j = (int)f2;
      i = (int)(c(i) * f1);
      localCustomView.layout(0, 0, j, i);
      i = 1;
      localCustomView.setDrawingCacheEnabled(i);
      localCustomView.buildDrawingCache();
      paramContext = localCustomView.getDrawingCache();
      paramImageView.setImageBitmap(paramContext);
    }
  }
  
  static int c(int paramInt)
  {
    Context localContext = (Context)f.get();
    if (localContext != null)
    {
      boolean bool = localContext instanceof InMobiAdActivity;
      if (bool) {
        return paramInt;
      }
    }
    int i = g;
    if (i == 0) {
      return paramInt;
    }
    double d1 = paramInt;
    double d2 = i;
    Double.isNaN(d2);
    d2 *= 1.0D;
    double d3 = h;
    Double.isNaN(d3);
    d2 /= d3;
    Double.isNaN(d1);
    return (int)(d1 * d2);
  }
  
  private void c(View paramView)
  {
    Object localObject1 = c;
    Object localObject2 = paramView.getClass();
    localObject1 = (Integer)((Map)localObject1).get(localObject2);
    int i = ((Integer)localObject1).intValue();
    int j = -1;
    if (j == i) {
      return;
    }
    localObject2 = d;
    localObject1 = Integer.valueOf(i);
    localObject1 = (NativeViewFactory.c)((Map)localObject2).get(localObject1);
    if (localObject1 == null) {
      return;
    }
    j = b;
    int k = 300;
    if (j >= k)
    {
      localObject2 = b();
      if (localObject2 != null)
      {
        LinkedList localLinkedList = b;
        k = localLinkedList.size();
        if (k > 0)
        {
          localObject2 = b;
          ((LinkedList)localObject2).removeFirst();
        }
      }
    }
    ((NativeViewFactory.c)localObject1).a(paramView);
  }
  
  public final View a(Context paramContext, ag paramag, b paramb)
  {
    boolean bool1 = paramag instanceof ai;
    int j = 3;
    int k = 5;
    int m = 1;
    int n = -1;
    if (bool1)
    {
      localObject1 = paramag;
      localObject1 = (ai)paramag;
      String str1 = "root";
      String str2 = d;
      boolean bool3 = str1.equalsIgnoreCase(str2);
      if (bool3)
      {
        j = 0;
        localObject2 = null;
      }
      else
      {
        str1 = "card_scrollable";
        str2 = d;
        bool3 = str1.equalsIgnoreCase(str2);
        if (bool3)
        {
          int i = A;
          if (i != m) {
            j = 1;
          } else {
            j = 2;
          }
        }
      }
    }
    else
    {
      localObject1 = b;
      int i1 = ((String)localObject1).hashCode();
      boolean bool2;
      switch (i1)
      {
      default: 
        break;
      case 1942407129: 
        localObject2 = "WEBVIEW";
        bool2 = ((String)localObject1).equals(localObject2);
        if (bool2) {
          j = 6;
        }
        break;
      case 81665115: 
        String str3 = "VIDEO";
        bool2 = ((String)localObject1).equals(str3);
        if (!bool2) {
          break;
        }
        break;
      case 79826725: 
        localObject2 = "TIMER";
        bool2 = ((String)localObject1).equals(localObject2);
        if (bool2) {
          j = 5;
        }
        break;
      case 69775675: 
        localObject2 = "IMAGE";
        bool2 = ((String)localObject1).equals(localObject2);
        if (bool2) {
          j = 1;
        }
        break;
      case 2571565: 
        localObject2 = "TEXT";
        bool2 = ((String)localObject1).equals(localObject2);
        if (bool2)
        {
          j = 0;
          localObject2 = null;
        }
        break;
      case 2241657: 
        localObject2 = "ICON";
        bool2 = ((String)localObject1).equals(localObject2);
        if (bool2) {
          j = 2;
        }
        break;
      case 70564: 
        localObject2 = "GIF";
        bool2 = ((String)localObject1).equals(localObject2);
        if (bool2) {
          j = 7;
        }
        break;
      case 67056: 
        localObject2 = "CTA";
        bool2 = ((String)localObject1).equals(localObject2);
        if (bool2) {
          j = 4;
        }
        break;
      }
      j = -1;
      switch (j)
      {
      default: 
        j = -1;
        break;
      case 7: 
        j = 10;
        break;
      case 6: 
        j = 9;
        break;
      case 5: 
        j = 8;
        break;
      case 4: 
        j = 5;
        break;
      case 3: 
        j = 7;
        break;
      case 1: 
      case 2: 
        j = 6;
        break;
      case 0: 
        j = 4;
      }
    }
    if (n == j) {
      return null;
    }
    Object localObject1 = d;
    Object localObject2 = Integer.valueOf(j);
    return ((NativeViewFactory.c)((Map)localObject1).get(localObject2)).a(paramContext, paramag, paramb);
  }
  
  public final void a(View paramView)
  {
    boolean bool1 = paramView instanceof at;
    if (!bool1)
    {
      bool1 = paramView instanceof NativeContainerLayout;
      if (!bool1) {}
    }
    else
    {
      Object localObject1 = paramView;
      localObject1 = (NativeContainerLayout)paramView;
      int i = ((NativeContainerLayout)localObject1).getChildCount();
      if (i != 0)
      {
        paramView = new java/util/Stack;
        paramView.<init>();
        paramView.push(localObject1);
        for (;;)
        {
          bool1 = paramView.isEmpty();
          if (bool1) {
            break;
          }
          localObject1 = (NativeContainerLayout)paramView.pop();
          i = ((NativeContainerLayout)localObject1).getChildCount() + -1;
          while (i >= 0)
          {
            Object localObject2 = ((NativeContainerLayout)localObject1).getChildAt(i);
            ((NativeContainerLayout)localObject1).removeViewAt(i);
            boolean bool2 = localObject2 instanceof NativeContainerLayout;
            if (bool2)
            {
              localObject2 = (NativeContainerLayout)localObject2;
              paramView.push(localObject2);
            }
            else
            {
              c((View)localObject2);
            }
            i += -1;
          }
          c((View)localObject1);
        }
        return;
      }
    }
    c((View)paramView);
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.NativeViewFactory
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */