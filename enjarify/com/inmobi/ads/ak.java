package com.inmobi.ads;

import android.graphics.Point;
import android.webkit.URLUtil;
import com.inmobi.commons.core.utilities.b.c;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ak
{
  private static final String l = "ak";
  int a;
  public boolean b;
  public boolean c;
  public ai d;
  JSONArray e;
  final ak f;
  Map g;
  Map h;
  ak.a i;
  boolean j;
  bc k;
  private JSONObject m;
  private JSONObject n;
  private Map o;
  private Map p;
  private bt q;
  private b r;
  private AdContainer.RenderingProperties.PlacementType s;
  private boolean t;
  
  ak()
  {
    f = null;
  }
  
  public ak(AdContainer.RenderingProperties.PlacementType paramPlacementType, JSONObject paramJSONObject, ak paramak, boolean paramBoolean, b paramb, bt parambt)
  {
    this(paramPlacementType, paramJSONObject, paramak, paramBoolean, paramb, parambt, (byte)0);
  }
  
  private ak(AdContainer.RenderingProperties.PlacementType paramPlacementType, JSONObject paramJSONObject, ak paramak, boolean paramBoolean, b paramb, bt parambt, byte paramByte)
  {
    s = paramPlacementType;
    f = paramak;
    if (paramb == null)
    {
      paramb = new com/inmobi/ads/b;
      paramb.<init>();
    }
    r = paramb;
    m = paramJSONObject;
    a = 0;
    b = false;
    q = parambt;
    paramPlacementType = new java/util/HashMap;
    paramPlacementType.<init>();
    o = paramPlacementType;
    paramPlacementType = new java/util/HashMap;
    paramPlacementType.<init>();
    p = paramPlacementType;
    paramPlacementType = new java/util/HashMap;
    paramPlacementType.<init>();
    h = paramPlacementType;
    paramPlacementType = new com/inmobi/ads/ak$a;
    paramPlacementType.<init>(this);
    i = paramPlacementType;
    t = paramBoolean;
    f();
  }
  
  public ak(AdContainer.RenderingProperties.PlacementType paramPlacementType, JSONObject paramJSONObject, b paramb, bt parambt)
  {
    this(paramPlacementType, paramJSONObject, null, false, paramb, parambt);
  }
  
  static int a(String paramString)
  {
    Object localObject = Locale.US;
    paramString = paramString.toLowerCase((Locale)localObject).trim();
    int i1 = paramString.hashCode();
    int i2 = -1412832500;
    int i3 = 2;
    int i4 = 1;
    boolean bool2;
    if (i1 != i2)
    {
      if (i1 != 0)
      {
        i2 = 112202875;
        if (i1 == i2)
        {
          localObject = "video";
          boolean bool1 = paramString.equals(localObject);
          if (bool1)
          {
            int i5 = 2;
            break label113;
          }
        }
      }
      else
      {
        localObject = "";
        bool2 = paramString.equals(localObject);
        if (bool2)
        {
          bool2 = true;
          break label113;
        }
      }
    }
    else
    {
      localObject = "companion";
      bool2 = paramString.equals(localObject);
      if (bool2)
      {
        i6 = 3;
        break label113;
      }
    }
    int i6 = -1;
    switch (i6)
    {
    default: 
      return 0;
    case 3: 
      label113:
      return i3;
    }
    return i4;
  }
  
  private static int a(JSONObject paramJSONObject, boolean paramBoolean)
  {
    int i1 = -1;
    try
    {
      localObject1 = n(paramJSONObject);
      String str;
      if (paramBoolean) {
        str = "delay";
      } else {
        str = "hideAfterDelay";
      }
      boolean bool1 = ((JSONObject)localObject1).isNull(str);
      if (bool1) {
        return i1;
      }
      if (paramBoolean) {
        localObject2 = "delay";
      } else {
        localObject2 = "hideAfterDelay";
      }
      paramBoolean = ((JSONObject)localObject1).getInt((String)localObject2);
      boolean bool4 = l(paramJSONObject);
      boolean bool2 = true;
      if (bool2 == bool4) {
        return paramBoolean;
      }
      boolean bool5 = l(paramJSONObject);
      int i3 = 4;
      if (i3 == bool5)
      {
        if (paramBoolean)
        {
          bool5 = true;
          if (paramBoolean <= bool5)
          {
            int[] arrayOfInt = new int[i3];
            int i5 = 25;
            int i6 = 0;
            arrayOfInt[0] = i5;
            i5 = 50;
            int i7 = 1;
            arrayOfInt[i7] = i5;
            i5 = 2;
            i7 = 75;
            arrayOfInt[i5] = i7;
            arrayOfInt[bool2] = bool5;
            double d1 = Double.MAX_VALUE;
            int i4 = -1;
            while (i6 < i3)
            {
              bool2 = arrayOfInt[i6];
              int i2 = paramBoolean - bool2;
              double d2 = i2 * i2;
              boolean bool3 = d2 < d1;
              if (bool3)
              {
                i4 = i6;
                d1 = d2;
              }
              i6 += 1;
            }
            return arrayOfInt[i4];
          }
        }
        return i1;
      }
      return i1;
    }
    catch (JSONException paramJSONObject)
    {
      Object localObject2 = com.inmobi.commons.core.a.a.a();
      Object localObject1 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject1).<init>(paramJSONObject);
      ((com.inmobi.commons.core.a.a)localObject2).a((com.inmobi.commons.core.e.a)localObject1);
    }
    return i1;
  }
  
  /* Error */
  private Point a(JSONObject paramJSONObject, Point paramPoint)
  {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: invokespecial 170	com/inmobi/ads/ak:i	(Lorg/json/JSONObject;)Lorg/json/JSONObject;
    //   5: astore_1
    //   6: ldc -84
    //   8: astore_3
    //   9: aload_1
    //   10: aload_3
    //   11: invokevirtual 137	org/json/JSONObject:isNull	(Ljava/lang/String;)Z
    //   14: istore 4
    //   16: iload 4
    //   18: ifeq +5 -> 23
    //   21: aload_2
    //   22: areturn
    //   23: new 174	android/graphics/Point
    //   26: astore_2
    //   27: aload_2
    //   28: invokespecial 175	android/graphics/Point:<init>	()V
    //   31: ldc -84
    //   33: astore_3
    //   34: aload_1
    //   35: aload_3
    //   36: invokevirtual 179	org/json/JSONObject:getJSONArray	(Ljava/lang/String;)Lorg/json/JSONArray;
    //   39: astore_1
    //   40: iconst_0
    //   41: istore 4
    //   43: aconst_null
    //   44: astore_3
    //   45: aload_1
    //   46: iconst_0
    //   47: invokevirtual 184	org/json/JSONArray:getInt	(I)I
    //   50: istore 4
    //   52: iload 4
    //   54: invokestatic 188	com/inmobi/commons/core/utilities/b/c:a	(I)I
    //   57: istore 4
    //   59: aload_2
    //   60: iload 4
    //   62: putfield 191	android/graphics/Point:x	I
    //   65: iconst_1
    //   66: istore 4
    //   68: aload_1
    //   69: iload 4
    //   71: invokevirtual 184	org/json/JSONArray:getInt	(I)I
    //   74: istore 5
    //   76: iload 5
    //   78: invokestatic 188	com/inmobi/commons/core/utilities/b/c:a	(I)I
    //   81: istore 5
    //   83: aload_2
    //   84: iload 5
    //   86: putfield 194	android/graphics/Point:y	I
    //   89: goto +6 -> 95
    //   92: pop
    //   93: aconst_null
    //   94: astore_2
    //   95: aload_2
    //   96: areturn
    //   97: pop
    //   98: goto -3 -> 95
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	101	0	this	ak
    //   0	101	1	paramJSONObject	JSONObject
    //   0	101	2	paramPoint	Point
    //   8	37	3	str	String
    //   14	28	4	bool	boolean
    //   50	20	4	i1	int
    //   74	11	5	i2	int
    //   92	1	7	localJSONException1	JSONException
    //   97	1	8	localJSONException2	JSONException
    // Exception table:
    //   from	to	target	type
    //   1	5	92	org/json/JSONException
    //   10	14	92	org/json/JSONException
    //   23	26	92	org/json/JSONException
    //   27	31	92	org/json/JSONException
    //   35	39	97	org/json/JSONException
    //   46	50	97	org/json/JSONException
    //   52	57	97	org/json/JSONException
    //   60	65	97	org/json/JSONException
    //   69	74	97	org/json/JSONException
    //   76	81	97	org/json/JSONException
    //   84	89	97	org/json/JSONException
  }
  
  private static NativeTracker a(int paramInt, NativeTracker.TrackerEventType paramTrackerEventType, JSONObject paramJSONObject)
  {
    String str = "url";
    boolean bool1 = paramJSONObject.isNull(str);
    if (bool1) {
      str = "";
    } else {
      str = paramJSONObject.getString("url").trim();
    }
    HashMap localHashMap1 = new java/util/HashMap;
    localHashMap1.<init>();
    Object localObject1 = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_VIDEO_RENDER;
    boolean bool2 = false;
    Object localObject2 = null;
    Object localObject3;
    label129:
    Object localObject4;
    if (localObject1 == paramTrackerEventType)
    {
      localObject1 = paramJSONObject.optJSONArray("events");
      int i1 = str.length();
      int i2;
      if (i1 != 0)
      {
        localObject3 = "http";
        i2 = str.startsWith((String)localObject3);
        if (i2 != 0)
        {
          i2 = URLUtil.isValidUrl(str);
          if (i2 == 0) {}
        }
        else
        {
          localObject3 = "http";
          i2 = str.startsWith((String)localObject3);
          if (i2 != 0) {
            break label129;
          }
        }
      }
      if (localObject1 == null) {
        return null;
      }
      localObject2 = new java/util/ArrayList;
      ((ArrayList)localObject2).<init>();
      if (localObject1 != null)
      {
        i2 = 0;
        localObject3 = null;
        for (;;)
        {
          int i4 = ((JSONArray)localObject1).length();
          if (i2 >= i4) {
            break;
          }
          localObject4 = NativeTracker.a(((JSONArray)localObject1).getString(i2));
          NativeTracker.TrackerEventType localTrackerEventType = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_CREATIVE_VIEW;
          if (localObject4 != localTrackerEventType)
          {
            localTrackerEventType = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_PLAY;
            if (localObject4 != localTrackerEventType)
            {
              localTrackerEventType = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_RENDER;
              if (localObject4 != localTrackerEventType) {
                break label222;
              }
            }
          }
          ((List)localObject2).add(localObject4);
          label222:
          int i3;
          i2 += 1;
        }
      }
      localObject1 = "referencedEvents";
      localHashMap1.put(localObject1, localObject2);
    }
    else
    {
      int i5 = str.length();
      if (i5 == 0) {
        break label431;
      }
      boolean bool4 = URLUtil.isValidUrl(str);
      if (!bool4) {
        break label431;
      }
    }
    localObject1 = new java/util/HashMap;
    ((HashMap)localObject1).<init>();
    localObject2 = "params";
    try
    {
      bool2 = paramJSONObject.isNull((String)localObject2);
      if (!bool2)
      {
        localObject2 = "params";
        paramJSONObject = paramJSONObject.getJSONObject((String)localObject2);
        localObject2 = paramJSONObject.keys();
        for (;;)
        {
          boolean bool3 = ((Iterator)localObject2).hasNext();
          if (!bool3) {
            break;
          }
          localObject3 = ((Iterator)localObject2).next();
          localObject3 = (String)localObject3;
          localObject4 = paramJSONObject.getString((String)localObject3);
          ((Map)localObject1).put(localObject3, localObject4);
        }
      }
      HashMap localHashMap2;
      return null;
    }
    catch (JSONException paramJSONObject)
    {
      localObject2 = com.inmobi.commons.core.a.a.a();
      localObject3 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject3).<init>(paramJSONObject);
      ((com.inmobi.commons.core.a.a)localObject2).a((com.inmobi.commons.core.e.a)localObject3);
      paramJSONObject = new com/inmobi/ads/NativeTracker;
      paramJSONObject.<init>(str, paramInt, paramTrackerEventType, (Map)localObject1);
      localHashMap2 = new java/util/HashMap;
      localHashMap2.<init>(localHashMap1);
      d = localHashMap2;
      return paramJSONObject;
    }
  }
  
  private static ag a(ak paramak, ag paramag)
  {
    Object localObject1;
    Object localObject2;
    for (;;)
    {
      localObject1 = (String)e;
      if (localObject1 == null) {
        break label118;
      }
      int i1 = ((String)localObject1).length();
      if (i1 == 0) {
        break label118;
      }
      localObject1 = ((String)localObject1).split("\\|");
      i1 = 0;
      localObject2 = localObject1[0];
      localObject2 = paramak.b((String)localObject2);
      if (localObject2 != null) {
        break;
      }
      paramak = f;
    }
    boolean bool = localObject2.equals(paramag);
    if (bool) {
      return null;
    }
    int i2 = localObject1.length;
    int i3 = 1;
    if (i3 == i2)
    {
      m = i3;
      return (ag)localObject2;
    }
    i2 = a(localObject1[i3]);
    m = i2;
    return (ag)localObject2;
    label118:
    return null;
  }
  
  /* Error */
  private ag a(JSONObject paramJSONObject, String paramString1, String paramString2)
  {
    // Byte code:
    //   0: aload_0
    //   1: astore 4
    //   3: aload_1
    //   4: astore 5
    //   6: aload_2
    //   7: astore 6
    //   9: aload_3
    //   10: astore 7
    //   12: aload_1
    //   13: invokestatic 308	com/inmobi/ads/ak:d	(Lorg/json/JSONObject;)Ljava/lang/String;
    //   16: astore 8
    //   18: aload_1
    //   19: invokestatic 310	com/inmobi/ads/ak:e	(Lorg/json/JSONObject;)Ljava/lang/String;
    //   22: astore 9
    //   24: aload_0
    //   25: aload_1
    //   26: invokespecial 170	com/inmobi/ads/ak:i	(Lorg/json/JSONObject;)Lorg/json/JSONObject;
    //   29: astore 10
    //   31: aload 10
    //   33: aload_2
    //   34: invokestatic 313	com/inmobi/ads/ak:a	(Lorg/json/JSONObject;Ljava/lang/String;)Z
    //   37: istore 11
    //   39: aconst_null
    //   40: astore 12
    //   42: iload 11
    //   44: ifne +5 -> 49
    //   47: aconst_null
    //   48: areturn
    //   49: aload_0
    //   50: aload_1
    //   51: invokespecial 316	com/inmobi/ads/ak:j	(Lorg/json/JSONObject;)Landroid/graphics/Point;
    //   54: astore 13
    //   56: aload_0
    //   57: aload_1
    //   58: aload 13
    //   60: invokespecial 319	com/inmobi/ads/ak:a	(Lorg/json/JSONObject;Landroid/graphics/Point;)Landroid/graphics/Point;
    //   63: astore 14
    //   65: aload_0
    //   66: aload_1
    //   67: invokespecial 321	com/inmobi/ads/ak:k	(Lorg/json/JSONObject;)Landroid/graphics/Point;
    //   70: astore 15
    //   72: aload_0
    //   73: aload_1
    //   74: aload 15
    //   76: invokespecial 323	com/inmobi/ads/ak:b	(Lorg/json/JSONObject;Landroid/graphics/Point;)Landroid/graphics/Point;
    //   79: astore 16
    //   81: aload_1
    //   82: invokestatic 326	com/inmobi/ads/ak:b	(Lorg/json/JSONObject;)Ljava/util/List;
    //   85: astore 17
    //   87: aload_1
    //   88: invokestatic 144	com/inmobi/ads/ak:l	(Lorg/json/JSONObject;)I
    //   91: istore 18
    //   93: aload_1
    //   94: iconst_1
    //   95: invokestatic 329	com/inmobi/ads/ak:a	(Lorg/json/JSONObject;Z)I
    //   98: istore 19
    //   100: aload_1
    //   101: iconst_0
    //   102: invokestatic 329	com/inmobi/ads/ak:a	(Lorg/json/JSONObject;Z)I
    //   105: istore 20
    //   107: aload_1
    //   108: invokestatic 331	com/inmobi/ads/ak:m	(Lorg/json/JSONObject;)Ljava/lang/String;
    //   111: astore 21
    //   113: ldc 121
    //   115: astore 22
    //   117: aload_1
    //   118: invokestatic 333	com/inmobi/ads/ak:g	(Lorg/json/JSONObject;)Ljava/lang/String;
    //   121: invokevirtual 103	java/lang/String:trim	()Ljava/lang/String;
    //   124: astore 23
    //   126: aload 21
    //   128: astore 24
    //   130: aload 23
    //   132: invokevirtual 107	java/lang/String:hashCode	()I
    //   135: istore 25
    //   137: iload 19
    //   139: istore 26
    //   141: ldc_w 334
    //   144: istore 19
    //   146: iconst_m1
    //   147: istore 27
    //   149: iconst_2
    //   150: istore 28
    //   152: iload 25
    //   154: iload 19
    //   156: if_icmpeq +43 -> 199
    //   159: ldc_w 336
    //   162: istore 19
    //   164: iload 25
    //   166: iload 19
    //   168: if_icmpeq +6 -> 174
    //   171: goto +53 -> 224
    //   174: ldc_w 339
    //   177: astore 21
    //   179: aload 23
    //   181: aload 21
    //   183: invokevirtual 119	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   186: istore 11
    //   188: iload 11
    //   190: ifeq +34 -> 224
    //   193: iconst_1
    //   194: istore 11
    //   196: goto +31 -> 227
    //   199: ldc_w 341
    //   202: astore 21
    //   204: aload 23
    //   206: aload 21
    //   208: invokevirtual 119	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   211: istore 11
    //   213: iload 11
    //   215: ifeq +9 -> 224
    //   218: iconst_2
    //   219: istore 11
    //   221: goto +6 -> 227
    //   224: iconst_m1
    //   225: istore 11
    //   227: iload 11
    //   229: iload 28
    //   231: if_icmpeq +12 -> 243
    //   234: iconst_0
    //   235: istore 11
    //   237: aconst_null
    //   238: astore 23
    //   240: goto +6 -> 246
    //   243: iconst_1
    //   244: istore 11
    //   246: aload_1
    //   247: invokestatic 344	com/inmobi/ads/ak:o	(Lorg/json/JSONObject;)Lorg/json/JSONArray;
    //   250: astore 21
    //   252: aload 21
    //   254: ifnull +179 -> 433
    //   257: aload 21
    //   259: invokevirtual 228	org/json/JSONArray:length	()I
    //   262: istore 19
    //   264: iload 19
    //   266: ifeq +167 -> 433
    //   269: iconst_0
    //   270: istore 19
    //   272: aconst_null
    //   273: astore 29
    //   275: aload 21
    //   277: iconst_0
    //   278: invokevirtual 231	org/json/JSONArray:getString	(I)Ljava/lang/String;
    //   281: astore 21
    //   283: aload 21
    //   285: invokestatic 350	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   288: istore 19
    //   290: iload 19
    //   292: ifeq +5 -> 297
    //   295: aconst_null
    //   296: areturn
    //   297: iconst_1
    //   298: istore 19
    //   300: iload 11
    //   302: iload 19
    //   304: if_icmpne +60 -> 364
    //   307: aload 4
    //   309: aload 21
    //   311: invokevirtual 300	com/inmobi/ads/ak:b	(Ljava/lang/String;)Lcom/inmobi/ads/ag;
    //   314: astore 29
    //   316: aload 29
    //   318: ifnonnull +39 -> 357
    //   321: aload 4
    //   323: getfield 46	com/inmobi/ads/ak:f	Lcom/inmobi/ads/ak;
    //   326: astore 23
    //   328: aload 23
    //   330: ifnull +27 -> 357
    //   333: aload 4
    //   335: getfield 46	com/inmobi/ads/ak:f	Lcom/inmobi/ads/ak;
    //   338: astore 23
    //   340: aload 23
    //   342: aload 21
    //   344: invokevirtual 300	com/inmobi/ads/ak:b	(Ljava/lang/String;)Lcom/inmobi/ads/ag;
    //   347: astore 23
    //   349: goto +21 -> 370
    //   352: astore 23
    //   354: goto +44 -> 398
    //   357: aload 29
    //   359: astore 23
    //   361: goto +9 -> 370
    //   364: iconst_0
    //   365: istore 11
    //   367: aconst_null
    //   368: astore 23
    //   370: aload 23
    //   372: astore 29
    //   374: aload 21
    //   376: astore 23
    //   378: goto +65 -> 443
    //   381: astore 23
    //   383: goto +9 -> 392
    //   386: astore 23
    //   388: aload 22
    //   390: astore 21
    //   392: iconst_0
    //   393: istore 19
    //   395: aconst_null
    //   396: astore 29
    //   398: invokestatic 158	com/inmobi/commons/core/a/a:a	()Lcom/inmobi/commons/core/a/a;
    //   401: astore 7
    //   403: aload 21
    //   405: astore 22
    //   407: new 160	com/inmobi/commons/core/e/a
    //   410: astore 21
    //   412: aload 21
    //   414: aload 23
    //   416: invokespecial 163	com/inmobi/commons/core/e/a:<init>	(Ljava/lang/Throwable;)V
    //   419: aload 7
    //   421: aload 21
    //   423: invokevirtual 166	com/inmobi/commons/core/a/a:a	(Lcom/inmobi/commons/core/e/a;)V
    //   426: aload 22
    //   428: astore 23
    //   430: goto +13 -> 443
    //   433: aload 22
    //   435: astore 23
    //   437: iconst_0
    //   438: istore 19
    //   440: aconst_null
    //   441: astore 29
    //   443: aload_2
    //   444: invokevirtual 107	java/lang/String:hashCode	()I
    //   447: istore 25
    //   449: bipush 6
    //   451: istore 30
    //   453: iload 25
    //   455: lookupswitch	default:+89->544, -1919329183:+321->776, -1884772963:+295->750, 67056:+269->724, 70564:+244->699, 2241657:+219->674, 2571565:+194->649, 69775675:+169->624, 79826725:+144->599, 81665115:+118->573, 1942407129:+92->547
    //   544: goto +260 -> 804
    //   547: ldc_w 357
    //   550: astore 21
    //   552: aload 6
    //   554: aload 21
    //   556: invokevirtual 119	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   559: istore 25
    //   561: iload 25
    //   563: ifeq +241 -> 804
    //   566: bipush 6
    //   568: istore 25
    //   570: goto +237 -> 807
    //   573: ldc_w 359
    //   576: astore 21
    //   578: aload 6
    //   580: aload 21
    //   582: invokevirtual 119	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   585: istore 25
    //   587: iload 25
    //   589: ifeq +215 -> 804
    //   592: bipush 7
    //   594: istore 25
    //   596: goto +211 -> 807
    //   599: ldc_w 361
    //   602: astore 21
    //   604: aload 6
    //   606: aload 21
    //   608: invokevirtual 119	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   611: istore 25
    //   613: iload 25
    //   615: ifeq +189 -> 804
    //   618: iconst_3
    //   619: istore 25
    //   621: goto +186 -> 807
    //   624: ldc_w 363
    //   627: astore 21
    //   629: aload 6
    //   631: aload 21
    //   633: invokevirtual 119	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   636: istore 25
    //   638: iload 25
    //   640: ifeq +164 -> 804
    //   643: iconst_4
    //   644: istore 25
    //   646: goto +161 -> 807
    //   649: ldc_w 365
    //   652: astore 21
    //   654: aload 6
    //   656: aload 21
    //   658: invokevirtual 119	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   661: istore 25
    //   663: iload 25
    //   665: ifeq +139 -> 804
    //   668: iconst_1
    //   669: istore 25
    //   671: goto +136 -> 807
    //   674: ldc_w 367
    //   677: astore 21
    //   679: aload 6
    //   681: aload 21
    //   683: invokevirtual 119	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   686: istore 25
    //   688: iload 25
    //   690: ifeq +114 -> 804
    //   693: iconst_2
    //   694: istore 25
    //   696: goto +111 -> 807
    //   699: ldc_w 369
    //   702: astore 21
    //   704: aload 6
    //   706: aload 21
    //   708: invokevirtual 119	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   711: istore 25
    //   713: iload 25
    //   715: ifeq +89 -> 804
    //   718: iconst_5
    //   719: istore 25
    //   721: goto +86 -> 807
    //   724: ldc_w 371
    //   727: astore 21
    //   729: aload 6
    //   731: aload 21
    //   733: invokevirtual 119	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   736: istore 25
    //   738: iload 25
    //   740: ifeq +64 -> 804
    //   743: bipush 9
    //   745: istore 25
    //   747: goto +60 -> 807
    //   750: ldc_w 373
    //   753: astore 21
    //   755: aload 6
    //   757: aload 21
    //   759: invokevirtual 119	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   762: istore 25
    //   764: iload 25
    //   766: ifeq +38 -> 804
    //   769: bipush 8
    //   771: istore 25
    //   773: goto +34 -> 807
    //   776: ldc_w 375
    //   779: astore 21
    //   781: aload 6
    //   783: aload 21
    //   785: invokevirtual 119	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   788: istore 25
    //   790: iload 25
    //   792: ifeq +12 -> 804
    //   795: iconst_0
    //   796: istore 25
    //   798: aconst_null
    //   799: astore 21
    //   801: goto +6 -> 807
    //   804: iconst_m1
    //   805: istore 25
    //   807: iload 25
    //   809: tableswitch	default:+55->864, 0:+3253->4062, 1:+3168->3977, 2:+3077->3886, 3:+2635->3444, 4:+2074->2883, 5:+2074->2883, 6:+1807->2616, 7:+580->1389, 8:+548->1357, 9:+82->891
    //   864: iload 20
    //   866: istore 31
    //   868: iload 18
    //   870: istore 32
    //   872: aload 6
    //   874: astore 14
    //   876: aload 24
    //   878: astore 33
    //   880: iload 26
    //   882: istore 34
    //   884: aload 8
    //   886: astore 35
    //   888: goto +4543 -> 5431
    //   891: aload_1
    //   892: invokestatic 378	com/inmobi/ads/ak:p	(Lorg/json/JSONObject;)Z
    //   895: istore 25
    //   897: iload 25
    //   899: ifne +5 -> 904
    //   902: aconst_null
    //   903: areturn
    //   904: aload 24
    //   906: astore 7
    //   908: aload_0
    //   909: astore 21
    //   911: iload 26
    //   913: istore 34
    //   915: aload 10
    //   917: astore 29
    //   919: aload_0
    //   920: aload 13
    //   922: aload 15
    //   924: aload 14
    //   926: aload 16
    //   928: aload 10
    //   930: invokespecial 381	com/inmobi/ads/ak:c	(Landroid/graphics/Point;Landroid/graphics/Point;Landroid/graphics/Point;Landroid/graphics/Point;Lorg/json/JSONObject;)Lcom/inmobi/ads/aw$a;
    //   933: astore 21
    //   935: ldc_w 383
    //   938: astore 13
    //   940: aload 5
    //   942: aload 13
    //   944: invokevirtual 264	org/json/JSONObject:getJSONObject	(Ljava/lang/String;)Lorg/json/JSONObject;
    //   947: astore 13
    //   949: ldc_w 385
    //   952: astore 15
    //   954: aload 13
    //   956: aload 15
    //   958: invokevirtual 137	org/json/JSONObject:isNull	(Ljava/lang/String;)Z
    //   961: istore 36
    //   963: iload 36
    //   965: ifne +94 -> 1059
    //   968: ldc_w 383
    //   971: astore 13
    //   973: aload 5
    //   975: aload 13
    //   977: invokevirtual 264	org/json/JSONObject:getJSONObject	(Ljava/lang/String;)Lorg/json/JSONObject;
    //   980: astore 13
    //   982: ldc_w 385
    //   985: astore 15
    //   987: aload 13
    //   989: aload 15
    //   991: invokevirtual 200	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   994: astore 13
    //   996: aload 13
    //   998: invokestatic 387	com/inmobi/ads/ak:d	(Ljava/lang/String;)I
    //   1001: istore 36
    //   1003: ldc_w 383
    //   1006: astore 15
    //   1008: aload 5
    //   1010: aload 15
    //   1012: invokevirtual 264	org/json/JSONObject:getJSONObject	(Ljava/lang/String;)Lorg/json/JSONObject;
    //   1015: astore 15
    //   1017: ldc_w 389
    //   1020: astore 14
    //   1022: aload 15
    //   1024: aload 14
    //   1026: invokevirtual 392	org/json/JSONObject:optString	(Ljava/lang/String;)Ljava/lang/String;
    //   1029: astore 15
    //   1031: goto +37 -> 1068
    //   1034: astore 23
    //   1036: iload 20
    //   1038: istore 31
    //   1040: iload 18
    //   1042: istore 32
    //   1044: aload 8
    //   1046: astore 35
    //   1048: aload 7
    //   1050: astore 33
    //   1052: aload 6
    //   1054: astore 14
    //   1056: goto +4408 -> 5464
    //   1059: iconst_0
    //   1060: istore 37
    //   1062: aconst_null
    //   1063: astore 15
    //   1065: iconst_2
    //   1066: istore 36
    //   1068: aload 17
    //   1070: ifnull +121 -> 1191
    //   1073: aload 17
    //   1075: invokeinterface 395 1 0
    //   1080: istore 38
    //   1082: iload 38
    //   1084: ifne +6 -> 1090
    //   1087: goto +104 -> 1191
    //   1090: new 397	com/inmobi/ads/aj
    //   1093: astore 14
    //   1095: aload 14
    //   1097: astore 10
    //   1099: iload 20
    //   1101: istore 39
    //   1103: aload 8
    //   1105: astore 40
    //   1107: iload 18
    //   1109: istore 19
    //   1111: aload 21
    //   1113: astore 41
    //   1115: aload 8
    //   1117: astore 21
    //   1119: aload 23
    //   1121: astore 8
    //   1123: iload 18
    //   1125: istore 42
    //   1127: aload 7
    //   1129: astore 33
    //   1131: aload_3
    //   1132: astore 29
    //   1134: aload 17
    //   1136: astore 7
    //   1138: iload 36
    //   1140: istore 43
    //   1142: aload 5
    //   1144: astore 13
    //   1146: aload_1
    //   1147: astore 5
    //   1149: aload 14
    //   1151: aload 40
    //   1153: aload 9
    //   1155: aload 41
    //   1157: aload 23
    //   1159: aload 17
    //   1161: iload 36
    //   1163: aload_1
    //   1164: invokespecial 400	com/inmobi/ads/aj:<init>	(Ljava/lang/String;Ljava/lang/String;Lcom/inmobi/ads/ah;Ljava/lang/String;Ljava/util/List;ILorg/json/JSONObject;)V
    //   1167: aload 13
    //   1169: astore 5
    //   1171: aload 14
    //   1173: astore 23
    //   1175: aload 40
    //   1177: astore 14
    //   1179: goto +79 -> 1258
    //   1182: astore 23
    //   1184: aload 21
    //   1186: astore 35
    //   1188: goto +124 -> 1312
    //   1191: iload 20
    //   1193: istore 39
    //   1195: iload 18
    //   1197: istore 42
    //   1199: aload 8
    //   1201: astore 14
    //   1203: aload 7
    //   1205: astore 33
    //   1207: aload_3
    //   1208: astore 29
    //   1210: new 397	com/inmobi/ads/aj
    //   1213: astore 44
    //   1215: aload 44
    //   1217: astore 10
    //   1219: aload 8
    //   1221: astore 40
    //   1223: aload 21
    //   1225: astore 41
    //   1227: aload 23
    //   1229: astore 8
    //   1231: iload 36
    //   1233: istore 28
    //   1235: aload_1
    //   1236: astore 6
    //   1238: aload 44
    //   1240: aload 14
    //   1242: aload 9
    //   1244: aload 21
    //   1246: aload 23
    //   1248: iload 36
    //   1250: aload_1
    //   1251: invokespecial 403	com/inmobi/ads/aj:<init>	(Ljava/lang/String;Ljava/lang/String;Lcom/inmobi/ads/ah;Ljava/lang/String;ILorg/json/JSONObject;)V
    //   1254: aload 44
    //   1256: astore 23
    //   1258: aload 23
    //   1260: aload 29
    //   1262: putfield 405	com/inmobi/ads/ag:g	Ljava/lang/String;
    //   1265: aload 23
    //   1267: aload 5
    //   1269: invokestatic 408	com/inmobi/ads/ak:a	(Lcom/inmobi/ads/ag;Lorg/json/JSONObject;)V
    //   1272: aload 15
    //   1274: ifnull +10 -> 1284
    //   1277: aload 23
    //   1279: aload 15
    //   1281: invokevirtual 411	com/inmobi/ads/ag:b	(Ljava/lang/String;)V
    //   1284: aload 23
    //   1286: astore 12
    //   1288: aload 14
    //   1290: astore 35
    //   1292: iload 39
    //   1294: istore 31
    //   1296: iload 42
    //   1298: istore 32
    //   1300: aload_2
    //   1301: astore 14
    //   1303: goto +4128 -> 5431
    //   1306: astore 23
    //   1308: aload 14
    //   1310: astore 35
    //   1312: iload 39
    //   1314: istore 31
    //   1316: iload 42
    //   1318: istore 32
    //   1320: goto +2118 -> 3438
    //   1323: astore 23
    //   1325: aload 7
    //   1327: astore 33
    //   1329: goto +13 -> 1342
    //   1332: astore 23
    //   1334: aload 24
    //   1336: astore 33
    //   1338: iload 26
    //   1340: istore 34
    //   1342: iload 20
    //   1344: istore 31
    //   1346: iload 18
    //   1348: istore 32
    //   1350: aload 8
    //   1352: astore 35
    //   1354: goto +2084 -> 3438
    //   1357: aload 24
    //   1359: astore 33
    //   1361: iload 26
    //   1363: istore 34
    //   1365: iload 20
    //   1367: istore 31
    //   1369: iload 18
    //   1371: istore 32
    //   1373: aload 8
    //   1375: astore 35
    //   1377: iconst_0
    //   1378: istore 11
    //   1380: aconst_null
    //   1381: astore 23
    //   1383: aload_2
    //   1384: astore 14
    //   1386: goto +4108 -> 5494
    //   1389: iload 20
    //   1391: istore 28
    //   1393: iload 18
    //   1395: istore 43
    //   1397: aload 8
    //   1399: astore 21
    //   1401: aload 24
    //   1403: astore 33
    //   1405: iload 26
    //   1407: istore 34
    //   1409: aload 29
    //   1411: astore 8
    //   1413: aload_3
    //   1414: astore 29
    //   1416: aload 4
    //   1418: getfield 73	com/inmobi/ads/ak:h	Ljava/util/Map;
    //   1421: astore 40
    //   1423: ldc_w 359
    //   1426: astore 41
    //   1428: aload 40
    //   1430: aload 41
    //   1432: invokeinterface 415 2 0
    //   1437: pop
    //   1438: aload 4
    //   1440: aload 10
    //   1442: invokespecial 418	com/inmobi/ads/ak:s	(Lorg/json/JSONObject;)Lcom/inmobi/ads/ax;
    //   1445: astore 45
    //   1447: new 420	com/inmobi/ads/bb$a
    //   1450: astore 41
    //   1452: aload 13
    //   1454: getfield 191	android/graphics/Point:x	I
    //   1457: istore 46
    //   1459: aload 13
    //   1461: getfield 194	android/graphics/Point:y	I
    //   1464: istore 36
    //   1466: aload 15
    //   1468: getfield 191	android/graphics/Point:x	I
    //   1471: istore 20
    //   1473: aload 15
    //   1475: getfield 194	android/graphics/Point:y	I
    //   1478: istore 37
    //   1480: iload 28
    //   1482: istore 30
    //   1484: aload 14
    //   1486: getfield 191	android/graphics/Point:x	I
    //   1489: istore 28
    //   1491: aload 14
    //   1493: getfield 194	android/graphics/Point:y	I
    //   1496: istore 38
    //   1498: iload 18
    //   1500: istore 47
    //   1502: aload 16
    //   1504: getfield 191	android/graphics/Point:x	I
    //   1507: istore 43
    //   1509: aload 16
    //   1511: getfield 194	android/graphics/Point:y	I
    //   1514: istore 39
    //   1516: aload 41
    //   1518: iload 46
    //   1520: iload 36
    //   1522: iload 20
    //   1524: iload 37
    //   1526: iload 28
    //   1528: iload 38
    //   1530: iload 43
    //   1532: iload 39
    //   1534: aload 45
    //   1536: invokespecial 423	com/inmobi/ads/bb$a:<init>	(IIIIIIIILcom/inmobi/ads/ax;)V
    //   1539: aload 4
    //   1541: getfield 64	com/inmobi/ads/ak:q	Lcom/inmobi/ads/bt;
    //   1544: astore 13
    //   1546: aload 13
    //   1548: ifnonnull +36 -> 1584
    //   1551: aload 4
    //   1553: aload 5
    //   1555: aload 23
    //   1557: aload 8
    //   1559: invokespecial 426	com/inmobi/ads/ak:a	(Lorg/json/JSONObject;Ljava/lang/String;Lcom/inmobi/ads/ag;)Lcom/inmobi/ads/bu;
    //   1562: astore 23
    //   1564: goto +27 -> 1591
    //   1567: astore 23
    //   1569: aload 21
    //   1571: astore 35
    //   1573: iload 30
    //   1575: istore 31
    //   1577: iload 47
    //   1579: istore 32
    //   1581: goto +1857 -> 3438
    //   1584: aload 4
    //   1586: getfield 64	com/inmobi/ads/ak:q	Lcom/inmobi/ads/bt;
    //   1589: astore 23
    //   1591: getstatic 431	com/inmobi/ads/AdContainer$RenderingProperties$PlacementType:PLACEMENT_TYPE_INLINE	Lcom/inmobi/ads/AdContainer$RenderingProperties$PlacementType;
    //   1594: astore 13
    //   1596: aload 4
    //   1598: getfield 51	com/inmobi/ads/ak:s	Lcom/inmobi/ads/AdContainer$RenderingProperties$PlacementType;
    //   1601: astore 15
    //   1603: dconst_0
    //   1604: dstore 48
    //   1606: aload 13
    //   1608: aload 15
    //   1610: if_acmpne +435 -> 2045
    //   1613: aload 8
    //   1615: ifnull +256 -> 1871
    //   1618: aload 8
    //   1620: getfield 434	com/inmobi/ads/ag:v	Ljava/util/Map;
    //   1623: astore 13
    //   1625: ldc_w 436
    //   1628: astore 15
    //   1630: aload 13
    //   1632: aload 15
    //   1634: invokeinterface 415 2 0
    //   1639: astore 13
    //   1641: aload 13
    //   1643: checkcast 438	java/lang/Boolean
    //   1646: astore 13
    //   1648: aload 13
    //   1650: invokevirtual 441	java/lang/Boolean:booleanValue	()Z
    //   1653: istore 36
    //   1655: iload 36
    //   1657: ifne +55 -> 1712
    //   1660: aload 4
    //   1662: getfield 82	com/inmobi/ads/ak:t	Z
    //   1665: istore 36
    //   1667: iload 36
    //   1669: ifeq +6 -> 1675
    //   1672: goto +40 -> 1712
    //   1675: iconst_m1
    //   1676: iconst_1
    //   1677: iushr
    //   1678: istore 36
    //   1680: iconst_1
    //   1681: istore 37
    //   1683: iconst_1
    //   1684: istore 38
    //   1686: iconst_0
    //   1687: istore 39
    //   1689: aconst_null
    //   1690: astore 16
    //   1692: iconst_1
    //   1693: istore 28
    //   1695: iconst_m1
    //   1696: iconst_1
    //   1697: iushr
    //   1698: istore 43
    //   1700: iconst_1
    //   1701: istore 42
    //   1703: iconst_0
    //   1704: istore 50
    //   1706: aconst_null
    //   1707: astore 44
    //   1709: goto +497 -> 2206
    //   1712: ldc_w 443
    //   1715: astore 13
    //   1717: iconst_0
    //   1718: istore 37
    //   1720: aconst_null
    //   1721: astore 15
    //   1723: aload 5
    //   1725: aload 13
    //   1727: iconst_0
    //   1728: invokevirtual 447	org/json/JSONObject:optBoolean	(Ljava/lang/String;Z)Z
    //   1731: istore 36
    //   1733: ldc_w 449
    //   1736: astore 15
    //   1738: iconst_1
    //   1739: istore 46
    //   1741: aload 5
    //   1743: aload 15
    //   1745: iload 46
    //   1747: invokevirtual 447	org/json/JSONObject:optBoolean	(Ljava/lang/String;Z)Z
    //   1750: istore 37
    //   1752: ldc_w 451
    //   1755: astore 40
    //   1757: aload 5
    //   1759: aload 40
    //   1761: iload 46
    //   1763: invokevirtual 447	org/json/JSONObject:optBoolean	(Ljava/lang/String;Z)Z
    //   1766: istore 20
    //   1768: ldc_w 453
    //   1771: astore 7
    //   1773: aload 5
    //   1775: aload 7
    //   1777: iload 46
    //   1779: invokevirtual 447	org/json/JSONObject:optBoolean	(Ljava/lang/String;Z)Z
    //   1782: istore 28
    //   1784: ldc_w 455
    //   1787: astore 6
    //   1789: aload 5
    //   1791: aload 6
    //   1793: iload 46
    //   1795: invokevirtual 447	org/json/JSONObject:optBoolean	(Ljava/lang/String;Z)Z
    //   1798: istore 46
    //   1800: aload 8
    //   1802: astore 6
    //   1804: aload 8
    //   1806: checkcast 457	com/inmobi/ads/bb
    //   1809: astore 6
    //   1811: aload 6
    //   1813: getfield 460	com/inmobi/ads/bb:E	I
    //   1816: istore 43
    //   1818: iload 36
    //   1820: istore 42
    //   1822: ldc_w 462
    //   1825: astore 13
    //   1827: aload 5
    //   1829: aload 13
    //   1831: dload 48
    //   1833: invokevirtual 466	org/json/JSONObject:optDouble	(Ljava/lang/String;D)D
    //   1836: dstore 48
    //   1838: dload 48
    //   1840: d2i
    //   1841: istore 39
    //   1843: iload 46
    //   1845: istore 38
    //   1847: iload 43
    //   1849: istore 36
    //   1851: iload 42
    //   1853: istore 50
    //   1855: iconst_m1
    //   1856: iconst_1
    //   1857: iushr
    //   1858: istore 43
    //   1860: iload 28
    //   1862: istore 42
    //   1864: iload 20
    //   1866: istore 28
    //   1868: goto +338 -> 2206
    //   1871: ldc_w 443
    //   1874: astore 13
    //   1876: iconst_1
    //   1877: istore 37
    //   1879: aload 5
    //   1881: aload 13
    //   1883: iload 37
    //   1885: invokevirtual 447	org/json/JSONObject:optBoolean	(Ljava/lang/String;Z)Z
    //   1888: istore 36
    //   1890: ldc_w 449
    //   1893: astore 15
    //   1895: iconst_0
    //   1896: istore 46
    //   1898: aconst_null
    //   1899: astore 10
    //   1901: aload 5
    //   1903: aload 15
    //   1905: iconst_0
    //   1906: invokevirtual 447	org/json/JSONObject:optBoolean	(Ljava/lang/String;Z)Z
    //   1909: istore 37
    //   1911: ldc_w 451
    //   1914: astore 40
    //   1916: aload 5
    //   1918: aload 40
    //   1920: iconst_0
    //   1921: invokevirtual 447	org/json/JSONObject:optBoolean	(Ljava/lang/String;Z)Z
    //   1924: istore 20
    //   1926: ldc_w 453
    //   1929: astore 7
    //   1931: aload 5
    //   1933: aload 7
    //   1935: iconst_0
    //   1936: invokevirtual 447	org/json/JSONObject:optBoolean	(Ljava/lang/String;Z)Z
    //   1939: istore 46
    //   1941: ldc_w 455
    //   1944: astore 7
    //   1946: iconst_1
    //   1947: istore 43
    //   1949: aload 5
    //   1951: aload 7
    //   1953: iload 43
    //   1955: invokevirtual 447	org/json/JSONObject:optBoolean	(Ljava/lang/String;Z)Z
    //   1958: istore 28
    //   1960: ldc_w 468
    //   1963: astore 6
    //   1965: iconst_m1
    //   1966: iconst_1
    //   1967: iushr
    //   1968: istore 38
    //   1970: aload 5
    //   1972: aload 6
    //   1974: iload 38
    //   1976: invokevirtual 472	org/json/JSONObject:optInt	(Ljava/lang/String;I)I
    //   1979: istore 43
    //   1981: ldc_w 462
    //   1984: astore 14
    //   1986: iload 36
    //   1988: istore 39
    //   1990: iload 37
    //   1992: istore 42
    //   1994: dconst_0
    //   1995: dstore 51
    //   1997: aload 5
    //   1999: aload 14
    //   2001: dload 51
    //   2003: invokevirtual 466	org/json/JSONObject:optDouble	(Ljava/lang/String;D)D
    //   2006: dstore 51
    //   2008: dload 51
    //   2010: d2i
    //   2011: istore 36
    //   2013: iload 39
    //   2015: istore 50
    //   2017: iload 28
    //   2019: istore 38
    //   2021: iload 36
    //   2023: istore 39
    //   2025: iload 46
    //   2027: istore 42
    //   2029: iload 20
    //   2031: istore 28
    //   2033: iload 43
    //   2035: istore 36
    //   2037: iconst_m1
    //   2038: iconst_1
    //   2039: iushr
    //   2040: istore 43
    //   2042: goto +164 -> 2206
    //   2045: ldc_w 443
    //   2048: astore 13
    //   2050: iconst_0
    //   2051: istore 20
    //   2053: aconst_null
    //   2054: astore 40
    //   2056: aload 5
    //   2058: aload 13
    //   2060: iconst_0
    //   2061: invokevirtual 447	org/json/JSONObject:optBoolean	(Ljava/lang/String;Z)Z
    //   2064: istore 36
    //   2066: ldc_w 449
    //   2069: astore 15
    //   2071: iconst_1
    //   2072: istore 38
    //   2074: aload 5
    //   2076: aload 15
    //   2078: iload 38
    //   2080: invokevirtual 447	org/json/JSONObject:optBoolean	(Ljava/lang/String;Z)Z
    //   2083: istore 37
    //   2085: ldc_w 451
    //   2088: astore 16
    //   2090: aload 5
    //   2092: aload 16
    //   2094: iload 38
    //   2096: invokevirtual 447	org/json/JSONObject:optBoolean	(Ljava/lang/String;Z)Z
    //   2099: istore 39
    //   2101: ldc_w 453
    //   2104: astore 10
    //   2106: aload 5
    //   2108: aload 10
    //   2110: iload 38
    //   2112: invokevirtual 447	org/json/JSONObject:optBoolean	(Ljava/lang/String;Z)Z
    //   2115: istore 46
    //   2117: ldc_w 455
    //   2120: astore 40
    //   2122: aload 5
    //   2124: aload 40
    //   2126: iload 38
    //   2128: invokevirtual 447	org/json/JSONObject:optBoolean	(Ljava/lang/String;Z)Z
    //   2131: istore 38
    //   2133: ldc_w 468
    //   2136: astore 40
    //   2138: iconst_m1
    //   2139: iconst_1
    //   2140: iushr
    //   2141: istore 43
    //   2143: aload 5
    //   2145: aload 40
    //   2147: iload 43
    //   2149: invokevirtual 472	org/json/JSONObject:optInt	(Ljava/lang/String;I)I
    //   2152: istore 20
    //   2154: ldc_w 462
    //   2157: astore 7
    //   2159: iload 36
    //   2161: istore 42
    //   2163: iload 37
    //   2165: istore 50
    //   2167: dconst_0
    //   2168: dstore 51
    //   2170: aload 5
    //   2172: aload 7
    //   2174: dload 51
    //   2176: invokevirtual 466	org/json/JSONObject:optDouble	(Ljava/lang/String;D)D
    //   2179: dstore 51
    //   2181: dload 51
    //   2183: d2i
    //   2184: istore 36
    //   2186: iload 39
    //   2188: istore 28
    //   2190: iload 36
    //   2192: istore 39
    //   2194: iload 20
    //   2196: istore 36
    //   2198: iload 42
    //   2200: istore 50
    //   2202: iload 46
    //   2204: istore 42
    //   2206: new 66	java/util/HashMap
    //   2209: astore 40
    //   2211: aload 40
    //   2213: invokespecial 67	java/util/HashMap:<init>	()V
    //   2216: ldc_w 474
    //   2219: astore 10
    //   2221: aload 5
    //   2223: aload 10
    //   2225: invokevirtual 137	org/json/JSONObject:isNull	(Ljava/lang/String;)Z
    //   2228: istore 46
    //   2230: iload 46
    //   2232: ifne +102 -> 2334
    //   2235: ldc_w 474
    //   2238: astore 10
    //   2240: aload 5
    //   2242: aload 10
    //   2244: invokevirtual 264	org/json/JSONObject:getJSONObject	(Ljava/lang/String;)Lorg/json/JSONObject;
    //   2247: astore 10
    //   2249: aload 10
    //   2251: invokevirtual 268	org/json/JSONObject:keys	()Ljava/util/Iterator;
    //   2254: astore 35
    //   2256: aload 35
    //   2258: invokeinterface 274 1 0
    //   2263: istore 27
    //   2265: iload 27
    //   2267: ifeq +60 -> 2327
    //   2270: aload 35
    //   2272: invokeinterface 278 1 0
    //   2277: astore 53
    //   2279: aload 53
    //   2281: astore 6
    //   2283: aload 53
    //   2285: checkcast 95	java/lang/String
    //   2288: astore 6
    //   2290: aload 8
    //   2292: astore 53
    //   2294: aload 10
    //   2296: aload 6
    //   2298: invokevirtual 477	org/json/JSONObject:get	(Ljava/lang/String;)Ljava/lang/Object;
    //   2301: astore 8
    //   2303: aload 40
    //   2305: aload 6
    //   2307: aload 8
    //   2309: invokeinterface 258 3 0
    //   2314: pop
    //   2315: aload 53
    //   2317: astore 8
    //   2319: iconst_m1
    //   2320: iconst_1
    //   2321: iushr
    //   2322: istore 43
    //   2324: goto -68 -> 2256
    //   2327: aload 8
    //   2329: astore 53
    //   2331: goto +7 -> 2338
    //   2334: aload 8
    //   2336: astore 53
    //   2338: new 457	com/inmobi/ads/bb
    //   2341: astore 6
    //   2343: aload 4
    //   2345: getfield 56	com/inmobi/ads/ak:r	Lcom/inmobi/ads/b;
    //   2348: astore 10
    //   2350: aload 10
    //   2352: getfield 480	com/inmobi/ads/b:m	Lcom/inmobi/ads/b$h;
    //   2355: astore 10
    //   2357: aload 10
    //   2359: getfield 484	com/inmobi/ads/b$h:l	Z
    //   2362: istore 54
    //   2364: aload 6
    //   2366: astore 10
    //   2368: aload 40
    //   2370: astore 4
    //   2372: aload 21
    //   2374: astore 40
    //   2376: aload 21
    //   2378: astore 35
    //   2380: aload 53
    //   2382: astore 21
    //   2384: iload 54
    //   2386: istore 27
    //   2388: aload 23
    //   2390: astore 8
    //   2392: iload 30
    //   2394: istore 31
    //   2396: aload 6
    //   2398: astore 23
    //   2400: iload 47
    //   2402: istore 32
    //   2404: iload 42
    //   2406: istore 43
    //   2408: iload 50
    //   2410: istore 55
    //   2412: iload 37
    //   2414: istore 42
    //   2416: iload 38
    //   2418: istore 50
    //   2420: aload_1
    //   2421: astore 22
    //   2423: iload 54
    //   2425: istore 47
    //   2427: aload 6
    //   2429: aload 40
    //   2431: aload 9
    //   2433: aload 41
    //   2435: aload 8
    //   2437: iload 28
    //   2439: iload 43
    //   2441: iload 55
    //   2443: iload 37
    //   2445: iload 38
    //   2447: aload 17
    //   2449: aload_1
    //   2450: iload 54
    //   2452: invokespecial 487	com/inmobi/ads/bb:<init>	(Ljava/lang/String;Ljava/lang/String;Lcom/inmobi/ads/ah;Lcom/inmobi/ads/bu;ZZZZZLjava/util/List;Lorg/json/JSONObject;Z)V
    //   2455: aload 6
    //   2457: checkcast 457	com/inmobi/ads/bb
    //   2460: astore 6
    //   2462: new 66	java/util/HashMap
    //   2465: astore 15
    //   2467: aload 15
    //   2469: aload 4
    //   2471: invokespecial 284	java/util/HashMap:<init>	(Ljava/util/Map;)V
    //   2474: aload 6
    //   2476: aload 15
    //   2478: putfield 490	com/inmobi/ads/bb:G	Ljava/util/Map;
    //   2481: aload 10
    //   2483: astore 6
    //   2485: aload 10
    //   2487: checkcast 457	com/inmobi/ads/bb
    //   2490: astore 6
    //   2492: iload 36
    //   2494: ifgt +8 -> 2502
    //   2497: iconst_m1
    //   2498: iconst_1
    //   2499: iushr
    //   2500: istore 36
    //   2502: aload 6
    //   2504: iload 36
    //   2506: putfield 460	com/inmobi/ads/bb:E	I
    //   2509: aload 23
    //   2511: aload 29
    //   2513: putfield 405	com/inmobi/ads/ag:g	Ljava/lang/String;
    //   2516: aload 23
    //   2518: aload 21
    //   2520: putfield 493	com/inmobi/ads/ag:y	Lcom/inmobi/ads/ag;
    //   2523: aload 21
    //   2525: ifnull +10 -> 2535
    //   2528: aload 21
    //   2530: aload 23
    //   2532: putfield 493	com/inmobi/ads/ag:y	Lcom/inmobi/ads/ag;
    //   2535: iload 39
    //   2537: ifeq +21 -> 2558
    //   2540: aload 23
    //   2542: astore 6
    //   2544: aload 23
    //   2546: checkcast 457	com/inmobi/ads/bb
    //   2549: astore 6
    //   2551: aload 6
    //   2553: iload 39
    //   2555: putfield 496	com/inmobi/ads/bb:F	I
    //   2558: aload_2
    //   2559: astore 14
    //   2561: aload_0
    //   2562: astore 4
    //   2564: goto +2930 -> 5494
    //   2567: astore 23
    //   2569: iload 30
    //   2571: istore 31
    //   2573: iload 47
    //   2575: istore 32
    //   2577: goto +26 -> 2603
    //   2580: astore 23
    //   2582: iload 43
    //   2584: istore 32
    //   2586: iload 30
    //   2588: istore 31
    //   2590: goto +13 -> 2603
    //   2593: astore 23
    //   2595: iload 28
    //   2597: istore 31
    //   2599: iload 43
    //   2601: istore 32
    //   2603: aload 21
    //   2605: astore 35
    //   2607: aload_2
    //   2608: astore 14
    //   2610: aload_0
    //   2611: astore 4
    //   2613: goto +2851 -> 5464
    //   2616: iload 20
    //   2618: istore 31
    //   2620: iload 18
    //   2622: istore 32
    //   2624: aload 24
    //   2626: astore 33
    //   2628: iload 26
    //   2630: istore 34
    //   2632: aload_3
    //   2633: astore 29
    //   2635: iconst_0
    //   2636: istore 20
    //   2638: aconst_null
    //   2639: astore 40
    //   2641: aload 8
    //   2643: astore 35
    //   2645: aload 23
    //   2647: ifnonnull +5 -> 2652
    //   2650: aconst_null
    //   2651: areturn
    //   2652: aload_1
    //   2653: invokestatic 498	com/inmobi/ads/ak:h	(Lorg/json/JSONObject;)Ljava/lang/String;
    //   2656: astore 21
    //   2658: aload 21
    //   2660: invokestatic 502	com/inmobi/ads/bc:c	(Ljava/lang/String;)Ljava/lang/String;
    //   2663: astore 4
    //   2665: ldc_w 504
    //   2668: astore 21
    //   2670: aload 21
    //   2672: aload 4
    //   2674: invokevirtual 119	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   2677: istore 25
    //   2679: iload 25
    //   2681: ifeq +22 -> 2703
    //   2684: aload 23
    //   2686: invokestatic 224	android/webkit/URLUtil:isValidUrl	(Ljava/lang/String;)Z
    //   2689: istore 25
    //   2691: iload 25
    //   2693: ifne +10 -> 2703
    //   2696: aconst_null
    //   2697: areturn
    //   2698: astore 23
    //   2700: goto -93 -> 2607
    //   2703: aload 35
    //   2705: astore 5
    //   2707: iconst_0
    //   2708: istore 43
    //   2710: aconst_null
    //   2711: astore 6
    //   2713: aload_0
    //   2714: astore 21
    //   2716: aload 29
    //   2718: astore 7
    //   2720: aload 10
    //   2722: astore 29
    //   2724: aload_0
    //   2725: aload 13
    //   2727: aload 15
    //   2729: aload 14
    //   2731: aload 16
    //   2733: aload 10
    //   2735: invokespecial 507	com/inmobi/ads/ak:a	(Landroid/graphics/Point;Landroid/graphics/Point;Landroid/graphics/Point;Landroid/graphics/Point;Lorg/json/JSONObject;)Lcom/inmobi/ads/ah;
    //   2738: astore 14
    //   2740: new 500	com/inmobi/ads/bc
    //   2743: astore 10
    //   2745: ldc_w 509
    //   2748: astore 21
    //   2750: aload_1
    //   2751: astore 8
    //   2753: aload_1
    //   2754: aload 21
    //   2756: invokevirtual 511	org/json/JSONObject:optBoolean	(Ljava/lang/String;)Z
    //   2759: istore 19
    //   2761: aload 10
    //   2763: astore 21
    //   2765: aload 35
    //   2767: astore 13
    //   2769: aload 9
    //   2771: astore 15
    //   2773: aload 23
    //   2775: astore 16
    //   2777: aload 10
    //   2779: aload 35
    //   2781: aload 9
    //   2783: aload 14
    //   2785: aload 23
    //   2787: iload 19
    //   2789: invokespecial 514	com/inmobi/ads/bc:<init>	(Ljava/lang/String;Ljava/lang/String;Lcom/inmobi/ads/ah;Ljava/lang/String;Z)V
    //   2792: aload 10
    //   2794: aload 4
    //   2796: putfield 517	com/inmobi/ads/bc:z	Ljava/lang/String;
    //   2799: aload 10
    //   2801: aload 7
    //   2803: putfield 405	com/inmobi/ads/ag:g	Ljava/lang/String;
    //   2806: ldc_w 519
    //   2809: astore 23
    //   2811: aload_1
    //   2812: aload 23
    //   2814: iconst_0
    //   2815: invokevirtual 447	org/json/JSONObject:optBoolean	(Ljava/lang/String;Z)Z
    //   2818: istore 11
    //   2820: iload 11
    //   2822: ifeq +32 -> 2854
    //   2825: iconst_1
    //   2826: istore 25
    //   2828: aload 10
    //   2830: iload 25
    //   2832: putfield 522	com/inmobi/ads/bc:A	Z
    //   2835: aload_0
    //   2836: astore 4
    //   2838: aload_0
    //   2839: aload 10
    //   2841: putfield 524	com/inmobi/ads/ak:k	Lcom/inmobi/ads/bc;
    //   2844: aload 10
    //   2846: astore 23
    //   2848: aload_2
    //   2849: astore 14
    //   2851: goto +2643 -> 5494
    //   2854: aload_0
    //   2855: astore 4
    //   2857: aload 10
    //   2859: astore 12
    //   2861: aload_2
    //   2862: astore 14
    //   2864: goto +2567 -> 5431
    //   2867: astore 23
    //   2869: aload_0
    //   2870: astore 4
    //   2872: goto +562 -> 3434
    //   2875: astore 23
    //   2877: aload_0
    //   2878: astore 4
    //   2880: goto +558 -> 3438
    //   2883: iload 20
    //   2885: istore 31
    //   2887: iload 18
    //   2889: istore 32
    //   2891: aload 24
    //   2893: astore 33
    //   2895: iload 26
    //   2897: istore 34
    //   2899: aload_3
    //   2900: astore 7
    //   2902: iconst_0
    //   2903: istore 43
    //   2905: aconst_null
    //   2906: astore 6
    //   2908: aload 5
    //   2910: astore 56
    //   2912: aload 8
    //   2914: astore 5
    //   2916: aload 56
    //   2918: astore 8
    //   2920: aload_0
    //   2921: astore 21
    //   2923: aload 10
    //   2925: astore 29
    //   2927: aload_0
    //   2928: aload 13
    //   2930: aload 15
    //   2932: aload 14
    //   2934: aload 16
    //   2936: aload 10
    //   2938: invokespecial 507	com/inmobi/ads/ak:a	(Landroid/graphics/Point;Landroid/graphics/Point;Landroid/graphics/Point;Landroid/graphics/Point;Lorg/json/JSONObject;)Lcom/inmobi/ads/ah;
    //   2941: astore 41
    //   2943: aload_1
    //   2944: invokestatic 378	com/inmobi/ads/ak:p	(Lorg/json/JSONObject;)Z
    //   2947: istore 11
    //   2949: iload 11
    //   2951: ifeq +112 -> 3063
    //   2954: ldc_w 383
    //   2957: astore 23
    //   2959: aload 56
    //   2961: aload 23
    //   2963: invokevirtual 264	org/json/JSONObject:getJSONObject	(Ljava/lang/String;)Lorg/json/JSONObject;
    //   2966: astore 23
    //   2968: ldc_w 385
    //   2971: astore 21
    //   2973: aload 23
    //   2975: aload 21
    //   2977: invokevirtual 137	org/json/JSONObject:isNull	(Ljava/lang/String;)Z
    //   2980: istore 11
    //   2982: iload 11
    //   2984: ifne +41 -> 3025
    //   2987: ldc_w 383
    //   2990: astore 23
    //   2992: aload 56
    //   2994: aload 23
    //   2996: invokevirtual 264	org/json/JSONObject:getJSONObject	(Ljava/lang/String;)Lorg/json/JSONObject;
    //   2999: astore 23
    //   3001: ldc_w 385
    //   3004: astore 21
    //   3006: aload 23
    //   3008: aload 21
    //   3010: invokevirtual 200	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   3013: astore 23
    //   3015: aload 23
    //   3017: invokestatic 387	com/inmobi/ads/ak:d	(Ljava/lang/String;)I
    //   3020: istore 11
    //   3022: goto +6 -> 3028
    //   3025: iconst_2
    //   3026: istore 11
    //   3028: ldc_w 383
    //   3031: astore 21
    //   3033: aload 8
    //   3035: aload 21
    //   3037: invokevirtual 264	org/json/JSONObject:getJSONObject	(Ljava/lang/String;)Lorg/json/JSONObject;
    //   3040: astore 21
    //   3042: ldc_w 389
    //   3045: astore 13
    //   3047: aload 21
    //   3049: aload 13
    //   3051: invokevirtual 392	org/json/JSONObject:optString	(Ljava/lang/String;)Ljava/lang/String;
    //   3054: astore 21
    //   3056: iload 11
    //   3058: istore 43
    //   3060: goto +9 -> 3069
    //   3063: iconst_0
    //   3064: istore 25
    //   3066: aconst_null
    //   3067: astore 21
    //   3069: aload 17
    //   3071: ifnull +176 -> 3247
    //   3074: aload 17
    //   3076: invokeinterface 395 1 0
    //   3081: istore 11
    //   3083: iload 11
    //   3085: ifne +6 -> 3091
    //   3088: goto +159 -> 3247
    //   3091: ldc_w 363
    //   3094: astore 23
    //   3096: aload_2
    //   3097: astore 29
    //   3099: aload 23
    //   3101: aload_2
    //   3102: invokevirtual 119	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   3105: istore 11
    //   3107: iload 11
    //   3109: ifeq +66 -> 3175
    //   3112: new 526	com/inmobi/ads/ao
    //   3115: astore 23
    //   3117: aload_1
    //   3118: invokestatic 528	com/inmobi/ads/ak:c	(Lorg/json/JSONObject;)Ljava/lang/String;
    //   3121: astore 13
    //   3123: aload 23
    //   3125: astore 10
    //   3127: aload 5
    //   3129: astore 40
    //   3131: aload 8
    //   3133: astore 15
    //   3135: aload 13
    //   3137: astore 8
    //   3139: aload 7
    //   3141: astore 13
    //   3143: aload 17
    //   3145: astore 7
    //   3147: aload 5
    //   3149: astore 14
    //   3151: aload_1
    //   3152: astore 5
    //   3154: aload 23
    //   3156: aload 40
    //   3158: aload 9
    //   3160: aload 41
    //   3162: aload 8
    //   3164: aload 17
    //   3166: iload 43
    //   3168: aload_1
    //   3169: invokespecial 529	com/inmobi/ads/ao:<init>	(Ljava/lang/String;Ljava/lang/String;Lcom/inmobi/ads/ah;Ljava/lang/String;Ljava/util/List;ILorg/json/JSONObject;)V
    //   3172: goto +195 -> 3367
    //   3175: aload 8
    //   3177: astore 15
    //   3179: aload 7
    //   3181: astore 13
    //   3183: aload 5
    //   3185: astore 14
    //   3187: new 531	com/inmobi/ads/am
    //   3190: astore 23
    //   3192: aload_1
    //   3193: invokestatic 528	com/inmobi/ads/ak:c	(Lorg/json/JSONObject;)Ljava/lang/String;
    //   3196: astore 8
    //   3198: aload 23
    //   3200: astore 10
    //   3202: aload 5
    //   3204: astore 40
    //   3206: aload 17
    //   3208: astore 7
    //   3210: aload_1
    //   3211: astore 5
    //   3213: aload 23
    //   3215: aload 14
    //   3217: aload 9
    //   3219: aload 41
    //   3221: aload 8
    //   3223: aload 17
    //   3225: iload 43
    //   3227: aload_1
    //   3228: invokespecial 532	com/inmobi/ads/am:<init>	(Ljava/lang/String;Ljava/lang/String;Lcom/inmobi/ads/ah;Ljava/lang/String;Ljava/util/List;ILorg/json/JSONObject;)V
    //   3231: goto +136 -> 3367
    //   3234: astore 23
    //   3236: aload 29
    //   3238: astore 14
    //   3240: aload 5
    //   3242: astore 35
    //   3244: goto +2220 -> 5464
    //   3247: aload 8
    //   3249: astore 15
    //   3251: aload 7
    //   3253: astore 13
    //   3255: aload 5
    //   3257: astore 14
    //   3259: aload_2
    //   3260: astore 29
    //   3262: ldc_w 363
    //   3265: astore 23
    //   3267: aload 23
    //   3269: aload_2
    //   3270: invokevirtual 119	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   3273: istore 11
    //   3275: iload 11
    //   3277: ifeq +48 -> 3325
    //   3280: new 526	com/inmobi/ads/ao
    //   3283: astore 23
    //   3285: aload_1
    //   3286: invokestatic 528	com/inmobi/ads/ak:c	(Lorg/json/JSONObject;)Ljava/lang/String;
    //   3289: astore 8
    //   3291: aload 23
    //   3293: astore 10
    //   3295: aload 5
    //   3297: astore 40
    //   3299: iload 43
    //   3301: istore 28
    //   3303: aload_1
    //   3304: astore 6
    //   3306: aload 23
    //   3308: aload 5
    //   3310: aload 9
    //   3312: aload 41
    //   3314: aload 8
    //   3316: iload 43
    //   3318: aload_1
    //   3319: invokespecial 533	com/inmobi/ads/ao:<init>	(Ljava/lang/String;Ljava/lang/String;Lcom/inmobi/ads/ah;Ljava/lang/String;ILorg/json/JSONObject;)V
    //   3322: goto +45 -> 3367
    //   3325: new 531	com/inmobi/ads/am
    //   3328: astore 23
    //   3330: aload_1
    //   3331: invokestatic 528	com/inmobi/ads/ak:c	(Lorg/json/JSONObject;)Ljava/lang/String;
    //   3334: astore 8
    //   3336: aload 23
    //   3338: astore 10
    //   3340: aload 5
    //   3342: astore 40
    //   3344: iload 43
    //   3346: istore 28
    //   3348: aload_1
    //   3349: astore 6
    //   3351: aload 23
    //   3353: aload 5
    //   3355: aload 9
    //   3357: aload 41
    //   3359: aload 8
    //   3361: iload 43
    //   3363: aload_1
    //   3364: invokespecial 534	com/inmobi/ads/am:<init>	(Ljava/lang/String;Ljava/lang/String;Lcom/inmobi/ads/ah;Ljava/lang/String;ILorg/json/JSONObject;)V
    //   3367: aload 23
    //   3369: aload 13
    //   3371: putfield 405	com/inmobi/ads/ag:g	Ljava/lang/String;
    //   3374: aload 23
    //   3376: aload 15
    //   3378: invokestatic 408	com/inmobi/ads/ak:a	(Lcom/inmobi/ads/ag;Lorg/json/JSONObject;)V
    //   3381: aload 21
    //   3383: ifnull +21 -> 3404
    //   3386: aload 23
    //   3388: aload 21
    //   3390: invokevirtual 411	com/inmobi/ads/ag:b	(Ljava/lang/String;)V
    //   3393: aload 14
    //   3395: astore 35
    //   3397: aload 29
    //   3399: astore 14
    //   3401: goto +2093 -> 5494
    //   3404: aload 23
    //   3406: astore 12
    //   3408: aload 14
    //   3410: astore 35
    //   3412: aload 29
    //   3414: astore 14
    //   3416: goto +2015 -> 5431
    //   3419: astore 23
    //   3421: aload 14
    //   3423: astore 35
    //   3425: aload 29
    //   3427: astore 14
    //   3429: goto +2035 -> 5464
    //   3432: astore 23
    //   3434: aload 5
    //   3436: astore 35
    //   3438: aload_2
    //   3439: astore 14
    //   3441: goto +2023 -> 5464
    //   3444: iload 20
    //   3446: istore 31
    //   3448: iload 18
    //   3450: istore 32
    //   3452: aload 6
    //   3454: astore 29
    //   3456: aload 24
    //   3458: astore 33
    //   3460: iload 26
    //   3462: istore 34
    //   3464: aload_3
    //   3465: astore 7
    //   3467: iconst_0
    //   3468: istore 43
    //   3470: aconst_null
    //   3471: astore 6
    //   3473: aload_0
    //   3474: astore 21
    //   3476: aload 29
    //   3478: astore 41
    //   3480: aload 10
    //   3482: astore 29
    //   3484: aload_0
    //   3485: aload 13
    //   3487: aload 15
    //   3489: aload 14
    //   3491: aload 16
    //   3493: aload 10
    //   3495: invokespecial 507	com/inmobi/ads/ak:a	(Landroid/graphics/Point;Landroid/graphics/Point;Landroid/graphics/Point;Landroid/graphics/Point;Lorg/json/JSONObject;)Lcom/inmobi/ads/ah;
    //   3498: astore 23
    //   3500: ldc_w 536
    //   3503: astore 21
    //   3505: aload 5
    //   3507: aload 21
    //   3509: invokevirtual 539	org/json/JSONObject:has	(Ljava/lang/String;)Z
    //   3512: istore 25
    //   3514: iload 25
    //   3516: ifeq +29 -> 3545
    //   3519: ldc_w 536
    //   3522: astore 21
    //   3524: aload 5
    //   3526: aload 21
    //   3528: invokevirtual 264	org/json/JSONObject:getJSONObject	(Ljava/lang/String;)Lorg/json/JSONObject;
    //   3531: astore 21
    //   3533: aload 4
    //   3535: aload 21
    //   3537: invokespecial 542	com/inmobi/ads/ak:q	(Lorg/json/JSONObject;)Lcom/inmobi/ads/ax$a;
    //   3540: astore 21
    //   3542: goto +9 -> 3551
    //   3545: iconst_0
    //   3546: istore 25
    //   3548: aconst_null
    //   3549: astore 21
    //   3551: ldc_w 544
    //   3554: astore 13
    //   3556: aload 5
    //   3558: aload 13
    //   3560: invokevirtual 539	org/json/JSONObject:has	(Ljava/lang/String;)Z
    //   3563: istore 36
    //   3565: iload 36
    //   3567: ifeq +29 -> 3596
    //   3570: ldc_w 544
    //   3573: astore 13
    //   3575: aload 5
    //   3577: aload 13
    //   3579: invokevirtual 264	org/json/JSONObject:getJSONObject	(Ljava/lang/String;)Lorg/json/JSONObject;
    //   3582: astore 13
    //   3584: aload 4
    //   3586: aload 13
    //   3588: invokespecial 542	com/inmobi/ads/ak:q	(Lorg/json/JSONObject;)Lcom/inmobi/ads/ax$a;
    //   3591: astore 13
    //   3593: goto +9 -> 3602
    //   3596: iconst_0
    //   3597: istore 36
    //   3599: aconst_null
    //   3600: astore 13
    //   3602: ldc_w 546
    //   3605: astore 15
    //   3607: iconst_1
    //   3608: istore 20
    //   3610: aload 5
    //   3612: aload 15
    //   3614: iload 20
    //   3616: invokevirtual 447	org/json/JSONObject:optBoolean	(Ljava/lang/String;Z)Z
    //   3619: istore 37
    //   3621: new 548	com/inmobi/ads/ax
    //   3624: astore 14
    //   3626: aload 14
    //   3628: aload 21
    //   3630: aload 13
    //   3632: invokespecial 551	com/inmobi/ads/ax:<init>	(Lcom/inmobi/ads/ax$a;Lcom/inmobi/ads/ax$a;)V
    //   3635: new 553	com/inmobi/ads/ay
    //   3638: astore 21
    //   3640: aload 21
    //   3642: aload 8
    //   3644: aload 9
    //   3646: aload 23
    //   3648: aload 14
    //   3650: invokespecial 556	com/inmobi/ads/ay:<init>	(Ljava/lang/String;Ljava/lang/String;Lcom/inmobi/ads/ah;Lcom/inmobi/ads/ax;)V
    //   3653: aload 21
    //   3655: iload 37
    //   3657: putfield 558	com/inmobi/ads/ay:z	Z
    //   3660: ldc_w 560
    //   3663: astore 23
    //   3665: aload 5
    //   3667: aload 23
    //   3669: invokevirtual 539	org/json/JSONObject:has	(Ljava/lang/String;)Z
    //   3672: istore 11
    //   3674: iload 11
    //   3676: ifeq +188 -> 3864
    //   3679: ldc_w 560
    //   3682: astore 23
    //   3684: aload 5
    //   3686: aload 23
    //   3688: invokevirtual 477	org/json/JSONObject:get	(Ljava/lang/String;)Ljava/lang/Object;
    //   3691: astore 23
    //   3693: aload 23
    //   3695: checkcast 133	org/json/JSONObject
    //   3698: astore 23
    //   3700: ldc_w 562
    //   3703: astore 13
    //   3705: aload 23
    //   3707: aload 13
    //   3709: invokevirtual 539	org/json/JSONObject:has	(Ljava/lang/String;)Z
    //   3712: istore 36
    //   3714: iload 36
    //   3716: ifeq +148 -> 3864
    //   3719: ldc_w 562
    //   3722: astore 13
    //   3724: aload 23
    //   3726: aload 13
    //   3728: invokevirtual 200	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   3731: astore 23
    //   3733: getstatic 93	java/util/Locale:US	Ljava/util/Locale;
    //   3736: astore 13
    //   3738: aload 23
    //   3740: aload 13
    //   3742: invokevirtual 565	java/lang/String:toUpperCase	(Ljava/util/Locale;)Ljava/lang/String;
    //   3745: astore 23
    //   3747: aload 23
    //   3749: invokevirtual 103	java/lang/String:trim	()Ljava/lang/String;
    //   3752: astore 23
    //   3754: aload 23
    //   3756: invokevirtual 107	java/lang/String:hashCode	()I
    //   3759: istore 36
    //   3761: ldc_w 566
    //   3764: istore 37
    //   3766: iload 36
    //   3768: iload 37
    //   3770: if_icmpeq +43 -> 3813
    //   3773: ldc_w 568
    //   3776: istore 37
    //   3778: iload 36
    //   3780: iload 37
    //   3782: if_icmpeq +6 -> 3788
    //   3785: goto +53 -> 3838
    //   3788: ldc_w 571
    //   3791: astore 13
    //   3793: aload 23
    //   3795: aload 13
    //   3797: invokevirtual 119	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   3800: istore 11
    //   3802: iload 11
    //   3804: ifeq +34 -> 3838
    //   3807: iconst_1
    //   3808: istore 11
    //   3810: goto +31 -> 3841
    //   3813: ldc_w 573
    //   3816: astore 13
    //   3818: aload 23
    //   3820: aload 13
    //   3822: invokevirtual 119	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   3825: istore 11
    //   3827: iload 11
    //   3829: ifeq +9 -> 3838
    //   3832: iconst_2
    //   3833: istore 11
    //   3835: goto +6 -> 3841
    //   3838: iconst_m1
    //   3839: istore 11
    //   3841: iconst_2
    //   3842: istore 36
    //   3844: iload 11
    //   3846: iload 36
    //   3848: if_icmpeq +6 -> 3854
    //   3851: goto +6 -> 3857
    //   3854: iconst_1
    //   3855: istore 43
    //   3857: aload 21
    //   3859: iload 43
    //   3861: putfield 575	com/inmobi/ads/ag:k	I
    //   3864: aload 21
    //   3866: aload 7
    //   3868: putfield 405	com/inmobi/ads/ag:g	Ljava/lang/String;
    //   3871: aload 21
    //   3873: astore 23
    //   3875: aload 41
    //   3877: astore 14
    //   3879: aload 8
    //   3881: astore 35
    //   3883: goto +1611 -> 5494
    //   3886: iload 20
    //   3888: istore 31
    //   3890: iload 18
    //   3892: istore 32
    //   3894: aload 6
    //   3896: astore 41
    //   3898: aload 24
    //   3900: astore 33
    //   3902: iload 26
    //   3904: istore 34
    //   3906: aload_3
    //   3907: astore 7
    //   3909: aload_0
    //   3910: astore 21
    //   3912: aload 10
    //   3914: astore 29
    //   3916: aload_0
    //   3917: aload 13
    //   3919: aload 15
    //   3921: aload 14
    //   3923: aload 16
    //   3925: aload 10
    //   3927: invokespecial 507	com/inmobi/ads/ak:a	(Landroid/graphics/Point;Landroid/graphics/Point;Landroid/graphics/Point;Landroid/graphics/Point;Lorg/json/JSONObject;)Lcom/inmobi/ads/ah;
    //   3930: astore 23
    //   3932: new 577	com/inmobi/ads/an
    //   3935: astore 21
    //   3937: aload_1
    //   3938: invokestatic 528	com/inmobi/ads/ak:c	(Lorg/json/JSONObject;)Ljava/lang/String;
    //   3941: astore 13
    //   3943: aload 21
    //   3945: aload 8
    //   3947: aload 9
    //   3949: aload 23
    //   3951: aload 13
    //   3953: invokespecial 580	com/inmobi/ads/an:<init>	(Ljava/lang/String;Ljava/lang/String;Lcom/inmobi/ads/ah;Ljava/lang/String;)V
    //   3956: aload 21
    //   3958: aload_3
    //   3959: putfield 405	com/inmobi/ads/ag:g	Ljava/lang/String;
    //   3962: aload 21
    //   3964: astore 23
    //   3966: aload 6
    //   3968: astore 14
    //   3970: aload 8
    //   3972: astore 35
    //   3974: goto +1520 -> 5494
    //   3977: iload 20
    //   3979: istore 31
    //   3981: iload 18
    //   3983: istore 32
    //   3985: aload 6
    //   3987: astore 41
    //   3989: aload 24
    //   3991: astore 33
    //   3993: iload 26
    //   3995: istore 34
    //   3997: aload_3
    //   3998: astore 7
    //   4000: aload_0
    //   4001: astore 21
    //   4003: aload 10
    //   4005: astore 29
    //   4007: aload_0
    //   4008: aload 13
    //   4010: aload 15
    //   4012: aload 14
    //   4014: aload 16
    //   4016: aload 10
    //   4018: invokespecial 582	com/inmobi/ads/ak:b	(Landroid/graphics/Point;Landroid/graphics/Point;Landroid/graphics/Point;Landroid/graphics/Point;Lorg/json/JSONObject;)Lcom/inmobi/ads/aw$a;
    //   4021: astore 21
    //   4023: new 584	com/inmobi/ads/aw
    //   4026: astore 13
    //   4028: aload 13
    //   4030: aload 8
    //   4032: aload 9
    //   4034: aload 21
    //   4036: aload 23
    //   4038: invokespecial 585	com/inmobi/ads/aw:<init>	(Ljava/lang/String;Ljava/lang/String;Lcom/inmobi/ads/ah;Ljava/lang/String;)V
    //   4041: aload 13
    //   4043: aload_3
    //   4044: putfield 405	com/inmobi/ads/ag:g	Ljava/lang/String;
    //   4047: aload 13
    //   4049: astore 23
    //   4051: aload 6
    //   4053: astore 14
    //   4055: aload 8
    //   4057: astore 35
    //   4059: goto +1435 -> 5494
    //   4062: iload 20
    //   4064: istore 31
    //   4066: iload 18
    //   4068: istore 32
    //   4070: aload 6
    //   4072: astore 41
    //   4074: aload 24
    //   4076: astore 33
    //   4078: iload 26
    //   4080: istore 34
    //   4082: iconst_1
    //   4083: istore 20
    //   4085: aload_3
    //   4086: astore 7
    //   4088: iconst_0
    //   4089: istore 43
    //   4091: aconst_null
    //   4092: astore 6
    //   4094: aload_0
    //   4095: astore 21
    //   4097: aload 10
    //   4099: astore 29
    //   4101: aload_0
    //   4102: aload 13
    //   4104: aload 15
    //   4106: aload 14
    //   4108: aload 16
    //   4110: aload 10
    //   4112: invokespecial 507	com/inmobi/ads/ak:a	(Landroid/graphics/Point;Landroid/graphics/Point;Landroid/graphics/Point;Landroid/graphics/Point;Lorg/json/JSONObject;)Lcom/inmobi/ads/ah;
    //   4115: astore 23
    //   4117: aload_1
    //   4118: invokestatic 378	com/inmobi/ads/ak:p	(Lorg/json/JSONObject;)Z
    //   4121: istore 25
    //   4123: iload 25
    //   4125: ifeq +114 -> 4239
    //   4128: ldc_w 383
    //   4131: astore 21
    //   4133: aload 5
    //   4135: aload 21
    //   4137: invokevirtual 264	org/json/JSONObject:getJSONObject	(Ljava/lang/String;)Lorg/json/JSONObject;
    //   4140: astore 21
    //   4142: ldc_w 385
    //   4145: astore 13
    //   4147: aload 21
    //   4149: aload 13
    //   4151: invokevirtual 137	org/json/JSONObject:isNull	(Ljava/lang/String;)Z
    //   4154: istore 25
    //   4156: iload 25
    //   4158: ifne +69 -> 4227
    //   4161: ldc_w 383
    //   4164: astore 21
    //   4166: aload 5
    //   4168: aload 21
    //   4170: invokevirtual 264	org/json/JSONObject:getJSONObject	(Ljava/lang/String;)Lorg/json/JSONObject;
    //   4173: astore 21
    //   4175: ldc_w 385
    //   4178: astore 13
    //   4180: aload 21
    //   4182: aload 13
    //   4184: invokevirtual 200	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   4187: astore 21
    //   4189: aload 21
    //   4191: invokestatic 387	com/inmobi/ads/ak:d	(Ljava/lang/String;)I
    //   4194: istore 25
    //   4196: ldc_w 383
    //   4199: astore 13
    //   4201: aload 5
    //   4203: aload 13
    //   4205: invokevirtual 264	org/json/JSONObject:getJSONObject	(Ljava/lang/String;)Lorg/json/JSONObject;
    //   4208: astore 13
    //   4210: ldc_w 389
    //   4213: astore 15
    //   4215: aload 13
    //   4217: aload 15
    //   4219: invokevirtual 392	org/json/JSONObject:optString	(Ljava/lang/String;)Ljava/lang/String;
    //   4222: astore 13
    //   4224: goto +27 -> 4251
    //   4227: iconst_0
    //   4228: istore 36
    //   4230: aconst_null
    //   4231: astore 13
    //   4233: iconst_2
    //   4234: istore 25
    //   4236: goto +15 -> 4251
    //   4239: iconst_0
    //   4240: istore 36
    //   4242: aconst_null
    //   4243: astore 13
    //   4245: iconst_0
    //   4246: istore 25
    //   4248: aconst_null
    //   4249: astore 21
    //   4251: ldc_w 587
    //   4254: astore 15
    //   4256: aload 10
    //   4258: aload 15
    //   4260: invokevirtual 539	org/json/JSONObject:has	(Ljava/lang/String;)Z
    //   4263: istore 37
    //   4265: iload 37
    //   4267: ifeq +136 -> 4403
    //   4270: ldc_w 587
    //   4273: astore 15
    //   4275: aload 10
    //   4277: aload 15
    //   4279: invokevirtual 200	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   4282: astore 15
    //   4284: aload 15
    //   4286: invokevirtual 103	java/lang/String:trim	()Ljava/lang/String;
    //   4289: astore 15
    //   4291: aload 15
    //   4293: invokevirtual 107	java/lang/String:hashCode	()I
    //   4296: istore 38
    //   4298: ldc_w 588
    //   4301: istore 39
    //   4303: iload 38
    //   4305: iload 39
    //   4307: if_icmpeq +43 -> 4350
    //   4310: ldc_w 590
    //   4313: istore 39
    //   4315: iload 38
    //   4317: iload 39
    //   4319: if_icmpeq +6 -> 4325
    //   4322: goto +53 -> 4375
    //   4325: ldc_w 593
    //   4328: astore 14
    //   4330: aload 15
    //   4332: aload 14
    //   4334: invokevirtual 119	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   4337: istore 37
    //   4339: iload 37
    //   4341: ifeq +34 -> 4375
    //   4344: iconst_1
    //   4345: istore 37
    //   4347: goto +31 -> 4378
    //   4350: ldc_w 595
    //   4353: astore 14
    //   4355: aload 15
    //   4357: aload 14
    //   4359: invokevirtual 119	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   4362: istore 37
    //   4364: iload 37
    //   4366: ifeq +9 -> 4375
    //   4369: iconst_2
    //   4370: istore 37
    //   4372: goto +6 -> 4378
    //   4375: iconst_m1
    //   4376: istore 37
    //   4378: iconst_2
    //   4379: istore 38
    //   4381: iload 37
    //   4383: iload 38
    //   4385: if_icmpeq +12 -> 4397
    //   4388: iconst_0
    //   4389: istore 37
    //   4391: aconst_null
    //   4392: astore 15
    //   4394: goto +18 -> 4412
    //   4397: iconst_1
    //   4398: istore 37
    //   4400: goto +12 -> 4412
    //   4403: iconst_2
    //   4404: istore 38
    //   4406: iconst_0
    //   4407: istore 37
    //   4409: aconst_null
    //   4410: astore 15
    //   4412: aload 17
    //   4414: ifnull +115 -> 4529
    //   4417: aload 17
    //   4419: invokeinterface 395 1 0
    //   4424: istore 39
    //   4426: iload 39
    //   4428: ifne +6 -> 4434
    //   4431: goto +98 -> 4529
    //   4434: new 597	com/inmobi/ads/ai
    //   4437: astore 16
    //   4439: aload 16
    //   4441: astore 10
    //   4443: iconst_1
    //   4444: istore 19
    //   4446: aload 8
    //   4448: astore 40
    //   4450: aload 41
    //   4452: astore 14
    //   4454: aload 23
    //   4456: astore 41
    //   4458: aload 8
    //   4460: astore 35
    //   4462: aload 17
    //   4464: astore 8
    //   4466: aload 7
    //   4468: astore 29
    //   4470: iconst_2
    //   4471: istore 50
    //   4473: iload 25
    //   4475: istore 28
    //   4477: iconst_0
    //   4478: istore 25
    //   4480: aconst_null
    //   4481: astore 21
    //   4483: aload_1
    //   4484: astore 6
    //   4486: aload 16
    //   4488: astore 17
    //   4490: aload 5
    //   4492: astore 16
    //   4494: iload 37
    //   4496: istore 55
    //   4498: aload 10
    //   4500: aload 40
    //   4502: aload 9
    //   4504: aload 23
    //   4506: aload 8
    //   4508: iload 28
    //   4510: aload_1
    //   4511: iload 37
    //   4513: invokespecial 600	com/inmobi/ads/ai:<init>	(Ljava/lang/String;Ljava/lang/String;Lcom/inmobi/ads/ah;Ljava/util/List;ILorg/json/JSONObject;I)V
    //   4516: aload 10
    //   4518: astore 23
    //   4520: iconst_0
    //   4521: istore 55
    //   4523: aconst_null
    //   4524: astore 5
    //   4526: goto +76 -> 4602
    //   4529: aload 41
    //   4531: astore 14
    //   4533: aload 8
    //   4535: astore 35
    //   4537: aload 7
    //   4539: astore 29
    //   4541: aload 5
    //   4543: astore 16
    //   4545: iconst_0
    //   4546: istore 55
    //   4548: aconst_null
    //   4549: astore 5
    //   4551: iconst_2
    //   4552: istore 50
    //   4554: new 597	com/inmobi/ads/ai
    //   4557: astore 17
    //   4559: aload 17
    //   4561: astore 10
    //   4563: aload 8
    //   4565: astore 40
    //   4567: aload 23
    //   4569: astore 41
    //   4571: iload 25
    //   4573: istore 54
    //   4575: aload_1
    //   4576: astore 7
    //   4578: iload 37
    //   4580: istore 43
    //   4582: aload 17
    //   4584: aload 8
    //   4586: aload 9
    //   4588: aload 23
    //   4590: iload 25
    //   4592: aload_1
    //   4593: iload 37
    //   4595: invokespecial 603	com/inmobi/ads/ai:<init>	(Ljava/lang/String;Ljava/lang/String;Lcom/inmobi/ads/ah;ILorg/json/JSONObject;I)V
    //   4598: aload 17
    //   4600: astore 23
    //   4602: aload 23
    //   4604: aload 29
    //   4606: putfield 405	com/inmobi/ads/ag:g	Ljava/lang/String;
    //   4609: aload 13
    //   4611: ifnull +10 -> 4621
    //   4614: aload 23
    //   4616: aload 13
    //   4618: invokevirtual 604	com/inmobi/ads/ai:b	(Ljava/lang/String;)V
    //   4621: aload 23
    //   4623: aload 16
    //   4625: invokestatic 408	com/inmobi/ads/ak:a	(Lcom/inmobi/ads/ag;Lorg/json/JSONObject;)V
    //   4628: ldc_w 606
    //   4631: astore 21
    //   4633: aload 16
    //   4635: aload 21
    //   4637: invokevirtual 179	org/json/JSONObject:getJSONArray	(Ljava/lang/String;)Lorg/json/JSONArray;
    //   4640: astore 21
    //   4642: iconst_0
    //   4643: istore 36
    //   4645: aconst_null
    //   4646: astore 13
    //   4648: aload 21
    //   4650: invokevirtual 228	org/json/JSONArray:length	()I
    //   4653: istore 37
    //   4655: iload 36
    //   4657: iload 37
    //   4659: if_icmpge +835 -> 5494
    //   4662: new 608	java/lang/StringBuilder
    //   4665: astore 15
    //   4667: aload 15
    //   4669: invokespecial 609	java/lang/StringBuilder:<init>	()V
    //   4672: aload 15
    //   4674: aload 29
    //   4676: invokevirtual 613	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4679: pop
    //   4680: ldc_w 615
    //   4683: astore 16
    //   4685: aload 15
    //   4687: aload 16
    //   4689: invokevirtual 613	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4692: pop
    //   4693: aload 15
    //   4695: iload 36
    //   4697: invokevirtual 618	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   4700: pop
    //   4701: ldc_w 620
    //   4704: astore 16
    //   4706: aload 15
    //   4708: aload 16
    //   4710: invokevirtual 613	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4713: pop
    //   4714: aload 15
    //   4716: invokevirtual 623	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   4719: astore 15
    //   4721: aload 21
    //   4723: iload 36
    //   4725: invokevirtual 626	org/json/JSONArray:getJSONObject	(I)Lorg/json/JSONObject;
    //   4728: astore 16
    //   4730: aload 16
    //   4732: invokestatic 628	com/inmobi/ads/ak:f	(Lorg/json/JSONObject;)Ljava/lang/String;
    //   4735: astore 10
    //   4737: getstatic 93	java/util/Locale:US	Ljava/util/Locale;
    //   4740: astore 40
    //   4742: aload 10
    //   4744: aload 40
    //   4746: invokevirtual 99	java/lang/String:toLowerCase	(Ljava/util/Locale;)Ljava/lang/String;
    //   4749: astore 10
    //   4751: aload 10
    //   4753: invokevirtual 103	java/lang/String:trim	()Ljava/lang/String;
    //   4756: astore 10
    //   4758: aload 10
    //   4760: invokevirtual 107	java/lang/String:hashCode	()I
    //   4763: istore 20
    //   4765: iload 20
    //   4767: lookupswitch	default:+89->4856, -938102371:+320->5087, -410956671:+295->5062, 98832:+269->5036, 102340:+243->5010, 3226745:+218->4985, 3556653:+193->4960, 100313435:+168->4935, 110364485:+142->4909, 112202875:+118->4885, 1224424441:+92->4859
    //   4856: goto +257 -> 5113
    //   4859: ldc_w 630
    //   4862: astore 40
    //   4864: aload 10
    //   4866: aload 40
    //   4868: invokevirtual 119	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   4871: istore 46
    //   4873: iload 46
    //   4875: ifeq +238 -> 5113
    //   4878: bipush 9
    //   4880: istore 20
    //   4882: goto +234 -> 5116
    //   4885: ldc 115
    //   4887: astore 40
    //   4889: aload 10
    //   4891: aload 40
    //   4893: invokevirtual 119	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   4896: istore 46
    //   4898: iload 46
    //   4900: ifeq +213 -> 5113
    //   4903: iconst_4
    //   4904: istore 20
    //   4906: goto +210 -> 5116
    //   4909: ldc_w 632
    //   4912: astore 40
    //   4914: aload 10
    //   4916: aload 40
    //   4918: invokevirtual 119	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   4921: istore 46
    //   4923: iload 46
    //   4925: ifeq +188 -> 5113
    //   4928: bipush 8
    //   4930: istore 20
    //   4932: goto +184 -> 5116
    //   4935: ldc_w 634
    //   4938: astore 40
    //   4940: aload 10
    //   4942: aload 40
    //   4944: invokevirtual 119	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   4947: istore 46
    //   4949: iload 46
    //   4951: ifeq +162 -> 5113
    //   4954: iconst_3
    //   4955: istore 20
    //   4957: goto +159 -> 5116
    //   4960: ldc_w 636
    //   4963: astore 40
    //   4965: aload 10
    //   4967: aload 40
    //   4969: invokevirtual 119	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   4972: istore 46
    //   4974: iload 46
    //   4976: ifeq +137 -> 5113
    //   4979: iconst_5
    //   4980: istore 20
    //   4982: goto +134 -> 5116
    //   4985: ldc_w 638
    //   4988: astore 40
    //   4990: aload 10
    //   4992: aload 40
    //   4994: invokevirtual 119	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   4997: istore 46
    //   4999: iload 46
    //   5001: ifeq +112 -> 5113
    //   5004: iconst_2
    //   5005: istore 20
    //   5007: goto +109 -> 5116
    //   5010: ldc_w 640
    //   5013: astore 40
    //   5015: aload 10
    //   5017: aload 40
    //   5019: invokevirtual 119	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   5022: istore 46
    //   5024: iload 46
    //   5026: ifeq +87 -> 5113
    //   5029: bipush 10
    //   5031: istore 20
    //   5033: goto +83 -> 5116
    //   5036: ldc_w 643
    //   5039: astore 40
    //   5041: aload 10
    //   5043: aload 40
    //   5045: invokevirtual 119	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   5048: istore 46
    //   5050: iload 46
    //   5052: ifeq +61 -> 5113
    //   5055: bipush 6
    //   5057: istore 20
    //   5059: goto +57 -> 5116
    //   5062: ldc_w 645
    //   5065: astore 40
    //   5067: aload 10
    //   5069: aload 40
    //   5071: invokevirtual 119	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   5074: istore 46
    //   5076: iload 46
    //   5078: ifeq +35 -> 5113
    //   5081: iconst_1
    //   5082: istore 20
    //   5084: goto +32 -> 5116
    //   5087: ldc_w 647
    //   5090: astore 40
    //   5092: aload 10
    //   5094: aload 40
    //   5096: invokevirtual 119	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   5099: istore 46
    //   5101: iload 46
    //   5103: ifeq +10 -> 5113
    //   5106: bipush 7
    //   5108: istore 20
    //   5110: goto +6 -> 5116
    //   5113: iconst_m1
    //   5114: istore 20
    //   5116: iload 20
    //   5118: tableswitch	default:+50->5168, 2:+122->5240, 3:+114->5232, 4:+106->5224, 5:+98->5216, 6:+90->5208, 7:+82->5200, 8:+74->5192, 9:+66->5184, 10:+58->5176
    //   5168: ldc_w 375
    //   5171: astore 10
    //   5173: goto +72 -> 5245
    //   5176: ldc_w 369
    //   5179: astore 10
    //   5181: goto +64 -> 5245
    //   5184: ldc_w 357
    //   5187: astore 10
    //   5189: goto +56 -> 5245
    //   5192: ldc_w 361
    //   5195: astore 10
    //   5197: goto +48 -> 5245
    //   5200: ldc_w 373
    //   5203: astore 10
    //   5205: goto +40 -> 5245
    //   5208: ldc_w 371
    //   5211: astore 10
    //   5213: goto +32 -> 5245
    //   5216: ldc_w 365
    //   5219: astore 10
    //   5221: goto +24 -> 5245
    //   5224: ldc_w 359
    //   5227: astore 10
    //   5229: goto +16 -> 5245
    //   5232: ldc_w 363
    //   5235: astore 10
    //   5237: goto +8 -> 5245
    //   5240: ldc_w 367
    //   5243: astore 10
    //   5245: aload 4
    //   5247: aload 16
    //   5249: aload 10
    //   5251: aload 15
    //   5253: invokespecial 650	com/inmobi/ads/ak:a	(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Lcom/inmobi/ads/ag;
    //   5256: astore 16
    //   5258: aload 16
    //   5260: ifnull +148 -> 5408
    //   5263: aload 16
    //   5265: aload 15
    //   5267: putfield 405	com/inmobi/ads/ag:g	Ljava/lang/String;
    //   5270: aload 16
    //   5272: aload 23
    //   5274: putfield 652	com/inmobi/ads/ag:t	Lcom/inmobi/ads/ag;
    //   5277: aload 23
    //   5279: getfield 655	com/inmobi/ads/ai:C	I
    //   5282: istore 37
    //   5284: bipush 16
    //   5286: istore 46
    //   5288: iload 37
    //   5290: iload 46
    //   5292: if_icmpge +116 -> 5408
    //   5295: aload 23
    //   5297: getfield 655	com/inmobi/ads/ai:C	I
    //   5300: istore 37
    //   5302: aload 23
    //   5304: getfield 660	com/inmobi/ads/ai:B	[Lcom/inmobi/ads/ag;
    //   5307: astore 10
    //   5309: aload 10
    //   5311: arraylength
    //   5312: istore 46
    //   5314: iload 37
    //   5316: iload 46
    //   5318: if_icmpne +56 -> 5374
    //   5321: aload 23
    //   5323: getfield 660	com/inmobi/ads/ai:B	[Lcom/inmobi/ads/ag;
    //   5326: astore 15
    //   5328: aload 15
    //   5330: arraylength
    //   5331: iconst_2
    //   5332: imul
    //   5333: istore 37
    //   5335: iload 37
    //   5337: anewarray 288	com/inmobi/ads/ag
    //   5340: astore 15
    //   5342: aload 23
    //   5344: getfield 660	com/inmobi/ads/ai:B	[Lcom/inmobi/ads/ag;
    //   5347: astore 10
    //   5349: aload 23
    //   5351: getfield 655	com/inmobi/ads/ai:C	I
    //   5354: istore 20
    //   5356: aload 10
    //   5358: iconst_0
    //   5359: aload 15
    //   5361: iconst_0
    //   5362: iload 20
    //   5364: invokestatic 666	java/lang/System:arraycopy	(Ljava/lang/Object;ILjava/lang/Object;II)V
    //   5367: aload 23
    //   5369: aload 15
    //   5371: putfield 660	com/inmobi/ads/ai:B	[Lcom/inmobi/ads/ag;
    //   5374: aload 23
    //   5376: getfield 660	com/inmobi/ads/ai:B	[Lcom/inmobi/ads/ag;
    //   5379: astore 15
    //   5381: aload 23
    //   5383: getfield 655	com/inmobi/ads/ai:C	I
    //   5386: istore 46
    //   5388: iload 46
    //   5390: iconst_1
    //   5391: iadd
    //   5392: istore 20
    //   5394: aload 23
    //   5396: iload 20
    //   5398: putfield 655	com/inmobi/ads/ai:C	I
    //   5401: aload 15
    //   5403: iload 46
    //   5405: aload 16
    //   5407: aastore
    //   5408: iload 36
    //   5410: iconst_1
    //   5411: iadd
    //   5412: istore 36
    //   5414: goto -766 -> 4648
    //   5417: astore 23
    //   5419: goto +45 -> 5464
    //   5422: astore 23
    //   5424: aload 41
    //   5426: astore 14
    //   5428: goto +32 -> 5460
    //   5431: aload 12
    //   5433: astore 23
    //   5435: goto +59 -> 5494
    //   5438: astore 23
    //   5440: iload 20
    //   5442: istore 31
    //   5444: iload 18
    //   5446: istore 32
    //   5448: aload 6
    //   5450: astore 14
    //   5452: aload 24
    //   5454: astore 33
    //   5456: iload 26
    //   5458: istore 34
    //   5460: aload 8
    //   5462: astore 35
    //   5464: invokestatic 158	com/inmobi/commons/core/a/a:a	()Lcom/inmobi/commons/core/a/a;
    //   5467: astore 21
    //   5469: new 160	com/inmobi/commons/core/e/a
    //   5472: astore 13
    //   5474: aload 13
    //   5476: aload 23
    //   5478: invokespecial 163	com/inmobi/commons/core/e/a:<init>	(Ljava/lang/Throwable;)V
    //   5481: aload 21
    //   5483: aload 13
    //   5485: invokevirtual 166	com/inmobi/commons/core/a/a:a	(Lcom/inmobi/commons/core/e/a;)V
    //   5488: iconst_0
    //   5489: istore 11
    //   5491: aconst_null
    //   5492: astore 23
    //   5494: aload 23
    //   5496: ifnull +240 -> 5736
    //   5499: iload 32
    //   5501: istore 25
    //   5503: aload 23
    //   5505: iload 32
    //   5507: putfield 668	com/inmobi/ads/ag:n	I
    //   5510: iload 34
    //   5512: istore 25
    //   5514: aload 23
    //   5516: iload 34
    //   5518: putfield 670	com/inmobi/ads/ag:o	I
    //   5521: iload 31
    //   5523: istore 25
    //   5525: aload 23
    //   5527: iload 31
    //   5529: putfield 672	com/inmobi/ads/ag:p	I
    //   5532: aload 33
    //   5534: astore 21
    //   5536: aload 23
    //   5538: aload 33
    //   5540: putfield 674	com/inmobi/ads/ag:q	Ljava/lang/String;
    //   5543: aload 33
    //   5545: ifnull +41 -> 5586
    //   5548: aload 33
    //   5550: invokevirtual 214	java/lang/String:length	()I
    //   5553: istore 36
    //   5555: iload 36
    //   5557: ifeq +29 -> 5586
    //   5560: aload 4
    //   5562: getfield 71	com/inmobi/ads/ak:p	Ljava/util/Map;
    //   5565: astore 13
    //   5567: aload 35
    //   5569: astore 15
    //   5571: aload 13
    //   5573: aload 35
    //   5575: aload 33
    //   5577: invokeinterface 258 3 0
    //   5582: pop
    //   5583: goto +7 -> 5590
    //   5586: aload 35
    //   5588: astore 15
    //   5590: aload 15
    //   5592: invokevirtual 214	java/lang/String:length	()I
    //   5595: istore 25
    //   5597: iload 25
    //   5599: ifeq +45 -> 5644
    //   5602: aload 4
    //   5604: getfield 69	com/inmobi/ads/ak:o	Ljava/util/Map;
    //   5607: astore 21
    //   5609: aload 21
    //   5611: aload 15
    //   5613: invokeinterface 677 2 0
    //   5618: istore 25
    //   5620: iload 25
    //   5622: ifne +22 -> 5644
    //   5625: aload 4
    //   5627: getfield 69	com/inmobi/ads/ak:o	Ljava/util/Map;
    //   5630: astore 21
    //   5632: aload 21
    //   5634: aload 15
    //   5636: aload 23
    //   5638: invokeinterface 258 3 0
    //   5643: pop
    //   5644: aload 4
    //   5646: getfield 73	com/inmobi/ads/ak:h	Ljava/util/Map;
    //   5649: astore 21
    //   5651: aload 21
    //   5653: aload 14
    //   5655: invokeinterface 677 2 0
    //   5660: istore 25
    //   5662: iload 25
    //   5664: ifeq +33 -> 5697
    //   5667: aload 4
    //   5669: getfield 73	com/inmobi/ads/ak:h	Ljava/util/Map;
    //   5672: aload 14
    //   5674: invokeinterface 415 2 0
    //   5679: checkcast 247	java/util/List
    //   5682: astore 21
    //   5684: aload 21
    //   5686: aload 23
    //   5688: invokeinterface 250 2 0
    //   5693: pop
    //   5694: goto +42 -> 5736
    //   5697: new 226	java/util/ArrayList
    //   5700: astore 21
    //   5702: aload 21
    //   5704: invokespecial 227	java/util/ArrayList:<init>	()V
    //   5707: aload 21
    //   5709: aload 23
    //   5711: invokeinterface 250 2 0
    //   5716: pop
    //   5717: aload 4
    //   5719: getfield 73	com/inmobi/ads/ak:h	Ljava/util/Map;
    //   5722: astore 13
    //   5724: aload 13
    //   5726: aload 14
    //   5728: aload 21
    //   5730: invokeinterface 258 3 0
    //   5735: pop
    //   5736: aload 23
    //   5738: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	5739	0	this	ak
    //   0	5739	1	paramJSONObject	JSONObject
    //   0	5739	2	paramString1	String
    //   0	5739	3	paramString2	String
    //   1	5717	4	localObject1	Object
    //   4	4546	5	localObject2	Object
    //   7	5442	6	localObject3	Object
    //   10	4567	7	localObject4	Object
    //   16	5445	8	localObject5	Object
    //   22	4565	9	str	String
    //   29	5328	10	localObject6	Object
    //   37	177	11	bool1	boolean
    //   219	1160	11	i1	int
    //   2818	165	11	bool2	boolean
    //   3020	64	11	i2	int
    //   3105	723	11	bool3	boolean
    //   3833	1657	11	i3	int
    //   40	5392	12	localObject7	Object
    //   54	5671	13	localObject8	Object
    //   63	5664	14	localObject9	Object
    //   70	5565	15	localObject10	Object
    //   79	5327	16	localObject11	Object
    //   85	4514	17	localObject12	Object
    //   91	5354	18	i4	int
    //   98	173	19	i5	int
    //   288	17	19	i6	int
    //   393	4052	19	bool4	boolean
    //   105	1418	20	i7	int
    //   1766	286	20	i8	int
    //   2152	1911	20	i9	int
    //   4083	1358	20	i10	int
    //   111	5618	21	localObject13	Object
    //   115	2307	22	localObject14	Object
    //   124	224	23	localObject15	Object
    //   352	1	23	localJSONException1	JSONException
    //   359	18	23	localObject16	Object
    //   381	1	23	localJSONException2	JSONException
    //   386	29	23	localJSONException3	JSONException
    //   428	8	23	localObject17	Object
    //   1034	124	23	localJSONException4	JSONException
    //   1173	1	23	localObject18	Object
    //   1182	65	23	localJSONException5	JSONException
    //   1256	29	23	localObject19	Object
    //   1306	1	23	localJSONException6	JSONException
    //   1323	1	23	localJSONException7	JSONException
    //   1332	1	23	localJSONException8	JSONException
    //   1381	182	23	localObject20	Object
    //   1567	1	23	localJSONException9	JSONException
    //   1589	956	23	localObject21	Object
    //   2567	1	23	localJSONException10	JSONException
    //   2580	1	23	localJSONException11	JSONException
    //   2593	92	23	localJSONException12	JSONException
    //   2698	88	23	localJSONException13	JSONException
    //   2809	38	23	localObject22	Object
    //   2867	1	23	localJSONException14	JSONException
    //   2875	1	23	localJSONException15	JSONException
    //   2957	257	23	localObject23	Object
    //   3234	1	23	localJSONException16	JSONException
    //   3265	140	23	localObject24	Object
    //   3419	1	23	localJSONException17	JSONException
    //   3432	1	23	localJSONException18	JSONException
    //   3498	1897	23	localObject25	Object
    //   5417	1	23	localJSONException19	JSONException
    //   5422	1	23	localJSONException20	JSONException
    //   5433	1	23	localObject26	Object
    //   5438	39	23	localJSONException21	JSONException
    //   5492	245	23	localObject27	Object
    //   128	5325	24	localObject28	Object
    //   135	319	25	i11	int
    //   559	3	25	bool5	boolean
    //   568	1	25	i12	int
    //   585	3	25	bool6	boolean
    //   594	1	25	i13	int
    //   611	3	25	bool7	boolean
    //   619	1	25	i14	int
    //   636	3	25	bool8	boolean
    //   644	1	25	i15	int
    //   661	28	25	bool9	boolean
    //   694	1	25	i16	int
    //   711	3	25	bool10	boolean
    //   719	1	25	i17	int
    //   736	3	25	bool11	boolean
    //   745	1	25	i18	int
    //   762	3	25	bool12	boolean
    //   771	1	25	i19	int
    //   788	9	25	bool13	boolean
    //   805	3	25	i20	int
    //   895	3262	25	bool14	boolean
    //   4194	1404	25	i21	int
    //   5618	45	25	bool15	boolean
    //   139	5318	26	i22	int
    //   147	1	27	i23	int
    //   2263	124	27	bool16	boolean
    //   150	1331	28	i24	int
    //   1489	205	28	i25	int
    //   1782	2727	28	i26	int
    //   273	4402	29	localObject29	Object
    //   451	2136	30	i27	int
    //   866	4662	31	i28	int
    //   870	4636	32	i29	int
    //   878	4698	33	localObject30	Object
    //   882	4635	34	i30	int
    //   886	4701	35	localObject31	Object
    //   961	3	36	bool17	boolean
    //   1001	520	36	i31	int
    //   1653	15	36	bool18	boolean
    //   1678	1	36	i32	int
    //   1731	256	36	i33	int
    //   2011	25	36	i34	int
    //   2064	96	36	i35	int
    //   2184	321	36	i36	int
    //   3563	152	36	bool19	boolean
    //   3759	1797	36	i37	int
    //   1060	659	37	i38	int
    //   1750	1906	37	i39	int
    //   3764	19	37	i40	int
    //   4263	102	37	bool20	boolean
    //   4370	966	37	i41	int
    //   1080	1047	38	i42	int
    //   2131	315	38	i43	int
    //   4296	109	38	i44	int
    //   1101	923	39	i45	int
    //   2099	455	39	i46	int
    //   4301	126	39	i47	int
    //   1105	3990	40	localObject32	Object
    //   1113	4312	41	localObject33	Object
    //   1125	1290	42	i48	int
    //   1140	814	43	i49	int
    //   1979	621	43	i50	int
    //   2708	1873	43	i51	int
    //   1213	495	44	localaj	aj
    //   1445	90	45	localax	ax
    //   1457	337	46	i52	int
    //   1798	3304	46	i53	int
    //   5286	118	46	i54	int
    //   1500	1074	47	i55	int
    //   1604	235	48	d1	double
    //   1704	2849	50	i56	int
    //   1995	187	51	d2	double
    //   2277	104	53	localObject34	Object
    //   2362	2212	54	i57	int
    //   2410	2087	55	i58	int
    //   4521	26	55	i59	int
    //   2910	83	56	localObject35	Object
    // Exception table:
    //   from	to	target	type
    //   321	326	352	org/json/JSONException
    //   333	338	352	org/json/JSONException
    //   342	347	352	org/json/JSONException
    //   283	288	381	org/json/JSONException
    //   309	314	381	org/json/JSONException
    //   277	281	386	org/json/JSONException
    //   975	980	1034	org/json/JSONException
    //   989	994	1034	org/json/JSONException
    //   996	1001	1034	org/json/JSONException
    //   1010	1015	1034	org/json/JSONException
    //   1024	1029	1034	org/json/JSONException
    //   1163	1167	1182	org/json/JSONException
    //   1210	1213	1306	org/json/JSONException
    //   1250	1254	1306	org/json/JSONException
    //   1260	1265	1306	org/json/JSONException
    //   1267	1272	1306	org/json/JSONException
    //   1279	1284	1306	org/json/JSONException
    //   928	933	1323	org/json/JSONException
    //   942	947	1323	org/json/JSONException
    //   956	961	1323	org/json/JSONException
    //   1073	1080	1323	org/json/JSONException
    //   1090	1093	1323	org/json/JSONException
    //   891	895	1332	org/json/JSONException
    //   1557	1562	1567	org/json/JSONException
    //   1618	1623	1567	org/json/JSONException
    //   1632	1639	1567	org/json/JSONException
    //   1641	1646	1567	org/json/JSONException
    //   1648	1653	1567	org/json/JSONException
    //   1660	1665	1567	org/json/JSONException
    //   1727	1731	1567	org/json/JSONException
    //   1745	1750	1567	org/json/JSONException
    //   1761	1766	1567	org/json/JSONException
    //   1777	1782	1567	org/json/JSONException
    //   1793	1798	1567	org/json/JSONException
    //   1804	1809	1567	org/json/JSONException
    //   1811	1816	1567	org/json/JSONException
    //   1831	1836	1567	org/json/JSONException
    //   1883	1888	1567	org/json/JSONException
    //   1905	1909	1567	org/json/JSONException
    //   1920	1924	1567	org/json/JSONException
    //   1935	1939	1567	org/json/JSONException
    //   1953	1958	1567	org/json/JSONException
    //   1974	1979	1567	org/json/JSONException
    //   2001	2006	1567	org/json/JSONException
    //   2242	2247	1567	org/json/JSONException
    //   2249	2254	1567	org/json/JSONException
    //   2256	2263	1567	org/json/JSONException
    //   2270	2277	1567	org/json/JSONException
    //   2283	2288	1567	org/json/JSONException
    //   2296	2301	1567	org/json/JSONException
    //   2307	2315	1567	org/json/JSONException
    //   1502	1507	2567	org/json/JSONException
    //   1509	1514	2567	org/json/JSONException
    //   1534	1539	2567	org/json/JSONException
    //   1539	1544	2567	org/json/JSONException
    //   1584	1589	2567	org/json/JSONException
    //   1591	1594	2567	org/json/JSONException
    //   1596	1601	2567	org/json/JSONException
    //   2060	2064	2567	org/json/JSONException
    //   2078	2083	2567	org/json/JSONException
    //   2094	2099	2567	org/json/JSONException
    //   2110	2115	2567	org/json/JSONException
    //   2126	2131	2567	org/json/JSONException
    //   2147	2152	2567	org/json/JSONException
    //   2174	2179	2567	org/json/JSONException
    //   2206	2209	2567	org/json/JSONException
    //   2211	2216	2567	org/json/JSONException
    //   2223	2228	2567	org/json/JSONException
    //   2338	2341	2567	org/json/JSONException
    //   2343	2348	2567	org/json/JSONException
    //   2350	2355	2567	org/json/JSONException
    //   2357	2362	2567	org/json/JSONException
    //   1484	1489	2580	org/json/JSONException
    //   1491	1496	2580	org/json/JSONException
    //   1416	1421	2593	org/json/JSONException
    //   1430	1438	2593	org/json/JSONException
    //   1440	1445	2593	org/json/JSONException
    //   1447	1450	2593	org/json/JSONException
    //   1452	1457	2593	org/json/JSONException
    //   1459	1464	2593	org/json/JSONException
    //   1466	1471	2593	org/json/JSONException
    //   1473	1478	2593	org/json/JSONException
    //   2450	2455	2698	org/json/JSONException
    //   2455	2460	2698	org/json/JSONException
    //   2462	2465	2698	org/json/JSONException
    //   2469	2474	2698	org/json/JSONException
    //   2476	2481	2698	org/json/JSONException
    //   2485	2490	2698	org/json/JSONException
    //   2504	2509	2698	org/json/JSONException
    //   2511	2516	2698	org/json/JSONException
    //   2518	2523	2698	org/json/JSONException
    //   2530	2535	2698	org/json/JSONException
    //   2544	2549	2698	org/json/JSONException
    //   2553	2558	2698	org/json/JSONException
    //   2684	2689	2698	org/json/JSONException
    //   2733	2738	2867	org/json/JSONException
    //   2740	2743	2867	org/json/JSONException
    //   2754	2759	2867	org/json/JSONException
    //   2787	2792	2867	org/json/JSONException
    //   2794	2799	2867	org/json/JSONException
    //   2801	2806	2867	org/json/JSONException
    //   2814	2818	2867	org/json/JSONException
    //   2830	2835	2867	org/json/JSONException
    //   2652	2656	2875	org/json/JSONException
    //   2658	2663	2875	org/json/JSONException
    //   2672	2677	2875	org/json/JSONException
    //   3101	3105	3234	org/json/JSONException
    //   3112	3115	3234	org/json/JSONException
    //   3117	3121	3234	org/json/JSONException
    //   3168	3172	3419	org/json/JSONException
    //   3187	3190	3419	org/json/JSONException
    //   3192	3196	3419	org/json/JSONException
    //   3227	3231	3419	org/json/JSONException
    //   3269	3273	3419	org/json/JSONException
    //   3280	3283	3419	org/json/JSONException
    //   3285	3289	3419	org/json/JSONException
    //   3318	3322	3419	org/json/JSONException
    //   3325	3328	3419	org/json/JSONException
    //   3330	3334	3419	org/json/JSONException
    //   3363	3367	3419	org/json/JSONException
    //   3369	3374	3419	org/json/JSONException
    //   3376	3381	3419	org/json/JSONException
    //   3388	3393	3419	org/json/JSONException
    //   2839	2844	3432	org/json/JSONException
    //   2936	2941	3432	org/json/JSONException
    //   2943	2947	3432	org/json/JSONException
    //   2961	2966	3432	org/json/JSONException
    //   2975	2980	3432	org/json/JSONException
    //   2994	2999	3432	org/json/JSONException
    //   3008	3013	3432	org/json/JSONException
    //   3015	3020	3432	org/json/JSONException
    //   3035	3040	3432	org/json/JSONException
    //   3049	3054	3432	org/json/JSONException
    //   3074	3081	3432	org/json/JSONException
    //   4511	4516	5417	org/json/JSONException
    //   4554	4557	5417	org/json/JSONException
    //   4593	4598	5417	org/json/JSONException
    //   4604	4609	5417	org/json/JSONException
    //   4616	4621	5417	org/json/JSONException
    //   4623	4628	5417	org/json/JSONException
    //   4635	4640	5417	org/json/JSONException
    //   4648	4653	5417	org/json/JSONException
    //   4662	4665	5417	org/json/JSONException
    //   4667	4672	5417	org/json/JSONException
    //   4674	4680	5417	org/json/JSONException
    //   4687	4693	5417	org/json/JSONException
    //   4695	4701	5417	org/json/JSONException
    //   4708	4714	5417	org/json/JSONException
    //   4714	4719	5417	org/json/JSONException
    //   4723	4728	5417	org/json/JSONException
    //   4730	4735	5417	org/json/JSONException
    //   4737	4740	5417	org/json/JSONException
    //   4744	4749	5417	org/json/JSONException
    //   4751	4756	5417	org/json/JSONException
    //   4758	4763	5417	org/json/JSONException
    //   4866	4871	5417	org/json/JSONException
    //   4891	4896	5417	org/json/JSONException
    //   4916	4921	5417	org/json/JSONException
    //   4942	4947	5417	org/json/JSONException
    //   4967	4972	5417	org/json/JSONException
    //   4992	4997	5417	org/json/JSONException
    //   5017	5022	5417	org/json/JSONException
    //   5043	5048	5417	org/json/JSONException
    //   5069	5074	5417	org/json/JSONException
    //   5094	5099	5417	org/json/JSONException
    //   5251	5256	5417	org/json/JSONException
    //   5265	5270	5417	org/json/JSONException
    //   5272	5277	5417	org/json/JSONException
    //   5277	5282	5417	org/json/JSONException
    //   5295	5300	5417	org/json/JSONException
    //   5302	5307	5417	org/json/JSONException
    //   5309	5312	5417	org/json/JSONException
    //   5321	5326	5417	org/json/JSONException
    //   5328	5331	5417	org/json/JSONException
    //   5335	5340	5417	org/json/JSONException
    //   5342	5347	5417	org/json/JSONException
    //   5349	5354	5417	org/json/JSONException
    //   5362	5367	5417	org/json/JSONException
    //   5369	5374	5417	org/json/JSONException
    //   5374	5379	5417	org/json/JSONException
    //   5381	5386	5417	org/json/JSONException
    //   5396	5401	5417	org/json/JSONException
    //   5405	5408	5417	org/json/JSONException
    //   3493	3498	5422	org/json/JSONException
    //   3507	3512	5422	org/json/JSONException
    //   3526	3531	5422	org/json/JSONException
    //   3535	3540	5422	org/json/JSONException
    //   3558	3563	5422	org/json/JSONException
    //   3577	3582	5422	org/json/JSONException
    //   3586	3591	5422	org/json/JSONException
    //   3614	3619	5422	org/json/JSONException
    //   3621	3624	5422	org/json/JSONException
    //   3630	3635	5422	org/json/JSONException
    //   3635	3638	5422	org/json/JSONException
    //   3648	3653	5422	org/json/JSONException
    //   3655	3660	5422	org/json/JSONException
    //   3667	3672	5422	org/json/JSONException
    //   3686	3691	5422	org/json/JSONException
    //   3693	3698	5422	org/json/JSONException
    //   3707	3712	5422	org/json/JSONException
    //   3726	3731	5422	org/json/JSONException
    //   3733	3736	5422	org/json/JSONException
    //   3740	3745	5422	org/json/JSONException
    //   3747	3752	5422	org/json/JSONException
    //   3754	3759	5422	org/json/JSONException
    //   3795	3800	5422	org/json/JSONException
    //   3820	3825	5422	org/json/JSONException
    //   3859	3864	5422	org/json/JSONException
    //   3866	3871	5422	org/json/JSONException
    //   3925	3930	5422	org/json/JSONException
    //   3932	3935	5422	org/json/JSONException
    //   3937	3941	5422	org/json/JSONException
    //   3951	3956	5422	org/json/JSONException
    //   3958	3962	5422	org/json/JSONException
    //   4016	4021	5422	org/json/JSONException
    //   4023	4026	5422	org/json/JSONException
    //   4036	4041	5422	org/json/JSONException
    //   4043	4047	5422	org/json/JSONException
    //   4110	4115	5422	org/json/JSONException
    //   4117	4121	5422	org/json/JSONException
    //   4135	4140	5422	org/json/JSONException
    //   4149	4154	5422	org/json/JSONException
    //   4168	4173	5422	org/json/JSONException
    //   4182	4187	5422	org/json/JSONException
    //   4189	4194	5422	org/json/JSONException
    //   4203	4208	5422	org/json/JSONException
    //   4217	4222	5422	org/json/JSONException
    //   4258	4263	5422	org/json/JSONException
    //   4277	4282	5422	org/json/JSONException
    //   4284	4289	5422	org/json/JSONException
    //   4291	4296	5422	org/json/JSONException
    //   4332	4337	5422	org/json/JSONException
    //   4357	4362	5422	org/json/JSONException
    //   4417	4424	5422	org/json/JSONException
    //   4434	4437	5422	org/json/JSONException
    //   443	447	5438	org/json/JSONException
    //   554	559	5438	org/json/JSONException
    //   580	585	5438	org/json/JSONException
    //   606	611	5438	org/json/JSONException
    //   631	636	5438	org/json/JSONException
    //   656	661	5438	org/json/JSONException
    //   681	686	5438	org/json/JSONException
    //   706	711	5438	org/json/JSONException
    //   731	736	5438	org/json/JSONException
    //   757	762	5438	org/json/JSONException
    //   783	788	5438	org/json/JSONException
  }
  
  private ah a(Point paramPoint1, Point paramPoint2, Point paramPoint3, Point paramPoint4, JSONObject paramJSONObject)
  {
    Point localPoint1 = paramPoint1;
    Point localPoint2 = paramPoint2;
    Point localPoint3 = paramPoint3;
    Point localPoint4 = paramPoint4;
    Object localObject1 = paramJSONObject;
    Object localObject2 = "border";
    boolean bool1 = paramJSONObject.isNull((String)localObject2);
    String str2;
    Object localObject3;
    String str3;
    Object localObject4;
    if (bool1)
    {
      localObject2 = "none";
      str1 = "straight";
      str2 = "#ff000000";
      localObject3 = localObject2;
      str3 = str1;
      localObject4 = str2;
    }
    else
    {
      localObject2 = paramJSONObject.getJSONObject("border");
      str1 = "style";
      bool2 = ((JSONObject)localObject2).isNull(str1);
      if (bool2)
      {
        localObject2 = "none";
        str1 = "straight";
        str2 = "#ff000000";
        localObject3 = localObject2;
        str3 = str1;
        localObject4 = str2;
      }
      else
      {
        str1 = f(((JSONObject)localObject2).getString("style"));
        str2 = "corner";
        boolean bool3 = ((JSONObject)localObject2).isNull(str2);
        if (bool3) {
          str2 = "straight";
        } else {
          str2 = g(((JSONObject)localObject2).getString("corner"));
        }
        localObject5 = "color";
        boolean bool4 = ((JSONObject)localObject2).isNull((String)localObject5);
        if (bool4)
        {
          localObject2 = "#ff000000";
          localObject4 = localObject2;
          localObject3 = str1;
          str3 = str2;
        }
        else
        {
          localObject5 = "color";
          localObject2 = ((JSONObject)localObject2).getString((String)localObject5).trim();
          localObject4 = localObject2;
          localObject3 = str1;
          str3 = str2;
        }
      }
    }
    localObject2 = "backgroundColor";
    bool1 = ((JSONObject)localObject1).isNull((String)localObject2);
    Object localObject6;
    if (bool1)
    {
      localObject2 = "#00000000";
      localObject6 = localObject2;
    }
    else
    {
      localObject2 = ((JSONObject)localObject1).getString("backgroundColor").trim();
      localObject6 = localObject2;
    }
    localObject2 = "fill";
    String str1 = "contentMode";
    boolean bool2 = ((JSONObject)localObject1).isNull(str1);
    Object localObject7;
    if (!bool2)
    {
      localObject2 = ((JSONObject)localObject1).getString("contentMode").trim();
      int i1 = -1;
      int i2 = ((String)localObject2).hashCode();
      int i3 = -1626174665;
      if (i2 != i3)
      {
        i3 = -1362001767;
        if (i2 != i3)
        {
          i3 = 3143043;
          if (i2 != i3)
          {
            i3 = 727618043;
            if (i2 == i3)
            {
              str2 = "aspectFill";
              bool1 = ((String)localObject2).equals(str2);
              if (bool1) {
                i1 = 3;
              }
            }
          }
          else
          {
            str2 = "fill";
            bool1 = ((String)localObject2).equals(str2);
            if (bool1) {
              i1 = 2;
            }
          }
        }
        else
        {
          str2 = "aspectFit";
          bool1 = ((String)localObject2).equals(str2);
          if (bool1) {
            i1 = 4;
          }
        }
      }
      else
      {
        str2 = "unspecified";
        bool1 = ((String)localObject2).equals(str2);
        if (bool1) {
          i1 = 1;
        }
      }
      switch (i1)
      {
      default: 
        localObject2 = "unspecified";
        break;
      case 4: 
        localObject2 = "aspectFit";
        break;
      case 3: 
        localObject2 = "aspectFill";
        break;
      case 2: 
        localObject2 = "fill";
      }
      localObject7 = localObject2;
      localObject2 = this;
    }
    else
    {
      localObject7 = localObject2;
      localObject2 = this;
    }
    ax localax = ((ak)localObject2).s((JSONObject)localObject1);
    localObject1 = new com/inmobi/ads/ah;
    int i4 = x;
    int i5 = y;
    int i6 = x;
    int i7 = y;
    int i8 = x;
    int i9 = y;
    int i10 = x;
    int i11 = y;
    Object localObject5 = localObject1;
    ((ah)localObject1).<init>(i4, i5, i6, i7, i8, i9, i10, i11, (String)localObject7, (String)localObject3, str3, (String)localObject4, (String)localObject6, localax);
    return (ah)localObject1;
  }
  
  static ai a(ag paramag)
  {
    boolean bool1 = paramag instanceof ai;
    if (bool1)
    {
      Object localObject = paramag;
      localObject = (ai)paramag;
      boolean bool2 = a((ai)localObject);
      if (bool2) {
        return (ai)localObject;
      }
    }
    for (paramag = (ai)t; paramag != null; paramag = (ai)t)
    {
      bool1 = a(paramag);
      if (bool1) {
        return paramag;
      }
    }
    return null;
  }
  
  private bu a(JSONObject paramJSONObject, String paramString, ag paramag)
  {
    String str = f(paramJSONObject);
    boolean bool1 = str.equalsIgnoreCase("VIDEO");
    if (bool1)
    {
      str = "assetValue";
      try
      {
        paramJSONObject = paramJSONObject.getJSONArray(str);
        if (paramJSONObject != null)
        {
          int i1 = paramJSONObject.length();
          if (i1 != 0)
          {
            if (paramag != null)
            {
              boolean bool2 = paramag instanceof bb;
              if (bool2)
              {
                paramJSONObject = e;
                return (bu)paramJSONObject;
              }
            }
            paramJSONObject = new com/inmobi/ads/bq;
            paramag = r;
            paramag = q;
            paramJSONObject.<init>(paramag);
            return paramJSONObject.a(paramString);
          }
        }
        return null;
      }
      catch (JSONException paramJSONObject)
      {
        paramString = com.inmobi.commons.core.a.a.a();
        paramag = new com/inmobi/commons/core/e/a;
        paramag.<init>(paramJSONObject);
        paramString.a(paramag);
      }
    }
    return null;
  }
  
  private static String a(bp parambp, bc parambc)
  {
    Object localObject = "REF_HTML";
    String str1 = z;
    boolean bool1 = ((String)localObject).equals(str1);
    int i3 = 3;
    int i4 = 2;
    boolean bool2;
    if (bool1)
    {
      localObject = parambp.a(i4);
      i4 = ((List)localObject).size();
      if (i4 > 0) {
        return get0b;
      }
      parambp = parambp.a(i3);
      int i1 = parambp.size();
      if (i1 > 0)
      {
        parambp = get0b;
        bool2 = URLUtil.isValidUrl(parambp);
        if (bool2)
        {
          z = "REF_IFRAME";
          return parambp;
        }
        parambp = "MalformedURL";
        parambc = "Rich";
        localObject = "REF_HTML";
        a(parambp, parambc, (String)localObject, null, null);
      }
    }
    else
    {
      localObject = "REF_IFRAME";
      String str2 = z;
      bool2 = ((String)localObject).equals(str2);
      if (bool2)
      {
        localObject = parambp.a(i3);
        i3 = ((List)localObject).size();
        if (i3 > 0)
        {
          parambp = get0b;
          bool2 = URLUtil.isValidUrl(parambp);
          if (bool2)
          {
            z = "REF_IFRAME";
            return parambp;
          }
          parambp = "MalformedURL";
          parambc = "Rich";
          localObject = "REF_IFRAME";
          a(parambp, parambc, (String)localObject, null, null);
        }
        else
        {
          parambp = parambp.a(i4);
          int i2 = parambp.size();
          if (i2 > 0)
          {
            z = "REF_HTML";
            return get0b;
          }
        }
      }
    }
    return null;
  }
  
  private static List a(JSONObject paramJSONObject)
  {
    LinkedList localLinkedList = new java/util/LinkedList;
    localLinkedList.<init>();
    Object localObject1 = "passThroughJson";
    try
    {
      paramJSONObject = paramJSONObject.getJSONObject((String)localObject1);
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      localObject2 = "macros";
      boolean bool1 = paramJSONObject.isNull((String)localObject2);
      int i2;
      Object localObject3;
      String str;
      if (!bool1)
      {
        localObject2 = "macros";
        localObject2 = paramJSONObject.getJSONObject((String)localObject2);
        localIterator = ((JSONObject)localObject2).keys();
        for (;;)
        {
          i2 = localIterator.hasNext();
          if (i2 == 0) {
            break;
          }
          localObject3 = localIterator.next();
          localObject3 = (String)localObject3;
          str = ((JSONObject)localObject2).getString((String)localObject3);
          ((Map)localObject1).put(localObject3, str);
        }
      }
      localObject2 = "urls";
      bool1 = paramJSONObject.isNull((String)localObject2);
      Iterator localIterator = null;
      if (!bool1)
      {
        localObject2 = "urls";
        paramJSONObject = paramJSONObject.getJSONArray((String)localObject2);
        int i1 = paramJSONObject.length();
        i2 = 0;
        localObject3 = null;
        while (i2 < i1)
        {
          str = paramJSONObject.getString(i2);
          NativeTracker localNativeTracker = new com/inmobi/ads/NativeTracker;
          NativeTracker.TrackerEventType localTrackerEventType = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_IAS;
          localNativeTracker.<init>(str, 0, localTrackerEventType, (Map)localObject1);
          localLinkedList.add(localNativeTracker);
          int i3;
          i2 += 1;
        }
      }
      boolean bool2 = localLinkedList.isEmpty();
      if (bool2)
      {
        paramJSONObject = new com/inmobi/ads/NativeTracker;
        localObject2 = "";
        localObject3 = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_IAS;
        paramJSONObject.<init>((String)localObject2, 0, (NativeTracker.TrackerEventType)localObject3, (Map)localObject1);
        localLinkedList.add(paramJSONObject);
      }
    }
    catch (Exception paramJSONObject)
    {
      paramJSONObject.getMessage();
      localObject1 = com.inmobi.commons.core.a.a.a();
      Object localObject2 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject2).<init>(paramJSONObject);
      ((com.inmobi.commons.core.a.a)localObject1).a((com.inmobi.commons.core.e.a)localObject2);
    }
    return localLinkedList;
  }
  
  private static void a(ag paramag, JSONObject paramJSONObject)
  {
    Object localObject1 = "";
    String str1 = "";
    boolean bool1 = p(paramJSONObject);
    boolean bool2 = false;
    if (bool1)
    {
      Object localObject2 = paramJSONObject.getJSONObject("assetOnclick");
      String str2 = "itemUrl";
      bool1 = ((JSONObject)localObject2).isNull(str2);
      if (!bool1)
      {
        localObject1 = paramJSONObject.getJSONObject("assetOnclick");
        localObject2 = "itemUrl";
        localObject1 = ((JSONObject)localObject1).getString((String)localObject2);
        bool2 = true;
      }
      localObject2 = paramJSONObject.getJSONObject("assetOnclick");
      str2 = "action";
      bool1 = ((JSONObject)localObject2).isNull(str2);
      if (!bool1)
      {
        paramJSONObject = paramJSONObject.getJSONObject("assetOnclick");
        str1 = paramJSONObject.getString("action");
        bool2 = true;
      }
    }
    paramag.a((String)localObject1);
    j = str1;
    h = bool2;
  }
  
  private static void a(bb parambb)
  {
    x = 8;
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    localHashMap.put("[ERRORCODE]", "601");
    NativeTracker.TrackerEventType localTrackerEventType = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_ERROR;
    parambb.a(localTrackerEventType, localHashMap);
  }
  
  private static void a(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    try
    {
      paramString4 = new java/util/HashMap;
      paramString4.<init>();
      paramString5 = "errorCode";
      paramString4.put(paramString5, paramString1);
      paramString1 = "type";
      paramString4.put(paramString1, paramString2);
      paramString1 = "dataType";
      paramString2 = paramString3.toString();
      paramString4.put(paramString1, paramString2);
      paramString1 = "clientRequestId";
      paramString2 = null;
      paramString4.put(paramString1, null);
      paramString1 = "impId";
      paramString4.put(paramString1, null);
      com.inmobi.commons.core.e.b.a();
      paramString1 = "ads";
      paramString2 = "EndCardCompanionFailure";
      com.inmobi.commons.core.e.b.a(paramString1, paramString2, paramString4);
      return;
    }
    catch (Exception localException)
    {
      localException.getMessage();
    }
  }
  
  private static boolean a(ai paramai)
  {
    paramai = d;
    return "card_scrollable".equalsIgnoreCase(paramai);
  }
  
  private static boolean a(JSONArray paramJSONArray)
  {
    int i1 = 2;
    try
    {
      i1 = paramJSONArray.getInt(i1);
      int i2 = 3;
      int i3 = paramJSONArray.getInt(i2);
      return (i1 > 0) && (i3 > 0);
    }
    catch (JSONException paramJSONArray)
    {
      com.inmobi.commons.core.a.a locala = com.inmobi.commons.core.a.a.a();
      com.inmobi.commons.core.e.a locala1 = new com/inmobi/commons/core/e/a;
      locala1.<init>(paramJSONArray);
      locala.a(locala1);
    }
    return false;
  }
  
  private static boolean a(JSONObject paramJSONObject, String paramString)
  {
    Object localObject = "geometry";
    boolean bool1 = paramJSONObject.isNull((String)localObject);
    if (bool1) {
      return false;
    }
    localObject = "geometry";
    try
    {
      localObject = paramJSONObject.getJSONArray((String)localObject);
      bool1 = a((JSONArray)localObject);
      if (!bool1) {
        return false;
      }
      int i1 = -1;
      int i2 = paramString.hashCode();
      boolean bool2 = true;
      String str;
      boolean bool3;
      switch (i2)
      {
      default: 
        break;
      case 1942407129: 
        str = "WEBVIEW";
        bool3 = paramString.equals(str);
        if (bool3) {
          i1 = 6;
        }
        break;
      case 81665115: 
        str = "VIDEO";
        bool3 = paramString.equals(str);
        if (bool3) {
          i1 = 4;
        }
        break;
      case 79826725: 
        str = "TIMER";
        bool3 = paramString.equals(str);
        if (bool3) {
          i1 = 5;
        }
        break;
      case 69775675: 
        str = "IMAGE";
        bool3 = paramString.equals(str);
        if (bool3) {
          i1 = 3;
        }
        break;
      case 2571565: 
        str = "TEXT";
        bool3 = paramString.equals(str);
        if (bool3) {
          i1 = 8;
        }
        break;
      case 2241657: 
        str = "ICON";
        bool3 = paramString.equals(str);
        if (bool3) {
          i1 = 2;
        }
        break;
      case 70564: 
        str = "GIF";
        bool3 = paramString.equals(str);
        if (bool3) {
          i1 = 7;
        }
        break;
      case 67056: 
        str = "CTA";
        bool3 = paramString.equals(str);
        if (bool3) {
          i1 = 9;
        }
        break;
      case -1919329183: 
        str = "CONTAINER";
        bool3 = paramString.equals(str);
        if (bool3) {
          i1 = 1;
        }
        break;
      }
      switch (i1)
      {
      default: 
        return false;
      case 8: 
      case 9: 
        paramString = "text";
        bool3 = paramJSONObject.isNull(paramString);
        if (bool3) {
          return false;
        }
        paramString = "text";
        paramJSONObject = paramJSONObject.getJSONObject(paramString);
        paramString = "size";
        try
        {
          paramJSONObject = paramJSONObject.getString(paramString);
          double d1 = Double.parseDouble(paramJSONObject);
          int i3 = (int)d1;
          if (i3 > 0) {
            return bool2;
          }
          return false;
        }
        catch (NumberFormatException paramJSONObject)
        {
          paramString = com.inmobi.commons.core.a.a.a();
          localObject = new com/inmobi/commons/core/e/a;
          ((com.inmobi.commons.core.e.a)localObject).<init>(paramJSONObject);
          paramString.a((com.inmobi.commons.core.e.a)localObject);
          return false;
        }
      }
      return bool2;
    }
    catch (JSONException paramJSONObject)
    {
      paramString = com.inmobi.commons.core.a.a.a();
      localObject = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject).<init>(paramJSONObject);
      paramString.a((com.inmobi.commons.core.e.a)localObject);
    }
    return false;
  }
  
  /* Error */
  private Point b(JSONObject paramJSONObject, Point paramPoint)
  {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: invokespecial 170	com/inmobi/ads/ak:i	(Lorg/json/JSONObject;)Lorg/json/JSONObject;
    //   5: astore_1
    //   6: ldc -84
    //   8: astore_3
    //   9: aload_1
    //   10: aload_3
    //   11: invokevirtual 137	org/json/JSONObject:isNull	(Ljava/lang/String;)Z
    //   14: istore 4
    //   16: iload 4
    //   18: ifeq +5 -> 23
    //   21: aload_2
    //   22: areturn
    //   23: new 174	android/graphics/Point
    //   26: astore_2
    //   27: aload_2
    //   28: invokespecial 175	android/graphics/Point:<init>	()V
    //   31: ldc -84
    //   33: astore_3
    //   34: aload_1
    //   35: aload_3
    //   36: invokevirtual 179	org/json/JSONObject:getJSONArray	(Ljava/lang/String;)Lorg/json/JSONArray;
    //   39: astore_1
    //   40: iconst_2
    //   41: istore 4
    //   43: aload_1
    //   44: iload 4
    //   46: invokevirtual 184	org/json/JSONArray:getInt	(I)I
    //   49: istore 4
    //   51: iload 4
    //   53: invokestatic 188	com/inmobi/commons/core/utilities/b/c:a	(I)I
    //   56: istore 4
    //   58: aload_2
    //   59: iload 4
    //   61: putfield 191	android/graphics/Point:x	I
    //   64: iconst_3
    //   65: istore 4
    //   67: aload_1
    //   68: iload 4
    //   70: invokevirtual 184	org/json/JSONArray:getInt	(I)I
    //   73: istore 5
    //   75: iload 5
    //   77: invokestatic 188	com/inmobi/commons/core/utilities/b/c:a	(I)I
    //   80: istore 5
    //   82: aload_2
    //   83: iload 5
    //   85: putfield 194	android/graphics/Point:y	I
    //   88: goto +6 -> 94
    //   91: pop
    //   92: aconst_null
    //   93: astore_2
    //   94: aload_2
    //   95: areturn
    //   96: pop
    //   97: goto -3 -> 94
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	100	0	this	ak
    //   0	100	1	paramJSONObject	JSONObject
    //   0	100	2	paramPoint	Point
    //   8	28	3	str	String
    //   14	3	4	bool	boolean
    //   41	28	4	i1	int
    //   73	11	5	i2	int
    //   91	1	7	localJSONException1	JSONException
    //   96	1	8	localJSONException2	JSONException
    // Exception table:
    //   from	to	target	type
    //   1	5	91	org/json/JSONException
    //   10	14	91	org/json/JSONException
    //   23	26	91	org/json/JSONException
    //   27	31	91	org/json/JSONException
    //   35	39	96	org/json/JSONException
    //   44	49	96	org/json/JSONException
    //   51	56	96	org/json/JSONException
    //   59	64	96	org/json/JSONException
    //   68	73	96	org/json/JSONException
    //   75	80	96	org/json/JSONException
    //   83	88	96	org/json/JSONException
  }
  
  private aw.a b(Point paramPoint1, Point paramPoint2, Point paramPoint3, Point paramPoint4, JSONObject paramJSONObject)
  {
    Point localPoint = paramPoint1;
    Object localObject1 = paramPoint2;
    Object localObject2 = paramPoint3;
    Object localObject3 = paramPoint4;
    Object localObject4 = paramJSONObject;
    Object localObject5 = "border";
    boolean bool1 = paramJSONObject.isNull((String)localObject5);
    Object localObject6;
    Object localObject7;
    Object localObject8;
    Object localObject9;
    boolean bool3;
    Object localObject10;
    boolean bool6;
    if (bool1)
    {
      localObject5 = "none";
      str1 = "straight";
      localObject6 = "#ff000000";
      localObject7 = localObject5;
      localObject8 = str1;
      localObject9 = localObject6;
    }
    else
    {
      localObject5 = paramJSONObject.getJSONObject("border");
      str1 = "style";
      boolean bool2 = ((JSONObject)localObject5).isNull(str1);
      if (bool2)
      {
        localObject5 = "none";
        str1 = "straight";
        localObject6 = "#ff000000";
        localObject7 = localObject5;
        localObject8 = str1;
        localObject9 = localObject6;
      }
      else
      {
        str1 = f(((JSONObject)localObject5).getString("style"));
        localObject6 = "corner";
        bool3 = ((JSONObject)localObject5).isNull((String)localObject6);
        if (bool3) {
          localObject6 = "straight";
        } else {
          localObject6 = g(((JSONObject)localObject5).getString("corner"));
        }
        localObject10 = "color";
        bool6 = ((JSONObject)localObject5).isNull((String)localObject10);
        if (bool6)
        {
          localObject5 = "#ff000000";
          localObject9 = localObject5;
          localObject7 = str1;
          localObject8 = localObject6;
        }
        else
        {
          localObject10 = "color";
          localObject5 = ((JSONObject)localObject5).getString((String)localObject10).trim();
          localObject9 = localObject5;
          localObject7 = str1;
          localObject8 = localObject6;
        }
      }
    }
    localObject5 = "backgroundColor";
    bool1 = ((JSONObject)localObject4).isNull((String)localObject5);
    Object localObject11;
    if (bool1)
    {
      localObject5 = "#00000000";
      localObject11 = localObject5;
    }
    else
    {
      localObject5 = ((JSONObject)localObject4).getString("backgroundColor").trim();
      localObject11 = localObject5;
    }
    localObject5 = ((JSONObject)localObject4).getJSONObject("text");
    String str1 = "size";
    try
    {
      str1 = ((JSONObject)localObject5).getString(str1);
      double d1 = Double.parseDouble(str1);
      int i1 = (int)d1;
      localObject6 = "length";
      bool3 = ((JSONObject)localObject5).isNull((String)localObject6);
      int i2;
      int i5;
      if (bool3)
      {
        i2 = -1 >>> 1;
        i5 = -1 >>> 1;
      }
      else
      {
        localObject6 = ((JSONObject)localObject5).getString("length");
        i2 = Integer.parseInt((String)localObject6);
        i5 = i2;
      }
      localObject6 = "color";
      boolean bool4 = ((JSONObject)localObject5).isNull((String)localObject6);
      Object localObject12;
      if (bool4)
      {
        localObject6 = "#ff000000";
        localObject12 = localObject6;
      }
      else
      {
        localObject6 = ((JSONObject)localObject5).getString("color").trim();
        localObject12 = localObject6;
      }
      localObject6 = "style";
      bool4 = ((JSONObject)localObject5).isNull((String)localObject6);
      bool6 = false;
      localObject10 = null;
      Object localObject13;
      Object localObject14;
      String str2;
      if (bool4)
      {
        localObject6 = new String[] { "none" };
        localObject13 = localObject6;
      }
      else
      {
        localObject6 = ((JSONObject)localObject5).getJSONArray("style");
        int i3 = ((JSONArray)localObject6).length();
        if (i3 == 0)
        {
          localObject6 = new String[] { "none" };
          localObject13 = localObject6;
        }
        else
        {
          localObject14 = new String[i3];
          i6 = 0;
          while (i6 < i3)
          {
            str2 = e(((JSONObject)localObject5).getJSONArray("style").getString(i6));
            localObject14[i6] = str2;
            i6 += 1;
          }
          localObject13 = localObject14;
        }
      }
      localObject6 = "align";
      boolean bool5 = ((JSONObject)localObject5).isNull((String)localObject6);
      int i10;
      if (!bool5)
      {
        localObject6 = "align";
        localObject5 = ((JSONObject)localObject5).getString((String)localObject6).trim();
        int i4 = -1;
        i7 = ((String)localObject5).hashCode();
        i6 = -1364013605;
        i8 = 2;
        i9 = 1;
        if (i7 != i6)
        {
          i6 = 3317767;
          if (i7 != i6)
          {
            i6 = 108511772;
            if (i7 == i6)
            {
              localObject14 = "right";
              bool1 = ((String)localObject5).equals(localObject14);
              if (bool1) {
                i4 = 2;
              }
            }
          }
          else
          {
            localObject14 = "left";
            bool1 = ((String)localObject5).equals(localObject14);
            if (bool1) {
              i4 = 1;
            }
          }
        }
        else
        {
          localObject14 = "centre";
          bool1 = ((String)localObject5).equals(localObject14);
          if (bool1) {
            i4 = 3;
          }
        }
        switch (i4)
        {
        default: 
          i8 = 0;
          str2 = null;
          break;
        case 2: 
          i8 = 1;
        }
        localObject5 = this;
        i10 = i8;
      }
      else
      {
        i10 = 0;
        localObject5 = this;
      }
      ax localax = ((ak)localObject5).s((JSONObject)localObject4);
      localObject4 = new com/inmobi/ads/aw$a;
      localObject10 = localObject4;
      int i7 = x;
      int i6 = y;
      int i8 = x;
      int i9 = y;
      int i11 = x;
      int i12 = y;
      int i13 = x;
      int i14 = y;
      ((aw.a)localObject4).<init>(i7, i6, i8, i9, i11, i12, i13, i14, (String)localObject7, (String)localObject8, (String)localObject9, (String)localObject11, i1, i10, i5, (String)localObject12, (String[])localObject13, localax);
      return (aw.a)localObject4;
    }
    catch (NumberFormatException localNumberFormatException)
    {
      localObject5 = this;
      localObject1 = new org/json/JSONException;
      localObject2 = localNumberFormatException.getMessage();
      ((JSONException)localObject1).<init>((String)localObject2);
      ((JSONException)localObject1).initCause(localNumberFormatException);
      localObject2 = com.inmobi.commons.core.a.a.a();
      localObject3 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject3).<init>(localNumberFormatException);
      ((com.inmobi.commons.core.a.a)localObject2).a((com.inmobi.commons.core.e.a)localObject3);
      throw ((Throwable)localObject1);
    }
  }
  
  private static List b(JSONObject paramJSONObject)
  {
    Object localObject1 = "trackers";
    boolean bool1 = paramJSONObject.isNull((String)localObject1);
    if (bool1) {
      return null;
    }
    localObject1 = new java/util/LinkedList;
    ((LinkedList)localObject1).<init>();
    Object localObject2 = "trackers";
    try
    {
      paramJSONObject = paramJSONObject.getJSONArray((String)localObject2);
      int i1 = paramJSONObject.length();
      if (i1 == 0) {
        return (List)localObject1;
      }
      locala = null;
      int i2 = 0;
      while (i2 < i1)
      {
        Object localObject3 = paramJSONObject.getJSONObject(i2);
        String str1 = "trackerType";
        boolean bool2 = ((JSONObject)localObject3).isNull(str1);
        if (!bool2)
        {
          str1 = "trackerType";
          str1 = ((JSONObject)localObject3).getString(str1);
          Object localObject4 = Locale.US;
          str1 = str1.toUpperCase((Locale)localObject4);
          str1 = str1.trim();
          int i6 = str1.hashCode();
          int i7 = -1430070305;
          int i15 = 2;
          int i16 = 3;
          boolean bool3;
          if (i6 != i7)
          {
            i7 = -158113182;
            if (i6 != i7)
            {
              i7 = 1110926088;
              if (i6 == i7)
              {
                localObject4 = "URL_WEBVIEW_PING";
                bool2 = str1.equals(localObject4);
                if (bool2)
                {
                  int i3 = 2;
                  break label252;
                }
              }
            }
            else
            {
              localObject4 = "URL_PING";
              bool3 = str1.equals(localObject4);
              if (bool3)
              {
                bool3 = true;
                break label252;
              }
            }
          }
          else
          {
            localObject4 = "HTML_SCRIPT";
            bool3 = str1.equals(localObject4);
            if (bool3)
            {
              i4 = 3;
              break label252;
            }
          }
          int i4 = -1;
          switch (i4)
          {
          default: 
            str1 = "unknown";
            break;
          case 3: 
            str1 = "html_script";
            break;
          case 2: 
            str1 = "webview_ping";
            break;
          case 1: 
            label252:
            str1 = "url_ping";
          }
          localObject4 = "url_ping";
          boolean bool4 = ((String)localObject4).equals(str1);
          if (bool4)
          {
            str1 = "eventId";
            int i5 = ((JSONObject)localObject3).optInt(str1, 0);
            localObject4 = "uiEvent";
            boolean bool5 = ((JSONObject)localObject3).isNull((String)localObject4);
            if (!bool5)
            {
              localObject4 = "uiEvent";
              localObject4 = ((JSONObject)localObject3).getString((String)localObject4);
              Object localObject5 = Locale.US;
              localObject5 = ((String)localObject4).toUpperCase((Locale)localObject5);
              localObject5 = ((String)localObject5).trim();
              int i17 = ((String)localObject5).hashCode();
              String str2;
              boolean bool10;
              switch (i17)
              {
              default: 
                break;
              case 2008409463: 
                str2 = "CLIENT_FILL";
                boolean bool6 = ((String)localObject5).equals(str2);
                if (bool6) {
                  int i8 = 2;
                }
                break;
              case 1963885793: 
                str2 = "VIDEO_VIEWABILITY";
                boolean bool7 = ((String)localObject5).equals(str2);
                if (bool7) {
                  int i9 = 6;
                }
                break;
              case 64212328: 
                str2 = "CLICK";
                boolean bool8 = ((String)localObject5).equals(str2);
                if (bool8) {
                  int i10 = 5;
                }
                break;
              case 2634405: 
                str2 = "VIEW";
                boolean bool9 = ((String)localObject5).equals(str2);
                if (bool9) {
                  int i11 = 4;
                }
                break;
              case 2342118: 
                str2 = "LOAD";
                bool10 = ((String)localObject5).equals(str2);
                if (bool10) {
                  bool10 = true;
                }
                break;
              case -45894975: 
                str2 = "IAS_VIEWABILITY";
                bool10 = ((String)localObject5).equals(str2);
                if (bool10) {
                  int i12 = 7;
                }
                break;
              case -825499301: 
                str2 = "FALLBACK_URL_CLICK";
                boolean bool11 = ((String)localObject5).equals(str2);
                if (bool11) {
                  int i13 = 8;
                }
                break;
              case -1881262698: 
                str2 = "RENDER";
                boolean bool12 = ((String)localObject5).equals(str2);
                if (bool12) {
                  i14 = 3;
                }
                break;
              }
              int i14 = -1;
              switch (i14)
              {
              default: 
                localObject5 = Locale.US;
                break;
              case 8: 
                localObject4 = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_FALLBACK_URL;
                break;
              case 7: 
                localObject4 = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_IAS;
                break;
              case 6: 
                localObject4 = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_VIDEO_RENDER;
                break;
              case 5: 
                localObject4 = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_CLICK;
                break;
              case 4: 
                localObject4 = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_PAGE_VIEW;
                break;
              case 3: 
                localObject4 = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_RENDER;
                break;
              case 2: 
                localObject4 = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_CLIENT_FILL;
                break;
              case 1: 
                localObject4 = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_LOAD;
                break;
              }
              localObject4 = ((String)localObject4).toUpperCase((Locale)localObject5);
              localObject4 = ((String)localObject4).trim();
              i14 = ((String)localObject4).hashCode();
              i17 = -1836567951;
              if (i14 != i17)
              {
                i16 = -1099027408;
                if (i14 != i16)
                {
                  i15 = 1331888222;
                  if (i14 != i15)
                  {
                    i15 = 1346121898;
                    if (i14 == i15)
                    {
                      localObject5 = "DOWNLOADER_INITIALIZED";
                      bool5 = ((String)localObject4).equals(localObject5);
                      if (bool5)
                      {
                        i15 = 1;
                        break label982;
                      }
                    }
                  }
                  else
                  {
                    localObject5 = "DOWNLOADER_ERROR";
                    bool5 = ((String)localObject4).equals(localObject5);
                    if (bool5)
                    {
                      i15 = 4;
                      break label982;
                    }
                  }
                }
                else
                {
                  localObject5 = "DOWNLOADER_DOWNLOADING";
                  bool5 = ((String)localObject4).equals(localObject5);
                  if (bool5) {
                    break label982;
                  }
                }
              }
              else
              {
                localObject5 = "DOWNLOADER_DOWNLOADED";
                bool5 = ((String)localObject4).equals(localObject5);
                if (bool5)
                {
                  i15 = 3;
                  break label982;
                }
              }
              i15 = -1;
              switch (i15)
              {
              default: 
                localObject4 = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_UNKNOWN;
                break;
              case 4: 
                localObject4 = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_DOWNLOADER_ERROR;
                break;
              case 3: 
                localObject4 = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_DOWNLOADER_DOWNLOADED;
                break;
              case 2: 
                localObject4 = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_DOWNLOADER_DOWNLOADING;
                break;
              case 1: 
                label982:
                localObject4 = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_DOWNLOADER_INIT;
              }
              localObject5 = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_UNKNOWN;
              if (localObject5 != localObject4)
              {
                localObject5 = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_IAS;
                if (localObject5 != localObject4)
                {
                  localObject3 = a(i5, (NativeTracker.TrackerEventType)localObject4, (JSONObject)localObject3);
                  if (localObject3 != null) {
                    ((List)localObject1).add(localObject3);
                  }
                }
                else
                {
                  localObject3 = a((JSONObject)localObject3);
                  ((List)localObject1).addAll((Collection)localObject3);
                }
              }
            }
          }
        }
        i2 += 1;
      }
      return (List)localObject1;
    }
    catch (JSONException paramJSONObject)
    {
      localObject2 = com.inmobi.commons.core.a.a.a();
      com.inmobi.commons.core.e.a locala = new com/inmobi/commons/core/e/a;
      locala.<init>(paramJSONObject);
      ((com.inmobi.commons.core.a.a)localObject2).a(locala);
    }
    return (List)localObject1;
  }
  
  private aw.a c(Point paramPoint1, Point paramPoint2, Point paramPoint3, Point paramPoint4, JSONObject paramJSONObject)
  {
    Point localPoint = paramPoint1;
    Object localObject1 = paramPoint2;
    Object localObject2 = paramPoint3;
    Object localObject3 = paramPoint4;
    Object localObject4 = paramJSONObject;
    Object localObject5 = "border";
    boolean bool1 = paramJSONObject.isNull((String)localObject5);
    Object localObject6;
    Object localObject7;
    Object localObject8;
    Object localObject9;
    boolean bool3;
    Object localObject10;
    if (bool1)
    {
      localObject5 = "none";
      str1 = "straight";
      localObject6 = "#ff000000";
      localObject7 = localObject5;
      localObject8 = str1;
      localObject9 = localObject6;
    }
    else
    {
      localObject5 = paramJSONObject.getJSONObject("border");
      str1 = "style";
      boolean bool2 = ((JSONObject)localObject5).isNull(str1);
      if (bool2)
      {
        localObject5 = "none";
        str1 = "straight";
        localObject6 = "#ff000000";
        localObject7 = localObject5;
        localObject8 = str1;
        localObject9 = localObject6;
      }
      else
      {
        str1 = f(((JSONObject)localObject5).getString("style"));
        localObject6 = "corner";
        bool3 = ((JSONObject)localObject5).isNull((String)localObject6);
        if (bool3) {
          localObject6 = "straight";
        } else {
          localObject6 = g(((JSONObject)localObject5).getString("corner"));
        }
        localObject10 = "color";
        boolean bool4 = ((JSONObject)localObject5).isNull((String)localObject10);
        if (bool4)
        {
          localObject5 = "#ff000000";
          localObject9 = localObject5;
          localObject7 = str1;
          localObject8 = localObject6;
        }
        else
        {
          localObject10 = "color";
          localObject5 = ((JSONObject)localObject5).getString((String)localObject10).trim();
          localObject9 = localObject5;
          localObject7 = str1;
          localObject8 = localObject6;
        }
      }
    }
    localObject5 = "backgroundColor";
    bool1 = ((JSONObject)localObject4).isNull((String)localObject5);
    Object localObject11;
    if (bool1)
    {
      localObject5 = "#00000000";
      localObject11 = localObject5;
    }
    else
    {
      localObject5 = ((JSONObject)localObject4).getString("backgroundColor").trim();
      localObject11 = localObject5;
    }
    localObject5 = ((JSONObject)localObject4).getJSONObject("text");
    String str1 = "size";
    try
    {
      str1 = ((JSONObject)localObject5).getString(str1);
      double d1 = Double.parseDouble(str1);
      int i1 = (int)d1;
      localObject6 = "color";
      bool3 = ((JSONObject)localObject5).isNull((String)localObject6);
      Object localObject12;
      if (bool3)
      {
        localObject6 = "#ff000000";
        localObject12 = localObject6;
      }
      else
      {
        localObject6 = ((JSONObject)localObject5).getString("color").trim();
        localObject12 = localObject6;
      }
      localObject6 = "style";
      bool3 = ((JSONObject)localObject5).isNull((String)localObject6);
      Object localObject13;
      if (bool3)
      {
        localObject13 = new String[] { "none" };
        localObject5 = this;
      }
      else
      {
        localObject6 = ((JSONObject)localObject5).getJSONArray("style");
        int i2 = ((JSONArray)localObject6).length();
        if (i2 == 0)
        {
          localObject13 = new String[] { "none" };
          localObject5 = this;
        }
        else
        {
          localObject10 = new String[i2];
          i3 = 0;
          while (i3 < i2)
          {
            String str2 = e(((JSONObject)localObject5).getJSONArray("style").getString(i3));
            localObject10[i3] = str2;
            i3 += 1;
          }
          localObject5 = this;
          localObject13 = localObject10;
        }
      }
      ax localax = ((ak)localObject5).s((JSONObject)localObject4);
      localObject4 = new com/inmobi/ads/aj$a;
      localObject10 = localObject4;
      int i3 = x;
      int i4 = y;
      int i5 = x;
      int i6 = y;
      int i7 = x;
      int i8 = y;
      int i9 = x;
      int i10 = y;
      ((aj.a)localObject4).<init>(i3, i4, i5, i6, i7, i8, i9, i10, (String)localObject7, (String)localObject8, (String)localObject9, (String)localObject11, i1, (String)localObject12, (String[])localObject13, localax);
      return (aw.a)localObject4;
    }
    catch (NumberFormatException localNumberFormatException)
    {
      localObject5 = this;
      localObject1 = new org/json/JSONException;
      localObject2 = localNumberFormatException.getMessage();
      ((JSONException)localObject1).<init>((String)localObject2);
      ((JSONException)localObject1).initCause(localNumberFormatException);
      localObject2 = com.inmobi.commons.core.a.a.a();
      localObject3 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject3).<init>(localNumberFormatException);
      ((com.inmobi.commons.core.a.a)localObject2).a((com.inmobi.commons.core.e.a)localObject3);
      throw ((Throwable)localObject1);
    }
  }
  
  private static String c(JSONObject paramJSONObject)
  {
    try
    {
      localObject1 = f(paramJSONObject);
      localObject2 = "ICON";
      boolean bool = ((String)localObject1).equalsIgnoreCase((String)localObject2);
      if (!bool)
      {
        localObject1 = f(paramJSONObject);
        localObject2 = "IMAGE";
        bool = ((String)localObject1).equalsIgnoreCase((String)localObject2);
        if (!bool)
        {
          localObject1 = f(paramJSONObject);
          localObject2 = "GIF";
          bool = ((String)localObject1).equalsIgnoreCase((String)localObject2);
          if (!bool) {
            break label119;
          }
        }
      }
      localObject1 = "assetValue";
      localObject1 = paramJSONObject.getJSONArray((String)localObject1);
      localObject2 = null;
      localObject1 = ((JSONArray)localObject1).getString(0);
      int i1 = ((String)localObject1).length();
      if (i1 != 0)
      {
        localObject1 = "assetValue";
        paramJSONObject = paramJSONObject.getJSONArray((String)localObject1);
        return paramJSONObject.getString(0);
      }
    }
    catch (JSONException paramJSONObject)
    {
      Object localObject1 = com.inmobi.commons.core.a.a.a();
      Object localObject2 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject2).<init>(paramJSONObject);
      ((com.inmobi.commons.core.a.a)localObject1).a((com.inmobi.commons.core.e.a)localObject2);
    }
    label119:
    return "";
  }
  
  private static int d(String paramString)
  {
    Object localObject = Locale.US;
    paramString = paramString.toUpperCase((Locale)localObject).trim();
    int i1 = paramString.hashCode();
    int i2 = -2084521848;
    int i3 = 4;
    int i4 = 3;
    int i5 = 1;
    int i6 = 2;
    boolean bool3;
    if (i1 != i2)
    {
      i2 = -1038134325;
      if (i1 != i2)
      {
        i2 = 69805756;
        if (i1 != i2)
        {
          i2 = 1411860198;
          if (i1 == i2)
          {
            localObject = "DEEPLINK";
            boolean bool1 = paramString.equals(localObject);
            if (bool1)
            {
              int i7 = 3;
              break label160;
            }
          }
        }
        else
        {
          localObject = "INAPP";
          boolean bool2 = paramString.equals(localObject);
          if (bool2)
          {
            int i8 = 2;
            break label160;
          }
        }
      }
      else
      {
        localObject = "EXTERNAL";
        bool3 = paramString.equals(localObject);
        if (bool3)
        {
          bool3 = true;
          break label160;
        }
      }
    }
    else
    {
      localObject = "DOWNLOAD";
      bool3 = paramString.equals(localObject);
      if (bool3)
      {
        i9 = 4;
        break label160;
      }
    }
    int i9 = -1;
    switch (i9)
    {
    default: 
      return i6;
    case 4: 
      return i3;
    case 3: 
      label160:
      return i4;
    }
    return i5;
  }
  
  private static String d(JSONObject paramJSONObject)
  {
    String str = "assetId";
    try
    {
      return paramJSONObject.getString(str);
    }
    catch (JSONException localJSONException)
    {
      com.inmobi.commons.core.a.a locala = com.inmobi.commons.core.a.a.a();
      com.inmobi.commons.core.e.a locala1 = new com/inmobi/commons/core/e/a;
      locala1.<init>(localJSONException);
      locala.a(locala1);
    }
    return Integer.toString(paramJSONObject.hashCode());
  }
  
  private void d()
  {
    Iterator localIterator = c("IMAGE").iterator();
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      Object localObject1 = (ag)localIterator.next();
      Object localObject2 = (String)e;
      boolean bool2 = URLUtil.isValidUrl((String)localObject2);
      if (!bool2)
      {
        localObject2 = a(this, (ag)localObject1);
        if (localObject2 != null)
        {
          Object localObject3 = b;
          Object localObject4 = b;
          boolean bool3 = ((String)localObject3).equals(localObject4);
          if (bool3)
          {
            localObject2 = e;
            e = localObject2;
          }
          else
          {
            localObject3 = "VIDEO";
            localObject4 = b;
            bool3 = ((String)localObject3).equals(localObject4);
            if (bool3)
            {
              int i1 = m;
              int i2 = 1;
              if (i2 != i1)
              {
                i1 = 2;
                int i3 = m;
                if (i1 == i3)
                {
                  localObject2 = (bb)localObject2;
                  localObject3 = ((bb)localObject2).b();
                  bp localbp = bo.a((bb)localObject2, (ag)localObject1);
                  if (localbp == null)
                  {
                    i2 = 0;
                    localObject4 = null;
                  }
                  else
                  {
                    localObject4 = localbp.a(i2);
                  }
                  boolean bool4;
                  bp.a locala;
                  if (localObject4 != null)
                  {
                    localObject4 = ((List)localObject4).iterator();
                    boolean bool5;
                    do
                    {
                      bool4 = ((Iterator)localObject4).hasNext();
                      if (!bool4) {
                        break;
                      }
                      locala = (bp.a)((Iterator)localObject4).next();
                      String str = b;
                      bool5 = URLUtil.isValidUrl(str);
                    } while (!bool5);
                  }
                  else
                  {
                    bool4 = false;
                    locala = null;
                  }
                  if ((localbp != null) && (locala != null))
                  {
                    ((bu)localObject3).a(localbp);
                    localObject3 = b;
                    e = localObject3;
                    localObject3 = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_CREATIVE_VIEW;
                    localObject3 = localbp.a((NativeTracker.TrackerEventType)localObject3);
                    ((ag)localObject1).a((List)localObject3);
                    localObject2 = u;
                    localObject3 = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_ERROR;
                    ((ag)localObject1).a((List)localObject2, (NativeTracker.TrackerEventType)localObject3);
                  }
                  else
                  {
                    a((bb)localObject2);
                    if (localbp == null) {
                      localObject1 = "NoBestFitCompanion";
                    } else {
                      localObject1 = "NoValidResource";
                    }
                    localObject2 = "Static";
                    localObject3 = "URL";
                    a((String)localObject1, (String)localObject2, (String)localObject3, null, null);
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  
  private static String e(String paramString)
  {
    Object localObject = Locale.US;
    paramString = paramString.toLowerCase((Locale)localObject).trim();
    int i1 = paramString.hashCode();
    boolean bool1;
    switch (i1)
    {
    default: 
      break;
    case 3387192: 
      localObject = "none";
      bool1 = paramString.equals(localObject);
      if (bool1) {
        bool1 = true;
      }
      break;
    case 3029637: 
      localObject = "bold";
      bool1 = paramString.equals(localObject);
      if (bool1) {
        int i2 = 2;
      }
      break;
    case -891985998: 
      localObject = "strike";
      boolean bool2 = paramString.equals(localObject);
      if (bool2) {
        int i3 = 4;
      }
      break;
    case -1026963764: 
      localObject = "underline";
      boolean bool3 = paramString.equals(localObject);
      if (bool3) {
        int i4 = 5;
      }
      break;
    case -1178781136: 
      localObject = "italic";
      boolean bool4 = paramString.equals(localObject);
      if (bool4) {
        i5 = 3;
      }
      break;
    }
    int i5 = -1;
    switch (i5)
    {
    default: 
      return "none";
    case 5: 
      return "underline";
    case 4: 
      return "strike";
    case 3: 
      return "italic";
    }
    return "bold";
  }
  
  private static String e(JSONObject paramJSONObject)
  {
    Object localObject = "assetName";
    try
    {
      return paramJSONObject.getString((String)localObject);
    }
    catch (JSONException paramJSONObject)
    {
      localObject = com.inmobi.commons.core.a.a.a();
      com.inmobi.commons.core.e.a locala = new com/inmobi/commons/core/e/a;
      locala.<init>(paramJSONObject);
      ((com.inmobi.commons.core.a.a)localObject).a(locala);
    }
    return "";
  }
  
  private void e()
  {
    Iterator localIterator = c("WEBVIEW").iterator();
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      Object localObject1 = (ag)localIterator.next();
      Object localObject2 = localObject1;
      localObject2 = (bc)localObject1;
      Object localObject3 = "URL";
      Object localObject4 = z;
      boolean bool2 = ((String)localObject3).equals(localObject4);
      if (!bool2)
      {
        localObject3 = "HTML";
        localObject4 = z;
        bool2 = ((String)localObject3).equals(localObject4);
        if (!bool2)
        {
          localObject3 = a(this, (ag)localObject1);
          if (localObject3 != null)
          {
            localObject4 = b;
            Object localObject5 = b;
            boolean bool3 = ((String)localObject4).equals(localObject5);
            if (bool3)
            {
              localObject2 = e;
              e = localObject2;
            }
            else
            {
              localObject4 = "VIDEO";
              localObject5 = b;
              bool3 = ((String)localObject4).equals(localObject5);
              if (bool3)
              {
                int i1 = 2;
                int i2 = m;
                if (i1 == i2)
                {
                  localObject3 = (bb)localObject3;
                  localObject4 = ((bb)localObject3).b();
                  localObject5 = bo.a((bb)localObject3, (ag)localObject1);
                  String str1;
                  if (localObject5 == null) {
                    str1 = null;
                  } else {
                    str1 = a((bp)localObject5, (bc)localObject2);
                  }
                  String str2 = "REF_IFRAME";
                  String str3 = z;
                  boolean bool4 = str2.equals(str3);
                  str3 = "REF_HTML";
                  String str4 = z;
                  boolean bool5 = str3.equals(str4);
                  if (localObject5 != null) {
                    if (bool4)
                    {
                      bool4 = URLUtil.isValidUrl(str1);
                      if (!bool4) {}
                    }
                    else if ((!bool5) || (str1 != null))
                    {
                      ((bu)localObject4).a((bp)localObject5);
                      e = str1;
                      localObject2 = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_CREATIVE_VIEW;
                      localObject2 = ((bp)localObject5).a((NativeTracker.TrackerEventType)localObject2);
                      ((ag)localObject1).a((List)localObject2);
                      continue;
                    }
                  }
                  a((bb)localObject3);
                  if (localObject5 == null) {
                    localObject1 = "NoBestFitCompanion";
                  } else {
                    localObject1 = "NoValidResource";
                  }
                  localObject3 = "Rich";
                  localObject4 = z;
                  a((String)localObject1, (String)localObject3, (String)localObject4, null, null);
                  localObject1 = "UNKNOWN";
                  z = ((String)localObject1);
                }
              }
            }
          }
        }
      }
    }
  }
  
  private static String f(String paramString)
  {
    Object localObject = Locale.US;
    paramString = paramString.toLowerCase((Locale)localObject).trim();
    int i1 = paramString.hashCode();
    int i2 = 3321844;
    int i3 = 2;
    boolean bool;
    if (i1 != i2)
    {
      i2 = 3387192;
      if (i1 == i2)
      {
        localObject = "none";
        bool = paramString.equals(localObject);
        if (bool)
        {
          bool = true;
          break label89;
        }
      }
    }
    else
    {
      localObject = "line";
      bool = paramString.equals(localObject);
      if (bool)
      {
        i4 = 2;
        break label89;
      }
    }
    int i4 = -1;
    label89:
    if (i4 != i3) {
      return "none";
    }
    return "line";
  }
  
  private static String f(JSONObject paramJSONObject)
  {
    Object localObject = "assetType";
    try
    {
      return paramJSONObject.getString((String)localObject);
    }
    catch (JSONException paramJSONObject)
    {
      localObject = com.inmobi.commons.core.a.a.a();
      com.inmobi.commons.core.e.a locala = new com/inmobi/commons/core/e/a;
      locala.<init>(paramJSONObject);
      ((com.inmobi.commons.core.a.a)localObject).a(locala);
    }
    return "";
  }
  
  private void f()
  {
    for (;;)
    {
      try
      {
        Object localObject1 = m;
        localObject3 = "styleRefs";
        localObject1 = ((JSONObject)localObject1).optJSONObject((String)localObject3);
        n = ((JSONObject)localObject1);
        localObject1 = m;
        localObject3 = "orientation";
        boolean bool1 = ((JSONObject)localObject1).isNull((String)localObject3);
        i4 = 2;
        locala1 = null;
        bool5 = true;
        int i3;
        if (bool1)
        {
          bool1 = false;
          localObject1 = null;
        }
        else
        {
          localObject1 = m;
          localObject4 = "orientation";
          localObject1 = ((JSONObject)localObject1).getString((String)localObject4);
          localObject4 = Locale.US;
          localObject1 = ((String)localObject1).toLowerCase((Locale)localObject4);
          localObject1 = ((String)localObject1).trim();
          int i5 = -1;
          int i6 = ((String)localObject1).hashCode();
          i8 = -1626174665;
          if (i6 != i8)
          {
            i8 = 729267099;
            if (i6 != i8)
            {
              i8 = 1430647483;
              if (i6 == i8)
              {
                localObject5 = "landscape";
                bool1 = ((String)localObject1).equals(localObject5);
                if (bool1)
                {
                  int i1 = 3;
                  continue;
                }
              }
            }
            else
            {
              localObject5 = "portrait";
              boolean bool2 = ((String)localObject1).equals(localObject5);
              if (bool2)
              {
                int i2 = 2;
                continue;
              }
            }
          }
          else
          {
            localObject5 = "unspecified";
            boolean bool3 = ((String)localObject1).equals(localObject5);
            if (bool3)
            {
              bool3 = true;
              continue;
            }
          }
          i3 = -1;
          switch (i3)
          {
          default: 
            i3 = 0;
            localObject1 = null;
            break;
          case 3: 
            i3 = 2;
            break;
          case 2: 
            i3 = 1;
          }
        }
        a = i3;
        localObject1 = m;
        localObject4 = "shouldAutoOpenLandingPage";
        bool4 = ((JSONObject)localObject1).optBoolean((String)localObject4, bool5);
        j = bool4;
        localObject1 = m;
        localObject4 = "disableBackButton";
        bool4 = ((JSONObject)localObject1).optBoolean((String)localObject4, false);
        b = bool4;
        localObject1 = m;
        localObject4 = "rootContainer";
        localObject1 = ((JSONObject)localObject1).getJSONObject((String)localObject4);
        localObject4 = "CONTAINER";
        localObject5 = "/rootContainer";
        localObject1 = a((JSONObject)localObject1, (String)localObject4, (String)localObject5);
        localObject1 = (ai)localObject1;
        d = ((ai)localObject1);
        try
        {
          localObject1 = m;
          localObject4 = "passThroughJson";
          bool4 = ((JSONObject)localObject1).isNull((String)localObject4);
          if (!bool4)
          {
            localObject1 = i;
            localObject4 = m;
            localObject5 = "passThroughJson";
            localObject4 = ((JSONObject)localObject4).getJSONObject((String)localObject5);
            a = ((JSONObject)localObject4);
          }
          localObject1 = m;
          localObject4 = "adContent";
          bool4 = ((JSONObject)localObject1).isNull((String)localObject4);
          if (!bool4)
          {
            localObject1 = m;
            localObject4 = "adContent";
            localObject1 = ((JSONObject)localObject1).getJSONObject((String)localObject4);
            if (localObject1 != null)
            {
              localObject4 = new com/inmobi/ads/ak$a$a;
              localObject5 = i;
              localObject5.getClass();
              ((ak.a.a)localObject4).<init>((ak.a)localObject5);
              localObject5 = "title";
              i8 = 0;
              localObject6 = null;
              localObject5 = ((JSONObject)localObject1).optString((String)localObject5, null);
              a = ((String)localObject5);
              localObject5 = "description";
              localObject5 = ((JSONObject)localObject1).optString((String)localObject5, null);
              b = ((String)localObject5);
              localObject5 = "ctaText";
              localObject5 = ((JSONObject)localObject1).optString((String)localObject5, null);
              d = ((String)localObject5);
              localObject5 = "iconUrl";
              localObject5 = ((JSONObject)localObject1).optString((String)localObject5, null);
              c = ((String)localObject5);
              localObject5 = "rating";
              long l1 = 0L;
              l1 = ((JSONObject)localObject1).optLong((String)localObject5, l1);
              float f1 = (float)l1;
              e = f1;
              localObject5 = "landingPageUrl";
              localObject5 = ((JSONObject)localObject1).optString((String)localObject5, null);
              f = ((String)localObject5);
              localObject5 = "isApp";
              bool4 = ((JSONObject)localObject1).optBoolean((String)localObject5);
              g = bool4;
              localObject1 = i;
              b = ((ak.a.a)localObject4);
            }
          }
          localObject1 = new com/inmobi/ads/ag;
          ((ag)localObject1).<init>();
          localObject4 = m;
          localObject5 = "onClick";
          bool6 = ((JSONObject)localObject4).isNull((String)localObject5);
          if (!bool6)
          {
            localObject4 = m;
            localObject5 = "onClick";
            localObject4 = ((JSONObject)localObject4).getJSONObject((String)localObject5);
            localObject5 = "";
            localObject6 = "";
            localObject8 = "itemUrl";
          }
        }
        catch (JSONException localJSONException1)
        {
          boolean bool10;
          boolean bool11;
          boolean bool7;
          int i7;
          localObject4 = com.inmobi.commons.core.a.a.a();
          localObject5 = new com/inmobi/commons/core/e/a;
          ((com.inmobi.commons.core.e.a)localObject5).<init>(localJSONException1);
          ((com.inmobi.commons.core.a.a)localObject4).a((com.inmobi.commons.core.e.a)localObject5);
        }
      }
      catch (JSONException localJSONException2)
      {
        int i4;
        boolean bool5;
        Object localObject4;
        int i8;
        Object localObject5;
        boolean bool4;
        Object localObject6;
        boolean bool6;
        Object localObject8;
        Object localObject9;
        Object localObject2;
        Object localObject3 = com.inmobi.commons.core.a.a.a();
        com.inmobi.commons.core.e.a locala1 = new com/inmobi/commons/core/e/a;
        locala1.<init>(localJSONException2);
        ((com.inmobi.commons.core.a.a)localObject3).a(locala1);
        return;
      }
      try
      {
        bool10 = ((JSONObject)localObject4).isNull((String)localObject8);
        if (!bool10)
        {
          localObject5 = "itemUrl";
          localObject5 = ((JSONObject)localObject4).getString((String)localObject5);
          bool10 = true;
        }
        else
        {
          bool10 = false;
          localObject8 = null;
        }
        localObject9 = "action";
        bool11 = ((JSONObject)localObject4).isNull((String)localObject9);
        if (!bool11)
        {
          localObject6 = "action";
          localObject6 = ((JSONObject)localObject4).getString((String)localObject6);
          bool10 = true;
        }
        ((ag)localObject1).a((String)localObject5);
        localObject5 = "fallbackUrl";
        localObject5 = ((JSONObject)localObject4).optString((String)localObject5);
        ((ag)localObject1).b((String)localObject5);
        j = ((String)localObject6);
        h = bool10;
        localObject5 = "appBundleId";
        localObject5 = ((JSONObject)localObject4).optString((String)localObject5);
        w = localObject5;
      }
      catch (JSONException localJSONException3) {}
    }
    localObject5 = "openMode";
    bool7 = ((JSONObject)localObject4).isNull((String)localObject5);
    if (!bool7)
    {
      localObject5 = "openMode";
      localObject5 = ((JSONObject)localObject4).getString((String)localObject5);
      i7 = d((String)localObject5);
      i = i7;
      localObject5 = "fallbackUrl";
      localObject4 = ((JSONObject)localObject4).optString((String)localObject5);
      ((ag)localObject1).b((String)localObject4);
    }
    localObject4 = m;
    localObject5 = "trackers";
    bool6 = ((JSONObject)localObject4).isNull((String)localObject5);
    if (!bool6)
    {
      localObject4 = m;
      localObject4 = b((JSONObject)localObject4);
      ((ag)localObject1).a((List)localObject4);
    }
    localObject4 = i;
    c = ((ag)localObject1);
    localObject2 = m;
    localObject4 = "prefetchNextPage";
    bool4 = ((JSONObject)localObject2).optBoolean((String)localObject4);
    c = bool4;
    localObject2 = m;
    localObject4 = "rewards";
    bool4 = ((JSONObject)localObject2).has((String)localObject4);
    if (bool4)
    {
      localObject2 = new java/util/HashMap;
      ((HashMap)localObject2).<init>();
      g = ((Map)localObject2);
    }
    localObject2 = m;
    localObject4 = "rewards";
    bool4 = ((JSONObject)localObject2).isNull((String)localObject4);
    if (!bool4)
    {
      localObject2 = m;
      localObject4 = "rewards";
      localObject2 = ((JSONObject)localObject2).getJSONObject((String)localObject4);
      if (localObject2 != null)
      {
        localObject4 = ((JSONObject)localObject2).keys();
        for (;;)
        {
          boolean bool8 = ((Iterator)localObject4).hasNext();
          if (!bool8) {
            break;
          }
          localObject5 = ((Iterator)localObject4).next();
          localObject5 = (String)localObject5;
          localObject6 = ((JSONObject)localObject2).getString((String)localObject5);
          localObject8 = g;
          ((Map)localObject8).put(localObject5, localObject6);
        }
      }
    }
    d();
    e();
    localObject2 = p;
    localObject2 = ((Map)localObject2).entrySet();
    localObject2 = ((Set)localObject2).iterator();
    for (;;)
    {
      bool6 = ((Iterator)localObject2).hasNext();
      if (!bool6) {
        break;
      }
      localObject4 = ((Iterator)localObject2).next();
      localObject4 = (Map.Entry)localObject4;
      localObject5 = ((Map.Entry)localObject4).getValue();
      localObject5 = (String)localObject5;
      localObject6 = o;
      localObject4 = ((Map.Entry)localObject4).getKey();
      localObject4 = ((Map)localObject6).get(localObject4);
      localObject4 = (ag)localObject4;
      i8 = n;
      int i10 = 4;
      if (i10 == i8)
      {
        localObject6 = o;
        localObject5 = ((Map)localObject6).get(localObject5);
        localObject5 = (ag)localObject5;
        localObject6 = "VIDEO";
        localObject9 = b;
        boolean bool9 = ((String)localObject6).equals(localObject9);
        if (bool9)
        {
          localObject6 = localObject5;
          localObject6 = (bb)localObject5;
          localObject6 = ((bb)localObject6).b();
          localObject6 = (bt)localObject6;
          localObject6 = b;
          localObject9 = ":";
          localObject6 = ((String)localObject6).split((String)localObject9);
          int i11;
          int i9;
          try
          {
            localObject9 = localObject6[bool5];
            i11 = Integer.parseInt((String)localObject9) * 60;
            localObject6 = localObject6[i4];
            i9 = Integer.parseInt((String)localObject6) + i11;
          }
          catch (ArrayIndexOutOfBoundsException localArrayIndexOutOfBoundsException)
          {
            localObject9 = com.inmobi.commons.core.a.a.a();
            com.inmobi.commons.core.e.a locala2 = new com/inmobi/commons/core/e/a;
            locala2.<init>(localArrayIndexOutOfBoundsException);
            ((com.inmobi.commons.core.a.a)localObject9).a(locala2);
            i9 = 0;
            Object localObject7 = null;
          }
          if (i9 == 0)
          {
            i9 /= 4;
            o = i9;
          }
          else
          {
            i11 = o;
            int i12 = 50;
            if (i11 != i12)
            {
              i12 = 75;
              if (i11 != i12)
              {
                i10 = 100;
                if (i11 != i10)
                {
                  i9 /= 4;
                  o = i9;
                }
                else
                {
                  o = i9;
                }
              }
              else
              {
                i9 *= 3;
                i9 /= i10;
                o = i9;
              }
            }
            else
            {
              i9 /= 2;
              o = i9;
            }
          }
          localObject5 = (bb)localObject5;
          localObject5 = z;
          ((List)localObject5).add(localObject4);
        }
      }
    }
    localObject2 = m;
    localObject3 = "pages";
    bool4 = ((JSONObject)localObject2).isNull((String)localObject3);
    if (bool4)
    {
      localObject2 = new org/json/JSONArray;
      ((JSONArray)localObject2).<init>();
      e = ((JSONArray)localObject2);
      return;
    }
    localObject2 = m;
    localObject3 = "pages";
    localObject2 = ((JSONObject)localObject2).getJSONArray((String)localObject3);
    e = ((JSONArray)localObject2);
  }
  
  private static String g(String paramString)
  {
    Object localObject = Locale.US;
    paramString = paramString.toLowerCase((Locale)localObject).trim();
    int i1 = paramString.hashCode();
    int i2 = -1349116587;
    int i3 = 2;
    boolean bool;
    if (i1 != i2)
    {
      i2 = 1787472634;
      if (i1 == i2)
      {
        localObject = "straight";
        bool = paramString.equals(localObject);
        if (bool)
        {
          bool = true;
          break label89;
        }
      }
    }
    else
    {
      localObject = "curved";
      bool = paramString.equals(localObject);
      if (bool)
      {
        i4 = 2;
        break label89;
      }
    }
    int i4 = -1;
    label89:
    if (i4 != i3) {
      return "straight";
    }
    return "curved";
  }
  
  private static String g(JSONObject paramJSONObject)
  {
    Object localObject = "valueType";
    try
    {
      return paramJSONObject.getString((String)localObject);
    }
    catch (JSONException paramJSONObject)
    {
      localObject = com.inmobi.commons.core.a.a.a();
      com.inmobi.commons.core.e.a locala = new com/inmobi/commons/core/e/a;
      locala.<init>(paramJSONObject);
      ((com.inmobi.commons.core.a.a)localObject).a(locala);
    }
    return "";
  }
  
  private boolean g()
  {
    Object localObject1 = c("VIDEO");
    boolean bool1 = true;
    if (localObject1 != null)
    {
      int i1 = ((List)localObject1).size();
      if (i1 > 0)
      {
        localObject1 = ((List)localObject1).iterator();
        Object localObject2;
        int i2;
        do
        {
          boolean bool2 = ((Iterator)localObject1).hasNext();
          if (!bool2) {
            break label186;
          }
          localObject2 = (ag)((Iterator)localObject1).next();
          a.length();
          localObject2 = (bb)localObject2;
          Object localObject3 = ((bb)localObject2).b();
          if (localObject3 == null) {
            return false;
          }
          localObject3 = ((bb)localObject2).b().c();
          if (localObject3 == null) {
            break label184;
          }
          i2 = ((List)localObject3).size();
          if (i2 == 0) {
            break label184;
          }
          localObject3 = ((bb)localObject2).b().b();
          if (localObject3 == null) {
            break;
          }
          i2 = ((String)localObject3).length();
        } while (i2 != 0);
        localObject1 = new java/util/HashMap;
        ((HashMap)localObject1).<init>();
        ((HashMap)localObject1).put("[ERRORCODE]", "403");
        NativeTracker.TrackerEventType localTrackerEventType = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_ERROR;
        ((bb)localObject2).a(localTrackerEventType, (Map)localObject1);
        return false;
        label184:
        return false;
        label186:
        return bool1;
      }
    }
    return bool1;
  }
  
  private static String h(JSONObject paramJSONObject)
  {
    Object localObject = "dataType";
    try
    {
      return paramJSONObject.getString((String)localObject);
    }
    catch (JSONException paramJSONObject)
    {
      localObject = com.inmobi.commons.core.a.a.a();
      com.inmobi.commons.core.e.a locala = new com/inmobi/commons/core/e/a;
      locala.<init>(paramJSONObject);
      ((com.inmobi.commons.core.a.a)localObject).a(locala);
    }
    return "";
  }
  
  private JSONObject i(JSONObject paramJSONObject)
  {
    Object localObject = "assetStyle";
    try
    {
      boolean bool = paramJSONObject.isNull((String)localObject);
      if (bool)
      {
        bool = false;
        localObject = null;
      }
      else
      {
        localObject = "assetStyle";
        localObject = paramJSONObject.getJSONObject((String)localObject);
      }
      if (localObject == null)
      {
        localObject = "assetStyleRef";
        bool = paramJSONObject.isNull((String)localObject);
        if (bool)
        {
          paramJSONObject = new org/json/JSONObject;
          paramJSONObject.<init>();
          return paramJSONObject;
        }
        localObject = "assetStyleRef";
        paramJSONObject = paramJSONObject.getString((String)localObject);
        localObject = n;
        if (localObject == null)
        {
          paramJSONObject = new org/json/JSONObject;
          paramJSONObject.<init>();
          localObject = paramJSONObject;
        }
        else
        {
          localObject = n;
          paramJSONObject = ((JSONObject)localObject).getJSONObject(paramJSONObject);
          localObject = paramJSONObject;
        }
      }
      return (JSONObject)localObject;
    }
    catch (JSONException paramJSONObject)
    {
      localObject = com.inmobi.commons.core.a.a.a();
      com.inmobi.commons.core.e.a locala = new com/inmobi/commons/core/e/a;
      locala.<init>(paramJSONObject);
      ((com.inmobi.commons.core.a.a)localObject).a(locala);
      paramJSONObject = new org/json/JSONObject;
      paramJSONObject.<init>();
    }
    return paramJSONObject;
  }
  
  private Point j(JSONObject paramJSONObject)
  {
    Point localPoint = new android/graphics/Point;
    localPoint.<init>();
    try
    {
      paramJSONObject = i(paramJSONObject);
      localObject = "geometry";
      boolean bool = paramJSONObject.isNull((String)localObject);
      if (bool) {
        return localPoint;
      }
      localObject = "geometry";
      paramJSONObject = paramJSONObject.getJSONArray((String)localObject);
      bool = false;
      localObject = null;
      int i1 = paramJSONObject.getInt(0);
      i1 = c.a(i1);
      x = i1;
      i1 = 1;
      int i2 = paramJSONObject.getInt(i1);
      i2 = c.a(i2);
      y = i2;
    }
    catch (JSONException paramJSONObject)
    {
      Object localObject = com.inmobi.commons.core.a.a.a();
      com.inmobi.commons.core.e.a locala = new com/inmobi/commons/core/e/a;
      locala.<init>(paramJSONObject);
      ((com.inmobi.commons.core.a.a)localObject).a(locala);
    }
    return localPoint;
  }
  
  private Point k(JSONObject paramJSONObject)
  {
    Point localPoint = new android/graphics/Point;
    localPoint.<init>();
    try
    {
      paramJSONObject = i(paramJSONObject);
      localObject = "geometry";
      boolean bool = paramJSONObject.isNull((String)localObject);
      if (bool) {
        return localPoint;
      }
      localObject = "geometry";
      paramJSONObject = paramJSONObject.getJSONArray((String)localObject);
      int i1 = 2;
      i1 = paramJSONObject.getInt(i1);
      i1 = c.a(i1);
      x = i1;
      i1 = 3;
      int i2 = paramJSONObject.getInt(i1);
      i2 = c.a(i2);
      y = i2;
    }
    catch (JSONException paramJSONObject)
    {
      Object localObject = com.inmobi.commons.core.a.a.a();
      com.inmobi.commons.core.e.a locala = new com/inmobi/commons/core/e/a;
      locala.<init>(paramJSONObject);
      ((com.inmobi.commons.core.a.a)localObject).a(locala);
    }
    return localPoint;
  }
  
  private static int l(JSONObject paramJSONObject)
  {
    int i1 = 2;
    try
    {
      paramJSONObject = n(paramJSONObject);
      localObject1 = "type";
      boolean bool1 = paramJSONObject.isNull((String)localObject1);
      if (bool1) {
        return i1;
      }
      localObject1 = "type";
      paramJSONObject = paramJSONObject.getString((String)localObject1);
      paramJSONObject = paramJSONObject.trim();
      localObject1 = Locale.US;
      paramJSONObject = paramJSONObject.toLowerCase((Locale)localObject1);
      int i2 = -1;
      int i3 = paramJSONObject.hashCode();
      int i4 = -921832806;
      int i5 = 3;
      int i6 = 1;
      boolean bool2;
      if (i3 != i4)
      {
        i4 = -284840886;
        if (i3 != i4)
        {
          i4 = 1728122231;
          if (i3 == i4)
          {
            localObject2 = "absolute";
            bool2 = paramJSONObject.equals(localObject2);
            if (bool2) {
              break label170;
            }
          }
        }
        else
        {
          localObject2 = "unknown";
          bool2 = paramJSONObject.equals(localObject2);
          if (bool2)
          {
            i1 = 1;
            break label170;
          }
        }
      }
      else
      {
        localObject2 = "percentage";
        bool2 = paramJSONObject.equals(localObject2);
        if (bool2)
        {
          i1 = 3;
          break label170;
        }
      }
      i1 = -1;
      switch (i1)
      {
      default: 
        return i6;
      case 3: 
        label170:
        return 4;
      }
      return i5;
    }
    catch (JSONException paramJSONObject)
    {
      Object localObject1 = com.inmobi.commons.core.a.a.a();
      Object localObject2 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject2).<init>(paramJSONObject);
      ((com.inmobi.commons.core.a.a)localObject1).a((com.inmobi.commons.core.e.a)localObject2);
    }
    return i1;
  }
  
  private static String m(JSONObject paramJSONObject)
  {
    try
    {
      paramJSONObject = n(paramJSONObject);
      localObject = "reference";
      boolean bool = paramJSONObject.isNull((String)localObject);
      if (bool) {
        return "";
      }
      localObject = "reference";
      return paramJSONObject.getString((String)localObject);
    }
    catch (JSONException paramJSONObject)
    {
      Object localObject = com.inmobi.commons.core.a.a.a();
      com.inmobi.commons.core.e.a locala = new com/inmobi/commons/core/e/a;
      locala.<init>(paramJSONObject);
      ((com.inmobi.commons.core.a.a)localObject).a(locala);
    }
    return "";
  }
  
  private static JSONObject n(JSONObject paramJSONObject)
  {
    Object localObject = "display";
    boolean bool = paramJSONObject.isNull((String)localObject);
    if (bool)
    {
      paramJSONObject = new org/json/JSONObject;
      paramJSONObject.<init>();
      return paramJSONObject;
    }
    localObject = "display";
    try
    {
      return paramJSONObject.getJSONObject((String)localObject);
    }
    catch (JSONException paramJSONObject)
    {
      localObject = com.inmobi.commons.core.a.a.a();
      com.inmobi.commons.core.e.a locala = new com/inmobi/commons/core/e/a;
      locala.<init>(paramJSONObject);
      ((com.inmobi.commons.core.a.a)localObject).a(locala);
      paramJSONObject = new org/json/JSONObject;
      paramJSONObject.<init>();
    }
    return paramJSONObject;
  }
  
  private static JSONArray o(JSONObject paramJSONObject)
  {
    Object localObject = "assetValue";
    try
    {
      return paramJSONObject.getJSONArray((String)localObject);
    }
    catch (JSONException paramJSONObject)
    {
      localObject = com.inmobi.commons.core.a.a.a();
      com.inmobi.commons.core.e.a locala = new com/inmobi/commons/core/e/a;
      locala.<init>(paramJSONObject);
      ((com.inmobi.commons.core.a.a)localObject).a(locala);
      paramJSONObject = new org/json/JSONArray;
      paramJSONObject.<init>();
    }
    return paramJSONObject;
  }
  
  private static boolean p(JSONObject paramJSONObject)
  {
    String str = "assetOnclick";
    boolean bool = paramJSONObject.isNull(str);
    return !bool;
  }
  
  private ax.a q(JSONObject paramJSONObject)
  {
    long l1 = paramJSONObject.optLong("absolute");
    long l2 = paramJSONObject.optLong("percentage");
    String str = paramJSONObject.optString("reference");
    paramJSONObject = new com/inmobi/ads/ax$a;
    paramJSONObject.<init>(l1, l2, str, this);
    return paramJSONObject;
  }
  
  private ax.a r(JSONObject paramJSONObject)
  {
    long l1 = paramJSONObject.optLong("absolute");
    long l2 = paramJSONObject.optLong("percentage");
    String str = paramJSONObject.optString("reference");
    paramJSONObject = new com/inmobi/ads/ax$a;
    paramJSONObject.<init>(l1, l2, str, this);
    return paramJSONObject;
  }
  
  private ax s(JSONObject paramJSONObject)
  {
    Object localObject = "startOffset";
    boolean bool1 = paramJSONObject.isNull((String)localObject);
    ax.a locala = null;
    if (!bool1)
    {
      localObject = paramJSONObject.getJSONObject("startOffset");
      localObject = r((JSONObject)localObject);
    }
    else
    {
      bool1 = false;
      localObject = null;
    }
    String str = "timerDuration";
    boolean bool2 = paramJSONObject.isNull(str);
    if (!bool2)
    {
      paramJSONObject = paramJSONObject.getJSONObject("timerDuration");
      locala = r(paramJSONObject);
    }
    paramJSONObject = new com/inmobi/ads/ax;
    paramJSONObject.<init>((ax.a)localObject, locala);
    return paramJSONObject;
  }
  
  final ai a(int paramInt)
  {
    Iterator localIterator = d.iterator();
    boolean bool2;
    do
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      localObject = (ag)localIterator.next();
      String str1 = d;
      String str2 = "card_scrollable";
      bool2 = str1.equalsIgnoreCase(str2);
    } while (!bool2);
    Object localObject = (ai)localObject;
    int i1 = C;
    if (paramInt >= i1) {
      return null;
    }
    return (ai)((ai)localObject).a(paramInt);
    return null;
  }
  
  final JSONObject a()
  {
    try
    {
      JSONArray localJSONArray = e;
      locala = null;
      return localJSONArray.getJSONObject(0);
    }
    catch (JSONException localJSONException)
    {
      com.inmobi.commons.core.a.a locala = com.inmobi.commons.core.a.a.a();
      com.inmobi.commons.core.e.a locala1 = new com/inmobi/commons/core/e/a;
      locala1.<init>(localJSONException);
      locala.a(locala1);
    }
    return null;
  }
  
  final int b()
  {
    Object localObject = d;
    if (localObject == null) {
      return 0;
    }
    localObject = ((ai)localObject).iterator();
    ag localag;
    boolean bool2;
    do
    {
      boolean bool1 = ((Iterator)localObject).hasNext();
      if (!bool1) {
        break;
      }
      localag = (ag)((Iterator)localObject).next();
      String str1 = d;
      String str2 = "card_scrollable";
      bool2 = str1.equalsIgnoreCase(str2);
    } while (!bool2);
    return C;
    return 0;
  }
  
  final ag b(String paramString)
  {
    if (paramString != null)
    {
      int i1 = paramString.length();
      if (i1 != 0)
      {
        Object localObject = o.get(paramString);
        if (localObject != null) {
          return (ag)o.get(paramString);
        }
        localObject = f;
        if (localObject != null) {
          return (ag)o.get(paramString);
        }
        return null;
      }
    }
    return null;
  }
  
  final List c(String paramString)
  {
    Map localMap = h;
    boolean bool = localMap.containsKey(paramString);
    if (bool) {
      return (List)h.get(paramString);
    }
    return Collections.emptyList();
  }
  
  final boolean c()
  {
    Object localObject1 = d;
    if (localObject1 == null) {
      return false;
    }
    localObject1 = ((ai)localObject1).iterator();
    boolean bool2;
    do
    {
      bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      localObject2 = (ag)((Iterator)localObject1).next();
      String str1 = d;
      String str2 = "card_scrollable";
      bool2 = str1.equalsIgnoreCase(str2);
    } while (!bool2);
    Object localObject2 = (ai)localObject2;
    break label74;
    boolean bool1 = false;
    localObject2 = null;
    label74:
    if (localObject2 == null) {
      return g();
    }
    int i1 = b();
    if (i1 <= 0) {
      return false;
    }
    return g();
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.ak
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */