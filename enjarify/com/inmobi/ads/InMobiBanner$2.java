package com.inmobi.ads;

import android.content.Context;
import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

final class InMobiBanner$2
  implements j.d
{
  public final void a(j paramj)
  {
    if (paramj != null) {
      try
      {
        Object localObject1 = InMobiBanner.access$500();
        localObject1 = ((ConcurrentHashMap)localObject1).get(paramj);
        localObject1 = (WeakReference)localObject1;
        if (localObject1 != null)
        {
          Object localObject2 = InMobiBanner.access$500();
          ((ConcurrentHashMap)localObject2).remove(paramj);
          localObject1 = ((WeakReference)localObject1).get();
          localObject1 = (InMobiBanner.BannerAdRequestListener)localObject1;
          if (localObject1 != null)
          {
            localObject2 = new com/inmobi/ads/InMobiBanner;
            Object localObject3 = paramj.a();
            long l = b;
            ((InMobiBanner)localObject2).<init>((Context)localObject3, l);
            localObject3 = d;
            ((InMobiBanner)localObject2).setExtras((Map)localObject3);
            localObject3 = c;
            ((InMobiBanner)localObject2).setKeywords((String)localObject3);
            paramj = paramj.k();
            InMobiBanner.access$600((InMobiBanner)localObject2, paramj);
            paramj = new com/inmobi/ads/InMobiAdRequestStatus;
            localObject3 = InMobiAdRequestStatus.StatusCode.NO_ERROR;
            paramj.<init>((InMobiAdRequestStatus.StatusCode)localObject3);
            ((InMobiBanner.BannerAdRequestListener)localObject1).onAdRequestCompleted(paramj, (InMobiBanner)localObject2);
          }
        }
      }
      catch (Exception paramj)
      {
        InMobiBanner.access$300();
        paramj.getMessage();
        return;
      }
    }
  }
  
  public final void a(j paramj, InMobiAdRequestStatus paramInMobiAdRequestStatus)
  {
    if (paramj != null) {
      try
      {
        Object localObject = InMobiBanner.access$500();
        localObject = ((ConcurrentHashMap)localObject).get(paramj);
        localObject = (WeakReference)localObject;
        if (localObject != null)
        {
          ConcurrentHashMap localConcurrentHashMap = InMobiBanner.access$500();
          localConcurrentHashMap.remove(paramj);
          paramj = ((WeakReference)localObject).get();
          paramj = (InMobiBanner.BannerAdRequestListener)paramj;
          if (paramj != null)
          {
            localObject = null;
            paramj.onAdRequestCompleted(paramInMobiAdRequestStatus, null);
          }
        }
      }
      catch (Exception paramj)
      {
        InMobiBanner.access$300();
        paramj.getMessage();
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.InMobiBanner.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */