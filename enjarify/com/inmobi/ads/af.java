package com.inmobi.ads;

import android.content.Context;
import android.os.Looper;
import android.support.v7.widget.RecyclerView;
import com.d.b.w;
import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;
import com.inmobi.commons.core.utilities.b.c;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

final class af
  extends j
{
  static final String x = "af";
  private a A;
  private int B = 0;
  WeakReference y;
  boolean z = false;
  
  private af(Context paramContext, long paramLong, j.b paramb)
  {
    super(paramContext, paramLong, paramb);
  }
  
  private void b(Context paramContext)
  {
    Object localObject = i();
    boolean bool = localObject instanceof ad;
    if (bool)
    {
      localObject = (ad)localObject;
      ((ad)localObject).a(paramContext);
    }
  }
  
  public final void C()
  {
    int i = a;
    int j = 1;
    if (j == i)
    {
      a = 9;
      boolean bool = m;
      if (!bool)
      {
        l();
        return;
      }
      j.d locald = p;
      if (locald != null)
      {
        locald = p;
        locald.a(this);
      }
    }
  }
  
  final void F()
  {
    j.b localb = f();
    String str = f;
    af.1 local1 = new com/inmobi/ads/af$1;
    local1.<init>(this);
    Looper localLooper = Looper.getMainLooper();
    a(localb, str, local1, localLooper);
  }
  
  public final void J()
  {
    try
    {
      super.r();
      Object localObject = null;
      f = null;
      return;
    }
    catch (Exception localException)
    {
      Logger.a(Logger.InternalLogLevel.ERROR, "InMobi", "Could not destroy native ad; SDK encountered unexpected error");
      localException.getMessage();
      com.inmobi.commons.core.a.a locala = com.inmobi.commons.core.a.a.a();
      com.inmobi.commons.core.e.a locala1 = new com/inmobi/commons/core/e/a;
      locala1.<init>(localException);
      locala.a(locala1);
    }
  }
  
  public final boolean K()
  {
    int i = a;
    int j = 5;
    return i == j;
  }
  
  public final void a(Context paramContext)
  {
    super.a(paramContext);
    b(paramContext);
  }
  
  final void a(boolean paramBoolean)
  {
    try
    {
      int i = m();
      if (i != 0)
      {
        String str = "IllegalState";
        c(str);
        return;
      }
      i = 1;
      a = i;
      super.a(paramBoolean);
      return;
    }
    catch (Exception localException)
    {
      Logger.a(Logger.InternalLogLevel.ERROR, "InMobi", "Unable to Prefetch ad; SDK encountered an unexpected error");
      localException.getMessage();
      com.inmobi.commons.core.a.a locala = com.inmobi.commons.core.a.a.a();
      com.inmobi.commons.core.e.a locala1 = new com/inmobi/commons/core/e/a;
      locala1.<init>(localException);
      locala.a(locala1);
    }
  }
  
  public final boolean a(a parama)
  {
    boolean bool = super.a(parama);
    if (!bool)
    {
      b(parama);
      return false;
    }
    return true;
  }
  
  protected final String b()
  {
    return "native";
  }
  
  protected final void b(long paramLong, boolean paramBoolean)
  {
    super.b(paramLong, paramBoolean);
    int i = 2;
    int j = 0;
    bt localbt = null;
    long l;
    int k;
    Object localObject1;
    Object localObject2;
    InMobiAdRequestStatus.StatusCode localStatusCode;
    if (!paramBoolean)
    {
      l = b;
      paramBoolean = paramLong < l;
      if (!paramBoolean)
      {
        k = a;
        if (i != k)
        {
          k = 5;
          int m = a;
          if (k != m) {}
        }
        else
        {
          a = 0;
          localObject1 = f();
          if (localObject1 != null)
          {
            localObject1 = f();
            localObject2 = new com/inmobi/ads/InMobiAdRequestStatus;
            localStatusCode = InMobiAdRequestStatus.StatusCode.AD_NO_LONGER_AVAILABLE;
            ((InMobiAdRequestStatus)localObject2).<init>(localStatusCode);
            ((j.b)localObject1).a((InMobiAdRequestStatus)localObject2);
          }
        }
      }
    }
    else
    {
      l = b;
      paramBoolean = paramLong < l;
      if (!paramBoolean)
      {
        k = a;
        if (i == k)
        {
          localObject1 = f();
          if (localObject1 != null)
          {
            localObject2 = A;
            paramBoolean = true;
            if (localObject2 != null)
            {
              boolean bool1 = localObject2 instanceof az;
              if (bool1)
              {
                localObject2 = (az)localObject2;
                com.inmobi.ads.cache.d.a();
                com.inmobi.ads.cache.a locala = com.inmobi.ads.cache.d.b(i);
                if (locala != null)
                {
                  boolean bool3 = locala.a();
                  if (bool3)
                  {
                    localbt = new com/inmobi/ads/bt;
                    String str1 = e;
                    String str2 = j;
                    String str3 = k;
                    List localList1 = ((az)localObject2).f();
                    List localList2 = ((az)localObject2).g();
                    localObject2 = e;
                    b.j localj = q;
                    localbt.<init>(str1, str2, str3, localList1, localList2, localj);
                    this.i = localbt;
                  }
                }
              }
              else
              {
                j = 1;
              }
            }
            if (j == 0)
            {
              localObject2 = new com/inmobi/ads/InMobiAdRequestStatus;
              localStatusCode = InMobiAdRequestStatus.StatusCode.INTERNAL_ERROR;
              ((InMobiAdRequestStatus)localObject2).<init>(localStatusCode);
              ((j.b)localObject1).a((InMobiAdRequestStatus)localObject2);
              return;
            }
            localObject1 = a();
            if (localObject1 != null)
            {
              boolean bool2 = s;
              if (bool2)
              {
                u = paramBoolean;
                E();
                return;
              }
              F();
            }
          }
        }
      }
    }
  }
  
  public final void b(InMobiAdRequestStatus paramInMobiAdRequestStatus)
  {
    int i = a;
    int j = 1;
    if (j == i)
    {
      i = 3;
      a = i;
      Object localObject = f();
      boolean bool = m;
      if ((!bool) && (localObject != null))
      {
        a((j.b)localObject, "VAR", "");
        a((j.b)localObject, "ARN", "");
        ((j.b)localObject).a(paramInMobiAdRequestStatus);
        return;
      }
      localObject = p;
      if (localObject != null)
      {
        localObject = p;
        ((j.d)localObject).a(this, paramInMobiAdRequestStatus);
      }
    }
  }
  
  protected final void b(a parama)
  {
    h.a(parama);
  }
  
  final void b(j.b paramb)
  {
    int i = a;
    int j = 7;
    int k = 5;
    if (i == k)
    {
      a = j;
    }
    else
    {
      i = a;
      if (i == j)
      {
        i = B + 1;
        B = i;
      }
    }
    Logger.InternalLogLevel localInternalLogLevel = Logger.InternalLogLevel.DEBUG;
    String str1 = "InMobi";
    Object localObject = new java/lang/StringBuilder;
    String str2 = "Successfully displayed fullscreen for placement id: ";
    ((StringBuilder)localObject).<init>(str2);
    long l = b;
    ((StringBuilder)localObject).append(l);
    localObject = ((StringBuilder)localObject).toString();
    Logger.a(localInternalLogLevel, str1, (String)localObject);
    i = B;
    if (i == 0)
    {
      if (paramb != null)
      {
        paramb.d();
        return;
      }
      g();
    }
  }
  
  protected final String c()
  {
    return null;
  }
  
  protected final void c(long paramLong, a parama)
  {
    super.c(paramLong, parama);
    A = parama;
    boolean bool1 = a(parama);
    if (bool1)
    {
      int i = q;
      boolean bool3 = true;
      if (i == 0)
      {
        bool2 = h;
        if (!bool2)
        {
          bool2 = false;
          a(bool3, null);
          break label66;
        }
      }
      c(parama);
      label66:
      boolean bool2 = h;
      if (bool2)
      {
        s = bool3;
        D();
      }
    }
  }
  
  final void c(j.b paramb)
  {
    int i = a;
    int j = 5;
    int k = 7;
    if (i == k)
    {
      i = B;
      if (i > 0)
      {
        i += -1;
        B = i;
      }
      else
      {
        a = j;
      }
    }
    Logger.InternalLogLevel localInternalLogLevel = Logger.InternalLogLevel.DEBUG;
    String str1 = "InMobi";
    Object localObject = new java/lang/StringBuilder;
    String str2 = "Successfully dismissed fullscreen for placement id: ";
    ((StringBuilder)localObject).<init>(str2);
    long l = b;
    ((StringBuilder)localObject).append(l);
    localObject = ((StringBuilder)localObject).toString();
    Logger.a(localInternalLogLevel, str1, (String)localObject);
    i = B;
    if (i == 0)
    {
      i = a;
      if (i == j)
      {
        if (paramb != null)
        {
          paramb.e();
          return;
        }
        g();
      }
    }
  }
  
  protected final AdContainer.RenderingProperties.PlacementType d()
  {
    return AdContainer.RenderingProperties.PlacementType.PLACEMENT_TYPE_INLINE;
  }
  
  protected final Map e()
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    String str1 = String.valueOf(aa);
    localHashMap.put("a-parentViewWidth", str1);
    localHashMap.put("a-productVersion", "NS-1.0.0-20160411");
    str1 = "url_ping";
    localHashMap.put("trackerType", str1);
    String str2 = "preload-request";
    boolean bool = m;
    if (bool) {
      str1 = "1";
    } else {
      str1 = "0";
    }
    localHashMap.put(str2, str1);
    return localHashMap;
  }
  
  public final void l()
  {
    boolean bool1 = v;
    if (bool1) {
      return;
    }
    j.b localb = f();
    Object localObject1 = RecyclerView.class;
    try
    {
      ((Class)localObject1).getName();
      localObject1 = w.class;
      ((Class)localObject1).getName();
      int i = 1;
      int j = a;
      if (i != j)
      {
        i = 2;
        j = a;
        if (i != j)
        {
          localObject1 = Logger.InternalLogLevel.DEBUG;
          localObject2 = x;
          Object localObject3 = new java/lang/StringBuilder;
          String str = "Fetching a Native ad for placement id: ";
          ((StringBuilder)localObject3).<init>(str);
          long l = b;
          ((StringBuilder)localObject3).append(l);
          localObject3 = ((StringBuilder)localObject3).toString();
          Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, (String)localObject3);
          i = 5;
          j = a;
          if (i == j)
          {
            boolean bool2 = h();
            if (!bool2)
            {
              a(localb, "VAR", "");
              localObject1 = "ARF";
              localObject2 = "";
              a(localb, (String)localObject1, (String)localObject2);
              if (localb != null)
              {
                localObject1 = a();
                b((Context)localObject1);
                localb.a();
              }
              return;
            }
          }
          super.l();
          return;
        }
      }
      localObject1 = Logger.InternalLogLevel.ERROR;
      localObject2 = x;
      Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "An ad load is already in progress. Please wait for the load to complete before requesting for another ad");
      a(localb, "ART", "LoadInProgress");
      return;
    }
    catch (NoClassDefFoundError localNoClassDefFoundError)
    {
      Object localObject2;
      localObject1 = "MissingDependency";
      a((String)localObject1);
      if (localb != null)
      {
        localObject1 = new com/inmobi/ads/InMobiAdRequestStatus;
        localObject2 = InMobiAdRequestStatus.StatusCode.MISSING_REQUIRED_DEPENDENCIES;
        ((InMobiAdRequestStatus)localObject1).<init>((InMobiAdRequestStatus.StatusCode)localObject2);
        localb.a((InMobiAdRequestStatus)localObject1);
      }
    }
  }
  
  final void n()
  {
    try
    {
      int i = m();
      if (i != 0)
      {
        str = "IllegalState";
        c(str);
        return;
      }
      i = 1;
      a = i;
      i = 0;
      String str = null;
      super.a(false);
      return;
    }
    catch (Exception localException)
    {
      Logger.a(Logger.InternalLogLevel.ERROR, "InMobi", "Unable to Prefetch ad; SDK encountered an unexpected error");
      localException.getMessage();
      com.inmobi.commons.core.a.a locala = com.inmobi.commons.core.a.a.a();
      com.inmobi.commons.core.e.a locala1 = new com/inmobi/commons/core/e/a;
      locala1.<init>(localException);
      locala.a(locala1);
    }
  }
  
  protected final int o()
  {
    int i = a;
    int j = 1;
    if (j == i)
    {
      Object localObject1 = Logger.InternalLogLevel.ERROR;
      Object localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("An ad load is already in progress. Please wait for the load to complete before requesting for another ad for placement id: ");
      long l = b;
      ((StringBuilder)localObject2).append(l);
      localObject2 = ((StringBuilder)localObject2).toString();
      Logger.a((Logger.InternalLogLevel)localObject1, "InMobi", (String)localObject2);
      localObject1 = f();
      a((j.b)localObject1, "ART", "LoadInProgress");
      return 2;
    }
    return super.o();
  }
  
  protected final boolean p()
  {
    int i = a;
    String str = null;
    int j = 1;
    if (j != i)
    {
      i = 2;
      int k = a;
      if (i != k) {
        return false;
      }
    }
    Object localObject = new com/inmobi/ads/InMobiAdRequestStatus;
    InMobiAdRequestStatus.StatusCode localStatusCode = InMobiAdRequestStatus.StatusCode.REQUEST_PENDING;
    ((InMobiAdRequestStatus)localObject).<init>(localStatusCode);
    a((InMobiAdRequestStatus)localObject, false);
    localObject = Logger.InternalLogLevel.ERROR;
    str = x;
    Logger.a((Logger.InternalLogLevel)localObject, str, "An ad load is already in progress. Please wait for the load to complete before requesting for another ad");
    return j;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.af
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */