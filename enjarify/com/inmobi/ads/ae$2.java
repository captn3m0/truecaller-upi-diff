package com.inmobi.ads;

import android.graphics.Rect;
import android.view.View;

final class ae$2
  implements ca.a
{
  private final Rect a;
  
  ae$2()
  {
    Rect localRect = new android/graphics/Rect;
    localRect.<init>();
    a = localRect;
  }
  
  public final boolean a(View paramView1, View paramView2, int paramInt, Object paramObject)
  {
    boolean bool1 = paramObject instanceof ad;
    if (!bool1) {
      return false;
    }
    paramObject = (ad)paramObject;
    boolean bool2 = i;
    if (bool2) {
      return false;
    }
    bool2 = paramView2 instanceof NativeVideoView;
    if (bool2)
    {
      paramObject = paramView2;
      paramObject = ((NativeVideoView)paramView2).getMediaPlayer();
      if (paramObject != null)
      {
        int i = 3;
        int j = a;
        if (i != j) {
          return false;
        }
      }
    }
    if ((paramView2 != null) && (paramView1 != null))
    {
      boolean bool3 = ((View)paramView2).isShown();
      if (bool3)
      {
        paramObject = paramView1.getParent();
        if (paramObject != null)
        {
          paramObject = a;
          boolean bool4 = ((View)paramView2).getGlobalVisibleRect((Rect)paramObject);
          if (!bool4) {
            return false;
          }
          long l1 = a.height();
          paramView2 = a;
          long l2 = paramView2.width();
          l1 *= l2;
          int k = paramView1.getWidth();
          l2 = k;
          int m = paramView1.getHeight();
          long l3 = m;
          l2 *= l3;
          l3 = 0L;
          bool3 = l2 < l3;
          if (bool3)
          {
            l1 *= 100;
            l3 = paramInt * l2;
            paramInt = l1 < l3;
            if (paramInt >= 0) {
              return true;
            }
          }
          return false;
        }
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.ae.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */