package com.inmobi.ads;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import com.inmobi.a.a.1;
import com.inmobi.commons.core.configs.a.a;
import com.inmobi.commons.core.configs.b.c;
import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;
import com.inmobi.commons.core.utilities.b.e;
import com.inmobi.rendering.RenderView;
import com.inmobi.rendering.RenderView.a;
import com.inmobi.signals.p;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class j
  implements bi.a, h.a, b.c, RenderView.a, com.inmobi.rendering.a
{
  private static final String x = "j";
  private long A;
  private WeakReference B;
  private RenderView C;
  private bj D;
  private long E;
  private long F = 0L;
  private j.a G;
  private Runnable H;
  private Set I;
  private InMobiAdRequest.MonetizationContext J;
  private bi K;
  private boolean L;
  private RenderView.a M;
  int a;
  long b;
  String c;
  Map d;
  b e;
  String f;
  JSONObject g;
  h h;
  bt i;
  String j;
  String k;
  String l;
  boolean m = false;
  ad n;
  ExecutorService o;
  j.d p;
  int q;
  Handler r;
  boolean s;
  RenderView t;
  boolean u;
  boolean v = false;
  String w;
  private WeakReference y;
  private long z;
  
  public j(Context paramContext, long paramLong, j.b paramb)
  {
    Object localObject1 = new com/inmobi/ads/j$5;
    ((j.5)localObject1).<init>(this);
    M = ((RenderView.a)localObject1);
    localObject1 = new java/lang/ref/WeakReference;
    ((WeakReference)localObject1).<init>(paramContext);
    y = ((WeakReference)localObject1);
    b = paramLong;
    paramContext = new java/lang/ref/WeakReference;
    paramContext.<init>(paramb);
    B = paramContext;
    l = "unknown";
    paramContext = new com/inmobi/ads/h;
    paramContext.<init>(this);
    h = paramContext;
    paramContext = new com/inmobi/ads/b;
    paramContext.<init>();
    e = paramContext;
    paramContext = com.inmobi.commons.core.configs.b.a();
    Object localObject2 = new com/inmobi/commons/core/configs/g;
    ((com.inmobi.commons.core.configs.g)localObject2).<init>();
    paramContext.a((com.inmobi.commons.core.configs.a)localObject2, null);
    paramContext = com.inmobi.commons.core.configs.b.a();
    localObject2 = e;
    paramContext.a((com.inmobi.commons.core.configs.a)localObject2, this);
    paramContext = new com/inmobi/ads/bj;
    paramContext.<init>(this);
    D = paramContext;
    paramContext = new java/util/HashSet;
    paramContext.<init>();
    I = paramContext;
    paramContext = Executors.newSingleThreadExecutor();
    o = paramContext;
    q = -1;
    paramContext = new com/inmobi/ads/j$7;
    paramContext.<init>(this);
    H = paramContext;
    paramContext = com.inmobi.commons.core.e.b.a();
    JSONObject localJSONObject = e.p;
    paramContext.a("ads", localJSONObject);
    paramContext = new android/os/Handler;
    localObject2 = Looper.getMainLooper();
    paramContext.<init>((Looper)localObject2);
    r = paramContext;
    s = false;
    w = "";
    paramContext = new org/json/JSONObject;
    paramContext.<init>();
    g = paramContext;
    a = 0;
  }
  
  private i J()
  {
    i locali = new com/inmobi/ads/i;
    locali.<init>();
    Object localObject1 = c;
    e = ((String)localObject1);
    localObject1 = d;
    g = ((Map)localObject1);
    long l1 = b;
    d = l1;
    localObject1 = b();
    h = ((String)localObject1);
    localObject1 = e;
    Object localObject2 = b();
    localObject1 = ((b)localObject1).a((String)localObject2);
    j = ((b.d)localObject1);
    localObject1 = e();
    k = ((Map)localObject1);
    i = "sdkJson";
    localObject1 = e.e;
    a = ((String)localObject1);
    int i1 = e.i;
    c = i1;
    i1 = e.g;
    b = i1;
    localObject1 = c();
    f = ((String)localObject1);
    localObject1 = k();
    m = ((InMobiAdRequest.MonetizationContext)localObject1);
    boolean bool = K();
    n = bool;
    localObject1 = new com/inmobi/commons/core/utilities/uid/d;
    localObject2 = e.s.a;
    ((com.inmobi.commons.core.utilities.uid.d)localObject1).<init>((Map)localObject2);
    l = ((com.inmobi.commons.core.utilities.uid.d)localObject1);
    return locali;
  }
  
  private boolean K()
  {
    b.h localh = e.m;
    boolean bool = l;
    if (bool)
    {
      bool = com.inmobi.commons.a.a.d();
      if (bool) {
        return true;
      }
    }
    return false;
  }
  
  private static String a(String paramString, Map paramMap)
  {
    if ((paramMap != null) && (paramString != null))
    {
      Iterator localIterator = paramMap.keySet().iterator();
      for (;;)
      {
        boolean bool = localIterator.hasNext();
        if (!bool) {
          break;
        }
        String str = (String)localIterator.next();
        CharSequence localCharSequence = (CharSequence)paramMap.get(str);
        paramString = paramString.replace(str, localCharSequence);
      }
      return paramString;
    }
    return paramString;
  }
  
  private void a(String paramString, WeakReference paramWeakReference)
  {
    Handler localHandler = r;
    j.8 local8 = new com/inmobi/ads/j$8;
    local8.<init>(this, paramString, paramWeakReference);
    localHandler.post(local8);
  }
  
  protected void A()
  {
    Object localObject = "RenderTimeOut";
    b((String)localObject);
    int i1 = a;
    int i2 = 2;
    if (i1 == i2)
    {
      i1 = 3;
      a = i1;
      localObject = f();
      if (localObject != null)
      {
        localObject = f();
        InMobiAdRequestStatus localInMobiAdRequestStatus = new com/inmobi/ads/InMobiAdRequestStatus;
        InMobiAdRequestStatus.StatusCode localStatusCode = InMobiAdRequestStatus.StatusCode.INTERNAL_ERROR;
        localInMobiAdRequestStatus.<init>(localStatusCode);
        ((j.b)localObject).a(localInMobiAdRequestStatus);
      }
    }
  }
  
  final void B()
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    long l1 = SystemClock.elapsedRealtime();
    long l2 = F;
    Long localLong = Long.valueOf(l1 - l2);
    localHashMap.put("latency", localLong);
    c("ads", "AdLoadSuccessful", localHashMap);
  }
  
  public void C()
  {
    int i1 = a;
    int i2 = 1;
    if (i2 == i1)
    {
      j.d locald = p;
      if (locald != null) {
        locald.a(this);
      }
    }
  }
  
  final void D()
  {
    ExecutorService localExecutorService = o;
    j.4 local4 = new com/inmobi/ads/j$4;
    local4.<init>(this);
    localExecutorService.execute(local4);
  }
  
  final void E()
  {
    boolean bool = s;
    if (bool)
    {
      bool = u;
      if (bool)
      {
        bool = L;
        if (bool)
        {
          y();
          F();
        }
      }
    }
  }
  
  void F() {}
  
  void G() {}
  
  public final Context a()
  {
    WeakReference localWeakReference = y;
    if (localWeakReference == null) {
      return null;
    }
    return (Context)localWeakReference.get();
  }
  
  public final void a(long paramLong)
  {
    Object localObject = "AdPrefetchSuccessful";
    d((String)localObject);
    boolean bool = v;
    if (!bool)
    {
      localObject = a();
      if (localObject != null)
      {
        localObject = G;
        if (localObject != null)
        {
          localObject = Message.obtain();
          what = 14;
          Bundle localBundle = new android/os/Bundle;
          localBundle.<init>();
          localBundle.putLong("placementId", paramLong);
          ((Message)localObject).setData(localBundle);
          G.sendMessage((Message)localObject);
          return;
        }
      }
    }
  }
  
  public void a(long paramLong, InMobiAdRequestStatus paramInMobiAdRequestStatus)
  {
    boolean bool = v;
    if (!bool)
    {
      Object localObject = a();
      if (localObject != null)
      {
        localObject = G;
        if (localObject != null)
        {
          localObject = r;
          j.1 local1 = new com/inmobi/ads/j$1;
          local1.<init>(this, paramLong, paramInMobiAdRequestStatus);
          ((Handler)localObject).post(local1);
          return;
        }
      }
    }
  }
  
  public final void a(long paramLong, a parama)
  {
    boolean bool = v;
    if (!bool)
    {
      Object localObject = a();
      if (localObject != null)
      {
        localObject = G;
        if (localObject != null)
        {
          localObject = Message.obtain();
          what = 2;
          Bundle localBundle = new android/os/Bundle;
          localBundle.<init>();
          localBundle.putLong("placementId", paramLong);
          ((Message)localObject).setData(localBundle);
          obj = parama;
          G.sendMessage((Message)localObject);
          return;
        }
      }
    }
  }
  
  public final void a(long paramLong, boolean paramBoolean)
  {
    boolean bool = v;
    if (!bool)
    {
      Object localObject = a();
      if (localObject != null)
      {
        localObject = G;
        if (localObject != null)
        {
          localObject = Message.obtain();
          what = 4;
          Bundle localBundle = new android/os/Bundle;
          localBundle.<init>();
          localBundle.putLong("placementId", paramLong);
          localBundle.putBoolean("assetAvailable", paramBoolean);
          ((Message)localObject).setData(localBundle);
          G.sendMessage((Message)localObject);
          return;
        }
      }
    }
  }
  
  protected void a(long paramLong, boolean paramBoolean, a parama)
  {
    long l1 = b;
    boolean bool = paramLong < l1;
    if (!bool)
    {
      int i1 = 1;
      int i2 = a;
      if ((i1 == i2) && (paramBoolean))
      {
        paramLong = d;
        z = paramLong;
        paramLong = parama.b();
        A = paramLong;
      }
    }
  }
  
  public void a(Context paramContext)
  {
    WeakReference localWeakReference = new java/lang/ref/WeakReference;
    localWeakReference.<init>(paramContext);
    y = localWeakReference;
  }
  
  public void a(InMobiAdRequest.MonetizationContext paramMonetizationContext)
  {
    J = paramMonetizationContext;
  }
  
  final void a(InMobiAdRequestStatus paramInMobiAdRequestStatus)
  {
    InMobiAdRequestStatus.StatusCode localStatusCode1 = InMobiAdRequestStatus.StatusCode.NO_FILL;
    InMobiAdRequestStatus.StatusCode localStatusCode2 = paramInMobiAdRequestStatus.getStatusCode();
    boolean bool1 = localStatusCode1.equals(localStatusCode2);
    if (bool1)
    {
      b("NoFill");
      return;
    }
    localStatusCode1 = InMobiAdRequestStatus.StatusCode.SERVER_ERROR;
    localStatusCode2 = paramInMobiAdRequestStatus.getStatusCode();
    bool1 = localStatusCode1.equals(localStatusCode2);
    if (bool1)
    {
      b("ServerError");
      return;
    }
    localStatusCode1 = InMobiAdRequestStatus.StatusCode.NETWORK_UNREACHABLE;
    localStatusCode2 = paramInMobiAdRequestStatus.getStatusCode();
    bool1 = localStatusCode1.equals(localStatusCode2);
    if (bool1)
    {
      b("NetworkUnreachable");
      return;
    }
    localStatusCode1 = InMobiAdRequestStatus.StatusCode.AD_ACTIVE;
    localStatusCode2 = paramInMobiAdRequestStatus.getStatusCode();
    bool1 = localStatusCode1.equals(localStatusCode2);
    if (bool1)
    {
      b("AdActive");
      return;
    }
    localStatusCode1 = InMobiAdRequestStatus.StatusCode.REQUEST_PENDING;
    localStatusCode2 = paramInMobiAdRequestStatus.getStatusCode();
    bool1 = localStatusCode1.equals(localStatusCode2);
    if (bool1)
    {
      b("RequestPending");
      return;
    }
    localStatusCode1 = InMobiAdRequestStatus.StatusCode.REQUEST_INVALID;
    localStatusCode2 = paramInMobiAdRequestStatus.getStatusCode();
    bool1 = localStatusCode1.equals(localStatusCode2);
    if (bool1)
    {
      b("RequestInvalid");
      return;
    }
    localStatusCode1 = InMobiAdRequestStatus.StatusCode.REQUEST_TIMED_OUT;
    localStatusCode2 = paramInMobiAdRequestStatus.getStatusCode();
    bool1 = localStatusCode1.equals(localStatusCode2);
    if (bool1)
    {
      b("RequestTimedOut");
      return;
    }
    localStatusCode1 = InMobiAdRequestStatus.StatusCode.EARLY_REFRESH_REQUEST;
    localStatusCode2 = paramInMobiAdRequestStatus.getStatusCode();
    bool1 = localStatusCode1.equals(localStatusCode2);
    if (bool1)
    {
      b("EarlyRefreshRequest");
      return;
    }
    localStatusCode1 = InMobiAdRequestStatus.StatusCode.INTERNAL_ERROR;
    paramInMobiAdRequestStatus = paramInMobiAdRequestStatus.getStatusCode();
    boolean bool2 = localStatusCode1.equals(paramInMobiAdRequestStatus);
    if (bool2)
    {
      paramInMobiAdRequestStatus = "InternalError";
      b(paramInMobiAdRequestStatus);
    }
  }
  
  void a(InMobiAdRequestStatus paramInMobiAdRequestStatus, boolean paramBoolean)
  {
    int i1 = a;
    int i2 = 1;
    if ((i1 == i2) && (paramBoolean))
    {
      paramBoolean = true;
      a = paramBoolean;
    }
    j.b localb = f();
    if (localb != null) {
      localb.a(paramInMobiAdRequestStatus);
    }
    a(paramInMobiAdRequestStatus);
  }
  
  final void a(j.b paramb)
  {
    WeakReference localWeakReference = new java/lang/ref/WeakReference;
    localWeakReference.<init>(paramb);
    B = localWeakReference;
  }
  
  final void a(j.b paramb, String paramString, Runnable paramRunnable, Looper paramLooper)
  {
    Object localObject1 = "html";
    Object localObject2 = l;
    boolean bool1 = ((String)localObject1).equals(localObject2);
    if (bool1)
    {
      paramb = r;
      paramRunnable = new com/inmobi/ads/j$10;
      paramRunnable.<init>(this, paramString);
      paramb.post(paramRunnable);
      return;
    }
    paramString = "inmobiJson";
    localObject1 = l;
    boolean bool2 = paramString.equals(localObject1);
    if (bool2)
    {
      paramString = new java/lang/ref/WeakReference;
      paramString.<init>(paramb);
      try
      {
        long l1 = SystemClock.elapsedRealtime();
        F = l1;
        ak localak = new com/inmobi/ads/ak;
        paramb = d();
        localObject1 = new org/json/JSONObject;
        localObject2 = f;
        ((JSONObject)localObject1).<init>((String)localObject2);
        localObject2 = e;
        Object localObject3 = i;
        localak.<init>(paramb, (JSONObject)localObject1, (b)localObject2, (bt)localObject3);
        boolean bool3 = localak.c();
        if (bool3)
        {
          paramb = a();
          if (paramb != null)
          {
            localObject3 = a();
            AdContainer.RenderingProperties localRenderingProperties = new com/inmobi/ads/AdContainer$RenderingProperties;
            paramb = d();
            localRenderingProperties.<init>(paramb);
            String str1 = j;
            String str2 = k;
            Set localSet = I;
            b localb = e;
            paramb = ad.b.a((Context)localObject3, localRenderingProperties, localak, str1, str2, localSet, localb);
            localObject1 = new com/inmobi/ads/j$9;
            ((j.9)localObject1).<init>(this, paramString);
            paramb.a((ad.c)localObject1);
            n = paramb;
            if ((paramRunnable != null) && (paramLooper != null))
            {
              paramb = new android/os/Handler;
              paramb.<init>(paramLooper);
              paramb.post(paramRunnable);
            }
            return;
          }
        }
        paramb = "DataModelValidationFailed";
        a(paramb, paramString);
        return;
      }
      catch (Exception paramb)
      {
        paramb.getMessage();
        a("InternalError", paramString);
        paramString = com.inmobi.commons.core.a.a.a();
        paramRunnable = new com/inmobi/commons/core/e/a;
        paramRunnable.<init>(paramb);
        paramString.a(paramRunnable);
      }
      catch (JSONException paramb)
      {
        a("InternalError", paramString);
        paramString = com.inmobi.commons.core.a.a.a();
        paramRunnable = new com/inmobi/commons/core/e/a;
        paramRunnable.<init>(paramb);
        paramString.a(paramRunnable);
        return;
      }
    }
  }
  
  final void a(j.b paramb, String paramString1, String paramString2)
  {
    j localj = this;
    if (paramb != null)
    {
      boolean bool1 = paramb.i();
      if (bool1)
      {
        Object localObject1 = e;
        Object localObject2 = b();
        Object localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>();
        ((StringBuilder)localObject3).append((String)localObject2);
        ((StringBuilder)localObject3).append("Dict");
        localObject2 = ((StringBuilder)localObject3).toString();
        localObject3 = k;
        localObject2 = (b.a)((Map)localObject3).get(localObject2);
        if (localObject2 == null) {
          localObject2 = j;
        }
        bool1 = h;
        if (bool1)
        {
          localObject1 = "";
          localObject2 = k;
          Object localObject4;
          if (localObject2 != null) {
            localObject4 = localObject2;
          } else {
            localObject4 = localObject1;
          }
          localObject1 = new com/inmobi/commons/core/f/b;
          Object localObject5 = UUID.randomUUID().toString();
          String str1 = l;
          long l1 = b;
          boolean bool2 = K();
          localObject2 = com.inmobi.commons.core.utilities.b.b.a(bool2).get("d-nettype-raw");
          Object localObject6 = localObject2;
          localObject6 = (String)localObject2;
          String str2 = b();
          long l2 = System.currentTimeMillis();
          localObject3 = localObject1;
          ((com.inmobi.commons.core.f.b)localObject1).<init>((String)localObject5, str1, paramString1, l1, (String)localObject4, paramString2, (String)localObject6, str2, l2);
          localObject2 = com.inmobi.a.a.a();
          localObject3 = c;
          localObject5 = j;
          localObject3 = ((b)localObject3).b((String)localObject5);
          boolean bool3 = h;
          if (bool3)
          {
            localObject3 = a;
            localObject5 = new com/inmobi/a/a$1;
            ((a.1)localObject5).<init>((com.inmobi.a.a)localObject2, (com.inmobi.commons.core.f.b)localObject1);
            ((ExecutorService)localObject3).execute((Runnable)localObject5);
          }
        }
      }
    }
  }
  
  public final void a(com.inmobi.commons.core.configs.a parama)
  {
    parama = (b)parama;
    e = parama;
    parama = com.inmobi.commons.core.e.b.a();
    JSONObject localJSONObject = e.p;
    parama.a("ads", localJSONObject);
  }
  
  public void a(RenderView paramRenderView)
  {
    boolean bool = v;
    if (!bool)
    {
      paramRenderView = a();
      if (paramRenderView != null)
      {
        paramRenderView = G;
        if (paramRenderView != null) {}
      }
    }
  }
  
  final void a(String paramString)
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    localHashMap.put("errorCode", paramString);
    c("ads", "AdLoadRejected", localHashMap);
  }
  
  public final void a(String paramString1, String paramString2, com.inmobi.rendering.b paramb)
  {
    ExecutorService localExecutorService = o;
    j.3 local3 = new com/inmobi/ads/j$3;
    local3.<init>(this, paramb, paramString1, paramString2);
    localExecutorService.execute(local3);
  }
  
  public final void a(String paramString1, String paramString2, Map paramMap)
  {
    c(paramString1, paramString2, paramMap);
  }
  
  public final void a(HashMap paramHashMap)
  {
    boolean bool = v;
    if (!bool)
    {
      Object localObject = a();
      if (localObject != null)
      {
        localObject = G;
        if (localObject != null)
        {
          localObject = f();
          if (localObject != null)
          {
            localObject = f();
            ((j.b)localObject).b(paramHashMap);
          }
          return;
        }
      }
    }
  }
  
  void a(boolean paramBoolean)
  {
    d("AdPrefetchRequested");
    ExecutorService localExecutorService = o;
    j.6 local6 = new com/inmobi/ads/j$6;
    local6.<init>(this, paramBoolean);
    localExecutorService.execute(local6);
  }
  
  final void a(boolean paramBoolean, RenderView paramRenderView)
  {
    b.k localk = e.o;
    boolean bool1 = j;
    Iterator localIterator = I.iterator();
    for (;;)
    {
      boolean bool2 = localIterator.hasNext();
      if (!bool2) {
        break;
      }
      Object localObject1 = (bm)localIterator.next();
      if (bool1)
      {
        int i1 = 3;
        int i2 = a;
        if (i1 == i2) {
          try
          {
            localObject2 = a();
            localObject3 = b;
            String str = "creativeType";
            localObject3 = ((Map)localObject3).get(str);
            localObject3 = (String)localObject3;
            localObject2 = q.a((Context)localObject2, paramBoolean, (String)localObject3, paramRenderView);
            if (localObject2 != null)
            {
              localObject3 = b;
              str = "avidAdSession";
              ((Map)localObject3).put(str, localObject2);
              localObject1 = b;
              localObject2 = "deferred";
              localObject3 = Boolean.valueOf(paramBoolean);
              ((Map)localObject1).put(localObject2, localObject3);
            }
          }
          catch (Exception localException)
          {
            localException.getMessage();
            Object localObject2 = com.inmobi.commons.core.a.a.a();
            Object localObject3 = new com/inmobi/commons/core/e/a;
            ((com.inmobi.commons.core.e.a)localObject3).<init>(localException);
            ((com.inmobi.commons.core.a.a)localObject2).a((com.inmobi.commons.core.e.a)localObject3);
          }
        }
      }
    }
  }
  
  public boolean a(a parama)
  {
    boolean bool1 = false;
    Object localObject1 = null;
    Object localObject2;
    try
    {
      localObject2 = new org/json/JSONObject;
      localObject3 = b;
      ((JSONObject)localObject2).<init>((String)localObject3);
      long l1 = d;
      z = l1;
      l1 = parama.b();
      A = l1;
      localObject3 = e;
      j = ((String)localObject3);
      parama = f;
      k = parama;
      parama = "markupType";
      parama = ((JSONObject)localObject2).optString(parama);
      int i1 = 2;
      int i3 = 1;
      if (parama != null)
      {
        int i4 = parama.length();
        if (i4 != 0)
        {
          i4 = parama.hashCode();
          int i5 = -1084172778;
          boolean bool7;
          if (i4 != i5)
          {
            i5 = 3213227;
            if (i4 == i5)
            {
              localObject4 = "html";
              bool7 = parama.equals(localObject4);
              if (bool7)
              {
                bool7 = true;
                break label187;
              }
            }
          }
          else
          {
            localObject4 = "inmobiJson";
            bool7 = parama.equals(localObject4);
            if (bool7)
            {
              i6 = 2;
              break label187;
            }
          }
          int i6 = -1;
          switch (i6)
          {
          default: 
            parama = "unknown";
            break;
          case 2: 
            parama = "inmobiJson";
            break;
          case 1: 
            label187:
            parama = "html";
            break;
          }
        }
      }
      parama = "html";
      l = parama;
      parama = "bidInfo";
      boolean bool8 = ((JSONObject)localObject2).has(parama);
      if (bool8)
      {
        parama = "bidInfo";
        parama = ((JSONObject)localObject2).getJSONObject(parama);
      }
      else
      {
        parama = new org/json/JSONObject;
        parama.<init>();
      }
      g = parama;
      parama = "unknown";
      Object localObject4 = l;
      bool8 = parama.equals(localObject4);
      if (bool8) {
        return false;
      }
      parama = "inmobiJson";
      localObject4 = l;
      bool8 = parama.equals(localObject4);
      if (bool8)
      {
        parama = "pubContent";
        parama = ((JSONObject)localObject2).getJSONObject(parama);
        parama = parama.toString();
        f = parama;
      }
      else
      {
        parama = "pubContent";
        parama = ((JSONObject)localObject2).getString(parama);
        parama = parama.trim();
        f = parama;
      }
      parama = f;
      if (parama != null)
      {
        parama = f;
        int i7 = parama.length();
        if (i7 != 0)
        {
          parama = "unknown";
          localObject4 = l;
          bool9 = parama.equals(localObject4);
          if (!bool9)
          {
            parama = f;
            localObject4 = "@__imm_aft@";
            long l2 = System.currentTimeMillis();
            long l3 = E;
            l2 -= l3;
            localObject5 = String.valueOf(l2);
            parama = parama.replace((CharSequence)localObject4, (CharSequence)localObject5);
            f = parama;
            bool9 = true;
            break label484;
          }
        }
      }
      boolean bool9 = false;
      parama = null;
      label484:
      localObject4 = "creativeId";
      Object localObject5 = "";
      try
      {
        localObject4 = ((JSONObject)localObject2).optString((String)localObject4, (String)localObject5);
        w = ((String)localObject4);
        localObject4 = I;
        boolean bool5 = ((Set)localObject4).isEmpty();
        if (bool5)
        {
          localObject4 = "viewability";
          bool5 = ((JSONObject)localObject2).has((String)localObject4);
          if (bool5)
          {
            localObject4 = new com/inmobi/ads/bm;
            ((bm)localObject4).<init>(i3);
            localObject5 = "viewability";
            localObject5 = ((JSONObject)localObject2).getJSONArray((String)localObject5);
            localObject5 = j.c.a((JSONArray)localObject5);
            b = ((Map)localObject5);
            localObject5 = I;
            ((Set)localObject5).add(localObject4);
            localObject4 = a();
            if (localObject4 != null)
            {
              boolean bool6 = localObject4 instanceof Activity;
              if (bool6)
              {
                localObject4 = (Activity)localObject4;
                localObject4 = ((Activity)localObject4).getApplication();
                u.a((Application)localObject4);
              }
            }
          }
          localObject4 = "metaInfo";
          bool5 = ((JSONObject)localObject2).has((String)localObject4);
          label864:
          String str2;
          if (bool5)
          {
            localObject4 = "metaInfo";
            localObject4 = ((JSONObject)localObject2).getJSONObject((String)localObject4);
            localObject5 = "unknown";
            Object localObject6 = "creativeType";
            boolean bool10 = ((JSONObject)localObject4).has((String)localObject6);
            if (bool10)
            {
              localObject5 = "creativeType";
              localObject5 = ((JSONObject)localObject4).getString((String)localObject5);
            }
            localObject6 = "iasEnabled";
            bool10 = ((JSONObject)localObject4).has((String)localObject6);
            if (bool10)
            {
              localObject6 = "iasEnabled";
              bool5 = ((JSONObject)localObject4).getBoolean((String)localObject6);
              if (bool5)
              {
                localObject4 = new com/inmobi/ads/bm;
                int i8 = 3;
                ((bm)localObject4).<init>(i8);
                localObject6 = new java/util/HashMap;
                ((HashMap)localObject6).<init>();
                int i9 = ((String)localObject5).hashCode();
                int i10 = 112202875;
                if (i9 != i10)
                {
                  i1 = 1425678798;
                  if (i9 == i1)
                  {
                    localObject3 = "nonvideo";
                    boolean bool2 = ((String)localObject5).equals(localObject3);
                    if (bool2)
                    {
                      bool2 = true;
                      break label864;
                    }
                  }
                }
                else
                {
                  String str1 = "video";
                  boolean bool4 = ((String)localObject5).equals(str1);
                  if (bool4) {
                    break label864;
                  }
                }
                int i2 = -1;
                switch (i2)
                {
                default: 
                  localObject3 = "unknown";
                  break;
                case 2: 
                  localObject3 = "video";
                  break;
                case 1: 
                  localObject3 = "nonvideo";
                }
                str2 = "creativeType";
                ((Map)localObject6).put(str2, localObject3);
                b = ((Map)localObject6);
                localObject3 = I;
                ((Set)localObject3).add(localObject4);
              }
            }
          }
          localObject3 = "tracking";
          boolean bool3 = ((JSONObject)localObject2).has((String)localObject3);
          if (bool3)
          {
            localObject3 = "web";
            str2 = "tracking";
            localObject2 = ((JSONObject)localObject2).getString(str2);
            boolean bool11 = ((String)localObject3).equals(localObject2);
            if (bool11) {
              q = 0;
            }
          }
        }
        bool1 = bool9;
      }
      catch (IllegalArgumentException localIllegalArgumentException)
      {
        bool1 = bool9;
        parama = localIllegalArgumentException;
      }
      catch (JSONException localJSONException)
      {
        bool1 = bool9;
        parama = localJSONException;
      }
      localObject2 = com.inmobi.commons.core.a.a.a();
    }
    catch (IllegalArgumentException parama)
    {
      localObject2 = com.inmobi.commons.core.a.a.a();
      localObject3 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject3).<init>(parama);
      ((com.inmobi.commons.core.a.a)localObject2).a((com.inmobi.commons.core.e.a)localObject3);
    }
    catch (JSONException parama) {}
    Object localObject3 = new com/inmobi/commons/core/e/a;
    ((com.inmobi.commons.core.e.a)localObject3).<init>(parama);
    ((com.inmobi.commons.core.a.a)localObject2).a((com.inmobi.commons.core.e.a)localObject3);
    return bool1;
  }
  
  protected abstract String b();
  
  public final void b(long paramLong, InMobiAdRequestStatus paramInMobiAdRequestStatus)
  {
    Object localObject = InMobiAdRequestStatus.StatusCode.EARLY_REFRESH_REQUEST;
    InMobiAdRequestStatus.StatusCode localStatusCode = paramInMobiAdRequestStatus.getStatusCode();
    boolean bool = ((InMobiAdRequestStatus.StatusCode)localObject).equals(localStatusCode);
    if (bool)
    {
      localObject = "EarlyRefreshRequest";
      c((String)localObject);
    }
    else
    {
      localObject = InMobiAdRequestStatus.StatusCode.NETWORK_UNREACHABLE;
      localStatusCode = paramInMobiAdRequestStatus.getStatusCode();
      bool = ((InMobiAdRequestStatus.StatusCode)localObject).equals(localStatusCode);
      if (bool)
      {
        localObject = "NetworkUnreachable";
        c((String)localObject);
      }
      else
      {
        localObject = "AdPrefetchFailed";
        d((String)localObject);
      }
    }
    bool = v;
    if (!bool)
    {
      localObject = a();
      if (localObject != null)
      {
        localObject = G;
        if (localObject != null)
        {
          localObject = Message.obtain();
          what = 13;
          obj = paramInMobiAdRequestStatus;
          paramInMobiAdRequestStatus = new android/os/Bundle;
          paramInMobiAdRequestStatus.<init>();
          paramInMobiAdRequestStatus.putLong("placementId", paramLong);
          ((Message)localObject).setData(paramInMobiAdRequestStatus);
          G.sendMessage((Message)localObject);
          return;
        }
      }
    }
  }
  
  public final void b(long paramLong, a parama)
  {
    boolean bool = v;
    if (!bool)
    {
      Object localObject = a();
      if (localObject != null)
      {
        localObject = G;
        if (localObject != null)
        {
          long l1 = SystemClock.elapsedRealtime();
          F = l1;
          localObject = Message.obtain();
          int i1 = 1;
          what = i1;
          obj = parama;
          parama = new android/os/Bundle;
          parama.<init>();
          parama.putLong("placementId", paramLong);
          parama.putBoolean("adAvailable", i1);
          ((Message)localObject).setData(parama);
          G.sendMessage((Message)localObject);
          return;
        }
      }
    }
  }
  
  protected void b(long paramLong, boolean paramBoolean) {}
  
  public void b(InMobiAdRequestStatus paramInMobiAdRequestStatus)
  {
    int i1 = a;
    int i2 = 1;
    if (i2 == i1)
    {
      j.d locald = p;
      if (locald != null) {
        locald.a(this, paramInMobiAdRequestStatus);
      }
    }
  }
  
  protected abstract void b(a parama);
  
  void b(j.b paramb) {}
  
  public void b(RenderView paramRenderView)
  {
    boolean bool = v;
    if (!bool)
    {
      paramRenderView = a();
      if (paramRenderView != null)
      {
        paramRenderView = G;
        if (paramRenderView != null) {}
      }
    }
  }
  
  final void b(String paramString)
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    localHashMap.put("errorCode", paramString);
    long l1 = SystemClock.elapsedRealtime();
    long l2 = F;
    Long localLong = Long.valueOf(l1 - l2);
    localHashMap.put("latency", localLong);
    c("ads", "AdLoadFailed", localHashMap);
  }
  
  public final void b(String paramString1, String paramString2, Map paramMap)
  {
    c(paramString1, paramString2, paramMap);
  }
  
  public final void b(HashMap paramHashMap)
  {
    boolean bool = v;
    if (!bool)
    {
      Object localObject = a();
      if (localObject != null)
      {
        localObject = G;
        if (localObject != null)
        {
          d("AdInteracted");
          localObject = f();
          if (localObject != null)
          {
            localObject = f();
            ((j.b)localObject).a(paramHashMap);
          }
          return;
        }
      }
    }
  }
  
  protected abstract String c();
  
  protected void c(long paramLong, a parama)
  {
    long l1 = b;
    boolean bool1 = paramLong < l1;
    if (!bool1)
    {
      int i1 = a;
      int i2 = 1;
      if (i1 == i2)
      {
        boolean bool2 = a(parama);
        if (bool2)
        {
          localObject = f();
          a((j.b)localObject, "ARF", "");
          paramLong = SystemClock.elapsedRealtime();
          F = paramLong;
          a = 2;
          return;
        }
        b("ParsingFailed");
        Object localObject = new com/inmobi/ads/InMobiAdRequestStatus;
        parama = InMobiAdRequestStatus.StatusCode.INTERNAL_ERROR;
        ((InMobiAdRequestStatus)localObject).<init>(parama);
        a((InMobiAdRequestStatus)localObject, i2);
      }
    }
  }
  
  final void c(a parama)
  {
    boolean bool1 = parama instanceof az;
    if (!bool1) {
      return;
    }
    parama = (az)parama;
    Context localContext = a();
    b.k localk = e.o;
    boolean bool2 = j;
    Iterator localIterator = I.iterator();
    for (;;)
    {
      boolean bool3 = localIterator.hasNext();
      if (!bool3) {
        break;
      }
      Object localObject1 = (bm)localIterator.next();
      if (bool2)
      {
        int i1 = 3;
        int i2 = a;
        if (i1 == i2)
        {
          Object localObject2 = "video";
          Object localObject3 = b;
          Object localObject4 = "creativeType";
          localObject3 = ((Map)localObject3).get(localObject4);
          if (localObject2 == localObject3) {
            try
            {
              localObject2 = new com/inmobi/ads/bt;
              Object localObject5 = i;
              Object localObject6 = j;
              String str = k;
              List localList1 = parama.f();
              List localList2 = parama.g();
              localObject3 = e;
              b.j localj = q;
              localObject4 = localObject2;
              ((bt)localObject2).<init>((String)localObject5, (String)localObject6, str, localList1, localList2, localj);
              localObject3 = new com/inmobi/ads/ak;
              localObject4 = d();
              localObject5 = new org/json/JSONObject;
              localObject6 = f;
              ((JSONObject)localObject5).<init>((String)localObject6);
              localObject6 = e;
              ((ak)localObject3).<init>((AdContainer.RenderingProperties.PlacementType)localObject4, (JSONObject)localObject5, (b)localObject6, (bt)localObject2);
              localObject2 = "VIDEO";
              localObject2 = ((ak)localObject3).c((String)localObject2);
              i2 = 0;
              localObject3 = null;
              localObject2 = ((List)localObject2).get(0);
              localObject2 = (bb)localObject2;
              if (localContext != null)
              {
                localObject3 = new java/util/HashSet;
                ((HashSet)localObject3).<init>();
                localObject2 = u;
                localObject2 = ((List)localObject2).iterator();
                for (;;)
                {
                  boolean bool4 = ((Iterator)localObject2).hasNext();
                  if (!bool4) {
                    break;
                  }
                  localObject4 = ((Iterator)localObject2).next();
                  localObject4 = (NativeTracker)localObject4;
                  localObject5 = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_IAS;
                  localObject6 = b;
                  if (localObject5 == localObject6)
                  {
                    localObject5 = a;
                    localObject4 = c;
                    localObject4 = a((String)localObject5, (Map)localObject4);
                    ((Set)localObject3).add(localObject4);
                  }
                }
                i1 = ((Set)localObject3).size();
                if (i1 != 0)
                {
                  localObject2 = b;
                  localObject4 = "avidAdSession";
                  localObject3 = r.a(localContext, (Set)localObject3);
                  ((Map)localObject2).put(localObject4, localObject3);
                  localObject1 = b;
                  localObject2 = "deferred";
                  localObject3 = Boolean.TRUE;
                  ((Map)localObject1).put(localObject2, localObject3);
                }
              }
            }
            catch (Exception localException)
            {
              localException.getMessage();
              localObject2 = com.inmobi.commons.core.a.a.a();
              localObject3 = new com/inmobi/commons/core/e/a;
              ((com.inmobi.commons.core.e.a)localObject3).<init>(localException);
              ((com.inmobi.commons.core.a.a)localObject2).a((com.inmobi.commons.core.e.a)localObject3);
            }
          }
        }
      }
    }
  }
  
  void c(j.b paramb) {}
  
  public void c(RenderView paramRenderView)
  {
    boolean bool = v;
    if (!bool)
    {
      paramRenderView = a();
      if (paramRenderView != null)
      {
        paramRenderView = G;
        if (paramRenderView != null) {}
      }
    }
  }
  
  final void c(String paramString)
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    localHashMap.put("errorCode", paramString);
    c("ads", "AdPrefetchRejected", localHashMap);
  }
  
  final void c(String paramString1, String paramString2, Map paramMap)
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    Object localObject1 = b();
    localHashMap.put("type", localObject1);
    long l1 = b;
    localObject1 = Long.valueOf(l1);
    localHashMap.put("plId", localObject1);
    localObject1 = j;
    localHashMap.put("impId", localObject1);
    Object localObject2 = "isPreloaded";
    boolean bool1 = m;
    if (bool1) {
      localObject1 = "1";
    } else {
      localObject1 = "0";
    }
    localHashMap.put(localObject2, localObject1);
    localObject2 = "networkType";
    int i1 = com.inmobi.commons.core.utilities.b.b.a();
    switch (i1)
    {
    default: 
      localObject1 = "NIL";
      break;
    case 1: 
      localObject1 = "wifi";
      break;
    case 0: 
      localObject1 = "carrier";
    }
    localHashMap.put(localObject2, localObject1);
    localObject2 = paramMap.get("clientRequestId");
    if (localObject2 == null)
    {
      localObject2 = "clientRequestId";
      localObject1 = k;
      localHashMap.put(localObject2, localObject1);
    }
    if (paramMap != null)
    {
      paramMap = paramMap.entrySet().iterator();
      for (;;)
      {
        boolean bool2 = paramMap.hasNext();
        if (!bool2) {
          break;
        }
        localObject2 = (Map.Entry)paramMap.next();
        localObject1 = ((Map.Entry)localObject2).getKey();
        localObject2 = ((Map.Entry)localObject2).getValue();
        localHashMap.put(localObject1, localObject2);
      }
    }
    try
    {
      com.inmobi.commons.core.e.b.a();
      com.inmobi.commons.core.e.b.a(paramString1, paramString2, localHashMap);
      return;
    }
    catch (Exception localException)
    {
      localException.getMessage();
    }
  }
  
  protected abstract AdContainer.RenderingProperties.PlacementType d();
  
  public void d(RenderView paramRenderView)
  {
    boolean bool = v;
    if (!bool)
    {
      paramRenderView = a();
      if (paramRenderView != null)
      {
        paramRenderView = G;
        if (paramRenderView != null) {}
      }
    }
  }
  
  final void d(String paramString)
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    c("ads", paramString, localHashMap);
  }
  
  protected Map e()
  {
    return null;
  }
  
  public final void e(String paramString)
  {
    ExecutorService localExecutorService = o;
    j.2 local2 = new com/inmobi/ads/j$2;
    local2.<init>(this, paramString);
    localExecutorService.execute(local2);
  }
  
  final j.b f()
  {
    j.b localb = (j.b)B.get();
    if (localb == null) {
      g();
    }
    return localb;
  }
  
  final void g()
  {
    Logger.a(Logger.InternalLogLevel.DEBUG, "InMobi", "Listener was garbage collected. Unable to give callback");
    d("ListenerNotFound");
  }
  
  final boolean h()
  {
    int i1 = a;
    int i2 = 1;
    if (i2 == i1) {
      return false;
    }
    long l1 = A;
    long l2 = -1;
    long l3 = 0L;
    boolean bool = l1 < l2;
    if (!bool)
    {
      l1 = z;
      bool = l1 < l3;
      if (bool)
      {
        l1 = System.currentTimeMillis();
        l2 = z;
        l1 -= l2;
        TimeUnit localTimeUnit = TimeUnit.SECONDS;
        Object localObject = e;
        String str = b();
        localObject = ((b)localObject).a(str);
        l2 = d;
        l2 = localTimeUnit.toMillis(l2);
        bool = l1 < l2;
        if (bool) {
          return i2;
        }
      }
      return false;
    }
    l1 = z;
    bool = l1 < l3;
    if (bool)
    {
      l1 = System.currentTimeMillis();
      l2 = A;
      bool = l1 < l2;
      if (bool) {
        return i2;
      }
    }
    return false;
  }
  
  final AdContainer i()
  {
    int i1 = a;
    String str1 = l;
    int i2 = str1.hashCode();
    int i3 = -1084172778;
    int i4 = 2;
    int i5 = 1;
    String str2;
    boolean bool;
    if (i2 != i3)
    {
      i3 = 3213227;
      if (i2 == i3)
      {
        str2 = "html";
        bool = str1.equals(str2);
        if (bool)
        {
          bool = true;
          break label97;
        }
      }
    }
    else
    {
      str2 = "inmobiJson";
      bool = str1.equals(str2);
      if (bool)
      {
        i6 = 2;
        break label97;
      }
    }
    int i6 = -1;
    label97:
    i2 = 3;
    i3 = 0;
    switch (i6)
    {
    default: 
      return null;
    case 2: 
      if ((i1 != 0) && (i5 != i1) && (i2 != i1) && (i4 != i1)) {
        return n;
      }
      return null;
    }
    if ((i1 != 0) && (i5 != i1) && (i2 != i1)) {
      return j();
    }
    return null;
  }
  
  protected RenderView j()
  {
    Object localObject1 = C;
    if (localObject1 != null)
    {
      localObject1 = s;
      boolean bool = ((AtomicBoolean)localObject1).get();
      if (!bool) {}
    }
    else
    {
      localObject1 = a();
      if (localObject1 != null)
      {
        localObject1 = new com/inmobi/rendering/RenderView;
        Object localObject2 = a();
        AdContainer.RenderingProperties localRenderingProperties = new com/inmobi/ads/AdContainer$RenderingProperties;
        Object localObject3 = d();
        localRenderingProperties.<init>((AdContainer.RenderingProperties.PlacementType)localObject3);
        localObject3 = I;
        String str = j;
        ((RenderView)localObject1).<init>((Context)localObject2, localRenderingProperties, (Set)localObject3, str);
        C = ((RenderView)localObject1);
        localObject1 = C;
        localObject2 = e;
        ((RenderView)localObject1).a(this, (b)localObject2);
      }
    }
    return C;
  }
  
  public InMobiAdRequest.MonetizationContext k()
  {
    return J;
  }
  
  public void l()
  {
    Object localObject1 = "AdLoadRequested";
    d((String)localObject1);
    boolean bool = com.inmobi.commons.core.utilities.d.a();
    if (!bool)
    {
      localObject1 = new com/inmobi/ads/InMobiAdRequestStatus;
      localObject2 = InMobiAdRequestStatus.StatusCode.NETWORK_UNREACHABLE;
      ((InMobiAdRequestStatus)localObject1).<init>((InMobiAdRequestStatus.StatusCode)localObject2);
      a((InMobiAdRequestStatus)localObject1, true);
      return;
    }
    bool = p();
    if (bool) {
      return;
    }
    localObject1 = o;
    Object localObject2 = H;
    ((ExecutorService)localObject1).execute((Runnable)localObject2);
  }
  
  final boolean m()
  {
    int i1 = a;
    int i3 = 1;
    if (i3 == i1)
    {
      l1 = b;
      localObject1 = new com/inmobi/ads/InMobiAdRequestStatus;
      localStatusCode = InMobiAdRequestStatus.StatusCode.REQUEST_PENDING;
      ((InMobiAdRequestStatus)localObject1).<init>(localStatusCode);
      b(l1, (InMobiAdRequestStatus)localObject1);
      localObject1 = Logger.InternalLogLevel.ERROR;
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("An ad prefetch is already in progress. Please wait for the prefetch to complete before requesting for another ad for placement id: ");
      l2 = b;
      ((StringBuilder)localObject2).append(l2);
      localObject2 = ((StringBuilder)localObject2).toString();
      Logger.a((Logger.InternalLogLevel)localObject1, "InMobi", (String)localObject2);
      return i3;
    }
    int i4 = 8;
    if (i4 != i1)
    {
      i4 = 7;
      if (i4 != i1)
      {
        i4 = 2;
        if (i4 == i1)
        {
          localObject1 = "html";
          String str = l;
          boolean bool = ((String)localObject1).equals(str);
          if (bool)
          {
            l1 = b;
            localObject1 = new com/inmobi/ads/InMobiAdRequestStatus;
            localStatusCode = InMobiAdRequestStatus.StatusCode.REQUEST_PENDING;
            ((InMobiAdRequestStatus)localObject1).<init>(localStatusCode);
            b(l1, (InMobiAdRequestStatus)localObject1);
            localObject1 = Logger.InternalLogLevel.ERROR;
            localObject2 = new java/lang/StringBuilder;
            ((StringBuilder)localObject2).<init>("An ad load is already in progress. Please wait for the load to complete before requesting prefetch for another ad for placement id: ");
            l2 = b;
            ((StringBuilder)localObject2).append(l2);
            localObject2 = ((StringBuilder)localObject2).toString();
            Logger.a((Logger.InternalLogLevel)localObject1, "InMobi", (String)localObject2);
            return i3;
          }
          localObject1 = "inmobiJson";
          str = l;
          bool = ((String)localObject1).equals(str);
          if (bool)
          {
            l1 = b;
            a(l1);
            return i3;
          }
        }
        int i2 = 5;
        i4 = a;
        if (i2 != i4)
        {
          i2 = 9;
          if (i2 != i4) {
            return false;
          }
        }
        l1 = b;
        a(l1);
        return i3;
      }
    }
    long l1 = b;
    Object localObject1 = new com/inmobi/ads/InMobiAdRequestStatus;
    InMobiAdRequestStatus.StatusCode localStatusCode = InMobiAdRequestStatus.StatusCode.AD_ACTIVE;
    ((InMobiAdRequestStatus)localObject1).<init>(localStatusCode);
    b(l1, (InMobiAdRequestStatus)localObject1);
    localObject1 = Logger.InternalLogLevel.ERROR;
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("An ad is currently being viewed by the user. Please wait for the user to close the ad before requesting for another ad for placement id: ");
    long l2 = b;
    ((StringBuilder)localObject2).append(l2);
    localObject2 = ((StringBuilder)localObject2).toString();
    Logger.a((Logger.InternalLogLevel)localObject1, "InMobi", (String)localObject2);
    return i3;
  }
  
  void n()
  {
    a(false);
  }
  
  protected int o()
  {
    int i1 = 1;
    try
    {
      a = i1;
      localObject1 = p.a();
      ((p)localObject1).e();
      com.inmobi.commons.core.utilities.uid.c.a();
      com.inmobi.commons.core.utilities.uid.c.c();
      localObject1 = new com/inmobi/commons/core/configs/h;
      ((com.inmobi.commons.core.configs.h)localObject1).<init>();
      localObject2 = com.inmobi.commons.core.configs.b.a();
      int i2 = 0;
      Object localObject3 = null;
      ((com.inmobi.commons.core.configs.b)localObject2).a((com.inmobi.commons.core.configs.a)localObject1, null);
      boolean bool2 = g;
      if (!bool2)
      {
        localObject1 = J();
        long l1 = System.currentTimeMillis();
        E = l1;
        localObject2 = G;
        if (localObject2 == null)
        {
          localObject2 = new com/inmobi/ads/j$a;
          ((j.a)localObject2).<init>(this);
          G = ((j.a)localObject2);
        }
        localObject2 = null;
        try
        {
          Object localObject4 = h;
          boolean bool4 = e.b();
          if (bool4) {
            c.b();
          }
          c = null;
          d = false;
          c = ((i)localObject1);
          localObject1 = "int";
          localObject3 = c;
          localObject3 = h;
          bool2 = ((String)localObject1).equals(localObject3);
          if (bool2)
          {
            ((h)localObject4).b();
            localObject5 = b;
            localObject1 = c;
            long l2 = d;
            localObject1 = c;
            String str1 = f;
            localObject1 = c;
            InMobiAdRequest.MonetizationContext localMonetizationContext = m;
            localObject1 = c;
            localObject1 = g;
            String str2 = g.a((Map)localObject1);
            localObject1 = ((c)localObject5).c(l2, str1, localMonetizationContext, str2);
            i2 = ((List)localObject1).size();
            long l3;
            if (i2 == 0)
            {
              d = false;
              localObject1 = c;
              int i3 = b;
              l3 = SystemClock.elapsedRealtime();
              long l4 = e;
              l3 -= l4;
              l4 = i3 * 1000;
              boolean bool3 = l3 < l4;
              if (bool3)
              {
                bool3 = true;
              }
              else
              {
                bool3 = false;
                localObject1 = null;
              }
              if (!bool3)
              {
                localObject1 = c;
                localObject1 = ((i)localObject1).a();
                localObject3 = "1";
                bool3 = ((String)localObject1).equals(localObject3);
                if (bool3)
                {
                  localObject1 = c;
                  localObject6 = ((h)localObject4).a((i)localObject1, i1);
                }
                else
                {
                  localObject6 = c;
                  localObject6 = ((h)localObject4).a((i)localObject6, false);
                }
              }
              else
              {
                localObject6 = new com/inmobi/ads/a/a;
                localObject1 = "Ignoring request to fetch an ad from the network sooner than the minimum request interval";
                ((com.inmobi.ads.a.a)localObject6).<init>((String)localObject1);
                throw ((Throwable)localObject6);
              }
            }
            else
            {
              d = i1;
              localObject6 = ((List)localObject1).get(0);
              localObject6 = (a)localObject6;
              localObject6 = f;
              localObject3 = "INMOBIJSON";
              localObject5 = ((List)localObject1).get(0);
              localObject5 = (a)localObject5;
              localObject5 = ((a)localObject5).d();
              boolean bool1 = ((String)localObject3).equalsIgnoreCase((String)localObject5);
              if (bool1)
              {
                localObject3 = a;
                localObject5 = c;
                l3 = d;
                Object localObject8 = ((List)localObject1).get(0);
                localObject8 = (a)localObject8;
                ((h.a)localObject3).b(l3, (a)localObject8);
                ((h)localObject4).a((List)localObject1);
              }
              else
              {
                localObject6 = ((h)localObject4).a();
              }
            }
          }
          else
          {
            localObject6 = ((h)localObject4).a();
          }
          localObject1 = new java/util/HashMap;
          ((HashMap)localObject1).<init>();
          localObject3 = "im-accid";
          Object localObject5 = com.inmobi.commons.a.a.e();
          ((Map)localObject1).put(localObject3, localObject5);
          localObject3 = "isPreloaded";
          localObject5 = c;
          localObject5 = ((i)localObject5).a();
          ((Map)localObject1).put(localObject3, localObject5);
          localObject3 = a;
          localObject4 = "ads";
          localObject5 = "AdCacheAdRequested";
          ((h.a)localObject3).a((String)localObject4, (String)localObject5, (Map)localObject1);
          k = ((String)localObject6);
          Object localObject6 = f();
          localObject1 = "VAR";
          localObject3 = "";
          a((j.b)localObject6, (String)localObject1, (String)localObject3);
          i1 = m;
          if (i1 != 0)
          {
            localObject6 = h;
            i1 = d;
            if (i1 == 0)
            {
              localObject6 = "AdPreLoadRequested";
              d((String)localObject6);
            }
          }
        }
        catch (com.inmobi.ads.a.a locala)
        {
          locala.getMessage();
          localObject7 = h;
          i1 = d;
          if (i1 == 0)
          {
            localObject7 = r;
            localObject1 = new com/inmobi/ads/j$11;
            ((j.11)localObject1).<init>(this);
            ((Handler)localObject7).post((Runnable)localObject1);
          }
        }
        return 0;
      }
      Object localObject7 = "LoadAfterMonetizationDisabled";
      d((String)localObject7);
      localObject7 = Logger.InternalLogLevel.ERROR;
      localObject1 = x;
      localObject2 = "SDK will not perform this load operation as monetization has been disabled. Please contact InMobi for further info.";
      Logger.a((Logger.InternalLogLevel)localObject7, (String)localObject1, (String)localObject2);
      return -1;
    }
    catch (Exception localException)
    {
      Logger.a(Logger.InternalLogLevel.ERROR, "InMobi", "Unable to load ad; SDK encountered an unexpected error");
      localException.getMessage();
      Object localObject1 = com.inmobi.commons.core.a.a.a();
      Object localObject2 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject2).<init>(localException);
      ((com.inmobi.commons.core.a.a)localObject1).a((com.inmobi.commons.core.e.a)localObject2);
    }
    return -2;
  }
  
  protected abstract boolean p();
  
  final void q()
  {
    AdContainer localAdContainer = i();
    if (localAdContainer == null) {
      return;
    }
    localAdContainer.a(2, null);
  }
  
  protected void r()
  {
    boolean bool = v;
    if (bool) {
      return;
    }
    v = true;
    bool = false;
    JSONObject localJSONObject = null;
    j = null;
    z = 0L;
    long l1 = -1;
    A = l1;
    I.clear();
    AdContainer localAdContainer = i();
    if (localAdContainer != null) {
      localAdContainer.destroy();
    }
    a = 0;
    l = "unknown";
    L = false;
    t = null;
    s = false;
    u = false;
    w = "";
    localJSONObject = new org/json/JSONObject;
    localJSONObject.<init>();
    g = localJSONObject;
  }
  
  public final void s()
  {
    boolean bool = v;
    if (!bool)
    {
      Object localObject = a();
      if (localObject != null)
      {
        localObject = G;
        if (localObject != null)
        {
          ((j.a)localObject).sendEmptyMessage(11);
          return;
        }
      }
    }
  }
  
  protected void t() {}
  
  public final void u()
  {
    boolean bool = v;
    if (!bool)
    {
      Object localObject = a();
      if (localObject != null)
      {
        localObject = G;
        if (localObject != null)
        {
          ((j.a)localObject).sendEmptyMessage(12);
          return;
        }
      }
    }
  }
  
  protected void v()
  {
    b("RenderFailed");
  }
  
  public final void w()
  {
    boolean bool = v;
    if (!bool)
    {
      Object localObject = a();
      if (localObject != null)
      {
        localObject = G;
        if (localObject != null)
        {
          int i1 = 7;
          int i2 = a;
          if (i1 == i2)
          {
            i1 = 3;
            a = i1;
            localObject = f();
            String str1 = "AVFB";
            String str2 = "";
            a((j.b)localObject, str1, str2);
            localObject = f();
            if (localObject != null)
            {
              localObject = f();
              ((j.b)localObject).b();
            }
          }
          return;
        }
      }
    }
  }
  
  public final void x()
  {
    boolean bool = v;
    if (!bool)
    {
      Object localObject = a();
      if (localObject != null)
      {
        localObject = G;
        if (localObject != null)
        {
          localObject = f();
          if (localObject != null)
          {
            localObject = f();
            ((j.b)localObject).f();
          }
          return;
        }
      }
    }
  }
  
  final void y()
  {
    D.removeMessages(0);
  }
  
  final void z()
  {
    Handler localHandler = r;
    j.12 local12 = new com/inmobi/ads/j$12;
    local12.<init>(this);
    localHandler.post(local12);
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */