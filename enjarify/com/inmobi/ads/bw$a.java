package com.inmobi.ads;

import android.view.View;
import android.view.ViewGroup;
import com.inmobi.rendering.RenderView;

abstract class bw$a
{
  private boolean a;
  
  public abstract View a(View paramView, ViewGroup paramViewGroup, boolean paramBoolean, RenderView paramRenderView);
  
  public void a()
  {
    boolean bool = a;
    if (bool) {
      return;
    }
    a = true;
  }
  
  public boolean b()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.bw.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */