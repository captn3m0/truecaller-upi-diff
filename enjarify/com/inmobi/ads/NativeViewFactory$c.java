package com.inmobi.ads;

import android.content.Context;
import android.view.View;
import java.lang.ref.WeakReference;
import java.util.LinkedList;

abstract class NativeViewFactory$c
{
  private int a;
  LinkedList b;
  private int d;
  
  public NativeViewFactory$c(NativeViewFactory paramNativeViewFactory)
  {
    paramNativeViewFactory = new java/util/LinkedList;
    paramNativeViewFactory.<init>();
    b = paramNativeViewFactory;
    a = 0;
    d = 0;
  }
  
  protected abstract View a(Context paramContext);
  
  public final View a(Context paramContext, ag paramag, b paramb)
  {
    Object localObject = new java/lang/ref/WeakReference;
    ((WeakReference)localObject).<init>(paramContext);
    NativeViewFactory.a((WeakReference)localObject);
    localObject = b;
    boolean bool = ((LinkedList)localObject).isEmpty();
    if (bool)
    {
      int i = a + 1;
      a = i;
      paramContext = a(paramContext);
    }
    else
    {
      int j = d + 1;
      d = j;
      paramContext = (View)b.removeFirst();
      localObject = c;
      NativeViewFactory.b((NativeViewFactory)localObject);
    }
    a(paramContext, paramag, paramb);
    return paramContext;
  }
  
  protected void a(View paramView, ag paramag, b paramb)
  {
    int i = x;
    paramView.setVisibility(i);
    paramView.setOnClickListener(null);
  }
  
  public boolean a(View paramView)
  {
    NativeViewFactory.b(paramView);
    paramView.setOnClickListener(null);
    b.addLast(paramView);
    float f = 1.0F;
    paramView.setScaleX(f);
    paramView.setScaleY(f);
    NativeViewFactory.a(c);
    return true;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Size:");
    int i = b.size();
    localStringBuilder.append(i);
    localStringBuilder.append(" Miss Count:");
    i = a;
    localStringBuilder.append(i);
    localStringBuilder.append(" Hit Count:");
    i = d;
    localStringBuilder.append(i);
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.NativeViewFactory.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */