package com.inmobi.ads;

import com.inmobi.commons.core.utilities.d;
import com.inmobi.rendering.a.c;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;

public class ag
{
  private static final String z = "ag";
  String a;
  String b;
  public ah c;
  String d;
  Object e;
  JSONObject f;
  String g;
  boolean h;
  int i;
  String j;
  int k;
  int l;
  int m;
  int n;
  int o;
  int p;
  String q;
  String r;
  String s;
  ag t;
  List u;
  public Map v;
  Object w;
  int x;
  public ag y;
  
  public ag()
  {
    this("", "root", "CONTAINER");
  }
  
  private ag(String paramString1, String paramString2, String paramString3)
  {
    this(paramString1, paramString2, paramString3, localah);
  }
  
  public ag(String paramString1, String paramString2, String paramString3, ah paramah)
  {
    this(paramString1, paramString2, paramString3, paramah, localLinkedList);
  }
  
  public ag(String paramString1, String paramString2, String paramString3, ah paramah, List paramList)
  {
    a = paramString1;
    d = paramString2;
    b = paramString3;
    c = paramah;
    e = null;
    g = "";
    h = false;
    i = 0;
    j = "";
    l = 0;
    k = 0;
    m = 0;
    n = 2;
    x = 0;
    o = -1;
    q = "";
    r = "";
    paramString1 = new org/json/JSONObject;
    paramString1.<init>();
    f = paramString1;
    paramString1 = new java/util/LinkedList;
    paramString1.<init>();
    u = paramString1;
    u.addAll(paramList);
    paramString1 = new java/util/HashMap;
    paramString1.<init>();
    v = paramString1;
  }
  
  public static ag a(String paramString1, String paramString2, String paramString3)
  {
    ag localag = new com/inmobi/ads/ag;
    localag.<init>();
    localag.a(paramString1);
    if (paramString2 != null) {
      localag.b(paramString2);
    }
    w = paramString3;
    return localag;
  }
  
  static void a(NativeTracker paramNativeTracker, Map paramMap)
  {
    c localc = c.a();
    paramMap = d.a(a, paramMap);
    paramNativeTracker = c;
    localc.a(paramMap, paramNativeTracker);
  }
  
  public final void a(NativeTracker.TrackerEventType paramTrackerEventType, Map paramMap)
  {
    Object localObject = u;
    int i1 = ((List)localObject).size();
    if (i1 == 0) {
      return;
    }
    localObject = u.iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject).hasNext();
      if (!bool) {
        break;
      }
      NativeTracker localNativeTracker = (NativeTracker)((Iterator)localObject).next();
      NativeTracker.TrackerEventType localTrackerEventType = b;
      if (paramTrackerEventType == localTrackerEventType) {
        a(localNativeTracker, paramMap);
      }
    }
  }
  
  final void a(String paramString)
  {
    paramString = paramString.trim();
    r = paramString;
  }
  
  final void a(List paramList)
  {
    u.addAll(paramList);
  }
  
  final void a(List paramList, NativeTracker.TrackerEventType paramTrackerEventType)
  {
    paramList = paramList.iterator();
    for (;;)
    {
      boolean bool = paramList.hasNext();
      if (!bool) {
        break;
      }
      NativeTracker localNativeTracker = (NativeTracker)paramList.next();
      Object localObject = b;
      if (paramTrackerEventType == localObject)
      {
        localObject = u;
        ((List)localObject).add(localNativeTracker);
      }
    }
  }
  
  final void b(String paramString)
  {
    paramString = paramString.trim();
    s = paramString;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.ag
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */