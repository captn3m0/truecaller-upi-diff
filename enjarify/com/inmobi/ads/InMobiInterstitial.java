package com.inmobi.ads;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import com.d.b.w;
import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONObject;

public final class InMobiInterstitial
{
  private static final String DEFAULT_CREATIVE_ID = "";
  private static final String TAG = "InMobiInterstitial";
  private static ConcurrentHashMap prefetchAdUnitMap;
  private InMobiInterstitial.a mClientCallbackHandler;
  private Context mContext;
  private String mCreativeId = "";
  private boolean mDidPubCalledLoad = false;
  private Map mExtras;
  private final j.b mInterstitialAdListener;
  private x mInterstitialAdUnit;
  private boolean mIsHardwareAccelerationDisabled;
  private boolean mIsInitialized = false;
  private String mKeywords;
  private InMobiInterstitial.InterstitialAdListener2 mListener2;
  private long mPlacementId;
  
  static
  {
    ConcurrentHashMap localConcurrentHashMap = new java/util/concurrent/ConcurrentHashMap;
    localConcurrentHashMap.<init>(2, 0.9F, 3);
    prefetchAdUnitMap = localConcurrentHashMap;
  }
  
  private InMobiInterstitial(Context paramContext, long paramLong)
  {
    InMobiInterstitial.2 local2 = new com/inmobi/ads/InMobiInterstitial$2;
    local2.<init>(this);
    mInterstitialAdListener = local2;
    mIsInitialized = true;
    mContext = paramContext;
    mPlacementId = paramLong;
  }
  
  public InMobiInterstitial(Context paramContext, long paramLong, InMobiInterstitial.InterstitialAdListener2 paramInterstitialAdListener2)
  {
    InMobiInterstitial.2 local2 = new com/inmobi/ads/InMobiInterstitial$2;
    local2.<init>(this);
    mInterstitialAdListener = local2;
    boolean bool = com.inmobi.commons.a.a.a();
    if (!bool)
    {
      paramContext = Logger.InternalLogLevel.ERROR;
      localObject = TAG;
      Logger.a(paramContext, (String)localObject, "Please initialize the SDK before creating an Interstitial ad");
      return;
    }
    if (paramInterstitialAdListener2 == null)
    {
      paramContext = Logger.InternalLogLevel.ERROR;
      localObject = TAG;
      Logger.a(paramContext, (String)localObject, "The Interstitial ad cannot be created as no event listener was supplied. Please attach a listener to proceed");
      return;
    }
    if (paramContext == null)
    {
      paramContext = Logger.InternalLogLevel.ERROR;
      localObject = TAG;
      Logger.a(paramContext, (String)localObject, "Unable to create Interstitial ad with null context object.");
      return;
    }
    mIsInitialized = true;
    paramContext = paramContext.getApplicationContext();
    mContext = paramContext;
    mPlacementId = paramLong;
    mListener2 = paramInterstitialAdListener2;
    paramContext = new com/inmobi/ads/InMobiInterstitial$a;
    Object localObject = mListener2;
    paramContext.<init>(this, (InMobiInterstitial.InterstitialAdListener2)localObject);
    mClientCallbackHandler = paramContext;
  }
  
  private void fireTRC(String paramString1, String paramString2)
  {
    x localx = mInterstitialAdUnit;
    if (localx != null)
    {
      j.b localb = mInterstitialAdListener;
      localx.a(localb, paramString1, paramString2);
    }
  }
  
  private static x getPrefetchUnit(Context paramContext, InMobiAdRequest paramInMobiAdRequest, j.d paramd)
  {
    paramContext = paramContext.getApplicationContext();
    long l = paramInMobiAdRequest.getPlacementId();
    Map localMap = paramInMobiAdRequest.getExtras();
    String str = paramInMobiAdRequest.getKeywords();
    Object localObject = bf.a(l, localMap, "int", str);
    paramContext = x.a.a(paramContext, (bf)localObject, null);
    localObject = paramInMobiAdRequest.getExtras();
    d = ((Map)localObject);
    paramInMobiAdRequest = paramInMobiAdRequest.getKeywords();
    c = paramInMobiAdRequest;
    paramInMobiAdRequest = InMobiAdRequest.MonetizationContext.MONETIZATION_CONTEXT_ACTIVITY;
    paramContext.a(paramInMobiAdRequest);
    m = true;
    p = paramd;
    return paramContext;
  }
  
  private void loadAdUnit()
  {
    Object localObject1 = Logger.InternalLogLevel.DEBUG;
    Object localObject2 = TAG;
    Object localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>("Fetching an Interstitial ad for placement id: ");
    long l = mInterstitialAdUnit.b;
    ((StringBuilder)localObject3).append(l);
    localObject3 = ((StringBuilder)localObject3).toString();
    Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, (String)localObject3);
    setCreativeId("");
    localObject1 = mInterstitialAdUnit;
    localObject2 = mInterstitialAdListener;
    ((x)localObject1).a((j.b)localObject2);
    localObject1 = mInterstitialAdUnit;
    localObject2 = mInterstitialAdListener;
    ((x)localObject1).d((j.b)localObject2);
  }
  
  public static void requestAd(Context paramContext, InMobiAdRequest paramInMobiAdRequest, InMobiInterstitial.InterstitialAdRequestListener paramInterstitialAdRequestListener)
  {
    boolean bool1 = com.inmobi.commons.a.a.a();
    if (!bool1)
    {
      paramContext = Logger.InternalLogLevel.ERROR;
      paramInMobiAdRequest = TAG;
      Logger.a(paramContext, paramInMobiAdRequest, "Please initialize the SDK before calling requestAd. Ignoring request");
      return;
    }
    if (paramInterstitialAdRequestListener == null)
    {
      paramContext = Logger.InternalLogLevel.ERROR;
      paramInMobiAdRequest = TAG;
      Logger.a(paramContext, paramInMobiAdRequest, "Please supply a non null InterstitialAdRequestListener. Ignoring request");
      return;
    }
    if (paramInMobiAdRequest == null)
    {
      paramContext = Logger.InternalLogLevel.ERROR;
      paramInMobiAdRequest = TAG;
      Logger.a(paramContext, paramInMobiAdRequest, "Please supply a non null InMobiAdRequest. Ignoring request");
      return;
    }
    if (paramContext == null)
    {
      paramContext = Logger.InternalLogLevel.ERROR;
      paramInMobiAdRequest = TAG;
      Logger.a(paramContext, paramInMobiAdRequest, "Please supply a non null Context. Ignoring request");
      return;
    }
    bool1 = false;
    Object localObject1 = null;
    Object localObject2 = RecyclerView.class;
    try
    {
      ((Class)localObject2).getName();
      localObject2 = w.class;
      ((Class)localObject2).getName();
      localObject2 = new com/inmobi/ads/InMobiInterstitial$1;
      ((InMobiInterstitial.1)localObject2).<init>();
      try
      {
        Object localObject3 = prefetchAdUnitMap;
        localObject3 = ((ConcurrentHashMap)localObject3).entrySet();
        localObject3 = ((Set)localObject3).iterator();
        Object localObject4;
        boolean bool3;
        do
        {
          do
          {
            boolean bool2 = ((Iterator)localObject3).hasNext();
            if (!bool2) {
              break;
            }
            localObject4 = ((Iterator)localObject3).next();
            localObject4 = (Map.Entry)localObject4;
            localObject4 = ((Map.Entry)localObject4).getKey();
            localObject4 = (x)localObject4;
          } while (localObject4 == null);
          long l1 = b;
          long l2 = paramInMobiAdRequest.getPlacementId();
          bool3 = l1 < l2;
        } while (bool3);
        localObject1 = localObject4;
        if (localObject1 != null)
        {
          localObject3 = prefetchAdUnitMap;
          localObject1 = ((ConcurrentHashMap)localObject3).get(localObject1);
          localObject1 = (ArrayList)localObject1;
          localObject3 = new java/lang/ref/WeakReference;
          ((WeakReference)localObject3).<init>(paramInterstitialAdRequestListener);
          ((ArrayList)localObject1).add(localObject3);
          paramContext = getPrefetchUnit(paramContext, paramInMobiAdRequest, (j.d)localObject2);
          paramInMobiAdRequest = prefetchAdUnitMap;
          paramInMobiAdRequest.put(paramContext, localObject1);
          paramContext.n();
          return;
        }
        paramContext = setupAndGetInterstitialAdUnit(paramContext, paramInMobiAdRequest, paramInterstitialAdRequestListener, (j.d)localObject2);
        paramContext.n();
        return;
      }
      catch (Exception localException)
      {
        localException.getMessage();
        return;
      }
      return;
    }
    catch (NoClassDefFoundError localNoClassDefFoundError)
    {
      paramContext = Logger.InternalLogLevel.ERROR;
      paramInMobiAdRequest = TAG;
      Logger.a(paramContext, paramInMobiAdRequest, "Some of the dependency libraries for Interstitial not found. Ignoring request");
      paramContext = new com/inmobi/ads/InMobiAdRequestStatus;
      paramInMobiAdRequest = InMobiAdRequestStatus.StatusCode.MISSING_REQUIRED_DEPENDENCIES;
      paramContext.<init>(paramInMobiAdRequest);
      paramInterstitialAdRequestListener.onAdRequestCompleted(paramContext, null);
    }
  }
  
  private void setCreativeId(String paramString)
  {
    mCreativeId = paramString;
  }
  
  private void setupAdUnit()
  {
    x localx = mInterstitialAdUnit;
    Object localObject = mContext;
    localx.a((Context)localObject);
    localx = mInterstitialAdUnit;
    localObject = mExtras;
    d = ((Map)localObject);
    localObject = mKeywords;
    c = ((String)localObject);
    localObject = InMobiAdRequest.MonetizationContext.MONETIZATION_CONTEXT_ACTIVITY;
    localx.a((InMobiAdRequest.MonetizationContext)localObject);
    boolean bool1 = mIsHardwareAccelerationDisabled;
    if (bool1)
    {
      localx = mInterstitialAdUnit;
      localObject = localx.i();
      if (localObject != null)
      {
        boolean bool2 = true;
        x = bool2;
        ((AdContainer)localObject).a();
      }
    }
    mInterstitialAdUnit.m = false;
  }
  
  static x setupAndGetInterstitialAdUnit(Context paramContext, InMobiAdRequest paramInMobiAdRequest, InMobiInterstitial.InterstitialAdRequestListener paramInterstitialAdRequestListener, j.d paramd)
  {
    paramContext = getPrefetchUnit(paramContext, paramInMobiAdRequest, paramd);
    p = paramd;
    paramInMobiAdRequest = new java/util/ArrayList;
    paramInMobiAdRequest.<init>();
    paramd = new java/lang/ref/WeakReference;
    paramd.<init>(paramInterstitialAdRequestListener);
    paramInMobiAdRequest.add(paramd);
    prefetchAdUnitMap.put(paramContext, paramInMobiAdRequest);
    return paramContext;
  }
  
  public final void disableHardwareAcceleration()
  {
    boolean bool = mIsInitialized;
    if (bool)
    {
      bool = true;
      mIsHardwareAccelerationDisabled = bool;
    }
  }
  
  public final JSONObject getAdMetaInfo()
  {
    boolean bool = mIsInitialized;
    if (bool)
    {
      localObject = mInterstitialAdUnit;
      if (localObject != null) {
        return g;
      }
    }
    Object localObject = new org/json/JSONObject;
    ((JSONObject)localObject).<init>();
    return (JSONObject)localObject;
  }
  
  public final String getCreativeId()
  {
    return mCreativeId;
  }
  
  public final boolean isReady()
  {
    boolean bool = mIsInitialized;
    if (bool)
    {
      x localx = mInterstitialAdUnit;
      if (localx != null) {
        return localx.J();
      }
    }
    return false;
  }
  
  public final void load()
  {
    try
    {
      boolean bool1 = com.inmobi.commons.a.a.a();
      if (!bool1)
      {
        localObject1 = Logger.InternalLogLevel.ERROR;
        localObject2 = TAG;
        localObject3 = "InMobiInterstitial is not initialized. Ignoring InMobiInterstitial.load()";
        Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, (String)localObject3);
        return;
      }
      Object localObject1 = mListener2;
      if (localObject1 == null)
      {
        localObject1 = Logger.InternalLogLevel.ERROR;
        localObject2 = TAG;
        localObject3 = "Listener supplied is null, the InMobiInterstitial cannot be loaded.";
        Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, (String)localObject3);
        return;
      }
      bool1 = mIsInitialized;
      if (bool1)
      {
        localObject1 = mContext;
        if (localObject1 != null)
        {
          localObject1 = y.d();
          long l = mPlacementId;
          Map localMap = mExtras;
          String str1 = "int";
          String str2 = mKeywords;
          localObject2 = bf.a(l, localMap, str1, str2);
          localObject1 = ((y)localObject1).a((bf)localObject2);
          boolean bool2 = true;
          mDidPubCalledLoad = bool2;
          if (localObject1 != null)
          {
            localObject1 = (x)localObject1;
            mInterstitialAdUnit = ((x)localObject1);
          }
          else
          {
            localObject1 = mContext;
            localObject3 = mInterstitialAdListener;
            localObject1 = x.a.a((Context)localObject1, (bf)localObject2, (j.b)localObject3);
            mInterstitialAdUnit = ((x)localObject1);
          }
          localObject1 = "ARR";
          localObject2 = "";
          fireTRC((String)localObject1, (String)localObject2);
          setupAdUnit();
          loadAdUnit();
        }
      }
      return;
    }
    catch (Exception localException)
    {
      Object localObject2 = Logger.InternalLogLevel.ERROR;
      Object localObject3 = TAG;
      Logger.a((Logger.InternalLogLevel)localObject2, (String)localObject3, "Unable to load ad; SDK encountered an unexpected error");
      localException.getMessage();
      localObject2 = com.inmobi.commons.core.a.a.a();
      localObject3 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject3).<init>(localException);
      ((com.inmobi.commons.core.a.a)localObject2).a((com.inmobi.commons.core.e.a)localObject3);
    }
  }
  
  public final void setExtras(Map paramMap)
  {
    boolean bool = mIsInitialized;
    if (bool) {
      mExtras = paramMap;
    }
  }
  
  public final void setInterstitialAdListener(InMobiInterstitial.InterstitialAdListener2 paramInterstitialAdListener2)
  {
    mListener2 = paramInterstitialAdListener2;
    InMobiInterstitial.a locala = new com/inmobi/ads/InMobiInterstitial$a;
    locala.<init>(this, paramInterstitialAdListener2);
    mClientCallbackHandler = locala;
  }
  
  public final void setKeywords(String paramString)
  {
    boolean bool = mIsInitialized;
    if (bool) {
      mKeywords = paramString;
    }
  }
  
  public final void show()
  {
    try
    {
      boolean bool = mDidPubCalledLoad;
      Object localObject1;
      if (!bool)
      {
        localObject1 = Logger.InternalLogLevel.ERROR;
        localObject2 = TAG;
        localObject3 = "load() must be called before trying to show the ad";
        Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, (String)localObject3);
        return;
      }
      bool = mIsInitialized;
      if (bool)
      {
        localObject1 = mInterstitialAdUnit;
        if (localObject1 != null)
        {
          localObject1 = "AVR";
          localObject2 = "";
          fireTRC((String)localObject1, (String)localObject2);
          localObject1 = mInterstitialAdUnit;
          localObject2 = mInterstitialAdListener;
          ((x)localObject1).e((j.b)localObject2);
        }
      }
      return;
    }
    catch (Exception localException)
    {
      Object localObject2 = Logger.InternalLogLevel.ERROR;
      Object localObject3 = TAG;
      Logger.a((Logger.InternalLogLevel)localObject2, (String)localObject3, "Unable to show ad; SDK encountered an unexpected error");
      localException.getMessage();
      localObject2 = com.inmobi.commons.core.a.a.a();
      localObject3 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject3).<init>(localException);
      ((com.inmobi.commons.core.a.a)localObject2).a((com.inmobi.commons.core.e.a)localObject3);
    }
  }
  
  public final void show(int paramInt1, int paramInt2)
  {
    show();
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.InMobiInterstitial
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */