package com.inmobi.ads;

import android.content.Context;
import android.widget.FrameLayout;

public abstract class NativeScrollableContainer
  extends FrameLayout
{
  private final int a;
  
  public NativeScrollableContainer(Context paramContext)
  {
    super(paramContext);
    a = 0;
  }
  
  public NativeScrollableContainer(Context paramContext, int paramInt)
  {
    super(paramContext);
    a = paramInt;
  }
  
  abstract void a(ai paramai, au paramau, int paramInt1, int paramInt2, NativeScrollableContainer.a parama);
  
  public final int getType()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.NativeScrollableContainer
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */