package com.inmobi.ads;

import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;

final class av
{
  public static au a(int paramInt, ak paramak, aq paramaq)
  {
    switch (paramInt)
    {
    default: 
      return null;
    case 1: 
      try
      {
        NativeRecyclerViewAdapter localNativeRecyclerViewAdapter = new com/inmobi/ads/NativeRecyclerViewAdapter;
        localNativeRecyclerViewAdapter.<init>(paramak, paramaq);
        return localNativeRecyclerViewAdapter;
      }
      catch (NoClassDefFoundError localNoClassDefFoundError)
      {
        Logger.a(Logger.InternalLogLevel.ERROR, "InMobi", "Error rendering ad! RecyclerView not found. Please check if the recyclerview support library was included");
        paramak = com.inmobi.commons.core.a.a.a();
        paramaq = new com/inmobi/commons/core/e/a;
        paramaq.<init>(localNoClassDefFoundError);
        paramak.a(paramaq);
        return null;
      }
    }
    al localal = new com/inmobi/ads/al;
    localal.<init>(paramak, paramaq);
    return localal;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.av
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */