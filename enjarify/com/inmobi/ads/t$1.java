package com.inmobi.ads;

import android.os.SystemClock;
import android.view.View;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

final class t$1
  implements ca.c
{
  t$1(t paramt) {}
  
  public final void a(List paramList1, List paramList2)
  {
    paramList1 = paramList1.iterator();
    Object localObject1;
    for (;;)
    {
      boolean bool1 = paramList1.hasNext();
      if (!bool1) {
        break;
      }
      localObject1 = (View)paramList1.next();
      Object localObject2 = (t.b)t.a(a).get(localObject1);
      if (localObject2 == null)
      {
        localObject2 = a;
        t.a((t)localObject2, (View)localObject1);
      }
      else
      {
        Object localObject3 = (t.b)t.b(a).get(localObject1);
        if (localObject3 != null)
        {
          Object localObject4 = a;
          localObject3 = a;
          boolean bool2 = localObject4.equals(localObject3);
          if (bool2) {}
        }
        else
        {
          long l = SystemClock.uptimeMillis();
          d = l;
          localObject3 = t.b(a);
          ((Map)localObject3).put(localObject1, localObject2);
        }
      }
    }
    paramList1 = paramList2.iterator();
    for (;;)
    {
      boolean bool3 = paramList1.hasNext();
      if (!bool3) {
        break;
      }
      paramList2 = (View)paramList1.next();
      localObject1 = t.b(a);
      ((Map)localObject1).remove(paramList2);
    }
    t.c(a);
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.t.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */