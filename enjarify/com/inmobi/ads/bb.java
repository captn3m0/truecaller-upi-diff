package com.inmobi.ads;

import android.text.TextUtils;
import com.inmobi.commons.a.a;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;

public final class bb
  extends ag
{
  boolean A;
  boolean B;
  boolean C;
  boolean D;
  int E;
  int F;
  public Map G;
  private boolean H;
  List z;
  
  public bb(String paramString1, String paramString2, ah paramah, bu parambu, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, List paramList, JSONObject paramJSONObject, boolean paramBoolean6)
  {
    super(paramString1, paramString2, str, paramah);
    e = parambu;
    int i = 2;
    this.i = i;
    A = paramBoolean1;
    B = paramBoolean2;
    C = paramBoolean3;
    D = paramBoolean4;
    paramString1 = new java/util/ArrayList;
    paramString1.<init>();
    z = paramString1;
    H = paramBoolean6;
    if (parambu != null)
    {
      paramString1 = parambu.a();
      r = paramString1;
      paramString1 = parambu.d();
      boolean bool1 = false;
      paramString2 = null;
      boolean bool2;
      Object localObject;
      NativeTracker.TrackerEventType localTrackerEventType;
      if (paramList != null)
      {
        paramah = paramList.iterator();
        for (;;)
        {
          bool2 = paramah.hasNext();
          if (!bool2) {
            break;
          }
          parambu = (NativeTracker)paramah.next();
          localObject = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_IAS;
          localTrackerEventType = b;
          if (localObject == localTrackerEventType)
          {
            paramString2 = c;
            localObject = a;
            paramBoolean1 = TextUtils.isEmpty((CharSequence)localObject);
            if (!paramBoolean1) {
              paramString1.add(parambu);
            }
          }
          else
          {
            paramString1.add(parambu);
          }
        }
      }
      paramah = paramString1.iterator();
      for (;;)
      {
        bool2 = paramah.hasNext();
        if (!bool2) {
          break;
        }
        parambu = (NativeTracker)paramah.next();
        localObject = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_IAS;
        localTrackerEventType = b;
        if (localObject == localTrackerEventType) {
          c = paramString2;
        }
      }
      bool1 = paramString1.isEmpty();
      if (!bool1) {
        a(paramString1);
      }
    }
    if (paramJSONObject != null) {
      f = paramJSONObject;
    }
    paramString1 = v;
    paramah = AdContainer.RenderingProperties.PlacementType.PLACEMENT_TYPE_INLINE;
    paramString1.put("placementType", paramah);
    paramString1 = v;
    paramah = Integer.valueOf(-1 << -1);
    paramString1.put("lastVisibleTimestamp", paramah);
    paramString1 = v;
    paramah = Boolean.FALSE;
    paramString1.put("visible", paramah);
    paramString1 = v;
    parambu = Integer.valueOf(0);
    paramString1.put("seekPosition", parambu);
    paramString1 = v;
    parambu = Boolean.FALSE;
    paramString1.put("didStartPlaying", parambu);
    paramString1 = v;
    parambu = Boolean.FALSE;
    paramString1.put("didPause", parambu);
    paramString1 = v;
    parambu = Boolean.FALSE;
    paramString1.put("didCompleteQ1", parambu);
    paramString1 = v;
    parambu = Boolean.FALSE;
    paramString1.put("didCompleteQ2", parambu);
    paramString1 = v;
    parambu = Boolean.FALSE;
    paramString1.put("didCompleteQ3", parambu);
    paramString1 = v;
    parambu = Boolean.FALSE;
    paramString1.put("didCompleteQ4", parambu);
    paramString1 = v;
    parambu = Boolean.FALSE;
    paramString1.put("didRequestFullScreen", parambu);
    paramString1 = v;
    parambu = Boolean.FALSE;
    paramString1.put("isFullScreen", parambu);
    paramString1 = v;
    parambu = Boolean.FALSE;
    paramString1.put("didImpressionFire", parambu);
    paramString1 = v;
    parambu = new java/util/HashMap;
    parambu.<init>();
    paramString1.put("mapViewabilityParams", parambu);
    paramString1 = v;
    parambu = Boolean.FALSE;
    paramString1.put("didSignalVideoCompleted", parambu);
    paramString1 = v;
    parambu = Boolean.valueOf(paramBoolean5);
    paramString1.put("shouldAutoPlay", parambu);
    paramString1 = v;
    parambu = Integer.valueOf(0);
    paramString1.put("lastMediaVolume", parambu);
    paramString1 = v;
    paramah = Integer.valueOf(0);
    paramString1.put("currentMediaVolume", paramah);
    paramString1 = v;
    paramah = Boolean.FALSE;
    paramString1.put("didQ4Fire", paramah);
  }
  
  public final void a(bb parambb)
  {
    Map localMap1 = v;
    Map localMap2 = v;
    localMap1.putAll(localMap2);
    localMap1 = G;
    localMap2 = G;
    localMap1.putAll(localMap2);
    parambb = u;
    u = parambb;
  }
  
  final boolean a()
  {
    boolean bool = H;
    if (bool)
    {
      bool = A;
      if (bool)
      {
        bool = a.d();
        if (!bool) {
          return true;
        }
      }
      return false;
    }
    return A;
  }
  
  public final bu b()
  {
    Object localObject = e;
    if (localObject == null) {
      return null;
    }
    return (bu)e;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.bb
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */