package com.inmobi.ads;

public final class InMobiAdRequestStatus
{
  private static final String AD_ACTIVE_MESSAGE = "The Ad Request could not be submitted as the user is viewing another Ad.";
  private static final String AD_AVAILABILITY_CHANGED_MESSAGE = "An ad is no longer available. Please call load() to fetch a fresh ad.";
  private static final String AD_EARLY_REFRESH_MESSAGE = "The Ad Request cannot be done so frequently. Please wait for some time before loading another ad.";
  private static final String GDPR_COMPLIANCE_ENFORCED = "Network Request dropped as current request is not GDPR compliant.";
  private static final String INTERNAL_ERROR_MESSAGE = "The InMobi SDK encountered an internal error.";
  private static final String MISSING_REQUIRED_DEPENDENCIES_MESSAGE = "The SDK rejected the ad request as one or more required dependencies could not be found. Please ensure you have included the required dependencies.";
  private static final String NETWORK_UNREACHABLE_MESSAGE = "The Internet is unreachable. Please check your Internet connection.";
  private static final String NO_FILL_MESSAGE = "Ad request successful but no ad served.";
  private static final String REPETITIVE_LOAD_MESSAGE = "The SDK rejected the ad load request. Multiple load() call on the same object is not allowed if the previous ad request was successful.";
  private static final String REQUEST_INVALID_MESSAGE = "An invalid ad request was sent and was rejected by the Ad Network. Please validate the ad request and try again";
  private static final String REQUEST_PENDING_MESSAGE = "The SDK is pending response to a previous ad request. Please wait for the previous ad request to complete before requesting for another ad.";
  private static final String REQUEST_TIMED_OUT_MESSAGE = "The Ad Request timed out waiting for a response from the network. This can be caused due to a bad network connection. Please try again after a few minutes.";
  private static final String SERVER_ERROR_MESSAGE = "The Ad Server encountered an error when processing the ad request. This may be a transient issue. Please try again in a few minutes";
  private String mMessage;
  private InMobiAdRequestStatus.StatusCode mStatusCode;
  
  public InMobiAdRequestStatus(InMobiAdRequestStatus.StatusCode paramStatusCode)
  {
    mStatusCode = paramStatusCode;
    setMessage();
  }
  
  private void setMessage()
  {
    Object localObject = InMobiAdRequestStatus.1.a;
    InMobiAdRequestStatus.StatusCode localStatusCode = mStatusCode;
    int i = localStatusCode.ordinal();
    int j = localObject[i];
    switch (j)
    {
    default: 
      break;
    case 13: 
      localObject = "Network Request dropped as current request is not GDPR compliant.";
      mMessage = ((String)localObject);
      break;
    case 12: 
      mMessage = "The SDK rejected the ad load request. Multiple load() call on the same object is not allowed if the previous ad request was successful.";
      return;
    case 11: 
      mMessage = "The SDK rejected the ad request as one or more required dependencies could not be found. Please ensure you have included the required dependencies.";
      return;
    case 10: 
      mMessage = "An ad is no longer available. Please call load() to fetch a fresh ad.";
      return;
    case 9: 
      mMessage = "The Ad Request cannot be done so frequently. Please wait for some time before loading another ad.";
      return;
    case 8: 
      mMessage = "The Ad Request could not be submitted as the user is viewing another Ad.";
      return;
    case 7: 
      mMessage = "Ad request successful but no ad served.";
      return;
    case 6: 
      mMessage = "The Ad Server encountered an error when processing the ad request. This may be a transient issue. Please try again in a few minutes";
      return;
    case 5: 
      mMessage = "The Ad Request timed out waiting for a response from the network. This can be caused due to a bad network connection. Please try again after a few minutes.";
      return;
    case 4: 
      mMessage = "The SDK is pending response to a previous ad request. Please wait for the previous ad request to complete before requesting for another ad.";
      return;
    case 3: 
      mMessage = "An invalid ad request was sent and was rejected by the Ad Network. Please validate the ad request and try again";
      return;
    case 2: 
      mMessage = "The Internet is unreachable. Please check your Internet connection.";
      return;
    case 1: 
      mMessage = "The InMobi SDK encountered an internal error.";
      return;
    }
  }
  
  public final String getMessage()
  {
    return mMessage;
  }
  
  public final InMobiAdRequestStatus.StatusCode getStatusCode()
  {
    return mStatusCode;
  }
  
  public final InMobiAdRequestStatus setCustomMessage(String paramString)
  {
    if (paramString != null) {
      mMessage = paramString;
    }
    return this;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.InMobiAdRequestStatus
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */