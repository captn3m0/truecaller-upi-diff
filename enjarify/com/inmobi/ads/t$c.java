package com.inmobi.ads;

import android.view.View;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

final class t$c
  implements Runnable
{
  private final ArrayList a;
  private WeakReference b;
  
  t$c(t paramt)
  {
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    a = ((ArrayList)localObject);
    localObject = new java/lang/ref/WeakReference;
    ((WeakReference)localObject).<init>(paramt);
    b = ((WeakReference)localObject);
  }
  
  public final void run()
  {
    t localt = (t)b.get();
    if (localt != null)
    {
      Object localObject1 = t.b(localt).entrySet().iterator();
      boolean bool1;
      Object localObject2;
      for (;;)
      {
        bool1 = ((Iterator)localObject1).hasNext();
        if (!bool1) {
          break;
        }
        localObject2 = (Map.Entry)((Iterator)localObject1).next();
        View localView = (View)((Map.Entry)localObject2).getKey();
        localObject2 = (t.b)((Map.Entry)localObject2).getValue();
        long l = d;
        int i = c;
        boolean bool2 = t.a(l, i);
        if (bool2)
        {
          Object localObject3 = b.get();
          if (localObject3 != null)
          {
            localObject3 = t.d(localt);
            localObject2 = a;
            ((t.a)localObject3).a(localView, localObject2);
            localObject2 = a;
            ((ArrayList)localObject2).add(localView);
          }
        }
      }
      localObject1 = a.iterator();
      for (;;)
      {
        bool1 = ((Iterator)localObject1).hasNext();
        if (!bool1) {
          break;
        }
        localObject2 = (View)((Iterator)localObject1).next();
        t.a(localt, (View)localObject2);
      }
      a.clear();
      localObject1 = t.b(localt);
      boolean bool3 = ((Map)localObject1).isEmpty();
      if (!bool3) {
        t.c(localt);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.t.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */