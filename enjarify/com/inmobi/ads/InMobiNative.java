package com.inmobi.ads;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import com.d.b.w;
import com.inmobi.commons.core.e.b;
import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;
import com.inmobi.commons.core.utilities.b.e;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONObject;

public final class InMobiNative
{
  private static final String TAG = "InMobiNative";
  private static ConcurrentHashMap prefetchAdUnitMap;
  private InMobiNative.a mClientCallbackHandler;
  private WeakReference mContextRef;
  private boolean mDownloaderEnabled = true;
  private Map mExtras;
  private String mKeywords;
  private InMobiNative.LockScreenListener mLockScreenListener;
  private final j.b mNativeAdListener;
  private af mNativeAdUnit;
  private InMobiNative.Downloader mNativeDownloader;
  private InMobiNative.NativeAdListener mNativeListener;
  private long mPlacementId;
  private WeakReference mPrimaryView;
  private boolean mPrimaryViewReturned;
  
  static
  {
    ConcurrentHashMap localConcurrentHashMap = new java/util/concurrent/ConcurrentHashMap;
    localConcurrentHashMap.<init>(5, 0.9F, 3);
    prefetchAdUnitMap = localConcurrentHashMap;
  }
  
  public InMobiNative(Context paramContext, long paramLong, InMobiNative.NativeAdListener paramNativeAdListener)
  {
    InMobiNative.2 local2 = new com/inmobi/ads/InMobiNative$2;
    local2.<init>(this);
    mNativeAdListener = local2;
    boolean bool = com.inmobi.commons.a.a.a();
    if (!bool)
    {
      paramContext = Logger.InternalLogLevel.ERROR;
      localObject = TAG;
      Logger.a(paramContext, (String)localObject, "Please initialize the SDK before creating a Native ad");
      return;
    }
    if (paramContext == null)
    {
      paramContext = Logger.InternalLogLevel.ERROR;
      localObject = TAG;
      Logger.a(paramContext, (String)localObject, "Context is null, Native ad cannot be created.");
      return;
    }
    if (paramNativeAdListener == null)
    {
      paramContext = Logger.InternalLogLevel.ERROR;
      localObject = TAG;
      Logger.a(paramContext, (String)localObject, "Listener supplied is null, the Native ad cannot be created.");
      return;
    }
    mPlacementId = paramLong;
    Object localObject = new java/lang/ref/WeakReference;
    ((WeakReference)localObject).<init>(paramContext);
    mContextRef = ((WeakReference)localObject);
    mNativeListener = paramNativeAdListener;
    paramContext = new com/inmobi/ads/InMobiNative$Downloader;
    paramContext.<init>(this);
    mNativeDownloader = paramContext;
    paramContext = new com/inmobi/ads/InMobiNative$a;
    paramContext.<init>(this);
    mClientCallbackHandler = paramContext;
  }
  
  private InMobiNative(Context paramContext, bf parambf)
  {
    Object localObject = new com/inmobi/ads/InMobiNative$2;
    ((InMobiNative.2)localObject).<init>(this);
    mNativeAdListener = ((j.b)localObject);
    boolean bool = com.inmobi.commons.a.a.a();
    if (!bool)
    {
      paramContext = Logger.InternalLogLevel.ERROR;
      parambf = TAG;
      Logger.a(paramContext, parambf, "Please initialize the SDK before creating an InMobiNative ad");
      return;
    }
    if (paramContext == null)
    {
      paramContext = Logger.InternalLogLevel.ERROR;
      parambf = TAG;
      Logger.a(paramContext, parambf, "Context is null, Native ad cannot be created.");
      return;
    }
    localObject = mNativeAdListener;
    paramContext = af.a.a(paramContext, parambf, (j.b)localObject, 0);
    mNativeAdUnit = paramContext;
    paramContext = new com/inmobi/ads/InMobiNative$a;
    paramContext.<init>(this);
    mClientCallbackHandler = paramContext;
  }
  
  private void fireTRC(String paramString1, String paramString2)
  {
    af localaf = mNativeAdUnit;
    if (localaf != null)
    {
      j.b localb = mNativeAdListener;
      localaf.a(localb, paramString1, paramString2);
    }
  }
  
  private void prepareAdUnit()
  {
    Object localObject1 = mContextRef;
    boolean bool1;
    if (localObject1 == null)
    {
      bool1 = false;
      localObject1 = null;
    }
    else
    {
      localObject1 = (Context)((WeakReference)localObject1).get();
    }
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = mNativeAdUnit;
    if (localObject2 == null)
    {
      long l = mPlacementId;
      localObject2 = mExtras;
      String str1 = "native";
      String str2 = mKeywords;
      localObject2 = bf.a(l, (Map)localObject2, str1, str2);
      boolean bool2 = localObject1 instanceof Activity;
      if (bool2) {
        localObject3 = InMobiAdRequest.MonetizationContext.MONETIZATION_CONTEXT_ACTIVITY;
      } else {
        localObject3 = InMobiAdRequest.MonetizationContext.MONETIZATION_CONTEXT_OTHER;
      }
      f = ((InMobiAdRequest.MonetizationContext)localObject3);
      Object localObject3 = mNativeAdListener;
      localObject1 = af.a.a((Context)localObject1, (bf)localObject2, (j.b)localObject3, 0);
      mNativeAdUnit = ((af)localObject1);
    }
    else
    {
      ((af)localObject2).a((Context)localObject1);
      localObject2 = mNativeAdUnit;
      bool1 = localObject1 instanceof Activity;
      if (bool1) {
        localObject1 = InMobiAdRequest.MonetizationContext.MONETIZATION_CONTEXT_ACTIVITY;
      } else {
        localObject1 = InMobiAdRequest.MonetizationContext.MONETIZATION_CONTEXT_OTHER;
      }
      ((af)localObject2).a((InMobiAdRequest.MonetizationContext)localObject1);
    }
    localObject1 = mNativeAdUnit;
    if (localObject1 != null)
    {
      m = false;
      localObject2 = mKeywords;
      c = ((String)localObject2);
      localObject2 = mExtras;
      d = ((Map)localObject2);
    }
  }
  
  public static void requestAd(Context paramContext, InMobiAdRequest paramInMobiAdRequest, InMobiNative.NativeAdRequestListener paramNativeAdRequestListener)
  {
    boolean bool1 = com.inmobi.commons.a.a.a();
    if (!bool1)
    {
      paramContext = Logger.InternalLogLevel.ERROR;
      paramInMobiAdRequest = TAG;
      Logger.a(paramContext, paramInMobiAdRequest, "Please initialize the SDK before calling requestAd. Ignoring request");
      return;
    }
    if (paramNativeAdRequestListener == null)
    {
      paramContext = Logger.InternalLogLevel.ERROR;
      paramInMobiAdRequest = TAG;
      Logger.a(paramContext, paramInMobiAdRequest, "Please supply a non null NativeAdRequestListener. Ignoring request");
      return;
    }
    if (paramInMobiAdRequest == null)
    {
      paramContext = Logger.InternalLogLevel.ERROR;
      paramInMobiAdRequest = TAG;
      Logger.a(paramContext, paramInMobiAdRequest, "Please supply a non null InMobiAdRequest. Ignoring request");
      return;
    }
    Object localObject1 = paramInMobiAdRequest.getMonetizationContext();
    if (localObject1 == null)
    {
      paramContext = Logger.InternalLogLevel.ERROR;
      paramInMobiAdRequest = TAG;
      Logger.a(paramContext, paramInMobiAdRequest, "Please supply a MonetizationContext type. Ignoring request");
      return;
    }
    if (paramContext == null)
    {
      paramContext = Logger.InternalLogLevel.ERROR;
      paramInMobiAdRequest = TAG;
      Logger.a(paramContext, paramInMobiAdRequest, "Please supply a non null Context. Ignoring request");
      return;
    }
    bool1 = false;
    localObject1 = null;
    Object localObject2 = RecyclerView.class;
    try
    {
      ((Class)localObject2).getName();
      localObject2 = w.class;
      ((Class)localObject2).getName();
      long l = paramInMobiAdRequest.getPlacementId();
      Map localMap = paramInMobiAdRequest.getExtras();
      String str1 = "native";
      String str2 = paramInMobiAdRequest.getKeywords();
      localObject2 = bf.a(l, localMap, str1, str2);
      Object localObject3 = paramInMobiAdRequest.getMonetizationContext();
      f = ((InMobiAdRequest.MonetizationContext)localObject3);
      localObject3 = new com/inmobi/ads/InMobiNative$1;
      ((InMobiNative.1)localObject3).<init>((bf)localObject2);
      try
      {
        paramContext = paramContext.getApplicationContext();
        int i = 2;
        paramContext = af.a.a(paramContext, (bf)localObject2, null, i);
        if (paramContext == null)
        {
          paramContext = Logger.InternalLogLevel.ERROR;
          paramInMobiAdRequest = TAG;
          paramNativeAdRequestListener = "SDK encountered an internal error while pre-fetching ad.";
          Logger.a(paramContext, paramInMobiAdRequest, paramNativeAdRequestListener);
          return;
        }
        localObject1 = paramInMobiAdRequest.getExtras();
        d = ((Map)localObject1);
        paramInMobiAdRequest = paramInMobiAdRequest.getKeywords();
        c = paramInMobiAdRequest;
        p = ((j.d)localObject3);
        boolean bool2 = true;
        m = bool2;
        paramInMobiAdRequest = prefetchAdUnitMap;
        localObject1 = new java/lang/ref/WeakReference;
        ((WeakReference)localObject1).<init>(paramNativeAdRequestListener);
        paramInMobiAdRequest.put(paramContext, localObject1);
        paramContext.n();
        return;
      }
      catch (Exception localException)
      {
        localException.getMessage();
        return;
      }
      return;
    }
    catch (NoClassDefFoundError localNoClassDefFoundError)
    {
      paramContext = Logger.InternalLogLevel.ERROR;
      paramInMobiAdRequest = TAG;
      Logger.a(paramContext, paramInMobiAdRequest, "Some of the dependency libraries for InMobiNative not found. Ignoring request");
      paramContext = new com/inmobi/ads/InMobiAdRequestStatus;
      paramInMobiAdRequest = InMobiAdRequestStatus.StatusCode.MISSING_REQUIRED_DEPENDENCIES;
      paramContext.<init>(paramInMobiAdRequest);
      paramNativeAdRequestListener.onAdRequestCompleted(paramContext, null);
    }
  }
  
  public final void destroy()
  {
    try
    {
      boolean bool = com.inmobi.commons.a.a.a();
      if (!bool)
      {
        localObject1 = Logger.InternalLogLevel.ERROR;
        localObject2 = TAG;
        localObject3 = "InMobiNative is not initialized. Ignoring InMobiNative.destroy()";
        Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, (String)localObject3);
      }
      Object localObject1 = mClientCallbackHandler;
      localObject2 = null;
      if (localObject1 != null)
      {
        localObject1 = mClientCallbackHandler;
        ((InMobiNative.a)localObject1).removeMessages(0);
      }
      localObject1 = mPrimaryView;
      localObject3 = null;
      if (localObject1 == null)
      {
        bool = false;
        localObject1 = null;
      }
      else
      {
        localObject1 = mPrimaryView;
        localObject1 = ((WeakReference)localObject1).get();
        localObject1 = (View)localObject1;
      }
      if (localObject1 != null)
      {
        localObject1 = (ViewGroup)localObject1;
        localObject1 = (ViewGroup)localObject1;
        ((ViewGroup)localObject1).removeAllViews();
      }
      localObject1 = mNativeAdUnit;
      if (localObject1 != null)
      {
        localObject1 = mNativeAdUnit;
        ((af)localObject1).J();
      }
      mNativeAdUnit = null;
      mNativeListener = null;
      mNativeDownloader = null;
      mPrimaryViewReturned = false;
      return;
    }
    catch (Exception localException)
    {
      Object localObject2 = Logger.InternalLogLevel.ERROR;
      Object localObject3 = TAG;
      Logger.a((Logger.InternalLogLevel)localObject2, (String)localObject3, "Failed to destroy ad; SDK encountered an unexpected error");
      localObject2 = com.inmobi.commons.core.a.a.a();
      localObject3 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject3).<init>(localException);
      ((com.inmobi.commons.core.a.a)localObject2).a((com.inmobi.commons.core.e.a)localObject3);
    }
  }
  
  public final String getAdCtaText()
  {
    boolean bool = com.inmobi.commons.a.a.a();
    Object localObject1;
    Object localObject2;
    if (!bool)
    {
      localObject1 = Logger.InternalLogLevel.ERROR;
      localObject2 = TAG;
      Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "InMobiNative is not initialized.Ignoring InMobiNative.getAdCtaText()");
      return null;
    }
    try
    {
      localObject1 = mNativeAdUnit;
      if (localObject1 != null)
      {
        localObject1 = mNativeAdUnit;
        localObject1 = ((af)localObject1).i();
        if (localObject1 != null)
        {
          localObject1 = ((AdContainer)localObject1).getDataModel();
          localObject1 = (ak)localObject1;
          if (localObject1 != null)
          {
            localObject1 = i;
            localObject1 = b;
            return d;
          }
        }
        return null;
      }
    }
    catch (Exception localException)
    {
      localObject2 = Logger.InternalLogLevel.ERROR;
      Object localObject3 = TAG;
      String str = "Could not get the ctaText; SDK encountered unexpected error";
      Logger.a((Logger.InternalLogLevel)localObject2, (String)localObject3, str);
      localObject2 = com.inmobi.commons.core.a.a.a();
      localObject3 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject3).<init>(localException);
      ((com.inmobi.commons.core.a.a)localObject2).a((com.inmobi.commons.core.e.a)localObject3);
    }
    return null;
  }
  
  public final String getAdDescription()
  {
    boolean bool = com.inmobi.commons.a.a.a();
    Object localObject1;
    Object localObject2;
    if (!bool)
    {
      localObject1 = Logger.InternalLogLevel.ERROR;
      localObject2 = TAG;
      Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "InMobiNative is not initialized.Ignoring InMobiNative.getAdDescription()");
      return null;
    }
    try
    {
      localObject1 = mNativeAdUnit;
      if (localObject1 != null)
      {
        localObject1 = mNativeAdUnit;
        localObject1 = ((af)localObject1).i();
        if (localObject1 != null)
        {
          localObject1 = ((AdContainer)localObject1).getDataModel();
          localObject1 = (ak)localObject1;
          if (localObject1 != null)
          {
            localObject1 = i;
            localObject1 = b;
            return b;
          }
        }
        return null;
      }
    }
    catch (Exception localException)
    {
      localObject2 = Logger.InternalLogLevel.ERROR;
      Object localObject3 = TAG;
      String str = "Could not get the description; SDK encountered unexpected error";
      Logger.a((Logger.InternalLogLevel)localObject2, (String)localObject3, str);
      localObject2 = com.inmobi.commons.core.a.a.a();
      localObject3 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject3).<init>(localException);
      ((com.inmobi.commons.core.a.a)localObject2).a((com.inmobi.commons.core.e.a)localObject3);
    }
    return null;
  }
  
  public final String getAdIconUrl()
  {
    boolean bool = com.inmobi.commons.a.a.a();
    Object localObject1;
    Object localObject2;
    if (!bool)
    {
      localObject1 = Logger.InternalLogLevel.ERROR;
      localObject2 = TAG;
      Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "InMobiNative is not initialized.Ignoring InMobiNative.getAdIconUrl()");
      return null;
    }
    try
    {
      localObject1 = mNativeAdUnit;
      if (localObject1 != null)
      {
        localObject1 = mNativeAdUnit;
        localObject1 = ((af)localObject1).i();
        if (localObject1 != null)
        {
          localObject1 = ((AdContainer)localObject1).getDataModel();
          localObject1 = (ak)localObject1;
          if (localObject1 != null)
          {
            localObject1 = i;
            localObject1 = b;
            return c;
          }
        }
        return null;
      }
    }
    catch (Exception localException)
    {
      localObject2 = Logger.InternalLogLevel.ERROR;
      Object localObject3 = TAG;
      String str = "Could not get the iconUrl; SDK encountered unexpected error";
      Logger.a((Logger.InternalLogLevel)localObject2, (String)localObject3, str);
      localObject2 = com.inmobi.commons.core.a.a.a();
      localObject3 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject3).<init>(localException);
      ((com.inmobi.commons.core.a.a)localObject2).a((com.inmobi.commons.core.e.a)localObject3);
    }
    return null;
  }
  
  public final String getAdLandingPageUrl()
  {
    boolean bool = com.inmobi.commons.a.a.a();
    Object localObject1;
    Object localObject2;
    if (!bool)
    {
      localObject1 = Logger.InternalLogLevel.ERROR;
      localObject2 = TAG;
      Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "InMobiNative is not initialized.Ignoring InMobiNative.getAdLandingPageUrl()");
      return null;
    }
    try
    {
      localObject1 = mNativeAdUnit;
      if (localObject1 != null)
      {
        localObject1 = mNativeAdUnit;
        localObject1 = ((af)localObject1).i();
        if (localObject1 != null)
        {
          localObject1 = ((AdContainer)localObject1).getDataModel();
          localObject1 = (ak)localObject1;
          if (localObject1 != null)
          {
            localObject1 = i;
            localObject1 = b;
            return f;
          }
        }
        return null;
      }
    }
    catch (Exception localException)
    {
      localObject2 = Logger.InternalLogLevel.ERROR;
      Object localObject3 = TAG;
      String str = "Could not get the adLandingPageUrl; SDK encountered unexpected error";
      Logger.a((Logger.InternalLogLevel)localObject2, (String)localObject3, str);
      localObject2 = com.inmobi.commons.core.a.a.a();
      localObject3 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject3).<init>(localException);
      ((com.inmobi.commons.core.a.a)localObject2).a((com.inmobi.commons.core.e.a)localObject3);
    }
    return null;
  }
  
  public final JSONObject getAdMetaInfo()
  {
    boolean bool = com.inmobi.commons.a.a.a();
    if (bool)
    {
      localObject = mNativeAdUnit;
      if (localObject != null) {
        return g;
      }
    }
    Object localObject = new org/json/JSONObject;
    ((JSONObject)localObject).<init>();
    return (JSONObject)localObject;
  }
  
  public final float getAdRating()
  {
    boolean bool = com.inmobi.commons.a.a.a();
    Object localObject1;
    Object localObject2;
    if (!bool)
    {
      localObject1 = Logger.InternalLogLevel.ERROR;
      localObject2 = TAG;
      Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "InMobiNative is not initialized.Ignoring InMobiNative.getAdRating()");
      return 0.0F;
    }
    try
    {
      localObject1 = mNativeAdUnit;
      if (localObject1 != null)
      {
        localObject1 = mNativeAdUnit;
        localObject1 = ((af)localObject1).i();
        if (localObject1 != null)
        {
          localObject1 = ((AdContainer)localObject1).getDataModel();
          localObject1 = (ak)localObject1;
          if (localObject1 != null)
          {
            localObject1 = i;
            localObject1 = b;
            return e;
          }
        }
        return 0.0F;
      }
    }
    catch (Exception localException)
    {
      localObject2 = com.inmobi.commons.core.a.a.a();
      Object localObject3 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject3).<init>(localException);
      ((com.inmobi.commons.core.a.a)localObject2).a((com.inmobi.commons.core.e.a)localObject3);
      localObject2 = Logger.InternalLogLevel.ERROR;
      localObject3 = "InMobi";
      String str = "Could not get rating; SDK encountered an unexpected error";
      Logger.a((Logger.InternalLogLevel)localObject2, (String)localObject3, str);
      localException.getMessage();
    }
    return 0.0F;
  }
  
  public final String getAdTitle()
  {
    boolean bool = com.inmobi.commons.a.a.a();
    Object localObject1;
    Object localObject2;
    if (!bool)
    {
      localObject1 = Logger.InternalLogLevel.ERROR;
      localObject2 = TAG;
      Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "InMobiNative is not initialized.Ignoring InMobiNative.getAdTitle()");
      return null;
    }
    try
    {
      localObject1 = mNativeAdUnit;
      if (localObject1 != null)
      {
        localObject1 = mNativeAdUnit;
        localObject1 = ((af)localObject1).i();
        if (localObject1 != null)
        {
          localObject1 = ((AdContainer)localObject1).getDataModel();
          localObject1 = (ak)localObject1;
          if (localObject1 != null)
          {
            localObject1 = i;
            localObject1 = b;
            return a;
          }
        }
        return null;
      }
    }
    catch (Exception localException)
    {
      localObject2 = Logger.InternalLogLevel.ERROR;
      Object localObject3 = TAG;
      String str = "Could not get the ad title; SDK encountered unexpected error";
      Logger.a((Logger.InternalLogLevel)localObject2, (String)localObject3, str);
      localObject2 = com.inmobi.commons.core.a.a.a();
      localObject3 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject3).<init>(localException);
      ((com.inmobi.commons.core.a.a)localObject2).a((com.inmobi.commons.core.e.a)localObject3);
    }
    return null;
  }
  
  public final String getCreativeId()
  {
    boolean bool = com.inmobi.commons.a.a.a();
    if (bool)
    {
      af localaf = mNativeAdUnit;
      if (localaf != null) {
        return w;
      }
    }
    return "";
  }
  
  public final JSONObject getCustomAdContent()
  {
    boolean bool = com.inmobi.commons.a.a.a();
    Object localObject1;
    Object localObject2;
    if (!bool)
    {
      localObject1 = Logger.InternalLogLevel.ERROR;
      localObject2 = TAG;
      Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "InMobiNative is not initialized.Ignoring InMobiNative.setExtras()");
      return null;
    }
    try
    {
      localObject1 = mNativeAdUnit;
      if (localObject1 != null)
      {
        localObject1 = mNativeAdUnit;
        localObject1 = ((af)localObject1).i();
        if (localObject1 != null)
        {
          localObject1 = ((AdContainer)localObject1).getDataModel();
          localObject1 = (ak)localObject1;
          if (localObject1 != null)
          {
            localObject1 = i;
            return a;
          }
        }
        return null;
      }
    }
    catch (Exception localException)
    {
      localObject2 = Logger.InternalLogLevel.ERROR;
      Object localObject3 = TAG;
      String str = "Could not get the ad customJson ; SDK encountered unexpected error";
      Logger.a((Logger.InternalLogLevel)localObject2, (String)localObject3, str);
      localObject2 = com.inmobi.commons.core.a.a.a();
      localObject3 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject3).<init>(localException);
      ((com.inmobi.commons.core.a.a)localObject2).a((com.inmobi.commons.core.e.a)localObject3);
    }
    return null;
  }
  
  public final InMobiNative.Downloader getDownloader()
  {
    try
    {
      boolean bool = com.inmobi.commons.a.a.a();
      if (!bool)
      {
        Logger.InternalLogLevel localInternalLogLevel = Logger.InternalLogLevel.ERROR;
        localObject1 = TAG;
        localObject2 = "InMobiNative is not initialized. Ignoring InMobiNative.getDownloader()";
        Logger.a(localInternalLogLevel, (String)localObject1, (String)localObject2);
        return null;
      }
      return mNativeDownloader;
    }
    catch (Exception localException)
    {
      Object localObject1 = Logger.InternalLogLevel.ERROR;
      Object localObject2 = TAG;
      Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "Failed to get Downloader; SDK encountered an unexpected error");
      localObject1 = com.inmobi.commons.core.a.a.a();
      localObject2 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject2).<init>(localException);
      ((com.inmobi.commons.core.a.a)localObject1).a((com.inmobi.commons.core.e.a)localObject2);
    }
    return null;
  }
  
  public final View getPrimaryViewOfWidth(Context paramContext, View paramView, ViewGroup paramViewGroup, int paramInt)
  {
    try
    {
      boolean bool1 = com.inmobi.commons.a.a.a();
      if (!bool1)
      {
        paramContext = Logger.InternalLogLevel.ERROR;
        paramView = TAG;
        paramViewGroup = "InMobiSdk is not initialized. Ignoring InMobiNative.getPrimaryView()";
        Logger.a(paramContext, paramView, paramViewGroup);
        return null;
      }
      if (paramContext == null)
      {
        paramContext = Logger.InternalLogLevel.ERROR;
        paramView = TAG;
        paramViewGroup = "View can not be rendered using null context";
        Logger.a(paramContext, paramView, paramViewGroup);
        return null;
      }
      Object localObject1 = mNativeAdUnit;
      if (localObject1 == null)
      {
        paramContext = Logger.InternalLogLevel.ERROR;
        paramView = TAG;
        paramViewGroup = "InMobiNative is not initialized. Ignoring InMobiNative.getPrimaryView()";
        Logger.a(paramContext, paramView, paramViewGroup);
        return null;
      }
      localObject1 = new java/lang/ref/WeakReference;
      ((WeakReference)localObject1).<init>(paramContext);
      mContextRef = ((WeakReference)localObject1);
      localObject1 = mNativeAdUnit;
      ((af)localObject1).a(paramContext);
      paramContext = new java/lang/ref/WeakReference;
      localObject1 = mNativeAdUnit;
      boolean bool2 = mDownloaderEnabled;
      Object localObject2 = Looper.myLooper();
      Looper localLooper = Looper.getMainLooper();
      boolean bool3 = true;
      Object localObject3;
      if (localObject2 == localLooper)
      {
        boolean bool4 = e.e();
        if (!bool4)
        {
          ((af)localObject1).J();
          paramView = null;
        }
        else
        {
          bool4 = ((af)localObject1).K();
          if (!bool4)
          {
            int i = a;
            int j = 7;
            if (i != j)
            {
              paramView = Logger.InternalLogLevel.ERROR;
              paramViewGroup = af.x;
              localObject3 = "Ad Load is not complete. Please wait for the Ad to be in a ready state before calling getPrimaryView().";
              Logger.a(paramView, paramViewGroup, (String)localObject3);
              paramView = y;
              if (paramView != null)
              {
                paramView = y;
                paramView = paramView.get();
                paramView = (View)paramView;
                if (paramView != null)
                {
                  paramViewGroup = new android/view/View;
                  localObject3 = com.inmobi.commons.a.a.b();
                  paramViewGroup.<init>((Context)localObject3);
                  paramView = paramView.getLayoutParams();
                  paramViewGroup.setLayoutParams(paramView);
                  paramView = paramViewGroup;
                  break label466;
                }
                paramView = null;
                break label466;
              }
              paramView = null;
              break label466;
            }
          }
          localObject2 = n;
          if (localObject2 != null)
          {
            boolean bool5 = z;
            r = bool5;
            p = paramInt;
            q = bool2;
            localObject3 = ((ad)localObject2).getViewableAd();
            paramView = ((bw)localObject3).a(paramView, paramViewGroup, bool3);
            paramViewGroup = new java/lang/ref/WeakReference;
            paramViewGroup.<init>(paramView);
            y = paramViewGroup;
            int k = q;
            if (k == 0)
            {
              bool6 = s;
              if (!bool6)
              {
                paramViewGroup = r;
                af.2 local2 = new com/inmobi/ads/af$2;
                local2.<init>((af)localObject1, (bw)localObject3);
                paramViewGroup.post(local2);
                break label466;
              }
            }
            boolean bool6 = false;
            paramViewGroup = null;
            paramViewGroup = new View[0];
            ((bw)localObject3).a(paramViewGroup);
          }
          else
          {
            paramView = null;
          }
        }
      }
      else
      {
        paramView = Logger.InternalLogLevel.ERROR;
        paramViewGroup = InMobiNative.class;
        paramViewGroup = paramViewGroup.getSimpleName();
        localObject3 = "Please ensure that you call getPrimaryView() on the UI thread";
        Logger.a(paramView, paramViewGroup, (String)localObject3);
        paramView = null;
      }
      label466:
      paramContext.<init>(paramView);
      mPrimaryView = paramContext;
      paramContext = mPrimaryView;
      paramContext = paramContext.get();
      paramContext = (View)paramContext;
      paramView = "AVR";
      paramViewGroup = "";
      fireTRC(paramView, paramViewGroup);
      if (paramContext == null)
      {
        paramContext = mNativeAdUnit;
        boolean bool7 = paramContext.K();
        if (bool7)
        {
          paramContext = "AVFB";
          paramView = "";
          fireTRC(paramContext, paramView);
        }
        else
        {
          paramContext = "AVRR";
          paramView = "";
          fireTRC(paramContext, paramView);
        }
        b.a();
        paramContext = "ads";
        paramView = "PrimaryViewInflationFailed";
        paramViewGroup = new java/util/HashMap;
        paramViewGroup.<init>();
        b.a(paramContext, paramView, paramViewGroup);
        return null;
      }
      paramView = "AVD";
      paramViewGroup = "";
      fireTRC(paramView, paramViewGroup);
      mPrimaryViewReturned = bool3;
      return paramContext;
    }
    catch (Exception paramContext)
    {
      paramView = com.inmobi.commons.core.a.a.a();
      paramViewGroup = new com/inmobi/commons/core/e/a;
      paramViewGroup.<init>(paramContext);
      paramView.a(paramViewGroup);
      Logger.a(Logger.InternalLogLevel.ERROR, "InMobi", "Could not pause ad; SDK encountered an unexpected error");
      paramContext.getMessage();
    }
    return null;
  }
  
  public final View getPrimaryViewOfWidth(View paramView, ViewGroup paramViewGroup, int paramInt)
  {
    Object localObject = mContextRef;
    if (localObject != null)
    {
      localObject = ((WeakReference)localObject).get();
      if (localObject != null)
      {
        localObject = (Context)mContextRef.get();
        return getPrimaryViewOfWidth((Context)localObject, paramView, paramViewGroup, paramInt);
      }
    }
    paramView = Logger.InternalLogLevel.ERROR;
    paramViewGroup = TAG;
    Logger.a(paramView, paramViewGroup, "InMobiNative is not initialized or provided context is null.");
    return null;
  }
  
  public final boolean isAppDownload()
  {
    boolean bool = com.inmobi.commons.a.a.a();
    Object localObject1;
    Object localObject2;
    if (!bool)
    {
      localObject1 = Logger.InternalLogLevel.ERROR;
      localObject2 = TAG;
      Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "InMobiNative is not initialized.Ignoring InMobiNative.isAppDownload()");
      return false;
    }
    try
    {
      localObject1 = mNativeAdUnit;
      if (localObject1 != null)
      {
        localObject1 = mNativeAdUnit;
        localObject1 = ((af)localObject1).i();
        if (localObject1 != null)
        {
          localObject1 = ((AdContainer)localObject1).getDataModel();
          localObject1 = (ak)localObject1;
          if (localObject1 != null)
          {
            localObject1 = i;
            localObject1 = b;
            return g;
          }
        }
        return false;
      }
    }
    catch (Exception localException)
    {
      localObject2 = Logger.InternalLogLevel.ERROR;
      Object localObject3 = TAG;
      String str = "Could not get isAppDownload; SDK encountered unexpected error";
      Logger.a((Logger.InternalLogLevel)localObject2, (String)localObject3, str);
      localObject2 = com.inmobi.commons.core.a.a.a();
      localObject3 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject3).<init>(localException);
      ((com.inmobi.commons.core.a.a)localObject2).a((com.inmobi.commons.core.e.a)localObject3);
    }
    return false;
  }
  
  public final boolean isReady()
  {
    boolean bool = com.inmobi.commons.a.a.a();
    if (!bool)
    {
      localObject = Logger.InternalLogLevel.ERROR;
      String str = TAG;
      Logger.a((Logger.InternalLogLevel)localObject, str, "InMobiNative is not initialized.Ignoring InMobiNative.isReady()");
      return false;
    }
    Object localObject = mNativeAdUnit;
    if (localObject == null) {
      return false;
    }
    return ((af)localObject).K();
  }
  
  public final void load()
  {
    try
    {
      boolean bool = com.inmobi.commons.a.a.a();
      if (!bool)
      {
        localObject1 = Logger.InternalLogLevel.ERROR;
        localObject2 = TAG;
        localObject3 = "InMobiNative is not initialized. Ignoring InMobiNative.load()";
        Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, (String)localObject3);
        return;
      }
      Object localObject1 = mClientCallbackHandler;
      localObject2 = null;
      a = false;
      bool = mPrimaryViewReturned;
      if (bool)
      {
        localObject1 = "ARR";
        localObject2 = "";
        fireTRC((String)localObject1, (String)localObject2);
        localObject1 = mNativeAdListener;
        localObject2 = new com/inmobi/ads/InMobiAdRequestStatus;
        localObject3 = InMobiAdRequestStatus.StatusCode.REPETITIVE_LOAD;
        ((InMobiAdRequestStatus)localObject2).<init>((InMobiAdRequestStatus.StatusCode)localObject3);
        ((j.b)localObject1).a((InMobiAdRequestStatus)localObject2);
        localObject1 = Logger.InternalLogLevel.ERROR;
        localObject2 = TAG;
        localObject3 = "You can call load() on an instance of InMobiNative only once if the ad request has been successful. Ignoring InMobiNative.load()";
        Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, (String)localObject3);
        return;
      }
      prepareAdUnit();
      localObject1 = mNativeAdUnit;
      if (localObject1 != null)
      {
        localObject1 = "ARR";
        localObject2 = "";
        fireTRC((String)localObject1, (String)localObject2);
        long l = mPlacementId;
        localObject3 = mExtras;
        String str1 = "native";
        String str2 = mKeywords;
        localObject1 = bf.a(l, (Map)localObject3, str1, str2);
        localObject2 = mNativeAdUnit;
        localObject2 = ((af)localObject2).k();
        f = ((InMobiAdRequest.MonetizationContext)localObject2);
        localObject2 = mNativeAdUnit;
        ((af)localObject2).l();
        localObject2 = as.d();
        ((as)localObject2).c((bf)localObject1);
      }
      return;
    }
    catch (Exception localException)
    {
      Object localObject2 = com.inmobi.commons.core.a.a.a();
      Object localObject3 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject3).<init>(localException);
      ((com.inmobi.commons.core.a.a)localObject2).a((com.inmobi.commons.core.e.a)localObject3);
      Logger.a(Logger.InternalLogLevel.ERROR, "InMobi", "Could not load ad; SDK encountered an unexpected error");
      localException.getMessage();
    }
  }
  
  public final void load(Context paramContext)
  {
    boolean bool = com.inmobi.commons.a.a.a();
    if (!bool)
    {
      paramContext = Logger.InternalLogLevel.ERROR;
      localObject = TAG;
      Logger.a(paramContext, (String)localObject, "InMobiNative is not initialized. Ignoring InMobiNative.load()");
      return;
    }
    if (paramContext == null)
    {
      paramContext = Logger.InternalLogLevel.ERROR;
      localObject = TAG;
      Logger.a(paramContext, (String)localObject, "Context is null, InMobiNative cannot be loaded.");
      return;
    }
    Object localObject = mNativeAdListener;
    if (localObject == null)
    {
      paramContext = Logger.InternalLogLevel.ERROR;
      localObject = TAG;
      Logger.a(paramContext, (String)localObject, "Listener supplied is null, the InMobiNative cannot be loaded.");
      return;
    }
    localObject = new java/lang/ref/WeakReference;
    ((WeakReference)localObject).<init>(paramContext);
    mContextRef = ((WeakReference)localObject);
    load();
  }
  
  public final void pause()
  {
    try
    {
      Object localObject1 = mNativeAdUnit;
      if (localObject1 != null)
      {
        localObject1 = mNativeAdUnit;
        int i = a;
        int j = 5;
        if (i == j)
        {
          localObject2 = ((af)localObject1).a();
          boolean bool = localObject2 instanceof Activity;
          if (!bool)
          {
            localObject1 = ((af)localObject1).i();
            if (localObject1 != null)
            {
              localObject1 = (ad)localObject1;
              ((ad)localObject1).q();
            }
          }
        }
      }
      return;
    }
    catch (Exception localException)
    {
      Object localObject2 = Logger.InternalLogLevel.ERROR;
      String str = TAG;
      Logger.a((Logger.InternalLogLevel)localObject2, str, "Could not pause ad; SDK encountered an unexpected error");
      localException.getMessage();
    }
  }
  
  public final void reportAdClickAndOpenLandingPage()
  {
    boolean bool1 = com.inmobi.commons.a.a.a();
    Object localObject1;
    Object localObject2;
    if (!bool1)
    {
      localObject1 = Logger.InternalLogLevel.ERROR;
      localObject2 = TAG;
      Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "InMobiNative is not initialized.Ignoring InMobiNative.reportAdClickAndOpenLandingPage()");
      return;
    }
    try
    {
      localObject1 = mNativeAdUnit;
      if (localObject1 != null)
      {
        localObject1 = mNativeAdUnit;
        localObject1 = ((af)localObject1).i();
        if (localObject1 != null)
        {
          localObject1 = (ad)localObject1;
          localObject2 = ((ad)localObject1).h();
          if (localObject2 != null)
          {
            boolean bool2 = false;
            localObject3 = null;
            Object localObject4 = i;
            localObject4 = c;
            ((ad)localObject1).a(null, (ag)localObject4);
            localObject2 = i;
            localObject2 = c;
            bool2 = true;
            ((ad)localObject1).a((ag)localObject2, bool2);
          }
        }
      }
      return;
    }
    catch (Exception localException)
    {
      localObject2 = Logger.InternalLogLevel.ERROR;
      Object localObject3 = TAG;
      Logger.a((Logger.InternalLogLevel)localObject2, (String)localObject3, "reportAdClickAndOpenLandingPage failed; SDK encountered unexpected error");
      localObject2 = com.inmobi.commons.core.a.a.a();
      localObject3 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject3).<init>(localException);
      ((com.inmobi.commons.core.a.a)localObject2).a((com.inmobi.commons.core.e.a)localObject3);
    }
  }
  
  public final void resume()
  {
    try
    {
      Object localObject1 = mNativeAdUnit;
      if (localObject1 != null)
      {
        localObject1 = mNativeAdUnit;
        int i = a;
        int j = 5;
        if (i == j)
        {
          localObject2 = ((af)localObject1).a();
          boolean bool = localObject2 instanceof Activity;
          if (!bool)
          {
            localObject1 = ((af)localObject1).i();
            if (localObject1 != null)
            {
              localObject1 = (ad)localObject1;
              ((ad)localObject1).p();
            }
          }
        }
      }
      return;
    }
    catch (Exception localException)
    {
      Object localObject2 = Logger.InternalLogLevel.ERROR;
      String str = TAG;
      Logger.a((Logger.InternalLogLevel)localObject2, str, "Could not resume ad; SDK encountered an unexpected error");
      localException.getMessage();
    }
  }
  
  public final void setDownloaderEnabled(boolean paramBoolean)
  {
    mDownloaderEnabled = paramBoolean;
  }
  
  public final void setExtras(Map paramMap)
  {
    boolean bool = com.inmobi.commons.a.a.a();
    Object localObject;
    if (!bool)
    {
      paramMap = Logger.InternalLogLevel.ERROR;
      localObject = TAG;
      Logger.a(paramMap, (String)localObject, "InMobiNative is not initialized.Ignoring InMobiNative.setExtras()");
      return;
    }
    try
    {
      localObject = mNativeAdUnit;
      if (localObject != null)
      {
        localObject = mNativeAdUnit;
        d = paramMap;
      }
      mExtras = paramMap;
      return;
    }
    catch (Exception paramMap)
    {
      localObject = com.inmobi.commons.core.a.a.a();
      com.inmobi.commons.core.e.a locala = new com/inmobi/commons/core/e/a;
      locala.<init>(paramMap);
      ((com.inmobi.commons.core.a.a)localObject).a(locala);
      Logger.a(Logger.InternalLogLevel.ERROR, "InMobi", "Could not set extras; SDK encountered an unexpected error");
      paramMap.getMessage();
    }
  }
  
  public final void setKeywords(String paramString)
  {
    boolean bool = com.inmobi.commons.a.a.a();
    Object localObject1;
    if (!bool)
    {
      paramString = Logger.InternalLogLevel.ERROR;
      localObject1 = TAG;
      Logger.a(paramString, (String)localObject1, "InMobiNative is not initialized.Ignoring InMobiNative.setKeywords()");
      return;
    }
    try
    {
      localObject1 = mNativeAdUnit;
      if (localObject1 != null)
      {
        localObject1 = mNativeAdUnit;
        c = paramString;
      }
      mKeywords = paramString;
      return;
    }
    catch (Exception paramString)
    {
      localObject1 = Logger.InternalLogLevel.ERROR;
      Object localObject2 = TAG;
      Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "Could not set keywords on Native ad; SDK encountered unexpected error");
      localObject1 = com.inmobi.commons.core.a.a.a();
      localObject2 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject2).<init>(paramString);
      ((com.inmobi.commons.core.a.a)localObject1).a((com.inmobi.commons.core.e.a)localObject2);
      paramString.getMessage();
    }
  }
  
  public final void setNativeAdListener(InMobiNative.NativeAdListener paramNativeAdListener)
  {
    mNativeListener = paramNativeAdListener;
  }
  
  public final void showOnLockScreen(InMobiNative.LockScreenListener paramLockScreenListener)
  {
    boolean bool1 = com.inmobi.commons.a.a.a();
    if (!bool1)
    {
      paramLockScreenListener = Logger.InternalLogLevel.ERROR;
      localObject1 = TAG;
      Logger.a(paramLockScreenListener, (String)localObject1, "Please initialize the SDK before calling showOnLockScreen.");
      return;
    }
    if (paramLockScreenListener == null)
    {
      paramLockScreenListener = Logger.InternalLogLevel.ERROR;
      localObject1 = TAG;
      Logger.a(paramLockScreenListener, (String)localObject1, "Please provided non null LockScreenListener. Ignoring showOnLockScreen");
      return;
    }
    Object localObject1 = mContextRef;
    if (localObject1 != null)
    {
      localObject1 = ((WeakReference)localObject1).get();
      if (localObject1 != null) {
        try
        {
          localObject1 = mNativeAdUnit;
          if (localObject1 == null)
          {
            long l = mPlacementId;
            Object localObject2 = mExtras;
            String str1 = "native";
            String str2 = mKeywords;
            localObject1 = bf.a(l, (Map)localObject2, str1, str2);
            Object localObject3 = mContextRef;
            localObject3 = ((WeakReference)localObject3).get();
            localObject3 = (Context)localObject3;
            localObject2 = mNativeAdListener;
            str1 = null;
            localObject1 = af.a.a((Context)localObject3, (bf)localObject1, (j.b)localObject2, 0);
            mNativeAdUnit = ((af)localObject1);
          }
          localObject1 = mNativeAdUnit;
          boolean bool2 = true;
          z = bool2;
          mLockScreenListener = paramLockScreenListener;
          return;
        }
        catch (Exception localException)
        {
          paramLockScreenListener = Logger.InternalLogLevel.ERROR;
          localObject1 = TAG;
          Logger.a(paramLockScreenListener, (String)localObject1, "SDK encountered unexpected error in showOnLockScreen");
          return;
        }
      }
    }
    paramLockScreenListener = Logger.InternalLogLevel.ERROR;
    localObject1 = TAG;
    Logger.a(paramLockScreenListener, (String)localObject1, "InMobiNative is not initialized. Provided context is null. Ignoring showOnLockScreen");
  }
  
  public final void takeAction()
  {
    boolean bool = com.inmobi.commons.a.a.a();
    Object localObject1;
    Object localObject2;
    if (!bool)
    {
      localObject1 = Logger.InternalLogLevel.ERROR;
      localObject2 = TAG;
      Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "Please initialize the SDK before calling takeAction.");
      return;
    }
    try
    {
      localObject1 = mNativeAdUnit;
      if (localObject1 != null)
      {
        localObject1 = mNativeAdUnit;
        localObject2 = n;
        if (localObject2 != null)
        {
          localObject1 = n;
          ((ad)localObject1).r();
        }
        return;
      }
      localObject1 = Logger.InternalLogLevel.ERROR;
      localObject2 = TAG;
      String str = "InMobiNative is not initialized. Ignoring takeAction";
      Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, str);
      return;
    }
    catch (Exception localException)
    {
      localObject1 = Logger.InternalLogLevel.ERROR;
      localObject2 = TAG;
      Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject2, "SDK encountered unexpected error in takeAction");
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.InMobiNative
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */