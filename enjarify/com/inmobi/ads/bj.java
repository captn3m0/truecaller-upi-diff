package com.inmobi.ads;

import android.os.Handler;
import java.lang.ref.WeakReference;

final class bj
  extends Handler
{
  private WeakReference a;
  
  bj(j paramj)
  {
    WeakReference localWeakReference = new java/lang/ref/WeakReference;
    localWeakReference.<init>(paramj);
    a = localWeakReference;
  }
  
  /* Error */
  public final void handleMessage(android.os.Message paramMessage)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 17	com/inmobi/ads/bj:a	Ljava/lang/ref/WeakReference;
    //   4: invokevirtual 21	java/lang/ref/WeakReference:get	()Ljava/lang/Object;
    //   7: checkcast 23	com/inmobi/ads/j
    //   10: astore_2
    //   11: aload_2
    //   12: ifnonnull +4 -> 16
    //   15: return
    //   16: aload_1
    //   17: getfield 29	android/os/Message:what	I
    //   20: istore_3
    //   21: iload_3
    //   22: ifeq +4 -> 26
    //   25: return
    //   26: aload_2
    //   27: instanceof 31
    //   30: istore_3
    //   31: iload_3
    //   32: ifeq +38 -> 70
    //   35: aload_2
    //   36: astore_1
    //   37: aload_2
    //   38: checkcast 31	com/inmobi/ads/x
    //   41: astore_1
    //   42: aload_1
    //   43: getfield 35	com/inmobi/ads/j:t	Lcom/inmobi/rendering/RenderView;
    //   46: astore 4
    //   48: aload 4
    //   50: ifnull +20 -> 70
    //   53: aload_1
    //   54: getfield 35	com/inmobi/ads/j:t	Lcom/inmobi/rendering/RenderView;
    //   57: astore_1
    //   58: aload_1
    //   59: invokevirtual 40	com/inmobi/rendering/RenderView:stopLoading	()V
    //   62: aload_2
    //   63: checkcast 23	com/inmobi/ads/j
    //   66: invokevirtual 43	com/inmobi/ads/j:z	()V
    //   69: return
    //   70: aload_2
    //   71: checkcast 23	com/inmobi/ads/j
    //   74: invokevirtual 47	com/inmobi/ads/j:i	()Lcom/inmobi/ads/AdContainer;
    //   77: astore_1
    //   78: aload_1
    //   79: checkcast 37	com/inmobi/rendering/RenderView
    //   82: astore_1
    //   83: aload_1
    //   84: ifnonnull +11 -> 95
    //   87: aload_2
    //   88: checkcast 23	com/inmobi/ads/j
    //   91: invokevirtual 43	com/inmobi/ads/j:z	()V
    //   94: return
    //   95: aload_1
    //   96: invokevirtual 40	com/inmobi/rendering/RenderView:stopLoading	()V
    //   99: aload_2
    //   100: checkcast 23	com/inmobi/ads/j
    //   103: invokevirtual 43	com/inmobi/ads/j:z	()V
    //   106: return
    //   107: astore_1
    //   108: goto +35 -> 143
    //   111: astore_1
    //   112: invokestatic 52	com/inmobi/commons/core/a/a:a	()Lcom/inmobi/commons/core/a/a;
    //   115: astore 4
    //   117: new 54	com/inmobi/commons/core/e/a
    //   120: astore 5
    //   122: aload 5
    //   124: aload_1
    //   125: invokespecial 57	com/inmobi/commons/core/e/a:<init>	(Ljava/lang/Throwable;)V
    //   128: aload 4
    //   130: aload 5
    //   132: invokevirtual 60	com/inmobi/commons/core/a/a:a	(Lcom/inmobi/commons/core/e/a;)V
    //   135: aload_2
    //   136: checkcast 23	com/inmobi/ads/j
    //   139: invokevirtual 43	com/inmobi/ads/j:z	()V
    //   142: return
    //   143: aload_2
    //   144: checkcast 23	com/inmobi/ads/j
    //   147: invokevirtual 43	com/inmobi/ads/j:z	()V
    //   150: aload_1
    //   151: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	152	0	this	bj
    //   0	152	1	paramMessage	android.os.Message
    //   10	134	2	localj	j
    //   20	2	3	i	int
    //   30	2	3	bool	boolean
    //   46	83	4	localObject	Object
    //   120	11	5	locala	com.inmobi.commons.core.e.a
    // Exception table:
    //   from	to	target	type
    //   37	41	107	finally
    //   42	46	107	finally
    //   53	57	107	finally
    //   58	62	107	finally
    //   70	77	107	finally
    //   78	82	107	finally
    //   95	99	107	finally
    //   112	115	107	finally
    //   117	120	107	finally
    //   124	128	107	finally
    //   130	135	107	finally
    //   37	41	111	java/lang/Exception
    //   42	46	111	java/lang/Exception
    //   53	57	111	java/lang/Exception
    //   58	62	111	java/lang/Exception
    //   70	77	111	java/lang/Exception
    //   78	82	111	java/lang/Exception
    //   95	99	111	java/lang/Exception
  }
}

/* Location:
 * Qualified Name:     com.inmobi.ads.bj
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */