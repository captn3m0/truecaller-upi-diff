package com.inmobi.rendering;

import com.inmobi.ads.bn;
import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;
import com.inmobi.rendering.mraid.f;
import java.util.HashMap;
import java.util.Map;

final class c$9
  implements Runnable
{
  c$9(c paramc, String paramString) {}
  
  public final void run()
  {
    try
    {
      Object localObject1 = b;
      localObject1 = c.a((c)localObject1);
      localObject2 = a;
      localObject3 = "Default";
      Object localObject4 = d;
      boolean bool = ((String)localObject3).equals(localObject4);
      if (!bool)
      {
        localObject3 = "Resized";
        localObject4 = d;
        bool = ((String)localObject3).equals(localObject4);
        if (!bool) {}
      }
      else
      {
        localObject3 = ((RenderView)localObject1).getResizeProperties();
        if (localObject3 != null)
        {
          bool = true;
          t = bool;
          localObject4 = g;
          ((f)localObject4).a();
          ((RenderView)localObject1).requestLayout();
          ((RenderView)localObject1).invalidate();
          n = bool;
          ((RenderView)localObject1).setFocusable(bool);
          ((RenderView)localObject1).setFocusableInTouchMode(bool);
          ((RenderView)localObject1).requestFocus();
          localObject3 = "Resized";
          ((RenderView)localObject1).setAndUpdateViewState((String)localObject3);
          localObject3 = c;
          ((RenderView.a)localObject3).c((RenderView)localObject1);
          bool = false;
          localObject3 = null;
          t = false;
          localObject3 = new java/util/HashMap;
          ((HashMap)localObject3).<init>();
          localObject4 = "command";
          String str = "resize";
          ((Map)localObject3).put(localObject4, str);
          localObject4 = "scheme";
          localObject2 = bn.a((String)localObject2);
          ((Map)localObject3).put(localObject4, localObject2);
          localObject1 = c;
          localObject2 = "ads";
          localObject4 = "CreativeInvokedAction";
          ((RenderView.a)localObject1).b((String)localObject2, (String)localObject4, (Map)localObject3);
        }
      }
      return;
    }
    catch (Exception localException)
    {
      Object localObject2 = c.a(b);
      Object localObject3 = a;
      ((RenderView)localObject2).b((String)localObject3, "Unexpected error", "resize");
      localObject2 = Logger.InternalLogLevel.ERROR;
      localObject3 = c.a();
      Logger.a((Logger.InternalLogLevel)localObject2, (String)localObject3, "Could not resize ad; SDK encountered an unexpected error");
      c.a();
      localException.getMessage();
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.rendering.c.9
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */