package com.inmobi.rendering;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Build.VERSION;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.inmobi.ads.AdContainer.RenderingProperties;
import com.inmobi.ads.AdContainer.RenderingProperties.PlacementType;
import com.inmobi.ads.bn;
import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;
import com.inmobi.rendering.mraid.MediaRenderView;
import com.inmobi.rendering.mraid.MediaRenderView.CustomMediaController;
import com.inmobi.rendering.mraid.MediaRenderView.a;
import com.inmobi.rendering.mraid.MraidMediaProcessor;
import com.inmobi.rendering.mraid.MraidMediaProcessor.1;
import com.inmobi.rendering.mraid.MraidMediaProcessor.2;
import com.inmobi.rendering.mraid.MraidMediaProcessor.3;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

final class c$11
  implements Runnable
{
  c$11(c paramc, String paramString1, String paramString2) {}
  
  public final void run()
  {
    try
    {
      Object localObject1 = c;
      localObject1 = c.a((c)localObject1);
      localObject2 = a;
      localObject3 = b;
      localObject3 = ((String)localObject3).trim();
      Object localObject4 = AdContainer.RenderingProperties.PlacementType.PLACEMENT_TYPE_FULLSCREEN;
      Object localObject5 = e;
      localObject5 = a;
      boolean bool;
      if (localObject4 != localObject5)
      {
        localObject4 = "Expanded";
        localObject5 = ((RenderView)localObject1).getViewState();
        bool = ((String)localObject4).equals(localObject5);
        if (!bool) {}
      }
      else
      {
        localObject4 = b;
        if (localObject4 == null) {
          break label622;
        }
        localObject4 = b;
        localObject4 = ((WeakReference)localObject4).get();
        if (localObject4 == null) {
          break label622;
        }
        bool = true;
        ((RenderView)localObject1).setAdActiveFlag(bool);
        localObject4 = h;
        localObject5 = b;
        localObject5 = ((WeakReference)localObject5).get();
        localObject5 = (Activity)localObject5;
        Object localObject6 = new com/inmobi/rendering/mraid/MediaRenderView;
        ((MediaRenderView)localObject6).<init>((Activity)localObject5);
        b = ((MediaRenderView)localObject6);
        localObject6 = b;
        localObject3 = MediaRenderView.a((String)localObject3);
        h = ((String)localObject3);
        localObject3 = "anonymous";
        g = ((String)localObject3);
        localObject3 = b;
        if (localObject3 == null)
        {
          localObject3 = Bitmap.Config.ARGB_8888;
          j = 24;
          localObject3 = Bitmap.createBitmap(j, j, (Bitmap.Config)localObject3);
          b = ((Bitmap)localObject3);
          localObject3 = h;
          localObject3 = MediaRenderView.b((String)localObject3);
          b = ((Bitmap)localObject3);
        }
        int k = 16908290;
        localObject3 = ((Activity)localObject5).findViewById(k);
        localObject3 = (ViewGroup)localObject3;
        localObject6 = new android/widget/RelativeLayout$LayoutParams;
        int j = -1;
        ((RelativeLayout.LayoutParams)localObject6).<init>(j, j);
        int m = 13;
        ((RelativeLayout.LayoutParams)localObject6).addRule(m);
        MediaRenderView localMediaRenderView = b;
        localMediaRenderView.setLayoutParams((ViewGroup.LayoutParams)localObject6);
        localObject6 = new android/widget/RelativeLayout;
        ((RelativeLayout)localObject6).<init>((Context)localObject5);
        localObject5 = new com/inmobi/rendering/mraid/MraidMediaProcessor$1;
        ((MraidMediaProcessor.1)localObject5).<init>((MraidMediaProcessor)localObject4);
        ((RelativeLayout)localObject6).setOnTouchListener((View.OnTouchListener)localObject5);
        int n = -16777216;
        ((RelativeLayout)localObject6).setBackgroundColor(n);
        localObject5 = b;
        ((RelativeLayout)localObject6).addView((View)localObject5);
        localObject5 = new android/view/ViewGroup$LayoutParams;
        ((ViewGroup.LayoutParams)localObject5).<init>(j, j);
        ((ViewGroup)localObject3).addView((View)localObject6, (ViewGroup.LayoutParams)localObject5);
        localObject3 = b;
        c = ((ViewGroup)localObject6);
        localObject3 = b;
        ((MediaRenderView)localObject3).requestFocus();
        localObject3 = b;
        localObject5 = new com/inmobi/rendering/mraid/MraidMediaProcessor$2;
        ((MraidMediaProcessor.2)localObject5).<init>((MraidMediaProcessor)localObject4);
        ((MediaRenderView)localObject3).setOnKeyListener((View.OnKeyListener)localObject5);
        localObject3 = b;
        localObject5 = new com/inmobi/rendering/mraid/MraidMediaProcessor$3;
        ((MraidMediaProcessor.3)localObject5).<init>((MraidMediaProcessor)localObject4);
        d = ((MediaRenderView.a)localObject5);
        localObject3 = b;
        localObject4 = h;
        ((MediaRenderView)localObject3).setVideoPath((String)localObject4);
        ((MediaRenderView)localObject3).setOnCompletionListener((MediaPlayer.OnCompletionListener)localObject3);
        ((MediaRenderView)localObject3).setOnPreparedListener((MediaPlayer.OnPreparedListener)localObject3);
        ((MediaRenderView)localObject3).setOnErrorListener((MediaPlayer.OnErrorListener)localObject3);
        localObject4 = a;
        if (localObject4 == null)
        {
          int i = Build.VERSION.SDK_INT;
          n = 19;
          if (i >= n)
          {
            localObject4 = new com/inmobi/rendering/mraid/MediaRenderView$CustomMediaController;
            localObject5 = ((MediaRenderView)localObject3).getContext();
            ((MediaRenderView.CustomMediaController)localObject4).<init>((Context)localObject5);
            a = ((MediaRenderView.CustomMediaController)localObject4);
            localObject4 = a;
            ((MediaRenderView.CustomMediaController)localObject4).setAnchorView((View)localObject3);
            localObject4 = a;
            ((MediaRenderView)localObject3).setMediaController((MediaController)localObject4);
          }
        }
        localObject3 = new java/util/HashMap;
        ((HashMap)localObject3).<init>();
        localObject4 = "command";
        localObject5 = "playVideo";
        ((Map)localObject3).put(localObject4, localObject5);
        localObject4 = "scheme";
        localObject2 = bn.a((String)localObject2);
        ((Map)localObject3).put(localObject4, localObject2);
        localObject1 = c;
        localObject2 = "ads";
        localObject4 = "CreativeInvokedAction";
        ((RenderView.a)localObject1).b((String)localObject2, (String)localObject4, (Map)localObject3);
      }
      return;
      label622:
      localObject3 = "Media playback is  not allowed before it is visible! Ignoring request ...";
      localObject4 = "playVideo";
      ((RenderView)localObject1).b((String)localObject2, (String)localObject3, (String)localObject4);
      return;
    }
    catch (Exception localException)
    {
      Object localObject2 = c.a(c);
      Object localObject3 = a;
      ((RenderView)localObject2).b((String)localObject3, "Unexpected error", "playVideo");
      Logger.a(Logger.InternalLogLevel.ERROR, "InMobi", "Error playing video; SDK encountered an unexpected error");
      c.a();
      localException.getMessage();
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.rendering.c.11
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */