package com.inmobi.rendering;

import android.view.View;
import android.view.View.OnClickListener;
import com.inmobi.ads.AdContainer;
import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;

final class InMobiAdActivity$6
  implements View.OnClickListener
{
  InMobiAdActivity$6(InMobiAdActivity paramInMobiAdActivity) {}
  
  public final void onClick(View paramView)
  {
    paramView = a;
    InMobiAdActivity.c(paramView);
    try
    {
      paramView = a;
      paramView = InMobiAdActivity.a(paramView);
      paramView.b();
      return;
    }
    catch (Exception paramView)
    {
      InMobiAdActivity.a();
      paramView.getMessage();
      Logger.a(Logger.InternalLogLevel.DEBUG, "InMobi", "SDK encountered unexpected error in processing close request");
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.rendering.InMobiAdActivity.6
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */