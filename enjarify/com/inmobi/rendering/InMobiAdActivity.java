package com.inmobi.rendering;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Point;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.SparseArray;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.inmobi.ads.AdContainer;
import com.inmobi.ads.AdContainer.RenderingProperties;
import com.inmobi.ads.AdContainer.RenderingProperties.PlacementType;
import com.inmobi.ads.AdContainer.a;
import com.inmobi.ads.NativeVideoView;
import com.inmobi.ads.NativeVideoWrapper;
import com.inmobi.ads.ad;
import com.inmobi.ads.ag;
import com.inmobi.ads.ah;
import com.inmobi.ads.ak;
import com.inmobi.ads.b.k;
import com.inmobi.ads.ba;
import com.inmobi.ads.bb;
import com.inmobi.ads.bw;
import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;
import com.inmobi.commons.core.utilities.b.c;
import com.inmobi.commons.core.utilities.b.d;
import com.inmobi.rendering.mraid.f;
import com.inmobi.rendering.mraid.g;
import java.util.HashMap;
import java.util.Map;

public class InMobiAdActivity
  extends Activity
{
  public static Map b;
  public static Map c;
  public static Integer d;
  public static Map e;
  public static Integer f = Integer.valueOf(0);
  private static final String g = "InMobiAdActivity";
  private static SparseArray h;
  private static RenderView i;
  private static RenderView.a j;
  public boolean a = false;
  private AdContainer k;
  private RenderView l;
  private CustomView m;
  private CustomView n;
  private NativeVideoView o;
  private int p;
  private int q;
  private boolean r = false;
  private boolean s = false;
  
  static
  {
    Object localObject = new android/util/SparseArray;
    ((SparseArray)localObject).<init>();
    h = (SparseArray)localObject;
    localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    b = (Map)localObject;
    localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    c = (Map)localObject;
    d = Integer.valueOf(0);
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    e = localHashMap;
  }
  
  public static int a(Intent paramIntent, InMobiAdActivity.a parama)
  {
    d = Integer.valueOf(d.intValue() + 1);
    Object localObject = b;
    Integer localInteger = d;
    ((Map)localObject).put(localInteger, parama);
    parama = c;
    localObject = d;
    parama.put(localObject, paramIntent);
    return d.intValue();
  }
  
  public static int a(AdContainer paramAdContainer)
  {
    int i1 = paramAdContainer.hashCode();
    h.put(i1, paramAdContainer);
    return i1;
  }
  
  public static void a(RenderView.a parama)
  {
    j = parama;
  }
  
  public static void a(RenderView paramRenderView)
  {
    i = paramRenderView;
  }
  
  public static void a(Object paramObject)
  {
    SparseArray localSparseArray = h;
    int i1 = paramObject.hashCode();
    localSparseArray.remove(i1);
  }
  
  public static void a(String[] paramArrayOfString, InMobiAdActivity.b paramb)
  {
    try
    {
      boolean bool = com.inmobi.commons.a.a.a();
      if (!bool) {
        return;
      }
      Object localObject1 = f;
      int i1 = ((Integer)localObject1).intValue() + 1;
      localObject1 = Integer.valueOf(i1);
      f = (Integer)localObject1;
      localObject1 = e;
      Object localObject2 = f;
      ((Map)localObject1).put(localObject2, paramb);
      paramb = f;
      int i2 = paramb.intValue();
      localObject1 = new android/content/Intent;
      localObject2 = com.inmobi.commons.a.a.b();
      Class localClass = InMobiAdActivity.class;
      ((Intent)localObject1).<init>((Context)localObject2, localClass);
      localObject2 = "com.inmobi.rendering.InMobiAdActivity.EXTRA_AD_ACTIVITY_TYPE";
      int i3 = 104;
      ((Intent)localObject1).putExtra((String)localObject2, i3);
      localObject2 = "id";
      ((Intent)localObject1).putExtra((String)localObject2, i2);
      paramb = "permissions";
      ((Intent)localObject1).putExtra(paramb, paramArrayOfString);
      paramArrayOfString = com.inmobi.commons.a.a.b();
      com.inmobi.commons.a.a.a(paramArrayOfString, (Intent)localObject1);
      return;
    }
    catch (Exception localException)
    {
      localException.getMessage();
    }
  }
  
  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    Object localObject = b;
    paramIntent = Integer.valueOf(paramInt1);
    localObject = (InMobiAdActivity.a)((Map)localObject).remove(paramIntent);
    if (localObject != null)
    {
      paramIntent = c;
      Integer localInteger = Integer.valueOf(paramInt1);
      paramIntent.remove(localInteger);
      ((InMobiAdActivity.a)localObject).a();
      paramInt1 = 1;
      a = paramInt1;
      finish();
    }
  }
  
  public void onBackPressed()
  {
    int i1 = p;
    boolean bool2 = true;
    int i3 = 102;
    int i2;
    if (i1 == i3)
    {
      Object localObject1 = k;
      if (localObject1 != null)
      {
        boolean bool1 = ((AdContainer)localObject1).c();
        if (!bool1)
        {
          i2 = 200;
          i3 = q;
          Object localObject2;
          Object localObject3;
          Object localObject4;
          if (i2 == i3)
          {
            localObject1 = (RenderView)k;
            if (localObject1 != null)
            {
              localObject2 = r;
              if (localObject2 != null)
              {
                i3 = 1;
              }
              else
              {
                i3 = 0;
                localObject2 = null;
              }
              if (i3 != 0)
              {
                localObject2 = r;
                localObject3 = "broadcastEvent('backButtonPressed')";
                ((RenderView)localObject1).a((String)localObject2, (String)localObject3);
              }
              bool3 = q;
              if (bool3) {
                return;
              }
              a = bool2;
              try
              {
                ((RenderView)localObject1).b();
                return;
              }
              catch (Exception localException2)
              {
                localException2.getMessage();
                localObject1 = Logger.InternalLogLevel.DEBUG;
                localObject4 = "InMobi";
                localObject2 = "SDK encountered unexpected error in processing close request";
                Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject4, (String)localObject2);
              }
            }
            return;
          }
          localObject1 = k;
          boolean bool3 = localObject1 instanceof ba;
          if (bool3)
          {
            localObject1 = (ba)localObject1;
            if (localObject1 != null)
            {
              localObject2 = ((ba)localObject1).h();
              bool3 = b;
              if (bool3) {
                return;
              }
              a = bool2;
              localObject4 = o;
              if (localObject4 != null)
              {
                localObject4 = (bb)((NativeVideoView)localObject4).getTag();
                if (localObject4 != null)
                {
                  localObject2 = AdContainer.RenderingProperties.PlacementType.PLACEMENT_TYPE_FULLSCREEN;
                  localObject3 = b.a;
                  if (localObject2 == localObject3)
                  {
                    localObject2 = o;
                    ((NativeVideoView)localObject2).a();
                  }
                  try
                  {
                    localObject2 = v;
                    localObject3 = "isFullScreen";
                    localObject2 = ((Map)localObject2).get(localObject3);
                    localObject2 = (Boolean)localObject2;
                    bool3 = ((Boolean)localObject2).booleanValue();
                    if (bool3)
                    {
                      localObject2 = v;
                      localObject3 = "seekPosition";
                      Object localObject5 = o;
                      int i5 = ((NativeVideoView)localObject5).getCurrentPosition();
                      localObject5 = Integer.valueOf(i5);
                      ((Map)localObject2).put(localObject3, localObject5);
                      bool3 = i;
                      if (!bool3)
                      {
                        localObject2 = v;
                        localObject3 = "didRequestFullScreen";
                        localObject2 = ((Map)localObject2).get(localObject3);
                        localObject2 = (Boolean)localObject2;
                        bool3 = ((Boolean)localObject2).booleanValue();
                        if (bool3)
                        {
                          localObject2 = v;
                          localObject3 = "didRequestFullScreen";
                          localObject5 = Boolean.FALSE;
                          ((Map)localObject2).put(localObject3, localObject5);
                          localObject2 = y;
                          if (localObject2 != null)
                          {
                            localObject2 = y;
                            localObject2 = v;
                            localObject3 = "didRequestFullScreen";
                            localObject5 = Boolean.FALSE;
                            ((Map)localObject2).put(localObject3, localObject5);
                          }
                          ((ba)localObject1).b();
                          localObject1 = v;
                          localObject4 = "isFullScreen";
                          localObject2 = Boolean.FALSE;
                          ((Map)localObject1).put(localObject4, localObject2);
                        }
                      }
                    }
                    return;
                  }
                  catch (Exception localException1)
                  {
                    localException1.getMessage();
                    localObject4 = Logger.InternalLogLevel.DEBUG;
                    localObject3 = "SDK encountered unexpected error in closing video";
                    Logger.a((Logger.InternalLogLevel)localObject4, "InMobi", (String)localObject3);
                    localObject4 = com.inmobi.commons.core.a.a.a();
                    localObject2 = new com/inmobi/commons/core/e/a;
                    ((com.inmobi.commons.core.e.a)localObject2).<init>(localException1);
                    ((com.inmobi.commons.core.a.a)localObject4).a((com.inmobi.commons.core.e.a)localObject2);
                  }
                }
                return;
              }
              finish();
            }
            return;
          }
          bool2 = localException1 instanceof ad;
          if (!bool2) {
            return;
          }
          ad localad = (ad)localException1;
          if (localad != null)
          {
            localObject4 = localad.h();
            bool2 = b;
            if (bool2) {
              return;
            }
            localad.b();
            return;
          }
          finish();
          return;
        }
      }
    }
    else
    {
      int i4 = 100;
      if (i2 == i4)
      {
        a = bool2;
        finish();
      }
    }
  }
  
  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
    paramConfiguration = l;
    if (paramConfiguration != null)
    {
      Object localObject = "Resized";
      String str = d;
      boolean bool = ((String)localObject).equals(str);
      if (bool)
      {
        localObject = paramConfiguration.getResizeProperties();
        if (localObject != null)
        {
          paramConfiguration = g;
          paramConfiguration.a();
        }
      }
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    boolean bool1 = com.inmobi.commons.a.a.a();
    if (!bool1)
    {
      finish();
      Logger.a(Logger.InternalLogLevel.DEBUG, "InMobi", "Session not found, AdActivity will be closed");
      return;
    }
    bool1 = false;
    paramBundle = null;
    r = false;
    Object localObject1 = getIntent();
    Object localObject2 = "com.inmobi.rendering.InMobiAdActivity.EXTRA_AD_ACTIVITY_TYPE";
    int i2 = 102;
    float f1 = 1.43E-43F;
    int i4 = ((Intent)localObject1).getIntExtra((String)localObject2, i2);
    p = i4;
    i4 = p;
    int i9 = 100;
    int i10 = 10;
    boolean bool8 = true;
    Object localObject3 = null;
    int i12 = -1;
    Object localObject4;
    Object localObject5;
    int i11;
    int i13;
    Object localObject6;
    Object localObject7;
    if (i4 == i9)
    {
      localObject1 = getIntent().getStringExtra("com.inmobi.rendering.InMobiAdActivity.IN_APP_BROWSER_URL");
      localObject2 = new com/inmobi/rendering/RenderView;
      localObject4 = new com/inmobi/ads/AdContainer$RenderingProperties;
      localObject5 = AdContainer.RenderingProperties.PlacementType.PLACEMENT_TYPE_FULLSCREEN;
      ((AdContainer.RenderingProperties)localObject4).<init>((AdContainer.RenderingProperties.PlacementType)localObject5);
      ((RenderView)localObject2).<init>(this, (AdContainer.RenderingProperties)localObject4, null, null);
      l = ((RenderView)localObject2);
      localObject2 = RenderView.a;
      localObject4 = i;
      if (localObject4 != null)
      {
        localObject2 = ((RenderView)localObject4).getListener();
        localObject4 = i.getAdConfig();
      }
      else
      {
        localObject4 = new com/inmobi/ads/b;
        ((com.inmobi.ads.b)localObject4).<init>();
        localObject3 = j;
        if (localObject3 != null) {
          localObject2 = localObject3;
        }
      }
      l.setIsInAppBrowser(bool8);
      l.a((RenderView.a)localObject2, (com.inmobi.ads.b)localObject4);
      localObject2 = new android/widget/RelativeLayout;
      ((RelativeLayout)localObject2).<init>(this);
      localObject4 = new android/widget/RelativeLayout$LayoutParams;
      ((RelativeLayout.LayoutParams)localObject4).<init>(i12, i12);
      ((RelativeLayout.LayoutParams)localObject4).addRule(i10);
      i10 = (char)-3;
      i11 = 2;
      ((RelativeLayout.LayoutParams)localObject4).addRule(i11, i10);
      ((RelativeLayout)localObject2).setBackgroundColor(i12);
      localObject3 = l;
      ((RelativeLayout)localObject2).addView((View)localObject3, (ViewGroup.LayoutParams)localObject4);
      f1 = ac;
      localObject3 = new android/widget/LinearLayout;
      ((LinearLayout)localObject3).<init>(this);
      localObject5 = new android/widget/RelativeLayout$LayoutParams;
      i13 = (int)(48.0F * f1);
      ((RelativeLayout.LayoutParams)localObject5).<init>(i12, i13);
      ((LinearLayout)localObject3).setOrientation(0);
      ((LinearLayout)localObject3).setId(i10);
      ((LinearLayout)localObject3).setWeightSum(100.0F);
      ((LinearLayout)localObject3).setBackgroundResource(17301658);
      ((LinearLayout)localObject3).setBackgroundColor(-7829368);
      ((RelativeLayout.LayoutParams)localObject5).addRule(12);
      ((ViewGroup)localObject2).addView((View)localObject3, (ViewGroup.LayoutParams)localObject5);
      paramBundle = new android/widget/LinearLayout$LayoutParams;
      paramBundle.<init>(i12, i12);
      weight = 25.0F;
      localObject6 = new com/inmobi/rendering/CustomView;
      ((CustomView)localObject6).<init>(this, f1, i11);
      localObject7 = new com/inmobi/rendering/InMobiAdActivity$2;
      ((InMobiAdActivity.2)localObject7).<init>(this);
      ((CustomView)localObject6).setOnTouchListener((View.OnTouchListener)localObject7);
      ((LinearLayout)localObject3).addView((View)localObject6, paramBundle);
      localObject6 = new com/inmobi/rendering/CustomView;
      ((CustomView)localObject6).<init>(this, f1, 3);
      localObject7 = new com/inmobi/rendering/InMobiAdActivity$3;
      ((InMobiAdActivity.3)localObject7).<init>(this);
      ((CustomView)localObject6).setOnTouchListener((View.OnTouchListener)localObject7);
      ((LinearLayout)localObject3).addView((View)localObject6, paramBundle);
      localObject6 = new com/inmobi/rendering/CustomView;
      ((CustomView)localObject6).<init>(this, f1, 4);
      localObject7 = new com/inmobi/rendering/InMobiAdActivity$4;
      ((InMobiAdActivity.4)localObject7).<init>(this);
      ((CustomView)localObject6).setOnTouchListener((View.OnTouchListener)localObject7);
      ((LinearLayout)localObject3).addView((View)localObject6, paramBundle);
      localObject6 = new com/inmobi/rendering/CustomView;
      ((CustomView)localObject6).<init>(this, f1, 6);
      localObject4 = new com/inmobi/rendering/InMobiAdActivity$5;
      ((InMobiAdActivity.5)localObject4).<init>(this);
      ((CustomView)localObject6).setOnTouchListener((View.OnTouchListener)localObject4);
      ((LinearLayout)localObject3).addView((View)localObject6, paramBundle);
      setContentView((View)localObject2);
      l.loadUrl((String)localObject1);
      l.setFullScreenActivityContext(this);
      return;
    }
    label923:
    int i8;
    int i3;
    if (i4 == i2)
    {
      localObject1 = getIntent();
      localObject2 = "com.inmobi.rendering.InMobiAdActivity.EXTRA_AD_CONTAINER_INDEX";
      boolean bool3 = ((Intent)localObject1).hasExtra((String)localObject2);
      if (bool3)
      {
        int i5 = getIntent().getIntExtra("com.inmobi.rendering.InMobiAdActivity.EXTRA_AD_CONTAINER_INDEX", i12);
        localObject2 = h;
        localObject1 = (AdContainer)((SparseArray)localObject2).get(i5);
        k = ((AdContainer)localObject1);
        localObject1 = k;
        if (localObject1 == null)
        {
          finish();
          return;
        }
        localObject1 = getIntent();
        localObject2 = "com.inmobi.rendering.InMobiAdActivity.EXTRA_AD_CONTAINER_TYPE";
        i5 = ((Intent)localObject1).getIntExtra((String)localObject2, 0);
        q = i5;
        i5 = q;
        if (i5 == 0)
        {
          paramBundle = k.getFullScreenEventsListener();
          if (paramBundle != null)
          {
            paramBundle = k.getFullScreenEventsListener();
            paramBundle.a();
          }
          finish();
          return;
        }
        localObject1 = getIntent();
        localObject2 = "com.inmobi.rendering.InMobiAdActivity.EXTRA_AD_ACTIVITY_IS_FULL_SCREEN";
        boolean bool4 = ((Intent)localObject1).getBooleanExtra((String)localObject2, false);
        if (bool4)
        {
          requestWindowFeature(i11);
          localObject1 = getWindow();
          i9 = 1024;
          ((Window)localObject1).setFlags(i9, i9);
        }
        int i6 = 200;
        i9 = q;
        if (i6 == i9)
        {
          localObject1 = "html";
          localObject2 = k.getMarkupType();
          boolean bool5 = ((String)localObject1).equals(localObject2);
          if (!bool5) {}
        }
        else
        {
          int i7 = 201;
          i9 = q;
          if (i7 != i9) {
            break label923;
          }
          localObject1 = "inmobiJson";
          localObject2 = k.getMarkupType();
          boolean bool6 = ((String)localObject1).equals(localObject2);
          if (bool6) {
            break label923;
          }
        }
        paramBundle = k.getFullScreenEventsListener();
        if (paramBundle != null)
        {
          paramBundle = k.getFullScreenEventsListener();
          paramBundle.a();
        }
        finish();
        return;
        try
        {
          localObject1 = k;
          ((AdContainer)localObject1).setFullScreenActivityContext(this);
          i8 = 16908290;
          localObject1 = findViewById(i8);
          localObject1 = (FrameLayout)localObject1;
          localObject2 = new android/widget/RelativeLayout;
          localObject4 = getApplicationContext();
          ((RelativeLayout)localObject2).<init>((Context)localObject4);
          i2 = (char)-2;
          f1 = 9.1833E-41F;
          ((RelativeLayout)localObject2).setId(i2);
          localObject4 = c.a();
          f1 = c;
          localObject5 = "html";
          Object localObject8 = k;
          localObject8 = ((AdContainer)localObject8).getMarkupType();
          boolean bool9 = ((String)localObject5).equals(localObject8);
          boolean bool2;
          if (bool9)
          {
            ((RelativeLayout)localObject2).setBackgroundColor(0);
            localObject5 = new android/widget/RelativeLayout$LayoutParams;
            ((RelativeLayout.LayoutParams)localObject5).<init>(i12, i12);
            ((RelativeLayout.LayoutParams)localObject5).addRule(i10);
            localObject6 = new android/widget/RelativeLayout$LayoutParams;
            float f2 = 50.0F * f1;
            i13 = (int)f2;
            ((RelativeLayout.LayoutParams)localObject6).<init>(i13, i13);
            i13 = 11;
            f2 = 1.5E-44F;
            ((RelativeLayout.LayoutParams)localObject6).addRule(i13);
            localObject8 = new com/inmobi/rendering/CustomView;
            ((CustomView)localObject8).<init>(this, f1, 0);
            m = ((CustomView)localObject8);
            paramBundle = m;
            i13 = (char)-4;
            f2 = 9.183E-41F;
            paramBundle.setId(i13);
            paramBundle = m;
            localObject8 = new com/inmobi/rendering/InMobiAdActivity$6;
            ((InMobiAdActivity.6)localObject8).<init>(this);
            paramBundle.setOnClickListener((View.OnClickListener)localObject8);
            paramBundle = new com/inmobi/rendering/CustomView;
            paramBundle.<init>(this, f1, i11);
            n = paramBundle;
            paramBundle = n;
            i2 = (char)-5;
            f1 = 9.1828E-41F;
            paramBundle.setId(i2);
            paramBundle = n;
            localObject4 = new com/inmobi/rendering/InMobiAdActivity$7;
            ((InMobiAdActivity.7)localObject4).<init>(this);
            paramBundle.setOnClickListener((View.OnClickListener)localObject4);
            paramBundle = k;
            paramBundle = paramBundle.getViewableAd();
            paramBundle = paramBundle.a();
            if (paramBundle != null)
            {
              localObject4 = paramBundle.getParent();
              localObject4 = (ViewGroup)localObject4;
              if (localObject4 != null) {
                ((ViewGroup)localObject4).removeView(paramBundle);
              }
              ((RelativeLayout)localObject2).addView(paramBundle, (ViewGroup.LayoutParams)localObject5);
              paramBundle = m;
              ((RelativeLayout)localObject2).addView(paramBundle, (ViewGroup.LayoutParams)localObject6);
              paramBundle = n;
              ((RelativeLayout)localObject2).addView(paramBundle, (ViewGroup.LayoutParams)localObject6);
              paramBundle = k;
              paramBundle = (RenderView)paramBundle;
              localObject4 = k;
              localObject4 = (RenderView)localObject4;
              bool2 = p;
              paramBundle.a(bool2);
              paramBundle = k;
              paramBundle = (RenderView)paramBundle;
              localObject4 = k;
              localObject4 = (RenderView)localObject4;
              bool2 = m;
              paramBundle.b(bool2);
            }
          }
          else
          {
            localObject4 = "inmobiJson";
            localObject6 = k;
            localObject6 = ((AdContainer)localObject6).getMarkupType();
            bool2 = ((String)localObject4).equals(localObject6);
            if (!bool2) {
              break label1811;
            }
            localObject4 = k;
            localObject4 = ((AdContainer)localObject4).getRenderingProperties();
            localObject4 = a;
            i10 = -16777216;
            ((RelativeLayout)localObject2).setBackgroundColor(i10);
            localObject6 = k;
            localObject6 = ((AdContainer)localObject6).getDataModel();
            localObject6 = (ak)localObject6;
            localObject7 = d;
            localObject7 = c;
            localObject7 = a;
            localObject5 = new com/inmobi/ads/b;
            ((com.inmobi.ads.b)localObject5).<init>();
            localObject8 = com.inmobi.commons.core.configs.b.a();
            ((com.inmobi.commons.core.configs.b)localObject8).a((com.inmobi.commons.core.configs.a)localObject5, null);
            localObject5 = k;
            localObject5 = ((AdContainer)localObject5).getViewableAd();
            boolean bool7 = c;
            if (bool7)
            {
              localObject6 = ((bw)localObject5).b();
            }
            else
            {
              bool7 = false;
              localObject6 = null;
            }
            if (localObject6 == null) {
              localObject6 = ((bw)localObject5).a(null, (ViewGroup)localObject2, false);
            }
            paramBundle = k;
            bool1 = paramBundle instanceof ba;
            if (bool1)
            {
              paramBundle = k;
              paramBundle = paramBundle.getVideoContainerView();
              paramBundle = (NativeVideoWrapper)paramBundle;
              if (paramBundle != null)
              {
                paramBundle = paramBundle.getVideoView();
                o = paramBundle;
                paramBundle = o;
                paramBundle.requestFocus();
                paramBundle = o;
                paramBundle = paramBundle.getTag();
                paramBundle = (bb)paramBundle;
                localObject5 = y;
                if (localObject5 != null)
                {
                  localObject5 = y;
                  localObject5 = (bb)localObject5;
                  paramBundle.a((bb)localObject5);
                }
                localObject5 = AdContainer.RenderingProperties.PlacementType.PLACEMENT_TYPE_INLINE;
                if (localObject5 == localObject4)
                {
                  paramBundle = v;
                  localObject4 = "placementType";
                  localObject5 = AdContainer.RenderingProperties.PlacementType.PLACEMENT_TYPE_INLINE;
                  paramBundle.put(localObject4, localObject5);
                }
                else
                {
                  paramBundle = v;
                  localObject4 = "placementType";
                  localObject5 = AdContainer.RenderingProperties.PlacementType.PLACEMENT_TYPE_FULLSCREEN;
                  paramBundle.put(localObject4, localObject5);
                }
              }
            }
            if (localObject6 != null)
            {
              paramBundle = new android/widget/RelativeLayout$LayoutParams;
              i3 = x;
              i11 = y;
              paramBundle.<init>(i3, i11);
              ((RelativeLayout)localObject2).addView((View)localObject6, paramBundle);
            }
            paramBundle = k;
            paramBundle.setRequestedScreenOrientation();
          }
          paramBundle = new android/widget/RelativeLayout$LayoutParams;
          paramBundle.<init>(i12, i12);
          ((FrameLayout)localObject1).addView((View)localObject2, paramBundle);
          return;
          label1811:
          paramBundle = k;
          paramBundle = paramBundle.getFullScreenEventsListener();
          if (paramBundle != null)
          {
            paramBundle = k;
            paramBundle = paramBundle.getFullScreenEventsListener();
            paramBundle.a();
          }
          finish();
          return;
        }
        catch (Exception paramBundle)
        {
          k.setFullScreenActivityContext(null);
          localObject1 = k.getFullScreenEventsListener();
          if (localObject1 != null)
          {
            localObject1 = k.getFullScreenEventsListener();
            ((AdContainer.a)localObject1).a();
          }
          finish();
          localObject1 = com.inmobi.commons.core.a.a.a();
          localObject2 = new com/inmobi/commons/core/e/a;
          ((com.inmobi.commons.core.e.a)localObject2).<init>(paramBundle);
          ((com.inmobi.commons.core.a.a)localObject1).a((com.inmobi.commons.core.e.a)localObject2);
          return;
        }
      }
    }
    else
    {
      int i1 = 103;
      if (i8 == i1)
      {
        paramBundle = getIntent();
        localObject1 = "id";
        i1 = paramBundle.getIntExtra((String)localObject1, i12);
        if (i1 != i12)
        {
          localObject1 = c;
          localObject2 = Integer.valueOf(i1);
          localObject1 = (Intent)((Map)localObject1).get(localObject2);
          startActivityForResult((Intent)localObject1, i1);
        }
        return;
      }
      i1 = 104;
      if (i8 == i1)
      {
        paramBundle = getIntent();
        localObject1 = "id";
        i1 = paramBundle.getIntExtra((String)localObject1, i12);
        if (i1 != i12)
        {
          localObject1 = getIntent();
          localObject2 = "permissions";
          localObject1 = ((Intent)localObject1).getStringArrayExtra((String)localObject2);
          if (localObject1 != null)
          {
            i9 = localObject1.length;
            if (i9 > 0)
            {
              i9 = Build.VERSION.SDK_INT;
              i3 = 23;
              f1 = 3.2E-44F;
              if (i9 >= i3)
              {
                com.inmobi.commons.core.utilities.a.b();
                requestPermissions((String[])localObject1, i1);
              }
            }
          }
        }
      }
    }
  }
  
  protected void onDestroy()
  {
    boolean bool1 = a;
    int i2 = 15;
    int i4 = 201;
    int i5 = 200;
    int i6 = 102;
    int i8 = 100;
    int i1;
    Object localObject4;
    Object localObject5;
    int i3;
    String str;
    Object localObject2;
    if (bool1)
    {
      i1 = p;
      Object localObject1;
      if (i8 == i1)
      {
        localObject1 = l;
        if (localObject1 != null)
        {
          localObject1 = ((RenderView)localObject1).getFullScreenEventsListener();
          if (localObject1 != null) {
            try
            {
              localObject1 = l;
              localObject1 = ((RenderView)localObject1).getFullScreenEventsListener();
              localObject4 = l;
              ((AdContainer.a)localObject1).b(localObject4);
              localObject1 = l;
              ((RenderView)localObject1).destroy();
              l = null;
            }
            catch (Exception localException5) {}
          }
        }
      }
      else if (i6 == i1)
      {
        localObject1 = k;
        if (localObject1 != null)
        {
          localObject1 = ((AdContainer)localObject1).getFullScreenEventsListener();
          if (localObject1 != null)
          {
            i1 = q;
            if (i5 == i1)
            {
              try
              {
                localObject1 = k;
                localObject1 = ((AdContainer)localObject1).getFullScreenEventsListener();
                ((AdContainer.a)localObject1).b(null);
              }
              catch (Exception localException6)
              {
                localException6.getMessage();
                localObject1 = Logger.InternalLogLevel.DEBUG;
                localObject4 = "InMobi";
                localObject5 = "SDK encountered unexpected error while finishing fullscreen view";
                Logger.a((Logger.InternalLogLevel)localObject1, (String)localObject4, (String)localObject5);
              }
            }
            else if (i4 == i1)
            {
              i1 = Build.VERSION.SDK_INT;
              if (i1 >= i2)
              {
                localObject1 = k;
                i3 = localObject1 instanceof ba;
                if (i3 != 0)
                {
                  localObject1 = (NativeVideoWrapper)((ba)localObject1).getVideoContainerView();
                  if (localObject1 != null)
                  {
                    localObject1 = (bb)((NativeVideoWrapper)localObject1).getVideoView().getTag();
                    try
                    {
                      localObject4 = k;
                      localObject4 = ((AdContainer)localObject4).getFullScreenEventsListener();
                      ((AdContainer.a)localObject4).b(localObject1);
                    }
                    catch (Exception localException1)
                    {
                      localException1.getMessage();
                      localObject4 = Logger.InternalLogLevel.DEBUG;
                      str = "SDK encountered unexpected error while finishing fullscreen view";
                      Logger.a((Logger.InternalLogLevel)localObject4, "InMobi", str);
                      localObject4 = com.inmobi.commons.core.a.a.a();
                      localObject5 = new com/inmobi/commons/core/e/a;
                      ((com.inmobi.commons.core.e.a)localObject5).<init>(localException1);
                      ((com.inmobi.commons.core.a.a)localObject4).a((com.inmobi.commons.core.e.a)localObject5);
                    }
                  }
                }
                else
                {
                  i3 = localException1 instanceof ad;
                  if (i3 != 0) {
                    try
                    {
                      AdContainer.a locala = ((AdContainer)localException1).getFullScreenEventsListener();
                      locala.b(null);
                    }
                    catch (Exception localException2)
                    {
                      localException2.getMessage();
                      localObject4 = Logger.InternalLogLevel.DEBUG;
                      str = "SDK encountered unexpected error while finishing fullscreen view";
                      Logger.a((Logger.InternalLogLevel)localObject4, "InMobi", str);
                      localObject4 = com.inmobi.commons.core.a.a.a();
                      localObject5 = new com/inmobi/commons/core/e/a;
                      ((com.inmobi.commons.core.e.a)localObject5).<init>(localException2);
                      ((com.inmobi.commons.core.a.a)localObject4).a((com.inmobi.commons.core.e.a)localObject5);
                    }
                  }
                }
              }
            }
          }
        }
      }
      localObject2 = k;
      if (localObject2 != null)
      {
        ((AdContainer)localObject2).destroy();
        k = null;
      }
    }
    else
    {
      i1 = p;
      if ((i8 != i1) && (i6 == i1))
      {
        localObject2 = k;
        if (localObject2 != null)
        {
          int i7 = q;
          if (i5 == i7)
          {
            localObject2 = (RenderView)localObject2;
            ((RenderView)localObject2).setFullScreenActivityContext(null);
            try
            {
              ((RenderView)localObject2).b();
            }
            catch (Exception localException7)
            {
              localException7.getMessage();
              localObject2 = Logger.InternalLogLevel.DEBUG;
              localObject4 = "InMobi";
              localObject5 = "SDK encountered unexpected error in processing close request";
              Logger.a((Logger.InternalLogLevel)localObject2, (String)localObject4, (String)localObject5);
            }
          }
          else if (i4 == i7)
          {
            i1 = Build.VERSION.SDK_INT;
            if (i1 >= i3)
            {
              localObject2 = k;
              boolean bool2 = localObject2 instanceof ba;
              if (bool2)
              {
                localObject2 = (ba)localObject2;
                localObject4 = o;
                if (localObject4 != null)
                {
                  localObject4 = (bb)((NativeVideoView)localObject4).getTag();
                  if (localObject4 != null)
                  {
                    localObject5 = AdContainer.RenderingProperties.PlacementType.PLACEMENT_TYPE_FULLSCREEN;
                    localObject2 = b.a;
                    if (localObject5 == localObject2)
                    {
                      localObject2 = o;
                      ((NativeVideoView)localObject2).a();
                    }
                    localObject2 = k.getFullScreenEventsListener();
                    if (localObject2 != null) {
                      try
                      {
                        localObject2 = k;
                        localObject2 = ((AdContainer)localObject2).getFullScreenEventsListener();
                        ((AdContainer.a)localObject2).b(localObject4);
                      }
                      catch (Exception localException3)
                      {
                        localException3.getMessage();
                        localObject4 = Logger.InternalLogLevel.DEBUG;
                        str = "SDK encountered unexpected error while finishing fullscreen view";
                        Logger.a((Logger.InternalLogLevel)localObject4, "InMobi", str);
                        localObject4 = com.inmobi.commons.core.a.a.a();
                        localObject5 = new com/inmobi/commons/core/e/a;
                        ((com.inmobi.commons.core.e.a)localObject5).<init>(localException3);
                        ((com.inmobi.commons.core.a.a)localObject4).a((com.inmobi.commons.core.e.a)localObject5);
                      }
                    }
                  }
                }
              }
              else
              {
                bool2 = localException3 instanceof ad;
                if (bool2)
                {
                  Object localObject3 = ((AdContainer)localException3).getFullScreenEventsListener();
                  if (localObject3 != null) {
                    try
                    {
                      localObject3 = k;
                      localObject3 = ((AdContainer)localObject3).getFullScreenEventsListener();
                      ((AdContainer.a)localObject3).b(null);
                    }
                    catch (Exception localException4)
                    {
                      localException4.getMessage();
                      localObject4 = Logger.InternalLogLevel.DEBUG;
                      str = "SDK encountered unexpected error while finishing fullscreen view";
                      Logger.a((Logger.InternalLogLevel)localObject4, "InMobi", str);
                      localObject4 = com.inmobi.commons.core.a.a.a();
                      localObject5 = new com/inmobi/commons/core/e/a;
                      ((com.inmobi.commons.core.e.a)localObject5).<init>(localException4);
                      ((com.inmobi.commons.core.a.a)localObject4).a((com.inmobi.commons.core.e.a)localObject5);
                    }
                  }
                }
              }
            }
          }
          a(k);
          AdContainer localAdContainer = k;
          localAdContainer.destroy();
          k = null;
        }
      }
    }
    super.onDestroy();
  }
  
  public void onMultiWindowModeChanged(boolean paramBoolean)
  {
    super.onMultiWindowModeChanged(paramBoolean);
    if (!paramBoolean)
    {
      Object localObject = l;
      if (localObject != null)
      {
        g localg = ((RenderView)localObject).getOrientationProperties();
        ((RenderView)localObject).setOrientationProperties(localg);
      }
      localObject = k;
      if (localObject != null) {
        ((AdContainer)localObject).setRequestedScreenOrientation();
      }
    }
  }
  
  public void onMultiWindowModeChanged(boolean paramBoolean, Configuration paramConfiguration)
  {
    super.onMultiWindowModeChanged(paramBoolean, paramConfiguration);
    onMultiWindowModeChanged(paramBoolean);
  }
  
  public void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    com.inmobi.commons.core.utilities.a.c();
    paramArrayOfString = e;
    Object localObject = Integer.valueOf(paramInt);
    localObject = (InMobiAdActivity.b)paramArrayOfString.remove(localObject);
    if (localObject != null) {
      ((InMobiAdActivity.b)localObject).a(paramArrayOfInt);
    }
    finish();
  }
  
  protected void onResume()
  {
    super.onResume();
    boolean bool1 = a;
    boolean bool6;
    Object localObject1;
    if (!bool1)
    {
      int i1 = 100;
      i4 = p;
      bool6 = true;
      if (i1 == i4)
      {
        localObject1 = l;
        if (localObject1 == null) {
          break label460;
        }
        localObject1 = ((RenderView)localObject1).getFullScreenEventsListener();
        if (localObject1 == null) {
          break label460;
        }
      }
    }
    try
    {
      boolean bool2 = r;
      if (bool2) {
        break label460;
      }
      r = bool6;
      localObject1 = l;
      localObject1 = ((RenderView)localObject1).getFullScreenEventsListener();
      localObject4 = l;
      ((AdContainer.a)localObject1).a(localObject4);
    }
    catch (Exception localException3)
    {
      Object localObject4;
      int i2;
      int i5;
      boolean bool3;
      int i3;
      boolean bool4;
      boolean bool5;
      Object localObject5;
      long l1;
      com.inmobi.commons.core.e.a locala;
      Object localObject2;
      Object localObject3;
      for (;;) {}
    }
    i2 = q;
    i5 = 200;
    if (i2 == i5)
    {
      i2 = 102;
      if (i2 == i4)
      {
        localObject1 = k;
        if (localObject1 == null) {
          break label460;
        }
        localObject1 = ((AdContainer)localObject1).getFullScreenEventsListener();
        if (localObject1 == null) {
          break label460;
        }
        bool3 = r;
        if (bool3) {
          break label460;
        }
        r = bool6;
        localObject1 = k;
        localObject1 = ((AdContainer)localObject1).getFullScreenEventsListener();
        ((AdContainer.a)localObject1).a(null);
        break label460;
      }
    }
    i3 = 201;
    int i4 = q;
    if (i3 == i4)
    {
      localObject1 = k;
      bool4 = localObject1 instanceof ba;
      if (bool4)
      {
        localObject1 = o;
        if (localObject1 != null)
        {
          localObject1 = (bb)((NativeVideoView)localObject1).getTag();
          if (localObject1 != null)
          {
            bool5 = s;
            if (bool5)
            {
              localObject4 = new android/os/Handler;
              localObject5 = Looper.getMainLooper();
              ((Handler)localObject4).<init>((Looper)localObject5);
              localObject5 = new com/inmobi/rendering/InMobiAdActivity$1;
              ((InMobiAdActivity.1)localObject5).<init>(this, (bb)localObject1);
              l1 = 50;
              ((Handler)localObject4).postDelayed((Runnable)localObject5, l1);
            }
          }
          localObject4 = k.getFullScreenEventsListener();
          if (localObject4 == null) {
            break label460;
          }
          try
          {
            bool5 = r;
            if (bool5) {
              break label460;
            }
            r = bool6;
            localObject4 = k;
            localObject4 = ((AdContainer)localObject4).getFullScreenEventsListener();
            ((AdContainer.a)localObject4).a(localObject1);
          }
          catch (Exception localException1)
          {
            localObject4 = com.inmobi.commons.core.a.a.a();
            locala = new com/inmobi/commons/core/e/a;
            locala.<init>(localException1);
            ((com.inmobi.commons.core.a.a)localObject4).a(locala);
          }
        }
      }
      localObject2 = k;
      bool5 = localObject2 instanceof ad;
      if (bool5) {
        try
        {
          bool5 = r;
          if (!bool5)
          {
            r = bool6;
            localObject2 = ((AdContainer)localObject2).getFullScreenEventsListener();
            ((AdContainer.a)localObject2).a(null);
          }
        }
        catch (Exception localException2)
        {
          localObject4 = com.inmobi.commons.core.a.a.a();
          locala = new com/inmobi/commons/core/e/a;
          locala.<init>(localException2);
          ((com.inmobi.commons.core.a.a)localObject4).a(locala);
        }
      }
    }
    label460:
    bool4 = false;
    localObject3 = null;
    s = false;
  }
  
  protected void onStart()
  {
    super.onStart();
    boolean bool1 = a;
    if (!bool1)
    {
      int i1 = 102;
      int i2 = p;
      if (i1 == i2)
      {
        Object localObject1 = k;
        if (localObject1 != null)
        {
          localObject1 = ((AdContainer)localObject1).getViewableAd();
          i2 = 200;
          int i3 = q;
          CustomView localCustomView = null;
          Object localObject3;
          Object localObject4;
          if (i2 == i3)
          {
            localObject3 = AdContainer.RenderingProperties.PlacementType.PLACEMENT_TYPE_FULLSCREEN;
            localObject4 = k.getRenderingProperties().a;
            if (localObject3 != localObject4) {
              return;
            }
            i2 = 2;
            try
            {
              localObject3 = new View[i2];
              localObject4 = m;
              localObject3[0] = localObject4;
              i3 = 1;
              localCustomView = n;
              localObject3[i3] = localCustomView;
              ((bw)localObject1).a((View[])localObject3);
              return;
            }
            catch (Exception localException3)
            {
              localException3.getMessage();
              localObject1 = k.getFullScreenEventsListener();
              if (localObject1 != null)
              {
                localObject1 = k.getFullScreenEventsListener();
                ((AdContainer.a)localObject1).a();
              }
              return;
            }
          }
          i2 = 201;
          if (i2 == i3) {
            try
            {
              localObject3 = new com/inmobi/ads/b;
              ((com.inmobi.ads.b)localObject3).<init>();
              localObject4 = com.inmobi.commons.core.configs.b.a();
              int i4 = 0;
              String str1 = null;
              ((com.inmobi.commons.core.configs.b)localObject4).a((com.inmobi.commons.core.configs.a)localObject3, null);
              localObject4 = ((bw)localObject1).b();
              if (localObject4 != null)
              {
                localObject4 = k;
                boolean bool3 = localObject4 instanceof ba;
                if (bool3)
                {
                  localObject4 = o;
                  localObject4 = ((NativeVideoView)localObject4).getTag();
                  localObject4 = (bb)localObject4;
                  if (localObject4 != null)
                  {
                    localObject3 = o;
                    i4 = g;
                    Map localMap = G;
                    String str2 = "time";
                    boolean bool4 = localMap.containsKey(str2);
                    if (bool4)
                    {
                      localObject4 = G;
                      str1 = "time";
                      localObject4 = ((Map)localObject4).get(str1);
                      localObject4 = (Integer)localObject4;
                      i4 = ((Integer)localObject4).intValue();
                    }
                    g = i4;
                    localObject3 = new View[0];
                    ((bw)localObject1).a((View[])localObject3);
                  }
                  return;
                }
                localObject3 = k;
                boolean bool2 = localObject3 instanceof ad;
                if (bool2) {
                  try
                  {
                    localObject3 = new View[0];
                    ((bw)localObject1).a((View[])localObject3);
                    return;
                  }
                  catch (Exception localException1)
                  {
                    localException1.getMessage();
                    Object localObject2 = k;
                    localObject2 = ((AdContainer)localObject2).getFullScreenEventsListener();
                    if (localObject2 != null)
                    {
                      localObject2 = k;
                      localObject2 = ((AdContainer)localObject2).getFullScreenEventsListener();
                      ((AdContainer.a)localObject2).a();
                    }
                  }
                }
              }
              return;
            }
            catch (Exception localException2)
            {
              localException2.getMessage();
              localObject3 = k.getFullScreenEventsListener();
              if (localObject3 != null)
              {
                localObject3 = k.getFullScreenEventsListener();
                ((AdContainer.a)localObject3).a();
              }
              localObject3 = com.inmobi.commons.core.a.a.a();
              localObject4 = new com/inmobi/commons/core/e/a;
              ((com.inmobi.commons.core.e.a)localObject4).<init>(localException2);
              ((com.inmobi.commons.core.a.a)localObject3).a((com.inmobi.commons.core.e.a)localObject4);
            }
          }
        }
      }
    }
  }
  
  public void onStop()
  {
    super.onStop();
    boolean bool = a;
    if (!bool)
    {
      bool = true;
      s = bool;
      NativeVideoView localNativeVideoView = o;
      if (localNativeVideoView != null) {
        localNativeVideoView.pause();
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.rendering.InMobiAdActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */