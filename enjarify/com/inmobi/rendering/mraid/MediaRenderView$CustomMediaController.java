package com.inmobi.rendering.mraid;

import android.content.Context;
import android.os.Build.VERSION;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.MediaController;
import java.lang.reflect.Field;

public final class MediaRenderView$CustomMediaController
  extends MediaController
{
  public MediaRenderView$CustomMediaController(Context paramContext)
  {
    super(paramContext);
  }
  
  public final void show(int paramInt)
  {
    super.show(paramInt);
    paramInt = Build.VERSION.SDK_INT;
    int i = 19;
    if (paramInt < i)
    {
      Object localObject1 = MediaController.class;
      String str = "mAnchor";
      try
      {
        localObject1 = ((Class)localObject1).getDeclaredField(str);
        i = 1;
        ((Field)localObject1).setAccessible(i);
        localObject1 = ((Field)localObject1).get(this);
        localObject1 = (View)localObject1;
        Object localObject2 = MediaController.class;
        Object localObject3 = "mDecor";
        localObject2 = ((Class)localObject2).getDeclaredField((String)localObject3);
        ((Field)localObject2).setAccessible(i);
        localObject2 = ((Field)localObject2).get(this);
        localObject2 = (View)localObject2;
        localObject3 = MediaController.class;
        Object localObject4 = "mDecorLayoutParams";
        localObject3 = ((Class)localObject3).getDeclaredField((String)localObject4);
        ((Field)localObject3).setAccessible(i);
        localObject3 = ((Field)localObject3).get(this);
        localObject3 = (WindowManager.LayoutParams)localObject3;
        localObject4 = MediaController.class;
        Object localObject5 = "mWindowManager";
        localObject4 = ((Class)localObject4).getDeclaredField((String)localObject5);
        ((Field)localObject4).setAccessible(i);
        localObject4 = ((Field)localObject4).get(this);
        localObject4 = (WindowManager)localObject4;
        int j = 2;
        localObject5 = new int[j];
        ((View)localObject1).getLocationOnScreen((int[])localObject5);
        int k = ((View)localObject1).getWidth();
        int m = -1 << -1;
        k = View.MeasureSpec.makeMeasureSpec(k, m);
        int n = ((View)localObject1).getHeight();
        m = View.MeasureSpec.makeMeasureSpec(n, m);
        ((View)localObject2).measure(k, m);
        k = 0;
        ((View)localObject2).setPadding(0, 0, 0, 0);
        m = 0;
        verticalMargin = 0.0F;
        horizontalMargin = 0.0F;
        m = ((View)localObject1).getWidth();
        width = m;
        m = 8388659;
        gravity = m;
        k = localObject5[0];
        x = k;
        i = localObject5[i];
        paramInt = ((View)localObject1).getHeight();
        i += paramInt;
        paramInt = ((View)localObject2).getMeasuredHeight();
        i -= paramInt;
        y = i;
        ((WindowManager)localObject4).updateViewLayout((View)localObject2, (ViewGroup.LayoutParams)localObject3);
        return;
      }
      catch (Exception localException) {}
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.rendering.mraid.MediaRenderView.CustomMediaController
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */