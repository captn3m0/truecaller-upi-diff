package com.inmobi.rendering.mraid;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CalendarContract.Events;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;

public class a
{
  private static final SimpleDateFormat[] a;
  private static String b = a.class.getSimpleName();
  
  static
  {
    SimpleDateFormat[] arrayOfSimpleDateFormat = new SimpleDateFormat[11];
    SimpleDateFormat localSimpleDateFormat = new java/text/SimpleDateFormat;
    Locale localLocale = Locale.US;
    localSimpleDateFormat.<init>("yyyy-MM-dd'T'hh:mmZ", localLocale);
    arrayOfSimpleDateFormat[0] = localSimpleDateFormat;
    localSimpleDateFormat = new java/text/SimpleDateFormat;
    localLocale = Locale.US;
    localSimpleDateFormat.<init>("yyyy-MM-dd'T'HH:mm:ssZ", localLocale);
    arrayOfSimpleDateFormat[1] = localSimpleDateFormat;
    localSimpleDateFormat = new java/text/SimpleDateFormat;
    localLocale = Locale.US;
    localSimpleDateFormat.<init>("yyyy-MM-dd'T'HH:mm:ssz", localLocale);
    arrayOfSimpleDateFormat[2] = localSimpleDateFormat;
    localSimpleDateFormat = new java/text/SimpleDateFormat;
    localLocale = Locale.US;
    localSimpleDateFormat.<init>("yyyy-MM-dd'T'HH:mm:ss", localLocale);
    arrayOfSimpleDateFormat[3] = localSimpleDateFormat;
    localSimpleDateFormat = new java/text/SimpleDateFormat;
    localLocale = Locale.US;
    localSimpleDateFormat.<init>("yyyy-MM-dd", localLocale);
    arrayOfSimpleDateFormat[4] = localSimpleDateFormat;
    localSimpleDateFormat = new java/text/SimpleDateFormat;
    localLocale = Locale.US;
    localSimpleDateFormat.<init>("yyyy-MM", localLocale);
    arrayOfSimpleDateFormat[5] = localSimpleDateFormat;
    localSimpleDateFormat = new java/text/SimpleDateFormat;
    localLocale = Locale.US;
    localSimpleDateFormat.<init>("yyyyMMddHHmmssZ", localLocale);
    arrayOfSimpleDateFormat[6] = localSimpleDateFormat;
    localSimpleDateFormat = new java/text/SimpleDateFormat;
    localLocale = Locale.US;
    localSimpleDateFormat.<init>("yyyyMMddHHmm", localLocale);
    arrayOfSimpleDateFormat[7] = localSimpleDateFormat;
    localSimpleDateFormat = new java/text/SimpleDateFormat;
    localLocale = Locale.US;
    localSimpleDateFormat.<init>("yyyyMMdd", localLocale);
    arrayOfSimpleDateFormat[8] = localSimpleDateFormat;
    localSimpleDateFormat = new java/text/SimpleDateFormat;
    localLocale = Locale.US;
    localSimpleDateFormat.<init>("yyyyMM", localLocale);
    arrayOfSimpleDateFormat[9] = localSimpleDateFormat;
    localSimpleDateFormat = new java/text/SimpleDateFormat;
    localLocale = Locale.US;
    localSimpleDateFormat.<init>("yyyy", localLocale);
    arrayOfSimpleDateFormat[10] = localSimpleDateFormat;
    a = arrayOfSimpleDateFormat;
  }
  
  public static int a(Context paramContext)
  {
    String str = "title";
    String[] arrayOfString = { "_id", str };
    Object localObject = paramContext.getContentResolver();
    Uri localUri = CalendarContract.Events.CONTENT_URI;
    paramContext = ((ContentResolver)localObject).query(localUri, arrayOfString, null, null, null);
    int i = 0;
    if (paramContext != null)
    {
      boolean bool = paramContext.moveToLast();
      if (bool)
      {
        int j = paramContext.getColumnIndex("title");
        int k = paramContext.getColumnIndex("_id");
        str = paramContext.getString(j);
        localObject = paramContext.getString(k);
        if (str != null) {
          i = Integer.parseInt((String)localObject);
        }
        paramContext.close();
      }
    }
    return i;
  }
  
  public static String a(String paramString)
  {
    String str = null;
    if (paramString != null)
    {
      Object localObject1 = "";
      boolean bool = ((String)localObject1).equals(paramString);
      if (!bool)
      {
        localObject1 = a;
        int j = localObject1.length;
        int k = 0;
        int m = 0;
        SimpleDateFormat localSimpleDateFormat = null;
        Object localObject2;
        while (m < j)
        {
          localObject2 = localObject1[m];
          try
          {
            paramString = ((SimpleDateFormat)localObject2).parse(paramString);
          }
          catch (ParseException localParseException)
          {
            m += 1;
          }
        }
        paramString = null;
        if (paramString != null)
        {
          int i = 3;
          DateFormat[] arrayOfDateFormat = new DateFormat[i];
          localSimpleDateFormat = new java/text/SimpleDateFormat;
          Object localObject3 = Locale.US;
          localSimpleDateFormat.<init>("yyyyMMdd'T'HHmmssZ", (Locale)localObject3);
          arrayOfDateFormat[0] = localSimpleDateFormat;
          localSimpleDateFormat = new java/text/SimpleDateFormat;
          localObject3 = Locale.US;
          localSimpleDateFormat.<init>("yyyyMMdd'T'HHmm", (Locale)localObject3);
          int n = 1;
          arrayOfDateFormat[n] = localSimpleDateFormat;
          m = 2;
          localObject2 = new java/text/SimpleDateFormat;
          localObject3 = "yyyyMMdd";
          Locale localLocale = Locale.US;
          ((SimpleDateFormat)localObject2).<init>((String)localObject3, localLocale);
          arrayOfDateFormat[m] = localObject2;
          while (k < i)
          {
            localSimpleDateFormat = arrayOfDateFormat[k];
            try
            {
              long l = paramString.getTime();
              localObject2 = Long.valueOf(l);
              str = localSimpleDateFormat.format(localObject2);
            }
            catch (IllegalArgumentException localIllegalArgumentException)
            {
              k += 1;
            }
          }
          return str;
        }
      }
    }
    return null;
  }
  
  public static String a(JSONArray paramJSONArray)
  {
    int i = 0;
    if (paramJSONArray != null)
    {
      int j = paramJSONArray.length();
      if (j != 0)
      {
        StringBuilder localStringBuilder1 = new java/lang/StringBuilder;
        localStringBuilder1.<init>();
        int k = 0;
        for (;;)
        {
          int m = paramJSONArray.length();
          if (k < m) {
            try
            {
              Object localObject = paramJSONArray.get(k);
              StringBuilder localStringBuilder2 = new java/lang/StringBuilder;
              localStringBuilder2.<init>();
              localStringBuilder2.append(localObject);
              localObject = ",";
              localStringBuilder2.append((String)localObject);
              localObject = localStringBuilder2.toString();
              localStringBuilder1.append((String)localObject);
              k += 1;
            }
            catch (JSONException localJSONException)
            {
              return null;
            }
          }
        }
        paramJSONArray = localStringBuilder1.toString();
        j = paramJSONArray.length();
        if (j == 0) {
          return null;
        }
        j += -1;
        i = paramJSONArray.charAt(j);
        k = 44;
        if (i == k) {
          paramJSONArray = paramJSONArray.substring(0, j);
        }
        return paramJSONArray;
      }
    }
    return null;
  }
  
  public static String a(JSONArray paramJSONArray, int paramInt1, int paramInt2)
  {
    int i = 0;
    if (paramJSONArray != null)
    {
      int j = paramJSONArray.length();
      if (j != 0)
      {
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        int k = 0;
        for (;;)
        {
          int m = paramJSONArray.length();
          if (k < m) {
            try
            {
              m = paramJSONArray.getInt(k);
              if ((m >= paramInt1) && (m <= paramInt2) && (m != 0))
              {
                localStringBuilder.append(m);
                String str = ",";
                localStringBuilder.append(str);
              }
              k += 1;
            }
            catch (JSONException localJSONException)
            {
              localJSONException.getMessage();
              return null;
            }
          }
        }
        paramJSONArray = localStringBuilder.toString();
        paramInt1 = paramJSONArray.length();
        if (paramInt1 == 0) {
          return null;
        }
        paramInt1 += -1;
        paramInt2 = paramJSONArray.charAt(paramInt1);
        i = 44;
        if (paramInt2 == i) {
          paramJSONArray = paramJSONArray.substring(0, paramInt1);
        }
        return paramJSONArray;
      }
    }
    return null;
  }
  
  public static GregorianCalendar b(String paramString)
  {
    SimpleDateFormat[] arrayOfSimpleDateFormat = a;
    int i = arrayOfSimpleDateFormat.length;
    int j = 0;
    while (j < i)
    {
      SimpleDateFormat localSimpleDateFormat = arrayOfSimpleDateFormat[j];
      try
      {
        Date localDate = localSimpleDateFormat.parse(paramString);
        GregorianCalendar localGregorianCalendar = new java/util/GregorianCalendar;
        localGregorianCalendar.<init>();
        localGregorianCalendar.setTime(localDate);
        localSimpleDateFormat.toPattern();
        return (GregorianCalendar)localGregorianCalendar;
      }
      catch (ParseException localParseException)
      {
        localSimpleDateFormat.toPattern();
        j += 1;
      }
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.rendering.mraid.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */