package com.inmobi.rendering.mraid;

import android.os.SystemClock;
import com.inmobi.commons.core.e.b;
import com.inmobi.commons.core.network.c;
import com.inmobi.signals.o;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class e$1
  implements Runnable
{
  public e$1(e parame) {}
  
  public final void run()
  {
    Object localObject1 = null;
    int i = 0;
    Object localObject3 = null;
    Object localObject4;
    long l1;
    Object localObject5;
    boolean bool;
    for (;;)
    {
      localObject4 = a;
      int j = e.a((e)localObject4);
      if (i > j) {
        return;
      }
      e.a();
      l1 = SystemClock.elapsedRealtime();
      localObject5 = new com/inmobi/commons/core/network/e;
      Object localObject6 = e.b(a);
      ((com.inmobi.commons.core.network.e)localObject5).<init>((c)localObject6);
      localObject5 = ((com.inmobi.commons.core.network.e)localObject5).a();
      try
      {
        localObject6 = o.a();
        localObject8 = a;
        localObject8 = e.b((e)localObject8);
        long l2 = ((c)localObject8).e();
        ((o)localObject6).a(l2);
        localObject6 = o.a();
        l2 = ((com.inmobi.commons.core.network.d)localObject5).c();
        ((o)localObject6).b(l2);
        localObject6 = o.a();
        l2 = SystemClock.elapsedRealtime() - l1;
        ((o)localObject6).c(l2);
      }
      catch (Exception localException3)
      {
        e.a();
        localException3.getMessage();
      }
      bool = ((com.inmobi.commons.core.network.d)localObject5).a();
      if (!bool) {
        break;
      }
      e.a();
      i += 1;
      localObject4 = a;
      j = e.a((e)localObject4);
      if (i > j) {
        return;
      }
      try
      {
        localObject4 = a;
        j = e.c((e)localObject4) * 1000;
        l1 = j;
        Thread.sleep(l1);
      }
      catch (InterruptedException localInterruptedException)
      {
        e.a();
      }
    }
    localObject3 = new com/inmobi/rendering/mraid/d;
    ((d)localObject3).<init>();
    Object localObject7 = d;
    Object localObject8 = "Content-Encoding";
    localObject7 = (List)((Map)localObject7).get(localObject8);
    label382:
    long l3;
    long l4;
    long l5;
    String str;
    if (localObject7 != null)
    {
      localObject7 = (String)((List)localObject7).get(0);
      localObject8 = "gzip";
      bool = ((String)localObject7).equals(localObject8);
      if (bool)
      {
        e.a();
        localObject7 = a;
        if (localObject7 != null)
        {
          localObject7 = a;
          int k = localObject7.length;
          if (k != 0)
          {
            k = a.length;
            localObject7 = new byte[k];
            localObject8 = a;
            byte[] arrayOfByte = a;
            int m = arrayOfByte.length;
            System.arraycopy(localObject8, 0, localObject7, 0, m);
            break label382;
          }
        }
        localObject7 = new byte[0];
        localObject1 = com.inmobi.commons.core.utilities.d.a((byte[])localObject7);
        if (localObject1 != null) {
          try
          {
            localObject7 = new java/lang/String;
            localObject8 = "UTF-8";
            ((String)localObject7).<init>((byte[])localObject1, (String)localObject8);
            ((d)localObject3).a((String)localObject7);
            e.a();
            try
            {
              localObject1 = new java/util/HashMap;
              ((HashMap)localObject1).<init>();
              localObject3 = "url";
              localObject7 = a;
              localObject7 = e.d((e)localObject7);
              ((Map)localObject1).put(localObject3, localObject7);
              localObject3 = "latency";
              l3 = SystemClock.elapsedRealtime() - l1;
              localObject4 = Long.valueOf(l3);
              ((Map)localObject1).put(localObject3, localObject4);
              localObject3 = a;
              localObject3 = e.b((e)localObject3);
              l4 = ((c)localObject3).e();
              l5 = ((com.inmobi.commons.core.network.d)localObject5).c();
              l4 += l5;
              str = "payloadSize";
              localObject3 = Long.valueOf(l4);
              ((Map)localObject1).put(str, localObject3);
              b.a();
              localObject3 = "ads";
              localObject4 = "MraidFetchLatency";
              b.a((String)localObject3, (String)localObject4, (Map)localObject1);
              return;
            }
            catch (Exception localException1)
            {
              e.a();
              localException1.getMessage();
              return;
            }
            return;
          }
          catch (UnsupportedEncodingException localUnsupportedEncodingException)
          {
            e.a();
            e.a();
            localUnsupportedEncodingException.getMessage();
          }
        }
      }
    }
    Object localObject2 = ((com.inmobi.commons.core.network.d)localObject5).b();
    ((d)localObject3).a((String)localObject2);
    e.a();
    try
    {
      localObject2 = new java/util/HashMap;
      ((HashMap)localObject2).<init>();
      localObject3 = "url";
      localObject7 = a;
      localObject7 = e.d((e)localObject7);
      ((Map)localObject2).put(localObject3, localObject7);
      localObject3 = "latency";
      l3 = SystemClock.elapsedRealtime() - l1;
      localObject4 = Long.valueOf(l3);
      ((Map)localObject2).put(localObject3, localObject4);
      localObject3 = a;
      localObject3 = e.b((e)localObject3);
      l4 = ((c)localObject3).e();
      l5 = ((com.inmobi.commons.core.network.d)localObject5).c();
      l4 += l5;
      str = "payloadSize";
      localObject3 = Long.valueOf(l4);
      ((Map)localObject2).put(str, localObject3);
      b.a();
      localObject3 = "ads";
      localObject4 = "MraidFetchLatency";
      b.a((String)localObject3, (String)localObject4, (Map)localObject2);
      return;
    }
    catch (Exception localException2)
    {
      e.a();
      localException2.getMessage();
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.rendering.mraid.e.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */