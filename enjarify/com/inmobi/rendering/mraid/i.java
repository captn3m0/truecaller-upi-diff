package com.inmobi.rendering.mraid;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Vibrator;
import com.inmobi.rendering.RenderView;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class i
{
  private static final String b = "i";
  public RenderView a;
  private i.a c;
  
  public i(RenderView paramRenderView)
  {
    a = paramRenderView;
    Object localObject = new android/os/HandlerThread;
    ((HandlerThread)localObject).<init>("SystemTasksHandlerThread");
    ((HandlerThread)localObject).start();
    i.a locala = new com/inmobi/rendering/mraid/i$a;
    localObject = ((HandlerThread)localObject).getLooper();
    locala.<init>((Looper)localObject, paramRenderView);
    c = locala;
  }
  
  public static String a(String paramString)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    int i = paramString.length();
    if (i != 0) {
      try
      {
        Object localObject1 = new org/json/JSONObject;
        ((JSONObject)localObject1).<init>(paramString);
        paramString = "frequency";
        paramString = ((JSONObject)localObject1).optString(paramString);
        if (paramString != null)
        {
          Object localObject2 = "";
          boolean bool1 = ((String)localObject2).equals(paramString);
          if (!bool1)
          {
            localObject2 = "daily";
            bool1 = ((String)localObject2).equals(paramString);
            if (!bool1)
            {
              localObject2 = "weekly";
              bool1 = ((String)localObject2).equals(paramString);
              if (!bool1)
              {
                localObject2 = "monthly";
                bool1 = ((String)localObject2).equals(paramString);
                if (!bool1)
                {
                  localObject2 = "yearly";
                  bool1 = ((String)localObject2).equals(paramString);
                  if (!bool1) {
                    return "";
                  }
                }
              }
            }
            localObject2 = "freq=";
            localStringBuilder.append((String)localObject2);
            localStringBuilder.append(paramString);
            localObject2 = ";";
            localStringBuilder.append((String)localObject2);
            localObject2 = "interval";
            localObject2 = ((JSONObject)localObject1).optString((String)localObject2);
            String str1;
            if (localObject2 != null)
            {
              str1 = "";
              boolean bool3 = str1.equals(localObject2);
              if (!bool3)
              {
                str1 = "interval=";
                localStringBuilder.append(str1);
                int j = Integer.parseInt((String)localObject2);
                localStringBuilder.append(j);
                localObject2 = ";";
                localStringBuilder.append((String)localObject2);
              }
            }
            localObject2 = "expires";
            localObject2 = ((JSONObject)localObject1).optString((String)localObject2);
            localObject2 = a.a((String)localObject2);
            if (localObject2 != null)
            {
              str1 = "until=";
              localStringBuilder.append(str1);
              str1 = "+";
              String str2 = "Z+";
              localObject2 = ((String)localObject2).replace(str1, str2);
              str1 = "-";
              str2 = "Z-";
              localObject2 = ((String)localObject2).replace(str1, str2);
              localStringBuilder.append((String)localObject2);
              localObject2 = ";";
              localStringBuilder.append((String)localObject2);
            }
            localObject2 = "weekly";
            boolean bool2 = paramString.equals(localObject2);
            if (bool2)
            {
              localObject2 = "daysInWeek";
              localObject2 = ((JSONObject)localObject1).optJSONArray((String)localObject2);
              localObject2 = a.a((JSONArray)localObject2);
              if (localObject2 != null)
              {
                str1 = "byday=";
                localStringBuilder.append(str1);
                localStringBuilder.append((String)localObject2);
                localObject2 = ";";
                localStringBuilder.append((String)localObject2);
              }
            }
            localObject2 = "monthly";
            bool2 = paramString.equals(localObject2);
            int m;
            int n;
            if (bool2)
            {
              localObject2 = "daysInMonth";
              localObject2 = ((JSONObject)localObject1).optJSONArray((String)localObject2);
              m = -31;
              n = 31;
              localObject2 = a.a((JSONArray)localObject2, m, n);
              if (localObject2 != null)
              {
                str1 = "bymonthday=";
                localStringBuilder.append(str1);
                localStringBuilder.append((String)localObject2);
                localObject2 = ";";
                localStringBuilder.append((String)localObject2);
              }
            }
            localObject2 = "yearly";
            bool2 = paramString.equals(localObject2);
            if (bool2)
            {
              localObject2 = "daysInYear";
              localObject2 = ((JSONObject)localObject1).optJSONArray((String)localObject2);
              m = 65170;
              n = 366;
              localObject2 = a.a((JSONArray)localObject2, m, n);
              if (localObject2 != null)
              {
                str1 = "byyearday=";
                localStringBuilder.append(str1);
                localStringBuilder.append((String)localObject2);
                localObject2 = ";";
                localStringBuilder.append((String)localObject2);
              }
            }
            localObject2 = "monthly";
            bool2 = paramString.equals(localObject2);
            if (bool2)
            {
              localObject2 = "weeksInMonth";
              localObject2 = ((JSONObject)localObject1).optJSONArray((String)localObject2);
              m = -4;
              n = 4;
              localObject2 = a.a((JSONArray)localObject2, m, n);
              if (localObject2 != null)
              {
                str1 = "byweekno=";
                localStringBuilder.append(str1);
                localStringBuilder.append((String)localObject2);
                localObject2 = ";";
                localStringBuilder.append((String)localObject2);
              }
            }
            localObject2 = "yearly";
            boolean bool4 = paramString.equals(localObject2);
            if (bool4)
            {
              paramString = "monthsInYear";
              paramString = ((JSONObject)localObject1).optJSONArray(paramString);
              i = 1;
              int k = 12;
              paramString = a.a(paramString, i, k);
              if (paramString != null)
              {
                localObject1 = "bymonth=";
                localStringBuilder.append((String)localObject1);
                localStringBuilder.append(paramString);
                paramString = ";";
                localStringBuilder.append(paramString);
              }
            }
            return localStringBuilder.toString();
          }
        }
        return "";
      }
      catch (JSONException localJSONException)
      {
        return "";
      }
    }
    return "";
  }
  
  public static void a(Context paramContext, int paramInt, String paramString1, String paramString2, String paramString3)
  {
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>();
    ((StringBuilder)localObject1).append(paramString1);
    ((StringBuilder)localObject1).append(" ");
    ((StringBuilder)localObject1).append(paramString2);
    String str = " ";
    ((StringBuilder)localObject1).append(str);
    ((StringBuilder)localObject1).append(paramString3);
    localObject1 = ((StringBuilder)localObject1).toString();
    switch (paramInt)
    {
    default: 
      paramInt = 0;
      localObject2 = null;
      break;
    case 3: 
    case 2: 
    case 1: 
      try
      {
        localObject2 = new java/lang/StringBuilder;
        paramString1 = "http://twitter.com/home?status=";
        ((StringBuilder)localObject2).<init>(paramString1);
        paramString1 = "UTF-8";
        paramString1 = URLEncoder.encode((String)localObject1, paramString1);
        ((StringBuilder)localObject2).append(paramString1);
        localObject2 = ((StringBuilder)localObject2).toString();
      }
      catch (UnsupportedEncodingException localUnsupportedEncodingException)
      {
        return;
      }
      localObject2 = new java/lang/StringBuilder;
      paramString1 = "https://m.google.com/app/plus/x/?v=compose&content=";
      ((StringBuilder)localObject2).<init>(paramString1);
      paramString1 = "UTF-8";
      paramString1 = URLEncoder.encode((String)localObject1, paramString1);
      ((StringBuilder)localObject2).append(paramString1);
      localObject2 = ((StringBuilder)localObject2).toString();
      break;
      localObject2 = new java/lang/StringBuilder;
      str = "https://www.facebook.com/dialog/feed?app_id=181821551957328&link=";
      ((StringBuilder)localObject2).<init>(str);
      str = "UTF-8";
      str = URLEncoder.encode(paramString2, str);
      ((StringBuilder)localObject2).append(str);
      str = "&picture=";
      ((StringBuilder)localObject2).append(str);
      str = "UTF-8";
      paramString3 = URLEncoder.encode(paramString3, str);
      ((StringBuilder)localObject2).append(paramString3);
      paramString3 = "&name=&description=";
      ((StringBuilder)localObject2).append(paramString3);
      paramString3 = "UTF-8";
      paramString1 = URLEncoder.encode(paramString1, paramString3);
      ((StringBuilder)localObject2).append(paramString1);
      paramString1 = "&redirect_uri=";
      ((StringBuilder)localObject2).append(paramString1);
      paramString1 = "UTF-8";
      paramString1 = URLEncoder.encode(paramString2, paramString1);
      ((StringBuilder)localObject2).append(paramString1);
      localObject2 = ((StringBuilder)localObject2).toString();
      break;
    }
    if (localObject2 != null)
    {
      paramString1 = new android/content/Intent;
      paramString2 = "android.intent.action.VIEW";
      paramString1.<init>(paramString2);
      localObject2 = Uri.parse((String)localObject2);
      paramString1.setData((Uri)localObject2);
      try
      {
        com.inmobi.commons.a.a.a(paramContext, paramString1);
        return;
      }
      catch (ActivityNotFoundException localActivityNotFoundException1)
      {
        localActivityNotFoundException1.getMessage();
        return;
      }
    }
    Object localObject2 = new android/content/Intent;
    ((Intent)localObject2).<init>();
    ((Intent)localObject2).setType("text/plain");
    paramString1 = "android.intent.extra.TEXT";
    ((Intent)localObject2).putExtra(paramString1, (String)localObject1);
    try
    {
      com.inmobi.commons.a.a.a(paramContext, (Intent)localObject2);
      return;
    }
    catch (ActivityNotFoundException localActivityNotFoundException2)
    {
      localActivityNotFoundException2.getMessage();
    }
  }
  
  public final void a(Context paramContext)
  {
    Object localObject = c;
    if (localObject != null)
    {
      int i = 1;
      boolean bool = ((i.a)localObject).hasMessages(i);
      if (bool)
      {
        c.removeMessages(i);
        localObject = "vibrator";
        paramContext = (Vibrator)paramContext.getSystemService((String)localObject);
        paramContext.cancel();
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.rendering.mraid.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */