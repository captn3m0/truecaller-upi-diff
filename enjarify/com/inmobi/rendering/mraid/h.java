package com.inmobi.rendering.mraid;

import org.json.JSONException;
import org.json.JSONObject;

public class h
{
  private static final String g = "h";
  String a = "top-right";
  int b;
  int c;
  int d = 0;
  int e = 0;
  boolean f = true;
  
  public static h a(String paramString, h paramh)
  {
    h localh = new com/inmobi/rendering/mraid/h;
    localh.<init>();
    try
    {
      JSONObject localJSONObject = new org/json/JSONObject;
      localJSONObject.<init>(paramString);
      paramString = "width";
      int i = localJSONObject.getInt(paramString);
      b = i;
      paramString = "height";
      i = localJSONObject.getInt(paramString);
      c = i;
      paramString = "offsetX";
      i = localJSONObject.getInt(paramString);
      d = i;
      paramString = "offsetY";
      i = localJSONObject.getInt(paramString);
      e = i;
      if (paramh != null)
      {
        paramString = "customClosePosition";
        String str = a;
        paramString = localJSONObject.optString(paramString, str);
        a = paramString;
        paramString = "allowOffscreen";
        boolean bool2 = f;
        boolean bool1 = localJSONObject.optBoolean(paramString, bool2);
        f = bool1;
      }
    }
    catch (JSONException localJSONException)
    {
      localh = null;
    }
    return localh;
  }
  
  public final String a()
  {
    String str1 = "";
    try
    {
      JSONObject localJSONObject = new org/json/JSONObject;
      localJSONObject.<init>();
      String str2 = "width";
      int i = b;
      localJSONObject.put(str2, i);
      str2 = "height";
      i = c;
      localJSONObject.put(str2, i);
      str2 = "customClosePosition";
      String str3 = a;
      localJSONObject.put(str2, str3);
      str2 = "offsetX";
      i = d;
      localJSONObject.put(str2, i);
      str2 = "offsetY";
      i = e;
      localJSONObject.put(str2, i);
      str2 = "allowOffscreen";
      boolean bool = f;
      localJSONObject.put(str2, bool);
      str1 = localJSONObject.toString();
    }
    catch (JSONException localJSONException)
    {
      for (;;) {}
    }
    return str1;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.rendering.mraid.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */