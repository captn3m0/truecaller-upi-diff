package com.inmobi.rendering.mraid;

import android.content.ContentResolver;
import android.content.Context;
import android.media.AudioManager;
import com.inmobi.commons.a.a;
import com.inmobi.rendering.RenderView;

public final class MraidMediaProcessor
{
  private static final String f = "MraidMediaProcessor";
  public RenderView a;
  public MediaRenderView b;
  public MraidMediaProcessor.RingerModeChangeReceiver c;
  public MraidMediaProcessor.a d;
  public MraidMediaProcessor.HeadphonesPluggedChangeReceiver e;
  
  public MraidMediaProcessor(RenderView paramRenderView)
  {
    a = paramRenderView;
  }
  
  public static boolean a()
  {
    Object localObject = a.b();
    if (localObject == null) {
      return false;
    }
    String str = "audio";
    localObject = (AudioManager)((Context)localObject).getSystemService(str);
    int i = 2;
    int j = ((AudioManager)localObject).getRingerMode();
    return i != j;
  }
  
  public static boolean d()
  {
    Context localContext = a.b();
    if (localContext == null) {
      return false;
    }
    return ((AudioManager)localContext.getSystemService("audio")).isWiredHeadsetOn();
  }
  
  public final void b()
  {
    Context localContext = a.b();
    if (localContext == null) {
      return;
    }
    MraidMediaProcessor.RingerModeChangeReceiver localRingerModeChangeReceiver = c;
    if (localRingerModeChangeReceiver != null)
    {
      localContext.unregisterReceiver(localRingerModeChangeReceiver);
      localContext = null;
      c = null;
    }
  }
  
  public final void c()
  {
    Object localObject = a.b();
    if (localObject == null) {
      return;
    }
    MraidMediaProcessor.a locala = d;
    if (locala != null)
    {
      localObject = ((Context)localObject).getContentResolver();
      locala = d;
      ((ContentResolver)localObject).unregisterContentObserver(locala);
      localObject = null;
      d = null;
    }
  }
  
  public final void e()
  {
    Context localContext = a.b();
    if (localContext == null) {
      return;
    }
    MraidMediaProcessor.HeadphonesPluggedChangeReceiver localHeadphonesPluggedChangeReceiver = e;
    if (localHeadphonesPluggedChangeReceiver != null)
    {
      localContext.unregisterReceiver(localHeadphonesPluggedChangeReceiver);
      localContext = null;
      e = null;
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.rendering.mraid.MraidMediaProcessor
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */