package com.inmobi.rendering.mraid;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public final class MraidMediaProcessor$RingerModeChangeReceiver
  extends BroadcastReceiver
{
  private String b;
  
  public MraidMediaProcessor$RingerModeChangeReceiver(MraidMediaProcessor paramMraidMediaProcessor, String paramString)
  {
    b = paramString;
  }
  
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    if (paramIntent != null)
    {
      paramContext = "android.media.RINGER_MODE_CHANGED";
      String str1 = paramIntent.getAction();
      boolean bool = paramContext.equals(str1);
      if (bool)
      {
        paramContext = "android.media.EXTRA_RINGER_MODE";
        int j = 2;
        int i = paramIntent.getIntExtra(paramContext, j);
        MraidMediaProcessor.f();
        paramIntent = a;
        String str2 = b;
        if (j != i)
        {
          i = 1;
        }
        else
        {
          i = 0;
          paramContext = null;
        }
        MraidMediaProcessor.a(paramIntent, str2, i);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.rendering.mraid.MraidMediaProcessor.RingerModeChangeReceiver
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */