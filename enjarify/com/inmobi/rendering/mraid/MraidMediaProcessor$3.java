package com.inmobi.rendering.mraid;

import android.view.View;
import android.view.ViewGroup;
import com.inmobi.rendering.RenderView;

public final class MraidMediaProcessor$3
  implements MediaRenderView.a
{
  public MraidMediaProcessor$3(MraidMediaProcessor paramMraidMediaProcessor) {}
  
  public final void a()
  {
    MraidMediaProcessor.f();
  }
  
  public final void a(MediaRenderView paramMediaRenderView)
  {
    MraidMediaProcessor.f();
    Object localObject = MraidMediaProcessor.b(a);
    ViewGroup localViewGroup = null;
    ((RenderView)localObject).setAdActiveFlag(false);
    localObject = c;
    if (localObject != null)
    {
      localViewGroup = (ViewGroup)((ViewGroup)localObject).getParent();
      localViewGroup.removeView((View)localObject);
    }
    c = null;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.rendering.mraid.MraidMediaProcessor.3
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */