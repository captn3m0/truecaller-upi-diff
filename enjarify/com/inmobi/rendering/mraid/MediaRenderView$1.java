package com.inmobi.rendering.mraid;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnVideoSizeChangedListener;
import android.view.View;
import android.widget.MediaController;

final class MediaRenderView$1
  implements MediaPlayer.OnVideoSizeChangedListener
{
  MediaRenderView$1(MediaRenderView paramMediaRenderView) {}
  
  public final void onVideoSizeChanged(MediaPlayer paramMediaPlayer, int paramInt1, int paramInt2)
  {
    MediaRenderView.b();
    paramMediaPlayer = MediaRenderView.a(a);
    if (paramMediaPlayer == null)
    {
      paramMediaPlayer = a;
      Object localObject = new com/inmobi/rendering/mraid/MediaRenderView$CustomMediaController;
      Context localContext = paramMediaPlayer.getContext();
      ((MediaRenderView.CustomMediaController)localObject).<init>(localContext);
      MediaRenderView.a(paramMediaPlayer, (MediaRenderView.CustomMediaController)localObject);
      paramMediaPlayer = MediaRenderView.a(a);
      localObject = a;
      paramMediaPlayer.setAnchorView((View)localObject);
      paramMediaPlayer = a;
      localObject = MediaRenderView.a(paramMediaPlayer);
      paramMediaPlayer.setMediaController((MediaController)localObject);
      a.requestLayout();
      paramMediaPlayer = a;
      paramMediaPlayer.requestFocus();
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.rendering.mraid.MediaRenderView.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */