package com.inmobi.rendering.mraid;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.provider.CalendarContract.Events;
import android.provider.CalendarContract.Reminders;
import com.inmobi.rendering.InMobiAdActivity.a;
import com.inmobi.rendering.RenderView;
import java.util.Calendar;
import java.util.GregorianCalendar;

public final class i$1
  implements InMobiAdActivity.a
{
  public i$1(i parami, Context paramContext, int paramInt, String paramString1, String paramString2, String paramString3, String paramString4) {}
  
  public final void a()
  {
    i.a();
    Object localObject1 = a;
    int i = a.a((Context)localObject1);
    int k = b;
    if (k == i)
    {
      i.a();
      return;
    }
    localObject1 = new android/content/ContentValues;
    ((ContentValues)localObject1).<init>();
    Object localObject2 = c;
    int m = -1;
    int i1 = ((String)localObject2).hashCode();
    int i2 = -1320822226;
    int i3 = 2;
    int i4 = 1;
    boolean bool2;
    if (i1 != i2)
    {
      i2 = -804109473;
      if (i1 != i2)
      {
        i2 = 476588369;
        if (i1 == i2)
        {
          str = "cancelled";
          bool2 = ((String)localObject2).equals(str);
          if (bool2) {
            m = 2;
          }
        }
      }
      else
      {
        str = "confirmed";
        bool2 = ((String)localObject2).equals(str);
        if (bool2) {
          m = 1;
        }
      }
    }
    else
    {
      str = "tentative";
      bool2 = ((String)localObject2).equals(str);
      if (bool2)
      {
        m = 0;
        localObject3 = null;
      }
    }
    switch (m)
    {
    default: 
      break;
    case 2: 
      localObject2 = "eventStatus";
      localObject3 = Integer.valueOf(i3);
      ((ContentValues)localObject1).put((String)localObject2, (Integer)localObject3);
      break;
    case 1: 
      localObject2 = "eventStatus";
      localObject3 = Integer.valueOf(i4);
      ((ContentValues)localObject1).put((String)localObject2, (Integer)localObject3);
      break;
    case 0: 
      localObject2 = "eventStatus";
      localObject3 = Integer.valueOf(0);
      ((ContentValues)localObject1).put((String)localObject2, (Integer)localObject3);
    }
    localObject2 = a.getContentResolver();
    Object localObject3 = CalendarContract.Events.CONTENT_URI;
    long l1 = a.a(a);
    localObject3 = ContentUris.withAppendedId((Uri)localObject3, l1);
    i1 = 0;
    String str = null;
    ((ContentResolver)localObject2).update((Uri)localObject3, (ContentValues)localObject1, null, null);
    localObject1 = d;
    if (localObject1 != null)
    {
      localObject3 = "";
      boolean bool1 = ((String)localObject3).equals(localObject1);
      if (!bool1)
      {
        int j = 60000;
        int n;
        try
        {
          localObject3 = d;
          str = "+";
          boolean bool3 = ((String)localObject3).startsWith(str);
          if (bool3)
          {
            localObject3 = d;
            localObject3 = ((String)localObject3).substring(i4);
            n = Integer.parseInt((String)localObject3);
            n /= j;
          }
          else
          {
            localObject3 = d;
            n = Integer.parseInt((String)localObject3);
            n /= j;
          }
        }
        catch (NumberFormatException localNumberFormatException)
        {
          localObject3 = a.b(d);
          if (localObject3 == null)
          {
            i.a();
            return;
          }
          long l2 = ((Calendar)localObject3).getTimeInMillis();
          localObject4 = a.b(e);
          long l3 = ((GregorianCalendar)localObject4).getTimeInMillis();
          l2 -= l3;
          i1 = (int)l2;
          n = i1 / j;
          if (n > 0)
          {
            localObject1 = i.a(g);
            localObject2 = f;
            ((RenderView)localObject1).b((String)localObject2, "Reminder format is incorrect. Reminder can be set only before the event starts", "createCalendarEvent");
            return;
          }
        }
        j = -n;
        localObject3 = CalendarContract.Reminders.CONTENT_URI;
        str = "event_id=?";
        Object localObject4 = new String[i4];
        Object localObject5 = new java/lang/StringBuilder;
        ((StringBuilder)localObject5).<init>();
        Context localContext = a;
        int i5 = a.a(localContext);
        ((StringBuilder)localObject5).append(i5);
        localObject5 = ((StringBuilder)localObject5).toString();
        localObject4[0] = localObject5;
        ((ContentResolver)localObject2).delete((Uri)localObject3, str, (String[])localObject4);
        if (j < 0)
        {
          localObject1 = i.a(g);
          localObject2 = f;
          ((RenderView)localObject1).b((String)localObject2, "Reminder format is incorrect. Reminder can be set only before the event starts", "createCalendarEvent");
          return;
        }
        localObject3 = new android/content/ContentValues;
        ((ContentValues)localObject3).<init>();
        i2 = a.a(a);
        localObject4 = Integer.valueOf(i2);
        ((ContentValues)localObject3).put("event_id", (Integer)localObject4);
        localObject4 = Integer.valueOf(i4);
        ((ContentValues)localObject3).put("method", (Integer)localObject4);
        str = "minutes";
        localObject1 = Integer.valueOf(j);
        ((ContentValues)localObject3).put(str, (Integer)localObject1);
        localObject1 = CalendarContract.Reminders.CONTENT_URI;
        ((ContentResolver)localObject2).insert((Uri)localObject1, (ContentValues)localObject3);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.rendering.mraid.i.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */