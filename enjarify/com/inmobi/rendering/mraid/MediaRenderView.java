package com.inmobi.rendering.mraid;

import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup;
import android.widget.VideoView;
import com.inmobi.commons.a.a;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public final class MediaRenderView
  extends VideoView
  implements Application.ActivityLifecycleCallbacks, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener
{
  private static final String l = "MediaRenderView";
  public MediaRenderView.CustomMediaController a;
  public Bitmap b;
  public ViewGroup c;
  public MediaRenderView.a d;
  int e;
  boolean f;
  public String g;
  public String h;
  boolean i;
  int j;
  int k;
  private boolean m = false;
  private MediaPlayer n;
  private WeakReference o;
  
  public MediaRenderView(Activity paramActivity)
  {
    super(paramActivity);
    boolean bool = true;
    setZOrderOnTop(bool);
    setFocusable(bool);
    setFocusableInTouchMode(bool);
    setDrawingCacheEnabled(bool);
    e = 100;
    j = -1;
    k = 0;
    f = false;
    WeakReference localWeakReference = new java/lang/ref/WeakReference;
    localWeakReference.<init>(paramActivity);
    o = localWeakReference;
    paramActivity.getApplication().registerActivityLifecycleCallbacks(this);
  }
  
  public static String a(String paramString)
  {
    String str1 = "";
    paramString = paramString.getBytes();
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    int i1 = paramString.length;
    int i2 = 0;
    while (i2 < i1)
    {
      int i3 = paramString[i2];
      int i4 = i3 & 0x80;
      if (i4 > 0)
      {
        ((StringBuilder)localObject).append("%");
        char[] arrayOfChar1 = new char[16];
        char[] tmp62_60 = arrayOfChar1;
        char[] tmp63_62 = tmp62_60;
        char[] tmp63_62 = tmp62_60;
        tmp63_62[0] = 48;
        tmp63_62[1] = 49;
        char[] tmp72_63 = tmp63_62;
        char[] tmp72_63 = tmp63_62;
        tmp72_63[2] = 50;
        tmp72_63[3] = 51;
        char[] tmp81_72 = tmp72_63;
        char[] tmp81_72 = tmp72_63;
        tmp81_72[4] = 52;
        tmp81_72[5] = 53;
        char[] tmp90_81 = tmp81_72;
        char[] tmp90_81 = tmp81_72;
        tmp90_81[6] = 54;
        tmp90_81[7] = 55;
        char[] tmp101_90 = tmp90_81;
        char[] tmp101_90 = tmp90_81;
        tmp101_90[8] = 56;
        tmp101_90[9] = 57;
        char[] tmp112_101 = tmp101_90;
        char[] tmp112_101 = tmp101_90;
        tmp112_101[10] = 97;
        tmp112_101[11] = 98;
        char[] tmp123_112 = tmp112_101;
        char[] tmp123_112 = tmp112_101;
        tmp123_112[12] = 99;
        tmp123_112[13] = 100;
        tmp123_112[14] = 101;
        tmp123_112[15] = 102;
        int i5 = 2;
        char[] arrayOfChar2 = new char[i5];
        int i6 = i3 >> 4 & 0xF;
        i6 = arrayOfChar1[i6];
        arrayOfChar2[0] = i6;
        i3 &= 0xF;
        i3 = arrayOfChar1[i3];
        i4 = 1;
        arrayOfChar2[i4] = i3;
        String str2 = new java/lang/String;
        str2.<init>(arrayOfChar2);
        ((StringBuilder)localObject).append(str2);
      }
      else
      {
        i3 = (char)i3;
        ((StringBuilder)localObject).append(i3);
      }
      i2 += 1;
    }
    try
    {
      paramString = new java/lang/String;
      localObject = ((StringBuilder)localObject).toString();
      localObject = ((String)localObject).getBytes();
      String str3 = "ISO-8859-1";
      paramString.<init>((byte[])localObject, str3);
      return paramString;
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException) {}
    return str1;
  }
  
  public static Bitmap b(String paramString)
  {
    Object localObject1 = "android.media.ThumbnailUtils";
    try
    {
      localObject1 = Class.forName((String)localObject1);
      Object localObject2 = "createVideoThumbnail";
      int i1 = 2;
      Class[] arrayOfClass = new Class[i1];
      Class localClass = String.class;
      arrayOfClass[0] = localClass;
      localClass = Integer.TYPE;
      int i2 = 1;
      arrayOfClass[i2] = localClass;
      localObject1 = ((Class)localObject1).getDeclaredMethod((String)localObject2, arrayOfClass);
      localObject2 = new Object[i1];
      localObject2[0] = paramString;
      paramString = Integer.valueOf(i2);
      localObject2[i2] = paramString;
      paramString = ((Method)localObject1).invoke(null, (Object[])localObject2);
      return (Bitmap)paramString;
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
      return null;
    }
    catch (NoSuchMethodException localNoSuchMethodException)
    {
      return null;
    }
    catch (InvocationTargetException localInvocationTargetException)
    {
      return null;
    }
    catch (ClassNotFoundException localClassNotFoundException) {}
    return null;
  }
  
  public final void a()
  {
    stopPlayback();
    Object localObject = c;
    if (localObject != null)
    {
      localObject = (ViewGroup)((ViewGroup)localObject).getParent();
      if (localObject != null)
      {
        ViewGroup localViewGroup = c;
        ((ViewGroup)localObject).removeView(localViewGroup);
      }
      localObject = (ViewGroup)getParent();
      if (localObject != null) {
        ((ViewGroup)localObject).removeView(this);
      }
      localObject = null;
      setBackgroundColor(0);
      c = null;
    }
    super.setMediaController(null);
    a = null;
    localObject = d;
    if (localObject != null) {
      ((MediaRenderView.a)localObject).a(this);
    }
  }
  
  public final ViewGroup getViewContainer()
  {
    return c;
  }
  
  public final void onActivityCreated(Activity paramActivity, Bundle paramBundle) {}
  
  public final void onActivityDestroyed(Activity paramActivity)
  {
    paramActivity.getApplication().unregisterActivityLifecycleCallbacks(this);
  }
  
  public final void onActivityPaused(Activity paramActivity) {}
  
  public final void onActivityResumed(Activity paramActivity) {}
  
  public final void onActivitySaveInstanceState(Activity paramActivity, Bundle paramBundle) {}
  
  public final void onActivityStarted(Activity paramActivity)
  {
    Object localObject = o.get();
    if (localObject != null)
    {
      localObject = (Activity)o.get();
      boolean bool = localObject.equals(paramActivity);
      if (bool)
      {
        bool = false;
        paramActivity = null;
        m = false;
        start();
      }
    }
  }
  
  public final void onActivityStopped(Activity paramActivity)
  {
    Activity localActivity = (Activity)o.get();
    if (localActivity != null)
    {
      boolean bool = localActivity.equals(paramActivity);
      if (bool)
      {
        m = true;
        int i1 = getCurrentPosition();
        if (i1 != 0)
        {
          i1 = getCurrentPosition();
          k = i1;
        }
        pause();
      }
    }
  }
  
  public final void onCompletion(MediaPlayer paramMediaPlayer) {}
  
  public final boolean onError(MediaPlayer paramMediaPlayer, int paramInt1, int paramInt2)
  {
    a();
    return false;
  }
  
  protected final void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
    getHolder().setSizeFromLayout();
  }
  
  public final void onPrepared(MediaPlayer paramMediaPlayer)
  {
    n = paramMediaPlayer;
    MediaRenderView.1 local1 = new com/inmobi/rendering/mraid/MediaRenderView$1;
    local1.<init>(this);
    paramMediaPlayer.setOnVideoSizeChangedListener(local1);
    int i1 = k;
    int i2 = getDuration();
    if (i1 < i2)
    {
      k = i1;
      seekTo(i1);
    }
    i = true;
    d.a();
    start();
  }
  
  protected final void onVisibilityChanged(View paramView, int paramInt)
  {
    super.onVisibilityChanged(paramView, paramInt);
    if (paramInt == 0)
    {
      int i1 = Build.VERSION.SDK_INT;
      paramInt = 16;
      if (i1 >= paramInt)
      {
        paramView = a.b();
        if (paramView != null)
        {
          localObject = new android/graphics/drawable/BitmapDrawable;
          paramView = paramView.getResources();
          Bitmap localBitmap = b;
          ((BitmapDrawable)localObject).<init>(paramView, localBitmap);
          setBackground((Drawable)localObject);
        }
        return;
      }
      paramView = new android/graphics/drawable/BitmapDrawable;
      Object localObject = b;
      paramView.<init>((Bitmap)localObject);
      setBackgroundDrawable(paramView);
    }
  }
  
  protected final void onWindowVisibilityChanged(int paramInt)
  {
    super.onWindowVisibilityChanged(paramInt);
  }
  
  public final void pause()
  {
    super.pause();
  }
  
  public final void setListener(MediaRenderView.a parama)
  {
    d = parama;
  }
  
  public final void setPlaybackData(String paramString)
  {
    paramString = a(paramString);
    h = paramString;
    g = "anonymous";
    paramString = b;
    if (paramString == null)
    {
      paramString = Bitmap.Config.ARGB_8888;
      int i1 = 24;
      paramString = Bitmap.createBitmap(i1, i1, paramString);
      b = paramString;
      paramString = b(h);
      b = paramString;
    }
  }
  
  public final void setViewContainer(ViewGroup paramViewGroup)
  {
    c = paramViewGroup;
  }
  
  public final void start()
  {
    boolean bool = m;
    if (bool) {
      return;
    }
    super.start();
  }
}

/* Location:
 * Qualified Name:     com.inmobi.rendering.mraid.MediaRenderView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */