package com.inmobi.rendering.mraid;

import org.json.JSONException;
import org.json.JSONObject;

public class g
{
  private static String e = "g";
  public boolean a = true;
  public String b = "none";
  public String c = "right";
  public String d = null;
  
  public static g a(String paramString, g paramg)
  {
    g localg = new com/inmobi/rendering/mraid/g;
    localg.<init>();
    d = paramString;
    try
    {
      JSONObject localJSONObject = new org/json/JSONObject;
      localJSONObject.<init>(paramString);
      paramString = "forceOrientation";
      String str = b;
      paramString = localJSONObject.optString(paramString, str);
      b = paramString;
      paramString = "allowOrientationChange";
      boolean bool1 = a;
      boolean bool2 = localJSONObject.optBoolean(paramString, bool1);
      a = bool2;
      paramString = "direction";
      paramg = c;
      paramString = localJSONObject.optString(paramString, paramg);
      c = paramString;
      paramString = b;
      paramg = "portrait";
      bool2 = paramString.equals(paramg);
      if (!bool2)
      {
        paramString = b;
        paramg = "landscape";
        bool2 = paramString.equals(paramg);
        if (!bool2)
        {
          paramString = "none";
          b = paramString;
        }
      }
      paramString = c;
      paramg = "left";
      bool2 = paramString.equals(paramg);
      if (!bool2)
      {
        paramString = c;
        paramg = "right";
        bool2 = paramString.equals(paramg);
        if (!bool2)
        {
          paramString = "right";
          c = paramString;
        }
      }
    }
    catch (JSONException localJSONException)
    {
      localg = null;
    }
    return localg;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.rendering.mraid.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */