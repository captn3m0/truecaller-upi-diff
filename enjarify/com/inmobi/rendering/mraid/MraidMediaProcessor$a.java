package com.inmobi.rendering.mraid;

import android.content.Context;
import android.database.ContentObserver;
import android.media.AudioManager;
import android.os.Handler;

public final class MraidMediaProcessor$a
  extends ContentObserver
{
  private Context b;
  private int c;
  private String d;
  
  public MraidMediaProcessor$a(MraidMediaProcessor paramMraidMediaProcessor, String paramString, Context paramContext, Handler paramHandler)
  {
    super(paramHandler);
    d = paramString;
    b = paramContext;
    c = -1;
  }
  
  public final void onChange(boolean paramBoolean)
  {
    super.onChange(paramBoolean);
    Object localObject1 = b;
    if (localObject1 != null)
    {
      Object localObject2 = "audio";
      localObject1 = (AudioManager)((Context)localObject1).getSystemService((String)localObject2);
      paramBoolean = ((AudioManager)localObject1).getStreamVolume(3);
      boolean bool = c;
      if (paramBoolean != bool)
      {
        c = paramBoolean;
        localObject2 = a;
        String str = d;
        MraidMediaProcessor.a((MraidMediaProcessor)localObject2, str, paramBoolean);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.rendering.mraid.MraidMediaProcessor.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */