package com.inmobi.rendering.mraid;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.inmobi.rendering.RenderView;
import java.lang.ref.WeakReference;

final class i$a
  extends Handler
{
  private static final String a = "i$a";
  private WeakReference b;
  
  public i$a(Looper paramLooper, RenderView paramRenderView)
  {
    super(paramLooper);
    paramLooper = new java/lang/ref/WeakReference;
    paramLooper.<init>(paramRenderView);
    b = paramLooper;
  }
  
  public final void handleMessage(Message paramMessage)
  {
    int i = what;
    int j = 1;
    if (i == j)
    {
      paramMessage = (String)obj;
      RenderView localRenderView = (RenderView)b.get();
      if (localRenderView != null)
      {
        String str = "broadcastEvent('vibrateComplete');";
        localRenderView.a(paramMessage, str);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.rendering.mraid.i.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */