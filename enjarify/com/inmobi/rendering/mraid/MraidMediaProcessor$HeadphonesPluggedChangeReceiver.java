package com.inmobi.rendering.mraid;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public final class MraidMediaProcessor$HeadphonesPluggedChangeReceiver
  extends BroadcastReceiver
{
  private String b;
  
  public MraidMediaProcessor$HeadphonesPluggedChangeReceiver(MraidMediaProcessor paramMraidMediaProcessor, String paramString)
  {
    b = paramString;
  }
  
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    if (paramIntent != null)
    {
      paramContext = "android.intent.action.HEADSET_PLUG";
      String str1 = paramIntent.getAction();
      boolean bool1 = paramContext.equals(str1);
      if (bool1)
      {
        paramContext = "state";
        boolean bool2 = false;
        str1 = null;
        int i = paramIntent.getIntExtra(paramContext, 0);
        MraidMediaProcessor.f();
        paramIntent = a;
        String str2 = b;
        int j = 1;
        if (j == i) {
          bool2 = true;
        }
        MraidMediaProcessor.b(paramIntent, str2, bool2);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.rendering.mraid.MraidMediaProcessor.HeadphonesPluggedChangeReceiver
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */