package com.inmobi.rendering.mraid;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.inmobi.commons.core.utilities.b.c;
import com.inmobi.commons.core.utilities.b.d;
import com.inmobi.rendering.CustomView;
import com.inmobi.rendering.RenderView;

public final class f
{
  private static final String d = "f";
  public RenderView a;
  public ViewGroup b;
  public int c;
  
  public f(RenderView paramRenderView)
  {
    a = paramRenderView;
  }
  
  public final void a()
  {
    Object localObject1 = b;
    if (localObject1 == null)
    {
      localObject1 = (ViewGroup)a.getParent();
      b = ((ViewGroup)localObject1);
      localObject1 = b;
      localObject2 = a;
      int i = ((ViewGroup)localObject1).indexOfChild((View)localObject2);
      c = i;
    }
    localObject1 = a.getResizeProperties();
    Object localObject2 = new android/widget/FrameLayout;
    Object localObject3 = a.getContainerContext();
    ((FrameLayout)localObject2).<init>((Context)localObject3);
    localObject3 = new android/view/ViewGroup$LayoutParams;
    int k = a.getWidth();
    int m = a.getHeight();
    ((ViewGroup.LayoutParams)localObject3).<init>(k, m);
    ((FrameLayout)localObject2).setId((char)-1);
    Object localObject4 = b;
    m = c;
    ((ViewGroup)localObject4).addView((View)localObject2, m, (ViewGroup.LayoutParams)localObject3);
    localObject2 = b;
    localObject3 = a;
    ((ViewGroup)localObject2).removeView((View)localObject3);
    float f1 = ac;
    float f2 = b * f1;
    k = 1056964608;
    float f3 = 0.5F;
    f2 += f3;
    int n = (int)f2;
    m = c;
    float f4 = m * f1 + f3;
    int i4 = (int)f4;
    Object localObject5 = b.getRootView();
    int i6 = 16908290;
    localObject5 = (FrameLayout)((View)localObject5).findViewById(i6);
    FrameLayout localFrameLayout = new android/widget/FrameLayout;
    Object localObject6 = a.getContainerContext();
    localFrameLayout.<init>((Context)localObject6);
    localObject6 = new android/widget/FrameLayout$LayoutParams;
    int i7 = -1;
    float f5 = 0.0F / 0.0F;
    ((FrameLayout.LayoutParams)localObject6).<init>(i7, i7);
    RelativeLayout localRelativeLayout = new android/widget/RelativeLayout;
    Object localObject7 = a.getContainerContext();
    localRelativeLayout.<init>((Context)localObject7);
    localObject7 = new android/widget/FrameLayout$LayoutParams;
    ((FrameLayout.LayoutParams)localObject7).<init>(n, i4);
    Object localObject8 = new android/widget/RelativeLayout$LayoutParams;
    ((RelativeLayout.LayoutParams)localObject8).<init>(n, i4);
    i4 = (char)-2;
    f1 = 9.1833E-41F;
    localFrameLayout.setId(i4);
    localObject2 = a.getParent();
    if (localObject2 != null)
    {
      localObject2 = (ViewGroup)a.getParent();
      ((ViewGroup)localObject2).removeAllViews();
    }
    localObject2 = a;
    localRelativeLayout.addView((View)localObject2, (ViewGroup.LayoutParams)localObject8);
    localObject2 = a;
    localObject3 = c.a();
    f2 = c;
    localObject8 = new com/inmobi/rendering/CustomView;
    Object localObject9 = a.getContainerContext();
    int i8 = 1;
    ((CustomView)localObject8).<init>((Context)localObject9, f2, i8);
    int i9 = (char)-5;
    ((CustomView)localObject8).setId(i9);
    localObject9 = new com/inmobi/rendering/mraid/f$1;
    ((f.1)localObject9).<init>(this);
    ((CustomView)localObject8).setOnClickListener((View.OnClickListener)localObject9);
    if (localObject2 != null)
    {
      i9 = ((String)localObject2).length();
      if (i9 != 0)
      {
        localObject9 = "top-left";
        boolean bool6 = ((String)localObject2).equals(localObject9);
        if (bool6) {
          break label604;
        }
        localObject9 = "top-right";
        bool6 = ((String)localObject2).equals(localObject9);
        if (bool6) {
          break label604;
        }
        localObject9 = "bottom-left";
        bool6 = ((String)localObject2).equals(localObject9);
        if (bool6) {
          break label604;
        }
        localObject9 = "bottom-right";
        bool6 = ((String)localObject2).equals(localObject9);
        if (bool6) {
          break label604;
        }
        localObject9 = "top-center";
        bool6 = ((String)localObject2).equals(localObject9);
        if (bool6) {
          break label604;
        }
        localObject9 = "bottom-center";
        bool6 = ((String)localObject2).equals(localObject9);
        if (bool6) {
          break label604;
        }
        localObject9 = "center";
        bool6 = ((String)localObject2).equals(localObject9);
        if (bool6) {
          break label604;
        }
        localObject2 = "top-right";
        break label604;
      }
    }
    localObject2 = "top-right";
    label604:
    localObject9 = new android/widget/RelativeLayout$LayoutParams;
    float f6 = 50.0F;
    f2 *= f6;
    n = (int)f2;
    ((RelativeLayout.LayoutParams)localObject9).<init>(n, n);
    localObject3 = "top-right";
    boolean bool2 = ((String)localObject2).equals(localObject3);
    if (!bool2)
    {
      localObject3 = "bottom-right";
      bool2 = ((String)localObject2).equals(localObject3);
      if (!bool2) {}
    }
    else
    {
      int i1 = 11;
      f2 = 1.5E-44F;
      ((RelativeLayout.LayoutParams)localObject9).addRule(i1);
    }
    localObject3 = "bottom-right";
    boolean bool3 = ((String)localObject2).equals(localObject3);
    if (!bool3)
    {
      localObject3 = "bottom-left";
      bool3 = ((String)localObject2).equals(localObject3);
      if (!bool3)
      {
        localObject3 = "bottom-center";
        bool3 = ((String)localObject2).equals(localObject3);
        if (!bool3) {
          break label755;
        }
      }
    }
    ((RelativeLayout.LayoutParams)localObject9).addRule(12);
    int i2 = 4;
    f2 = 5.6E-45F;
    ((RelativeLayout.LayoutParams)localObject9).addRule(i2);
    label755:
    localObject3 = "bottom-center";
    boolean bool4 = ((String)localObject2).equals(localObject3);
    if (!bool4)
    {
      localObject3 = "top-center";
      bool4 = ((String)localObject2).equals(localObject3);
      if (!bool4)
      {
        localObject3 = "center";
        bool4 = ((String)localObject2).equals(localObject3);
        if (!bool4) {
          break label821;
        }
      }
    }
    int i3 = 13;
    f2 = 1.8E-44F;
    ((RelativeLayout.LayoutParams)localObject9).addRule(i3);
    label821:
    localObject3 = "top-center";
    boolean bool5 = ((String)localObject2).equals(localObject3);
    if (bool5)
    {
      i5 = 10;
      f1 = 1.4E-44F;
      ((RelativeLayout.LayoutParams)localObject9).addRule(i5);
    }
    localRelativeLayout.addView((View)localObject8, (ViewGroup.LayoutParams)localObject9);
    localFrameLayout.addView(localRelativeLayout, (ViewGroup.LayoutParams)localObject7);
    ((FrameLayout)localObject5).addView(localFrameLayout, (ViewGroup.LayoutParams)localObject6);
    localObject2 = c.a();
    f1 = c;
    f2 = b * f1 + f3;
    i3 = (int)f2;
    float f7 = c * f1 + f3;
    int i11 = (int)f7;
    f5 = d * f1 + f3;
    i7 = (int)f5;
    int i12 = e;
    float f8 = i12 * f1 + f3;
    int i5 = (int)f8;
    k = 2;
    f3 = 2.8E-45F;
    localObject7 = new int[k];
    localObject4 = new int[k];
    b.getLocationOnScreen((int[])localObject7);
    ((FrameLayout)localObject5).getLocationOnScreen((int[])localObject4);
    int i13 = localObject7[i8];
    int i10 = localObject4[i8];
    i13 -= i10;
    localObject7[i8] = i13;
    i13 = 0;
    localObject8 = null;
    i10 = localObject7[0];
    k = localObject4[0];
    i10 -= k;
    localObject7[0] = i10;
    k = localObject7[0] + i7;
    localObject7[0] = k;
    k = localObject7[i8] + i5;
    localObject7[i8] = k;
    boolean bool1 = f;
    if (!bool1)
    {
      int j = ((FrameLayout)localObject5).getWidth();
      i5 = localObject7[0];
      j -= i5;
      if (i3 > j)
      {
        j = ((FrameLayout)localObject5).getWidth() - i3;
        localObject7[0] = j;
      }
      j = ((FrameLayout)localObject5).getHeight();
      i5 = localObject7[i8];
      j -= i5;
      if (i11 > j)
      {
        j = ((FrameLayout)localObject5).getHeight() - i11;
        localObject7[i8] = j;
      }
      j = localObject7[0];
      if (j < 0) {
        localObject7[0] = 0;
      }
      j = localObject7[i8];
      if (j < 0) {
        localObject7[i8] = 0;
      }
    }
    localObject1 = new android/widget/FrameLayout$LayoutParams;
    ((FrameLayout.LayoutParams)localObject1).<init>(i3, i11);
    i5 = localObject7[0];
    leftMargin = i5;
    i5 = localObject7[i8];
    topMargin = i5;
    gravity = 8388611;
    localFrameLayout.setLayoutParams((ViewGroup.LayoutParams)localObject1);
    localFrameLayout.setBackgroundColor(0);
  }
}

/* Location:
 * Qualified Name:     com.inmobi.rendering.mraid.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */