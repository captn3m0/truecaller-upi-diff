package com.inmobi.rendering;

import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;

final class c$2
  implements InMobiAdActivity.b
{
  c$2(c paramc, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, String paramString9, String paramString10, String paramString11) {}
  
  public final void a(int[] paramArrayOfInt)
  {
    int m = paramArrayOfInt.length;
    int n = 2;
    if (m == n)
    {
      localObject = null;
      m = paramArrayOfInt[0];
      if (m == 0)
      {
        m = 1;
        int i1 = paramArrayOfInt[m];
        if (i1 == 0) {
          try
          {
            paramArrayOfInt = l;
            localObject = c.a(paramArrayOfInt);
            str1 = a;
            String str2 = b;
            String str3 = c;
            String str4 = d;
            String str5 = e;
            String str6 = f;
            String str7 = g;
            String str8 = h;
            String str9 = i;
            String str10 = j;
            String str11 = k;
            ((RenderView)localObject).a(str1, str2, str3, str4, str5, str6, str7, str8, str9, str10, str11);
            return;
          }
          catch (Exception paramArrayOfInt)
          {
            localObject = c.a(l);
            String str1 = a;
            ((RenderView)localObject).b(str1, "Unexpected error", "createCalendarEvent");
            Logger.a(Logger.InternalLogLevel.ERROR, "InMobi", "Could not create calendar event; SDK encountered unexpected error");
            c.a();
            paramArrayOfInt.getMessage();
            return;
          }
        }
      }
    }
    paramArrayOfInt = c.a(l);
    Object localObject = a;
    paramArrayOfInt.b((String)localObject, "Permission denied by user.", "createCalendarEvent");
  }
}

/* Location:
 * Qualified Name:     com.inmobi.rendering.c.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */