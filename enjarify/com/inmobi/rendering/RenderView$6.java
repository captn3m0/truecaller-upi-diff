package com.inmobi.rendering;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.webkit.ConsoleMessage;
import android.webkit.GeolocationPermissions.Callback;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebChromeClient.CustomViewCallback;
import android.webkit.WebView;
import android.widget.AbsoluteLayout.LayoutParams;
import android.widget.FrameLayout;
import java.lang.ref.WeakReference;

final class RenderView$6
  extends WebChromeClient
{
  RenderView$6(RenderView paramRenderView) {}
  
  private void a()
  {
    Object localObject = RenderView.n(a);
    if (localObject == null) {
      return;
    }
    localObject = RenderView.o(a);
    if (localObject != null)
    {
      RenderView.o(a).onCustomViewHidden();
      localObject = a;
      RenderView.a((RenderView)localObject, null);
    }
    localObject = RenderView.n(a);
    if (localObject != null)
    {
      localObject = RenderView.n(a).getParent();
      if (localObject != null)
      {
        localObject = (ViewGroup)RenderView.n(a).getParent();
        View localView = RenderView.n(a);
        ((ViewGroup)localObject).removeView(localView);
        localObject = a;
        RenderView.a((RenderView)localObject, null);
      }
    }
  }
  
  public final boolean onConsoleMessage(ConsoleMessage paramConsoleMessage)
  {
    paramConsoleMessage.message();
    paramConsoleMessage.lineNumber();
    paramConsoleMessage.sourceId();
    RenderView.g();
    return true;
  }
  
  public final void onGeolocationPermissionsShowPrompt(String paramString, GeolocationPermissions.Callback paramCallback)
  {
    Object localObject1 = RenderView.m(a);
    if (localObject1 != null)
    {
      localObject1 = RenderView.m(a).get();
      if (localObject1 != null)
      {
        localObject1 = new android/app/AlertDialog$Builder;
        Object localObject2 = (Context)RenderView.m(a).get();
        ((AlertDialog.Builder)localObject1).<init>((Context)localObject2);
        localObject1 = ((AlertDialog.Builder)localObject1).setTitle("Location Permission");
        localObject2 = "Allow location access";
        localObject1 = ((AlertDialog.Builder)localObject1).setMessage((CharSequence)localObject2);
        Object localObject3 = new com/inmobi/rendering/RenderView$6$7;
        ((RenderView.6.7)localObject3).<init>(this, paramCallback, paramString);
        localObject1 = ((AlertDialog.Builder)localObject1).setPositiveButton(17039370, (DialogInterface.OnClickListener)localObject3);
        int i = 17039360;
        localObject3 = new com/inmobi/rendering/RenderView$6$6;
        ((RenderView.6.6)localObject3).<init>(this, paramCallback, paramString);
        localObject1 = ((AlertDialog.Builder)localObject1).setNegativeButton(i, (DialogInterface.OnClickListener)localObject3).create();
        ((AlertDialog)localObject1).show();
      }
    }
    super.onGeolocationPermissionsShowPrompt(paramString, paramCallback);
  }
  
  public final void onHideCustomView()
  {
    a();
    super.onHideCustomView();
  }
  
  public final boolean onJsAlert(WebView paramWebView, String paramString1, String paramString2, JsResult paramJsResult)
  {
    RenderView.g();
    paramWebView = a;
    boolean bool = RenderView.a(paramWebView, paramJsResult);
    if (bool)
    {
      paramWebView = a.getFullScreenActivity();
      if (paramWebView != null)
      {
        AlertDialog.Builder localBuilder = new android/app/AlertDialog$Builder;
        localBuilder.<init>(paramWebView);
        paramWebView = localBuilder.setMessage(paramString2).setTitle(paramString1);
        paramString2 = new com/inmobi/rendering/RenderView$6$1;
        paramString2.<init>(this, paramJsResult);
        paramWebView = paramWebView.setPositiveButton(17039370, paramString2);
        paramString1 = null;
        paramWebView = paramWebView.setCancelable(false).create();
        paramWebView.show();
      }
      else
      {
        paramJsResult.cancel();
      }
    }
    return true;
  }
  
  public final boolean onJsConfirm(WebView paramWebView, String paramString1, String paramString2, JsResult paramJsResult)
  {
    RenderView.g();
    paramWebView = a;
    boolean bool = RenderView.a(paramWebView, paramJsResult);
    if (bool)
    {
      paramWebView = a.getFullScreenActivity();
      if (paramWebView != null)
      {
        paramString1 = new android/app/AlertDialog$Builder;
        paramString1.<init>(paramWebView);
        paramWebView = paramString1.setMessage(paramString2);
        paramString2 = new com/inmobi/rendering/RenderView$6$3;
        paramString2.<init>(this, paramJsResult);
        paramWebView = paramWebView.setPositiveButton(17039370, paramString2);
        int i = 17039360;
        paramString2 = new com/inmobi/rendering/RenderView$6$2;
        paramString2.<init>(this, paramJsResult);
        paramWebView = paramWebView.setNegativeButton(i, paramString2).create();
        paramWebView.show();
      }
      else
      {
        paramJsResult.cancel();
      }
    }
    return true;
  }
  
  public final boolean onJsPrompt(WebView paramWebView, String paramString1, String paramString2, String paramString3, JsPromptResult paramJsPromptResult)
  {
    RenderView.g();
    paramWebView = a;
    boolean bool1 = RenderView.a(paramWebView, paramJsPromptResult);
    boolean bool2 = true;
    if (bool1)
    {
      paramWebView = a.getFullScreenActivity();
      if (paramWebView == null)
      {
        paramJsPromptResult.cancel();
        return bool2;
      }
      return false;
    }
    return bool2;
  }
  
  public final void onShowCustomView(View paramView, WebChromeClient.CustomViewCallback paramCustomViewCallback)
  {
    Object localObject = RenderView.m(a);
    if (localObject != null)
    {
      localObject = RenderView.m(a).get();
      if (localObject != null)
      {
        RenderView.a(a, paramView);
        RenderView.a(a, paramCustomViewCallback);
        paramView = RenderView.n(a);
        paramCustomViewCallback = new com/inmobi/rendering/RenderView$6$4;
        paramCustomViewCallback.<init>(this);
        paramView.setOnTouchListener(paramCustomViewCallback);
        paramView = (FrameLayout)((Activity)RenderView.m(a).get()).findViewById(16908290);
        paramCustomViewCallback = RenderView.n(a);
        int i = -16777216;
        paramCustomViewCallback.setBackgroundColor(i);
        paramCustomViewCallback = RenderView.n(a);
        localObject = new android/widget/AbsoluteLayout$LayoutParams;
        int j = -1;
        ((AbsoluteLayout.LayoutParams)localObject).<init>(j, j, 0, 0);
        paramView.addView(paramCustomViewCallback, (ViewGroup.LayoutParams)localObject);
        RenderView.n(a).requestFocus();
        paramView = RenderView.n(a);
        paramCustomViewCallback = new com/inmobi/rendering/RenderView$6$5;
        paramCustomViewCallback.<init>(this);
        paramView.setOnKeyListener(paramCustomViewCallback);
        boolean bool = true;
        paramView.setFocusable(bool);
        paramView.setFocusableInTouchMode(bool);
        paramView.requestFocus();
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.rendering.RenderView.6
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */