package com.inmobi.rendering;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build.VERSION;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.inmobi.ads.AdContainer.RenderingProperties;
import com.inmobi.ads.AdContainer.RenderingProperties.PlacementType;
import com.inmobi.commons.core.utilities.b;
import java.util.List;

final class RenderView$5
  extends WebViewClient
{
  RenderView$5(RenderView paramRenderView) {}
  
  public final void onLoadResource(WebView paramWebView, String paramString)
  {
    RenderView.g();
    paramWebView = a.getUrl();
    if ((paramString != null) && (paramWebView != null))
    {
      String str = "/mraid.js";
      boolean bool1 = paramString.contains(str);
      if (bool1)
      {
        paramString = "about:blank";
        bool1 = paramWebView.equals(paramString);
        if (!bool1)
        {
          paramString = "file:";
          bool1 = paramWebView.startsWith(paramString);
          if (!bool1)
          {
            paramString = RenderView.j(a);
            bool1 = paramString.contains(paramWebView);
            if (!bool1)
            {
              paramString = RenderView.j(a);
              paramString.add(paramWebView);
            }
            paramWebView = a;
            boolean bool2 = RenderView.k(paramWebView);
            if (!bool2)
            {
              paramWebView = a;
              bool1 = true;
              RenderView.a(paramWebView, bool1);
              RenderView.g();
              paramWebView = a;
              paramString = paramWebView.getMraidJsString();
              paramWebView.c(paramString);
            }
          }
        }
      }
    }
  }
  
  public final void onPageFinished(WebView paramWebView, String paramString)
  {
    RenderView.g();
    paramWebView = RenderView.j(a);
    boolean bool1 = paramWebView.contains(paramString);
    if (bool1)
    {
      paramWebView = a;
      bool1 = RenderView.k(paramWebView);
      if (!bool1)
      {
        paramWebView = a;
        boolean bool2 = true;
        RenderView.a(paramWebView, bool2);
        RenderView.g();
        paramWebView = a;
        paramString = paramWebView.getMraidJsString();
        paramWebView.c(paramString);
      }
    }
    paramWebView = "Loading";
    paramString = RenderView.f(a);
    bool1 = paramWebView.equals(paramString);
    if (bool1)
    {
      paramWebView = RenderView.b(a);
      paramString = a;
      paramWebView.a(paramString);
      RenderView.l(a);
      paramWebView = RenderView.d(a);
      if (paramWebView != null)
      {
        a.setAndUpdateViewState("Expanded");
        return;
      }
      paramWebView = a;
      paramString = "Default";
      paramWebView.setAndUpdateViewState(paramString);
    }
  }
  
  public final void onPageStarted(WebView paramWebView, String paramString, Bitmap paramBitmap)
  {
    RenderView.g();
    RenderView.a(a, false);
    a.setAndUpdateViewState("Loading");
  }
  
  public final void onReceivedError(WebView paramWebView, int paramInt, String paramString1, String paramString2)
  {
    RenderView.g();
  }
  
  public final void onReceivedError(WebView paramWebView, WebResourceRequest paramWebResourceRequest, WebResourceError paramWebResourceError)
  {
    RenderView.g();
    paramWebView = new java/lang/StringBuilder;
    paramWebView.<init>("Loading error. Error code:");
    int i = paramWebResourceError.getErrorCode();
    paramWebView.append(i);
    paramWebView.append(" Error msg:");
    paramWebResourceError = paramWebResourceError.getDescription();
    paramWebView.append(paramWebResourceError);
    paramWebView.append(" Failing url:");
    paramWebResourceRequest = paramWebResourceRequest.getUrl();
    paramWebView.append(paramWebResourceRequest);
  }
  
  public final boolean shouldOverrideUrlLoading(WebView paramWebView, WebResourceRequest paramWebResourceRequest)
  {
    int i = Build.VERSION.SDK_INT;
    int j = 21;
    if (i >= j)
    {
      paramWebResourceRequest = paramWebResourceRequest.getUrl().toString();
      RenderView.g();
      RenderView.c(a);
      Object localObject = a;
      boolean bool1 = RenderView.g((RenderView)localObject);
      j = 1;
      if (bool1)
      {
        paramWebView.loadUrl(paramWebResourceRequest);
        return j;
      }
      paramWebView = a;
      boolean bool2 = RenderView.h(paramWebView);
      if (!bool2)
      {
        paramWebView = "about:blank";
        bool2 = paramWebView.equals(paramWebResourceRequest);
        if (!bool2)
        {
          paramWebView = a;
          RenderView.i(paramWebView);
        }
      }
      paramWebView = AdContainer.RenderingProperties.PlacementType.PLACEMENT_TYPE_FULLSCREEN;
      localObject = ca).a;
      if (paramWebView == localObject)
      {
        paramWebView = a;
        bool2 = RenderView.h(paramWebView);
        if (bool2)
        {
          bool2 = b.a(paramWebResourceRequest);
          if (bool2)
          {
            RenderView.g();
            return false;
          }
        }
        RenderView.g();
        paramWebView = b.a(a.getContainerContext(), paramWebResourceRequest, null);
        if (paramWebView != null)
        {
          paramWebView = a.getListener();
          paramWebView.x();
        }
        return j;
      }
      RenderView.g();
      paramWebView = b.a(a.getContainerContext(), paramWebResourceRequest, null);
      if (paramWebView != null)
      {
        paramWebView = a.getListener();
        paramWebView.x();
      }
      return j;
    }
    return false;
  }
  
  public final boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString)
  {
    RenderView.g();
    RenderView.c(a);
    Object localObject = a;
    boolean bool1 = RenderView.g((RenderView)localObject);
    boolean bool2 = true;
    if (bool1)
    {
      paramWebView.loadUrl(paramString);
      return bool2;
    }
    paramWebView = a;
    boolean bool3 = RenderView.h(paramWebView);
    if (!bool3)
    {
      paramWebView = "about:blank";
      bool3 = paramWebView.equals(paramString);
      if (!bool3)
      {
        paramWebView = a;
        RenderView.i(paramWebView);
      }
    }
    paramWebView = AdContainer.RenderingProperties.PlacementType.PLACEMENT_TYPE_FULLSCREEN;
    localObject = ca).a;
    if (paramWebView == localObject)
    {
      paramWebView = a;
      bool3 = RenderView.h(paramWebView);
      if (bool3)
      {
        bool3 = b.a(paramString);
        if (bool3)
        {
          RenderView.g();
          return false;
        }
      }
      RenderView.g();
      paramWebView = b.a(a.getContainerContext(), paramString, null);
      if (paramWebView != null)
      {
        paramWebView = a.getListener();
        paramWebView.x();
      }
      return bool2;
    }
    RenderView.g();
    paramWebView = b.a(a.getContainerContext(), paramString, null);
    if (paramWebView != null)
    {
      paramWebView = a.getListener();
      paramWebView.x();
    }
    return bool2;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.rendering.RenderView.5
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */