package com.inmobi.rendering;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.webkit.URLUtil;
import android.widget.FrameLayout;
import com.inmobi.ads.AdContainer;
import com.inmobi.ads.AdContainer.RenderingProperties;
import com.inmobi.ads.AdContainer.RenderingProperties.PlacementType;
import com.inmobi.ads.b;
import com.inmobi.ads.bn;
import com.inmobi.commons.a.a;
import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;
import java.util.HashMap;
import java.util.Map;

final class c$8
  implements Runnable
{
  c$8(c paramc, String paramString1, String paramString2) {}
  
  public final void run()
  {
    try
    {
      Object localObject1 = c;
      localObject1 = c.a((c)localObject1);
      localObject2 = a;
      localObject3 = b;
      String str = "Default";
      Object localObject4 = d;
      boolean bool1 = str.equals(localObject4);
      if (!bool1)
      {
        str = "Resized";
        localObject4 = d;
        bool1 = str.equals(localObject4);
        if (!bool1) {
          return;
        }
      }
      bool1 = true;
      t = bool1;
      localObject4 = f;
      Object localObject5 = c;
      Object localObject6;
      if (localObject5 == null)
      {
        localObject5 = a;
        localObject5 = ((RenderView)localObject5).getParent();
        localObject5 = (ViewGroup)localObject5;
        c = ((ViewGroup)localObject5);
        localObject5 = c;
        localObject6 = a;
        int i = ((ViewGroup)localObject5).indexOfChild((View)localObject6);
        d = i;
      }
      localObject5 = a;
      if (localObject5 != null)
      {
        localObject5 = a;
        localObject5 = ((RenderView)localObject5).getExpandProperties();
        boolean bool3 = URLUtil.isValidUrl((String)localObject3);
        b = bool3;
        bool3 = b;
        int m;
        if (bool3)
        {
          localObject6 = new com/inmobi/rendering/RenderView;
          localObject7 = a;
          localObject7 = ((RenderView)localObject7).getContainerContext();
          Object localObject8 = new com/inmobi/ads/AdContainer$RenderingProperties;
          AdContainer.RenderingProperties.PlacementType localPlacementType = AdContainer.RenderingProperties.PlacementType.PLACEMENT_TYPE_INLINE;
          ((AdContainer.RenderingProperties)localObject8).<init>(localPlacementType);
          localPlacementType = null;
          Object localObject9 = a;
          localObject9 = ((RenderView)localObject9).getImpressionId();
          ((RenderView)localObject6).<init>((Context)localObject7, (AdContainer.RenderingProperties)localObject8, null, (String)localObject9);
          localObject7 = a;
          localObject7 = ((RenderView)localObject7).getListener();
          localObject8 = a;
          localObject8 = ((RenderView)localObject8).getAdConfig();
          ((RenderView)localObject6).a((RenderView.a)localObject7, (b)localObject8);
          localObject7 = a;
          ((RenderView)localObject6).setOriginalRenderView((RenderView)localObject7);
          ((RenderView)localObject6).loadUrl((String)localObject3);
          m = InMobiAdActivity.a((AdContainer)localObject6);
          if (localObject5 != null)
          {
            localObject5 = a;
            boolean bool2 = m;
            ((RenderView)localObject6).setUseCustomClose(bool2);
          }
        }
        else
        {
          localObject3 = new android/widget/FrameLayout;
          localObject5 = a;
          localObject5 = ((RenderView)localObject5).getContainerContext();
          ((FrameLayout)localObject3).<init>((Context)localObject5);
          localObject5 = new android/view/ViewGroup$LayoutParams;
          localObject6 = a;
          int k = ((RenderView)localObject6).getWidth();
          localObject7 = a;
          n = ((RenderView)localObject7).getHeight();
          ((ViewGroup.LayoutParams)localObject5).<init>(k, n);
          k = (char)-1;
          ((FrameLayout)localObject3).setId(k);
          localObject6 = c;
          n = d;
          ((ViewGroup)localObject6).addView((View)localObject3, n, (ViewGroup.LayoutParams)localObject5);
          localObject3 = c;
          localObject5 = a;
          ((ViewGroup)localObject3).removeView((View)localObject5);
          localObject3 = a;
          m = InMobiAdActivity.a((AdContainer)localObject3);
        }
        localObject5 = a;
        localObject5 = ((RenderView)localObject5).getContainerContext();
        localObject6 = new android/content/Intent;
        Object localObject7 = InMobiAdActivity.class;
        ((Intent)localObject6).<init>((Context)localObject5, (Class)localObject7);
        localObject5 = "com.inmobi.rendering.InMobiAdActivity.EXTRA_AD_ACTIVITY_TYPE";
        int n = 102;
        ((Intent)localObject6).putExtra((String)localObject5, n);
        localObject5 = "com.inmobi.rendering.InMobiAdActivity.EXTRA_AD_CONTAINER_INDEX";
        ((Intent)localObject6).putExtra((String)localObject5, m);
        localObject3 = "com.inmobi.rendering.InMobiAdActivity.EXTRA_AD_CONTAINER_TYPE";
        int j = 200;
        ((Intent)localObject6).putExtra((String)localObject3, j);
        localObject3 = a;
        localObject3 = ((RenderView)localObject3).getContainerContext();
        a.a((Context)localObject3, (Intent)localObject6);
      }
      ((RenderView)localObject1).requestLayout();
      ((RenderView)localObject1).invalidate();
      n = bool1;
      ((RenderView)localObject1).setFocusable(bool1);
      ((RenderView)localObject1).setFocusableInTouchMode(bool1);
      ((RenderView)localObject1).requestFocus();
      localObject3 = new java/util/HashMap;
      ((HashMap)localObject3).<init>();
      str = "command";
      localObject4 = "expand";
      ((Map)localObject3).put(str, localObject4);
      str = "scheme";
      localObject2 = bn.a((String)localObject2);
      ((Map)localObject3).put(str, localObject2);
      localObject1 = c;
      localObject2 = "ads";
      str = "CreativeInvokedAction";
      ((RenderView.a)localObject1).b((String)localObject2, str, (Map)localObject3);
      return;
    }
    catch (Exception localException)
    {
      Object localObject2 = c.a(c);
      Object localObject3 = a;
      ((RenderView)localObject2).b((String)localObject3, "Unexpected error", "expand");
      Logger.a(Logger.InternalLogLevel.ERROR, "InMobi", "Failed to expand ad; SDK encountered an unexpected error");
      c.a();
      localException.getMessage();
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.rendering.c.8
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */