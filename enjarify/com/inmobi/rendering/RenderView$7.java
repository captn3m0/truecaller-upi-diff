package com.inmobi.rendering;

import com.inmobi.ads.cache.a;
import com.inmobi.ads.cache.b;
import com.inmobi.ads.cache.f;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

final class RenderView$7
  implements f
{
  RenderView$7(RenderView paramRenderView) {}
  
  public final void a(b paramb)
  {
    Object localObject1 = g;
    if (localObject1 != null)
    {
      localObject1 = a;
      int i = ((List)localObject1).size();
      if (i > 0)
      {
        localObject1 = new org/json/JSONObject;
        ((JSONObject)localObject1).<init>();
        localObject2 = "url";
      }
    }
    try
    {
      localObject3 = a;
      localObject3 = ((List)localObject3).get(0);
      localObject3 = (a)localObject3;
      localObject3 = d;
      ((JSONObject)localObject1).put((String)localObject2, localObject3);
      localObject2 = "reason";
      localObject3 = a;
      localObject3 = ((List)localObject3).get(0);
      localObject3 = (a)localObject3;
      int j = l;
      ((JSONObject)localObject1).put((String)localObject2, j);
    }
    catch (JSONException localJSONException)
    {
      Object localObject3;
      for (;;) {}
    }
    localObject1 = ((JSONObject)localObject1).toString().replace("\"", "\\\"");
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("sendSaveContentResult(\"saveContent_");
    localObject3 = h;
    ((StringBuilder)localObject2).append((String)localObject3);
    localObject3 = "\", 'failed', \"";
    ((StringBuilder)localObject2).append((String)localObject3);
    ((StringBuilder)localObject2).append((String)localObject1);
    ((StringBuilder)localObject2).append("\");");
    localObject1 = ((StringBuilder)localObject2).toString();
    RenderView.g();
    localObject2 = a;
    paramb = g;
    ((RenderView)localObject2).a(paramb, (String)localObject1);
  }
  
  public final void b(b paramb)
  {
    Object localObject1 = g;
    if (localObject1 != null)
    {
      localObject1 = a;
      int i = ((List)localObject1).size();
      if (i > 0)
      {
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>("sendSaveContentResult(\"saveContent_");
        Object localObject2 = h;
        ((StringBuilder)localObject1).append((String)localObject2);
        ((StringBuilder)localObject1).append("\", 'success', \"");
        localObject2 = a.get(0)).k;
        ((StringBuilder)localObject1).append((String)localObject2);
        ((StringBuilder)localObject1).append("\");");
        localObject1 = ((StringBuilder)localObject1).toString();
        RenderView.g();
        localObject2 = a;
        paramb = g;
        ((RenderView)localObject2).a(paramb, (String)localObject1);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.rendering.RenderView.7
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */