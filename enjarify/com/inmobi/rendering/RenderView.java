package com.inmobi.rendering;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Looper;
import android.provider.CalendarContract.Events;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.webkit.WebChromeClient;
import android.webkit.WebChromeClient.CustomViewCallback;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout.LayoutParams;
import com.inmobi.ads.AdContainer;
import com.inmobi.ads.AdContainer.RenderingProperties;
import com.inmobi.ads.AdContainer.RenderingProperties.PlacementType;
import com.inmobi.ads.AdContainer.a;
import com.inmobi.ads.NativeTracker;
import com.inmobi.ads.NativeTracker.TrackerEventType;
import com.inmobi.ads.NativeVideoView;
import com.inmobi.ads.NativeVideoWrapper;
import com.inmobi.ads.b.f;
import com.inmobi.ads.b.h;
import com.inmobi.ads.ba;
import com.inmobi.ads.bb;
import com.inmobi.ads.bm;
import com.inmobi.ads.bn;
import com.inmobi.ads.bp;
import com.inmobi.ads.bu;
import com.inmobi.ads.bw;
import com.inmobi.ads.bx;
import com.inmobi.ads.m;
import com.inmobi.ads.q;
import com.inmobi.ads.z;
import com.inmobi.rendering.mraid.MediaRenderView;
import com.inmobi.rendering.mraid.MraidMediaProcessor;
import com.inmobi.rendering.mraid.d;
import com.inmobi.rendering.mraid.e;
import com.inmobi.rendering.mraid.e.1;
import com.inmobi.rendering.mraid.g;
import com.inmobi.rendering.mraid.h;
import com.inmobi.rendering.mraid.i;
import com.inmobi.rendering.mraid.i.1;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONException;
import org.json.JSONObject;

public final class RenderView
  extends WebView
  implements AdContainer, b
{
  public static final RenderView.a a;
  private static final String x = RenderView.class.getSimpleName();
  private boolean A;
  private WeakReference B;
  private c C;
  private com.inmobi.ads.b D;
  private List E;
  private boolean F;
  private com.inmobi.rendering.mraid.b G;
  private h H;
  private g I;
  private JSONObject J;
  private JSONObject K;
  private boolean L;
  private boolean M;
  private final Object N;
  private final Object O;
  private boolean P;
  private View Q;
  private WebChromeClient.CustomViewCallback R;
  private int S;
  private boolean T;
  private String U;
  private AdContainer V;
  private m W;
  private Set aa;
  private bw ab;
  private final AdContainer.a ac;
  private final WebViewClient ad;
  private final WebChromeClient ae;
  WeakReference b;
  RenderView.a c;
  String d;
  AdContainer.RenderingProperties e;
  com.inmobi.rendering.mraid.c f;
  com.inmobi.rendering.mraid.f g;
  MraidMediaProcessor h;
  i i;
  public boolean j;
  boolean k;
  boolean l;
  public boolean m;
  boolean n;
  boolean o;
  boolean p;
  boolean q;
  String r;
  public AtomicBoolean s;
  boolean t;
  a u;
  public boolean v;
  final com.inmobi.ads.cache.f w;
  private RenderView y;
  private WeakReference z;
  
  static
  {
    RenderView.1 local1 = new com/inmobi/rendering/RenderView$1;
    local1.<init>();
    a = local1;
  }
  
  public RenderView(Context paramContext, AdContainer.RenderingProperties paramRenderingProperties, Set paramSet, String paramString)
  {
    super(localContext);
    localContext = null;
    A = false;
    d = "Default";
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    E = ((List)localObject1);
    boolean bool = true;
    k = bool;
    l = bool;
    m = false;
    L = bool;
    n = false;
    o = false;
    p = false;
    q = false;
    r = null;
    M = false;
    Object localObject2 = new java/util/concurrent/atomic/AtomicBoolean;
    ((AtomicBoolean)localObject2).<init>(false);
    s = ((AtomicBoolean)localObject2);
    localObject2 = new java/lang/Object;
    localObject2.<init>();
    N = localObject2;
    localObject2 = new java/lang/Object;
    localObject2.<init>();
    O = localObject2;
    P = bool;
    S = -1;
    T = false;
    localObject1 = new com/inmobi/rendering/RenderView$4;
    ((RenderView.4)localObject1).<init>(this);
    ac = ((AdContainer.a)localObject1);
    localObject1 = new com/inmobi/rendering/RenderView$5;
    ((RenderView.5)localObject1).<init>(this);
    ad = ((WebViewClient)localObject1);
    localObject1 = new com/inmobi/rendering/RenderView$6;
    ((RenderView.6)localObject1).<init>(this);
    ae = ((WebChromeClient)localObject1);
    localObject1 = new com/inmobi/rendering/RenderView$7;
    ((RenderView.7)localObject1).<init>(this);
    w = ((com.inmobi.ads.cache.f)localObject1);
    bool = paramContext instanceof Activity;
    if (bool)
    {
      localObject1 = new java/lang/ref/WeakReference;
      paramContext = (Activity)paramContext;
      ((WeakReference)localObject1).<init>(paramContext);
      z = ((WeakReference)localObject1);
    }
    y = null;
    e = paramRenderingProperties;
    t = false;
    aa = paramSet;
    U = paramString;
    setReferenceContainer(this);
    V = this;
    paramContext = new com/inmobi/ads/m;
    paramContext.<init>();
    W = paramContext;
  }
  
  private void c(boolean paramBoolean)
  {
    boolean bool1 = o;
    if (bool1 == paramBoolean) {
      return;
    }
    int i1 = Build.VERSION.SDK_INT;
    int i2 = 23;
    if (i1 > i2)
    {
      Activity localActivity = getFullScreenActivity();
      if (localActivity != null)
      {
        localActivity = getFullScreenActivity();
        boolean bool2 = localActivity.isInMultiWindowMode();
        if (bool2) {
          return;
        }
      }
    }
    d(paramBoolean);
  }
  
  public static void d() {}
  
  private void d(boolean paramBoolean)
  {
    boolean bool = t;
    if (!bool)
    {
      o = paramBoolean;
      if (!paramBoolean)
      {
        localObject1 = i;
        localObject2 = getContainerContext();
        ((i)localObject1).a((Context)localObject2);
      }
      else
      {
        localObject1 = c;
        ((RenderView.a)localObject1).b(this);
      }
      paramBoolean = o;
      Object localObject2 = new java/lang/StringBuilder;
      String str = "window.mraidview.broadcastEvent('viewableChange',";
      ((StringBuilder)localObject2).<init>(str);
      ((StringBuilder)localObject2).append(paramBoolean);
      ((StringBuilder)localObject2).append(");");
      Object localObject1 = ((StringBuilder)localObject2).toString();
      c((String)localObject1);
    }
  }
  
  private static String e(String paramString)
  {
    String str = "UTF-8";
    try
    {
      return URLEncoder.encode(paramString, str);
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException) {}
    return paramString;
  }
  
  static void e() {}
  
  static void f() {}
  
  private void h()
  {
    InMobiAdActivity.a(this);
    Object localObject1 = getFullScreenActivity();
    Object localObject2;
    if (localObject1 != null)
    {
      localObject2 = localObject1;
      localObject2 = (InMobiAdActivity)localObject1;
      a = true;
      ((Activity)localObject1).finish();
      int i1 = S;
      int i2 = -1;
      if (i1 != i2) {
        ((Activity)localObject1).overridePendingTransition(0, i1);
      }
    }
    else
    {
      localObject1 = AdContainer.RenderingProperties.PlacementType.PLACEMENT_TYPE_INLINE;
      localObject2 = e.a;
      if (localObject1 == localObject2)
      {
        setAndUpdateViewState("Default");
        localObject1 = y;
        if (localObject1 != null)
        {
          localObject2 = "Default";
          ((RenderView)localObject1).setAndUpdateViewState((String)localObject2);
        }
      }
      else
      {
        localObject1 = "Default";
        localObject2 = d;
        boolean bool = ((String)localObject1).equals(localObject2);
        if (bool)
        {
          localObject1 = "Hidden";
          setAndUpdateViewState((String)localObject1);
        }
      }
      localObject1 = c;
      if (localObject1 != null) {
        ((RenderView.a)localObject1).d(this);
      }
    }
  }
  
  private void i()
  {
    setVisibility(0);
    requestLayout();
  }
  
  public final void a()
  {
    P = false;
    try
    {
      Object localObject1 = getClass();
      Object localObject2 = "setLayerType";
      int i1 = 2;
      Class[] arrayOfClass = new Class[i1];
      Class localClass = Integer.TYPE;
      arrayOfClass[0] = localClass;
      localClass = Paint.class;
      int i2 = 1;
      arrayOfClass[i2] = localClass;
      localObject1 = ((Class)localObject1).getMethod((String)localObject2, arrayOfClass);
      localObject2 = new Object[i1];
      Integer localInteger = Integer.valueOf(i2);
      localObject2[0] = localInteger;
      localObject2[i2] = null;
      ((Method)localObject1).invoke(this, (Object[])localObject2);
      return;
    }
    catch (InvocationTargetException localInvocationTargetException) {}catch (NoSuchMethodException|IllegalArgumentException|IllegalAccessException localNoSuchMethodException) {}
  }
  
  public final void a(int paramInt, Map paramMap)
  {
    switch (paramInt)
    {
    default: 
      
    case 3: 
      
    case 2: 
      c("inmobi.recordEvent(120,null);");
      return;
    }
  }
  
  public final void a(RenderView.a parama, com.inmobi.ads.b paramb)
  {
    D = paramb;
    c = parama;
    parama = new java/lang/ref/WeakReference;
    paramb = (ViewGroup)getParent();
    parama.<init>(paramb);
    B = parama;
    parama = "row";
    paramb = "staging";
    boolean bool1 = parama.contains(paramb);
    boolean bool3 = true;
    int i1;
    if (bool1)
    {
      i1 = Build.VERSION.SDK_INT;
      i3 = 19;
      if (i1 >= i3) {
        WebView.setWebContentsDebuggingEnabled(bool3);
      }
    }
    parama = getRenderingConfig();
    if (parama != null)
    {
      parama = getRenderingConfig();
      i1 = f;
      setBackgroundColor(i1);
    }
    parama = getMraidConfig();
    int i3 = 0;
    if (parama != null)
    {
      parama = new com/inmobi/rendering/mraid/d;
      parama.<init>();
      parama = a;
      Object localObject1 = "last_updated_ts";
      long l1 = 0L;
      long l2 = parama.b((String)localObject1, l1);
      long l3 = System.currentTimeMillis();
      long l4 = 1000L;
      l3 = l3 / l4 - l2;
      parama = getMraidConfig();
      l2 = a;
      boolean bool2 = l3 < l2;
      if (bool2)
      {
        bool2 = true;
      }
      else
      {
        bool2 = false;
        parama = null;
      }
      if (bool2)
      {
        parama = new com/inmobi/rendering/mraid/e;
        localObject1 = getMraidConfigd;
        Object localObject2 = getMraidConfig();
        int i4 = b;
        Object localObject3 = getMraidConfig();
        int i5 = c;
        parama.<init>((String)localObject1, i4, i5);
        localObject1 = a;
        if (localObject1 != null)
        {
          localObject1 = new com/inmobi/commons/core/network/c;
          localObject3 = a;
          ((com.inmobi.commons.core.network.c)localObject1).<init>("GET", (String)localObject3);
          b = ((com.inmobi.commons.core.network.c)localObject1);
          localObject1 = new java/util/HashMap;
          ((HashMap)localObject1).<init>();
          localObject3 = "gzip";
          ((Map)localObject1).put("Accept-Encoding", localObject3);
          b.a((Map)localObject1);
          localObject1 = new java/lang/Thread;
          localObject2 = new com/inmobi/rendering/mraid/e$1;
          ((e.1)localObject2).<init>(parama);
          ((Thread)localObject1).<init>((Runnable)localObject2);
          ((Thread)localObject1).start();
        }
      }
    }
    int i2 = Build.VERSION.SDK_INT;
    int i6 = 16;
    if (i2 >= i6)
    {
      i2 = 2;
      setImportantForAccessibility(i2);
    }
    setScrollable(false);
    i2 = Build.VERSION.SDK_INT;
    i6 = 17;
    if (i2 >= i6)
    {
      parama = getSettings();
      parama.setMediaPlaybackRequiresUserGesture(false);
    }
    getSettings().setJavaScriptEnabled(bool3);
    getSettings().setGeolocationEnabled(bool3);
    parama = ad;
    setWebViewClient(parama);
    parama = ae;
    setWebChromeClient(parama);
    parama = new com/inmobi/rendering/c;
    paramb = e;
    parama.<init>(this, paramb);
    C = parama;
    parama = C;
    addJavascriptInterface(parama, "sdkController");
    parama = new com/inmobi/rendering/mraid/c;
    parama.<init>(this);
    f = parama;
    parama = new com/inmobi/rendering/mraid/f;
    parama.<init>(this);
    g = parama;
    parama = new com/inmobi/rendering/mraid/MraidMediaProcessor;
    parama.<init>(this);
    h = parama;
    parama = new com/inmobi/rendering/mraid/i;
    parama.<init>(this);
    i = parama;
    parama = new com/inmobi/rendering/mraid/b;
    parama.<init>();
    G = parama;
    parama = new com/inmobi/rendering/mraid/h;
    parama.<init>();
    H = parama;
    parama = new com/inmobi/rendering/mraid/g;
    parama.<init>();
    I = parama;
  }
  
  public final void a(String paramString)
  {
    t = false;
    AtomicBoolean localAtomicBoolean = s;
    boolean bool = localAtomicBoolean.get();
    if (!bool)
    {
      String str1 = "";
      String str2 = "text/html";
      String str3 = "UTF-8";
      loadDataWithBaseURL(str1, paramString, str2, str3, null);
    }
  }
  
  public final void a(String paramString1, String paramString2)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(paramString1);
    localStringBuilder.append(".");
    localStringBuilder.append(paramString2);
    paramString1 = localStringBuilder.toString();
    c(paramString1);
  }
  
  public final void a(String paramString1, String paramString2, String paramString3)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(paramString2);
    localStringBuilder.append("(");
    localStringBuilder.append(paramString3);
    localStringBuilder.append(");");
    paramString2 = localStringBuilder.toString();
    a(paramString1, paramString2);
  }
  
  public final void a(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    try
    {
      Object localObject = getContainerContext();
      com.inmobi.commons.core.utilities.b.b((Context)localObject, paramString3);
      paramString4 = new java/util/HashMap;
      paramString4.<init>();
      paramString4.put("command", "openExternal");
      localObject = bn.a(paramString2);
      paramString4.put("scheme", localObject);
      c.b("ads", "CreativeInvokedAction", paramString4);
      getListener().x();
      paramString4 = new java/lang/StringBuilder;
      paramString4.<init>("broadcastEvent('");
      paramString4.append(paramString1);
      paramString4.append("Successful','");
      paramString4.append(paramString3);
      paramString4.append("');");
      paramString1 = paramString4.toString();
      a(paramString2, paramString1);
      return;
    }
    catch (ActivityNotFoundException localActivityNotFoundException)
    {
      for (;;)
      {
        localStringBuilder = new java/lang/StringBuilder;
        str = "No app can handle the URI (";
        localStringBuilder.<init>(str);
        paramString3 = e(paramString3);
        localStringBuilder.append(paramString3);
        localStringBuilder.append(")");
        paramString3 = localStringBuilder.toString();
        b(paramString2, paramString3, paramString1);
        localActivityNotFoundException.getMessage();
        if (paramString4 == null) {
          break;
        }
        paramString3 = paramString4;
        paramString4 = null;
      }
      return;
    }
    catch (URISyntaxException localURISyntaxException)
    {
      for (;;)
      {
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        String str = "Cannot resolve URI (";
        localStringBuilder.<init>(str);
        paramString3 = e(paramString3);
        localStringBuilder.append(paramString3);
        localStringBuilder.append(")");
        paramString3 = localStringBuilder.toString();
        b(paramString2, paramString3, paramString1);
        localURISyntaxException.getMessage();
        if (paramString4 == null) {
          break;
        }
        paramString3 = paramString4;
        paramString4 = null;
      }
    }
  }
  
  public final void a(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, String paramString9, String paramString10, String paramString11)
  {
    Object localObject1 = paramString9;
    i locali = i;
    Context localContext = getContainerContext();
    int i1 = com.inmobi.rendering.mraid.a.a(localContext);
    Object localObject2 = com.inmobi.rendering.mraid.a.b(paramString3);
    if (localObject2 == null) {
      return;
    }
    int i2 = 1;
    ((Calendar)localObject2).get(i2);
    int i3 = 2;
    ((Calendar)localObject2).get(i3);
    int i4 = 5;
    ((Calendar)localObject2).get(i4);
    Object localObject3 = com.inmobi.rendering.mraid.a.b(paramString4);
    if (localObject3 == null) {
      return;
    }
    ((Calendar)localObject3).get(i2);
    ((Calendar)localObject3).get(i3);
    ((Calendar)localObject3).get(i4);
    Object localObject4 = new android/content/Intent;
    ((Intent)localObject4).<init>("android.intent.action.INSERT");
    Object localObject5 = CalendarContract.Events.CONTENT_URI;
    localObject4 = ((Intent)localObject4).setData((Uri)localObject5).putExtra("calendar_id", paramString2);
    long l1 = ((Calendar)localObject2).getTimeInMillis();
    localObject2 = ((Intent)localObject4).putExtra("beginTime", l1);
    long l2 = ((Calendar)localObject3).getTimeInMillis();
    localObject2 = ((Intent)localObject2).putExtra("endTime", l2);
    i4 = 0;
    localObject5 = null;
    localObject2 = ((Intent)localObject2).putExtra("allDay", false);
    localObject3 = paramString6;
    localObject2 = ((Intent)localObject2).putExtra("title", paramString6);
    localObject3 = paramString5;
    localObject2 = ((Intent)localObject2).putExtra("eventLocation", paramString5);
    localObject4 = "description";
    localObject3 = paramString7;
    Intent localIntent = ((Intent)localObject2).putExtra((String)localObject4, paramString7);
    localObject2 = "transparent";
    boolean bool1 = paramString9.equals(localObject2);
    if (bool1)
    {
      localObject1 = "availability";
      localIntent.putExtra((String)localObject1, i2);
    }
    else
    {
      localObject2 = "opaque";
      boolean bool2 = paramString9.equals(localObject2);
      if (bool2)
      {
        localObject1 = "availability";
        localIntent.putExtra((String)localObject1, 0);
      }
    }
    localObject1 = i.a(paramString10);
    int i5 = ((String)localObject1).length();
    if (i5 != 0)
    {
      localObject2 = "rrule";
      localIntent.putExtra((String)localObject2, (String)localObject1);
    }
    localObject1 = new com/inmobi/rendering/mraid/i$1;
    localObject2 = localObject1;
    localObject4 = paramString8;
    localObject5 = paramString11;
    localObject3 = paramString3;
    ((i.1)localObject1).<init>(locali, localContext, i1, paramString8, paramString11, paramString3, paramString1);
    int i6 = InMobiAdActivity.a(localIntent, (InMobiAdActivity.a)localObject1);
    localObject2 = new android/content/Intent;
    ((Intent)localObject2).<init>(localContext, InMobiAdActivity.class);
    ((Intent)localObject2).putExtra("com.inmobi.rendering.InMobiAdActivity.EXTRA_AD_ACTIVITY_TYPE", 103);
    ((Intent)localObject2).putExtra("id", i6);
    com.inmobi.commons.a.a.a(localContext, (Intent)localObject2);
  }
  
  public final void a(boolean paramBoolean)
  {
    setCloseRegionDisabled(paramBoolean);
    Object localObject = getRootView();
    if (localObject != null)
    {
      int i1 = (char)-5;
      localObject = (CustomView)((View)localObject).findViewById(i1);
      if (localObject != null)
      {
        boolean bool = p;
        int i2;
        if (bool) {
          i2 = 8;
        } else {
          i2 = 0;
        }
        ((CustomView)localObject).setVisibility(i2);
      }
    }
  }
  
  public final void b()
  {
    Object localObject1 = h;
    Object localObject2 = b;
    if (localObject2 != null)
    {
      b.a();
      i1 = 0;
      localObject2 = null;
      b = null;
    }
    localObject1 = "Expanded";
    localObject2 = d;
    boolean bool1 = ((String)localObject1).equals(localObject2);
    int i1 = (char)-1;
    boolean bool2 = true;
    Object localObject3;
    Object localObject4;
    int i2;
    RelativeLayout.LayoutParams localLayoutParams;
    ViewGroup localViewGroup1;
    int i3;
    ViewGroup localViewGroup2;
    int i4;
    if (bool1)
    {
      localObject1 = "Default";
      localObject3 = d;
      bool1 = ((String)localObject1).equals(localObject3);
      if (!bool1)
      {
        t = bool2;
        localObject1 = f;
        localObject4 = a.getOriginalRenderView();
        if (localObject4 == null)
        {
          localObject2 = c.getRootView().findViewById(i1);
          localObject4 = (ViewGroup)a.getParent();
          localObject3 = a;
          ((ViewGroup)localObject4).removeView((View)localObject3);
          ((ViewGroup)((View)localObject2).getParent()).removeView((View)localObject2);
          localObject2 = c;
          localObject4 = a;
          i2 = d;
          localLayoutParams = new android/widget/RelativeLayout$LayoutParams;
          localViewGroup1 = c;
          i3 = localViewGroup1.getWidth();
          localViewGroup2 = c;
          i4 = localViewGroup2.getHeight();
          localLayoutParams.<init>(i3, i4);
          ((ViewGroup)localObject2).addView((View)localObject4, i2, localLayoutParams);
          localObject1 = a;
          ((RenderView)localObject1).i();
        }
        h();
        t = false;
      }
      L = false;
    }
    else
    {
      localObject1 = "Resized";
      localObject3 = d;
      bool1 = ((String)localObject1).equals(localObject3);
      if (bool1)
      {
        localObject1 = "Default";
        localObject3 = d;
        bool1 = ((String)localObject1).equals(localObject3);
        if (!bool1)
        {
          t = bool2;
          localObject1 = g;
          localObject4 = (ViewGroup)a.getParent();
          localObject3 = ((ViewGroup)localObject4).getRootView();
          int i5 = (char)-2;
          localObject3 = ((View)localObject3).findViewById(i5);
          localObject2 = b.getRootView().findViewById(i1);
          ((ViewGroup)((View)localObject3).getParent()).removeView((View)localObject3);
          localObject3 = (ViewGroup)((View)localObject2).getParent();
          ((ViewGroup)localObject3).removeView((View)localObject2);
          localObject2 = a;
          ((ViewGroup)localObject4).removeView((View)localObject2);
          localObject2 = b;
          localObject4 = a;
          i2 = c;
          localLayoutParams = new android/widget/RelativeLayout$LayoutParams;
          localViewGroup1 = b;
          i3 = localViewGroup1.getWidth();
          localViewGroup2 = b;
          i4 = localViewGroup2.getHeight();
          localLayoutParams.<init>(i3, i4);
          ((ViewGroup)localObject2).addView((View)localObject4, i2, localLayoutParams);
          a.i();
          setAndUpdateViewState("Default");
          localObject1 = c;
          ((RenderView.a)localObject1).d(this);
          t = false;
        }
      }
      else
      {
        localObject1 = "Default";
        localObject2 = d;
        bool1 = ((String)localObject1).equals(localObject2);
        if (bool1)
        {
          setAndUpdateViewState("Hidden");
          localObject1 = AdContainer.RenderingProperties.PlacementType.PLACEMENT_TYPE_FULLSCREEN;
          localObject2 = e.a;
          if (localObject1 == localObject2)
          {
            h();
          }
          else
          {
            localObject1 = (ViewGroup)getParent();
            ((ViewGroup)localObject1).removeAllViews();
          }
        }
      }
    }
    E.clear();
    n = false;
  }
  
  public final void b(String paramString)
  {
    t = false;
    AtomicBoolean localAtomicBoolean = s;
    boolean bool = localAtomicBoolean.get();
    if (!bool) {
      loadUrl(paramString);
    }
  }
  
  public final void b(String paramString1, String paramString2, String paramString3)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("broadcastEvent('error',\"");
    localStringBuilder.append(paramString2);
    localStringBuilder.append("\", \"");
    localStringBuilder.append(paramString3);
    localStringBuilder.append("\")");
    paramString2 = localStringBuilder.toString();
    a(paramString1, paramString2);
  }
  
  public final void b(boolean paramBoolean)
  {
    setUseCustomClose(paramBoolean);
    Object localObject = getRootView();
    if (localObject != null)
    {
      localObject = getRootView();
      int i1 = (char)-4;
      localObject = (CustomView)((View)localObject).findViewById(i1);
      if (localObject != null)
      {
        boolean bool = m;
        int i2;
        if (bool) {
          i2 = 8;
        } else {
          i2 = 0;
        }
        ((CustomView)localObject).setVisibility(i2);
      }
    }
  }
  
  public final void c(String paramString)
  {
    Object localObject1 = getContainerContext();
    if (localObject1 == null) {
      return;
    }
    localObject1 = new android/os/Handler;
    Object localObject2 = getContainerContext().getMainLooper();
    ((Handler)localObject1).<init>((Looper)localObject2);
    localObject2 = new com/inmobi/rendering/RenderView$2;
    ((RenderView.2)localObject2).<init>(this, paramString);
    ((Handler)localObject1).post((Runnable)localObject2);
  }
  
  public final void c(String paramString1, String paramString2, String paramString3)
  {
    if (paramString3 != null)
    {
      Object localObject = "http";
      boolean bool = paramString3.startsWith((String)localObject);
      if (bool)
      {
        bool = URLUtil.isValidUrl(paramString3);
        if (!bool) {}
      }
      else
      {
        localObject = "http";
        bool = paramString3.startsWith((String)localObject);
        if (bool)
        {
          localObject = "play.google.com";
          bool = paramString3.contains((CharSequence)localObject);
          if (!bool)
          {
            localObject = "market.android.com";
            bool = paramString3.contains((CharSequence)localObject);
            if (!bool)
            {
              localObject = "market%3A%2F%2F";
              bool = paramString3.contains((CharSequence)localObject);
              if (!bool)
              {
                InMobiAdActivity.a(this);
                localObject = new android/content/Intent;
                Context localContext = getContainerContext();
                ((Intent)localObject).<init>(localContext, InMobiAdActivity.class);
                ((Intent)localObject).putExtra("com.inmobi.rendering.InMobiAdActivity.EXTRA_AD_ACTIVITY_TYPE", 100);
                ((Intent)localObject).putExtra("com.inmobi.rendering.InMobiAdActivity.IN_APP_BROWSER_URL", paramString3);
                com.inmobi.commons.a.a.a(getContainerContext(), (Intent)localObject);
                localObject = new java/lang/StringBuilder;
                ((StringBuilder)localObject).<init>("broadcastEvent('");
                ((StringBuilder)localObject).append(paramString1);
                ((StringBuilder)localObject).append("Successful','");
                ((StringBuilder)localObject).append(paramString3);
                ((StringBuilder)localObject).append("');");
                paramString1 = ((StringBuilder)localObject).toString();
                a(paramString2, paramString1);
                paramString1 = new java/util/HashMap;
                paramString1.<init>();
                paramString1.put("command", "openEmbedded");
                paramString2 = bn.a(paramString2);
                paramString1.put("scheme", paramString2);
                c.b("ads", "CreativeInvokedAction", paramString1);
                return;
              }
            }
          }
        }
        a(paramString1, paramString2, paramString3, null);
        return;
      }
    }
    b(paramString2, "Invalid URL", paramString1);
  }
  
  public final boolean c()
  {
    return s.get();
  }
  
  public final boolean d(String paramString)
  {
    int i1 = paramString.hashCode();
    boolean bool2 = true;
    Object localObject;
    boolean bool3;
    switch (i1)
    {
    default: 
      break;
    case 1921345160: 
      localObject = "postToSocial";
      bool3 = paramString.equals(localObject);
      if (bool3) {
        bool3 = true;
      }
      break;
    case 1772979069: 
      localObject = "redirectFraudDetection";
      bool3 = paramString.equals(localObject);
      if (bool3)
      {
        bool3 = false;
        paramString = null;
      }
      break;
    case 1642189884: 
      localObject = "saveContent";
      bool3 = paramString.equals(localObject);
      if (bool3) {
        int i2 = 3;
      }
      break;
    case 1509574865: 
      localObject = "html5video";
      boolean bool4 = paramString.equals(localObject);
      if (bool4) {
        int i3 = 5;
      }
      break;
    case -178324674: 
      localObject = "calendar";
      boolean bool5 = paramString.equals(localObject);
      if (bool5) {
        int i4 = 6;
      }
      break;
    case -1647691422: 
      localObject = "inlineVideo";
      boolean bool6 = paramString.equals(localObject);
      if (bool6) {
        int i5 = 4;
      }
      break;
    case -1886160473: 
      localObject = "playVideo";
      boolean bool7 = paramString.equals(localObject);
      if (bool7) {
        i6 = 2;
      }
      break;
    }
    int i6 = -1;
    switch (i6)
    {
    default: 
      return false;
    case 6: 
      paramString = new android/content/Intent;
      paramString.<init>("android.intent.action.VIEW");
      paramString.setType("vnd.android.cursor.item/event");
      paramString = getContainerContext().getPackageManager().resolveActivity(paramString, 65536);
      localObject = com.inmobi.commons.a.a.b();
      boolean bool1 = com.inmobi.commons.a.a.b((Context)localObject, "android.permission.WRITE_CALENDAR");
      Context localContext = com.inmobi.commons.a.a.b();
      String str = "android.permission.READ_CALENDAR";
      boolean bool9 = com.inmobi.commons.a.a.b(localContext, str);
      if ((paramString != null) && (bool1) && (bool9)) {
        return bool2;
      }
      return false;
    case 4: 
    case 5: 
      boolean bool8 = F;
      if (bool8)
      {
        bool8 = P;
        if (bool8) {
          return bool2;
        }
      }
      return false;
    }
    return bool2;
  }
  
  public final void destroy()
  {
    Object localObject = s;
    boolean bool1 = ((AtomicBoolean)localObject).get();
    if (bool1) {
      return;
    }
    bool1 = L;
    boolean bool2 = true;
    if (!bool1)
    {
      L = bool2;
      return;
    }
    s.set(bool2);
    t = bool2;
    int i1 = -1;
    S = i1;
    removeJavascriptInterface("sdkController");
    localObject = b;
    if (localObject != null) {
      ((WeakReference)localObject).clear();
    }
    localObject = B;
    if (localObject != null) {
      ((WeakReference)localObject).clear();
    }
    localObject = ab;
    if (localObject != null)
    {
      ((bw)localObject).d();
      localObject = ab;
      ((bw)localObject).e();
    }
    i1 = 0;
    V = null;
    localObject = getParent();
    if (localObject != null)
    {
      localObject = (ViewGroup)localObject;
      ((ViewGroup)localObject).removeView(this);
      removeAllViews();
    }
    super.destroy();
  }
  
  public final com.inmobi.ads.b getAdConfig()
  {
    return D;
  }
  
  public final m getApkDownloader()
  {
    return W;
  }
  
  public final Context getContainerContext()
  {
    Object localObject = b;
    if (localObject != null)
    {
      localObject = ((WeakReference)localObject).get();
      if (localObject != null) {
        return (Context)b.get();
      }
    }
    return getContext();
  }
  
  public final String getCurrentPosition()
  {
    JSONObject localJSONObject = K;
    if (localJSONObject == null) {
      return "";
    }
    return localJSONObject.toString();
  }
  
  public final Object getCurrentPositionMonitor()
  {
    return O;
  }
  
  public final Object getDataModel()
  {
    return null;
  }
  
  public final String getDefaultPosition()
  {
    JSONObject localJSONObject = J;
    if (localJSONObject == null) {
      return "";
    }
    return localJSONObject.toString();
  }
  
  public final Object getDefaultPositionMonitor()
  {
    return N;
  }
  
  final int getDownloadProgress()
  {
    getReferenceContainer().getApkDownloader();
    return 0;
  }
  
  final int getDownloadStatus()
  {
    getReferenceContainer().getApkDownloader();
    return -2;
  }
  
  public final com.inmobi.rendering.mraid.b getExpandProperties()
  {
    return G;
  }
  
  public final Activity getFullScreenActivity()
  {
    WeakReference localWeakReference = b;
    if (localWeakReference == null) {
      return null;
    }
    return (Activity)localWeakReference.get();
  }
  
  public final AdContainer.a getFullScreenEventsListener()
  {
    return ac;
  }
  
  public final String getImpressionId()
  {
    return U;
  }
  
  public final RenderView.a getListener()
  {
    Object localObject = c;
    if (localObject == null)
    {
      localObject = new com/inmobi/rendering/RenderView$3;
      ((RenderView.3)localObject).<init>(this);
      c = ((RenderView.a)localObject);
      return (RenderView.a)localObject;
    }
    return (RenderView.a)localObject;
  }
  
  public final String getMarkupType()
  {
    return "html";
  }
  
  public final MraidMediaProcessor getMediaProcessor()
  {
    return h;
  }
  
  public final b.f getMraidConfig()
  {
    return D.n;
  }
  
  public final String getMraidJsString()
  {
    Object localObject = new com/inmobi/rendering/mraid/d;
    ((d)localObject).<init>();
    localObject = a;
    String str = "mraid_js_string";
    localObject = ((com.inmobi.commons.core.d.c)localObject).c(str);
    if (localObject == null) {
      localObject = "var imIsObjValid=function(a){return\"undefined\"!=typeof a&&null!=a?!0:!1},EventListeners=function(a){this.event=a;this.count=0;var b=[];this.add=function(a){b.push(a);++this.count};this.remove=function(a){var e=!1,d=this;b=b.filter(function(b){if(b=b===a)--d.count,e=!0;return!b});return e};this.removeAll=function(){b=[];this.count=0};this.broadcast=function(a){b.forEach(function(e){try{e.apply({},a)}catch(d){}})};this.toString=function(){var c=[a,\":\"];b.forEach(function(a){c.push(\"|\",String(a),\"|\")});\nreturn c.join(\"\")}},InmobiObj=function(){this.listeners=[];this.addEventListener=function(a,b){try{if(imIsObjValid(b)&&imIsObjValid(a)){var c=this.listeners;c[a]||(c[a]=new EventListeners);c[a].add(b);\"micIntensityChange\"==a&&window.imraidview.startListeningMicIntensity();\"deviceMuted\"==a&&window.imraidview.startListeningDeviceMuteEvents();\"deviceVolumeChange\"==a&&window.imraidview.startListeningDeviceVolumeChange();\"volumeChange\"==a&&window.imraidview.startListeningVolumeChange();\"headphones\"==a&&\nwindow.imraidview.startListeningHeadphonePluggedEvents();\"backButtonPressed\"==a&&window.imraidview.startListeningForBackButtonPressedEvent();\"downloadStatusChanged\"==a&&window.imraidview.registerDownloaderCallbacks()}}catch(e){this.log(e)}};this.removeEventListener=function(a,b){if(imIsObjValid(a)){var c=this.listeners;imIsObjValid(c[a])&&(imIsObjValid(b)?c[a].remove(b):c[a].removeAll());\"micIntensityChange\"==a&&0==c[a].count&&window.imraidview.stopListeningMicIntensity();\"deviceMuted\"==a&&0==c[a].count&&\nwindow.imraidview.stopListeningDeviceMuteEvents();\"deviceVolumeChange\"==a&&0==c[a].count&&window.imraidview.stopListeningDeviceVolumeChange();\"volumeChange\"==a&&0==c[a].count&&window.imraidview.stopListeningVolumeChange();\"headphones\"==a&&0==c[a].count&&window.imraidview.stopListeningHeadphonePluggedEvents();\"backButtonPressed\"==a&&0==c[a].count&&window.imraidview.stopListeningForBackButtonPressedEvent();\"downloadStatusChanged\"==a&&0==c[a].count&&window.imraidview.unregisterDownloaderCallbacks()}};\nthis.broadcastEvent=function(a){if(imIsObjValid(a)){for(var b=Array(arguments.length),c=0;c<arguments.length;c++)b[c]=arguments[c];c=b.shift();try{this.listeners[c]&&this.listeners[c].broadcast(b)}catch(e){}}};this.sendSaveContentResult=function(a){if(imIsObjValid(a)){for(var b=Array(arguments.length),c=0;c<arguments.length;c++)if(2==c){var e=arguments[c],e=JSON.parse(e);b[c]=e}else b[c]=arguments[c];e=b[1];\"success\"!=e&&(c=b[0].substring(b[0].indexOf(\"_\")+1),imraid.saveContentIDMap[c]&&delete imraid.saveContentIDMap[c]);\nwindow.imraid.broadcastEvent(b[0],b[1],b[2])}}},__im__iosNativeMessageHandler=void 0;window.webkit&&(window.webkit.messageHandlers&&window.webkit.messageHandlers.nativeMessageHandler)&&(__im__iosNativeMessageHandler=window.webkit.messageHandlers.nativeMessageHandler);\nvar __im__iosNativeCall={nativeCallInFlight:!1,nativeCallQueue:[],executeNativeCall:function(a){this.nativeCallInFlight?this.nativeCallQueue.push(a):(this.nativeCallInFlight=!0,imIsObjValid(__im__iosNativeMessageHandler)?__im__iosNativeMessageHandler.postMessage(a):window.location=a)},nativeCallComplete:function(a){0==this.nativeCallQueue.length?this.nativeCallInFlight=!1:(a=this.nativeCallQueue.shift(),imIsObjValid(__im__iosNativeMessageHandler)?__im__iosNativeMessageHandler.postMessage(a):window.location=\na)}},IOSNativeCall=function(){this.urlScheme=\"\";this.executeNativeCall=function(a){if(imIsObjValid(__im__iosNativeMessageHandler)){e={};e.command=a;e.scheme=this.urlScheme;for(var b={},c=1;c<arguments.length;c+=2)d=arguments[c+1],null!=d&&(b[arguments[c]]=\"\"+d);e.params=b}else for(var e=this.urlScheme+\"://\"+a,d,b=!0,c=1;c<arguments.length;c+=2)d=arguments[c+1],null!=d&&(b?(e+=\"?\",b=!1):e+=\"&\",e+=arguments[c]+\"=\"+escape(d));__im__iosNativeCall.executeNativeCall(e);return\"OK\"};this.nativeCallComplete=\nfunction(a){__im__iosNativeCall.nativeCallComplete(a);return\"OK\"};this.updateKV=function(a,b){this[a]=b;var c=this.broadcastMap[a];c&&this.broadcastEvent(c,b)}};\n(function(){var a=window.mraidview={};a.orientationProperties={allowOrientationChange:!0,forceOrientation:\"none\",direction:\"right\"};var b=[],c=!1;a.detectAndBlockFraud=function(e){a.isPossibleFraud()&&a.fireRedirectFraudBeacon(e);return!1};a.popupBlocked=function(e){a.firePopupBlockedBeacon(e)};a.zeroPad=function(a){var d=\"\";10>a&&(d+=\"0\");return d+a};a.supports=function(a){console.log(\"bridge: supports (MRAID)\");if(\"string\"!=typeof a)window.mraid.broadcastEvent(\"error\",\"Supports method expects string parameter\",\n\"supports\");else return\"false\"!=sdkController.supports(\"window.mraidview\",a)};a.useCustomClose=function(a){try{sdkController.useCustomClose(\"window.mraidview\",a)}catch(d){imraidview.showAlert(\"use CustomClose: \"+d)}};a.close=function(){try{sdkController.close(\"window.mraidview\")}catch(a){imraidview.showAlert(\"close: \"+a)}};a.stackCommands=function(a,d){c?b.push(a):(eval(a),d&&(c=!0))};a.expand=function(a){try{\"undefined\"==typeof a&&(a=null),sdkController.expand(\"window.mraidview\",a)}catch(d){imraidview.showAlert(\"executeNativeExpand: \"+\nd+\", URL = \"+a)}};a.setExpandProperties=function(e){try{e?this.props=e:e=null;if(\"undefined\"!=typeof e.lockOrientation&&null!=e.lockOrientation&&\"undefined\"!=typeof e.orientation&&null!=e.orientation){var d={};d.allowOrientationChange=!e.lockOrientation;d.forceOrientation=e.orientation;a.setOrientationProperties(d)}sdkController.setExpandProperties(\"window.mraidview\",a.stringify(e))}catch(b){imraidview.showAlert(\"executeNativesetExpandProperties: \"+b+\", props = \"+e)}};a.getExpandProperties=function(){try{return eval(\"(\"+\nsdkController.getExpandProperties(\"window.mraidview\")+\")\")}catch(a){imraidview.showAlert(\"getExpandProperties: \"+a)}};a.setOrientationProperties=function(e){try{e?(\"undefined\"!=typeof e.allowOrientationChange&&(a.orientationProperties.allowOrientationChange=e.allowOrientationChange),\"undefined\"!=typeof e.forceOrientation&&(a.orientationProperties.forceOrientation=e.forceOrientation)):e=null,sdkController.setOrientationProperties(\"window.mraidview\",a.stringify(a.orientationProperties))}catch(d){imraidview.showAlert(\"setOrientationProperties: \"+\nd+\", props = \"+e)}};a.getOrientationProperties=function(){return{forceOrientation:a.orientationProperties.forceOrientation,allowOrientationChange:a.orientationProperties.allowOrientationChange}};a.resizeProps=null;a.setResizeProperties=function(e){var d,b;try{d=parseInt(e.width);b=parseInt(e.height);if(isNaN(d)||isNaN(b)||1>d||1>b)throw\"Invalid\";e.width=d;e.height=b;a.resizeProps=e;sdkController.setResizeProperties(\"window.mraidview\",a.stringify(e))}catch(c){window.mraid.broadcastEvent(\"error\",\"Invalid properties.\",\n\"setResizeProperties\")}};a.getResizeProperties=function(){try{return eval(\"(\"+sdkController.getResizeProperties(\"window.mraidview\")+\")\")}catch(a){imraidview.showAlert(\"getResizeProperties: \"+a)}};a.open=function(a){\"undefined\"==typeof a&&(a=null);try{sdkController.open(\"window.mraidview\",a)}catch(d){imraidview.showAlert(\"open: \"+d)}};a.getScreenSize=function(){try{return eval(\"(\"+sdkController.getScreenSize(\"window.mraidview\")+\")\")}catch(a){imraidview.showAlert(\"getScreenSize: \"+a)}};a.getMaxSize=\nfunction(){try{return eval(\"(\"+sdkController.getMaxSize(\"window.mraidview\")+\")\")}catch(a){imraidview.showAlert(\"getMaxSize: \"+a)}};a.getCurrentPosition=function(){try{return eval(\"(\"+sdkController.getCurrentPosition(\"window.mraidview\")+\")\")}catch(a){imraidview.showAlert(\"getCurrentPosition: \"+a)}};a.getDefaultPosition=function(){try{return eval(\"(\"+sdkController.getDefaultPosition(\"window.mraidview\")+\")\")}catch(a){imraidview.showAlert(\"getDefaultPosition: \"+a)}};a.getState=function(){try{return String(sdkController.getState(\"window.mraidview\"))}catch(a){imraidview.showAlert(\"getState: \"+\na)}};a.isViewable=function(){try{return sdkController.isViewable(\"window.mraidview\")}catch(a){imraidview.showAlert(\"isViewable: \"+a)}};a.getPlacementType=function(){return sdkController.getPlacementType(\"window.mraidview\")};a.close=function(){try{sdkController.close(\"window.mraidview\")}catch(a){imraidview.showAlert(\"close: \"+a)}};\"function\"!=typeof String.prototype.startsWith&&(String.prototype.startsWith=function(a){return 0==this.indexOf(a)});a.playVideo=function(a){var d=\"\";null!=a&&(d=a);try{sdkController.playVideo(\"window.mraidview\",\nd)}catch(b){imraidview.showAlert(\"playVideo: \"+b)}};a.stringify=function(e){if(\"undefined\"===typeof JSON){var d=\"\",b;if(\"undefined\"==typeof e.length)return a.stringifyArg(e);for(b=0;b<e.length;b++)0<b&&(d+=\",\"),d+=a.stringifyArg(e[b]);return d+\"]\"}return JSON.stringify(e)};a.stringifyArg=function(a){var d,b,c;b=typeof a;d=\"\";if(\"number\"===b||\"boolean\"===b)d+=args;else if(a instanceof Array)d=d+\"[\"+a+\"]\";else if(a instanceof Object){b=!0;d+=\"{\";for(c in a)null!==a[c]&&(b||(d+=\",\"),d=d+'\"'+c+'\":',b=\ntypeof a[c],d=\"number\"===b||\"boolean\"===b?d+a[c]:\"function\"===typeof a[c]?d+'\"\"':a[c]instanceof Object?d+this.stringify(args[i][c]):d+'\"'+a[c]+'\"',b=!1);d+=\"}\"}else a=a.replace(/\\\\/g,\"\\\\\\\\\"),a=a.replace(/\"/g,'\\\\\"'),d=d+'\"'+a+'\"';imraidview.showAlert(\"json:\"+d);return d};getPID=function(a){var d=\"\";null!=a&&(\"undefined\"!=typeof a.id&&null!=a.id)&&(d=a.id);return d};a.resize=function(){if(null==a.resizeProps)window.mraid.broadcastEvent(\"error\",\"Valid resize dimensions must be provided before calling resize\",\n\"resize\");else try{sdkController.resize(\"window.mraidview\")}catch(b){imraidview.showAlert(\"resize called in bridge\")}};a.createCalendarEvent=function(a){var d={};\"object\"!=typeof a&&window.mraid.broadcastEvent(\"error\",\"createCalendarEvent method expects parameter\",\"createCalendarEvent\");if(\"string\"!=typeof a.start||\"string\"!=typeof a.end)window.mraid.broadcastEvent(\"error\",\"createCalendarEvent method expects string parameters for start and end dates\",\"createCalendarEvent\");else{\"string\"!=typeof a.id&&\n(a.id=\"\");\"string\"!=typeof a.location&&(a.location=\"\");\"string\"!=typeof a.description&&(a.description=\"\");\"string\"!=typeof a.summary&&(a.summary=\"\");\"string\"==typeof a.status&&(\"pending\"==a.status||\"tentative\"==a.status||\"confirmed\"==a.status||\"cancelled\"==a.status)||(a.status=\"\");\"string\"==typeof a.transparency&&(\"opaque\"==a.transparency||\"transparent\"==a.transparency)||(a.transparency=\"\");if(null==a.recurrence||\"\"==a.recurrence)d={};else{\"string\"==typeof a.summary&&(d.frequency=a.recurrence.frequency);\nnull!=a.recurrence.interval&&(d.interval=a.recurrence.interval);\"string\"==typeof a.summary&&(d.expires=a.recurrence.expires);null!=a.recurrence.exceptionDates&&(d.exceptionDates=a.recurrence.exceptionDates);if(null!=a.recurrence.daysInWeek){var b=formatDaysInWeek(a.recurrence.daysInWeek);null!=b?d.daysInWeek=b:imraidview.showAlert(\"daysInWeek invalid format \")}d.daysInMonth=a.recurrence.daysInMonth;d.daysInYear=a.recurrence.daysInYear;d.weeksInMonth=a.recurrence.weeksInMonth;d.monthsInYear=a.recurrence.monthsInYear}\"string\"!=\ntypeof a.reminder&&(a.reminder=\"\");try{sdkController.createCalendarEvent(\"window.mraidview\",a.id,a.start,a.end,a.location,a.description,a.summary,a.status,a.transparency,JSON.stringify(d),a.reminder)}catch(c){sdkController.createCalendarEvent(\"window.mraidview\",a.start,a.end,a.location,a.description)}}};formatDaysInWeek=function(a){try{if(0!=a.length){for(var d=0;d<a.length;d++)switch(a[d]){case 0:a[d]=\"SU\";break;case 1:a[d]=\"MO\";break;case 2:a[d]=\"TU\";break;case 3:a[d]=\"WE\";break;case 4:a[d]=\"TH\";\nbreak;case 5:a[d]=\"FR\";break;case 6:a[d]=\"SA\";break;default:return null}return a}}catch(b){}return null};a.storePicture=function(b){console.log(\"bridge: storePicture\");if(\"string\"!=typeof b)window.mraid.broadcastEvent(\"error\",\"storePicture method expects url as string parameter\",\"storePicture\");else{if(a.supports(\"storePicture\"))return!window.confirm(\"Do you want to download the file?\")?(window.mraid.broadcastEvent(\"error\",\"Store picture on \"+b+\" was cancelled by user.\",\"storePicture\"),!1):sdkController.storePicture(\"window.mraidview\",\nb);window.mraid.broadcastEvent(\"error\",\"Store picture on \"+b+\" was cancelled because it is unsupported in this device/app.\",\"storePicture\")}};a.fireMediaTrackingEvent=function(a,d){};a.fireMediaErrorEvent=function(a,d){};a.fireMediaTimeUpdateEvent=function(a,d,b){};a.fireMediaCloseEvent=function(a,d,b){};a.fireMediaVolumeChangeEvent=function(a,d,b){};a.broadcastEvent=function(){window.mraid.broadcastEvent.apply(window.mraid,arguments)}})();\n(function(){var a=window.mraid=new InmobiObj,b=window.mraidview,c=!1;b.isAdShownToUser=!1;b.onUserInteraction=function(){c=!0};b.isPossibleFraud=function(){return a.supports(\"redirectFraudDetection\")&&(!b.isAdShownToUser||!c)};b.fireRedirectFraudBeacon=function(a){if(\"undefined\"!=typeof inmobi&&inmobi.recordEvent){var d={};d.trigger=a;d.isAdShown=b.isAdShownToUser.toString();inmobi.recordEvent(135,d)}};b.firePopupBlockedBeacon=function(a){if(\"undefined\"!=typeof inmobi&&inmobi.recordEvent){var d={};\nd.trigger=a;inmobi.recordEvent(136,d)}};window.onbeforeunload=function(){b.detectAndBlockFraud(\"redirect\")};a.addEventListener(\"viewableChange\",function(a){a&&!b.isAdShownToUser&&(b.isAdShownToUser=!0)});a.useCustomClose=b.useCustomClose;a.close=b.close;a.getExpandProperties=b.getExpandProperties;a.setExpandProperties=function(c){\"undefined\"!=typeof c&&(\"useCustomClose\"in c&&\"undefined\"!=typeof a.getState()&&\"expanded\"!=a.getState())&&a.useCustomClose(c.useCustomClose);b.setExpandProperties(c)};a.getResizeProperties=\nb.getResizeProperties;a.setResizeProperties=b.setResizeProperties;a.getOrientationProperties=b.getOrientationProperties;a.setOrientationProperties=b.setOrientationProperties;a.expand=b.expand;a.getMaxSize=b.getMaxSize;a.getState=b.getState;a.isViewable=b.isViewable;a.createCalendarEvent=function(a){b.detectAndBlockFraud(\"mraid.createCalendarEvent\")||b.createCalendarEvent(a)};a.open=function(c){b.detectAndBlockFraud(\"mraid.open\")||(\"string\"!=typeof c?a.broadcastEvent(\"error\",\"URL is required.\",\"open\"):\nb.open(c))};a.resize=b.resize;a.getVersion=function(){return\"2.0\"};a.getPlacementType=b.getPlacementType;a.playVideo=function(a){b.playVideo(a)};a.getScreenSize=b.getScreenSize;a.getCurrentPosition=b.getCurrentPosition;a.getDefaultPosition=b.getDefaultPosition;a.supports=function(a){return b.supports(a)};a.storePicture=function(c){\"string\"!=typeof c?a.broadcastEvent(\"error\",\"Request must specify a valid URL\",\"storePicture\"):b.storePicture(c)}})();\n(function(){var a=window.imraidview={},b,c=!0;a.setOrientationProperties=function(d){try{d?(\"undefined\"!=typeof d.allowOrientationChange&&(mraidview.orientationProperties.allowOrientationChange=d.allowOrientationChange),\"undefined\"!=typeof d.forceOrientation&&(mraidview.orientationProperties.forceOrientation=d.forceOrientation),\"undefined\"!=typeof d.direction&&(mraidview.orientationProperties.direction=d.direction)):d=null,sdkController.setOrientationProperties(\"window.imraidview\",mraidview.stringify(mraidview.orientationProperties))}catch(b){a.showAlert(\"setOrientationProperties: \"+\nb+\", props = \"+d)}};a.getOrientationProperties=function(){return mraidview.orientationProperties};a.getWindowOrientation=function(){var a=window.orientation;0>a&&(a+=360);window.innerWidth!==this.previousWidth&&0==a&&window.innerWidth>window.innerHeight&&(a=90);return a};var e=function(){window.setTimeout(function(){if(c||a.getWindowOrientation()!==b)c=!1,b=a.getWindowOrientation(),sdkController.onOrientationChange(\"window.imraidview\"),imraid.broadcastEvent(\"orientationChange\",b)},200)};a.registerOrientationListener=\nfunction(){b=a.getWindowOrientation();window.addEventListener(\"resize\",e,!1);window.addEventListener(\"orientationchange\",e,!1)};a.unRegisterOrientationListener=function(){window.removeEventListener(\"resize\",e,!1);window.removeEventListener(\"orientationchange\",e,!1)};window.imraidview.registerOrientationListener();a.firePostStatusEvent=function(a){window.imraid.broadcastEvent(\"postStatus\",a)};a.fireMediaTrackingEvent=function(a,b){var c={};c.name=a;var e=\"inmobi_media_\"+a;\"undefined\"!=typeof b&&(null!=\nb&&\"\"!=b)&&(e=e+\"_\"+b);window.imraid.broadcastEvent(e,c)};a.fireMediaErrorEvent=function(a,b){var c={name:\"error\"};c.code=b;var e=\"inmobi_media_\"+c.name;\"undefined\"!=typeof a&&(null!=a&&\"\"!=a)&&(e=e+\"_\"+a);window.imraid.broadcastEvent(e,c)};a.fireMediaTimeUpdateEvent=function(a,b,c){var e={name:\"timeupdate\",target:{}};e.target.currentTime=b;e.target.duration=c;b=\"inmobi_media_\"+e.name;\"undefined\"!=typeof a&&(null!=a&&\"\"!=a)&&(b=b+\"_\"+a);window.imraid.broadcastEvent(b,e)};a.saveContent=function(a,\nb,c){window.imraid.addEventListener(\"saveContent_\"+a,c);sdkController.saveContent(\"window.imraidview\",a,b)};a.cancelSaveContent=function(a){sdkController.cancelSaveContent(\"window.imraidview\",a)};a.disableCloseRegion=function(a){sdkController.disableCloseRegion(\"window.imraidview\",a)};a.fireGalleryImageSelectedEvent=function(a,b,c){var e=new Image;e.src=\"data:image/jpeg;base64,\"+a;e.width=b;e.height=c;window.imraid.broadcastEvent(\"galleryImageSelected\",e)};a.fireCameraPictureCatpturedEvent=function(a,\nb,c){var e=new Image;e.src=\"data:image/jpeg;base64,\"+a;e.width=b;e.height=c;window.imraid.broadcastEvent(\"cameraPictureCaptured\",e)};a.fireMediaCloseEvent=function(a,b,c){var e={name:\"close\"};e.viaUserInteraction=b;e.target={};e.target.currentTime=c;b=\"inmobi_media_\"+e.name;\"undefined\"!=typeof a&&(null!=a&&\"\"!=a)&&(b=b+\"_\"+a);window.imraid.broadcastEvent(b,e)};a.fireMediaVolumeChangeEvent=function(a,b,c){var e={name:\"volumechange\",target:{}};e.target.volume=b;e.target.muted=c;b=\"inmobi_media_\"+e.name;\n\"undefined\"!=typeof a&&(null!=a&&\"\"!=a)&&(b=b+\"_\"+a);window.imraid.broadcastEvent(b,e)};a.fireDeviceMuteChangeEvent=function(a){window.imraid.broadcastEvent(\"deviceMuted\",a)};a.fireDeviceVolumeChangeEvent=function(a){window.imraid.broadcastEvent(\"deviceVolumeChange\",a)};a.fireHeadphonePluggedEvent=function(a){window.imraid.broadcastEvent(\"headphones\",a)};a.showAlert=function(a){sdkController.showAlert(\"window.imraidview\",a)};a.openExternal=function(b,c){try{600<=getSdkVersionInt()?sdkController.openExternal(\"window.imraidview\",\nb,c):sdkController.openExternal(\"window.imraidview\",b)}catch(e){a.showAlert(\"openExternal: \"+e)}};a.log=function(b){try{sdkController.log(\"window.imraidview\",b)}catch(c){a.showAlert(\"log: \"+c)}};a.getPlatform=function(){return\"android\"};a.asyncPing=function(b){try{sdkController.asyncPing(\"window.imraidview\",b)}catch(c){a.showAlert(\"asyncPing: \"+c)}};a.startListeningDeviceMuteEvents=function(){sdkController.registerDeviceMuteEventListener(\"window.imraidview\")};a.stopListeningDeviceMuteEvents=function(){sdkController.unregisterDeviceMuteEventListener(\"window.imraidview\")};\na.startListeningDeviceVolumeChange=function(){sdkController.registerDeviceVolumeChangeEventListener(\"window.imraidview\")};a.stopListeningDeviceVolumeChange=function(){sdkController.unregisterDeviceVolumeChangeEventListener(\"window.imraidview\")};a.startListeningHeadphonePluggedEvents=function(){sdkController.registerHeadphonePluggedEventListener(\"window.imraidview\")};a.stopListeningHeadphonePluggedEvents=function(){sdkController.unregisterHeadphonePluggedEventListener(\"window.imraidview\")};getSdkVersionInt=\nfunction(){for(var b=a.getSdkVersion().split(\".\"),c=b.length,e=\"\",f=0;f<c;f++)e+=b[f];return parseInt(e)};a.getSdkVersion=function(){return window._im_imaiview.getSdkVersion()};a.supports=function(a){console.log(\"bridge: supports (IMRAID)\");if(\"string\"!=typeof a)window.imraid.broadcastEvent(\"error\",\"Supports method expects string parameter\",\"supports\");else return\"false\"!=sdkController.supports(\"window.imraidview\",a)};a.postToSocial=function(a,b,c,e){a=parseInt(a);isNaN(a)?window.imraid.broadcastEvent(\"error\",\n\"socialType must be an integer\",\"postToSocial\"):(\"string\"!=typeof b&&(b=\"\"),\"string\"!=typeof c&&(c=\"\"),\"string\"!=typeof e&&(e=\"\"),sdkController.postToSocial(\"window.imraidview\",a,b,c,e))};a.incentCompleted=function(a){if(\"object\"!=typeof a||null==a)sdkController.incentCompleted(\"window.imraidview\",null);else try{sdkController.incentCompleted(\"window.imraidview\",JSON.stringify(a))}catch(b){sdkController.incentCompleted(\"window.imraidview\",null)}};a.getOrientation=function(){try{return String(sdkController.getOrientation(\"window.imraidview\"))}catch(b){a.showAlert(\"getOrientation: \"+\nb)}};a.acceptAction=function(b){try{sdkController.acceptAction(\"window.imraidview\",mraidview.stringify(b))}catch(c){a.showAlert(\"acceptAction: \"+c+\", params = \"+b)}};a.rejectAction=function(b){try{sdkController.rejectAction(\"window.imraidview\",mraidview.stringify(b))}catch(c){a.showAlert(\"rejectAction: \"+c+\", params = \"+b)}};a.updateToPassbook=function(b){window.imraid.broadcastEvent(\"error\",\"Method not supported\",\"updateToPassbook\");a.log(\"Method not supported\")};a.isDeviceMuted=function(){return\"false\"!=\nsdkController.isDeviceMuted(\"window.imraidview\")};a.getDeviceVolume=function(){return 603>=getSdkVersionInt()?-1:sdkController.getDeviceVolume(\"window.imraidview\")};a.isHeadPhonesPlugged=function(){return\"false\"!=sdkController.isHeadphonePlugged(\"window.imraidview\")};a.sendSaveContentResult=function(){window.imraid.sendSaveContentResult.apply(window.imraid,arguments)};a.broadcastEvent=function(){window.imraid.broadcastEvent.apply(window.imraid,arguments)};a.disableBackButton=function(a){void 0==a||\n\"boolean\"!=typeof a?console.log(\"disableBackButton called with invalid params\"):sdkController.disableBackButton(\"window.imraidview\",a)};a.isBackButtonDisabled=function(){return sdkController.isBackButtonDisabled(\"window.imraidview\")};a.startListeningForBackButtonPressedEvent=function(){sdkController.registerBackButtonPressedEventListener(\"window.imraidview\")};a.stopListeningForBackButtonPressedEvent=function(){sdkController.unregisterBackButtonPressedEventListener(\"window.imraidview\")};a.hideStatusBar=\nfunction(){};a.setOpaqueBackground=function(){};a.startDownloader=function(a,b,c){682<=getSdkVersionInt()&&sdkController.startDownloader(\"window.imraidview\",a,b,c)};a.registerDownloaderCallbacks=function(){682<=getSdkVersionInt()&&sdkController.registerDownloaderCallbacks(\"window.imraidview\")};a.unregisterDownloaderCallbacks=function(){682<=getSdkVersionInt()&&sdkController.unregisterDownloaderCallbacks(\"window.imraidview\")};a.getDownloadProgress=function(){return 682<=getSdkVersionInt()?sdkController.getDownloadProgress(\"window.imraidview\"):\n-1};a.getDownloadStatus=function(){return 682<=getSdkVersionInt()?sdkController.getDownloadStatus(\"window.imraidview\"):-1};a.fireEvent=function(a){700<=getSdkVersionInt()&&(\"fireSkip\"===a?sdkController.fireSkip(\"window.imraidview\"):\"fireComplete\"===a?sdkController.fireComplete(\"window.imraidview\"):\"showEndCard\"===a&&sdkController.showEndCard(\"window.imraidview\"))};a.saveBlob=function(a){700<=getSdkVersionInt()&&sdkController.saveBlob(\"window.imraidview\",a)};a.getBlob=function(a,b){700<=getSdkVersionInt()&&\nsdkController.getBlob(a,b)};a.setCloseEndCardTracker=function(a){700<=getSdkVersionInt()&&sdkController.setCloseEndCardTracker(\"window.imraidview\",a)}})();\n(function(){var a=window.imraid=new InmobiObj,b=window.imraidview;a.getOrientation=b.getOrientation;a.setOrientationProperties=b.setOrientationProperties;a.getOrientationProperties=b.getOrientationProperties;a.saveContentIDMap={};a.saveContent=function(c,e,d){var k=arguments.length,h,f=null;if(3>k){if(\"function\"===typeof arguments[k-1])h=arguments[k-1];else return;f={reason:1}}else a.saveContentIDMap[c]&&(h=arguments[2],f={reason:11,url:arguments[1]});\"function\"!==!h&&(f?(window.imraid.addEventListener(\"saveContent_failed_\"+\nc,h),window.imraid.sendSaveContentResult(\"saveContent_failed_\"+c,\"failed\",JSON.stringify(f))):(a.removeEventListener(\"saveContent_\"+c),a.saveContentIDMap[c]=!0,b.saveContent(c,e,d)))};a.cancelSaveContent=function(a){b.cancelSaveContent(a)};a.asyncPing=function(c){\"string\"!=typeof c?a.broadcastEvent(\"error\",\"URL is required.\",\"asyncPing\"):b.asyncPing(c)};a.disableCloseRegion=b.disableCloseRegion;a.getSdkVersion=b.getSdkVersion;a.log=function(c){\"undefined\"==typeof c?a.broadcastEvent(\"error\",\"message is required.\",\n\"log\"):\"string\"==typeof c?b.log(c):b.log(JSON.stringify(c))};a.getInMobiAIVersion=function(){return\"2.0\"};a.getVendorName=function(){return\"inmobi\"};a.openExternal=function(a,e){mraidview.detectAndBlockFraud(\"imraid.openExternal\")||b.openExternal(a,e)};a.updateToPassbook=function(c){mraidview.detectAndBlockFraud(\"imraid.updateToPassbook\")||(\"string\"!=typeof c?a.broadcastEvent(\"error\",\"Request must specify a valid URL\",\"updateToPassbook\"):b.updateToPassbook(c))};a.postToSocial=function(a,e,d,k){mraidview.detectAndBlockFraud(\"imraid.postToSocial\")||\nb.postToSocial(a,e,d,k)};a.getPlatform=b.getPlatform;a.incentCompleted=b.incentCompleted;a.loadSKStore=b.loadSKStore;a.showSKStore=function(a){mraidview.detectAndBlockFraud(\"imraid.showSKStore\")||b.showSKStore(a)};a.supports=function(a){return b.supports(a)};a.isDeviceMuted=function(){return!imIsObjValid(a.listeners.deviceMuted)?-1:b.isDeviceMuted()};a.isHeadPhonesPlugged=function(){return!imIsObjValid(a.listeners.headphones)?!1:b.isHeadPhonesPlugged()};a.getDeviceVolume=function(){return b.getDeviceVolume()};\na.setDeviceVolume=function(a){b.setDeviceVolume(a)};a.hideStatusBar=function(){b.hideStatusBar()};a.setOpaqueBackground=function(){b.setOpaqueBackground()};a.disableBackButton=b.disableBackButton;a.isBackButtonDisabled=b.isBackButtonDisabled;a.startDownloader=b.startDownloader;a.getDownloadProgress=b.getDownloadProgress;a.getDownloadStatus=b.getDownloadStatus;a.fireEvent=b.fireEvent;a.saveBlob=b.saveBlob;a.getBlob=b.getBlob;a.setCloseEndCardTracker=b.setCloseEndCardTracker})();\n(function(){var a=window._im_imaiview={ios:{}};window.imaiview=a;a.broadcastEvent=function(){for(var a=Array(arguments.length),c=0;c<arguments.length;c++)a[c]=arguments[c];c=a.shift();try{window.mraid.broadcastEvent(c,a)}catch(e){}};a.getPlatform=function(){return\"android\"};a.getPlatformVersion=function(){return sdkController.getPlatformVersion(\"window.imaiview\")};a.log=function(a){sdkController.log(\"window.imaiview\",a)};a.openEmbedded=function(a){sdkController.openEmbedded(\"window.imaiview\",a)};\na.openExternal=function(a,c){600<=getSdkVersionInt()?sdkController.openExternal(\"window.imaiview\",a,c):sdkController.openExternal(\"window.imaiview\",a)};a.ping=function(a,c){sdkController.ping(\"window.imaiview\",a,c)};a.pingInWebView=function(a,c){sdkController.pingInWebView(\"window.imaiview\",a,c)};a.getSdkVersion=function(){try{var a=sdkController.getSdkVersion(\"window.imaiview\");if(\"string\"==typeof a&&null!=a)return a}catch(c){return\"3.7.0\"}};a.onUserInteraction=function(a){if(\"object\"!=typeof a||\nnull==a)sdkController.onUserInteraction(\"window.imaiview\",null);else try{sdkController.onUserInteraction(\"window.imaiview\",JSON.stringify(a))}catch(c){sdkController.onUserInteraction(\"window.imaiview\",null)}};a.fireAdReady=function(){sdkController.fireAdReady(\"window.imaiview\")};a.fireAdFailed=function(){sdkController.fireAdFailed(\"window.imaiview\")};a.broadcastEvent=function(){window.imai.broadcastEvent.apply(window.imai,arguments)}})();\n(function(){var a=window._im_imaiview;window._im_imai=new InmobiObj;window._im_imai.ios=new InmobiObj;var b=window._im_imai;window.imai=window._im_imai;b.matchString=function(a,b){if(\"string\"!=typeof a||null==a||null==b)return-1;var d=-1;try{d=a.indexOf(b)}catch(k){}return d};b.isHttpUrl=function(a){return\"string\"!=typeof a||null==a?!1:0==b.matchString(a,\"http://\")?!0:0==b.matchString(a,\"https://\")?!0:!1};b.appendTapParams=function(a,e,d){if(!imIsObjValid(e)||!imIsObjValid(d))return a;b.isHttpUrl(a)&&\n(a=-1==b.matchString(a,\"?\")?a+(\"?u-tap-o=\"+e+\",\"+d):a+(\"&u-tap-o=\"+e+\",\"+d));return a};b.performAdClick=function(a,e){e=e||event;if(imIsObjValid(a)){var d=a.clickConfig,k=a.landingConfig;if(!imIsObjValid(d)&&!imIsObjValid(k))b.log(\"click/landing config are invalid, Nothing to process .\"),this.broadcastEvent(\"error\",\"click/landing config are invalid, Nothing to process .\");else{var h=null,f=null,g=null,m=null,n=null,l=null,q=null,p=null;if(imIsObjValid(e))try{m=e.changedTouches[0].pageX,n=e.changedTouches[0].pageY}catch(r){n=\nm=0}imIsObjValid(k)?imIsObjValid(d)?(l=k.url,q=k.fallbackUrl,p=k.urlType,h=d.url,f=d.pingWV,g=d.fr):(l=k.url,p=k.urlType):(l=d.url,p=d.urlType);d=b.getPlatform();try{if(\"boolean\"!=typeof g&&\"number\"!=typeof g||null==g)g=!0;if(0>g||1<g)g=!0;if(\"boolean\"!=typeof f&&\"number\"!=typeof f||null==f)f=!0;if(0>f||1<f)f=!0;if(\"number\"!=typeof p||null==p)p=0;h=b.appendTapParams(h,m,n);imIsObjValid(h)?!0==f?b.pingInWebView(h,g):b.ping(h,g):b.log(\"clickurl provided is null.\");if(imIsObjValid(l))switch(imIsObjValid(h)||\n(l=b.appendTapParams(l,m,n)),p){case 1:b.openEmbedded(l);break;case 2:\"ios\"==d?b.ios.openItunesProductView(l):this.broadcastEvent(\"error\",\"Cannot process openItunesProductView for os\"+d);break;default:b.openExternal(l,q)}else b.log(\"Landing url provided is null.\")}catch(s){}}}else b.log(\" invalid config, nothing to process .\"),this.broadcastEvent(\"error\",\"invalid config, nothing to process .\")};b.performActionClick=function(a,e){e=e||event;if(imIsObjValid(a)){var d=a.clickConfig,k=a.landingConfig;\nif(!imIsObjValid(d)&&!imIsObjValid(k))b.log(\"click/landing config are invalid, Nothing to process .\"),this.broadcastEvent(\"error\",\"click/landing config are invalid, Nothing to process .\");else{var h=null,f=null,g=null,m=null,n=null;if(imIsObjValid(e))try{m=e.changedTouches[0].pageX,n=e.changedTouches[0].pageY}catch(l){n=m=0}imIsObjValid(d)&&(h=d.url,f=d.pingWV,g=d.fr);try{if(\"boolean\"!=typeof g&&\"number\"!=typeof g||null==g)g=!0;if(0>g||1<g)g=!0;if(\"boolean\"!=typeof f&&\"number\"!=typeof f||null==f)f=\n!0;if(0>f||1<f)f=!0;h=b.appendTapParams(h,m,n);imIsObjValid(h)?!0==f?b.pingInWebView(h,g):b.ping(h,g):b.log(\"clickurl provided is null.\");b.onUserInteraction(k)}catch(q){}}}else b.log(\" invalid config, nothing to process .\"),this.broadcastEvent(\"error\",\"invalid config, nothing to process .\")};b.getVersion=function(){return\"1.0\"};b.getPlatform=a.getPlatform;b.getPlatformVersion=a.getPlatformVersion;b.log=a.log;b.openEmbedded=function(b){mraidview.detectAndBlockFraud(\"imai.openEmbedded\")||a.openEmbedded(b)};\nb.openExternal=function(b,e){mraidview.detectAndBlockFraud(\"imai.openExternal\")||a.openExternal(b,e)};b.ping=a.ping;b.pingInWebView=a.pingInWebView;b.onUserInteraction=a.onUserInteraction;b.getSdkVersion=a.getSdkVersion;b.loadSKStore=a.loadSKStore;b.showSKStore=function(b){mraidview.detectAndBlockFraud(\"imai.showSKStore\")||a.showSKStore(b)};b.ios.openItunesProductView=function(b){mraidview.detectAndBlockFraud(\"imai.ios.openItunesProductView\")||a.ios.openItunesProductView(b)};b.fireAdReady=a.fireAdReady;\nb.fireAdFailed=a.fireAdFailed})();";
    }
    return (String)localObject;
  }
  
  public final g getOrientationProperties()
  {
    return I;
  }
  
  public final RenderView getOriginalRenderView()
  {
    return y;
  }
  
  public final Activity getPubActivity()
  {
    WeakReference localWeakReference = z;
    if (localWeakReference == null) {
      return null;
    }
    return (Activity)localWeakReference.get();
  }
  
  public final AdContainer getReferenceContainer()
  {
    return V;
  }
  
  public final b.h getRenderingConfig()
  {
    return D.m;
  }
  
  public final AdContainer.RenderingProperties getRenderingProperties()
  {
    return e;
  }
  
  public final h getResizeProperties()
  {
    return H;
  }
  
  public final String getState()
  {
    return d;
  }
  
  public final View getVideoContainerView()
  {
    return null;
  }
  
  public final String getViewState()
  {
    return d;
  }
  
  public final bw getViewableAd()
  {
    Object localObject1 = ab;
    if (localObject1 == null)
    {
      localObject1 = new com/inmobi/ads/bx;
      ((bx)localObject1).<init>(this);
      ab = ((bw)localObject1);
      localObject1 = getFullScreenActivity();
      if (localObject1 == null) {
        localObject1 = getPubActivity();
      } else {
        localObject1 = getFullScreenActivity();
      }
      Object localObject2 = aa;
      if (localObject2 != null)
      {
        if (localObject1 != null) {
          try
          {
            Iterator localIterator = ((Set)localObject2).iterator();
            for (;;)
            {
              boolean bool1 = localIterator.hasNext();
              if (!bool1) {
                break;
              }
              localObject2 = localIterator.next();
              localObject2 = (bm)localObject2;
              int i1 = a;
              int i2 = 1;
              label228:
              Object localObject6;
              if (i1 != i2)
              {
                int i3 = 3;
                if (i1 == i3)
                {
                  localObject3 = b;
                  Object localObject4 = "avidAdSession";
                  localObject3 = ((Map)localObject3).get(localObject4);
                  Object localObject5 = localObject3;
                  localObject5 = (com.b.a.a.a.f.a)localObject3;
                  localObject3 = b;
                  localObject4 = "deferred";
                  boolean bool2 = ((Map)localObject3).containsKey(localObject4);
                  if (bool2)
                  {
                    localObject2 = b;
                    localObject3 = "deferred";
                    localObject2 = ((Map)localObject2).get(localObject3);
                    localObject2 = (Boolean)localObject2;
                    bool1 = ((Boolean)localObject2).booleanValue();
                    if (bool1)
                    {
                      bool3 = true;
                      break label228;
                    }
                  }
                  bool1 = false;
                  localObject2 = null;
                  boolean bool3 = false;
                  if (localObject5 != null)
                  {
                    q localq = new com/inmobi/ads/q;
                    localObject4 = ab;
                    localObject2 = localq;
                    localObject3 = this;
                    localObject6 = localObject1;
                    localq.<init>(this, (Activity)localObject1, (bw)localObject4, (com.b.a.a.a.f.a)localObject5, bool3);
                    ab = localq;
                  }
                }
              }
              else
              {
                localObject3 = new com/inmobi/ads/z;
                localObject6 = ab;
                localObject2 = b;
                ((z)localObject3).<init>(this, (Activity)localObject1, (bw)localObject6, (Map)localObject2);
                ab = ((bw)localObject3);
              }
            }
            localHashMap = new java/util/HashMap;
          }
          catch (Exception localException)
          {
            localException.getMessage();
          }
        }
        HashMap localHashMap;
        localHashMap.<init>();
        Object localObject3 = getMarkupType();
        localHashMap.put("type", localObject3);
        localObject2 = getImpressionId();
        if (localObject2 != null)
        {
          localObject2 = "impId";
          localObject3 = getImpressionId();
          localHashMap.put(localObject2, localObject3);
        }
        com.inmobi.commons.core.e.b.a();
        localObject2 = "ads";
        localObject3 = "TrackersForService";
        com.inmobi.commons.core.e.b.a((String)localObject2, (String)localObject3, localHashMap);
      }
    }
    return ab;
  }
  
  public final void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    boolean bool = isHardwareAccelerated();
    F = bool;
    WeakReference localWeakReference = B;
    if (localWeakReference == null)
    {
      localWeakReference = new java/lang/ref/WeakReference;
      ViewGroup localViewGroup = (ViewGroup)getParent();
      localWeakReference.<init>(localViewGroup);
      B = localWeakReference;
    }
  }
  
  public final void onDetachedFromWindow()
  {
    E.clear();
    getMediaProcessor().b();
    getMediaProcessor().c();
    getMediaProcessor().e();
    i locali = i;
    Object localObject1 = getContainerContext();
    locali.a((Context)localObject1);
    try
    {
      super.onDetachedFromWindow();
      return;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      localIllegalArgumentException.getMessage();
      try
      {
        localObject1 = new java/util/HashMap;
        ((HashMap)localObject1).<init>();
        String str2 = "type";
        Object localObject2 = "IllegalArgumentException";
        ((Map)localObject1).put(str2, localObject2);
        str2 = "message";
        localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>();
        String str1 = localIllegalArgumentException.getMessage();
        ((StringBuilder)localObject2).append(str1);
        str1 = ((StringBuilder)localObject2).toString();
        ((Map)localObject1).put(str2, str1);
        com.inmobi.commons.core.e.b.a();
        str1 = "ads";
        str2 = "ExceptionCaught";
        com.inmobi.commons.core.e.b.a(str1, str2, (Map)localObject1);
        return;
      }
      catch (Exception localException)
      {
        localException.getMessage();
      }
    }
  }
  
  public final boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
  {
    paramMotionEvent.getAction();
    c("window.mraidview.onUserInteraction();");
    return super.onInterceptTouchEvent(paramMotionEvent);
  }
  
  public final void onScreenStateChanged(int paramInt)
  {
    super.onScreenStateChanged(paramInt);
    if (paramInt == 0)
    {
      c(false);
      return;
    }
    paramInt = T;
    if (paramInt == 0)
    {
      paramInt = 1;
      c(paramInt);
    }
  }
  
  protected final void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    if ((paramInt1 != 0) && (paramInt2 != 0))
    {
      paramInt1 = com.inmobi.commons.core.utilities.b.c.b(paramInt1);
      paramInt2 = com.inmobi.commons.core.utilities.b.c.b(paramInt2);
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      String str1 = "window.mraidview.broadcastEvent('sizeChange',";
      localStringBuilder.<init>(str1);
      localStringBuilder.append(paramInt1);
      localStringBuilder.append(",");
      localStringBuilder.append(paramInt2);
      localStringBuilder.append(");");
      String str2 = localStringBuilder.toString();
      c(str2);
    }
  }
  
  public final void onWindowFocusChanged(boolean paramBoolean)
  {
    super.onWindowFocusChanged(paramBoolean);
    boolean bool = paramBoolean ^ true;
    T = bool;
    c(paramBoolean);
  }
  
  public final void onWindowVisibilityChanged(int paramInt)
  {
    super.onWindowVisibilityChanged(paramInt);
    if (paramInt == 0) {
      paramInt = 1;
    } else {
      paramInt = 0;
    }
    int i1 = o;
    if (i1 == paramInt) {
      return;
    }
    d(paramInt);
  }
  
  public final void setAdActiveFlag(boolean paramBoolean)
  {
    n = paramBoolean;
  }
  
  public final void setAndUpdateViewState(String paramString)
  {
    d = paramString;
    paramString = d;
    Object localObject = Locale.ENGLISH;
    paramString = paramString.toLowerCase((Locale)localObject);
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("window.mraidview.broadcastEvent('stateChange','");
    ((StringBuilder)localObject).append(paramString);
    ((StringBuilder)localObject).append("');");
    paramString = ((StringBuilder)localObject).toString();
    c(paramString);
  }
  
  public final void setBlobProvider(a parama)
  {
    u = parama;
  }
  
  final void setCloseEndCardTracker(String paramString)
  {
    Object localObject1 = getReferenceContainer();
    boolean bool = localObject1 instanceof ba;
    if (bool)
    {
      localObject1 = (NativeVideoWrapper)((ba)localObject1).getVideoContainerView();
      if (localObject1 != null)
      {
        localObject1 = (bb)((NativeVideoWrapper)localObject1).getVideoView().getTag();
        if (localObject1 != null)
        {
          Object localObject2 = ((bb)localObject1).b();
          if (localObject2 != null)
          {
            localObject2 = ((bb)localObject1).b().f();
            if (localObject2 != null)
            {
              localObject1 = ((bb)localObject1).b().f();
              localObject2 = new com/inmobi/ads/NativeTracker;
              NativeTracker.TrackerEventType localTrackerEventType = NativeTracker.TrackerEventType.TRACKER_EVENT_TYPE_END_CARD_CLOSE;
              ((NativeTracker)localObject2).<init>(paramString, 0, localTrackerEventType, null);
              ((bp)localObject1).a((NativeTracker)localObject2);
            }
          }
        }
      }
    }
  }
  
  public final void setCloseRegionDisabled(boolean paramBoolean)
  {
    p = paramBoolean;
  }
  
  public final void setCurrentPosition()
  {
    ??? = new org/json/JSONObject;
    ((JSONObject)???).<init>();
    K = ((JSONObject)???);
    int i1 = 2;
    ??? = new int[i1];
    getLocationOnScreen((int[])???);
    Object localObject2 = null;
    for (;;)
    {
      try
      {
        JSONObject localJSONObject = K;
        Object localObject4 = "x";
        int i2 = ???[0];
        i2 = com.inmobi.commons.core.utilities.b.c.b(i2);
        localJSONObject.put((String)localObject4, i2);
        localJSONObject = K;
        localObject4 = "y";
        i2 = 1;
        i1 = ???[i2];
        i1 = com.inmobi.commons.core.utilities.b.c.b(i1);
        localJSONObject.put((String)localObject4, i1);
        i1 = getWidth();
        i1 = com.inmobi.commons.core.utilities.b.c.b(i1);
        int i3 = getHeight();
        i3 = com.inmobi.commons.core.utilities.b.c.b(i3);
        localObject4 = K;
        String str = "width";
        ((JSONObject)localObject4).put(str, i1);
        ??? = K;
        localObject4 = "height";
        ((JSONObject)???).put((String)localObject4, i3);
      }
      catch (JSONException localJSONException)
      {
        continue;
      }
      synchronized (O)
      {
        l = false;
        localObject2 = O;
        localObject2.notifyAll();
        return;
      }
    }
  }
  
  public final void setCurrentPositionLock()
  {
    l = true;
  }
  
  public final void setDefaultPosition()
  {
    int i1 = 2;
    ??? = new int[i1];
    Object localObject2 = new org/json/JSONObject;
    ((JSONObject)localObject2).<init>();
    J = ((JSONObject)localObject2);
    localObject2 = B;
    if (localObject2 == null)
    {
      localObject2 = new java/lang/ref/WeakReference;
      localViewGroup = (ViewGroup)getParent();
      ((WeakReference)localObject2).<init>(localViewGroup);
      B = ((WeakReference)localObject2);
    }
    localObject2 = B.get();
    ViewGroup localViewGroup = null;
    if (localObject2 != null)
    {
      localObject2 = (ViewGroup)B.get();
      ((ViewGroup)localObject2).getLocationOnScreen((int[])???);
    }
    try
    {
      localObject2 = J;
      Object localObject4 = "x";
      int i2 = ???[0];
      i2 = com.inmobi.commons.core.utilities.b.c.b(i2);
      ((JSONObject)localObject2).put((String)localObject4, i2);
      localObject2 = J;
      localObject4 = "y";
      i2 = 1;
      i1 = ???[i2];
      i1 = com.inmobi.commons.core.utilities.b.c.b(i1);
      ((JSONObject)localObject2).put((String)localObject4, i1);
      ??? = B;
      ??? = ((WeakReference)???).get();
      ??? = (ViewGroup)???;
      i1 = ((ViewGroup)???).getWidth();
      i1 = com.inmobi.commons.core.utilities.b.c.b(i1);
      localObject2 = B;
      localObject2 = ((WeakReference)localObject2).get();
      localObject2 = (ViewGroup)localObject2;
      int i3 = ((ViewGroup)localObject2).getHeight();
      i3 = com.inmobi.commons.core.utilities.b.c.b(i3);
      localObject4 = J;
      String str = "width";
      ((JSONObject)localObject4).put(str, i1);
      ??? = J;
      localObject4 = "height";
      ((JSONObject)???).put((String)localObject4, i3);
    }
    catch (JSONException localJSONException)
    {
      for (;;) {}
    }
    ??? = J;
    localObject2 = "x";
    ((JSONObject)???).put((String)localObject2, 0);
    ??? = J;
    localObject2 = "y";
    ((JSONObject)???).put((String)localObject2, 0);
    ??? = J;
    localObject2 = "width";
    ((JSONObject)???).put((String)localObject2, 0);
    ??? = J;
    localObject2 = "height";
    ((JSONObject)???).put((String)localObject2, 0);
    synchronized (N)
    {
      k = false;
      localObject2 = N;
      localObject2.notifyAll();
      return;
    }
  }
  
  public final void setDefaultPositionLock()
  {
    k = true;
  }
  
  public final void setDisableBackButton(boolean paramBoolean)
  {
    q = paramBoolean;
  }
  
  public final void setExitAnimation(int paramInt)
  {
    S = paramInt;
  }
  
  public final void setExpandProperties(com.inmobi.rendering.mraid.b paramb)
  {
    boolean bool = b;
    if (bool)
    {
      bool = a;
      setUseCustomClose(bool);
    }
    G = paramb;
  }
  
  public final void setFullScreenActivityContext(Activity paramActivity)
  {
    WeakReference localWeakReference = new java/lang/ref/WeakReference;
    localWeakReference.<init>(paramActivity);
    b = localWeakReference;
    paramActivity = I;
    if (paramActivity != null) {
      setOrientationProperties(paramActivity);
    }
  }
  
  final void setIsInAppBrowser(boolean paramBoolean)
  {
    A = paramBoolean;
  }
  
  public final void setIsPreload(boolean paramBoolean)
  {
    v = paramBoolean;
  }
  
  public final void setOrientationProperties(g paramg)
  {
    I = paramg;
    Object localObject = b;
    if (localObject != null)
    {
      localObject = ((WeakReference)localObject).get();
      if (localObject != null)
      {
        boolean bool1 = a;
        if (!bool1)
        {
          localObject = b;
          int i2 = -1;
          int i3 = ((String)localObject).hashCode();
          int i4 = 729267099;
          int i5 = 1;
          String str1;
          String str2;
          if (i3 != i4)
          {
            i4 = 1430647483;
            if (i3 == i4)
            {
              str1 = "landscape";
              bool1 = ((String)localObject).equals(str1);
              if (bool1)
              {
                i2 = 0;
                str2 = null;
              }
            }
          }
          else
          {
            str1 = "portrait";
            bool1 = ((String)localObject).equals(str1);
            if (bool1) {
              i2 = 1;
            }
          }
          int i1 = 9;
          i3 = 4;
          i4 = 2;
          int i6 = 8;
          int i7 = 3;
          int i8;
          switch (i2)
          {
          default: 
            i8 = com.inmobi.commons.core.utilities.b.c.b();
            if (i8 == i4)
            {
              ((Activity)b.get()).setRequestedOrientation(i1);
              return;
            }
            break;
          case 1: 
            i8 = com.inmobi.commons.core.utilities.b.c.b();
            if (i8 == i4)
            {
              ((Activity)b.get()).setRequestedOrientation(i1);
              return;
            }
            ((Activity)b.get()).setRequestedOrientation(i5);
            return;
          case 0: 
            i1 = com.inmobi.commons.core.utilities.b.c.b();
            if (i1 != i7)
            {
              i1 = com.inmobi.commons.core.utilities.b.c.b();
              if (i1 != i3) {
                i5 = 0;
              }
            }
            if (i5 != 0)
            {
              i8 = com.inmobi.commons.core.utilities.b.c.b();
              if (i7 == i8)
              {
                ((Activity)b.get()).setRequestedOrientation(0);
                return;
              }
              ((Activity)b.get()).setRequestedOrientation(i6);
              return;
            }
            localObject = c;
            str2 = "left";
            boolean bool2 = ((String)localObject).equals(str2);
            if (bool2)
            {
              ((Activity)b.get()).setRequestedOrientation(i6);
              return;
            }
            paramg = c;
            localObject = "right";
            boolean bool3 = paramg.equals(localObject);
            if (!bool3) {
              return;
            }
            ((Activity)b.get()).setRequestedOrientation(0);
            return;
          }
          int i9 = com.inmobi.commons.core.utilities.b.c.b();
          if (i9 == i3)
          {
            ((Activity)b.get()).setRequestedOrientation(i6);
            return;
          }
          i9 = com.inmobi.commons.core.utilities.b.c.b();
          if (i9 == i7)
          {
            ((Activity)b.get()).setRequestedOrientation(0);
            return;
          }
          paramg = (Activity)b.get();
          paramg.setRequestedOrientation(i5);
        }
      }
    }
  }
  
  public final void setOriginalRenderView(RenderView paramRenderView)
  {
    y = paramRenderView;
  }
  
  public final void setReferenceContainer(AdContainer paramAdContainer)
  {
    V = paramAdContainer;
  }
  
  public final void setRenderViewEventListener(RenderView.a parama)
  {
    c = parama;
  }
  
  public final void setRequestedScreenOrientation()
  {
    Object localObject = getFullScreenActivity();
    if (localObject != null)
    {
      localObject = I;
      if (localObject != null) {
        setOrientationProperties((g)localObject);
      }
    }
  }
  
  public final void setResizeProperties(h paramh)
  {
    H = paramh;
  }
  
  public final void setScrollable(boolean paramBoolean)
  {
    setScrollContainer(paramBoolean);
    setVerticalScrollBarEnabled(paramBoolean);
    setHorizontalScrollBarEnabled(paramBoolean);
  }
  
  public final void setUseCustomClose(boolean paramBoolean)
  {
    m = paramBoolean;
  }
  
  public final void stopLoading()
  {
    AtomicBoolean localAtomicBoolean = s;
    boolean bool = localAtomicBoolean.get();
    if (!bool) {
      super.stopLoading();
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.rendering.RenderView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */