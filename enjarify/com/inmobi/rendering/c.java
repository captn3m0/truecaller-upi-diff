package com.inmobi.rendering;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.provider.Settings.System;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewTreeObserver;
import android.webkit.URLUtil;
import android.widget.FrameLayout;
import com.google.android.gms.plus.PlusShare.Builder;
import com.inmobi.ads.AdContainer.RenderingProperties;
import com.inmobi.ads.AdContainer.RenderingProperties.PlacementType;
import com.inmobi.ads.ad;
import com.inmobi.ads.ad.6;
import com.inmobi.ads.ag;
import com.inmobi.ads.b.h;
import com.inmobi.ads.bn;
import com.inmobi.ads.cache.AssetStore;
import com.inmobi.ads.cache.AssetStore.4;
import com.inmobi.commons.core.network.a.a;
import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;
import com.inmobi.commons.core.utilities.b.d;
import com.inmobi.rendering.mraid.MraidMediaProcessor;
import com.inmobi.rendering.mraid.MraidMediaProcessor.HeadphonesPluggedChangeReceiver;
import com.inmobi.rendering.mraid.MraidMediaProcessor.RingerModeChangeReceiver;
import com.inmobi.rendering.mraid.MraidMediaProcessor.a;
import com.inmobi.rendering.mraid.g;
import com.inmobi.rendering.mraid.h;
import com.inmobi.rendering.mraid.i;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import org.json.JSONException;
import org.json.JSONObject;

public class c
{
  static final String[] a = tmp18_5;
  private static final String b = "c";
  private RenderView c;
  private AdContainer.RenderingProperties d;
  private g e;
  
  static
  {
    String[] tmp4_1 = new String[4];
    String[] tmp5_4 = tmp4_1;
    String[] tmp5_4 = tmp4_1;
    tmp5_4[0] = "tel";
    tmp5_4[1] = "sms";
    tmp5_4[2] = "calendar";
    String[] tmp18_5 = tmp5_4;
    tmp18_5[3] = "inlineVideo";
  }
  
  c(RenderView paramRenderView, AdContainer.RenderingProperties paramRenderingProperties)
  {
    c = paramRenderView;
    d = paramRenderingProperties;
  }
  
  public void asyncPing(String paramString1, String paramString2)
  {
    boolean bool = URLUtil.isValidUrl(paramString2);
    if (!bool)
    {
      c.b(paramString1, "Invalid url", "asyncPing");
      return;
    }
    try
    {
      Object localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      Object localObject2 = "command";
      String str = "ping";
      ((Map)localObject1).put(localObject2, str);
      localObject2 = "scheme";
      str = bn.a(paramString1);
      ((Map)localObject1).put(localObject2, str);
      localObject2 = c;
      str = "ads";
      Object localObject3 = "CreativeInvokedAction";
      localObject2 = c;
      ((RenderView.a)localObject2).b(str, (String)localObject3, (Map)localObject1);
      localObject1 = new com/inmobi/commons/core/network/c;
      localObject2 = "GET";
      ((com.inmobi.commons.core.network.c)localObject1).<init>((String)localObject2, paramString2);
      paramString2 = null;
      t = false;
      long l = SystemClock.elapsedRealtime();
      paramString2 = new com/inmobi/commons/core/network/a;
      localObject3 = new com/inmobi/rendering/c$3;
      ((c.3)localObject3).<init>(this, (com.inmobi.commons.core.network.c)localObject1, l);
      paramString2.<init>((com.inmobi.commons.core.network.c)localObject1, (a.a)localObject3);
      paramString2.a();
      return;
    }
    catch (Exception paramString2)
    {
      c.b(paramString1, "Unexpected error", "asyncPing");
      paramString2.getMessage();
    }
  }
  
  public void cancelSaveContent(String paramString1, String paramString2)
  {
    try
    {
      RenderView.d();
      return;
    }
    catch (Exception paramString2)
    {
      c.b(paramString1, "Unexpected error", "cancelSaveContent");
      paramString2.getMessage();
    }
  }
  
  public void close(String paramString)
  {
    Handler localHandler = new android/os/Handler;
    Object localObject = c.getContainerContext().getMainLooper();
    localHandler.<init>((Looper)localObject);
    localObject = new com/inmobi/rendering/c$12;
    ((c.12)localObject).<init>(this, paramString);
    localHandler.post((Runnable)localObject);
  }
  
  public void createCalendarEvent(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, String paramString9, String paramString10, String paramString11)
  {
    c localc = this;
    String str = paramString1;
    Object localObject1 = c;
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = "calendar";
    boolean bool = ((RenderView)localObject1).d((String)localObject2);
    if (!bool) {
      return;
    }
    if (paramString3 != null)
    {
      localObject1 = paramString3.trim();
      int i = ((String)localObject1).length();
      if ((i != 0) && (paramString4 != null))
      {
        localObject1 = paramString4.trim();
        i = ((String)localObject1).length();
        if (i != 0)
        {
          localObject1 = com.inmobi.commons.a.a.b();
          if (localObject1 == null) {
            return;
          }
          int j = Build.VERSION.SDK_INT;
          int k = 23;
          if (j >= k)
          {
            localObject2 = "android.permission.WRITE_CALENDAR";
            j = ((Context)localObject1).checkSelfPermission((String)localObject2);
            if (j == 0)
            {
              localObject2 = "android.permission.READ_CALENDAR";
              i = ((Context)localObject1).checkSelfPermission((String)localObject2);
              if (i == 0) {}
            }
            else
            {
              localObject1 = new String[] { "android.permission.WRITE_CALENDAR", "android.permission.READ_CALENDAR" };
              c.2 local2 = new com/inmobi/rendering/c$2;
              localObject2 = local2;
              str = paramString11;
              local2.<init>(this, paramString1, paramString2, paramString3, paramString4, paramString5, paramString6, paramString7, paramString8, paramString9, paramString10, paramString11);
              InMobiAdActivity.a((String[])localObject1, local2);
              return;
            }
          }
          try
          {
            localObject2 = c;
            ((RenderView)localObject2).a(paramString1, paramString2, paramString3, paramString4, paramString5, paramString6, paramString7, paramString8, paramString9, paramString10, paramString11);
            return;
          }
          catch (Exception localException)
          {
            c.b(str, "Unexpected error", "createCalendarEvent");
            Logger.a(Logger.InternalLogLevel.ERROR, "InMobi", "Could not create calendar event; SDK encountered unexpected error");
            localException.getMessage();
            return;
          }
        }
      }
    }
    c.b(str, "Mandatory parameter(s) start and/or end date not supplied", "createCalendarEvent");
  }
  
  public void disableBackButton(String paramString, boolean paramBoolean)
  {
    paramString = c;
    if (paramString == null) {
      return;
    }
    paramString.setDisableBackButton(paramBoolean);
  }
  
  public void disableCloseRegion(String paramString, boolean paramBoolean)
  {
    Object localObject = c;
    if (localObject == null) {
      return;
    }
    Handler localHandler = new android/os/Handler;
    localObject = ((RenderView)localObject).getContainerContext().getMainLooper();
    localHandler.<init>((Looper)localObject);
    localObject = new com/inmobi/rendering/c$4;
    ((c.4)localObject).<init>(this, paramBoolean, paramString);
    localHandler.post((Runnable)localObject);
  }
  
  public void expand(String paramString1, String paramString2)
  {
    Object localObject1 = d.a;
    Object localObject2 = AdContainer.RenderingProperties.PlacementType.PLACEMENT_TYPE_FULLSCREEN;
    if (localObject1 != localObject2)
    {
      localObject1 = c;
      if (localObject1 == null) {
        return;
      }
      boolean bool1 = o;
      if (bool1)
      {
        if (paramString2 != null)
        {
          int i = paramString2.length();
          if (i != 0)
          {
            localObject1 = "http";
            boolean bool2 = paramString2.startsWith((String)localObject1);
            if (!bool2)
            {
              c.b(paramString1, "Invalid URL", "expand");
              return;
            }
          }
        }
        localObject1 = new android/os/Handler;
        localObject2 = c.getContainerContext().getMainLooper();
        ((Handler)localObject1).<init>((Looper)localObject2);
        localObject2 = new com/inmobi/rendering/c$8;
        ((c.8)localObject2).<init>(this, paramString1, paramString2);
        ((Handler)localObject1).post((Runnable)localObject2);
        return;
      }
      paramString2 = c;
      localObject1 = "Creative is not visible. Ignoring request.";
      localObject2 = "expand";
      paramString2.b(paramString1, (String)localObject1, (String)localObject2);
    }
  }
  
  public void fireAdFailed(String paramString)
  {
    try
    {
      Object localObject = c;
      localObject = ((RenderView)localObject).getListener();
      ((RenderView.a)localObject).u();
      return;
    }
    catch (Exception localException)
    {
      c.b(paramString, "Unexpected error", "fireAdFailed");
      localException.getMessage();
    }
  }
  
  public void fireAdReady(String paramString)
  {
    try
    {
      Object localObject = c;
      localObject = ((RenderView)localObject).getListener();
      ((RenderView.a)localObject).s();
      return;
    }
    catch (Exception localException)
    {
      c.b(paramString, "Unexpected error", "fireAdReady");
      localException.getMessage();
    }
  }
  
  public void fireComplete(String paramString)
  {
    paramString = c;
    if (paramString == null) {
      return;
    }
    RenderView.f();
  }
  
  public void fireSkip(String paramString) {}
  
  public void getBlob(String paramString1, String paramString2)
  {
    RenderView localRenderView = c;
    if (localRenderView == null) {
      return;
    }
    a locala = u;
    if (locala != null)
    {
      locala = u;
      locala.a(paramString1, paramString2, localRenderView);
    }
  }
  
  public String getCurrentPosition(String arg1)
  {
    ??? = c;
    if (??? == null) {
      return "";
    }
    for (;;)
    {
      Object localObject1;
      synchronized (???.getCurrentPositionMonitor())
      {
        localObject1 = c;
        ((RenderView)localObject1).setCurrentPositionLock();
        localObject1 = new android/os/Handler;
        Object localObject3 = c;
        localObject3 = ((RenderView)localObject3).getContainerContext();
        localObject3 = ((Context)localObject3).getMainLooper();
        ((Handler)localObject1).<init>((Looper)localObject3);
        localObject3 = new com/inmobi/rendering/c$7;
        ((c.7)localObject3).<init>(this);
        ((Handler)localObject1).post((Runnable)localObject3);
        localObject1 = c;
        boolean bool = l;
        if (!bool) {}
      }
      try
      {
        localObject1 = c;
        localObject1 = ((RenderView)localObject1).getCurrentPositionMonitor();
        localObject1.wait();
      }
      catch (InterruptedException localInterruptedException) {}
      return c.getCurrentPosition();
      localObject2 = finally;
      throw ((Throwable)localObject2);
    }
  }
  
  public String getDefaultPosition(String arg1)
  {
    ??? = c;
    if (??? == null)
    {
      ??? = new org/json/JSONObject;
      ???.<init>();
      return ???.toString();
    }
    for (;;)
    {
      Object localObject1;
      synchronized (???.getDefaultPositionMonitor())
      {
        localObject1 = c;
        ((RenderView)localObject1).setDefaultPositionLock();
        localObject1 = new android/os/Handler;
        Object localObject3 = c;
        localObject3 = ((RenderView)localObject3).getContainerContext();
        localObject3 = ((Context)localObject3).getMainLooper();
        ((Handler)localObject1).<init>((Looper)localObject3);
        localObject3 = new com/inmobi/rendering/c$6;
        ((c.6)localObject3).<init>(this);
        ((Handler)localObject1).post((Runnable)localObject3);
        localObject1 = c;
        boolean bool = k;
        if (!bool) {}
      }
      try
      {
        localObject1 = c;
        localObject1 = ((RenderView)localObject1).getDefaultPositionMonitor();
        localObject1.wait();
      }
      catch (InterruptedException localInterruptedException) {}
      return c.getDefaultPosition();
      localObject2 = finally;
      throw ((Throwable)localObject2);
    }
  }
  
  public int getDeviceVolume(String paramString)
  {
    Object localObject = c;
    int i = -1;
    if (localObject == null) {
      return i;
    }
    try
    {
      localObject = ((RenderView)localObject).getMediaProcessor();
      Context localContext = com.inmobi.commons.a.a.b();
      if (localContext == null) {
        return i;
      }
      localObject = a;
      localObject = ((RenderView)localObject).getRenderingConfig();
      boolean bool = l;
      if (bool)
      {
        bool = com.inmobi.commons.a.a.d();
        if (bool) {
          return 0;
        }
      }
      localObject = "audio";
      localObject = localContext.getSystemService((String)localObject);
      localObject = (AudioManager)localObject;
      int j = 3;
      return ((AudioManager)localObject).getStreamVolume(j);
    }
    catch (Exception localException)
    {
      c.b(paramString, "Unexpected error", "getDeviceVolume");
      localException.getMessage();
    }
    return i;
  }
  
  public int getDownloadProgress(String paramString)
  {
    RenderView localRenderView = c;
    int i = -1;
    if (localRenderView == null) {
      return i;
    }
    try
    {
      return localRenderView.getDownloadProgress();
    }
    catch (Exception localException)
    {
      c.b(paramString, "Unexpected error", "getDownloadProgress");
      localException.getMessage();
    }
    return i;
  }
  
  public int getDownloadStatus(String paramString)
  {
    RenderView localRenderView = c;
    int i = -1;
    if (localRenderView == null) {
      return i;
    }
    try
    {
      return localRenderView.getDownloadStatus();
    }
    catch (Exception localException)
    {
      c.b(paramString, "Unexpected error", "getDownloadStatus");
      localException.getMessage();
    }
    return i;
  }
  
  public String getExpandProperties(String paramString)
  {
    paramString = c;
    if (paramString == null) {
      return "";
    }
    return getExpandPropertiesc;
  }
  
  public String getMaxSize(String paramString)
  {
    localJSONObject = new org/json/JSONObject;
    localJSONObject.<init>();
    try
    {
      ??? = c;
      ??? = ((RenderView)???).getFullScreenActivity();
      if (??? == null)
      {
        ??? = c;
        ??? = ((RenderView)???).getContainerContext();
        boolean bool = ??? instanceof Activity;
        if (!bool) {
          return getScreenSize(paramString);
        }
        ??? = c;
        ??? = ((RenderView)???).getContainerContext();
        ??? = (Activity)???;
      }
      int i = 16908290;
      ??? = ((Activity)???).findViewById(i);
      ??? = (FrameLayout)???;
      ??? = (FrameLayout)???;
      i = ((FrameLayout)???).getWidth();
      i = com.inmobi.commons.core.utilities.b.c.b(i);
      int j = ((FrameLayout)???).getHeight();
      j = com.inmobi.commons.core.utilities.b.c.b(j);
      Object localObject2 = c;
      localObject2 = ((RenderView)localObject2).getFullScreenActivity();
      Object localObject3;
      if ((localObject2 != null) && ((i == 0) || (j == 0)))
      {
        c.a locala1 = new com/inmobi/rendering/c$a;
        locala1.<init>((View)???);
        ??? = ((FrameLayout)???).getViewTreeObserver();
        ((ViewTreeObserver)???).addOnGlobalLayoutListener(locala1);
        try
        {
          synchronized (c.a.a(locala1))
          {
            localObject3 = c.a.a(locala1);
            localObject3.wait();
          }
        }
        catch (InterruptedException localInterruptedException)
        {
          j = c.a.b(locala2);
          i = c.a.c(locala2);
          int k = j;
          j = i;
          i = k;
        }
        throw locala2;
      }
      ??? = "width";
      RenderView localRenderView;
      return localJSONObject.toString();
    }
    catch (Exception localException)
    {
      try
      {
        localJSONObject.put((String)???, i);
        ??? = "height";
        localJSONObject.put((String)???, j);
      }
      catch (JSONException localJSONException)
      {
        for (;;) {}
      }
      localException = localException;
      localRenderView = c;
      localObject3 = "Unexpected error";
      localObject2 = "getMaxSize";
      localRenderView.b(paramString, (String)localObject3, (String)localObject2);
      localException.getMessage();
    }
  }
  
  public String getOrientation(String paramString)
  {
    int i = com.inmobi.commons.core.utilities.b.c.b();
    int j = 1;
    if (i == j) {
      return "0";
    }
    j = 3;
    if (i == j) {
      return "90";
    }
    j = 2;
    if (i == j) {
      return "180";
    }
    j = 4;
    if (i == j) {
      return "270";
    }
    return "-1";
  }
  
  public String getOrientationProperties(String paramString)
  {
    return e.d;
  }
  
  public String getPlacementType(String paramString)
  {
    paramString = AdContainer.RenderingProperties.PlacementType.PLACEMENT_TYPE_FULLSCREEN;
    AdContainer.RenderingProperties.PlacementType localPlacementType = d.a;
    if (paramString == localPlacementType) {
      return "interstitial";
    }
    return "inline";
  }
  
  public String getPlatform(String paramString)
  {
    return "android";
  }
  
  public String getPlatformVersion(String paramString)
  {
    return Integer.toString(Build.VERSION.SDK_INT);
  }
  
  public String getResizeProperties(String paramString)
  {
    paramString = c;
    if (paramString == null) {
      return "";
    }
    paramString = paramString.getResizeProperties();
    if (paramString == null) {
      return "";
    }
    return paramString.a();
  }
  
  public String getScreenSize(String paramString)
  {
    JSONObject localJSONObject = new org/json/JSONObject;
    localJSONObject.<init>();
    String str1 = "width";
    try
    {
      try
      {
        localObject = com.inmobi.commons.core.utilities.b.c.a();
        int i = a;
        localJSONObject.put(str1, i);
        str1 = "height";
        localObject = com.inmobi.commons.core.utilities.b.c.a();
        i = b;
        localJSONObject.put(str1, i);
      }
      catch (Exception localException)
      {
        Object localObject = c;
        String str2 = "Unexpected error";
        String str3 = "getScreenSize";
        ((RenderView)localObject).b(paramString, str2, str3);
        localException.getMessage();
      }
    }
    catch (JSONException localJSONException)
    {
      for (;;) {}
    }
    return localJSONObject.toString();
  }
  
  public String getSdkVersion(String paramString)
  {
    return "7.1.1";
  }
  
  public String getState(String paramString)
  {
    paramString = c.getState();
    Locale localLocale = Locale.ENGLISH;
    return paramString.toLowerCase(localLocale);
  }
  
  public String getVersion(String paramString)
  {
    return "2.0";
  }
  
  public void incentCompleted(String paramString1, String paramString2)
  {
    Object localObject1;
    if (paramString2 == null) {
      try
      {
        paramString2 = c;
        paramString2 = paramString2.getListener();
        localObject1 = new java/util/HashMap;
        ((HashMap)localObject1).<init>();
        paramString2.a((HashMap)localObject1);
        return;
      }
      catch (Exception paramString2)
      {
        c.b(paramString1, "Unexpected error", "incentCompleted");
        paramString2.getMessage();
        return;
      }
    }
    try
    {
      localObject1 = new org/json/JSONObject;
      ((JSONObject)localObject1).<init>(paramString2);
      paramString2 = new java/util/HashMap;
      paramString2.<init>();
      Object localObject2 = ((JSONObject)localObject1).keys();
      Object localObject3;
      for (;;)
      {
        boolean bool = ((Iterator)localObject2).hasNext();
        if (!bool) {
          break;
        }
        localObject3 = ((Iterator)localObject2).next();
        localObject3 = (String)localObject3;
        Object localObject4 = ((JSONObject)localObject1).get((String)localObject3);
        paramString2.put(localObject3, localObject4);
      }
      try
      {
        localObject1 = c;
        localObject1 = ((RenderView)localObject1).getListener();
        ((RenderView.a)localObject1).a(paramString2);
        return;
      }
      catch (Exception paramString2)
      {
        localObject1 = c;
        localObject2 = "Unexpected error";
        localObject3 = "incentCompleted";
        ((RenderView)localObject1).b(paramString1, (String)localObject2, (String)localObject3);
        paramString2.getMessage();
        return;
      }
      return;
    }
    catch (JSONException localJSONException)
    {
      try
      {
        paramString2 = c;
        paramString2 = paramString2.getListener();
        localObject1 = new java/util/HashMap;
        ((HashMap)localObject1).<init>();
        paramString2.a((HashMap)localObject1);
        return;
      }
      catch (Exception paramString2)
      {
        c.b(paramString1, "Unexpected error", "incentCompleted");
        paramString2.getMessage();
      }
    }
  }
  
  public boolean isBackButtonDisabled(String paramString)
  {
    paramString = c;
    if (paramString == null) {
      return false;
    }
    return q;
  }
  
  public String isDeviceMuted(String paramString)
  {
    paramString = c;
    if (paramString == null) {
      return "false";
    }
    boolean bool = false;
    try
    {
      paramString.getMediaProcessor();
      bool = MraidMediaProcessor.a();
    }
    catch (Exception paramString)
    {
      paramString.getMessage();
    }
    return String.valueOf(bool);
  }
  
  public String isHeadphonePlugged(String paramString)
  {
    paramString = c;
    if (paramString == null) {
      return "false";
    }
    boolean bool = false;
    try
    {
      paramString.getMediaProcessor();
      bool = MraidMediaProcessor.d();
    }
    catch (Exception paramString)
    {
      paramString.getMessage();
    }
    return String.valueOf(bool);
  }
  
  public boolean isViewable(String paramString)
  {
    paramString = c;
    if (paramString == null) {
      return false;
    }
    return o;
  }
  
  public void log(String paramString1, String paramString2) {}
  
  public void onOrientationChange(String paramString) {}
  
  public void onUserInteraction(String paramString1, String paramString2)
  {
    Object localObject1;
    if (paramString2 == null) {
      try
      {
        paramString2 = c;
        paramString2 = paramString2.getListener();
        localObject1 = new java/util/HashMap;
        ((HashMap)localObject1).<init>();
        paramString2.b((HashMap)localObject1);
        return;
      }
      catch (Exception paramString2)
      {
        c.b(paramString1, "Unexpected error", "onUserInteraction");
        paramString2.getMessage();
        return;
      }
    }
    try
    {
      localObject1 = new org/json/JSONObject;
      ((JSONObject)localObject1).<init>(paramString2);
      paramString2 = new java/util/HashMap;
      paramString2.<init>();
      Object localObject2 = ((JSONObject)localObject1).keys();
      Object localObject3;
      for (;;)
      {
        boolean bool = ((Iterator)localObject2).hasNext();
        if (!bool) {
          break;
        }
        localObject3 = ((Iterator)localObject2).next();
        localObject3 = (String)localObject3;
        Object localObject4 = ((JSONObject)localObject1).get((String)localObject3);
        paramString2.put(localObject3, localObject4);
      }
      try
      {
        localObject1 = c;
        localObject1 = ((RenderView)localObject1).getListener();
        ((RenderView.a)localObject1).b(paramString2);
        return;
      }
      catch (Exception paramString2)
      {
        localObject1 = c;
        localObject2 = "Unexpected error";
        localObject3 = "onUserInteraction";
        ((RenderView)localObject1).b(paramString1, (String)localObject2, (String)localObject3);
        paramString2.getMessage();
        return;
      }
      return;
    }
    catch (JSONException localJSONException)
    {
      try
      {
        paramString2 = c;
        paramString2 = paramString2.getListener();
        localObject1 = new java/util/HashMap;
        ((HashMap)localObject1).<init>();
        paramString2.b((HashMap)localObject1);
        return;
      }
      catch (Exception paramString2)
      {
        c.b(paramString1, "Unexpected error", "onUserInteraction");
        paramString2.getMessage();
      }
    }
  }
  
  public void open(String paramString1, String paramString2)
  {
    Object localObject = c;
    if (localObject == null) {
      return;
    }
    Handler localHandler = new android/os/Handler;
    localObject = ((RenderView)localObject).getContainerContext().getMainLooper();
    localHandler.<init>((Looper)localObject);
    localObject = new com/inmobi/rendering/c$1;
    ((c.1)localObject).<init>(this, paramString1, paramString2);
    localHandler.post((Runnable)localObject);
  }
  
  public void openEmbedded(String paramString1, String paramString2)
  {
    Object localObject = c;
    if (localObject == null) {
      return;
    }
    Handler localHandler = new android/os/Handler;
    localObject = ((RenderView)localObject).getContainerContext().getMainLooper();
    localHandler.<init>((Looper)localObject);
    localObject = new com/inmobi/rendering/c$5;
    ((c.5)localObject).<init>(this, paramString1, paramString2);
    localHandler.post((Runnable)localObject);
  }
  
  public void openExternal(String paramString1, String paramString2, String paramString3)
  {
    Object localObject = c;
    if (localObject == null) {
      return;
    }
    if (paramString2 != null)
    {
      localObject = "http";
      bool1 = paramString2.startsWith((String)localObject);
      if (bool1)
      {
        bool1 = URLUtil.isValidUrl(paramString2);
        if (!bool1)
        {
          bool1 = true;
          break label56;
        }
      }
    }
    boolean bool1 = false;
    localObject = null;
    label56:
    String str;
    if (bool1)
    {
      if (paramString3 != null)
      {
        paramString2 = "http";
        boolean bool2 = paramString3.startsWith(paramString2);
        if (bool2)
        {
          bool2 = URLUtil.isValidUrl(paramString3);
          if (!bool2)
          {
            c.b(paramString1, "Invalid URL", "openExternal");
            return;
          }
        }
      }
      try
      {
        paramString2 = c;
        localObject = "openExternal";
        str = null;
        paramString2.a((String)localObject, paramString1, paramString3, null);
        return;
      }
      catch (Exception paramString2)
      {
        c.b(paramString1, "Unexpected error", "openExternal");
        Logger.a(Logger.InternalLogLevel.ERROR, "InMobi", "Could not open URL; SDK encountered an unexpected error");
        paramString2.getMessage();
        return;
      }
    }
    try
    {
      localObject = c;
      str = "openExternal";
      ((RenderView)localObject).a(str, paramString1, paramString2, paramString3);
      return;
    }
    catch (Exception paramString2)
    {
      c.b(paramString1, "Unexpected error", "openExternal");
      Logger.a(Logger.InternalLogLevel.ERROR, "InMobi", "Could not open URL; SDK encountered an unexpected error");
      paramString2.getMessage();
    }
  }
  
  public void ping(String paramString1, String paramString2, boolean paramBoolean)
  {
    Object localObject = c;
    if (localObject == null) {
      return;
    }
    if (paramString2 != null)
    {
      localObject = paramString2.trim();
      int i = ((String)localObject).length();
      if (i != 0)
      {
        boolean bool = URLUtil.isValidUrl(paramString2);
        if (bool) {
          try
          {
            localObject = com.inmobi.rendering.a.c.a();
            com.inmobi.rendering.a.c.1 local1 = new com/inmobi/rendering/a/c$1;
            local1.<init>((com.inmobi.rendering.a.c)localObject, paramString2, paramBoolean);
            local1.start();
            return;
          }
          catch (Exception paramString2)
          {
            c.b(paramString1, "Unexpected error", "ping");
            Logger.a(Logger.InternalLogLevel.ERROR, "InMobi", "Failed to fire ping; SDK encountered unexpected error");
            paramString2.getMessage();
            return;
          }
        }
      }
    }
    RenderView localRenderView = c;
    paramString2 = String.valueOf(paramString2);
    paramString2 = "Invalid URL:".concat(paramString2);
    localRenderView.b(paramString1, paramString2, "ping");
  }
  
  public void pingInWebView(String paramString1, String paramString2, boolean paramBoolean)
  {
    Object localObject = c;
    if (localObject == null) {
      return;
    }
    if (paramString2 != null)
    {
      localObject = paramString2.trim();
      int i = ((String)localObject).length();
      if (i != 0)
      {
        boolean bool = URLUtil.isValidUrl(paramString2);
        if (bool) {
          try
          {
            localObject = com.inmobi.rendering.a.c.a();
            com.inmobi.rendering.a.c.3 local3 = new com/inmobi/rendering/a/c$3;
            local3.<init>((com.inmobi.rendering.a.c)localObject, paramString2, paramBoolean);
            local3.start();
            return;
          }
          catch (Exception paramString2)
          {
            c.b(paramString1, "Unexpected error", "pingInWebView");
            Logger.a(Logger.InternalLogLevel.ERROR, "InMobi", "Failed to fire ping; SDK encountered unexpected error");
            paramString2.getMessage();
            return;
          }
        }
      }
    }
    RenderView localRenderView = c;
    paramString2 = String.valueOf(paramString2);
    paramString2 = "Invalid URL:".concat(paramString2);
    localRenderView.b(paramString1, paramString2, "pingInWebView");
  }
  
  public void playVideo(String paramString1, String paramString2)
  {
    Object localObject1 = c;
    if (localObject1 == null) {
      return;
    }
    if (paramString2 != null)
    {
      localObject1 = paramString2.trim();
      int i = ((String)localObject1).length();
      if (i != 0)
      {
        localObject1 = "http";
        boolean bool = paramString2.startsWith((String)localObject1);
        if (bool)
        {
          localObject1 = "mp4";
          bool = paramString2.endsWith((String)localObject1);
          if (!bool)
          {
            localObject1 = "avi";
            bool = paramString2.endsWith((String)localObject1);
            if (!bool)
            {
              localObject1 = "m4v";
              bool = paramString2.endsWith((String)localObject1);
              if (!bool) {
                break label139;
              }
            }
          }
          localObject1 = new android/os/Handler;
          Object localObject2 = c.getContainerContext().getMainLooper();
          ((Handler)localObject1).<init>((Looper)localObject2);
          localObject2 = new com/inmobi/rendering/c$11;
          ((c.11)localObject2).<init>(this, paramString1, paramString2);
          ((Handler)localObject1).post((Runnable)localObject2);
          return;
        }
      }
    }
    label139:
    c.b(paramString1, "Null or empty or invalid media playback URL supplied", "playVideo");
  }
  
  public void postToSocial(String paramString1, int paramInt, String paramString2, String paramString3, String paramString4)
  {
    Object localObject1 = c;
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = "postToSocial";
    try
    {
      boolean bool1 = ((RenderView)localObject1).d((String)localObject2);
      if (bool1)
      {
        localObject2 = i;
        localObject1 = ((RenderView)localObject1).getContainerContext();
        if (paramString2 != null)
        {
          int i = paramString2.length();
          if ((i != 0) && (paramString3 != null))
          {
            i = paramString3.length();
            if (i != 0)
            {
              Object localObject3 = "http";
              boolean bool2 = paramString3.startsWith((String)localObject3);
              if ((bool2) && (paramString4 != null))
              {
                int j = paramString4.length();
                if (j != 0)
                {
                  localObject3 = "http";
                  boolean bool3 = paramString4.startsWith((String)localObject3);
                  if (bool3)
                  {
                    localObject3 = ".jpg";
                    bool3 = paramString4.endsWith((String)localObject3);
                    if (bool3)
                    {
                      bool3 = false;
                      localObject3 = null;
                      Object localObject4;
                      switch (paramInt)
                      {
                      default: 
                        localRenderView = a;
                        break;
                      case 3: 
                        localObject2 = new java/lang/StringBuilder;
                        ((StringBuilder)localObject2).<init>();
                        ((StringBuilder)localObject2).append(paramString2);
                        localObject3 = " ";
                        ((StringBuilder)localObject2).append((String)localObject3);
                        ((StringBuilder)localObject2).append(paramString3);
                        localObject3 = " ";
                        ((StringBuilder)localObject2).append((String)localObject3);
                        ((StringBuilder)localObject2).append(paramString4);
                        localObject2 = ((StringBuilder)localObject2).toString();
                        localObject3 = "com.twitter.android";
                        localObject4 = new android/content/Intent;
                        ((Intent)localObject4).<init>();
                        String str = "text/plain";
                        ((Intent)localObject4).setType(str);
                        ((Intent)localObject4).setPackage((String)localObject3);
                        localObject3 = "android.intent.extra.TEXT";
                        ((Intent)localObject4).putExtra((String)localObject3, (String)localObject2);
                        localObject3 = localObject4;
                        break;
                      case 2: 
                        localObject2 = "ads";
                        bool1 = com.inmobi.commons.core.utilities.f.a((String)localObject2);
                        if (bool1)
                        {
                          localObject2 = new java/lang/StringBuilder;
                          ((StringBuilder)localObject2).<init>();
                          ((StringBuilder)localObject2).append(paramString2);
                          localObject3 = " ";
                          ((StringBuilder)localObject2).append((String)localObject3);
                          ((StringBuilder)localObject2).append(paramString3);
                          localObject3 = " ";
                          ((StringBuilder)localObject2).append((String)localObject3);
                          ((StringBuilder)localObject2).append(paramString4);
                          localObject2 = ((StringBuilder)localObject2).toString();
                          localObject3 = new com/google/android/gms/plus/PlusShare$Builder;
                          ((PlusShare.Builder)localObject3).<init>((Context)localObject1);
                          localObject4 = "text/plain";
                          localObject3 = ((PlusShare.Builder)localObject3).setType((String)localObject4);
                          localObject2 = ((PlusShare.Builder)localObject3).setText((CharSequence)localObject2);
                          localObject3 = Uri.parse(paramString4);
                          localObject2 = ((PlusShare.Builder)localObject2).setContentUrl((Uri)localObject3);
                          localObject3 = ((PlusShare.Builder)localObject2).getIntent();
                        }
                        break;
                      }
                      if (localObject3 != null) {
                        try
                        {
                          com.inmobi.commons.a.a.a((Context)localObject1, (Intent)localObject3);
                          return;
                        }
                        catch (ActivityNotFoundException localActivityNotFoundException) {}
                      }
                      i.a((Context)localObject1, paramInt, paramString2, paramString3, paramString4);
                      break label531;
                      paramString2 = "Unsupported type of social network";
                      paramString3 = "postToSocial";
                      localRenderView.b(paramString1, paramString2, paramString3);
                      return;
                    }
                  }
                }
              }
            }
          }
        }
        RenderView localRenderView = a;
        paramString2 = "Attempting to share with null/empty/invalid parameters";
        paramString3 = "postToSocial";
        localRenderView.b(paramString1, paramString2, paramString3);
        return;
      }
      label531:
      return;
    }
    catch (Exception localException)
    {
      c.b(paramString1, "Unexpected error", "postToSocial");
      Logger.a(Logger.InternalLogLevel.ERROR, "InMobi", "Could not post to social network; SDK encountered an unexpected error");
      localException.getMessage();
    }
  }
  
  public void registerBackButtonPressedEventListener(String paramString)
  {
    RenderView localRenderView = c;
    if (localRenderView == null) {
      return;
    }
    try
    {
      r = paramString;
      return;
    }
    catch (Exception localException)
    {
      c.b(paramString, "Unexpected error", "registerBackButtonPressedEventListener");
      localException.getMessage();
    }
  }
  
  public void registerDeviceMuteEventListener(String paramString)
  {
    Object localObject1 = c;
    if (localObject1 == null) {
      return;
    }
    try
    {
      localObject1 = ((RenderView)localObject1).getMediaProcessor();
      localObject2 = com.inmobi.commons.a.a.b();
      if (localObject2 != null)
      {
        localObject3 = c;
        if (localObject3 == null)
        {
          localObject3 = new com/inmobi/rendering/mraid/MraidMediaProcessor$RingerModeChangeReceiver;
          ((MraidMediaProcessor.RingerModeChangeReceiver)localObject3).<init>((MraidMediaProcessor)localObject1, paramString);
          c = ((MraidMediaProcessor.RingerModeChangeReceiver)localObject3);
          localObject1 = c;
          localObject3 = new android/content/IntentFilter;
          str = "android.media.RINGER_MODE_CHANGED";
          ((IntentFilter)localObject3).<init>(str);
          ((Context)localObject2).registerReceiver((BroadcastReceiver)localObject1, (IntentFilter)localObject3);
        }
        return;
      }
    }
    catch (Exception localException)
    {
      Object localObject2 = c;
      Object localObject3 = "Unexpected error";
      String str = "registerDeviceMuteEventListener";
      ((RenderView)localObject2).b(paramString, (String)localObject3, str);
      localException.getMessage();
    }
  }
  
  public void registerDeviceVolumeChangeEventListener(String paramString)
  {
    Object localObject1 = c;
    if (localObject1 == null) {
      return;
    }
    try
    {
      localObject1 = ((RenderView)localObject1).getMediaProcessor();
      localObject2 = com.inmobi.commons.a.a.b();
      if (localObject2 != null)
      {
        localObject3 = d;
        if (localObject3 == null)
        {
          localObject3 = new com/inmobi/rendering/mraid/MraidMediaProcessor$a;
          localObject4 = new android/os/Handler;
          ((Handler)localObject4).<init>();
          ((MraidMediaProcessor.a)localObject3).<init>((MraidMediaProcessor)localObject1, paramString, (Context)localObject2, (Handler)localObject4);
          d = ((MraidMediaProcessor.a)localObject3);
          localObject2 = ((Context)localObject2).getContentResolver();
          localObject3 = Settings.System.CONTENT_URI;
          boolean bool = true;
          localObject1 = d;
          ((ContentResolver)localObject2).registerContentObserver((Uri)localObject3, bool, (ContentObserver)localObject1);
        }
        return;
      }
    }
    catch (Exception localException)
    {
      Object localObject2 = c;
      Object localObject3 = "Unexpected error";
      Object localObject4 = "registerDeviceVolumeChangeEventListener";
      ((RenderView)localObject2).b(paramString, (String)localObject3, (String)localObject4);
      localException.getMessage();
    }
  }
  
  public void registerDownloaderCallbacks(String paramString)
  {
    RenderView localRenderView = c;
    if (localRenderView == null) {
      return;
    }
    try
    {
      Object localObject = localRenderView.getReferenceContainer();
      boolean bool = localObject instanceof ad;
      if (bool)
      {
        localObject = (ad)localObject;
        ((ad)localObject).a(localRenderView);
      }
      return;
    }
    catch (Exception localException)
    {
      c.b(paramString, "Unexpected error", "registerDownloaderCallbacks");
      localException.getMessage();
    }
  }
  
  public void registerHeadphonePluggedEventListener(String paramString)
  {
    Object localObject1 = c;
    if (localObject1 == null) {
      return;
    }
    try
    {
      localObject1 = ((RenderView)localObject1).getMediaProcessor();
      localObject2 = com.inmobi.commons.a.a.b();
      if (localObject2 != null)
      {
        localObject3 = e;
        if (localObject3 == null)
        {
          localObject3 = new com/inmobi/rendering/mraid/MraidMediaProcessor$HeadphonesPluggedChangeReceiver;
          ((MraidMediaProcessor.HeadphonesPluggedChangeReceiver)localObject3).<init>((MraidMediaProcessor)localObject1, paramString);
          e = ((MraidMediaProcessor.HeadphonesPluggedChangeReceiver)localObject3);
          localObject1 = e;
          localObject3 = new android/content/IntentFilter;
          str = "android.intent.action.HEADSET_PLUG";
          ((IntentFilter)localObject3).<init>(str);
          ((Context)localObject2).registerReceiver((BroadcastReceiver)localObject1, (IntentFilter)localObject3);
        }
        return;
      }
    }
    catch (Exception localException)
    {
      Object localObject2 = c;
      Object localObject3 = "Unexpected error";
      String str = "registerHeadphonePluggedEventListener";
      ((RenderView)localObject2).b(paramString, (String)localObject3, str);
      localException.getMessage();
    }
  }
  
  public void resize(String paramString)
  {
    Object localObject1 = d.a;
    Object localObject2 = AdContainer.RenderingProperties.PlacementType.PLACEMENT_TYPE_FULLSCREEN;
    if (localObject1 != localObject2)
    {
      localObject1 = c;
      if (localObject1 == null) {
        return;
      }
      boolean bool = o;
      if (bool)
      {
        localObject1 = new android/os/Handler;
        localObject2 = c.getContainerContext().getMainLooper();
        ((Handler)localObject1).<init>((Looper)localObject2);
        localObject2 = new com/inmobi/rendering/c$9;
        ((c.9)localObject2).<init>(this, paramString);
        ((Handler)localObject1).post((Runnable)localObject2);
        return;
      }
      localObject1 = c;
      localObject2 = "Creative is not visible. Ignoring request.";
      String str = "resize";
      ((RenderView)localObject1).b(paramString, (String)localObject2, str);
    }
  }
  
  public void saveBlob(String paramString1, String paramString2)
  {
    paramString1 = c;
    if (paramString1 == null) {
      return;
    }
    a locala = u;
    if (locala != null)
    {
      paramString1 = u;
      paramString1.e(paramString2);
    }
  }
  
  public void saveContent(String paramString1, String paramString2, String paramString3)
  {
    int j;
    if (paramString2 != null)
    {
      int i = paramString2.length();
      if ((i != 0) && (paramString3 != null))
      {
        i = paramString3.length();
        if (i != 0) {
          try
          {
            localObject1 = c;
            localObject2 = "saveContent";
            j = ((RenderView)localObject1).d((String)localObject2);
            if (j == 0)
            {
              localObject2 = new org/json/JSONObject;
              ((JSONObject)localObject2).<init>();
              localObject3 = "url";
            }
          }
          catch (Exception paramString2)
          {
            Object localObject3;
            int k;
            label98:
            c.b(paramString1, "Unexpected error", "saveContent");
            paramString2.getMessage();
            return;
          }
        }
      }
    }
    try
    {
      ((JSONObject)localObject2).put((String)localObject3, paramString3);
      paramString3 = "reason";
      k = 5;
      ((JSONObject)localObject2).put(paramString3, k);
    }
    catch (JSONException localJSONException1)
    {
      break label98;
    }
    paramString3 = ((JSONObject)localObject2).toString();
    Object localObject2 = "\"";
    localObject3 = "\\\"";
    paramString3 = paramString3.replace((CharSequence)localObject2, (CharSequence)localObject3);
    localObject2 = new java/lang/StringBuilder;
    localObject3 = "sendSaveContentResult(\"saveContent_";
    ((StringBuilder)localObject2).<init>((String)localObject3);
    ((StringBuilder)localObject2).append(paramString2);
    paramString2 = "\", 'failed', \"";
    ((StringBuilder)localObject2).append(paramString2);
    ((StringBuilder)localObject2).append(paramString3);
    paramString2 = "\");";
    ((StringBuilder)localObject2).append(paramString2);
    paramString2 = ((StringBuilder)localObject2).toString();
    ((RenderView)localObject1).a(paramString1, paramString2);
    return;
    localObject2 = new java/util/HashSet;
    ((HashSet)localObject2).<init>();
    ((Set)localObject2).add(paramString3);
    paramString3 = new com/inmobi/ads/cache/b;
    localObject3 = UUID.randomUUID();
    localObject3 = ((UUID)localObject3).toString();
    Object localObject1 = w;
    paramString3.<init>((String)localObject3, (Set)localObject2, (com.inmobi.ads.cache.f)localObject1, paramString2);
    g = paramString1;
    paramString2 = AssetStore.a();
    localObject1 = c;
    localObject2 = new com/inmobi/ads/cache/AssetStore$4;
    ((AssetStore.4)localObject2).<init>(paramString2, paramString3);
    ((ExecutorService)localObject1).execute((Runnable)localObject2);
    return;
    localObject1 = new org/json/JSONObject;
    ((JSONObject)localObject1).<init>();
    localObject2 = "url";
    if (paramString3 == null) {
      paramString3 = "";
    }
    try
    {
      ((JSONObject)localObject1).put((String)localObject2, paramString3);
      paramString3 = "reason";
      j = 1;
      ((JSONObject)localObject1).put(paramString3, j);
    }
    catch (JSONException localJSONException2)
    {
      for (;;) {}
    }
    paramString3 = ((JSONObject)localObject1).toString().replace("\"", "\\\"");
    localObject1 = new java/lang/StringBuilder;
    localObject2 = "sendSaveContentResult(\"saveContent_";
    ((StringBuilder)localObject1).<init>((String)localObject2);
    if (paramString2 == null) {
      paramString2 = "";
    }
    ((StringBuilder)localObject1).append(paramString2);
    ((StringBuilder)localObject1).append("\", 'failed', \"");
    ((StringBuilder)localObject1).append(paramString3);
    ((StringBuilder)localObject1).append("\");");
    paramString2 = ((StringBuilder)localObject1).toString();
    c.a(paramString1, paramString2);
  }
  
  public void setCloseEndCardTracker(String paramString1, String paramString2)
  {
    RenderView localRenderView = c;
    if (localRenderView == null) {
      return;
    }
    try
    {
      localRenderView.setCloseEndCardTracker(paramString2);
      return;
    }
    catch (Exception paramString2)
    {
      c.b(paramString1, "Unexpected error", "getDownloadStatus");
      paramString2.getMessage();
    }
  }
  
  public void setExpandProperties(String paramString1, String paramString2)
  {
    Object localObject = c;
    if (localObject == null) {
      return;
    }
    String str = "Expanded";
    localObject = ((RenderView)localObject).getState();
    boolean bool = str.equals(localObject);
    if (bool) {
      return;
    }
    try
    {
      paramString2 = com.inmobi.rendering.mraid.b.a(paramString2);
      localObject = c;
      ((RenderView)localObject).setExpandProperties(paramString2);
      return;
    }
    catch (Exception paramString2)
    {
      c.b(paramString1, "Unexpected error", "setExpandProperties");
      paramString2.getMessage();
    }
  }
  
  public void setOrientationProperties(String paramString1, String paramString2)
  {
    paramString1 = c.getOrientationProperties();
    paramString1 = g.a(paramString2, paramString1);
    e = paramString1;
    paramString1 = c;
    paramString2 = e;
    paramString1.setOrientationProperties(paramString2);
  }
  
  public void setResizeProperties(String paramString1, String paramString2)
  {
    Object localObject = c;
    if (localObject == null) {
      return;
    }
    localObject = ((RenderView)localObject).getResizeProperties();
    paramString2 = h.a(paramString2, (h)localObject);
    if (paramString2 == null)
    {
      localObject = c;
      String str1 = "setResizeProperties";
      String str2 = "All mandatory fields are not present";
      ((RenderView)localObject).b(paramString1, str1, str2);
    }
    c.setResizeProperties(paramString2);
  }
  
  public void showAlert(String paramString1, String paramString2) {}
  
  public void showEndCard(String paramString)
  {
    paramString = c;
    if (paramString == null) {
      return;
    }
    paramString = paramString.getReferenceContainer();
    boolean bool = paramString instanceof ad;
    if (bool)
    {
      paramString = (ad)paramString;
      Handler localHandler = new android/os/Handler;
      Object localObject = Looper.getMainLooper();
      localHandler.<init>((Looper)localObject);
      localObject = new com/inmobi/ads/ad$6;
      ((ad.6)localObject).<init>(paramString);
      localHandler.post((Runnable)localObject);
    }
  }
  
  public void startDownloader(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    RenderView localRenderView = c;
    if (localRenderView == null) {
      return;
    }
    try
    {
      boolean bool1 = TextUtils.isEmpty(paramString2);
      if (bool1)
      {
        paramString2 = c;
        paramString3 = "Invalid URL";
        paramString4 = "startDownloader";
        paramString2.b(paramString1, paramString3, paramString4);
        return;
      }
      localRenderView = c;
      Object localObject = localRenderView.getReferenceContainer();
      boolean bool2 = localObject instanceof ad;
      if (bool2)
      {
        localObject = (ad)localObject;
        ag.a(paramString2, paramString3, paramString4);
        ((ad)localObject).a(localRenderView);
        return;
      }
      bool1 = localObject instanceof RenderView;
      if (bool1) {
        ag.a(paramString2, paramString3, paramString4);
      }
      return;
    }
    catch (Exception paramString2)
    {
      c.b(paramString1, "Unexpected error", "startDownloader");
      paramString2.getMessage();
    }
  }
  
  public void storePicture(String paramString1, String paramString2) {}
  
  public String supports(String paramString1, String paramString2)
  {
    paramString1 = Arrays.asList(a);
    boolean bool = paramString1.contains(paramString2);
    if (!bool)
    {
      paramString1 = c;
      bool = paramString1.d(paramString2);
      if (!bool) {
        return "false";
      }
    }
    return String.valueOf(c.d(paramString2));
  }
  
  public void unregisterBackButtonPressedEventListener(String paramString)
  {
    RenderView localRenderView = c;
    if (localRenderView == null) {
      return;
    }
    try
    {
      r = null;
      return;
    }
    catch (Exception localException)
    {
      c.b(paramString, "Unexpected error", "unregisterBackButtonPressedEventListener");
      localException.getMessage();
    }
  }
  
  public void unregisterDeviceMuteEventListener(String paramString)
  {
    Object localObject = c;
    if (localObject == null) {
      return;
    }
    try
    {
      localObject = ((RenderView)localObject).getMediaProcessor();
      ((MraidMediaProcessor)localObject).b();
      return;
    }
    catch (Exception localException)
    {
      c.b(paramString, "Unexpected error", "unRegisterDeviceMuteEventListener");
      localException.getMessage();
    }
  }
  
  public void unregisterDeviceVolumeChangeEventListener(String paramString)
  {
    Object localObject = c;
    if (localObject == null) {
      return;
    }
    try
    {
      localObject = ((RenderView)localObject).getMediaProcessor();
      ((MraidMediaProcessor)localObject).c();
      return;
    }
    catch (Exception localException)
    {
      c.b(paramString, "Unexpected error", "unregisterDeviceVolumeChangeEventListener");
      localException.getMessage();
    }
  }
  
  public void unregisterDownloaderCallbacks(String paramString)
  {
    RenderView localRenderView = c;
    if (localRenderView == null) {
      return;
    }
    try
    {
      Object localObject = localRenderView.getReferenceContainer();
      boolean bool = localObject instanceof ad;
      if (bool)
      {
        localObject = (ad)localObject;
        List localList = w;
        if (localList != null)
        {
          localObject = w;
          ((List)localObject).remove(localRenderView);
        }
      }
      return;
    }
    catch (Exception localException)
    {
      c.b(paramString, "Unexpected error", "unregisterDownloaderCallbacks");
      localException.getMessage();
    }
  }
  
  public void unregisterHeadphonePluggedEventListener(String paramString)
  {
    Object localObject = c;
    if (localObject == null) {
      return;
    }
    try
    {
      localObject = ((RenderView)localObject).getMediaProcessor();
      ((MraidMediaProcessor)localObject).e();
      return;
    }
    catch (Exception localException)
    {
      c.b(paramString, "Unexpected error", "unregisterHeadphonePluggedEventListener");
      localException.getMessage();
    }
  }
  
  public void useCustomClose(String paramString, boolean paramBoolean)
  {
    Handler localHandler = new android/os/Handler;
    Object localObject = c.getContainerContext().getMainLooper();
    localHandler.<init>((Looper)localObject);
    localObject = new com/inmobi/rendering/c$10;
    ((c.10)localObject).<init>(this, paramBoolean, paramString);
    localHandler.post((Runnable)localObject);
  }
}

/* Location:
 * Qualified Name:     com.inmobi.rendering.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */