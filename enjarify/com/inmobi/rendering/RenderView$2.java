package com.inmobi.rendering;

import android.os.Build.VERSION;
import java.util.concurrent.atomic.AtomicBoolean;

final class RenderView$2
  implements Runnable
{
  RenderView$2(RenderView paramRenderView, String paramString) {}
  
  public final void run()
  {
    try
    {
      Object localObject1 = b;
      localObject1 = RenderView.a((RenderView)localObject1);
      boolean bool = ((AtomicBoolean)localObject1).get();
      if (!bool)
      {
        localObject1 = new java/lang/StringBuilder;
        Object localObject2 = "javascript:try{";
        ((StringBuilder)localObject1).<init>((String)localObject2);
        localObject2 = a;
        ((StringBuilder)localObject1).append((String)localObject2);
        localObject2 = "}catch(e){}";
        ((StringBuilder)localObject1).append((String)localObject2);
        localObject1 = ((StringBuilder)localObject1).toString();
        RenderView.g();
        int i = Build.VERSION.SDK_INT;
        int j = 19;
        if (i < j)
        {
          localObject2 = b;
          RenderView.a((RenderView)localObject2, (String)localObject1);
          return;
        }
        localObject2 = b;
        RenderView.b((RenderView)localObject2, (String)localObject1);
      }
      return;
    }
    catch (Exception localException)
    {
      RenderView.g();
      localException.getMessage();
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.rendering.RenderView.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */