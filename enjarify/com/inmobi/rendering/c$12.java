package com.inmobi.rendering;

import com.inmobi.ads.AdContainer;
import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;

final class c$12
  implements Runnable
{
  c$12(c paramc, String paramString) {}
  
  public final void run()
  {
    try
    {
      Object localObject = b;
      localObject = c.a((c)localObject);
      localObject = ((RenderView)localObject).getReferenceContainer();
      ((AdContainer)localObject).b();
      return;
    }
    catch (Exception localException)
    {
      RenderView localRenderView = c.a(b);
      String str = a;
      localRenderView.b(str, "Unexpected error", "close");
      Logger.a(Logger.InternalLogLevel.ERROR, "InMobi", "Failed to close ad; SDK encountered an unexpected error");
      c.a();
      localException.getMessage();
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.rendering.c.12
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */