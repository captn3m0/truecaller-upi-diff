package com.inmobi.rendering.a;

import android.os.Build.VERSION;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import com.inmobi.ads.b.e;
import com.inmobi.commons.core.configs.b.c;
import com.inmobi.commons.core.utilities.d;
import com.inmobi.commons.core.utilities.g;
import com.inmobi.commons.core.utilities.g.b;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

public class c
  implements b.c
{
  private static final String a = "c";
  private static c b;
  private static final Object c;
  private static ExecutorService d;
  private static c.a e;
  private static HandlerThread f;
  private static List g;
  private static b h;
  private static AtomicBoolean i;
  private static b.e j;
  private static final Object k;
  private long l;
  private final c.d m;
  
  static
  {
    Object localObject = new java/lang/Object;
    localObject.<init>();
    c = localObject;
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    g = (List)localObject;
    localObject = new java/util/concurrent/atomic/AtomicBoolean;
    ((AtomicBoolean)localObject).<init>(false);
    i = (AtomicBoolean)localObject;
    localObject = new java/lang/Object;
    localObject.<init>();
    k = localObject;
  }
  
  public c()
  {
    long l1 = 0L;
    l = l1;
    Object localObject1 = new com/inmobi/rendering/a/c$7;
    ((c.7)localObject1).<init>(this);
    m = ((c.d)localObject1);
    int n = 5;
    try
    {
      localObject1 = Executors.newFixedThreadPool(n);
      d = (ExecutorService)localObject1;
      localObject1 = new android/os/HandlerThread;
      Object localObject2 = "pingHandlerThread";
      ((HandlerThread)localObject1).<init>((String)localObject2);
      f = (HandlerThread)localObject1;
      ((HandlerThread)localObject1).start();
      localObject1 = new com/inmobi/rendering/a/c$a;
      localObject2 = f;
      localObject2 = ((HandlerThread)localObject2).getLooper();
      ((c.a)localObject1).<init>(this, (Looper)localObject2);
      e = (c.a)localObject1;
      localObject1 = new com/inmobi/ads/b;
      ((com.inmobi.ads.b)localObject1).<init>();
      localObject2 = com.inmobi.commons.core.configs.b.a();
      ((com.inmobi.commons.core.configs.b)localObject2).a((com.inmobi.commons.core.configs.a)localObject1, this);
      localObject1 = l;
      j = (b.e)localObject1;
      localObject1 = new com/inmobi/rendering/a/b;
      ((b)localObject1).<init>();
      h = (b)localObject1;
      g.a();
      localObject1 = "android.net.conn.CONNECTIVITY_CHANGE";
      localObject2 = new com/inmobi/rendering/a/c$5;
      ((c.5)localObject2).<init>(this);
      g.a((String)localObject1, (g.b)localObject2);
      n = Build.VERSION.SDK_INT;
      int i1 = 23;
      if (n >= i1)
      {
        g.a();
        localObject1 = "android.os.action.DEVICE_IDLE_MODE_CHANGED";
        localObject2 = new com/inmobi/rendering/a/c$6;
        ((c.6)localObject2).<init>(this);
        g.a((String)localObject1, (g.b)localObject2);
      }
      return;
    }
    catch (Exception localException)
    {
      localException.getMessage();
    }
  }
  
  public static c a()
  {
    c localc1 = b;
    if (localc1 == null) {
      synchronized (c)
      {
        localc1 = b;
        if (localc1 == null)
        {
          localc1 = new com/inmobi/rendering/a/c;
          localc1.<init>();
          b = localc1;
        }
      }
    }
    return localc2;
  }
  
  private static HashMap c(a parama)
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    try
    {
      Object localObject = j;
      int n = a;
      int i1 = f;
      n = n - i1 + 1;
      if (n > 0)
      {
        parama = "X-im-retry-count";
        localObject = String.valueOf(n);
        localHashMap.put(parama, localObject);
      }
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
    return localHashMap;
  }
  
  private static void i()
  {
    try
    {
      ??? = i;
      boolean bool = false;
      Object localObject2 = null;
      ((AtomicBoolean)???).set(false);
      synchronized (k)
      {
        localObject2 = i;
        bool = ((AtomicBoolean)localObject2).get();
        if (!bool)
        {
          localObject2 = f;
          if (localObject2 != null)
          {
            localObject2 = f;
            localObject2 = ((HandlerThread)localObject2).getLooper();
            ((Looper)localObject2).quit();
            localObject2 = f;
            ((Thread)localObject2).interrupt();
            bool = false;
            localObject2 = null;
            f = null;
            e = null;
          }
          localObject2 = g;
          ((List)localObject2).clear();
        }
        return;
      }
      return;
    }
    catch (Exception localException)
    {
      localException.getMessage();
    }
  }
  
  public final void a(com.inmobi.commons.core.configs.a parama)
  {
    j = l;
  }
  
  public final void a(String paramString, Map paramMap)
  {
    c.2 local2 = new com/inmobi/rendering/a/c$2;
    local2.<init>(this, paramString, paramMap);
    local2.start();
  }
  
  public final void b()
  {
    try
    {
      boolean bool1 = d.a();
      if (!bool1) {
        return;
      }
      synchronized (k)
      {
        Object localObject2 = i;
        int n = 1;
        boolean bool2 = ((AtomicBoolean)localObject2).compareAndSet(false, n);
        if (bool2)
        {
          localObject2 = f;
          Object localObject4;
          if (localObject2 == null)
          {
            localObject2 = new android/os/HandlerThread;
            localObject4 = "pingHandlerThread";
            ((HandlerThread)localObject2).<init>((String)localObject4);
            f = (HandlerThread)localObject2;
            ((HandlerThread)localObject2).start();
          }
          localObject2 = e;
          if (localObject2 == null)
          {
            localObject2 = new com/inmobi/rendering/a/c$a;
            localObject4 = f;
            localObject4 = ((HandlerThread)localObject4).getLooper();
            ((c.a)localObject2).<init>(this, (Looper)localObject4);
            e = (c.a)localObject2;
          }
          bool2 = b.a();
          if (bool2)
          {
            localObject2 = i;
            ((AtomicBoolean)localObject2).set(false);
            i();
          }
          else
          {
            localObject2 = Message.obtain();
            what = n;
            c.a locala = e;
            locala.sendMessage((Message)localObject2);
          }
        }
        return;
      }
      return;
    }
    catch (Exception localException)
    {
      localException.getMessage();
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.rendering.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */