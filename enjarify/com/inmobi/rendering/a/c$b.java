package com.inmobi.rendering.a;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.atomic.AtomicBoolean;

final class c$b
{
  c.d a;
  
  public c$b(c.d paramd)
  {
    a = paramd;
  }
  
  public final void a(a parama)
  {
    g.set(false);
    Handler localHandler = new android/os/Handler;
    Object localObject = Looper.getMainLooper();
    localHandler.<init>((Looper)localObject);
    localObject = new com/inmobi/rendering/a/c$b$1;
    ((c.b.1)localObject).<init>(this, parama, localHandler);
    localHandler.post((Runnable)localObject);
  }
}

/* Location:
 * Qualified Name:     com.inmobi.rendering.a.c.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */