package com.inmobi.rendering.a;

import android.content.ContentValues;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class b
{
  static final String[] a = tmp38_24;
  private static final String b = "b";
  
  static
  {
    String[] tmp5_2 = new String[8];
    String[] tmp6_5 = tmp5_2;
    String[] tmp6_5 = tmp5_2;
    tmp6_5[0] = "id";
    tmp6_5[1] = "pending_attempts";
    String[] tmp15_6 = tmp6_5;
    String[] tmp15_6 = tmp6_5;
    tmp15_6[2] = "url";
    tmp15_6[3] = "ping_in_webview";
    String[] tmp24_15 = tmp15_6;
    String[] tmp24_15 = tmp15_6;
    tmp24_15[4] = "follow_redirect";
    tmp24_15[5] = "ts";
    tmp24_15[6] = "created_ts";
    String[] tmp38_24 = tmp24_15;
    tmp38_24[7] = "track_extras";
  }
  
  b()
  {
    com.inmobi.commons.core.d.b localb = com.inmobi.commons.core.d.b.a();
    localb.a("click", "(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, pending_attempts INTEGER NOT NULL, url TEXT NOT NULL, ping_in_webview TEXT NOT NULL, follow_redirect TEXT NOT NULL, ts TEXT NOT NULL, track_extras TEXT, created_ts TEXT NOT NULL )");
    localb.b();
  }
  
  static a a(ContentValues paramContentValues)
  {
    int i = paramContentValues.getAsInteger("id").intValue();
    int j = paramContentValues.getAsInteger("pending_attempts").intValue();
    String str = paramContentValues.getAsString("url");
    long l1 = Long.valueOf(paramContentValues.getAsString("ts")).longValue();
    long l2 = Long.valueOf(paramContentValues.getAsString("created_ts")).longValue();
    boolean bool1 = Boolean.valueOf(paramContentValues.getAsString("follow_redirect")).booleanValue();
    boolean bool2 = Boolean.valueOf(paramContentValues.getAsString("ping_in_webview")).booleanValue();
    Object localObject = "track_extras";
    paramContentValues = paramContentValues.getAsString((String)localObject);
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    if (paramContentValues != null) {
      try
      {
        localObject = new org/json/JSONObject;
        ((JSONObject)localObject).<init>(paramContentValues);
        paramContentValues = a((JSONObject)localObject);
        localHashMap.putAll(paramContentValues);
      }
      catch (Exception paramContentValues)
      {
        paramContentValues.getMessage();
      }
      catch (JSONException paramContentValues)
      {
        paramContentValues.getMessage();
      }
    }
    paramContentValues = new com/inmobi/rendering/a/a;
    paramContentValues.<init>(i, str, localHashMap, bool1, bool2, j, l1, l2);
    return paramContentValues;
  }
  
  private static Map a(JSONObject paramJSONObject)
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    Iterator localIterator = paramJSONObject.keys();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      String str1 = (String)localIterator.next();
      String str2 = (String)paramJSONObject.get(str1);
      localHashMap.put(str1, str2);
    }
    return localHashMap;
  }
  
  public static void a(a parama)
  {
    com.inmobi.commons.core.d.b localb = com.inmobi.commons.core.d.b.a();
    String[] arrayOfString = new String[1];
    parama = String.valueOf(a);
    arrayOfString[0] = parama;
    localb.a("click", "id = ?", arrayOfString);
    localb.b();
  }
  
  public static boolean a()
  {
    com.inmobi.commons.core.d.b localb = com.inmobi.commons.core.d.b.a();
    String str = "click";
    int i = localb.a(str);
    return i == 0;
  }
  
  static ContentValues b(a parama)
  {
    ContentValues localContentValues = new android/content/ContentValues;
    localContentValues.<init>();
    Object localObject1 = Integer.valueOf(a);
    localContentValues.put("id", (Integer)localObject1);
    localObject1 = b;
    localContentValues.put("url", (String)localObject1);
    localObject1 = Integer.valueOf(f);
    localContentValues.put("pending_attempts", (Integer)localObject1);
    localObject1 = Long.toString(d);
    localContentValues.put("ts", (String)localObject1);
    long l = e;
    localObject1 = Long.toString(l);
    localContentValues.put("created_ts", (String)localObject1);
    localObject1 = Boolean.toString(parama.i);
    localContentValues.put("follow_redirect", (String)localObject1);
    boolean bool = h;
    localObject1 = Boolean.toString(bool);
    localContentValues.put("ping_in_webview", (String)localObject1);
    Object localObject2 = c;
    if (localObject2 != null)
    {
      localObject2 = c;
      int i = ((Map)localObject2).size();
      if (i > 0)
      {
        localObject2 = new org/json/JSONObject;
        parama = c;
        ((JSONObject)localObject2).<init>(parama);
        parama = "track_extras";
        localObject2 = ((JSONObject)localObject2).toString();
        localContentValues.put(parama, (String)localObject2);
      }
    }
    return localContentValues;
  }
  
  public final boolean a(a parama, int paramInt)
  {
    try
    {
      ContentValues localContentValues = b(parama);
      com.inmobi.commons.core.d.b localb = com.inmobi.commons.core.d.b.a();
      Object localObject = "click";
      int i = localb.a((String)localObject);
      if (i >= paramInt)
      {
        try
        {
          localHashMap = new java/util/HashMap;
          localHashMap.<init>();
          localObject = "pingUrl";
          parama = b;
          localHashMap.put(localObject, parama);
          parama = "errorCode";
          localObject = "MaxDbLimitBreach";
          localHashMap.put(parama, localObject);
          com.inmobi.commons.core.e.b.a();
          parama = "ads";
          localObject = "PingDiscarded";
          com.inmobi.commons.core.e.b.a(parama, (String)localObject, localHashMap);
        }
        catch (Exception parama)
        {
          parama.getMessage();
        }
        String str1 = "click";
        String[] arrayOfString = a;
        String str2 = "ts= (SELECT MIN(ts) FROM click LIMIT 1)";
        localObject = localb;
        parama = localb.a(str1, arrayOfString, str2, null, null, null, null, null);
        paramInt = 0;
        HashMap localHashMap = null;
        parama = parama.get(0);
        parama = (ContentValues)parama;
        parama = a(parama);
        a(parama);
      }
      parama = "click";
      localb.a(parama, localContentValues);
      localb.b();
      return true;
    }
    finally {}
  }
}

/* Location:
 * Qualified Name:     com.inmobi.rendering.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */