package com.inmobi.rendering.a;

import android.content.Context;
import android.os.Handler;
import android.webkit.WebSettings;
import android.webkit.WebViewClient;
import com.inmobi.commons.core.network.WebViewNetworkTask;
import com.inmobi.commons.core.network.WebViewNetworkTask.NetworkTaskWebView;
import java.util.HashMap;
import java.util.Map;

final class c$b$1
  implements Runnable
{
  c$b$1(c.b paramb, a parama, Handler paramHandler) {}
  
  public final void run()
  {
    Object localObject1 = new com/inmobi/commons/core/network/c;
    Object localObject2 = a.b;
    ((com.inmobi.commons.core.network.c)localObject1).<init>("GET", (String)localObject2);
    Object localObject3 = c.b(a);
    boolean bool = ((HashMap)localObject3).isEmpty();
    if (!bool) {
      ((com.inmobi.commons.core.network.c)localObject1).a((Map)localObject3);
    }
    localObject3 = new com/inmobi/commons/core/network/WebViewNetworkTask;
    localObject2 = new com/inmobi/rendering/a/c$b$1$1;
    ((c.b.1.1)localObject2).<init>(this);
    ((WebViewNetworkTask)localObject3).<init>((com.inmobi.commons.core.network.c)localObject1, (WebViewClient)localObject2);
    try
    {
      localObject1 = new com/inmobi/commons/core/network/WebViewNetworkTask$NetworkTaskWebView;
      localObject2 = com.inmobi.commons.a.a.b();
      ((WebViewNetworkTask.NetworkTaskWebView)localObject1).<init>((WebViewNetworkTask)localObject3, (Context)localObject2);
      c = ((WebViewNetworkTask.NetworkTaskWebView)localObject1);
      localObject1 = c;
      localObject2 = b;
      ((WebViewNetworkTask.NetworkTaskWebView)localObject1).setWebViewClient((WebViewClient)localObject2);
      localObject1 = c;
      localObject1 = ((WebViewNetworkTask.NetworkTaskWebView)localObject1).getSettings();
      bool = true;
      ((WebSettings)localObject1).setJavaScriptEnabled(bool);
      localObject1 = c;
      localObject1 = ((WebViewNetworkTask.NetworkTaskWebView)localObject1).getSettings();
      int i = 2;
      ((WebSettings)localObject1).setCacheMode(i);
      localObject1 = c;
      localObject2 = a;
      localObject2 = ((com.inmobi.commons.core.network.c)localObject2).c();
      localObject3 = a;
      localObject3 = ((com.inmobi.commons.core.network.c)localObject3).b();
      ((WebViewNetworkTask.NetworkTaskWebView)localObject1).loadUrl((String)localObject2, (Map)localObject3);
      return;
    }
    catch (Exception localException)
    {
      localException.getMessage();
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.rendering.a.c.b.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */