package com.inmobi.rendering.a;

import android.content.ContentValues;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.inmobi.ads.b.e;
import com.inmobi.commons.core.configs.h;
import com.inmobi.commons.core.utilities.d;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

final class c$a
  extends Handler
{
  public c$a(c paramc, Looper paramLooper)
  {
    super(paramLooper);
  }
  
  private void a(a parama, int paramInt)
  {
    Message localMessage = Message.obtain();
    what = 5;
    obj = parama;
    arg1 = paramInt;
    sendMessage(localMessage);
  }
  
  public final void handleMessage(Message paramMessage)
  {
    a locala = this;
    Object localObject1 = paramMessage;
    try
    {
      int i = what;
      int m = 2;
      int n = 1;
      Object localObject5;
      int i1;
      long l1;
      int i3;
      Object localObject6;
      int i4;
      switch (i)
      {
      default: 
        break;
      case 5: 
        Object localObject2 = obj;
        localObject2 = (a)localObject2;
        localObject4 = new java/util/HashMap;
        ((HashMap)localObject4).<init>();
        localObject5 = "pingUrl";
        localObject2 = b;
        ((Map)localObject4).put(localObject5, localObject2);
        i = arg1;
        switch (i)
        {
        default: 
          break;
        case 2: 
          localObject2 = "errorCode";
          localObject5 = "ExpiredClick";
          ((Map)localObject4).put(localObject2, localObject5);
          break;
        case 1: 
          localObject2 = "errorCode";
          localObject5 = "MaxRetryCountReached";
          ((Map)localObject4).put(localObject2, localObject5);
        }
        try
        {
          com.inmobi.commons.core.e.b.a();
          localObject2 = "ads";
          localObject5 = "PingDiscarded";
          com.inmobi.commons.core.e.b.a((String)localObject2, (String)localObject5, (Map)localObject4);
        }
        catch (Exception localException1)
        {
          c.d();
          localException1.getMessage();
        }
      case 4: 
        localObject3 = obj;
        localObject3 = (a)localObject3;
        c.d();
        c.e();
        b.a((a)localObject3);
        localObject1 = c.f();
        ((List)localObject1).remove(localObject3);
        localObject3 = c.f();
        bool1 = ((List)localObject3).isEmpty();
        if (bool1)
        {
          c.e();
          bool1 = b.a();
          if (bool1)
          {
            c.d();
            localObject3 = c.g();
            ((AtomicBoolean)localObject3).set(false);
            return;
          }
          localObject3 = Message.obtain();
          what = n;
          locala.sendMessage((Message)localObject3);
          return;
        }
        localObject3 = c.f();
        localObject3 = ((List)localObject3).get(0);
        localObject3 = (a)localObject3;
        localObject1 = Message.obtain();
        n = h;
        if (n != 0) {
          i1 = 3;
        } else {
          i1 = 2;
        }
        what = i1;
        obj = localObject3;
        locala.sendMessage((Message)localObject1);
        break;
      case 3: 
        bool1 = d.a();
        if (!bool1)
        {
          localObject3 = c.g();
          ((AtomicBoolean)localObject3).set(false);
          c.h();
          return;
        }
        localObject3 = obj;
        localObject3 = (a)localObject3;
        int i2 = f;
        if (i2 == 0)
        {
          a((a)localObject3, n);
          return;
        }
        localObject1 = c.c();
        l1 = f;
        boolean bool4 = ((a)localObject3).a(l1);
        if (bool4)
        {
          a((a)localObject3, m);
          return;
        }
        localObject1 = c.c();
        i3 = a;
        i1 = f;
        i3 = i3 - i1 + n;
        if (i3 == 0) {
          c.d();
        } else {
          c.d();
        }
        localObject1 = new com/inmobi/rendering/a/c$b;
        localObject6 = new com/inmobi/rendering/a/c$a$2;
        ((c.a.2)localObject6).<init>(locala);
        ((c.b)localObject1).<init>((c.d)localObject6);
        ((c.b)localObject1).a((a)localObject3);
        return;
      case 2: 
        bool1 = d.a();
        if (!bool1)
        {
          localObject3 = c.g();
          ((AtomicBoolean)localObject3).set(false);
          c.h();
          return;
        }
        localObject3 = obj;
        localObject3 = (a)localObject3;
        i3 = f;
        if (i3 == 0)
        {
          a((a)localObject3, n);
          return;
        }
        localObject1 = c.c();
        l1 = f;
        boolean bool5 = ((a)localObject3).a(l1);
        if (bool5)
        {
          a((a)localObject3, m);
          return;
        }
        localObject1 = c.c();
        i4 = a;
        i1 = f;
        i4 = i4 - i1 + n;
        if (i4 == 0) {
          c.d();
        } else {
          c.d();
        }
        localObject1 = new com/inmobi/rendering/a/c$c;
        localObject6 = new com/inmobi/rendering/a/c$a$1;
        ((c.a.1)localObject6).<init>(locala);
        ((c.c)localObject1).<init>((c.d)localObject6);
        ((c.c)localObject1).a((a)localObject3);
        return;
      }
      Object localObject3 = new com/inmobi/commons/core/configs/h;
      ((h)localObject3).<init>();
      localObject1 = com.inmobi.commons.core.configs.b.a();
      int i6 = 0;
      Object localObject4 = null;
      ((com.inmobi.commons.core.configs.b)localObject1).a((com.inmobi.commons.core.configs.a)localObject3, null);
      boolean bool1 = g;
      if (!bool1)
      {
        c.e();
        localObject3 = c.c();
        int j = e;
        localObject1 = c.c();
        i4 = b;
        localObject5 = new java/util/ArrayList;
        ((ArrayList)localObject5).<init>();
        Object localObject7 = com.inmobi.commons.core.d.b.a();
        Object localObject8 = "click";
        int i7 = ((com.inmobi.commons.core.d.b)localObject7).a((String)localObject8);
        if (i7 != 0)
        {
          i7 = -1;
          if (i7 != j) {
            localObject4 = Integer.toString(j);
          }
          Object localObject9 = localObject4;
          String str1 = "click";
          String[] arrayOfString = b.a;
          String str2 = "ts";
          localObject3 = new java/lang/StringBuilder;
          localObject4 = "ts < ";
          ((StringBuilder)localObject3).<init>((String)localObject4);
          long l2 = System.currentTimeMillis();
          l3 = i4;
          l3 = l2 - l3;
          ((StringBuilder)localObject3).append(l3);
          localObject3 = ((StringBuilder)localObject3).toString();
          String str3 = "ts ASC ";
          localObject8 = localObject7;
          localObject1 = localObject7;
          localObject7 = localObject3;
          localObject3 = ((com.inmobi.commons.core.d.b)localObject8).a(str1, arrayOfString, null, null, str2, (String)localObject3, str3, (String)localObject9);
          ((com.inmobi.commons.core.d.b)localObject8).b();
          localObject3 = ((List)localObject3).iterator();
          for (;;)
          {
            boolean bool6 = ((Iterator)localObject3).hasNext();
            if (!bool6) {
              break;
            }
            localObject1 = ((Iterator)localObject3).next();
            localObject1 = (ContentValues)localObject1;
            localObject1 = b.a((ContentValues)localObject1);
            ((List)localObject5).add(localObject1);
          }
        }
        c.a((List)localObject5);
        localObject3 = c.f();
        boolean bool2 = ((List)localObject3).isEmpty();
        if (bool2)
        {
          c.e();
          bool2 = b.a();
          if (bool2)
          {
            localObject3 = c.g();
            ((AtomicBoolean)localObject3).set(false);
            return;
          }
          localObject3 = Message.obtain();
          what = n;
          localObject1 = c.c();
          int i5 = b * 1000;
          l1 = i5;
          locala.sendMessageDelayed((Message)localObject3, l1);
          return;
        }
        localObject3 = c.f();
        localObject3 = ((List)localObject3).get(0);
        localObject3 = (a)localObject3;
        localObject1 = Message.obtain();
        boolean bool3 = h;
        if (bool3) {
          i6 = 3;
        } else {
          i6 = 2;
        }
        what = i6;
        obj = localObject3;
        long l3 = System.currentTimeMillis();
        long l4 = d;
        l3 -= l4;
        localObject3 = c.c();
        l4 = b * 1000;
        bool2 = l3 < l4;
        if (bool2)
        {
          localObject3 = c.c();
          int k = b * 1000;
          l4 = k - l3;
          locala.sendMessageDelayed((Message)localObject1, l4);
          return;
        }
        locala.sendMessage((Message)localObject1);
        return;
      }
      return;
    }
    catch (Exception localException2)
    {
      c.d();
      localException2.getMessage();
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.rendering.a.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */