package com.inmobi.rendering.a;

import android.os.SystemClock;
import com.inmobi.ads.b.e;
import com.inmobi.commons.core.network.NetworkError;
import com.inmobi.commons.core.network.NetworkError.ErrorCode;
import com.inmobi.commons.core.network.d;
import com.inmobi.commons.core.network.e;
import com.inmobi.signals.o;
import java.util.HashMap;
import java.util.Map;

final class c$c
{
  private c.d a;
  
  public c$c(c.d paramd)
  {
    a = paramd;
  }
  
  public final void a(a parama)
  {
    try
    {
      Object localObject1 = new com/inmobi/commons/core/network/c;
      localObject3 = "GET";
      localObject4 = b;
      ((com.inmobi.commons.core.network.c)localObject1).<init>((String)localObject3, (String)localObject4);
      localObject3 = c.b(parama);
      boolean bool1 = ((HashMap)localObject3).isEmpty();
      if (!bool1) {
        ((com.inmobi.commons.core.network.c)localObject1).a((Map)localObject3);
      }
      boolean bool2 = false;
      localObject3 = null;
      t = false;
      localObject3 = c;
      ((com.inmobi.commons.core.network.c)localObject1).b((Map)localObject3);
      bool2 = i;
      r = bool2;
      localObject3 = c.c();
      int i = c * 1000;
      p = i;
      localObject3 = c.c();
      i = c * 1000;
      q = i;
      long l1 = SystemClock.elapsedRealtime();
      Object localObject5 = new com/inmobi/commons/core/network/e;
      ((e)localObject5).<init>((com.inmobi.commons.core.network.c)localObject1);
      localObject5 = ((e)localObject5).a();
      try
      {
        o localo = o.a();
        long l2 = ((com.inmobi.commons.core.network.c)localObject1).e();
        localo.a(l2);
        localObject1 = o.a();
        long l3 = ((d)localObject5).c();
        ((o)localObject1).b(l3);
        localObject1 = o.a();
        l3 = SystemClock.elapsedRealtime() - l1;
        ((o)localObject1).c(l3);
      }
      catch (Exception localException1)
      {
        c.d();
        localException1.getMessage();
      }
      boolean bool4 = ((d)localObject5).a();
      if (bool4)
      {
        localObject2 = b;
        localObject2 = a;
        boolean bool3 = i;
        if (!bool3)
        {
          localObject3 = NetworkError.ErrorCode.HTTP_SEE_OTHER;
          if (localObject3 != localObject2)
          {
            localObject3 = NetworkError.ErrorCode.HTTP_MOVED_TEMP;
            if (localObject3 != localObject2) {}
          }
          else
          {
            localObject2 = a;
            ((c.d)localObject2).a(parama);
            return;
          }
        }
        localObject2 = a;
        ((c.d)localObject2).b(parama);
        return;
      }
      Object localObject2 = a;
      ((c.d)localObject2).a(parama);
      return;
    }
    catch (Exception localException2)
    {
      c.d();
      localException2.getMessage();
      c.d locald = a;
      Object localObject3 = new com/inmobi/commons/core/network/NetworkError;
      Object localObject4 = NetworkError.ErrorCode.UNKNOWN_ERROR;
      ((NetworkError)localObject3).<init>((NetworkError.ErrorCode)localObject4, "Unknown error");
      locald.b(parama);
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.rendering.a.c.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */