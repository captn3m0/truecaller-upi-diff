package com.inmobi.rendering.a;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build.VERSION;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import java.util.concurrent.atomic.AtomicBoolean;

final class c$b$1$1
  extends WebViewClient
{
  AtomicBoolean a;
  boolean b;
  
  c$b$1$1(c.b.1 param1) {}
  
  public final void onPageFinished(WebView paramWebView, String paramString)
  {
    paramWebView = a;
    boolean bool1 = true;
    paramWebView.set(bool1);
    boolean bool2 = b;
    if (!bool2)
    {
      paramWebView = c.a.g;
      bool2 = paramWebView.get();
      if (!bool2)
      {
        paramWebView = c.c.a;
        paramString = c.a;
        paramWebView.a(paramString);
      }
    }
  }
  
  public final void onPageStarted(WebView paramWebView, String paramString, Bitmap paramBitmap)
  {
    paramString = new java/util/concurrent/atomic/AtomicBoolean;
    paramString.<init>(false);
    a = paramString;
    b = false;
    paramString = new java/lang/Thread;
    paramBitmap = new com/inmobi/rendering/a/c$b$1$1$1;
    paramBitmap.<init>(this, paramWebView);
    paramString.<init>(paramBitmap);
    paramString.start();
  }
  
  public final void onReceivedError(WebView paramWebView, int paramInt, String paramString1, String paramString2)
  {
    b = true;
    paramWebView = c.c.a;
    a locala = c.a;
    paramWebView.b(locala);
  }
  
  public final void onReceivedError(WebView paramWebView, WebResourceRequest paramWebResourceRequest, WebResourceError paramWebResourceError)
  {
    b = true;
    paramWebView = c.c.a;
    paramWebResourceRequest = c.a;
    paramWebView.b(paramWebResourceRequest);
  }
  
  public final void onReceivedHttpError(WebView paramWebView, WebResourceRequest paramWebResourceRequest, WebResourceResponse paramWebResourceResponse)
  {
    b = true;
    paramWebView = c.c.a;
    paramWebResourceRequest = c.a;
    paramWebView.b(paramWebResourceRequest);
  }
  
  public final boolean shouldOverrideUrlLoading(WebView paramWebView, WebResourceRequest paramWebResourceRequest)
  {
    int i = Build.VERSION.SDK_INT;
    int j = 21;
    if (i >= j)
    {
      paramWebView = c.a;
      boolean bool = i;
      if (!bool)
      {
        paramWebView = paramWebResourceRequest.getUrl().toString();
        paramWebResourceRequest = c.a.b;
        bool = paramWebView.equals(paramWebResourceRequest);
        if (!bool) {
          return true;
        }
      }
      return false;
    }
    return false;
  }
  
  public final boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString)
  {
    paramWebView = c.a;
    boolean bool = i;
    if (!bool)
    {
      paramWebView = c.a.b;
      bool = paramString.equals(paramWebView);
      if (!bool) {
        return true;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.rendering.a.c.b.1.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */