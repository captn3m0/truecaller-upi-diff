package com.inmobi.rendering;

import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

final class InMobiAdActivity$4
  implements View.OnTouchListener
{
  InMobiAdActivity$4(InMobiAdActivity paramInMobiAdActivity) {}
  
  public final boolean onTouch(View paramView, MotionEvent paramMotionEvent)
  {
    int i = paramMotionEvent.getAction();
    int j = 1;
    if (i == j)
    {
      k = -7829368;
      paramView.setBackgroundColor(k);
      paramView = InMobiAdActivity.d(a);
      boolean bool = paramView.canGoBack();
      if (bool)
      {
        paramView = InMobiAdActivity.d(a);
        paramView.goBack();
      }
      else
      {
        InMobiAdActivity.c(a);
        paramView = a;
        paramView.finish();
      }
      return j;
    }
    int k = paramMotionEvent.getAction();
    if (k == 0)
    {
      paramView.setBackgroundColor(-16711681);
      return j;
    }
    return j;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.rendering.InMobiAdActivity.4
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */