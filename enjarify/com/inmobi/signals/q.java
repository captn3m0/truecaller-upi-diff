package com.inmobi.signals;

import com.inmobi.commons.core.configs.a;
import org.json.JSONException;
import org.json.JSONObject;

public final class q
  extends a
{
  private static final String d = a.class.getSimpleName();
  public q.b a;
  q.a b;
  JSONObject c;
  
  public q()
  {
    Object localObject = new com/inmobi/signals/q$b;
    ((q.b)localObject).<init>();
    a = ((q.b)localObject);
    localObject = new com/inmobi/signals/q$a;
    ((q.a)localObject).<init>();
    b = ((q.a)localObject);
    try
    {
      localObject = new org/json/JSONObject;
      ((JSONObject)localObject).<init>();
      String str = "enabled";
      boolean bool = true;
      ((JSONObject)localObject).put(str, bool);
      str = "samplingFactor";
      bool = false;
      ((JSONObject)localObject).put(str, 0);
      c = ((JSONObject)localObject);
      return;
    }
    catch (JSONException localJSONException) {}
  }
  
  public final String a()
  {
    return "signals";
  }
  
  public final void a(JSONObject paramJSONObject)
  {
    super.a(paramJSONObject);
    JSONObject localJSONObject = paramJSONObject.getJSONObject("ice");
    Object localObject1 = a;
    int i = localJSONObject.getInt("sampleInterval");
    b = i;
    localObject1 = a;
    i = localJSONObject.getInt("sampleHistorySize");
    d = i;
    localObject1 = a;
    i = localJSONObject.getInt("stopRequestTimeout");
    c = i;
    localObject1 = a;
    boolean bool1 = localJSONObject.getBoolean("enabled");
    a = bool1;
    localObject1 = a;
    Object localObject2 = localJSONObject.getString("endPoint");
    e = ((String)localObject2);
    localObject1 = a;
    int j = localJSONObject.getInt("maxRetries");
    f = j;
    localObject1 = a;
    j = localJSONObject.getInt("retryInterval");
    g = j;
    localObject1 = a;
    boolean bool2 = localJSONObject.getBoolean("locationEnabled");
    h = bool2;
    localObject1 = a;
    bool2 = localJSONObject.getBoolean("sessionEnabled");
    i = bool2;
    localObject1 = localJSONObject.getJSONObject("w");
    localObject2 = a;
    int n = ((JSONObject)localObject1).getInt("wf");
    j = n;
    localObject2 = a;
    boolean bool4 = ((JSONObject)localObject1).getBoolean("cwe");
    l = bool4;
    localObject2 = a;
    boolean bool5 = ((JSONObject)localObject1).getBoolean("vwe");
    k = bool5;
    localObject1 = localJSONObject.getJSONObject("c");
    localObject2 = a;
    bool4 = ((JSONObject)localObject1).getBoolean("oe");
    n = bool4;
    localObject2 = a;
    bool4 = ((JSONObject)localObject1).getBoolean("cce");
    p = bool4;
    localObject2 = a;
    bool4 = ((JSONObject)localObject1).getBoolean("vce");
    o = bool4;
    localObject2 = a;
    int i1 = ((JSONObject)localObject1).getInt("cof");
    m = i1;
    localJSONObject = localJSONObject.getJSONObject("ar");
    localObject1 = a;
    bool2 = localJSONObject.getBoolean("e");
    q = bool2;
    localObject1 = a;
    int k = localJSONObject.getInt("sampleInterval");
    r = k;
    localObject1 = a;
    int i2 = localJSONObject.getInt("maxHistorySize");
    s = i2;
    localJSONObject = paramJSONObject.getJSONObject("carb");
    localObject1 = b;
    boolean bool3 = localJSONObject.getBoolean("enabled");
    a = bool3;
    localObject1 = b;
    localObject2 = localJSONObject.getString("getEndPoint");
    b = ((String)localObject2);
    localObject1 = b;
    localObject2 = localJSONObject.getString("postEndPoint");
    c = ((String)localObject2);
    localObject1 = b;
    int m = localJSONObject.getInt("retrieveFrequency");
    d = m;
    localObject1 = b;
    m = localJSONObject.getInt("maxRetries");
    e = m;
    localObject1 = b;
    m = localJSONObject.getInt("retryInterval");
    f = m;
    localObject1 = b;
    m = localJSONObject.getInt("timeoutInterval");
    g = m;
    localObject1 = b;
    long l = localJSONObject.getLong("maxGetResponseSize");
    h = l;
    paramJSONObject = paramJSONObject.optJSONObject("telemetry");
    c = paramJSONObject;
  }
  
  public final JSONObject b()
  {
    JSONObject localJSONObject1 = super.b();
    JSONObject localJSONObject2 = new org/json/JSONObject;
    localJSONObject2.<init>();
    int i = a.b;
    localJSONObject2.put("sampleInterval", i);
    i = a.c;
    localJSONObject2.put("stopRequestTimeout", i);
    i = a.d;
    localJSONObject2.put("sampleHistorySize", i);
    boolean bool1 = a.a;
    localJSONObject2.put("enabled", bool1);
    String str = a.e;
    localJSONObject2.put("endPoint", str);
    int j = a.f;
    localJSONObject2.put("maxRetries", j);
    j = a.g;
    localJSONObject2.put("retryInterval", j);
    boolean bool2 = a.h;
    localJSONObject2.put("locationEnabled", bool2);
    bool2 = a.i;
    localJSONObject2.put("sessionEnabled", bool2);
    JSONObject localJSONObject3 = new org/json/JSONObject;
    localJSONObject3.<init>();
    int m = a.j;
    localJSONObject3.put("wf", m);
    boolean bool3 = a.k;
    localJSONObject3.put("vwe", bool3);
    bool3 = a.l;
    localJSONObject3.put("cwe", bool3);
    localJSONObject2.put("w", localJSONObject3);
    localJSONObject3 = new org/json/JSONObject;
    localJSONObject3.<init>();
    int n = a.m;
    localJSONObject3.put("cof", n);
    boolean bool4 = a.o;
    localJSONObject3.put("vce", bool4);
    bool4 = a.p;
    localJSONObject3.put("cce", bool4);
    bool4 = a.n;
    localJSONObject3.put("oe", bool4);
    localJSONObject2.put("c", localJSONObject3);
    localJSONObject3 = new org/json/JSONObject;
    localJSONObject3.<init>();
    bool4 = a.q;
    localJSONObject3.put("e", bool4);
    int i1 = a.r;
    localJSONObject3.put("sampleInterval", i1);
    i1 = a.s;
    localJSONObject3.put("maxHistorySize", i1);
    localJSONObject2.put("ar", localJSONObject3);
    localJSONObject1.put("ice", localJSONObject2);
    localJSONObject2 = new org/json/JSONObject;
    localJSONObject2.<init>();
    bool2 = b.a;
    localJSONObject2.put("enabled", bool2);
    str = b.b;
    localJSONObject2.put("getEndPoint", str);
    str = b.c;
    localJSONObject2.put("postEndPoint", str);
    int k = b.d;
    localJSONObject2.put("retrieveFrequency", k);
    k = b.e;
    localJSONObject2.put("maxRetries", k);
    k = b.f;
    localJSONObject2.put("retryInterval", k);
    k = b.g;
    localJSONObject2.put("timeoutInterval", k);
    long l = b.h;
    localJSONObject2.put("maxGetResponseSize", l);
    localJSONObject1.put("carb", localJSONObject2);
    localJSONObject3 = c;
    localJSONObject1.put("telemetry", localJSONObject3);
    return localJSONObject1;
  }
  
  public final boolean c()
  {
    Object localObject = a;
    int i = b;
    if (i >= 0)
    {
      localObject = a;
      i = d;
      if (i >= 0)
      {
        localObject = a;
        i = c;
        if (i >= 0)
        {
          localObject = a.e.trim();
          i = ((String)localObject).length();
          if (i != 0)
          {
            localObject = a;
            i = f;
            if (i >= 0)
            {
              localObject = a;
              i = g;
              if (i >= 0)
              {
                localObject = a;
                i = j;
                if (i >= 0)
                {
                  localObject = a;
                  i = m;
                  if (i >= 0)
                  {
                    localObject = a;
                    i = s;
                    if (i >= 0)
                    {
                      localObject = a;
                      i = r;
                      if (i >= 0)
                      {
                        localObject = b.b.trim();
                        i = ((String)localObject).length();
                        if (i != 0)
                        {
                          localObject = b.c.trim();
                          i = ((String)localObject).length();
                          if (i != 0)
                          {
                            localObject = b.b;
                            String str = "http://";
                            boolean bool1 = ((String)localObject).startsWith(str);
                            if (!bool1)
                            {
                              localObject = b.b;
                              str = "https://";
                              bool1 = ((String)localObject).startsWith(str);
                              if (!bool1) {}
                            }
                            else
                            {
                              localObject = b.c;
                              str = "http://";
                              bool1 = ((String)localObject).startsWith(str);
                              if (!bool1)
                              {
                                localObject = b.c;
                                str = "https://";
                                bool1 = ((String)localObject).startsWith(str);
                                if (!bool1) {}
                              }
                              else
                              {
                                localObject = b;
                                int j = d;
                                if (j >= 0)
                                {
                                  localObject = b;
                                  j = e;
                                  if (j >= 0)
                                  {
                                    localObject = b;
                                    j = f;
                                    if (j >= 0)
                                    {
                                      localObject = b;
                                      j = g;
                                      if (j >= 0)
                                      {
                                        localObject = b;
                                        long l1 = h;
                                        long l2 = 0L;
                                        boolean bool2 = l1 < l2;
                                        if (!bool2) {
                                          return true;
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                        return false;
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    return false;
  }
  
  public final a d()
  {
    q localq = new com/inmobi/signals/q;
    localq.<init>();
    return localq;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.signals.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */