package com.inmobi.signals;

import android.content.Context;
import android.location.Criteria;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.SystemClock;
import com.inmobi.commons.core.configs.a.a;
import com.inmobi.commons.core.configs.b.c;
import com.inmobi.commons.core.utilities.f;
import com.inmobi.commons.core.utilities.uid.d;
import java.lang.reflect.InvocationHandler;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import org.json.JSONObject;

public class p
  implements b.c
{
  private static final String b = "p";
  private static final Object c;
  private static volatile p d;
  public q a;
  private i e;
  private g f;
  private boolean g = false;
  
  static
  {
    Object localObject = new java/lang/Object;
    localObject.<init>();
    c = localObject;
  }
  
  private p()
  {
    Object localObject = new com/inmobi/signals/q;
    ((q)localObject).<init>();
    a = ((q)localObject);
    localObject = com.inmobi.commons.core.configs.b.a();
    q localq = a;
    ((com.inmobi.commons.core.configs.b)localObject).a(localq, this);
    localObject = com.inmobi.commons.core.utilities.b.h.a();
    boolean bool = a.a.b();
    ((com.inmobi.commons.core.utilities.b.h)localObject).a(bool);
    n.a();
    n.a(a.a.a());
    localObject = com.inmobi.commons.core.e.b.a();
    JSONObject localJSONObject = a.c;
    ((com.inmobi.commons.core.e.b)localObject).a("signals", localJSONObject);
  }
  
  public static p a()
  {
    p localp1 = d;
    if (localp1 == null) {
      synchronized (c)
      {
        localp1 = d;
        if (localp1 == null)
        {
          localp1 = new com/inmobi/signals/p;
          localp1.<init>();
          d = localp1;
        }
      }
    }
    return localp2;
  }
  
  private void f()
  {
    try
    {
      boolean bool1 = g;
      if (!bool1) {
        return;
      }
      Object localObject1 = a;
      localObject1 = a;
      bool1 = a;
      if (bool1)
      {
        localObject1 = o.a();
        Object localObject3 = a();
        localObject3 = a;
        localObject3 = a;
        boolean bool2 = ((q.b)localObject3).b();
        if (bool2)
        {
          localObject3 = UUID.randomUUID();
          localObject3 = ((UUID)localObject3).toString();
          Object localObject4 = com.inmobi.commons.core.utilities.b.h.a();
          a = ((String)localObject3);
          localObject4 = com.inmobi.commons.core.utilities.b.h.a();
          long l1 = System.currentTimeMillis();
          b = l1;
          localObject4 = com.inmobi.commons.core.utilities.b.h.a();
          l1 = 0L;
          c = l1;
          long l2 = SystemClock.elapsedRealtime();
          f = l2;
          a = l1;
          b = l1;
          c = l1;
          d = l1;
          e = l1;
          f = l1;
          localObject1 = new java/util/HashMap;
          ((HashMap)localObject1).<init>();
          localObject4 = "sessionId";
          ((Map)localObject1).put(localObject4, localObject3);
          try
          {
            com.inmobi.commons.core.e.b.a();
            localObject3 = "signals";
            localObject4 = "SDKSessionStarted";
            com.inmobi.commons.core.e.b.a((String)localObject3, (String)localObject4, (Map)localObject1);
          }
          catch (Exception localException)
          {
            localException.getMessage();
          }
        }
        i locali = e;
        if (locali == null)
        {
          locali = new com/inmobi/signals/i;
          locali.<init>();
          e = locali;
        }
        locali = e;
        locali.a();
      }
      return;
    }
    finally {}
  }
  
  public final void a(com.inmobi.commons.core.configs.a parama)
  {
    parama = (q)parama;
    a = parama;
    n.a();
    n.a(a.a.a());
    parama = com.inmobi.commons.core.utilities.b.h.a();
    boolean bool = a.a.b();
    parama.a(bool);
    parama = com.inmobi.commons.core.e.b.a();
    JSONObject localJSONObject = a.c;
    parama.a("signals", localJSONObject);
  }
  
  public final void b()
  {
    try
    {
      boolean bool1 = g;
      if (!bool1)
      {
        bool1 = true;
        g = bool1;
        f();
        n localn = n.a();
        try
        {
          boolean bool2 = n.a;
          Object localObject1 = null;
          Object localObject2;
          if (bool2)
          {
            localObject2 = "signals";
            bool2 = f.a((String)localObject2);
            if (bool2)
            {
              localObject2 = com.inmobi.commons.a.a.b();
              Object localObject3 = n.d;
              if (localObject3 == null)
              {
                localObject3 = new com/inmobi/signals/n$a;
                ((n.a)localObject3).<init>((byte)0);
                n.e = (n.a)localObject3;
                localObject3 = n.e;
                String str1 = "com.google.android.gms.location.LocationServices";
                localObject2 = h.a((Context)localObject2, (InvocationHandler)localObject3, (InvocationHandler)localObject3, str1);
                n.d = localObject2;
                h.a(localObject2);
              }
            }
          }
          bool2 = n.a;
          if (bool2)
          {
            bool2 = n.b();
            if (bool2)
            {
              bool2 = localn.f();
              if (bool2)
              {
                localObject2 = b;
                if (localObject2 != null)
                {
                  localObject2 = new android/location/Criteria;
                  ((Criteria)localObject2).<init>();
                  int i = 2;
                  ((Criteria)localObject2).setBearingAccuracy(i);
                  ((Criteria)localObject2).setPowerRequirement(i);
                  ((Criteria)localObject2).setCostAllowed(false);
                  localObject1 = b;
                  String str2 = ((LocationManager)localObject1).getBestProvider((Criteria)localObject2, bool1);
                  if (str2 != null)
                  {
                    localObject2 = b;
                    localObject1 = c;
                    localObject1 = ((HandlerThread)localObject1).getLooper();
                    ((LocationManager)localObject2).requestSingleUpdate(str2, localn, (Looper)localObject1);
                  }
                }
              }
            }
          }
          return;
        }
        catch (Exception localException)
        {
          localException.getMessage();
        }
      }
      return;
    }
    finally {}
  }
  
  public final void c()
  {
    try
    {
      boolean bool1 = g;
      if (bool1)
      {
        bool1 = false;
        Object localObject1 = null;
        g = false;
        localObject1 = o.a();
        Object localObject4 = a();
        localObject4 = a;
        localObject4 = a;
        boolean bool2 = ((q.b)localObject4).b();
        long l1;
        Object localObject5;
        if (bool2)
        {
          localObject4 = com.inmobi.commons.core.utilities.b.h.a();
          l1 = System.currentTimeMillis();
          c = l1;
          try
          {
            localObject4 = new java/util/HashMap;
            ((HashMap)localObject4).<init>();
            localObject5 = "sessionId";
            Object localObject6 = com.inmobi.commons.core.utilities.b.h.a();
            localObject6 = a;
            ((Map)localObject4).put(localObject5, localObject6);
            localObject5 = "totalNetworkTime";
            long l2 = e;
            localObject6 = Long.valueOf(l2);
            ((Map)localObject4).put(localObject5, localObject6);
            localObject5 = "sessionDuration";
            l2 = SystemClock.elapsedRealtime();
            long l3 = f;
            l2 -= l3;
            localObject1 = Long.valueOf(l2);
            ((Map)localObject4).put(localObject5, localObject1);
            com.inmobi.commons.core.e.b.a();
            localObject1 = "signals";
            localObject5 = "SDKSessionEnded";
            com.inmobi.commons.core.e.b.a((String)localObject1, (String)localObject5, (Map)localObject4);
          }
          catch (Exception localException)
          {
            localException.getMessage();
          }
        }
        Object localObject2 = e;
        if (localObject2 != null)
        {
          localObject2 = e;
          localObject4 = a;
          boolean bool4 = true;
          i.a.a((i.a)localObject4, bool4);
          localObject2 = a;
          int i = 2;
          localObject5 = a();
          localObject5 = a;
          localObject5 = a;
          int j = c * 1000;
          l1 = j;
          ((i.a)localObject2).sendEmptyMessageDelayed(i, l1);
        }
        localObject2 = n.a();
        boolean bool3 = n.a;
        if (bool3)
        {
          bool3 = n.b();
          if (bool3)
          {
            bool3 = ((n)localObject2).f();
            if (bool3)
            {
              localObject4 = b;
              if (localObject4 != null)
              {
                localObject4 = b;
                ((LocationManager)localObject4).removeUpdates((LocationListener)localObject2);
              }
            }
          }
        }
      }
      return;
    }
    finally {}
  }
  
  final d d()
  {
    d locald = new com/inmobi/commons/core/utilities/uid/d;
    HashMap localHashMap = a.s.a;
    locald.<init>(localHashMap);
    return locald;
  }
  
  public final void e()
  {
    boolean bool = g;
    if (!bool) {
      return;
    }
    Object localObject = a.b;
    bool = a;
    if (bool)
    {
      localObject = f;
      if (localObject == null)
      {
        localObject = new com/inmobi/signals/g;
        ((g)localObject).<init>();
        f = ((g)localObject);
      }
      localObject = f;
      q.a locala = a.b;
      ((g)localObject).a(locala);
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.signals.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */