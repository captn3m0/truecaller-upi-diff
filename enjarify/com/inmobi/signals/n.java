package com.inmobi.signals;

import android.content.ContentResolver;
import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.HandlerThread;
import android.provider.Settings.Secure;
import android.provider.Settings.SettingNotFoundException;
import com.inmobi.commons.core.e.b;
import com.inmobi.commons.core.e.f;
import com.inmobi.commons.core.utilities.b.g;
import com.inmobi.commons.core.utilities.e;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class n
  implements LocationListener
{
  static boolean a = false;
  static Object d = null;
  static n.a e = null;
  private static final String f = "n";
  private static n g;
  private static Object h;
  private static boolean i = false;
  LocationManager b;
  HandlerThread c;
  
  static
  {
    Object localObject = new java/lang/Object;
    localObject.<init>();
    h = localObject;
  }
  
  private n()
  {
    Object localObject = new android/os/HandlerThread;
    ((HandlerThread)localObject).<init>("LThread");
    c = ((HandlerThread)localObject);
    c.start();
    localObject = (LocationManager)com.inmobi.commons.a.a.b().getSystemService("location");
    b = ((LocationManager)localObject);
  }
  
  private static Location a(Location paramLocation1, Location paramLocation2)
  {
    String str1;
    if ((paramLocation1 == null) && (paramLocation2 == null))
    {
      try
      {
        paramLocation1 = b.a();
        paramLocation2 = new com/inmobi/commons/core/e/f;
        str1 = "signals";
        String str2 = "LocationFixFailed";
        paramLocation2.<init>(str1, str2);
        paramLocation1.a(paramLocation2);
      }
      catch (Exception paramLocation1)
      {
        paramLocation1.getMessage();
      }
      return null;
    }
    if (paramLocation1 == null)
    {
      paramLocation2.getTime();
      return paramLocation2;
    }
    if (paramLocation2 == null)
    {
      paramLocation1.getTime();
      return paramLocation1;
    }
    long l1 = paramLocation1.getTime();
    long l2 = paramLocation2.getTime();
    l1 -= l2;
    l2 = 120000L;
    int j = 1;
    boolean bool1 = l1 < l2;
    int m;
    if (bool1)
    {
      m = 1;
      f1 = Float.MIN_VALUE;
    }
    else
    {
      m = 0;
      f1 = 0.0F;
    }
    long l3 = 4294847296L;
    boolean bool2 = l1 < l3;
    if (bool2) {
      bool2 = true;
    } else {
      bool2 = false;
    }
    l3 = 0L;
    boolean bool3 = l1 < l3;
    int n;
    if (bool3)
    {
      n = 1;
    }
    else
    {
      n = 0;
      str1 = null;
    }
    if (m != 0)
    {
      paramLocation1.getTime();
      return paramLocation1;
    }
    if (bool2)
    {
      paramLocation2.getTime();
      return paramLocation2;
    }
    float f2 = paramLocation1.getAccuracy();
    float f1 = paramLocation2.getAccuracy();
    f2 -= f1;
    int i1 = (int)f2;
    if (i1 > 0)
    {
      m = 1;
      f1 = Float.MIN_VALUE;
    }
    else
    {
      m = 0;
      f1 = 0.0F;
    }
    if (i1 < 0) {
      bool2 = true;
    } else {
      bool2 = false;
    }
    int k = 200;
    if (i1 <= k) {
      j = 0;
    }
    if ((!bool2) && ((n == 0) || ((m != 0) && (j != 0))))
    {
      paramLocation2.getTime();
      return paramLocation2;
    }
    paramLocation1.getTime();
    return paramLocation1;
  }
  
  public static n a()
  {
    n localn1 = g;
    if (localn1 == null) {
      synchronized (h)
      {
        localn1 = g;
        if (localn1 == null)
        {
          localn1 = new com/inmobi/signals/n;
          localn1.<init>();
          g = localn1;
        }
      }
    }
    return localn2;
  }
  
  private HashMap a(Location paramLocation, boolean paramBoolean)
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    Context localContext = com.inmobi.commons.a.a.b();
    if (localContext == null) {
      return localHashMap;
    }
    if (paramLocation != null)
    {
      long l1 = paramLocation.getTime();
      long l2 = 0L;
      double d1 = 0.0D;
      boolean bool1 = l1 < l2;
      if (bool1)
      {
        str1 = "u-ll-ts";
        long l3 = paramLocation.getTime();
        localObject1 = Long.valueOf(l3);
        localHashMap.put(str1, localObject1);
      }
      String str1 = "u-latlong-accu";
      Object localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      d1 = paramLocation.getLatitude();
      ((StringBuilder)localObject1).append(d1);
      ((StringBuilder)localObject1).append(",");
      d1 = paramLocation.getLongitude();
      ((StringBuilder)localObject1).append(d1);
      String str2 = ",";
      ((StringBuilder)localObject1).append(str2);
      float f1 = paramLocation.getAccuracy();
      int j = (int)f1;
      ((StringBuilder)localObject1).append(j);
      paramLocation = ((StringBuilder)localObject1).toString();
      localHashMap.put(str1, paramLocation);
      paramLocation = "sdk-collected";
      localObject2 = Integer.valueOf(paramBoolean);
      localHashMap.put(paramLocation, localObject2);
    }
    boolean bool2 = a;
    if (bool2)
    {
      paramLocation = "loc-allowed";
      paramBoolean = f();
      localObject2 = Integer.valueOf(paramBoolean);
      localHashMap.put(paramLocation, localObject2);
    }
    bool2 = f();
    if (bool2)
    {
      bool2 = b();
      if (bool2)
      {
        paramLocation = "signals";
        localObject2 = "android.permission.ACCESS_COARSE_LOCATION";
        bool2 = e.a(localContext, paramLocation, (String)localObject2);
        if (bool2)
        {
          paramLocation = "loc-granularity";
          localObject2 = "coarse";
          localHashMap.put(paramLocation, localObject2);
        }
        paramLocation = "signals";
        localObject2 = "android.permission.ACCESS_FINE_LOCATION";
        bool2 = e.a(localContext, paramLocation, (String)localObject2);
        if (!bool2) {
          return localHashMap;
        }
        paramLocation = "loc-granularity";
        localObject2 = "fine";
        localHashMap.put(paramLocation, localObject2);
        return localHashMap;
      }
    }
    paramLocation = "loc-granularity";
    Object localObject2 = "none";
    localHashMap.put(paramLocation, localObject2);
    return localHashMap;
  }
  
  public static void a(boolean paramBoolean)
  {
    a = paramBoolean;
  }
  
  static boolean b()
  {
    try
    {
      Context localContext = com.inmobi.commons.a.a.b();
      String str1 = "signals";
      String str2 = "android.permission.ACCESS_FINE_LOCATION";
      boolean bool = e.a(localContext, str1, str2);
      if (!bool)
      {
        localContext = com.inmobi.commons.a.a.b();
        str1 = "signals";
        str2 = "android.permission.ACCESS_COARSE_LOCATION";
        bool = e.a(localContext, str1, str2);
        if (!bool) {
          return false;
        }
      }
      return true;
    }
    catch (Exception localException) {}
    return false;
  }
  
  private Location h()
  {
    Location localLocation = null;
    label79:
    Object localObject2;
    try
    {
      boolean bool = a;
      if (bool)
      {
        bool = f();
        if (bool)
        {
          bool = i;
          if (bool)
          {
            localObject1 = j();
          }
          else
          {
            bool = false;
            localObject1 = null;
          }
          try
          {
            localObject3 = b;
            if (localObject3 == null) {
              break label87;
            }
            localLocation = i();
          }
          catch (Exception localException2)
          {
            localObject3 = localObject1;
            localObject1 = localException2;
            break label79;
          }
        }
      }
      bool = false;
      Object localObject1 = null;
    }
    catch (Exception localException1)
    {
      Object localObject3 = null;
      localException1.getMessage();
      localObject2 = localObject3;
    }
    label87:
    return a((Location)localObject2, localLocation);
  }
  
  private Location i()
  {
    Object localObject1 = new android/location/Criteria;
    ((Criteria)localObject1).<init>();
    Object localObject2 = com.inmobi.commons.a.a.b();
    Object localObject3 = "signals";
    String str2 = "android.permission.ACCESS_FINE_LOCATION";
    boolean bool = e.a((Context)localObject2, (String)localObject3, str2);
    int k = 1;
    Object localObject4;
    if (bool)
    {
      ((Criteria)localObject1).setAccuracy(k);
    }
    else
    {
      localObject2 = com.inmobi.commons.a.a.b();
      str2 = "signals";
      localObject4 = "android.permission.ACCESS_COARSE_LOCATION";
      bool = e.a((Context)localObject2, str2, (String)localObject4);
      if (bool)
      {
        j = 2;
        ((Criteria)localObject1).setAccuracy(j);
      }
    }
    ((Criteria)localObject1).setCostAllowed(false);
    localObject1 = b.getBestProvider((Criteria)localObject1, k);
    int j = 0;
    localObject2 = null;
    if (localObject1 != null)
    {
      try
      {
        localObject3 = b;
        localObject1 = ((LocationManager)localObject3).getLastKnownLocation((String)localObject1);
        localObject2 = localObject1;
      }
      catch (Exception localException1)
      {
        try
        {
          localObject3 = new java/util/HashMap;
          ((HashMap)localObject3).<init>();
          str2 = "type";
          localObject4 = "SecurityException";
          ((Map)localObject3).put(str2, localObject4);
          str2 = "message";
          localObject4 = new java/lang/StringBuilder;
          ((StringBuilder)localObject4).<init>();
          String str1 = localException1.getMessage();
          ((StringBuilder)localObject4).append(str1);
          str1 = ((StringBuilder)localObject4).toString();
          ((Map)localObject3).put(str2, str1);
          b.a();
          str1 = "signals";
          str2 = "ExceptionCaught";
          b.a(str1, str2, (Map)localObject3);
        }
        catch (Exception localException2)
        {
          localException2.getMessage();
        }
      }
      if (localObject2 == null) {
        localObject2 = k();
      }
    }
    return (Location)localObject2;
  }
  
  private static Location j()
  {
    Object localObject1 = null;
    Object localObject2 = "com.google.android.gms.location.LocationServices";
    try
    {
      localObject2 = Class.forName((String)localObject2);
      Object localObject3 = "FusedLocationApi";
      localObject2 = ((Class)localObject2).getDeclaredField((String)localObject3);
      localObject3 = "com.google.android.gms.common.api.GoogleApiClient";
      localObject3 = Class.forName((String)localObject3);
      Object localObject4 = "com.google.android.gms.location.FusedLocationProviderApi";
      localObject4 = Class.forName((String)localObject4);
      Object localObject5 = "getLastLocation";
      int j = 1;
      Class[] arrayOfClass = new Class[j];
      arrayOfClass[0] = localObject3;
      localObject3 = ((Class)localObject4).getMethod((String)localObject5, arrayOfClass);
      localObject2 = ((Field)localObject2).get(null);
      localObject4 = new Object[j];
      localObject5 = d;
      localObject4[0] = localObject5;
      localObject2 = ((Method)localObject3).invoke(localObject2, (Object[])localObject4);
      localObject2 = (Location)localObject2;
      localObject1 = localObject2;
    }
    catch (InvocationTargetException|ClassNotFoundException|IllegalAccessException|NoSuchMethodException|NoSuchFieldException localInvocationTargetException)
    {
      for (;;) {}
    }
    return (Location)localObject1;
  }
  
  private Location k()
  {
    Object localObject1 = b;
    Location localLocation = null;
    if (localObject1 != null)
    {
      int j = 1;
      localObject1 = ((LocationManager)localObject1).getProviders(j);
      int k = ((List)localObject1).size() - j;
      while (k >= 0)
      {
        String str1 = (String)((List)localObject1).get(k);
        Object localObject2 = b;
        boolean bool = ((LocationManager)localObject2).isProviderEnabled(str1);
        if (bool)
        {
          try
          {
            localObject2 = b;
            localLocation = ((LocationManager)localObject2).getLastKnownLocation(str1);
          }
          catch (SecurityException localSecurityException)
          {
            try
            {
              localObject2 = new java/util/HashMap;
              ((HashMap)localObject2).<init>();
              String str3 = "type";
              Object localObject3 = "SecurityException";
              ((Map)localObject2).put(str3, localObject3);
              str3 = "message";
              localObject3 = new java/lang/StringBuilder;
              ((StringBuilder)localObject3).<init>();
              String str2 = localSecurityException.getMessage();
              ((StringBuilder)localObject3).append(str2);
              str2 = ((StringBuilder)localObject3).toString();
              ((Map)localObject2).put(str3, str2);
              b.a();
              str2 = "signals";
              str3 = "ExceptionCaught";
              b.a(str2, str3, (Map)localObject2);
            }
            catch (Exception localException)
            {
              localException.getMessage();
            }
          }
          if (localLocation != null) {
            break;
          }
        }
        k += -1;
      }
    }
    return localLocation;
  }
  
  public final HashMap c()
  {
    try
    {
      Object localObject1 = h();
      boolean bool = true;
      localObject1 = a((Location)localObject1, bool);
      return (HashMap)localObject1;
    }
    finally {}
  }
  
  public final HashMap d()
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    String str1 = "loc-consent-status";
    boolean bool = b();
    if (bool)
    {
      bool = f();
      if (bool) {
        str2 = "AUTHORISED";
      } else {
        str2 = "DENIED";
      }
    }
    else
    {
      str2 = "DENIED";
    }
    Locale localLocale = Locale.ENGLISH;
    String str2 = str2.toLowerCase(localLocale);
    localHashMap.put(str1, str2);
    return localHashMap;
  }
  
  public final HashMap e()
  {
    try
    {
      HashMap localHashMap = new java/util/HashMap;
      localHashMap.<init>();
      Object localObject2 = h();
      boolean bool;
      Object localObject3;
      if (localObject2 != null)
      {
        bool = true;
        localObject2 = a((Location)localObject2, bool);
      }
      else
      {
        localObject2 = g.c();
        bool = false;
        localObject3 = null;
        localObject2 = a((Location)localObject2, false);
      }
      localObject2 = ((HashMap)localObject2).entrySet();
      localObject2 = ((Set)localObject2).iterator();
      for (;;)
      {
        bool = ((Iterator)localObject2).hasNext();
        if (!bool) {
          break;
        }
        localObject3 = ((Iterator)localObject2).next();
        localObject3 = (Map.Entry)localObject3;
        Object localObject4 = ((Map.Entry)localObject3).getKey();
        localObject3 = ((Map.Entry)localObject3).getValue();
        localObject3 = localObject3.toString();
        localHashMap.put(localObject4, localObject3);
      }
      return localHashMap;
    }
    finally {}
  }
  
  final boolean f()
  {
    Object localObject1 = com.inmobi.commons.a.a.b();
    if (localObject1 == null) {
      return false;
    }
    int j = Build.VERSION.SDK_INT;
    int k = 19;
    boolean bool2 = true;
    int m;
    if (j >= k)
    {
      try
      {
        localObject1 = ((Context)localObject1).getContentResolver();
        localObject2 = "location_mode";
        m = Settings.Secure.getInt((ContentResolver)localObject1, (String)localObject2);
      }
      catch (Settings.SettingNotFoundException localSettingNotFoundException)
      {
        m = 0;
        localObject1 = null;
      }
      if (m != 0) {
        return bool2;
      }
      return false;
    }
    Object localObject2 = b;
    if (localObject2 != null)
    {
      localObject2 = "signals";
      String str = "android.permission.ACCESS_FINE_LOCATION";
      boolean bool1 = e.a((Context)localObject1, (String)localObject2, str);
      boolean bool3;
      if (bool1)
      {
        localObject1 = b;
        localObject2 = "gps";
        bool1 = ((LocationManager)localObject1).isProviderEnabled((String)localObject2);
        m = 0;
        localObject1 = null;
      }
      else
      {
        localObject2 = "signals";
        str = "android.permission.ACCESS_COARSE_LOCATION";
        bool3 = e.a((Context)localObject1, (String)localObject2, str);
        if (bool3)
        {
          localObject1 = b;
          bool3 = ((LocationManager)localObject1).isProviderEnabled("network");
          bool1 = false;
          localObject2 = null;
        }
        else
        {
          bool3 = false;
          localObject1 = null;
          bool1 = false;
          localObject2 = null;
        }
      }
      if ((bool3) || (bool1)) {
        return bool2;
      }
    }
    return false;
  }
  
  public void onLocationChanged(Location paramLocation)
  {
    if (paramLocation != null) {}
    try
    {
      paramLocation.getTime();
      paramLocation.getLatitude();
      paramLocation.getLongitude();
      paramLocation.getAccuracy();
      boolean bool = b();
      if (bool)
      {
        paramLocation = b;
        paramLocation.removeUpdates(this);
      }
      return;
    }
    catch (Exception paramLocation)
    {
      com.inmobi.commons.core.a.a locala = com.inmobi.commons.core.a.a.a();
      com.inmobi.commons.core.e.a locala1 = new com/inmobi/commons/core/e/a;
      locala1.<init>(paramLocation);
      locala.a(locala1);
    }
  }
  
  public void onProviderDisabled(String paramString) {}
  
  public void onProviderEnabled(String paramString) {}
  
  public void onStatusChanged(String paramString, int paramInt, Bundle paramBundle) {}
}

/* Location:
 * Qualified Name:     com.inmobi.signals.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */