package com.inmobi.signals;

import android.content.Context;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class h
{
  private static final String a = "h";
  
  public static Object a(Context paramContext, InvocationHandler paramInvocationHandler1, InvocationHandler paramInvocationHandler2, String paramString)
  {
    Object localObject1 = "com.google.android.gms.common.api.GoogleApiClient";
    try
    {
      localObject1 = Class.forName((String)localObject1);
      localObject1 = ((Class)localObject1).getDeclaredClasses();
      int i = localObject1.length;
      Object localObject2 = null;
      Object localObject3 = null;
      Object localObject4 = null;
      int j = 0;
      Object localObject5 = null;
      boolean bool1;
      for (;;)
      {
        bool1 = true;
        if (j >= i) {
          break;
        }
        Object localObject6 = localObject1[j];
        String str1 = ((Class)localObject6).getSimpleName();
        String str2 = "ConnectionCallbacks";
        boolean bool2 = str1.equalsIgnoreCase(str2);
        Object localObject7;
        if (bool2)
        {
          localObject3 = new Class[bool1];
          localObject3[0] = localObject6;
          localObject7 = ((Class)localObject6).getClassLoader();
          localObject3 = Proxy.newProxyInstance((ClassLoader)localObject7, (Class[])localObject3, paramInvocationHandler1);
        }
        else
        {
          str1 = ((Class)localObject6).getSimpleName();
          str2 = "OnConnectionFailedListener";
          bool2 = str1.equalsIgnoreCase(str2);
          if (bool2)
          {
            localObject4 = new Class[bool1];
            localObject4[0] = localObject6;
            localObject7 = ((Class)localObject6).getClassLoader();
            localObject4 = Proxy.newProxyInstance((ClassLoader)localObject7, (Class[])localObject4, paramInvocationHandler2);
          }
          else
          {
            localObject7 = ((Class)localObject6).getSimpleName();
            str1 = "Builder";
            bool1 = ((String)localObject7).equalsIgnoreCase(str1);
            if (bool1) {
              localObject2 = localObject6;
            }
          }
        }
        j += 1;
      }
      if (localObject2 != null)
      {
        paramInvocationHandler1 = new Class[bool1];
        paramInvocationHandler2 = Context.class;
        paramInvocationHandler1[0] = paramInvocationHandler2;
        paramInvocationHandler1 = ((Class)localObject2).getDeclaredConstructor(paramInvocationHandler1);
        paramInvocationHandler2 = new Object[bool1];
        paramInvocationHandler2[0] = paramContext;
        paramContext = paramInvocationHandler1.newInstance(paramInvocationHandler2);
        paramInvocationHandler1 = "com.google.android.gms.common.api.Api";
        paramInvocationHandler1 = Class.forName(paramInvocationHandler1);
        paramInvocationHandler2 = "com.google.android.gms.common.api.GoogleApiClient$ConnectionCallbacks";
        paramInvocationHandler2 = Class.forName(paramInvocationHandler2);
        localObject1 = "com.google.android.gms.common.api.GoogleApiClient$OnConnectionFailedListener";
        localObject1 = Class.forName((String)localObject1);
        Object localObject8 = "addApi";
        localObject5 = new Class[bool1];
        localObject5[0] = paramInvocationHandler1;
        paramInvocationHandler1 = ((Class)localObject2).getMethod((String)localObject8, (Class[])localObject5);
        localObject8 = "addConnectionCallbacks";
        localObject5 = new Class[bool1];
        localObject5[0] = paramInvocationHandler2;
        paramInvocationHandler2 = ((Class)localObject2).getMethod((String)localObject8, (Class[])localObject5);
        localObject8 = "addOnConnectionFailedListener";
        localObject5 = new Class[bool1];
        localObject5[0] = localObject1;
        localObject1 = ((Class)localObject2).getMethod((String)localObject8, (Class[])localObject5);
        localObject8 = "build";
        localObject8 = ((Class)localObject2).getMethod((String)localObject8, null);
        paramString = Class.forName(paramString);
        localObject5 = "API";
        paramString = paramString.getDeclaredField((String)localObject5);
        localObject5 = new Object[bool1];
        paramString = paramString.get(null);
        localObject5[0] = paramString;
        paramInvocationHandler1.invoke(paramContext, (Object[])localObject5);
        paramInvocationHandler1 = new Object[bool1];
        paramInvocationHandler1[0] = localObject3;
        paramInvocationHandler2.invoke(paramContext, paramInvocationHandler1);
        paramInvocationHandler1 = new Object[bool1];
        paramInvocationHandler1[0] = localObject4;
        ((Method)localObject1).invoke(paramContext, paramInvocationHandler1);
        return ((Method)localObject8).invoke(paramContext, null);
      }
    }
    catch (ClassCastException|NoSuchMethodException|InstantiationException|IllegalAccessException|InvocationTargetException|ClassNotFoundException|NoSuchFieldException localClassCastException) {}
    return null;
  }
  
  public static void a(Object paramObject)
  {
    if (paramObject == null) {
      return;
    }
    Object localObject = "com.google.android.gms.common.api.GoogleApiClient";
    try
    {
      localObject = Class.forName((String)localObject);
      String str = "connect";
      localObject = ((Class)localObject).getMethod(str, null);
      ((Method)localObject).invoke(paramObject, null);
      return;
    }
    catch (InvocationTargetException localInvocationTargetException) {}catch (ClassNotFoundException|NoSuchMethodException|IllegalAccessException localClassNotFoundException) {}
  }
  
  public static void b(Object paramObject)
  {
    if (paramObject == null) {
      return;
    }
    Object localObject = "com.google.android.gms.common.api.GoogleApiClient";
    try
    {
      localObject = Class.forName((String)localObject);
      String str = "disconnect";
      localObject = ((Class)localObject).getMethod(str, null);
      ((Method)localObject).invoke(paramObject, null);
      return;
    }
    catch (ClassNotFoundException localClassNotFoundException) {}catch (NoSuchMethodException|IllegalAccessException|InvocationTargetException localNoSuchMethodException) {}
  }
}

/* Location:
 * Qualified Name:     com.inmobi.signals.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */