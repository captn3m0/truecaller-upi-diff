package com.inmobi.signals;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.inmobi.commons.core.e.f;
import com.inmobi.commons.core.utilities.e;
import com.inmobi.commons.core.utilities.uid.d;
import com.inmobi.signals.activityrecognition.ActivityRecognitionManager;
import com.inmobi.signals.b.c;
import com.inmobi.signals.b.c.a;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

final class i$a
  extends Handler
{
  private List a;
  private boolean b;
  
  i$a(Looper paramLooper)
  {
    super(paramLooper);
    paramLooper = new java/util/ArrayList;
    paramLooper.<init>();
    a = paramLooper;
  }
  
  private void a(m paramm)
  {
    Object localObject = a;
    if (localObject != null)
    {
      localObject = a;
      if (localObject == null)
      {
        localObject = b;
        if (localObject == null)
        {
          i = 0;
          localObject = null;
          break label39;
        }
      }
      int i = 1;
      label39:
      if (i != 0)
      {
        a.add(paramm);
        paramm = a;
        int j = paramm.size();
        localObject = aa.a;
        i = d;
        if (j > i)
        {
          try
          {
            paramm = com.inmobi.commons.core.e.b.a();
            localObject = new com/inmobi/commons/core/e/f;
            String str1 = "signals";
            String str2 = "SampleSizeExceeded";
            ((f)localObject).<init>(str1, str2);
            paramm.a((f)localObject);
          }
          catch (Exception paramm)
          {
            i.b();
            paramm.getMessage();
          }
          for (;;)
          {
            paramm = a;
            j = paramm.size();
            localObject = aa.a;
            i = d;
            if (j <= i) {
              break;
            }
            paramm = a;
            paramm.remove(0);
          }
        }
      }
    }
  }
  
  public final void handleMessage(Message paramMessage)
  {
    int i = what;
    int j = 3;
    Object localObject1;
    int k;
    Object localObject3;
    Object localObject4;
    switch (i)
    {
    default: 
      break;
    case 4: 
      localObject1 = new com/inmobi/signals/l;
      ((l)localObject1).<init>();
      paramMessage = n.a().c();
      a = paramMessage;
      paramMessage = a;
      c = paramMessage;
      o.a();
      paramMessage = o.b();
      b = paramMessage;
      com.inmobi.signals.activityrecognition.b.a();
      paramMessage = com.inmobi.signals.activityrecognition.b.c();
      d = paramMessage;
      paramMessage = aa.a;
      Object localObject2 = new com/inmobi/signals/k;
      String str1 = e;
      k = f;
      int m = g;
      localObject3 = p.a().d();
      localObject4 = localObject2;
      ((k)localObject2).<init>(str1, k, m, (d)localObject3, (l)localObject1);
      paramMessage = new com/inmobi/signals/j;
      paramMessage.<init>((k)localObject2);
      localObject2 = new java/lang/Thread;
      localObject4 = new com/inmobi/signals/j$1;
      ((j.1)localObject4).<init>(paramMessage);
      ((Thread)localObject2).<init>((Runnable)localObject4);
      ((Thread)localObject2).start();
      com.inmobi.signals.activityrecognition.b.a();
      com.inmobi.signals.activityrecognition.b.d();
      paramMessage = new java/util/ArrayList;
      paramMessage.<init>();
      a = paramMessage;
      break;
    case 3: 
      i.b();
      boolean bool1 = b;
      if (bool1)
      {
        sendEmptyMessage(2);
        return;
      }
      paramMessage = aa.a;
      boolean bool2 = q;
      k = 0;
      if (bool2)
      {
        bool1 = a;
        if (bool1)
        {
          bool1 = true;
          break label288;
        }
      }
      bool1 = false;
      paramMessage = null;
      if (bool1)
      {
        paramMessage = com.inmobi.signals.activityrecognition.b.a();
        bool2 = com.inmobi.signals.activityrecognition.b.e();
        if (bool2)
        {
          bool2 = com.inmobi.signals.activityrecognition.b.f();
          if (bool2)
          {
            localObject4 = a;
            bool2 = ((Handler)localObject4).hasMessages(0);
            if (!bool2)
            {
              ActivityRecognitionManager.a();
              paramMessage = a;
              paramMessage.sendEmptyMessage(0);
            }
          }
        }
      }
      else
      {
        paramMessage = com.inmobi.signals.activityrecognition.b.a();
        paramMessage.b();
      }
      paramMessage = new com/inmobi/signals/m;
      paramMessage.<init>();
      localObject4 = com.inmobi.signals.b.b.a();
      a = ((com.inmobi.signals.b.a)localObject4);
      localObject4 = n.a().d();
      c = ((Map)localObject4);
      localObject4 = aa.a;
      int n = k;
      if (n != 0)
      {
        bool2 = a;
        if (bool2)
        {
          bool2 = true;
          break label443;
        }
      }
      bool2 = false;
      localObject4 = null;
      if (bool2)
      {
        bool2 = com.inmobi.commons.a.a.a();
        if (bool2)
        {
          localObject3 = "android.permission.ACCESS_COARSE_LOCATION";
          String[] tmp469_466 = new String[3];
          String[] tmp470_469 = tmp469_466;
          String[] tmp470_469 = tmp469_466;
          tmp470_469[0] = "android.permission.ACCESS_WIFI_STATE";
          tmp470_469[1] = "android.permission.CHANGE_WIFI_STATE";
          tmp470_469[2] = localObject3;
          localObject4 = tmp470_469;
          n = 0;
          while (n < j)
          {
            localObject3 = localObject4[n];
            localObject1 = com.inmobi.commons.a.a.b();
            String str2 = "signals";
            boolean bool3 = e.a((Context)localObject1, str2, (String)localObject3);
            if (!bool3) {
              break label541;
            }
            int i1;
            n += 1;
          }
          k = 1;
        }
        if (k != 0)
        {
          localObject4 = new com/inmobi/signals/i$a$1;
          ((i.a.1)localObject4).<init>(this, paramMessage);
          bool2 = c.a((c.a)localObject4);
          if (bool2) {
            break label583;
          }
          a(paramMessage);
          break label583;
        }
      }
      a(paramMessage);
      long l = aa.a.b * 1000;
      sendEmptyMessageDelayed(j, l);
      return;
    case 2: 
      i.b();
      com.inmobi.signals.activityrecognition.b.a().b();
      removeMessages(j);
      sendEmptyMessage(4);
      return;
    case 1: 
      label288:
      label443:
      label541:
      label583:
      i.b();
      sendEmptyMessage(j);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.signals.i.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */