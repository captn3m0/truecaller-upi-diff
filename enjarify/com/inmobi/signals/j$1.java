package com.inmobi.signals;

import android.os.SystemClock;
import com.inmobi.commons.core.e.b;
import com.inmobi.commons.core.e.f;
import com.inmobi.commons.core.network.c;
import com.inmobi.commons.core.network.d;
import com.inmobi.commons.core.network.e;
import com.inmobi.commons.core.utilities.b.h;
import java.util.HashMap;
import java.util.Map;

final class j$1
  implements Runnable
{
  j$1(j paramj) {}
  
  public final void run()
  {
    int i = 0;
    b localb = null;
    long l1;
    Object localObject2;
    Object localObject1;
    Object localObject5;
    for (;;)
    {
      k localk = j.a(a);
      int j = a;
      if (i > j) {
        return;
      }
      j.a();
      l1 = SystemClock.elapsedRealtime();
      localObject2 = new com/inmobi/commons/core/network/e;
      Object localObject3 = j.a(a);
      ((e)localObject2).<init>((c)localObject3);
      localObject2 = ((e)localObject2).a();
      try
      {
        localObject3 = o.a();
        Object localObject4 = a;
        localObject4 = j.a((j)localObject4);
        long l2 = ((k)localObject4).e();
        ((o)localObject3).a(l2);
        localObject3 = o.a();
        l2 = ((d)localObject2).c();
        ((o)localObject3).b(l2);
        localObject3 = o.a();
        l2 = SystemClock.elapsedRealtime() - l1;
        ((o)localObject3).c(l2);
      }
      catch (Exception localException3)
      {
        j.a();
        localException3.getMessage();
      }
      boolean bool = ((d)localObject2).a();
      if (!bool) {
        break;
      }
      j.a();
      i += 1;
      localObject1 = j.a(a);
      int k = a;
      if (i > k) {
        try
        {
          localb = b.a();
          localObject1 = new com/inmobi/commons/core/e/f;
          localObject5 = "signals";
          localObject2 = "RetryCountExceeded";
          ((f)localObject1).<init>((String)localObject5, (String)localObject2);
          localb.a((f)localObject1);
          return;
        }
        catch (Exception localException1)
        {
          j.a();
          localException1.getMessage();
          return;
        }
      }
      try
      {
        localObject1 = a;
        localObject1 = j.a((j)localObject1);
        k = b * 1000;
        l1 = k;
        Thread.sleep(l1);
      }
      catch (InterruptedException localInterruptedException)
      {
        j.a();
      }
    }
    j.a();
    try
    {
      HashMap localHashMap = new java/util/HashMap;
      localHashMap.<init>();
      localObject1 = "url";
      localObject5 = a;
      localObject5 = j.a((j)localObject5);
      localObject5 = o;
      localHashMap.put(localObject1, localObject5);
      localObject1 = "latency";
      long l3 = SystemClock.elapsedRealtime();
      long l4 = 0L;
      l3 -= l4;
      localObject5 = Long.valueOf(l3);
      localHashMap.put(localObject1, localObject5);
      localObject1 = a;
      localObject1 = j.a((j)localObject1);
      l1 = ((k)localObject1).e();
      long l5 = ((d)localObject2).c();
      l1 += l5;
      localObject2 = "payloadSize";
      localObject1 = Long.valueOf(l1);
      localHashMap.put(localObject2, localObject1);
      b.a();
      localObject1 = "signals";
      localObject5 = "NICElatency";
      b.a((String)localObject1, (String)localObject5, localHashMap);
      localHashMap = new java/util/HashMap;
      localHashMap.<init>();
      localObject1 = "sessionId";
      localObject5 = h.a();
      localObject5 = a;
      localHashMap.put(localObject1, localObject5);
      localObject1 = o.a();
      localObject5 = "totalWifiSentBytes";
      l5 = a;
      localObject2 = Long.valueOf(l5);
      localHashMap.put(localObject5, localObject2);
      localObject5 = "totalWifiReceivedBytes";
      l5 = b;
      localObject2 = Long.valueOf(l5);
      localHashMap.put(localObject5, localObject2);
      localObject5 = "totalCarrierSentBytes";
      l5 = c;
      localObject2 = Long.valueOf(l5);
      localHashMap.put(localObject5, localObject2);
      localObject5 = "totalCarrierReceivedBytes";
      l5 = d;
      localObject2 = Long.valueOf(l5);
      localHashMap.put(localObject5, localObject2);
      localObject5 = "totalNetworkTime";
      l5 = e;
      localObject1 = Long.valueOf(l5);
      localHashMap.put(localObject5, localObject1);
      b.a();
      localObject1 = "signals";
      localObject5 = "SDKNetworkStats";
      b.a((String)localObject1, (String)localObject5, localHashMap);
      return;
    }
    catch (Exception localException2)
    {
      j.a();
      localException2.getMessage();
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.signals.j.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */