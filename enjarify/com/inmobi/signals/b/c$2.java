package com.inmobi.signals.b;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import com.inmobi.signals.p;
import com.inmobi.signals.q;
import com.inmobi.signals.q.b;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

final class c$2
  extends BroadcastReceiver
{
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    paramContext = c.b();
    paramIntent = c.d();
    Object localObject1 = "wifi";
    paramIntent = (WifiManager)paramIntent.getSystemService((String)localObject1);
    c.c();
    if ((paramContext != null) && (paramIntent != null))
    {
      paramIntent = paramIntent.getScanResults();
      localObject1 = aa.a;
      int i = j;
      boolean bool2 = b.a(i);
      int j = 1;
      boolean bool1 = b.a(i, j);
      ArrayList localArrayList = new java/util/ArrayList;
      localArrayList.<init>();
      if (paramIntent != null)
      {
        paramIntent = paramIntent.iterator();
        for (;;)
        {
          boolean bool3 = paramIntent.hasNext();
          if (!bool3) {
            break;
          }
          ScanResult localScanResult = (ScanResult)paramIntent.next();
          Object localObject2 = SSID;
          boolean bool4 = b.a(bool2, (String)localObject2);
          if (!bool4)
          {
            bool4 = false;
            localObject2 = null;
            if (localScanResult != null)
            {
              a locala = new com/inmobi/signals/b/a;
              locala.<init>();
              String str = BSSID;
              long l = b.a(str);
              a = l;
              if (!bool1) {
                localObject2 = SSID;
              }
              b = ((String)localObject2);
              int k = level;
              c = k;
              localObject2 = locala;
            }
            localArrayList.add(localObject2);
          }
        }
      }
      c.a(localArrayList);
      paramIntent = c.e();
      paramContext.a(paramIntent);
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.signals.b.c.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */