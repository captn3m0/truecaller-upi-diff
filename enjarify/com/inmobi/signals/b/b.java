package com.inmobi.signals.b;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import com.inmobi.commons.core.utilities.e;
import com.inmobi.signals.p;
import com.inmobi.signals.q;
import com.inmobi.signals.q.b;

public final class b
{
  static long a(String paramString)
  {
    String str1 = "\\:";
    paramString = paramString.split(str1);
    int i = 6;
    byte[] arrayOfByte = new byte[i];
    int j = 0;
    int k;
    for (;;)
    {
      k = 16;
      if (j < i) {
        try
        {
          String str2 = paramString[j];
          k = (byte)Integer.parseInt(str2, k);
          arrayOfByte[j] = k;
          j += 1;
        }
        catch (NumberFormatException localNumberFormatException)
        {
          return 0L;
        }
      }
    }
    long l1 = arrayOfByte[5];
    long l2 = 255L;
    l1 &= l2;
    long l3 = (arrayOfByte[4] & l2) << 8;
    l1 |= l3;
    long l4 = (arrayOfByte[3] & l2) << k | l1;
    l1 = (arrayOfByte[2] & l2) << 24;
    l4 |= l1;
    l1 = (arrayOfByte[1] & l2) << 32;
    l4 |= l1;
    return (arrayOfByte[0] & l2) << 40 | l4;
  }
  
  public static a a()
  {
    boolean bool1 = com.inmobi.commons.a.a.a();
    boolean bool3 = false;
    int j = 1;
    if (bool1)
    {
      localObject = com.inmobi.commons.a.a.b();
      String str1 = "signals";
      String str2 = "android.permission.ACCESS_WIFI_STATE";
      bool1 = e.a((Context)localObject, str1, str2);
      if (bool1)
      {
        bool1 = true;
        break label46;
      }
    }
    bool1 = false;
    Object localObject = null;
    label46:
    if (bool1)
    {
      localObject = aa.a;
      boolean bool4 = l;
      if (bool4)
      {
        bool1 = a;
        if (bool1) {
          bool3 = true;
        }
      }
      if (bool3)
      {
        int i = aa.a.j;
        bool3 = a(i);
        boolean bool2 = a(i, j);
        return a(bool3, bool2);
      }
    }
    return null;
  }
  
  private static a a(boolean paramBoolean1, boolean paramBoolean2)
  {
    Object localObject1 = com.inmobi.commons.a.a.b();
    Object localObject2 = null;
    if (localObject1 == null) {
      return null;
    }
    Object localObject3 = "wifi";
    a locala;
    try
    {
      localObject1 = ((Context)localObject1).getSystemService((String)localObject3);
      localObject1 = (WifiManager)localObject1;
      localObject1 = ((WifiManager)localObject1).getConnectionInfo();
      if (localObject1 != null)
      {
        localObject3 = ((WifiInfo)localObject1).getBSSID();
        String str = ((WifiInfo)localObject1).getSSID();
        if (localObject3 != null)
        {
          paramBoolean1 = a(paramBoolean1, str);
          if (!paramBoolean1)
          {
            locala = new com/inmobi/signals/b/a;
            locala.<init>();
            try
            {
              long l = a((String)localObject3);
              a = l;
              if (str != null)
              {
                localObject3 = "\"";
                boolean bool = str.startsWith((String)localObject3);
                if (bool)
                {
                  localObject3 = "\"";
                  bool = str.endsWith((String)localObject3);
                  if (bool)
                  {
                    int i = str.length();
                    int j = 1;
                    i -= j;
                    localObject3 = str.substring(j, i);
                    break label165;
                  }
                }
              }
              localObject3 = str;
              label165:
              if (!paramBoolean2) {
                localObject2 = localObject3;
              }
              b = ((String)localObject2);
              paramBoolean2 = ((WifiInfo)localObject1).getRssi();
              c = paramBoolean2;
              paramBoolean2 = ((WifiInfo)localObject1).getIpAddress();
              d = paramBoolean2;
            }
            catch (Exception localException1)
            {
              break label226;
            }
          }
        }
      }
      paramBoolean1 = false;
      locala = null;
    }
    catch (Exception localException2)
    {
      paramBoolean1 = false;
      locala = null;
      label226:
      localObject1 = com.inmobi.commons.core.a.a.a();
      localObject2 = new com/inmobi/commons/core/e/a;
      ((com.inmobi.commons.core.e.a)localObject2).<init>(localException2);
      ((com.inmobi.commons.core.a.a)localObject1).a((com.inmobi.commons.core.e.a)localObject2);
    }
    return locala;
  }
  
  static boolean a(int paramInt)
  {
    int i = 2;
    paramInt = a(paramInt, i);
    return paramInt == 0;
  }
  
  static boolean a(int paramInt1, int paramInt2)
  {
    paramInt1 &= paramInt2;
    return paramInt1 == paramInt2;
  }
  
  static boolean a(boolean paramBoolean, String paramString)
  {
    if ((paramBoolean) && (paramString != null))
    {
      String str = "_nomap";
      paramBoolean = paramString.endsWith(str);
      if (paramBoolean) {
        return true;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.signals.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */