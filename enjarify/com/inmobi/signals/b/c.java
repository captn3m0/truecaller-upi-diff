package com.inmobi.signals.b;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.Looper;
import com.inmobi.commons.a.a;
import java.util.List;

public class c
{
  private static final String a = "c";
  private static Context b;
  private static c.a c;
  private static Handler d;
  private static boolean e = false;
  private static final IntentFilter f;
  private static List g;
  private static Runnable h;
  private static final BroadcastReceiver i;
  
  static
  {
    Object localObject = new android/content/IntentFilter;
    ((IntentFilter)localObject).<init>("android.net.wifi.SCAN_RESULTS");
    f = (IntentFilter)localObject;
    localObject = new com/inmobi/signals/b/c$1;
    ((c.1)localObject).<init>();
    h = (Runnable)localObject;
    localObject = new com/inmobi/signals/b/c$2;
    ((c.2)localObject).<init>();
    i = (BroadcastReceiver)localObject;
  }
  
  public static List a()
  {
    return g;
  }
  
  private static boolean a(Looper paramLooper, c.a parama)
  {
    synchronized (c.class)
    {
      Object localObject = d;
      IntentFilter localIntentFilter = null;
      if (localObject != null) {
        return false;
      }
      localObject = a.b();
      if (localObject == null) {
        return false;
      }
      String str = "wifi";
      localObject = ((Context)localObject).getSystemService(str);
      localObject = (WifiManager)localObject;
      if (localObject != null)
      {
        boolean bool1 = ((WifiManager)localObject).isWifiEnabled();
        if (bool1)
        {
          c = parama;
          parama = new android/os/Handler;
          parama.<init>(paramLooper);
          d = parama;
          paramLooper = h;
          long l = 10000L;
          parama.postDelayed(paramLooper, l);
          boolean bool2 = e;
          if (!bool2)
          {
            bool2 = true;
            e = bool2;
            paramLooper = b;
            parama = i;
            localIntentFilter = f;
            bool1 = false;
            str = null;
            Handler localHandler = d;
            paramLooper.registerReceiver(parama, localIntentFilter, null, localHandler);
          }
          bool2 = ((WifiManager)localObject).startScan();
          return bool2;
        }
      }
      return false;
    }
  }
  
  public static boolean a(c.a parama)
  {
    b = a.b();
    return a(Looper.myLooper(), parama);
  }
  
  private static void f()
  {
    synchronized (c.class)
    {
      Object localObject1 = d;
      if (localObject1 == null) {
        return;
      }
      localObject1 = d;
      Object localObject3 = h;
      ((Handler)localObject1).removeCallbacks((Runnable)localObject3);
      boolean bool = e;
      if (bool)
      {
        bool = false;
        localObject1 = null;
        e = false;
      }
      try
      {
        localObject1 = b;
        localObject3 = i;
        ((Context)localObject1).unregisterReceiver((BroadcastReceiver)localObject3);
      }
      catch (IllegalArgumentException localIllegalArgumentException)
      {
        for (;;) {}
      }
      bool = false;
      localObject1 = null;
      d = null;
      c = null;
      b = null;
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.signals.b.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */