package com.inmobi.signals.activityrecognition;

import android.app.PendingIntent;
import android.content.Intent;
import com.inmobi.commons.a.a;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

final class ActivityRecognitionManager$a
  implements InvocationHandler
{
  public final Object invoke(Object paramObject, Method paramMethod, Object[] paramArrayOfObject)
  {
    if (paramArrayOfObject != null)
    {
      paramObject = paramMethod.getName();
      boolean bool = ((String)paramObject).equals("onConnected");
      if (bool)
      {
        bool = a.a();
        if (bool)
        {
          paramObject = new android/content/Intent;
          paramMethod = a.b();
          paramArrayOfObject = ActivityRecognitionManager.class;
          ((Intent)paramObject).<init>(paramMethod, paramArrayOfObject);
          paramMethod = a.b();
          int i = 134217728;
          int j = 0;
          localObject1 = null;
          paramObject = PendingIntent.getService(paramMethod, 0, (Intent)paramObject, i);
          paramMethod = "com.google.android.gms.location.ActivityRecognition";
          try
          {
            paramMethod = Class.forName(paramMethod);
            paramArrayOfObject = "ActivityRecognitionApi";
            paramMethod = paramMethod.getDeclaredField(paramArrayOfObject);
            paramArrayOfObject = "com.google.android.gms.common.api.GoogleApiClient";
            paramArrayOfObject = Class.forName(paramArrayOfObject);
            Object localObject2 = "com.google.android.gms.location.ActivityRecognitionApi";
            localObject2 = Class.forName((String)localObject2);
            Object localObject3 = "requestActivityUpdates";
            int k = 3;
            Class[] arrayOfClass = new Class[k];
            arrayOfClass[0] = paramArrayOfObject;
            paramArrayOfObject = Long.TYPE;
            int m = 1;
            arrayOfClass[m] = paramArrayOfObject;
            paramArrayOfObject = PendingIntent.class;
            int n = 2;
            arrayOfClass[n] = paramArrayOfObject;
            paramArrayOfObject = ((Class)localObject2).getMethod((String)localObject3, arrayOfClass);
            paramMethod = paramMethod.get(null);
            localObject2 = new Object[k];
            localObject3 = ActivityRecognitionManager.d();
            localObject2[0] = localObject3;
            j = 1000;
            localObject1 = Integer.valueOf(j);
            localObject2[m] = localObject1;
            localObject2[n] = paramObject;
            paramArrayOfObject.invoke(paramMethod, (Object[])localObject2);
          }
          catch (NoSuchFieldException localNoSuchFieldException)
          {
            ActivityRecognitionManager.e();
          }
          catch (NoSuchMethodException localNoSuchMethodException)
          {
            ActivityRecognitionManager.e();
          }
          catch (IllegalAccessException localIllegalAccessException)
          {
            ActivityRecognitionManager.e();
          }
          catch (ClassNotFoundException localClassNotFoundException)
          {
            ActivityRecognitionManager.e();
          }
          catch (InvocationTargetException localInvocationTargetException)
          {
            ActivityRecognitionManager.e();
          }
        }
        return null;
      }
      paramObject = paramMethod.getName();
      Object localObject1 = "onConnectionSuspended";
      bool = ((String)paramObject).equals(localObject1);
      if (bool) {
        return null;
      }
    }
    return paramMethod.invoke(this, paramArrayOfObject);
  }
}

/* Location:
 * Qualified Name:     com.inmobi.signals.activityrecognition.ActivityRecognitionManager.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */