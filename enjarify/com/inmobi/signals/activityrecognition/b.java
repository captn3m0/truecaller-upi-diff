package com.inmobi.signals.activityrecognition;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import com.inmobi.commons.a.a;
import com.inmobi.commons.core.utilities.e;
import java.util.ArrayList;
import java.util.List;

public class b
{
  private static final String b = "b";
  private static final Object c;
  private static volatile b d;
  private static List e;
  public Handler a;
  private HandlerThread f;
  
  static
  {
    Object localObject = new java/lang/Object;
    localObject.<init>();
    c = localObject;
  }
  
  private b()
  {
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    e = (List)localObject;
    localObject = new android/os/HandlerThread;
    ((HandlerThread)localObject).<init>("ActivityRecognitionSampler");
    f = ((HandlerThread)localObject);
    f.start();
    localObject = new com/inmobi/signals/activityrecognition/b$a;
    Looper localLooper = f.getLooper();
    ((b.a)localObject).<init>(localLooper);
    a = ((Handler)localObject);
  }
  
  public static b a()
  {
    b localb1 = d;
    if (localb1 == null) {
      synchronized (c)
      {
        localb1 = d;
        if (localb1 == null)
        {
          localb1 = new com/inmobi/signals/activityrecognition/b;
          localb1.<init>();
          d = localb1;
        }
      }
    }
    return localb2;
  }
  
  public static List c()
  {
    return e;
  }
  
  public static void d()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    e = localArrayList;
  }
  
  public static boolean e()
  {
    boolean bool = a.a();
    if (bool)
    {
      Context localContext = a.b();
      String str1 = "signals";
      String str2 = "com.google.android.gms.permission.ACTIVITY_RECOGNITION";
      bool = e.a(localContext, str1, str2);
      if (bool) {
        return true;
      }
    }
    return false;
  }
  
  public static boolean f()
  {
    try
    {
      Object localObject = a.b();
      localObject = ((Context)localObject).getPackageManager();
      Intent localIntent = new android/content/Intent;
      Context localContext = a.b();
      Class localClass = ActivityRecognitionManager.class;
      localIntent.<init>(localContext, localClass);
      int i = 65536;
      localObject = ((PackageManager)localObject).queryIntentServices(localIntent, i);
      int j = ((List)localObject).size();
      return j > 0;
    }
    catch (Exception localException) {}
    return false;
  }
  
  public final void b()
  {
    boolean bool = e();
    if (bool)
    {
      bool = f();
      if (bool)
      {
        Handler localHandler = a;
        bool = localHandler.hasMessages(0);
        if (bool)
        {
          ActivityRecognitionManager.b();
          localHandler = a;
          localHandler.removeMessages(0);
        }
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.signals.activityrecognition.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */