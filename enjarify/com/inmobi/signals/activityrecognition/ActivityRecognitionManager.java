package com.inmobi.signals.activityrecognition;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import com.inmobi.commons.a.a;
import com.inmobi.commons.core.utilities.f;
import com.inmobi.signals.h;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ActivityRecognitionManager
  extends IntentService
{
  private static final String a = "ActivityRecognitionManager";
  private static Object b;
  private static Object c = null;
  private static Object d = null;
  
  static
  {
    Object localObject = new java/lang/Object;
    localObject.<init>();
    b = localObject;
  }
  
  public ActivityRecognitionManager()
  {
    super("Activity service");
  }
  
  public static void a()
  {
    Object localObject = "signals";
    boolean bool = f.a((String)localObject);
    if (bool)
    {
      localObject = d;
      if (localObject == null)
      {
        bool = a.a();
        if (bool)
        {
          localObject = a.b();
          ActivityRecognitionManager.a locala1 = new com/inmobi/signals/activityrecognition/ActivityRecognitionManager$a;
          locala1.<init>((byte)0);
          ActivityRecognitionManager.a locala2 = new com/inmobi/signals/activityrecognition/ActivityRecognitionManager$a;
          locala2.<init>((byte)0);
          String str = "com.google.android.gms.location.ActivityRecognition";
          localObject = h.a((Context)localObject, locala1, locala2, str);
          d = localObject;
          h.a(localObject);
        }
      }
    }
  }
  
  static void b()
  {
    Object localObject1 = "signals";
    boolean bool = f.a((String)localObject1);
    if (bool)
    {
      localObject1 = d;
      if (localObject1 != null)
      {
        h.b(localObject1);
        localObject1 = b;
        Object localObject2 = null;
        try
        {
          c = null;
          d = null;
        }
        finally {}
      }
    }
  }
  
  public static int c()
  {
    ??? = c;
    int i = -1;
    if (??? != null) {
      synchronized (b)
      {
        Object localObject2 = c;
        if (localObject2 != null) {
          localObject2 = "com.google.android.gms.location.DetectedActivity";
        }
        try
        {
          localObject2 = Class.forName((String)localObject2);
          Object localObject3 = "getType";
          localObject2 = ((Class)localObject2).getMethod((String)localObject3, null);
          localObject3 = c;
          localObject2 = ((Method)localObject2).invoke(localObject3, null);
          localObject2 = (Integer)localObject2;
          i = ((Integer)localObject2).intValue();
          c = null;
        }
        catch (ClassNotFoundException|NoSuchMethodException|InvocationTargetException|IllegalAccessException|Exception localClassNotFoundException)
        {
          for (;;) {}
        }
      }
    } else {
      return i;
    }
  }
  
  protected void onHandleIntent(Intent paramIntent)
  {
    Object localObject1 = d;
    if (localObject1 != null) {
      localObject1 = "com.google.android.gms.location.ActivityRecognitionResult";
    }
    try
    {
      localObject1 = Class.forName((String)localObject1);
      ??? = "hasResult";
      int i = 1;
      Object localObject3 = new Class[i];
      Class localClass1 = Intent.class;
      localObject3[0] = localClass1;
      ??? = ((Class)localObject1).getMethod((String)???, (Class[])localObject3);
      localObject3 = new Object[i];
      localObject3[0] = paramIntent;
      localClass1 = null;
      ??? = ((Method)???).invoke(null, (Object[])localObject3);
      ??? = (Boolean)???;
      boolean bool = ((Boolean)???).booleanValue();
      if (bool)
      {
        ??? = "extractResult";
        localObject3 = new Class[i];
        Class localClass2 = Intent.class;
        localObject3[0] = localClass2;
        ??? = ((Class)localObject1).getMethod((String)???, (Class[])localObject3);
        Object[] arrayOfObject = new Object[i];
        arrayOfObject[0] = paramIntent;
        paramIntent = ((Method)???).invoke(null, arrayOfObject);
        ??? = "getMostProbableActivity";
        localObject1 = ((Class)localObject1).getMethod((String)???, null);
        synchronized (b)
        {
          paramIntent = ((Method)localObject1).invoke(paramIntent, null);
          c = paramIntent;
          return;
        }
      }
      return;
    }
    catch (InvocationTargetException localInvocationTargetException) {}catch (ClassNotFoundException|NoSuchMethodException|IllegalAccessException localClassNotFoundException)
    {
      for (;;) {}
    }
  }
}

/* Location:
 * Qualified Name:     com.inmobi.signals.activityrecognition.ActivityRecognitionManager
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */