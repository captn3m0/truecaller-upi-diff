package com.inmobi.signals;

import android.content.Context;
import android.content.pm.PackageManager;
import com.inmobi.commons.core.d.c;

public class g
{
  private static final String a = "g";
  private q.a b;
  private boolean c = false;
  private a d;
  private e e;
  
  g()
  {
    Object localObject = new com/inmobi/signals/a;
    ((a)localObject).<init>();
    d = ((a)localObject);
    localObject = new com/inmobi/signals/e;
    ((e)localObject).<init>();
    e = ((e)localObject);
  }
  
  private static boolean a(String paramString)
  {
    Object localObject = com.inmobi.commons.a.a.b();
    boolean bool = false;
    if (localObject == null) {
      return false;
    }
    try
    {
      localObject = ((Context)localObject).getPackageManager();
      int i = 256;
      ((PackageManager)localObject).getPackageInfo(paramString, i);
      bool = true;
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
    return bool;
  }
  
  public final void a(q.a parama)
  {
    try
    {
      b = parama;
      boolean bool1 = c;
      if (!bool1)
      {
        parama = d;
        parama = a;
        Object localObject = "carb_last_update_ts";
        long l1 = 0L;
        long l2 = parama.b((String)localObject, l1);
        bool1 = true;
        boolean bool2 = l2 < l1;
        if (!bool2)
        {
          bool2 = true;
        }
        else
        {
          long l3 = System.currentTimeMillis() - l2;
          q.a locala = b;
          int i = d * 1000;
          long l4 = i;
          boolean bool3 = l3 < l4;
          if (!bool3)
          {
            bool2 = true;
          }
          else
          {
            bool2 = false;
            localObject = null;
          }
        }
        if (bool2)
        {
          c = bool1;
          parama = new java/lang/Thread;
          localObject = new com/inmobi/signals/g$1;
          ((g.1)localObject).<init>(this);
          parama.<init>((Runnable)localObject);
          parama.start();
        }
      }
      return;
    }
    finally {}
  }
}

/* Location:
 * Qualified Name:     com.inmobi.signals.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */