package com.inmobi.signals.a;

import android.os.Build.VERSION;
import android.telephony.CellIdentityCdma;
import android.telephony.CellIdentityGsm;
import android.telephony.CellIdentityLte;
import android.telephony.CellIdentityWcdma;
import android.telephony.CellInfo;
import android.telephony.CellInfoCdma;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoWcdma;
import android.telephony.CellSignalStrengthCdma;
import android.telephony.CellSignalStrengthGsm;
import android.telephony.CellSignalStrengthLte;
import android.telephony.CellSignalStrengthWcdma;
import org.json.JSONException;
import org.json.JSONObject;

public class b
{
  private static final String d = "b";
  String a;
  int b;
  int c;
  
  public b() {}
  
  public b(CellInfo paramCellInfo, String paramString1, String paramString2, int paramInt)
  {
    boolean bool1 = paramCellInfo instanceof CellInfoGsm;
    int j;
    int k;
    if (bool1)
    {
      c = paramInt;
      paramCellInfo = (CellInfoGsm)paramCellInfo;
      paramInt = paramCellInfo.getCellSignalStrength().getDbm();
      b = paramInt;
      paramCellInfo = paramCellInfo.getCellIdentity();
      j = paramCellInfo.getLac();
      k = paramCellInfo.getCid();
      paramCellInfo = a(paramString1, paramString2, j, k, -1, -1 >>> 1);
      a = paramCellInfo;
      return;
    }
    bool1 = paramCellInfo instanceof CellInfoCdma;
    if (bool1)
    {
      c = paramInt;
      paramCellInfo = (CellInfoCdma)paramCellInfo;
      int m = paramCellInfo.getCellSignalStrength().getDbm();
      b = m;
      paramCellInfo = paramCellInfo.getCellIdentity();
      m = paramCellInfo.getSystemId();
      paramInt = paramCellInfo.getNetworkId();
      int n = paramCellInfo.getBasestationId();
      paramCellInfo = a(paramString1, m, paramInt, n);
      a = paramCellInfo;
      return;
    }
    int i = Build.VERSION.SDK_INT;
    int i1 = 18;
    boolean bool2;
    int i2;
    if (i >= i1)
    {
      bool2 = paramCellInfo instanceof CellInfoWcdma;
      if (bool2)
      {
        c = paramInt;
        paramCellInfo = (CellInfoWcdma)paramCellInfo;
        paramInt = paramCellInfo.getCellSignalStrength().getDbm();
        b = paramInt;
        paramCellInfo = paramCellInfo.getCellIdentity();
        j = paramCellInfo.getLac();
        k = paramCellInfo.getCid();
        i2 = paramCellInfo.getPsc();
        paramCellInfo = a(paramString1, paramString2, j, k, i2, -1 >>> 1);
        a = paramCellInfo;
      }
    }
    else
    {
      bool2 = paramCellInfo instanceof CellInfoLte;
      if (bool2)
      {
        c = paramInt;
        paramCellInfo = (CellInfoLte)paramCellInfo;
        CellSignalStrengthLte localCellSignalStrengthLte = paramCellInfo.getCellSignalStrength();
        paramInt = localCellSignalStrengthLte.getDbm();
        b = paramInt;
        paramCellInfo = paramCellInfo.getCellIdentity();
        j = paramCellInfo.getTac();
        k = paramCellInfo.getCi();
        i2 = -1;
        int i3 = paramCellInfo.getPci();
        paramCellInfo = a(paramString1, paramString2, j, k, i2, i3);
        a = paramCellInfo;
      }
    }
  }
  
  public static String a(String paramString, int paramInt1, int paramInt2, int paramInt3)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(paramString);
    localStringBuilder.append("#");
    localStringBuilder.append(paramInt1);
    localStringBuilder.append("#");
    localStringBuilder.append(paramInt2);
    localStringBuilder.append("#");
    localStringBuilder.append(paramInt3);
    return localStringBuilder.toString();
  }
  
  public static String a(String paramString1, String paramString2, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(paramString1);
    localStringBuilder.append("#");
    localStringBuilder.append(paramString2);
    localStringBuilder.append("#");
    localStringBuilder.append(paramInt1);
    localStringBuilder.append("#");
    localStringBuilder.append(paramInt2);
    paramString1 = "#";
    localStringBuilder.append(paramString1);
    int i = -1;
    if (paramInt3 == i) {
      paramString1 = "";
    } else {
      paramString1 = Integer.valueOf(paramInt3);
    }
    localStringBuilder.append(paramString1);
    paramString1 = "#";
    localStringBuilder.append(paramString1);
    i = -1 >>> 1;
    if (paramInt4 == i) {
      paramString1 = "";
    } else {
      paramString1 = Integer.valueOf(paramInt4);
    }
    localStringBuilder.append(paramString1);
    return localStringBuilder.toString();
  }
  
  public final JSONObject a()
  {
    JSONObject localJSONObject = new org/json/JSONObject;
    localJSONObject.<init>();
    String str1 = "id";
    try
    {
      String str2 = a;
      localJSONObject.put(str1, str2);
      int i = b;
      int j = -1 >>> 1;
      if (i != j)
      {
        str1 = "ss";
        j = b;
        localJSONObject.put(str1, j);
      }
      str1 = "nt";
      j = c;
      localJSONObject.put(str1, j);
    }
    catch (JSONException localJSONException)
    {
      for (;;) {}
    }
    return localJSONObject;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.signals.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */