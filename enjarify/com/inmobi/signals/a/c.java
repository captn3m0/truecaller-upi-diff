package com.inmobi.signals.a;

import android.content.Context;
import android.os.Build.VERSION;
import android.telephony.CellInfo;
import android.telephony.NeighboringCellInfo;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import com.inmobi.commons.core.utilities.e;
import com.inmobi.signals.p;
import com.inmobi.signals.q;
import com.inmobi.signals.q.b;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public class c
{
  private static final String a = "c";
  
  public static Map a()
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    Object localObject1 = com.inmobi.commons.a.a.b();
    if (localObject1 == null) {
      return localHashMap;
    }
    Object localObject2 = aa.a;
    boolean bool1 = n;
    int j = 0;
    int k = 1;
    if (bool1)
    {
      bool2 = a;
      if (bool2)
      {
        bool2 = true;
        break label65;
      }
    }
    boolean bool2 = false;
    localObject2 = null;
    label65:
    if (!bool2) {
      return localHashMap;
    }
    localObject2 = aa.a;
    int m = m;
    bool1 = a(m, 2);
    boolean bool3 = a(m, k);
    a locala = new com/inmobi/signals/a/a;
    locala.<init>();
    Object localObject3 = "phone";
    localObject1 = (TelephonyManager)((Context)localObject1).getSystemService((String)localObject3);
    if (!bool1)
    {
      localObject4 = a(((TelephonyManager)localObject1).getNetworkOperator());
      int i1 = localObject4[0];
      a = i1;
      i = localObject4[k];
      b = i;
      localObject4 = ((TelephonyManager)localObject1).getNetworkCountryIso();
      if (localObject4 != null)
      {
        localObject3 = Locale.ENGLISH;
        localObject4 = ((String)localObject4).toLowerCase((Locale)localObject3);
        e = ((String)localObject4);
      }
    }
    if (!bool3)
    {
      localObject1 = a(((TelephonyManager)localObject1).getSimOperator());
      n = localObject1[0];
      c = n;
      int i2 = localObject1[k];
      d = i2;
    }
    localObject1 = "s-ho";
    int n = c;
    int i = 0;
    Object localObject4 = null;
    j = -1;
    if (n == j)
    {
      n = d;
      if (n == j)
      {
        n = 0;
        localObject2 = null;
        break label335;
      }
    }
    localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>();
    k = c;
    ((StringBuilder)localObject2).append(k);
    String str = "_";
    ((StringBuilder)localObject2).append(str);
    k = d;
    ((StringBuilder)localObject2).append(k);
    localObject2 = ((StringBuilder)localObject2).toString();
    label335:
    localHashMap.put(localObject1, localObject2);
    localObject1 = "s-co";
    n = a;
    if (n == j)
    {
      n = b;
      if (n == j) {}
    }
    else
    {
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      i = a;
      ((StringBuilder)localObject2).append(i);
      ((StringBuilder)localObject2).append("_");
      i = b;
      ((StringBuilder)localObject2).append(i);
      localObject4 = ((StringBuilder)localObject2).toString();
    }
    localHashMap.put(localObject1, localObject4);
    localObject2 = e;
    localHashMap.put("s-iso", localObject2);
    return localHashMap;
  }
  
  private static boolean a(int paramInt1, int paramInt2)
  {
    paramInt1 &= paramInt2;
    return paramInt1 == paramInt2;
  }
  
  private static int[] a(String paramString)
  {
    int i = 2;
    int[] arrayOfInt = new int[i];
    int[] tmp7_6 = arrayOfInt;
    tmp7_6[0] = -1;
    tmp7_6[1] = -1;
    int j;
    if (paramString != null)
    {
      String str1 = "";
      boolean bool = paramString.equals(str1);
      if (!bool) {
        j = 3;
      }
    }
    try
    {
      String str2 = paramString.substring(0, j);
      int k = Integer.parseInt(str2);
      paramString = paramString.substring(j);
      int m = Integer.parseInt(paramString);
      arrayOfInt[0] = k;
      j = 1;
      arrayOfInt[j] = m;
    }
    catch (IndexOutOfBoundsException|NumberFormatException localIndexOutOfBoundsException)
    {
      for (;;) {}
    }
    return arrayOfInt;
    return arrayOfInt;
  }
  
  public static Map b()
  {
    Object localObject1 = aa.a;
    boolean bool1 = p;
    int i = 1;
    int j = 0;
    if (bool1)
    {
      bool3 = a;
      if (bool3)
      {
        bool3 = true;
        break label45;
      }
    }
    boolean bool3 = false;
    localObject1 = null;
    label45:
    bool1 = false;
    Object localObject2 = null;
    label116:
    Object localObject4;
    if (bool3)
    {
      localObject1 = com.inmobi.commons.a.a.b();
      Object localObject3;
      String str1;
      String str2;
      if (localObject1 != null)
      {
        localObject3 = "signals";
        boolean bool4 = e.a((Context)localObject1, (String)localObject3, "android.permission.ACCESS_COARSE_LOCATION");
        str1 = "signals";
        str2 = "android.permission.ACCESS_FINE_LOCATION";
        bool3 = e.a((Context)localObject1, str1, str2);
        if ((bool4) || (bool3))
        {
          bool3 = true;
          break label116;
        }
      }
      bool3 = false;
      localObject1 = null;
      if (bool3)
      {
        localObject1 = com.inmobi.commons.a.a.b();
        if (localObject1 != null)
        {
          localObject1 = (TelephonyManager)((Context)localObject1).getSystemService("phone");
          localObject3 = a(((TelephonyManager)localObject1).getNetworkOperator());
          str2 = String.valueOf(localObject3[0]);
          String str3 = String.valueOf(localObject3[i]);
          i = Build.VERSION.SDK_INT;
          int i1 = 17;
          int i2;
          int m;
          if (i >= i1)
          {
            localObject4 = ((TelephonyManager)localObject1).getAllCellInfo();
            if (localObject4 != null)
            {
              i2 = 0;
              CellInfo localCellInfo = null;
              i1 = 0;
              str1 = null;
              for (;;)
              {
                int i3 = ((List)localObject4).size();
                if (i1 >= i3) {
                  break;
                }
                localCellInfo = (CellInfo)((List)localObject4).get(i1);
                boolean bool5 = localCellInfo.isRegistered();
                if (bool5) {
                  break;
                }
                i1 += 1;
              }
              if (localCellInfo != null)
              {
                localObject2 = new com/inmobi/signals/a/b;
                m = ((TelephonyManager)localObject1).getNetworkType();
                ((b)localObject2).<init>(localCellInfo, str2, str3, m);
                break label485;
              }
            }
          }
          localObject4 = ((TelephonyManager)localObject1).getCellLocation();
          if (localObject4 != null)
          {
            j = localObject3[0];
            int n = -1;
            if (j != n)
            {
              localObject2 = new com/inmobi/signals/a/b;
              ((b)localObject2).<init>();
              boolean bool2 = localObject4 instanceof CdmaCellLocation;
              n = -1 >>> 1;
              if (bool2)
              {
                localObject4 = (CdmaCellLocation)localObject4;
                b = n;
                m = ((TelephonyManager)localObject1).getNetworkType();
                c = m;
                m = ((CdmaCellLocation)localObject4).getSystemId();
                int k = ((CdmaCellLocation)localObject4).getNetworkId();
                i = ((CdmaCellLocation)localObject4).getBaseStationId();
                localObject1 = b.a(str2, m, k, i);
                a = ((String)localObject1);
              }
              else
              {
                localObject4 = (GsmCellLocation)localObject4;
                b = n;
                m = ((TelephonyManager)localObject1).getNetworkType();
                c = m;
                i2 = ((GsmCellLocation)localObject4).getLac();
                int i4 = ((GsmCellLocation)localObject4).getCid();
                int i5 = ((GsmCellLocation)localObject4).getPsc();
                int i6 = -1 >>> 1;
                localObject1 = b.a(str2, str3, i2, i4, i5, i6);
                a = ((String)localObject1);
              }
            }
          }
        }
      }
    }
    label485:
    localObject1 = new java/util/HashMap;
    ((HashMap)localObject1).<init>();
    if (localObject2 != null)
    {
      localObject4 = "c-sc";
      localObject2 = ((b)localObject2).a().toString();
      ((HashMap)localObject1).put(localObject4, localObject2);
    }
    return (Map)localObject1;
  }
  
  public static Map c()
  {
    boolean bool1 = com.inmobi.commons.a.a.a();
    int i = 1;
    Object localObject3;
    if (bool1)
    {
      bool1 = com.inmobi.commons.a.a.a();
      int j = 0;
      localObject1 = null;
      Object localObject4;
      if (bool1)
      {
        localObject2 = com.inmobi.commons.a.a.b();
        localObject3 = "signals";
        localObject4 = "android.permission.ACCESS_COARSE_LOCATION";
        bool1 = e.a((Context)localObject2, (String)localObject3, (String)localObject4);
        if (bool1)
        {
          bool1 = true;
          break label59;
        }
      }
      bool1 = false;
      localObject2 = null;
      label59:
      if (bool1)
      {
        localObject2 = aa.a;
        bool2 = o;
        if (bool2)
        {
          bool1 = a;
          if (bool1)
          {
            bool1 = true;
            break label106;
          }
        }
        bool1 = false;
        localObject2 = null;
        label106:
        if (bool1)
        {
          localObject2 = com.inmobi.commons.a.a.b();
          if (localObject2 == null)
          {
            localObject2 = new java/util/ArrayList;
            ((ArrayList)localObject2).<init>();
            break label607;
          }
          localObject2 = (TelephonyManager)((Context)localObject2).getSystemService("phone");
          localObject3 = new java/util/ArrayList;
          ((ArrayList)localObject3).<init>();
          localObject4 = a(((TelephonyManager)localObject2).getNetworkOperator());
          String str1 = String.valueOf(localObject4[0]);
          String str2 = String.valueOf(localObject4[i]);
          int k = Build.VERSION.SDK_INT;
          int n = 17;
          boolean bool3;
          int i2;
          if (k >= n)
          {
            localObject4 = ((TelephonyManager)localObject2).getAllCellInfo();
            if (localObject4 != null)
            {
              localObject1 = ((List)localObject4).iterator();
              for (;;)
              {
                bool3 = ((Iterator)localObject1).hasNext();
                if (!bool3) {
                  break;
                }
                localObject4 = (CellInfo)((Iterator)localObject1).next();
                boolean bool4 = ((CellInfo)localObject4).isRegistered();
                if (!bool4)
                {
                  b localb = new com/inmobi/signals/a/b;
                  i2 = ((TelephonyManager)localObject2).getNetworkType();
                  localb.<init>((CellInfo)localObject4, str1, str2, i2);
                  ((List)localObject3).add(localb);
                }
              }
            }
          }
          localObject2 = ((TelephonyManager)localObject2).getNeighboringCellInfo();
          if (localObject2 != null)
          {
            bool3 = ((List)localObject2).isEmpty();
            if (!bool3)
            {
              localObject2 = ((List)localObject2).iterator();
              bool3 = ((Iterator)localObject2).hasNext();
              if (bool3)
              {
                localObject2 = (NeighboringCellInfo)((Iterator)localObject2).next();
                localObject4 = new com/inmobi/signals/a/b;
                ((b)localObject4).<init>();
                int i1 = ((NeighboringCellInfo)localObject2).getNetworkType();
                c = i1;
                i2 = ((NeighboringCellInfo)localObject2).getRssi();
                int i3 = 99;
                if (i2 == i3)
                {
                  j = -1 >>> 1;
                  b = j;
                }
                else
                {
                  i2 = 3;
                  if (i1 != i2)
                  {
                    i2 = 15;
                    if (i1 != i2) {
                      switch (i1)
                      {
                      default: 
                        break;
                      }
                    }
                  }
                  j = 1;
                  if (j != 0)
                  {
                    j = ((NeighboringCellInfo)localObject2).getRssi() + -116;
                    b = j;
                  }
                  else
                  {
                    j = ((NeighboringCellInfo)localObject2).getRssi() * 2 + -113;
                    b = j;
                  }
                }
                i2 = ((NeighboringCellInfo)localObject2).getLac();
                i3 = ((NeighboringCellInfo)localObject2).getCid();
                int i4 = -1;
                int i5 = -1 >>> 1;
                localObject2 = b.a(str1, str2, i2, i3, i4, i5);
                a = ((String)localObject2);
                ((List)localObject3).add(localObject4);
                localObject2 = localObject3;
                break label607;
              }
              localObject2 = new java/util/ArrayList;
              ((ArrayList)localObject2).<init>();
              break label607;
            }
          }
          localObject2 = new java/util/ArrayList;
          ((ArrayList)localObject2).<init>();
          break label607;
        }
      }
    }
    Object localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>();
    label607:
    Object localObject1 = new java/util/HashMap;
    ((HashMap)localObject1).<init>();
    boolean bool2 = ((List)localObject2).isEmpty();
    if (!bool2)
    {
      localObject3 = new org/json/JSONArray;
      ((JSONArray)localObject3).<init>();
      int m = ((List)localObject2).size() - i;
      localObject2 = ((b)((List)localObject2).get(m)).a();
      ((JSONArray)localObject3).put(localObject2);
      localObject2 = "v-sc";
      String str3 = ((JSONArray)localObject3).toString();
      ((HashMap)localObject1).put(localObject2, str3);
    }
    return (Map)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.signals.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */