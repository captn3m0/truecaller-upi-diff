package com.inmobi.signals;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

final class n$a
  implements InvocationHandler
{
  public final Object invoke(Object paramObject, Method paramMethod, Object[] paramArrayOfObject)
  {
    if (paramArrayOfObject != null)
    {
      paramObject = paramMethod.getName();
      boolean bool = ((String)paramObject).equals("onConnected");
      if (bool)
      {
        n.g();
        n.b(true);
        return null;
      }
      paramObject = paramMethod.getName();
      String str = "onConnectionSuspended";
      bool = ((String)paramObject).equals(str);
      if (bool)
      {
        n.b(false);
        n.g();
        return null;
      }
    }
    return paramMethod.invoke(this, paramArrayOfObject);
  }
}

/* Location:
 * Qualified Name:     com.inmobi.signals.n.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */