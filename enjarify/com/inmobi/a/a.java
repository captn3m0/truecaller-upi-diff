package com.inmobi.a;

import com.inmobi.ads.b.a;
import com.inmobi.ads.b.i;
import com.inmobi.commons.core.b.c;
import com.inmobi.commons.core.b.d;
import com.inmobi.commons.core.b.e;
import com.inmobi.commons.core.configs.b.c;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class a
  implements e, b.c
{
  public static AtomicBoolean b;
  private static final String e = "a";
  private static final Object f;
  private static volatile a g;
  public ExecutorService a;
  public com.inmobi.ads.b c;
  public String d;
  private com.inmobi.commons.core.f.a h;
  private d i;
  
  static
  {
    Object localObject = new java/lang/Object;
    localObject.<init>();
    f = localObject;
    localObject = new java/util/concurrent/atomic/AtomicBoolean;
    ((AtomicBoolean)localObject).<init>(false);
    b = (AtomicBoolean)localObject;
  }
  
  private a()
  {
    Object localObject = new com/inmobi/ads/b;
    ((com.inmobi.ads.b)localObject).<init>();
    c = ((com.inmobi.ads.b)localObject);
    localObject = c.f;
    d = ((String)localObject);
    localObject = new com/inmobi/commons/core/f/a;
    ((com.inmobi.commons.core.f.a)localObject).<init>();
    h = ((com.inmobi.commons.core.f.a)localObject);
    localObject = Executors.newSingleThreadExecutor();
    a = ((ExecutorService)localObject);
  }
  
  public static a a()
  {
    a locala1 = g;
    if (locala1 == null) {
      synchronized (f)
      {
        locala1 = g;
        if (locala1 == null)
        {
          locala1 = new com/inmobi/a/a;
          locala1.<init>();
          g = locala1;
        }
      }
    }
    return locala2;
  }
  
  private static String a(List paramList)
  {
    try
    {
      Object localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      Object localObject2 = null;
      Object localObject3 = com.inmobi.commons.core.utilities.b.b.a(false);
      ((Map)localObject1).putAll((Map)localObject3);
      localObject3 = "im-accid";
      Object localObject4 = com.inmobi.commons.a.a.e();
      ((Map)localObject1).put(localObject3, localObject4);
      localObject3 = "version";
      localObject4 = "2.0.0";
      ((Map)localObject1).put(localObject3, localObject4);
      localObject3 = "component";
      localObject4 = "trc";
      ((Map)localObject1).put(localObject3, localObject4);
      localObject3 = "adtype";
      localObject2 = paramList.get(0);
      localObject2 = (com.inmobi.commons.core.f.b)localObject2;
      localObject2 = j;
      ((Map)localObject1).put(localObject3, localObject2);
      localObject2 = "mk-version";
      localObject3 = com.inmobi.commons.a.b.a();
      ((Map)localObject1).put(localObject2, localObject3);
      localObject2 = com.inmobi.commons.core.utilities.b.a.a();
      localObject2 = b;
      ((Map)localObject1).putAll((Map)localObject2);
      localObject2 = new org/json/JSONObject;
      ((JSONObject)localObject2).<init>((Map)localObject1);
      localObject1 = new org/json/JSONArray;
      ((JSONArray)localObject1).<init>();
      paramList = paramList.iterator();
      for (;;)
      {
        boolean bool = paramList.hasNext();
        if (!bool) {
          break;
        }
        localObject3 = paramList.next();
        localObject3 = (com.inmobi.commons.core.f.b)localObject3;
        localObject4 = new org/json/JSONObject;
        ((JSONObject)localObject4).<init>();
        String str1 = "event-id";
        String str2 = b;
        ((JSONObject)localObject4).put(str1, str2);
        str1 = "ad-markup-type";
        str2 = c;
        ((JSONObject)localObject4).put(str1, str2);
        str1 = "event-name";
        str2 = d;
        ((JSONObject)localObject4).put(str1, str2);
        str1 = "im-plid";
        long l = e;
        ((JSONObject)localObject4).put(str1, l);
        str1 = "request-id";
        str2 = f;
        ((JSONObject)localObject4).put(str1, str2);
        str1 = "event-type";
        str2 = g;
        ((JSONObject)localObject4).put(str1, str2);
        str1 = "d-nettype-raw";
        str2 = h;
        ((JSONObject)localObject4).put(str1, str2);
        str1 = "ts";
        l = i;
        ((JSONObject)localObject4).put(str1, l);
        ((JSONArray)localObject1).put(localObject4);
      }
      paramList = "extra-info";
      ((JSONObject)localObject2).put(paramList, localObject1);
      return ((JSONObject)localObject2).toString();
    }
    catch (JSONException localJSONException) {}
    return null;
  }
  
  private void b(String paramString)
  {
    ExecutorService localExecutorService = a;
    a.4 local4 = new com/inmobi/a/a$4;
    local4.<init>(this, paramString);
    localExecutorService.execute(local4);
  }
  
  public final c a(String paramString)
  {
    Object localObject1 = c.b(paramString);
    int j = com.inmobi.commons.core.utilities.b.b.a();
    int k = 1;
    int n;
    if (j != k)
    {
      localObject1 = f;
      n = b;
      paramString = com.inmobi.commons.core.f.a.a(n, paramString);
    }
    else
    {
      localObject1 = g;
      n = b;
      paramString = com.inmobi.commons.core.f.a.a(n, paramString);
    }
    boolean bool2 = paramString.isEmpty();
    if (!bool2)
    {
      localObject1 = new java/util/ArrayList;
      ((ArrayList)localObject1).<init>();
      localObject2 = paramString.iterator();
      int m;
      Integer localInteger;
      for (;;)
      {
        boolean bool1 = ((Iterator)localObject2).hasNext();
        if (!bool1) {
          break;
        }
        m = nexta;
        localInteger = Integer.valueOf(m);
        ((List)localObject1).add(localInteger);
      }
      paramString = a(paramString);
      if (paramString != null)
      {
        localObject2 = new com/inmobi/commons/core/b/c;
        m = 0;
        localInteger = null;
        ((c)localObject2).<init>((List)localObject1, paramString, false);
        break label174;
      }
    }
    j = 0;
    Object localObject2 = null;
    label174:
    return (c)localObject2;
  }
  
  public final void a(com.inmobi.commons.core.configs.a parama)
  {
    parama = (com.inmobi.ads.b)parama;
    c = parama;
    parama = c.f;
    d = parama;
  }
}

/* Location:
 * Qualified Name:     com.inmobi.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */