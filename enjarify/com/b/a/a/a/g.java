package com.b.a.a.a;

import android.os.AsyncTask;

public final class g
  extends AsyncTask
{
  g.a a;
  
  /* Error */
  private static String a(String... paramVarArgs)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_1
    //   2: aload_0
    //   3: iconst_0
    //   4: aaload
    //   5: astore_0
    //   6: aload_0
    //   7: invokestatic 16	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   10: istore_2
    //   11: iload_2
    //   12: ifeq +11 -> 23
    //   15: ldc 18
    //   17: invokestatic 16	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   20: pop
    //   21: aconst_null
    //   22: areturn
    //   23: new 20	java/net/URL
    //   26: astore_3
    //   27: aload_3
    //   28: aload_0
    //   29: invokespecial 23	java/net/URL:<init>	(Ljava/lang/String;)V
    //   32: aload_3
    //   33: invokevirtual 27	java/net/URL:openConnection	()Ljava/net/URLConnection;
    //   36: astore_3
    //   37: aload_3
    //   38: invokestatic 33	com/google/firebase/perf/network/FirebasePerfUrlConnection:instrument	(Ljava/lang/Object;)Ljava/lang/Object;
    //   41: astore_3
    //   42: aload_3
    //   43: checkcast 35	java/net/URLConnection
    //   46: astore_3
    //   47: aload_3
    //   48: invokevirtual 38	java/net/URLConnection:connect	()V
    //   51: new 40	java/io/BufferedInputStream
    //   54: astore 4
    //   56: aload_3
    //   57: invokevirtual 44	java/net/URLConnection:getInputStream	()Ljava/io/InputStream;
    //   60: astore_3
    //   61: aload 4
    //   63: aload_3
    //   64: invokespecial 47	java/io/BufferedInputStream:<init>	(Ljava/io/InputStream;)V
    //   67: new 49	java/io/StringWriter
    //   70: astore_3
    //   71: aload_3
    //   72: invokespecial 50	java/io/StringWriter:<init>	()V
    //   75: sipush 1024
    //   78: istore 5
    //   80: iload 5
    //   82: newarray <illegal type>
    //   84: astore 6
    //   86: aload 4
    //   88: aload 6
    //   90: invokevirtual 57	java/io/InputStream:read	([B)I
    //   93: istore 7
    //   95: iconst_m1
    //   96: istore 8
    //   98: iload 7
    //   100: iload 8
    //   102: if_icmpeq +27 -> 129
    //   105: new 59	java/lang/String
    //   108: astore 9
    //   110: aload 9
    //   112: aload 6
    //   114: iconst_0
    //   115: iload 7
    //   117: invokespecial 62	java/lang/String:<init>	([BII)V
    //   120: aload_3
    //   121: aload 9
    //   123: invokevirtual 67	java/io/Writer:write	(Ljava/lang/String;)V
    //   126: goto -40 -> 86
    //   129: aload_3
    //   130: invokevirtual 73	java/lang/Object:toString	()Ljava/lang/String;
    //   133: astore_0
    //   134: aload 4
    //   136: invokevirtual 76	java/io/InputStream:close	()V
    //   139: aload_0
    //   140: areturn
    //   141: pop
    //   142: ldc 78
    //   144: invokestatic 16	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   147: pop
    //   148: aconst_null
    //   149: areturn
    //   150: astore_0
    //   151: goto +14 -> 165
    //   154: astore_0
    //   155: aconst_null
    //   156: astore 4
    //   158: goto +130 -> 288
    //   161: astore_0
    //   162: aconst_null
    //   163: astore 4
    //   165: new 80	java/lang/StringBuilder
    //   168: astore_1
    //   169: ldc 82
    //   171: astore_3
    //   172: aload_1
    //   173: aload_3
    //   174: invokespecial 83	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   177: aload_0
    //   178: invokevirtual 88	java/io/IOException:getLocalizedMessage	()Ljava/lang/String;
    //   181: astore_0
    //   182: aload_1
    //   183: aload_0
    //   184: invokevirtual 92	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   187: pop
    //   188: aload_1
    //   189: invokevirtual 93	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   192: astore_0
    //   193: aload_0
    //   194: invokestatic 16	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   197: pop
    //   198: aload 4
    //   200: ifnull +20 -> 220
    //   203: aload 4
    //   205: invokevirtual 76	java/io/InputStream:close	()V
    //   208: goto +12 -> 220
    //   211: pop
    //   212: ldc 78
    //   214: astore_0
    //   215: aload_0
    //   216: invokestatic 16	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   219: pop
    //   220: aconst_null
    //   221: areturn
    //   222: pop
    //   223: aconst_null
    //   224: astore 4
    //   226: new 80	java/lang/StringBuilder
    //   229: astore_1
    //   230: ldc 95
    //   232: astore_3
    //   233: aload_1
    //   234: aload_3
    //   235: invokespecial 83	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   238: aload_1
    //   239: aload_0
    //   240: invokevirtual 92	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   243: pop
    //   244: ldc 97
    //   246: astore_0
    //   247: aload_1
    //   248: aload_0
    //   249: invokevirtual 92	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   252: pop
    //   253: aload_1
    //   254: invokevirtual 93	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   257: astore_0
    //   258: aload_0
    //   259: invokestatic 16	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   262: pop
    //   263: aload 4
    //   265: ifnull +20 -> 285
    //   268: aload 4
    //   270: invokevirtual 76	java/io/InputStream:close	()V
    //   273: goto +12 -> 285
    //   276: pop
    //   277: ldc 78
    //   279: astore_0
    //   280: aload_0
    //   281: invokestatic 16	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   284: pop
    //   285: aconst_null
    //   286: areturn
    //   287: astore_0
    //   288: aload 4
    //   290: ifnull +20 -> 310
    //   293: aload 4
    //   295: invokevirtual 76	java/io/InputStream:close	()V
    //   298: goto +12 -> 310
    //   301: pop
    //   302: ldc 78
    //   304: invokestatic 16	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   307: pop
    //   308: aconst_null
    //   309: areturn
    //   310: aload_0
    //   311: athrow
    //   312: pop
    //   313: goto -87 -> 226
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	316	0	paramVarArgs	String[]
    //   1	253	1	localStringBuilder	StringBuilder
    //   10	2	2	bool	boolean
    //   26	209	3	localObject	Object
    //   54	240	4	localBufferedInputStream	java.io.BufferedInputStream
    //   78	3	5	i	int
    //   84	29	6	arrayOfByte	byte[]
    //   93	23	7	j	int
    //   96	7	8	k	int
    //   108	14	9	str	String
    //   141	1	10	localIOException1	java.io.IOException
    //   211	1	11	localIOException2	java.io.IOException
    //   222	1	12	localMalformedURLException1	java.net.MalformedURLException
    //   276	1	13	localIOException3	java.io.IOException
    //   301	1	14	localIOException4	java.io.IOException
    //   312	1	15	localMalformedURLException2	java.net.MalformedURLException
    // Exception table:
    //   from	to	target	type
    //   134	139	141	java/io/IOException
    //   67	70	150	java/io/IOException
    //   71	75	150	java/io/IOException
    //   80	84	150	java/io/IOException
    //   88	93	150	java/io/IOException
    //   105	108	150	java/io/IOException
    //   115	120	150	java/io/IOException
    //   121	126	150	java/io/IOException
    //   129	133	150	java/io/IOException
    //   23	26	154	finally
    //   28	32	154	finally
    //   32	36	154	finally
    //   37	41	154	finally
    //   42	46	154	finally
    //   47	51	154	finally
    //   51	54	154	finally
    //   56	60	154	finally
    //   63	67	154	finally
    //   23	26	161	java/io/IOException
    //   28	32	161	java/io/IOException
    //   32	36	161	java/io/IOException
    //   37	41	161	java/io/IOException
    //   42	46	161	java/io/IOException
    //   47	51	161	java/io/IOException
    //   51	54	161	java/io/IOException
    //   56	60	161	java/io/IOException
    //   63	67	161	java/io/IOException
    //   203	208	211	java/io/IOException
    //   23	26	222	java/net/MalformedURLException
    //   28	32	222	java/net/MalformedURLException
    //   32	36	222	java/net/MalformedURLException
    //   37	41	222	java/net/MalformedURLException
    //   42	46	222	java/net/MalformedURLException
    //   47	51	222	java/net/MalformedURLException
    //   51	54	222	java/net/MalformedURLException
    //   56	60	222	java/net/MalformedURLException
    //   63	67	222	java/net/MalformedURLException
    //   268	273	276	java/io/IOException
    //   67	70	287	finally
    //   71	75	287	finally
    //   80	84	287	finally
    //   88	93	287	finally
    //   105	108	287	finally
    //   115	120	287	finally
    //   121	126	287	finally
    //   129	133	287	finally
    //   165	168	287	finally
    //   173	177	287	finally
    //   177	181	287	finally
    //   183	188	287	finally
    //   188	192	287	finally
    //   193	198	287	finally
    //   226	229	287	finally
    //   234	238	287	finally
    //   239	244	287	finally
    //   248	253	287	finally
    //   253	257	287	finally
    //   258	263	287	finally
    //   293	298	301	java/io/IOException
    //   67	70	312	java/net/MalformedURLException
    //   71	75	312	java/net/MalformedURLException
    //   80	84	312	java/net/MalformedURLException
    //   88	93	312	java/net/MalformedURLException
    //   105	108	312	java/net/MalformedURLException
    //   115	120	312	java/net/MalformedURLException
    //   121	126	312	java/net/MalformedURLException
    //   129	133	312	java/net/MalformedURLException
  }
  
  protected final void onCancelled()
  {
    g.a locala = a;
    if (locala != null) {
      locala.c();
    }
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */