package com.b.a.a.a;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Handler;
import java.util.concurrent.Executor;

public final class c
  implements g.a
{
  private static c d;
  c.a a;
  Context b;
  c.c c;
  private g e;
  private c.b f;
  private final Runnable g;
  
  static
  {
    c localc = new com/b/a/a/a/c;
    localc.<init>();
    d = localc;
  }
  
  public c()
  {
    Object localObject = new com/b/a/a/a/c$b;
    ((c.b)localObject).<init>(this);
    f = ((c.b)localObject);
    localObject = new com/b/a/a/a/c$1;
    ((c.1)localObject).<init>(this);
    g = ((Runnable)localObject);
  }
  
  public static c a()
  {
    return d;
  }
  
  private void d()
  {
    Object localObject = c;
    if (localObject != null)
    {
      Handler localHandler = a;
      localObject = b.g;
      long l = 2000L;
      localHandler.postDelayed((Runnable)localObject, l);
    }
  }
  
  public final void a(String paramString)
  {
    e = null;
    a.a = paramString;
    paramString = a;
    if (paramString != null) {
      paramString.a();
    }
  }
  
  final void b()
  {
    boolean bool = a.a();
    if (!bool)
    {
      Object localObject1 = e;
      if (localObject1 == null)
      {
        localObject1 = new com/b/a/a/a/g;
        ((g)localObject1).<init>();
        e = ((g)localObject1);
        e.a = this;
        localObject1 = f;
        int i = Build.VERSION.SDK_INT;
        int j = 11;
        if (i >= j)
        {
          localObject1 = a.e;
          localObject2 = AsyncTask.THREAD_POOL_EXECUTOR;
          String[] arrayOfString = { "https://mobile-static.adsafeprotected.com/avid-v2.js" };
          ((g)localObject1).executeOnExecutor((Executor)localObject2, arrayOfString);
          return;
        }
        localObject1 = a.e;
        Object localObject2 = { "https://mobile-static.adsafeprotected.com/avid-v2.js" };
        ((g)localObject1).execute((Object[])localObject2);
      }
    }
  }
  
  public final void c()
  {
    e = null;
    d();
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */