package com.b.a.a.a;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public final class d
  implements c.a, e.a, com.b.a.a.a.e.b
{
  private static d a;
  private static Context b;
  
  static
  {
    d locald = new com/b/a/a/a/d;
    locald.<init>();
    a = locald;
  }
  
  public static com.b.a.a.a.f.a.a a(String paramString)
  {
    return (com.b.a.a.a.f.a.a)aa.get(paramString);
  }
  
  public static void a(Activity paramActivity)
  {
    Object localObject1 = com.b.a.a.a.a.a.a();
    Object localObject2 = a.iterator();
    boolean bool2;
    do
    {
      bool1 = ((Iterator)localObject2).hasNext();
      if (!bool1) {
        break;
      }
      locala = (com.b.a.a.a.j.a)((Iterator)localObject2).next();
      bool2 = locala.b(paramActivity);
    } while (!bool2);
    break label55;
    boolean bool1 = false;
    com.b.a.a.a.j.a locala = null;
    label55:
    if (locala == null)
    {
      localObject1 = a;
      localObject2 = new com/b/a/a/a/j/a;
      ((com.b.a.a.a.j.a)localObject2).<init>(paramActivity);
      ((ArrayList)localObject1).add(localObject2);
    }
  }
  
  public static void a(com.b.a.a.a.f.a parama, com.b.a.a.a.f.a.a parama1)
  {
    com.b.a.a.a.e.a locala = com.b.a.a.a.e.a.a();
    HashMap localHashMap = b;
    String str = a;
    localHashMap.put(str, parama);
    localHashMap = a;
    parama = a;
    localHashMap.put(parama, parama1);
    d = locala;
    parama = b;
    int i = parama.size();
    int j = 1;
    if (i == j)
    {
      parama = c;
      if (parama != null)
      {
        parama = c;
        parama.a(locala);
      }
    }
  }
  
  public static d b()
  {
    return a;
  }
  
  private void c()
  {
    ad = this;
    Object localObject = e.a();
    boolean bool1 = true;
    b = bool1;
    ((e)localObject).d();
    localObject = e.a();
    boolean bool2 = ((e)localObject).b();
    if (bool2)
    {
      localObject = f.a();
      f.c();
      ((f)localObject).b();
    }
  }
  
  public final void a()
  {
    Object localObject = ab;
    boolean bool1 = ((HashMap)localObject).isEmpty() ^ true;
    if (bool1)
    {
      localObject = com.b.a.a.a.e.a.a();
      boolean bool2 = false;
      com.b.a.a.a.f.a.a.a locala = null;
      c = null;
      localObject = aa.values().iterator();
      for (;;)
      {
        bool2 = ((Iterator)localObject).hasNext();
        if (!bool2) {
          break;
        }
        locala = nextb;
        locala.a();
      }
      ac = this;
      localObject = com.b.a.a.a.e.a.a();
      bool1 = ((com.b.a.a.a.e.a)localObject).b();
      if (bool1) {
        c();
      }
    }
  }
  
  public final void a(Context paramContext)
  {
    Context localContext = b;
    if (localContext == null)
    {
      b = paramContext.getApplicationContext();
      paramContext = e.a();
      localContext = b;
      paramContext.a(localContext);
      ac = this;
      paramContext = b;
      com.b.a.a.a.g.b.a(paramContext);
    }
  }
  
  public final void a(com.b.a.a.a.e.a parama)
  {
    parama = b;
    boolean bool = parama.isEmpty();
    if (bool) {
      return;
    }
    bool = a.a();
    if (!bool)
    {
      aa = this;
      parama = c.a();
      Object localObject = b;
      b = ((Context)localObject);
      localObject = new com/b/a/a/a/c$c;
      ((c.c)localObject).<init>(parama);
      c = ((c.c)localObject);
      parama.b();
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      f localf = f.a();
      f.c();
      localf.b();
      return;
    }
    f.a();
    f.d();
  }
  
  public final void b(com.b.a.a.a.e.a parama)
  {
    boolean bool = parama.b();
    if (bool)
    {
      bool = a.a();
      if (bool)
      {
        c();
        return;
      }
    }
    aa.clear();
    parama = f.a();
    f.d();
    a.clear();
    b.a();
    parama = e.a();
    parama.c();
    a = null;
    b = false;
    c = false;
    d = null;
    parama = c.a();
    Object localObject = c;
    if (localObject != null)
    {
      localObject = c;
      Handler localHandler = a;
      localObject = c.a(b);
      localHandler.removeCallbacks((Runnable)localObject);
      c = null;
    }
    a = null;
    b = null;
    b = null;
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */