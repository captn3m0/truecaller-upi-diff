package com.b.a.a.a;

import android.view.View;
import com.b.a.a.a.d.d.a;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import org.json.JSONObject;

public final class f
  implements d.a
{
  private static f c;
  private static f.a d;
  private static final Runnable j;
  List a;
  com.b.a.a.a.i.b b;
  private int e;
  private com.b.a.a.a.d.a f;
  private com.b.a.a.a.i.a g;
  private double h;
  private double i;
  
  static
  {
    Object localObject = new com/b/a/a/a/f;
    ((f)localObject).<init>();
    c = (f)localObject;
    localObject = new com/b/a/a/a/f$1;
    ((f.1)localObject).<init>();
    j = (Runnable)localObject;
  }
  
  public f()
  {
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    a = ((List)localObject);
    localObject = new com/b/a/a/a/i/a;
    com.b.a.a.a.e.a locala = com.b.a.a.a.e.a.a();
    ((com.b.a.a.a.i.a)localObject).<init>(locala);
    g = ((com.b.a.a.a.i.a)localObject);
    localObject = new com/b/a/a/a/d/a;
    ((com.b.a.a.a.d.a)localObject).<init>();
    f = ((com.b.a.a.a.d.a)localObject);
    localObject = new com/b/a/a/a/i/b;
    locala = com.b.a.a.a.e.a.a();
    com.b.a.a.a.i.a.c localc = new com/b/a/a/a/i/a/c;
    localc.<init>();
    ((com.b.a.a.a.i.b)localObject).<init>(locala, localc);
    b = ((com.b.a.a.a.i.b)localObject);
  }
  
  public static f a()
  {
    return c;
  }
  
  private void a(View paramView, com.b.a.a.a.d.d paramd, JSONObject paramJSONObject, com.b.a.a.a.i.c paramc)
  {
    com.b.a.a.a.i.c localc = com.b.a.a.a.i.c.a;
    boolean bool;
    if (paramc == localc)
    {
      bool = true;
    }
    else
    {
      bool = false;
      paramc = null;
    }
    paramd.a(paramView, paramJSONObject, this, bool);
  }
  
  static void c()
  {
    f.a locala = d;
    if (locala == null)
    {
      locala = new com/b/a/a/a/f$a;
      locala.<init>((byte)0);
      d = locala;
      Runnable localRunnable = j;
      long l = 200L;
      locala.postDelayed(localRunnable, l);
    }
  }
  
  static void d()
  {
    f.a locala = d;
    if (locala != null)
    {
      Runnable localRunnable = j;
      locala.removeCallbacks(localRunnable);
      locala = null;
      d = null;
    }
  }
  
  private void g()
  {
    e = 0;
    double d1 = com.b.a.a.a.g.c.a();
    h = d1;
  }
  
  private void h()
  {
    double d1 = com.b.a.a.a.g.c.a();
    i = d1;
    j();
  }
  
  private void i()
  {
    Object localObject1 = g;
    ((com.b.a.a.a.i.a)localObject1).a();
    double d1 = com.b.a.a.a.g.c.a();
    Object localObject2 = f.a;
    Object localObject3 = g.e;
    int k = ((HashSet)localObject3).size();
    HashSet localHashSet1 = null;
    Object localObject4;
    if (k > 0)
    {
      localObject3 = ((com.b.a.a.a.d.d)localObject2).a(null);
      localObject4 = b;
      HashSet localHashSet2 = g.e;
      ((com.b.a.a.a.i.b)localObject4).b((JSONObject)localObject3, localHashSet2, d1);
    }
    localObject3 = g.d;
    k = ((HashSet)localObject3).size();
    if (k > 0)
    {
      localObject3 = ((com.b.a.a.a.d.d)localObject2).a(null);
      localObject4 = com.b.a.a.a.i.c.a;
      a(null, (com.b.a.a.a.d.d)localObject2, (JSONObject)localObject3, (com.b.a.a.a.i.c)localObject4);
      com.b.a.a.a.g.b.a((JSONObject)localObject3);
      localObject2 = b;
      localHashSet1 = g.d;
      ((com.b.a.a.a.i.b)localObject2).a((JSONObject)localObject3, localHashSet1, d1);
    }
    else
    {
      localObject1 = b;
      ((com.b.a.a.a.i.b)localObject1).a();
    }
    g.b();
  }
  
  private void j()
  {
    Object localObject = a;
    int k = ((List)localObject).size();
    if (k > 0)
    {
      localObject = a.iterator();
      for (;;)
      {
        boolean bool = ((Iterator)localObject).hasNext();
        if (!bool) {
          break;
        }
        ((Iterator)localObject).next();
      }
    }
  }
  
  public final void a(View paramView, com.b.a.a.a.d.d paramd, JSONObject paramJSONObject)
  {
    boolean bool1 = com.b.a.a.a.g.d.a(paramView);
    if (!bool1) {
      return;
    }
    Object localObject1 = g;
    Object localObject2 = c;
    boolean bool2 = ((HashSet)localObject2).contains(paramView);
    if (bool2)
    {
      localObject1 = com.b.a.a.a.i.c.a;
    }
    else
    {
      bool1 = f;
      if (bool1) {
        localObject1 = com.b.a.a.a.i.c.b;
      } else {
        localObject1 = com.b.a.a.a.i.c.c;
      }
    }
    localObject2 = com.b.a.a.a.i.c.c;
    if (localObject1 == localObject2) {
      return;
    }
    localObject2 = paramd.a(paramView);
    com.b.a.a.a.g.b.a(paramJSONObject, (JSONObject)localObject2);
    paramJSONObject = g;
    Object localObject3 = a;
    int k = ((HashMap)localObject3).size();
    ArrayList localArrayList = null;
    if (k == 0)
    {
      k = 0;
      localObject3 = null;
    }
    else
    {
      localObject3 = (String)a.get(paramView);
      if (localObject3 != null)
      {
        paramJSONObject = a;
        paramJSONObject.remove(paramView);
      }
    }
    int m = 1;
    if (localObject3 != null)
    {
      com.b.a.a.a.g.b.a((JSONObject)localObject2, (String)localObject3);
      localObject3 = g;
      f = m;
      k = 1;
    }
    else
    {
      k = 0;
      localObject3 = null;
    }
    if (k == 0)
    {
      localObject3 = g;
      HashMap localHashMap = b;
      int n = localHashMap.size();
      if (n != 0)
      {
        localArrayList = (ArrayList)b.get(paramView);
        if (localArrayList != null)
        {
          localObject3 = b;
          ((HashMap)localObject3).remove(paramView);
          Collections.sort(localArrayList);
        }
      }
      if (localArrayList != null) {
        com.b.a.a.a.g.b.a((JSONObject)localObject2, localArrayList);
      }
      a(paramView, paramd, (JSONObject)localObject2, (com.b.a.a.a.i.c)localObject1);
    }
    int i1 = e + m;
    e = i1;
  }
  
  final void b()
  {
    g();
    i();
    h();
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */