package com.b.a.a.a;

import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

final class e$1
  extends BroadcastReceiver
{
  e$1(e parame) {}
  
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    if (paramIntent == null) {
      return;
    }
    String str1 = "android.intent.action.SCREEN_OFF";
    String str2 = paramIntent.getAction();
    boolean bool1 = str1.equals(str2);
    if (bool1)
    {
      e.a(a, true);
      return;
    }
    str1 = "android.intent.action.USER_PRESENT";
    str2 = paramIntent.getAction();
    bool1 = str1.equals(str2);
    str2 = null;
    if (bool1)
    {
      e.a(a, false);
      return;
    }
    str1 = "android.intent.action.SCREEN_ON";
    paramIntent = paramIntent.getAction();
    boolean bool2 = str1.equals(paramIntent);
    if (bool2)
    {
      paramIntent = "keyguard";
      paramContext = (KeyguardManager)paramContext.getSystemService(paramIntent);
      if (paramContext != null)
      {
        boolean bool3 = paramContext.inKeyguardRestrictedInputMode();
        if (!bool3)
        {
          paramContext = a;
          e.a(paramContext, false);
        }
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.e.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */