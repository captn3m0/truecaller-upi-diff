package com.b.a.a.a.f.a;

import android.content.Context;
import android.view.View;
import android.webkit.WebView;
import com.b.a.a.a.f.f;
import java.lang.ref.WeakReference;
import org.json.JSONObject;

public abstract class a
  implements com.b.a.a.a.f.a.a.a.a
{
  public final b a;
  public com.b.a.a.a.f.a.a.a b;
  public com.b.a.a.a.c.b c;
  public c d;
  public boolean e;
  public boolean f;
  public final j g;
  public a.a h;
  public double i;
  private com.b.a.a.a.f.a.a.d j;
  private com.b.a.a.a.j.b k;
  
  public a(Context paramContext, String paramString, f paramf)
  {
    b localb1 = new com/b/a/a/a/f/a/b;
    String str1 = a().toString();
    String str2 = b().toString();
    b localb2 = localb1;
    localb1.<init>(paramContext, paramString, str1, str2, paramf);
    a = localb1;
    paramContext = new com/b/a/a/a/f/a/a/a;
    paramString = a;
    paramContext.<init>(paramString);
    b = paramContext;
    paramContext = b;
    c = this;
    paramString = new com/b/a/a/a/f/a/a/d;
    localb2 = a;
    paramString.<init>(localb2, paramContext);
    j = paramString;
    paramContext = new com/b/a/a/a/j/b;
    paramString = null;
    paramContext.<init>(null);
    k = paramContext;
    boolean bool = b ^ true;
    e = bool;
    bool = e;
    if (!bool)
    {
      paramContext = new com/b/a/a/a/c/b;
      paramString = b;
      paramContext.<init>(this, paramString);
      c = paramContext;
    }
    paramContext = new com/b/a/a/a/f/a/j;
    paramContext.<init>();
    g = paramContext;
    m();
  }
  
  private void b(boolean paramBoolean)
  {
    f = paramBoolean;
    c localc = d;
    if (localc != null)
    {
      if (paramBoolean)
      {
        localc.c();
        return;
      }
      localc.d();
    }
  }
  
  private boolean c(View paramView)
  {
    return k.b(paramView);
  }
  
  private void l()
  {
    boolean bool = f;
    if (bool)
    {
      com.b.a.a.a.f.a.a.a locala = b;
      String str = com.b.a.a.a.g.b.a().toString();
      locala.b(str);
    }
  }
  
  private void m()
  {
    double d1 = com.b.a.a.a.g.c.a();
    i = d1;
    a.a locala = a.a.a;
    h = locala;
  }
  
  public abstract k a();
  
  public final void a(View paramView)
  {
    boolean bool = c(paramView);
    if (!bool)
    {
      m();
      com.b.a.a.a.j.b localb = k;
      localb.a(paramView);
      g();
      j();
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    boolean bool = f;
    if (bool)
    {
      String str;
      if (paramBoolean) {
        str = "active";
      } else {
        str = "inactive";
      }
      com.b.a.a.a.f.a.a.a locala = b;
      locala.c(str);
    }
  }
  
  public abstract i b();
  
  public final void b(View paramView)
  {
    boolean bool = c(paramView);
    if (bool)
    {
      m();
      l();
      paramView = k;
      paramView.a(null);
      h();
      j();
    }
  }
  
  public final View c()
  {
    return (View)k.a.get();
  }
  
  public void d() {}
  
  public void e()
  {
    l();
    Object localObject = c;
    if (localObject != null) {
      ((com.b.a.a.a.c.b)localObject).a();
    }
    b.a(null);
    j.a(null);
    e = false;
    j();
    localObject = d;
    if (localObject != null) {
      ((c)localObject).a(this);
    }
  }
  
  public final void f()
  {
    j();
  }
  
  protected void g() {}
  
  protected void h() {}
  
  protected final void i()
  {
    com.b.a.a.a.f.a.a.d locald = j;
    WebView localWebView = k();
    locald.a(localWebView);
  }
  
  public final void j()
  {
    Object localObject = b;
    boolean bool1 = a;
    if (bool1)
    {
      bool1 = e;
      if (bool1)
      {
        localObject = k;
        bool1 = ((com.b.a.a.a.j.b)localObject).a();
        if (!bool1)
        {
          bool1 = true;
          break label46;
        }
      }
    }
    bool1 = false;
    localObject = null;
    label46:
    boolean bool2 = f;
    if (bool2 != bool1) {
      b(bool1);
    }
  }
  
  public abstract WebView k();
}

/* Location:
 * Qualified Name:     com.b.a.a.a.f.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */