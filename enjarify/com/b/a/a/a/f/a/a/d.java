package com.b.a.a.a.f.a.a;

import android.webkit.WebView;
import com.b.a.a.a.f.a.b;
import java.lang.ref.WeakReference;

public final class d
  implements c.a
{
  private final b a;
  private final com.b.a.a.a.j.c b;
  private final a c;
  private c d;
  
  public d(b paramb, a parama)
  {
    com.b.a.a.a.j.c localc = new com/b/a/a/a/j/c;
    localc.<init>(null);
    b = localc;
    a = paramb;
    c = parama;
  }
  
  private void b()
  {
    c localc = d;
    if (localc != null)
    {
      a = null;
      d = null;
    }
  }
  
  public final void a()
  {
    a locala = c;
    WebView localWebView = (WebView)b.a.get();
    locala.a(localWebView);
  }
  
  public final void a(WebView paramWebView)
  {
    Object localObject1 = b.a.get();
    if (localObject1 == paramWebView) {
      return;
    }
    localObject1 = c;
    Object localObject2 = null;
    ((a)localObject1).a(null);
    b();
    localObject1 = b;
    ((com.b.a.a.a.j.c)localObject1).a(paramWebView);
    if (paramWebView != null)
    {
      localObject1 = new com/b/a/a/a/f/a/a/c;
      localObject2 = a;
      ((c)localObject1).<init>((b)localObject2);
      d = ((c)localObject1);
      localObject1 = d;
      a = this;
      localObject2 = "avid";
      paramWebView.addJavascriptInterface(localObject1, (String)localObject2);
    }
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.f.a.a.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */