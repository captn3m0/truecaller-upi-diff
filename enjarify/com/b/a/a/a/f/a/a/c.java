package com.b.a.a.a.f.a.a;

import android.os.Handler;
import com.b.a.a.a.f.a.b;
import org.json.JSONObject;

public final class c
{
  c.a a;
  private final b b;
  private final Handler c;
  
  public c(b paramb)
  {
    Handler localHandler = new android/os/Handler;
    localHandler.<init>();
    c = localHandler;
    b = paramb;
  }
  
  public final String getAvidAdSessionContext()
  {
    Handler localHandler = c;
    c.b localb = new com/b/a/a/a/f/a/a/c$b;
    localb.<init>(this);
    localHandler.post(localb);
    return b.b().toString();
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.f.a.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */