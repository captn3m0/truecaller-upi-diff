package com.b.a.a.a.f.a.b;

import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import java.util.ArrayList;
import java.util.Iterator;

public final class b
  implements a, c.a
{
  public final com.b.a.a.a.j.c a;
  public int b = 0;
  private final c c;
  private final ArrayList d;
  
  public b(WebView paramWebView)
  {
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    d = ((ArrayList)localObject);
    localObject = new com/b/a/a/a/j/c;
    ((com.b.a.a.a.j.c)localObject).<init>(paramWebView);
    a = ((com.b.a.a.a.j.c)localObject);
    paramWebView.getSettings().setJavaScriptEnabled(true);
    localObject = new com/b/a/a/a/f/a/b/c;
    ((c)localObject).<init>();
    c = ((c)localObject);
    localObject = c;
    a = this;
    paramWebView.setWebViewClient((WebViewClient)localObject);
  }
  
  private void b(String paramString)
  {
    paramString = "(function () {\nvar script=document.createElement('script');script.setAttribute(\"type\",\"text/javascript\");script.setAttribute(\"src\",\"%SCRIPT_SRC%\");document.body.appendChild(script);\n})();".replace("%SCRIPT_SRC%", paramString);
    a.a(paramString);
  }
  
  public final void a()
  {
    int i = 2;
    b = i;
    Iterator localIterator = d.iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      String str = (String)localIterator.next();
      b(str);
    }
    d.clear();
  }
  
  public final void a(String paramString)
  {
    int i = b;
    int j = 2;
    if (i == j)
    {
      b(paramString);
      return;
    }
    d.add(paramString);
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.f.a.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */