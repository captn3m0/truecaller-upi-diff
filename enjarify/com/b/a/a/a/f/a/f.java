package com.b.a.a.a.f.a;

import android.content.Context;
import android.webkit.WebView;
import com.b.a.a.a.f.a.b.b;
import com.b.a.a.a.j.d;
import java.lang.ref.WeakReference;

public abstract class f
  extends a
{
  public b j;
  private final WebView k;
  
  public f(Context paramContext, String paramString, com.b.a.a.a.f.f paramf)
  {
    super(paramContext, paramString, paramf);
    paramString = new android/webkit/WebView;
    paramContext = paramContext.getApplicationContext();
    paramString.<init>(paramContext);
    k = paramString;
    paramContext = new com/b/a/a/a/f/a/b/b;
    paramString = k;
    paramContext.<init>(paramString);
    j = paramContext;
  }
  
  public final void d()
  {
    super.d();
    i();
    Object localObject = j;
    WebView localWebView = (WebView)a.a.get();
    if (localWebView != null)
    {
      int i = b;
      if (i == 0)
      {
        i = 1;
        b = i;
        localObject = "<html><body></body></html>";
        String str = "text/html";
        localWebView.loadData((String)localObject, str, null);
      }
    }
  }
  
  public final WebView k()
  {
    return k;
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.f.a.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */