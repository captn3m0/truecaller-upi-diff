package com.b.a.a.a.f.a;

import android.content.Context;
import com.b.a.a.a.f.f;
import org.json.JSONException;
import org.json.JSONObject;

public final class b
{
  public String a;
  private f b;
  private String c;
  private String d;
  
  public b(Context paramContext, String paramString1, String paramString2, String paramString3, f paramf)
  {
    com.b.a.a.a.b localb = com.b.a.a.a.b.a();
    String str = a;
    if (str == null)
    {
      paramContext = paramContext.getApplicationContext().getPackageName();
      a = paramContext;
    }
    a = paramString1;
    b = paramf;
    c = paramString2;
    d = paramString3;
  }
  
  public final JSONObject a()
  {
    JSONObject localJSONObject = new org/json/JSONObject;
    localJSONObject.<init>();
    String str = "avidAdSessionId";
    try
    {
      Object localObject = a;
      localJSONObject.put(str, localObject);
      str = "bundleIdentifier";
      localObject = com.b.a.a.a.b.a();
      localObject = a;
      localJSONObject.put(str, localObject);
      str = "partner";
      com.b.a.a.a.b.a();
      localObject = com.b.a.a.a.b.c();
      localJSONObject.put(str, localObject);
      str = "partnerVersion";
      localObject = b;
      localObject = a;
      localJSONObject.put(str, localObject);
      str = "avidLibraryVersion";
      com.b.a.a.a.b.a();
      localObject = com.b.a.a.a.b.b();
      localJSONObject.put(str, localObject);
      str = "avidAdSessionType";
      localObject = c;
      localJSONObject.put(str, localObject);
      str = "mediaType";
      localObject = d;
      localJSONObject.put(str, localObject);
      str = "isDeferred";
      localObject = b;
      boolean bool = b;
      localJSONObject.put(str, bool);
    }
    catch (JSONException localJSONException)
    {
      localJSONException.printStackTrace();
    }
    return localJSONObject;
  }
  
  public final JSONObject b()
  {
    JSONObject localJSONObject = a();
    String str1 = "avidApiLevel";
    String str2 = "2";
    try
    {
      localJSONObject.put(str1, str2);
      str1 = "mode";
      str2 = "stub";
      localJSONObject.put(str1, str2);
    }
    catch (JSONException localJSONException)
    {
      localJSONException.printStackTrace();
    }
    return localJSONObject;
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.f.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */