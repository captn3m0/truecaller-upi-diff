package com.b.a.a.a.f.a.a;

import android.text.TextUtils;
import android.webkit.WebView;
import com.b.a.a.a.j.c;
import com.b.a.a.a.j.d;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONObject;

public final class a
{
  public boolean a;
  public boolean b;
  public a.a c;
  private final com.b.a.a.a.f.a.b d;
  private c e;
  private final ArrayList f;
  
  public a(com.b.a.a.a.f.a.b paramb)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    f = localArrayList;
    d = paramb;
    paramb = new com/b/a/a/a/j/c;
    paramb.<init>(null);
    e = paramb;
  }
  
  private void b(String paramString, JSONObject paramJSONObject)
  {
    if (paramJSONObject != null) {
      paramJSONObject = paramJSONObject.toString();
    } else {
      paramJSONObject = null;
    }
    boolean bool = TextUtils.isEmpty(paramJSONObject);
    if (!bool)
    {
      paramString = com.b.a.a.a.g.a.a(paramString, paramJSONObject);
      a(paramString);
      return;
    }
    paramString = com.b.a.a.a.g.a.c(paramString);
    a(paramString);
  }
  
  private void c()
  {
    String str = com.b.a.a.a.g.a.d(d.a().toString());
    a(str);
  }
  
  private void d()
  {
    a.a locala = c;
    if (locala != null) {
      locala.f();
    }
  }
  
  private void e()
  {
    Iterator localIterator = f.iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      Object localObject = (b)localIterator.next();
      String str = a;
      localObject = b;
      b(str, (JSONObject)localObject);
    }
    f.clear();
  }
  
  public final void a()
  {
    c localc = e;
    boolean bool = localc.a();
    if (bool) {
      return;
    }
    a = true;
    localc = e;
    String str = com.b.a.a.a.a.a;
    localc.a(str);
    c();
    b();
    e();
    d();
  }
  
  public final void a(WebView paramWebView)
  {
    Object localObject = e.a.get();
    if (localObject == paramWebView) {
      return;
    }
    localObject = e;
    ((c)localObject).a(paramWebView);
    paramWebView = null;
    a = false;
    boolean bool = com.b.a.a.a.a.a();
    if (bool) {
      a();
    }
  }
  
  public final void a(String paramString)
  {
    e.b(paramString);
  }
  
  public final void a(String paramString, JSONObject paramJSONObject)
  {
    boolean bool = a;
    if (bool)
    {
      b(paramString, paramJSONObject);
      return;
    }
    ArrayList localArrayList = f;
    b localb = new com/b/a/a/a/f/a/a/b;
    localb.<init>(paramString, paramJSONObject);
    localArrayList.add(localb);
  }
  
  public final void b()
  {
    boolean bool = a;
    if (bool)
    {
      bool = b;
      if (bool)
      {
        String str = com.b.a.a.a.g.a.e("publishReadyEventForDeferredAdSession()");
        a(str);
      }
    }
  }
  
  public final void b(String paramString)
  {
    paramString = com.b.a.a.a.g.a.a(paramString);
    a(paramString);
  }
  
  public final void c(String paramString)
  {
    paramString = com.b.a.a.a.g.a.b(paramString);
    a(paramString);
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.f.a.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */