package com.b.a.a.a.f.a;

public enum k
{
  private final String e;
  
  static
  {
    Object localObject = new com/b/a/a/a/f/a/k;
    ((k)localObject).<init>("DISPLAY", 0, "display");
    a = (k)localObject;
    localObject = new com/b/a/a/a/f/a/k;
    int i = 1;
    ((k)localObject).<init>("VIDEO", i, "video");
    b = (k)localObject;
    localObject = new com/b/a/a/a/f/a/k;
    int j = 2;
    ((k)localObject).<init>("MANAGED_DISPLAY", j, "managedDisplay");
    c = (k)localObject;
    localObject = new com/b/a/a/a/f/a/k;
    int k = 3;
    ((k)localObject).<init>("MANAGED_VIDEO", k, "managedVideo");
    d = (k)localObject;
    localObject = new k[4];
    k localk = a;
    localObject[0] = localk;
    localk = b;
    localObject[i] = localk;
    localk = c;
    localObject[j] = localk;
    localk = d;
    localObject[k] = localk;
    f = (k[])localObject;
  }
  
  private k(String paramString1)
  {
    e = paramString1;
  }
  
  public final String toString()
  {
    return e;
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.f.a.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */