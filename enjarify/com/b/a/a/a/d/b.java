package com.b.a.a.a.d;

import android.app.Activity;
import android.os.Build.VERSION;
import android.view.View;
import android.view.Window;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONObject;

public final class b
  implements d
{
  private final d a;
  
  public b(d paramd)
  {
    a = paramd;
  }
  
  public final JSONObject a(View paramView)
  {
    return com.b.a.a.a.g.b.a(0, 0, 0, 0);
  }
  
  public final void a(View paramView, JSONObject paramJSONObject, d.a parama, boolean paramBoolean)
  {
    paramView = com.b.a.a.a.a.a.a();
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    paramView = a.iterator();
    d locald = null;
    Object localObject2 = null;
    for (;;)
    {
      boolean bool1 = paramView.hasNext();
      if (!bool1) {
        break;
      }
      Object localObject3 = (com.b.a.a.a.j.a)paramView.next();
      Object localObject4 = (Activity)a.get();
      boolean bool2;
      if (localObject4 == null)
      {
        bool2 = true;
      }
      else
      {
        int i = Build.VERSION.SDK_INT;
        int j = 17;
        if (i >= j) {
          bool2 = ((Activity)localObject4).isDestroyed();
        } else {
          bool2 = ((Activity)localObject4).isFinishing();
        }
      }
      if (bool2)
      {
        paramView.remove();
      }
      else
      {
        localObject3 = (Activity)a.get();
        if (localObject3 != null)
        {
          localObject4 = ((Activity)localObject3).getWindow();
          if (localObject4 != null)
          {
            bool1 = ((Activity)localObject3).hasWindowFocus();
            if (bool1)
            {
              localObject3 = ((Window)localObject4).getDecorView();
              if (localObject3 == null) {
                break label204;
              }
              bool2 = ((View)localObject3).isShown();
              if (!bool2) {
                break label204;
              }
              break label210;
            }
          }
          bool1 = false;
          localObject3 = null;
          break label210;
        }
        label204:
        bool1 = false;
        localObject3 = null;
        label210:
        if (localObject3 != null) {
          localObject2 = localObject3;
        }
      }
    }
    if (localObject2 != null) {
      ((ArrayList)localObject1).add(localObject2);
    }
    paramView = ((List)localObject1).iterator();
    for (;;)
    {
      paramBoolean = paramView.hasNext();
      if (!paramBoolean) {
        break;
      }
      localObject1 = (View)paramView.next();
      locald = a;
      parama.a((View)localObject1, locald, paramJSONObject);
    }
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.d.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */