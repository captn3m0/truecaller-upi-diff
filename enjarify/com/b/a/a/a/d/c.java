package com.b.a.a.a.d;

import android.os.Build.VERSION;
import android.view.View;
import android.view.ViewGroup;
import com.b.a.a.a.g.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import org.json.JSONObject;

public final class c
  implements d
{
  private final int[] a;
  
  public c()
  {
    int[] arrayOfInt = new int[2];
    a = arrayOfInt;
  }
  
  public final JSONObject a(View paramView)
  {
    int i = paramView.getWidth();
    int j = paramView.getHeight();
    int[] arrayOfInt = a;
    paramView.getLocationOnScreen(arrayOfInt);
    paramView = a;
    int k = paramView[0];
    int m = paramView[1];
    return b.a(k, m, i, j);
  }
  
  public final void a(View paramView, JSONObject paramJSONObject, d.a parama, boolean paramBoolean)
  {
    int i = paramView instanceof ViewGroup;
    if (i == 0) {
      return;
    }
    paramView = (ViewGroup)paramView;
    i = 0;
    Object localObject1 = null;
    Object localObject2;
    boolean bool1;
    if (paramBoolean)
    {
      paramBoolean = Build.VERSION.SDK_INT;
      boolean bool2 = true;
      if (paramBoolean >= bool2)
      {
        localObject2 = new java/util/HashMap;
        ((HashMap)localObject2).<init>();
        View localView;
        for (;;)
        {
          int m = paramView.getChildCount();
          if (i >= m) {
            break;
          }
          localView = paramView.getChildAt(i);
          float f1 = localView.getZ();
          Object localObject3 = Float.valueOf(f1);
          localObject3 = (ArrayList)((HashMap)localObject2).get(localObject3);
          if (localObject3 == null)
          {
            localObject3 = new java/util/ArrayList;
            ((ArrayList)localObject3).<init>();
            float f2 = localView.getZ();
            Float localFloat = Float.valueOf(f2);
            ((HashMap)localObject2).put(localFloat, localObject3);
          }
          ((ArrayList)localObject3).add(localView);
          int j;
          i += 1;
        }
        paramView = new java/util/ArrayList;
        localObject1 = ((HashMap)localObject2).keySet();
        paramView.<init>((Collection)localObject1);
        Collections.sort(paramView);
        paramView = paramView.iterator();
        bool1 = paramView.hasNext();
        if (bool1)
        {
          localObject1 = (Float)paramView.next();
          localObject1 = ((ArrayList)((HashMap)localObject2).get(localObject1)).iterator();
          for (;;)
          {
            boolean bool3 = ((Iterator)localObject1).hasNext();
            if (!bool3) {
              break;
            }
            localView = (View)((Iterator)localObject1).next();
            parama.a(localView, this, paramJSONObject);
          }
        }
        return;
      }
    }
    for (;;)
    {
      paramBoolean = paramView.getChildCount();
      if (bool1 >= paramBoolean) {
        break;
      }
      localObject2 = paramView.getChildAt(bool1);
      parama.a((View)localObject2, this, paramJSONObject);
      int k;
      bool1 += true;
    }
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.d.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */