package com.b.a.a.a.b;

public abstract class a
{
  protected com.b.a.a.a.f.a.a a;
  protected com.b.a.a.a.f.a.a.a b;
  
  public a(com.b.a.a.a.f.a.a parama, com.b.a.a.a.f.a.a.a parama1)
  {
    a = parama;
    b = parama1;
  }
  
  public final void a()
  {
    a = null;
    b = null;
  }
  
  public final void b()
  {
    Object localObject = a;
    if (localObject != null) {
      return;
    }
    localObject = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject).<init>("The AVID ad session is ended. Please ensure you are not recording events after the session has ended.");
    throw ((Throwable)localObject);
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */