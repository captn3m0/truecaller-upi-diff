package com.b.a.a.a.i;

import android.view.View;
import com.b.a.a.a.f.a.j;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

public final class a
{
  public final HashMap a;
  public final HashMap b;
  public final HashSet c;
  public final HashSet d;
  public final HashSet e;
  public boolean f;
  private final com.b.a.a.a.e.a g;
  
  public a(com.b.a.a.a.e.a parama)
  {
    Object localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    a = ((HashMap)localObject);
    localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    b = ((HashMap)localObject);
    localObject = new java/util/HashSet;
    ((HashSet)localObject).<init>();
    c = ((HashSet)localObject);
    localObject = new java/util/HashSet;
    ((HashSet)localObject).<init>();
    d = ((HashSet)localObject);
    localObject = new java/util/HashSet;
    ((HashSet)localObject).<init>();
    e = ((HashSet)localObject);
    g = parama;
  }
  
  private void a(View paramView, com.b.a.a.a.f.a.a parama)
  {
    ArrayList localArrayList = (ArrayList)b.get(paramView);
    if (localArrayList == null)
    {
      localArrayList = new java/util/ArrayList;
      localArrayList.<init>();
      HashMap localHashMap = b;
      localHashMap.put(paramView, localArrayList);
    }
    paramView = a.a;
    localArrayList.add(paramView);
  }
  
  private void a(com.b.a.a.a.f.a.a parama)
  {
    Iterator localIterator = g.a.iterator();
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      Object localObject = (com.b.a.a.a.j.b)localIterator.next();
      boolean bool2 = ((com.b.a.a.a.j.b)localObject).a();
      if (!bool2)
      {
        localObject = (View)a.get();
        a((View)localObject, parama);
      }
    }
  }
  
  private boolean a(View paramView)
  {
    boolean bool1 = paramView.hasWindowFocus();
    if (!bool1) {
      return false;
    }
    HashSet localHashSet = new java/util/HashSet;
    localHashSet.<init>();
    while (paramView != null)
    {
      boolean bool2 = com.b.a.a.a.g.d.a(paramView);
      if (bool2)
      {
        localHashSet.add(paramView);
        paramView = paramView.getParent();
        bool2 = paramView instanceof View;
        if (bool2) {
          paramView = (View)paramView;
        } else {
          paramView = null;
        }
      }
      else
      {
        return false;
      }
    }
    c.addAll(localHashSet);
    return true;
  }
  
  public final void a()
  {
    Iterator localIterator = g.a.values().iterator();
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      Object localObject1 = (com.b.a.a.a.f.a.a)localIterator.next();
      Object localObject2 = ((com.b.a.a.a.f.a.a)localObject1).c();
      boolean bool2 = f;
      if ((bool2) && (localObject2 != null))
      {
        bool2 = a((View)localObject2);
        if (bool2)
        {
          Object localObject3 = d;
          String str = a.a;
          ((HashSet)localObject3).add(str);
          localObject3 = a;
          str = a.a;
          ((HashMap)localObject3).put(localObject2, str);
          a((com.b.a.a.a.f.a.a)localObject1);
        }
        else
        {
          localObject2 = e;
          localObject1 = a.a;
          ((HashSet)localObject2).add(localObject1);
        }
      }
    }
  }
  
  public final void b()
  {
    a.clear();
    b.clear();
    c.clear();
    d.clear();
    e.clear();
    f = false;
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.i.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */