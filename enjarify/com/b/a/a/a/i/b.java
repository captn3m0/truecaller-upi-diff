package com.b.a.a.a.i;

import com.b.a.a.a.e.a;
import com.b.a.a.a.i.a.b.b;
import com.b.a.a.a.i.a.c;
import com.b.a.a.a.i.a.d;
import com.b.a.a.a.i.a.e;
import com.b.a.a.a.i.a.f;
import java.util.HashSet;
import org.json.JSONObject;

public final class b
  implements b.b
{
  private final a a;
  private JSONObject b;
  private final c c;
  
  public b(a parama, c paramc)
  {
    a = parama;
    c = paramc;
  }
  
  public final void a()
  {
    c localc = c;
    d locald = new com/b/a/a/a/i/a/d;
    locald.<init>(this);
    localc.a(locald);
  }
  
  public final void a(JSONObject paramJSONObject)
  {
    b = paramJSONObject;
  }
  
  public final void a(JSONObject paramJSONObject, HashSet paramHashSet, double paramDouble)
  {
    c localc = c;
    f localf = new com/b/a/a/a/i/a/f;
    a locala = a;
    localf.<init>(this, locala, paramHashSet, paramJSONObject, paramDouble);
    localc.a(localf);
  }
  
  public final JSONObject b()
  {
    return b;
  }
  
  public final void b(JSONObject paramJSONObject, HashSet paramHashSet, double paramDouble)
  {
    c localc = c;
    e locale = new com/b/a/a/a/i/a/e;
    a locala = a;
    locale.<init>(this, locala, paramHashSet, paramJSONObject, paramDouble);
    localc.a(locale);
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.i.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */