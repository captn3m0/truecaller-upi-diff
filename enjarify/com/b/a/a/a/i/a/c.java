package com.b.a.a.a.i.a;

import java.util.ArrayDeque;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public final class c
  implements b.a
{
  private final BlockingQueue a;
  private final ThreadPoolExecutor b;
  private final ArrayDeque c;
  private b d;
  
  public c()
  {
    Object localObject = new java/util/ArrayDeque;
    ((ArrayDeque)localObject).<init>();
    c = ((ArrayDeque)localObject);
    d = null;
    localObject = new java/util/concurrent/LinkedBlockingQueue;
    ((LinkedBlockingQueue)localObject).<init>();
    a = ((BlockingQueue)localObject);
    localObject = new java/util/concurrent/ThreadPoolExecutor;
    TimeUnit localTimeUnit = TimeUnit.SECONDS;
    BlockingQueue localBlockingQueue = a;
    ((ThreadPoolExecutor)localObject).<init>(1, 1, 1L, localTimeUnit, localBlockingQueue);
    b = ((ThreadPoolExecutor)localObject);
  }
  
  private void b()
  {
    b localb = (b)c.poll();
    d = localb;
    localb = d;
    if (localb != null)
    {
      ThreadPoolExecutor localThreadPoolExecutor = b;
      localb.a(localThreadPoolExecutor);
    }
  }
  
  public final void a()
  {
    d = null;
    b();
  }
  
  public final void a(b paramb)
  {
    e = this;
    ArrayDeque localArrayDeque = c;
    localArrayDeque.add(paramb);
    paramb = d;
    if (paramb == null) {
      b();
    }
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.i.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */