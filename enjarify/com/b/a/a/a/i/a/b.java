package com.b.a.a.a.i.a;

import android.os.AsyncTask;
import android.os.Build.VERSION;
import java.util.concurrent.ThreadPoolExecutor;

public abstract class b
  extends AsyncTask
{
  b.a e;
  protected final b.b f;
  
  public b(b.b paramb)
  {
    f = paramb;
  }
  
  protected void a(String paramString)
  {
    paramString = e;
    if (paramString != null) {
      paramString.a();
    }
  }
  
  public final void a(ThreadPoolExecutor paramThreadPoolExecutor)
  {
    int i = Build.VERSION.SDK_INT;
    int j = 11;
    if (i > j)
    {
      Object[] arrayOfObject = new Object[0];
      executeOnExecutor(paramThreadPoolExecutor, arrayOfObject);
      return;
    }
    paramThreadPoolExecutor = new Object[0];
    execute(paramThreadPoolExecutor);
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.i.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */