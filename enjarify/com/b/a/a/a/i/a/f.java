package com.b.a.a.a.i.a;

import android.text.TextUtils;
import com.b.a.a.a.f.a.a.a;
import com.b.a.a.a.f.a.b;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import org.json.JSONObject;

public final class f
  extends a
{
  public f(b.b paramb, com.b.a.a.a.e.a parama, HashSet paramHashSet, JSONObject paramJSONObject, double paramDouble)
  {
    super(paramb, parama, paramHashSet, paramJSONObject, paramDouble);
  }
  
  protected final void a(String paramString)
  {
    boolean bool1 = TextUtils.isEmpty(paramString);
    if (!bool1)
    {
      Iterator localIterator = a.a.values().iterator();
      for (;;)
      {
        boolean bool2 = localIterator.hasNext();
        if (!bool2) {
          break;
        }
        com.b.a.a.a.f.a.a locala = (com.b.a.a.a.f.a.a)localIterator.next();
        Object localObject = b;
        String str = a.a;
        boolean bool3 = ((HashSet)localObject).contains(str);
        if (bool3)
        {
          double d1 = d;
          double d2 = i;
          boolean bool4 = d1 < d2;
          if (bool4)
          {
            b.a(paramString);
            localObject = a.a.b;
            h = ((a.a)localObject);
          }
        }
      }
    }
    super.a(paramString);
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.i.a.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */