package com.b.a.a.a.g;

import org.json.JSONObject;

public final class a
{
  public static String a(String paramString)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("setNativeViewState(");
    localStringBuilder.append(paramString);
    localStringBuilder.append(")");
    return e(localStringBuilder.toString());
  }
  
  public static String a(String paramString1, String paramString2)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("publishVideoEvent(");
    paramString1 = JSONObject.quote(paramString1);
    localStringBuilder.append(paramString1);
    localStringBuilder.append(",");
    localStringBuilder.append(paramString2);
    localStringBuilder.append(")");
    return e(localStringBuilder.toString());
  }
  
  public static String b(String paramString)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("setAppState(");
    paramString = JSONObject.quote(paramString);
    localStringBuilder.append(paramString);
    localStringBuilder.append(")");
    return e(localStringBuilder.toString());
  }
  
  public static String c(String paramString)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("publishVideoEvent(");
    paramString = JSONObject.quote(paramString);
    localStringBuilder.append(paramString);
    localStringBuilder.append(")");
    return e(localStringBuilder.toString());
  }
  
  public static String d(String paramString)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("setAvidAdSessionContext(");
    localStringBuilder.append(paramString);
    localStringBuilder.append(")");
    return e(localStringBuilder.toString());
  }
  
  public static String e(String paramString)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("javascript: if(window.avidbridge!==undefined){avidbridge.");
    localStringBuilder.append(paramString);
    localStringBuilder.append("}");
    return localStringBuilder.toString();
  }
  
  public static String f(String paramString)
  {
    paramString = String.valueOf(paramString);
    return "javascript: ".concat(paramString);
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.g.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */