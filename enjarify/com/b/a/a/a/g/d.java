package com.b.a.a.a.g;

import android.os.Build.VERSION;
import android.view.View;

public final class d
{
  public static boolean a(View paramView)
  {
    int i = paramView.getVisibility();
    if (i != 0) {
      return false;
    }
    i = Build.VERSION.SDK_INT;
    int j = 11;
    boolean bool1 = true;
    if (i >= j)
    {
      float f = paramView.getAlpha();
      double d1 = f;
      double d2 = 0.0D;
      boolean bool2 = d1 < d2;
      if (bool2) {
        return bool1;
      }
      return false;
    }
    return bool1;
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.g.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */