package com.b.a.a.a.g;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class b
{
  static float a = getSystemgetDisplayMetricsdensity;
  private static String[] b;
  
  static
  {
    String[] tmp4_1 = new String[4];
    String[] tmp5_4 = tmp4_1;
    String[] tmp5_4 = tmp4_1;
    tmp5_4[0] = "x";
    tmp5_4[1] = "y";
    tmp5_4[2] = "width";
    String[] tmp18_5 = tmp5_4;
    tmp18_5[3] = "height";
    b = tmp18_5;
  }
  
  public static JSONObject a()
  {
    JSONObject localJSONObject = a(0, 0, 0, 0);
    double d = c.a();
    return a(localJSONObject, d);
  }
  
  public static JSONObject a(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    JSONObject localJSONObject = new org/json/JSONObject;
    localJSONObject.<init>();
    String str1 = "x";
    float f1 = paramInt1;
    try
    {
      float f2 = a;
      f1 /= f2;
      double d1 = f1;
      localJSONObject.put(str1, d1);
      str2 = "y";
      float f3 = paramInt2;
      float f4 = a;
      f3 /= f4;
      double d2 = f3;
      localJSONObject.put(str2, d2);
      str2 = "width";
      f3 = paramInt3;
      float f5 = a;
      f3 /= f5;
      double d3 = f3;
      localJSONObject.put(str2, d3);
      str2 = "height";
      f3 = paramInt4;
      f5 = a;
      f3 /= f5;
      d3 = f3;
      localJSONObject.put(str2, d3);
    }
    catch (JSONException localJSONException)
    {
      String str2 = "Error with creating viewStateObject";
      TextUtils.isEmpty(str2);
    }
    return localJSONObject;
  }
  
  public static JSONObject a(JSONObject paramJSONObject, double paramDouble)
  {
    JSONObject localJSONObject = new org/json/JSONObject;
    localJSONObject.<init>();
    String str1 = "timestamp";
    try
    {
      localJSONObject.put(str1, paramDouble);
      String str2 = "rootView";
      localJSONObject.put(str2, paramJSONObject);
    }
    catch (JSONException localJSONException)
    {
      paramJSONObject = "Error with creating treeJSONObject";
      TextUtils.isEmpty(paramJSONObject);
    }
    return localJSONObject;
  }
  
  public static void a(Context paramContext)
  {
    if (paramContext != null)
    {
      paramContext = paramContext.getResources().getDisplayMetrics();
      float f = density;
      a = f;
    }
  }
  
  public static void a(JSONObject paramJSONObject)
  {
    Object localObject = paramJSONObject.optJSONArray("childViews");
    if (localObject == null) {
      return;
    }
    int i = ((JSONArray)localObject).length();
    int j = 0;
    int k = 0;
    int m = 0;
    while (j < i)
    {
      JSONObject localJSONObject = ((JSONArray)localObject).optJSONObject(j);
      if (localJSONObject != null)
      {
        String str1 = "x";
        int n = localJSONObject.optInt(str1);
        String str2 = "y";
        int i1 = localJSONObject.optInt(str2);
        String str3 = "width";
        int i2 = localJSONObject.optInt(str3);
        String str4 = "height";
        int i3 = localJSONObject.optInt(str4);
        n += i2;
        k = Math.max(k, n);
        i1 += i3;
        m = Math.max(m, i1);
      }
      j += 1;
    }
    localObject = "width";
    try
    {
      paramJSONObject.put((String)localObject, k);
      localObject = "height";
      paramJSONObject.put((String)localObject, m);
      return;
    }
    catch (JSONException localJSONException)
    {
      localJSONException;
    }
  }
  
  public static void a(JSONObject paramJSONObject, String paramString)
  {
    String str = "id";
    try
    {
      paramJSONObject.put(str, paramString);
      return;
    }
    catch (JSONException localJSONException)
    {
      TextUtils.isEmpty("Error with setting avid id");
    }
  }
  
  public static void a(JSONObject paramJSONObject, List paramList)
  {
    JSONArray localJSONArray = new org/json/JSONArray;
    localJSONArray.<init>();
    paramList = paramList.iterator();
    for (;;)
    {
      boolean bool = paramList.hasNext();
      if (!bool) {
        break;
      }
      String str = (String)paramList.next();
      localJSONArray.put(str);
    }
    paramList = "isFriendlyObstructionFor";
    try
    {
      paramJSONObject.put(paramList, localJSONArray);
      return;
    }
    catch (JSONException localJSONException)
    {
      TextUtils.isEmpty("Error with setting friendly obstruction");
    }
  }
  
  public static void a(JSONObject paramJSONObject1, JSONObject paramJSONObject2)
  {
    Object localObject = "childViews";
    try
    {
      localObject = paramJSONObject1.optJSONArray((String)localObject);
      if (localObject == null)
      {
        localObject = new org/json/JSONArray;
        ((JSONArray)localObject).<init>();
        String str = "childViews";
        paramJSONObject1.put(str, localObject);
      }
      ((JSONArray)localObject).put(paramJSONObject2);
      return;
    }
    catch (JSONException localJSONException)
    {
      localJSONException;
    }
  }
  
  private static boolean a(JSONArray paramJSONArray1, JSONArray paramJSONArray2)
  {
    boolean bool = true;
    if ((paramJSONArray1 == null) && (paramJSONArray2 == null)) {
      return bool;
    }
    if (((paramJSONArray1 == null) && (paramJSONArray2 != null)) || ((paramJSONArray1 != null) && (paramJSONArray2 == null))) {
      return false;
    }
    int i = paramJSONArray1.length();
    int j = paramJSONArray2.length();
    if (i == j) {
      return bool;
    }
    return false;
  }
  
  public static boolean b(JSONObject paramJSONObject1, JSONObject paramJSONObject2)
  {
    if (paramJSONObject2 == null) {
      return false;
    }
    Object localObject1 = b;
    int i = localObject1.length;
    int j = 0;
    Object localObject2 = null;
    boolean bool2;
    String str1;
    for (;;)
    {
      bool2 = true;
      if (j >= i) {
        break;
      }
      str1 = localObject1[j];
      double d1 = paramJSONObject1.optDouble(str1);
      double d2 = paramJSONObject2.optDouble(str1);
      boolean bool3 = d1 < d2;
      if (bool3)
      {
        i1 = 0;
        localObject1 = null;
        break label82;
      }
      j += 1;
    }
    int i1 = 1;
    label82:
    if (i1 != 0)
    {
      localObject1 = paramJSONObject1.optString("id", "");
      localObject2 = "";
      Object localObject3 = paramJSONObject2.optString("id", (String)localObject2);
      i1 = ((String)localObject1).equals(localObject3);
      if (i1 != 0)
      {
        localObject1 = paramJSONObject1.optJSONArray("isFriendlyObstructionFor");
        localObject3 = paramJSONObject2.optJSONArray("isFriendlyObstructionFor");
        int k = a((JSONArray)localObject1, (JSONArray)localObject3);
        if (k == 0)
        {
          i1 = 0;
          localObject1 = null;
        }
        else
        {
          if (localObject1 != null)
          {
            k = 0;
            localObject2 = null;
            for (;;)
            {
              int n = ((JSONArray)localObject1).length();
              if (k >= n) {
                break;
              }
              str1 = ((JSONArray)localObject1).optString(k, "");
              String str2 = ((JSONArray)localObject3).optString(k, "");
              boolean bool4 = str1.equals(str2);
              if (!bool4)
              {
                i1 = 0;
                localObject1 = null;
                break label237;
              }
              int m;
              k += 1;
            }
          }
          i1 = 1;
        }
        label237:
        if (i1 != 0)
        {
          paramJSONObject1 = paramJSONObject1.optJSONArray("childViews");
          localObject1 = "childViews";
          paramJSONObject2 = paramJSONObject2.optJSONArray((String)localObject1);
          i1 = a(paramJSONObject1, paramJSONObject2);
          int i3;
          if (i1 == 0)
          {
            i3 = 0;
            paramJSONObject1 = null;
          }
          else
          {
            if (paramJSONObject1 != null)
            {
              i1 = 0;
              localObject1 = null;
              for (;;)
              {
                i = paramJSONObject1.length();
                if (i1 >= i) {
                  break;
                }
                localObject3 = paramJSONObject1.optJSONObject(i1);
                localObject2 = paramJSONObject2.optJSONObject(i1);
                boolean bool1 = b((JSONObject)localObject3, (JSONObject)localObject2);
                if (!bool1)
                {
                  i3 = 0;
                  paramJSONObject1 = null;
                  break label346;
                }
                int i2;
                i1 += 1;
              }
            }
            i3 = 1;
          }
          label346:
          if (i3 != 0) {
            return bool2;
          }
        }
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.g.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */