package com.b.a.a.a;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

public final class e
{
  private static e e;
  Context a;
  boolean b;
  boolean c;
  e.a d;
  private BroadcastReceiver f;
  
  static
  {
    e locale = new com/b/a/a/a/e;
    locale.<init>();
    e = locale;
  }
  
  public static e a()
  {
    return e;
  }
  
  private void e()
  {
    Object localObject = new com/b/a/a/a/e$1;
    ((e.1)localObject).<init>(this);
    f = ((BroadcastReceiver)localObject);
    localObject = new android/content/IntentFilter;
    ((IntentFilter)localObject).<init>();
    ((IntentFilter)localObject).addAction("android.intent.action.SCREEN_OFF");
    ((IntentFilter)localObject).addAction("android.intent.action.SCREEN_ON");
    ((IntentFilter)localObject).addAction("android.intent.action.USER_PRESENT");
    Context localContext = a;
    BroadcastReceiver localBroadcastReceiver = f;
    localContext.registerReceiver(localBroadcastReceiver, (IntentFilter)localObject);
  }
  
  public final void a(Context paramContext)
  {
    c();
    a = paramContext;
    e();
  }
  
  public final boolean b()
  {
    boolean bool = c;
    return !bool;
  }
  
  final void c()
  {
    Context localContext = a;
    if (localContext != null)
    {
      BroadcastReceiver localBroadcastReceiver = f;
      if (localBroadcastReceiver != null)
      {
        localContext.unregisterReceiver(localBroadcastReceiver);
        localContext = null;
        f = null;
      }
    }
  }
  
  final void d()
  {
    boolean bool1 = c ^ true;
    Iterator localIterator = aa.values().iterator();
    for (;;)
    {
      boolean bool2 = localIterator.hasNext();
      if (!bool2) {
        break;
      }
      com.b.a.a.a.f.a.a locala = (com.b.a.a.a.f.a.a)localIterator.next();
      locala.a(bool1);
    }
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */