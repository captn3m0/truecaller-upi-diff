package com.b.a.a.a.j;

import android.webkit.WebView;
import com.b.a.a.a.g.a;
import java.lang.ref.WeakReference;

public final class c
  extends b
{
  public c(WebView paramWebView)
  {
    super(paramWebView);
  }
  
  public final void a(String paramString)
  {
    paramString = a.f(paramString);
    b(paramString);
  }
  
  public final void b(String paramString)
  {
    WebView localWebView = (WebView)a.get();
    if (localWebView != null) {
      localWebView.loadUrl(paramString);
    }
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.j.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */