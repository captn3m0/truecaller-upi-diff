package com.b.a.a.a.j;

import java.lang.ref.WeakReference;

public class d
{
  public WeakReference a;
  
  public d(Object paramObject)
  {
    WeakReference localWeakReference = new java/lang/ref/WeakReference;
    localWeakReference.<init>(paramObject);
    a = localWeakReference;
  }
  
  public final void a(Object paramObject)
  {
    WeakReference localWeakReference = new java/lang/ref/WeakReference;
    localWeakReference.<init>(paramObject);
    a = localWeakReference;
  }
  
  public final boolean a()
  {
    Object localObject = a.get();
    return localObject == null;
  }
  
  public final boolean b(Object paramObject)
  {
    Object localObject = a.get();
    if ((localObject != null) && (paramObject != null))
    {
      boolean bool = localObject.equals(paramObject);
      if (bool) {
        return true;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.j.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */