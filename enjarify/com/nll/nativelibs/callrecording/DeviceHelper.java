package com.nll.nativelibs.callrecording;

import android.os.Build;
import android.os.Build.VERSION;
import com.truecaller.log.AssertionUtil;
import java.util.Locale;

public class DeviceHelper
{
  private static String getDeviceManufacturer()
  {
    try
    {
      String str = Build.MANUFACTURER;
      Locale localLocale = Locale.ENGLISH;
      return str.toUpperCase(localLocale);
    }
    catch (Exception localException)
    {
      AssertionUtil.reportThrowableButNeverCrash(localException;
    }
    return "";
  }
  
  private static String getDeviceModel()
  {
    try
    {
      String str = Build.MODEL;
      Locale localLocale = Locale.ENGLISH;
      return str.toUpperCase(localLocale);
    }
    catch (Exception localException)
    {
      AssertionUtil.reportThrowableButNeverCrash(localException;
    }
    return "";
  }
  
  public static boolean isAndroid71FixRequired()
  {
    String str1 = Build.VERSION.RELEASE;
    String str2 = "7.1.1";
    boolean bool = str1.equals(str2);
    if (!bool)
    {
      str1 = Build.VERSION.RELEASE;
      str2 = "7.1.2";
      bool = str1.equals(str2);
      if (!bool) {
        return false;
      }
    }
    return true;
  }
  
  private static boolean isHuaweiAndroid8AndAbove()
  {
    String str1 = getDeviceManufacturer();
    String str2 = "HUAWEI";
    boolean bool = str1.equals(str2);
    if (bool)
    {
      int i = Build.VERSION.SDK_INT;
      int j = 26;
      if (i >= j) {
        return true;
      }
    }
    return false;
  }
  
  private static boolean isHuaweiWithApi3()
  {
    boolean bool1 = isHuaweiAndroid8AndAbove();
    boolean bool2 = true;
    if (bool1) {
      return bool2;
    }
    String str1 = getDeviceManufacturer();
    bool1 = str1.equals("HUAWEI");
    if (bool1)
    {
      str1 = "ro.build.hw_emui_api_level";
      int i = PropManager.getInt(str1);
      String[] arrayOfString = new String[bool2];
      String str2 = String.valueOf(i);
      String str3 = "emuiVersion: ".concat(str2);
      arrayOfString[0] = str3;
      int j = 10;
      if (i >= j) {
        return bool2;
      }
      return false;
    }
    return false;
  }
  
  private static boolean isMotorolaWithApi3()
  {
    String str1 = getDeviceManufacturer();
    String str2 = "MOTOROLA";
    boolean bool1 = str1.equals(str2);
    if (bool1)
    {
      int i = Build.VERSION.SDK_INT;
      int j = 26;
      if (i >= j)
      {
        boolean bool2 = isNexus();
        if (!bool2) {
          return true;
        }
      }
    }
    return false;
  }
  
  private static boolean isNexus()
  {
    return getDeviceModel().contains("NEXUS");
  }
  
  static boolean useApi3()
  {
    boolean bool = isHuaweiWithApi3();
    if (!bool)
    {
      bool = isMotorolaWithApi3();
      if (!bool) {
        return false;
      }
    }
    return true;
  }
}

/* Location:
 * Qualified Name:     com.nll.nativelibs.callrecording.DeviceHelper
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */