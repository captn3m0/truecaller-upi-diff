package com.nll.nativelibs.callrecording;

public abstract interface AudioRecordWrapper$ErrorListener
{
  public abstract void onError(Exception paramException);
}

/* Location:
 * Qualified Name:     com.nll.nativelibs.callrecording.AudioRecordWrapper.ErrorListener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */