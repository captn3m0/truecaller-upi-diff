package com.nll.nativelibs.callrecording;

import android.content.Context;
import android.media.AudioRecord;
import android.os.Handler;
import android.os.Looper;
import com.truecaller.log.UnmutedException.d;

public class AudioRecordWrapper
  extends AudioRecord
{
  private static final int ANDROID_71_DELAY = 5000;
  private AudioRecordWrapper.ErrorListener errorListener;
  boolean useApi3;
  
  AudioRecordWrapper(Context paramContext, com.truecaller.callrecording.a parama, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
  {
    super(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5);
    paramInt1 = DeviceHelper.useApi3();
    useApi3 = paramInt1;
    Object localObject2 = a;
    String str1 = "callRecordingSerial";
    long l1 = 0L;
    long l2 = ((com.truecaller.common.g.a)localObject2).a(str1, l1);
    parama = a;
    String str2 = "callRecordingLicense";
    parama = parama.b(str2, "");
    paramInt3 = 1;
    String[] arrayOfString = new String[paramInt3];
    localObject1 = String.valueOf(parama);
    Object localObject3 = "AudioRecordWrapper:: Key: ".concat((String)localObject1);
    localObject1 = null;
    arrayOfString[0] = localObject3;
    arrayOfString = new String[paramInt3];
    localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>("Remaining license time in seconds  ");
    long l3 = Native.getExpiry(l2, parama);
    ((StringBuilder)localObject3).append(l3);
    localObject3 = ((StringBuilder)localObject3).toString();
    arrayOfString[0] = localObject3;
    arrayOfString = new String[paramInt3];
    localObject3 = new java/lang/StringBuilder;
    String str3 = "Package and cert check result  ";
    ((StringBuilder)localObject3).<init>(str3);
    i = Native.checkPackageAndCert(paramContext);
    ((StringBuilder)localObject3).append(i);
    localObject3 = ((StringBuilder)localObject3).toString();
    arrayOfString[0] = localObject3;
    paramInt4 = DeviceHelper.isAndroid71FixRequired();
    if (paramInt4 != 0)
    {
      sleepForAndroid71();
      paramInt4 = Native.FIX_ANDROID_7_1_ON;
      Native.fixAndroid71(paramInt4);
    }
    paramInt4 = useApi3;
    int j;
    if (paramInt4 != 0) {
      j = Native.start3(paramContext, this, l2, parama);
    } else {
      j = Native.start7(paramContext, this, l2, parama);
    }
    parama = new String[paramInt3];
    str1 = String.valueOf(j);
    localObject2 = "Start result ".concat(str1);
    parama[0] = localObject2;
    if (j != 0)
    {
      parama = errorListener;
      localObject2 = new com/truecaller/log/UnmutedException$d;
      str1 = "Error in call recorder: ";
      paramContext = String.valueOf(j);
      paramContext = str1.concat(paramContext);
      ((UnmutedException.d)localObject2).<init>(paramContext);
      parama.onError((Exception)localObject2);
    }
  }
  
  private void sleepForAndroid71()
  {
    long l = 5000L;
    try
    {
      Thread.sleep(l);
      return;
    }
    catch (InterruptedException localInterruptedException)
    {
      localInterruptedException;
    }
  }
  
  void setErrorListener(AudioRecordWrapper.ErrorListener paramErrorListener)
  {
    errorListener = paramErrorListener;
  }
  
  public void startRecording()
  {
    super.startRecording();
    new String[1][0] = "startRecording:: ";
    Handler localHandler = new android/os/Handler;
    Object localObject = Looper.getMainLooper();
    localHandler.<init>((Looper)localObject);
    localObject = new com/nll/nativelibs/callrecording/AudioRecordWrapper$1;
    ((AudioRecordWrapper.1)localObject).<init>(this);
    localHandler.postDelayed((Runnable)localObject, 500L);
  }
}

/* Location:
 * Qualified Name:     com.nll.nativelibs.callrecording.AudioRecordWrapper
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */