package com.nll.nativelibs.callrecording;

import android.text.TextUtils;
import java.util.ArrayList;

public class PropManager
{
  public static String get(String paramString)
  {
    String str = "/system/bin/getprop %s";
    int i = 1;
    Object[] arrayOfObject = new Object[i];
    arrayOfObject[0] = paramString;
    paramString = getRuntimeExecResult(String.format(str, arrayOfObject));
    int j = paramString.size();
    if (j > 0) {
      return (String)paramString.get(0);
    }
    return null;
  }
  
  public static ArrayList get()
  {
    return getRuntimeExecResult("/system/bin/getprop");
  }
  
  public static int getInt(String paramString)
  {
    return toInt(get(paramString));
  }
  
  /* Error */
  private static ArrayList getRuntimeExecResult(String paramString)
  {
    // Byte code:
    //   0: new 23	java/util/ArrayList
    //   3: astore_1
    //   4: aload_1
    //   5: invokespecial 41	java/util/ArrayList:<init>	()V
    //   8: aconst_null
    //   9: astore_2
    //   10: invokestatic 47	java/lang/Runtime:getRuntime	()Ljava/lang/Runtime;
    //   13: astore_3
    //   14: aload_3
    //   15: aload_0
    //   16: invokevirtual 51	java/lang/Runtime:exec	(Ljava/lang/String;)Ljava/lang/Process;
    //   19: astore_0
    //   20: aload_0
    //   21: invokevirtual 57	java/lang/Process:getInputStream	()Ljava/io/InputStream;
    //   24: astore_3
    //   25: aload_0
    //   26: invokevirtual 60	java/lang/Process:getErrorStream	()Ljava/io/InputStream;
    //   29: astore_2
    //   30: new 62	java/util/Scanner
    //   33: astore_0
    //   34: aload_0
    //   35: aload_3
    //   36: invokespecial 65	java/util/Scanner:<init>	(Ljava/io/InputStream;)V
    //   39: ldc 67
    //   41: astore 4
    //   43: aload_0
    //   44: aload 4
    //   46: invokevirtual 71	java/util/Scanner:useDelimiter	(Ljava/lang/String;)Ljava/util/Scanner;
    //   49: astore_0
    //   50: aload_0
    //   51: invokevirtual 75	java/util/Scanner:hasNext	()Z
    //   54: istore 5
    //   56: iload 5
    //   58: ifeq +19 -> 77
    //   61: aload_0
    //   62: invokevirtual 79	java/util/Scanner:next	()Ljava/lang/String;
    //   65: astore 4
    //   67: aload_1
    //   68: aload 4
    //   70: invokevirtual 83	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   73: pop
    //   74: goto -24 -> 50
    //   77: new 62	java/util/Scanner
    //   80: astore_0
    //   81: aload_0
    //   82: aload_2
    //   83: invokespecial 65	java/util/Scanner:<init>	(Ljava/io/InputStream;)V
    //   86: ldc 67
    //   88: astore 4
    //   90: aload_0
    //   91: aload 4
    //   93: invokevirtual 71	java/util/Scanner:useDelimiter	(Ljava/lang/String;)Ljava/util/Scanner;
    //   96: astore_0
    //   97: aload_0
    //   98: invokevirtual 75	java/util/Scanner:hasNext	()Z
    //   101: istore 5
    //   103: iload 5
    //   105: ifeq +19 -> 124
    //   108: aload_0
    //   109: invokevirtual 79	java/util/Scanner:next	()Ljava/lang/String;
    //   112: astore 4
    //   114: aload_1
    //   115: aload 4
    //   117: invokevirtual 83	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   120: pop
    //   121: goto -24 -> 97
    //   124: aload_3
    //   125: ifnull +15 -> 140
    //   128: aload_3
    //   129: invokevirtual 88	java/io/InputStream:close	()V
    //   132: goto +8 -> 140
    //   135: astore_0
    //   136: aload_0
    //   137: invokevirtual 93	java/io/IOException:printStackTrace	()V
    //   140: aload_2
    //   141: ifnull +71 -> 212
    //   144: aload_2
    //   145: invokevirtual 88	java/io/InputStream:close	()V
    //   148: goto +64 -> 212
    //   151: astore_0
    //   152: goto +71 -> 223
    //   155: astore_0
    //   156: aload_3
    //   157: astore 6
    //   159: aload_2
    //   160: astore_3
    //   161: aload 6
    //   163: astore_2
    //   164: goto +12 -> 176
    //   167: astore_0
    //   168: aconst_null
    //   169: astore_3
    //   170: goto +53 -> 223
    //   173: astore_0
    //   174: aconst_null
    //   175: astore_3
    //   176: aload_0
    //   177: invokevirtual 93	java/io/IOException:printStackTrace	()V
    //   180: aload_2
    //   181: ifnull +15 -> 196
    //   184: aload_2
    //   185: invokevirtual 88	java/io/InputStream:close	()V
    //   188: goto +8 -> 196
    //   191: astore_0
    //   192: aload_0
    //   193: invokevirtual 93	java/io/IOException:printStackTrace	()V
    //   196: aload_3
    //   197: ifnull +15 -> 212
    //   200: aload_3
    //   201: invokevirtual 88	java/io/InputStream:close	()V
    //   204: goto +8 -> 212
    //   207: astore_0
    //   208: aload_0
    //   209: invokevirtual 93	java/io/IOException:printStackTrace	()V
    //   212: aload_1
    //   213: areturn
    //   214: astore_0
    //   215: aload_3
    //   216: astore 6
    //   218: aload_2
    //   219: astore_3
    //   220: aload 6
    //   222: astore_2
    //   223: aload_3
    //   224: ifnull +15 -> 239
    //   227: aload_3
    //   228: invokevirtual 88	java/io/InputStream:close	()V
    //   231: goto +8 -> 239
    //   234: astore_1
    //   235: aload_1
    //   236: invokevirtual 93	java/io/IOException:printStackTrace	()V
    //   239: aload_2
    //   240: ifnull +15 -> 255
    //   243: aload_2
    //   244: invokevirtual 88	java/io/InputStream:close	()V
    //   247: goto +8 -> 255
    //   250: astore_1
    //   251: aload_1
    //   252: invokevirtual 93	java/io/IOException:printStackTrace	()V
    //   255: aload_0
    //   256: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	257	0	paramString	String
    //   3	210	1	localArrayList	ArrayList
    //   234	2	1	localIOException1	java.io.IOException
    //   250	2	1	localIOException2	java.io.IOException
    //   9	235	2	localObject1	Object
    //   13	215	3	localObject2	Object
    //   41	75	4	str	String
    //   54	50	5	bool	boolean
    //   157	64	6	localObject3	Object
    // Exception table:
    //   from	to	target	type
    //   128	132	135	java/io/IOException
    //   25	29	151	finally
    //   30	33	151	finally
    //   35	39	151	finally
    //   44	49	151	finally
    //   50	54	151	finally
    //   61	65	151	finally
    //   68	74	151	finally
    //   77	80	151	finally
    //   82	86	151	finally
    //   91	96	151	finally
    //   97	101	151	finally
    //   108	112	151	finally
    //   115	121	151	finally
    //   25	29	155	java/io/IOException
    //   30	33	155	java/io/IOException
    //   35	39	155	java/io/IOException
    //   44	49	155	java/io/IOException
    //   50	54	155	java/io/IOException
    //   61	65	155	java/io/IOException
    //   68	74	155	java/io/IOException
    //   77	80	155	java/io/IOException
    //   82	86	155	java/io/IOException
    //   91	96	155	java/io/IOException
    //   97	101	155	java/io/IOException
    //   108	112	155	java/io/IOException
    //   115	121	155	java/io/IOException
    //   10	13	167	finally
    //   15	19	167	finally
    //   20	24	167	finally
    //   10	13	173	java/io/IOException
    //   15	19	173	java/io/IOException
    //   20	24	173	java/io/IOException
    //   184	188	191	java/io/IOException
    //   144	148	207	java/io/IOException
    //   200	204	207	java/io/IOException
    //   176	180	214	finally
    //   227	231	234	java/io/IOException
    //   243	247	250	java/io/IOException
  }
  
  private static int toInt(String paramString)
  {
    if (paramString != null)
    {
      boolean bool = TextUtils.isDigitsOnly(paramString);
      if (bool) {
        return Integer.parseInt(paramString);
      }
    }
    return 0;
  }
}

/* Location:
 * Qualified Name:     com.nll.nativelibs.callrecording.PropManager
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */