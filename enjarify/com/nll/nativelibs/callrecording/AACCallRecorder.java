package com.nll.nativelibs.callrecording;

import android.content.Context;
import android.media.AudioRecord;
import android.media.MediaCodec;
import android.media.MediaFormat;
import android.media.MediaMuxer;
import com.truecaller.callrecording.CallRecorder;
import com.truecaller.callrecording.CallRecorder.RecordingState;
import com.truecaller.callrecording.CallRecorder.a;
import com.truecaller.callrecording.a;
import com.truecaller.log.AssertionUtil;

public class AACCallRecorder
  implements AudioRecordWrapper.ErrorListener, CallRecorder
{
  private static final int AUDIO_FORMAT = 2;
  private static final int AUDIO_SOURCE = 1;
  private static final int BIT_RATE_AUDIO = 32768;
  private static final int CHANNEL_CONFIG = 16;
  private static final int CHANNEL_COUNT = 1;
  private static final int FRAMES_PER_BUFFER = 24;
  private static final int MAX_INPUT_SIZE = 8192;
  private static final String MIME_TYPE_AUDIO = "audio/mp4a-latm";
  private static final int SAMPLES_PER_FRAME = 1024;
  private static final int SAMPLE_RATE = 8000;
  private AudioRecordWrapper aRecorder;
  private final AACCallRecorder.EncodingThread mEncodingThread;
  private CallRecorder.a mErrorListener;
  MediaMuxer mMuxer;
  private String mPath;
  volatile boolean mRecord;
  private boolean mRunning;
  private boolean mStarted;
  private CallRecorder.RecordingState mState;
  
  public AACCallRecorder(Context paramContext, a parama, CallRecorder.a parama1)
  {
    Object localObject1 = parama1;
    setErrorListener(parama1);
    localObject1 = null;
    try
    {
      mPath = null;
      localObject2 = CallRecorder.RecordingState.INITIALIZING;
      mState = ((CallRecorder.RecordingState)localObject2);
      int i = 16;
      int j = 8000;
      int k = 2;
      i = AudioRecord.getMinBufferSize(j, i, k);
      int m = 24576;
      int n = 1024;
      int i1 = 1;
      int i2;
      if (m < i)
      {
        i = (i / n + i1) * 1024;
        m = i * 2;
        i2 = m;
      }
      else
      {
        i2 = 24576;
      }
      localObject2 = new String[i1];
      m = 0;
      Object localObject3 = "mBufferSize: ";
      Object localObject4 = String.valueOf(i2);
      localObject3 = ((String)localObject3).concat((String)localObject4);
      localObject2[0] = localObject3;
      localObject2 = new com/nll/nativelibs/callrecording/AudioRecordWrapper;
      int i3 = 1;
      int i4 = 8000;
      int i5 = 16;
      int i6 = 2;
      localObject3 = localObject2;
      localObject4 = paramContext;
      ((AudioRecordWrapper)localObject2).<init>(paramContext, parama, i3, i4, i5, i6, i2);
      aRecorder = ((AudioRecordWrapper)localObject2);
      localObject2 = aRecorder;
      ((AudioRecordWrapper)localObject2).setErrorListener(localAACCallRecorder);
      localObject2 = aRecorder;
      i = ((AudioRecordWrapper)localObject2).getState();
      if (i == i1)
      {
        localObject2 = "audio/mp4a-latm";
        localObject2 = MediaFormat.createAudioFormat((String)localObject2, j, i1);
        Object localObject5 = "aac-profile";
        ((MediaFormat)localObject2).setInteger((String)localObject5, k);
        localObject5 = "max-input-size";
        k = 8192;
        ((MediaFormat)localObject2).setInteger((String)localObject5, k);
        localObject5 = "bitrate";
        k = 32768;
        ((MediaFormat)localObject2).setInteger((String)localObject5, k);
        localObject5 = "audio/mp4a-latm";
        localObject5 = MediaCodec.createEncoderByType((String)localObject5);
        ((MediaCodec)localObject5).configure((MediaFormat)localObject2, null, null, i1);
        localObject1 = new com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread;
        localObject2 = aRecorder;
        ((AACCallRecorder.EncodingThread)localObject1).<init>(localAACCallRecorder, (AudioRecord)localObject2, (MediaCodec)localObject5, n);
        mEncodingThread = ((AACCallRecorder.EncodingThread)localObject1);
        localObject1 = "AudioRecordWrapper initialized: ";
        new String[1][0] = localObject1;
        return;
      }
      localObject1 = "AudioRecordWrapper initialization failed: ";
      new String[1][0] = localObject1;
      localObject1 = new java/lang/Exception;
      localObject2 = "AudioRecordWrapper initialization failed";
      ((Exception)localObject1).<init>((String)localObject2);
      throw ((Throwable)localObject1);
    }
    catch (Exception localException)
    {
      AssertionUtil.reportThrowableButNeverCrash(localException);
      Object localObject2 = CallRecorder.RecordingState.ERROR;
      mState = ((CallRecorder.RecordingState)localObject2);
      throw localException;
    }
  }
  
  public String getOutputFile()
  {
    return mPath;
  }
  
  public CallRecorder.RecordingState getRecordingState()
  {
    return mState;
  }
  
  public boolean isRecording()
  {
    CallRecorder.RecordingState localRecordingState1 = mState;
    CallRecorder.RecordingState localRecordingState2 = CallRecorder.RecordingState.RECORDING;
    return localRecordingState1 == localRecordingState2;
  }
  
  public void onError(Exception paramException)
  {
    Object localObject = CallRecorder.RecordingState.ERROR;
    mState = ((CallRecorder.RecordingState)localObject);
    localObject = mErrorListener;
    if (localObject != null) {
      ((CallRecorder.a)localObject).onError(paramException);
    }
  }
  
  public void pause()
  {
    CallRecorder.RecordingState localRecordingState = CallRecorder.RecordingState.PAUSED;
    mState = localRecordingState;
    mRecord = false;
  }
  
  public void prepare()
  {
    int i = 1;
    Object localObject1 = new String[i];
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("prepare: State: ");
    String str = mState.name();
    ((StringBuilder)localObject2).append(str);
    localObject2 = ((StringBuilder)localObject2).toString();
    str = null;
    localObject1[0] = localObject2;
    localObject1 = mState;
    localObject2 = CallRecorder.RecordingState.INITIALIZING;
    if (localObject1 == localObject2)
    {
      new String[1][0] = "prepare: INITIALIZING";
      localObject1 = aRecorder;
      int j = ((AudioRecordWrapper)localObject1).getState();
      if (j == i)
      {
        localObject3 = mPath;
        if (localObject3 != null)
        {
          localObject3 = CallRecorder.RecordingState.READY;
          mState = ((CallRecorder.RecordingState)localObject3);
          return;
        }
      }
      new String[1][0] = "prepare:: Error";
      localObject3 = CallRecorder.RecordingState.ERROR;
      mState = ((CallRecorder.RecordingState)localObject3);
      localObject3 = new java/lang/IllegalStateException;
      ((IllegalStateException)localObject3).<init>("Error in Audio Recorder State");
      throw ((Throwable)localObject3);
    }
    new String[1][0] = "prepare:: Error - release";
    Object localObject3 = CallRecorder.RecordingState.ERROR;
    mState = ((CallRecorder.RecordingState)localObject3);
    release();
    localObject3 = new java/lang/IllegalArgumentException;
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("Prepare called in wrong state : ");
    localObject2 = mState;
    ((StringBuilder)localObject1).append(localObject2);
    localObject1 = ((StringBuilder)localObject1).toString();
    ((IllegalArgumentException)localObject3).<init>((String)localObject1);
    throw ((Throwable)localObject3);
  }
  
  public void release()
  {
    new String[1][0] = "release:: ";
    CallRecorder.RecordingState localRecordingState1 = mState;
    CallRecorder.RecordingState localRecordingState2 = CallRecorder.RecordingState.RECORDING;
    if (localRecordingState1 != localRecordingState2)
    {
      localRecordingState1 = mState;
      localRecordingState2 = CallRecorder.RecordingState.PAUSED;
      if (localRecordingState1 != localRecordingState2) {}
    }
    else
    {
      stop();
    }
  }
  
  /* Error */
  public void reset()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: iconst_1
    //   3: istore_1
    //   4: iload_1
    //   5: anewarray 77	java/lang/String
    //   8: astore_2
    //   9: aconst_null
    //   10: astore_3
    //   11: new 174	java/lang/StringBuilder
    //   14: astore 4
    //   16: ldc -36
    //   18: astore 5
    //   20: aload 4
    //   22: aload 5
    //   24: invokespecial 177	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   27: aload_0
    //   28: getfield 63	com/nll/nativelibs/callrecording/AACCallRecorder:mState	Lcom/truecaller/callrecording/CallRecorder$RecordingState;
    //   31: astore 5
    //   33: aload 5
    //   35: invokevirtual 181	com/truecaller/callrecording/CallRecorder$RecordingState:name	()Ljava/lang/String;
    //   38: astore 5
    //   40: aload 4
    //   42: aload 5
    //   44: invokevirtual 185	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   47: pop
    //   48: aload 4
    //   50: invokevirtual 188	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   53: astore 4
    //   55: aload_2
    //   56: iconst_0
    //   57: aload 4
    //   59: aastore
    //   60: aload_0
    //   61: getfield 63	com/nll/nativelibs/callrecording/AACCallRecorder:mState	Lcom/truecaller/callrecording/CallRecorder$RecordingState;
    //   64: astore_2
    //   65: getstatic 156	com/truecaller/callrecording/CallRecorder$RecordingState:ERROR	Lcom/truecaller/callrecording/CallRecorder$RecordingState;
    //   68: astore_3
    //   69: aload_2
    //   70: aload_3
    //   71: if_acmpeq +16 -> 87
    //   74: aload_0
    //   75: invokevirtual 205	com/nll/nativelibs/callrecording/AACCallRecorder:release	()V
    //   78: getstatic 61	com/truecaller/callrecording/CallRecorder$RecordingState:INITIALIZING	Lcom/truecaller/callrecording/CallRecorder$RecordingState;
    //   81: astore_2
    //   82: aload_0
    //   83: aload_2
    //   84: putfield 63	com/nll/nativelibs/callrecording/AACCallRecorder:mState	Lcom/truecaller/callrecording/CallRecorder$RecordingState;
    //   87: aload_0
    //   88: monitorexit
    //   89: return
    //   90: astore_2
    //   91: goto +32 -> 123
    //   94: astore_2
    //   95: aload_2
    //   96: invokestatic 153	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   99: getstatic 156	com/truecaller/callrecording/CallRecorder$RecordingState:ERROR	Lcom/truecaller/callrecording/CallRecorder$RecordingState;
    //   102: astore_3
    //   103: aload_0
    //   104: aload_3
    //   105: putfield 63	com/nll/nativelibs/callrecording/AACCallRecorder:mState	Lcom/truecaller/callrecording/CallRecorder$RecordingState;
    //   108: aload_0
    //   109: getfield 161	com/nll/nativelibs/callrecording/AACCallRecorder:mErrorListener	Lcom/truecaller/callrecording/CallRecorder$a;
    //   112: astore_3
    //   113: aload_3
    //   114: aload_2
    //   115: invokeinterface 167 2 0
    //   120: aload_0
    //   121: monitorexit
    //   122: return
    //   123: aload_0
    //   124: monitorexit
    //   125: aload_2
    //   126: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	127	0	this	AACCallRecorder
    //   3	2	1	i	int
    //   8	76	2	localObject1	Object
    //   90	1	2	localObject2	Object
    //   94	32	2	localException	Exception
    //   10	104	3	localObject3	Object
    //   14	44	4	localObject4	Object
    //   18	25	5	localObject5	Object
    // Exception table:
    //   from	to	target	type
    //   4	8	90	finally
    //   11	14	90	finally
    //   22	27	90	finally
    //   27	31	90	finally
    //   33	38	90	finally
    //   42	48	90	finally
    //   48	53	90	finally
    //   57	60	90	finally
    //   60	64	90	finally
    //   65	68	90	finally
    //   74	78	90	finally
    //   78	81	90	finally
    //   83	87	90	finally
    //   95	99	90	finally
    //   99	102	90	finally
    //   104	108	90	finally
    //   108	112	90	finally
    //   114	120	90	finally
    //   4	8	94	java/lang/Exception
    //   11	14	94	java/lang/Exception
    //   22	27	94	java/lang/Exception
    //   27	31	94	java/lang/Exception
    //   33	38	94	java/lang/Exception
    //   42	48	94	java/lang/Exception
    //   48	53	94	java/lang/Exception
    //   57	60	94	java/lang/Exception
    //   60	64	94	java/lang/Exception
    //   65	68	94	java/lang/Exception
    //   74	78	94	java/lang/Exception
    //   78	81	94	java/lang/Exception
    //   83	87	94	java/lang/Exception
  }
  
  public void resume()
  {
    CallRecorder.RecordingState localRecordingState = CallRecorder.RecordingState.RECORDING;
    mState = localRecordingState;
    mRecord = true;
  }
  
  public void setErrorListener(CallRecorder.a parama)
  {
    if (parama == null)
    {
      parama = new com/nll/nativelibs/callrecording/AACCallRecorder$1;
      parama.<init>(this);
    }
    mErrorListener = parama;
  }
  
  /* Error */
  public void setOutputFile(String paramString)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: ldc -29
    //   4: astore_2
    //   5: iconst_1
    //   6: anewarray 77	java/lang/String
    //   9: iconst_0
    //   10: aload_2
    //   11: aastore
    //   12: aload_0
    //   13: getfield 63	com/nll/nativelibs/callrecording/AACCallRecorder:mState	Lcom/truecaller/callrecording/CallRecorder$RecordingState;
    //   16: astore_2
    //   17: getstatic 61	com/truecaller/callrecording/CallRecorder$RecordingState:INITIALIZING	Lcom/truecaller/callrecording/CallRecorder$RecordingState;
    //   20: astore_3
    //   21: aload_2
    //   22: aload_3
    //   23: if_acmpne +43 -> 66
    //   26: iconst_1
    //   27: istore 4
    //   29: iload 4
    //   31: anewarray 77	java/lang/String
    //   34: astore_2
    //   35: aconst_null
    //   36: astore_3
    //   37: ldc -27
    //   39: astore 5
    //   41: aload_1
    //   42: invokestatic 232	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   45: astore 6
    //   47: aload 5
    //   49: aload 6
    //   51: invokevirtual 87	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   54: astore 5
    //   56: aload_2
    //   57: iconst_0
    //   58: aload 5
    //   60: aastore
    //   61: aload_0
    //   62: aload_1
    //   63: putfield 56	com/nll/nativelibs/callrecording/AACCallRecorder:mPath	Ljava/lang/String;
    //   66: aload_0
    //   67: monitorexit
    //   68: return
    //   69: astore_1
    //   70: goto +28 -> 98
    //   73: astore_1
    //   74: getstatic 156	com/truecaller/callrecording/CallRecorder$RecordingState:ERROR	Lcom/truecaller/callrecording/CallRecorder$RecordingState;
    //   77: astore_2
    //   78: aload_0
    //   79: aload_2
    //   80: putfield 63	com/nll/nativelibs/callrecording/AACCallRecorder:mState	Lcom/truecaller/callrecording/CallRecorder$RecordingState;
    //   83: aload_0
    //   84: getfield 161	com/nll/nativelibs/callrecording/AACCallRecorder:mErrorListener	Lcom/truecaller/callrecording/CallRecorder$a;
    //   87: astore_2
    //   88: aload_2
    //   89: aload_1
    //   90: invokeinterface 167 2 0
    //   95: aload_0
    //   96: monitorexit
    //   97: return
    //   98: aload_0
    //   99: monitorexit
    //   100: aload_1
    //   101: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	102	0	this	AACCallRecorder
    //   0	102	1	paramString	String
    //   4	85	2	localObject	Object
    //   20	17	3	localRecordingState	CallRecorder.RecordingState
    //   27	3	4	i	int
    //   39	20	5	str1	String
    //   45	5	6	str2	String
    // Exception table:
    //   from	to	target	type
    //   5	12	69	finally
    //   12	16	69	finally
    //   17	20	69	finally
    //   29	34	69	finally
    //   41	45	69	finally
    //   49	54	69	finally
    //   58	61	69	finally
    //   62	66	69	finally
    //   74	77	69	finally
    //   79	83	69	finally
    //   83	87	69	finally
    //   89	95	69	finally
    //   5	12	73	java/lang/Exception
    //   12	16	73	java/lang/Exception
    //   17	20	73	java/lang/Exception
    //   29	34	73	java/lang/Exception
    //   41	45	73	java/lang/Exception
    //   49	54	73	java/lang/Exception
    //   58	61	73	java/lang/Exception
    //   62	66	73	java/lang/Exception
  }
  
  public void start()
  {
    boolean bool1 = true;
    try
    {
      Object localObject1 = new String[bool1];
      Object localObject2 = new java/lang/StringBuilder;
      Object localObject3 = "start:: mState: ";
      ((StringBuilder)localObject2).<init>((String)localObject3);
      localObject3 = mState;
      ((StringBuilder)localObject2).append(localObject3);
      localObject2 = ((StringBuilder)localObject2).toString();
      localObject3 = null;
      localObject1[0] = localObject2;
      localObject1 = mState;
      localObject2 = CallRecorder.RecordingState.READY;
      if (localObject1 != localObject2)
      {
        localObject4 = "start:: Error";
        new String[1][0] = localObject4;
        localObject4 = CallRecorder.RecordingState.ERROR;
        mState = ((CallRecorder.RecordingState)localObject4);
        return;
      }
      localObject1 = new android/media/MediaMuxer;
      localObject2 = mPath;
      ((MediaMuxer)localObject1).<init>((String)localObject2, 0);
      mMuxer = ((MediaMuxer)localObject1);
      boolean bool2 = mStarted;
      if (!bool2)
      {
        mStarted = bool1;
        mRunning = bool1;
        mRecord = bool1;
        localObject4 = CallRecorder.RecordingState.RECORDING;
        mState = ((CallRecorder.RecordingState)localObject4);
        localObject4 = mEncodingThread;
        ((AACCallRecorder.EncodingThread)localObject4).start();
        return;
      }
      Object localObject4 = new java/lang/IllegalStateException;
      localObject1 = "Already mStarted";
      ((IllegalStateException)localObject4).<init>((String)localObject1);
      throw ((Throwable)localObject4);
    }
    finally {}
  }
  
  public void stop()
  {
    boolean bool = true;
    try
    {
      Object localObject1 = new String[bool];
      Object localObject3 = new java/lang/StringBuilder;
      Object localObject4 = "stop: mState: ";
      ((StringBuilder)localObject3).<init>((String)localObject4);
      localObject4 = mState;
      ((StringBuilder)localObject3).append(localObject4);
      localObject3 = ((StringBuilder)localObject3).toString();
      localObject4 = null;
      localObject1[0] = localObject3;
      localObject1 = mState;
      localObject3 = CallRecorder.RecordingState.RECORDING;
      if (localObject1 != localObject3)
      {
        localObject1 = CallRecorder.RecordingState.ERROR;
        mState = ((CallRecorder.RecordingState)localObject1);
        return;
      }
      bool = mStarted;
      if (bool)
      {
        bool = mRunning;
        if (bool)
        {
          mRunning = false;
          localObject1 = mEncodingThread;
          ((AACCallRecorder.EncodingThread)localObject1).finish();
          localObject1 = CallRecorder.RecordingState.STOPPED;
          mState = ((CallRecorder.RecordingState)localObject1);
          return;
        }
        localObject1 = new java/lang/IllegalStateException;
        localObject3 = "Already finished";
        ((IllegalStateException)localObject1).<init>((String)localObject3);
        throw ((Throwable)localObject1);
      }
      localObject1 = new java/lang/IllegalStateException;
      localObject3 = "Not mStarted";
      ((IllegalStateException)localObject1).<init>((String)localObject3);
      throw ((Throwable)localObject1);
    }
    finally {}
  }
}

/* Location:
 * Qualified Name:     com.nll.nativelibs.callrecording.AACCallRecorder
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */