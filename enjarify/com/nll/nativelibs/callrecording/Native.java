package com.nll.nativelibs.callrecording;

import android.content.Context;
import android.media.AudioRecord;

public class Native
{
  public static int AUDIO_SOURCE_DOWN_LINK = 3;
  public static int AUDIO_SOURCE_UP_LINK = 2;
  public static int BLUETOOTH_NOISE_REDUCTION_OFF = 0;
  public static int BLUETOOTH_NOISE_REDUCTION_ON = 1;
  public static int FIX_ANDROID_7_1_OFF = 0;
  public static int FIX_ANDROID_7_1_ON = 1;
  
  static
  {
    System.loadLibrary("acr");
  }
  
  public static native int checkPackageAndCert(Context paramContext);
  
  public static native int fixAndroid71(int paramInt);
  
  public static native long getExpiry(long paramLong, String paramString);
  
  public static native int setBluetoothNoiseReduction(int paramInt);
  
  public static native int setSource(int paramInt);
  
  static native int start3(Context paramContext, AudioRecord paramAudioRecord, long paramLong, String paramString);
  
  static native int start7(Context paramContext, AudioRecord paramAudioRecord, long paramLong, String paramString);
  
  public static native int stop3();
  
  static native int stop7();
}

/* Location:
 * Qualified Name:     com.nll.nativelibs.callrecording.Native
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */