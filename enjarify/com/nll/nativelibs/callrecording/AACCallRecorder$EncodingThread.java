package com.nll.nativelibs.callrecording;

import android.media.AudioRecord;
import android.media.MediaCodec;

class AACCallRecorder$EncodingThread
  extends Thread
{
  private static final int TIMEOUT_USEC = 5000;
  private boolean isMuxerStarted;
  private AudioRecord mAudioRecorder;
  private final int mBufferSize;
  private final MediaCodec mEncoder;
  private volatile boolean mRunning = true;
  private long mTimeLast;
  
  AACCallRecorder$EncodingThread(AACCallRecorder paramAACCallRecorder, AudioRecord paramAudioRecord, MediaCodec paramMediaCodec, int paramInt)
  {
    mAudioRecorder = paramAudioRecord;
    mEncoder = paramMediaCodec;
    mBufferSize = paramInt;
    mEncoder.start();
  }
  
  public void finish()
  {
    new String[1][0] = "finish:: ";
    mRunning = false;
  }
  
  /* Error */
  public void run()
  {
    // Byte code:
    //   0: aload_0
    //   1: astore_1
    //   2: aload_0
    //   3: getfield 35	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:mBufferSize	I
    //   6: istore_2
    //   7: iload_2
    //   8: newarray <illegal type>
    //   10: astore_3
    //   11: aload_0
    //   12: getfield 31	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:mAudioRecorder	Landroid/media/AudioRecord;
    //   15: astore 4
    //   17: aload 4
    //   19: invokevirtual 49	android/media/AudioRecord:startRecording	()V
    //   22: new 51	android/media/MediaCodec$BufferInfo
    //   25: astore 4
    //   27: aload 4
    //   29: invokespecial 52	android/media/MediaCodec$BufferInfo:<init>	()V
    //   32: aconst_null
    //   33: astore 5
    //   35: aload_0
    //   36: getfield 33	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:mEncoder	Landroid/media/MediaCodec;
    //   39: astore 6
    //   41: aload 6
    //   43: invokevirtual 56	android/media/MediaCodec:getOutputBuffers	()[Ljava/nio/ByteBuffer;
    //   46: astore 6
    //   48: iconst_0
    //   49: istore 7
    //   51: aconst_null
    //   52: astore 8
    //   54: aload_1
    //   55: getfield 29	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:mRunning	Z
    //   58: istore 9
    //   60: iload 9
    //   62: ifeq +614 -> 676
    //   65: aload_1
    //   66: getfield 31	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:mAudioRecorder	Landroid/media/AudioRecord;
    //   69: astore 10
    //   71: aload_3
    //   72: arraylength
    //   73: istore 11
    //   75: aload 10
    //   77: aload_3
    //   78: iconst_0
    //   79: iload 11
    //   81: invokevirtual 60	android/media/AudioRecord:read	([BII)I
    //   84: istore 9
    //   86: bipush -2
    //   88: istore 11
    //   90: iload 9
    //   92: iload 11
    //   94: if_icmpeq +572 -> 666
    //   97: bipush -3
    //   99: istore 12
    //   101: iload 9
    //   103: iload 12
    //   105: if_icmpne +6 -> 111
    //   108: goto +558 -> 666
    //   111: aload_1
    //   112: getfield 22	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:this$0	Lcom/nll/nativelibs/callrecording/AACCallRecorder;
    //   115: astore 10
    //   117: aload 10
    //   119: getfield 65	com/nll/nativelibs/callrecording/AACCallRecorder:mRecord	Z
    //   122: istore 9
    //   124: iload 9
    //   126: ifeq +146 -> 272
    //   129: aload 5
    //   131: ifnonnull +16 -> 147
    //   134: aload_1
    //   135: getfield 33	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:mEncoder	Landroid/media/MediaCodec;
    //   138: astore 5
    //   140: aload 5
    //   142: invokevirtual 68	android/media/MediaCodec:getInputBuffers	()[Ljava/nio/ByteBuffer;
    //   145: astore 5
    //   147: aload_1
    //   148: getfield 33	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:mEncoder	Landroid/media/MediaCodec;
    //   151: astore 10
    //   153: iconst_m1
    //   154: i2l
    //   155: lstore 13
    //   157: aload 10
    //   159: lload 13
    //   161: invokevirtual 72	android/media/MediaCodec:dequeueInputBuffer	(J)I
    //   164: istore 15
    //   166: iload 15
    //   168: iflt +104 -> 272
    //   171: aload 5
    //   173: iload 15
    //   175: aaload
    //   176: astore 10
    //   178: aload 10
    //   180: invokevirtual 78	java/nio/ByteBuffer:clear	()Ljava/nio/Buffer;
    //   183: pop
    //   184: aload 10
    //   186: aload_3
    //   187: invokevirtual 82	java/nio/ByteBuffer:put	([B)Ljava/nio/ByteBuffer;
    //   190: pop
    //   191: aload_1
    //   192: getfield 84	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:mTimeLast	J
    //   195: lstore 13
    //   197: lconst_1
    //   198: lstore 16
    //   200: lload 13
    //   202: lload 16
    //   204: ladd
    //   205: lstore 13
    //   207: invokestatic 92	java/lang/System:nanoTime	()J
    //   210: lstore 16
    //   212: ldc2_w 93
    //   215: lstore 18
    //   217: lload 16
    //   219: lload 18
    //   221: ldiv
    //   222: lstore 16
    //   224: lload 13
    //   226: lload 16
    //   228: invokestatic 102	java/lang/Math:max	(JJ)J
    //   231: lstore 13
    //   233: aload_1
    //   234: lload 13
    //   236: putfield 84	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:mTimeLast	J
    //   239: aload_1
    //   240: getfield 33	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:mEncoder	Landroid/media/MediaCodec;
    //   243: astore 20
    //   245: aload_3
    //   246: arraylength
    //   247: istore 21
    //   249: aload_1
    //   250: getfield 84	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:mTimeLast	J
    //   253: lstore 13
    //   255: lload 13
    //   257: lstore 18
    //   259: aload 20
    //   261: iload 15
    //   263: iconst_0
    //   264: iload 21
    //   266: lload 13
    //   268: iconst_0
    //   269: invokevirtual 106	android/media/MediaCodec:queueInputBuffer	(IIIJI)V
    //   272: aload_1
    //   273: getfield 33	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:mEncoder	Landroid/media/MediaCodec;
    //   276: astore 10
    //   278: ldc2_w 107
    //   281: lstore 13
    //   283: aload 10
    //   285: aload 4
    //   287: lload 13
    //   289: invokevirtual 114	android/media/MediaCodec:dequeueOutputBuffer	(Landroid/media/MediaCodec$BufferInfo;J)I
    //   292: istore 9
    //   294: iconst_m1
    //   295: istore 22
    //   297: iload 9
    //   299: iload 22
    //   301: if_icmpeq -247 -> 54
    //   304: iload 9
    //   306: iload 12
    //   308: if_icmpne +19 -> 327
    //   311: aload_1
    //   312: getfield 33	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:mEncoder	Landroid/media/MediaCodec;
    //   315: astore 6
    //   317: aload 6
    //   319: invokevirtual 56	android/media/MediaCodec:getOutputBuffers	()[Ljava/nio/ByteBuffer;
    //   322: astore 6
    //   324: goto -270 -> 54
    //   327: iconst_1
    //   328: istore 12
    //   330: iload 9
    //   332: iload 11
    //   334: if_icmpne +65 -> 399
    //   337: aload_1
    //   338: getfield 33	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:mEncoder	Landroid/media/MediaCodec;
    //   341: astore 8
    //   343: aload 8
    //   345: invokevirtual 118	android/media/MediaCodec:getOutputFormat	()Landroid/media/MediaFormat;
    //   348: astore 8
    //   350: aload_1
    //   351: getfield 22	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:this$0	Lcom/nll/nativelibs/callrecording/AACCallRecorder;
    //   354: astore 10
    //   356: aload 10
    //   358: getfield 122	com/nll/nativelibs/callrecording/AACCallRecorder:mMuxer	Landroid/media/MediaMuxer;
    //   361: astore 10
    //   363: aload 10
    //   365: aload 8
    //   367: invokevirtual 128	android/media/MediaMuxer:addTrack	(Landroid/media/MediaFormat;)I
    //   370: istore 7
    //   372: aload_1
    //   373: getfield 22	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:this$0	Lcom/nll/nativelibs/callrecording/AACCallRecorder;
    //   376: astore 10
    //   378: aload 10
    //   380: getfield 122	com/nll/nativelibs/callrecording/AACCallRecorder:mMuxer	Landroid/media/MediaMuxer;
    //   383: astore 10
    //   385: aload 10
    //   387: invokevirtual 129	android/media/MediaMuxer:start	()V
    //   390: aload_1
    //   391: iload 12
    //   393: putfield 131	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:isMuxerStarted	Z
    //   396: goto -342 -> 54
    //   399: iload 9
    //   401: ifge +39 -> 440
    //   404: iload 12
    //   406: anewarray 44	java/lang/String
    //   409: astore 23
    //   411: ldc -123
    //   413: astore 24
    //   415: iload 9
    //   417: invokestatic 137	java/lang/String:valueOf	(I)Ljava/lang/String;
    //   420: astore 10
    //   422: aload 24
    //   424: aload 10
    //   426: invokevirtual 141	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   429: astore 10
    //   431: aload 23
    //   433: iconst_0
    //   434: aload 10
    //   436: aastore
    //   437: goto -383 -> 54
    //   440: aload 6
    //   442: iload 9
    //   444: aaload
    //   445: astore 23
    //   447: aload 23
    //   449: ifnull +162 -> 611
    //   452: aload 4
    //   454: getfield 144	android/media/MediaCodec$BufferInfo:flags	I
    //   457: iconst_2
    //   458: iand
    //   459: istore 12
    //   461: iload 12
    //   463: ifeq +9 -> 472
    //   466: aload 4
    //   468: iconst_0
    //   469: putfield 147	android/media/MediaCodec$BufferInfo:size	I
    //   472: aload 4
    //   474: getfield 147	android/media/MediaCodec$BufferInfo:size	I
    //   477: istore 12
    //   479: iload 12
    //   481: ifeq +89 -> 570
    //   484: aload 4
    //   486: getfield 150	android/media/MediaCodec$BufferInfo:offset	I
    //   489: istore 12
    //   491: aload 23
    //   493: iload 12
    //   495: invokevirtual 154	java/nio/ByteBuffer:position	(I)Ljava/nio/Buffer;
    //   498: pop
    //   499: aload 4
    //   501: getfield 150	android/media/MediaCodec$BufferInfo:offset	I
    //   504: istore 12
    //   506: aload 4
    //   508: getfield 147	android/media/MediaCodec$BufferInfo:size	I
    //   511: istore 22
    //   513: iload 12
    //   515: iload 22
    //   517: iadd
    //   518: istore 12
    //   520: aload 23
    //   522: iload 12
    //   524: invokevirtual 157	java/nio/ByteBuffer:limit	(I)Ljava/nio/Buffer;
    //   527: pop
    //   528: aload_1
    //   529: getfield 22	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:this$0	Lcom/nll/nativelibs/callrecording/AACCallRecorder;
    //   532: astore 24
    //   534: aload 24
    //   536: getfield 65	com/nll/nativelibs/callrecording/AACCallRecorder:mRecord	Z
    //   539: istore 12
    //   541: iload 12
    //   543: ifeq +27 -> 570
    //   546: aload_1
    //   547: getfield 22	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:this$0	Lcom/nll/nativelibs/callrecording/AACCallRecorder;
    //   550: astore 24
    //   552: aload 24
    //   554: getfield 122	com/nll/nativelibs/callrecording/AACCallRecorder:mMuxer	Landroid/media/MediaMuxer;
    //   557: astore 24
    //   559: aload 24
    //   561: iload 7
    //   563: aload 23
    //   565: aload 4
    //   567: invokevirtual 161	android/media/MediaMuxer:writeSampleData	(ILjava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V
    //   570: aload_1
    //   571: getfield 33	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:mEncoder	Landroid/media/MediaCodec;
    //   574: astore 23
    //   576: aload 23
    //   578: iload 9
    //   580: iconst_0
    //   581: invokevirtual 165	android/media/MediaCodec:releaseOutputBuffer	(IZ)V
    //   584: aload 4
    //   586: getfield 144	android/media/MediaCodec$BufferInfo:flags	I
    //   589: iconst_4
    //   590: iand
    //   591: istore 9
    //   593: iload 9
    //   595: ifeq -541 -> 54
    //   598: ldc -89
    //   600: astore_3
    //   601: iconst_1
    //   602: anewarray 44	java/lang/String
    //   605: iconst_0
    //   606: aload_3
    //   607: aastore
    //   608: goto +68 -> 676
    //   611: new 169	java/lang/IllegalStateException
    //   614: astore_3
    //   615: new 171	java/lang/StringBuilder
    //   618: astore 4
    //   620: ldc -83
    //   622: astore 5
    //   624: aload 4
    //   626: aload 5
    //   628: invokespecial 176	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   631: aload 4
    //   633: iload 9
    //   635: invokevirtual 180	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   638: pop
    //   639: ldc -74
    //   641: astore 5
    //   643: aload 4
    //   645: aload 5
    //   647: invokevirtual 185	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   650: pop
    //   651: aload 4
    //   653: invokevirtual 189	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   656: astore 4
    //   658: aload_3
    //   659: aload 4
    //   661: invokespecial 190	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
    //   664: aload_3
    //   665: athrow
    //   666: ldc -64
    //   668: astore_3
    //   669: iconst_1
    //   670: anewarray 44	java/lang/String
    //   673: iconst_0
    //   674: aload_3
    //   675: aastore
    //   676: aload_1
    //   677: getfield 31	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:mAudioRecorder	Landroid/media/AudioRecord;
    //   680: astore_3
    //   681: aload_3
    //   682: invokevirtual 195	android/media/AudioRecord:stop	()V
    //   685: aload_1
    //   686: getfield 31	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:mAudioRecorder	Landroid/media/AudioRecord;
    //   689: astore_3
    //   690: aload_3
    //   691: invokevirtual 198	android/media/AudioRecord:release	()V
    //   694: goto +8 -> 702
    //   697: astore_3
    //   698: aload_3
    //   699: invokestatic 204	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   702: aload_1
    //   703: getfield 33	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:mEncoder	Landroid/media/MediaCodec;
    //   706: astore_3
    //   707: aload_3
    //   708: invokevirtual 205	android/media/MediaCodec:stop	()V
    //   711: aload_1
    //   712: getfield 33	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:mEncoder	Landroid/media/MediaCodec;
    //   715: astore_3
    //   716: aload_3
    //   717: invokevirtual 206	android/media/MediaCodec:release	()V
    //   720: goto +8 -> 728
    //   723: astore_3
    //   724: aload_3
    //   725: invokestatic 204	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   728: aload_1
    //   729: getfield 22	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:this$0	Lcom/nll/nativelibs/callrecording/AACCallRecorder;
    //   732: astore_3
    //   733: aload_3
    //   734: getfield 122	com/nll/nativelibs/callrecording/AACCallRecorder:mMuxer	Landroid/media/MediaMuxer;
    //   737: astore_3
    //   738: aload_3
    //   739: ifnull +45 -> 784
    //   742: aload_1
    //   743: getfield 131	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:isMuxerStarted	Z
    //   746: istore_2
    //   747: iload_2
    //   748: ifeq +22 -> 770
    //   751: aload_1
    //   752: getfield 22	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:this$0	Lcom/nll/nativelibs/callrecording/AACCallRecorder;
    //   755: astore_3
    //   756: aload_3
    //   757: getfield 122	com/nll/nativelibs/callrecording/AACCallRecorder:mMuxer	Landroid/media/MediaMuxer;
    //   760: astore_3
    //   761: aload_3
    //   762: invokevirtual 207	android/media/MediaMuxer:stop	()V
    //   765: aload_1
    //   766: iconst_0
    //   767: putfield 131	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:isMuxerStarted	Z
    //   770: aload_1
    //   771: getfield 22	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:this$0	Lcom/nll/nativelibs/callrecording/AACCallRecorder;
    //   774: astore_3
    //   775: aload_3
    //   776: getfield 122	com/nll/nativelibs/callrecording/AACCallRecorder:mMuxer	Landroid/media/MediaMuxer;
    //   779: astore_3
    //   780: aload_3
    //   781: invokevirtual 208	android/media/MediaMuxer:release	()V
    //   784: return
    //   785: invokestatic 204	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   788: return
    //   789: astore_3
    //   790: aload_3
    //   791: astore 4
    //   793: goto +133 -> 926
    //   796: astore_3
    //   797: aload_3
    //   798: invokestatic 204	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   801: aload_1
    //   802: getfield 22	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:this$0	Lcom/nll/nativelibs/callrecording/AACCallRecorder;
    //   805: astore 4
    //   807: aload 4
    //   809: aload_3
    //   810: invokevirtual 212	com/nll/nativelibs/callrecording/AACCallRecorder:onError	(Ljava/lang/Exception;)V
    //   813: aload_1
    //   814: getfield 31	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:mAudioRecorder	Landroid/media/AudioRecord;
    //   817: astore_3
    //   818: aload_3
    //   819: invokevirtual 195	android/media/AudioRecord:stop	()V
    //   822: aload_1
    //   823: getfield 31	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:mAudioRecorder	Landroid/media/AudioRecord;
    //   826: astore_3
    //   827: aload_3
    //   828: invokevirtual 198	android/media/AudioRecord:release	()V
    //   831: goto +8 -> 839
    //   834: astore_3
    //   835: aload_3
    //   836: invokestatic 204	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   839: aload_1
    //   840: getfield 33	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:mEncoder	Landroid/media/MediaCodec;
    //   843: astore_3
    //   844: aload_3
    //   845: invokevirtual 205	android/media/MediaCodec:stop	()V
    //   848: aload_1
    //   849: getfield 33	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:mEncoder	Landroid/media/MediaCodec;
    //   852: astore_3
    //   853: aload_3
    //   854: invokevirtual 206	android/media/MediaCodec:release	()V
    //   857: goto +8 -> 865
    //   860: astore_3
    //   861: aload_3
    //   862: invokestatic 204	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   865: aload_1
    //   866: getfield 22	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:this$0	Lcom/nll/nativelibs/callrecording/AACCallRecorder;
    //   869: astore_3
    //   870: aload_3
    //   871: getfield 122	com/nll/nativelibs/callrecording/AACCallRecorder:mMuxer	Landroid/media/MediaMuxer;
    //   874: astore_3
    //   875: aload_3
    //   876: ifnull +45 -> 921
    //   879: aload_1
    //   880: getfield 131	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:isMuxerStarted	Z
    //   883: istore_2
    //   884: iload_2
    //   885: ifeq +22 -> 907
    //   888: aload_1
    //   889: getfield 22	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:this$0	Lcom/nll/nativelibs/callrecording/AACCallRecorder;
    //   892: astore_3
    //   893: aload_3
    //   894: getfield 122	com/nll/nativelibs/callrecording/AACCallRecorder:mMuxer	Landroid/media/MediaMuxer;
    //   897: astore_3
    //   898: aload_3
    //   899: invokevirtual 207	android/media/MediaMuxer:stop	()V
    //   902: aload_1
    //   903: iconst_0
    //   904: putfield 131	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:isMuxerStarted	Z
    //   907: aload_1
    //   908: getfield 22	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:this$0	Lcom/nll/nativelibs/callrecording/AACCallRecorder;
    //   911: astore_3
    //   912: aload_3
    //   913: getfield 122	com/nll/nativelibs/callrecording/AACCallRecorder:mMuxer	Landroid/media/MediaMuxer;
    //   916: astore_3
    //   917: aload_3
    //   918: invokevirtual 208	android/media/MediaMuxer:release	()V
    //   921: return
    //   922: invokestatic 204	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   925: return
    //   926: aload_1
    //   927: getfield 31	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:mAudioRecorder	Landroid/media/AudioRecord;
    //   930: astore_3
    //   931: aload_3
    //   932: invokevirtual 195	android/media/AudioRecord:stop	()V
    //   935: aload_1
    //   936: getfield 31	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:mAudioRecorder	Landroid/media/AudioRecord;
    //   939: astore_3
    //   940: aload_3
    //   941: invokevirtual 198	android/media/AudioRecord:release	()V
    //   944: goto +8 -> 952
    //   947: astore_3
    //   948: aload_3
    //   949: invokestatic 204	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   952: aload_1
    //   953: getfield 33	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:mEncoder	Landroid/media/MediaCodec;
    //   956: astore_3
    //   957: aload_3
    //   958: invokevirtual 205	android/media/MediaCodec:stop	()V
    //   961: aload_1
    //   962: getfield 33	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:mEncoder	Landroid/media/MediaCodec;
    //   965: astore_3
    //   966: aload_3
    //   967: invokevirtual 206	android/media/MediaCodec:release	()V
    //   970: goto +8 -> 978
    //   973: astore_3
    //   974: aload_3
    //   975: invokestatic 204	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   978: aload_1
    //   979: getfield 22	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:this$0	Lcom/nll/nativelibs/callrecording/AACCallRecorder;
    //   982: astore_3
    //   983: aload_3
    //   984: getfield 122	com/nll/nativelibs/callrecording/AACCallRecorder:mMuxer	Landroid/media/MediaMuxer;
    //   987: astore_3
    //   988: aload_3
    //   989: ifnull +53 -> 1042
    //   992: aload_1
    //   993: getfield 131	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:isMuxerStarted	Z
    //   996: istore_2
    //   997: iload_2
    //   998: ifeq +22 -> 1020
    //   1001: aload_1
    //   1002: getfield 22	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:this$0	Lcom/nll/nativelibs/callrecording/AACCallRecorder;
    //   1005: astore_3
    //   1006: aload_3
    //   1007: getfield 122	com/nll/nativelibs/callrecording/AACCallRecorder:mMuxer	Landroid/media/MediaMuxer;
    //   1010: astore_3
    //   1011: aload_3
    //   1012: invokevirtual 207	android/media/MediaMuxer:stop	()V
    //   1015: aload_1
    //   1016: iconst_0
    //   1017: putfield 131	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:isMuxerStarted	Z
    //   1020: aload_1
    //   1021: getfield 22	com/nll/nativelibs/callrecording/AACCallRecorder$EncodingThread:this$0	Lcom/nll/nativelibs/callrecording/AACCallRecorder;
    //   1024: astore_3
    //   1025: aload_3
    //   1026: getfield 122	com/nll/nativelibs/callrecording/AACCallRecorder:mMuxer	Landroid/media/MediaMuxer;
    //   1029: astore_3
    //   1030: aload_3
    //   1031: invokevirtual 208	android/media/MediaMuxer:release	()V
    //   1034: goto +8 -> 1042
    //   1037: astore_3
    //   1038: aload_3
    //   1039: invokestatic 204	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   1042: aload 4
    //   1044: athrow
    //   1045: pop
    //   1046: goto -370 -> 676
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1049	0	this	EncodingThread
    //   1	1020	1	localEncodingThread	EncodingThread
    //   6	2	2	i	int
    //   746	252	2	bool1	boolean
    //   10	681	3	localObject1	Object
    //   697	2	3	localException1	Exception
    //   706	11	3	localMediaCodec1	MediaCodec
    //   723	2	3	localException2	Exception
    //   732	49	3	localObject2	Object
    //   789	2	3	localObject3	Object
    //   796	14	3	localException3	Exception
    //   817	11	3	localAudioRecord	AudioRecord
    //   834	2	3	localException4	Exception
    //   843	11	3	localMediaCodec2	MediaCodec
    //   860	2	3	localException5	Exception
    //   869	72	3	localObject4	Object
    //   947	2	3	localException6	Exception
    //   956	11	3	localMediaCodec3	MediaCodec
    //   973	2	3	localException7	Exception
    //   982	49	3	localObject5	Object
    //   1037	2	3	localException8	Exception
    //   15	1028	4	localObject6	Object
    //   33	613	5	localObject7	Object
    //   39	402	6	localObject8	Object
    //   49	513	7	j	int
    //   52	314	8	localObject9	Object
    //   58	3	9	bool2	boolean
    //   84	22	9	k	int
    //   122	3	9	bool3	boolean
    //   292	342	9	m	int
    //   69	366	10	localObject10	Object
    //   73	262	11	n	int
    //   99	424	12	i1	int
    //   539	3	12	bool4	boolean
    //   155	133	13	l1	long
    //   164	98	15	i2	int
    //   198	29	16	l2	long
    //   215	43	18	l3	long
    //   243	17	20	localMediaCodec4	MediaCodec
    //   247	18	21	i3	int
    //   295	223	22	i4	int
    //   409	168	23	localObject11	Object
    //   413	147	24	localObject12	Object
    //   785	1	43	localException9	Exception
    //   922	1	44	localException10	Exception
    // Exception table:
    //   from	to	target	type
    //   676	680	697	java/lang/Exception
    //   681	685	697	java/lang/Exception
    //   685	689	697	java/lang/Exception
    //   690	694	697	java/lang/Exception
    //   702	706	723	java/lang/Exception
    //   707	711	723	java/lang/Exception
    //   711	715	723	java/lang/Exception
    //   716	720	723	java/lang/Exception
    //   728	732	785	java/lang/Exception
    //   733	737	785	java/lang/Exception
    //   742	746	785	java/lang/Exception
    //   751	755	785	java/lang/Exception
    //   756	760	785	java/lang/Exception
    //   761	765	785	java/lang/Exception
    //   766	770	785	java/lang/Exception
    //   770	774	785	java/lang/Exception
    //   775	779	785	java/lang/Exception
    //   780	784	785	java/lang/Exception
    //   2	6	789	finally
    //   7	10	789	finally
    //   11	15	789	finally
    //   17	22	789	finally
    //   22	25	789	finally
    //   27	32	789	finally
    //   35	39	789	finally
    //   41	46	789	finally
    //   54	58	789	finally
    //   65	69	789	finally
    //   71	73	789	finally
    //   79	84	789	finally
    //   111	115	789	finally
    //   117	122	789	finally
    //   272	276	789	finally
    //   287	292	789	finally
    //   311	315	789	finally
    //   317	322	789	finally
    //   337	341	789	finally
    //   343	348	789	finally
    //   350	354	789	finally
    //   356	361	789	finally
    //   365	370	789	finally
    //   372	376	789	finally
    //   378	383	789	finally
    //   385	390	789	finally
    //   391	396	789	finally
    //   404	409	789	finally
    //   415	420	789	finally
    //   424	429	789	finally
    //   434	437	789	finally
    //   442	445	789	finally
    //   452	457	789	finally
    //   468	472	789	finally
    //   472	477	789	finally
    //   484	489	789	finally
    //   493	499	789	finally
    //   499	504	789	finally
    //   506	511	789	finally
    //   522	528	789	finally
    //   528	532	789	finally
    //   534	539	789	finally
    //   546	550	789	finally
    //   552	557	789	finally
    //   565	570	789	finally
    //   570	574	789	finally
    //   580	584	789	finally
    //   584	589	789	finally
    //   601	608	789	finally
    //   611	614	789	finally
    //   615	618	789	finally
    //   626	631	789	finally
    //   633	639	789	finally
    //   645	651	789	finally
    //   651	656	789	finally
    //   659	664	789	finally
    //   664	666	789	finally
    //   669	676	789	finally
    //   797	801	789	finally
    //   801	805	789	finally
    //   809	813	789	finally
    //   2	6	796	java/lang/Exception
    //   7	10	796	java/lang/Exception
    //   11	15	796	java/lang/Exception
    //   17	22	796	java/lang/Exception
    //   22	25	796	java/lang/Exception
    //   27	32	796	java/lang/Exception
    //   35	39	796	java/lang/Exception
    //   41	46	796	java/lang/Exception
    //   54	58	796	java/lang/Exception
    //   65	69	796	java/lang/Exception
    //   71	73	796	java/lang/Exception
    //   79	84	796	java/lang/Exception
    //   111	115	796	java/lang/Exception
    //   117	122	796	java/lang/Exception
    //   272	276	796	java/lang/Exception
    //   287	292	796	java/lang/Exception
    //   311	315	796	java/lang/Exception
    //   317	322	796	java/lang/Exception
    //   337	341	796	java/lang/Exception
    //   343	348	796	java/lang/Exception
    //   350	354	796	java/lang/Exception
    //   356	361	796	java/lang/Exception
    //   365	370	796	java/lang/Exception
    //   372	376	796	java/lang/Exception
    //   378	383	796	java/lang/Exception
    //   385	390	796	java/lang/Exception
    //   391	396	796	java/lang/Exception
    //   404	409	796	java/lang/Exception
    //   415	420	796	java/lang/Exception
    //   424	429	796	java/lang/Exception
    //   434	437	796	java/lang/Exception
    //   442	445	796	java/lang/Exception
    //   452	457	796	java/lang/Exception
    //   468	472	796	java/lang/Exception
    //   472	477	796	java/lang/Exception
    //   484	489	796	java/lang/Exception
    //   493	499	796	java/lang/Exception
    //   499	504	796	java/lang/Exception
    //   506	511	796	java/lang/Exception
    //   522	528	796	java/lang/Exception
    //   528	532	796	java/lang/Exception
    //   534	539	796	java/lang/Exception
    //   546	550	796	java/lang/Exception
    //   552	557	796	java/lang/Exception
    //   565	570	796	java/lang/Exception
    //   570	574	796	java/lang/Exception
    //   580	584	796	java/lang/Exception
    //   584	589	796	java/lang/Exception
    //   601	608	796	java/lang/Exception
    //   611	614	796	java/lang/Exception
    //   615	618	796	java/lang/Exception
    //   626	631	796	java/lang/Exception
    //   633	639	796	java/lang/Exception
    //   645	651	796	java/lang/Exception
    //   651	656	796	java/lang/Exception
    //   659	664	796	java/lang/Exception
    //   664	666	796	java/lang/Exception
    //   669	676	796	java/lang/Exception
    //   813	817	834	java/lang/Exception
    //   818	822	834	java/lang/Exception
    //   822	826	834	java/lang/Exception
    //   827	831	834	java/lang/Exception
    //   839	843	860	java/lang/Exception
    //   844	848	860	java/lang/Exception
    //   848	852	860	java/lang/Exception
    //   853	857	860	java/lang/Exception
    //   865	869	922	java/lang/Exception
    //   870	874	922	java/lang/Exception
    //   879	883	922	java/lang/Exception
    //   888	892	922	java/lang/Exception
    //   893	897	922	java/lang/Exception
    //   898	902	922	java/lang/Exception
    //   903	907	922	java/lang/Exception
    //   907	911	922	java/lang/Exception
    //   912	916	922	java/lang/Exception
    //   917	921	922	java/lang/Exception
    //   926	930	947	java/lang/Exception
    //   931	935	947	java/lang/Exception
    //   935	939	947	java/lang/Exception
    //   940	944	947	java/lang/Exception
    //   952	956	973	java/lang/Exception
    //   957	961	973	java/lang/Exception
    //   961	965	973	java/lang/Exception
    //   966	970	973	java/lang/Exception
    //   978	982	1037	java/lang/Exception
    //   983	987	1037	java/lang/Exception
    //   992	996	1037	java/lang/Exception
    //   1001	1005	1037	java/lang/Exception
    //   1006	1010	1037	java/lang/Exception
    //   1011	1015	1037	java/lang/Exception
    //   1016	1020	1037	java/lang/Exception
    //   1020	1024	1037	java/lang/Exception
    //   1025	1029	1037	java/lang/Exception
    //   1030	1034	1037	java/lang/Exception
    //   134	138	1045	finally
    //   140	145	1045	finally
    //   147	151	1045	finally
    //   159	164	1045	finally
    //   173	176	1045	finally
    //   178	184	1045	finally
    //   186	191	1045	finally
    //   191	195	1045	finally
    //   207	210	1045	finally
    //   219	222	1045	finally
    //   226	231	1045	finally
    //   234	239	1045	finally
    //   239	243	1045	finally
    //   245	247	1045	finally
    //   249	253	1045	finally
    //   268	272	1045	finally
  }
}

/* Location:
 * Qualified Name:     com.nll.nativelibs.callrecording.AACCallRecorder.EncodingThread
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */