package d;

public abstract class h
  implements t
{
  private final t a;
  
  public h(t paramt)
  {
    if (paramt != null)
    {
      a = paramt;
      return;
    }
    paramt = new java/lang/IllegalArgumentException;
    paramt.<init>("delegate == null");
    throw paramt;
  }
  
  public void a_(c paramc, long paramLong)
  {
    a.a_(paramc, paramLong);
  }
  
  public void close()
  {
    a.close();
  }
  
  public void flush()
  {
    a.flush();
  }
  
  public final v l_()
  {
    return a.l_();
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    String str = getClass().getSimpleName();
    localStringBuilder.append(str);
    localStringBuilder.append("(");
    str = a.toString();
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     d.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */