package d;

import java.io.OutputStream;

final class n$1
  implements t
{
  n$1(v paramv, OutputStream paramOutputStream) {}
  
  public final void a_(c paramc, long paramLong)
  {
    long l1 = b;
    long l2 = 0L;
    w.a(l1, l2, paramLong);
    for (;;)
    {
      l1 = 0L;
      boolean bool = paramLong < l1;
      if (!bool) {
        break;
      }
      a.f();
      q localq = a;
      int j = c;
      int i = b;
      long l3 = j - i;
      i = (int)Math.min(paramLong, l3);
      Object localObject = b;
      byte[] arrayOfByte = a;
      int k = b;
      ((OutputStream)localObject).write(arrayOfByte, k, i);
      j = b + i;
      b = j;
      l3 = i;
      paramLong -= l3;
      long l4 = b - l3;
      b = l4;
      j = b;
      i = c;
      if (j == i)
      {
        localObject = localq.b();
        a = ((q)localObject);
        r.a(localq);
      }
    }
  }
  
  public final void close()
  {
    b.close();
  }
  
  public final void flush()
  {
    b.flush();
  }
  
  public final v l_()
  {
    return a;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("sink(");
    OutputStream localOutputStream = b;
    localStringBuilder.append(localOutputStream);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     d.n.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */