package d;

import java.io.InterruptedIOException;
import java.util.concurrent.TimeUnit;

public class v
{
  public static final v c;
  private boolean a;
  private long b;
  private long d;
  
  static
  {
    v.1 local1 = new d/v$1;
    local1.<init>();
    c = local1;
  }
  
  public v a(long paramLong)
  {
    a = true;
    b = paramLong;
    return this;
  }
  
  public v a(long paramLong, TimeUnit paramTimeUnit)
  {
    long l = 0L;
    boolean bool = paramLong < l;
    if (!bool)
    {
      if (paramTimeUnit != null)
      {
        paramLong = paramTimeUnit.toNanos(paramLong);
        d = paramLong;
        return this;
      }
      localObject = new java/lang/IllegalArgumentException;
      ((IllegalArgumentException)localObject).<init>("unit == null");
      throw ((Throwable)localObject);
    }
    paramTimeUnit = new java/lang/IllegalArgumentException;
    Object localObject = String.valueOf(paramLong);
    localObject = "timeout < 0: ".concat((String)localObject);
    paramTimeUnit.<init>((String)localObject);
    throw paramTimeUnit;
  }
  
  public long c()
  {
    boolean bool = a;
    if (bool) {
      return b;
    }
    IllegalStateException localIllegalStateException = new java/lang/IllegalStateException;
    localIllegalStateException.<init>("No deadline");
    throw localIllegalStateException;
  }
  
  public v d()
  {
    d = 0L;
    return this;
  }
  
  public void f()
  {
    boolean bool1 = Thread.interrupted();
    if (!bool1)
    {
      bool1 = a;
      if (bool1)
      {
        long l1 = b;
        long l2 = System.nanoTime();
        l1 -= l2;
        l2 = 0L;
        boolean bool2 = l1 < l2;
        if (!bool2)
        {
          localInterruptedIOException = new java/io/InterruptedIOException;
          localInterruptedIOException.<init>("deadline reached");
          throw localInterruptedIOException;
        }
      }
      return;
    }
    Thread.currentThread().interrupt();
    InterruptedIOException localInterruptedIOException = new java/io/InterruptedIOException;
    localInterruptedIOException.<init>("interrupted");
    throw localInterruptedIOException;
  }
  
  public long o_()
  {
    return d;
  }
  
  public boolean p_()
  {
    return a;
  }
  
  public v q_()
  {
    a = false;
    return this;
  }
}

/* Location:
 * Qualified Name:     d.v
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */