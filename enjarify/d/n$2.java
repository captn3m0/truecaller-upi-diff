package d;

import java.io.IOException;
import java.io.InputStream;

final class n$2
  implements u
{
  n$2(v paramv, InputStream paramInputStream) {}
  
  public final long a(c paramc, long paramLong)
  {
    long l1 = 0L;
    boolean bool1 = paramLong < l1;
    if (!bool1)
    {
      bool1 = paramLong < l1;
      if (!bool1) {
        return l1;
      }
      try
      {
        Object localObject1 = a;
        ((v)localObject1).f();
        int j = 1;
        localObject1 = paramc.f(j);
        int k = c;
        k = 8192 - k;
        long l2 = k;
        paramLong = Math.min(paramLong, l2);
        int m = (int)paramLong;
        localObject2 = b;
        byte[] arrayOfByte = a;
        int i = c;
        int n = ((InputStream)localObject2).read(arrayOfByte, i, m);
        m = -1;
        if (n == m) {
          return -1;
        }
        m = c + n;
        c = m;
        l1 = b;
        paramLong = n;
        l1 += paramLong;
        b = l1;
        return paramLong;
      }
      catch (AssertionError paramc)
      {
        boolean bool2 = n.a(paramc);
        if (bool2)
        {
          localObject2 = new java/io/IOException;
          ((IOException)localObject2).<init>(paramc);
          throw ((Throwable)localObject2);
        }
        throw paramc;
      }
    }
    paramc = new java/lang/IllegalArgumentException;
    Object localObject2 = String.valueOf(paramLong);
    localObject2 = "byteCount < 0: ".concat((String)localObject2);
    paramc.<init>((String)localObject2);
    throw paramc;
  }
  
  public final void close()
  {
    b.close();
  }
  
  public final v l_()
  {
    return a;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("source(");
    InputStream localInputStream = b;
    localStringBuilder.append(localInputStream);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     d.n.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */