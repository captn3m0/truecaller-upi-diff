package d;

import java.io.EOFException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.nio.charset.Charset;

public final class c
  implements d, e, Cloneable, ByteChannel
{
  private static final byte[] c;
  q a;
  public long b;
  
  static
  {
    byte[] arrayOfByte = new byte[16];
    arrayOfByte[0] = 48;
    arrayOfByte[1] = 49;
    arrayOfByte[2] = 50;
    arrayOfByte[3] = 51;
    arrayOfByte[4] = 52;
    arrayOfByte[5] = 53;
    arrayOfByte[6] = 54;
    arrayOfByte[7] = 55;
    arrayOfByte[8] = 56;
    arrayOfByte[9] = 57;
    arrayOfByte[10] = 97;
    arrayOfByte[11] = 98;
    arrayOfByte[12] = 99;
    arrayOfByte[13] = 100;
    arrayOfByte[14] = 101;
    arrayOfByte[15] = 102;
    c = arrayOfByte;
  }
  
  private String a(long paramLong, Charset paramCharset)
  {
    long l1 = b;
    long l2 = 0L;
    w.a(l1, l2, paramLong);
    if (paramCharset != null)
    {
      l1 = 2147483647L;
      boolean bool1 = paramLong < l1;
      if (!bool1)
      {
        l1 = 0L;
        bool1 = paramLong < l1;
        if (!bool1) {
          return "";
        }
        Object localObject1 = a;
        int i = b;
        long l3 = i + paramLong;
        int j = c;
        long l4 = j;
        boolean bool2 = l3 < l4;
        if (bool2)
        {
          localObject1 = new java/lang/String;
          localObject2 = g(paramLong);
          ((String)localObject1).<init>((byte[])localObject2, paramCharset);
          return (String)localObject1;
        }
        String str = new java/lang/String;
        byte[] arrayOfByte = a;
        j = b;
        int k = (int)paramLong;
        str.<init>(arrayOfByte, j, k, paramCharset);
        int m = (int)(b + paramLong);
        b = m;
        l2 = b - paramLong;
        b = l2;
        int n = b;
        int i1 = c;
        if (n == i1)
        {
          localObject2 = ((q)localObject1).b();
          a = ((q)localObject2);
          r.a((q)localObject1);
        }
        return str;
      }
      paramCharset = new java/lang/IllegalArgumentException;
      localObject2 = String.valueOf(paramLong);
      localObject2 = "byteCount > Integer.MAX_VALUE: ".concat((String)localObject2);
      paramCharset.<init>((String)localObject2);
      throw paramCharset;
    }
    Object localObject2 = new java/lang/IllegalArgumentException;
    ((IllegalArgumentException)localObject2).<init>("charset == null");
    throw ((Throwable)localObject2);
  }
  
  private String m(long paramLong)
  {
    Charset localCharset = w.a;
    return a(paramLong, localCharset);
  }
  
  public final int a(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    int i = paramArrayOfByte.length;
    long l1 = i;
    long l2 = paramInt1;
    long l3 = paramInt2;
    w.a(l1, l2, l3);
    q localq = a;
    if (localq == null) {
      return -1;
    }
    int j = c;
    int k = b;
    j -= k;
    paramInt2 = Math.min(paramInt2, j);
    byte[] arrayOfByte = a;
    k = b;
    System.arraycopy(arrayOfByte, k, paramArrayOfByte, paramInt1, paramInt2);
    int m = b + paramInt2;
    b = m;
    long l4 = b;
    l1 = paramInt2;
    l4 -= l1;
    b = l4;
    m = b;
    paramInt1 = c;
    if (m == paramInt1)
    {
      paramArrayOfByte = localq.b();
      a = paramArrayOfByte;
      r.a(localq);
    }
    return paramInt2;
  }
  
  public final long a(byte paramByte, long paramLong1, long paramLong2)
  {
    c localc = this;
    long l1 = 0L;
    boolean bool1 = paramLong1 < l1;
    if (!bool1)
    {
      bool1 = paramLong2 < paramLong1;
      if (!bool1)
      {
        long l2 = b;
        boolean bool2 = paramLong2 < l2;
        if (!bool2) {
          l2 = paramLong2;
        }
        long l3 = -1;
        boolean bool3 = paramLong1 < l2;
        if (!bool3) {
          return l3;
        }
        q localq = a;
        if (localq == null) {
          return l3;
        }
        long l4 = b;
        long l5 = l4 - paramLong1;
        boolean bool4 = l5 < paramLong1;
        byte b1;
        byte b2;
        boolean bool6;
        if (bool4)
        {
          for (;;)
          {
            boolean bool5 = l4 < paramLong1;
            if (!bool5) {
              break;
            }
            localq = g;
            b1 = c;
            b2 = b;
            b1 -= b2;
            l1 = b1;
            l4 -= l1;
          }
          l1 = paramLong1;
        }
        else
        {
          for (l4 = l1;; l4 = l1)
          {
            b1 = c;
            b2 = b;
            b1 -= b2;
            l1 = b1 + l4;
            bool6 = l1 < paramLong1;
            if (!bool6) {
              break;
            }
            localq = f;
          }
          l1 = paramLong1;
        }
        for (;;)
        {
          bool6 = l4 < l2;
          if (!bool6) {
            break;
          }
          byte[] arrayOfByte = a;
          long l6 = c;
          int j = b;
          long l7 = j + l2 - l4;
          l6 = Math.min(l6, l7);
          int i = (int)l6;
          int k = b;
          l7 = k + l1 - l4;
          b1 = (int)l7;
          while (b1 < i)
          {
            b2 = arrayOfByte[b1];
            k = paramByte;
            if (b2 == paramByte)
            {
              b2 = b;
              return b1 - b2 + l4;
            }
            b1 += 1;
          }
          k = paramByte;
          b1 = c;
          b2 = b;
          b1 -= b2;
          l1 = b1 + l4;
          localq = f;
          l4 = l1;
        }
        return l3;
      }
    }
    IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
    Object localObject = new Object[3];
    Long localLong1 = Long.valueOf(b);
    localObject[0] = localLong1;
    Long localLong2 = Long.valueOf(paramLong1);
    localObject[1] = localLong2;
    localLong1 = Long.valueOf(paramLong2);
    localObject[2] = localLong1;
    localObject = String.format("size=%s fromIndex=%s toIndex=%s", (Object[])localObject);
    localIllegalArgumentException.<init>((String)localObject);
    throw localIllegalArgumentException;
  }
  
  public final long a(c paramc, long paramLong)
  {
    if (paramc != null)
    {
      long l1 = 0L;
      boolean bool1 = paramLong < l1;
      if (!bool1)
      {
        long l2 = b;
        boolean bool2 = l2 < l1;
        if (!bool2) {
          return -1;
        }
        boolean bool3 = paramLong < l2;
        if (bool3) {
          paramLong = l2;
        }
        paramc.a_(this, paramLong);
        return paramLong;
      }
      paramc = new java/lang/IllegalArgumentException;
      String str = String.valueOf(paramLong);
      str = "byteCount < 0: ".concat(str);
      paramc.<init>(str);
      throw paramc;
    }
    paramc = new java/lang/IllegalArgumentException;
    paramc.<init>("sink == null");
    throw paramc;
  }
  
  public final long a(t paramt)
  {
    long l1 = b;
    long l2 = 0L;
    boolean bool = l1 < l2;
    if (bool) {
      paramt.a_(this, l1);
    }
    return l1;
  }
  
  public final long a(u paramu)
  {
    if (paramu != null)
    {
      long l2;
      for (long l1 = 0L;; l1 += l2)
      {
        l2 = paramu.a(this, 8192L);
        long l3 = -1;
        boolean bool = l2 < l3;
        if (!bool) {
          break;
        }
      }
      return l1;
    }
    paramu = new java/lang/IllegalArgumentException;
    paramu.<init>("source == null");
    throw paramu;
  }
  
  public final c a(int paramInt)
  {
    int i = 128;
    if (paramInt < i)
    {
      b(paramInt);
    }
    else
    {
      int j = 2048;
      int k = 63;
      if (paramInt < j)
      {
        j = paramInt >> 6 | 0xC0;
        b(j);
        paramInt = paramInt & k | i;
        b(paramInt);
      }
      else
      {
        j = 65536;
        if (paramInt < j)
        {
          j = 55296;
          if (paramInt >= j)
          {
            j = 57343;
            if (paramInt <= j)
            {
              b(k);
              break label211;
            }
          }
          j = paramInt >> 12 | 0xE0;
          b(j);
          j = paramInt >> 6 & k | i;
          b(j);
          paramInt = paramInt & k | i;
          b(paramInt);
        }
        else
        {
          j = 1114111;
          if (paramInt > j) {
            break label213;
          }
          j = paramInt >> 18 | 0xF0;
          b(j);
          j = paramInt >> 12 & k | i;
          b(j);
          j = paramInt >> 6 & k | i;
          b(j);
          paramInt = paramInt & k | i;
          b(paramInt);
        }
      }
    }
    label211:
    return this;
    label213:
    IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Unexpected code point: ");
    String str = Integer.toHexString(paramInt);
    localStringBuilder.append(str);
    str = localStringBuilder.toString();
    localIllegalArgumentException.<init>(str);
    throw localIllegalArgumentException;
  }
  
  public final c a(c paramc, long paramLong1, long paramLong2)
  {
    if (paramc != null)
    {
      long l1 = b;
      long l2 = paramLong1;
      long l3 = paramLong2;
      w.a(l1, paramLong1, paramLong2);
      l1 = 0L;
      boolean bool1 = paramLong2 < l1;
      if (!bool1) {
        return this;
      }
      l2 = b + paramLong2;
      b = l2;
      int j;
      for (q localq1 = a;; localq1 = f)
      {
        int i = c;
        j = b;
        i -= j;
        long l4 = i;
        boolean bool3 = paramLong1 < l4;
        if (bool3) {
          break;
        }
        i = c;
        j = b;
        i -= j;
        l4 = i;
        paramLong1 -= l4;
      }
      for (;;)
      {
        boolean bool2 = paramLong2 < l1;
        if (!bool2) {
          break;
        }
        q localq2 = localq1.a();
        j = b;
        l3 = j + paramLong1;
        int k = (int)l3;
        b = k;
        k = b;
        int m = (int)paramLong2;
        k += m;
        m = c;
        k = Math.min(k, m);
        c = k;
        q localq3 = a;
        if (localq3 == null)
        {
          g = localq2;
          f = localq2;
          a = localq2;
        }
        else
        {
          localq3 = g;
          localq3.a(localq2);
        }
        k = c;
        m = b;
        k -= m;
        paramLong1 = k;
        paramLong2 -= paramLong1;
        localq1 = f;
        paramLong1 = l1;
      }
      return this;
    }
    paramc = new java/lang/IllegalArgumentException;
    paramc.<init>("out == null");
    throw paramc;
  }
  
  public final c a(f paramf)
  {
    if (paramf != null)
    {
      paramf.a(this);
      return this;
    }
    paramf = new java/lang/IllegalArgumentException;
    paramf.<init>("byteString == null");
    throw paramf;
  }
  
  public final c a(String paramString)
  {
    int i = paramString.length();
    return a(paramString, 0, i);
  }
  
  public final c a(String paramString, int paramInt1, int paramInt2)
  {
    if (paramString != null)
    {
      if (paramInt1 >= 0)
      {
        if (paramInt2 >= paramInt1)
        {
          int i = paramString.length();
          if (paramInt2 <= i)
          {
            while (paramInt1 < paramInt2)
            {
              i = paramString.charAt(paramInt1);
              int j = 128;
              int k;
              int m;
              int n;
              int i1;
              if (i < j)
              {
                k = 1;
                q localq = f(k);
                byte[] arrayOfByte = a;
                m = c - paramInt1;
                n = 8192 - m;
                n = Math.min(paramInt2, n);
                i1 = paramInt1 + 1;
                paramInt1 += m;
                i = (byte)i;
                arrayOfByte[paramInt1] = i;
                for (paramInt1 = i1; paramInt1 < n; paramInt1 = i1)
                {
                  i = paramString.charAt(paramInt1);
                  if (i >= j) {
                    break;
                  }
                  i1 = paramInt1 + 1;
                  paramInt1 += m;
                  i = (byte)i;
                  arrayOfByte[paramInt1] = i;
                }
                m += paramInt1;
                i = c;
                m -= i;
                i = c + m;
                c = i;
                long l1 = b;
                long l2 = m;
                l1 += l2;
                b = l1;
              }
              else
              {
                k = 2048;
                if (i < k)
                {
                  k = i >> 6 | 0xC0;
                  b(k);
                  i = i & 0x3F | j;
                  b(i);
                  paramInt1 += 1;
                }
                else
                {
                  k = 55296;
                  int i2 = 63;
                  if (i >= k)
                  {
                    k = 57343;
                    if (i <= k)
                    {
                      m = paramInt1 + 1;
                      if (m < paramInt2) {
                        n = paramString.charAt(m);
                      } else {
                        n = 0;
                      }
                      i1 = 56319;
                      if (i <= i1)
                      {
                        i1 = 56320;
                        if ((n >= i1) && (n <= k))
                        {
                          i = (i & 0xFFFF27FF) << 10;
                          m = 0xFFFF23FF & n;
                          i = (i | m) + 65536;
                          k = i >> 18 | 0xF0;
                          b(k);
                          k = i >> 12 & i2 | j;
                          b(k);
                          k = i >> 6 & i2 | j;
                          b(k);
                          i = i & i2 | j;
                          b(i);
                          paramInt1 += 2;
                          continue;
                        }
                      }
                      b(i2);
                      paramInt1 = m;
                      continue;
                    }
                  }
                  k = i >> 12 | 0xE0;
                  b(k);
                  k = i >> 6 & i2 | j;
                  b(k);
                  i = i & 0x3F | j;
                  b(i);
                  paramInt1 += 1;
                }
              }
            }
            return this;
          }
          localObject = new java/lang/IllegalArgumentException;
          localStringBuilder = new java/lang/StringBuilder;
          localStringBuilder.<init>("endIndex > string.length: ");
          localStringBuilder.append(paramInt2);
          localStringBuilder.append(" > ");
          int i3 = paramString.length();
          localStringBuilder.append(i3);
          paramString = localStringBuilder.toString();
          ((IllegalArgumentException)localObject).<init>(paramString);
          throw ((Throwable)localObject);
        }
        paramString = new java/lang/IllegalArgumentException;
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>("endIndex < beginIndex: ");
        localStringBuilder.append(paramInt2);
        localStringBuilder.append(" < ");
        localStringBuilder.append(paramInt1);
        localObject = localStringBuilder.toString();
        paramString.<init>((String)localObject);
        throw paramString;
      }
      paramString = new java/lang/IllegalArgumentException;
      Object localObject = String.valueOf(paramInt1);
      localObject = "beginIndex < 0: ".concat((String)localObject);
      paramString.<init>((String)localObject);
      throw paramString;
    }
    paramString = new java/lang/IllegalArgumentException;
    paramString.<init>("string == null");
    throw paramString;
  }
  
  public final c a(String paramString, int paramInt1, int paramInt2, Charset paramCharset)
  {
    if (paramString != null)
    {
      if (paramInt1 >= 0)
      {
        if (paramInt2 >= paramInt1)
        {
          int i = paramString.length();
          if (paramInt2 <= i)
          {
            if (paramCharset != null)
            {
              Charset localCharset = w.a;
              boolean bool = paramCharset.equals(localCharset);
              if (bool) {
                return a(paramString, paramInt1, paramInt2);
              }
              paramString = paramString.substring(paramInt1, paramInt2).getBytes(paramCharset);
              paramInt2 = paramString.length;
              return b(paramString, 0, paramInt2);
            }
            paramString = new java/lang/IllegalArgumentException;
            paramString.<init>("charset == null");
            throw paramString;
          }
          localObject = new java/lang/IllegalArgumentException;
          paramCharset = new java/lang/StringBuilder;
          paramCharset.<init>("endIndex > string.length: ");
          paramCharset.append(paramInt2);
          paramCharset.append(" > ");
          int j = paramString.length();
          paramCharset.append(j);
          paramString = paramCharset.toString();
          ((IllegalArgumentException)localObject).<init>(paramString);
          throw ((Throwable)localObject);
        }
        paramString = new java/lang/IllegalArgumentException;
        paramCharset = new java/lang/StringBuilder;
        paramCharset.<init>("endIndex < beginIndex: ");
        paramCharset.append(paramInt2);
        paramCharset.append(" < ");
        paramCharset.append(paramInt1);
        localObject = paramCharset.toString();
        paramString.<init>((String)localObject);
        throw paramString;
      }
      paramString = new java/lang/IllegalAccessError;
      Object localObject = String.valueOf(paramInt1);
      localObject = "beginIndex < 0: ".concat((String)localObject);
      paramString.<init>((String)localObject);
      throw paramString;
    }
    paramString = new java/lang/IllegalArgumentException;
    paramString.<init>("string == null");
    throw paramString;
  }
  
  public final String a(Charset paramCharset)
  {
    try
    {
      long l = b;
      return a(l, paramCharset);
    }
    catch (EOFException paramCharset)
    {
      AssertionError localAssertionError = new java/lang/AssertionError;
      localAssertionError.<init>(paramCharset);
      throw localAssertionError;
    }
  }
  
  public final void a(long paramLong)
  {
    long l = b;
    boolean bool = l < paramLong;
    if (!bool) {
      return;
    }
    EOFException localEOFException = new java/io/EOFException;
    localEOFException.<init>();
    throw localEOFException;
  }
  
  public final void a(byte[] paramArrayOfByte)
  {
    int i = 0;
    for (;;)
    {
      int j = paramArrayOfByte.length;
      if (i >= j) {
        return;
      }
      j = paramArrayOfByte.length - i;
      j = a(paramArrayOfByte, i, j);
      int k = -1;
      if (j == k) {
        break;
      }
      i += j;
    }
    paramArrayOfByte = new java/io/EOFException;
    paramArrayOfByte.<init>();
    throw paramArrayOfByte;
  }
  
  public final void a_(c paramc, long paramLong)
  {
    if (paramc != null)
    {
      if (paramc != this)
      {
        long l1 = b;
        long l2 = 0L;
        w.a(l1, l2, paramLong);
        for (;;)
        {
          l1 = 0L;
          boolean bool1 = paramLong < l1;
          if (!bool1) {
            break label723;
          }
          q localq1 = a;
          int j = c;
          q localq2 = a;
          int k = b;
          j -= k;
          l1 = j;
          bool1 = false;
          q localq3 = null;
          boolean bool5 = paramLong < l1;
          int i;
          int i4;
          Object localObject;
          if (bool5)
          {
            localq1 = a;
            if (localq1 != null)
            {
              localq1 = g;
            }
            else
            {
              j = 0;
              localq1 = null;
            }
            if (localq1 != null)
            {
              boolean bool2 = e;
              if (bool2)
              {
                l3 = c + paramLong;
                bool2 = d;
                int m;
                if (bool2)
                {
                  bool2 = false;
                  localq2 = null;
                }
                else
                {
                  m = b;
                }
                long l4 = m;
                l3 -= l4;
                l4 = 8192L;
                boolean bool3 = l3 < l4;
                if (!bool3)
                {
                  localq2 = a;
                  i = (int)paramLong;
                  localq2.a(localq1, i);
                  l1 = b - paramLong;
                  b = l1;
                  l1 = b + paramLong;
                  b = l1;
                  return;
                }
              }
            }
            localq1 = a;
            n = (int)paramLong;
            if (n > 0)
            {
              i2 = c;
              int i3 = b;
              i2 -= i3;
              if (n <= i2)
              {
                i2 = 1024;
                q localq4;
                if (n >= i2)
                {
                  localq4 = localq1.a();
                }
                else
                {
                  localq4 = r.a();
                  byte[] arrayOfByte = a;
                  i4 = b;
                  localObject = a;
                  System.arraycopy(arrayOfByte, i4, localObject, 0, n);
                }
                i3 = b + n;
                c = i3;
                i3 = b + n;
                b = i3;
                localq1 = g;
                localq1.a(localq4);
                a = localq4;
                break label425;
              }
            }
            paramc = new java/lang/IllegalArgumentException;
            paramc.<init>();
            throw paramc;
          }
          label425:
          localq1 = a;
          int n = c;
          int i2 = b;
          n -= i2;
          long l3 = n;
          localq2 = localq1.b();
          a = localq2;
          localq2 = a;
          if (localq2 == null)
          {
            a = localq1;
            localq1 = a;
            g = localq1;
            f = localq1;
          }
          else
          {
            localq1 = g.a(localq1);
            localq2 = g;
            if (localq2 == localq1) {
              break;
            }
            localq2 = g;
            boolean bool4 = e;
            if (bool4)
            {
              int i1 = c;
              i4 = b;
              i1 -= i4;
              q localq5 = g;
              i4 = c;
              i4 = 8192 - i4;
              localObject = g;
              boolean bool6 = d;
              if (!bool6)
              {
                localq3 = g;
                i = b;
              }
              i4 += i;
              if (i1 <= i4)
              {
                localq3 = g;
                localq1.a(localq3, i1);
                localq1.b();
                r.a(localq1);
              }
            }
          }
          l1 = b - l3;
          b = l1;
          l1 = b + l3;
          b = l1;
          paramLong -= l3;
        }
        paramc = new java/lang/IllegalStateException;
        paramc.<init>();
        throw paramc;
        label723:
        return;
      }
      paramc = new java/lang/IllegalArgumentException;
      paramc.<init>("source == this");
      throw paramc;
    }
    paramc = new java/lang/IllegalArgumentException;
    paramc.<init>("source == null");
    throw paramc;
  }
  
  public final c b()
  {
    return this;
  }
  
  public final c b(int paramInt)
  {
    q localq = f(1);
    byte[] arrayOfByte = a;
    int i = c;
    int j = i + 1;
    c = j;
    paramInt = (byte)paramInt;
    arrayOfByte[i] = paramInt;
    long l = b + 1L;
    b = l;
    return this;
  }
  
  public final c b(byte[] paramArrayOfByte)
  {
    if (paramArrayOfByte != null)
    {
      int i = paramArrayOfByte.length;
      return b(paramArrayOfByte, 0, i);
    }
    paramArrayOfByte = new java/lang/IllegalArgumentException;
    paramArrayOfByte.<init>("source == null");
    throw paramArrayOfByte;
  }
  
  public final c b(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    if (paramArrayOfByte != null)
    {
      int i = paramArrayOfByte.length;
      long l1 = i;
      long l2 = paramInt1;
      long l3 = paramInt2;
      w.a(l1, l2, l3);
      paramInt2 += paramInt1;
      while (paramInt1 < paramInt2)
      {
        i = 1;
        q localq = f(i);
        int j = paramInt2 - paramInt1;
        int k = c;
        k = 8192 - k;
        j = Math.min(j, k);
        byte[] arrayOfByte = a;
        int m = c;
        System.arraycopy(paramArrayOfByte, paramInt1, arrayOfByte, m, j);
        paramInt1 += j;
        k = c + j;
        c = k;
      }
      long l4 = b + l3;
      b = l4;
      return this;
    }
    paramArrayOfByte = new java/lang/IllegalArgumentException;
    paramArrayOfByte.<init>("source == null");
    throw paramArrayOfByte;
  }
  
  public final boolean b(long paramLong)
  {
    long l = b;
    boolean bool = l < paramLong;
    return !bool;
  }
  
  public final boolean b(f paramf)
  {
    int i = paramf.h();
    if (i >= 0)
    {
      long l1 = b;
      long l2 = 0L;
      l1 -= l2;
      long l3 = i;
      boolean bool = l1 < l3;
      if (!bool)
      {
        int j = paramf.h() - 0;
        if (j >= i)
        {
          j = 0;
          while (j < i)
          {
            l3 = j + l2;
            int k = c(l3);
            int m = j + 0;
            m = paramf.a(m);
            if (k != m) {
              return false;
            }
            j += 1;
          }
          return true;
        }
      }
    }
    return false;
  }
  
  public final byte c(long paramLong)
  {
    long l1 = b;
    long l2 = 1L;
    long l3 = paramLong;
    w.a(l1, paramLong, l2);
    l1 = b;
    l3 = l1 - paramLong;
    boolean bool1 = l3 < paramLong;
    int i;
    int j;
    long l4;
    boolean bool2;
    if (bool1) {
      for (localq = a;; localq = f)
      {
        i = c;
        j = b;
        i -= j;
        l4 = i;
        bool2 = paramLong < l4;
        if (bool2)
        {
          arrayOfByte = a;
          k = b;
          m = (int)paramLong;
          k += m;
          return arrayOfByte[k];
        }
        paramLong -= l4;
      }
    }
    paramLong -= l1;
    q localq = a;
    do
    {
      localq = g;
      i = c;
      j = b;
      i -= j;
      l4 = i;
      paramLong += l4;
      l4 = 0L;
      bool2 = paramLong < l4;
    } while (bool2);
    byte[] arrayOfByte = a;
    int k = b;
    int m = (int)paramLong;
    k += m;
    return arrayOfByte[k];
  }
  
  public final c c(int paramInt)
  {
    q localq = f(2);
    byte[] arrayOfByte = a;
    int i = c;
    int j = i + 1;
    int k = (byte)(paramInt >>> 8 & 0xFF);
    arrayOfByte[i] = k;
    i = j + 1;
    paramInt = (byte)(paramInt & 0xFF);
    arrayOfByte[j] = paramInt;
    c = i;
    long l = b + 2;
    b = l;
    return this;
  }
  
  public final OutputStream c()
  {
    c.1 local1 = new d/c$1;
    local1.<init>(this);
    return local1;
  }
  
  public final void close() {}
  
  public final c d(int paramInt)
  {
    q localq = f(4);
    byte[] arrayOfByte = a;
    int i = c;
    int j = i + 1;
    int k = (byte)(paramInt >>> 24 & 0xFF);
    arrayOfByte[i] = k;
    i = j + 1;
    k = (byte)(paramInt >>> 16 & 0xFF);
    arrayOfByte[j] = k;
    j = i + 1;
    k = (byte)(paramInt >>> 8 & 0xFF);
    arrayOfByte[i] = k;
    i = j + 1;
    paramInt = (byte)(paramInt & 0xFF);
    arrayOfByte[j] = paramInt;
    c = i;
    long l = b + 4;
    b = l;
    return this;
  }
  
  public final d d()
  {
    return this;
  }
  
  public final f d(long paramLong)
  {
    f localf = new d/f;
    byte[] arrayOfByte = g(paramLong);
    localf.<init>(arrayOfByte);
    return localf;
  }
  
  public final c e(int paramInt)
  {
    paramInt = w.a(paramInt);
    return d(paramInt);
  }
  
  public final String e(long paramLong)
  {
    long l1 = 0L;
    boolean bool1 = paramLong < l1;
    if (!bool1)
    {
      l1 = 1L;
      long l2 = Long.MAX_VALUE;
      boolean bool2 = paramLong < l2;
      if (bool2) {
        l2 = paramLong + l1;
      }
      byte b1 = 10;
      long l3 = a(b1, 0L, l2);
      long l4 = -1;
      boolean bool3 = l3 < l4;
      if (bool3) {
        return f(l3);
      }
      l3 = b;
      boolean bool4 = l2 < l3;
      if (bool4)
      {
        l1 = l2 - l1;
        int i = c(l1);
        int j = 13;
        if (i == j)
        {
          i = c(l2);
          j = 10;
          if (i == j) {
            return f(l2);
          }
        }
      }
      c localc = new d/c;
      localc.<init>();
      l3 = b;
      l3 = Math.min(32, l3);
      localObject1 = this;
      Object localObject2 = localc;
      a(localc, 0L, l3);
      localObject1 = new java/io/EOFException;
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("\\n not found: limit=");
      paramLong = Math.min(b, paramLong);
      ((StringBuilder)localObject2).append(paramLong);
      ((StringBuilder)localObject2).append(" content=");
      str = localc.o().f();
      ((StringBuilder)localObject2).append(str);
      ((StringBuilder)localObject2).append('…');
      str = ((StringBuilder)localObject2).toString();
      ((EOFException)localObject1).<init>(str);
      throw ((Throwable)localObject1);
    }
    Object localObject1 = new java/lang/IllegalArgumentException;
    String str = String.valueOf(paramLong);
    str = "limit < 0: ".concat(str);
    ((IllegalArgumentException)localObject1).<init>(str);
    throw ((Throwable)localObject1);
  }
  
  public final boolean e()
  {
    long l1 = b;
    long l2 = 0L;
    boolean bool = l1 < l2;
    return !bool;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this == paramObject) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof c;
    if (!bool2) {
      return false;
    }
    paramObject = (c)paramObject;
    long l1 = b;
    long l2 = b;
    bool2 = l1 < l2;
    if (bool2) {
      return false;
    }
    l2 = 0L;
    bool2 = l1 < l2;
    if (!bool2) {
      return bool1;
    }
    q localq = a;
    paramObject = a;
    int i = b;
    int j = b;
    for (;;)
    {
      long l3 = b;
      boolean bool3 = l2 < l3;
      if (!bool3) {
        break;
      }
      int m = c - i;
      int n = c - j;
      m = Math.min(m, n);
      l3 = m;
      bool3 = j;
      j = i;
      i = 0;
      int k;
      for (;;)
      {
        long l4 = i;
        boolean bool4 = l4 < l3;
        if (!bool4) {
          break;
        }
        byte[] arrayOfByte = a;
        int i2 = j + 1;
        j = arrayOfByte[j];
        arrayOfByte = a;
        int i1 = bool3 + true;
        k = arrayOfByte[bool3];
        if (j != k) {
          return false;
        }
        i += 1;
        j = i2;
        k = i1;
      }
      i = c;
      if (j == i)
      {
        localq = f;
        i = b;
      }
      else
      {
        i = j;
      }
      j = c;
      if (k == j)
      {
        paramObject = f;
        j = b;
      }
      else
      {
        j = k;
      }
      l2 += l3;
    }
    return bool1;
  }
  
  final q f(int paramInt)
  {
    if (paramInt > 0)
    {
      int i = 8192;
      if (paramInt <= i)
      {
        q localq = a;
        if (localq == null)
        {
          localObject = r.a();
          a = ((q)localObject);
          localObject = a;
          g = ((q)localObject);
          f = ((q)localObject);
          return (q)localObject;
        }
        localq = g;
        int j = c + paramInt;
        if (j <= i)
        {
          paramInt = e;
          if (paramInt != 0) {}
        }
        else
        {
          localObject = r.a();
          localq = localq.a((q)localObject);
        }
        return localq;
      }
    }
    Object localObject = new java/lang/IllegalArgumentException;
    ((IllegalArgumentException)localObject).<init>();
    throw ((Throwable)localObject);
  }
  
  public final InputStream f()
  {
    c.2 local2 = new d/c$2;
    local2.<init>(this);
    return local2;
  }
  
  final String f(long paramLong)
  {
    long l1 = 1L;
    long l2 = 0L;
    boolean bool = paramLong < l2;
    if (bool)
    {
      l2 = paramLong - l1;
      int i = c(l2);
      int j = 13;
      if (i == j)
      {
        str = m(l2);
        h(2);
        return str;
      }
    }
    String str = m(paramLong);
    h(l1);
    return str;
  }
  
  public final void flush() {}
  
  public final long g()
  {
    long l1 = b;
    long l2 = 0L;
    boolean bool1 = l1 < l2;
    if (!bool1) {
      return l2;
    }
    q localq = a.g;
    int j = c;
    int i = 8192;
    if (j < i)
    {
      boolean bool2 = e;
      if (bool2)
      {
        int k = c;
        int m = b;
        k -= m;
        l2 = k;
        l1 -= l2;
      }
    }
    return l1;
  }
  
  public final byte[] g(long paramLong)
  {
    long l1 = b;
    long l2 = 0L;
    w.a(l1, l2, paramLong);
    l1 = 2147483647L;
    boolean bool = paramLong < l1;
    if (!bool)
    {
      localObject = new byte[(int)paramLong];
      a((byte[])localObject);
      return (byte[])localObject;
    }
    IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
    Object localObject = String.valueOf(paramLong);
    localObject = "byteCount > Integer.MAX_VALUE: ".concat((String)localObject);
    localIllegalArgumentException.<init>((String)localObject);
    throw localIllegalArgumentException;
  }
  
  public final byte h()
  {
    long l1 = b;
    long l2 = 0L;
    boolean bool = l1 < l2;
    if (bool)
    {
      localObject = a;
      int j = b;
      int k = c;
      byte[] arrayOfByte = a;
      int i = j + 1;
      j = arrayOfByte[j];
      long l3 = b;
      long l4 = 1L;
      l3 -= l4;
      b = l3;
      if (i == k)
      {
        q localq = ((q)localObject).b();
        a = localq;
        r.a((q)localObject);
      }
      else
      {
        b = i;
      }
      return j;
    }
    Object localObject = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject).<init>("size == 0");
    throw ((Throwable)localObject);
  }
  
  public final void h(long paramLong)
  {
    for (;;)
    {
      long l1 = 0L;
      boolean bool = paramLong < l1;
      if (!bool) {
        return;
      }
      q localq1 = a;
      if (localq1 == null) {
        break;
      }
      int j = c;
      int k = a.b;
      l1 = j - k;
      l1 = Math.min(paramLong, l1);
      k = (int)l1;
      long l2 = b;
      long l3 = k;
      l2 -= l3;
      b = l2;
      paramLong -= l3;
      localq1 = a;
      int i = b + k;
      b = i;
      localq1 = a;
      j = b;
      q localq2 = a;
      k = c;
      if (j == k)
      {
        localq1 = a;
        localq2 = localq1.b();
        a = localq2;
        r.a(localq1);
      }
    }
    EOFException localEOFException = new java/io/EOFException;
    localEOFException.<init>();
    throw localEOFException;
  }
  
  public final int hashCode()
  {
    q localq1 = a;
    if (localq1 == null) {
      return 0;
    }
    int i = 1;
    q localq2;
    do
    {
      int j = b;
      int k = c;
      while (j < k)
      {
        i *= 31;
        byte[] arrayOfByte = a;
        int m = arrayOfByte[j];
        i += m;
        j += 1;
      }
      localq1 = f;
      localq2 = a;
    } while (localq1 != localq2);
    return i;
  }
  
  public final c i(long paramLong)
  {
    long l1 = 0L;
    boolean bool1 = paramLong < l1;
    if (!bool1) {
      return b(48);
    }
    bool1 = false;
    int i = 1;
    boolean bool2 = paramLong < l1;
    if (bool2)
    {
      paramLong = -paramLong;
      bool1 = paramLong < l1;
      if (bool1) {
        return a("-9223372036854775808");
      }
      bool1 = true;
    }
    long l2 = 100000000L;
    long l3 = 10;
    boolean bool3 = paramLong < l2;
    long l4;
    boolean bool4;
    if (bool3)
    {
      l2 = 10000L;
      bool3 = paramLong < l2;
      if (bool3)
      {
        l2 = 100;
        bool3 = paramLong < l2;
        if (bool3)
        {
          bool2 = paramLong < l3;
          if (!bool2) {
            i = 2;
          }
        }
        else
        {
          l4 = 1000L;
          bool4 = paramLong < l4;
          if (bool4) {
            i = 3;
          } else {
            i = 4;
          }
        }
      }
      else
      {
        l4 = 1000000L;
        bool4 = paramLong < l4;
        if (bool4)
        {
          l4 = 100000L;
          bool4 = paramLong < l4;
          if (bool4) {
            i = 5;
          } else {
            i = 6;
          }
        }
        else
        {
          l4 = 10000000L;
          bool4 = paramLong < l4;
          if (bool4) {
            i = 7;
          } else {
            i = 8;
          }
        }
      }
    }
    else
    {
      l4 = 1000000000000L;
      bool4 = paramLong < l4;
      if (bool4)
      {
        l4 = 10000000000L;
        bool4 = paramLong < l4;
        if (bool4)
        {
          l4 = 1000000000L;
          bool4 = paramLong < l4;
          if (bool4) {
            i = 9;
          } else {
            i = 10;
          }
        }
        else
        {
          l4 = 100000000000L;
          bool4 = paramLong < l4;
          if (bool4) {
            i = 11;
          } else {
            i = 12;
          }
        }
      }
      else
      {
        l4 = 1000000000000000L;
        bool4 = paramLong < l4;
        if (bool4)
        {
          l4 = 10000000000000L;
          bool4 = paramLong < l4;
          if (bool4)
          {
            i = 13;
          }
          else
          {
            l4 = 100000000000000L;
            bool4 = paramLong < l4;
            if (bool4) {
              i = 14;
            } else {
              i = 15;
            }
          }
        }
        else
        {
          l4 = 100000000000000000L;
          bool4 = paramLong < l4;
          if (bool4)
          {
            l4 = 10000000000000000L;
            bool4 = paramLong < l4;
            if (bool4) {
              i = 16;
            } else {
              i = 17;
            }
          }
          else
          {
            l4 = 1000000000000000000L;
            bool4 = paramLong < l4;
            if (bool4) {
              i = 18;
            } else {
              i = 19;
            }
          }
        }
      }
    }
    if (bool1) {
      i += 1;
    }
    q localq = f(i);
    byte[] arrayOfByte1 = a;
    int j = c + i;
    for (;;)
    {
      boolean bool5 = paramLong < l1;
      if (!bool5) {
        break;
      }
      long l5 = paramLong % l3;
      int m = (int)l5;
      j += -1;
      byte[] arrayOfByte2 = c;
      int k = arrayOfByte2[m];
      arrayOfByte1[j] = k;
      paramLong /= l3;
    }
    if (bool1)
    {
      j += -1;
      n = 45;
      arrayOfByte1[j] = n;
    }
    int n = c + i;
    c = n;
    paramLong = b;
    l1 = i;
    paramLong += l1;
    b = paramLong;
    return this;
  }
  
  public final short i()
  {
    long l1 = b;
    long l2 = 2;
    boolean bool = l1 < l2;
    if (!bool)
    {
      localObject1 = a;
      int j = b;
      int i = c;
      int k = i - j;
      int m = 2;
      if (k < m)
      {
        int n = (h() & 0xFF) << 8;
        j = h() & 0xFF;
        return (short)(n | j);
      }
      byte[] arrayOfByte = a;
      m = j + 1;
      j = (arrayOfByte[j] & 0xFF) << 8;
      int i1 = m + 1;
      k = arrayOfByte[m] & 0xFF;
      j |= k;
      long l3 = b - l2;
      b = l3;
      if (i1 == i)
      {
        q localq = ((q)localObject1).b();
        a = localq;
        r.a((q)localObject1);
      }
      else
      {
        b = i1;
      }
      return (short)j;
    }
    Object localObject1 = new java/lang/IllegalStateException;
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("size < 2: ");
    l2 = b;
    ((StringBuilder)localObject2).append(l2);
    localObject2 = ((StringBuilder)localObject2).toString();
    ((IllegalStateException)localObject1).<init>((String)localObject2);
    throw ((Throwable)localObject1);
  }
  
  public final boolean isOpen()
  {
    return true;
  }
  
  public final int j()
  {
    long l1 = b;
    long l2 = 4;
    boolean bool = l1 < l2;
    if (!bool)
    {
      localObject1 = a;
      int j = b;
      int i = c;
      int k = i - j;
      int m = 4;
      if (k < m)
      {
        int n = (h() & 0xFF) << 24;
        j = (h() & 0xFF) << 16;
        n |= j;
        j = (h() & 0xFF) << 8;
        n |= j;
        j = h() & 0xFF;
        return n | j;
      }
      byte[] arrayOfByte = a;
      m = j + 1;
      j = (arrayOfByte[j] & 0xFF) << 24;
      int i1 = m + 1;
      m = (arrayOfByte[m] & 0xFF) << 16;
      j |= m;
      m = i1 + 1;
      i1 = (arrayOfByte[i1] & 0xFF) << 8;
      j |= i1;
      i1 = m + 1;
      k = arrayOfByte[m] & 0xFF;
      j |= k;
      long l3 = b - l2;
      b = l3;
      if (i1 == i)
      {
        q localq = ((q)localObject1).b();
        a = localq;
        r.a((q)localObject1);
      }
      else
      {
        b = i1;
      }
      return j;
    }
    Object localObject1 = new java/lang/IllegalStateException;
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("size < 4: ");
    l2 = b;
    ((StringBuilder)localObject2).append(l2);
    localObject2 = ((StringBuilder)localObject2).toString();
    ((IllegalStateException)localObject1).<init>((String)localObject2);
    throw ((Throwable)localObject1);
  }
  
  public final c j(long paramLong)
  {
    long l1 = 0L;
    boolean bool = paramLong < l1;
    if (!bool) {
      return b(48);
    }
    l1 = Long.highestOneBit(paramLong);
    int i = Long.numberOfTrailingZeros(l1);
    int j = 4;
    i = i / j + 1;
    q localq = f(i);
    byte[] arrayOfByte1 = a;
    int k = c + i + -1;
    int m = c;
    while (k >= m)
    {
      byte[] arrayOfByte2 = c;
      long l2 = 15 & paramLong;
      int n = (int)l2;
      int i1 = arrayOfByte2[n];
      arrayOfByte1[k] = i1;
      paramLong >>>= j;
      k += -1;
    }
    int i2 = c + i;
    c = i2;
    paramLong = b;
    l1 = i;
    paramLong += l1;
    b = paramLong;
    return this;
  }
  
  public final short k()
  {
    return w.a(i());
  }
  
  public final int l()
  {
    return w.a(j());
  }
  
  public final v l_()
  {
    return v.c;
  }
  
  public final long m()
  {
    c localc = this;
    long l1 = b;
    long l2 = 0L;
    boolean bool1 = l1 < l2;
    if (bool1)
    {
      l1 = -7;
      bool1 = false;
      long l3 = l2;
      int j = 0;
      Object localObject1 = null;
      int k = 0;
      Object localObject2;
      label201:
      label321:
      int i;
      label405:
      do
      {
        localObject2 = a;
        Object localObject3 = a;
        int m = b;
        int n = c;
        int i1;
        Object localObject4;
        Object localObject7;
        for (;;)
        {
          boolean bool2 = true;
          if (m >= n) {
            break label405;
          }
          i1 = localObject3[m];
          int i2 = 48;
          if (i1 >= i2)
          {
            int i3 = 57;
            if (i1 <= i3)
            {
              i2 -= i1;
              long l4 = -922337203685477580L;
              bool2 = l3 < l4;
              if (!bool2)
              {
                bool2 = l3 < l4;
                if (!bool2)
                {
                  localObject4 = localObject2;
                  localObject5 = localObject3;
                  l5 = i2;
                  bool2 = l5 < l1;
                  if (bool2) {
                    break label201;
                  }
                }
                else
                {
                  localObject4 = localObject2;
                  localObject5 = localObject3;
                }
                l3 *= 10;
                l5 = i2;
                l3 += l5;
                break label321;
              }
              localObject6 = new d/c;
              ((c)localObject6).<init>();
              localObject6 = ((c)localObject6).i(l3).b(i1);
              if (j == 0) {
                ((c)localObject6).h();
              }
              localObject7 = new java/lang/NumberFormatException;
              localObject1 = new java/lang/StringBuilder;
              ((StringBuilder)localObject1).<init>("Number too large: ");
              localObject6 = ((c)localObject6).p();
              ((StringBuilder)localObject1).append((String)localObject6);
              localObject6 = ((StringBuilder)localObject1).toString();
              ((NumberFormatException)localObject7).<init>((String)localObject6);
              throw ((Throwable)localObject7);
            }
          }
          localObject4 = localObject2;
          Object localObject5 = localObject3;
          int i4 = 45;
          if ((i1 != i4) || (bool1)) {
            break;
          }
          long l5 = 1L;
          l1 -= l5;
          j = 1;
          m += 1;
          bool1 += true;
          localObject2 = localObject4;
          localObject3 = localObject5;
        }
        if (i != 0)
        {
          k = 1;
        }
        else
        {
          localObject6 = new java/lang/NumberFormatException;
          localObject7 = new java/lang/StringBuilder;
          ((StringBuilder)localObject7).<init>("Expected leading [0-9] or '-' character but was 0x");
          localObject1 = Integer.toHexString(i1);
          ((StringBuilder)localObject7).append((String)localObject1);
          localObject7 = ((StringBuilder)localObject7).toString();
          ((NumberFormatException)localObject6).<init>((String)localObject7);
          throw ((Throwable)localObject6);
          localObject4 = localObject2;
        }
        if (m == n)
        {
          localObject2 = ((q)localObject4).b();
          a = ((q)localObject2);
          r.a((q)localObject4);
        }
        else
        {
          localObject2 = localObject4;
          b = m;
        }
        if (k != 0) {
          break;
        }
        localObject2 = a;
      } while (localObject2 != null);
      l1 = b;
      long l6 = i;
      l1 -= l6;
      b = l1;
      if (j != 0) {
        return l3;
      }
      return -l3;
    }
    Object localObject6 = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject6).<init>("size == 0");
    throw ((Throwable)localObject6);
  }
  
  public final long n()
  {
    long l1 = b;
    long l2 = 0L;
    boolean bool1 = l1 < l2;
    if (bool1)
    {
      localObject1 = null;
      l1 = l2;
      bool1 = false;
      int j = 0;
      q localq;
      label168:
      int i;
      do
      {
        localq = a;
        Object localObject2 = a;
        int k = b;
        int m = c;
        while (k < m)
        {
          int n = localObject2[k];
          int i1 = 48;
          if (n >= i1)
          {
            i1 = 57;
            if (n <= i1)
            {
              i1 = n + -48;
              break label168;
            }
          }
          i1 = 97;
          if (n >= i1)
          {
            i1 = 102;
            if (n <= i1)
            {
              i1 = n + -97 + 10;
              break label168;
            }
          }
          i1 = 65;
          Object localObject3;
          Object localObject4;
          if (n >= i1)
          {
            i1 = 70;
            if (n <= i1)
            {
              i1 = n + -65 + 10;
              long l3 = 0xF000000000000000 & l1;
              boolean bool2 = l3 < l2;
              if (!bool2)
              {
                n = 4;
                l1 <<= n;
                long l4 = i1;
                l1 |= l4;
                k += 1;
                bool1 += true;
                continue;
              }
              localObject3 = new d/c;
              ((c)localObject3).<init>();
              localObject1 = ((c)localObject3).j(l1).b(n);
              localObject4 = new java/lang/NumberFormatException;
              localObject3 = new java/lang/StringBuilder;
              ((StringBuilder)localObject3).<init>("Number too large: ");
              localObject1 = ((c)localObject1).p();
              ((StringBuilder)localObject3).append((String)localObject1);
              localObject1 = ((StringBuilder)localObject3).toString();
              ((NumberFormatException)localObject4).<init>((String)localObject1);
              throw ((Throwable)localObject4);
            }
          }
          if (i != 0)
          {
            j = 1;
          }
          else
          {
            localObject1 = new java/lang/NumberFormatException;
            localObject4 = new java/lang/StringBuilder;
            ((StringBuilder)localObject4).<init>("Expected leading [0-9a-fA-F] character but was 0x");
            localObject3 = Integer.toHexString(n);
            ((StringBuilder)localObject4).append((String)localObject3);
            localObject4 = ((StringBuilder)localObject4).toString();
            ((NumberFormatException)localObject1).<init>((String)localObject4);
            throw ((Throwable)localObject1);
          }
        }
        if (k == m)
        {
          localObject2 = localq.b();
          a = ((q)localObject2);
          r.a(localq);
        }
        else
        {
          b = k;
        }
        if (j != 0) {
          break;
        }
        localq = a;
      } while (localq != null);
      l2 = b;
      long l5 = i;
      l2 -= l5;
      b = l2;
      return l1;
    }
    Object localObject1 = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject1).<init>("size == 0");
    throw ((Throwable)localObject1);
  }
  
  public final f o()
  {
    f localf = new d/f;
    byte[] arrayOfByte = r();
    localf.<init>(arrayOfByte);
    return localf;
  }
  
  public final String p()
  {
    try
    {
      long l = b;
      Charset localCharset = w.a;
      return a(l, localCharset);
    }
    catch (EOFException localEOFException)
    {
      AssertionError localAssertionError = new java/lang/AssertionError;
      localAssertionError.<init>(localEOFException);
      throw localAssertionError;
    }
  }
  
  public final String q()
  {
    return e(Long.MAX_VALUE);
  }
  
  public final byte[] r()
  {
    try
    {
      long l = b;
      return g(l);
    }
    catch (EOFException localEOFException)
    {
      AssertionError localAssertionError = new java/lang/AssertionError;
      localAssertionError.<init>(localEOFException);
      throw localAssertionError;
    }
  }
  
  public final int read(ByteBuffer paramByteBuffer)
  {
    q localq = a;
    if (localq == null) {
      return -1;
    }
    int i = paramByteBuffer.remaining();
    int j = c;
    int k = b;
    j -= k;
    i = Math.min(i, j);
    byte[] arrayOfByte = a;
    k = b;
    paramByteBuffer.put(arrayOfByte, k, i);
    int m = b + i;
    b = m;
    long l1 = b;
    long l2 = i;
    l1 -= l2;
    b = l1;
    m = b;
    j = c;
    if (m == j)
    {
      paramByteBuffer = localq.b();
      a = paramByteBuffer;
      r.a(localq);
    }
    return i;
  }
  
  public final void s()
  {
    try
    {
      long l = b;
      h(l);
      return;
    }
    catch (EOFException localEOFException)
    {
      AssertionError localAssertionError = new java/lang/AssertionError;
      localAssertionError.<init>(localEOFException);
      throw localAssertionError;
    }
  }
  
  public final long t()
  {
    return a((byte)0, 0L, Long.MAX_VALUE);
  }
  
  public final String toString()
  {
    long l1 = b;
    long l2 = 2147483647L;
    boolean bool = l1 < l2;
    if (!bool)
    {
      int i = (int)l1;
      if (i == 0)
      {
        localObject1 = f.b;
      }
      else
      {
        localObject1 = new d/s;
        ((s)localObject1).<init>(this, i);
      }
      return ((f)localObject1).toString();
    }
    Object localObject1 = new java/lang/IllegalArgumentException;
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("size > Integer.MAX_VALUE: ");
    l2 = b;
    ((StringBuilder)localObject2).append(l2);
    localObject2 = ((StringBuilder)localObject2).toString();
    ((IllegalArgumentException)localObject1).<init>((String)localObject2);
    throw ((Throwable)localObject1);
  }
  
  public final c u()
  {
    c localc = new d/c;
    localc.<init>();
    long l1 = b;
    long l2 = 0L;
    boolean bool = l1 < l2;
    if (!bool) {
      return localc;
    }
    q localq1 = a.a();
    a = localq1;
    localq1 = a;
    g = localq1;
    f = localq1;
    localq1 = a;
    for (;;)
    {
      localq1 = f;
      q localq2 = a;
      if (localq1 == localq2) {
        break;
      }
      localq2 = a.g;
      q localq3 = localq1.a();
      localq2.a(localq3);
    }
    l1 = b;
    b = l1;
    return localc;
  }
  
  public final int write(ByteBuffer paramByteBuffer)
  {
    if (paramByteBuffer != null)
    {
      int i = paramByteBuffer.remaining();
      int j = i;
      while (j > 0)
      {
        int k = 1;
        q localq = f(k);
        int m = c;
        m = 8192 - m;
        m = Math.min(j, m);
        byte[] arrayOfByte = a;
        int n = c;
        paramByteBuffer.get(arrayOfByte, n, m);
        j -= m;
        int i1 = c + m;
        c = i1;
      }
      long l1 = b;
      long l2 = i;
      l1 += l2;
      b = l1;
      return i;
    }
    paramByteBuffer = new java/lang/IllegalArgumentException;
    paramByteBuffer.<init>("source == null");
    throw paramByteBuffer;
  }
}

/* Location:
 * Qualified Name:     d.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */