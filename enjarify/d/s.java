package d;

import java.util.Arrays;

final class s
  extends f
{
  final transient byte[][] f;
  final transient int[] g;
  
  s(c paramc, int paramInt)
  {
    super(null);
    long l1 = b;
    long l2 = paramInt;
    long l3 = 0L;
    w.a(l1, l3, l2);
    Object localObject1 = a;
    int i = 0;
    Object localObject2 = localObject1;
    int j = 0;
    localObject1 = null;
    int k = 0;
    Object localObject3 = null;
    int m;
    while (j < paramInt)
    {
      m = c;
      int n = b;
      if (m != n)
      {
        m = c;
        n = b;
        m -= n;
        j += m;
        k += 1;
        localObject2 = f;
      }
      else
      {
        paramc = new java/lang/AssertionError;
        paramc.<init>("s.limit == s.pos");
        throw paramc;
      }
    }
    localObject1 = new byte[k][];
    f = ((byte[][])localObject1);
    k *= 2;
    localObject1 = new int[k];
    g = ((int[])localObject1);
    localObject1 = a;
    int i1 = 0;
    paramc = null;
    while (i < paramInt)
    {
      localObject3 = f;
      localObject2 = a;
      localObject3[i1] = localObject2;
      k = c;
      int i2 = b;
      k -= i2;
      i += k;
      if (i > paramInt) {
        i = paramInt;
      }
      localObject3 = g;
      localObject3[i1] = i;
      localObject2 = f;
      i2 = localObject2.length + i1;
      m = b;
      localObject3[i2] = m;
      k = 1;
      d = k;
      i1 += 1;
      localObject1 = f;
    }
  }
  
  private int b(int paramInt)
  {
    int[] arrayOfInt = g;
    byte[][] arrayOfByte = f;
    int i = arrayOfByte.length;
    paramInt += 1;
    paramInt = Arrays.binarySearch(arrayOfInt, 0, i, paramInt);
    if (paramInt >= 0) {
      return paramInt;
    }
    return paramInt ^ 0xFFFFFFFF;
  }
  
  private f j()
  {
    f localf = new d/f;
    byte[] arrayOfByte = i();
    localf.<init>(arrayOfByte);
    return localf;
  }
  
  private Object writeReplace()
  {
    return j();
  }
  
  public final byte a(int paramInt)
  {
    int[] arrayOfInt1 = g;
    Object localObject = f;
    int i = localObject.length + -1;
    long l1 = arrayOfInt1[i];
    long l2 = paramInt;
    long l3 = 1L;
    w.a(l1, l2, l3);
    int j = b(paramInt);
    if (j == 0)
    {
      i = 0;
      localObject = null;
    }
    else
    {
      localObject = g;
      k = j + -1;
      i = localObject[k];
    }
    int[] arrayOfInt2 = g;
    byte[][] arrayOfByte = f;
    int m = arrayOfByte.length + j;
    int k = arrayOfInt2[m];
    arrayOfInt1 = arrayOfByte[j];
    paramInt = paramInt - i + k;
    return arrayOfInt1[paramInt];
  }
  
  public final f a(int paramInt1, int paramInt2)
  {
    return j().a(paramInt1, paramInt2);
  }
  
  public final String a()
  {
    return j().a();
  }
  
  final void a(c paramc)
  {
    byte[][] arrayOfByte = f;
    int i = arrayOfByte.length;
    int j = 0;
    int k = 0;
    q localq1 = null;
    while (j < i)
    {
      int[] arrayOfInt = g;
      int m = i + j;
      m = arrayOfInt[m];
      int n = arrayOfInt[j];
      q localq2 = new d/q;
      byte[] arrayOfByte1 = f[j];
      int i1 = m + n - k;
      localq2.<init>(arrayOfByte1, m, i1);
      localq1 = a;
      if (localq1 == null)
      {
        g = localq2;
        f = localq2;
        a = localq2;
      }
      else
      {
        localq1 = a.g;
        localq1.a(localq2);
      }
      j += 1;
      k = n;
    }
    long l1 = b;
    long l2 = k;
    l1 += l2;
    b = l1;
  }
  
  public final boolean a(int paramInt1, f paramf, int paramInt2, int paramInt3)
  {
    int i = h() - paramInt3;
    if (i < 0) {
      return false;
    }
    i = b(0);
    while (paramInt3 > 0)
    {
      int[] arrayOfInt1;
      if (i == 0)
      {
        j = 0;
        arrayOfInt1 = null;
      }
      else
      {
        arrayOfInt1 = g;
        k = i + -1;
        j = arrayOfInt1[k];
      }
      int[] arrayOfInt2 = g;
      int k = arrayOfInt2[i] - j + j - paramInt1;
      k = Math.min(paramInt3, k);
      int[] arrayOfInt3 = g;
      byte[][] arrayOfByte = f;
      int m = arrayOfByte.length + i;
      int n = arrayOfInt3[m];
      int j = paramInt1 - j + n;
      arrayOfInt3 = arrayOfByte[i];
      boolean bool = paramf.a(paramInt2, arrayOfInt3, j, k);
      if (!bool) {
        return false;
      }
      paramInt1 += k;
      paramInt2 += k;
      paramInt3 -= k;
      i += 1;
    }
    return true;
  }
  
  public final boolean a(int paramInt1, byte[] paramArrayOfByte, int paramInt2, int paramInt3)
  {
    if (paramInt1 >= 0)
    {
      int i = h() - paramInt3;
      if ((paramInt1 <= i) && (paramInt2 >= 0))
      {
        i = paramArrayOfByte.length - paramInt3;
        if (paramInt2 <= i)
        {
          i = b(paramInt1);
          while (paramInt3 > 0)
          {
            int[] arrayOfInt1;
            if (i == 0)
            {
              j = 0;
              arrayOfInt1 = null;
            }
            else
            {
              arrayOfInt1 = g;
              k = i + -1;
              j = arrayOfInt1[k];
            }
            int[] arrayOfInt2 = g;
            int k = arrayOfInt2[i] - j + j - paramInt1;
            k = Math.min(paramInt3, k);
            int[] arrayOfInt3 = g;
            byte[][] arrayOfByte = f;
            int m = arrayOfByte.length + i;
            int n = arrayOfInt3[m];
            int j = paramInt1 - j + n;
            arrayOfInt3 = arrayOfByte[i];
            boolean bool = w.a(arrayOfInt3, j, paramArrayOfByte, paramInt2, k);
            if (!bool) {
              return false;
            }
            paramInt1 += k;
            paramInt2 += k;
            paramInt3 -= k;
            i += 1;
          }
          return true;
        }
      }
    }
    return false;
  }
  
  public final String b()
  {
    return j().b();
  }
  
  public final f c()
  {
    return j().c();
  }
  
  public final f d()
  {
    return j().d();
  }
  
  public final f e()
  {
    return j().e();
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (paramObject == this) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof f;
    if (bool2)
    {
      paramObject = (f)paramObject;
      int i = ((f)paramObject).h();
      int j = h();
      if (i == j)
      {
        i = h();
        boolean bool3 = a(0, (f)paramObject, 0, i);
        if (bool3) {
          return bool1;
        }
      }
    }
    return false;
  }
  
  public final String f()
  {
    return j().f();
  }
  
  public final f g()
  {
    return j().g();
  }
  
  public final int h()
  {
    int[] arrayOfInt = g;
    int i = f.length + -1;
    return arrayOfInt[i];
  }
  
  public final int hashCode()
  {
    int i = d;
    if (i != 0) {
      return i;
    }
    byte[][] arrayOfByte = f;
    i = arrayOfByte.length;
    int j = 0;
    int k = 1;
    int i1;
    for (int m = 0; j < i; m = i1)
    {
      byte[] arrayOfByte1 = f[j];
      int[] arrayOfInt = g;
      int n = i + j;
      n = arrayOfInt[n];
      i1 = arrayOfInt[j];
      m = i1 - m + n;
      while (n < m)
      {
        k *= 31;
        int i2 = arrayOfByte1[n];
        k += i2;
        n += 1;
      }
      j += 1;
    }
    d = k;
    return k;
  }
  
  public final byte[] i()
  {
    Object localObject = g;
    byte[][] arrayOfByte = f;
    int i = arrayOfByte.length + -1;
    int j = localObject[i];
    localObject = new byte[j];
    int k = arrayOfByte.length;
    i = 0;
    int i1;
    for (int m = 0; i < k; m = i1)
    {
      int[] arrayOfInt = g;
      int n = k + i;
      n = arrayOfInt[n];
      i1 = arrayOfInt[i];
      byte[] arrayOfByte1 = f[i];
      int i2 = i1 - m;
      System.arraycopy(arrayOfByte1, n, localObject, m, i2);
      i += 1;
    }
    return (byte[])localObject;
  }
  
  public final String toString()
  {
    return j().toString();
  }
}

/* Location:
 * Qualified Name:     d.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */