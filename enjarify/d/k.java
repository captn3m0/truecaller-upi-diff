package d;

import java.util.zip.CRC32;
import java.util.zip.Deflater;

public final class k
  implements t
{
  private final d a;
  private final Deflater b;
  private final g c;
  private boolean d;
  private final CRC32 e;
  
  public k(t paramt)
  {
    Object localObject = new java/util/zip/CRC32;
    ((CRC32)localObject).<init>();
    e = ((CRC32)localObject);
    if (paramt != null)
    {
      localObject = new java/util/zip/Deflater;
      ((Deflater)localObject).<init>(-1, true);
      b = ((Deflater)localObject);
      paramt = n.a(paramt);
      a = paramt;
      paramt = new d/g;
      localObject = a;
      Deflater localDeflater = b;
      paramt.<init>((d)localObject, localDeflater);
      c = paramt;
      paramt = a.b();
      paramt.c(8075);
      paramt.b(8);
      paramt.b(0);
      paramt.d(0);
      paramt.b(0);
      paramt.b(0);
      return;
    }
    paramt = new java/lang/IllegalArgumentException;
    paramt.<init>("sink == null");
    throw paramt;
  }
  
  private void b(c paramc, long paramLong)
  {
    for (paramc = a;; paramc = f)
    {
      long l = 0L;
      boolean bool = paramLong < l;
      if (!bool) {
        break;
      }
      int i = c;
      int j = b;
      i -= j;
      l = i;
      j = (int)Math.min(paramLong, l);
      CRC32 localCRC32 = e;
      byte[] arrayOfByte = a;
      int k = b;
      localCRC32.update(arrayOfByte, k, j);
      l = j;
      paramLong -= l;
    }
  }
  
  public final void a_(c paramc, long paramLong)
  {
    long l = 0L;
    boolean bool = paramLong < l;
    if (!bool)
    {
      bool = paramLong < l;
      if (!bool) {
        return;
      }
      b(paramc, paramLong);
      c.a_(paramc, paramLong);
      return;
    }
    paramc = new java/lang/IllegalArgumentException;
    String str = String.valueOf(paramLong);
    str = "byteCount < 0: ".concat(str);
    paramc.<init>(str);
    throw paramc;
  }
  
  public final void close()
  {
    boolean bool1 = d;
    if (bool1) {
      return;
    }
    bool1 = false;
    Object localObject1 = null;
    Object localObject4;
    try
    {
      localObject4 = c;
      ((g)localObject4).b();
      localObject4 = a;
      Object localObject7 = e;
      long l = ((CRC32)localObject7).getValue();
      int i = (int)l;
      ((d)localObject4).g(i);
      localObject4 = a;
      localObject7 = b;
      l = ((Deflater)localObject7).getBytesRead();
      i = (int)l;
      ((d)localObject4).g(i);
    }
    finally {}
    Object localObject3;
    try
    {
      localObject4 = b;
      ((Deflater)localObject4).end();
    }
    finally
    {
      if (localObject2 == null) {
        localObject3 = localObject5;
      }
    }
    try
    {
      d locald = a;
      locald.close();
    }
    finally
    {
      if (localObject3 == null) {
        localObject3 = localObject6;
      }
    }
    boolean bool2 = true;
    d = bool2;
    if (localObject3 != null) {
      w.a((Throwable)localObject3);
    }
  }
  
  public final void flush()
  {
    c.flush();
  }
  
  public final v l_()
  {
    return a.l_();
  }
}

/* Location:
 * Qualified Name:     d.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */