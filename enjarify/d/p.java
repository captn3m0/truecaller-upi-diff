package d;

import java.io.EOFException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

final class p
  implements e
{
  public final c a;
  public final u b;
  boolean c;
  
  p(u paramu)
  {
    c localc = new d/c;
    localc.<init>();
    a = localc;
    if (paramu != null)
    {
      b = paramu;
      return;
    }
    paramu = new java/lang/NullPointerException;
    paramu.<init>("source == null");
    throw paramu;
  }
  
  private long a(byte paramByte, long paramLong1, long paramLong2)
  {
    boolean bool1 = c;
    if (!bool1)
    {
      long l1 = 0L;
      boolean bool2 = paramLong2 < l1;
      if (!bool2)
      {
        for (;;)
        {
          l1 = -1;
          bool2 = paramLong1 < paramLong2;
          if (!bool2) {
            break label159;
          }
          c localc1 = a;
          int i = paramByte;
          long l2 = localc1.a(paramByte, paramLong1, paramLong2);
          boolean bool3 = l2 < l1;
          if (bool3) {
            return l2;
          }
          c localc2 = a;
          l2 = b;
          bool3 = l2 < paramLong2;
          if (!bool3) {
            break;
          }
          u localu = b;
          c localc3 = a;
          long l3 = 8192L;
          long l4 = localu.a(localc3, l3);
          boolean bool4 = l4 < l1;
          if (!bool4) {
            break;
          }
          paramLong1 = Math.max(paramLong1, l2);
        }
        return l1;
        label159:
        return l1;
      }
      localObject1 = new java/lang/IllegalArgumentException;
      Object localObject2 = new Object[2];
      Long localLong1 = Long.valueOf(l1);
      localObject2[0] = localLong1;
      Long localLong2 = Long.valueOf(paramLong2);
      localObject2[1] = localLong2;
      localObject2 = String.format("fromIndex=%s toIndex=%s", (Object[])localObject2);
      ((IllegalArgumentException)localObject1).<init>((String)localObject2);
      throw ((Throwable)localObject1);
    }
    Object localObject1 = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject1).<init>("closed");
    throw ((Throwable)localObject1);
  }
  
  public final long a(c paramc, long paramLong)
  {
    if (paramc != null)
    {
      long l1 = 0L;
      boolean bool1 = paramLong < l1;
      if (!bool1)
      {
        bool1 = c;
        if (!bool1)
        {
          c localc1 = a;
          long l2 = b;
          boolean bool2 = l2 < l1;
          if (!bool2)
          {
            u localu = b;
            c localc2 = a;
            l1 = localu.a(localc2, 8192L);
            l2 = -1;
            bool2 = l1 < l2;
            if (!bool2) {
              return l2;
            }
          }
          l1 = a.b;
          paramLong = Math.min(paramLong, l1);
          return a.a(paramc, paramLong);
        }
        paramc = new java/lang/IllegalStateException;
        paramc.<init>("closed");
        throw paramc;
      }
      paramc = new java/lang/IllegalArgumentException;
      String str = String.valueOf(paramLong);
      str = "byteCount < 0: ".concat(str);
      paramc.<init>(str);
      throw paramc;
    }
    paramc = new java/lang/IllegalArgumentException;
    paramc.<init>("sink == null");
    throw paramc;
  }
  
  public final long a(t paramt)
  {
    long l1 = 0L;
    long l2 = l1;
    for (;;)
    {
      localObject = b;
      c localc1 = a;
      l3 = ((u)localObject).a(localc1, 8192L);
      long l4 = -1;
      boolean bool1 = l3 < l4;
      if (!bool1) {
        break;
      }
      localObject = a;
      l3 = ((c)localObject).g();
      bool2 = l3 < l1;
      if (bool2)
      {
        l2 += l3;
        c localc2 = a;
        paramt.a_(localc2, l3);
      }
    }
    Object localObject = a;
    long l3 = b;
    boolean bool2 = l3 < l1;
    if (bool2)
    {
      l1 = a.b;
      l2 += l1;
      c localc3 = a;
      l3 = b;
      paramt.a_(localc3, l3);
    }
    return l2;
  }
  
  public final String a(Charset paramCharset)
  {
    if (paramCharset != null)
    {
      c localc = a;
      u localu = b;
      localc.a(localu);
      return a.a(paramCharset);
    }
    paramCharset = new java/lang/IllegalArgumentException;
    paramCharset.<init>("charset == null");
    throw paramCharset;
  }
  
  public final void a(long paramLong)
  {
    boolean bool = b(paramLong);
    if (bool) {
      return;
    }
    EOFException localEOFException = new java/io/EOFException;
    localEOFException.<init>();
    throw localEOFException;
  }
  
  public final void a(byte[] paramArrayOfByte)
  {
    try
    {
      int i = paramArrayOfByte.length;
      long l1 = i;
      a(l1);
      a.a(paramArrayOfByte);
      return;
    }
    catch (EOFException localEOFException)
    {
      int j = 0;
      for (;;)
      {
        c localc = a;
        long l2 = b;
        long l3 = 0L;
        boolean bool = l2 < l3;
        if (!bool) {
          break label113;
        }
        localc = a;
        long l4 = b;
        int k = (int)l4;
        int m = localc.a(paramArrayOfByte, j, k);
        int n = -1;
        if (m == n) {
          break;
        }
        j += m;
      }
      paramArrayOfByte = new java/lang/AssertionError;
      paramArrayOfByte.<init>();
      throw paramArrayOfByte;
      label113:
      throw localEOFException;
    }
  }
  
  public final c b()
  {
    return a;
  }
  
  public final boolean b(long paramLong)
  {
    long l1 = 0L;
    boolean bool1 = paramLong < l1;
    if (!bool1)
    {
      boolean bool2 = c;
      if (!bool2)
      {
        boolean bool3;
        do
        {
          localObject1 = a;
          l1 = b;
          bool1 = l1 < paramLong;
          if (!bool1) {
            break;
          }
          localObject1 = b;
          c localc = a;
          l1 = ((u)localObject1).a(localc, 8192L);
          long l2 = -1;
          bool3 = l1 < l2;
        } while (bool3);
        return false;
        return true;
      }
      localObject2 = new java/lang/IllegalStateException;
      ((IllegalStateException)localObject2).<init>("closed");
      throw ((Throwable)localObject2);
    }
    Object localObject1 = new java/lang/IllegalArgumentException;
    Object localObject2 = String.valueOf(paramLong);
    localObject2 = "byteCount < 0: ".concat((String)localObject2);
    ((IllegalArgumentException)localObject1).<init>((String)localObject2);
    throw ((Throwable)localObject1);
  }
  
  public final boolean b(f paramf)
  {
    int i = paramf.h();
    boolean bool1 = c;
    if (!bool1)
    {
      bool1 = false;
      if (i >= 0)
      {
        int j = paramf.h() - 0;
        if (j >= i)
        {
          j = 0;
          while (j < i)
          {
            long l1 = j + 0L;
            long l2 = 1L + l1;
            boolean bool2 = b(l2);
            if (!bool2) {
              return false;
            }
            c localc = a;
            int k = localc.c(l1);
            int m = j + 0;
            m = paramf.a(m);
            if (k != m) {
              return false;
            }
            j += 1;
          }
          return true;
        }
      }
      return false;
    }
    paramf = new java/lang/IllegalStateException;
    paramf.<init>("closed");
    throw paramf;
  }
  
  public final void close()
  {
    boolean bool = c;
    if (bool) {
      return;
    }
    c = true;
    b.close();
    a.s();
  }
  
  public final f d(long paramLong)
  {
    a(paramLong);
    return a.d(paramLong);
  }
  
  public final String e(long paramLong)
  {
    long l1 = 0L;
    boolean bool1 = paramLong < l1;
    if (!bool1)
    {
      l1 = 1L;
      long l2 = Long.MAX_VALUE;
      boolean bool2 = paramLong < l2;
      if (!bool2) {
        l3 = l2;
      } else {
        l3 = paramLong + l1;
      }
      byte b1 = 10;
      Object localObject1 = this;
      long l4 = a(b1, 0L, l3);
      long l5 = -1;
      boolean bool3 = l4 < l5;
      if (bool3) {
        return a.f(l4);
      }
      boolean bool4 = l3 < l2;
      if (bool4)
      {
        bool1 = b(l3);
        if (bool1)
        {
          c localc = a;
          l4 = l3 - l1;
          int i = localc.c(l4);
          int j = 13;
          if (i == j)
          {
            l1 += l3;
            boolean bool5 = b(l1);
            if (bool5)
            {
              localObject2 = a;
              int k = ((c)localObject2).c(l3);
              int m = 10;
              if (k == m) {
                return a.f(l3);
              }
            }
          }
        }
      }
      localObject1 = new d/c;
      ((c)localObject1).<init>();
      localObject2 = a;
      long l6 = b;
      long l3 = Math.min(32, l6);
      Object localObject3 = localObject1;
      ((c)localObject2).a((c)localObject1, 0L, l3);
      localObject2 = new java/io/EOFException;
      localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>("\\n not found: limit=");
      paramLong = Math.min(a.b, paramLong);
      ((StringBuilder)localObject3).append(paramLong);
      ((StringBuilder)localObject3).append(" content=");
      str = ((c)localObject1).o().f();
      ((StringBuilder)localObject3).append(str);
      ((StringBuilder)localObject3).append('…');
      str = ((StringBuilder)localObject3).toString();
      ((EOFException)localObject2).<init>(str);
      throw ((Throwable)localObject2);
    }
    Object localObject2 = new java/lang/IllegalArgumentException;
    String str = String.valueOf(paramLong);
    str = "limit < 0: ".concat(str);
    ((IllegalArgumentException)localObject2).<init>(str);
    throw ((Throwable)localObject2);
  }
  
  public final boolean e()
  {
    boolean bool1 = c;
    if (!bool1)
    {
      localObject = a;
      bool1 = ((c)localObject).e();
      if (bool1)
      {
        localObject = b;
        c localc = a;
        long l1 = ((u)localObject).a(localc, 8192L);
        long l2 = -1;
        boolean bool2 = l1 < l2;
        if (!bool2) {
          return true;
        }
      }
      return false;
    }
    Object localObject = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject).<init>("closed");
    throw ((Throwable)localObject);
  }
  
  public final InputStream f()
  {
    p.1 local1 = new d/p$1;
    local1.<init>(this);
    return local1;
  }
  
  public final byte[] g(long paramLong)
  {
    a(paramLong);
    return a.g(paramLong);
  }
  
  public final byte h()
  {
    a(1L);
    return a.h();
  }
  
  public final void h(long paramLong)
  {
    boolean bool1 = c;
    if (!bool1)
    {
      for (;;)
      {
        long l1 = 0L;
        boolean bool2 = paramLong < l1;
        if (!bool2) {
          break;
        }
        c localc1 = a;
        long l2 = b;
        boolean bool3 = l2 < l1;
        if (!bool3)
        {
          localObject1 = b;
          c localc2 = a;
          l1 = ((u)localObject1).a(localc2, 8192L);
          l2 = -1;
          bool3 = l1 < l2;
          if (!bool3)
          {
            localObject2 = new java/io/EOFException;
            ((EOFException)localObject2).<init>();
            throw ((Throwable)localObject2);
          }
        }
        Object localObject1 = a;
        l1 = b;
        l1 = Math.min(paramLong, l1);
        localc1 = a;
        localc1.h(l1);
        paramLong -= l1;
      }
      return;
    }
    Object localObject2 = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject2).<init>("closed");
    throw ((Throwable)localObject2);
  }
  
  public final short i()
  {
    a(2);
    return a.i();
  }
  
  public final boolean isOpen()
  {
    boolean bool = c;
    return !bool;
  }
  
  public final int j()
  {
    a(4);
    return a.j();
  }
  
  public final short k()
  {
    a(2);
    return w.a(a.i());
  }
  
  public final int l()
  {
    a(4);
    return w.a(a.j());
  }
  
  public final v l_()
  {
    return b.l_();
  }
  
  public final long m()
  {
    long l1 = 1L;
    a(l1);
    String str = null;
    int i = 0;
    NumberFormatException localNumberFormatException = null;
    Object localObject;
    byte b1;
    for (;;)
    {
      int j = i + 1;
      long l2 = j;
      boolean bool = b(l2);
      if (!bool) {
        break label156;
      }
      localObject = a;
      long l3 = i;
      b1 = ((c)localObject).c(l3);
      byte b2 = 48;
      if (b1 >= b2)
      {
        b2 = 57;
        if (b1 <= b2) {}
      }
      else
      {
        if (i != 0) {
          break;
        }
        b2 = 45;
        if (b1 != b2) {
          break;
        }
      }
      i = j;
    }
    if (i == 0)
    {
      localNumberFormatException = new java/lang/NumberFormatException;
      Object[] arrayOfObject = new Object[1];
      localObject = Byte.valueOf(b1);
      arrayOfObject[0] = localObject;
      str = String.format("Expected leading [0-9] or '-' character but was %#x", arrayOfObject);
      localNumberFormatException.<init>(str);
      throw localNumberFormatException;
    }
    label156:
    return a.m();
  }
  
  public final long n()
  {
    long l1 = 1L;
    a(l1);
    String str = null;
    int i = 0;
    NumberFormatException localNumberFormatException = null;
    Object localObject;
    byte b1;
    for (;;)
    {
      int j = i + 1;
      long l2 = j;
      boolean bool = b(l2);
      if (!bool) {
        break label185;
      }
      localObject = a;
      long l3 = i;
      b1 = ((c)localObject).c(l3);
      byte b2 = 48;
      if (b1 >= b2)
      {
        b2 = 57;
        if (b1 <= b2) {}
      }
      else
      {
        b2 = 97;
        if (b1 >= b2)
        {
          b2 = 102;
          if (b1 <= b2) {}
        }
        else
        {
          b2 = 65;
          if (b1 < b2) {
            break;
          }
          b2 = 70;
          if (b1 > b2) {
            break;
          }
        }
      }
      i = j;
    }
    if (i == 0)
    {
      localNumberFormatException = new java/lang/NumberFormatException;
      Object[] arrayOfObject = new Object[1];
      localObject = Byte.valueOf(b1);
      arrayOfObject[0] = localObject;
      str = String.format("Expected leading [0-9a-fA-F] character but was %#x", arrayOfObject);
      localNumberFormatException.<init>(str);
      throw localNumberFormatException;
    }
    label185:
    return a.n();
  }
  
  public final String q()
  {
    return e(Long.MAX_VALUE);
  }
  
  public final byte[] r()
  {
    c localc = a;
    u localu = b;
    localc.a(localu);
    return a.r();
  }
  
  public final int read(ByteBuffer paramByteBuffer)
  {
    Object localObject = a;
    long l1 = b;
    long l2 = 0L;
    boolean bool = l1 < l2;
    if (!bool)
    {
      localObject = b;
      c localc = a;
      l1 = ((u)localObject).a(localc, 8192L);
      l2 = -1;
      bool = l1 < l2;
      if (!bool) {
        return -1;
      }
    }
    return a.read(paramByteBuffer);
  }
  
  public final long t()
  {
    return a((byte)0, 0L, Long.MAX_VALUE);
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("buffer(");
    u localu = b;
    localStringBuilder.append(localu);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     d.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */