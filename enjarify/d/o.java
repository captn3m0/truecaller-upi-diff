package d;

import java.io.OutputStream;
import java.nio.ByteBuffer;

final class o
  implements d
{
  public final c a;
  public final t b;
  boolean c;
  
  o(t paramt)
  {
    c localc = new d/c;
    localc.<init>();
    a = localc;
    if (paramt != null)
    {
      b = paramt;
      return;
    }
    paramt = new java/lang/NullPointerException;
    paramt.<init>("sink == null");
    throw paramt;
  }
  
  public final long a(u paramu)
  {
    if (paramu != null)
    {
      long l1 = 0L;
      for (;;)
      {
        c localc = a;
        long l2 = 8192L;
        long l3 = paramu.a(localc, l2);
        long l4 = -1;
        boolean bool = l3 < l4;
        if (!bool) {
          break;
        }
        l1 += l3;
        v();
      }
      return l1;
    }
    paramu = new java/lang/IllegalArgumentException;
    paramu.<init>("source == null");
    throw paramu;
  }
  
  public final void a_(c paramc, long paramLong)
  {
    boolean bool = c;
    if (!bool)
    {
      a.a_(paramc, paramLong);
      v();
      return;
    }
    paramc = new java/lang/IllegalStateException;
    paramc.<init>("closed");
    throw paramc;
  }
  
  public final c b()
  {
    return a;
  }
  
  public final d b(String paramString)
  {
    boolean bool = c;
    if (!bool)
    {
      a.a(paramString);
      return v();
    }
    paramString = new java/lang/IllegalStateException;
    paramString.<init>("closed");
    throw paramString;
  }
  
  public final d c(f paramf)
  {
    boolean bool = c;
    if (!bool)
    {
      a.a(paramf);
      return v();
    }
    paramf = new java/lang/IllegalStateException;
    paramf.<init>("closed");
    throw paramf;
  }
  
  public final d c(byte[] paramArrayOfByte)
  {
    boolean bool = c;
    if (!bool)
    {
      a.b(paramArrayOfByte);
      return v();
    }
    paramArrayOfByte = new java/lang/IllegalStateException;
    paramArrayOfByte.<init>("closed");
    throw paramArrayOfByte;
  }
  
  public final d c(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    boolean bool = c;
    if (!bool)
    {
      a.b(paramArrayOfByte, paramInt1, paramInt2);
      return v();
    }
    paramArrayOfByte = new java/lang/IllegalStateException;
    paramArrayOfByte.<init>("closed");
    throw paramArrayOfByte;
  }
  
  public final OutputStream c()
  {
    o.1 local1 = new d/o$1;
    local1.<init>(this);
    return local1;
  }
  
  public final void close()
  {
    boolean bool1 = c;
    if (bool1) {
      return;
    }
    bool1 = false;
    Object localObject1 = null;
    Object localObject4;
    try
    {
      localObject4 = a;
      long l1 = b;
      long l2 = 0L;
      boolean bool2 = l1 < l2;
      if (bool2)
      {
        localObject4 = b;
        c localc1 = a;
        c localc2 = a;
        l2 = b;
        ((t)localObject4).a_(localc1, l2);
      }
    }
    finally {}
    Object localObject3;
    try
    {
      localObject4 = b;
      ((t)localObject4).close();
    }
    finally
    {
      if (localObject2 == null) {
        localObject3 = localObject5;
      }
    }
    boolean bool3 = true;
    c = bool3;
    if (localObject3 != null) {
      w.a((Throwable)localObject3);
    }
  }
  
  public final d d()
  {
    boolean bool1 = c;
    if (!bool1)
    {
      localObject = a;
      long l1 = b;
      long l2 = 0L;
      boolean bool2 = l1 < l2;
      if (bool2)
      {
        t localt = b;
        c localc = a;
        localt.a_(localc, l1);
      }
      return this;
    }
    Object localObject = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject).<init>("closed");
    throw ((Throwable)localObject);
  }
  
  public final void flush()
  {
    boolean bool1 = c;
    if (!bool1)
    {
      localObject = a;
      long l1 = b;
      long l2 = 0L;
      boolean bool2 = l1 < l2;
      if (bool2)
      {
        localObject = b;
        c localc = a;
        l2 = b;
        ((t)localObject).a_(localc, l2);
      }
      b.flush();
      return;
    }
    Object localObject = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject).<init>("closed");
    throw ((Throwable)localObject);
  }
  
  public final d g(int paramInt)
  {
    boolean bool = c;
    if (!bool)
    {
      a.e(paramInt);
      return v();
    }
    IllegalStateException localIllegalStateException = new java/lang/IllegalStateException;
    localIllegalStateException.<init>("closed");
    throw localIllegalStateException;
  }
  
  public final d h(int paramInt)
  {
    boolean bool = c;
    if (!bool)
    {
      a.d(paramInt);
      return v();
    }
    IllegalStateException localIllegalStateException = new java/lang/IllegalStateException;
    localIllegalStateException.<init>("closed");
    throw localIllegalStateException;
  }
  
  public final d i(int paramInt)
  {
    boolean bool = c;
    if (!bool)
    {
      a.c(paramInt);
      return v();
    }
    IllegalStateException localIllegalStateException = new java/lang/IllegalStateException;
    localIllegalStateException.<init>("closed");
    throw localIllegalStateException;
  }
  
  public final boolean isOpen()
  {
    boolean bool = c;
    return !bool;
  }
  
  public final d j(int paramInt)
  {
    boolean bool = c;
    if (!bool)
    {
      a.b(paramInt);
      return v();
    }
    IllegalStateException localIllegalStateException = new java/lang/IllegalStateException;
    localIllegalStateException.<init>("closed");
    throw localIllegalStateException;
  }
  
  public final d k(long paramLong)
  {
    boolean bool = c;
    if (!bool)
    {
      a.j(paramLong);
      return v();
    }
    IllegalStateException localIllegalStateException = new java/lang/IllegalStateException;
    localIllegalStateException.<init>("closed");
    throw localIllegalStateException;
  }
  
  public final d l(long paramLong)
  {
    boolean bool = c;
    if (!bool)
    {
      a.i(paramLong);
      return v();
    }
    IllegalStateException localIllegalStateException = new java/lang/IllegalStateException;
    localIllegalStateException.<init>("closed");
    throw localIllegalStateException;
  }
  
  public final v l_()
  {
    return b.l_();
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("buffer(");
    t localt = b;
    localStringBuilder.append(localt);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public final d v()
  {
    boolean bool1 = c;
    if (!bool1)
    {
      localObject = a;
      long l1 = ((c)localObject).g();
      long l2 = 0L;
      boolean bool2 = l1 < l2;
      if (bool2)
      {
        t localt = b;
        c localc = a;
        localt.a_(localc, l1);
      }
      return this;
    }
    Object localObject = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject).<init>("closed");
    throw ((Throwable)localObject);
  }
  
  public final int write(ByteBuffer paramByteBuffer)
  {
    boolean bool = c;
    if (!bool)
    {
      int i = a.write(paramByteBuffer);
      v();
      return i;
    }
    paramByteBuffer = new java/lang/IllegalStateException;
    paramByteBuffer.<init>("closed");
    throw paramByteBuffer;
  }
}

/* Location:
 * Qualified Name:     d.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */