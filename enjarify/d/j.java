package d;

import java.util.concurrent.TimeUnit;

public final class j
  extends v
{
  public v a;
  
  public j(v paramv)
  {
    if (paramv != null)
    {
      a = paramv;
      return;
    }
    paramv = new java/lang/IllegalArgumentException;
    paramv.<init>("delegate == null");
    throw paramv;
  }
  
  public final j a(v paramv)
  {
    if (paramv != null)
    {
      a = paramv;
      return this;
    }
    paramv = new java/lang/IllegalArgumentException;
    paramv.<init>("delegate == null");
    throw paramv;
  }
  
  public final v a(long paramLong)
  {
    return a.a(paramLong);
  }
  
  public final v a(long paramLong, TimeUnit paramTimeUnit)
  {
    return a.a(paramLong, paramTimeUnit);
  }
  
  public final long c()
  {
    return a.c();
  }
  
  public final v d()
  {
    return a.d();
  }
  
  public final void f()
  {
    a.f();
  }
  
  public final long o_()
  {
    return a.o_();
  }
  
  public final boolean p_()
  {
    return a.p_();
  }
  
  public final v q_()
  {
    return a.q_();
  }
}

/* Location:
 * Qualified Name:     d.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */