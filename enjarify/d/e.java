package d;

import java.io.InputStream;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.Charset;

public abstract interface e
  extends u, ReadableByteChannel
{
  public abstract long a(t paramt);
  
  public abstract String a(Charset paramCharset);
  
  public abstract void a(long paramLong);
  
  public abstract void a(byte[] paramArrayOfByte);
  
  public abstract c b();
  
  public abstract boolean b(long paramLong);
  
  public abstract boolean b(f paramf);
  
  public abstract f d(long paramLong);
  
  public abstract String e(long paramLong);
  
  public abstract boolean e();
  
  public abstract InputStream f();
  
  public abstract byte[] g(long paramLong);
  
  public abstract byte h();
  
  public abstract void h(long paramLong);
  
  public abstract short i();
  
  public abstract int j();
  
  public abstract short k();
  
  public abstract int l();
  
  public abstract long m();
  
  public abstract long n();
  
  public abstract String q();
  
  public abstract byte[] r();
  
  public abstract long t();
}

/* Location:
 * Qualified Name:     d.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */