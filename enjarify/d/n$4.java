package d;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;

final class n$4
  extends a
{
  n$4(Socket paramSocket) {}
  
  protected final IOException a(IOException paramIOException)
  {
    SocketTimeoutException localSocketTimeoutException = new java/net/SocketTimeoutException;
    String str = "timeout";
    localSocketTimeoutException.<init>(str);
    if (paramIOException != null) {
      localSocketTimeoutException.initCause(paramIOException);
    }
    return localSocketTimeoutException;
  }
  
  protected final void a()
  {
    try
    {
      Socket localSocket1 = a;
      localSocket1.close();
      return;
    }
    catch (AssertionError localAssertionError)
    {
      boolean bool = n.a(localAssertionError);
      if (bool)
      {
        localLogger = n.a;
        localLevel = Level.WARNING;
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>("Failed to close timed out socket ");
        localSocket2 = a;
        ((StringBuilder)localObject).append(localSocket2);
        localObject = ((StringBuilder)localObject).toString();
        localLogger.log(localLevel, (String)localObject, localAssertionError);
        return;
      }
      throw localAssertionError;
    }
    catch (Exception localException)
    {
      Logger localLogger = n.a;
      Level localLevel = Level.WARNING;
      Object localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>("Failed to close timed out socket ");
      Socket localSocket2 = a;
      ((StringBuilder)localObject).append(localSocket2);
      localObject = ((StringBuilder)localObject).toString();
      localLogger.log(localLevel, (String)localObject, localException);
    }
  }
}

/* Location:
 * Qualified Name:     d.n.4
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */