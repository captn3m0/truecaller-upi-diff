package d;

import java.io.IOException;
import java.io.InputStream;

final class p$1
  extends InputStream
{
  p$1(p paramp) {}
  
  public final int available()
  {
    Object localObject = a;
    boolean bool = c;
    if (!bool) {
      return (int)Math.min(a.a.b, 2147483647L);
    }
    localObject = new java/io/IOException;
    ((IOException)localObject).<init>("closed");
    throw ((Throwable)localObject);
  }
  
  public final void close()
  {
    a.close();
  }
  
  public final int read()
  {
    Object localObject = a;
    boolean bool1 = c;
    if (!bool1)
    {
      localObject = a.a;
      long l1 = b;
      long l2 = 0L;
      boolean bool2 = l1 < l2;
      if (!bool2)
      {
        localObject = a.b;
        c localc = a.a;
        l1 = ((u)localObject).a(localc, 8192L);
        l2 = -1;
        bool2 = l1 < l2;
        if (!bool2) {
          return -1;
        }
      }
      return a.a.h() & 0xFF;
    }
    localObject = new java/io/IOException;
    ((IOException)localObject).<init>("closed");
    throw ((Throwable)localObject);
  }
  
  public final int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    Object localObject = a;
    boolean bool1 = c;
    if (!bool1)
    {
      int i = paramArrayOfByte.length;
      long l1 = i;
      long l2 = paramInt1;
      long l3 = paramInt2;
      w.a(l1, l2, l3);
      localObject = a.a;
      long l4 = b;
      long l5 = 0L;
      boolean bool2 = l4 < l5;
      if (!bool2)
      {
        localObject = a.b;
        c localc = a.a;
        l4 = ((u)localObject).a(localc, 8192L);
        l5 = -1;
        bool2 = l4 < l5;
        if (!bool2) {
          return -1;
        }
      }
      return a.a.a(paramArrayOfByte, paramInt1, paramInt2);
    }
    paramArrayOfByte = new java/io/IOException;
    paramArrayOfByte.<init>("closed");
    throw paramArrayOfByte;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    p localp = a;
    localStringBuilder.append(localp);
    localStringBuilder.append(".inputStream()");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     d.p.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */