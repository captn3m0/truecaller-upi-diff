package d;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.logging.Logger;

public final class n
{
  static final Logger a = Logger.getLogger(n.class.getName());
  
  public static d a(t paramt)
  {
    o localo = new d/o;
    localo.<init>(paramt);
    return localo;
  }
  
  public static e a(u paramu)
  {
    p localp = new d/p;
    localp.<init>(paramu);
    return localp;
  }
  
  public static t a()
  {
    n.3 local3 = new d/n$3;
    local3.<init>();
    return local3;
  }
  
  public static t a(OutputStream paramOutputStream)
  {
    v localv = new d/v;
    localv.<init>();
    return a(paramOutputStream, localv);
  }
  
  private static t a(OutputStream paramOutputStream, v paramv)
  {
    if (paramOutputStream != null)
    {
      if (paramv != null)
      {
        n.1 local1 = new d/n$1;
        local1.<init>(paramv, paramOutputStream);
        return local1;
      }
      paramOutputStream = new java/lang/IllegalArgumentException;
      paramOutputStream.<init>("timeout == null");
      throw paramOutputStream;
    }
    paramOutputStream = new java/lang/IllegalArgumentException;
    paramOutputStream.<init>("out == null");
    throw paramOutputStream;
  }
  
  public static t a(Socket paramSocket)
  {
    if (paramSocket != null)
    {
      Object localObject = paramSocket.getOutputStream();
      if (localObject != null)
      {
        localObject = c(paramSocket);
        paramSocket = a(paramSocket.getOutputStream(), (v)localObject);
        a.1 local1 = new d/a$1;
        local1.<init>((a)localObject, paramSocket);
        return local1;
      }
      paramSocket = new java/io/IOException;
      paramSocket.<init>("socket's output stream == null");
      throw paramSocket;
    }
    paramSocket = new java/lang/IllegalArgumentException;
    paramSocket.<init>("socket == null");
    throw paramSocket;
  }
  
  public static u a(File paramFile)
  {
    if (paramFile != null)
    {
      FileInputStream localFileInputStream = new java/io/FileInputStream;
      localFileInputStream.<init>(paramFile);
      return a(localFileInputStream);
    }
    paramFile = new java/lang/IllegalArgumentException;
    paramFile.<init>("file == null");
    throw paramFile;
  }
  
  public static u a(InputStream paramInputStream)
  {
    v localv = new d/v;
    localv.<init>();
    return a(paramInputStream, localv);
  }
  
  private static u a(InputStream paramInputStream, v paramv)
  {
    if (paramInputStream != null)
    {
      if (paramv != null)
      {
        n.2 local2 = new d/n$2;
        local2.<init>(paramv, paramInputStream);
        return local2;
      }
      paramInputStream = new java/lang/IllegalArgumentException;
      paramInputStream.<init>("timeout == null");
      throw paramInputStream;
    }
    paramInputStream = new java/lang/IllegalArgumentException;
    paramInputStream.<init>("in == null");
    throw paramInputStream;
  }
  
  static boolean a(AssertionError paramAssertionError)
  {
    Object localObject = paramAssertionError.getCause();
    if (localObject != null)
    {
      localObject = paramAssertionError.getMessage();
      if (localObject != null)
      {
        paramAssertionError = paramAssertionError.getMessage();
        localObject = "getsockname failed";
        boolean bool = paramAssertionError.contains((CharSequence)localObject);
        if (bool) {
          return true;
        }
      }
    }
    return false;
  }
  
  public static t b(File paramFile)
  {
    if (paramFile != null)
    {
      FileOutputStream localFileOutputStream = new java/io/FileOutputStream;
      localFileOutputStream.<init>(paramFile);
      return a(localFileOutputStream);
    }
    paramFile = new java/lang/IllegalArgumentException;
    paramFile.<init>("file == null");
    throw paramFile;
  }
  
  public static u b(Socket paramSocket)
  {
    if (paramSocket != null)
    {
      Object localObject = paramSocket.getInputStream();
      if (localObject != null)
      {
        localObject = c(paramSocket);
        paramSocket = a(paramSocket.getInputStream(), (v)localObject);
        a.2 local2 = new d/a$2;
        local2.<init>((a)localObject, paramSocket);
        return local2;
      }
      paramSocket = new java/io/IOException;
      paramSocket.<init>("socket's input stream == null");
      throw paramSocket;
    }
    paramSocket = new java/lang/IllegalArgumentException;
    paramSocket.<init>("socket == null");
    throw paramSocket;
  }
  
  private static a c(Socket paramSocket)
  {
    n.4 local4 = new d/n$4;
    local4.<init>(paramSocket);
    return local4;
  }
  
  public static t c(File paramFile)
  {
    if (paramFile != null)
    {
      FileOutputStream localFileOutputStream = new java/io/FileOutputStream;
      localFileOutputStream.<init>(paramFile, true);
      return a(localFileOutputStream);
    }
    paramFile = new java/lang/IllegalArgumentException;
    paramFile.<init>("file == null");
    throw paramFile;
  }
}

/* Location:
 * Qualified Name:     d.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */