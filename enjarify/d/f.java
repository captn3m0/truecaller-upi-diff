package d;

import java.io.EOFException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class f
  implements Serializable, Comparable
{
  static final char[] a;
  public static final f b = a(new byte[0]);
  private static final long serialVersionUID = 1L;
  final byte[] c;
  transient int d;
  transient String e;
  
  static
  {
    char[] arrayOfChar = new char[16];
    arrayOfChar[0] = 48;
    arrayOfChar[1] = 49;
    arrayOfChar[2] = 50;
    arrayOfChar[3] = 51;
    arrayOfChar[4] = 52;
    arrayOfChar[5] = 53;
    arrayOfChar[6] = 54;
    arrayOfChar[7] = 55;
    arrayOfChar[8] = 56;
    arrayOfChar[9] = 57;
    arrayOfChar[10] = 97;
    arrayOfChar[11] = 98;
    arrayOfChar[12] = 99;
    arrayOfChar[13] = 100;
    arrayOfChar[14] = 101;
    arrayOfChar[15] = 102;
    a = arrayOfChar;
  }
  
  f(byte[] paramArrayOfByte)
  {
    c = paramArrayOfByte;
  }
  
  private static int a(char paramChar)
  {
    char c1 = '0';
    char c2;
    if (paramChar >= c1)
    {
      c2 = '9';
      if (paramChar <= c2) {
        return paramChar - c1;
      }
    }
    c1 = 'a';
    if (paramChar >= c1)
    {
      c2 = 'f';
      if (paramChar <= c2) {
        return paramChar - c1 + 10;
      }
    }
    c1 = 'A';
    if (paramChar >= c1)
    {
      c2 = 'F';
      if (paramChar <= c2) {
        return paramChar - c1 + 10;
      }
    }
    IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
    String str = String.valueOf(paramChar);
    str = "Unexpected hex digit: ".concat(str);
    localIllegalArgumentException.<init>(str);
    throw localIllegalArgumentException;
  }
  
  public static f a(String paramString)
  {
    if (paramString != null)
    {
      f localf = new d/f;
      Object localObject = w.a;
      localObject = paramString.getBytes((Charset)localObject);
      localf.<init>((byte[])localObject);
      e = paramString;
      return localf;
    }
    paramString = new java/lang/IllegalArgumentException;
    paramString.<init>("s == null");
    throw paramString;
  }
  
  public static f a(String paramString, Charset paramCharset)
  {
    if (paramString != null)
    {
      if (paramCharset != null)
      {
        f localf = new d/f;
        paramString = paramString.getBytes(paramCharset);
        localf.<init>(paramString);
        return localf;
      }
      paramString = new java/lang/IllegalArgumentException;
      paramString.<init>("charset == null");
      throw paramString;
    }
    paramString = new java/lang/IllegalArgumentException;
    paramString.<init>("s == null");
    throw paramString;
  }
  
  public static f a(byte... paramVarArgs)
  {
    if (paramVarArgs != null)
    {
      f localf = new d/f;
      paramVarArgs = (byte[])paramVarArgs.clone();
      localf.<init>(paramVarArgs);
      return localf;
    }
    paramVarArgs = new java/lang/IllegalArgumentException;
    paramVarArgs.<init>("data == null");
    throw paramVarArgs;
  }
  
  public static f b(String paramString)
  {
    if (paramString != null)
    {
      paramString = b.a(paramString);
      if (paramString != null)
      {
        f localf = new d/f;
        localf.<init>(paramString);
        return localf;
      }
      return null;
    }
    paramString = new java/lang/IllegalArgumentException;
    paramString.<init>("base64 == null");
    throw paramString;
  }
  
  public static f c(String paramString)
  {
    int i = paramString.length() % 2;
    if (i == 0)
    {
      i = paramString.length() / 2;
      localObject = new byte[i];
      int j = 0;
      for (;;)
      {
        int k = localObject.length;
        if (j >= k) {
          break;
        }
        k = j * 2;
        int m = a(paramString.charAt(k)) << 4;
        k += 1;
        k = a(paramString.charAt(k));
        m += k;
        k = (byte)m;
        localObject[j] = k;
        j += 1;
      }
      return a((byte[])localObject);
    }
    Object localObject = new java/lang/IllegalArgumentException;
    paramString = String.valueOf(paramString);
    paramString = "Unexpected hex string: ".concat(paramString);
    ((IllegalArgumentException)localObject).<init>(paramString);
    throw ((Throwable)localObject);
  }
  
  private f d(String paramString)
  {
    try
    {
      paramString = MessageDigest.getInstance(paramString);
      localObject = c;
      paramString = paramString.digest((byte[])localObject);
      return a(paramString);
    }
    catch (NoSuchAlgorithmException paramString)
    {
      Object localObject = new java/lang/AssertionError;
      ((AssertionError)localObject).<init>(paramString);
      throw ((Throwable)localObject);
    }
  }
  
  private void readObject(ObjectInputStream paramObjectInputStream)
  {
    int i = paramObjectInputStream.readInt();
    if (paramObjectInputStream != null)
    {
      if (i >= 0)
      {
        Object localObject1 = new byte[i];
        int j = 0;
        while (j < i)
        {
          int k = i - j;
          k = paramObjectInputStream.read((byte[])localObject1, j, k);
          int m = -1;
          if (k != m)
          {
            j += k;
          }
          else
          {
            paramObjectInputStream = new java/io/EOFException;
            paramObjectInputStream.<init>();
            throw paramObjectInputStream;
          }
        }
        paramObjectInputStream = new d/f;
        paramObjectInputStream.<init>((byte[])localObject1);
        localObject2 = f.class;
        localObject1 = "c";
        try
        {
          localObject2 = ((Class)localObject2).getDeclaredField((String)localObject1);
          boolean bool = true;
          ((Field)localObject2).setAccessible(bool);
          paramObjectInputStream = c;
          ((Field)localObject2).set(this, paramObjectInputStream);
          return;
        }
        catch (IllegalAccessException localIllegalAccessException)
        {
          paramObjectInputStream = new java/lang/AssertionError;
          paramObjectInputStream.<init>();
          throw paramObjectInputStream;
        }
        catch (NoSuchFieldException localNoSuchFieldException)
        {
          paramObjectInputStream = new java/lang/AssertionError;
          paramObjectInputStream.<init>();
          throw paramObjectInputStream;
        }
      }
      paramObjectInputStream = new java/lang/IllegalArgumentException;
      Object localObject2 = String.valueOf(i);
      localObject2 = "byteCount < 0: ".concat((String)localObject2);
      paramObjectInputStream.<init>((String)localObject2);
      throw paramObjectInputStream;
    }
    paramObjectInputStream = new java/lang/IllegalArgumentException;
    paramObjectInputStream.<init>("in == null");
    throw paramObjectInputStream;
  }
  
  private void writeObject(ObjectOutputStream paramObjectOutputStream)
  {
    int i = c.length;
    paramObjectOutputStream.writeInt(i);
    byte[] arrayOfByte = c;
    paramObjectOutputStream.write(arrayOfByte);
  }
  
  public byte a(int paramInt)
  {
    return c[paramInt];
  }
  
  public f a(int paramInt1, int paramInt2)
  {
    if (paramInt1 >= 0)
    {
      byte[] arrayOfByte = c;
      int i = arrayOfByte.length;
      if (paramInt2 <= i)
      {
        i = paramInt2 - paramInt1;
        if (i >= 0)
        {
          if (paramInt1 == 0)
          {
            j = arrayOfByte.length;
            if (paramInt2 == j) {
              return this;
            }
          }
          localObject1 = new byte[i];
          System.arraycopy(c, paramInt1, localObject1, 0, i);
          localObject2 = new d/f;
          ((f)localObject2).<init>((byte[])localObject1);
          return (f)localObject2;
        }
        localObject2 = new java/lang/IllegalArgumentException;
        ((IllegalArgumentException)localObject2).<init>("endIndex < beginIndex");
        throw ((Throwable)localObject2);
      }
      localObject2 = new java/lang/IllegalArgumentException;
      Object localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>("endIndex > length(");
      int j = c.length;
      ((StringBuilder)localObject1).append(j);
      ((StringBuilder)localObject1).append(")");
      localObject1 = ((StringBuilder)localObject1).toString();
      ((IllegalArgumentException)localObject2).<init>((String)localObject1);
      throw ((Throwable)localObject2);
    }
    Object localObject2 = new java/lang/IllegalArgumentException;
    ((IllegalArgumentException)localObject2).<init>("beginIndex < 0");
    throw ((Throwable)localObject2);
  }
  
  public String a()
  {
    String str = e;
    if (str != null) {
      return str;
    }
    str = new java/lang/String;
    byte[] arrayOfByte = c;
    Charset localCharset = w.a;
    str.<init>(arrayOfByte, localCharset);
    e = str;
    return str;
  }
  
  void a(c paramc)
  {
    byte[] arrayOfByte = c;
    int i = arrayOfByte.length;
    paramc.b(arrayOfByte, 0, i);
  }
  
  public boolean a(int paramInt1, f paramf, int paramInt2, int paramInt3)
  {
    byte[] arrayOfByte = c;
    return paramf.a(0, arrayOfByte, 0, paramInt3);
  }
  
  public boolean a(int paramInt1, byte[] paramArrayOfByte, int paramInt2, int paramInt3)
  {
    if (paramInt1 >= 0)
    {
      byte[] arrayOfByte = c;
      int i = arrayOfByte.length - paramInt3;
      if ((paramInt1 <= i) && (paramInt2 >= 0))
      {
        i = paramArrayOfByte.length - paramInt3;
        if (paramInt2 <= i)
        {
          paramInt1 = w.a(arrayOfByte, paramInt1, paramArrayOfByte, paramInt2, paramInt3);
          if (paramInt1 != 0) {
            return true;
          }
        }
      }
    }
    return false;
  }
  
  public String b()
  {
    return b.a(c);
  }
  
  public f c()
  {
    return d("MD5");
  }
  
  public f d()
  {
    return d("SHA-1");
  }
  
  public f e()
  {
    return d("SHA-256");
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (paramObject == this) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof f;
    if (bool2)
    {
      paramObject = (f)paramObject;
      int i = ((f)paramObject).h();
      byte[] arrayOfByte = c;
      int j = arrayOfByte.length;
      if (i == j)
      {
        i = arrayOfByte.length;
        boolean bool3 = ((f)paramObject).a(0, arrayOfByte, 0, i);
        if (bool3) {
          return bool1;
        }
      }
    }
    return false;
  }
  
  public String f()
  {
    Object localObject = c;
    int i = localObject.length * 2;
    char[] arrayOfChar1 = new char[i];
    int j = localObject.length;
    int k = 0;
    int m = 0;
    while (k < j)
    {
      int n = localObject[k];
      int i1 = m + 1;
      char[] arrayOfChar2 = a;
      int i2 = n >> 4 & 0xF;
      i2 = arrayOfChar2[i2];
      arrayOfChar1[m] = i2;
      m = i1 + 1;
      n &= 0xF;
      n = arrayOfChar2[n];
      arrayOfChar1[i1] = n;
      k += 1;
    }
    localObject = new java/lang/String;
    ((String)localObject).<init>(arrayOfChar1);
    return (String)localObject;
  }
  
  public f g()
  {
    int i = 0;
    f localf = null;
    for (;;)
    {
      byte[] arrayOfByte = c;
      int j = arrayOfByte.length;
      if (i >= j) {
        break;
      }
      j = arrayOfByte[i];
      int k = 65;
      if (j >= k)
      {
        int m = 90;
        if (j <= m)
        {
          arrayOfByte = (byte[])arrayOfByte.clone();
          int n = i + 1;
          j = (byte)(j + 32);
          arrayOfByte[i] = j;
          for (;;)
          {
            i = arrayOfByte.length;
            if (n >= i) {
              break;
            }
            i = arrayOfByte[n];
            if ((i >= k) && (i <= m))
            {
              i = (byte)(i + 32);
              arrayOfByte[n] = i;
            }
            n += 1;
          }
          localf = new d/f;
          localf.<init>(arrayOfByte);
          return localf;
        }
      }
      i += 1;
    }
    return this;
  }
  
  public int h()
  {
    return c.length;
  }
  
  public int hashCode()
  {
    int i = d;
    if (i != 0) {
      return i;
    }
    i = Arrays.hashCode(c);
    d = i;
    return i;
  }
  
  public byte[] i()
  {
    return (byte[])c.clone();
  }
  
  public String toString()
  {
    Object localObject = c;
    int i = localObject.length;
    if (i == 0) {
      return "[size=0]";
    }
    localObject = a();
    int j = ((String)localObject).length();
    int k = 0;
    String str1 = null;
    int m = 0;
    int n = 0;
    String str2 = null;
    int i1;
    int i2;
    for (;;)
    {
      i1 = -1;
      i2 = 64;
      if (m >= j) {
        break;
      }
      if (n == i2) {
        break label153;
      }
      int i3 = ((String)localObject).codePointAt(m);
      boolean bool = Character.isISOControl(i3);
      if (bool)
      {
        i4 = 10;
        if (i3 != i4)
        {
          i4 = 13;
          if (i3 != i4) {
            break label118;
          }
        }
      }
      int i4 = (char)-3;
      if (i3 == i4)
      {
        label118:
        m = -1;
        break label153;
      }
      n += 1;
      i1 = Character.charCount(i3);
      m += i1;
    }
    m = ((String)localObject).length();
    label153:
    if (m == i1)
    {
      localObject = c;
      i = localObject.length;
      if (i <= i2)
      {
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>("[hex=");
        str3 = f();
        ((StringBuilder)localObject).append(str3);
        ((StringBuilder)localObject).append("]");
        return ((StringBuilder)localObject).toString();
      }
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>("[size=");
      j = c.length;
      ((StringBuilder)localObject).append(j);
      ((StringBuilder)localObject).append(" hex=");
      str3 = a(0, i2).f();
      ((StringBuilder)localObject).append(str3);
      ((StringBuilder)localObject).append("…]");
      return ((StringBuilder)localObject).toString();
    }
    String str3 = ((String)localObject).substring(0, m).replace("\\", "\\\\").replace("\n", "\\n");
    str1 = "\r";
    str2 = "\\r";
    str3 = str3.replace(str1, str2);
    i = ((String)localObject).length();
    if (m < i)
    {
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>("[size=");
      k = c.length;
      ((StringBuilder)localObject).append(k);
      ((StringBuilder)localObject).append(" text=");
      ((StringBuilder)localObject).append(str3);
      ((StringBuilder)localObject).append("…]");
      return ((StringBuilder)localObject).toString();
    }
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("[text=");
    ((StringBuilder)localObject).append(str3);
    ((StringBuilder)localObject).append("]");
    return ((StringBuilder)localObject).toString();
  }
}

/* Location:
 * Qualified Name:     d.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */