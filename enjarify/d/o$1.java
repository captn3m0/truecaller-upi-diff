package d;

import java.io.IOException;
import java.io.OutputStream;

final class o$1
  extends OutputStream
{
  o$1(o paramo) {}
  
  public final void close()
  {
    a.close();
  }
  
  public final void flush()
  {
    o localo = a;
    boolean bool = c;
    if (!bool)
    {
      localo = a;
      localo.flush();
    }
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    o localo = a;
    localStringBuilder.append(localo);
    localStringBuilder.append(".outputStream()");
    return localStringBuilder.toString();
  }
  
  public final void write(int paramInt)
  {
    Object localObject = a;
    boolean bool = c;
    if (!bool)
    {
      localObject = a.a;
      paramInt = (byte)paramInt;
      ((c)localObject).b(paramInt);
      a.v();
      return;
    }
    IOException localIOException = new java/io/IOException;
    localIOException.<init>("closed");
    throw localIOException;
  }
  
  public final void write(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    o localo = a;
    boolean bool = c;
    if (!bool)
    {
      a.a.b(paramArrayOfByte, paramInt1, paramInt2);
      a.v();
      return;
    }
    paramArrayOfByte = new java/io/IOException;
    paramArrayOfByte.<init>("closed");
    throw paramArrayOfByte;
  }
}

/* Location:
 * Qualified Name:     d.o.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */