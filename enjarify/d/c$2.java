package d;

import java.io.InputStream;

final class c$2
  extends InputStream
{
  c$2(c paramc) {}
  
  public final int available()
  {
    return (int)Math.min(a.b, 2147483647L);
  }
  
  public final void close() {}
  
  public final int read()
  {
    c localc = a;
    long l1 = b;
    long l2 = 0L;
    boolean bool = l1 < l2;
    if (bool) {
      return a.h() & 0xFF;
    }
    return -1;
  }
  
  public final int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    return a.a(paramArrayOfByte, paramInt1, paramInt2);
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    c localc = a;
    localStringBuilder.append(localc);
    localStringBuilder.append(".inputStream()");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     d.c.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */