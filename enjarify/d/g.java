package d;

import java.util.zip.Deflater;

public final class g
  implements t
{
  private final d a;
  private final Deflater b;
  private boolean c;
  
  g(d paramd, Deflater paramDeflater)
  {
    if (paramd != null)
    {
      if (paramDeflater != null)
      {
        a = paramd;
        b = paramDeflater;
        return;
      }
      paramd = new java/lang/IllegalArgumentException;
      paramd.<init>("inflater == null");
      throw paramd;
    }
    paramd = new java/lang/IllegalArgumentException;
    paramd.<init>("source == null");
    throw paramd;
  }
  
  public g(t paramt, Deflater paramDeflater)
  {
    this(paramt, paramDeflater);
  }
  
  private void a(boolean paramBoolean)
  {
    c localc = a.b();
    Object localObject;
    boolean bool1;
    do
    {
      for (;;)
      {
        int i = 1;
        localObject = localc.f(i);
        byte[] arrayOfByte;
        int j;
        int k;
        int n;
        if (paramBoolean)
        {
          localDeflater = b;
          arrayOfByte = a;
          j = c;
          k = c;
          k = 8192 - k;
          int m = 2;
          n = localDeflater.deflate(arrayOfByte, j, k, m);
        }
        else
        {
          localDeflater = b;
          arrayOfByte = a;
          j = c;
          k = c;
          k = 8192 - k;
          n = localDeflater.deflate(arrayOfByte, j, k);
        }
        if (n <= 0) {
          break;
        }
        int i1 = c + n;
        c = i1;
        long l1 = b;
        long l2 = n;
        l1 += l2;
        b = l1;
        localObject = a;
        ((d)localObject).v();
      }
      Deflater localDeflater = b;
      bool1 = localDeflater.needsInput();
    } while (!bool1);
    paramBoolean = b;
    boolean bool2 = c;
    if (paramBoolean == bool2)
    {
      q localq = ((q)localObject).b();
      a = localq;
      r.a((q)localObject);
    }
  }
  
  public final void a_(c paramc, long paramLong)
  {
    long l1 = b;
    long l2 = 0L;
    w.a(l1, l2, paramLong);
    for (;;)
    {
      l1 = 0L;
      boolean bool = paramLong < l1;
      if (!bool) {
        break;
      }
      q localq = a;
      int j = c;
      int i = b;
      long l3 = j - i;
      l3 = Math.min(paramLong, l3);
      i = (int)l3;
      Object localObject = b;
      byte[] arrayOfByte = a;
      int k = b;
      ((Deflater)localObject).setInput(arrayOfByte, k, i);
      localObject = null;
      a(false);
      long l4 = b;
      long l5 = i;
      l4 -= l5;
      b = l4;
      j = b + i;
      b = j;
      j = b;
      i = c;
      if (j == i)
      {
        localObject = localq.b();
        a = ((q)localObject);
        r.a(localq);
      }
      paramLong -= l5;
    }
  }
  
  final void b()
  {
    b.finish();
    a(false);
  }
  
  /* Error */
  public final void close()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 114	d/g:c	Z
    //   4: istore_1
    //   5: iload_1
    //   6: ifeq +4 -> 10
    //   9: return
    //   10: iconst_0
    //   11: istore_1
    //   12: aconst_null
    //   13: astore_2
    //   14: aload_0
    //   15: invokevirtual 116	d/g:b	()V
    //   18: goto +4 -> 22
    //   21: astore_2
    //   22: aload_0
    //   23: getfield 20	d/g:b	Ljava/util/zip/Deflater;
    //   26: astore_3
    //   27: aload_3
    //   28: invokevirtual 119	java/util/zip/Deflater:end	()V
    //   31: goto +10 -> 41
    //   34: astore_3
    //   35: aload_2
    //   36: ifnonnull +5 -> 41
    //   39: aload_3
    //   40: astore_2
    //   41: aload_0
    //   42: getfield 18	d/g:a	Ld/d;
    //   45: astore_3
    //   46: aload_3
    //   47: invokeinterface 122 1 0
    //   52: goto +10 -> 62
    //   55: astore_3
    //   56: aload_2
    //   57: ifnonnull +5 -> 62
    //   60: aload_3
    //   61: astore_2
    //   62: iconst_1
    //   63: istore 4
    //   65: aload_0
    //   66: iload 4
    //   68: putfield 114	d/g:c	Z
    //   71: aload_2
    //   72: ifnull +7 -> 79
    //   75: aload_2
    //   76: invokestatic 125	d/w:a	(Ljava/lang/Throwable;)V
    //   79: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	80	0	this	g
    //   4	8	1	bool1	boolean
    //   13	1	2	localObject1	Object
    //   21	15	2	localObject2	Object
    //   40	36	2	localObject3	Object
    //   26	2	3	localDeflater	Deflater
    //   34	6	3	localObject4	Object
    //   45	2	3	locald	d
    //   55	6	3	localObject5	Object
    //   63	4	4	bool2	boolean
    // Exception table:
    //   from	to	target	type
    //   14	18	21	finally
    //   22	26	34	finally
    //   27	31	34	finally
    //   41	45	55	finally
    //   46	52	55	finally
  }
  
  public final void flush()
  {
    a(true);
    a.flush();
  }
  
  public final v l_()
  {
    return a.l_();
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("DeflaterSink(");
    d locald = a;
    localStringBuilder.append(locald);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     d.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */