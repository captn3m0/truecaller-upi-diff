package d;

import java.nio.charset.Charset;

final class w
{
  public static final Charset a = Charset.forName("UTF-8");
  
  public static int a(int paramInt)
  {
    int i = (0xFF000000 & paramInt) >>> 24;
    int j = (0xFF0000 & paramInt) >>> 8;
    i |= j;
    j = (0xFF00 & paramInt) << 8;
    i |= j;
    return (paramInt & 0xFF) << 24 | i;
  }
  
  public static short a(short paramShort)
  {
    paramShort &= (char)-1;
    int i = (0xFF00 & paramShort) >>> 8;
    return (short)((paramShort & 0xFF) << 8 | i);
  }
  
  public static void a(long paramLong1, long paramLong2, long paramLong3)
  {
    long l1 = paramLong2 | paramLong3;
    long l2 = 0L;
    boolean bool1 = l1 < l2;
    if (!bool1)
    {
      boolean bool2 = paramLong2 < paramLong1;
      if (!bool2)
      {
        l1 = paramLong1 - paramLong2;
        boolean bool3 = l1 < paramLong3;
        if (!bool3) {
          return;
        }
      }
    }
    ArrayIndexOutOfBoundsException localArrayIndexOutOfBoundsException = new java/lang/ArrayIndexOutOfBoundsException;
    Object[] arrayOfObject = new Object[3];
    Object localObject = Long.valueOf(paramLong1);
    arrayOfObject[0] = localObject;
    Long localLong = Long.valueOf(paramLong2);
    arrayOfObject[1] = localLong;
    localLong = Long.valueOf(paramLong3);
    arrayOfObject[2] = localLong;
    localObject = String.format("size=%s offset=%s byteCount=%s", arrayOfObject);
    localArrayIndexOutOfBoundsException.<init>((String)localObject);
    throw localArrayIndexOutOfBoundsException;
  }
  
  public static void a(Throwable paramThrowable)
  {
    throw paramThrowable;
  }
  
  public static boolean a(byte[] paramArrayOfByte1, int paramInt1, byte[] paramArrayOfByte2, int paramInt2, int paramInt3)
  {
    int i = 0;
    while (i < paramInt3)
    {
      int j = i + paramInt1;
      j = paramArrayOfByte1[j];
      int k = i + paramInt2;
      k = paramArrayOfByte2[k];
      if (j != k) {
        return false;
      }
      i += 1;
    }
    return true;
  }
}

/* Location:
 * Qualified Name:     d.w
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */