package d;

public abstract class i
  implements u
{
  protected final u d;
  
  public i(u paramu)
  {
    if (paramu != null)
    {
      d = paramu;
      return;
    }
    paramu = new java/lang/IllegalArgumentException;
    paramu.<init>("delegate == null");
    throw paramu;
  }
  
  public long a(c paramc, long paramLong)
  {
    return d.a(paramc, paramLong);
  }
  
  public void close()
  {
    d.close();
  }
  
  public final v l_()
  {
    return d.l_();
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    String str = getClass().getSimpleName();
    localStringBuilder.append(str);
    localStringBuilder.append("(");
    str = d.toString();
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     d.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */