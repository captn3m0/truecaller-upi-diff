package d;

import java.io.OutputStream;
import java.nio.channels.WritableByteChannel;

public abstract interface d
  extends t, WritableByteChannel
{
  public abstract long a(u paramu);
  
  public abstract c b();
  
  public abstract d b(String paramString);
  
  public abstract d c(f paramf);
  
  public abstract d c(byte[] paramArrayOfByte);
  
  public abstract d c(byte[] paramArrayOfByte, int paramInt1, int paramInt2);
  
  public abstract OutputStream c();
  
  public abstract d d();
  
  public abstract void flush();
  
  public abstract d g(int paramInt);
  
  public abstract d h(int paramInt);
  
  public abstract d i(int paramInt);
  
  public abstract d j(int paramInt);
  
  public abstract d k(long paramLong);
  
  public abstract d l(long paramLong);
  
  public abstract d v();
}

/* Location:
 * Qualified Name:     d.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */