package d;

import java.io.EOFException;
import java.io.IOException;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

public final class m
  implements u
{
  private final e a;
  private final Inflater b;
  private int c;
  private boolean d;
  
  m(e parame, Inflater paramInflater)
  {
    if (parame != null)
    {
      if (paramInflater != null)
      {
        a = parame;
        b = paramInflater;
        return;
      }
      parame = new java/lang/IllegalArgumentException;
      parame.<init>("inflater == null");
      throw parame;
    }
    parame = new java/lang/IllegalArgumentException;
    parame.<init>("source == null");
    throw parame;
  }
  
  public m(u paramu, Inflater paramInflater)
  {
    this(paramu, paramInflater);
  }
  
  private void c()
  {
    int i = c;
    if (i == 0) {
      return;
    }
    int j = b.getRemaining();
    i -= j;
    j = c - i;
    c = j;
    e locale = a;
    long l = i;
    locale.h(l);
  }
  
  public final long a(c paramc, long paramLong)
  {
    long l1 = 0L;
    boolean bool1 = paramLong < l1;
    if (!bool1)
    {
      bool1 = d;
      if (!bool1)
      {
        bool1 = paramLong < l1;
        if (!bool1) {
          return l1;
        }
        for (;;)
        {
          boolean bool3 = b();
          int j = 1;
          try
          {
            q localq = paramc.f(j);
            int i = c;
            i = 8192 - i;
            long l2 = i;
            l2 = Math.min(paramLong, l2);
            int k = (int)l2;
            Inflater localInflater = b;
            byte[] arrayOfByte = a;
            int m = c;
            i = localInflater.inflate(arrayOfByte, m, k);
            if (i > 0)
            {
              n = c + i;
              c = n;
              paramLong = b;
              l1 = i;
              paramLong += l1;
              b = paramLong;
              return l1;
            }
            localInflater = b;
            boolean bool2 = localInflater.finished();
            if (!bool2)
            {
              localInflater = b;
              bool2 = localInflater.needsDictionary();
              if (!bool2)
              {
                if (!bool3) {
                  continue;
                }
                paramc = new java/io/EOFException;
                localObject = "source exhausted prematurely";
                paramc.<init>((String)localObject);
                throw paramc;
              }
            }
            c();
            int n = b;
            int i1 = c;
            if (n == i1)
            {
              localObject = localq.b();
              a = ((q)localObject);
              r.a(localq);
            }
            return -1;
          }
          catch (DataFormatException paramc)
          {
            localObject = new java/io/IOException;
            ((IOException)localObject).<init>(paramc);
            throw ((Throwable)localObject);
          }
        }
      }
      paramc = new java/lang/IllegalStateException;
      paramc.<init>("closed");
      throw paramc;
    }
    paramc = new java/lang/IllegalArgumentException;
    Object localObject = String.valueOf(paramLong);
    localObject = "byteCount < 0: ".concat((String)localObject);
    paramc.<init>((String)localObject);
    throw paramc;
  }
  
  public final boolean b()
  {
    Object localObject = b;
    boolean bool1 = ((Inflater)localObject).needsInput();
    if (!bool1) {
      return false;
    }
    c();
    localObject = b;
    int i = ((Inflater)localObject).getRemaining();
    if (i == 0)
    {
      localObject = a;
      boolean bool2 = ((e)localObject).e();
      if (bool2) {
        return true;
      }
      localObject = a.b().a;
      int k = c;
      int m = b;
      k -= m;
      c = k;
      Inflater localInflater = b;
      byte[] arrayOfByte = a;
      int j = b;
      int n = c;
      localInflater.setInput(arrayOfByte, j, n);
      return false;
    }
    localObject = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject).<init>("?");
    throw ((Throwable)localObject);
  }
  
  public final void close()
  {
    boolean bool = d;
    if (bool) {
      return;
    }
    b.end();
    d = true;
    a.close();
  }
  
  public final v l_()
  {
    return a.l_();
  }
}

/* Location:
 * Qualified Name:     d.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */