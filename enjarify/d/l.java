package d;

import java.io.EOFException;
import java.io.IOException;
import java.util.zip.CRC32;
import java.util.zip.Inflater;

public final class l
  implements u
{
  private int a = 0;
  private final e b;
  private final Inflater c;
  private final m d;
  private final CRC32 e;
  
  public l(u paramu)
  {
    Object localObject = new java/util/zip/CRC32;
    ((CRC32)localObject).<init>();
    e = ((CRC32)localObject);
    if (paramu != null)
    {
      localObject = new java/util/zip/Inflater;
      ((Inflater)localObject).<init>(true);
      c = ((Inflater)localObject);
      paramu = n.a(paramu);
      b = paramu;
      paramu = new d/m;
      localObject = b;
      Inflater localInflater = c;
      paramu.<init>((e)localObject, localInflater);
      d = paramu;
      return;
    }
    paramu = new java/lang/IllegalArgumentException;
    paramu.<init>("source == null");
    throw paramu;
  }
  
  private void a(c paramc, long paramLong1, long paramLong2)
  {
    boolean bool;
    for (paramc = a;; paramc = f)
    {
      int i = c;
      int j = b;
      i -= j;
      l1 = i;
      bool = paramLong1 < l1;
      if (bool) {
        break;
      }
      i = c;
      j = b;
      i -= j;
      l1 = i;
      paramLong1 -= l1;
    }
    long l1 = 0L;
    for (;;)
    {
      bool = paramLong2 < l1;
      if (!bool) {
        break;
      }
      int k = b;
      int m = (int)(k + paramLong1);
      long l2 = Math.min(c - m, paramLong2);
      int n = (int)l2;
      CRC32 localCRC32 = e;
      byte[] arrayOfByte = a;
      localCRC32.update(arrayOfByte, m, n);
      paramLong1 = n;
      paramLong2 -= paramLong1;
      paramc = f;
      paramLong1 = l1;
    }
  }
  
  private static void a(String paramString, int paramInt1, int paramInt2)
  {
    if (paramInt2 == paramInt1) {
      return;
    }
    IOException localIOException = new java/io/IOException;
    Object[] arrayOfObject = new Object[3];
    arrayOfObject[0] = paramString;
    Integer localInteger1 = Integer.valueOf(paramInt2);
    arrayOfObject[1] = localInteger1;
    Integer localInteger2 = Integer.valueOf(paramInt1);
    arrayOfObject[2] = localInteger2;
    paramString = String.format("%s: actual 0x%08x != expected 0x%08x", arrayOfObject);
    localIOException.<init>(paramString);
    throw localIOException;
  }
  
  public final long a(c paramc, long paramLong)
  {
    l locall = this;
    c localc = paramc;
    long l1 = paramLong;
    long l2 = 0L;
    boolean bool1 = paramLong < l2;
    if (!bool1)
    {
      bool1 = paramLong < l2;
      if (!bool1) {
        return l2;
      }
      int j = a;
      long l3 = -1;
      int i1 = 1;
      long l5;
      long l6;
      Object localObject3;
      int i5;
      if (j == 0)
      {
        b.a(10);
        localObject1 = b.b();
        long l4 = 3;
        int i2 = ((c)localObject1).c(l4);
        j = i2 >> 1 & i1;
        int i3;
        if (j == i1)
        {
          i3 = 1;
        }
        else
        {
          j = 0;
          localObject1 = null;
          i3 = 0;
        }
        if (i3 != 0)
        {
          localObject2 = b.b();
          l5 = 0L;
          l6 = 10;
          localObject1 = this;
          a((c)localObject2, l5, l6);
        }
        j = b.i();
        localObject2 = "ID1ID2";
        int i = 8075;
        a((String)localObject2, i, j);
        localObject1 = b;
        l4 = 8;
        ((e)localObject1).h(l4);
        j = i2 >> 2 & i1;
        if (j == i1)
        {
          localObject1 = b;
          l4 = 2;
          ((e)localObject1).a(l4);
          if (i3 != 0)
          {
            localObject2 = b.b();
            l5 = 0L;
            l6 = 2;
            localObject1 = this;
            a((c)localObject2, l5, l6);
          }
          localObject1 = b.b();
          j = ((c)localObject1).k();
          localObject2 = b;
          l6 = j;
          ((e)localObject2).a(l6);
          if (i3 != 0)
          {
            localObject2 = b.b();
            l5 = 0L;
            localObject1 = this;
            l7 = l6;
            a((c)localObject2, l5, l6);
          }
          else
          {
            l7 = l6;
          }
          localObject1 = b;
          l4 = l7;
          ((e)localObject1).h(l7);
        }
        j = i2 >> 3 & i1;
        long l7 = 1L;
        long l8;
        if (j == i1)
        {
          localObject1 = b;
          l8 = ((e)localObject1).t();
          boolean bool2 = l8 < l3;
          if (bool2)
          {
            if (i3 != 0)
            {
              localObject2 = b.b();
              l5 = 0L;
              l6 = l8 + l7;
              localObject1 = this;
              a((c)localObject2, l5, l6);
            }
            localObject1 = b;
            l4 = l8 + l7;
            ((e)localObject1).h(l4);
          }
          else
          {
            localObject1 = new java/io/EOFException;
            ((EOFException)localObject1).<init>();
            throw ((Throwable)localObject1);
          }
        }
        int k = i2 >> 4 & i1;
        if (k == i1)
        {
          localObject1 = b;
          l8 = ((e)localObject1).t();
          boolean bool3 = l8 < l3;
          if (bool3)
          {
            if (i3 != 0)
            {
              localObject2 = b.b();
              l5 = 0L;
              l6 = l8 + l7;
              localObject1 = this;
              a((c)localObject2, l5, l6);
            }
            localObject1 = b;
            l4 = l8 + l7;
            ((e)localObject1).h(l4);
          }
          else
          {
            localObject1 = new java/io/EOFException;
            ((EOFException)localObject1).<init>();
            throw ((Throwable)localObject1);
          }
        }
        if (i3 != 0)
        {
          localObject2 = b;
          i4 = ((e)localObject2).k();
          localObject3 = e;
          l5 = ((CRC32)localObject3).getValue();
          i5 = (int)l5;
          i = (short)i5;
          a("FHCRC", i4, i);
          localObject1 = e;
          ((CRC32)localObject1).reset();
        }
        a = i1;
      }
      int m = a;
      int i4 = 2;
      if (m == i1)
      {
        l5 = b;
        localObject1 = d;
        l1 = ((m)localObject1).a(localc, l1);
        boolean bool4 = l1 < l3;
        if (bool4)
        {
          localObject1 = this;
          localObject2 = paramc;
          l6 = l1;
          a(paramc, l5, l1);
          return l1;
        }
        a = i4;
      }
      int n = a;
      if (n == i4)
      {
        i4 = b.l();
        i5 = (int)e.getValue();
        a("CRC", i4, i5);
        localObject2 = b;
        i4 = ((e)localObject2).l();
        localObject3 = c;
        l5 = ((Inflater)localObject3).getBytesWritten();
        i5 = (int)l5;
        a("ISIZE", i4, i5);
        a = 3;
        localObject1 = b;
        boolean bool5 = ((e)localObject1).e();
        if (!bool5)
        {
          localObject1 = new java/io/IOException;
          ((IOException)localObject1).<init>("gzip finished without exhausting source");
          throw ((Throwable)localObject1);
        }
      }
      return l3;
    }
    Object localObject1 = new java/lang/IllegalArgumentException;
    Object localObject2 = String.valueOf(paramLong);
    localObject2 = "byteCount < 0: ".concat((String)localObject2);
    ((IllegalArgumentException)localObject1).<init>((String)localObject2);
    throw ((Throwable)localObject1);
  }
  
  public final void close()
  {
    d.close();
  }
  
  public final v l_()
  {
    return b.l_();
  }
}

/* Location:
 * Qualified Name:     d.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */