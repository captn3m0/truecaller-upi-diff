package d;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.concurrent.TimeUnit;

public class a
  extends v
{
  private static final long a = TimeUnit.SECONDS.toMillis(60);
  static a b;
  private static final long d;
  private boolean e;
  private a f;
  private long g;
  
  static
  {
    TimeUnit localTimeUnit = TimeUnit.MILLISECONDS;
    long l = a;
    d = localTimeUnit.toNanos(l);
  }
  
  private static void a(a parama, long paramLong, boolean paramBoolean)
  {
    synchronized (a.class)
    {
      Object localObject = b;
      if (localObject == null)
      {
        localObject = new d/a;
        ((a)localObject).<init>();
        b = (a)localObject;
        localObject = new d/a$a;
        ((a.a)localObject).<init>();
        ((a.a)localObject).start();
      }
      long l1 = System.nanoTime();
      long l2 = 0L;
      boolean bool = paramLong < l2;
      if ((bool) && (paramBoolean))
      {
        l2 = parama.c() - l1;
        paramLong = Math.min(paramLong, l2) + l1;
        g = paramLong;
      }
      else
      {
        bool = paramLong < l2;
        if (bool)
        {
          paramLong += l1;
          g = paramLong;
        }
        else
        {
          if (!paramBoolean) {
            break label245;
          }
          paramLong = parama.c();
          g = paramLong;
        }
      }
      paramLong = g - l1;
      for (a locala1 = b;; locala1 = f)
      {
        a locala2 = f;
        if (locala2 == null) {
          break;
        }
        locala2 = f;
        l2 = g - l1;
        bool = paramLong < l2;
        if (bool) {
          break;
        }
      }
      a locala3 = f;
      f = locala3;
      f = parama;
      parama = b;
      if (locala1 == parama)
      {
        parama = a.class;
        parama.notify();
        return;
      }
      return;
      label245:
      parama = new java/lang/AssertionError;
      parama.<init>();
      throw parama;
    }
  }
  
  private static boolean a(a parama)
  {
    synchronized (a.class)
    {
      for (a locala1 = b; locala1 != null; locala1 = f)
      {
        a locala2 = f;
        if (locala2 == parama)
        {
          locala2 = f;
          f = locala2;
          locala1 = null;
          f = null;
          return false;
        }
      }
      return true;
    }
  }
  
  static a e()
  {
    a locala1 = bf;
    if (locala1 == null)
    {
      l1 = System.nanoTime();
      l2 = a;
      a.class.wait(l2);
      locala1 = bf;
      if (locala1 == null)
      {
        l2 = System.nanoTime() - l1;
        l1 = d;
        boolean bool1 = l2 < l1;
        if (!bool1) {
          return b;
        }
      }
      return null;
    }
    long l1 = System.nanoTime();
    long l2 = g - l1;
    l1 = 0L;
    boolean bool2 = l2 < l1;
    if (bool2)
    {
      l1 = 1000000L;
      long l3 = l2 / l1;
      l1 *= l3;
      int i = (int)(l2 - l1);
      a.class.wait(l3, i);
      return null;
    }
    a locala2 = b;
    a locala3 = f;
    f = locala3;
    f = null;
    return locala1;
  }
  
  protected IOException a(IOException paramIOException)
  {
    InterruptedIOException localInterruptedIOException = new java/io/InterruptedIOException;
    String str = "timeout";
    localInterruptedIOException.<init>(str);
    if (paramIOException != null) {
      localInterruptedIOException.initCause(paramIOException);
    }
    return localInterruptedIOException;
  }
  
  protected void a() {}
  
  final void a(boolean paramBoolean)
  {
    boolean bool = n_();
    if ((bool) && (paramBoolean)) {
      throw a(null);
    }
  }
  
  final IOException b(IOException paramIOException)
  {
    boolean bool = n_();
    if (!bool) {
      return paramIOException;
    }
    return a(paramIOException);
  }
  
  public final void m_()
  {
    boolean bool1 = e;
    if (!bool1)
    {
      long l1 = o_();
      boolean bool2 = p_();
      long l2 = 0L;
      boolean bool3 = l1 < l2;
      if ((!bool3) && (!bool2)) {
        return;
      }
      e = true;
      a(this, l1, bool2);
      return;
    }
    IllegalStateException localIllegalStateException = new java/lang/IllegalStateException;
    localIllegalStateException.<init>("Unbalanced enter/exit");
    throw localIllegalStateException;
  }
  
  public final boolean n_()
  {
    boolean bool = e;
    if (!bool) {
      return false;
    }
    e = false;
    return a(this);
  }
}

/* Location:
 * Qualified Name:     d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */