package org.a.a.c;

import org.a.a.i;
import org.a.a.j;

public final class o
  extends e
{
  private static final long serialVersionUID = -3205227092378684157L;
  private final int b = 100;
  
  public o(i parami, j paramj)
  {
    super(parami, paramj);
  }
  
  public final long a(long paramLong, int paramInt)
  {
    long l1 = paramInt;
    long l2 = b;
    l1 *= l2;
    return a.a(paramLong, l1);
  }
  
  public final long a(long paramLong1, long paramLong2)
  {
    int i = b;
    paramLong2 = g.a(paramLong2, i);
    return a.a(paramLong1, paramLong2);
  }
  
  public final int b(long paramLong1, long paramLong2)
  {
    int i = a.b(paramLong1, paramLong2);
    int j = b;
    return i / j;
  }
  
  public final long c(long paramLong1, long paramLong2)
  {
    paramLong1 = a.c(paramLong1, paramLong2);
    paramLong2 = b;
    return paramLong1 / paramLong2;
  }
  
  public final long d()
  {
    long l1 = a.d();
    long l2 = b;
    return l1 * l2;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this == paramObject) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof o;
    if (bool2)
    {
      paramObject = (o)paramObject;
      Object localObject1 = a;
      Object localObject2 = a;
      bool2 = localObject1.equals(localObject2);
      if (bool2)
      {
        localObject1 = d;
        localObject2 = d;
        if (localObject1 == localObject2)
        {
          int i = b;
          int j = b;
          if (i == j) {
            return bool1;
          }
        }
      }
      return false;
    }
    return false;
  }
  
  public final int hashCode()
  {
    long l1 = b;
    long l2 = l1 >>> 32;
    int i = (int)(l1 ^ l2);
    int j = d.hashCode();
    i += j;
    j = a.hashCode();
    return i + j;
  }
}

/* Location:
 * Qualified Name:     org.a.a.c.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */