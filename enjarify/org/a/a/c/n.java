package org.a.a.c;

import org.a.a.c;
import org.a.a.i;

public final class n
  extends d
{
  final int a;
  final i c;
  final i d;
  
  public n(f paramf)
  {
    this(paramf, locald);
  }
  
  public n(f paramf, org.a.a.d paramd)
  {
    this(paramf, locali, paramd);
  }
  
  public n(f paramf, i parami, org.a.a.d paramd)
  {
    super(localc, paramd);
    int i = a;
    a = i;
    c = parami;
    paramf = c;
    d = paramf;
  }
  
  public n(c paramc, i parami, org.a.a.d paramd)
  {
    super(paramc, paramd);
    d = parami;
    paramc = paramc.d();
    c = paramc;
    a = 100;
  }
  
  public final int a(long paramLong)
  {
    c localc = b;
    int i = localc.a(paramLong);
    if (i >= 0)
    {
      j = a;
      return i % j;
    }
    int j = a;
    int k = j + -1;
    i = (i + 1) % j;
    return k + i;
  }
  
  public final long b(long paramLong, int paramInt)
  {
    int i = a + -1;
    int j = 0;
    c localc1 = null;
    g.a(this, paramInt, 0, i);
    c localc2 = b;
    i = localc2.a(paramLong);
    if (i >= 0)
    {
      j = a;
      i /= j;
    }
    else
    {
      i += 1;
      j = a;
      i = i / j + -1;
    }
    localc1 = b;
    int k = a;
    i = i * k + paramInt;
    return localc1.b(paramLong, i);
  }
  
  public final long d(long paramLong)
  {
    return b.d(paramLong);
  }
  
  public final i d()
  {
    return c;
  }
  
  public final long e(long paramLong)
  {
    return b.e(paramLong);
  }
  
  public final i e()
  {
    return d;
  }
  
  public final long f(long paramLong)
  {
    return b.f(paramLong);
  }
  
  public final int g()
  {
    return 0;
  }
  
  public final long g(long paramLong)
  {
    return b.g(paramLong);
  }
  
  public final int h()
  {
    return a + -1;
  }
  
  public final long h(long paramLong)
  {
    return b.h(paramLong);
  }
  
  public final long i(long paramLong)
  {
    return b.i(paramLong);
  }
}

/* Location:
 * Qualified Name:     org.a.a.c.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */