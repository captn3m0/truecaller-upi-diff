package org.a.a.c;

import java.io.Serializable;
import org.a.a.i;
import org.a.a.j;

public abstract class c
  extends i
  implements Serializable
{
  private static final long serialVersionUID = -2554245107589433218L;
  final j d;
  
  protected c(j paramj)
  {
    if (paramj != null)
    {
      d = paramj;
      return;
    }
    paramj = new java/lang/IllegalArgumentException;
    paramj.<init>("The type must not be null");
    throw paramj;
  }
  
  public final j a()
  {
    return d;
  }
  
  public int b(long paramLong1, long paramLong2)
  {
    return g.a(c(paramLong1, paramLong2));
  }
  
  public final boolean b()
  {
    return true;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("DurationField[");
    String str = d.m;
    localStringBuilder.append(str);
    localStringBuilder.append(']');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     org.a.a.c.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */