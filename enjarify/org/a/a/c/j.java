package org.a.a.c;

import org.a.a.c;
import org.a.a.i;

public final class j
  extends d
{
  private final int a;
  private final int c;
  private final int d;
  
  public j(c paramc)
  {
    this(paramc, locald, 99);
  }
  
  public j(c paramc, org.a.a.d paramd)
  {
    this(paramc, paramd, 1);
  }
  
  private j(c paramc, org.a.a.d paramd, int paramInt)
  {
    super(paramc, paramd);
    if (paramInt != 0)
    {
      a = paramInt;
      int i = paramc.g() + paramInt;
      int j = -1 << -1;
      if (j < i)
      {
        i = paramc.g() + paramInt;
        c = i;
      }
      else
      {
        c = j;
      }
      i = paramc.h() + paramInt;
      j = -1 >>> 1;
      if (j > i)
      {
        int k = paramc.h() + paramInt;
        d = k;
        return;
      }
      d = j;
      return;
    }
    paramc = new java/lang/IllegalArgumentException;
    paramc.<init>("The offset cannot be zero");
    throw paramc;
  }
  
  public final int a(long paramLong)
  {
    int i = super.a(paramLong);
    int j = a;
    return i + j;
  }
  
  public final long a(long paramLong, int paramInt)
  {
    paramLong = super.a(paramLong, paramInt);
    paramInt = a(paramLong);
    int i = c;
    int j = d;
    g.a(this, paramInt, i, j);
    return paramLong;
  }
  
  public final long a(long paramLong1, long paramLong2)
  {
    paramLong1 = super.a(paramLong1, paramLong2);
    int i = a(paramLong1);
    int j = c;
    int k = d;
    g.a(this, i, j, k);
    return paramLong1;
  }
  
  public final long b(long paramLong, int paramInt)
  {
    int i = c;
    int j = d;
    g.a(this, paramInt, i, j);
    i = a;
    paramInt -= i;
    return super.b(paramLong, paramInt);
  }
  
  public final boolean b(long paramLong)
  {
    return b.b(paramLong);
  }
  
  public final long d(long paramLong)
  {
    return b.d(paramLong);
  }
  
  public final long e(long paramLong)
  {
    return b.e(paramLong);
  }
  
  public final long f(long paramLong)
  {
    return b.f(paramLong);
  }
  
  public final i f()
  {
    return b.f();
  }
  
  public final int g()
  {
    return c;
  }
  
  public final long g(long paramLong)
  {
    return b.g(paramLong);
  }
  
  public final int h()
  {
    return d;
  }
  
  public final long h(long paramLong)
  {
    return b.h(paramLong);
  }
  
  public final long i(long paramLong)
  {
    return b.i(paramLong);
  }
}

/* Location:
 * Qualified Name:     org.a.a.c.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */