package org.a.a.c;

import java.io.Serializable;
import java.util.Locale;
import org.a.a.c;
import org.a.a.d;

public abstract class a
  implements Serializable
{
  private static final long serialVersionUID = 1971226328211649661L;
  
  private int d()
  {
    c localc = a();
    long l = b();
    return localc.a(l);
  }
  
  public final String a(Locale paramLocale)
  {
    c localc = a();
    long l = b();
    return localc.a(l, paramLocale);
  }
  
  public abstract c a();
  
  protected abstract long b();
  
  public final String b(Locale paramLocale)
  {
    c localc = a();
    long l = b();
    return localc.b(l, paramLocale);
  }
  
  protected org.a.a.a c()
  {
    UnsupportedOperationException localUnsupportedOperationException = new java/lang/UnsupportedOperationException;
    localUnsupportedOperationException.<init>("The method getChronology() was added in v1.4 and needs to be implemented by subclasses of AbstractReadableInstantFieldProperty");
    throw localUnsupportedOperationException;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this == paramObject) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof a;
    if (!bool2) {
      return false;
    }
    paramObject = (a)paramObject;
    int i = d();
    int j = ((a)paramObject).d();
    if (i == j)
    {
      Object localObject = a().a();
      d locald = ((a)paramObject).a().a();
      boolean bool3 = localObject.equals(locald);
      if (bool3)
      {
        localObject = c();
        paramObject = ((a)paramObject).c();
        boolean bool4 = g.a(localObject, paramObject);
        if (bool4) {
          return bool1;
        }
      }
    }
    return false;
  }
  
  public int hashCode()
  {
    int i = d() * 17;
    int j = a().a().hashCode();
    i += j;
    j = c().hashCode();
    return i + j;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Property[");
    String str = a().b();
    localStringBuilder.append(str);
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     org.a.a.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */