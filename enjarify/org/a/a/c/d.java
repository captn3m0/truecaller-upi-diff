package org.a.a.c;

import org.a.a.c;
import org.a.a.i;

public abstract class d
  extends b
{
  protected final c b;
  
  protected d(c paramc, org.a.a.d paramd)
  {
    super(paramd);
    if (paramc != null)
    {
      boolean bool = paramc.c();
      if (bool)
      {
        b = paramc;
        return;
      }
      paramc = new java/lang/IllegalArgumentException;
      paramc.<init>("The field must be supported");
      throw paramc;
    }
    paramc = new java/lang/IllegalArgumentException;
    paramc.<init>("The field must not be null");
    throw paramc;
  }
  
  public int a(long paramLong)
  {
    return b.a(paramLong);
  }
  
  public long b(long paramLong, int paramInt)
  {
    return b.b(paramLong, paramInt);
  }
  
  public long d(long paramLong)
  {
    return b.d(paramLong);
  }
  
  public i d()
  {
    return b.d();
  }
  
  public i e()
  {
    return b.e();
  }
  
  public int g()
  {
    return b.g();
  }
  
  public int h()
  {
    return b.h();
  }
}

/* Location:
 * Qualified Name:     org.a.a.c.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */