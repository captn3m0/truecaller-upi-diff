package org.a.a.c;

import java.io.Serializable;
import org.a.a.j;

public final class i
  extends org.a.a.i
  implements Serializable
{
  public static final org.a.a.i a;
  private static final long serialVersionUID = 2656707858124633367L;
  
  static
  {
    i locali = new org/a/a/c/i;
    locali.<init>();
    a = locali;
  }
  
  private Object readResolve()
  {
    return a;
  }
  
  public final long a(long paramLong, int paramInt)
  {
    long l = paramInt;
    return g.a(paramLong, l);
  }
  
  public final long a(long paramLong1, long paramLong2)
  {
    return g.a(paramLong1, paramLong2);
  }
  
  public final j a()
  {
    return j.a();
  }
  
  public final int b(long paramLong1, long paramLong2)
  {
    return g.a(g.b(paramLong1, paramLong2));
  }
  
  public final boolean b()
  {
    return true;
  }
  
  public final long c(long paramLong1, long paramLong2)
  {
    return g.b(paramLong1, paramLong2);
  }
  
  public final boolean c()
  {
    return true;
  }
  
  public final long d()
  {
    return 1L;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool = paramObject instanceof i;
    return bool;
  }
  
  public final int hashCode()
  {
    return 1;
  }
  
  public final String toString()
  {
    return "DurationField[millis]";
  }
}

/* Location:
 * Qualified Name:     org.a.a.c.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */