package org.a.a.c;

import org.a.a.d;
import org.a.a.i;

public class k
  extends l
{
  private final int b;
  private final i c;
  
  public k(d paramd, i parami1, i parami2)
  {
    super(paramd, parami1);
    boolean bool = parami2.c();
    if (bool)
    {
      long l1 = parami2.d();
      long l2 = a;
      l1 /= l2;
      int j = (int)l1;
      b = j;
      int i = b;
      j = 2;
      if (i >= j)
      {
        c = parami2;
        return;
      }
      paramd = new java/lang/IllegalArgumentException;
      paramd.<init>("The effective range must be at least 2");
      throw paramd;
    }
    paramd = new java/lang/IllegalArgumentException;
    paramd.<init>("Range duration field must be precise");
    throw paramd;
  }
  
  public final int a(long paramLong)
  {
    long l1 = 0L;
    boolean bool = paramLong < l1;
    if (!bool)
    {
      l1 = a;
      paramLong /= l1;
      l1 = b;
      return (int)(paramLong % l1);
    }
    int i = b + -1;
    paramLong += 1L;
    long l2 = a;
    paramLong /= l2;
    l2 = b;
    int j = (int)(paramLong % l2);
    return i + j;
  }
  
  public final long b(long paramLong, int paramInt)
  {
    int i = g();
    int j = b + -1;
    g.a(this, paramInt, i, j);
    i = a(paramLong);
    long l1 = paramInt - i;
    long l2 = a;
    l1 *= l2;
    return paramLong + l1;
  }
  
  public final i e()
  {
    return c;
  }
  
  public final int h()
  {
    return b + -1;
  }
}

/* Location:
 * Qualified Name:     org.a.a.c.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */