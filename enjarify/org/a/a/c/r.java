package org.a.a.c;

import org.a.a.c;
import org.a.a.i;

public final class r
  extends d
{
  public r(c paramc, org.a.a.d paramd)
  {
    super(paramc, paramd);
    int i = paramc.g();
    if (i == 0) {
      return;
    }
    paramc = new java/lang/IllegalArgumentException;
    paramc.<init>("Wrapped field's minumum value must be zero");
    throw paramc;
  }
  
  public final int a(long paramLong)
  {
    c localc = b;
    int i = localc.a(paramLong);
    if (i == 0) {
      i = h();
    }
    return i;
  }
  
  public final long a(long paramLong, int paramInt)
  {
    return b.a(paramLong, paramInt);
  }
  
  public final long a(long paramLong1, long paramLong2)
  {
    return b.a(paramLong1, paramLong2);
  }
  
  public final int b(long paramLong1, long paramLong2)
  {
    return b.b(paramLong1, paramLong2);
  }
  
  public final long b(long paramLong, int paramInt)
  {
    int i = h();
    int j = 1;
    g.a(this, paramInt, j, i);
    if (paramInt == i) {
      paramInt = 0;
    }
    return b.b(paramLong, paramInt);
  }
  
  public final boolean b(long paramLong)
  {
    return b.b(paramLong);
  }
  
  public final int c(long paramLong)
  {
    return b.c(paramLong) + 1;
  }
  
  public final long c(long paramLong1, long paramLong2)
  {
    return b.c(paramLong1, paramLong2);
  }
  
  public final long d(long paramLong)
  {
    return b.d(paramLong);
  }
  
  public final long e(long paramLong)
  {
    return b.e(paramLong);
  }
  
  public final long f(long paramLong)
  {
    return b.f(paramLong);
  }
  
  public final i f()
  {
    return b.f();
  }
  
  public final int g()
  {
    return 1;
  }
  
  public final long g(long paramLong)
  {
    return b.g(paramLong);
  }
  
  public final int h()
  {
    return b.h() + 1;
  }
  
  public final long h(long paramLong)
  {
    return b.h(paramLong);
  }
  
  public final long i(long paramLong)
  {
    return b.i(paramLong);
  }
}

/* Location:
 * Qualified Name:     org.a.a.c.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */