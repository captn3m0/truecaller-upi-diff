package org.a.a.c;

import org.a.a.d;
import org.a.a.i;

public abstract class l
  extends b
{
  final long a;
  private final i b;
  
  public l(d paramd, i parami)
  {
    super(paramd);
    boolean bool = parami.c();
    if (bool)
    {
      long l1 = parami.d();
      a = l1;
      l1 = a;
      long l2 = 1L;
      bool = l1 < l2;
      if (!bool)
      {
        b = parami;
        return;
      }
      paramd = new java/lang/IllegalArgumentException;
      paramd.<init>("The unit milliseconds must be at least 1");
      throw paramd;
    }
    paramd = new java/lang/IllegalArgumentException;
    paramd.<init>("Unit duration field must be precise");
    throw paramd;
  }
  
  public long b(long paramLong, int paramInt)
  {
    int i = g();
    int j = d(paramLong, paramInt);
    g.a(this, paramInt, i, j);
    i = a(paramLong);
    long l1 = paramInt - i;
    long l2 = a;
    l1 *= l2;
    return paramLong + l1;
  }
  
  protected int d(long paramLong, int paramInt)
  {
    return c(paramLong);
  }
  
  public long d(long paramLong)
  {
    long l1 = 0L;
    boolean bool = paramLong < l1;
    if (!bool)
    {
      l1 = a;
      l1 = paramLong % l1;
      return paramLong - l1;
    }
    paramLong += 1L;
    l1 = a;
    long l2 = paramLong % l1;
    return paramLong - l2 - l1;
  }
  
  public final i d()
  {
    return b;
  }
  
  public long e(long paramLong)
  {
    long l1 = 0L;
    boolean bool = paramLong < l1;
    if (bool)
    {
      paramLong -= 1L;
      l1 = a;
      long l2 = paramLong % l1;
      return paramLong - l2 + l1;
    }
    l1 = a;
    l1 = paramLong % l1;
    return paramLong - l1;
  }
  
  public int g()
  {
    return 0;
  }
  
  public long i(long paramLong)
  {
    long l1 = 0L;
    boolean bool = paramLong < l1;
    if (!bool)
    {
      l1 = a;
      return paramLong % l1;
    }
    l1 = 1L;
    paramLong += l1;
    long l2 = a;
    return paramLong % l2 + l2 - l1;
  }
}

/* Location:
 * Qualified Name:     org.a.a.c.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */