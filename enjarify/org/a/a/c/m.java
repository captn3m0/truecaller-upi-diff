package org.a.a.c;

import org.a.a.j;

public final class m
  extends c
{
  private static final long serialVersionUID = -8346152187724495365L;
  private final long a;
  
  public m(j paramj, long paramLong)
  {
    super(paramj);
    a = paramLong;
  }
  
  public final long a(long paramLong, int paramInt)
  {
    long l1 = paramInt;
    long l2 = a;
    l1 *= l2;
    return g.a(paramLong, l1);
  }
  
  public final long a(long paramLong1, long paramLong2)
  {
    long l1 = a;
    long l2 = 1L;
    long l3 = 0L;
    boolean bool1 = l1 < l2;
    if (bool1)
    {
      bool1 = paramLong2 < l2;
      if (!bool1)
      {
        paramLong2 = l1;
      }
      else
      {
        boolean bool2 = paramLong2 < l3;
        if (bool2)
        {
          bool2 = l1 < l3;
          if (bool2)
          {
            l2 = paramLong2 * l1;
            l3 = l2 / l1;
            bool1 = l3 < paramLong2;
            if (!bool1)
            {
              l3 = -1;
              long l4 = Long.MIN_VALUE;
              boolean bool3 = paramLong2 < l4;
              if (!bool3)
              {
                bool3 = l1 < l3;
                if (!bool3) {}
              }
              else
              {
                bool3 = l1 < l4;
                if (!bool3)
                {
                  bool1 = paramLong2 < l3;
                  if (!bool1) {}
                }
                else
                {
                  paramLong2 = l2;
                  break label215;
                }
              }
            }
            ArithmeticException localArithmeticException = new java/lang/ArithmeticException;
            Object localObject = new java/lang/StringBuilder;
            ((StringBuilder)localObject).<init>("Multiplication overflows a long: ");
            ((StringBuilder)localObject).append(paramLong2);
            ((StringBuilder)localObject).append(" * ");
            ((StringBuilder)localObject).append(l1);
            localObject = ((StringBuilder)localObject).toString();
            localArithmeticException.<init>((String)localObject);
            throw localArithmeticException;
          }
        }
        paramLong2 = l3;
      }
    }
    label215:
    return g.a(paramLong1, paramLong2);
  }
  
  public final long c(long paramLong1, long paramLong2)
  {
    paramLong1 = g.b(paramLong1, paramLong2);
    paramLong2 = a;
    return paramLong1 / paramLong2;
  }
  
  public final boolean c()
  {
    return true;
  }
  
  public final long d()
  {
    return a;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this == paramObject) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof m;
    if (bool2)
    {
      paramObject = (m)paramObject;
      j localj1 = d;
      j localj2 = d;
      if (localj1 == localj2)
      {
        long l1 = a;
        long l2 = a;
        boolean bool3 = l1 < l2;
        if (!bool3) {
          return bool1;
        }
      }
      return false;
    }
    return false;
  }
  
  public final int hashCode()
  {
    long l1 = a;
    long l2 = l1 >>> 32;
    int i = (int)(l1 ^ l2);
    int j = d.hashCode();
    return i + j;
  }
}

/* Location:
 * Qualified Name:     org.a.a.c.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */