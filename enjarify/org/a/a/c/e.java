package org.a.a.c;

import org.a.a.i;
import org.a.a.j;

public class e
  extends c
{
  private static final long serialVersionUID = 8019982251647420015L;
  final i a;
  
  public e(i parami, j paramj)
  {
    super(paramj);
    if (parami != null)
    {
      boolean bool = parami.b();
      if (bool)
      {
        a = parami;
        return;
      }
      parami = new java/lang/IllegalArgumentException;
      parami.<init>("The field must be supported");
      throw parami;
    }
    parami = new java/lang/IllegalArgumentException;
    parami.<init>("The field must not be null");
    throw parami;
  }
  
  public long a(long paramLong, int paramInt)
  {
    return a.a(paramLong, paramInt);
  }
  
  public long a(long paramLong1, long paramLong2)
  {
    return a.a(paramLong1, paramLong2);
  }
  
  public long c(long paramLong1, long paramLong2)
  {
    return a.c(paramLong1, paramLong2);
  }
  
  public final boolean c()
  {
    return a.c();
  }
  
  public long d()
  {
    return a.d();
  }
}

/* Location:
 * Qualified Name:     org.a.a.c.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */