package org.a.a.c;

import java.util.Locale;
import org.a.a.c;
import org.a.a.d;
import org.a.a.i;
import org.a.a.k;
import org.a.a.z;

public abstract class b
  extends c
{
  final d g;
  
  protected b(d paramd)
  {
    if (paramd != null)
    {
      g = paramd;
      return;
    }
    paramd = new java/lang/IllegalArgumentException;
    paramd.<init>("The type must not be null");
    throw paramd;
  }
  
  public abstract int a(long paramLong);
  
  protected int a(String paramString, Locale paramLocale)
  {
    try
    {
      return Integer.parseInt(paramString);
    }
    catch (NumberFormatException localNumberFormatException)
    {
      paramLocale = new org/a/a/k;
      d locald = g;
      paramLocale.<init>(locald, paramString);
      throw paramLocale;
    }
  }
  
  public int a(Locale paramLocale)
  {
    int i = h();
    if (i >= 0)
    {
      int j = 10;
      if (i < j) {
        return 1;
      }
      j = 100;
      if (i < j) {
        return 2;
      }
      j = 1000;
      if (i < j) {
        return 3;
      }
    }
    return Integer.toString(i).length();
  }
  
  public long a(long paramLong, int paramInt)
  {
    return d().a(paramLong, paramInt);
  }
  
  public long a(long paramLong1, long paramLong2)
  {
    return d().a(paramLong1, paramLong2);
  }
  
  public long a(long paramLong, String paramString, Locale paramLocale)
  {
    int i = a(paramString, paramLocale);
    return b(paramLong, i);
  }
  
  public String a(int paramInt, Locale paramLocale)
  {
    return Integer.toString(paramInt);
  }
  
  public String a(long paramLong, Locale paramLocale)
  {
    int i = a(paramLong);
    return a(i, paramLocale);
  }
  
  public final String a(z paramz, Locale paramLocale)
  {
    d locald = g;
    int i = paramz.a(locald);
    return a(i, paramLocale);
  }
  
  public final d a()
  {
    return g;
  }
  
  public int b(long paramLong1, long paramLong2)
  {
    return d().b(paramLong1, paramLong2);
  }
  
  public abstract long b(long paramLong, int paramInt);
  
  public final String b()
  {
    return g.x;
  }
  
  public String b(int paramInt, Locale paramLocale)
  {
    return a(paramInt, paramLocale);
  }
  
  public String b(long paramLong, Locale paramLocale)
  {
    int i = a(paramLong);
    return b(i, paramLocale);
  }
  
  public final String b(z paramz, Locale paramLocale)
  {
    d locald = g;
    int i = paramz.a(locald);
    return b(i, paramLocale);
  }
  
  public boolean b(long paramLong)
  {
    return false;
  }
  
  public int c(long paramLong)
  {
    return h();
  }
  
  public long c(long paramLong1, long paramLong2)
  {
    return d().c(paramLong1, paramLong2);
  }
  
  public final boolean c()
  {
    return true;
  }
  
  public abstract long d(long paramLong);
  
  public abstract i d();
  
  public long e(long paramLong)
  {
    long l = d(paramLong);
    boolean bool = l < paramLong;
    if (bool)
    {
      int i = 1;
      paramLong = a(l, i);
    }
    return paramLong;
  }
  
  public long f(long paramLong)
  {
    long l1 = d(paramLong);
    long l2 = e(paramLong);
    long l3 = paramLong - l1;
    paramLong = l2 - paramLong;
    boolean bool = l3 < paramLong;
    if (!bool) {
      return l1;
    }
    return l2;
  }
  
  public i f()
  {
    return null;
  }
  
  public long g(long paramLong)
  {
    long l1 = d(paramLong);
    long l2 = e(paramLong);
    long l3 = paramLong - l1;
    paramLong = l2 - paramLong;
    boolean bool = paramLong < l3;
    if (!bool) {
      return l2;
    }
    return l1;
  }
  
  public abstract int h();
  
  public long h(long paramLong)
  {
    long l1 = d(paramLong);
    long l2 = e(paramLong);
    long l3 = paramLong - l1;
    paramLong = l2 - paramLong;
    boolean bool = l3 < paramLong;
    if (bool) {
      return l1;
    }
    bool = paramLong < l3;
    if (bool) {
      return l2;
    }
    int i = a(l2) & 0x1;
    if (i == 0) {
      return l2;
    }
    return l1;
  }
  
  public long i(long paramLong)
  {
    long l = d(paramLong);
    return paramLong - l;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("DateTimeField[");
    String str = g.x;
    localStringBuilder.append(str);
    localStringBuilder.append(']');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     org.a.a.c.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */