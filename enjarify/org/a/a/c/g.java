package org.a.a.c;

import org.a.a.c;
import org.a.a.d;
import org.a.a.k;

public final class g
{
  public static int a(long paramLong)
  {
    long l = 2147483648L;
    boolean bool = l < paramLong;
    if (!bool)
    {
      l = 2147483647L;
      bool = paramLong < l;
      if (!bool) {
        return (int)paramLong;
      }
    }
    ArithmeticException localArithmeticException = new java/lang/ArithmeticException;
    String str = String.valueOf(paramLong);
    str = "Value cannot fit in an int: ".concat(str);
    localArithmeticException.<init>(str);
    throw localArithmeticException;
  }
  
  public static long a(long paramLong, int paramInt)
  {
    long l1;
    switch (paramInt)
    {
    default: 
      l1 = paramInt;
      long l2 = paramLong * l1;
      l1 = l2 / l1;
      boolean bool1 = l1 < paramLong;
      if (!bool1) {
        return l2;
      }
      break;
    case 1: 
      return paramLong;
    case 0: 
      return 0L;
    case -1: 
      l1 = Long.MIN_VALUE;
      boolean bool2 = paramLong < l1;
      if (bool2) {
        return -paramLong;
      }
      localArithmeticException = new java/lang/ArithmeticException;
      localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>("Multiplication overflows a long: ");
      localStringBuilder.append(paramLong);
      localStringBuilder.append(" * ");
      localStringBuilder.append(paramInt);
      str = localStringBuilder.toString();
      localArithmeticException.<init>(str);
      throw localArithmeticException;
    }
    ArithmeticException localArithmeticException = new java/lang/ArithmeticException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Multiplication overflows a long: ");
    localStringBuilder.append(paramLong);
    localStringBuilder.append(" * ");
    localStringBuilder.append(paramInt);
    String str = localStringBuilder.toString();
    localArithmeticException.<init>(str);
    throw localArithmeticException;
  }
  
  public static long a(long paramLong1, long paramLong2)
  {
    long l1 = paramLong1 + paramLong2;
    long l2 = paramLong1 ^ l1;
    long l3 = 0L;
    boolean bool = l2 < l3;
    if (bool)
    {
      l2 = paramLong1 ^ paramLong2;
      bool = l2 < l3;
      if (!bool)
      {
        ArithmeticException localArithmeticException = new java/lang/ArithmeticException;
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>("The calculation caused an overflow: ");
        localStringBuilder.append(paramLong1);
        localStringBuilder.append(" + ");
        localStringBuilder.append(paramLong2);
        String str = localStringBuilder.toString();
        localArithmeticException.<init>(str);
        throw localArithmeticException;
      }
    }
    return l1;
  }
  
  public static void a(c paramc, int paramInt1, int paramInt2, int paramInt3)
  {
    if ((paramInt1 >= paramInt2) && (paramInt1 <= paramInt3)) {
      return;
    }
    k localk = new org/a/a/k;
    paramc = paramc.a();
    Integer localInteger1 = Integer.valueOf(paramInt1);
    Integer localInteger2 = Integer.valueOf(paramInt2);
    Integer localInteger3 = Integer.valueOf(paramInt3);
    localk.<init>(paramc, localInteger1, localInteger2, localInteger3);
    throw localk;
  }
  
  public static void a(d paramd, int paramInt1, int paramInt2, int paramInt3)
  {
    if ((paramInt1 >= paramInt2) && (paramInt1 <= paramInt3)) {
      return;
    }
    k localk = new org/a/a/k;
    Integer localInteger1 = Integer.valueOf(paramInt1);
    Integer localInteger2 = Integer.valueOf(paramInt2);
    Integer localInteger3 = Integer.valueOf(paramInt3);
    localk.<init>(paramd, localInteger1, localInteger2, localInteger3);
    throw localk;
  }
  
  public static boolean a(Object paramObject1, Object paramObject2)
  {
    if (paramObject1 == paramObject2) {
      return true;
    }
    if ((paramObject1 != null) && (paramObject2 != null)) {
      return paramObject1.equals(paramObject2);
    }
    return false;
  }
  
  public static long b(long paramLong1, long paramLong2)
  {
    long l1 = paramLong1 - paramLong2;
    long l2 = paramLong1 ^ l1;
    long l3 = 0L;
    boolean bool = l2 < l3;
    if (bool)
    {
      l2 = paramLong1 ^ paramLong2;
      bool = l2 < l3;
      if (bool)
      {
        ArithmeticException localArithmeticException = new java/lang/ArithmeticException;
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>("The calculation caused an overflow: ");
        localStringBuilder.append(paramLong1);
        localStringBuilder.append(" - ");
        localStringBuilder.append(paramLong2);
        String str = localStringBuilder.toString();
        localArithmeticException.<init>(str);
        throw localArithmeticException;
      }
    }
    return l1;
  }
}

/* Location:
 * Qualified Name:     org.a.a.c.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */