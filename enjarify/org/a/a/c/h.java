package org.a.a.c;

import org.a.a.d;
import org.a.a.i;

public abstract class h
  extends b
{
  private final i a;
  final long b;
  
  public h(d paramd, long paramLong)
  {
    super(paramd);
    b = paramLong;
    h.a locala = new org/a/a/c/h$a;
    paramd = paramd.x();
    locala.<init>(this, paramd);
    a = locala;
  }
  
  public abstract long a(long paramLong, int paramInt);
  
  public abstract long a(long paramLong1, long paramLong2);
  
  public final int b(long paramLong1, long paramLong2)
  {
    return g.a(c(paramLong1, paramLong2));
  }
  
  public long c(long paramLong1, long paramLong2)
  {
    boolean bool1 = paramLong1 < paramLong2;
    if (bool1) {
      return -c(paramLong2, paramLong1);
    }
    long l1 = paramLong1 - paramLong2;
    long l2 = b;
    l1 /= l2;
    l2 = a(paramLong2, l1);
    long l3 = 1L;
    boolean bool2 = l2 < paramLong1;
    if (bool2)
    {
      do
      {
        l1 += l3;
        l2 = a(paramLong2, l1);
        bool2 = l2 < paramLong1;
      } while (!bool2);
      l1 -= l3;
    }
    else
    {
      l2 = a(paramLong2, l1);
      bool2 = l2 < paramLong1;
      if (bool2) {
        do
        {
          l1 -= l3;
          l2 = a(paramLong2, l1);
          bool2 = l2 < paramLong1;
        } while (bool2);
      }
    }
    return l1;
  }
  
  public final i d()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     org.a.a.c.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */