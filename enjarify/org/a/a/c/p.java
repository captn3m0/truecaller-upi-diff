package org.a.a.c;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Locale;
import org.a.a.c;
import org.a.a.d;
import org.a.a.i;
import org.a.a.z;

public final class p
  extends c
  implements Serializable
{
  private static HashMap a;
  private static final long serialVersionUID = -1934618396111902255L;
  private final d b;
  private final i c;
  
  private p(d paramd, i parami)
  {
    if ((paramd != null) && (parami != null))
    {
      b = paramd;
      c = parami;
      return;
    }
    paramd = new java/lang/IllegalArgumentException;
    paramd.<init>();
    throw paramd;
  }
  
  public static p a(d paramd, i parami)
  {
    synchronized (p.class)
    {
      Object localObject1 = a;
      Object localObject2 = null;
      if (localObject1 == null)
      {
        localObject1 = new java/util/HashMap;
        int i = 7;
        ((HashMap)localObject1).<init>(i);
        a = (HashMap)localObject1;
      }
      else
      {
        localObject1 = a;
        localObject1 = ((HashMap)localObject1).get(paramd);
        localObject1 = (p)localObject1;
        if (localObject1 != null)
        {
          i locali = c;
          if (locali != parami) {}
        }
        else
        {
          localObject2 = localObject1;
        }
      }
      if (localObject2 == null)
      {
        localObject2 = new org/a/a/c/p;
        ((p)localObject2).<init>(paramd, parami);
        parami = a;
        parami.put(paramd, localObject2);
      }
      return (p)localObject2;
    }
  }
  
  private UnsupportedOperationException i()
  {
    UnsupportedOperationException localUnsupportedOperationException = new java/lang/UnsupportedOperationException;
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    d locald = b;
    ((StringBuilder)localObject).append(locald);
    ((StringBuilder)localObject).append(" field is unsupported");
    localObject = ((StringBuilder)localObject).toString();
    localUnsupportedOperationException.<init>((String)localObject);
    return localUnsupportedOperationException;
  }
  
  private Object readResolve()
  {
    d locald = b;
    i locali = c;
    return a(locald, locali);
  }
  
  public final int a(long paramLong)
  {
    throw i();
  }
  
  public final int a(Locale paramLocale)
  {
    throw i();
  }
  
  public final long a(long paramLong, int paramInt)
  {
    return c.a(paramLong, paramInt);
  }
  
  public final long a(long paramLong1, long paramLong2)
  {
    return c.a(paramLong1, paramLong2);
  }
  
  public final long a(long paramLong, String paramString, Locale paramLocale)
  {
    throw i();
  }
  
  public final String a(int paramInt, Locale paramLocale)
  {
    throw i();
  }
  
  public final String a(long paramLong, Locale paramLocale)
  {
    throw i();
  }
  
  public final String a(z paramz, Locale paramLocale)
  {
    throw i();
  }
  
  public final d a()
  {
    return b;
  }
  
  public final int b(long paramLong1, long paramLong2)
  {
    return c.b(paramLong1, paramLong2);
  }
  
  public final long b(long paramLong, int paramInt)
  {
    throw i();
  }
  
  public final String b()
  {
    return b.x;
  }
  
  public final String b(int paramInt, Locale paramLocale)
  {
    throw i();
  }
  
  public final String b(long paramLong, Locale paramLocale)
  {
    throw i();
  }
  
  public final String b(z paramz, Locale paramLocale)
  {
    throw i();
  }
  
  public final boolean b(long paramLong)
  {
    throw i();
  }
  
  public final int c(long paramLong)
  {
    throw i();
  }
  
  public final long c(long paramLong1, long paramLong2)
  {
    return c.c(paramLong1, paramLong2);
  }
  
  public final boolean c()
  {
    return false;
  }
  
  public final long d(long paramLong)
  {
    throw i();
  }
  
  public final i d()
  {
    return c;
  }
  
  public final long e(long paramLong)
  {
    throw i();
  }
  
  public final i e()
  {
    return null;
  }
  
  public final long f(long paramLong)
  {
    throw i();
  }
  
  public final i f()
  {
    return null;
  }
  
  public final int g()
  {
    throw i();
  }
  
  public final long g(long paramLong)
  {
    throw i();
  }
  
  public final int h()
  {
    throw i();
  }
  
  public final long h(long paramLong)
  {
    throw i();
  }
  
  public final long i(long paramLong)
  {
    throw i();
  }
  
  public final String toString()
  {
    return "UnsupportedDateTimeField";
  }
}

/* Location:
 * Qualified Name:     org.a.a.c.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */