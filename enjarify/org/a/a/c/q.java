package org.a.a.c;

import java.io.Serializable;
import java.util.HashMap;
import org.a.a.i;
import org.a.a.j;

public final class q
  extends i
  implements Serializable
{
  private static HashMap a;
  private static final long serialVersionUID = -6390301302770925357L;
  private final j b;
  
  private q(j paramj)
  {
    b = paramj;
  }
  
  public static q a(j paramj)
  {
    synchronized (q.class)
    {
      Object localObject = a;
      if (localObject == null)
      {
        localObject = new java/util/HashMap;
        int i = 7;
        ((HashMap)localObject).<init>(i);
        a = (HashMap)localObject;
        localObject = null;
      }
      else
      {
        localObject = a;
        localObject = ((HashMap)localObject).get(paramj);
        localObject = (q)localObject;
      }
      if (localObject == null)
      {
        localObject = new org/a/a/c/q;
        ((q)localObject).<init>(paramj);
        HashMap localHashMap = a;
        localHashMap.put(paramj, localObject);
      }
      return (q)localObject;
    }
  }
  
  private UnsupportedOperationException e()
  {
    UnsupportedOperationException localUnsupportedOperationException = new java/lang/UnsupportedOperationException;
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    j localj = b;
    ((StringBuilder)localObject).append(localj);
    ((StringBuilder)localObject).append(" field is unsupported");
    localObject = ((StringBuilder)localObject).toString();
    localUnsupportedOperationException.<init>((String)localObject);
    return localUnsupportedOperationException;
  }
  
  private Object readResolve()
  {
    return a(b);
  }
  
  public final long a(long paramLong, int paramInt)
  {
    throw e();
  }
  
  public final long a(long paramLong1, long paramLong2)
  {
    throw e();
  }
  
  public final j a()
  {
    return b;
  }
  
  public final int b(long paramLong1, long paramLong2)
  {
    throw e();
  }
  
  public final boolean b()
  {
    return false;
  }
  
  public final long c(long paramLong1, long paramLong2)
  {
    throw e();
  }
  
  public final boolean c()
  {
    return true;
  }
  
  public final long d()
  {
    return 0L;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this == paramObject) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof q;
    if (bool2)
    {
      paramObject = (q)paramObject;
      String str1 = b.m;
      if (str1 == null)
      {
        paramObject = b.m;
        if (paramObject == null) {
          return bool1;
        }
        return false;
      }
      paramObject = b.m;
      String str2 = b.m;
      return ((String)paramObject).equals(str2);
    }
    return false;
  }
  
  public final int hashCode()
  {
    return b.m.hashCode();
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("UnsupportedDurationField[");
    String str = b.m;
    localStringBuilder.append(str);
    localStringBuilder.append(']');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     org.a.a.c.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */