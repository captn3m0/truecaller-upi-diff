package org.a.a.c;

import org.a.a.c;
import org.a.a.i;

public final class f
  extends d
{
  final int a;
  final i c;
  final i d;
  private final int e;
  private final int f;
  
  public f(c paramc, org.a.a.d paramd)
  {
    this(paramc, locali, paramd);
  }
  
  private f(c paramc, i parami, org.a.a.d paramd)
  {
    super(paramc, paramd);
    i locali = paramc.d();
    if (locali == null)
    {
      i = 0;
      paramd = null;
      c = null;
    }
    else
    {
      o localo = new org/a/a/c/o;
      paramd = paramd.x();
      localo.<init>(locali, paramd);
      c = localo;
    }
    d = parami;
    int j = 100;
    a = j;
    int i = paramc.g();
    if (i >= 0) {
      i /= j;
    } else {
      i = (i + 1) / j + -1;
    }
    int k = paramc.h();
    if (k >= 0) {
      k /= j;
    } else {
      k = (k + 1) / j + -1;
    }
    e = i;
    f = k;
  }
  
  public final int a(long paramLong)
  {
    c localc = b;
    int i = localc.a(paramLong);
    if (i >= 0)
    {
      j = a;
      return i / j;
    }
    i += 1;
    int j = a;
    return i / j + -1;
  }
  
  public final long a(long paramLong, int paramInt)
  {
    c localc = b;
    int i = a;
    paramInt *= i;
    return localc.a(paramLong, paramInt);
  }
  
  public final long a(long paramLong1, long paramLong2)
  {
    c localc = b;
    long l = a;
    paramLong2 *= l;
    return localc.a(paramLong1, paramLong2);
  }
  
  public final int b(long paramLong1, long paramLong2)
  {
    int i = b.b(paramLong1, paramLong2);
    int j = a;
    return i / j;
  }
  
  public final long b(long paramLong, int paramInt)
  {
    int i = e;
    int j = f;
    g.a(this, paramInt, i, j);
    c localc1 = b;
    i = localc1.a(paramLong);
    if (i >= 0)
    {
      j = a;
      i %= j;
    }
    else
    {
      j = a;
      k = j + -1;
      i = (i + 1) % j + k;
    }
    c localc2 = b;
    int k = a;
    paramInt = paramInt * k + i;
    return localc2.b(paramLong, paramInt);
  }
  
  public final long c(long paramLong1, long paramLong2)
  {
    paramLong1 = b.c(paramLong1, paramLong2);
    paramLong2 = a;
    return paramLong1 / paramLong2;
  }
  
  public final long d(long paramLong)
  {
    c localc = b;
    int i = a(paramLong);
    int j = a;
    i *= j;
    paramLong = localc.b(paramLong, i);
    return localc.d(paramLong);
  }
  
  public final i d()
  {
    return c;
  }
  
  public final i e()
  {
    i locali = d;
    if (locali != null) {
      return locali;
    }
    return super.e();
  }
  
  public final int g()
  {
    return e;
  }
  
  public final int h()
  {
    return f;
  }
  
  public final long i(long paramLong)
  {
    long l = b.i(paramLong);
    int i = a(l);
    return b(paramLong, i);
  }
}

/* Location:
 * Qualified Name:     org.a.a.c.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */