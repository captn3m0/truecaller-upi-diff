package org.a.a;

import java.io.Serializable;
import org.a.a.a.g;

public final class q
  extends g
  implements Serializable, Cloneable, t
{
  private static final long serialVersionUID = 2852608688135209575L;
  private c c;
  private int d;
  
  public q() {}
  
  public q(f paramf)
  {
    super(paramf);
  }
  
  public final void a(long paramLong)
  {
    int i = d;
    c localc;
    switch (i)
    {
    default: 
      break;
    case 5: 
      localc = c;
      paramLong = localc.h(paramLong);
      break;
    case 4: 
      localc = c;
      paramLong = localc.g(paramLong);
      break;
    case 3: 
      localc = c;
      paramLong = localc.f(paramLong);
      break;
    case 2: 
      localc = c;
      paramLong = localc.e(paramLong);
      break;
    case 1: 
      localc = c;
      paramLong = localc.d(paramLong);
    }
    super.a(paramLong);
  }
  
  public final Object clone()
  {
    try
    {
      return super.clone();
    }
    catch (CloneNotSupportedException localCloneNotSupportedException)
    {
      InternalError localInternalError = new java/lang/InternalError;
      localInternalError.<init>("Clone error");
      throw localInternalError;
    }
  }
}

/* Location:
 * Qualified Name:     org.a.a.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */