package org.a.a;

final class j$a
  extends j
{
  private static final long serialVersionUID = 31156755687123L;
  private final byte n;
  
  j$a(String paramString, byte paramByte)
  {
    super(paramString);
    n = paramByte;
  }
  
  private Object readResolve()
  {
    int i = n;
    switch (i)
    {
    default: 
      return this;
    case 12: 
      return l;
    case 11: 
      return k;
    case 10: 
      return j;
    case 9: 
      return i;
    case 8: 
      return h;
    case 7: 
      return g;
    case 6: 
      return f;
    case 5: 
      return e;
    case 4: 
      return d;
    case 3: 
      return c;
    case 2: 
      return b;
    }
    return a;
  }
  
  public final i a(a parama)
  {
    parama = e.a(parama);
    int i = n;
    switch (i)
    {
    default: 
      parama = new java/lang/InternalError;
      parama.<init>();
      throw parama;
    case 12: 
      return parama.c();
    case 11: 
      return parama.f();
    case 10: 
      return parama.i();
    case 9: 
      return parama.l();
    case 8: 
      return parama.o();
    case 7: 
      return parama.s();
    case 6: 
      return parama.w();
    case 5: 
      return parama.B();
    case 4: 
      return parama.D();
    case 3: 
      return parama.y();
    case 2: 
      return parama.H();
    }
    return parama.J();
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this == paramObject) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof a;
    if (bool2)
    {
      int i = n;
      paramObject = (a)paramObject;
      int j = n;
      if (i == j) {
        return bool1;
      }
      return false;
    }
    return false;
  }
  
  public final int hashCode()
  {
    int i = n;
    return 1 << i;
  }
}

/* Location:
 * Qualified Name:     org.a.a.j.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */