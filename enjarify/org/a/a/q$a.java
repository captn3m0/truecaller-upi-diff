package org.a.a;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import org.a.a.a.g;

public final class q$a
  extends org.a.a.c.a
{
  private static final long serialVersionUID = -4481126543819298617L;
  public q a;
  public c b;
  
  public q$a(q paramq, c paramc)
  {
    a = paramq;
    b = paramc;
  }
  
  private void readObject(ObjectInputStream paramObjectInputStream)
  {
    Object localObject = (q)paramObjectInputStream.readObject();
    a = ((q)localObject);
    paramObjectInputStream = (d)paramObjectInputStream.readObject();
    localObject = a.b;
    paramObjectInputStream = paramObjectInputStream.a((a)localObject);
    b = paramObjectInputStream;
  }
  
  private void writeObject(ObjectOutputStream paramObjectOutputStream)
  {
    Object localObject = a;
    paramObjectOutputStream.writeObject(localObject);
    localObject = b.a();
    paramObjectOutputStream.writeObject(localObject);
  }
  
  public final c a()
  {
    return b;
  }
  
  public final long b()
  {
    return a.a;
  }
  
  public final a c()
  {
    return a.b;
  }
}

/* Location:
 * Qualified Name:     org.a.a.q.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */