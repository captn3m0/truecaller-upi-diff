package org.a.a;

import java.lang.reflect.Method;
import java.text.DateFormatSymbols;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import org.a.a.b.q;

public final class e
{
  public static final e.a a;
  private static volatile e.a b;
  private static final AtomicReference c;
  
  static
  {
    Object localObject = new org/a/a/e$b;
    ((e.b)localObject).<init>();
    a = (e.a)localObject;
    b = (e.a)localObject;
    localObject = new java/util/concurrent/atomic/AtomicReference;
    ((AtomicReference)localObject).<init>();
    c = (AtomicReference)localObject;
  }
  
  public static final long a()
  {
    return b.a();
  }
  
  public static final long a(w paramw)
  {
    if (paramw == null) {
      return 0L;
    }
    return paramw.a();
  }
  
  public static final long a(x paramx)
  {
    if (paramx == null) {
      return a();
    }
    return paramx.a();
  }
  
  public static final DateFormatSymbols a(Locale paramLocale)
  {
    Object localObject = DateFormatSymbols.class;
    String str = "getInstance";
    int i = 1;
    try
    {
      Class[] arrayOfClass = new Class[i];
      Class localClass = Locale.class;
      arrayOfClass[0] = localClass;
      localObject = ((Class)localObject).getMethod(str, arrayOfClass);
      str = null;
      Object[] arrayOfObject = new Object[i];
      arrayOfObject[0] = paramLocale;
      localObject = ((Method)localObject).invoke(null, arrayOfObject);
      return (DateFormatSymbols)localObject;
    }
    catch (Exception localException)
    {
      localObject = new java/text/DateFormatSymbols;
      ((DateFormatSymbols)localObject).<init>(paramLocale);
    }
    return (DateFormatSymbols)localObject;
  }
  
  public static final a a(a parama)
  {
    if (parama == null) {
      return q.M();
    }
    return parama;
  }
  
  public static final f a(f paramf)
  {
    if (paramf == null) {
      return f.a();
    }
    return paramf;
  }
  
  public static final s a(s params)
  {
    if (params == null) {
      return s.a();
    }
    return params;
  }
  
  private static void a(Map paramMap, String paramString1, String paramString2)
  {
    try
    {
      paramString2 = f.a(paramString2);
      paramMap.put(paramString1, paramString2);
      return;
    }
    catch (RuntimeException localRuntimeException) {}
  }
  
  public static final Map b()
  {
    Object localObject = (Map)c.get();
    if (localObject == null)
    {
      localObject = new java/util/LinkedHashMap;
      ((LinkedHashMap)localObject).<init>();
      f localf = f.a;
      ((Map)localObject).put("UT", localf);
      localf = f.a;
      ((Map)localObject).put("UTC", localf);
      localf = f.a;
      ((Map)localObject).put("GMT", localf);
      a((Map)localObject, "EST", "America/New_York");
      a((Map)localObject, "EDT", "America/New_York");
      a((Map)localObject, "CST", "America/Chicago");
      a((Map)localObject, "CDT", "America/Chicago");
      a((Map)localObject, "MST", "America/Denver");
      a((Map)localObject, "MDT", "America/Denver");
      a((Map)localObject, "PST", "America/Los_Angeles");
      a((Map)localObject, "PDT", "America/Los_Angeles");
      localObject = Collections.unmodifiableMap((Map)localObject);
      AtomicReference localAtomicReference = c;
      localf = null;
      boolean bool = localAtomicReference.compareAndSet(null, localObject);
      if (!bool) {
        localObject = (Map)c.get();
      }
    }
    return (Map)localObject;
  }
  
  public static final a b(x paramx)
  {
    if (paramx == null) {
      return q.M();
    }
    paramx = paramx.b();
    if (paramx == null) {
      return q.M();
    }
    return paramx;
  }
}

/* Location:
 * Qualified Name:     org.a.a.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */