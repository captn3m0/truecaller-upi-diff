package org.a.a.e;

import java.util.Collections;
import java.util.Set;

public final class g
  implements f
{
  private static final Set a = Collections.singleton("UTC");
  
  public final Set a()
  {
    return a;
  }
  
  public final org.a.a.f a(String paramString)
  {
    String str = "UTC";
    boolean bool = str.equalsIgnoreCase(paramString);
    if (bool) {
      return org.a.a.f.a;
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     org.a.a.e.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */