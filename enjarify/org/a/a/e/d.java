package org.a.a.e;

import org.a.a.f;

public final class d
  extends f
{
  private static final long serialVersionUID = -3513011772763289092L;
  private final String c;
  private final int d;
  private final int e;
  
  public d(String paramString1, String paramString2, int paramInt1, int paramInt2)
  {
    super(paramString1);
    c = paramString2;
    d = paramInt1;
    e = paramInt2;
  }
  
  public final String a(long paramLong)
  {
    return c;
  }
  
  public final int b(long paramLong)
  {
    return d;
  }
  
  public final int c(long paramLong)
  {
    return e;
  }
  
  public final boolean d()
  {
    return true;
  }
  
  public final int e(long paramLong)
  {
    return d;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this == paramObject) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof d;
    if (bool2)
    {
      paramObject = (d)paramObject;
      String str1 = b;
      String str2 = b;
      bool2 = str1.equals(str2);
      if (bool2)
      {
        int i = e;
        int j = e;
        if (i == j)
        {
          i = d;
          int k = d;
          if (i == k) {
            return bool1;
          }
        }
      }
      return false;
    }
    return false;
  }
  
  public final long h(long paramLong)
  {
    return paramLong;
  }
  
  public final int hashCode()
  {
    int i = b.hashCode();
    int j = e * 37;
    i += j;
    j = d * 31;
    return i + j;
  }
  
  public final long i(long paramLong)
  {
    return paramLong;
  }
}

/* Location:
 * Qualified Name:     org.a.a.e.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */