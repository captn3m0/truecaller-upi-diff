package org.a.a.e;

import java.io.DataInput;
import java.io.IOException;
import java.util.Arrays;
import org.a.a.f;

final class b$c
  extends f
{
  private static final long serialVersionUID = 7811976468055766265L;
  private final long[] c;
  private final int[] d;
  private final int[] e;
  private final String[] f;
  private final b.a g;
  
  private b$c(String paramString, long[] paramArrayOfLong, int[] paramArrayOfInt1, int[] paramArrayOfInt2, String[] paramArrayOfString, b.a parama)
  {
    super(paramString);
    c = paramArrayOfLong;
    d = paramArrayOfInt1;
    e = paramArrayOfInt2;
    f = paramArrayOfString;
    g = parama;
  }
  
  static c a(DataInput paramDataInput, String paramString)
  {
    int i = paramDataInput.readUnsignedShort();
    Object localObject1 = new String[i];
    int j = 0;
    int k = 0;
    while (k < i)
    {
      localObject2 = paramDataInput.readUTF();
      localObject1[k] = localObject2;
      k += 1;
    }
    k = paramDataInput.readInt();
    long[] arrayOfLong = new long[k];
    int[] arrayOfInt1 = new int[k];
    int[] arrayOfInt2 = new int[k];
    String[] arrayOfString = new String[k];
    while (j < k)
    {
      long l1 = b.a(paramDataInput);
      arrayOfLong[j] = l1;
      int m = (int)b.a(paramDataInput);
      arrayOfInt1[j] = m;
      l1 = b.a(paramDataInput);
      m = (int)l1;
      arrayOfInt2[j] = m;
      int n = 256;
      if (i < n) {}
      try
      {
        n = paramDataInput.readUnsignedByte();
        break label162;
        n = paramDataInput.readUnsignedShort();
        label162:
        localObject2 = localObject1[n];
        arrayOfString[j] = localObject2;
        j += 1;
      }
      catch (ArrayIndexOutOfBoundsException localArrayIndexOutOfBoundsException)
      {
        paramDataInput = new java/io/IOException;
        paramDataInput.<init>("Invalid encoding");
        throw paramDataInput;
      }
    }
    i = 0;
    b.a locala1 = null;
    boolean bool = paramDataInput.readBoolean();
    b.a locala2;
    if (bool)
    {
      locala1 = new org/a/a/e/b$a;
      long l2 = b.a(paramDataInput);
      j = (int)l2;
      localObject1 = b.d.a(paramDataInput);
      paramDataInput = b.d.a(paramDataInput);
      locala1.<init>(paramString, j, (b.d)localObject1, paramDataInput);
      locala2 = locala1;
    }
    else
    {
      locala2 = null;
    }
    paramDataInput = new org/a/a/e/b$c;
    Object localObject2 = paramDataInput;
    paramDataInput.<init>(paramString, arrayOfLong, arrayOfInt1, arrayOfInt2, arrayOfString, locala2);
    return paramDataInput;
  }
  
  public final String a(long paramLong)
  {
    Object localObject = c;
    int i = Arrays.binarySearch((long[])localObject, paramLong);
    if (i >= 0) {
      return f[i];
    }
    i ^= 0xFFFFFFFF;
    int j = localObject.length;
    String[] arrayOfString;
    if (i < j)
    {
      if (i > 0)
      {
        arrayOfString = f;
        i += -1;
        return arrayOfString[i];
      }
      return "UTC";
    }
    localObject = g;
    if (localObject == null)
    {
      arrayOfString = f;
      i += -1;
      return arrayOfString[i];
    }
    return ((b.a)localObject).a(paramLong);
  }
  
  public final int b(long paramLong)
  {
    Object localObject = c;
    int i = Arrays.binarySearch((long[])localObject, paramLong);
    if (i >= 0) {
      return d[i];
    }
    i ^= 0xFFFFFFFF;
    int j = localObject.length;
    int[] arrayOfInt;
    if (i < j)
    {
      if (i > 0)
      {
        arrayOfInt = d;
        i += -1;
        return arrayOfInt[i];
      }
      return 0;
    }
    localObject = g;
    if (localObject == null)
    {
      arrayOfInt = d;
      i += -1;
      return arrayOfInt[i];
    }
    return ((b.a)localObject).b(paramLong);
  }
  
  public final int c(long paramLong)
  {
    long[] arrayOfLong = c;
    int i = Arrays.binarySearch(arrayOfLong, paramLong);
    if (i >= 0) {
      return e[i];
    }
    i ^= 0xFFFFFFFF;
    int j = arrayOfLong.length;
    if (i < j)
    {
      if (i > 0)
      {
        localObject = e;
        i += -1;
        return localObject[i];
      }
      return 0;
    }
    Object localObject = g;
    if (localObject == null)
    {
      localObject = e;
      i += -1;
      return localObject[i];
    }
    return c;
  }
  
  public final boolean d()
  {
    return false;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this == paramObject) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof c;
    if (bool2)
    {
      paramObject = (c)paramObject;
      Object localObject1 = b;
      Object localObject2 = b;
      bool2 = ((String)localObject1).equals(localObject2);
      if (bool2)
      {
        localObject1 = c;
        localObject2 = c;
        bool2 = Arrays.equals((long[])localObject1, (long[])localObject2);
        if (bool2)
        {
          localObject1 = f;
          localObject2 = f;
          bool2 = Arrays.equals((Object[])localObject1, (Object[])localObject2);
          if (bool2)
          {
            localObject1 = d;
            localObject2 = d;
            bool2 = Arrays.equals((int[])localObject1, (int[])localObject2);
            if (bool2)
            {
              localObject1 = e;
              localObject2 = e;
              bool2 = Arrays.equals((int[])localObject1, (int[])localObject2);
              if (bool2)
              {
                localObject1 = g;
                if (localObject1 == null)
                {
                  paramObject = g;
                  if (paramObject != null) {
                    break label186;
                  }
                }
                else
                {
                  paramObject = g;
                  boolean bool3 = ((b.a)localObject1).equals(paramObject);
                  if (!bool3) {
                    break label186;
                  }
                }
                return bool1;
              }
            }
          }
        }
      }
      label186:
      return false;
    }
    return false;
  }
  
  public final long h(long paramLong)
  {
    long[] arrayOfLong = c;
    int i = Arrays.binarySearch(arrayOfLong, paramLong);
    if (i >= 0) {
      i += 1;
    } else {
      i ^= 0xFFFFFFFF;
    }
    int j = arrayOfLong.length;
    if (i < j) {
      return arrayOfLong[i];
    }
    b.a locala = g;
    if (locala == null) {
      return paramLong;
    }
    i = arrayOfLong.length + -1;
    long l = arrayOfLong[i];
    boolean bool = paramLong < l;
    if (bool) {
      paramLong = l;
    }
    return g.h(paramLong);
  }
  
  public final long i(long paramLong)
  {
    long[] arrayOfLong = c;
    int i = Arrays.binarySearch(arrayOfLong, paramLong);
    long l1 = 1L;
    long l2 = Long.MIN_VALUE;
    if (i >= 0)
    {
      bool1 = paramLong < l2;
      if (bool1) {
        return paramLong - l1;
      }
      return paramLong;
    }
    i ^= 0xFFFFFFFF;
    int j = arrayOfLong.length;
    if (i < j)
    {
      if (i > 0)
      {
        i += -1;
        l3 = arrayOfLong[i];
        bool1 = l3 < l2;
        if (bool1) {
          return l3 - l1;
        }
      }
      return paramLong;
    }
    b.a locala = g;
    if (locala != null)
    {
      l3 = locala.i(paramLong);
      boolean bool2 = l3 < paramLong;
      if (bool2) {
        return l3;
      }
    }
    i += -1;
    long l3 = arrayOfLong[i];
    boolean bool1 = l3 < l2;
    if (bool1) {
      return l3 - l1;
    }
    return paramLong;
  }
}

/* Location:
 * Qualified Name:     org.a.a.e.b.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */