package org.a.a.e;

import java.io.DataInput;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.a.a.f;

public final class b
{
  static long a(DataInput paramDataInput)
  {
    int i = paramDataInput.readUnsignedByte();
    int j = i >> 6;
    int k = 26;
    switch (j)
    {
    default: 
      return (i << 26 >> k) * 1800000L;
    case 3: 
      return paramDataInput.readLong();
    case 2: 
      long l1 = i << 58 >> k;
      long l2 = paramDataInput.readUnsignedByte() << 24;
      l1 |= l2;
      l2 = paramDataInput.readUnsignedByte() << 16;
      l1 |= l2;
      l2 = paramDataInput.readUnsignedByte() << 8;
      l1 |= l2;
      l2 = paramDataInput.readUnsignedByte();
      return (l1 | l2) * 1000L;
    }
    i = i << k >> 2;
    j = paramDataInput.readUnsignedByte() << 16;
    i |= j;
    j = paramDataInput.readUnsignedByte() << 8;
    i |= j;
    return (paramDataInput.readUnsignedByte() | i) * 60000L;
  }
  
  private static f a(DataInput paramDataInput, String paramString)
  {
    int i = paramDataInput.readUnsignedByte();
    int j = 67;
    if (i != j)
    {
      j = 70;
      if (i != j)
      {
        j = 80;
        if (i == j) {
          return b.c.a(paramDataInput, paramString);
        }
        paramDataInput = new java/io/IOException;
        paramDataInput.<init>("Invalid encoding");
        throw paramDataInput;
      }
      Object localObject = new org/a/a/e/d;
      String str = paramDataInput.readUTF();
      long l1 = a(paramDataInput);
      int k = (int)l1;
      long l2 = a(paramDataInput);
      int m = (int)l2;
      ((d)localObject).<init>(paramString, str, k, m);
      paramDataInput = f.a;
      boolean bool = ((f)localObject).equals(paramDataInput);
      if (bool) {
        localObject = f.a;
      }
      return (f)localObject;
    }
    return a.b(b.c.a(paramDataInput, paramString));
  }
  
  public static f a(InputStream paramInputStream, String paramString)
  {
    boolean bool = paramInputStream instanceof DataInput;
    if (bool) {
      return a((DataInput)paramInputStream, paramString);
    }
    DataInputStream localDataInputStream = new java/io/DataInputStream;
    localDataInputStream.<init>(paramInputStream);
    return a(localDataInputStream, paramString);
  }
}

/* Location:
 * Qualified Name:     org.a.a.e.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */