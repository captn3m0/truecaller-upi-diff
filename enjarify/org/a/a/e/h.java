package org.a.a.e;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public final class h
  implements f
{
  final ClassLoader a;
  private final File b;
  private final String c;
  private final Map d;
  private final Set e;
  
  public h(File paramFile)
  {
    boolean bool = paramFile.exists();
    if (bool)
    {
      bool = paramFile.isDirectory();
      if (bool)
      {
        b = paramFile;
        c = null;
        a = null;
        paramFile = a(b("ZoneInfoMap"));
        d = paramFile;
        paramFile = new java/util/TreeSet;
        localObject = d.keySet();
        paramFile.<init>((Collection)localObject);
        paramFile = Collections.unmodifiableSortedSet(paramFile);
        e = paramFile;
        return;
      }
      localObject = new java/io/IOException;
      paramFile = String.valueOf(paramFile);
      paramFile = "File doesn't refer to a directory: ".concat(paramFile);
      ((IOException)localObject).<init>(paramFile);
      throw ((Throwable)localObject);
    }
    Object localObject = new java/io/IOException;
    paramFile = String.valueOf(paramFile);
    paramFile = "File directory doesn't exist: ".concat(paramFile);
    ((IOException)localObject).<init>(paramFile);
    throw ((Throwable)localObject);
  }
  
  public h(String paramString)
  {
    this(paramString, (byte)0);
  }
  
  private h(String paramString, byte paramByte)
  {
    if (paramString != null)
    {
      Object localObject = "/";
      paramByte = paramString.endsWith((String)localObject);
      if (paramByte == 0)
      {
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>();
        ((StringBuilder)localObject).append(paramString);
        char c1 = '/';
        ((StringBuilder)localObject).append(c1);
        paramString = ((StringBuilder)localObject).toString();
      }
      b = null;
      c = paramString;
      paramString = getClass().getClassLoader();
      a = paramString;
      paramString = a(b("ZoneInfoMap"));
      d = paramString;
      paramString = new java/util/TreeSet;
      localObject = d.keySet();
      paramString.<init>((Collection)localObject);
      paramString = Collections.unmodifiableSortedSet(paramString);
      e = paramString;
      return;
    }
    paramString = new java/lang/IllegalArgumentException;
    paramString.<init>("No resource path provided");
    throw paramString;
  }
  
  /* Error */
  private static Map a(InputStream paramInputStream)
  {
    // Byte code:
    //   0: new 124	java/util/concurrent/ConcurrentHashMap
    //   3: astore_1
    //   4: aload_1
    //   5: invokespecial 125	java/util/concurrent/ConcurrentHashMap:<init>	()V
    //   8: new 127	java/io/DataInputStream
    //   11: astore_2
    //   12: aload_2
    //   13: aload_0
    //   14: invokespecial 130	java/io/DataInputStream:<init>	(Ljava/io/InputStream;)V
    //   17: aload_2
    //   18: aload_1
    //   19: invokestatic 133	org/a/a/e/h:a	(Ljava/io/DataInputStream;Ljava/util/Map;)V
    //   22: aload_2
    //   23: invokevirtual 136	java/io/DataInputStream:close	()V
    //   26: new 140	java/lang/ref/SoftReference
    //   29: astore_2
    //   30: getstatic 145	org/a/a/f:a	Lorg/a/a/f;
    //   33: astore_3
    //   34: aload_2
    //   35: aload_3
    //   36: invokespecial 148	java/lang/ref/SoftReference:<init>	(Ljava/lang/Object;)V
    //   39: aload_1
    //   40: ldc -118
    //   42: aload_2
    //   43: invokeinterface 152 3 0
    //   48: pop
    //   49: aload_1
    //   50: areturn
    //   51: astore_0
    //   52: aload_2
    //   53: invokevirtual 136	java/io/DataInputStream:close	()V
    //   56: aload_0
    //   57: athrow
    //   58: pop
    //   59: goto -33 -> 26
    //   62: pop
    //   63: goto -7 -> 56
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	66	0	paramInputStream	InputStream
    //   3	47	1	localConcurrentHashMap	java.util.concurrent.ConcurrentHashMap
    //   11	42	2	localObject	Object
    //   33	3	3	localf	org.a.a.f
    //   58	1	4	localIOException1	IOException
    //   62	1	5	localIOException2	IOException
    // Exception table:
    //   from	to	target	type
    //   18	22	51	finally
    //   22	26	58	java/io/IOException
    //   52	56	62	java/io/IOException
  }
  
  private static void a(DataInputStream paramDataInputStream, Map paramMap)
  {
    int i = paramDataInputStream.readUnsignedShort();
    String[] arrayOfString = new String[i];
    int j = 0;
    int k = 0;
    String str1 = null;
    String str2;
    while (k < i)
    {
      str2 = paramDataInputStream.readUTF().intern();
      arrayOfString[k] = str2;
      k += 1;
    }
    i = paramDataInputStream.readUnsignedShort();
    while (j < i) {
      try
      {
        k = paramDataInputStream.readUnsignedShort();
        str1 = arrayOfString[k];
        int m = paramDataInputStream.readUnsignedShort();
        str2 = arrayOfString[m];
        paramMap.put(str1, str2);
        j += 1;
      }
      catch (ArrayIndexOutOfBoundsException localArrayIndexOutOfBoundsException)
      {
        paramDataInputStream = new java/io/IOException;
        paramDataInputStream.<init>("Corrupt zone info map");
        throw paramDataInputStream;
      }
    }
  }
  
  private InputStream b(String paramString)
  {
    Object localObject1 = b;
    Object localObject2;
    if (localObject1 != null)
    {
      localObject2 = new java/io/FileInputStream;
      File localFile = new java/io/File;
      localFile.<init>((File)localObject1, paramString);
      ((FileInputStream)localObject2).<init>(localFile);
    }
    else
    {
      paramString = c.concat(paramString);
      localObject1 = new org/a/a/e/h$1;
      ((h.1)localObject1).<init>(this, paramString);
      localObject1 = AccessController.doPrivileged((PrivilegedAction)localObject1);
      localObject2 = localObject1;
      localObject2 = (InputStream)localObject1;
      if (localObject2 == null)
      {
        localObject1 = new java/lang/StringBuilder;
        int i = 40;
        ((StringBuilder)localObject1).<init>(i);
        localObject2 = "Resource not found: \"";
        ((StringBuilder)localObject1).append((String)localObject2);
        ((StringBuilder)localObject1).append(paramString);
        ((StringBuilder)localObject1).append("\" ClassLoader: ");
        paramString = a;
        if (paramString != null) {
          paramString = paramString.toString();
        } else {
          paramString = "system";
        }
        ((StringBuilder)localObject1).append(paramString);
        paramString = new java/io/IOException;
        localObject1 = ((StringBuilder)localObject1).toString();
        paramString.<init>((String)localObject1);
        throw paramString;
      }
    }
    return (InputStream)localObject2;
  }
  
  /* Error */
  private org.a.a.f c(String paramString)
  {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: invokespecial 40	org/a/a/e/h:b	(Ljava/lang/String;)Ljava/io/InputStream;
    //   5: astore_2
    //   6: aload_2
    //   7: aload_1
    //   8: invokestatic 203	org/a/a/e/b:a	(Ljava/io/InputStream;Ljava/lang/String;)Lorg/a/a/f;
    //   11: astore_3
    //   12: aload_0
    //   13: getfield 45	org/a/a/e/h:d	Ljava/util/Map;
    //   16: astore 4
    //   18: new 140	java/lang/ref/SoftReference
    //   21: astore 5
    //   23: aload 5
    //   25: aload_3
    //   26: invokespecial 148	java/lang/ref/SoftReference:<init>	(Ljava/lang/Object;)V
    //   29: aload 4
    //   31: aload_1
    //   32: aload 5
    //   34: invokeinterface 152 3 0
    //   39: pop
    //   40: aload_2
    //   41: ifnull +7 -> 48
    //   44: aload_2
    //   45: invokevirtual 204	java/io/InputStream:close	()V
    //   48: aload_3
    //   49: areturn
    //   50: astore_1
    //   51: goto +43 -> 94
    //   54: astore_3
    //   55: goto +12 -> 67
    //   58: astore_1
    //   59: aconst_null
    //   60: astore_2
    //   61: goto +33 -> 94
    //   64: astore_3
    //   65: aconst_null
    //   66: astore_2
    //   67: aload_3
    //   68: invokevirtual 209	java/lang/Exception:printStackTrace	()V
    //   71: aload_0
    //   72: getfield 45	org/a/a/e/h:d	Ljava/util/Map;
    //   75: astore_3
    //   76: aload_3
    //   77: aload_1
    //   78: invokeinterface 213 2 0
    //   83: pop
    //   84: aload_2
    //   85: ifnull +7 -> 92
    //   88: aload_2
    //   89: invokevirtual 204	java/io/InputStream:close	()V
    //   92: aconst_null
    //   93: areturn
    //   94: aload_2
    //   95: ifnull +7 -> 102
    //   98: aload_2
    //   99: invokevirtual 204	java/io/InputStream:close	()V
    //   102: aload_1
    //   103: athrow
    //   104: pop
    //   105: goto -57 -> 48
    //   108: pop
    //   109: goto -17 -> 92
    //   112: pop
    //   113: goto -11 -> 102
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	116	0	this	h
    //   0	116	1	paramString	String
    //   5	94	2	localInputStream	InputStream
    //   11	38	3	localf	org.a.a.f
    //   54	1	3	localIOException1	IOException
    //   64	4	3	localIOException2	IOException
    //   75	2	3	localMap1	Map
    //   16	14	4	localMap2	Map
    //   21	12	5	localSoftReference	SoftReference
    //   104	1	9	localIOException3	IOException
    //   108	1	10	localIOException4	IOException
    //   112	1	11	localIOException5	IOException
    // Exception table:
    //   from	to	target	type
    //   7	11	50	finally
    //   12	16	50	finally
    //   18	21	50	finally
    //   25	29	50	finally
    //   32	40	50	finally
    //   67	71	50	finally
    //   71	75	50	finally
    //   77	84	50	finally
    //   7	11	54	java/io/IOException
    //   12	16	54	java/io/IOException
    //   18	21	54	java/io/IOException
    //   25	29	54	java/io/IOException
    //   32	40	54	java/io/IOException
    //   1	5	58	finally
    //   1	5	64	java/io/IOException
    //   44	48	104	java/io/IOException
    //   88	92	108	java/io/IOException
    //   98	102	112	java/io/IOException
  }
  
  public final Set a()
  {
    return e;
  }
  
  public final org.a.a.f a(String paramString)
  {
    for (;;)
    {
      boolean bool = false;
      org.a.a.f localf = null;
      if (paramString == null) {
        return null;
      }
      Object localObject = d.get(paramString);
      if (localObject == null) {
        return null;
      }
      bool = localObject instanceof SoftReference;
      if (bool)
      {
        localObject = (SoftReference)localObject;
        localf = (org.a.a.f)((SoftReference)localObject).get();
        if (localf != null) {
          return localf;
        }
        return c(paramString);
      }
      bool = paramString.equals(localObject);
      if (bool) {
        return c(paramString);
      }
      paramString = (String)localObject;
      paramString = (String)localObject;
    }
  }
}

/* Location:
 * Qualified Name:     org.a.a.e.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */