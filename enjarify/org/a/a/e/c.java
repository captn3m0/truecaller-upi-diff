package org.a.a.e;

import java.text.DateFormatSymbols;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public final class c
  implements e
{
  private HashMap a;
  private HashMap b;
  
  public c()
  {
    HashMap localHashMap = a();
    a = localHashMap;
    localHashMap = a();
    b = localHashMap;
  }
  
  private static HashMap a()
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>(7);
    return localHashMap;
  }
  
  private String[] c(Locale paramLocale, String paramString1, String paramString2)
  {
    Object localObject1 = null;
    if ((paramLocale != null) && (paramString1 != null) && (paramString2 != null)) {
      try
      {
        Object localObject2 = a;
        localObject2 = ((HashMap)localObject2).get(paramLocale);
        localObject2 = (Map)localObject2;
        if (localObject2 == null)
        {
          localObject2 = a;
          localObject3 = a();
          ((HashMap)localObject2).put(paramLocale, localObject3);
          localObject2 = localObject3;
        }
        Object localObject3 = ((Map)localObject2).get(paramString1);
        localObject3 = (Map)localObject3;
        if (localObject3 == null)
        {
          localObject3 = a();
          ((Map)localObject2).put(paramString1, localObject3);
          localObject2 = Locale.ENGLISH;
          localObject2 = org.a.a.e.a((Locale)localObject2);
          localObject2 = ((DateFormatSymbols)localObject2).getZoneStrings();
          int i = localObject2.length;
          int j = 0;
          Object localObject4 = null;
          int k;
          Object localObject6;
          for (;;)
          {
            k = 5;
            if (j >= i) {
              break;
            }
            localObject5 = localObject2[j];
            if (localObject5 != null)
            {
              int m = localObject5.length;
              if (m >= k)
              {
                localObject6 = localObject5[0];
                boolean bool1 = paramString1.equals(localObject6);
                if (bool1) {
                  break label206;
                }
              }
            }
            j += 1;
          }
          Object localObject5 = null;
          label206:
          paramLocale = org.a.a.e.a(paramLocale);
          paramLocale = paramLocale.getZoneStrings();
          int i1 = paramLocale.length;
          i = 0;
          Object localObject7 = null;
          while (i < i1)
          {
            localObject4 = paramLocale[i];
            if (localObject4 != null)
            {
              int n = localObject4.length;
              if (n >= k)
              {
                localObject6 = localObject4[0];
                boolean bool2 = paramString1.equals(localObject6);
                if (bool2)
                {
                  localObject1 = localObject4;
                  break;
                }
              }
            }
            i += 1;
          }
          if ((localObject5 != null) && (localObject1 != null))
          {
            int i2 = 2;
            paramString1 = localObject5[i2];
            localObject2 = new String[i2];
            localObject7 = localObject1[i2];
            localObject2[0] = localObject7;
            i = 1;
            localObject4 = localObject1[i];
            localObject2[i] = localObject4;
            ((Map)localObject3).put(paramString1, localObject2);
            paramString1 = localObject5[i2];
            i1 = 4;
            localObject4 = localObject5[i1];
            boolean bool3 = paramString1.equals(localObject4);
            j = 3;
            if (bool3)
            {
              paramString1 = new java/lang/StringBuilder;
              paramString1.<init>();
              String str = localObject5[i1];
              paramString1.append(str);
              str = "-Summer";
              paramString1.append(str);
              paramString1 = paramString1.toString();
              paramLocale = new String[i2];
              localObject2 = localObject1[i1];
              paramLocale[0] = localObject2;
              localObject1 = localObject1[j];
              paramLocale[i] = localObject1;
              ((Map)localObject3).put(paramString1, paramLocale);
            }
            else
            {
              paramString1 = localObject5[i1];
              paramLocale = new String[i2];
              localObject2 = localObject1[i1];
              paramLocale[0] = localObject2;
              localObject1 = localObject1[j];
              paramLocale[i] = localObject1;
              ((Map)localObject3).put(paramString1, paramLocale);
            }
          }
        }
        paramLocale = ((Map)localObject3).get(paramString2);
        paramLocale = (String[])paramLocale;
        paramLocale = (String[])paramLocale;
        return paramLocale;
      }
      finally {}
    }
    return null;
  }
  
  public final String a(Locale paramLocale, String paramString1, String paramString2)
  {
    paramLocale = c(paramLocale, paramString1, paramString2);
    if (paramLocale == null) {
      return null;
    }
    return paramLocale[0];
  }
  
  public final String[] a(Locale paramLocale, String paramString1, String paramString2, boolean paramBoolean)
  {
    Object localObject1 = null;
    if ((paramLocale != null) && (paramString1 != null) && (paramString2 != null))
    {
      paramString2 = "Etc/";
      try
      {
        boolean bool1 = paramString1.startsWith(paramString2);
        int j = 4;
        if (bool1) {
          paramString1 = paramString1.substring(j);
        }
        paramString2 = b;
        paramString2 = paramString2.get(paramLocale);
        paramString2 = (Map)paramString2;
        if (paramString2 == null)
        {
          paramString2 = b;
          localObject2 = a();
          paramString2.put(paramLocale, localObject2);
          paramString2 = (String)localObject2;
        }
        Object localObject2 = paramString2.get(paramString1);
        localObject2 = (Map)localObject2;
        if (localObject2 == null)
        {
          localObject2 = a();
          paramString2.put(paramString1, localObject2);
          paramString2 = Locale.ENGLISH;
          paramString2 = org.a.a.e.a(paramString2);
          paramString2 = paramString2.getZoneStrings();
          int k = paramString2.length;
          int m = 0;
          Object localObject3 = null;
          int n;
          Object localObject5;
          for (;;)
          {
            n = 5;
            if (m >= k) {
              break;
            }
            localObject4 = paramString2[m];
            if (localObject4 != null)
            {
              int i1 = localObject4.length;
              if (i1 >= n)
              {
                localObject5 = localObject4[0];
                boolean bool2 = paramString1.equals(localObject5);
                if (bool2) {
                  break label213;
                }
              }
            }
            m += 1;
          }
          Object localObject4 = null;
          label213:
          paramLocale = org.a.a.e.a(paramLocale);
          paramLocale = paramLocale.getZoneStrings();
          int i = paramLocale.length;
          k = 0;
          Object localObject6 = null;
          while (k < i)
          {
            localObject3 = paramLocale[k];
            if (localObject3 != null)
            {
              int i2 = localObject3.length;
              if (i2 >= n)
              {
                localObject5 = localObject3[0];
                boolean bool3 = paramString1.equals(localObject5);
                if (bool3)
                {
                  localObject1 = localObject3;
                  break;
                }
              }
            }
            k += 1;
          }
          if ((localObject4 != null) && (localObject1 != null))
          {
            paramLocale = Boolean.TRUE;
            int i3 = 2;
            paramString2 = new String[i3];
            localObject6 = localObject1[i3];
            paramString2[0] = localObject6;
            k = 1;
            localObject3 = localObject1[k];
            paramString2[k] = localObject3;
            ((Map)localObject2).put(paramLocale, paramString2);
            paramLocale = Boolean.FALSE;
            paramString1 = new String[i3];
            paramString2 = localObject1[j];
            paramString1[0] = paramString2;
            i = 3;
            paramString2 = localObject1[i];
            paramString1[k] = paramString2;
            ((Map)localObject2).put(paramLocale, paramString1);
          }
        }
        paramLocale = Boolean.valueOf(paramBoolean);
        paramLocale = ((Map)localObject2).get(paramLocale);
        paramLocale = (String[])paramLocale;
        paramLocale = (String[])paramLocale;
        return paramLocale;
      }
      finally {}
    }
    return null;
  }
  
  public final String b(Locale paramLocale, String paramString1, String paramString2)
  {
    paramLocale = c(paramLocale, paramString1, paramString2);
    if (paramLocale == null) {
      return null;
    }
    return paramLocale[1];
  }
}

/* Location:
 * Qualified Name:     org.a.a.e.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */