package org.a.a.e;

import java.io.DataInput;
import org.a.a.a;
import org.a.a.b.q;
import org.a.a.c;

final class b$b
{
  final char a;
  final int b;
  final int c;
  final int d;
  final boolean e;
  final int f;
  
  private b$b(char paramChar, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean, int paramInt4)
  {
    char c1 = 'u';
    if (paramChar != c1)
    {
      c1 = 'w';
      if (paramChar != c1)
      {
        c1 = 's';
        if (paramChar != c1)
        {
          IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
          String str = String.valueOf(paramChar);
          str = "Unknown mode: ".concat(str);
          localIllegalArgumentException.<init>(str);
          throw localIllegalArgumentException;
        }
      }
    }
    a = paramChar;
    b = paramInt1;
    c = paramInt2;
    d = paramInt3;
    e = paramBoolean;
    f = paramInt4;
  }
  
  private long a(a parama, long paramLong)
  {
    long l;
    try
    {
      l = c(parama, paramLong);
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      i = b;
      j = 2;
      if (i != j) {
        break label97;
      }
    }
    int i = c;
    int j = 29;
    c localc;
    if (i == j)
    {
      for (;;)
      {
        localc = parama.E();
        boolean bool = localc.b(paramLong);
        if (bool) {
          break;
        }
        localc = parama.E();
        i = 1;
        paramLong = localc.a(paramLong, i);
      }
      l = c(parama, paramLong);
      return l;
    }
    label97:
    throw localc;
  }
  
  static b a(DataInput paramDataInput)
  {
    b localb = new org/a/a/e/b$b;
    char c1 = (char)paramDataInput.readUnsignedByte();
    int i = paramDataInput.readUnsignedByte();
    int j = paramDataInput.readByte();
    int k = paramDataInput.readUnsignedByte();
    boolean bool = paramDataInput.readBoolean();
    int m = (int)b.a(paramDataInput);
    localb.<init>(c1, i, j, k, bool, m);
    return localb;
  }
  
  private long b(a parama, long paramLong)
  {
    long l;
    try
    {
      l = c(parama, paramLong);
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      i = b;
      j = 2;
      if (i != j) {
        break label97;
      }
    }
    int i = c;
    int j = 29;
    c localc;
    if (i == j)
    {
      for (;;)
      {
        localc = parama.E();
        boolean bool = localc.b(paramLong);
        if (bool) {
          break;
        }
        localc = parama.E();
        i = -1;
        paramLong = localc.a(paramLong, i);
      }
      l = c(parama, paramLong);
      return l;
    }
    label97:
    throw localc;
  }
  
  private long c(a parama, long paramLong)
  {
    int i = c;
    long l;
    if (i >= 0)
    {
      parama = parama.u();
      i = c;
      l = parama.b(paramLong, i);
    }
    else
    {
      c localc = parama.u();
      int j = 1;
      paramLong = localc.b(paramLong, j);
      localc = parama.C();
      paramLong = localc.a(paramLong, j);
      parama = parama.u();
      i = c;
      l = parama.a(paramLong, i);
    }
    return l;
  }
  
  private long d(a parama, long paramLong)
  {
    c localc = parama.t();
    int i = localc.a(paramLong);
    int j = d - i;
    if (j != 0)
    {
      boolean bool = e;
      if (bool)
      {
        if (j < 0) {
          j += 7;
        }
      }
      else if (j > 0) {
        j += -7;
      }
      parama = parama.t();
      paramLong = parama.a(paramLong, j);
    }
    return paramLong;
  }
  
  public final long a(long paramLong, int paramInt1, int paramInt2)
  {
    int i = a;
    c localc1 = null;
    int j = 119;
    if (i == j)
    {
      paramInt1 += paramInt2;
    }
    else
    {
      paramInt2 = 115;
      if (i != paramInt2) {
        paramInt1 = 0;
      }
    }
    long l1 = paramInt1;
    paramLong += l1;
    q localq = q.L();
    c localc2 = localq.C();
    int k = b;
    long l2 = localc2.b(paramLong, k);
    c localc3 = localq.e();
    long l3 = localc3.b(l2, 0);
    c localc4 = localq.e();
    int m = f;
    l3 = localc4.a(l3, m);
    l3 = a(localq, l3);
    k = d;
    m = 1;
    boolean bool;
    c localc5;
    if (k == 0)
    {
      bool = l3 < paramLong;
      if (!bool)
      {
        localc5 = localq.E();
        paramLong = localc5.a(l3, m);
        l3 = a(localq, paramLong);
      }
    }
    else
    {
      l3 = d(localq, l3);
      bool = l3 < paramLong;
      if (!bool)
      {
        localc5 = localq.E();
        paramLong = localc5.a(l3, m);
        localc1 = localq.C();
        j = b;
        paramLong = localc1.b(paramLong, j);
        paramLong = a(localq, paramLong);
        l3 = d(localq, paramLong);
      }
    }
    return l3 - l1;
  }
  
  public final long b(long paramLong, int paramInt1, int paramInt2)
  {
    int i = a;
    c localc1 = null;
    int j = 119;
    if (i == j)
    {
      paramInt1 += paramInt2;
    }
    else
    {
      paramInt2 = 115;
      if (i != paramInt2) {
        paramInt1 = 0;
      }
    }
    long l1 = paramInt1;
    paramLong += l1;
    q localq = q.L();
    c localc2 = localq.C();
    int k = b;
    long l2 = localc2.b(paramLong, k);
    c localc3 = localq.e();
    long l3 = localc3.b(l2, 0);
    c localc4 = localq.e();
    int m = f;
    l3 = localc4.a(l3, m);
    l3 = b(localq, l3);
    k = d;
    m = -1;
    boolean bool;
    c localc5;
    if (k == 0)
    {
      bool = l3 < paramLong;
      if (!bool)
      {
        localc5 = localq.E();
        paramLong = localc5.a(l3, m);
        l3 = b(localq, paramLong);
      }
    }
    else
    {
      l3 = d(localq, l3);
      bool = l3 < paramLong;
      if (!bool)
      {
        localc5 = localq.E();
        paramLong = localc5.a(l3, m);
        localc1 = localq.C();
        j = b;
        paramLong = localc1.b(paramLong, j);
        paramLong = b(localq, paramLong);
        l3 = d(localq, paramLong);
      }
    }
    return l3 - l1;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this == paramObject) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof b;
    if (bool2)
    {
      paramObject = (b)paramObject;
      int i = a;
      int k = a;
      if (i == k)
      {
        i = b;
        k = b;
        if (i == k)
        {
          i = c;
          k = c;
          if (i == k)
          {
            i = d;
            k = d;
            if (i == k)
            {
              boolean bool3 = e;
              boolean bool4 = e;
              if (bool3 == bool4)
              {
                int j = f;
                int m = f;
                if (j == m) {
                  return bool1;
                }
              }
            }
          }
        }
      }
      return false;
    }
    return false;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("[OfYear]\nMode: ");
    char c1 = a;
    localStringBuilder.append(c1);
    c1 = '\n';
    localStringBuilder.append(c1);
    localStringBuilder.append("MonthOfYear: ");
    int i = b;
    localStringBuilder.append(i);
    localStringBuilder.append(c1);
    localStringBuilder.append("DayOfMonth: ");
    i = c;
    localStringBuilder.append(i);
    localStringBuilder.append(c1);
    localStringBuilder.append("DayOfWeek: ");
    i = d;
    localStringBuilder.append(i);
    localStringBuilder.append(c1);
    localStringBuilder.append("AdvanceDayOfWeek: ");
    boolean bool = e;
    localStringBuilder.append(bool);
    localStringBuilder.append(c1);
    localStringBuilder.append("MillisOfDay: ");
    int j = f;
    localStringBuilder.append(j);
    localStringBuilder.append(c1);
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     org.a.a.e.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */