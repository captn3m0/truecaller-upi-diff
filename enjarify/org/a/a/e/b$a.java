package org.a.a.e;

import org.a.a.f;

final class b$a
  extends f
{
  private static final long serialVersionUID = 6941492635554961361L;
  final int c;
  final b.d d;
  final b.d e;
  
  b$a(String paramString, int paramInt, b.d paramd1, b.d paramd2)
  {
    super(paramString);
    c = paramInt;
    d = paramd1;
    e = paramd2;
  }
  
  private b.d j(long paramLong)
  {
    int i = c;
    b.d locald1 = d;
    b.d locald2 = e;
    long l;
    try
    {
      int j = c;
      l = locald1.a(paramLong, i, j);
    }
    catch (IllegalArgumentException|ArithmeticException localIllegalArgumentException1)
    {
      l = paramLong;
    }
    try
    {
      int k = c;
      paramLong = locald2.a(paramLong, i, k);
    }
    catch (IllegalArgumentException|ArithmeticException localIllegalArgumentException2)
    {
      boolean bool;
      for (;;) {}
    }
    bool = l < paramLong;
    if (bool) {
      return locald1;
    }
    return locald2;
  }
  
  public final String a(long paramLong)
  {
    return jb;
  }
  
  public final int b(long paramLong)
  {
    int i = c;
    int j = jc;
    return i + j;
  }
  
  public final int c(long paramLong)
  {
    return c;
  }
  
  public final boolean d()
  {
    return false;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this == paramObject) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof a;
    if (bool2)
    {
      paramObject = (a)paramObject;
      Object localObject1 = b;
      Object localObject2 = b;
      bool2 = ((String)localObject1).equals(localObject2);
      if (bool2)
      {
        int i = c;
        int j = c;
        if (i == j)
        {
          localObject1 = d;
          localObject2 = d;
          boolean bool3 = ((b.d)localObject1).equals(localObject2);
          if (bool3)
          {
            localObject1 = e;
            paramObject = e;
            boolean bool4 = ((b.d)localObject1).equals(paramObject);
            if (bool4) {
              return bool1;
            }
          }
        }
      }
      return false;
    }
    return false;
  }
  
  public final long h(long paramLong)
  {
    int i = c;
    b.d locald1 = d;
    b.d locald2 = e;
    long l1 = 0L;
    long l2;
    try
    {
      int j = c;
      l2 = locald1.a(paramLong, i, j);
      boolean bool2 = paramLong < l1;
      if (bool2)
      {
        bool2 = l2 < l1;
        if (bool2) {
          l2 = paramLong;
        }
      }
    }
    catch (IllegalArgumentException|ArithmeticException localIllegalArgumentException1)
    {
      l2 = paramLong;
    }
    try
    {
      int k = c;
      long l3 = locald2.a(paramLong, i, k);
      boolean bool3 = paramLong < l1;
      if (bool3)
      {
        bool3 = l3 < l1;
        if (bool3) {}
      }
      else
      {
        paramLong = l3;
      }
    }
    catch (IllegalArgumentException|ArithmeticException localIllegalArgumentException2)
    {
      boolean bool1;
      for (;;) {}
    }
    bool1 = l2 < paramLong;
    if (bool1) {
      return paramLong;
    }
    return l2;
  }
  
  public final long i(long paramLong)
  {
    long l1 = 1L;
    paramLong += l1;
    int i = c;
    b.d locald1 = d;
    b.d locald2 = e;
    long l2 = 0L;
    long l3;
    try
    {
      int j = c;
      l3 = locald1.b(paramLong, i, j);
      boolean bool2 = paramLong < l2;
      if (bool2)
      {
        bool2 = l3 < l2;
        if (bool2) {
          l3 = paramLong;
        }
      }
    }
    catch (IllegalArgumentException|ArithmeticException localIllegalArgumentException1)
    {
      l3 = paramLong;
    }
    try
    {
      int k = c;
      long l4 = locald2.b(paramLong, i, k);
      boolean bool3 = paramLong < l2;
      if (bool3)
      {
        bool3 = l4 < l2;
        if (bool3) {}
      }
      else
      {
        paramLong = l4;
      }
    }
    catch (IllegalArgumentException|ArithmeticException localIllegalArgumentException2)
    {
      boolean bool1;
      for (;;) {}
    }
    bool1 = l3 < paramLong;
    if (bool1) {
      paramLong = l3;
    }
    return paramLong - l1;
  }
}

/* Location:
 * Qualified Name:     org.a.a.e.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */