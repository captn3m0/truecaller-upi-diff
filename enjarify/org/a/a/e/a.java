package org.a.a.e;

import org.a.a.f;

public final class a
  extends f
{
  private static final int c = 0;
  private static final long serialVersionUID = 5472298452022250685L;
  private final f d;
  private final transient a.a[] e;
  
  static
  {
    Object localObject = "org.joda.time.tz.CachedDateTimeZone.size";
    int i;
    try
    {
      localObject = Integer.getInteger((String)localObject);
    }
    catch (SecurityException localSecurityException)
    {
      i = 0;
      localObject = null;
    }
    int j = 1;
    if (localObject == null)
    {
      i = 512;
    }
    else
    {
      i = ((Integer)localObject).intValue() + -1;
      int k = 0;
      while (i > 0)
      {
        k += 1;
        i >>= 1;
      }
      i = j << k;
    }
    c = i - j;
  }
  
  private a(f paramf)
  {
    super((String)localObject);
    localObject = new a.a[c + 1];
    e = ((a.a[])localObject);
    d = paramf;
  }
  
  public static a b(f paramf)
  {
    a locala = new org/a/a/e/a;
    locala.<init>(paramf);
    return locala;
  }
  
  private a.a j(long paramLong)
  {
    int i = 32;
    long l1 = paramLong >> i;
    int j = (int)l1;
    a.a[] arrayOfa = e;
    int k = c & j;
    a.a locala = arrayOfa[k];
    if (locala != null)
    {
      long l2 = a >> i;
      i = (int)l2;
      if (i == j) {}
    }
    else
    {
      locala = k(paramLong);
      arrayOfa[k] = locala;
    }
    return locala;
  }
  
  private a.a k(long paramLong)
  {
    long l1 = -4294967296L;
    paramLong &= l1;
    a.a locala1 = new org/a/a/e/a$a;
    f localf1 = d;
    locala1.<init>(localf1, paramLong);
    long l2 = 0xFFFFFFFF | paramLong;
    Object localObject = locala1;
    for (;;)
    {
      f localf2 = d;
      long l3 = localf2.h(paramLong);
      boolean bool1 = l3 < paramLong;
      if (!bool1) {
        break;
      }
      boolean bool2 = l3 < l2;
      if (bool2) {
        break;
      }
      a.a locala2 = new org/a/a/e/a$a;
      f localf3 = d;
      locala2.<init>(localf3, l3);
      c = locala2;
      localObject = locala2;
      paramLong = l3;
    }
    return locala1;
  }
  
  public final String a(long paramLong)
  {
    long l;
    for (a.a locala1 = j(paramLong);; locala1 = c)
    {
      a.a locala2 = c;
      if (locala2 == null) {
        break;
      }
      locala2 = c;
      l = a;
      boolean bool = paramLong < l;
      if (bool) {
        break;
      }
    }
    Object localObject = d;
    if (localObject == null)
    {
      localObject = b;
      l = a;
      localObject = ((f)localObject).a(l);
      d = ((String)localObject);
    }
    return d;
  }
  
  public final int b(long paramLong)
  {
    long l;
    for (a.a locala1 = j(paramLong);; locala1 = c)
    {
      a.a locala2 = c;
      if (locala2 == null) {
        break;
      }
      locala2 = c;
      l = a;
      boolean bool = paramLong < l;
      if (bool) {
        break;
      }
    }
    int i = e;
    int j = -1 << -1;
    if (i == j)
    {
      f localf = b;
      l = a;
      i = localf.b(l);
      e = i;
    }
    return e;
  }
  
  public final int c(long paramLong)
  {
    long l;
    for (a.a locala1 = j(paramLong);; locala1 = c)
    {
      a.a locala2 = c;
      if (locala2 == null) {
        break;
      }
      locala2 = c;
      l = a;
      boolean bool = paramLong < l;
      if (bool) {
        break;
      }
    }
    int i = f;
    int j = -1 << -1;
    if (i == j)
    {
      f localf = b;
      l = a;
      i = localf.c(l);
      f = i;
    }
    return f;
  }
  
  public final boolean d()
  {
    return d.d();
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    boolean bool = paramObject instanceof a;
    if (bool)
    {
      f localf = d;
      paramObject = d;
      return localf.equals(paramObject);
    }
    return false;
  }
  
  public final long h(long paramLong)
  {
    return d.h(paramLong);
  }
  
  public final int hashCode()
  {
    return d.hashCode();
  }
  
  public final long i(long paramLong)
  {
    return d.i(paramLong);
  }
}

/* Location:
 * Qualified Name:     org.a.a.e.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */