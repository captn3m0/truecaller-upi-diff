package org.a.a.e;

import java.io.DataInput;

final class b$d
{
  final b.b a;
  final String b;
  final int c;
  
  private b$d(b.b paramb, String paramString, int paramInt)
  {
    a = paramb;
    b = paramString;
    c = paramInt;
  }
  
  static d a(DataInput paramDataInput)
  {
    d locald = new org/a/a/e/b$d;
    b.b localb = b.b.a(paramDataInput);
    String str = paramDataInput.readUTF();
    int i = (int)b.a(paramDataInput);
    locald.<init>(localb, str, i);
    return locald;
  }
  
  public final long a(long paramLong, int paramInt1, int paramInt2)
  {
    return a.a(paramLong, paramInt1, paramInt2);
  }
  
  public final long b(long paramLong, int paramInt1, int paramInt2)
  {
    return a.b(paramLong, paramInt1, paramInt2);
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this == paramObject) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof d;
    if (bool2)
    {
      paramObject = (d)paramObject;
      int i = c;
      int j = c;
      if (i == j)
      {
        Object localObject = b;
        String str = b;
        boolean bool3 = ((String)localObject).equals(str);
        if (bool3)
        {
          localObject = a;
          paramObject = a;
          boolean bool4 = ((b.b)localObject).equals(paramObject);
          if (bool4) {
            return bool1;
          }
        }
      }
      return false;
    }
    return false;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    Object localObject = a;
    localStringBuilder.append(localObject);
    localStringBuilder.append(" named ");
    localObject = b;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(" at ");
    int i = c;
    localStringBuilder.append(i);
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     org.a.a.e.b.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */