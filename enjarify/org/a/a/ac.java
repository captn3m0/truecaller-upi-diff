package org.a.a;

final class ac
  extends f
{
  static final f c;
  private static final long serialVersionUID = -3513011772763289092L;
  
  static
  {
    ac localac = new org/a/a/ac;
    localac.<init>();
    c = localac;
  }
  
  public ac()
  {
    super("UTC");
  }
  
  public final String a(long paramLong)
  {
    return "UTC";
  }
  
  public final int b(long paramLong)
  {
    return 0;
  }
  
  public final int c(long paramLong)
  {
    return 0;
  }
  
  public final boolean d()
  {
    return true;
  }
  
  public final int e(long paramLong)
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    return paramObject instanceof ac;
  }
  
  public final long h(long paramLong)
  {
    return paramLong;
  }
  
  public final int hashCode()
  {
    return b.hashCode();
  }
  
  public final long i(long paramLong)
  {
    return paramLong;
  }
}

/* Location:
 * Qualified Name:     org.a.a.ac
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */