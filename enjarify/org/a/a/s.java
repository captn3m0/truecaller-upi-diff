package org.a.a;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public final class s
  implements Serializable
{
  static int a = 0;
  static int b = 1;
  static int c = 2;
  static int d = 3;
  static int e = 4;
  static int f = 5;
  static int g = 6;
  static int h = 7;
  private static final Map j;
  private static s k;
  private static s l;
  private static s m;
  private static s n;
  private static final long serialVersionUID = 2274324892792009998L;
  public final j[] i;
  private final String o;
  private final int[] p;
  
  static
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>(32);
    j = localHashMap;
  }
  
  private s(String paramString, j[] paramArrayOfj, int[] paramArrayOfInt)
  {
    o = paramString;
    i = paramArrayOfj;
    p = paramArrayOfInt;
  }
  
  public static s a()
  {
    s locals = k;
    if (locals == null)
    {
      locals = new org/a/a/s;
      String str = "Standard";
      int i1 = 8;
      j[] arrayOfj = new j[i1];
      j localj = j.j();
      arrayOfj[0] = localj;
      localj = j.i();
      arrayOfj[1] = localj;
      localj = j.g();
      arrayOfj[2] = localj;
      localj = j.f();
      arrayOfj[3] = localj;
      localj = j.d();
      arrayOfj[4] = localj;
      localj = j.c();
      arrayOfj[5] = localj;
      localj = j.b();
      arrayOfj[6] = localj;
      int i2 = 7;
      localj = j.a();
      arrayOfj[i2] = localj;
      int[] arrayOfInt = new int[i1];
      int[] tmp116_114 = arrayOfInt;
      int[] tmp117_116 = tmp116_114;
      int[] tmp117_116 = tmp116_114;
      tmp117_116[0] = 0;
      tmp117_116[1] = 1;
      int[] tmp124_117 = tmp117_116;
      int[] tmp124_117 = tmp117_116;
      tmp124_117[2] = 2;
      tmp124_117[3] = 3;
      int[] tmp131_124 = tmp124_117;
      int[] tmp131_124 = tmp124_117;
      tmp131_124[4] = 4;
      tmp131_124[5] = 5;
      tmp131_124[6] = 6;
      tmp131_124[7] = 7;
      locals.<init>(str, arrayOfj, arrayOfInt);
      k = locals;
    }
    return locals;
  }
  
  public static s b()
  {
    s locals = l;
    if (locals == null)
    {
      locals = new org/a/a/s;
      String str = "Time";
      int i1 = 4;
      j[] arrayOfj = new j[i1];
      j localj = j.d();
      arrayOfj[0] = localj;
      localj = j.c();
      arrayOfj[1] = localj;
      localj = j.b();
      arrayOfj[2] = localj;
      localj = j.a();
      arrayOfj[3] = localj;
      int i2 = 8;
      int[] arrayOfInt = new int[i2];
      int[] tmp74_72 = arrayOfInt;
      int[] tmp75_74 = tmp74_72;
      int[] tmp75_74 = tmp74_72;
      tmp75_74[0] = -1;
      tmp75_74[1] = -1;
      int[] tmp82_75 = tmp75_74;
      int[] tmp82_75 = tmp75_74;
      tmp82_75[2] = -1;
      tmp82_75[3] = -1;
      int[] tmp89_82 = tmp82_75;
      int[] tmp89_82 = tmp82_75;
      tmp89_82[4] = 0;
      tmp89_82[5] = 1;
      tmp89_82[6] = 2;
      tmp89_82[7] = 3;
      locals.<init>(str, arrayOfj, arrayOfInt);
      l = locals;
    }
    return locals;
  }
  
  public static s c()
  {
    s locals = m;
    if (locals == null)
    {
      locals = new org/a/a/s;
      String str = "Days";
      int i1 = 1;
      j[] arrayOfj = new j[i1];
      j localj = j.f();
      arrayOfj[0] = localj;
      int i2 = 8;
      int[] arrayOfInt = new int[i2];
      int[] tmp44_42 = arrayOfInt;
      int[] tmp45_44 = tmp44_42;
      int[] tmp45_44 = tmp44_42;
      tmp45_44[0] = -1;
      tmp45_44[1] = -1;
      int[] tmp52_45 = tmp45_44;
      int[] tmp52_45 = tmp45_44;
      tmp52_45[2] = -1;
      tmp52_45[3] = 0;
      int[] tmp59_52 = tmp52_45;
      int[] tmp59_52 = tmp52_45;
      tmp59_52[4] = -1;
      tmp59_52[5] = -1;
      tmp59_52[6] = -1;
      tmp59_52[7] = -1;
      locals.<init>(str, arrayOfj, arrayOfInt);
      m = locals;
    }
    return locals;
  }
  
  public static s d()
  {
    s locals = n;
    if (locals == null)
    {
      locals = new org/a/a/s;
      String str = "Seconds";
      int i1 = 1;
      j[] arrayOfj = new j[i1];
      j localj = j.b();
      arrayOfj[0] = localj;
      int i2 = 8;
      int[] arrayOfInt = new int[i2];
      int[] tmp44_42 = arrayOfInt;
      int[] tmp45_44 = tmp44_42;
      int[] tmp45_44 = tmp44_42;
      tmp45_44[0] = -1;
      tmp45_44[1] = -1;
      int[] tmp52_45 = tmp45_44;
      int[] tmp52_45 = tmp45_44;
      tmp52_45[2] = -1;
      tmp52_45[3] = -1;
      int[] tmp59_52 = tmp52_45;
      int[] tmp59_52 = tmp52_45;
      tmp59_52[4] = -1;
      tmp59_52[5] = -1;
      tmp59_52[6] = 0;
      tmp59_52[7] = -1;
      locals.<init>(str, arrayOfj, arrayOfInt);
      n = locals;
    }
    return locals;
  }
  
  final int a(aa paramaa, int paramInt)
  {
    int[] arrayOfInt = p;
    paramInt = arrayOfInt[paramInt];
    int i1 = -1;
    if (paramInt == i1) {
      return 0;
    }
    return paramaa.d(paramInt);
  }
  
  final boolean a(int paramInt1, int[] paramArrayOfInt, int paramInt2)
  {
    int[] arrayOfInt = p;
    paramInt1 = arrayOfInt[paramInt1];
    int i1 = -1;
    if (paramInt1 != i1)
    {
      paramArrayOfInt[paramInt1] = paramInt2;
      return true;
    }
    UnsupportedOperationException localUnsupportedOperationException = new java/lang/UnsupportedOperationException;
    localUnsupportedOperationException.<init>("Field is not supported");
    throw localUnsupportedOperationException;
  }
  
  public final boolean a(j paramj)
  {
    int i1 = b(paramj);
    return i1 >= 0;
  }
  
  public final int b(j paramj)
  {
    j[] arrayOfj = i;
    int i1 = arrayOfj.length;
    int i2 = 0;
    while (i2 < i1)
    {
      j localj = i[i2];
      if (localj == paramj) {
        return i2;
      }
      i2 += 1;
    }
    return -1;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    boolean bool = paramObject instanceof s;
    if (!bool) {
      return false;
    }
    paramObject = (s)paramObject;
    j[] arrayOfj = i;
    paramObject = i;
    return Arrays.equals(arrayOfj, (Object[])paramObject);
  }
  
  public final int hashCode()
  {
    int i1 = 0;
    int i2 = 0;
    for (;;)
    {
      Object localObject = i;
      int i3 = localObject.length;
      if (i1 >= i3) {
        break;
      }
      localObject = localObject[i1];
      int i4 = localObject.hashCode();
      i2 += i4;
      i1 += 1;
    }
    return i2;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("PeriodType[");
    String str = o;
    localStringBuilder.append(str);
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     org.a.a.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */