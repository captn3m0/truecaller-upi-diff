package org.a.a.d;

import java.util.Locale;
import org.a.a.a;
import org.a.a.c;
import org.a.a.d;
import org.a.a.f;
import org.a.a.z;

final class c$n
  extends c.f
{
  protected c$n(d paramd, int paramInt, boolean paramBoolean)
  {
    super(paramd, paramInt, paramBoolean);
  }
  
  public final int a()
  {
    return b;
  }
  
  public final void a(Appendable paramAppendable, long paramLong, a parama, int paramInt, f paramf, Locale paramLocale)
  {
    try
    {
      d locald = a;
      parama = locald.a(parama);
      int i = parama.a(paramLong);
      h.a(paramAppendable, i);
      return;
    }
    catch (RuntimeException localRuntimeException)
    {
      paramAppendable.append((char)-3);
    }
  }
  
  public final void a(Appendable paramAppendable, z paramz, Locale paramLocale)
  {
    paramLocale = a;
    boolean bool = paramz.b(paramLocale);
    char c = (char)-3;
    if (bool) {
      try
      {
        paramLocale = a;
        int i = paramz.a(paramLocale);
        h.a(paramAppendable, i);
        return;
      }
      catch (RuntimeException localRuntimeException)
      {
        paramAppendable.append(c);
        return;
      }
    }
    paramAppendable.append(c);
  }
}

/* Location:
 * Qualified Name:     org.a.a.d.c.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */