package org.a.a.d;

import java.util.Locale;
import org.a.a.c;
import org.a.a.i;

final class e$a
  implements Comparable
{
  c a;
  int b;
  String c;
  Locale d;
  
  public final int a(a parama)
  {
    parama = a;
    i locali1 = a.e();
    i locali2 = parama.e();
    int i = e.a(locali1, locali2);
    if (i != 0) {
      return i;
    }
    locali1 = a.d();
    parama = parama.d();
    return e.a(locali1, parama);
  }
  
  final long a(long paramLong, boolean paramBoolean)
  {
    Object localObject = c;
    if (localObject == null)
    {
      localObject = a;
      int i = b;
      paramLong = ((c)localObject).c(paramLong, i);
    }
    else
    {
      c localc1 = a;
      Locale localLocale = d;
      paramLong = localc1.a(paramLong, (String)localObject, localLocale);
    }
    if (paramBoolean)
    {
      c localc2 = a;
      paramLong = localc2.d(paramLong);
    }
    return paramLong;
  }
  
  final void a(c paramc, int paramInt)
  {
    a = paramc;
    b = paramInt;
    c = null;
    d = null;
  }
}

/* Location:
 * Qualified Name:     org.a.a.d.e.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */