package org.a.a.d;

import java.util.Locale;
import org.a.a.a;
import org.a.a.f;
import org.a.a.z;

final class c$a
  implements k, m
{
  private final char a;
  
  c$a(char paramChar)
  {
    a = paramChar;
  }
  
  public final int a()
  {
    return 1;
  }
  
  public final int a(e parame, CharSequence paramCharSequence, int paramInt)
  {
    int i = paramCharSequence.length();
    if (paramInt >= i) {
      return paramInt ^ 0xFFFFFFFF;
    }
    i = paramCharSequence.charAt(paramInt);
    int j = a;
    if (i != j)
    {
      i = Character.toUpperCase(i);
      j = Character.toUpperCase(j);
      if (i != j)
      {
        i = Character.toLowerCase(i);
        j = Character.toLowerCase(j);
        if (i != j) {
          return paramInt ^ 0xFFFFFFFF;
        }
      }
    }
    return paramInt + 1;
  }
  
  public final void a(Appendable paramAppendable, long paramLong, a parama, int paramInt, f paramf, Locale paramLocale)
  {
    char c = a;
    paramAppendable.append(c);
  }
  
  public final void a(Appendable paramAppendable, z paramz, Locale paramLocale)
  {
    char c = a;
    paramAppendable.append(c);
  }
  
  public final int b()
  {
    return 1;
  }
}

/* Location:
 * Qualified Name:     org.a.a.d.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */