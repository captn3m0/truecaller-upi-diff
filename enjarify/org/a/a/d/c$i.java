package org.a.a.d;

import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.a.a.a.g;
import org.a.a.c;
import org.a.a.d;
import org.a.a.f;
import org.a.a.q;
import org.a.a.q.a;
import org.a.a.z;

final class c$i
  implements k, m
{
  private static Map a;
  private final d b;
  private final boolean c;
  
  static
  {
    ConcurrentHashMap localConcurrentHashMap = new java/util/concurrent/ConcurrentHashMap;
    localConcurrentHashMap.<init>();
    a = localConcurrentHashMap;
  }
  
  c$i(d paramd, boolean paramBoolean)
  {
    b = paramd;
    c = paramBoolean;
  }
  
  public final int a()
  {
    boolean bool = c;
    if (bool) {
      return 6;
    }
    return 20;
  }
  
  public final int a(e parame, CharSequence paramCharSequence, int paramInt)
  {
    i locali = this;
    Object localObject1 = parame;
    int i = paramInt;
    Locale localLocale = b;
    Object localObject2 = (Map)a.get(localLocale);
    if (localObject2 == null)
    {
      localObject2 = new java/util/concurrent/ConcurrentHashMap;
      ((ConcurrentHashMap)localObject2).<init>();
      localObject3 = a;
      ((Map)localObject3).put(localLocale, localObject2);
    }
    Object localObject3 = b;
    localObject3 = (Object[])((Map)localObject2).get(localObject3);
    int j = 1;
    int k;
    Object localObject7;
    Object localObject10;
    Object localObject11;
    if (localObject3 == null)
    {
      localObject3 = new java/util/concurrent/ConcurrentHashMap;
      k = 32;
      ((ConcurrentHashMap)localObject3).<init>(k);
      Object localObject4 = new org/a/a/q;
      Object localObject5 = f.a;
      ((q)localObject4).<init>((f)localObject5);
      localObject5 = b;
      if (localObject5 != null)
      {
        Object localObject6 = b;
        localObject6 = ((d)localObject5).a((org.a.a.a)localObject6);
        boolean bool1 = ((c)localObject6).c();
        if (bool1)
        {
          localObject5 = new org/a/a/q$a;
          ((q.a)localObject5).<init>((q)localObject4, (c)localObject6);
          localObject4 = ((org.a.a.c.a)localObject5).a();
          int n = ((c)localObject4).g();
          localObject6 = ((org.a.a.c.a)localObject5).a();
          int i2 = ((c)localObject6).h();
          int m = i2 - n;
          if (m > k) {
            return i ^ 0xFFFFFFFF;
          }
          localObject7 = ((org.a.a.c.a)localObject5).a();
          k = ((c)localObject7).a(localLocale);
          while (n <= i2)
          {
            Object localObject8 = a;
            Object localObject9 = b;
            q localq = a;
            long l1 = a;
            long l2 = ((c)localObject9).b(l1, n);
            ((q)localObject8).a(l2);
            localObject8 = ((q.a)localObject5).b(localLocale);
            localObject9 = Boolean.TRUE;
            ((Map)localObject3).put(localObject8, localObject9);
            localObject8 = ((q.a)localObject5).b(localLocale).toLowerCase(localLocale);
            localObject9 = Boolean.TRUE;
            ((Map)localObject3).put(localObject8, localObject9);
            localObject8 = ((q.a)localObject5).b(localLocale).toUpperCase(localLocale);
            localObject9 = Boolean.TRUE;
            ((Map)localObject3).put(localObject8, localObject9);
            localObject8 = ((q.a)localObject5).a(localLocale);
            localObject9 = Boolean.TRUE;
            ((Map)localObject3).put(localObject8, localObject9);
            localObject8 = ((q.a)localObject5).a(localLocale).toLowerCase(localLocale);
            localObject9 = Boolean.TRUE;
            ((Map)localObject3).put(localObject8, localObject9);
            localObject8 = ((q.a)localObject5).a(localLocale).toUpperCase(localLocale);
            localObject9 = Boolean.TRUE;
            ((Map)localObject3).put(localObject8, localObject9);
            n += 1;
          }
          localObject4 = "en";
          localObject5 = localLocale.getLanguage();
          boolean bool2 = ((String)localObject4).equals(localObject5);
          if (bool2)
          {
            localObject4 = b;
            localObject5 = d.w();
            if (localObject4 == localObject5)
            {
              localObject4 = Boolean.TRUE;
              ((Map)localObject3).put("BCE", localObject4);
              localObject4 = Boolean.TRUE;
              ((Map)localObject3).put("bce", localObject4);
              localObject4 = Boolean.TRUE;
              ((Map)localObject3).put("CE", localObject4);
              localObject7 = "ce";
              localObject4 = Boolean.TRUE;
              ((Map)localObject3).put(localObject7, localObject4);
              k = 3;
            }
          }
          int i1 = 2;
          localObject4 = new Object[i1];
          localObject4[0] = localObject3;
          localObject5 = Integer.valueOf(k);
          localObject4[j] = localObject5;
          localObject10 = b;
          ((Map)localObject2).put(localObject10, localObject4);
          localObject2 = localObject3;
        }
        else
        {
          localObject1 = new java/lang/IllegalArgumentException;
          localObject11 = new java/lang/StringBuilder;
          ((StringBuilder)localObject11).<init>("Field '");
          ((StringBuilder)localObject11).append(localObject5);
          ((StringBuilder)localObject11).append("' is not supported");
          localObject11 = ((StringBuilder)localObject11).toString();
          ((IllegalArgumentException)localObject1).<init>((String)localObject11);
          throw ((Throwable)localObject1);
        }
      }
      else
      {
        localObject1 = new java/lang/IllegalArgumentException;
        ((IllegalArgumentException)localObject1).<init>("The DateTimeFieldType must not be null");
        throw ((Throwable)localObject1);
      }
    }
    else
    {
      localObject2 = (Map)localObject3[0];
      localObject3 = (Integer)localObject3[j];
      k = ((Integer)localObject3).intValue();
    }
    int i3 = paramCharSequence.length();
    j = i + k;
    i3 = Math.min(i3, j);
    while (i3 > i)
    {
      localObject10 = paramCharSequence;
      localObject7 = paramCharSequence.subSequence(i, i3).toString();
      boolean bool3 = ((Map)localObject2).containsKey(localObject7);
      if (bool3)
      {
        localObject11 = b;
        localObject2 = parame.a();
        localObject1 = a;
        localObject1 = ((d)localObject11).a((org.a.a.a)localObject1);
        a = ((c)localObject1);
        b = 0;
        c = ((String)localObject7);
        d = localLocale;
        return i3;
      }
      i3 += -1;
    }
    return i ^ 0xFFFFFFFF;
  }
  
  public final void a(Appendable paramAppendable, long paramLong, org.a.a.a parama, int paramInt, f paramf, Locale paramLocale)
  {
    try
    {
      d locald = b;
      parama = locald.a(parama);
      paramInt = c;
      String str;
      if (paramInt != 0) {
        str = parama.b(paramLong, paramLocale);
      } else {
        str = parama.a(paramLong, paramLocale);
      }
      paramAppendable.append(str);
      return;
    }
    catch (RuntimeException localRuntimeException)
    {
      paramAppendable.append((char)-3);
    }
  }
  
  public final void a(Appendable paramAppendable, z paramz, Locale paramLocale)
  {
    try
    {
      Object localObject = b;
      boolean bool1 = paramz.b((d)localObject);
      if (bool1)
      {
        localObject = b;
        org.a.a.a locala = paramz.b();
        localObject = ((d)localObject).a(locala);
        boolean bool2 = c;
        if (bool2) {
          paramz = ((c)localObject).b(paramz, paramLocale);
        } else {
          paramz = ((c)localObject).a(paramz, paramLocale);
        }
      }
      else
      {
        paramz = "�";
      }
      paramAppendable.append(paramz);
      return;
    }
    catch (RuntimeException localRuntimeException)
    {
      paramAppendable.append((char)-3);
    }
  }
  
  public final int b()
  {
    return a();
  }
}

/* Location:
 * Qualified Name:     org.a.a.d.c.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */