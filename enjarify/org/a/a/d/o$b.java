package org.a.a.d;

import java.util.HashSet;
import java.util.Set;

final class o$b
  extends o.d
{
  private final o.f a;
  private final o.f b;
  private final String[] c;
  
  o$b(o.f paramf1, o.f paramf2)
  {
    a = paramf1;
    b = paramf2;
    paramf1 = new java/util/HashSet;
    paramf1.<init>();
    paramf2 = a.a();
    int i = paramf2.length;
    int j = 0;
    while (j < i)
    {
      String str1 = paramf2[j];
      String[] arrayOfString = b.a();
      int k = arrayOfString.length;
      int m = 0;
      while (m < k)
      {
        String str2 = arrayOfString[m];
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        localStringBuilder.append(str1);
        localStringBuilder.append(str2);
        str2 = localStringBuilder.toString();
        paramf1.add(str2);
        m += 1;
      }
      j += 1;
    }
    paramf2 = new String[paramf1.size()];
    paramf1 = (String[])paramf1.toArray(paramf2);
    c = paramf1;
  }
  
  public final int a(int paramInt)
  {
    int i = a.a(paramInt);
    paramInt = b.a(paramInt);
    return i + paramInt;
  }
  
  public final void a(StringBuffer paramStringBuffer, int paramInt)
  {
    a.a(paramStringBuffer, paramInt);
    b.a(paramStringBuffer, paramInt);
  }
  
  public final String[] a()
  {
    return (String[])c.clone();
  }
}

/* Location:
 * Qualified Name:     org.a.a.d.o.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */