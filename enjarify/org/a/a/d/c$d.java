package org.a.a.d;

import java.util.Locale;
import org.a.a.a;
import org.a.a.d;
import org.a.a.f;
import org.a.a.z;

final class c$d
  implements k, m
{
  protected int a;
  protected int b;
  private final d c;
  
  protected c$d(d paramd, int paramInt1, int paramInt2)
  {
    c = paramd;
    int i = 18;
    if (paramInt2 <= i) {
      i = paramInt2;
    }
    a = paramInt1;
    b = i;
  }
  
  private void a(Appendable paramAppendable, long paramLong, a parama)
  {
    d locald = c;
    parama = locald.a(parama);
    int i = a;
    try
    {
      paramLong = parama.i(paramLong);
      long l1 = 0L;
      int j = 48;
      boolean bool1 = paramLong < l1;
      if (!bool1)
      {
        for (;;)
        {
          i += -1;
          if (i < 0) {
            break;
          }
          paramAppendable.append(j);
        }
        return;
      }
      long[] arrayOfLong = a(paramLong, parama);
      int m = 0;
      l1 = arrayOfLong[0];
      int n = 1;
      int i1 = (int)arrayOfLong[n];
      long l2 = 0x7FFFFFFF & l1;
      boolean bool2 = l2 < l1;
      String str;
      if (!bool2)
      {
        i2 = (int)l1;
        str = Integer.toString(i2);
      }
      else
      {
        str = Long.toString(l1);
      }
      int i2 = str.length();
      while (i2 < i1)
      {
        paramAppendable.append(j);
        i += -1;
        i1 += -1;
      }
      if (i < i1)
      {
        int i3;
        while ((i < i1) && (i2 > n))
        {
          int k = i2 + -1;
          k = str.charAt(k);
          if (k != j) {
            break;
          }
          i1 += -1;
          i2 += -1;
        }
        i1 = str.length();
        if (i3 < i1)
        {
          while (m < i3)
          {
            i1 = str.charAt(m);
            paramAppendable.append(i1);
            m += 1;
          }
          return;
        }
      }
      paramAppendable.append(str);
      return;
    }
    catch (RuntimeException localRuntimeException)
    {
      c.a(paramAppendable, i);
    }
  }
  
  private long[] a(long paramLong, org.a.a.c paramc)
  {
    paramc = paramc.d();
    long l1 = paramc.d();
    int i = b;
    long l2;
    for (;;)
    {
      switch (i)
      {
      default: 
        l2 = 1L;
        break;
      case 18: 
        l2 = 1000000000000000000L;
        break;
      case 17: 
        l2 = 100000000000000000L;
        break;
      case 16: 
        l2 = 10000000000000000L;
        break;
      case 15: 
        l2 = 1000000000000000L;
        break;
      case 14: 
        l2 = 100000000000000L;
        break;
      case 13: 
        l2 = 10000000000000L;
        break;
      case 12: 
        l2 = 1000000000000L;
        break;
      case 11: 
        l2 = 100000000000L;
        break;
      case 10: 
        l2 = 10000000000L;
        break;
      case 9: 
        l2 = 1000000000L;
        break;
      case 8: 
        l2 = 100000000L;
        break;
      case 7: 
        l2 = 10000000L;
        break;
      case 6: 
        l2 = 1000000L;
        break;
      case 5: 
        l2 = 100000L;
        break;
      case 4: 
        l2 = 10000L;
        break;
      case 3: 
        l2 = 1000L;
        break;
      case 2: 
        l2 = 100;
        break;
      case 1: 
        l2 = 10;
      }
      long l3 = l1 * l2 / l2;
      boolean bool = l3 < l1;
      if (!bool) {
        break;
      }
      i += -1;
    }
    long[] arrayOfLong = new long[2];
    paramLong = paramLong * l2 / l1;
    arrayOfLong[0] = paramLong;
    long l4 = i;
    arrayOfLong[1] = l4;
    return arrayOfLong;
  }
  
  public final int a()
  {
    return b;
  }
  
  public final int a(e parame, CharSequence paramCharSequence, int paramInt)
  {
    Object localObject1 = c;
    Object localObject2 = a;
    localObject1 = ((d)localObject1).a((a)localObject2);
    int i = b;
    int j = paramCharSequence.length() - paramInt;
    i = Math.min(i, j);
    org.a.a.i locali = ((org.a.a.c)localObject1).d();
    long l1 = locali.d();
    long l2 = 10;
    l1 *= l2;
    long l3 = 0L;
    int k = 0;
    while (k < i)
    {
      int m = paramInt + k;
      m = paramCharSequence.charAt(m);
      int n = 48;
      if (m < n) {
        break;
      }
      n = 57;
      if (m > n) {
        break;
      }
      k += 1;
      l1 /= l2;
      m += -48;
      long l4 = m * l1;
      l3 += l4;
    }
    l3 /= l2;
    if (k == 0) {
      return paramInt ^ 0xFFFFFFFF;
    }
    long l5 = 2147483647L;
    boolean bool = l3 < l5;
    if (bool) {
      return paramInt ^ 0xFFFFFFFF;
    }
    paramCharSequence = new org/a/a/c/k;
    localObject2 = d.a();
    locali = org.a.a.c.i.a;
    localObject1 = ((org.a.a.c)localObject1).d();
    paramCharSequence.<init>((d)localObject2, locali, (org.a.a.i)localObject1);
    int i1 = (int)l3;
    parame.a().a(paramCharSequence, i1);
    return paramInt + k;
  }
  
  public final void a(Appendable paramAppendable, long paramLong, a parama, int paramInt, f paramf, Locale paramLocale)
  {
    a(paramAppendable, paramLong, parama);
  }
  
  public final void a(Appendable paramAppendable, z paramz, Locale paramLocale)
  {
    long l = paramz.b().a(paramz, 0L);
    paramz = paramz.b();
    a(paramAppendable, l, paramz);
  }
  
  public final int b()
  {
    return b;
  }
}

/* Location:
 * Qualified Name:     org.a.a.d.c.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */