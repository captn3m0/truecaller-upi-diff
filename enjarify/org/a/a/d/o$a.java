package org.a.a.d;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.a.a.aa;

final class o$a
  implements p, q
{
  private final q[] a;
  private final p[] b;
  
  o$a(List paramList)
  {
    ArrayList localArrayList1 = new java/util/ArrayList;
    localArrayList1.<init>();
    ArrayList localArrayList2 = new java/util/ArrayList;
    localArrayList2.<init>();
    a(paramList, localArrayList1, localArrayList2);
    int i = localArrayList1.size();
    if (i <= 0)
    {
      a = null;
    }
    else
    {
      i = localArrayList1.size();
      paramList = new q[i];
      paramList = (q[])localArrayList1.toArray(paramList);
      a = paramList;
    }
    i = localArrayList2.size();
    if (i <= 0)
    {
      b = null;
      return;
    }
    paramList = new p[localArrayList2.size()];
    paramList = (p[])localArrayList2.toArray(paramList);
    b = paramList;
  }
  
  private static void a(List paramList1, List paramList2, List paramList3)
  {
    int i = paramList1.size();
    int j = 0;
    while (j < i)
    {
      Object localObject = paramList1.get(j);
      boolean bool = localObject instanceof q;
      if (bool)
      {
        bool = localObject instanceof a;
        if (bool)
        {
          localObject = a;
          a(paramList2, (Object[])localObject);
        }
        else
        {
          paramList2.add(localObject);
        }
      }
      int k = j + 1;
      localObject = paramList1.get(k);
      bool = localObject instanceof p;
      if (bool)
      {
        bool = localObject instanceof a;
        if (bool)
        {
          localObject = b;
          a(paramList3, (Object[])localObject);
        }
        else
        {
          paramList3.add(localObject);
        }
      }
      j += 2;
    }
  }
  
  private static void a(List paramList, Object[] paramArrayOfObject)
  {
    if (paramArrayOfObject != null)
    {
      int i = 0;
      for (;;)
      {
        int j = paramArrayOfObject.length;
        if (i >= j) {
          break;
        }
        Object localObject = paramArrayOfObject[i];
        paramList.add(localObject);
        i += 1;
      }
    }
  }
  
  public final int a(aa paramaa, int paramInt, Locale paramLocale)
  {
    q[] arrayOfq = a;
    int i = arrayOfq.length;
    int j = 0;
    while (j < paramInt)
    {
      i += -1;
      if (i < 0) {
        break;
      }
      q localq = arrayOfq[i];
      int k = -1 >>> 1;
      int m = localq.a(paramaa, k, paramLocale);
      j += m;
    }
    return j;
  }
  
  public final int a(aa paramaa, Locale paramLocale)
  {
    q[] arrayOfq = a;
    int i = arrayOfq.length;
    int j = 0;
    for (;;)
    {
      i += -1;
      if (i < 0) {
        break;
      }
      q localq = arrayOfq[i];
      int k = localq.a(paramaa, paramLocale);
      j += k;
    }
    return j;
  }
  
  public final void a(StringBuffer paramStringBuffer, aa paramaa, Locale paramLocale)
  {
    q[] arrayOfq = a;
    int i = arrayOfq.length;
    int j = 0;
    while (j < i)
    {
      q localq = arrayOfq[j];
      localq.a(paramStringBuffer, paramaa, paramLocale);
      j += 1;
    }
  }
}

/* Location:
 * Qualified Name:     org.a.a.d.o.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */