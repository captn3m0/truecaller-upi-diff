package org.a.a.d;

final class l
  implements d, k
{
  private final k a;
  
  private l(k paramk)
  {
    a = paramk;
  }
  
  static d a(k paramk)
  {
    boolean bool = paramk instanceof f;
    if (bool) {
      return a;
    }
    bool = paramk instanceof d;
    if (bool) {
      return (d)paramk;
    }
    if (paramk == null) {
      return null;
    }
    l locall = new org/a/a/d/l;
    locall.<init>(paramk);
    return locall;
  }
  
  public final int a(e parame, CharSequence paramCharSequence, int paramInt)
  {
    return a.a(parame, paramCharSequence, paramInt);
  }
  
  public final int a(e parame, String paramString, int paramInt)
  {
    return a.a(parame, paramString, paramInt);
  }
  
  public final int b()
  {
    return a.b();
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {
      return true;
    }
    boolean bool = paramObject instanceof l;
    if (bool)
    {
      paramObject = (l)paramObject;
      k localk = a;
      paramObject = a;
      return localk.equals(paramObject);
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     org.a.a.d.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */