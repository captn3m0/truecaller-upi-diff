package org.a.a.d;

import java.util.Arrays;
import java.util.Locale;
import org.a.a.a;
import org.a.a.c;
import org.a.a.d;
import org.a.a.f;
import org.a.a.i;
import org.a.a.j;
import org.a.a.l;

public final class e
{
  final a a;
  final Locale b;
  f c;
  Integer d;
  Integer e;
  e.a[] f;
  int g;
  boolean h;
  private final long i;
  private final int j;
  private final f k;
  private final Integer l;
  private Object m;
  
  public e(a parama, Locale paramLocale, Integer paramInteger, int paramInt)
  {
    parama = org.a.a.e.a(parama);
    long l1 = 0L;
    i = l1;
    f localf = parama.a();
    k = localf;
    parama = parama.b();
    a = parama;
    if (paramLocale == null) {
      paramLocale = Locale.getDefault();
    }
    b = paramLocale;
    j = paramInt;
    l = paramInteger;
    parama = k;
    c = parama;
    parama = l;
    e = parama;
    parama = new e.a[8];
    f = parama;
  }
  
  static int a(i parami1, i parami2)
  {
    if (parami1 != null)
    {
      boolean bool1 = parami1.b();
      if (bool1)
      {
        if (parami2 != null)
        {
          bool1 = parami2.b();
          if (bool1) {
            return -parami1.compareTo(parami2);
          }
        }
        return 1;
      }
    }
    if (parami2 != null)
    {
      boolean bool2 = parami2.b();
      if (bool2) {
        return -1;
      }
    }
    return 0;
  }
  
  public final long a(CharSequence paramCharSequence)
  {
    Object localObject1;
    int n;
    e.a locala;
    int i9;
    for (;;)
    {
      localObject1 = f;
      n = g;
      boolean bool1 = h;
      if (bool1)
      {
        localObject1 = (e.a[])((e.a[])localObject1).clone();
        f = ((e.a[])localObject1);
        h = false;
      }
      int i1 = 10;
      if (n > i1)
      {
        Arrays.sort((Object[])localObject1, 0, n);
      }
      else
      {
        int i2 = 0;
        localObject3 = null;
        while (i2 < n)
        {
          int i6 = i2;
          while (i6 > 0)
          {
            int i8 = i6 + -1;
            Object localObject4 = localObject1[i8];
            locala = localObject1[i6];
            i9 = ((e.a)localObject4).a(locala);
            if (i9 <= 0) {
              break;
            }
            localObject4 = localObject1[i6];
            locala = localObject1[i8];
            localObject1[i6] = locala;
            localObject1[i8] = localObject4;
            int i7;
            i6 += -1;
          }
          i2 += 1;
        }
      }
      if (n <= 0) {
        break;
      }
      localObject3 = j.i();
      Object localObject5 = a;
      localObject3 = ((j)localObject3).a((a)localObject5);
      localObject5 = j.f();
      Object localObject6 = a;
      localObject5 = ((j)localObject5).a((a)localObject6);
      localObject6 = 0a.d();
      i3 = a((i)localObject6, (i)localObject3);
      if (i3 < 0) {
        break;
      }
      i3 = a((i)localObject6, (i)localObject5);
      if (i3 > 0) {
        break;
      }
      localObject1 = d.s();
      n = j;
      a((d)localObject1, n);
    }
    long l1 = i;
    int i3 = 0;
    Object localObject3 = null;
    for (;;)
    {
      i9 = 1;
      if (i3 < n) {
        try
        {
          locala = localObject1[i3];
          l1 = locala.a(l1, i9);
          i3 += 1;
        }
        catch (org.a.a.k localk)
        {
          break label364;
        }
      }
    }
    int i4 = 0;
    localObject3 = null;
    label364:
    Object localObject7;
    while (i4 < n)
    {
      locala = localk[i4];
      int i10 = n + -1;
      boolean bool2;
      if (i4 == i10) {
        bool2 = true;
      } else {
        bool2 = false;
      }
      l1 = locala.a(l1, bool2);
      int i5;
      i4 += 1;
      continue;
      if (paramCharSequence != null)
      {
        localObject7 = new java/lang/StringBuilder;
        localObject3 = "Cannot parse \"";
        ((StringBuilder)localObject7).<init>((String)localObject3);
        ((StringBuilder)localObject7).append(paramCharSequence);
        char c1 = '"';
        ((StringBuilder)localObject7).append(c1);
        paramCharSequence = ((StringBuilder)localObject7).toString();
        localObject7 = a;
        if (localObject7 != null)
        {
          if (paramCharSequence != null)
          {
            localObject7 = new java/lang/StringBuilder;
            ((StringBuilder)localObject7).<init>();
            ((StringBuilder)localObject7).append(paramCharSequence);
            ((StringBuilder)localObject7).append(": ");
            paramCharSequence = a;
            ((StringBuilder)localObject7).append(paramCharSequence);
            paramCharSequence = ((StringBuilder)localObject7).toString();
            a = paramCharSequence;
          }
        }
        else {
          a = paramCharSequence;
        }
      }
      throw localk;
    }
    Object localObject2 = d;
    if (localObject2 != null)
    {
      int i11 = ((Integer)localObject2).intValue();
      long l2 = i11;
      l1 -= l2;
    }
    else
    {
      localObject2 = c;
      if (localObject2 != null)
      {
        int i12 = ((f)localObject2).e(l1);
        long l3 = i12;
        l1 -= l3;
        localObject7 = c;
        n = ((f)localObject7).b(l1);
        if (i12 != n)
        {
          localObject2 = new java/lang/StringBuilder;
          ((StringBuilder)localObject2).<init>("Illegal instant due to time zone offset transition (");
          localObject7 = c;
          ((StringBuilder)localObject2).append(localObject7);
          n = 41;
          ((StringBuilder)localObject2).append(n);
          localObject2 = ((StringBuilder)localObject2).toString();
          if (paramCharSequence != null)
          {
            localObject7 = new java/lang/StringBuilder;
            localObject3 = "Cannot parse \"";
            ((StringBuilder)localObject7).<init>((String)localObject3);
            ((StringBuilder)localObject7).append(paramCharSequence);
            paramCharSequence = "\": ";
            ((StringBuilder)localObject7).append(paramCharSequence);
            ((StringBuilder)localObject7).append((String)localObject2);
            localObject2 = ((StringBuilder)localObject7).toString();
          }
          paramCharSequence = new org/a/a/l;
          paramCharSequence.<init>((String)localObject2);
          throw paramCharSequence;
        }
      }
    }
    return l1;
  }
  
  final long a(k paramk, CharSequence paramCharSequence)
  {
    int n = 0;
    IllegalArgumentException localIllegalArgumentException = null;
    int i1 = paramk.a(this, paramCharSequence, 0);
    if (i1 >= 0)
    {
      n = paramCharSequence.length();
      if (i1 >= n) {
        return a(paramCharSequence);
      }
    }
    else
    {
      i1 ^= 0xFFFFFFFF;
    }
    localIllegalArgumentException = new java/lang/IllegalArgumentException;
    paramk = h.a(paramCharSequence.toString(), i1);
    localIllegalArgumentException.<init>(paramk);
    throw localIllegalArgumentException;
  }
  
  final e.a a()
  {
    Object localObject1 = f;
    int n = g;
    int i1 = localObject1.length;
    if (n != i1)
    {
      boolean bool = h;
      if (!bool) {}
    }
    else
    {
      i2 = localObject1.length;
      if (n == i2) {
        i2 = n * 2;
      } else {
        i2 = localObject1.length;
      }
      localObject2 = new e.a[i2];
      System.arraycopy(localObject1, 0, localObject2, 0, n);
      f = ((e.a[])localObject2);
      h = false;
      localObject1 = localObject2;
    }
    int i2 = 0;
    m = null;
    Object localObject2 = localObject1[n];
    if (localObject2 == null)
    {
      localObject2 = new org/a/a/d/e$a;
      ((e.a)localObject2).<init>();
      localObject1[n] = localObject2;
    }
    n += 1;
    g = n;
    return (e.a)localObject2;
  }
  
  public final void a(Integer paramInteger)
  {
    m = null;
    d = paramInteger;
  }
  
  public final void a(d paramd, int paramInt)
  {
    e.a locala = a();
    a locala1 = a;
    paramd = paramd.a(locala1);
    locala.a(paramd, paramInt);
  }
  
  public final void a(f paramf)
  {
    m = null;
    c = paramf;
  }
  
  public final boolean a(Object paramObject)
  {
    boolean bool = paramObject instanceof e.b;
    if (bool)
    {
      Object localObject = paramObject;
      localObject = (e.b)paramObject;
      bool = ((e.b)localObject).a(this);
      if (bool)
      {
        m = paramObject;
        return true;
      }
    }
    return false;
  }
  
  public final Object b()
  {
    Object localObject = m;
    if (localObject == null)
    {
      localObject = new org/a/a/d/e$b;
      ((e.b)localObject).<init>(this);
      m = localObject;
    }
    return m;
  }
}

/* Location:
 * Qualified Name:     org.a.a.d.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */