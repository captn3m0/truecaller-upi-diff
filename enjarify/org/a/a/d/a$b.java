package org.a.a.d;

import java.util.Locale;

final class a$b
{
  private final int a;
  private final Locale b;
  
  public a$b(int paramInt1, int paramInt2, int paramInt3, Locale paramLocale)
  {
    b = paramLocale;
    paramInt2 <<= 4;
    paramInt1 += paramInt2;
    paramInt2 = paramInt3 << 8;
    paramInt1 += paramInt2;
    a = paramInt1;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this == paramObject) {
      return bool1;
    }
    if (paramObject == null) {
      return false;
    }
    boolean bool2 = paramObject instanceof b;
    if (!bool2) {
      return false;
    }
    paramObject = (b)paramObject;
    int i = a;
    int j = a;
    if (i != j) {
      return false;
    }
    Locale localLocale = b;
    if (localLocale == null)
    {
      paramObject = b;
      if (paramObject != null) {
        return false;
      }
    }
    else
    {
      paramObject = b;
      boolean bool3 = localLocale.equals(paramObject);
      if (!bool3) {
        return false;
      }
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    int i = (a + 31) * 31;
    Locale localLocale = b;
    int j;
    if (localLocale == null)
    {
      j = 0;
      localLocale = null;
    }
    else
    {
      j = localLocale.hashCode();
    }
    return i + j;
  }
}

/* Location:
 * Qualified Name:     org.a.a.d.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */