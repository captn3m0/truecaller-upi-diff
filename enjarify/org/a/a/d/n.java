package org.a.a.d;

import java.util.Locale;
import org.a.a.aa;
import org.a.a.s;

public final class n
{
  final q a;
  final p b;
  private final Locale c;
  private final s d;
  
  public n(q paramq, p paramp)
  {
    a = paramq;
    b = paramp;
    c = null;
    d = null;
  }
  
  private n(q paramq, p paramp, Locale paramLocale, s params)
  {
    a = paramq;
    b = paramp;
    c = paramLocale;
    d = params;
  }
  
  private void a()
  {
    Object localObject = a;
    if (localObject != null) {
      return;
    }
    localObject = new java/lang/UnsupportedOperationException;
    ((UnsupportedOperationException)localObject).<init>("Printing not supported");
    throw ((Throwable)localObject);
  }
  
  private static void b(aa paramaa)
  {
    if (paramaa != null) {
      return;
    }
    paramaa = new java/lang/IllegalArgumentException;
    paramaa.<init>("Period must not be null");
    throw paramaa;
  }
  
  public final String a(aa paramaa)
  {
    a();
    b(paramaa);
    q localq = a;
    StringBuffer localStringBuffer = new java/lang/StringBuffer;
    Locale localLocale = c;
    int i = localq.a(paramaa, localLocale);
    localStringBuffer.<init>(i);
    localLocale = c;
    localq.a(localStringBuffer, paramaa, localLocale);
    return localStringBuffer.toString();
  }
  
  public final n a(s params)
  {
    Object localObject = d;
    if (params == localObject) {
      return this;
    }
    localObject = new org/a/a/d/n;
    q localq = a;
    p localp = b;
    Locale localLocale = c;
    ((n)localObject).<init>(localq, localp, localLocale, params);
    return (n)localObject;
  }
}

/* Location:
 * Qualified Name:     org.a.a.d.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */