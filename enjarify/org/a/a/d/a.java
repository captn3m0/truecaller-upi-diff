package org.a.a.d;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReferenceArray;
import org.a.a.d;

public final class a
{
  private static final ConcurrentHashMap a;
  private static final AtomicReferenceArray b;
  
  static
  {
    Object localObject = new java/util/concurrent/ConcurrentHashMap;
    ((ConcurrentHashMap)localObject).<init>();
    a = (ConcurrentHashMap)localObject;
    localObject = new java/util/concurrent/atomic/AtomicReferenceArray;
    ((AtomicReferenceArray)localObject).<init>(25);
    b = (AtomicReferenceArray)localObject;
  }
  
  private static int a(char paramChar)
  {
    char c = '-';
    if (paramChar != c)
    {
      c = 'F';
      if (paramChar != c)
      {
        c = 'S';
        if (paramChar != c)
        {
          switch (paramChar)
          {
          default: 
            IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
            String str = String.valueOf(paramChar);
            str = "Invalid style character: ".concat(str);
            localIllegalArgumentException.<init>(str);
            throw localIllegalArgumentException;
          case 'M': 
            return 2;
          }
          return 1;
        }
        return 3;
      }
      return 0;
    }
    return 4;
  }
  
  private static String a(String paramString, int[] paramArrayOfInt)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    int i = paramArrayOfInt[0];
    int j = paramString.length();
    char c1 = paramString.charAt(i);
    char c2 = 'Z';
    char c3 = 'A';
    int m;
    int n;
    int k;
    if ((c1 < c3) || (c1 > c2))
    {
      m = 122;
      n = 97;
      if ((c1 < n) || (c1 > m)) {}
    }
    else
    {
      localStringBuilder.append(c1);
      for (;;)
      {
        k = i + 1;
        if (k >= j) {
          break;
        }
        c3 = paramString.charAt(k);
        if (c3 != c1) {
          break;
        }
        localStringBuilder.append(c1);
        i = k;
      }
    }
    c1 = '\'';
    localStringBuilder.append(c1);
    int i1 = 0;
    while (i < j)
    {
      int i2 = paramString.charAt(i);
      if (i2 == c1)
      {
        int i3 = i + 1;
        if (i3 < j)
        {
          char c4 = paramString.charAt(i3);
          if (c4 == c1)
          {
            localStringBuilder.append(i2);
            i = i3;
            break label246;
          }
        }
        i1 ^= 0x1;
      }
      else
      {
        if ((i1 == 0) && (((i2 >= c3) && (i2 <= k)) || ((i2 >= n) && (i2 <= m))))
        {
          i += -1;
          break;
        }
        localStringBuilder.append(i2);
      }
      label246:
      i += 1;
    }
    paramArrayOfInt[0] = i;
    return localStringBuilder.toString();
  }
  
  private static b a(int paramInt1, int paramInt2)
  {
    int i = 4;
    if (paramInt1 == i) {
      i = 1;
    } else if (paramInt2 == i) {
      i = 0;
    } else {
      i = 2;
    }
    a.a locala = new org/a/a/d/a$a;
    locala.<init>(paramInt1, paramInt2, i);
    b localb = new org/a/a/d/b;
    localb.<init>(locala, locala);
    return localb;
  }
  
  public static b a(String paramString)
  {
    if (paramString != null)
    {
      int i = paramString.length();
      if (i != 0)
      {
        Object localObject = (b)a.get(paramString);
        if (localObject == null)
        {
          localObject = new org/a/a/d/c;
          ((c)localObject).<init>();
          a((c)localObject, paramString);
          localObject = ((c)localObject).a();
          ConcurrentHashMap localConcurrentHashMap = a;
          int j = localConcurrentHashMap.size();
          int k = 500;
          if (j < k)
          {
            localConcurrentHashMap = a;
            paramString = (b)localConcurrentHashMap.putIfAbsent(paramString, localObject);
            if (paramString != null) {
              localObject = paramString;
            }
          }
        }
        return (b)localObject;
      }
    }
    paramString = new java/lang/IllegalArgumentException;
    paramString.<init>("Invalid pattern specification");
    throw paramString;
  }
  
  private static void a(c paramc, String paramString)
  {
    int i = paramString.length();
    int j = 1;
    int[] arrayOfInt = new int[j];
    int k = 0;
    Object localObject1 = null;
    while (k < i)
    {
      arrayOfInt[0] = k;
      localObject1 = a(paramString, arrayOfInt);
      int i1 = arrayOfInt[0];
      int i2 = ((String)localObject1).length();
      if (i2 == 0) {
        break;
      }
      int i5 = ((String)localObject1).charAt(0);
      int i6 = 4;
      int i9 = 2;
      Object localObject2;
      int i3;
      int m;
      int i8;
      switch (i5)
      {
      default: 
        paramc = new java/lang/IllegalArgumentException;
        paramString = String.valueOf(localObject1);
        paramString = "Illegal pattern component: ".concat(paramString);
        paramc.<init>(paramString);
        throw paramc;
      case 122: 
        if (i2 >= i6)
        {
          localObject1 = new org/a/a/d/c$k;
          ((c.k)localObject1).<init>(0);
          i2 = 0;
          localObject2 = null;
          paramc.a((m)localObject1, null);
        }
        else
        {
          localObject1 = new org/a/a/d/c$k;
          ((c.k)localObject1).<init>(j);
          paramc.a((m)localObject1, (k)localObject1);
        }
        break;
      case 119: 
        paramc.g(i2);
        break;
      case 115: 
        paramc.a(i2);
        break;
      case 109: 
        paramc.b(i2);
        break;
      case 107: 
        localObject1 = d.h();
        paramc.a((d)localObject1, i2, i9);
        break;
      case 104: 
        localObject1 = d.j();
        paramc.a((d)localObject1, i2, i9);
        break;
      case 101: 
        paramc.d(i2);
        break;
      case 100: 
        paramc.e(i2);
        break;
      case 97: 
        localObject1 = d.k();
        paramc.a((d)localObject1);
        break;
      case 90: 
        if (i2 == j)
        {
          localObject1 = "Z";
          paramc.a((String)localObject1, false);
        }
        else if (i2 == i9)
        {
          localObject1 = "Z";
          paramc.a((String)localObject1, j);
        }
        else
        {
          localObject1 = c.j.a;
          paramc.a((m)localObject1, (k)localObject1);
        }
        break;
      case 89: 
      case 120: 
      case 121: 
        Object localObject3;
        Object localObject4;
        if (i2 == i9)
        {
          k = i1 + 1;
          boolean bool1;
          if (k < i)
          {
            k = arrayOfInt[0] + j;
            arrayOfInt[0] = k;
            localObject1 = a(paramString, arrayOfInt);
            bool1 = c((String)localObject1) ^ j;
            i3 = arrayOfInt[0] - j;
            arrayOfInt[0] = i3;
          }
          else
          {
            bool1 = true;
          }
          i3 = 120;
          if (i5 != i3)
          {
            localObject2 = new org/a/a/b;
            ((org.a.a.b)localObject2).<init>();
            i3 = ((org.a.a.b)localObject2).g() + -30;
            localObject3 = new org/a/a/d/c$m;
            localObject4 = d.s();
            ((c.m)localObject3).<init>((d)localObject4, i3, bool1);
            paramc.a(localObject3);
          }
          else
          {
            localObject2 = new org/a/a/b;
            ((org.a.a.b)localObject2).<init>();
            localObject3 = ((org.a.a.a.a)localObject2).b().z();
            long l = ((org.a.a.a.a)localObject2).a();
            i3 = ((org.a.a.c)localObject3).a(l) + -30;
            localObject3 = new org/a/a/d/c$m;
            localObject4 = d.p();
            ((c.m)localObject3).<init>((d)localObject4, i3, bool1);
            paramc.a(localObject3);
          }
        }
        else
        {
          m = 9;
          int i7 = i1 + 1;
          if (i7 < i)
          {
            i7 = arrayOfInt[0] + j;
            arrayOfInt[0] = i7;
            localObject4 = a(paramString, arrayOfInt);
            boolean bool2 = c((String)localObject4);
            if (bool2) {
              m = i3;
            }
            i8 = arrayOfInt[0] - j;
            arrayOfInt[0] = i8;
          }
          i8 = 89;
          if (i5 != i8)
          {
            switch (i5)
            {
            default: 
              break;
            case 121: 
              paramc.c(i3, m);
              break;
            case 120: 
              paramc.b(i3, m);
              break;
            }
          }
          else
          {
            localObject3 = d.t();
            paramc.a((d)localObject3, i3, m);
          }
        }
        break;
      case 83: 
        paramc.a(i3, i3);
        break;
      case 77: 
        m = 3;
        if (i3 >= m)
        {
          if (i3 >= i8)
          {
            localObject1 = d.r();
            paramc.a((d)localObject1);
          }
          else
          {
            localObject1 = d.r();
            paramc.b((d)localObject1);
          }
        }
        else {
          paramc.h(i3);
        }
        break;
      case 75: 
        localObject1 = d.i();
        paramc.a((d)localObject1, i3, i9);
        break;
      case 72: 
        paramc.c(i3);
        break;
      case 71: 
        localObject1 = d.w();
        paramc.a((d)localObject1);
        break;
      case 69: 
        if (i3 >= i8)
        {
          localObject1 = d.l();
          paramc.a((d)localObject1);
        }
        else
        {
          localObject1 = d.l();
          paramc.b((d)localObject1);
        }
        break;
      case 68: 
        paramc.f(i3);
        break;
      case 67: 
        localObject1 = d.v();
        paramc.b((d)localObject1, i3, i3);
        break;
      case 39: 
        localObject1 = ((String)localObject1).substring(j);
        int i4 = ((String)localObject1).length();
        if (i4 == j)
        {
          m = ((String)localObject1).charAt(0);
          paramc.a(m);
        }
        else
        {
          localObject2 = new java/lang/String;
          ((String)localObject2).<init>((String)localObject1);
          paramc.a((String)localObject2);
        }
        break;
      }
      int n = i1 + 1;
    }
  }
  
  public static b b(String paramString)
  {
    int i = paramString.length();
    int j = 2;
    if (i == j)
    {
      localIllegalArgumentException = null;
      i = a(paramString.charAt(0));
      int k = a(paramString.charAt(1));
      j = 4;
      if ((i == j) && (k == j))
      {
        paramString = new java/lang/IllegalArgumentException;
        paramString.<init>("Style '--' is invalid");
        throw paramString;
      }
      j = (i << 2) + i + k;
      Object localObject = b;
      int m = ((AtomicReferenceArray)localObject).length();
      if (j >= m) {
        return a(i, k);
      }
      localObject = (b)b.get(j);
      if (localObject == null)
      {
        localObject = a(i, k);
        paramString = b;
        i = 0;
        localIllegalArgumentException = null;
        boolean bool = paramString.compareAndSet(j, null, localObject);
        if (!bool)
        {
          paramString = b.get(j);
          localObject = paramString;
          localObject = (b)paramString;
        }
      }
      return (b)localObject;
    }
    IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
    paramString = String.valueOf(paramString);
    paramString = "Invalid style specification: ".concat(paramString);
    localIllegalArgumentException.<init>(paramString);
    throw localIllegalArgumentException;
  }
  
  private static boolean c(String paramString)
  {
    int i = paramString.length();
    if (i > 0)
    {
      int j = paramString.charAt(0);
      boolean bool = true;
      switch (j)
      {
      default: 
        break;
      case 77: 
        j = 2;
        if (i <= j) {
          return bool;
        }
        break;
      case 67: 
      case 68: 
      case 70: 
      case 72: 
      case 75: 
      case 83: 
      case 87: 
      case 89: 
      case 99: 
      case 100: 
      case 101: 
      case 104: 
      case 107: 
      case 109: 
      case 115: 
      case 119: 
      case 120: 
      case 121: 
        return bool;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     org.a.a.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */