package org.a.a.d;

import java.io.IOException;
import java.util.Locale;
import org.a.a.a;
import org.a.a.f;
import org.a.a.x;
import org.a.a.z;

public final class b
{
  public final m a;
  public final k b;
  public final Locale c;
  public final boolean d;
  public final a e;
  public final Integer f;
  public final int g;
  private final f h;
  
  b(m paramm, k paramk)
  {
    a = paramm;
    b = paramk;
    c = null;
    d = false;
    e = null;
    h = null;
    f = null;
    g = 2000;
  }
  
  public b(m paramm, k paramk, Locale paramLocale, boolean paramBoolean, a parama, f paramf, Integer paramInteger, int paramInt)
  {
    a = paramm;
    b = paramk;
    c = paramLocale;
    d = paramBoolean;
    e = parama;
    h = paramf;
    f = paramInteger;
    g = paramInt;
  }
  
  private m a()
  {
    Object localObject = a;
    if (localObject != null) {
      return (m)localObject;
    }
    localObject = new java/lang/UnsupportedOperationException;
    ((UnsupportedOperationException)localObject).<init>("Printing not supported");
    throw ((Throwable)localObject);
  }
  
  private void a(Appendable paramAppendable, long paramLong, a parama)
  {
    b localb = this;
    m localm = a();
    a locala = parama;
    locala = b(parama);
    Object localObject1 = locala.a();
    int i = ((f)localObject1).b(paramLong);
    long l1 = i;
    long l2 = paramLong + l1;
    long l3 = paramLong ^ l2;
    long l4 = 0L;
    boolean bool1 = l3 < l4;
    if (bool1)
    {
      l1 ^= paramLong;
      boolean bool2 = l1 < l4;
      if (!bool2)
      {
        localObject1 = f.a;
        i = 0;
        l2 = paramLong;
        localObject2 = localObject1;
        break label108;
      }
    }
    Object localObject2 = localObject1;
    label108:
    localObject1 = locala.b();
    Locale localLocale = c;
    localm.a(paramAppendable, l2, (a)localObject1, i, (f)localObject2, localLocale);
  }
  
  private a b(a parama)
  {
    parama = org.a.a.e.a(parama);
    Object localObject = e;
    if (localObject != null) {
      parama = (a)localObject;
    }
    localObject = h;
    if (localObject != null) {
      parama = parama.a((f)localObject);
    }
    return parama;
  }
  
  private k b()
  {
    Object localObject = b;
    if (localObject != null) {
      return (k)localObject;
    }
    localObject = new java/lang/UnsupportedOperationException;
    ((UnsupportedOperationException)localObject).<init>("Parsing not supported");
    throw ((Throwable)localObject);
  }
  
  public final long a(String paramString)
  {
    k localk = b();
    a locala = e;
    locala = b(locala);
    e locale = new org/a/a/d/e;
    Locale localLocale = c;
    Integer localInteger = f;
    int i = g;
    locale.<init>(locala, localLocale, localInteger, i);
    return locale.a(localk, paramString);
  }
  
  public final String a(long paramLong)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    int i = a().a();
    localStringBuilder.<init>(i);
    i = 0;
    try
    {
      a(localStringBuilder, paramLong, null);
      return localStringBuilder.toString();
    }
    catch (IOException localIOException)
    {
      for (;;) {}
    }
  }
  
  public final String a(x paramx)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    m localm = a();
    int i = localm.a();
    localStringBuilder.<init>(i);
    try
    {
      long l = org.a.a.e.a(paramx);
      paramx = org.a.a.e.b(paramx);
      a(localStringBuilder, l, paramx);
    }
    catch (IOException localIOException)
    {
      for (;;) {}
    }
    return localStringBuilder.toString();
  }
  
  public final String a(z paramz)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    m localm = a();
    int i = localm.a();
    localStringBuilder.<init>(i);
    try
    {
      localm = a();
      Locale localLocale = c;
      localm.a(localStringBuilder, paramz, localLocale);
    }
    catch (IOException localIOException)
    {
      for (;;) {}
    }
    return localStringBuilder.toString();
  }
  
  public final b a(a parama)
  {
    Object localObject = e;
    if (localObject == parama) {
      return this;
    }
    localObject = new org/a/a/d/b;
    m localm = a;
    k localk = b;
    Locale localLocale = c;
    boolean bool = d;
    f localf = h;
    Integer localInteger = f;
    int i = g;
    ((b)localObject).<init>(localm, localk, localLocale, bool, parama, localf, localInteger, i);
    return (b)localObject;
  }
  
  public final b a(f paramf)
  {
    Object localObject = h;
    if (localObject == paramf) {
      return this;
    }
    localObject = new org/a/a/d/b;
    m localm = a;
    k localk = b;
    Locale localLocale = c;
    a locala = e;
    Integer localInteger = f;
    int i = g;
    ((b)localObject).<init>(localm, localk, localLocale, false, locala, paramf, localInteger, i);
    return (b)localObject;
  }
  
  public final void a(StringBuffer paramStringBuffer, long paramLong)
  {
    try
    {
      a(paramStringBuffer, paramLong, null);
      return;
    }
    catch (IOException localIOException) {}
  }
  
  public final org.a.a.b b(String paramString)
  {
    Object localObject1 = b();
    Object localObject2 = b(null);
    e locale = new org/a/a/d/e;
    Locale localLocale = c;
    Integer localInteger = f;
    int i = g;
    locale.<init>((a)localObject2, localLocale, localInteger, i);
    int j = 0;
    localLocale = null;
    int k = ((k)localObject1).a(locale, paramString, 0);
    if (k >= 0)
    {
      j = paramString.length();
      if (k >= j)
      {
        long l = locale.a(paramString);
        boolean bool = d;
        if (bool)
        {
          paramString = d;
          if (paramString != null)
          {
            int m = d.intValue();
            paramString = f.a(m);
            localObject2 = ((a)localObject2).a(paramString);
            break label158;
          }
        }
        paramString = c;
        if (paramString != null)
        {
          paramString = c;
          localObject2 = ((a)localObject2).a(paramString);
        }
        label158:
        paramString = new org/a/a/b;
        paramString.<init>(l, (a)localObject2);
        localObject1 = h;
        if (localObject1 != null) {
          paramString = paramString.a((f)localObject1);
        }
        return paramString;
      }
    }
    else
    {
      k ^= 0xFFFFFFFF;
    }
    localObject2 = new java/lang/IllegalArgumentException;
    paramString = h.a(paramString, k);
    ((IllegalArgumentException)localObject2).<init>(paramString);
    throw ((Throwable)localObject2);
  }
}

/* Location:
 * Qualified Name:     org.a.a.d.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */