package org.a.a.d;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;
import java.util.TreeSet;
import org.a.a.aa;

final class o$g
  implements p, q
{
  volatile q a;
  volatile p b;
  private final String c;
  private final String d;
  private final String[] e;
  private final boolean f;
  private final boolean g;
  private final q h;
  private final p i;
  
  o$g(String paramString1, String paramString2, q paramq, p paramp, boolean paramBoolean)
  {
    c = paramString1;
    d = paramString2;
    boolean bool1 = true;
    if (paramString2 != null)
    {
      bool2 = paramString1.equals(paramString2);
      if (!bool2)
      {
        localTreeSet = new java/util/TreeSet;
        Comparator localComparator = String.CASE_INSENSITIVE_ORDER;
        localTreeSet.<init>(localComparator);
        localTreeSet.add(paramString1);
        localTreeSet.add(paramString2);
        paramString1 = new java/util/ArrayList;
        paramString1.<init>(localTreeSet);
        Collections.reverse(paramString1);
        int j = paramString1.size();
        paramString2 = new String[j];
        paramString1 = (String[])paramString1.toArray(paramString2);
        e = paramString1;
        break label131;
      }
    }
    paramString2 = new String[bool1];
    boolean bool2 = false;
    TreeSet localTreeSet = null;
    paramString2[0] = paramString1;
    e = paramString2;
    label131:
    h = paramq;
    i = paramp;
    f = paramBoolean;
    g = bool1;
  }
  
  public final int a(aa paramaa, int paramInt, Locale paramLocale)
  {
    q localq1 = h;
    int j = localq1.a(paramaa, paramInt, paramLocale);
    if (j < paramInt)
    {
      q localq2 = a;
      int k = localq2.a(paramaa, paramInt, paramLocale);
      j += k;
    }
    return j;
  }
  
  public final int a(aa paramaa, Locale paramLocale)
  {
    q localq1 = h;
    q localq2 = a;
    int j = localq1.a(paramaa, paramLocale);
    int k = localq2.a(paramaa, paramLocale);
    j += k;
    boolean bool1 = f;
    int m = 1;
    int i2;
    if (bool1)
    {
      int n = localq1.a(paramaa, m, paramLocale);
      if (n > 0)
      {
        boolean bool2 = g;
        if (bool2)
        {
          int i1 = 2;
          i2 = localq2.a(paramaa, i1, paramLocale);
          if (i2 > 0)
          {
            if (i2 > m) {
              paramaa = c;
            } else {
              paramaa = d;
            }
            i2 = paramaa.length();
            j += i2;
          }
        }
        else
        {
          paramaa = c;
          i2 = paramaa.length();
          j += i2;
        }
      }
    }
    else
    {
      boolean bool3 = g;
      if (bool3)
      {
        i2 = localq2.a(paramaa, m, paramLocale);
        if (i2 > 0)
        {
          paramaa = c;
          i2 = paramaa.length();
          j += i2;
        }
      }
    }
    return j;
  }
  
  final g a(q paramq, p paramp)
  {
    a = paramq;
    b = paramp;
    return this;
  }
  
  public final void a(StringBuffer paramStringBuffer, aa paramaa, Locale paramLocale)
  {
    Object localObject = h;
    q localq = a;
    ((q)localObject).a(paramStringBuffer, paramaa, paramLocale);
    boolean bool1 = f;
    int j = 1;
    if (bool1)
    {
      int k = ((q)localObject).a(paramaa, j, paramLocale);
      if (k > 0)
      {
        boolean bool2 = g;
        if (bool2)
        {
          int m = localq.a(paramaa, 2, paramLocale);
          if (m > 0)
          {
            if (m > j) {
              localObject = c;
            } else {
              localObject = d;
            }
            paramStringBuffer.append((String)localObject);
          }
        }
        else
        {
          localObject = c;
          paramStringBuffer.append((String)localObject);
        }
      }
    }
    else
    {
      boolean bool3 = g;
      if (bool3)
      {
        int n = localq.a(paramaa, j, paramLocale);
        if (n > 0)
        {
          localObject = c;
          paramStringBuffer.append((String)localObject);
        }
      }
    }
    localq.a(paramStringBuffer, paramaa, paramLocale);
  }
}

/* Location:
 * Qualified Name:     org.a.a.d.o.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */