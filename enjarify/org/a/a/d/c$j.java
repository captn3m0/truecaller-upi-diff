package org.a.a.d;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.a.a.a;
import org.a.a.f;
import org.a.a.z;

 enum c$j
  implements k, m
{
  static final int b;
  static final int c;
  private static final List d;
  private static final Map e;
  private static final List f;
  
  static
  {
    Object localObject1 = new org/a/a/d/c$j;
    ((j)localObject1).<init>("INSTANCE");
    a = (j)localObject1;
    int i = 1;
    localObject1 = new j[i];
    Object localObject2 = a;
    localObject1[0] = localObject2;
    g = (j[])localObject1;
    localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    f = (List)localObject1;
    localObject1 = new java/util/ArrayList;
    localObject2 = f.b();
    ((ArrayList)localObject1).<init>((Collection)localObject2);
    d = (List)localObject1;
    Collections.sort((List)localObject1);
    localObject1 = new java/util/HashMap;
    ((HashMap)localObject1).<init>();
    e = (Map)localObject1;
    localObject1 = d.iterator();
    int j = 0;
    localObject2 = null;
    int k = 0;
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      String str = (String)((Iterator)localObject1).next();
      int n = str.indexOf('/');
      Object localObject4;
      if (n >= 0)
      {
        int i1 = str.length();
        if (n < i1) {
          n += 1;
        }
        k = Math.max(k, n);
        i1 = n + 1;
        Object localObject3 = str.substring(0, i1);
        localObject4 = str.substring(n);
        Map localMap = e;
        boolean bool2 = localMap.containsKey(localObject3);
        if (!bool2)
        {
          localMap = e;
          ArrayList localArrayList = new java/util/ArrayList;
          localArrayList.<init>();
          localMap.put(localObject3, localArrayList);
        }
        localMap = e;
        localObject3 = (List)localMap.get(localObject3);
        ((List)localObject3).add(localObject4);
      }
      else
      {
        localObject4 = f;
        ((List)localObject4).add(str);
      }
      int m = str.length();
      j = Math.max(j, m);
    }
    b = j;
    c = k;
  }
  
  private c$j() {}
  
  public final int a()
  {
    return b;
  }
  
  public final int a(e parame, CharSequence paramCharSequence, int paramInt)
  {
    Object localObject1 = f;
    int i = paramCharSequence.length();
    int j = c + paramInt;
    j = Math.min(i, j);
    String str1 = "";
    int k = paramInt;
    int m;
    while (k < j)
    {
      m = paramCharSequence.charAt(k);
      int n = 47;
      if (m == n)
      {
        int i2 = k + 1;
        CharSequence localCharSequence = paramCharSequence.subSequence(paramInt, i2);
        str1 = localCharSequence.toString();
        j = str1.length() + paramInt;
        if (k < i)
        {
          localObject2 = new java/lang/StringBuilder;
          ((StringBuilder)localObject2).<init>();
          ((StringBuilder)localObject2).append(str1);
          i2 = paramCharSequence.charAt(i2);
          ((StringBuilder)localObject2).append(i2);
          localObject1 = ((StringBuilder)localObject2).toString();
        }
        else
        {
          localObject1 = str1;
        }
        localObject2 = e;
        localObject1 = (List)((Map)localObject2).get(localObject1);
        if (localObject1 != null) {
          break label194;
        }
        return paramInt ^ 0xFFFFFFFF;
      }
      k += 1;
    }
    j = paramInt;
    label194:
    i = 0;
    Object localObject2 = null;
    k = 0;
    for (;;)
    {
      m = ((List)localObject1).size();
      if (k >= m) {
        break;
      }
      String str2 = (String)((List)localObject1).get(k);
      boolean bool = c.a(paramCharSequence, j, str2);
      if (bool) {
        if (localObject2 != null)
        {
          int i1 = str2.length();
          int i3 = ((String)localObject2).length();
          if (i1 <= i3) {}
        }
        else
        {
          localObject2 = str2;
        }
      }
      k += 1;
    }
    if (localObject2 != null)
    {
      paramCharSequence = new java/lang/StringBuilder;
      paramCharSequence.<init>();
      paramCharSequence.append(str1);
      paramCharSequence.append((String)localObject2);
      paramCharSequence = f.a(paramCharSequence.toString());
      parame.a(paramCharSequence);
      int i4 = ((String)localObject2).length();
      return j + i4;
    }
    return paramInt ^ 0xFFFFFFFF;
  }
  
  public final void a(Appendable paramAppendable, long paramLong, a parama, int paramInt, f paramf, Locale paramLocale)
  {
    String str;
    if (paramf != null) {
      str = b;
    } else {
      str = "";
    }
    paramAppendable.append(str);
  }
  
  public final void a(Appendable paramAppendable, z paramz, Locale paramLocale) {}
  
  public final int b()
  {
    return b;
  }
}

/* Location:
 * Qualified Name:     org.a.a.d.c.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */