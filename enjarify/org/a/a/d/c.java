package org.a.a.d;

import java.util.ArrayList;
import java.util.List;

public final class c
{
  private ArrayList a;
  private Object b;
  
  public c()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    a = localArrayList;
  }
  
  static void a(Appendable paramAppendable, int paramInt)
  {
    for (;;)
    {
      paramInt += -1;
      if (paramInt < 0) {
        break;
      }
      char c = (char)-3;
      paramAppendable.append(c);
    }
  }
  
  static boolean a(CharSequence paramCharSequence, int paramInt, String paramString)
  {
    int i = paramString.length();
    int j = paramCharSequence.length() - paramInt;
    if (j < i) {
      return false;
    }
    j = 0;
    while (j < i)
    {
      int k = paramInt + j;
      k = paramCharSequence.charAt(k);
      int m = paramString.charAt(j);
      if (k != m) {
        return false;
      }
      j += 1;
    }
    return true;
  }
  
  static boolean b(CharSequence paramCharSequence, int paramInt, String paramString)
  {
    int i = paramString.length();
    int j = paramCharSequence.length() - paramInt;
    if (j < i) {
      return false;
    }
    j = 0;
    while (j < i)
    {
      int k = paramInt + j;
      k = paramCharSequence.charAt(k);
      int m = paramString.charAt(j);
      if (k != m)
      {
        char c1 = Character.toUpperCase(k);
        char c2 = Character.toUpperCase(m);
        if (c1 != c2)
        {
          c1 = Character.toLowerCase(c1);
          c2 = Character.toLowerCase(c2);
          if (c1 != c2) {
            return false;
          }
        }
      }
      j += 1;
    }
    return true;
  }
  
  private static boolean b(Object paramObject)
  {
    boolean bool = paramObject instanceof m;
    if (bool)
    {
      bool = paramObject instanceof c.b;
      if (bool) {
        return ((c.b)paramObject).c();
      }
      return true;
    }
    return false;
  }
  
  private c c(org.a.a.d paramd, int paramInt1, int paramInt2)
  {
    if (paramd != null)
    {
      if (paramInt2 < paramInt1) {
        paramInt2 = paramInt1;
      }
      if ((paramInt1 >= 0) && (paramInt2 > 0))
      {
        c.d locald = new org/a/a/d/c$d;
        locald.<init>(paramd, paramInt1, paramInt2);
        return a(locald);
      }
      paramd = new java/lang/IllegalArgumentException;
      paramd.<init>();
      throw paramd;
    }
    paramd = new java/lang/IllegalArgumentException;
    paramd.<init>("Field type must not be null");
    throw paramd;
  }
  
  private static void c(d paramd)
  {
    if (paramd != null) {
      return;
    }
    paramd = new java/lang/IllegalArgumentException;
    paramd.<init>("No parser supplied");
    throw paramd;
  }
  
  private static boolean c(Object paramObject)
  {
    boolean bool = paramObject instanceof k;
    if (bool)
    {
      bool = paramObject instanceof c.b;
      if (bool) {
        return ((c.b)paramObject).d();
      }
      return true;
    }
    return false;
  }
  
  private Object e()
  {
    Object localObject1 = b;
    if (localObject1 == null)
    {
      Object localObject2 = a;
      int i = ((ArrayList)localObject2).size();
      int j = 2;
      if (i == j)
      {
        localObject2 = a;
        j = 0;
        localObject2 = ((ArrayList)localObject2).get(0);
        Object localObject3 = a;
        int k = 1;
        localObject3 = ((ArrayList)localObject3).get(k);
        if (localObject2 != null)
        {
          if ((localObject2 == localObject3) || (localObject3 == null)) {
            localObject1 = localObject2;
          }
        }
        else {
          localObject1 = localObject3;
        }
      }
      if (localObject1 == null)
      {
        localObject1 = new org/a/a/d/c$b;
        localObject2 = a;
        ((c.b)localObject1).<init>((List)localObject2);
      }
      b = localObject1;
    }
    return localObject1;
  }
  
  public final b a()
  {
    Object localObject1 = e();
    boolean bool1 = b(localObject1);
    Object localObject2 = null;
    Object localObject3;
    if (bool1)
    {
      localObject3 = localObject1;
      localObject3 = (m)localObject1;
    }
    else
    {
      bool1 = false;
      localObject3 = null;
    }
    boolean bool2 = c(localObject1);
    if (bool2)
    {
      localObject2 = localObject1;
      localObject2 = (k)localObject1;
    }
    if ((localObject3 == null) && (localObject2 == null))
    {
      localObject1 = new java/lang/UnsupportedOperationException;
      ((UnsupportedOperationException)localObject1).<init>("Both printing and parsing not supported");
      throw ((Throwable)localObject1);
    }
    localObject1 = new org/a/a/d/b;
    ((b)localObject1).<init>((m)localObject3, (k)localObject2);
    return (b)localObject1;
  }
  
  public final c a(char paramChar)
  {
    c.a locala = new org/a/a/d/c$a;
    locala.<init>(paramChar);
    return a(locala);
  }
  
  public final c a(int paramInt)
  {
    org.a.a.d locald = org.a.a.d.c();
    return a(locald, paramInt, 2);
  }
  
  public final c a(int paramInt1, int paramInt2)
  {
    org.a.a.d locald = org.a.a.d.d();
    return c(locald, paramInt1, paramInt2);
  }
  
  final c a(Object paramObject)
  {
    b = null;
    a.add(paramObject);
    a.add(paramObject);
    return this;
  }
  
  public final c a(String paramString)
  {
    int i = paramString.length();
    Object localObject;
    switch (i)
    {
    default: 
      localObject = new org/a/a/d/c$h;
      ((c.h)localObject).<init>(paramString);
      return a(localObject);
    case 1: 
      localObject = new org/a/a/d/c$a;
      char c = paramString.charAt(0);
      ((c.a)localObject).<init>(c);
      return a(localObject);
    }
    return this;
  }
  
  public final c a(String paramString, boolean paramBoolean)
  {
    c.l locall = new org/a/a/d/c$l;
    locall.<init>(null, paramString, paramBoolean, 2);
    return a(locall);
  }
  
  public final c a(String paramString, boolean paramBoolean, int paramInt)
  {
    c.l locall = new org/a/a/d/c$l;
    locall.<init>(paramString, paramString, paramBoolean, paramInt);
    return a(locall);
  }
  
  public final c a(b paramb)
  {
    if (paramb != null)
    {
      m localm = a;
      paramb = b;
      return a(localm, paramb);
    }
    paramb = new java/lang/IllegalArgumentException;
    paramb.<init>("No formatter supplied");
    throw paramb;
  }
  
  public final c a(d paramd)
  {
    c(paramd);
    paramd = f.a(paramd);
    return a(null, paramd);
  }
  
  final c a(m paramm, k paramk)
  {
    b = null;
    a.add(paramm);
    a.add(paramk);
    return this;
  }
  
  public final c a(org.a.a.d paramd)
  {
    if (paramd != null)
    {
      c.i locali = new org/a/a/d/c$i;
      locali.<init>(paramd, false);
      return a(locali);
    }
    paramd = new java/lang/IllegalArgumentException;
    paramd.<init>("Field type must not be null");
    throw paramd;
  }
  
  public final c a(org.a.a.d paramd, int paramInt)
  {
    if (paramd != null)
    {
      if (paramInt > 0)
      {
        c.c localc = new org/a/a/d/c$c;
        localc.<init>(paramd, paramInt);
        return a(localc);
      }
      paramd = new java/lang/IllegalArgumentException;
      String str = String.valueOf(paramInt);
      str = "Illegal number of digits: ".concat(str);
      paramd.<init>(str);
      throw paramd;
    }
    paramd = new java/lang/IllegalArgumentException;
    paramd.<init>("Field type must not be null");
    throw paramd;
  }
  
  public final c a(org.a.a.d paramd, int paramInt1, int paramInt2)
  {
    if (paramd != null)
    {
      if (paramInt2 < paramInt1) {
        paramInt2 = paramInt1;
      }
      if ((paramInt1 >= 0) && (paramInt2 > 0))
      {
        int i = 1;
        if (paramInt1 <= i)
        {
          c.n localn = new org/a/a/d/c$n;
          localn.<init>(paramd, paramInt2, false);
          return a(localn);
        }
        c.g localg = new org/a/a/d/c$g;
        localg.<init>(paramd, paramInt2, false, paramInt1);
        return a(localg);
      }
      paramd = new java/lang/IllegalArgumentException;
      paramd.<init>();
      throw paramd;
    }
    paramd = new java/lang/IllegalArgumentException;
    paramd.<init>("Field type must not be null");
    throw paramd;
  }
  
  public final c a(d[] paramArrayOfd)
  {
    int i = paramArrayOfd.length;
    int j = 0;
    int k = 1;
    if (i == k)
    {
      d locald = paramArrayOfd[0];
      if (locald != null)
      {
        paramArrayOfd = f.a(paramArrayOfd[0]);
        return a(null, paramArrayOfd);
      }
      paramArrayOfd = new java/lang/IllegalArgumentException;
      paramArrayOfd.<init>("No parser supplied");
      throw paramArrayOfd;
    }
    k[] arrayOfk = new k[i];
    for (;;)
    {
      int m = i + -1;
      if (j >= m) {
        break label105;
      }
      k localk = f.a(paramArrayOfd[j]);
      arrayOfk[j] = localk;
      if (localk == null) {
        break;
      }
      j += 1;
    }
    paramArrayOfd = new java/lang/IllegalArgumentException;
    paramArrayOfd.<init>("Incomplete parser array");
    throw paramArrayOfd;
    label105:
    paramArrayOfd = f.a(paramArrayOfd[j]);
    arrayOfk[j] = paramArrayOfd;
    paramArrayOfd = new org/a/a/d/c$e;
    paramArrayOfd.<init>(arrayOfk);
    return a(null, paramArrayOfd);
  }
  
  public final c b(int paramInt)
  {
    org.a.a.d locald = org.a.a.d.e();
    return a(locald, paramInt, 2);
  }
  
  public final c b(int paramInt1, int paramInt2)
  {
    org.a.a.d locald = org.a.a.d.p();
    return b(locald, paramInt1, paramInt2);
  }
  
  public final c b(d paramd)
  {
    c(paramd);
    k[] arrayOfk = new k[2];
    paramd = f.a(paramd);
    arrayOfk[0] = paramd;
    arrayOfk[1] = null;
    c.e locale = new org/a/a/d/c$e;
    locale.<init>(arrayOfk);
    return a(null, locale);
  }
  
  public final c b(org.a.a.d paramd)
  {
    if (paramd != null)
    {
      c.i locali = new org/a/a/d/c$i;
      locali.<init>(paramd, true);
      return a(locali);
    }
    paramd = new java/lang/IllegalArgumentException;
    paramd.<init>("Field type must not be null");
    throw paramd;
  }
  
  public final c b(org.a.a.d paramd, int paramInt1, int paramInt2)
  {
    if (paramd != null)
    {
      if (paramInt2 < paramInt1) {
        paramInt2 = paramInt1;
      }
      if ((paramInt1 >= 0) && (paramInt2 > 0))
      {
        int i = 1;
        if (paramInt1 <= i)
        {
          c.n localn = new org/a/a/d/c$n;
          localn.<init>(paramd, paramInt2, i);
          return a(localn);
        }
        c.g localg = new org/a/a/d/c$g;
        localg.<init>(paramd, paramInt2, i, paramInt1);
        return a(localg);
      }
      paramd = new java/lang/IllegalArgumentException;
      paramd.<init>();
      throw paramd;
    }
    paramd = new java/lang/IllegalArgumentException;
    paramd.<init>("Field type must not be null");
    throw paramd;
  }
  
  public final d b()
  {
    Object localObject = e();
    boolean bool = c(localObject);
    if (bool) {
      return l.a((k)localObject);
    }
    localObject = new java/lang/UnsupportedOperationException;
    ((UnsupportedOperationException)localObject).<init>("Parsing is not supported");
    throw ((Throwable)localObject);
  }
  
  public final c c()
  {
    org.a.a.d locald = org.a.a.d.f();
    return c(locald, 1, 9);
  }
  
  public final c c(int paramInt)
  {
    org.a.a.d locald = org.a.a.d.g();
    return a(locald, paramInt, 2);
  }
  
  public final c c(int paramInt1, int paramInt2)
  {
    org.a.a.d locald = org.a.a.d.s();
    return b(locald, paramInt1, paramInt2);
  }
  
  public final c d()
  {
    org.a.a.d locald = org.a.a.d.g();
    return c(locald, 1, 9);
  }
  
  public final c d(int paramInt)
  {
    org.a.a.d locald = org.a.a.d.l();
    return a(locald, paramInt, 1);
  }
  
  public final c e(int paramInt)
  {
    org.a.a.d locald = org.a.a.d.m();
    return a(locald, paramInt, 2);
  }
  
  public final c f(int paramInt)
  {
    org.a.a.d locald = org.a.a.d.n();
    return a(locald, paramInt, 3);
  }
  
  public final c g(int paramInt)
  {
    org.a.a.d locald = org.a.a.d.o();
    return a(locald, paramInt, 2);
  }
  
  public final c h(int paramInt)
  {
    org.a.a.d locald = org.a.a.d.r();
    return a(locald, paramInt, 2);
  }
}

/* Location:
 * Qualified Name:     org.a.a.d.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */