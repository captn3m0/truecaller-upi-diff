package org.a.a.d;

import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import org.a.a.a;
import org.a.a.f;
import org.a.a.z;

final class c$k
  implements k, m
{
  private final Map a;
  private final int b;
  
  c$k(int paramInt)
  {
    b = paramInt;
    a = null;
  }
  
  public final int a()
  {
    int i = b;
    int j = 1;
    if (i == j) {
      return 4;
    }
    return 20;
  }
  
  public final int a(e parame, CharSequence paramCharSequence, int paramInt)
  {
    Map localMap = a;
    if (localMap == null) {
      localMap = org.a.a.e.b();
    }
    Object localObject = null;
    Iterator localIterator = localMap.keySet().iterator();
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      String str = (String)localIterator.next();
      boolean bool2 = c.a(paramCharSequence, paramInt, str);
      if (bool2) {
        if (localObject != null)
        {
          int i = str.length();
          int j = ((String)localObject).length();
          if (i <= j) {}
        }
        else
        {
          localObject = str;
        }
      }
    }
    if (localObject != null)
    {
      paramCharSequence = (f)localMap.get(localObject);
      parame.a(paramCharSequence);
      int k = ((String)localObject).length();
      return paramInt + k;
    }
    return paramInt ^ 0xFFFFFFFF;
  }
  
  public final void a(Appendable paramAppendable, long paramLong, a parama, int paramInt, f paramf, Locale paramLocale)
  {
    long l = paramInt;
    paramLong -= l;
    if (paramf != null)
    {
      int i = b;
      paramInt = 0;
      String str1 = null;
      Object localObject2;
      boolean bool1;
      String str2;
      boolean bool2;
      int j;
      switch (i)
      {
      default: 
        break;
      case 1: 
        if (paramLocale == null) {
          paramLocale = Locale.getDefault();
        }
        parama = paramf.a(paramLong);
        if (parama == null)
        {
          localObject1 = b;
        }
        else
        {
          localObject2 = f.c();
          bool1 = localObject2 instanceof org.a.a.e.c;
          if (bool1)
          {
            localObject2 = (org.a.a.e.c)localObject2;
            str2 = b;
            bool2 = paramf.d(paramLong);
            parama = ((org.a.a.e.c)localObject2).a(paramLocale, str2, parama, bool2);
            if (parama != null)
            {
              paramInt = 0;
              str1 = parama[0];
            }
            parama = str1;
          }
          else
          {
            str1 = b;
            parama = ((org.a.a.e.e)localObject2).a(paramLocale, str1, parama);
          }
          if (parama != null)
          {
            localObject1 = parama;
          }
          else
          {
            j = paramf.b(paramLong);
            localObject1 = f.b(j);
          }
        }
        break;
      case 0: 
        if (paramLocale == null) {
          paramLocale = Locale.getDefault();
        }
        parama = paramf.a(paramLong);
        if (parama == null)
        {
          localObject1 = b;
        }
        else
        {
          localObject2 = f.c();
          bool1 = localObject2 instanceof org.a.a.e.c;
          if (bool1)
          {
            localObject2 = (org.a.a.e.c)localObject2;
            str2 = b;
            bool2 = paramf.d(paramLong);
            parama = ((org.a.a.e.c)localObject2).a(paramLocale, str2, parama, bool2);
            if (parama != null)
            {
              paramInt = 1;
              str1 = parama[paramInt];
            }
            parama = str1;
          }
          else
          {
            str1 = b;
            parama = ((org.a.a.e.e)localObject2).b(paramLocale, str1, parama);
          }
          if (parama != null)
          {
            localObject1 = parama;
          }
          else
          {
            j = paramf.b(paramLong);
            localObject1 = f.b(j);
          }
        }
        break;
      }
    }
    Object localObject1 = "";
    paramAppendable.append((CharSequence)localObject1);
  }
  
  public final void a(Appendable paramAppendable, z paramz, Locale paramLocale) {}
  
  public final int b()
  {
    int i = b;
    int j = 1;
    if (i == j) {
      return 4;
    }
    return 20;
  }
}

/* Location:
 * Qualified Name:     org.a.a.d.c.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */