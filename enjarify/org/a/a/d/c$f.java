package org.a.a.d;

import org.a.a.d;

abstract class c$f
  implements k, m
{
  protected final d a;
  protected final int b;
  protected final boolean c;
  
  c$f(d paramd, int paramInt, boolean paramBoolean)
  {
    a = paramd;
    b = paramInt;
    c = paramBoolean;
  }
  
  public int a(e parame, CharSequence paramCharSequence, int paramInt)
  {
    f localf = this;
    Object localObject = paramCharSequence;
    int i = paramInt;
    int j = b;
    int k = paramCharSequence.length() - paramInt;
    j = Math.min(j, k);
    k = 0;
    int m = j;
    j = 0;
    d locald = null;
    int n = 0;
    float f1 = 0.0F;
    int i1 = 0;
    float f2 = 0.0F;
    int i2;
    for (;;)
    {
      i2 = 48;
      if (j >= m) {
        break;
      }
      int i3 = i + j;
      int i4 = ((CharSequence)localObject).charAt(i3);
      int i5 = 57;
      if (j == 0)
      {
        int i6 = 43;
        int i7 = 45;
        if ((i4 == i7) || (i4 == i6))
        {
          boolean bool = c;
          if (bool)
          {
            n = 1;
            f1 = Float.MIN_VALUE;
            if (i4 == i7)
            {
              i1 = 1;
              f2 = Float.MIN_VALUE;
            }
            else
            {
              i1 = 0;
              f2 = 0.0F;
            }
            if (i4 != i6)
            {
              n = 0;
              f1 = 0.0F;
            }
            i4 = j + 1;
            if (i4 < m)
            {
              i3 += 1;
              i3 = ((CharSequence)localObject).charAt(i3);
              if ((i3 >= i2) && (i3 <= i5))
              {
                m += 1;
                j = paramCharSequence.length() - i;
                m = Math.min(m, j);
                j = i4;
                i8 = i1;
                f3 = f2;
                i1 = n;
                f2 = f1;
                n = i8;
                f1 = f3;
                continue;
              }
            }
            int i8 = i1;
            float f3 = f2;
            i1 = n;
            f2 = f1;
            n = i8;
            f1 = f3;
            break;
          }
        }
      }
      if ((i4 < i2) || (i4 > i5)) {
        break;
      }
      j += 1;
    }
    if (j == 0) {
      return i ^ 0xFFFFFFFF;
    }
    k = 9;
    int i9;
    if (j >= k)
    {
      if (i1 != 0)
      {
        k = i + 1;
        i += j;
        localObject = ((CharSequence)localObject).subSequence(k, i).toString();
        i9 = Integer.parseInt((String)localObject);
      }
      else
      {
        j += i;
        localObject = ((CharSequence)localObject).subSequence(i, j).toString();
        i9 = Integer.parseInt((String)localObject);
        i = j;
      }
    }
    else
    {
      if ((n == 0) && (i1 == 0)) {
        k = i;
      } else {
        k = i + 1;
      }
      m = k + 1;
    }
    try
    {
      k = ((CharSequence)localObject).charAt(k) - i2;
      i += j;
      while (m < i)
      {
        j = k << 3;
        k <<= 1;
        j += k;
        k = m + 1;
        m = ((CharSequence)localObject).charAt(m);
        j = j + m - i2;
        m = k;
        k = j;
      }
      if (n != 0) {
        i9 = -k;
      } else {
        i9 = k;
      }
      locald = a;
      parame.a(locald, i9);
      return i;
    }
    catch (StringIndexOutOfBoundsException localStringIndexOutOfBoundsException) {}
    return i ^ 0xFFFFFFFF;
  }
  
  public final int b()
  {
    return b;
  }
}

/* Location:
 * Qualified Name:     org.a.a.d.c.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */