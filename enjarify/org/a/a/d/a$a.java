package org.a.a.d;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.concurrent.ConcurrentHashMap;
import org.a.a.f;
import org.a.a.z;

final class a$a
  implements k, m
{
  private static final ConcurrentHashMap a;
  private final int b;
  private final int c;
  private final int d;
  
  static
  {
    ConcurrentHashMap localConcurrentHashMap = new java/util/concurrent/ConcurrentHashMap;
    localConcurrentHashMap.<init>();
    a = localConcurrentHashMap;
  }
  
  a$a(int paramInt1, int paramInt2, int paramInt3)
  {
    b = paramInt1;
    c = paramInt2;
    d = paramInt3;
  }
  
  private b a(Locale paramLocale)
  {
    if (paramLocale == null) {
      paramLocale = Locale.getDefault();
    }
    a.b localb = new org/a/a/d/a$b;
    int i = d;
    int j = b;
    int k = c;
    localb.<init>(i, j, k, paramLocale);
    Object localObject = (b)a.get(localb);
    if (localObject == null)
    {
      localObject = a.a(b(paramLocale));
      paramLocale = (b)a.putIfAbsent(localb, localObject);
      if (paramLocale != null) {
        localObject = paramLocale;
      }
    }
    return (b)localObject;
  }
  
  private String b(Locale paramLocale)
  {
    int i = d;
    switch (i)
    {
    default: 
      i = 0;
      localObject = null;
      break;
    case 2: 
      i = b;
      int j = c;
      localObject = DateFormat.getDateTimeInstance(i, j, paramLocale);
      break;
    case 1: 
      i = c;
      localObject = DateFormat.getTimeInstance(i, paramLocale);
      break;
    case 0: 
      i = b;
      localObject = DateFormat.getDateInstance(i, paramLocale);
    }
    boolean bool = localObject instanceof SimpleDateFormat;
    if (bool) {
      return ((SimpleDateFormat)localObject).toPattern();
    }
    Object localObject = new java/lang/IllegalArgumentException;
    paramLocale = String.valueOf(paramLocale);
    paramLocale = "No datetime pattern for locale: ".concat(paramLocale);
    ((IllegalArgumentException)localObject).<init>(paramLocale);
    throw ((Throwable)localObject);
  }
  
  public final int a()
  {
    return 40;
  }
  
  public final int a(e parame, CharSequence paramCharSequence, int paramInt)
  {
    Locale localLocale = b;
    return ab.a(parame, paramCharSequence, paramInt);
  }
  
  public final void a(Appendable paramAppendable, long paramLong, org.a.a.a parama, int paramInt, f paramf, Locale paramLocale)
  {
    aa.a(paramAppendable, paramLong, parama, paramInt, paramf, paramLocale);
  }
  
  public final void a(Appendable paramAppendable, z paramz, Locale paramLocale)
  {
    aa.a(paramAppendable, paramz, paramLocale);
  }
  
  public final int b()
  {
    return 40;
  }
}

/* Location:
 * Qualified Name:     org.a.a.d.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */