package org.a.a.d;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public final class o
{
  private static final ConcurrentMap c;
  public int a = 1;
  public int b = 2;
  private int d;
  private boolean e;
  private o.f f;
  private List g;
  private boolean h;
  private boolean i;
  private o.c[] j;
  
  static
  {
    ConcurrentHashMap localConcurrentHashMap = new java/util/concurrent/ConcurrentHashMap;
    localConcurrentHashMap.<init>();
    c = localConcurrentHashMap;
  }
  
  public o()
  {
    int k = 10;
    d = k;
    e = false;
    f = null;
    Object localObject = g;
    if (localObject == null)
    {
      localObject = new java/util/ArrayList;
      ((ArrayList)localObject).<init>();
      g = ((List)localObject);
    }
    else
    {
      ((List)localObject).clear();
    }
    h = false;
    i = false;
    o.c[] arrayOfc = new o.c[k];
    j = arrayOfc;
  }
  
  private static n a(List paramList, boolean paramBoolean1, boolean paramBoolean2)
  {
    if ((paramBoolean1) && (paramBoolean2))
    {
      paramList = new java/lang/IllegalStateException;
      paramList.<init>("Builder has created neither a printer nor a parser");
      throw paramList;
    }
    int k = paramList.size();
    int m = 2;
    if (k >= m)
    {
      Object localObject1 = paramList.get(0);
      boolean bool = localObject1 instanceof o.g;
      if (bool)
      {
        localObject1 = (o.g)paramList.get(0);
        Object localObject2 = b;
        if (localObject2 == null)
        {
          localObject2 = a;
          if (localObject2 == null)
          {
            paramList = a(paramList.subList(m, k), paramBoolean1, paramBoolean2);
            localObject3 = a;
            paramList = b;
            paramList = ((o.g)localObject1).a((q)localObject3, paramList);
            localObject3 = new org/a/a/d/n;
            ((n)localObject3).<init>(paramList, paramList);
            return (n)localObject3;
          }
        }
      }
    }
    paramList = a(paramList);
    k = 1;
    m = 0;
    if (paramBoolean1)
    {
      localObject3 = new org/a/a/d/n;
      paramList = (p)paramList[k];
      ((n)localObject3).<init>(null, paramList);
      return (n)localObject3;
    }
    if (paramBoolean2)
    {
      localObject3 = new org/a/a/d/n;
      paramList = (q)paramList[0];
      ((n)localObject3).<init>(paramList, null);
      return (n)localObject3;
    }
    Object localObject3 = new org/a/a/d/n;
    q localq = (q)paramList[0];
    paramList = (p)paramList[k];
    ((n)localObject3).<init>(localq, paramList);
    return (n)localObject3;
  }
  
  private o a(o.f paramf)
  {
    Object localObject1 = g;
    int k = ((List)localObject1).size();
    int m = 0;
    Object localObject2 = null;
    if (k > 0)
    {
      localObject1 = g;
      m = ((List)localObject1).size() + -2;
      localObject2 = ((List)localObject1).get(m);
      localObject1 = g;
      int n = ((List)localObject1).size() + -1;
      localObject1 = ((List)localObject1).get(n);
    }
    else
    {
      k = 0;
      localObject1 = null;
    }
    if ((localObject2 != null) && (localObject1 != null) && (localObject2 == localObject1))
    {
      boolean bool = localObject2 instanceof o.c;
      if (bool)
      {
        b();
        localObject1 = new org/a/a/d/o$c;
        localObject2 = (o.c)localObject2;
        ((o.c)localObject1).<init>((o.c)localObject2, paramf);
        paramf = g;
        m = paramf.size() + -2;
        paramf.set(m, localObject1);
        paramf = g;
        m = paramf.size() + -1;
        paramf.set(m, localObject1);
        paramf = j;
        m = a;
        paramf[m] = localObject1;
        return this;
      }
    }
    paramf = new java/lang/IllegalStateException;
    paramf.<init>("No field to apply suffix to");
    throw paramf;
  }
  
  private o a(q paramq, p paramp)
  {
    g.add(paramq);
    g.add(paramp);
    boolean bool = h | false;
    h = bool;
    bool = i | false;
    i = bool;
    return this;
  }
  
  private void a(int paramInt1, int paramInt2)
  {
    o.c localc = new org/a/a/d/o$c;
    int k = b;
    int m = d;
    boolean bool = e;
    o.c[] arrayOfc = j;
    o.f localf = f;
    localc.<init>(paramInt2, k, m, bool, paramInt1, arrayOfc, localf);
    a(localc, localc);
    j[paramInt1] = localc;
    f = null;
  }
  
  private static Object[] a(List paramList)
  {
    int k = paramList.size();
    int m = 2;
    int n = 1;
    switch (k)
    {
    default: 
      localObject1 = new org/a/a/d/o$a;
      ((o.a)localObject1).<init>(paramList);
      paramList = new Object[m];
      paramList[0] = localObject1;
      paramList[n] = localObject1;
      return paramList;
    case 1: 
      localObject1 = new Object[m];
      Object localObject2 = paramList.get(0);
      localObject1[0] = localObject2;
      paramList = paramList.get(n);
      localObject1[n] = paramList;
      return (Object[])localObject1;
    }
    paramList = new Object[m];
    Object localObject1 = o.e.a;
    paramList[0] = localObject1;
    localObject1 = o.e.a;
    paramList[n] = localObject1;
    return paramList;
  }
  
  private void b()
  {
    Object localObject = f;
    if (localObject == null)
    {
      f = null;
      return;
    }
    localObject = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject).<init>("Prefix not followed by field");
    throw ((Throwable)localObject);
  }
  
  public final n a()
  {
    Object localObject1 = g;
    boolean bool1 = h;
    boolean bool2 = i;
    localObject1 = a((List)localObject1, bool1, bool2);
    o.c[] arrayOfc = j;
    int k = arrayOfc.length;
    int m = 0;
    while (m < k)
    {
      Object localObject2 = arrayOfc[m];
      if (localObject2 != null)
      {
        Object localObject3 = j;
        HashSet localHashSet1 = new java/util/HashSet;
        localHashSet1.<init>();
        HashSet localHashSet2 = new java/util/HashSet;
        localHashSet2.<init>();
        int n = localObject3.length;
        int i1 = 0;
        while (i1 < n)
        {
          Object localObject4 = localObject3[i1];
          if (localObject4 != null)
          {
            boolean bool3 = localObject2.equals(localObject4);
            if (!bool3)
            {
              o.f localf = b;
              localHashSet1.add(localf);
              localObject4 = c;
              localHashSet2.add(localObject4);
            }
          }
          i1 += 1;
        }
        localObject3 = b;
        if (localObject3 != null)
        {
          localObject3 = b;
          ((o.f)localObject3).a(localHashSet1);
        }
        localObject3 = c;
        if (localObject3 != null)
        {
          localObject2 = c;
          ((o.f)localObject2).a(localHashSet2);
        }
      }
      m += 1;
    }
    arrayOfc = (o.c[])j.clone();
    j = arrayOfc;
    return (n)localObject1;
  }
  
  public final o a(String paramString)
  {
    b();
    o.e locale = new org/a/a/d/o$e;
    locale.<init>(paramString);
    a(locale, locale);
    return this;
  }
  
  public final o a(String paramString1, String paramString2, boolean paramBoolean)
  {
    b();
    Object localObject1 = g;
    int k = ((List)localObject1).size();
    if (k == 0)
    {
      if (!paramBoolean)
      {
        localObject1 = new org/a/a/d/o$g;
        localObject2 = o.e.a;
        localObject3 = localObject1;
        localObject4 = paramString2;
        ((o.g)localObject1).<init>(paramString1, paramString2, (q)localObject2, (p)localObject2, paramBoolean);
        a((q)localObject1, (p)localObject1);
      }
      return this;
    }
    k = 0;
    Object localObject5 = null;
    int m = ((List)localObject1).size();
    int n;
    for (;;)
    {
      m += -1;
      n = 1;
      if (m < 0) {
        break;
      }
      localObject4 = ((List)localObject1).get(m);
      boolean bool = localObject4 instanceof o.g;
      if (bool)
      {
        localObject5 = (o.g)((List)localObject1).get(m);
        m += n;
        int i1 = ((List)localObject1).size();
        localObject1 = ((List)localObject1).subList(m, i1);
        break;
      }
      m += -1;
    }
    if (localObject5 != null)
    {
      k = ((List)localObject1).size();
      if (k == 0)
      {
        paramString1 = new java/lang/IllegalStateException;
        paramString1.<init>("Cannot have two adjacent separators");
        throw paramString1;
      }
    }
    localObject5 = a((List)localObject1);
    ((List)localObject1).clear();
    Object localObject3 = new org/a/a/d/o$g;
    Object localObject4 = localObject5[0];
    Object localObject6 = localObject4;
    localObject6 = (q)localObject4;
    localObject5 = localObject5[n];
    Object localObject7 = localObject5;
    localObject7 = (p)localObject5;
    localObject4 = localObject3;
    Object localObject2 = paramString2;
    ((o.g)localObject3).<init>(paramString1, paramString2, (q)localObject6, (p)localObject7, paramBoolean);
    ((List)localObject1).add(localObject3);
    ((List)localObject1).add(localObject3);
    return this;
  }
  
  public final void a(int paramInt)
  {
    int k = a;
    a(paramInt, k);
  }
  
  public final o b(String paramString)
  {
    o.h localh = new org/a/a/d/o$h;
    localh.<init>(paramString);
    return a(localh);
  }
}

/* Location:
 * Qualified Name:     org.a.a.d.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */