package org.a.a.d;

import java.util.Locale;
import org.a.a.a;
import org.a.a.f;
import org.a.a.z;

final class c$l
  implements k, m
{
  private final String a;
  private final String b;
  private final boolean c;
  private final int d;
  private final int e;
  
  c$l(String paramString1, String paramString2, boolean paramBoolean, int paramInt)
  {
    a = paramString1;
    b = paramString2;
    c = paramBoolean;
    int i = 2;
    if (paramInt >= i)
    {
      d = i;
      e = paramInt;
      return;
    }
    paramString1 = new java/lang/IllegalArgumentException;
    paramString1.<init>();
    throw paramString1;
  }
  
  private static int a(CharSequence paramCharSequence, int paramInt1, int paramInt2)
  {
    paramInt2 = Math.min(paramCharSequence.length() - paramInt1, paramInt2);
    int i = 0;
    while (paramInt2 > 0)
    {
      int j = paramInt1 + i;
      j = paramCharSequence.charAt(j);
      int k = 48;
      if (j < k) {
        break;
      }
      k = 57;
      if (j > k) {
        break;
      }
      i += 1;
      paramInt2 += -1;
    }
    return i;
  }
  
  public final int a()
  {
    int i = d;
    int j = i + 1 << 1;
    boolean bool = c;
    if (bool)
    {
      i += -1;
      j += i;
    }
    String str = a;
    if (str != null)
    {
      i = str.length();
      if (i > j)
      {
        str = a;
        j = str.length();
      }
    }
    return j;
  }
  
  public final int a(e parame, CharSequence paramCharSequence, int paramInt)
  {
    int i = paramCharSequence.length() - paramInt;
    String str = b;
    int j = 43;
    int k = 45;
    int m = 0;
    if (str != null)
    {
      int n = str.length();
      if (n == 0)
      {
        if (i > 0)
        {
          n = paramCharSequence.charAt(paramInt);
          if ((n == k) || (n == j)) {}
        }
        else
        {
          paramCharSequence = Integer.valueOf(0);
          parame.a(paramCharSequence);
          return paramInt;
        }
      }
      else
      {
        str = b;
        i1 = c.b(paramCharSequence, paramInt, str);
        if (i1 != 0)
        {
          paramCharSequence = Integer.valueOf(0);
          parame.a(paramCharSequence);
          int i2 = b.length();
          return paramInt + i2;
        }
      }
    }
    int i1 = 1;
    if (i <= i1) {
      return paramInt ^ 0xFFFFFFFF;
    }
    int i3 = paramCharSequence.charAt(paramInt);
    if (i3 == k)
    {
      j = 1;
    }
    else
    {
      if (i3 != j) {
        break label711;
      }
      j = 0;
    }
    i += -1;
    paramInt += i1;
    k = 2;
    i3 = a(paramCharSequence, paramInt, k);
    if (i3 < k) {
      return paramInt ^ 0xFFFFFFFF;
    }
    i3 = h.a(paramCharSequence, paramInt);
    int i4 = 23;
    if (i3 > i4) {
      return paramInt ^ 0xFFFFFFFF;
    }
    i4 = 3600000;
    i3 *= i4;
    i += -2;
    paramInt += k;
    if (i > 0)
    {
      i4 = paramCharSequence.charAt(paramInt);
      int i5 = 58;
      int i6 = 48;
      int i7;
      if (i4 == i5)
      {
        i += -1;
        paramInt += 1;
        m = 1;
      }
      else
      {
        if (i4 < i6) {
          break label688;
        }
        i7 = 57;
        if (i4 > i7) {
          break label688;
        }
      }
      i4 = a(paramCharSequence, paramInt, k);
      if ((i4 != 0) || (m != 0))
      {
        if (i4 < k) {
          return paramInt ^ 0xFFFFFFFF;
        }
        i4 = h.a(paramCharSequence, paramInt);
        i7 = 59;
        if (i4 > i7) {
          return paramInt ^ 0xFFFFFFFF;
        }
        int i8 = 60000;
        i4 *= i8;
        i3 += i4;
        i += -2;
        paramInt += 2;
        if (i > 0) {
          if (m != 0)
          {
            i4 = paramCharSequence.charAt(paramInt);
            if (i4 == i5)
            {
              i += -1;
              paramInt += 1;
            }
          }
          else
          {
            i4 = a(paramCharSequence, paramInt, k);
            if ((i4 != 0) || (m != 0))
            {
              if (i4 < k) {
                return paramInt ^ 0xFFFFFFFF;
              }
              i4 = h.a(paramCharSequence, paramInt);
              if (i4 > i7) {
                return paramInt ^ 0xFFFFFFFF;
              }
              i4 *= 1000;
              i3 += i4;
              i += -2;
              paramInt += 2;
              if (i > 0) {
                if (m != 0)
                {
                  i = paramCharSequence.charAt(paramInt);
                  i4 = 46;
                  if (i != i4)
                  {
                    i = paramCharSequence.charAt(paramInt);
                    i4 = 44;
                    if (i != i4) {}
                  }
                  else
                  {
                    paramInt += 1;
                  }
                }
                else
                {
                  i = a(paramCharSequence, paramInt, 3);
                  if ((i != 0) || (m != 0))
                  {
                    if (i <= 0) {
                      return paramInt ^ 0xFFFFFFFF;
                    }
                    m = paramInt + 1;
                    paramInt = (paramCharSequence.charAt(paramInt) - i6) * 100;
                    i3 += paramInt;
                    if (i > i1)
                    {
                      paramInt = m + 1;
                      i1 = (paramCharSequence.charAt(m) - i6) * 10;
                      i3 += i1;
                      if (i > k)
                      {
                        i = paramInt + 1;
                        int i9 = paramCharSequence.charAt(paramInt) - i6;
                        i3 += i9;
                        paramInt = i;
                      }
                    }
                    else
                    {
                      paramInt = m;
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    label688:
    if (j != 0) {
      i3 = -i3;
    }
    paramCharSequence = Integer.valueOf(i3);
    parame.a(paramCharSequence);
    return paramInt;
    label711:
    return paramInt ^ 0xFFFFFFFF;
  }
  
  public final void a(Appendable paramAppendable, long paramLong, a parama, int paramInt, f paramf, Locale paramLocale)
  {
    if (paramf == null) {
      return;
    }
    if (paramInt == 0)
    {
      String str = a;
      if (str != null)
      {
        paramAppendable.append(str);
        return;
      }
    }
    char c1;
    if (paramInt >= 0)
    {
      c1 = '+';
      paramAppendable.append(c1);
    }
    else
    {
      c1 = '-';
      paramAppendable.append(c1);
      paramInt = -paramInt;
    }
    int i = 3600000;
    int j = paramInt / i;
    int m = 2;
    h.a(paramAppendable, j, m);
    int n = e;
    int i2 = 1;
    if (n == i2) {
      return;
    }
    j *= i;
    paramInt -= j;
    if (paramInt == 0)
    {
      i = d;
      if (i <= i2) {
        return;
      }
    }
    i = 60000;
    j = paramInt / i;
    boolean bool3 = c;
    i2 = 58;
    if (bool3) {
      paramAppendable.append(i2);
    }
    h.a(paramAppendable, j, m);
    int i1 = e;
    if (i1 == m) {
      return;
    }
    j *= i;
    paramInt -= j;
    if (paramInt == 0)
    {
      i = d;
      if (i <= m) {
        return;
      }
    }
    i = paramInt / 1000;
    boolean bool2 = c;
    if (bool2) {
      paramAppendable.append(i2);
    }
    h.a(paramAppendable, i, m);
    int k = e;
    m = 3;
    if (k == m) {
      return;
    }
    i *= 1000;
    paramInt -= i;
    if (paramInt == 0)
    {
      i = d;
      if (i <= m) {
        return;
      }
    }
    boolean bool1 = c;
    if (bool1)
    {
      char c2 = '.';
      paramAppendable.append(c2);
    }
    h.a(paramAppendable, paramInt, m);
  }
  
  public final void a(Appendable paramAppendable, z paramz, Locale paramLocale) {}
  
  public final int b()
  {
    return a();
  }
}

/* Location:
 * Qualified Name:     org.a.a.d.c.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */