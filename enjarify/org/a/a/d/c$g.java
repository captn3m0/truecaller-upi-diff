package org.a.a.d;

import java.util.Locale;
import org.a.a.a;
import org.a.a.d;
import org.a.a.f;
import org.a.a.z;

class c$g
  extends c.f
{
  protected final int d;
  
  protected c$g(d paramd, int paramInt1, boolean paramBoolean, int paramInt2)
  {
    super(paramd, paramInt1, paramBoolean);
    d = paramInt2;
  }
  
  public final int a()
  {
    return b;
  }
  
  public final void a(Appendable paramAppendable, long paramLong, a parama, int paramInt, f paramf, Locale paramLocale)
  {
    try
    {
      d locald = a;
      parama = locald.a(parama);
      i = parama.a(paramLong);
      int j = d;
      h.a(paramAppendable, i, j);
      return;
    }
    catch (RuntimeException localRuntimeException)
    {
      int i = d;
      c.a(paramAppendable, i);
    }
  }
  
  public final void a(Appendable paramAppendable, z paramz, Locale paramLocale)
  {
    paramLocale = a;
    boolean bool = paramz.b(paramLocale);
    if (bool) {
      try
      {
        paramLocale = a;
        j = paramz.a(paramLocale);
        int i = d;
        h.a(paramAppendable, j, i);
        return;
      }
      catch (RuntimeException localRuntimeException) {}
    }
    int j = d;
    c.a(paramAppendable, j);
  }
}

/* Location:
 * Qualified Name:     org.a.a.d.c.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */