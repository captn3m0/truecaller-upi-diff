package org.a.a.d;

import java.util.Locale;
import org.a.a.a;
import org.a.a.c;
import org.a.a.d;
import org.a.a.f;
import org.a.a.z;

final class c$m
  implements k, m
{
  private final d a;
  private final int b;
  private final boolean c;
  
  c$m(d paramd, int paramInt, boolean paramBoolean)
  {
    a = paramd;
    b = paramInt;
    c = paramBoolean;
  }
  
  private int a(long paramLong, a parama)
  {
    try
    {
      d locald = a;
      parama = locald.a(parama);
      int i = parama.a(paramLong);
      if (i < 0) {
        i = -i;
      }
      return i % 100;
    }
    catch (RuntimeException localRuntimeException) {}
    return -1;
  }
  
  private int a(z paramz)
  {
    d locald = a;
    boolean bool = paramz.b(locald);
    if (bool) {
      try
      {
        locald = a;
        int i = paramz.a(locald);
        if (i < 0) {
          i = -i;
        }
        return i % 100;
      }
      catch (RuntimeException localRuntimeException) {}
    }
    return -1;
  }
  
  public final int a()
  {
    return 2;
  }
  
  public final int a(e parame, CharSequence paramCharSequence, int paramInt)
  {
    int i = paramCharSequence.length() - paramInt;
    boolean bool = c;
    int k = 57;
    int m = 2;
    int n = 48;
    int i1 = 1;
    int j;
    int i3;
    if (!bool)
    {
      i = Math.min(m, i);
      if (i < m) {
        return paramInt ^ 0xFFFFFFFF;
      }
    }
    else
    {
      j = paramInt;
      paramInt = 0;
      int i2 = 0;
      i3 = 0;
      while (paramInt < i)
      {
        int i4 = j + paramInt;
        i4 = paramCharSequence.charAt(i4);
        if (paramInt == 0)
        {
          int i5 = 45;
          if (i4 != i5)
          {
            int i6 = 43;
            if (i4 != i6) {}
          }
          else
          {
            if (i4 == i5) {
              i3 = 1;
            } else {
              i3 = 0;
            }
            if (i3 != 0)
            {
              paramInt += 1;
              i2 = 1;
              continue;
            }
            j += 1;
            i += -1;
            i2 = 1;
            continue;
          }
        }
        if ((i4 < n) || (i4 > k)) {
          break;
        }
        paramInt += 1;
      }
      if (paramInt == 0) {
        return j ^ 0xFFFFFFFF;
      }
      if ((i2 != 0) || (paramInt != m)) {
        break label425;
      }
      paramInt = j;
    }
    i = paramCharSequence.charAt(paramInt);
    int i7;
    Object localObject;
    if ((i >= n) && (i <= k))
    {
      i -= n;
      j = paramInt + 1;
      i7 = paramCharSequence.charAt(j);
      if ((i7 >= n) && (i7 <= k))
      {
        j = i << 3;
        i <<= i1;
        j = j + i + i7 - n;
        i7 = b;
        localObject = e;
        if (localObject != null)
        {
          paramCharSequence = e;
          i7 = paramCharSequence.intValue();
        }
        i7 += -50;
        i = 100;
        if (i7 >= 0) {
          k = i7 % 100;
        } else {
          k = (i7 + 1) % i + 99;
        }
        if (j >= k)
        {
          i = 0;
          localObject = null;
        }
        i7 = i7 + i - k;
        j += i7;
        paramCharSequence = a;
        parame.a(paramCharSequence, j);
        return paramInt + m;
      }
      return paramInt ^ 0xFFFFFFFF;
    }
    return paramInt ^ 0xFFFFFFFF;
    label425:
    i = 9;
    if (paramInt >= i)
    {
      paramInt += j;
      paramCharSequence = paramCharSequence.subSequence(j, paramInt).toString();
      i7 = Integer.parseInt(paramCharSequence);
    }
    else
    {
      if (i3 != 0) {
        i = j + 1;
      } else {
        i = j;
      }
      k = i + 1;
    }
    try
    {
      i = paramCharSequence.charAt(i) - n;
      paramInt += j;
      while (k < paramInt)
      {
        j = i << 3;
        i <<= 1;
        j += i;
        i = k + 1;
        k = paramCharSequence.charAt(k);
        j = j + k - n;
        k = i;
        i = j;
      }
      if (i3 != 0) {
        i7 = -i;
      } else {
        i7 = i;
      }
      localObject = a;
      parame.a((d)localObject, i7);
      return paramInt;
    }
    catch (StringIndexOutOfBoundsException localStringIndexOutOfBoundsException) {}
    return j ^ 0xFFFFFFFF;
  }
  
  public final void a(Appendable paramAppendable, long paramLong, a parama, int paramInt, f paramf, Locale paramLocale)
  {
    int i = a(paramLong, parama);
    if (i < 0)
    {
      i = (char)-3;
      paramAppendable.append(i);
      paramAppendable.append(i);
      return;
    }
    h.a(paramAppendable, i, 2);
  }
  
  public final void a(Appendable paramAppendable, z paramz, Locale paramLocale)
  {
    int i = a(paramz);
    if (i < 0)
    {
      i = (char)-3;
      paramAppendable.append(i);
      paramAppendable.append(i);
      return;
    }
    h.a(paramAppendable, i, 2);
  }
  
  public final int b()
  {
    boolean bool = c;
    if (bool) {
      return 4;
    }
    return 2;
  }
}

/* Location:
 * Qualified Name:     org.a.a.d.c.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */