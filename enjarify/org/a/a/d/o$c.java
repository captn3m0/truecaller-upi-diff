package org.a.a.d;

import java.util.Locale;
import org.a.a.aa;
import org.a.a.j;
import org.a.a.s;

final class o$c
  implements p, q
{
  final int a;
  final o.f b;
  final o.f c;
  private final int d;
  private final int e;
  private final int f;
  private final boolean g;
  private final c[] h;
  
  o$c(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean, int paramInt4, c[] paramArrayOfc, o.f paramf)
  {
    d = paramInt1;
    e = paramInt2;
    f = paramInt3;
    g = paramBoolean;
    a = paramInt4;
    h = paramArrayOfc;
    b = paramf;
    c = null;
  }
  
  o$c(c paramc, o.f paramf)
  {
    int i = d;
    d = i;
    i = e;
    e = i;
    i = f;
    f = i;
    boolean bool = g;
    g = bool;
    int j = a;
    a = j;
    Object localObject = h;
    h = ((c[])localObject);
    localObject = b;
    b = ((o.f)localObject);
    paramc = c;
    if (paramc != null)
    {
      localObject = new org/a/a/d/o$b;
      ((o.b)localObject).<init>(paramc, paramf);
      paramf = (o.f)localObject;
    }
    c = paramf;
  }
  
  private long a(aa paramaa)
  {
    int i = e;
    int j = 4;
    s locals;
    if (i == j)
    {
      i = 0;
      locals = null;
    }
    else
    {
      locals = paramaa.b();
    }
    long l1 = Long.MAX_VALUE;
    if (locals != null)
    {
      int k = a;
      boolean bool1 = a(locals, k);
      if (!bool1) {
        return l1;
      }
    }
    int m = a;
    Object localObject;
    int n;
    long l2;
    switch (m)
    {
    default: 
      return l1;
    case 8: 
    case 9: 
      localObject = j.b();
      m = paramaa.a((j)localObject);
      j localj = j.a();
      n = paramaa.a(localj);
      l2 = m;
      long l3 = 1000L;
      l2 *= l3;
      l4 = n;
      l2 += l4;
      break;
    case 7: 
      localObject = j.a();
      m = paramaa.a((j)localObject);
      l2 = m;
      break;
    case 6: 
      localObject = j.b();
      m = paramaa.a((j)localObject);
      l2 = m;
      break;
    case 5: 
      localObject = j.c();
      m = paramaa.a((j)localObject);
      l2 = m;
      break;
    case 4: 
      localObject = j.d();
      m = paramaa.a((j)localObject);
      l2 = m;
      break;
    case 3: 
      localObject = j.f();
      m = paramaa.a((j)localObject);
      l2 = m;
      break;
    case 2: 
      localObject = j.g();
      m = paramaa.a((j)localObject);
      l2 = m;
      break;
    case 1: 
      localObject = j.i();
      m = paramaa.a((j)localObject);
      l2 = m;
      break;
    case 0: 
      localObject = j.j();
      m = paramaa.a((j)localObject);
      l2 = m;
    }
    long l4 = 0L;
    boolean bool3 = l2 < l4;
    if (!bool3)
    {
      m = e;
      n = 5;
      if (m != n)
      {
        n = 9;
        boolean bool4;
        switch (m)
        {
        default: 
          break;
        case 2: 
          bool4 = b(paramaa);
          if (bool4)
          {
            paramaa = h;
            m = a;
            paramaa = paramaa[m];
            if (paramaa == this)
            {
              do
              {
                do
                {
                  m += 1;
                  if (m > n) {
                    break;
                  }
                  bool4 = a(locals, m);
                } while (!bool4);
                paramaa = h[m];
              } while (paramaa == null);
              return l1;
            }
          }
          return l1;
        case 1: 
          bool4 = b(paramaa);
          if (bool4)
          {
            paramaa = h;
            m = a;
            paramaa = paramaa[m];
            if (paramaa == this)
            {
              int i1 = Math.min(m, 8) + -1;
              while ((i1 >= 0) && (i1 <= n))
              {
                boolean bool2 = a(locals, i1);
                if (bool2)
                {
                  localObject = h[i1];
                  if (localObject != null) {
                    return l1;
                  }
                }
                i1 += -1;
              }
            }
          }
          return l1;
        }
      }
      else
      {
        return l1;
      }
    }
    return l2;
  }
  
  private static boolean a(s params, int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return false;
    case 8: 
    case 9: 
      localj = j.b();
      paramInt = params.a(localj);
      if (paramInt == 0)
      {
        localj = j.a();
        boolean bool = params.a(localj);
        if (!bool) {
          return false;
        }
      }
      return true;
    case 7: 
      localj = j.a();
      return params.a(localj);
    case 6: 
      localj = j.b();
      return params.a(localj);
    case 5: 
      localj = j.c();
      return params.a(localj);
    case 4: 
      localj = j.d();
      return params.a(localj);
    case 3: 
      localj = j.f();
      return params.a(localj);
    case 2: 
      localj = j.g();
      return params.a(localj);
    case 1: 
      localj = j.i();
      return params.a(localj);
    }
    j localj = j.j();
    return params.a(localj);
  }
  
  private static boolean b(aa paramaa)
  {
    int i = paramaa.d();
    int j = 0;
    while (j < i)
    {
      int k = paramaa.d(j);
      if (k != 0) {
        return false;
      }
      j += 1;
    }
    return true;
  }
  
  public final int a(aa paramaa, int paramInt, Locale paramLocale)
  {
    if (paramInt <= 0) {
      return 0;
    }
    paramInt = e;
    int i = 4;
    if (paramInt != i)
    {
      long l1 = a(paramaa);
      long l2 = Long.MAX_VALUE;
      boolean bool = l1 < l2;
      if (!bool) {
        return 0;
      }
    }
    return 1;
  }
  
  public final int a(aa paramaa, Locale paramLocale)
  {
    long l1 = a(paramaa);
    long l2 = Long.MAX_VALUE;
    boolean bool1 = l1 < l2;
    if (!bool1) {
      return 0;
    }
    int j = h.a(l1);
    int k = d;
    j = Math.max(j, k);
    k = a;
    int i = 8;
    if (k >= i)
    {
      long l3 = 0L;
      boolean bool2 = l1 < l3;
      if (bool2) {
        m = 5;
      } else {
        m = 4;
      }
      j = Math.max(j, m) + 1;
      int m = a;
      int n = 9;
      long l4 = 1000L;
      if (m == n)
      {
        long l5 = Math.abs(l1) % l4;
        boolean bool3 = l5 < l3;
        if (!bool3) {
          j += -4;
        }
      }
      l1 /= l4;
    }
    int i1 = (int)l1;
    paramaa = b;
    int i2;
    if (paramaa != null)
    {
      i2 = paramaa.a(i1);
      j += i2;
    }
    paramaa = c;
    if (paramaa != null)
    {
      i2 = paramaa.a(i1);
      j += i2;
    }
    return j;
  }
  
  public final void a(StringBuffer paramStringBuffer, aa paramaa, Locale paramLocale)
  {
    long l1 = a(paramaa);
    long l2 = Long.MAX_VALUE;
    boolean bool1 = l1 < l2;
    if (!bool1) {
      return;
    }
    int j = (int)l1;
    int k = a;
    long l3 = 1000L;
    int m = 8;
    if (k >= m)
    {
      l2 = l1 / l3;
      j = (int)l2;
    }
    o.f localf = b;
    if (localf != null) {
      localf.a(paramStringBuffer, j);
    }
    k = paramStringBuffer.length();
    int n = d;
    int i1 = 1;
    if (n <= i1) {
      h.a(paramStringBuffer, j);
    } else {
      h.a(paramStringBuffer, j, n);
    }
    n = a;
    if (n >= m)
    {
      long l4 = Math.abs(l1) % l3;
      int i = (int)l4;
      int i2 = a;
      if ((i2 == m) || (i > 0))
      {
        long l5 = 0L;
        boolean bool2 = l1 < l5;
        if (bool2)
        {
          l5 = -1000L;
          bool2 = l1 < l5;
          if (bool2)
          {
            c1 = '-';
            paramStringBuffer.insert(k, c1);
          }
        }
        paramStringBuffer.append('.');
        char c1 = '\003';
        h.a(paramStringBuffer, i, c1);
      }
    }
    paramaa = c;
    if (paramaa != null) {
      paramaa.a(paramStringBuffer, j);
    }
  }
}

/* Location:
 * Qualified Name:     org.a.a.d.o.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */