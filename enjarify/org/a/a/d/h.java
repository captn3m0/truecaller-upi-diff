package org.a.a.d;

import java.io.IOException;

public final class h
{
  private static final double a = Math.log(10.0D);
  
  public static int a(long paramLong)
  {
    int i = 1;
    long l = 0L;
    double d1 = 0.0D;
    boolean bool = paramLong < l;
    if (bool)
    {
      l = Long.MIN_VALUE;
      d1 = -0.0D;
      bool = paramLong < l;
      if (bool) {
        return a(-paramLong) + i;
      }
      return 20;
    }
    l = 10;
    d1 = 4.9E-323D;
    bool = paramLong < l;
    if (bool) {
      return i;
    }
    l = 100;
    d1 = 4.94E-322D;
    bool = paramLong < l;
    if (bool) {
      return 2;
    }
    l = 1000L;
    d1 = 4.94E-321D;
    bool = paramLong < l;
    if (bool) {
      return 3;
    }
    l = 10000L;
    d1 = 4.9407E-320D;
    bool = paramLong < l;
    if (bool) {
      return 4;
    }
    double d2 = Math.log(paramLong);
    d1 = a;
    return (int)(d2 / d1) + i;
  }
  
  static int a(CharSequence paramCharSequence, int paramInt)
  {
    int i = paramCharSequence.charAt(paramInt) + '￐';
    int j = i << 3;
    i <<= 1;
    j += i;
    paramInt += 1;
    int k = paramCharSequence.charAt(paramInt);
    return j + k + -48;
  }
  
  static String a(String paramString, int paramInt)
  {
    int i = paramInt + 32;
    int j = paramString.length();
    int k = i + 3;
    String str1;
    if (j <= k)
    {
      str1 = paramString;
    }
    else
    {
      j = 0;
      str1 = paramString.substring(0, i);
      String str2 = "...";
      str1 = str1.concat(str2);
    }
    j = 34;
    if (paramInt <= 0)
    {
      paramString = new java/lang/StringBuilder;
      paramString.<init>("Invalid format: \"");
      paramString.append(str1);
      paramString.append(j);
      return paramString.toString();
    }
    int m = paramString.length();
    if (paramInt >= m)
    {
      paramString = new java/lang/StringBuilder;
      paramString.<init>("Invalid format: \"");
      paramString.append(str1);
      paramString.append("\" is too short");
      return paramString.toString();
    }
    paramString = new java/lang/StringBuilder;
    paramString.<init>("Invalid format: \"");
    paramString.append(str1);
    paramString.append("\" is malformed at \"");
    String str3 = str1.substring(paramInt);
    paramString.append(str3);
    paramString.append(j);
    return paramString.toString();
  }
  
  public static void a(Appendable paramAppendable, int paramInt)
  {
    if (paramInt < 0)
    {
      paramAppendable.append('-');
      i = -1 << -1;
      if (paramInt != i)
      {
        paramInt = -paramInt;
      }
      else
      {
        paramAppendable.append("2147483648");
        return;
      }
    }
    int i = 10;
    if (paramInt < i)
    {
      paramInt = (char)(paramInt + 48);
      paramAppendable.append(paramInt);
      return;
    }
    i = 100;
    if (paramInt < i)
    {
      i = (paramInt + 1) * 13421772 >> 27;
      char c = (char)(i + 48);
      paramAppendable.append(c);
      int j = i << 3;
      paramInt -= j;
      i <<= 1;
      paramInt = (char)(paramInt - i + 48);
      paramAppendable.append(paramInt);
      return;
    }
    String str = Integer.toString(paramInt);
    paramAppendable.append(str);
  }
  
  public static void a(Appendable paramAppendable, int paramInt1, int paramInt2)
  {
    int i = 10;
    int j = 48;
    if (paramInt1 < 0)
    {
      paramAppendable.append('-');
      k = -1 << -1;
      if (paramInt1 != k)
      {
        paramInt1 = -paramInt1;
      }
      else
      {
        while (paramInt2 > i)
        {
          paramAppendable.append(j);
          paramInt2 += -1;
        }
        paramAppendable.append("2147483648");
        return;
      }
    }
    int k = 1;
    if (paramInt1 < i)
    {
      while (paramInt2 > k)
      {
        paramAppendable.append(j);
        paramInt2 += -1;
      }
      paramInt1 = (char)(paramInt1 + j);
      paramAppendable.append(paramInt1);
      return;
    }
    i = 100;
    if (paramInt1 < i)
    {
      for (;;)
      {
        i = 2;
        if (paramInt2 <= i) {
          break;
        }
        paramAppendable.append(j);
        paramInt2 += -1;
      }
      paramInt2 = (paramInt1 + 1) * 13421772 >> 27;
      i = (char)(paramInt2 + 48);
      paramAppendable.append(i);
      i = paramInt2 << 3;
      paramInt1 -= i;
      paramInt2 <<= k;
      paramInt1 = (char)(paramInt1 - paramInt2 + j);
      paramAppendable.append(paramInt1);
      return;
    }
    i = 1000;
    if (paramInt1 < i)
    {
      i = 3;
    }
    else
    {
      i = 10000;
      if (paramInt1 < i)
      {
        i = 4;
      }
      else
      {
        double d1 = Math.log(paramInt1);
        double d2 = a;
        d1 /= d2;
        i = (int)d1 + k;
      }
    }
    while (paramInt2 > i)
    {
      paramAppendable.append(j);
      paramInt2 += -1;
    }
    String str = Integer.toString(paramInt1);
    paramAppendable.append(str);
  }
  
  public static void a(StringBuffer paramStringBuffer, int paramInt)
  {
    try
    {
      a(paramStringBuffer, paramInt);
      return;
    }
    catch (IOException localIOException) {}
  }
  
  public static void a(StringBuffer paramStringBuffer, int paramInt1, int paramInt2)
  {
    try
    {
      a(paramStringBuffer, paramInt1, paramInt2);
      return;
    }
    catch (IOException localIOException) {}
  }
  
  public static void a(StringBuffer paramStringBuffer, long paramLong)
  {
    int i = (int)paramLong;
    long l = i;
    boolean bool = l < paramLong;
    if (!bool) {}
    try
    {
      a(paramStringBuffer, i);
      return;
    }
    catch (IOException localIOException) {}
    String str = Long.toString(paramLong);
    paramStringBuffer.append(str);
    return;
  }
}

/* Location:
 * Qualified Name:     org.a.a.d.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */