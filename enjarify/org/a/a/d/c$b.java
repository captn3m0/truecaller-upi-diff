package org.a.a.d;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.a.a.a;
import org.a.a.f;
import org.a.a.z;

final class c$b
  implements k, m
{
  private final m[] a;
  private final k[] b;
  private final int c;
  private final int d;
  
  c$b(List paramList)
  {
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    a(paramList, (List)localObject1, localArrayList);
    int i = 0;
    paramList = null;
    boolean bool = ((List)localObject1).contains(null);
    int k = 0;
    Object localObject2;
    int m;
    if (!bool)
    {
      bool = ((List)localObject1).isEmpty();
      if (!bool)
      {
        int j = ((List)localObject1).size();
        localObject2 = new m[j];
        a = ((m[])localObject2);
        m = 0;
        localObject2 = null;
        int n = 0;
        while (m < j)
        {
          m localm = (m)((List)localObject1).get(m);
          int i1 = localm.a();
          n += i1;
          m[] arrayOfm = a;
          arrayOfm[m] = localm;
          m += 1;
        }
        c = n;
        break label171;
      }
    }
    a = null;
    c = 0;
    label171:
    int i2 = localArrayList.contains(null);
    if (i2 == 0)
    {
      i2 = localArrayList.isEmpty();
      if (i2 == 0)
      {
        i = localArrayList.size();
        localObject1 = new k[i];
        b = ((k[])localObject1);
        i2 = 0;
        localObject1 = null;
        int i3;
        while (k < i)
        {
          k localk = (k)localArrayList.get(k);
          m = localk.b();
          i2 += m;
          localObject2 = b;
          localObject2[k] = localk;
          k += 1;
        }
        d = i3;
        return;
      }
    }
    b = null;
    d = 0;
  }
  
  private static void a(List paramList1, List paramList2, List paramList3)
  {
    int i = paramList1.size();
    int j = 0;
    while (j < i)
    {
      Object localObject = paramList1.get(j);
      boolean bool = localObject instanceof b;
      if (bool)
      {
        localObject = a;
        a(paramList2, (Object[])localObject);
      }
      else
      {
        paramList2.add(localObject);
      }
      int k = j + 1;
      localObject = paramList1.get(k);
      bool = localObject instanceof b;
      if (bool)
      {
        localObject = b;
        a(paramList3, (Object[])localObject);
      }
      else
      {
        paramList3.add(localObject);
      }
      j += 2;
    }
  }
  
  private static void a(List paramList, Object[] paramArrayOfObject)
  {
    if (paramArrayOfObject != null)
    {
      int i = 0;
      for (;;)
      {
        int j = paramArrayOfObject.length;
        if (i >= j) {
          break;
        }
        Object localObject = paramArrayOfObject[i];
        paramList.add(localObject);
        i += 1;
      }
    }
  }
  
  public final int a()
  {
    return c;
  }
  
  public final int a(e parame, CharSequence paramCharSequence, int paramInt)
  {
    k[] arrayOfk = b;
    if (arrayOfk != null)
    {
      int i = arrayOfk.length;
      int j = 0;
      while ((j < i) && (paramInt >= 0))
      {
        k localk = arrayOfk[j];
        paramInt = localk.a(parame, paramCharSequence, paramInt);
        j += 1;
      }
      return paramInt;
    }
    parame = new java/lang/UnsupportedOperationException;
    parame.<init>();
    throw parame;
  }
  
  public final void a(Appendable paramAppendable, long paramLong, a parama, int paramInt, f paramf, Locale paramLocale)
  {
    Object localObject1 = a;
    if (localObject1 != null)
    {
      Locale localLocale;
      if (paramLocale == null) {
        localLocale = Locale.getDefault();
      } else {
        localLocale = paramLocale;
      }
      int i = localObject1.length;
      Object localObject2 = null;
      int j = 0;
      while (j < i)
      {
        localObject2 = localObject1[j];
        ((m)localObject2).a(paramAppendable, paramLong, parama, paramInt, paramf, localLocale);
        j += 1;
      }
      return;
    }
    localObject1 = new java/lang/UnsupportedOperationException;
    ((UnsupportedOperationException)localObject1).<init>();
    throw ((Throwable)localObject1);
  }
  
  public final void a(Appendable paramAppendable, z paramz, Locale paramLocale)
  {
    m[] arrayOfm = a;
    if (arrayOfm != null)
    {
      if (paramLocale == null) {
        paramLocale = Locale.getDefault();
      }
      int i = arrayOfm.length;
      int j = 0;
      while (j < i)
      {
        m localm = arrayOfm[j];
        localm.a(paramAppendable, paramz, paramLocale);
        j += 1;
      }
      return;
    }
    paramAppendable = new java/lang/UnsupportedOperationException;
    paramAppendable.<init>();
    throw paramAppendable;
  }
  
  public final int b()
  {
    return d;
  }
  
  final boolean c()
  {
    m[] arrayOfm = a;
    return arrayOfm != null;
  }
  
  final boolean d()
  {
    k[] arrayOfk = b;
    return arrayOfk != null;
  }
}

/* Location:
 * Qualified Name:     org.a.a.d.c.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */