package org.a.a;

import java.io.Serializable;

public abstract class j
  implements Serializable
{
  static final j a;
  static final j b;
  static final j c;
  static final j d;
  static final j e;
  static final j f;
  static final j g;
  static final j h;
  static final j i;
  static final j j;
  static final j k;
  static final j l;
  private static final long serialVersionUID = 8765135187319L;
  public final String m;
  
  static
  {
    j.a locala = new org/a/a/j$a;
    locala.<init>("eras", (byte)1);
    a = locala;
    locala = new org/a/a/j$a;
    locala.<init>("centuries", (byte)2);
    b = locala;
    locala = new org/a/a/j$a;
    locala.<init>("weekyears", (byte)3);
    c = locala;
    locala = new org/a/a/j$a;
    locala.<init>("years", (byte)4);
    d = locala;
    locala = new org/a/a/j$a;
    locala.<init>("months", (byte)5);
    e = locala;
    locala = new org/a/a/j$a;
    locala.<init>("weeks", (byte)6);
    f = locala;
    locala = new org/a/a/j$a;
    locala.<init>("days", (byte)7);
    g = locala;
    locala = new org/a/a/j$a;
    locala.<init>("halfdays", (byte)8);
    h = locala;
    locala = new org/a/a/j$a;
    locala.<init>("hours", (byte)9);
    i = locala;
    locala = new org/a/a/j$a;
    locala.<init>("minutes", (byte)10);
    j = locala;
    locala = new org/a/a/j$a;
    locala.<init>("seconds", (byte)11);
    k = locala;
    locala = new org/a/a/j$a;
    locala.<init>("millis", (byte)12);
    l = locala;
  }
  
  protected j(String paramString)
  {
    m = paramString;
  }
  
  public static j a()
  {
    return l;
  }
  
  public static j b()
  {
    return k;
  }
  
  public static j c()
  {
    return j;
  }
  
  public static j d()
  {
    return i;
  }
  
  public static j e()
  {
    return h;
  }
  
  public static j f()
  {
    return g;
  }
  
  public static j g()
  {
    return f;
  }
  
  public static j h()
  {
    return c;
  }
  
  public static j i()
  {
    return e;
  }
  
  public static j j()
  {
    return d;
  }
  
  public static j k()
  {
    return b;
  }
  
  public static j l()
  {
    return a;
  }
  
  public abstract i a(a parama);
  
  public String toString()
  {
    return m;
  }
}

/* Location:
 * Qualified Name:     org.a.a.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */