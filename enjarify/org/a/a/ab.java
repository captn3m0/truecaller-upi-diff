package org.a.a;

import org.a.a.a.l;
import org.a.a.d.n;

public final class ab
  extends l
{
  public static final ab a;
  public static final ab b;
  public static final ab c;
  public static final ab d;
  public static final ab e;
  public static final ab f;
  private static final n g;
  private static final long serialVersionUID = 87525275727380862L;
  
  static
  {
    Object localObject = new org/a/a/ab;
    ((ab)localObject).<init>(0);
    a = (ab)localObject;
    localObject = new org/a/a/ab;
    ((ab)localObject).<init>(1);
    b = (ab)localObject;
    localObject = new org/a/a/ab;
    ((ab)localObject).<init>(2);
    c = (ab)localObject;
    localObject = new org/a/a/ab;
    ((ab)localObject).<init>(3);
    d = (ab)localObject;
    localObject = new org/a/a/ab;
    ((ab)localObject).<init>(-1 >>> 1);
    e = (ab)localObject;
    localObject = new org/a/a/ab;
    ((ab)localObject).<init>(-1 << -1);
    f = (ab)localObject;
    localObject = org.a.a.d.j.a();
    s locals = s.d();
    g = ((n)localObject).a(locals);
  }
  
  private ab(int paramInt)
  {
    super(paramInt);
  }
  
  private static ab a(int paramInt)
  {
    int i = -1 << -1;
    if (paramInt != i)
    {
      i = -1 >>> 1;
      if (paramInt != i)
      {
        switch (paramInt)
        {
        default: 
          ab localab = new org/a/a/ab;
          localab.<init>(paramInt);
          return localab;
        case 3: 
          return d;
        case 2: 
          return c;
        case 1: 
          return b;
        }
        return a;
      }
      return e;
    }
    return f;
  }
  
  public static ab a(x paramx1, x paramx2)
  {
    j localj = j.b();
    return a(l.a(paramx1, paramx2, localj));
  }
  
  private Object readResolve()
  {
    return a(k);
  }
  
  public final j a()
  {
    return j.b();
  }
  
  public final s b()
  {
    return s.d();
  }
  
  public final int c()
  {
    return k;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("PT");
    String str = String.valueOf(k);
    localStringBuilder.append(str);
    localStringBuilder.append("S");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     org.a.a.ab
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */