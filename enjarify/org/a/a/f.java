package org.a.a;

import java.io.File;
import java.io.Serializable;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicReference;
import org.a.a.d.b;
import org.a.a.e.c;
import org.a.a.e.d;
import org.a.a.e.e;
import org.a.a.e.g;

public abstract class f
  implements Serializable
{
  public static final f a = ac.c;
  private static final AtomicReference c;
  private static final AtomicReference d;
  private static final AtomicReference e;
  private static final long serialVersionUID = 5546345482340108586L;
  public final String b;
  
  static
  {
    AtomicReference localAtomicReference = new java/util/concurrent/atomic/AtomicReference;
    localAtomicReference.<init>();
    c = localAtomicReference;
    localAtomicReference = new java/util/concurrent/atomic/AtomicReference;
    localAtomicReference.<init>();
    d = localAtomicReference;
    localAtomicReference = new java/util/concurrent/atomic/AtomicReference;
    localAtomicReference.<init>();
    e = localAtomicReference;
  }
  
  protected f(String paramString)
  {
    if (paramString != null)
    {
      b = paramString;
      return;
    }
    paramString = new java/lang/IllegalArgumentException;
    paramString.<init>("Id must not be null");
    throw paramString;
  }
  
  public static f a()
  {
    f localf = (f)e.get();
    if (localf == null) {
      localObject = "user.timezone";
    }
    try
    {
      localObject = System.getProperty((String)localObject);
      if (localObject != null) {
        localf = a((String)localObject);
      }
    }
    catch (RuntimeException localRuntimeException)
    {
      boolean bool;
      for (;;) {}
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      for (;;) {}
    }
    if (localf == null)
    {
      localObject = TimeZone.getDefault();
      localf = a((TimeZone)localObject);
    }
    if (localf == null) {
      localf = a;
    }
    Object localObject = e;
    bool = ((AtomicReference)localObject).compareAndSet(null, localf);
    if (!bool) {
      localf = (f)e.get();
    }
    return localf;
  }
  
  public static f a(int paramInt)
  {
    int i = -86399999;
    if (paramInt >= i)
    {
      i = 86399999;
      if (paramInt <= i) {
        return a(b(paramInt), paramInt);
      }
    }
    IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
    String str = String.valueOf(paramInt);
    str = "Millis out of range: ".concat(str);
    localIllegalArgumentException.<init>(str);
    throw localIllegalArgumentException;
  }
  
  public static f a(String paramString)
  {
    if (paramString == null) {
      return a();
    }
    Object localObject = "UTC";
    boolean bool1 = paramString.equals(localObject);
    if (bool1) {
      return a;
    }
    localObject = e().a(paramString);
    if (localObject != null) {
      return (f)localObject;
    }
    localObject = "+";
    bool1 = paramString.startsWith((String)localObject);
    if (!bool1)
    {
      localObject = "-";
      bool1 = paramString.startsWith((String)localObject);
      if (!bool1)
      {
        localObject = new java/lang/IllegalArgumentException;
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>("The datetime zone id '");
        localStringBuilder.append(paramString);
        localStringBuilder.append("' is not recognised");
        paramString = localStringBuilder.toString();
        ((IllegalArgumentException)localObject).<init>(paramString);
        throw ((Throwable)localObject);
      }
    }
    int i = d(paramString);
    long l1 = i;
    long l2 = 0L;
    boolean bool2 = l1 < l2;
    if (!bool2) {
      return a;
    }
    return a(b(i), i);
  }
  
  private static f a(String paramString, int paramInt)
  {
    if (paramInt == 0) {
      return a;
    }
    d locald = new org/a/a/e/d;
    locald.<init>(paramString, null, paramInt, paramInt);
    return locald;
  }
  
  public static f a(TimeZone paramTimeZone)
  {
    if (paramTimeZone == null) {
      return a();
    }
    paramTimeZone = paramTimeZone.getID();
    if (paramTimeZone != null)
    {
      Object localObject1 = "UTC";
      boolean bool1 = paramTimeZone.equals(localObject1);
      if (bool1) {
        return a;
      }
      bool1 = false;
      localObject1 = null;
      Object localObject2 = c(paramTimeZone);
      org.a.a.e.f localf = e();
      if (localObject2 != null) {
        localObject1 = localf.a((String)localObject2);
      }
      if (localObject1 == null) {
        localObject1 = localf.a(paramTimeZone);
      }
      if (localObject1 != null) {
        return (f)localObject1;
      }
      if (localObject2 == null)
      {
        localObject1 = "GMT+";
        bool1 = paramTimeZone.startsWith((String)localObject1);
        if (!bool1)
        {
          localObject1 = "GMT-";
          bool1 = paramTimeZone.startsWith((String)localObject1);
          if (!bool1) {}
        }
        else
        {
          paramTimeZone = paramTimeZone.substring(3);
          int i = paramTimeZone.length();
          int j = 2;
          if (i > j)
          {
            i = paramTimeZone.charAt(1);
            int k = 57;
            if (i > k)
            {
              boolean bool2 = Character.isDigit(i);
              if (bool2) {
                paramTimeZone = b(paramTimeZone);
              }
            }
          }
          int m = d(paramTimeZone);
          long l1 = m;
          long l2 = 0L;
          boolean bool3 = l1 < l2;
          if (!bool3) {
            return a;
          }
          return a(b(m), m);
        }
      }
      localObject1 = new java/lang/IllegalArgumentException;
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("The datetime zone id '");
      ((StringBuilder)localObject2).append(paramTimeZone);
      ((StringBuilder)localObject2).append("' is not recognised");
      paramTimeZone = ((StringBuilder)localObject2).toString();
      ((IllegalArgumentException)localObject1).<init>(paramTimeZone);
      throw ((Throwable)localObject1);
    }
    paramTimeZone = new java/lang/IllegalArgumentException;
    paramTimeZone.<init>("The TimeZone id must not be null");
    throw paramTimeZone;
  }
  
  public static void a(org.a.a.e.f paramf)
  {
    SecurityManager localSecurityManager = System.getSecurityManager();
    if (localSecurityManager != null)
    {
      o localo = new org/a/a/o;
      String str = "DateTimeZone.setProvider";
      localo.<init>(str);
      localSecurityManager.checkPermission(localo);
    }
    b(paramf);
    c.set(paramf);
  }
  
  public static void a(f paramf)
  {
    SecurityManager localSecurityManager = System.getSecurityManager();
    if (localSecurityManager != null)
    {
      o localo = new org/a/a/o;
      String str = "DateTimeZone.setDefault";
      localo.<init>(str);
      localSecurityManager.checkPermission(localo);
    }
    if (paramf != null)
    {
      e.set(paramf);
      return;
    }
    paramf = new java/lang/IllegalArgumentException;
    paramf.<init>("The datetime zone must not be null");
    throw paramf;
  }
  
  public static String b(int paramInt)
  {
    StringBuffer localStringBuffer = new java/lang/StringBuffer;
    localStringBuffer.<init>();
    char c1;
    if (paramInt >= 0)
    {
      c1 = '+';
      localStringBuffer.append(c1);
    }
    else
    {
      c1 = '-';
      localStringBuffer.append(c1);
      paramInt = -paramInt;
    }
    int i = 3600000;
    int j = paramInt / i;
    int k = 2;
    org.a.a.d.h.a(localStringBuffer, j, k);
    j *= i;
    paramInt -= j;
    i = 60000;
    j = paramInt / i;
    char c2 = ':';
    localStringBuffer.append(c2);
    org.a.a.d.h.a(localStringBuffer, j, k);
    j *= i;
    paramInt -= j;
    if (paramInt == 0) {
      return localStringBuffer.toString();
    }
    i = paramInt / 1000;
    localStringBuffer.append(c2);
    org.a.a.d.h.a(localStringBuffer, i, k);
    i *= 1000;
    paramInt -= i;
    if (paramInt == 0) {
      return localStringBuffer.toString();
    }
    localStringBuffer.append('.');
    org.a.a.d.h.a(localStringBuffer, paramInt, 3);
    return localStringBuffer.toString();
  }
  
  private static String b(String paramString)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(paramString);
    int i = 0;
    paramString = null;
    for (;;)
    {
      int j = localStringBuilder.length();
      if (i >= j) {
        break;
      }
      j = localStringBuilder.charAt(i);
      int m = 10;
      int k = Character.digit(j, m);
      if (k >= 0)
      {
        k = (char)(k + 48);
        localStringBuilder.setCharAt(i, k);
      }
      i += 1;
    }
    return localStringBuilder.toString();
  }
  
  public static Set b()
  {
    return e().a();
  }
  
  private static org.a.a.e.f b(org.a.a.e.f paramf)
  {
    Object localObject1 = paramf.a();
    if (localObject1 != null)
    {
      int i = ((Set)localObject1).size();
      if (i != 0)
      {
        Object localObject2 = "UTC";
        boolean bool = ((Set)localObject1).contains(localObject2);
        if (bool)
        {
          localObject1 = a;
          localObject2 = paramf.a("UTC");
          bool = ((f)localObject1).equals(localObject2);
          if (bool) {
            return paramf;
          }
          paramf = new java/lang/IllegalArgumentException;
          paramf.<init>("Invalid UTC zone provided");
          throw paramf;
        }
        paramf = new java/lang/IllegalArgumentException;
        paramf.<init>("The provider doesn't support UTC");
        throw paramf;
      }
    }
    paramf = new java/lang/IllegalArgumentException;
    paramf.<init>("The provider doesn't have any available ids");
    throw paramf;
  }
  
  private static String c(String paramString)
  {
    return (String)f.a.a.get(paramString);
  }
  
  public static e c()
  {
    e locale = (e)d.get();
    if (locale == null)
    {
      locale = g();
      AtomicReference localAtomicReference = d;
      boolean bool = localAtomicReference.compareAndSet(null, locale);
      if (!bool) {
        locale = (e)d.get();
      }
    }
    return locale;
  }
  
  private static int d(String paramString)
  {
    return -(int)f.a.b.a(paramString);
  }
  
  private static org.a.a.e.f e()
  {
    org.a.a.e.f localf = (org.a.a.e.f)c.get();
    if (localf == null)
    {
      localf = f();
      AtomicReference localAtomicReference = c;
      boolean bool = localAtomicReference.compareAndSet(null, localf);
      if (!bool) {
        localf = (org.a.a.e.f)c.get();
      }
    }
    return localf;
  }
  
  private static org.a.a.e.f f()
  {
    Object localObject1 = "org.joda.time.DateTimeZone.Provider";
    try
    {
      localObject1 = System.getProperty((String)localObject1);
      if (localObject1 != null) {
        try
        {
          localObject1 = Class.forName((String)localObject1);
          localObject1 = ((Class)localObject1).newInstance();
          localObject1 = (org.a.a.e.f)localObject1;
          return b((org.a.a.e.f)localObject1);
        }
        catch (Exception localException1)
        {
          localObject3 = new java/lang/RuntimeException;
          ((RuntimeException)localObject3).<init>(localException1);
          throw ((Throwable)localObject3);
        }
      }
    }
    catch (SecurityException localSecurityException1)
    {
      Object localObject3;
      String str;
      for (;;) {}
    }
    str = "org.joda.time.DateTimeZone.Folder";
    for (;;)
    {
      try
      {
        str = System.getProperty(str);
        if (str != null) {
          try
          {
            localObject3 = new org/a/a/e/h;
            File localFile = new java/io/File;
            localFile.<init>(str);
            ((org.a.a.e.h)localObject3).<init>(localFile);
            return b((org.a.a.e.f)localObject3);
          }
          catch (Exception localException2)
          {
            localObject3 = new java/lang/RuntimeException;
            ((RuntimeException)localObject3).<init>(localException2);
            throw ((Throwable)localObject3);
          }
        }
      }
      catch (SecurityException localSecurityException2)
      {
        Object localObject2;
        continue;
      }
      try
      {
        localObject2 = new org/a/a/e/h;
        localObject3 = "org/joda/time/tz/data";
        ((org.a.a.e.h)localObject2).<init>((String)localObject3);
        return b((org.a.a.e.f)localObject2);
      }
      catch (Exception localException3)
      {
        localException3.printStackTrace();
        localObject2 = new org/a/a/e/g;
        ((g)localObject2).<init>();
        return (org.a.a.e.f)localObject2;
      }
    }
  }
  
  private static e g()
  {
    Object localObject = "org.joda.time.DateTimeZone.NameProvider";
    try
    {
      localObject = System.getProperty((String)localObject);
      if (localObject != null) {
        try
        {
          localObject = Class.forName((String)localObject);
          localObject = ((Class)localObject).newInstance();
          localObject = (e)localObject;
        }
        catch (Exception localException)
        {
          RuntimeException localRuntimeException = new java/lang/RuntimeException;
          localRuntimeException.<init>(localException);
          throw localRuntimeException;
        }
      }
    }
    catch (SecurityException localSecurityException)
    {
      c localc;
      for (;;) {}
    }
    localc = null;
    if (localc == null)
    {
      localc = new org/a/a/e/c;
      localc.<init>();
    }
    return localc;
  }
  
  public final long a(long paramLong1, long paramLong2)
  {
    int i = b(paramLong2);
    long l = i;
    l = paramLong1 - l;
    int j = b(l);
    if (j == i) {
      return l;
    }
    return g(paramLong1);
  }
  
  public abstract String a(long paramLong);
  
  public abstract int b(long paramLong);
  
  public abstract int c(long paramLong);
  
  public abstract boolean d();
  
  public final boolean d(long paramLong)
  {
    int i = b(paramLong);
    int j = c(paramLong);
    return i == j;
  }
  
  public int e(long paramLong)
  {
    int i = b(paramLong);
    long l1 = i;
    l1 = paramLong - l1;
    int j = b(l1);
    if (i != j)
    {
      int k = i - j;
      if (k < 0)
      {
        long l2 = h(l1);
        long l3 = Long.MAX_VALUE;
        boolean bool3 = l2 < l1;
        if (!bool3) {
          l2 = l3;
        }
        l1 = j;
        paramLong -= l1;
        l1 = h(paramLong);
        bool3 = l1 < paramLong;
        if (!bool3) {
          l1 = l3;
        }
        boolean bool4 = l2 < l1;
        if (bool4) {
          return i;
        }
      }
    }
    else if (i >= 0)
    {
      paramLong = i(l1);
      boolean bool2 = paramLong < l1;
      if (bool2)
      {
        int m = b(paramLong);
        i = m - i;
        l1 -= paramLong;
        paramLong = i;
        boolean bool1 = l1 < paramLong;
        if (!bool1) {
          return m;
        }
      }
    }
    return j;
  }
  
  public abstract boolean equals(Object paramObject);
  
  public final long f(long paramLong)
  {
    int i = b(paramLong);
    long l1 = i;
    long l2 = paramLong + l1;
    long l3 = paramLong ^ l2;
    long l4 = 0L;
    boolean bool2 = l3 < l4;
    if (bool2)
    {
      paramLong ^= l1;
      boolean bool1 = paramLong < l4;
      if (!bool1)
      {
        ArithmeticException localArithmeticException = new java/lang/ArithmeticException;
        localArithmeticException.<init>("Adding time zone offset caused overflow");
        throw localArithmeticException;
      }
    }
    return l2;
  }
  
  public final long g(long paramLong)
  {
    int i = b(paramLong);
    long l1 = i;
    l1 = paramLong - l1;
    int j = b(l1);
    if ((i != j) && (i < 0))
    {
      l2 = h(l1);
      l3 = Long.MAX_VALUE;
      bool2 = l2 < l1;
      if (!bool2) {
        l2 = l3;
      }
      l1 = j;
      l1 = paramLong - l1;
      long l4 = h(l1);
      boolean bool3 = l4 < l1;
      if (bool3) {
        l3 = l4;
      }
      boolean bool4 = l2 < l3;
      if (bool4) {}
    }
    else
    {
      i = j;
    }
    long l5 = i;
    long l6 = paramLong - l5;
    long l2 = paramLong ^ l6;
    long l3 = 0L;
    boolean bool2 = l2 < l3;
    if (bool2)
    {
      paramLong ^= l5;
      boolean bool1 = paramLong < l3;
      if (bool1)
      {
        ArithmeticException localArithmeticException = new java/lang/ArithmeticException;
        localArithmeticException.<init>("Subtracting time zone offset caused overflow");
        throw localArithmeticException;
      }
    }
    return l6;
  }
  
  public abstract long h(long paramLong);
  
  public int hashCode()
  {
    return b.hashCode() + 57;
  }
  
  public abstract long i(long paramLong);
  
  public String toString()
  {
    return b;
  }
  
  protected Object writeReplace()
  {
    f.b localb = new org/a/a/f$b;
    String str = b;
    localb.<init>(str);
    return localb;
  }
}

/* Location:
 * Qualified Name:     org.a.a.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */