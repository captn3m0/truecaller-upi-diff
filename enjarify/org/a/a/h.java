package org.a.a;

import java.io.Serializable;
import org.a.a.c.g;

public final class h
  extends org.a.a.a.h
  implements Serializable, w
{
  public static final h a;
  private static final long serialVersionUID = 2471658376918L;
  
  static
  {
    h localh = new org/a/a/h;
    localh.<init>(0L);
    a = localh;
  }
  
  private h(long paramLong)
  {
    super(paramLong);
  }
  
  public h(x paramx1, x paramx2)
  {
    super(paramx1, paramx2);
  }
  
  public static h a(long paramLong)
  {
    long l = 0L;
    boolean bool = paramLong < l;
    if (!bool) {
      return a;
    }
    h localh = new org/a/a/h;
    paramLong = g.a(paramLong, 86400000);
    localh.<init>(paramLong);
    return localh;
  }
  
  public static h b(long paramLong)
  {
    long l = 0L;
    boolean bool = paramLong < l;
    if (!bool) {
      return a;
    }
    h localh = new org/a/a/h;
    paramLong = g.a(paramLong, 3600000);
    localh.<init>(paramLong);
    return localh;
  }
  
  public static h c(long paramLong)
  {
    long l = 0L;
    boolean bool = paramLong < l;
    if (!bool) {
      return a;
    }
    h localh = new org/a/a/h;
    paramLong = g.a(paramLong, 60000);
    localh.<init>(paramLong);
    return localh;
  }
  
  public static h d(long paramLong)
  {
    long l = 0L;
    boolean bool = paramLong < l;
    if (!bool) {
      return a;
    }
    h localh = new org/a/a/h;
    paramLong = g.a(paramLong, 1000);
    localh.<init>(paramLong);
    return localh;
  }
  
  public static h e(long paramLong)
  {
    long l = 0L;
    boolean bool = paramLong < l;
    if (!bool) {
      return a;
    }
    h localh = new org/a/a/h;
    localh.<init>(paramLong);
    return localh;
  }
}

/* Location:
 * Qualified Name:     org.a.a.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */