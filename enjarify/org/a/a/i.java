package org.a.a;

public abstract class i
  implements Comparable
{
  private long d(long paramLong1, long paramLong2)
  {
    long l = Long.MIN_VALUE;
    boolean bool = paramLong2 < l;
    if (bool)
    {
      paramLong2 = -paramLong2;
      return a(paramLong1, paramLong2);
    }
    ArithmeticException localArithmeticException = new java/lang/ArithmeticException;
    localArithmeticException.<init>("Long.MIN_VALUE cannot be negated");
    throw localArithmeticException;
  }
  
  public abstract long a(long paramLong, int paramInt);
  
  public abstract long a(long paramLong1, long paramLong2);
  
  public abstract j a();
  
  public abstract int b(long paramLong1, long paramLong2);
  
  public final long b(long paramLong, int paramInt)
  {
    int i = -1 << -1;
    if (paramInt == i)
    {
      long l = paramInt;
      return d(paramLong, l);
    }
    paramInt = -paramInt;
    return a(paramLong, paramInt);
  }
  
  public abstract boolean b();
  
  public abstract long c(long paramLong1, long paramLong2);
  
  public abstract boolean c();
  
  public abstract long d();
}

/* Location:
 * Qualified Name:     org.a.a.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */