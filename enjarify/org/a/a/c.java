package org.a.a;

import java.util.Locale;

public abstract class c
{
  public abstract int a(long paramLong);
  
  public abstract int a(Locale paramLocale);
  
  public abstract long a(long paramLong, int paramInt);
  
  public abstract long a(long paramLong1, long paramLong2);
  
  public abstract long a(long paramLong, String paramString, Locale paramLocale);
  
  public abstract String a(int paramInt, Locale paramLocale);
  
  public abstract String a(long paramLong, Locale paramLocale);
  
  public abstract String a(z paramz, Locale paramLocale);
  
  public abstract d a();
  
  public abstract int b(long paramLong1, long paramLong2);
  
  public abstract long b(long paramLong, int paramInt);
  
  public abstract String b();
  
  public abstract String b(int paramInt, Locale paramLocale);
  
  public abstract String b(long paramLong, Locale paramLocale);
  
  public abstract String b(z paramz, Locale paramLocale);
  
  public abstract boolean b(long paramLong);
  
  public abstract int c(long paramLong);
  
  public long c(long paramLong, int paramInt)
  {
    return b(paramLong, paramInt);
  }
  
  public abstract long c(long paramLong1, long paramLong2);
  
  public abstract boolean c();
  
  public abstract long d(long paramLong);
  
  public abstract i d();
  
  public abstract long e(long paramLong);
  
  public abstract i e();
  
  public abstract long f(long paramLong);
  
  public abstract i f();
  
  public abstract int g();
  
  public abstract long g(long paramLong);
  
  public abstract int h();
  
  public abstract long h(long paramLong);
  
  public abstract long i(long paramLong);
}

/* Location:
 * Qualified Name:     org.a.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */