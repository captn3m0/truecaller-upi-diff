package org.a.a;

import org.a.a.a.l;
import org.a.a.d.n;

public final class g
  extends l
{
  public static final g a;
  public static final g b;
  public static final g c;
  public static final g d;
  public static final g e;
  public static final g f;
  public static final g g;
  public static final g h;
  public static final g i;
  public static final g j;
  private static final n l;
  private static final long serialVersionUID = 87525275727380865L;
  
  static
  {
    Object localObject = new org/a/a/g;
    ((g)localObject).<init>(0);
    a = (g)localObject;
    localObject = new org/a/a/g;
    ((g)localObject).<init>(1);
    b = (g)localObject;
    localObject = new org/a/a/g;
    ((g)localObject).<init>(2);
    c = (g)localObject;
    localObject = new org/a/a/g;
    ((g)localObject).<init>(3);
    d = (g)localObject;
    localObject = new org/a/a/g;
    ((g)localObject).<init>(4);
    e = (g)localObject;
    localObject = new org/a/a/g;
    ((g)localObject).<init>(5);
    f = (g)localObject;
    localObject = new org/a/a/g;
    ((g)localObject).<init>(6);
    g = (g)localObject;
    localObject = new org/a/a/g;
    ((g)localObject).<init>(7);
    h = (g)localObject;
    localObject = new org/a/a/g;
    ((g)localObject).<init>(-1 >>> 1);
    i = (g)localObject;
    localObject = new org/a/a/g;
    ((g)localObject).<init>(-1 << -1);
    j = (g)localObject;
    localObject = org.a.a.d.j.a();
    s locals = s.c();
    l = ((n)localObject).a(locals);
  }
  
  private g(int paramInt)
  {
    super(paramInt);
  }
  
  private static g a(int paramInt)
  {
    int k = -1 << -1;
    if (paramInt != k)
    {
      k = -1 >>> 1;
      if (paramInt != k)
      {
        switch (paramInt)
        {
        default: 
          g localg = new org/a/a/g;
          localg.<init>(paramInt);
          return localg;
        case 7: 
          return h;
        case 6: 
          return g;
        case 5: 
          return f;
        case 4: 
          return e;
        case 3: 
          return d;
        case 2: 
          return c;
        case 1: 
          return b;
        }
        return a;
      }
      return i;
    }
    return j;
  }
  
  public static g a(x paramx1, x paramx2)
  {
    j localj = j.f();
    return a(l.a(paramx1, paramx2, localj));
  }
  
  private Object readResolve()
  {
    return a(k);
  }
  
  public final j a()
  {
    return j.f();
  }
  
  public final s b()
  {
    return s.c();
  }
  
  public final int c()
  {
    return k;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("P");
    String str = String.valueOf(k);
    localStringBuilder.append(str);
    localStringBuilder.append("D");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     org.a.a.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */