package org.a.a.a;

import org.a.a.a;
import org.a.a.c;
import org.a.a.c.g;
import org.a.a.d;
import org.a.a.z;

public abstract class e
  implements Comparable, z
{
  private int c(d paramd)
  {
    int i = 0;
    for (;;)
    {
      int j = 3;
      if (i >= j) {
        break;
      }
      d locald = b(i);
      if (locald == paramd) {
        return i;
      }
      i += 1;
    }
    return -1;
  }
  
  public int a(d paramd)
  {
    int i = c(paramd);
    int j = -1;
    if (i != j) {
      return a(i);
    }
    IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Field '");
    localStringBuilder.append(paramd);
    localStringBuilder.append("' is not supported");
    paramd = localStringBuilder.toString();
    localIllegalArgumentException.<init>(paramd);
    throw localIllegalArgumentException;
  }
  
  public int a(z paramz)
  {
    if (this == paramz) {
      return 0;
    }
    int i = 0;
    int j;
    for (;;)
    {
      j = 3;
      if (i >= j) {
        break label58;
      }
      d locald1 = b(i);
      d locald2 = paramz.b(i);
      if (locald1 != locald2) {
        break;
      }
      i += 1;
    }
    paramz = new java/lang/ClassCastException;
    paramz.<init>("ReadablePartial objects must have matching field types");
    throw paramz;
    label58:
    i = 0;
    while (i < j)
    {
      int k = a(i);
      int m = paramz.a(i);
      if (k > m) {
        return 1;
      }
      k = a(i);
      m = paramz.a(i);
      if (k < m) {
        return -1;
      }
      i += 1;
    }
    return 0;
  }
  
  protected abstract c a(int paramInt, a parama);
  
  public final d b(int paramInt)
  {
    a locala = b();
    return a(paramInt, locala).a();
  }
  
  public boolean b(d paramd)
  {
    int i = c(paramd);
    int j = -1;
    return i != j;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    int i = paramObject instanceof z;
    if (i == 0) {
      return false;
    }
    paramObject = (z)paramObject;
    i = 0;
    a locala = null;
    for (;;)
    {
      int k = 3;
      if (i >= k) {
        break label94;
      }
      int m = a(i);
      int n = ((z)paramObject).a(i);
      if (m != n) {
        break;
      }
      d locald1 = b(i);
      d locald2 = ((z)paramObject).b(i);
      if (locald1 != locald2) {
        break;
      }
      int j;
      i += 1;
    }
    return false;
    label94:
    locala = b();
    paramObject = ((z)paramObject).b();
    return g.a(locala, paramObject);
  }
  
  public int hashCode()
  {
    int i = 157;
    int j = 0;
    for (;;)
    {
      int k = 3;
      if (j >= k) {
        break;
      }
      i *= 23;
      k = a(j);
      i = (i + k) * 23;
      d locald = b(j);
      k = locald.hashCode();
      i += k;
      j += 1;
    }
    j = b().hashCode();
    return i + j;
  }
}

/* Location:
 * Qualified Name:     org.a.a.a.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */