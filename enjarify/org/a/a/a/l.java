package org.a.a.a;

import java.io.Serializable;
import org.a.a.a;
import org.a.a.aa;
import org.a.a.e;
import org.a.a.i;
import org.a.a.j;
import org.a.a.s;
import org.a.a.x;

public abstract class l
  implements Serializable, Comparable, aa
{
  private static final long serialVersionUID = 9386874258972L;
  protected volatile int k;
  
  protected l(int paramInt)
  {
    k = paramInt;
  }
  
  public static int a(x paramx1, x paramx2, j paramj)
  {
    if ((paramx1 != null) && (paramx2 != null))
    {
      a locala = e.b(paramx1);
      paramj = paramj.a(locala);
      long l1 = paramx2.a();
      long l2 = paramx1.a();
      return paramj.b(l1, l2);
    }
    paramx1 = new java/lang/IllegalArgumentException;
    paramx1.<init>("ReadableInstant objects must not be null");
    throw paramx1;
  }
  
  public final int a(j paramj)
  {
    j localj = a();
    if (paramj == localj) {
      return k;
    }
    return 0;
  }
  
  public abstract j a();
  
  public abstract s b();
  
  public final j c(int paramInt)
  {
    if (paramInt == 0) {
      return a();
    }
    IndexOutOfBoundsException localIndexOutOfBoundsException = new java/lang/IndexOutOfBoundsException;
    String str = String.valueOf(paramInt);
    localIndexOutOfBoundsException.<init>(str);
    throw localIndexOutOfBoundsException;
  }
  
  public final int d()
  {
    return 1;
  }
  
  public final int d(int paramInt)
  {
    if (paramInt == 0) {
      return k;
    }
    IndexOutOfBoundsException localIndexOutOfBoundsException = new java/lang/IndexOutOfBoundsException;
    String str = String.valueOf(paramInt);
    localIndexOutOfBoundsException.<init>(str);
    throw localIndexOutOfBoundsException;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this == paramObject) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof aa;
    if (!bool2) {
      return false;
    }
    paramObject = (aa)paramObject;
    s locals1 = ((aa)paramObject).b();
    s locals2 = b();
    if (locals1 == locals2)
    {
      int j = ((aa)paramObject).d(0);
      int i = k;
      if (j == i) {
        return bool1;
      }
    }
    return false;
  }
  
  public int hashCode()
  {
    int i = (k + 459) * 27;
    int j = a().hashCode();
    return i + j;
  }
}

/* Location:
 * Qualified Name:     org.a.a.a.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */