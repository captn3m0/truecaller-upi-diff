package org.a.a.a;

import java.io.Serializable;
import org.a.a.a;
import org.a.a.c.g;
import org.a.a.e;
import org.a.a.w;
import org.a.a.x;
import org.a.a.y;

public abstract class i
  extends d
  implements Serializable, y
{
  private static final long serialVersionUID = 576586928732749278L;
  private volatile a a;
  private volatile long b;
  private volatile long c;
  
  protected i(w paramw, x paramx)
  {
    a locala = e.b(paramx);
    a = locala;
    long l1 = e.a(paramx);
    c = l1;
    long l2 = e.a(paramw);
    l1 = c;
    l2 = -l2;
    l2 = g.a(l1, l2);
    b = l2;
    l2 = b;
    l1 = c;
    boolean bool = l1 < l2;
    if (!bool) {
      return;
    }
    paramw = new java/lang/IllegalArgumentException;
    paramw.<init>("The end instant must be greater than the start instant");
    throw paramw;
  }
  
  public final a a()
  {
    return a;
  }
  
  public final long b()
  {
    return b;
  }
  
  public final long c()
  {
    return c;
  }
}

/* Location:
 * Qualified Name:     org.a.a.a.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */