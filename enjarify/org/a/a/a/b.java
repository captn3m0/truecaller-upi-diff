package org.a.a.a;

import org.a.a.d.h;
import org.a.a.w;

public abstract class b
  implements w
{
  public boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this == paramObject) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof w;
    if (!bool2) {
      return false;
    }
    paramObject = (w)paramObject;
    long l1 = a();
    long l2 = ((w)paramObject).a();
    boolean bool3 = l1 < l2;
    if (!bool3) {
      return bool1;
    }
    return false;
  }
  
  public int hashCode()
  {
    long l1 = a();
    long l2 = l1 >>> 32;
    return (int)(l1 ^ l2);
  }
  
  public String toString()
  {
    long l1 = a();
    StringBuffer localStringBuffer = new java/lang/StringBuffer;
    localStringBuffer.<init>();
    String str1 = "PT";
    localStringBuffer.append(str1);
    long l2 = 0L;
    boolean bool1 = l1 < l2;
    if (bool1)
    {
      bool2 = true;
    }
    else
    {
      bool2 = false;
      str1 = null;
    }
    h.a(localStringBuffer, l1);
    int k;
    for (;;)
    {
      int j = localStringBuffer.length();
      int i;
      if (bool2) {
        i = 7;
      } else {
        i = 6;
      }
      k = 3;
      if (j >= i) {
        break;
      }
      if (!bool2) {
        k = 2;
      }
      String str2 = "0";
      localStringBuffer.insert(k, str2);
    }
    l2 = 1000L;
    long l3 = l1 / l2 * l2;
    boolean bool2 = l3 < l1;
    int m;
    if (!bool2)
    {
      m = localStringBuffer.length() - k;
      localStringBuffer.setLength(m);
    }
    else
    {
      m = localStringBuffer.length() - k;
      String str3 = ".";
      localStringBuffer.insert(m, str3);
    }
    localStringBuffer.append('S');
    return localStringBuffer.toString();
  }
}

/* Location:
 * Qualified Name:     org.a.a.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */