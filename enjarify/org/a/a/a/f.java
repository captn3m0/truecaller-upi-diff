package org.a.a.a;

import org.a.a.aa;
import org.a.a.d.n;
import org.a.a.s;

public abstract class f
  implements aa
{
  public final int a(org.a.a.j paramj)
  {
    s locals = b();
    int i = locals.b(paramj);
    int j = -1;
    if (i == j) {
      return 0;
    }
    return d(i);
  }
  
  public final org.a.a.j c(int paramInt)
  {
    return bi[paramInt];
  }
  
  public final int[] c()
  {
    int i = d();
    int[] arrayOfInt = new int[i];
    int j = 0;
    for (;;)
    {
      int k = arrayOfInt.length;
      if (j >= k) {
        break;
      }
      k = d(j);
      arrayOfInt[j] = k;
      j += 1;
    }
    return arrayOfInt;
  }
  
  public final int d()
  {
    return bi.length;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this == paramObject) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof aa;
    if (!bool2) {
      return false;
    }
    paramObject = (aa)paramObject;
    int i = d();
    int j = ((aa)paramObject).d();
    if (i != j) {
      return false;
    }
    i = d();
    j = 0;
    while (j < i)
    {
      int k = d(j);
      int m = ((aa)paramObject).d(j);
      if (k == m)
      {
        org.a.a.j localj1 = c(j);
        org.a.a.j localj2 = ((aa)paramObject).c(j);
        if (localj1 == localj2)
        {
          j += 1;
          continue;
        }
      }
      return false;
    }
    return bool1;
  }
  
  public int hashCode()
  {
    int i = d();
    int j = 17;
    int k = 0;
    while (k < i)
    {
      j *= 27;
      int m = d(k);
      j = (j + m) * 27;
      org.a.a.j localj = c(k);
      m = localj.hashCode();
      j += m;
      k += 1;
    }
    return j;
  }
  
  public String toString()
  {
    return org.a.a.d.j.a().a(this);
  }
}

/* Location:
 * Qualified Name:     org.a.a.a.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */