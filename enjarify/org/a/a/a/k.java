package org.a.a.a;

import java.io.Serializable;
import org.a.a.a;
import org.a.a.aa;
import org.a.a.e;
import org.a.a.s;

public abstract class k
  extends f
  implements Serializable, aa
{
  private static final aa a;
  private static final long serialVersionUID = -2110953284060001145L;
  protected final s b;
  private final int[] c;
  
  static
  {
    k.1 local1 = new org/a/a/a/k$1;
    local1.<init>();
    a = local1;
  }
  
  protected k(long paramLong, s params, a parama)
  {
    params = e.a(params);
    parama = e.a(parama);
    b = params;
    int[] arrayOfInt = parama.a(this, paramLong);
    c = arrayOfInt;
  }
  
  protected k(int[] paramArrayOfInt, s params)
  {
    b = params;
    c = paramArrayOfInt;
  }
  
  public final s b()
  {
    return b;
  }
  
  public final int d(int paramInt)
  {
    return c[paramInt];
  }
}

/* Location:
 * Qualified Name:     org.a.a.a.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */