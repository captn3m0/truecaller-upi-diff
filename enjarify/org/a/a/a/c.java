package org.a.a.a;

import org.a.a.a;
import org.a.a.c.g;
import org.a.a.d.b;
import org.a.a.d.i.a;
import org.a.a.e;
import org.a.a.x;

public abstract class c
  implements x
{
  public final int a(x paramx)
  {
    if (this == paramx) {
      return 0;
    }
    long l1 = paramx.a();
    long l2 = a();
    boolean bool = l2 < l1;
    if (!bool) {
      return 0;
    }
    bool = l2 < l1;
    if (bool) {
      return -1;
    }
    return 1;
  }
  
  public final boolean b(x paramx)
  {
    long l = e.a(paramx);
    return c(l);
  }
  
  public final boolean c(long paramLong)
  {
    long l = a();
    boolean bool = l < paramLong;
    return bool;
  }
  
  public final boolean c(x paramx)
  {
    long l = e.a(paramx);
    return d(l);
  }
  
  public final boolean d(long paramLong)
  {
    long l = a();
    boolean bool = l < paramLong;
    return bool;
  }
  
  public final boolean d(x paramx)
  {
    long l = e.a(paramx);
    return e(l);
  }
  
  public final boolean e(long paramLong)
  {
    long l = a();
    boolean bool = l < paramLong;
    return !bool;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this == paramObject) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof x;
    if (!bool2) {
      return false;
    }
    paramObject = (x)paramObject;
    long l1 = a();
    long l2 = ((x)paramObject).a();
    bool2 = l1 < l2;
    if (!bool2)
    {
      a locala = b();
      paramObject = ((x)paramObject).b();
      boolean bool3 = g.a(locala, paramObject);
      if (bool3) {
        return bool1;
      }
    }
    return false;
  }
  
  public int hashCode()
  {
    long l1 = a();
    long l2 = a() >>> 32;
    int i = (int)(l1 ^ l2);
    int j = b().hashCode();
    return i + j;
  }
  
  public String toString()
  {
    return i.a.b().a(this);
  }
}

/* Location:
 * Qualified Name:     org.a.a.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */