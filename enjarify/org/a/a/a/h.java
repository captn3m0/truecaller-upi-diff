package org.a.a.a;

import java.io.Serializable;
import org.a.a.c.g;
import org.a.a.e;
import org.a.a.w;
import org.a.a.x;

public abstract class h
  extends b
  implements Serializable, w
{
  private static final long serialVersionUID = 2581698638990L;
  public volatile long b;
  
  protected h(long paramLong)
  {
    b = paramLong;
  }
  
  protected h(x paramx1, x paramx2)
  {
    if (paramx1 == paramx2)
    {
      b = 0L;
      return;
    }
    long l1 = e.a(paramx1);
    long l2 = g.b(e.a(paramx2), l1);
    b = l2;
  }
  
  public final long a()
  {
    return b;
  }
}

/* Location:
 * Qualified Name:     org.a.a.a.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */