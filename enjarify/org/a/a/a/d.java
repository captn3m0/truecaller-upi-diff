package org.a.a.a;

import org.a.a.a;
import org.a.a.c.g;
import org.a.a.d.b;
import org.a.a.d.i.a;
import org.a.a.e;
import org.a.a.x;
import org.a.a.y;

public abstract class d
  implements y
{
  private boolean a(long paramLong)
  {
    long l1 = b();
    long l2 = c();
    boolean bool1 = paramLong < l1;
    if (!bool1)
    {
      boolean bool2 = paramLong < l2;
      if (bool2) {
        return true;
      }
    }
    return false;
  }
  
  public final boolean a(x paramx)
  {
    if (paramx == null)
    {
      l = e.a();
      return a(l);
    }
    long l = paramx.a();
    return a(l);
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this == paramObject) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof y;
    if (!bool2) {
      return false;
    }
    paramObject = (y)paramObject;
    long l1 = b();
    long l2 = ((y)paramObject).b();
    bool2 = l1 < l2;
    if (!bool2)
    {
      l1 = c();
      l2 = ((y)paramObject).c();
      bool2 = l1 < l2;
      if (!bool2)
      {
        a locala = a();
        paramObject = ((y)paramObject).a();
        boolean bool3 = g.a(locala, paramObject);
        if (bool3) {
          return bool1;
        }
      }
    }
    return false;
  }
  
  public int hashCode()
  {
    long l1 = b();
    long l2 = c();
    int i = 32;
    long l3 = l1 >>> i;
    int j = ((int)(l1 ^ l3) + 3007) * 31;
    long l4 = l2 >>> i;
    int k = (int)(l2 ^ l4);
    j = (j + k) * 31;
    k = a().hashCode();
    return j + k;
  }
  
  public String toString()
  {
    b localb = i.a.b();
    Object localObject = a();
    localb = localb.a((a)localObject);
    localObject = new java/lang/StringBuffer;
    ((StringBuffer)localObject).<init>(48);
    long l = b();
    localb.a((StringBuffer)localObject, l);
    ((StringBuffer)localObject).append('/');
    l = c();
    localb.a((StringBuffer)localObject, l);
    return ((StringBuffer)localObject).toString();
  }
}

/* Location:
 * Qualified Name:     org.a.a.a.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */