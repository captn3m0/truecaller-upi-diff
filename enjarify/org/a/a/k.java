package org.a.a;

public final class k
  extends IllegalArgumentException
{
  private static final long serialVersionUID = 6305711765985447737L;
  public String a;
  private final d b;
  private final j c;
  private final String d;
  private final Number e;
  private final String f;
  private final Number g;
  private final Number h;
  
  public k(d paramd, Number paramNumber1, Number paramNumber2, Number paramNumber3)
  {
    super(str);
    b = paramd;
    c = null;
    paramd = x;
    d = paramd;
    e = paramNumber1;
    f = null;
    g = paramNumber2;
    h = paramNumber3;
    paramd = super.getMessage();
    a = paramd;
  }
  
  public k(d paramd, Number paramNumber, String paramString)
  {
    super(paramString);
    b = paramd;
    c = null;
    paramd = x;
    d = paramd;
    e = paramNumber;
    f = null;
    g = null;
    h = null;
    paramd = super.getMessage();
    a = paramd;
  }
  
  public k(d paramd, String paramString)
  {
    super(str1);
    b = paramd;
    c = null;
    paramd = x;
    d = paramd;
    f = paramString;
    e = null;
    g = null;
    h = null;
    paramd = super.getMessage();
    a = paramd;
  }
  
  private static String a(String paramString1, Number paramNumber1, Number paramNumber2, Number paramNumber3, String paramString2)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    String str = "Value ";
    localStringBuilder.<init>(str);
    localStringBuilder.append(paramNumber1);
    paramNumber1 = " for ";
    localStringBuilder.append(paramNumber1);
    localStringBuilder.append(paramString1);
    char c1 = ' ';
    localStringBuilder.append(c1);
    if (paramNumber2 == null)
    {
      if (paramNumber3 == null)
      {
        paramString1 = "is not supported";
        localStringBuilder.append(paramString1);
      }
      else
      {
        paramString1 = "must not be larger than ";
        localStringBuilder.append(paramString1);
        localStringBuilder.append(paramNumber3);
      }
    }
    else if (paramNumber3 == null)
    {
      paramString1 = "must not be smaller than ";
      localStringBuilder.append(paramString1);
      localStringBuilder.append(paramNumber2);
    }
    else
    {
      paramString1 = "must be in the range [";
      localStringBuilder.append(paramString1);
      localStringBuilder.append(paramNumber2);
      localStringBuilder.append(',');
      localStringBuilder.append(paramNumber3);
      c1 = ']';
      localStringBuilder.append(c1);
    }
    if (paramString2 != null)
    {
      paramString1 = ": ";
      localStringBuilder.append(paramString1);
      localStringBuilder.append(paramString2);
    }
    return localStringBuilder.toString();
  }
  
  public final String getMessage()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     org.a.a.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */