package org.a.a;

import java.io.Serializable;
import org.a.a.a.k;
import org.a.a.b.q;
import org.a.a.c.g;

public final class r
  extends k
  implements Serializable, aa
{
  public static final r a;
  private static final long serialVersionUID = 741052353876488155L;
  
  static
  {
    r localr = new org/a/a/r;
    localr.<init>();
    a = localr;
  }
  
  public r()
  {
    super(0L, null, null);
  }
  
  private r(long paramLong, s params, a parama)
  {
    super(paramLong, params, parama);
  }
  
  private r(int[] paramArrayOfInt, s params)
  {
    super(paramArrayOfInt, params);
  }
  
  public static r a()
  {
    r localr = new org/a/a/r;
    int[] arrayOfInt = new int[8];
    int[] tmp10_9 = arrayOfInt;
    int[] tmp11_10 = tmp10_9;
    int[] tmp11_10 = tmp10_9;
    tmp11_10[0] = 0;
    tmp11_10[1] = 0;
    int[] tmp18_11 = tmp11_10;
    int[] tmp18_11 = tmp11_10;
    tmp18_11[2] = 0;
    tmp18_11[3] = 0;
    int[] tmp25_18 = tmp18_11;
    int[] tmp25_18 = tmp18_11;
    tmp25_18[4] = 10;
    tmp25_18[5] = 0;
    tmp25_18[6] = 0;
    tmp25_18[7] = 0;
    s locals = s.a();
    localr.<init>(arrayOfInt, locals);
    return localr;
  }
  
  public static r a(int paramInt)
  {
    r localr = new org/a/a/r;
    int[] arrayOfInt = new int[8];
    arrayOfInt[0] = 0;
    arrayOfInt[1] = 0;
    arrayOfInt[2] = 0;
    arrayOfInt[3] = 0;
    arrayOfInt[4] = 0;
    arrayOfInt[5] = 0;
    arrayOfInt[6] = paramInt;
    arrayOfInt[7] = 0;
    s locals = s.a();
    localr.<init>(arrayOfInt, locals);
    return localr;
  }
  
  public static r b(int paramInt)
  {
    r localr = new org/a/a/r;
    int[] arrayOfInt = new int[8];
    arrayOfInt[0] = 0;
    arrayOfInt[1] = 0;
    arrayOfInt[2] = 0;
    arrayOfInt[3] = 0;
    arrayOfInt[4] = 0;
    arrayOfInt[5] = 0;
    arrayOfInt[6] = 0;
    arrayOfInt[7] = paramInt;
    s locals = s.a();
    localr.<init>(arrayOfInt, locals);
    return localr;
  }
  
  private int e()
  {
    s locals = b;
    int i = s.a;
    return locals.a(this, i);
  }
  
  private r e(int paramInt)
  {
    int[] arrayOfInt = c();
    s locals = b;
    int i = s.a;
    locals.a(i, arrayOfInt, paramInt);
    r localr = new org/a/a/r;
    locals = b;
    localr.<init>(arrayOfInt, locals);
    return localr;
  }
  
  private int f()
  {
    s locals = b;
    int i = s.b;
    return locals.a(this, i);
  }
  
  private r f(int paramInt)
  {
    int[] arrayOfInt = c();
    s locals = b;
    int i = s.b;
    locals.a(i, arrayOfInt, paramInt);
    r localr = new org/a/a/r;
    locals = b;
    localr.<init>(arrayOfInt, locals);
    return localr;
  }
  
  private int g()
  {
    s locals = b;
    int i = s.c;
    return locals.a(this, i);
  }
  
  private int h()
  {
    s locals = b;
    int i = s.d;
    return locals.a(this, i);
  }
  
  private int i()
  {
    s locals = b;
    int i = s.e;
    return locals.a(this, i);
  }
  
  private int j()
  {
    s locals = b;
    int i = s.f;
    return locals.a(this, i);
  }
  
  private int k()
  {
    s locals = b;
    int i = s.g;
    return locals.a(this, i);
  }
  
  private int l()
  {
    s locals = b;
    int i = s.h;
    return locals.a(this, i);
  }
  
  public final r a(s params)
  {
    params = e.a(params);
    long l1 = l();
    long l2 = k() * 1000L;
    l1 += l2;
    l2 = j() * 60000L;
    l1 += l2;
    l2 = i() * 3600000L;
    l1 += l2;
    l2 = h() * 86400000L;
    l1 += l2;
    int i = g();
    l2 = i;
    long l3 = 604800000L;
    l2 *= l3;
    l1 += l2;
    Object localObject1 = new org/a/a/r;
    q localq = q.L();
    ((r)localObject1).<init>(l1, params, localq);
    int j = e();
    int m = f();
    if ((j != 0) || (m != 0))
    {
      long l4 = j;
      long l5 = 12;
      l4 *= l5;
      l1 = m;
      l4 += l1;
      localObject2 = j.d;
      boolean bool1 = params.a((j)localObject2);
      if (bool1)
      {
        int k = g.a(l4 / l5);
        localObject1 = ((r)localObject1).e(k);
        k *= 12;
        l1 = k;
        l4 -= l1;
      }
      localObject2 = j.e;
      boolean bool2 = params.a((j)localObject2);
      if (bool2)
      {
        int n = g.a(l4);
        localObject2 = ((r)localObject1).f(n);
        long l6 = n;
        l4 -= l6;
        localObject1 = localObject2;
      }
      l1 = 0L;
      boolean bool3 = l4 < l1;
      if (bool3) {}
    }
    else
    {
      return (r)localObject1;
    }
    params = new java/lang/UnsupportedOperationException;
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("Unable to normalize as PeriodType is missing either years or months but period has a month/year amount: ");
    String str = toString();
    ((StringBuilder)localObject2).append(str);
    localObject2 = ((StringBuilder)localObject2).toString();
    params.<init>((String)localObject2);
    throw params;
  }
}

/* Location:
 * Qualified Name:     org.a.a.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */