package org.a.a;

final class d$a
  extends d
{
  private static final long serialVersionUID = 271537018459014L;
  private final transient j A;
  private final byte y;
  private final transient j z;
  
  d$a(String paramString, byte paramByte, j paramj1, j paramj2)
  {
    super(paramString);
    y = paramByte;
    z = paramj1;
    A = paramj2;
  }
  
  private Object readResolve()
  {
    int i = y;
    switch (i)
    {
    default: 
      return this;
    case 23: 
      return d.w;
    case 22: 
      return d.v;
    case 21: 
      return d.u;
    case 20: 
      return d.t;
    case 19: 
      return d.s;
    case 18: 
      return d.r;
    case 17: 
      return d.q;
    case 16: 
      return d.p;
    case 15: 
      return d.o;
    case 14: 
      return d.n;
    case 13: 
      return d.m;
    case 12: 
      return d.l;
    case 11: 
      return d.k;
    case 10: 
      return d.j;
    case 9: 
      return d.i;
    case 8: 
      return d.h;
    case 7: 
      return d.g;
    case 6: 
      return d.f;
    case 5: 
      return d.e;
    case 4: 
      return d.d;
    case 3: 
      return d.c;
    case 2: 
      return d.b;
    }
    return d.a;
  }
  
  public final c a(a parama)
  {
    parama = e.a(parama);
    int i = y;
    switch (i)
    {
    default: 
      parama = new java/lang/InternalError;
      parama.<init>();
      throw parama;
    case 23: 
      return parama.d();
    case 22: 
      return parama.e();
    case 21: 
      return parama.g();
    case 20: 
      return parama.h();
    case 19: 
      return parama.j();
    case 18: 
      return parama.k();
    case 17: 
      return parama.m();
    case 16: 
      return parama.n();
    case 15: 
      return parama.q();
    case 14: 
      return parama.p();
    case 13: 
      return parama.r();
    case 12: 
      return parama.t();
    case 11: 
      return parama.x();
    case 10: 
      return parama.z();
    case 9: 
      return parama.A();
    case 8: 
      return parama.u();
    case 7: 
      return parama.C();
    case 6: 
      return parama.v();
    case 5: 
      return parama.E();
    case 4: 
      return parama.G();
    case 3: 
      return parama.I();
    case 2: 
      return parama.F();
    }
    return parama.K();
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this == paramObject) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof a;
    if (bool2)
    {
      int i = y;
      paramObject = (a)paramObject;
      int j = y;
      if (i == j) {
        return bool1;
      }
      return false;
    }
    return false;
  }
  
  public final int hashCode()
  {
    int i = y;
    return 1 << i;
  }
  
  public final j x()
  {
    return z;
  }
}

/* Location:
 * Qualified Name:     org.a.a.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */