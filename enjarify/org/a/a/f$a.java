package org.a.a;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.a.a.d.b;
import org.a.a.d.c;

final class f$a
{
  static final Map a;
  static final b b;
  
  static
  {
    Object localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    ((Map)localObject).put("GMT", "UTC");
    ((Map)localObject).put("WET", "WET");
    ((Map)localObject).put("CET", "CET");
    ((Map)localObject).put("MET", "CET");
    ((Map)localObject).put("ECT", "CET");
    ((Map)localObject).put("EET", "EET");
    ((Map)localObject).put("MIT", "Pacific/Apia");
    ((Map)localObject).put("HST", "Pacific/Honolulu");
    ((Map)localObject).put("AST", "America/Anchorage");
    ((Map)localObject).put("PST", "America/Los_Angeles");
    ((Map)localObject).put("MST", "America/Denver");
    ((Map)localObject).put("PNT", "America/Phoenix");
    ((Map)localObject).put("CST", "America/Chicago");
    ((Map)localObject).put("EST", "America/New_York");
    ((Map)localObject).put("IET", "America/Indiana/Indianapolis");
    ((Map)localObject).put("PRT", "America/Puerto_Rico");
    ((Map)localObject).put("CNT", "America/St_Johns");
    ((Map)localObject).put("AGT", "America/Argentina/Buenos_Aires");
    ((Map)localObject).put("BET", "America/Sao_Paulo");
    ((Map)localObject).put("ART", "Africa/Cairo");
    ((Map)localObject).put("CAT", "Africa/Harare");
    ((Map)localObject).put("EAT", "Africa/Addis_Ababa");
    ((Map)localObject).put("NET", "Asia/Yerevan");
    ((Map)localObject).put("PLT", "Asia/Karachi");
    ((Map)localObject).put("IST", "Asia/Kolkata");
    ((Map)localObject).put("BST", "Asia/Dhaka");
    ((Map)localObject).put("VST", "Asia/Ho_Chi_Minh");
    ((Map)localObject).put("CTT", "Asia/Shanghai");
    ((Map)localObject).put("JST", "Asia/Tokyo");
    ((Map)localObject).put("ACT", "Australia/Darwin");
    ((Map)localObject).put("AET", "Australia/Sydney");
    ((Map)localObject).put("SST", "Pacific/Guadalcanal");
    ((Map)localObject).put("NST", "Pacific/Auckland");
    a = Collections.unmodifiableMap((Map)localObject);
    localObject = new org/a/a/f$a$1;
    ((f.a.1)localObject).<init>();
    c localc = new org/a/a/d/c;
    localc.<init>();
    b = localc.a(null, true, 4).a().a((a)localObject);
  }
}

/* Location:
 * Qualified Name:     org.a.a.f.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */