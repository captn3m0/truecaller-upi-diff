package org.a.a;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import org.a.a.b.q;
import org.a.a.d.i.a;

public final class p
  extends org.a.a.a.j
  implements Serializable, z
{
  private static final Set c;
  private static final long serialVersionUID = 272699618552757L;
  public final long a;
  public final a b;
  private transient int d;
  
  static
  {
    Object localObject = new java/util/HashSet;
    ((HashSet)localObject).<init>();
    c = (Set)localObject;
    j localj = j.f();
    ((Set)localObject).add(localj);
    localObject = c;
    localj = j.g();
    ((Set)localObject).add(localj);
    localObject = c;
    localj = j.i();
    ((Set)localObject).add(localj);
    localObject = c;
    localj = j.h();
    ((Set)localObject).add(localj);
    localObject = c;
    localj = j.j();
    ((Set)localObject).add(localj);
    localObject = c;
    localj = j.k();
    ((Set)localObject).add(localj);
    localObject = c;
    localj = j.l();
    ((Set)localObject).add(localj);
  }
  
  public p()
  {
    this(l, localq);
  }
  
  public p(long paramLong)
  {
    this(paramLong, localq);
  }
  
  public p(long paramLong, a parama)
  {
    parama = e.a(parama);
    f localf1 = parama.a();
    f localf2 = f.a;
    if (localf2 == null) {
      localf2 = f.a();
    }
    if (localf2 != localf1)
    {
      long l = localf1.f(paramLong);
      paramLong = localf2.a(l, paramLong);
    }
    parama = parama.b();
    paramLong = parama.u().d(paramLong);
    a = paramLong;
    b = parama;
  }
  
  public static p a()
  {
    p localp = new org/a/a/p;
    localp.<init>();
    return localp;
  }
  
  private Object readResolve()
  {
    Object localObject1 = b;
    long l;
    Object localObject2;
    if (localObject1 == null)
    {
      localObject1 = new org/a/a/p;
      l = a;
      localObject2 = q.L();
      ((p)localObject1).<init>(l, (a)localObject2);
      return localObject1;
    }
    localObject1 = f.a;
    f localf = b.a();
    boolean bool = ((f)localObject1).equals(localf);
    if (!bool)
    {
      localObject1 = new org/a/a/p;
      l = a;
      localObject2 = b.b();
      ((p)localObject1).<init>(l, (a)localObject2);
      return localObject1;
    }
    return this;
  }
  
  public final int a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      IndexOutOfBoundsException localIndexOutOfBoundsException = new java/lang/IndexOutOfBoundsException;
      localObject = String.valueOf(paramInt);
      localObject = "Invalid index: ".concat((String)localObject);
      localIndexOutOfBoundsException.<init>((String)localObject);
      throw localIndexOutOfBoundsException;
    case 2: 
      localObject = b.u();
      l = a;
      return ((c)localObject).a(l);
    case 1: 
      localObject = b.C();
      l = a;
      return ((c)localObject).a(l);
    }
    Object localObject = b.E();
    long l = a;
    return ((c)localObject).a(l);
  }
  
  public final int a(d paramd)
  {
    if (paramd != null)
    {
      boolean bool = b(paramd);
      if (bool)
      {
        localObject = b;
        paramd = paramd.a((a)localObject);
        long l = a;
        return paramd.a(l);
      }
      Object localObject = new java/lang/IllegalArgumentException;
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>("Field '");
      localStringBuilder.append(paramd);
      localStringBuilder.append("' is not supported");
      paramd = localStringBuilder.toString();
      ((IllegalArgumentException)localObject).<init>(paramd);
      throw ((Throwable)localObject);
    }
    paramd = new java/lang/IllegalArgumentException;
    paramd.<init>("The DateTimeFieldType must not be null");
    throw paramd;
  }
  
  public final int a(z paramz)
  {
    if (this == paramz) {
      return 0;
    }
    boolean bool1 = paramz instanceof p;
    if (bool1)
    {
      Object localObject = paramz;
      localObject = (p)paramz;
      a locala1 = b;
      a locala2 = b;
      boolean bool2 = locala1.equals(locala2);
      if (bool2)
      {
        long l1 = a;
        long l2 = a;
        boolean bool3 = l1 < l2;
        if (bool3) {
          return -1;
        }
        bool3 = l1 < l2;
        if (!bool3) {
          return 0;
        }
        return 1;
      }
    }
    return super.a((z)paramz);
  }
  
  public final b a(f paramf)
  {
    paramf = e.a(paramf);
    a locala = b.a(paramf);
    long l = a + 21600000L;
    l = paramf.g(l);
    l = locala.u().d(l);
    paramf = new org/a/a/b;
    paramf.<init>(l, locala);
    return paramf;
  }
  
  public final c a(int paramInt, a parama)
  {
    switch (paramInt)
    {
    default: 
      parama = new java/lang/IndexOutOfBoundsException;
      String str = String.valueOf(paramInt);
      str = "Invalid index: ".concat(str);
      parama.<init>(str);
      throw parama;
    case 2: 
      return parama.u();
    case 1: 
      return parama.C();
    }
    return parama.E();
  }
  
  public final a b()
  {
    return b;
  }
  
  public final boolean b(d paramd)
  {
    a locala = null;
    if (paramd == null) {
      return false;
    }
    Object localObject1 = paramd.x();
    Object localObject2 = c;
    boolean bool1 = ((Set)localObject2).contains(localObject1);
    if (!bool1)
    {
      localObject2 = b;
      localObject1 = ((j)localObject1).a((a)localObject2);
      long l1 = ((i)localObject1).d();
      i locali = b.s();
      long l2 = locali.d();
      boolean bool2 = l1 < l2;
      if (bool2) {
        return false;
      }
    }
    locala = b;
    return paramd.a(locala).c();
  }
  
  public final int c()
  {
    c localc = b.E();
    long l = a;
    return localc.a(l);
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this == paramObject) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof p;
    if (bool2)
    {
      Object localObject = paramObject;
      localObject = (p)paramObject;
      a locala1 = b;
      a locala2 = b;
      boolean bool3 = locala1.equals(locala2);
      if (bool3)
      {
        long l1 = a;
        long l2 = a;
        boolean bool4 = l1 < l2;
        if (!bool4) {
          return bool1;
        }
        return false;
      }
    }
    return super.equals(paramObject);
  }
  
  public final int hashCode()
  {
    int i = d;
    if (i == 0)
    {
      i = super.hashCode();
      d = i;
    }
    return i;
  }
  
  public final String toString()
  {
    return i.a.d().a(this);
  }
}

/* Location:
 * Qualified Name:     org.a.a.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */