package org.a.a;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

final class f$b
  implements Serializable
{
  private static final long serialVersionUID = -6471952376487863581L;
  private transient String a;
  
  f$b(String paramString)
  {
    a = paramString;
  }
  
  private void readObject(ObjectInputStream paramObjectInputStream)
  {
    paramObjectInputStream = paramObjectInputStream.readUTF();
    a = paramObjectInputStream;
  }
  
  private Object readResolve()
  {
    return f.a(a);
  }
  
  private void writeObject(ObjectOutputStream paramObjectOutputStream)
  {
    String str = a;
    paramObjectOutputStream.writeUTF(str);
  }
}

/* Location:
 * Qualified Name:     org.a.a.f.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */