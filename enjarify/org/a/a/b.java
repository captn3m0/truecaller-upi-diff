package org.a.a;

import java.io.Serializable;
import java.util.Locale;
import org.a.a.a.g;
import org.a.a.d.i.a;
import org.a.a.d.k;
import org.a.a.d.m;

public final class b
  extends g
  implements Serializable, v
{
  private static final long serialVersionUID = -5171125899451703815L;
  
  public b() {}
  
  public b(long paramLong)
  {
    super(paramLong);
  }
  
  public b(long paramLong, a parama)
  {
    super(paramLong, parama);
  }
  
  public b(f paramf)
  {
    super(paramf, (byte)0);
  }
  
  public static b a(String paramString)
  {
    Object localObject = i.a.a();
    boolean bool1 = d;
    boolean bool2 = true;
    if (bool1 != bool2)
    {
      org.a.a.d.b localb = new org/a/a/d/b;
      m localm = a;
      k localk = b;
      Locale localLocale = c;
      boolean bool3 = true;
      a locala = e;
      Integer localInteger = f;
      int i = g;
      localb.<init>(localm, localk, localLocale, bool3, locala, null, localInteger, i);
      localObject = localb;
    }
    return ((org.a.a.d.b)localObject).b(paramString);
  }
  
  public static b ay_()
  {
    b localb = new org/a/a/b;
    localb.<init>();
    return localb;
  }
  
  public final b a(int paramInt)
  {
    if (paramInt == 0) {
      return this;
    }
    i locali = b.s();
    long l1 = a;
    long l2 = locali.a(l1, paramInt);
    return a_(l2);
  }
  
  public final b a(f paramf)
  {
    paramf = e.a(b.a(paramf));
    Object localObject = b;
    if (paramf == localObject) {
      return this;
    }
    localObject = new org/a/a/b;
    long l = a;
    ((b)localObject).<init>(l, paramf);
    return (b)localObject;
  }
  
  public final b a_(long paramLong)
  {
    long l = a;
    boolean bool = paramLong < l;
    if (!bool) {
      return this;
    }
    b localb = new org/a/a/b;
    a locala = b;
    localb.<init>(paramLong, locala);
    return localb;
  }
  
  public final b az_()
  {
    p localp = e();
    f localf = b().a();
    return localp.a(localf);
  }
  
  public final b b(int paramInt)
  {
    if (paramInt == 0) {
      return this;
    }
    i locali = b.l();
    long l1 = a;
    long l2 = locali.a(l1, paramInt);
    return a_(l2);
  }
  
  public final b b(long paramLong)
  {
    long l1 = 0L;
    boolean bool = paramLong < l1;
    if (!bool) {
      return this;
    }
    a locala = b;
    long l2 = a;
    paramLong = locala.a(l2, paramLong);
    return a_(paramLong);
  }
  
  public final b c()
  {
    i locali = b.c();
    long l1 = a;
    long l2 = locali.a(l1, 10000);
    return a_(l2);
  }
  
  public final b c(int paramInt)
  {
    if (paramInt == 0) {
      return this;
    }
    i locali = b.s();
    long l1 = a;
    long l2 = locali.b(l1, paramInt);
    return a_(l2);
  }
  
  public final b d()
  {
    i locali = b.B();
    long l1 = a;
    long l2 = locali.b(l1, 1);
    return a_(l2);
  }
  
  public final p e()
  {
    p localp = new org/a/a/p;
    long l = a;
    a locala = b;
    localp.<init>(l, locala);
    return localp;
  }
  
  public final b f()
  {
    c localc = b.d();
    long l1 = a;
    long l2 = localc.b(l1, 0);
    return a_(l2);
  }
}

/* Location:
 * Qualified Name:     org.a.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */