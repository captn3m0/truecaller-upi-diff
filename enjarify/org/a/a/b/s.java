package org.a.a.b;

import java.util.HashMap;
import org.a.a.c;
import org.a.a.f;
import org.a.a.i;
import org.a.a.l;

public final class s
  extends a
{
  private static final long serialVersionUID = -1079258847191166848L;
  
  private s(org.a.a.a parama, f paramf)
  {
    super(parama, paramf);
  }
  
  public static s a(org.a.a.a parama, f paramf)
  {
    if (parama != null)
    {
      parama = parama.b();
      if (parama != null)
      {
        if (paramf != null)
        {
          s locals = new org/a/a/b/s;
          locals.<init>(parama, paramf);
          return locals;
        }
        parama = new java/lang/IllegalArgumentException;
        parama.<init>("DateTimeZone must not be null");
        throw parama;
      }
      parama = new java/lang/IllegalArgumentException;
      parama.<init>("UTC chronology must not be null");
      throw parama;
    }
    parama = new java/lang/IllegalArgumentException;
    parama.<init>("Must supply a chronology");
    throw parama;
  }
  
  private c a(c paramc, HashMap paramHashMap)
  {
    if (paramc != null)
    {
      boolean bool = paramc.c();
      if (bool)
      {
        bool = paramHashMap.containsKey(paramc);
        if (bool) {
          return (c)paramHashMap.get(paramc);
        }
        s.a locala = new org/a/a/b/s$a;
        Object localObject1 = b;
        Object localObject2 = localObject1;
        localObject2 = (f)localObject1;
        localObject1 = paramc.d();
        i locali1 = a((i)localObject1, paramHashMap);
        localObject1 = paramc.e();
        i locali2 = a((i)localObject1, paramHashMap);
        localObject1 = paramc.f();
        i locali3 = a((i)localObject1, paramHashMap);
        localObject1 = locala;
        locala.<init>(paramc, (f)localObject2, locali1, locali2, locali3);
        paramHashMap.put(paramc, locala);
        return locala;
      }
    }
    return paramc;
  }
  
  private i a(i parami, HashMap paramHashMap)
  {
    if (parami != null)
    {
      boolean bool = parami.b();
      if (bool)
      {
        bool = paramHashMap.containsKey(parami);
        if (bool) {
          return (i)paramHashMap.get(parami);
        }
        s.b localb = new org/a/a/b/s$b;
        f localf = (f)b;
        localb.<init>(parami, localf);
        paramHashMap.put(parami, localb);
        return localb;
      }
    }
    return parami;
  }
  
  static boolean a(i parami)
  {
    if (parami != null)
    {
      long l1 = parami.d();
      long l2 = 43200000L;
      boolean bool = l1 < l2;
      if (bool) {
        return true;
      }
    }
    return false;
  }
  
  public final long a(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7)
  {
    org.a.a.a locala = a;
    int i = paramInt3;
    int j = paramInt7;
    long l1 = locala.a(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7);
    long l2 = Long.MAX_VALUE;
    paramInt5 = l1 < l2;
    if (paramInt5 == 0) {
      return l2;
    }
    long l3 = Long.MIN_VALUE;
    paramInt7 = l1 < l3;
    if (paramInt7 == 0) {
      return l3;
    }
    f localf = (f)b;
    int k = localf.e(l1);
    long l4 = k;
    l4 = l1 - l4;
    long l5 = 604800000L;
    long l6 = 0L;
    boolean bool2 = l1 < l5;
    if (bool2)
    {
      bool1 = l4 < l6;
      if (bool1) {
        return l2;
      }
    }
    l2 = 3690167296L;
    boolean bool1 = l1 < l2;
    if (bool1)
    {
      paramInt3 = l4 < l6;
      if (paramInt3 > 0) {
        return l3;
      }
    }
    paramInt3 = localf.b(l4);
    if (k == paramInt3) {
      return l4;
    }
    l locall = new org/a/a/l;
    String str = b;
    locall.<init>(l1, str);
    throw locall;
  }
  
  public final org.a.a.a a(f paramf)
  {
    if (paramf == null) {
      paramf = f.a();
    }
    Object localObject = b;
    if (paramf == localObject) {
      return this;
    }
    localObject = f.a;
    if (paramf == localObject) {
      return a;
    }
    localObject = new org/a/a/b/s;
    org.a.a.a locala = a;
    ((s)localObject).<init>(locala, paramf);
    return (org.a.a.a)localObject;
  }
  
  public final f a()
  {
    return (f)b;
  }
  
  protected final void a(a.a parama)
  {
    Object localObject1 = new java/util/HashMap;
    ((HashMap)localObject1).<init>();
    Object localObject2 = l;
    localObject2 = a((i)localObject2, (HashMap)localObject1);
    l = ((i)localObject2);
    localObject2 = k;
    localObject2 = a((i)localObject2, (HashMap)localObject1);
    k = ((i)localObject2);
    localObject2 = j;
    localObject2 = a((i)localObject2, (HashMap)localObject1);
    j = ((i)localObject2);
    localObject2 = i;
    localObject2 = a((i)localObject2, (HashMap)localObject1);
    i = ((i)localObject2);
    localObject2 = h;
    localObject2 = a((i)localObject2, (HashMap)localObject1);
    h = ((i)localObject2);
    localObject2 = g;
    localObject2 = a((i)localObject2, (HashMap)localObject1);
    g = ((i)localObject2);
    localObject2 = f;
    localObject2 = a((i)localObject2, (HashMap)localObject1);
    f = ((i)localObject2);
    localObject2 = e;
    localObject2 = a((i)localObject2, (HashMap)localObject1);
    e = ((i)localObject2);
    localObject2 = d;
    localObject2 = a((i)localObject2, (HashMap)localObject1);
    d = ((i)localObject2);
    localObject2 = c;
    localObject2 = a((i)localObject2, (HashMap)localObject1);
    c = ((i)localObject2);
    localObject2 = b;
    localObject2 = a((i)localObject2, (HashMap)localObject1);
    b = ((i)localObject2);
    localObject2 = a;
    localObject2 = a((i)localObject2, (HashMap)localObject1);
    a = ((i)localObject2);
    localObject2 = E;
    localObject2 = a((c)localObject2, (HashMap)localObject1);
    E = ((c)localObject2);
    localObject2 = F;
    localObject2 = a((c)localObject2, (HashMap)localObject1);
    F = ((c)localObject2);
    localObject2 = G;
    localObject2 = a((c)localObject2, (HashMap)localObject1);
    G = ((c)localObject2);
    localObject2 = H;
    localObject2 = a((c)localObject2, (HashMap)localObject1);
    H = ((c)localObject2);
    localObject2 = I;
    localObject2 = a((c)localObject2, (HashMap)localObject1);
    I = ((c)localObject2);
    localObject2 = x;
    localObject2 = a((c)localObject2, (HashMap)localObject1);
    x = ((c)localObject2);
    localObject2 = y;
    localObject2 = a((c)localObject2, (HashMap)localObject1);
    y = ((c)localObject2);
    localObject2 = z;
    localObject2 = a((c)localObject2, (HashMap)localObject1);
    z = ((c)localObject2);
    localObject2 = D;
    localObject2 = a((c)localObject2, (HashMap)localObject1);
    D = ((c)localObject2);
    localObject2 = A;
    localObject2 = a((c)localObject2, (HashMap)localObject1);
    A = ((c)localObject2);
    localObject2 = B;
    localObject2 = a((c)localObject2, (HashMap)localObject1);
    B = ((c)localObject2);
    localObject2 = C;
    localObject2 = a((c)localObject2, (HashMap)localObject1);
    C = ((c)localObject2);
    localObject2 = m;
    localObject2 = a((c)localObject2, (HashMap)localObject1);
    m = ((c)localObject2);
    localObject2 = n;
    localObject2 = a((c)localObject2, (HashMap)localObject1);
    n = ((c)localObject2);
    localObject2 = o;
    localObject2 = a((c)localObject2, (HashMap)localObject1);
    o = ((c)localObject2);
    localObject2 = p;
    localObject2 = a((c)localObject2, (HashMap)localObject1);
    p = ((c)localObject2);
    localObject2 = q;
    localObject2 = a((c)localObject2, (HashMap)localObject1);
    q = ((c)localObject2);
    localObject2 = r;
    localObject2 = a((c)localObject2, (HashMap)localObject1);
    r = ((c)localObject2);
    localObject2 = s;
    localObject2 = a((c)localObject2, (HashMap)localObject1);
    s = ((c)localObject2);
    localObject2 = u;
    localObject2 = a((c)localObject2, (HashMap)localObject1);
    u = ((c)localObject2);
    localObject2 = t;
    localObject2 = a((c)localObject2, (HashMap)localObject1);
    t = ((c)localObject2);
    localObject2 = v;
    localObject2 = a((c)localObject2, (HashMap)localObject1);
    v = ((c)localObject2);
    localObject2 = w;
    localObject1 = a((c)localObject2, (HashMap)localObject1);
    w = ((c)localObject1);
  }
  
  public final org.a.a.a b()
  {
    return a;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this == paramObject) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof s;
    if (!bool2) {
      return false;
    }
    paramObject = (s)paramObject;
    Object localObject = a;
    org.a.a.a locala = a;
    bool2 = localObject.equals(locala);
    if (bool2)
    {
      localObject = (f)b;
      paramObject = (f)b;
      boolean bool3 = ((f)localObject).equals(paramObject);
      if (bool3) {
        return bool1;
      }
    }
    return false;
  }
  
  public final int hashCode()
  {
    int i = ((f)b).hashCode() * 11 + 326565;
    int j = a.hashCode() * 7;
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("ZonedChronology[");
    Object localObject = a;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", ");
    localObject = b).b;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(']');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     org.a.a.b.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */