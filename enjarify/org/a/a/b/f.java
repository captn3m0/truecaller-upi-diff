package org.a.a.b;

abstract class f
  extends c
{
  private static final int[] o;
  private static final int[] p;
  private static final long[] q;
  private static final long[] r;
  private static final long serialVersionUID = 538276888268L;
  
  static
  {
    int i = 12;
    Object localObject1 = new int[i];
    Object tmp8_7 = localObject1;
    Object tmp9_8 = tmp8_7;
    Object tmp9_8 = tmp8_7;
    tmp9_8[0] = 31;
    tmp9_8[1] = 28;
    Object tmp18_9 = tmp9_8;
    Object tmp18_9 = tmp9_8;
    tmp18_9[2] = 31;
    tmp18_9[3] = 30;
    Object tmp27_18 = tmp18_9;
    Object tmp27_18 = tmp18_9;
    tmp27_18[4] = 31;
    tmp27_18[5] = 30;
    Object tmp36_27 = tmp27_18;
    Object tmp36_27 = tmp27_18;
    tmp36_27[6] = 31;
    tmp36_27[7] = 31;
    Object tmp47_36 = tmp36_27;
    Object tmp47_36 = tmp36_27;
    tmp47_36[8] = 30;
    tmp47_36[9] = 31;
    tmp47_36[10] = 30;
    tmp47_36[11] = 31;
    o = (int[])localObject1;
    localObject1 = new int[i];
    Object tmp77_76 = localObject1;
    Object tmp78_77 = tmp77_76;
    Object tmp78_77 = tmp77_76;
    tmp78_77[0] = 31;
    tmp78_77[1] = 29;
    Object tmp87_78 = tmp78_77;
    Object tmp87_78 = tmp78_77;
    tmp87_78[2] = 31;
    tmp87_78[3] = 30;
    Object tmp96_87 = tmp87_78;
    Object tmp96_87 = tmp87_78;
    tmp96_87[4] = 31;
    tmp96_87[5] = 30;
    Object tmp105_96 = tmp96_87;
    Object tmp105_96 = tmp96_87;
    tmp105_96[6] = 31;
    tmp105_96[7] = 31;
    Object tmp116_105 = tmp105_96;
    Object tmp116_105 = tmp105_96;
    tmp116_105[8] = 30;
    tmp116_105[9] = 31;
    tmp116_105[10] = 30;
    tmp116_105[11] = 31;
    p = (int[])localObject1;
    localObject1 = new long[i];
    q = (long[])localObject1;
    long[] arrayOfLong1 = new long[i];
    r = arrayOfLong1;
    long l1 = 0L;
    int j = 0;
    long[] arrayOfLong2 = null;
    long l2 = l1;
    for (;;)
    {
      int k = 11;
      if (j >= k) {
        break;
      }
      k = o[j];
      long l3 = k;
      long l4 = 86400000L;
      l3 *= l4;
      l1 += l3;
      Object localObject2 = q;
      int m = j + 1;
      localObject2[m] = l1;
      localObject2 = p;
      long l5 = localObject2[j] * l4;
      l2 += l5;
      arrayOfLong2 = r;
      arrayOfLong2[m] = l2;
      j = m;
    }
  }
  
  f(org.a.a.a parama, int paramInt)
  {
    super(parama, paramInt);
  }
  
  final int a(long paramLong, int paramInt)
  {
    long l = b(paramInt);
    paramLong -= l;
    int i = 10;
    paramLong >>= i;
    int j = (int)paramLong;
    boolean bool = c(paramInt);
    paramInt = 2;
    int m = 3;
    int n = 5;
    int i1 = 6;
    int i2 = 8;
    int i3 = 9;
    int i4 = 11;
    int i5 = 12;
    int i6 = 1;
    int i7 = 4;
    int i8 = 7;
    int i9 = 2615625;
    if (bool)
    {
      k = 15356250;
      if (j < k)
      {
        k = 7678125;
        if (j < k)
        {
          if (j < i9) {
            return i6;
          }
          k = 5062500;
          if (j < k) {
            return paramInt;
          }
          return m;
        }
        k = 10209375;
        if (j < k) {
          return i7;
        }
        k = 12825000;
        if (j < k) {
          return n;
        }
        return i1;
      }
      k = 23118750;
      if (j < k)
      {
        k = 17971875;
        if (j < k) {
          return i8;
        }
        k = 20587500;
        if (j < k) {
          return i2;
        }
        return i3;
      }
      k = 25734375;
      if (j < k) {
        return i;
      }
      k = 28265625;
      if (j < k) {
        return i4;
      }
      return i5;
    }
    int k = 15271875;
    if (j < k)
    {
      k = 7593750;
      if (j < k)
      {
        if (j < i9) {
          return i6;
        }
        k = 4978125;
        if (j < k) {
          return paramInt;
        }
        return m;
      }
      k = 10125000;
      if (j < k) {
        return i7;
      }
      k = 12740625;
      if (j < k) {
        return n;
      }
      return i1;
    }
    k = 23034375;
    if (j < k)
    {
      k = 17887500;
      if (j < k) {
        return i8;
      }
      k = 20503125;
      if (j < k) {
        return i2;
      }
      return i3;
    }
    k = 25650000;
    if (j < k) {
      return i;
    }
    k = 28181250;
    if (j < k) {
      return i4;
    }
    return i5;
  }
  
  final int b(int paramInt1, int paramInt2)
  {
    paramInt1 = c(paramInt1);
    if (paramInt1 != 0)
    {
      arrayOfInt = p;
      paramInt2 += -1;
      return arrayOfInt[paramInt2];
    }
    int[] arrayOfInt = o;
    paramInt2 += -1;
    return arrayOfInt[paramInt2];
  }
  
  final long b(long paramLong1, long paramLong2)
  {
    int i = a(paramLong1);
    int j = a(paramLong2);
    long l1 = b(i);
    paramLong1 -= l1;
    l1 = b(j);
    paramLong2 -= l1;
    l1 = 5097600000L;
    boolean bool2 = paramLong2 < l1;
    if (!bool2)
    {
      bool2 = c(j);
      long l2 = 86400000L;
      boolean bool3;
      if (bool2)
      {
        bool3 = c(i);
        if (!bool3) {
          paramLong2 -= l2;
        }
      }
      else
      {
        bool2 = paramLong1 < l1;
        if (!bool2)
        {
          bool3 = c(i);
          if (bool3) {
            paramLong1 -= l2;
          }
        }
      }
    }
    i -= j;
    boolean bool1 = paramLong1 < paramLong2;
    if (bool1) {
      i += -1;
    }
    return i;
  }
  
  final int c(long paramLong, int paramInt)
  {
    int i = 28;
    if ((paramInt <= i) && (paramInt > 0)) {
      return i;
    }
    return f(paramLong);
  }
  
  final long c(int paramInt1, int paramInt2)
  {
    paramInt1 = c(paramInt1);
    if (paramInt1 != 0)
    {
      arrayOfLong = r;
      paramInt2 += -1;
      return arrayOfLong[paramInt2];
    }
    long[] arrayOfLong = q;
    paramInt2 += -1;
    return arrayOfLong[paramInt2];
  }
  
  final long d(long paramLong, int paramInt)
  {
    int i = a(paramLong);
    int j = b(paramLong, i);
    int k = e(paramLong);
    int m = 59;
    if (j > m)
    {
      boolean bool = c(i);
      if (bool)
      {
        bool = c(paramInt);
        if (!bool) {
          j += -1;
        }
      }
      else
      {
        bool = c(paramInt);
        if (bool) {
          j += 1;
        }
      }
    }
    long l1 = a(paramInt, 1, j);
    long l2 = k;
    return l1 + l2;
  }
  
  final boolean g(long paramLong)
  {
    org.a.a.c localc = this.j;
    int i = localc.a(paramLong);
    int j = 29;
    if (i == j)
    {
      localc = l;
      boolean bool = localc.b(paramLong);
      if (bool) {
        return true;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     org.a.a.b.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */