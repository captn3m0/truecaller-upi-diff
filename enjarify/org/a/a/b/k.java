package org.a.a.b;

import java.util.Locale;
import java.util.TreeMap;
import org.a.a.c.l;
import org.a.a.d;
import org.a.a.i;

final class k
  extends l
{
  private final c b;
  
  k(c paramc, i parami)
  {
    super(locald, parami);
    b = paramc;
  }
  
  public final int a(long paramLong)
  {
    return c.d(paramLong);
  }
  
  public final int a(String paramString, Locale paramLocale)
  {
    paramLocale = (Integer)ah.get(paramString);
    if (paramLocale != null) {
      return paramLocale.intValue();
    }
    paramLocale = new org/a/a/k;
    d locald = d.l();
    paramLocale.<init>(locald, paramString);
    throw paramLocale;
  }
  
  public final int a(Locale paramLocale)
  {
    return ak;
  }
  
  public final String a(int paramInt, Locale paramLocale)
  {
    return ab[paramInt];
  }
  
  public final String b(int paramInt, Locale paramLocale)
  {
    return ac[paramInt];
  }
  
  public final i e()
  {
    return b.d;
  }
  
  public final int g()
  {
    return 1;
  }
  
  public final int h()
  {
    return 7;
  }
}

/* Location:
 * Qualified Name:     org.a.a.b.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */