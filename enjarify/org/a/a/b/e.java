package org.a.a.b;

import org.a.a.c.l;
import org.a.a.d;
import org.a.a.i;

final class e
  extends l
{
  private final c b;
  
  e(c paramc, i parami)
  {
    super(locald, parami);
    b = paramc;
  }
  
  public final int a(long paramLong)
  {
    c localc = b;
    int i = localc.a(paramLong);
    return localc.b(paramLong, i);
  }
  
  public final boolean b(long paramLong)
  {
    return b.g(paramLong);
  }
  
  public final int c(long paramLong)
  {
    c localc1 = b;
    int i = localc1.a(paramLong);
    c localc2 = b;
    boolean bool = localc2.c(i);
    if (bool) {
      return 366;
    }
    return 365;
  }
  
  public final int d(long paramLong, int paramInt)
  {
    int i = 365;
    if ((paramInt <= i) && (paramInt > 0)) {
      return i;
    }
    return c(paramLong);
  }
  
  public final i e()
  {
    return b.g;
  }
  
  public final int g()
  {
    return 1;
  }
  
  public final int h()
  {
    return 366;
  }
}

/* Location:
 * Qualified Name:     org.a.a.b.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */