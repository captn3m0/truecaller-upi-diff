package org.a.a.b;

import org.a.a.c.l;
import org.a.a.d;
import org.a.a.i;

final class h
  extends l
{
  private final c b;
  
  h(c paramc, i parami)
  {
    super(locald, parami);
    b = paramc;
  }
  
  public final int a(long paramLong)
  {
    return b.c(paramLong);
  }
  
  public final int c(long paramLong)
  {
    int i = b.b(paramLong);
    return b.a(i);
  }
  
  public final int d(long paramLong, int paramInt)
  {
    int i = 52;
    if (paramInt > i) {
      return c(paramLong);
    }
    return i;
  }
  
  public final long d(long paramLong)
  {
    long l = 259200000L;
    paramLong += l;
    return super.d(paramLong) - l;
  }
  
  public final long e(long paramLong)
  {
    long l = 259200000L;
    paramLong += l;
    return super.e(paramLong) - l;
  }
  
  public final i e()
  {
    return b.e;
  }
  
  public final int g()
  {
    return 1;
  }
  
  public final int h()
  {
    return 53;
  }
  
  public final long i(long paramLong)
  {
    paramLong += 259200000L;
    return super.i(paramLong);
  }
}

/* Location:
 * Qualified Name:     org.a.a.b.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */