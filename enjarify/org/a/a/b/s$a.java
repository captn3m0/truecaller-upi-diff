package org.a.a.b;

import java.util.Locale;
import org.a.a.c;
import org.a.a.c.b;
import org.a.a.d;
import org.a.a.f;
import org.a.a.i;
import org.a.a.k;
import org.a.a.l;

final class s$a
  extends b
{
  final c a;
  final f b;
  final i c;
  final boolean d;
  final i e;
  final i f;
  
  s$a(c paramc, f paramf, i parami1, i parami2, i parami3)
  {
    super(locald);
    boolean bool1 = paramc.c();
    if (bool1)
    {
      a = paramc;
      b = paramf;
      c = parami1;
      boolean bool2 = s.a(parami1);
      d = bool2;
      e = parami2;
      f = parami3;
      return;
    }
    paramc = new java/lang/IllegalArgumentException;
    paramc.<init>();
    throw paramc;
  }
  
  private int j(long paramLong)
  {
    f localf = b;
    int i = localf.b(paramLong);
    long l1 = i;
    long l2 = paramLong + l1 ^ paramLong;
    long l3 = 0L;
    boolean bool1 = l2 < l3;
    if (bool1)
    {
      paramLong ^= l1;
      boolean bool2 = paramLong < l3;
      if (!bool2)
      {
        ArithmeticException localArithmeticException = new java/lang/ArithmeticException;
        localArithmeticException.<init>("Adding time zone offset caused overflow");
        throw localArithmeticException;
      }
    }
    return i;
  }
  
  public final int a(long paramLong)
  {
    paramLong = b.f(paramLong);
    return a.a(paramLong);
  }
  
  public final int a(Locale paramLocale)
  {
    return a.a(paramLocale);
  }
  
  public final long a(long paramLong, int paramInt)
  {
    boolean bool = d;
    if (bool)
    {
      int i = j(paramLong);
      c localc = a;
      long l1 = i;
      paramLong += l1;
      return localc.a(paramLong, paramInt) - l1;
    }
    long l2 = b.f(paramLong);
    l2 = a.a(l2, paramInt);
    return b.a(l2, paramLong);
  }
  
  public final long a(long paramLong1, long paramLong2)
  {
    boolean bool = d;
    if (bool)
    {
      int i = j(paramLong1);
      c localc = a;
      long l1 = i;
      paramLong1 += l1;
      return localc.a(paramLong1, paramLong2) - l1;
    }
    long l2 = b.f(paramLong1);
    paramLong2 = a.a(l2, paramLong2);
    return b.a(paramLong2, paramLong1);
  }
  
  public final long a(long paramLong, String paramString, Locale paramLocale)
  {
    long l1 = b.f(paramLong);
    long l2 = a.a(l1, paramString, paramLocale);
    return b.a(l2, paramLong);
  }
  
  public final String a(int paramInt, Locale paramLocale)
  {
    return a.a(paramInt, paramLocale);
  }
  
  public final String a(long paramLong, Locale paramLocale)
  {
    paramLong = b.f(paramLong);
    return a.a(paramLong, paramLocale);
  }
  
  public final int b(long paramLong1, long paramLong2)
  {
    int i = j(paramLong2);
    c localc = a;
    boolean bool = d;
    int j;
    if (bool) {
      bool = i;
    } else {
      j = j(paramLong1);
    }
    long l = j;
    paramLong1 += l;
    l = i;
    paramLong2 += l;
    return localc.b(paramLong1, paramLong2);
  }
  
  public final long b(long paramLong, int paramInt)
  {
    Object localObject1 = b;
    long l = ((f)localObject1).f(paramLong);
    l = a.b(l, paramInt);
    f localf = b;
    paramLong = localf.a(l, paramLong);
    int i = a(paramLong);
    if (i == paramInt) {
      return paramLong;
    }
    l locall = new org/a/a/l;
    Object localObject2 = b.b;
    locall.<init>(l, (String)localObject2);
    localObject2 = new org/a/a/k;
    localObject1 = a.a();
    Integer localInteger = Integer.valueOf(paramInt);
    String str = locall.getMessage();
    ((k)localObject2).<init>((d)localObject1, localInteger, str);
    ((k)localObject2).initCause(locall);
    throw ((Throwable)localObject2);
  }
  
  public final String b(int paramInt, Locale paramLocale)
  {
    return a.b(paramInt, paramLocale);
  }
  
  public final String b(long paramLong, Locale paramLocale)
  {
    paramLong = b.f(paramLong);
    return a.b(paramLong, paramLocale);
  }
  
  public final boolean b(long paramLong)
  {
    paramLong = b.f(paramLong);
    return a.b(paramLong);
  }
  
  public final int c(long paramLong)
  {
    paramLong = b.f(paramLong);
    return a.c(paramLong);
  }
  
  public final long c(long paramLong1, long paramLong2)
  {
    int i = j(paramLong2);
    c localc = a;
    boolean bool = d;
    int j;
    if (bool) {
      bool = i;
    } else {
      j = j(paramLong1);
    }
    long l = j;
    paramLong1 += l;
    l = i;
    paramLong2 += l;
    return localc.c(paramLong1, paramLong2);
  }
  
  public final long d(long paramLong)
  {
    boolean bool = d;
    if (bool)
    {
      int i = j(paramLong);
      c localc = a;
      long l1 = i;
      paramLong += l1;
      return localc.d(paramLong) - l1;
    }
    long l2 = b.f(paramLong);
    l2 = a.d(l2);
    return b.a(l2, paramLong);
  }
  
  public final i d()
  {
    return c;
  }
  
  public final long e(long paramLong)
  {
    boolean bool = d;
    if (bool)
    {
      int i = j(paramLong);
      c localc = a;
      long l1 = i;
      paramLong += l1;
      return localc.e(paramLong) - l1;
    }
    long l2 = b.f(paramLong);
    l2 = a.e(l2);
    return b.a(l2, paramLong);
  }
  
  public final i e()
  {
    return e;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this == paramObject) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof a;
    if (bool2)
    {
      paramObject = (a)paramObject;
      Object localObject1 = a;
      Object localObject2 = a;
      bool2 = localObject1.equals(localObject2);
      if (bool2)
      {
        localObject1 = b;
        localObject2 = b;
        bool2 = ((f)localObject1).equals(localObject2);
        if (bool2)
        {
          localObject1 = c;
          localObject2 = c;
          bool2 = localObject1.equals(localObject2);
          if (bool2)
          {
            localObject1 = e;
            paramObject = e;
            boolean bool3 = localObject1.equals(paramObject);
            if (bool3) {
              return bool1;
            }
          }
        }
      }
      return false;
    }
    return false;
  }
  
  public final i f()
  {
    return f;
  }
  
  public final int g()
  {
    return a.g();
  }
  
  public final int h()
  {
    return a.h();
  }
  
  public final int hashCode()
  {
    int i = a.hashCode();
    int j = b.hashCode();
    return i ^ j;
  }
  
  public final long i(long paramLong)
  {
    paramLong = b.f(paramLong);
    return a.i(paramLong);
  }
}

/* Location:
 * Qualified Name:     org.a.a.b.s.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */