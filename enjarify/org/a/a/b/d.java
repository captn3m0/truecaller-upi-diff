package org.a.a.b;

import org.a.a.c.l;
import org.a.a.i;

final class d
  extends l
{
  private final c b;
  
  d(c paramc, i parami)
  {
    super(locald, parami);
    b = paramc;
  }
  
  public final int a(long paramLong)
  {
    c localc = b;
    int i = localc.a(paramLong);
    int j = localc.a(paramLong, i);
    return localc.a(paramLong, i, j);
  }
  
  public final boolean b(long paramLong)
  {
    return b.g(paramLong);
  }
  
  public final int c(long paramLong)
  {
    return b.f(paramLong);
  }
  
  public final int d(long paramLong, int paramInt)
  {
    return b.c(paramLong, paramInt);
  }
  
  public final i e()
  {
    return b.f;
  }
  
  public final int g()
  {
    return 1;
  }
  
  public final int h()
  {
    return 31;
  }
}

/* Location:
 * Qualified Name:     org.a.a.b.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */