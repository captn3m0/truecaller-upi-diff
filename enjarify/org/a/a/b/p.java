package org.a.a.b;

import java.util.concurrent.ConcurrentHashMap;

public final class p
  extends f
{
  private static final p o = a(org.a.a.f.a, 4);
  private static final ConcurrentHashMap p;
  private static final long serialVersionUID = -861407383323710522L;
  
  static
  {
    ConcurrentHashMap localConcurrentHashMap = new java/util/concurrent/ConcurrentHashMap;
    localConcurrentHashMap.<init>();
    p = localConcurrentHashMap;
  }
  
  private p(org.a.a.a parama, int paramInt)
  {
    super(parama, paramInt);
  }
  
  public static p N()
  {
    return o;
  }
  
  private static p a(org.a.a.f paramf, int paramInt)
  {
    if (paramf == null) {
      paramf = org.a.a.f.a();
    }
    Object localObject1 = (p[])p.get(paramf);
    if (localObject1 == null)
    {
      int i = 7;
      localObject1 = new p[i];
      p[] arrayOfp = (p[])p.putIfAbsent(paramf, localObject1);
      if (arrayOfp != null) {
        localObject1 = arrayOfp;
      }
    }
    int j = paramInt + -1;
    try
    {
      Object localObject2 = localObject1[j];
      if (localObject2 == null) {
        try
        {
          localObject2 = localObject1[j];
          if (localObject2 == null)
          {
            localObject2 = org.a.a.f.a;
            if (paramf == localObject2)
            {
              paramf = new org/a/a/b/p;
              localObject2 = null;
              paramf.<init>(null, paramInt);
            }
            else
            {
              localObject2 = org.a.a.f.a;
              localObject2 = a((org.a.a.f)localObject2, paramInt);
              p localp = new org/a/a/b/p;
              paramf = s.a((org.a.a.a)localObject2, paramf);
              localp.<init>(paramf, paramInt);
              paramf = localp;
            }
            localObject1[j] = paramf;
            localObject2 = paramf;
          }
        }
        finally {}
      }
      return (p)localObject2;
    }
    catch (ArrayIndexOutOfBoundsException localArrayIndexOutOfBoundsException)
    {
      paramf = new java/lang/IllegalArgumentException;
      String str = String.valueOf(paramInt);
      str = "Invalid min days in first week: ".concat(str);
      paramf.<init>(str);
      throw paramf;
    }
  }
  
  private Object readResolve()
  {
    Object localObject = a;
    int i = n;
    if (i == 0) {
      i = 4;
    }
    if (localObject == null) {}
    for (localObject = org.a.a.f.a;; localObject = ((org.a.a.a)localObject).a()) {
      return a((org.a.a.f)localObject, i);
    }
  }
  
  public final org.a.a.a a(org.a.a.f paramf)
  {
    if (paramf == null) {
      paramf = org.a.a.f.a();
    }
    org.a.a.f localf = a();
    if (paramf == localf) {
      return this;
    }
    return a(paramf, 4);
  }
  
  protected final void a(a.a parama)
  {
    org.a.a.a locala = a;
    if (locala == null) {
      super.a(parama);
    }
  }
  
  public final org.a.a.a b()
  {
    return o;
  }
  
  final boolean c(int paramInt)
  {
    int i = paramInt & 0x3;
    if (i == 0)
    {
      i = paramInt % 100;
      if (i == 0)
      {
        paramInt %= 400;
        if (paramInt != 0) {}
      }
      else
      {
        return true;
      }
    }
    return false;
  }
  
  final long d(int paramInt)
  {
    int i = paramInt / 100;
    int j;
    if (paramInt < 0)
    {
      j = (paramInt + 3 >> 2) - i;
      i = i + 3 >> 2;
      j = j + i + -1;
    }
    else
    {
      j = (paramInt >> 2) - i;
      i >>= 2;
      j += i;
      boolean bool = c(paramInt);
      if (bool) {
        j += -1;
      }
    }
    long l1 = paramInt * 365L;
    long l2 = j - 719527;
    return (l1 + l2) * 86400000L;
  }
}

/* Location:
 * Qualified Name:     org.a.a.b.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */