package org.a.a.b;

import java.util.concurrent.ConcurrentHashMap;
import org.a.a.c;
import org.a.a.c.n;
import org.a.a.d;
import org.a.a.i;

public final class q
  extends a
{
  private static final q n;
  private static final ConcurrentHashMap o;
  private static final long serialVersionUID = -6212696554273812441L;
  
  static
  {
    Object localObject1 = new java/util/concurrent/ConcurrentHashMap;
    ((ConcurrentHashMap)localObject1).<init>();
    o = (ConcurrentHashMap)localObject1;
    localObject1 = new org/a/a/b/q;
    Object localObject2 = p.N();
    ((q)localObject1).<init>((org.a.a.a)localObject2);
    n = (q)localObject1;
    localObject1 = o;
    localObject2 = org.a.a.f.a;
    q localq = n;
    ((ConcurrentHashMap)localObject1).put(localObject2, localq);
  }
  
  private q(org.a.a.a parama)
  {
    super(parama, null);
  }
  
  public static q L()
  {
    return n;
  }
  
  public static q M()
  {
    return b(org.a.a.f.a());
  }
  
  public static q b(org.a.a.f paramf)
  {
    if (paramf == null) {
      paramf = org.a.a.f.a();
    }
    Object localObject1 = (q)o.get(paramf);
    if (localObject1 == null)
    {
      localObject1 = new org/a/a/b/q;
      Object localObject2 = s.a(n, paramf);
      ((q)localObject1).<init>((org.a.a.a)localObject2);
      localObject2 = o;
      paramf = (q)((ConcurrentHashMap)localObject2).putIfAbsent(paramf, localObject1);
      if (paramf != null) {
        localObject1 = paramf;
      }
    }
    return (q)localObject1;
  }
  
  private Object writeReplace()
  {
    q.a locala = new org/a/a/b/q$a;
    org.a.a.f localf = a();
    locala.<init>(localf);
    return locala;
  }
  
  public final org.a.a.a a(org.a.a.f paramf)
  {
    if (paramf == null) {
      paramf = org.a.a.f.a();
    }
    org.a.a.f localf = a();
    if (paramf == localf) {
      return this;
    }
    return b(paramf);
  }
  
  protected final void a(a.a parama)
  {
    Object localObject1 = a.a();
    Object localObject2 = org.a.a.f.a;
    if (localObject1 == localObject2)
    {
      localObject1 = new org/a/a/c/f;
      localObject2 = r.a;
      Object localObject3 = d.v();
      ((org.a.a.c.f)localObject1).<init>((c)localObject2, (d)localObject3);
      H = ((c)localObject1);
      localObject1 = H.d();
      k = ((i)localObject1);
      localObject1 = new org/a/a/c/n;
      localObject2 = (org.a.a.c.f)H;
      localObject3 = d.u();
      ((n)localObject1).<init>((org.a.a.c.f)localObject2, (d)localObject3);
      G = ((c)localObject1);
      localObject1 = new org/a/a/c/n;
      localObject2 = (org.a.a.c.f)H;
      localObject3 = h;
      d locald = d.q();
      ((n)localObject1).<init>((org.a.a.c.f)localObject2, (i)localObject3, locald);
      C = ((c)localObject1);
    }
  }
  
  public final org.a.a.a b()
  {
    return n;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    boolean bool = paramObject instanceof q;
    if (bool)
    {
      paramObject = (q)paramObject;
      org.a.a.f localf = a();
      paramObject = ((q)paramObject).a();
      return localf.equals(paramObject);
    }
    return false;
  }
  
  public final int hashCode()
  {
    int i = "ISO".hashCode() * 11;
    int j = a().hashCode();
    return i + j;
  }
  
  public final String toString()
  {
    String str = "ISOChronology";
    org.a.a.f localf = a();
    if (localf != null)
    {
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localStringBuilder.append(str);
      localStringBuilder.append('[');
      str = b;
      localStringBuilder.append(str);
      char c = ']';
      localStringBuilder.append(c);
      str = localStringBuilder.toString();
    }
    return str;
  }
}

/* Location:
 * Qualified Name:     org.a.a.b.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */