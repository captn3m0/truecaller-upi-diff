package org.a.a.b;

import org.a.a.c.g;
import org.a.a.c.h;
import org.a.a.d;

final class i
  extends h
{
  private final c a;
  
  i(c paramc)
  {
    super(locald, 31556952000L);
    a = paramc;
  }
  
  public final int a(long paramLong)
  {
    return a.b(paramLong);
  }
  
  public final long a(long paramLong, int paramInt)
  {
    if (paramInt == 0) {
      return paramLong;
    }
    int i = a(paramLong) + paramInt;
    return b(paramLong, i);
  }
  
  public final long a(long paramLong1, long paramLong2)
  {
    int i = g.a(paramLong2);
    return a(paramLong1, i);
  }
  
  public final long b(long paramLong, int paramInt)
  {
    int i = Math.abs(paramInt);
    int j = -292275054;
    int k = 292278993;
    g.a(this, i, j, k);
    i = a(paramLong);
    if (i == paramInt) {
      return paramLong;
    }
    j = c.d(paramLong);
    i = a.a(i);
    c localc = a;
    k = localc.a(paramInt);
    if (k < i) {
      i = k;
    }
    localc = a;
    k = localc.c(paramLong);
    if (k <= i) {
      i = k;
    }
    localc = a;
    paramLong = localc.d(paramLong, paramInt);
    k = a(paramLong);
    long l1 = 604800000L;
    if (k < paramInt) {
      paramLong += l1;
    } else if (k > paramInt) {
      paramLong -= l1;
    }
    paramInt = a.c(paramLong);
    long l2 = (i - paramInt) * l1;
    paramLong += l2;
    return a.i.b(paramLong, j);
  }
  
  public final boolean b(long paramLong)
  {
    c localc = a;
    int i = localc.b(paramLong);
    i = localc.a(i);
    int j = 52;
    return i > j;
  }
  
  public final long c(long paramLong1, long paramLong2)
  {
    boolean bool1 = paramLong1 < paramLong2;
    if (bool1) {
      return -b(paramLong2, paramLong1);
    }
    int i = a(paramLong1);
    int j = a(paramLong2);
    long l = d(paramLong1);
    paramLong1 -= l;
    l = d(paramLong2);
    paramLong2 -= l;
    l = 31449600000L;
    boolean bool3 = paramLong2 < l;
    if (!bool3)
    {
      c localc = a;
      int k = localc.a(i);
      int m = 52;
      if (k <= m)
      {
        l = 604800000L;
        paramLong2 -= l;
      }
    }
    i -= j;
    boolean bool2 = paramLong1 < paramLong2;
    if (bool2) {
      i += -1;
    }
    return i;
  }
  
  public final long d(long paramLong)
  {
    paramLong = a.k.d(paramLong);
    c localc = a;
    int i = localc.c(paramLong);
    int j = 1;
    if (i > j)
    {
      long l1 = 604800000L;
      i -= j;
      long l2 = i * l1;
      paramLong -= l2;
    }
    return paramLong;
  }
  
  public final org.a.a.i e()
  {
    return null;
  }
  
  public final org.a.a.i f()
  {
    return a.d;
  }
  
  public final int g()
  {
    return -292275054;
  }
  
  public final int h()
  {
    return 292278993;
  }
  
  public final long i(long paramLong)
  {
    long l = d(paramLong);
    return paramLong - l;
  }
}

/* Location:
 * Qualified Name:     org.a.a.b.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */