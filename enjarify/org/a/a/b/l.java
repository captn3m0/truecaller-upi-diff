package org.a.a.b;

import java.util.Locale;
import java.util.TreeMap;
import org.a.a.c.b;
import org.a.a.c.g;
import org.a.a.c.q;
import org.a.a.d;
import org.a.a.i;
import org.a.a.j;
import org.a.a.k;

final class l
  extends b
{
  private final c a;
  
  l(c paramc)
  {
    super(locald);
    a = paramc;
  }
  
  public final int a(long paramLong)
  {
    c localc = a;
    int i = localc.a(paramLong);
    if (i <= 0) {
      return 0;
    }
    return 1;
  }
  
  public final int a(Locale paramLocale)
  {
    return aj;
  }
  
  public final long a(long paramLong, String paramString, Locale paramLocale)
  {
    paramLocale = (Integer)ag.get(paramString);
    if (paramLocale != null)
    {
      int i = paramLocale.intValue();
      return b(paramLong, i);
    }
    k localk = new org/a/a/k;
    d locald = d.w();
    localk.<init>(locald, paramString);
    throw localk;
  }
  
  public final String a(int paramInt, Locale paramLocale)
  {
    return aa[paramInt];
  }
  
  public final long b(long paramLong, int paramInt)
  {
    c localc = null;
    int i = 1;
    g.a(this, paramInt, 0, i);
    int j = a(paramLong);
    if (j != paramInt)
    {
      paramInt = a.a(paramLong);
      localc = a;
      paramInt = -paramInt;
      return localc.d(paramLong, paramInt);
    }
    return paramLong;
  }
  
  public final long d(long paramLong)
  {
    int i = a(paramLong);
    int j = 1;
    if (i == j) {
      return a.d(0L, j);
    }
    return Long.MIN_VALUE;
  }
  
  public final i d()
  {
    return q.a(j.l());
  }
  
  public final long e(long paramLong)
  {
    int i = a(paramLong);
    if (i == 0) {
      return a.d(0L, 1);
    }
    return Long.MAX_VALUE;
  }
  
  public final i e()
  {
    return null;
  }
  
  public final long f(long paramLong)
  {
    return d(paramLong);
  }
  
  public final int g()
  {
    return 0;
  }
  
  public final long g(long paramLong)
  {
    return d(paramLong);
  }
  
  public final int h()
  {
    return 1;
  }
  
  public final long h(long paramLong)
  {
    return d(paramLong);
  }
}

/* Location:
 * Qualified Name:     org.a.a.b.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */