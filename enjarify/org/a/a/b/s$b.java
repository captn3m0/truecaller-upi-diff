package org.a.a.b;

import org.a.a.c.c;
import org.a.a.f;
import org.a.a.i;
import org.a.a.j;

final class s$b
  extends c
{
  private static final long serialVersionUID = -485345310999208286L;
  final i a;
  final boolean b;
  final f c;
  
  s$b(i parami, f paramf)
  {
    super(localj);
    boolean bool1 = parami.b();
    if (bool1)
    {
      a = parami;
      boolean bool2 = s.a(parami);
      b = bool2;
      c = paramf;
      return;
    }
    parami = new java/lang/IllegalArgumentException;
    parami.<init>();
    throw parami;
  }
  
  private int a(long paramLong)
  {
    f localf = c;
    int i = localf.b(paramLong);
    long l1 = i;
    long l2 = paramLong + l1 ^ paramLong;
    long l3 = 0L;
    boolean bool1 = l2 < l3;
    if (bool1)
    {
      paramLong ^= l1;
      boolean bool2 = paramLong < l3;
      if (!bool2)
      {
        ArithmeticException localArithmeticException = new java/lang/ArithmeticException;
        localArithmeticException.<init>("Adding time zone offset caused overflow");
        throw localArithmeticException;
      }
    }
    return i;
  }
  
  private int b(long paramLong)
  {
    f localf = c;
    int i = localf.e(paramLong);
    long l1 = i;
    long l2 = paramLong - l1 ^ paramLong;
    long l3 = 0L;
    boolean bool1 = l2 < l3;
    if (bool1)
    {
      paramLong ^= l1;
      boolean bool2 = paramLong < l3;
      if (bool2)
      {
        ArithmeticException localArithmeticException = new java/lang/ArithmeticException;
        localArithmeticException.<init>("Subtracting time zone offset caused overflow");
        throw localArithmeticException;
      }
    }
    return i;
  }
  
  public final long a(long paramLong, int paramInt)
  {
    int i = a(paramLong);
    i locali = a;
    long l1 = i;
    paramLong += l1;
    paramLong = locali.a(paramLong, paramInt);
    paramInt = b;
    if (paramInt == 0) {
      i = b(paramLong);
    }
    long l2 = i;
    return paramLong - l2;
  }
  
  public final long a(long paramLong1, long paramLong2)
  {
    int i = a(paramLong1);
    i locali = a;
    long l = i;
    paramLong1 += l;
    paramLong1 = locali.a(paramLong1, paramLong2);
    boolean bool = b;
    if (!bool) {
      i = b(paramLong1);
    }
    paramLong2 = i;
    return paramLong1 - paramLong2;
  }
  
  public final int b(long paramLong1, long paramLong2)
  {
    int i = a(paramLong2);
    i locali = a;
    boolean bool = b;
    int j;
    if (bool) {
      bool = i;
    } else {
      j = a(paramLong1);
    }
    long l = j;
    paramLong1 += l;
    l = i;
    paramLong2 += l;
    return locali.b(paramLong1, paramLong2);
  }
  
  public final long c(long paramLong1, long paramLong2)
  {
    int i = a(paramLong2);
    i locali = a;
    boolean bool = b;
    int j;
    if (bool) {
      bool = i;
    } else {
      j = a(paramLong1);
    }
    long l = j;
    paramLong1 += l;
    l = i;
    paramLong2 += l;
    return locali.c(paramLong1, paramLong2);
  }
  
  public final boolean c()
  {
    boolean bool = b;
    if (bool) {
      return a.c();
    }
    Object localObject = a;
    bool = ((i)localObject).c();
    if (bool)
    {
      localObject = c;
      bool = ((f)localObject).d();
      if (bool) {
        return true;
      }
    }
    return false;
  }
  
  public final long d()
  {
    return a.d();
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this == paramObject) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof b;
    if (bool2)
    {
      paramObject = (b)paramObject;
      Object localObject = a;
      i locali = a;
      bool2 = localObject.equals(locali);
      if (bool2)
      {
        localObject = c;
        paramObject = c;
        boolean bool3 = ((f)localObject).equals(paramObject);
        if (bool3) {
          return bool1;
        }
      }
      return false;
    }
    return false;
  }
  
  public final int hashCode()
  {
    int i = a.hashCode();
    int j = c.hashCode();
    return i ^ j;
  }
}

/* Location:
 * Qualified Name:     org.a.a.b.s.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */