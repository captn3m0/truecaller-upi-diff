package org.a.a.b;

import org.a.a.c.g;
import org.a.a.c.m;
import org.a.a.c.r;

abstract class c
  extends a
{
  private static final org.a.a.c A;
  private static final org.a.a.c B;
  private static final org.a.a.c C;
  private static final org.a.a.c D;
  private static final org.a.a.c E;
  private static final org.a.a.c F;
  private static final org.a.a.i o = org.a.a.c.i.a;
  private static final org.a.a.i p;
  private static final org.a.a.i q;
  private static final org.a.a.i r;
  private static final org.a.a.i s;
  private static final long serialVersionUID = 8283225332206808863L;
  private static final org.a.a.i t;
  private static final org.a.a.i u;
  private static final org.a.a.c v;
  private static final org.a.a.c w;
  private static final org.a.a.c x;
  private static final org.a.a.c y;
  private static final org.a.a.c z;
  private final transient c.b[] G;
  final int n;
  
  static
  {
    Object localObject1 = new org/a/a/c/m;
    Object localObject2 = org.a.a.j.b();
    ((m)localObject1).<init>((org.a.a.j)localObject2, 1000L);
    p = (org.a.a.i)localObject1;
    localObject1 = new org/a/a/c/m;
    localObject2 = org.a.a.j.c();
    ((m)localObject1).<init>((org.a.a.j)localObject2, 60000L);
    q = (org.a.a.i)localObject1;
    localObject1 = new org/a/a/c/m;
    localObject2 = org.a.a.j.d();
    ((m)localObject1).<init>((org.a.a.j)localObject2, 3600000L);
    r = (org.a.a.i)localObject1;
    localObject1 = new org/a/a/c/m;
    localObject2 = org.a.a.j.e();
    ((m)localObject1).<init>((org.a.a.j)localObject2, 43200000L);
    s = (org.a.a.i)localObject1;
    localObject1 = new org/a/a/c/m;
    localObject2 = org.a.a.j.f();
    ((m)localObject1).<init>((org.a.a.j)localObject2, 86400000L);
    t = (org.a.a.i)localObject1;
    localObject1 = new org/a/a/c/m;
    localObject2 = org.a.a.j.g();
    ((m)localObject1).<init>((org.a.a.j)localObject2, 604800000L);
    u = (org.a.a.i)localObject1;
    localObject1 = new org/a/a/c/k;
    localObject2 = org.a.a.d.a();
    Object localObject3 = o;
    org.a.a.i locali = p;
    ((org.a.a.c.k)localObject1).<init>((org.a.a.d)localObject2, (org.a.a.i)localObject3, locali);
    v = (org.a.a.c)localObject1;
    localObject1 = new org/a/a/c/k;
    localObject2 = org.a.a.d.b();
    localObject3 = o;
    locali = t;
    ((org.a.a.c.k)localObject1).<init>((org.a.a.d)localObject2, (org.a.a.i)localObject3, locali);
    w = (org.a.a.c)localObject1;
    localObject1 = new org/a/a/c/k;
    localObject2 = org.a.a.d.c();
    localObject3 = p;
    locali = q;
    ((org.a.a.c.k)localObject1).<init>((org.a.a.d)localObject2, (org.a.a.i)localObject3, locali);
    x = (org.a.a.c)localObject1;
    localObject1 = new org/a/a/c/k;
    localObject2 = org.a.a.d.d();
    localObject3 = p;
    locali = t;
    ((org.a.a.c.k)localObject1).<init>((org.a.a.d)localObject2, (org.a.a.i)localObject3, locali);
    y = (org.a.a.c)localObject1;
    localObject1 = new org/a/a/c/k;
    localObject2 = org.a.a.d.e();
    localObject3 = q;
    locali = r;
    ((org.a.a.c.k)localObject1).<init>((org.a.a.d)localObject2, (org.a.a.i)localObject3, locali);
    z = (org.a.a.c)localObject1;
    localObject1 = new org/a/a/c/k;
    localObject2 = org.a.a.d.f();
    localObject3 = q;
    locali = t;
    ((org.a.a.c.k)localObject1).<init>((org.a.a.d)localObject2, (org.a.a.i)localObject3, locali);
    A = (org.a.a.c)localObject1;
    localObject1 = new org/a/a/c/k;
    localObject2 = org.a.a.d.g();
    localObject3 = r;
    locali = t;
    ((org.a.a.c.k)localObject1).<init>((org.a.a.d)localObject2, (org.a.a.i)localObject3, locali);
    B = (org.a.a.c)localObject1;
    localObject1 = new org/a/a/c/k;
    localObject2 = org.a.a.d.i();
    localObject3 = r;
    locali = s;
    ((org.a.a.c.k)localObject1).<init>((org.a.a.d)localObject2, (org.a.a.i)localObject3, locali);
    C = (org.a.a.c)localObject1;
    localObject1 = new org/a/a/c/r;
    localObject2 = B;
    localObject3 = org.a.a.d.h();
    ((r)localObject1).<init>((org.a.a.c)localObject2, (org.a.a.d)localObject3);
    D = (org.a.a.c)localObject1;
    localObject1 = new org/a/a/c/r;
    localObject2 = C;
    localObject3 = org.a.a.d.j();
    ((r)localObject1).<init>((org.a.a.c)localObject2, (org.a.a.d)localObject3);
    E = (org.a.a.c)localObject1;
    localObject1 = new org/a/a/b/c$a;
    ((c.a)localObject1).<init>();
    F = (org.a.a.c)localObject1;
  }
  
  c(org.a.a.a parama, int paramInt)
  {
    super(parama, null);
    int i = 1024;
    parama = new c.b[i];
    G = parama;
    if (paramInt > 0)
    {
      i = 7;
      if (paramInt <= i)
      {
        n = paramInt;
        return;
      }
    }
    parama = new java/lang/IllegalArgumentException;
    String str = String.valueOf(paramInt);
    str = "Invalid min days in first week: ".concat(str);
    parama.<init>(str);
    throw parama;
  }
  
  private long b(int paramInt1, int paramInt2, int paramInt3)
  {
    org.a.a.d locald = org.a.a.d.s();
    int i = 292278994;
    int j = -292275055;
    g.a(locald, paramInt1, j, i);
    locald = org.a.a.d.r();
    int k = 1;
    g.a(locald, paramInt2, k, 12);
    locald = org.a.a.d.m();
    int m = b(paramInt1, paramInt2);
    g.a(locald, paramInt3, k, m);
    long l1 = a(paramInt1, paramInt2, paramInt3);
    long l2 = 0L;
    boolean bool = l1 < l2;
    if ((bool) && (paramInt1 == i)) {
      return Long.MAX_VALUE;
    }
    bool = l1 < l2;
    if ((bool) && (paramInt1 == j)) {
      return Long.MIN_VALUE;
    }
    return l1;
  }
  
  static int d(long paramLong)
  {
    long l1 = 7;
    long l2 = 86400000L;
    long l3 = 0L;
    boolean bool1 = paramLong < l3;
    if (!bool1)
    {
      paramLong /= l2;
    }
    else
    {
      l3 = 86399999L;
      paramLong = (paramLong - l3) / l2;
      l2 = -3;
      boolean bool2 = paramLong < l2;
      if (bool2) {
        return (int)((paramLong + 4) % l1) + 7;
      }
    }
    return (int)((paramLong + 3) % l1) + 1;
  }
  
  static int e(long paramLong)
  {
    long l1 = 86400000L;
    long l2 = 0L;
    boolean bool = paramLong < l2;
    if (!bool) {
      return (int)(paramLong % l1);
    }
    return (int)((paramLong + 1L) % l1) + 86399999;
  }
  
  private int e(long paramLong, int paramInt)
  {
    long l1 = e(paramInt);
    int i = 1;
    boolean bool = paramLong < l1;
    if (bool)
    {
      paramInt -= i;
      return a(paramInt);
    }
    paramInt += i;
    long l2 = e(paramInt);
    paramInt = paramLong < l2;
    if (paramInt >= 0) {
      return i;
    }
    return (int)((paramLong - l1) / 604800000L) + i;
  }
  
  private long e(int paramInt)
  {
    long l1 = b(paramInt);
    paramInt = d(l1);
    int i = n;
    i = 8 - i;
    long l2 = 86400000L;
    if (paramInt > i)
    {
      l3 = (8 - paramInt) * l2;
      return l1 + l3;
    }
    long l3 = (paramInt + -1) * l2;
    return l1 - l3;
  }
  
  private c.b f(int paramInt)
  {
    Object localObject = G;
    int i = paramInt & 0x3FF;
    localObject = localObject[i];
    if (localObject != null)
    {
      int j = a;
      if (j == paramInt) {}
    }
    else
    {
      localObject = new org/a/a/b/c$b;
      long l = d(paramInt);
      ((c.b)localObject).<init>(paramInt, l);
      c.b[] arrayOfb = G;
      arrayOfb[i] = localObject;
    }
    return (c.b)localObject;
  }
  
  final int a(int paramInt)
  {
    long l = e(paramInt);
    paramInt += 1;
    return (int)((e(paramInt) - l) / 604800000L);
  }
  
  final int a(long paramLong)
  {
    boolean bool1 = true;
    long l1 = (paramLong >> bool1) + 31083597720000L;
    long l2 = 15778476000L;
    long l3 = 0L;
    boolean bool2 = l1 < l3;
    if (bool2)
    {
      l1 -= l2;
      l4 = 1L;
      l1 += l4;
    }
    l1 /= l2;
    int i = (int)l1;
    l2 = b(i);
    long l4 = paramLong - l2;
    bool1 = l4 < l3;
    if (bool1)
    {
      i += -1;
    }
    else
    {
      l3 = 31536000000L;
      bool1 = l4 < l3;
      if (!bool1)
      {
        bool1 = c(i);
        if (bool1) {
          l3 = 31622400000L;
        }
        l2 += l3;
        bool1 = l2 < paramLong;
        if (!bool1) {
          i += 1;
        }
      }
    }
    return i;
  }
  
  abstract int a(long paramLong, int paramInt);
  
  final int a(long paramLong, int paramInt1, int paramInt2)
  {
    long l1 = b(paramInt1);
    long l2 = c(paramInt1, paramInt2);
    l1 += l2;
    return (int)((paramLong - l1) / 86400000L) + 1;
  }
  
  final long a(int paramInt1, int paramInt2)
  {
    long l1 = b(paramInt1);
    long l2 = c(paramInt1, paramInt2);
    return l1 + l2;
  }
  
  final long a(int paramInt1, int paramInt2, int paramInt3)
  {
    long l1 = b(paramInt1);
    long l2 = c(paramInt1, paramInt2);
    l1 += l2;
    l2 = (paramInt3 + -1) * 86400000L;
    return l1 + l2;
  }
  
  public final long a(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7)
  {
    Object localObject = a;
    if (localObject != null)
    {
      i = paramInt1;
      return ((org.a.a.a)localObject).a(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7);
    }
    g.a(org.a.a.d.g(), paramInt4, 0, 23);
    localObject = org.a.a.d.e();
    int i = 59;
    g.a((org.a.a.d)localObject, paramInt5, 0, i);
    g.a(org.a.a.d.c(), paramInt6, 0, i);
    localObject = org.a.a.d.a();
    i = 999;
    g.a((org.a.a.d)localObject, paramInt7, 0, i);
    paramInt4 *= 3600000;
    int j = 60000;
    paramInt5 *= j;
    paramInt4 += paramInt5;
    paramInt6 *= 1000;
    long l1 = paramInt4 + paramInt6 + paramInt7;
    paramInt5 = (int)l1;
    long l2 = b(paramInt1, paramInt2, paramInt3);
    long l3 = Long.MIN_VALUE;
    paramInt4 = l2 < l3;
    if (paramInt4 == 0)
    {
      paramInt3 += 1;
      l2 = b(paramInt1, paramInt2, paramInt3);
      paramInt1 = 86400000;
      paramInt5 -= paramInt1;
    }
    long l4 = paramInt5 + l2;
    long l5 = 0L;
    paramInt5 = l4 < l5;
    if (paramInt5 < 0)
    {
      paramInt5 = l2 < l5;
      if (paramInt5 > 0) {
        return Long.MAX_VALUE;
      }
    }
    paramInt5 = l4 < l5;
    if (paramInt5 > 0)
    {
      paramInt5 = l2 < l5;
      if (paramInt5 < 0) {
        return l3;
      }
    }
    return l4;
  }
  
  public final org.a.a.f a()
  {
    org.a.a.a locala = a;
    if (locala != null) {
      return locala.a();
    }
    return org.a.a.f.a;
  }
  
  protected void a(a.a parama)
  {
    Object localObject1 = o;
    a = ((org.a.a.i)localObject1);
    localObject1 = p;
    b = ((org.a.a.i)localObject1);
    localObject1 = q;
    c = ((org.a.a.i)localObject1);
    localObject1 = r;
    d = ((org.a.a.i)localObject1);
    localObject1 = s;
    e = ((org.a.a.i)localObject1);
    localObject1 = t;
    f = ((org.a.a.i)localObject1);
    localObject1 = u;
    g = ((org.a.a.i)localObject1);
    localObject1 = v;
    m = ((org.a.a.c)localObject1);
    localObject1 = w;
    n = ((org.a.a.c)localObject1);
    localObject1 = x;
    o = ((org.a.a.c)localObject1);
    localObject1 = y;
    p = ((org.a.a.c)localObject1);
    localObject1 = z;
    q = ((org.a.a.c)localObject1);
    localObject1 = A;
    r = ((org.a.a.c)localObject1);
    localObject1 = B;
    s = ((org.a.a.c)localObject1);
    localObject1 = C;
    u = ((org.a.a.c)localObject1);
    localObject1 = D;
    t = ((org.a.a.c)localObject1);
    localObject1 = E;
    v = ((org.a.a.c)localObject1);
    localObject1 = F;
    w = ((org.a.a.c)localObject1);
    localObject1 = new org/a/a/b/j;
    ((j)localObject1).<init>(this);
    E = ((org.a.a.c)localObject1);
    localObject1 = new org/a/a/b/o;
    Object localObject2 = E;
    ((o)localObject1).<init>((org.a.a.c)localObject2, this);
    F = ((org.a.a.c)localObject1);
    localObject1 = new org/a/a/c/j;
    localObject2 = F;
    ((org.a.a.c.j)localObject1).<init>((org.a.a.c)localObject2);
    localObject2 = new org/a/a/c/f;
    Object localObject3 = org.a.a.d.v();
    ((org.a.a.c.f)localObject2).<init>((org.a.a.c)localObject1, (org.a.a.d)localObject3);
    H = ((org.a.a.c)localObject2);
    localObject1 = H.d();
    k = ((org.a.a.i)localObject1);
    localObject1 = new org/a/a/c/n;
    localObject2 = (org.a.a.c.f)H;
    ((org.a.a.c.n)localObject1).<init>((org.a.a.c.f)localObject2);
    localObject2 = new org/a/a/c/j;
    localObject3 = org.a.a.d.u();
    ((org.a.a.c.j)localObject2).<init>((org.a.a.c)localObject1, (org.a.a.d)localObject3);
    G = ((org.a.a.c)localObject2);
    localObject1 = new org/a/a/b/l;
    ((l)localObject1).<init>(this);
    I = ((org.a.a.c)localObject1);
    localObject1 = new org/a/a/b/k;
    localObject2 = f;
    ((k)localObject1).<init>(this, (org.a.a.i)localObject2);
    x = ((org.a.a.c)localObject1);
    localObject1 = new org/a/a/b/d;
    localObject2 = f;
    ((d)localObject1).<init>(this, (org.a.a.i)localObject2);
    y = ((org.a.a.c)localObject1);
    localObject1 = new org/a/a/b/e;
    localObject2 = f;
    ((e)localObject1).<init>(this, (org.a.a.i)localObject2);
    z = ((org.a.a.c)localObject1);
    localObject1 = new org/a/a/b/n;
    ((n)localObject1).<init>(this);
    D = ((org.a.a.c)localObject1);
    localObject1 = new org/a/a/b/i;
    ((i)localObject1).<init>(this);
    B = ((org.a.a.c)localObject1);
    localObject1 = new org/a/a/b/h;
    localObject2 = g;
    ((h)localObject1).<init>(this, (org.a.a.i)localObject2);
    A = ((org.a.a.c)localObject1);
    localObject1 = new org/a/a/c/n;
    localObject2 = B;
    localObject3 = k;
    org.a.a.d locald = org.a.a.d.q();
    ((org.a.a.c.n)localObject1).<init>((org.a.a.c)localObject2, (org.a.a.i)localObject3, locald);
    localObject2 = new org/a/a/c/j;
    localObject3 = org.a.a.d.q();
    ((org.a.a.c.j)localObject2).<init>((org.a.a.c)localObject1, (org.a.a.d)localObject3);
    C = ((org.a.a.c)localObject2);
    localObject1 = E.d();
    j = ((org.a.a.i)localObject1);
    localObject1 = D.d();
    i = ((org.a.a.i)localObject1);
    localObject1 = B.d();
    h = ((org.a.a.i)localObject1);
  }
  
  abstract int b(int paramInt1, int paramInt2);
  
  final int b(long paramLong)
  {
    int i = a(paramLong);
    int j = e(paramLong, i);
    int k = 1;
    if (j == k)
    {
      paramLong += 604800000L;
      return a(paramLong);
    }
    k = 51;
    if (j > k)
    {
      paramLong -= 1209600000L;
      return a(paramLong);
    }
    return i;
  }
  
  final int b(long paramLong, int paramInt)
  {
    long l = b(paramInt);
    return (int)((paramLong - l) / 86400000L) + 1;
  }
  
  final long b(int paramInt)
  {
    return fb;
  }
  
  abstract long b(long paramLong1, long paramLong2);
  
  final int c(long paramLong)
  {
    int i = a(paramLong);
    return e(paramLong, i);
  }
  
  int c(long paramLong, int paramInt)
  {
    return f(paramLong);
  }
  
  abstract long c(int paramInt1, int paramInt2);
  
  abstract boolean c(int paramInt);
  
  abstract long d(int paramInt);
  
  abstract long d(long paramLong, int paramInt);
  
  public boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this == paramObject) {
      return bool1;
    }
    if (paramObject != null)
    {
      Object localObject = getClass();
      Class localClass = paramObject.getClass();
      if (localObject == localClass)
      {
        paramObject = (c)paramObject;
        int i = n;
        int j = n;
        if (i == j)
        {
          localObject = a();
          paramObject = ((c)paramObject).a();
          boolean bool2 = ((org.a.a.f)localObject).equals(paramObject);
          if (bool2) {
            return bool1;
          }
        }
        return false;
      }
    }
    return false;
  }
  
  final int f(long paramLong)
  {
    int i = a(paramLong);
    int j = a(paramLong, i);
    return b(i, j);
  }
  
  boolean g(long paramLong)
  {
    return false;
  }
  
  public int hashCode()
  {
    int i = getClass().getName().hashCode() * 11;
    int j = a().hashCode();
    i += j;
    j = n;
    return i + j;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    char c = '<';
    localStringBuilder.<init>(c);
    Object localObject = getClass().getName();
    int j = ((String)localObject).lastIndexOf('.');
    if (j >= 0)
    {
      j += 1;
      localObject = ((String)localObject).substring(j);
    }
    localStringBuilder.append((String)localObject);
    c = '[';
    localStringBuilder.append(c);
    localObject = a();
    if (localObject != null)
    {
      localObject = b;
      localStringBuilder.append((String)localObject);
    }
    int i = n;
    j = 4;
    if (i != j)
    {
      localObject = ",mdfw=";
      localStringBuilder.append((String)localObject);
      i = n;
      localStringBuilder.append(i);
    }
    localStringBuilder.append(']');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     org.a.a.b.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */