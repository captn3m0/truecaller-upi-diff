package org.a.a.b;

import org.a.a.c.g;
import org.a.a.i;

final class o
  extends org.a.a.c.d
{
  private final c a;
  
  o(org.a.a.c paramc, c paramc1)
  {
    super(paramc, locald);
    a = paramc1;
  }
  
  public final int a(long paramLong)
  {
    org.a.a.c localc = b;
    int i = localc.a(paramLong);
    if (i <= 0) {
      i = 1 - i;
    }
    return i;
  }
  
  public final long a(long paramLong, int paramInt)
  {
    return b.a(paramLong, paramInt);
  }
  
  public final long a(long paramLong1, long paramLong2)
  {
    return b.a(paramLong1, paramLong2);
  }
  
  public final int b(long paramLong1, long paramLong2)
  {
    return b.b(paramLong1, paramLong2);
  }
  
  public final long b(long paramLong, int paramInt)
  {
    int i = b.h();
    int j = 1;
    g.a(this, paramInt, j, i);
    c localc = a;
    i = localc.a(paramLong);
    if (i <= 0) {
      paramInt = 1 - paramInt;
    }
    return super.b(paramLong, paramInt);
  }
  
  public final long c(long paramLong1, long paramLong2)
  {
    return b.c(paramLong1, paramLong2);
  }
  
  public final long d(long paramLong)
  {
    return b.d(paramLong);
  }
  
  public final long e(long paramLong)
  {
    return b.e(paramLong);
  }
  
  public final i e()
  {
    return a.h;
  }
  
  public final int g()
  {
    return 1;
  }
  
  public final int h()
  {
    return b.h();
  }
  
  public final long i(long paramLong)
  {
    return b.i(paramLong);
  }
}

/* Location:
 * Qualified Name:     org.a.a.b.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */