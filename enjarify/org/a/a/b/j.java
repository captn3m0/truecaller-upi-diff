package org.a.a.b;

import org.a.a.c.g;
import org.a.a.c.h;
import org.a.a.d;
import org.a.a.i;

final class j
  extends h
{
  protected final c a;
  
  j(c paramc)
  {
    super(locald, 31556952000L);
    a = paramc;
  }
  
  public final int a(long paramLong)
  {
    return a.a(paramLong);
  }
  
  public final long a(long paramLong, int paramInt)
  {
    if (paramInt == 0) {
      return paramLong;
    }
    int i = a(paramLong);
    int j = i + paramInt;
    int k = i ^ j;
    if (k < 0)
    {
      k = i ^ paramInt;
      if (k >= 0)
      {
        ArithmeticException localArithmeticException = new java/lang/ArithmeticException;
        Object localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>("The calculation caused an overflow: ");
        ((StringBuilder)localObject).append(i);
        ((StringBuilder)localObject).append(" + ");
        ((StringBuilder)localObject).append(paramInt);
        localObject = ((StringBuilder)localObject).toString();
        localArithmeticException.<init>((String)localObject);
        throw localArithmeticException;
      }
    }
    return b(paramLong, j);
  }
  
  public final long a(long paramLong1, long paramLong2)
  {
    int i = g.a(paramLong2);
    return a(paramLong1, i);
  }
  
  public final long b(long paramLong, int paramInt)
  {
    g.a(this, paramInt, -292275054, 292278993);
    return a.d(paramLong, paramInt);
  }
  
  public final boolean b(long paramLong)
  {
    c localc = a;
    int i = a(paramLong);
    return localc.c(i);
  }
  
  public final long c(long paramLong, int paramInt)
  {
    g.a(this, paramInt, -292275055, 292278994);
    return a.d(paramLong, paramInt);
  }
  
  public final long c(long paramLong1, long paramLong2)
  {
    boolean bool = paramLong1 < paramLong2;
    if (bool) {
      return -a.b(paramLong2, paramLong1);
    }
    return a.b(paramLong1, paramLong2);
  }
  
  public final long d(long paramLong)
  {
    c localc = a;
    int i = a(paramLong);
    return localc.b(i);
  }
  
  public final long e(long paramLong)
  {
    int i = a(paramLong);
    c localc1 = a;
    long l = localc1.b(i);
    boolean bool = paramLong < l;
    if (bool)
    {
      c localc2 = a;
      i += 1;
      paramLong = localc2.b(i);
    }
    return paramLong;
  }
  
  public final i e()
  {
    return null;
  }
  
  public final i f()
  {
    return a.c;
  }
  
  public final int g()
  {
    return -292275054;
  }
  
  public final int h()
  {
    return 292278993;
  }
  
  public final long i(long paramLong)
  {
    long l = d(paramLong);
    return paramLong - l;
  }
}

/* Location:
 * Qualified Name:     org.a.a.b.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */