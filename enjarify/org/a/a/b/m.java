package org.a.a.b;

import java.text.DateFormatSymbols;
import java.util.Comparator;
import java.util.Locale;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.a.a.e;

final class m
{
  private static ConcurrentMap n;
  final String[] a;
  final String[] b;
  final String[] c;
  final String[] d;
  final String[] e;
  final String[] f;
  final TreeMap g;
  final TreeMap h;
  final TreeMap i;
  final int j;
  final int k;
  final int l;
  final int m;
  private final int o;
  private final int p;
  
  static
  {
    ConcurrentHashMap localConcurrentHashMap = new java/util/concurrent/ConcurrentHashMap;
    localConcurrentHashMap.<init>();
    n = localConcurrentHashMap;
  }
  
  private m(Locale paramLocale)
  {
    Object localObject1 = e.a(paramLocale);
    Object localObject2 = ((DateFormatSymbols)localObject1).getEras();
    a = ((String[])localObject2);
    localObject2 = b(((DateFormatSymbols)localObject1).getWeekdays());
    b = ((String[])localObject2);
    localObject2 = b(((DateFormatSymbols)localObject1).getShortWeekdays());
    c = ((String[])localObject2);
    localObject2 = a(((DateFormatSymbols)localObject1).getMonths());
    d = ((String[])localObject2);
    localObject2 = a(((DateFormatSymbols)localObject1).getShortMonths());
    e = ((String[])localObject2);
    localObject1 = ((DateFormatSymbols)localObject1).getAmPmStrings();
    f = ((String[])localObject1);
    int i1 = 13;
    localObject2 = new Integer[i1];
    int i2 = 0;
    Object localObject3 = null;
    int i3 = 0;
    Object localObject4 = null;
    while (i3 < i1)
    {
      Integer localInteger = Integer.valueOf(i3);
      localObject2[i3] = localInteger;
      i3 += 1;
    }
    localObject1 = new java/util/TreeMap;
    localObject4 = String.CASE_INSENSITIVE_ORDER;
    ((TreeMap)localObject1).<init>((Comparator)localObject4);
    g = ((TreeMap)localObject1);
    localObject1 = g;
    localObject4 = a;
    a((TreeMap)localObject1, (String[])localObject4, (Integer[])localObject2);
    localObject1 = "en";
    paramLocale = paramLocale.getLanguage();
    boolean bool = ((String)localObject1).equals(paramLocale);
    if (bool)
    {
      paramLocale = g;
      localObject3 = localObject2[0];
      paramLocale.put("BCE", localObject3);
      paramLocale = g;
      localObject1 = "CE";
      i2 = 1;
      localObject3 = localObject2[i2];
      paramLocale.put(localObject1, localObject3);
    }
    paramLocale = new java/util/TreeMap;
    localObject1 = String.CASE_INSENSITIVE_ORDER;
    paramLocale.<init>((Comparator)localObject1);
    h = paramLocale;
    paramLocale = h;
    localObject1 = b;
    a(paramLocale, (String[])localObject1, (Integer[])localObject2);
    paramLocale = h;
    localObject1 = c;
    a(paramLocale, (String[])localObject1, (Integer[])localObject2);
    a(h, 7, (Integer[])localObject2);
    paramLocale = new java/util/TreeMap;
    localObject1 = String.CASE_INSENSITIVE_ORDER;
    paramLocale.<init>((Comparator)localObject1);
    i = paramLocale;
    paramLocale = i;
    localObject1 = d;
    a(paramLocale, (String[])localObject1, (Integer[])localObject2);
    paramLocale = i;
    localObject1 = e;
    a(paramLocale, (String[])localObject1, (Integer[])localObject2);
    a(i, 12, (Integer[])localObject2);
    int i4 = c(a);
    j = i4;
    i4 = c(b);
    k = i4;
    i4 = c(c);
    o = i4;
    i4 = c(d);
    l = i4;
    i4 = c(e);
    p = i4;
    i4 = c(f);
    m = i4;
  }
  
  static m a(Locale paramLocale)
  {
    if (paramLocale == null) {
      paramLocale = Locale.getDefault();
    }
    Object localObject = (m)n.get(paramLocale);
    if (localObject == null)
    {
      localObject = new org/a/a/b/m;
      ((m)localObject).<init>(paramLocale);
      ConcurrentMap localConcurrentMap = n;
      paramLocale = (m)localConcurrentMap.putIfAbsent(paramLocale, localObject);
      if (paramLocale != null) {
        localObject = paramLocale;
      }
    }
    return (m)localObject;
  }
  
  private static void a(TreeMap paramTreeMap, int paramInt, Integer[] paramArrayOfInteger)
  {
    int i1 = 1;
    while (i1 <= paramInt)
    {
      String str = String.valueOf(i1).intern();
      Integer localInteger = paramArrayOfInteger[i1];
      paramTreeMap.put(str, localInteger);
      i1 += 1;
    }
  }
  
  private static void a(TreeMap paramTreeMap, String[] paramArrayOfString, Integer[] paramArrayOfInteger)
  {
    int i1 = paramArrayOfString.length;
    for (;;)
    {
      i1 += -1;
      if (i1 < 0) {
        break;
      }
      String str = paramArrayOfString[i1];
      if (str != null)
      {
        Integer localInteger = paramArrayOfInteger[i1];
        paramTreeMap.put(str, localInteger);
      }
    }
  }
  
  private static String[] a(String[] paramArrayOfString)
  {
    int i1 = 13;
    String[] arrayOfString = new String[i1];
    int i2 = 1;
    while (i2 < i1)
    {
      int i3 = i2 + -1;
      String str = paramArrayOfString[i3];
      arrayOfString[i2] = str;
      i2 += 1;
    }
    return arrayOfString;
  }
  
  private static String[] b(String[] paramArrayOfString)
  {
    int i1 = 8;
    String[] arrayOfString = new String[i1];
    int i2 = 1;
    while (i2 < i1)
    {
      int i3 = 7;
      if (i2 < i3) {
        i3 = i2 + 1;
      } else {
        i3 = 1;
      }
      String str = paramArrayOfString[i3];
      arrayOfString[i2] = str;
      i2 += 1;
    }
    return arrayOfString;
  }
  
  private static int c(String[] paramArrayOfString)
  {
    int i1 = paramArrayOfString.length;
    int i2 = 0;
    for (;;)
    {
      i1 += -1;
      if (i1 < 0) {
        break;
      }
      String str = paramArrayOfString[i1];
      if (str != null)
      {
        int i3 = str.length();
        if (i3 > i2) {
          i2 = i3;
        }
      }
    }
    return i2;
  }
}

/* Location:
 * Qualified Name:     org.a.a.b.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */