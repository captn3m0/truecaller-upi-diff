package org.a.a.b;

import org.a.a.c;
import org.a.a.c.g;
import org.a.a.i;

final class r
  extends org.a.a.c.d
{
  static final c a;
  
  static
  {
    r localr = new org/a/a/b/r;
    localr.<init>();
    a = localr;
  }
  
  private r()
  {
    super(localc, locald);
  }
  
  public final int a(long paramLong)
  {
    c localc = b;
    int i = localc.a(paramLong);
    if (i < 0) {
      i = -i;
    }
    return i;
  }
  
  public final long a(long paramLong, int paramInt)
  {
    return b.a(paramLong, paramInt);
  }
  
  public final long a(long paramLong1, long paramLong2)
  {
    return b.a(paramLong1, paramLong2);
  }
  
  public final int b(long paramLong1, long paramLong2)
  {
    return b.b(paramLong1, paramLong2);
  }
  
  public final long b(long paramLong, int paramInt)
  {
    int i = b.h();
    g.a(this, paramInt, 0, i);
    c localc = b;
    i = localc.a(paramLong);
    if (i < 0) {
      paramInt = -paramInt;
    }
    return super.b(paramLong, paramInt);
  }
  
  public final long c(long paramLong1, long paramLong2)
  {
    return b.c(paramLong1, paramLong2);
  }
  
  public final long d(long paramLong)
  {
    return b.d(paramLong);
  }
  
  public final long e(long paramLong)
  {
    return b.e(paramLong);
  }
  
  public final i e()
  {
    return Nh;
  }
  
  public final int g()
  {
    return 0;
  }
  
  public final int h()
  {
    return b.h();
  }
  
  public final long i(long paramLong)
  {
    return b.i(paramLong);
  }
}

/* Location:
 * Qualified Name:     org.a.a.b.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */