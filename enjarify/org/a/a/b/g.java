package org.a.a.b;

import org.a.a.c.h;
import org.a.a.d;
import org.a.a.i;

class g
  extends h
{
  private final c a;
  private final int c;
  private final int d;
  
  g(c paramc)
  {
    super(locald, 2629746000L);
    a = paramc;
    c = 12;
    d = 2;
  }
  
  public final int a(long paramLong)
  {
    c localc = a;
    int i = localc.a(paramLong);
    return localc.a(paramLong, i);
  }
  
  public final long a(long paramLong, int paramInt)
  {
    if (paramInt == 0) {
      return paramLong;
    }
    int i = c.e(paramLong);
    long l = i;
    c localc1 = a;
    int j = localc1.a(paramLong);
    c localc2 = a;
    int k = localc2.a(paramLong, j);
    int m = k + -1;
    int n = m + paramInt;
    int i1;
    int i2;
    if ((k > 0) && (n < 0))
    {
      float f1 = Math.signum(c + paramInt);
      float f2 = Math.signum(paramInt);
      boolean bool = f1 < f2;
      if (!bool)
      {
        i1 = j + -1;
        i2 = c;
        paramInt += i2;
      }
      else
      {
        i1 = j + 1;
        i2 = c;
        paramInt -= i2;
      }
      paramInt += m;
    }
    else
    {
      paramInt = i1;
      i1 = j;
    }
    m = 1;
    if (paramInt >= 0)
    {
      i2 = c;
      int i3 = paramInt / i2;
      i1 += i3;
      paramInt = paramInt % i2 + m;
    }
    else
    {
      i2 = c;
      i2 = paramInt / i2;
      i1 = i1 + i2 - m;
      paramInt = Math.abs(paramInt);
      i2 = c;
      paramInt %= i2;
      if (paramInt == 0) {
        paramInt = i2;
      }
      i2 = c - paramInt;
      paramInt = i2 + 1;
      if (paramInt == m) {
        i1 += 1;
      }
    }
    c localc3 = a;
    int i4 = localc3.a(paramLong, j, k);
    c localc4 = a;
    int i5 = localc4.b(i1, paramInt);
    if (i4 > i5) {
      i4 = i5;
    }
    return a.a(i1, paramInt, i4) + l;
  }
  
  public final long a(long paramLong1, long paramLong2)
  {
    g localg = this;
    long l1 = paramLong1;
    int i = (int)paramLong2;
    long l2 = i;
    boolean bool1 = l2 < paramLong2;
    if (!bool1) {
      return a(paramLong1, i);
    }
    i = c.e(paramLong1);
    long l3 = i;
    c localc1 = a;
    int k = localc1.a(paramLong1);
    c localc2 = a;
    int j = localc2.a(paramLong1, k);
    int m = j + -1;
    long l4 = m + paramLong2;
    long l5 = 0L;
    boolean bool2 = l4 < l5;
    int n;
    if (!bool2)
    {
      l5 = k;
      n = c;
      l6 = n;
      l6 = l4 / l6;
      l5 += l6;
      l6 = n;
      l4 %= l6;
      l6 = 1L;
      l4 += l6;
    }
    else
    {
      l5 = k;
      n = c;
      l6 = n;
      l6 = l4 / l6;
      l5 += l6;
      l6 = 1L;
      l5 -= l6;
      l4 = Math.abs(l4);
      int i1 = c;
      long l7 = i1;
      l4 %= l7;
      int i2 = (int)l4;
      if (i2 != 0) {
        i1 = i2;
      }
      m = c - i1 + 1;
      l4 = m;
      l6 = 1L;
      bool3 = l4 < l6;
      if (!bool3) {
        l5 += l6;
      }
    }
    long l6 = 4002692242L;
    boolean bool3 = l5 < l6;
    if (!bool3)
    {
      l6 = 292278993L;
      bool3 = l5 < l6;
      if (!bool3)
      {
        int i3 = (int)l5;
        int i4 = (int)l4;
        c localc3 = a;
        int i5 = localc3.a(l1, k, j);
        localObject = a;
        int i6 = ((c)localObject).b(i3, i4);
        if (i5 > i6) {
          i5 = i6;
        }
        return a.a(i3, i4, i5) + l3;
      }
    }
    IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
    Object localObject = String.valueOf(paramLong2);
    localObject = "Magnitude of add amount is too large: ".concat((String)localObject);
    localIllegalArgumentException.<init>((String)localObject);
    throw localIllegalArgumentException;
  }
  
  public final long b(long paramLong, int paramInt)
  {
    int i = c;
    org.a.a.c.g.a(this, paramInt, 1, i);
    c localc1 = a;
    i = localc1.a(paramLong);
    c localc2 = a;
    int j = localc2.a(paramLong, i);
    int k = localc2.a(paramLong, i, j);
    c localc3 = a;
    j = localc3.b(i, paramInt);
    if (k > j) {
      k = j;
    }
    long l = a.a(i, paramInt, k);
    paramLong = c.e(paramLong);
    return l + paramLong;
  }
  
  public final boolean b(long paramLong)
  {
    c localc1 = a;
    int i = localc1.a(paramLong);
    c localc2 = a;
    boolean bool = localc2.c(i);
    if (bool)
    {
      localc2 = a;
      int j = localc2.a(paramLong, i);
      int k = d;
      return j == k;
    }
    return false;
  }
  
  public final long c(long paramLong1, long paramLong2)
  {
    boolean bool1 = paramLong1 < paramLong2;
    if (bool1) {
      return -b(paramLong2, paramLong1);
    }
    c localc1 = a;
    int i = localc1.a(paramLong1);
    c localc2 = a;
    int j = localc2.a(paramLong1, i);
    c localc3 = a;
    int k = localc3.a(paramLong2);
    c localc4 = a;
    int m = localc4.a(paramLong2, k);
    int n = i - k;
    long l1 = n;
    long l2 = c;
    l1 *= l2;
    l2 = j;
    l1 += l2;
    l2 = m;
    l1 -= l2;
    c localc5 = a;
    int i1 = localc5.a(paramLong1, i, j);
    Object localObject = a;
    int i2 = ((c)localObject).b(i, j);
    if (i1 == i2)
    {
      localObject = a;
      i2 = ((c)localObject).a(paramLong2, k, m);
      if (i2 > i1)
      {
        localObject = a.j;
        paramLong2 = ((org.a.a.c)localObject).b(paramLong2, i1);
      }
    }
    localc5 = a;
    long l3 = localc5.a(i, j);
    paramLong1 -= l3;
    localc1 = a;
    l3 = localc1.a(k, m);
    paramLong2 -= l3;
    boolean bool2 = paramLong1 < paramLong2;
    if (bool2)
    {
      paramLong1 = 1L;
      l1 -= paramLong1;
    }
    return l1;
  }
  
  public final long d(long paramLong)
  {
    int i = a.a(paramLong);
    int j = a.a(paramLong, i);
    return a.a(i, j);
  }
  
  public final i e()
  {
    return a.g;
  }
  
  public final i f()
  {
    return a.c;
  }
  
  public final int g()
  {
    return 1;
  }
  
  public final int h()
  {
    return c;
  }
  
  public final long i(long paramLong)
  {
    long l = d(paramLong);
    return paramLong - l;
  }
}

/* Location:
 * Qualified Name:     org.a.a.b.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */