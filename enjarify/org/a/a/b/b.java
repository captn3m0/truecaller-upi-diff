package org.a.a.b;

import java.io.Serializable;
import org.a.a.a;
import org.a.a.aa;
import org.a.a.c;
import org.a.a.c.g;
import org.a.a.c.p;
import org.a.a.c.q;
import org.a.a.d;
import org.a.a.i;
import org.a.a.j;
import org.a.a.z;

public abstract class b
  extends a
  implements Serializable
{
  private static final long serialVersionUID = -7310865996721419676L;
  
  public c A()
  {
    d locald = d.q();
    i locali = y();
    return p.a(locald, locali);
  }
  
  public i B()
  {
    return q.a(j.i());
  }
  
  public c C()
  {
    d locald = d.r();
    i locali = B();
    return p.a(locald, locali);
  }
  
  public i D()
  {
    return q.a(j.j());
  }
  
  public c E()
  {
    d locald = d.s();
    i locali = D();
    return p.a(locald, locali);
  }
  
  public c F()
  {
    d locald = d.t();
    i locali = D();
    return p.a(locald, locali);
  }
  
  public c G()
  {
    d locald = d.u();
    i locali = D();
    return p.a(locald, locali);
  }
  
  public i H()
  {
    return q.a(j.k());
  }
  
  public c I()
  {
    d locald = d.v();
    i locali = H();
    return p.a(locald, locali);
  }
  
  public i J()
  {
    return q.a(j.l());
  }
  
  public c K()
  {
    d locald = d.w();
    i locali = J();
    return p.a(locald, locali);
  }
  
  public long a(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7)
  {
    long l1 = E().b(0L, paramInt1);
    long l2 = C().b(l1, paramInt2);
    l2 = u().b(l2, paramInt3);
    l2 = m().b(l2, paramInt4);
    l2 = j().b(l2, paramInt5);
    l2 = g().b(l2, paramInt6);
    return d().b(l2, paramInt7);
  }
  
  public final long a(long paramLong1, long paramLong2)
  {
    long l = 0L;
    boolean bool = paramLong2 < l;
    if (!bool) {
      return paramLong1;
    }
    paramLong2 = g.a(paramLong2, 1);
    return g.a(paramLong1, paramLong2);
  }
  
  public final long a(z paramz, long paramLong)
  {
    int i = 0;
    for (;;)
    {
      int j = 3;
      if (i >= j) {
        break;
      }
      c localc = paramz.b(i).a(this);
      int k = paramz.a(i);
      paramLong = localc.b(paramLong, k);
      i += 1;
    }
    return paramLong;
  }
  
  public final int[] a(aa paramaa, long paramLong)
  {
    int i = paramaa.d();
    int[] arrayOfInt = new int[i];
    long l = 0L;
    int j = paramLong < l;
    if (j != 0)
    {
      j = 0;
      while (j < i)
      {
        i locali = paramaa.c(j).a(this);
        boolean bool = locali.c();
        if (bool)
        {
          int m = locali.b(paramLong, l);
          l = locali.a(l, m);
          arrayOfInt[j] = m;
        }
        int k;
        j += 1;
      }
    }
    return arrayOfInt;
  }
  
  public final long b(aa paramaa, long paramLong)
  {
    if (paramaa != null)
    {
      int i = 0;
      int j = paramaa.d();
      while (i < j)
      {
        int k = paramaa.d(i);
        long l1 = k;
        long l2 = 0L;
        boolean bool = l1 < l2;
        if (bool)
        {
          i locali = paramaa.c(i).a(this);
          long l3 = 1L;
          l1 *= l3;
          paramLong = locali.a(paramLong, l1);
        }
        i += 1;
      }
    }
    return paramLong;
  }
  
  public i c()
  {
    return q.a(j.a());
  }
  
  public c d()
  {
    d locald = d.a();
    i locali = c();
    return p.a(locald, locali);
  }
  
  public c e()
  {
    d locald = d.b();
    i locali = c();
    return p.a(locald, locali);
  }
  
  public i f()
  {
    return q.a(j.b());
  }
  
  public c g()
  {
    d locald = d.c();
    i locali = f();
    return p.a(locald, locali);
  }
  
  public c h()
  {
    d locald = d.d();
    i locali = f();
    return p.a(locald, locali);
  }
  
  public i i()
  {
    return q.a(j.c());
  }
  
  public c j()
  {
    d locald = d.e();
    i locali = i();
    return p.a(locald, locali);
  }
  
  public c k()
  {
    d locald = d.f();
    i locali = i();
    return p.a(locald, locali);
  }
  
  public i l()
  {
    return q.a(j.d());
  }
  
  public c m()
  {
    d locald = d.g();
    i locali = l();
    return p.a(locald, locali);
  }
  
  public c n()
  {
    d locald = d.h();
    i locali = l();
    return p.a(locald, locali);
  }
  
  public i o()
  {
    return q.a(j.e());
  }
  
  public c p()
  {
    d locald = d.i();
    i locali = l();
    return p.a(locald, locali);
  }
  
  public c q()
  {
    d locald = d.j();
    i locali = l();
    return p.a(locald, locali);
  }
  
  public c r()
  {
    d locald = d.k();
    i locali = o();
    return p.a(locald, locali);
  }
  
  public i s()
  {
    return q.a(j.f());
  }
  
  public c t()
  {
    d locald = d.l();
    i locali = s();
    return p.a(locald, locali);
  }
  
  public c u()
  {
    d locald = d.m();
    i locali = s();
    return p.a(locald, locali);
  }
  
  public c v()
  {
    d locald = d.n();
    i locali = s();
    return p.a(locald, locali);
  }
  
  public i w()
  {
    return q.a(j.g());
  }
  
  public c x()
  {
    d locald = d.o();
    i locali = w();
    return p.a(locald, locali);
  }
  
  public i y()
  {
    return q.a(j.h());
  }
  
  public c z()
  {
    d locald = d.p();
    i locali = y();
    return p.a(locald, locali);
  }
}

/* Location:
 * Qualified Name:     org.a.a.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */