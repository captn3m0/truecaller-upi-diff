package org.a.a.b;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import org.a.a.f;

final class q$a
  implements Serializable
{
  private static final long serialVersionUID = -6212696554273812441L;
  private transient f a;
  
  q$a(f paramf)
  {
    a = paramf;
  }
  
  private void readObject(ObjectInputStream paramObjectInputStream)
  {
    paramObjectInputStream = (f)paramObjectInputStream.readObject();
    a = paramObjectInputStream;
  }
  
  private Object readResolve()
  {
    return q.b(a);
  }
  
  private void writeObject(ObjectOutputStream paramObjectOutputStream)
  {
    f localf = a;
    paramObjectOutputStream.writeObject(localf);
  }
}

/* Location:
 * Qualified Name:     org.a.a.b.q.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */