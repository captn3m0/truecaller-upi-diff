package org.npci.upi.security.services;

public abstract interface ServiceConnectionStatusNotifier
{
  public abstract void serviceConnected(CLServices paramCLServices);
  
  public abstract void serviceDisconnected();
}

/* Location:
 * Qualified Name:     org.npci.upi.security.services.ServiceConnectionStatusNotifier
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */