package org.npci.upi.security.services;

import android.os.IBinder;
import android.os.Parcel;

class CLRemoteService$Stub$Proxy
  implements CLRemoteService
{
  private IBinder mRemote;
  
  CLRemoteService$Stub$Proxy(IBinder paramIBinder)
  {
    mRemote = paramIBinder;
  }
  
  public IBinder asBinder()
  {
    return mRemote;
  }
  
  public String getChallenge(String paramString1, String paramString2)
  {
    Parcel localParcel1 = Parcel.obtain();
    Parcel localParcel2 = Parcel.obtain();
    String str = "org.npci.upi.security.services.CLRemoteService";
    try
    {
      localParcel1.writeInterfaceToken(str);
      localParcel1.writeString(paramString1);
      localParcel1.writeString(paramString2);
      paramString1 = mRemote;
      int i = 1;
      str = null;
      paramString1.transact(i, localParcel1, localParcel2, 0);
      localParcel2.readException();
      paramString1 = localParcel2.readString();
      return paramString1;
    }
    finally
    {
      localParcel2.recycle();
      localParcel1.recycle();
    }
  }
  
  public void getCredential(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, CLResultReceiver paramCLResultReceiver)
  {
    Parcel localParcel1 = Parcel.obtain();
    Parcel localParcel2 = Parcel.obtain();
    String str = "org.npci.upi.security.services.CLRemoteService";
    try
    {
      localParcel1.writeInterfaceToken(str);
      localParcel1.writeString(paramString1);
      localParcel1.writeString(paramString2);
      localParcel1.writeString(paramString3);
      localParcel1.writeString(paramString4);
      localParcel1.writeString(paramString5);
      localParcel1.writeString(paramString6);
      localParcel1.writeString(paramString7);
      localParcel1.writeString(paramString8);
      if (paramCLResultReceiver != null) {
        paramString1 = paramCLResultReceiver.asBinder();
      } else {
        paramString1 = null;
      }
      localParcel1.writeStrongBinder(paramString1);
      paramString1 = mRemote;
      int i = 3;
      paramString3 = null;
      paramString1.transact(i, localParcel1, localParcel2, 0);
      localParcel2.readException();
      return;
    }
    finally
    {
      localParcel2.recycle();
      localParcel1.recycle();
    }
  }
  
  public String getInterfaceDescriptor()
  {
    return "org.npci.upi.security.services.CLRemoteService";
  }
  
  public boolean registerApp(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    Parcel localParcel1 = Parcel.obtain();
    Parcel localParcel2 = Parcel.obtain();
    String str = "org.npci.upi.security.services.CLRemoteService";
    try
    {
      localParcel1.writeInterfaceToken(str);
      localParcel1.writeString(paramString1);
      localParcel1.writeString(paramString2);
      localParcel1.writeString(paramString3);
      localParcel1.writeString(paramString4);
      paramString1 = mRemote;
      int i = 2;
      boolean bool = false;
      paramString3 = null;
      paramString1.transact(i, localParcel1, localParcel2, 0);
      localParcel2.readException();
      int j = localParcel2.readInt();
      if (j != 0) {
        bool = true;
      }
      return bool;
    }
    finally
    {
      localParcel2.recycle();
      localParcel1.recycle();
    }
  }
}

/* Location:
 * Qualified Name:     org.npci.upi.security.services.CLRemoteService.Stub.Proxy
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */