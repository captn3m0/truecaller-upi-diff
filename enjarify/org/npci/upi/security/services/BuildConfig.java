package org.npci.upi.security.services;

public final class BuildConfig
{
  public static final String APPLICATION_ID = "org.npci.upi.security.services";
  public static final String BUILD_TYPE = "release";
  public static final boolean DEBUG = false;
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "2.0";
}

/* Location:
 * Qualified Name:     org.npci.upi.security.services.BuildConfig
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */