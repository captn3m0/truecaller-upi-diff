package org.npci.upi.security.services;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;

public abstract class CLResultReceiver$Stub
  extends Binder
  implements CLResultReceiver
{
  private static final String DESCRIPTOR = "org.npci.upi.security.services.CLResultReceiver";
  static final int TRANSACTION_sendCredential = 1;
  static final int TRANSACTION_triggerOtp = 2;
  
  public CLResultReceiver$Stub()
  {
    attachInterface(this, "org.npci.upi.security.services.CLResultReceiver");
  }
  
  public static CLResultReceiver asInterface(IBinder paramIBinder)
  {
    if (paramIBinder == null) {
      return null;
    }
    Object localObject = paramIBinder.queryLocalInterface("org.npci.upi.security.services.CLResultReceiver");
    if (localObject != null)
    {
      boolean bool = localObject instanceof CLResultReceiver;
      if (bool) {
        return (CLResultReceiver)localObject;
      }
    }
    localObject = new org/npci/upi/security/services/CLResultReceiver$Stub$Proxy;
    ((CLResultReceiver.Stub.Proxy)localObject).<init>(paramIBinder);
    return (CLResultReceiver)localObject;
  }
  
  public IBinder asBinder()
  {
    return this;
  }
  
  public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
  {
    int i = 1598968902;
    boolean bool = true;
    if (paramInt1 != i)
    {
      i = 0;
      Object localObject1 = null;
      switch (paramInt1)
      {
      default: 
        return super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
      case 2: 
        localObject2 = "org.npci.upi.security.services.CLResultReceiver";
        paramParcel1.enforceInterface((String)localObject2);
        paramInt1 = paramParcel1.readInt();
        if (paramInt1 != 0)
        {
          localObject2 = Bundle.CREATOR.createFromParcel(paramParcel1);
          localObject1 = localObject2;
          localObject1 = (Bundle)localObject2;
        }
        triggerOtp((Bundle)localObject1);
        paramParcel2.writeNoException();
        return bool;
      }
      Object localObject2 = "org.npci.upi.security.services.CLResultReceiver";
      paramParcel1.enforceInterface((String)localObject2);
      paramInt1 = paramParcel1.readInt();
      if (paramInt1 != 0)
      {
        localObject2 = Bundle.CREATOR.createFromParcel(paramParcel1);
        localObject1 = localObject2;
        localObject1 = (Bundle)localObject2;
      }
      sendCredential((Bundle)localObject1);
      paramParcel2.writeNoException();
      return bool;
    }
    paramParcel2.writeString("org.npci.upi.security.services.CLResultReceiver");
    return bool;
  }
}

/* Location:
 * Qualified Name:     org.npci.upi.security.services.CLResultReceiver.Stub
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */