package org.npci.upi.security.services;

import android.os.Bundle;
import android.os.ResultReceiver;

class CLRemoteResultReceiver$1
  extends CLResultReceiver.Stub
{
  CLRemoteResultReceiver$1(CLRemoteResultReceiver paramCLRemoteResultReceiver) {}
  
  public void sendCredential(Bundle paramBundle)
  {
    CLRemoteResultReceiver.access$000(this$0).send(1, paramBundle);
  }
  
  public void triggerOtp(Bundle paramBundle)
  {
    CLRemoteResultReceiver.access$000(this$0).send(2, paramBundle);
  }
}

/* Location:
 * Qualified Name:     org.npci.upi.security.services.CLRemoteResultReceiver.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */