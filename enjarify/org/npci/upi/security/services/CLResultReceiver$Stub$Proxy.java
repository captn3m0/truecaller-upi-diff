package org.npci.upi.security.services;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;

class CLResultReceiver$Stub$Proxy
  implements CLResultReceiver
{
  private IBinder mRemote;
  
  CLResultReceiver$Stub$Proxy(IBinder paramIBinder)
  {
    mRemote = paramIBinder;
  }
  
  public IBinder asBinder()
  {
    return mRemote;
  }
  
  public String getInterfaceDescriptor()
  {
    return "org.npci.upi.security.services.CLResultReceiver";
  }
  
  public void sendCredential(Bundle paramBundle)
  {
    Parcel localParcel1 = Parcel.obtain();
    Parcel localParcel2 = Parcel.obtain();
    String str = "org.npci.upi.security.services.CLResultReceiver";
    try
    {
      localParcel1.writeInterfaceToken(str);
      int i = 1;
      if (paramBundle != null)
      {
        localParcel1.writeInt(i);
        paramBundle.writeToParcel(localParcel1, 0);
      }
      else
      {
        localParcel1.writeInt(0);
      }
      paramBundle = mRemote;
      paramBundle.transact(i, localParcel1, localParcel2, 0);
      localParcel2.readException();
      return;
    }
    finally
    {
      localParcel2.recycle();
      localParcel1.recycle();
    }
  }
  
  public void triggerOtp(Bundle paramBundle)
  {
    Parcel localParcel1 = Parcel.obtain();
    Parcel localParcel2 = Parcel.obtain();
    String str = "org.npci.upi.security.services.CLResultReceiver";
    try
    {
      localParcel1.writeInterfaceToken(str);
      str = null;
      if (paramBundle != null)
      {
        i = 1;
        localParcel1.writeInt(i);
        paramBundle.writeToParcel(localParcel1, 0);
      }
      else
      {
        localParcel1.writeInt(0);
      }
      paramBundle = mRemote;
      int i = 2;
      paramBundle.transact(i, localParcel1, localParcel2, 0);
      localParcel2.readException();
      return;
    }
    finally
    {
      localParcel2.recycle();
      localParcel1.recycle();
    }
  }
}

/* Location:
 * Qualified Name:     org.npci.upi.security.services.CLResultReceiver.Stub.Proxy
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */