package org.npci.upi.security.services;

import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.IBinder;
import android.os.RemoteException;

public class CLServices
{
  private static final Uri GET_CHALLENGE_URI = Uri.parse("content://org.npci.upi.security.pinactivitycomponent.clservices/getChallenge");
  private static final Uri GET_CREDENTIAL_URI = Uri.parse("content://org.npci.upi.security.pinactivitycomponent.clservices/getCredential");
  private static final String PROVIDER_NAME = "org.npci.upi.security.pinactivitycomponent.clservices";
  private static final Uri REGISTER_APP_URI = Uri.parse("content://org.npci.upi.security.pinactivitycomponent.clservices/registerApp");
  private static CLServices clServices = null;
  private CLRemoteService clRemoteService = null;
  private Context mContext;
  private ServiceConnectionStatusNotifier notifier;
  private ServiceConnection serviceConnection;
  
  private CLServices(Context paramContext, ServiceConnectionStatusNotifier paramServiceConnectionStatusNotifier)
  {
    Object localObject = new org/npci/upi/security/services/CLServices$1;
    ((CLServices.1)localObject).<init>(this);
    serviceConnection = ((ServiceConnection)localObject);
    mContext = paramContext;
    notifier = paramServiceConnectionStatusNotifier;
    paramContext = new android/content/Intent;
    paramContext.<init>();
    paramContext.setAction("org.npci.upi.security.services.CLRemoteService");
    paramServiceConnectionStatusNotifier = mContext.getPackageName();
    paramContext.setPackage(paramServiceConnectionStatusNotifier);
    paramServiceConnectionStatusNotifier = mContext;
    localObject = serviceConnection;
    paramServiceConnectionStatusNotifier.bindService(paramContext, (ServiceConnection)localObject, 1);
  }
  
  public static void initService(Context paramContext, ServiceConnectionStatusNotifier paramServiceConnectionStatusNotifier)
  {
    CLServices localCLServices = clServices;
    if (localCLServices == null)
    {
      localCLServices = new org/npci/upi/security/services/CLServices;
      localCLServices.<init>(paramContext, paramServiceConnectionStatusNotifier);
      clServices = localCLServices;
      return;
    }
    paramContext = new java/lang/RuntimeException;
    paramContext.<init>("Service already initiated");
    throw paramContext;
  }
  
  public String getChallenge(String paramString1, String paramString2)
  {
    CLServices.class.getName();
    String str = null;
    if (paramString1 != null)
    {
      Object localObject = paramString1.trim();
      boolean bool = ((String)localObject).isEmpty();
      if ((!bool) && (paramString2 != null))
      {
        localObject = paramString2.trim();
        bool = ((String)localObject).isEmpty();
        if (!bool)
        {
          try
          {
            localObject = clRemoteService;
            str = ((CLRemoteService)localObject).getChallenge(paramString1, paramString2);
          }
          catch (RemoteException paramString1)
          {
            paramString1.printStackTrace();
          }
          return str;
        }
      }
    }
    CLServices.class.getName();
    return null;
  }
  
  public void getCredential(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, CLRemoteResultReceiver paramCLRemoteResultReceiver)
  {
    Object localObject = CLServices.class;
    ((Class)localObject).getName();
    try
    {
      CLRemoteService localCLRemoteService = clRemoteService;
      localObject = paramCLRemoteResultReceiver.getBinder();
      CLResultReceiver localCLResultReceiver = CLResultReceiver.Stub.asInterface((IBinder)localObject);
      localCLRemoteService.getCredential(paramString1, paramString2, paramString3, paramString4, paramString5, paramString6, paramString7, paramString8, localCLResultReceiver);
      return;
    }
    catch (RemoteException localRemoteException)
    {
      localRemoteException;
    }
  }
  
  public boolean registerApp(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    CLServices.class.getName();
    boolean bool1 = false;
    if (paramString1 != null)
    {
      Object localObject = paramString1.trim();
      boolean bool2 = ((String)localObject).isEmpty();
      if ((!bool2) && (paramString2 != null))
      {
        localObject = paramString2.trim();
        bool2 = ((String)localObject).isEmpty();
        if ((!bool2) && (paramString3 != null))
        {
          localObject = paramString3.trim();
          bool2 = ((String)localObject).isEmpty();
          if ((!bool2) && (paramString4 != null))
          {
            localObject = paramString4.trim();
            bool2 = ((String)localObject).isEmpty();
            if (!bool2)
            {
              try
              {
                localObject = clRemoteService;
                bool1 = ((CLRemoteService)localObject).registerApp(paramString1, paramString2, paramString3, paramString4);
              }
              catch (RemoteException paramString1)
              {
                paramString1.printStackTrace();
              }
              return bool1;
            }
          }
        }
      }
    }
    CLServices.class.getName();
    return false;
  }
  
  public void unbindService()
  {
    Context localContext = mContext;
    ServiceConnection localServiceConnection = serviceConnection;
    localContext.unbindService(localServiceConnection);
  }
}

/* Location:
 * Qualified Name:     org.npci.upi.security.services.CLServices
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */