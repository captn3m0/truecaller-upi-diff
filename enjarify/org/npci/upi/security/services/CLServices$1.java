package org.npci.upi.security.services;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;

class CLServices$1
  implements ServiceConnection
{
  CLServices$1(CLServices paramCLServices) {}
  
  public void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
  {
    paramComponentName = this$0;
    paramIBinder = CLRemoteService.Stub.asInterface(paramIBinder);
    CLServices.access$002(paramComponentName, paramIBinder);
    paramComponentName = CLServices.access$200(this$0);
    paramIBinder = CLServices.access$100();
    paramComponentName.serviceConnected(paramIBinder);
  }
  
  public void onServiceDisconnected(ComponentName paramComponentName)
  {
    CLServices.access$002(this$0, null);
    CLServices.access$200(this$0).serviceDisconnected();
  }
}

/* Location:
 * Qualified Name:     org.npci.upi.security.services.CLServices.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */