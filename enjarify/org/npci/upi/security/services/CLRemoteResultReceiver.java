package org.npci.upi.security.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.ResultReceiver;

public class CLRemoteResultReceiver
  extends Service
{
  private IBinder mBinder;
  private ResultReceiver mResultReceiver;
  
  public CLRemoteResultReceiver(ResultReceiver paramResultReceiver)
  {
    CLRemoteResultReceiver.1 local1 = new org/npci/upi/security/services/CLRemoteResultReceiver$1;
    local1.<init>(this);
    mBinder = local1;
    mResultReceiver = paramResultReceiver;
  }
  
  public IBinder getBinder()
  {
    return mBinder;
  }
  
  public IBinder onBind(Intent paramIntent)
  {
    return mBinder;
  }
}

/* Location:
 * Qualified Name:     org.npci.upi.security.services.CLRemoteResultReceiver
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */