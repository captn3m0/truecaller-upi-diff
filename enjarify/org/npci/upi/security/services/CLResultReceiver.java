package org.npci.upi.security.services;

import android.os.Bundle;
import android.os.IInterface;

public abstract interface CLResultReceiver
  extends IInterface
{
  public abstract void sendCredential(Bundle paramBundle);
  
  public abstract void triggerOtp(Bundle paramBundle);
}

/* Location:
 * Qualified Name:     org.npci.upi.security.services.CLResultReceiver
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */