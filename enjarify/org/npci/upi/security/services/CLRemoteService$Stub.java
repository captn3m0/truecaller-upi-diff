package org.npci.upi.security.services;

import android.os.Binder;
import android.os.IBinder;
import android.os.Parcel;

public abstract class CLRemoteService$Stub
  extends Binder
  implements CLRemoteService
{
  private static final String DESCRIPTOR = "org.npci.upi.security.services.CLRemoteService";
  static final int TRANSACTION_getChallenge = 1;
  static final int TRANSACTION_getCredential = 3;
  static final int TRANSACTION_registerApp = 2;
  
  public CLRemoteService$Stub()
  {
    attachInterface(this, "org.npci.upi.security.services.CLRemoteService");
  }
  
  public static CLRemoteService asInterface(IBinder paramIBinder)
  {
    if (paramIBinder == null) {
      return null;
    }
    Object localObject = paramIBinder.queryLocalInterface("org.npci.upi.security.services.CLRemoteService");
    if (localObject != null)
    {
      boolean bool = localObject instanceof CLRemoteService;
      if (bool) {
        return (CLRemoteService)localObject;
      }
    }
    localObject = new org/npci/upi/security/services/CLRemoteService$Stub$Proxy;
    ((CLRemoteService.Stub.Proxy)localObject).<init>(paramIBinder);
    return (CLRemoteService)localObject;
  }
  
  public IBinder asBinder()
  {
    return this;
  }
  
  public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
  {
    int i = paramInt1;
    Object localObject1 = paramParcel1;
    int k = 1598968902;
    boolean bool = true;
    if (paramInt1 != k)
    {
      Object localObject2;
      Object localObject3;
      switch (paramInt1)
      {
      default: 
        return super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
      case 3: 
        paramParcel1.enforceInterface("org.npci.upi.security.services.CLRemoteService");
        localObject2 = paramParcel1.readString();
        localObject3 = paramParcel1.readString();
        Object localObject4 = paramParcel1.readString();
        Object localObject5 = paramParcel1.readString();
        Object localObject6 = paramParcel1.readString();
        Object localObject7 = paramParcel1.readString();
        Object localObject8 = paramParcel1.readString();
        Object localObject9 = paramParcel1.readString();
        CLResultReceiver localCLResultReceiver = CLResultReceiver.Stub.asInterface(paramParcel1.readStrongBinder());
        localObject10 = this;
        localObject1 = localObject2;
        localObject2 = localObject3;
        localObject3 = localObject4;
        localObject4 = localObject5;
        localObject5 = localObject6;
        localObject6 = localObject7;
        localObject7 = localObject8;
        localObject8 = localObject9;
        localObject9 = localCLResultReceiver;
        getCredential((String)localObject1, (String)localObject2, (String)localObject3, (String)localObject4, (String)localObject5, (String)localObject6, (String)localObject7, (String)localObject8, localCLResultReceiver);
        paramParcel2.writeNoException();
        return bool;
      case 2: 
        paramParcel1.enforceInterface("org.npci.upi.security.services.CLRemoteService");
        localObject10 = paramParcel1.readString();
        localObject2 = paramParcel1.readString();
        localObject3 = paramParcel1.readString();
        localObject1 = paramParcel1.readString();
        int j = registerApp((String)localObject10, (String)localObject2, (String)localObject3, (String)localObject1);
        paramParcel2.writeNoException();
        paramParcel2.writeInt(j);
        return bool;
      }
      paramParcel1.enforceInterface("org.npci.upi.security.services.CLRemoteService");
      Object localObject10 = paramParcel1.readString();
      localObject1 = paramParcel1.readString();
      localObject10 = getChallenge((String)localObject10, (String)localObject1);
      paramParcel2.writeNoException();
      paramParcel2.writeString((String)localObject10);
      return bool;
    }
    paramParcel2.writeString("org.npci.upi.security.services.CLRemoteService");
    return bool;
  }
}

/* Location:
 * Qualified Name:     org.npci.upi.security.services.CLRemoteService.Stub
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */