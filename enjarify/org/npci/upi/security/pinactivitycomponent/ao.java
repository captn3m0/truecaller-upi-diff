package org.npci.upi.security.pinactivitycomponent;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.v4.content.b;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import org.json.JSONArray;
import org.json.JSONObject;
import org.npci.upi.security.pinactivitycomponent.widget.FormItemPager;
import org.npci.upi.security.pinactivitycomponent.widget.FormItemView;
import org.npci.upi.security.pinactivitycomponent.widget.a;
import org.npci.upi.security.pinactivitycomponent.widget.m;

public class ao
  extends ae
  implements m
{
  private static final String s = ae.class.getSimpleName();
  LinearLayout q;
  LinearLayout r;
  private int t = 0;
  private Timer u = null;
  private Boolean v = null;
  private HashMap w;
  private boolean x;
  
  public ao()
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    w = localHashMap;
    x = false;
  }
  
  private void a(View paramView)
  {
    int i = R.id.main_inner_layout;
    paramView = (LinearLayout)paramView.findViewById(i);
    JSONArray localJSONArray = c;
    if (localJSONArray != null)
    {
      i = 0;
      localJSONArray = null;
      for (;;)
      {
        Object localObject1 = c;
        int j = ((JSONArray)localObject1).length();
        if (i < j) {
          try
          {
            localObject1 = c;
            localObject1 = ((JSONArray)localObject1).getJSONObject(i);
            Object localObject2 = "subtype";
            localObject2 = ((JSONObject)localObject1).getString((String)localObject2);
            Object localObject3 = "dLength";
            int k = ((JSONObject)localObject1).optInt((String)localObject3);
            if (k == 0)
            {
              k = 6;
            }
            else
            {
              localObject3 = "dLength";
              k = ((JSONObject)localObject1).optInt((String)localObject3);
            }
            Object localObject4 = "MPIN";
            boolean bool1 = ((String)localObject2).equals(localObject4);
            if (!bool1)
            {
              localObject4 = "NMPIN";
              bool1 = ((String)localObject2).equals(localObject4);
              if (!bool1)
              {
                localObject4 = "ATMPIN";
                bool1 = ((String)localObject4).equals(localObject2);
                if (!bool1)
                {
                  localObject4 = "OTP";
                  bool1 = ((String)localObject4).equals(localObject2);
                  if (!bool1)
                  {
                    localObject4 = "SMS";
                    bool1 = ((String)localObject4).equals(localObject2);
                    if (!bool1)
                    {
                      localObject4 = "EMAIL";
                      bool1 = ((String)localObject4).equals(localObject2);
                      if (!bool1)
                      {
                        localObject4 = "HOTP";
                        bool1 = ((String)localObject4).equals(localObject2);
                        if (!bool1)
                        {
                          localObject4 = "TOTP";
                          bool1 = ((String)localObject4).equals(localObject2);
                          if (!bool1) {
                            break label686;
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
            localObject4 = "NMPIN";
            bool1 = ((String)localObject2).equals(localObject4);
            if (!bool1)
            {
              localObject4 = "MPIN";
              bool1 = ((String)localObject2).equals(localObject4);
              if (bool1)
              {
                bool1 = e();
                if (bool1) {}
              }
              else
              {
                localObject4 = "";
                String str = "MPIN";
                boolean bool2 = ((String)localObject2).equals(str);
                if (bool2) {}
                for (int n = R.string.npci_mpin_title;; i1 = R.string.npci_atm_title)
                {
                  localObject4 = getString(n);
                  break label528;
                  str = "OTP";
                  bool2 = str.equals(localObject2);
                  if (bool2) {
                    break;
                  }
                  str = "SMS";
                  bool2 = str.equals(localObject2);
                  if (bool2) {
                    break;
                  }
                  str = "EMAIL";
                  bool2 = str.equals(localObject2);
                  if (bool2) {
                    break;
                  }
                  str = "HOTP";
                  bool2 = str.equals(localObject2);
                  if (bool2) {
                    break;
                  }
                  str = "TOTP";
                  bool2 = str.equals(localObject2);
                  if (bool2) {
                    break;
                  }
                  str = "ATMPIN";
                  boolean bool3 = str.equals(localObject2);
                  if (!bool3) {
                    break label528;
                  }
                }
                int i1 = R.string.npci_otp_title;
                localObject4 = getString(i1);
                g = i;
                localObject2 = getActivity();
                boolean bool4 = localObject2 instanceof GetCredential;
                if (bool4)
                {
                  localObject2 = getActivity();
                  localObject2 = (GetCredential)localObject2;
                  bool4 = ((GetCredential)localObject2).checkSMSReadPermission();
                  if (bool4) {
                    b(k);
                  }
                }
                label528:
                localObject2 = a((String)localObject4, i, k);
                ((FormItemView)localObject2).setFormDataTag(localObject1);
              }
            }
            for (localObject1 = f;; localObject1 = f)
            {
              ((ArrayList)localObject1).add(localObject2);
              paramView.addView((View)localObject2);
              break;
              int i2 = R.string.npci_set_mpin_title;
              localObject2 = getString(i2);
              localObject2 = a((String)localObject2, i, k);
              int m = R.string.npci_confirm_mpin_title;
              localObject4 = getString(m);
              localObject3 = a((String)localObject4, i, k);
              localObject4 = new java/util/ArrayList;
              ((ArrayList)localObject4).<init>();
              ((ArrayList)localObject4).add(localObject2);
              ((ArrayList)localObject4).add(localObject3);
              localObject2 = new org/npci/upi/security/pinactivitycomponent/widget/FormItemPager;
              localObject3 = getActivity();
              ((FormItemPager)localObject2).<init>((Context)localObject3);
              ((FormItemPager)localObject2).a((ArrayList)localObject4, this);
              ((FormItemPager)localObject2).setFormDataTag(localObject1);
            }
          }
          catch (Exception localException)
          {
            label686:
            i += 1;
          }
        }
      }
    }
  }
  
  private void d()
  {
    int i = g;
    int k = -1;
    int n;
    if (i != k)
    {
      localObject1 = f;
      k = g;
      localObject1 = ((ArrayList)localObject1).get(k);
      bool1 = localObject1 instanceof FormItemView;
      if (bool1)
      {
        localObject1 = f;
        k = g;
        localObject1 = (FormItemView)((ArrayList)localObject1).get(k);
        str = ((FormItemView)localObject1).getInputValue();
        if (str != null)
        {
          k = str.length();
          n = ((FormItemView)localObject1).getInputLength();
          if (k == n) {}
        }
        else
        {
          k = R.string.invalid_otp;
          str = getString(k);
          b((View)localObject1, str);
          return;
        }
      }
    }
    boolean bool1 = false;
    Object localObject1 = null;
    k = 0;
    String str = null;
    Object localObject2;
    Object localObject3;
    int j;
    for (;;)
    {
      localObject2 = f;
      n = ((ArrayList)localObject2).size();
      if (k >= n) {
        break;
      }
      localObject2 = f.get(k);
      boolean bool2 = localObject2 instanceof FormItemView;
      if (bool2)
      {
        localObject2 = (FormItemView)f.get(k);
        localObject3 = ((FormItemView)localObject2).getInputValue();
        int i2 = ((String)localObject3).length();
        int i3 = ((FormItemView)localObject2).getInputLength();
        if (i2 != i3)
        {
          j = R.string.componentMessage;
          localObject1 = getString(j);
          b((View)localObject2, (String)localObject1);
          return;
        }
      }
      k += 1;
    }
    int m = x;
    if (m == 0)
    {
      m = 1;
      x = m;
      for (;;)
      {
        localObject2 = f;
        int i1 = ((ArrayList)localObject2).size();
        if (j >= i1) {
          break;
        }
        try
        {
          localObject2 = f;
          localObject2 = ((ArrayList)localObject2).get(j);
          localObject2 = (a)localObject2;
          localObject2 = ((a)localObject2).getFormDataTag();
          localObject2 = (JSONObject)localObject2;
          localObject3 = "type";
          localObject3 = ((JSONObject)localObject2).getString((String)localObject3);
          Object localObject4 = "subtype";
          localObject2 = ((JSONObject)localObject2).getString((String)localObject4);
          localObject4 = b;
          Object localObject5 = "credential";
          Object localObject6 = f;
          localObject6 = ((ArrayList)localObject6).get(j);
          localObject6 = (a)localObject6;
          localObject6 = ((a)localObject6).getInputValue();
          ((JSONObject)localObject4).put((String)localObject5, localObject6);
          localObject4 = p;
          localObject4 = (GetCredential)localObject4;
          localObject4 = ((GetCredential)localObject4).getCLContext();
          localObject4 = ((c)localObject4).a();
          localObject5 = b;
          localObject4 = ((aq)localObject4).a((JSONObject)localObject5);
          localObject5 = p;
          localObject5 = (GetCredential)localObject5;
          localObject5 = ((GetCredential)localObject5).getCLContext();
          localObject5 = ((c)localObject5).b();
          localObject6 = b;
          localObject3 = ((j)localObject5).a((String)localObject4, (String)localObject3, (String)localObject2, (JSONObject)localObject6);
          if (localObject3 != null)
          {
            localObject4 = w;
            localObject3 = w.a(localObject3);
            ((HashMap)localObject4).put(localObject2, localObject3);
          }
        }
        catch (Exception localException)
        {
          localObject3 = s;
          ad.a((String)localObject3, localException);
        }
        j += 1;
      }
      localObject1 = new android/os/Bundle;
      ((Bundle)localObject1).<init>();
      localObject3 = w;
      ((Bundle)localObject1).putSerializable("credBlocks", (Serializable)localObject3);
      ResultReceiver localResultReceiver = ((GetCredential)p).getCLContext().d();
      localResultReceiver.send(m, (Bundle)localObject1);
      localObject1 = ((GetCredential)p).getCLContext().c();
      ((Activity)localObject1).finish();
    }
  }
  
  private boolean e()
  {
    Object localObject1 = v;
    if (localObject1 != null) {
      return ((Boolean)localObject1).booleanValue();
    }
    localObject1 = c;
    if (localObject1 != null)
    {
      localObject1 = new java/util/ArrayList;
      ((ArrayList)localObject1).<init>();
      int i = 0;
      String str1 = null;
      for (;;)
      {
        Object localObject2 = c;
        int j = ((JSONArray)localObject2).length();
        if (i >= j) {
          break;
        }
        try
        {
          localObject2 = c;
          localObject2 = ((JSONArray)localObject2).getJSONObject(i);
          str2 = "subtype";
          localObject2 = ((JSONObject)localObject2).getString(str2);
          if (localObject2 != null) {
            ((List)localObject1).add(localObject2);
          }
        }
        catch (Exception localException)
        {
          String str2 = s;
          ad.a(str2, localException);
        }
        i += 1;
      }
      str1 = "OTP";
      boolean bool1 = ((List)localObject1).contains(str1);
      if (!bool1)
      {
        str1 = "SMS";
        bool1 = ((List)localObject1).contains(str1);
        if (!bool1)
        {
          str1 = "EMAIL";
          bool1 = ((List)localObject1).contains(str1);
          if (!bool1)
          {
            str1 = "HOTP";
            bool1 = ((List)localObject1).contains(str1);
            if (!bool1)
            {
              str1 = "TOTP";
              bool1 = ((List)localObject1).contains(str1);
              if (!bool1) {
                break label228;
              }
              str1 = "MPIN";
              boolean bool2 = ((List)localObject1).contains(str1);
              if (!bool2) {
                break label228;
              }
            }
          }
        }
      }
    }
    label228:
    for (localObject1 = Boolean.TRUE;; localObject1 = Boolean.FALSE)
    {
      v = ((Boolean)localObject1);
      return v.booleanValue();
    }
  }
  
  private void f()
  {
    int i = g;
    int m = -1;
    if (i != m)
    {
      localObject1 = f;
      m = g;
      localObject1 = ((ArrayList)localObject1).get(m);
      j = localObject1 instanceof FormItemView;
      if (j != 0)
      {
        localObject1 = f;
        m = g;
        localObject1 = (FormItemView)((ArrayList)localObject1).get(m);
        a((FormItemView)localObject1);
        ((FormItemView)localObject1).setNonMaskedField();
      }
    }
    int j = 0;
    Object localObject1 = null;
    ArrayList localArrayList = f;
    m = localArrayList.size();
    while (j < m)
    {
      int n = g;
      if (j != n)
      {
        a locala = (a)f.get(j);
        Object localObject2 = getActivity();
        int i1 = R.drawable.ic_visibility_on;
        Drawable localDrawable1 = b.a((Context)localObject2, i1);
        localObject2 = getActivity();
        i1 = R.drawable.ic_visibility_off;
        Drawable localDrawable2 = b.a((Context)localObject2, i1);
        int i2 = R.string.action_hide;
        Object localObject3 = getString(i2);
        i2 = R.string.action_show;
        String str = getString(i2);
        ap localap = new org/npci/upi/security/pinactivitycomponent/ap;
        localObject2 = localap;
        localap.<init>(this, locala, (String)localObject3, str, localDrawable2, localDrawable1);
        boolean bool1 = true;
        boolean bool2 = true;
        localObject2 = locala;
        localObject3 = localap;
        locala.a(str, localDrawable1, localap, 0, bool1, bool2);
      }
      int k;
      j += 1;
    }
  }
  
  public void a()
  {
    int i = t;
    ArrayList localArrayList = f;
    int k = localArrayList.size() + -1;
    if (i < k)
    {
      Object localObject = f;
      k = t + 1;
      localObject = (a)((ArrayList)localObject).get(k);
      boolean bool = ((a)localObject).a();
      if (bool)
      {
        int j = t + 1;
        t = j;
        j = t;
        localArrayList = f;
        k = localArrayList.size() + -1;
        if (j >= k) {
          d();
        }
      }
      return;
    }
    d();
  }
  
  public void a(int paramInt)
  {
    Object localObject = f.get(paramInt);
    boolean bool = localObject instanceof FormItemPager;
    if (!bool) {
      t = paramInt;
    }
  }
  
  public void a(int paramInt, String paramString)
  {
    int i = g;
    int j = -1;
    if (i != j)
    {
      i = g;
      if (i == paramInt)
      {
        Object localObject = f;
        i = g;
        localObject = ((ArrayList)localObject).get(i);
        paramInt = localObject instanceof FormItemView;
        if (paramInt != 0)
        {
          localObject = u;
          a((Timer)localObject);
          localObject = f;
          i = g;
          ((FormItemView)((ArrayList)localObject).get(i)).a(false);
          localObject = f;
          j = g;
          localObject = (FormItemView)((ArrayList)localObject).get(j);
          String str = "";
          ((FormItemView)localObject).a(str, null, false, false);
          localObject = f;
          i = g;
          localObject = (FormItemView)((ArrayList)localObject).get(i);
          paramString = getActivity();
          j = R.drawable.ic_tick_ok;
          paramString = b.a(paramString, j);
          j = 1;
          ((FormItemView)localObject).a(paramString, j);
        }
      }
    }
  }
  
  public void a(View paramView, String paramString)
  {
    b(paramView, paramString);
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    int i = R.layout.fragment_pin;
    return paramLayoutInflater.inflate(i, paramViewGroup, false);
  }
  
  public void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    int i = R.id.main_inner_layout;
    paramBundle = (LinearLayout)paramView.findViewById(i);
    r = paramBundle;
    i = R.id.main_layout;
    paramBundle = (LinearLayout)paramView.findViewById(i);
    q = paramBundle;
    b();
    a(paramView);
    f();
  }
}

/* Location:
 * Qualified Name:     org.npci.upi.security.pinactivitycomponent.ao
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */