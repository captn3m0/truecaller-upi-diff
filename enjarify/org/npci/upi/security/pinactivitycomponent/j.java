package org.npci.upi.security.pinactivitycomponent;

import in.org.npci.commonlibrary.Data;
import in.org.npci.commonlibrary.Message;
import in.org.npci.commonlibrary.e;
import in.org.npci.commonlibrary.f;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONException;
import org.json.JSONObject;

public class j
{
  private e a;
  private String b;
  private i c;
  
  public j(e parame, i parami, String paramString)
  {
    a = parame;
    b = paramString;
    c = parami;
  }
  
  private Message a(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6)
  {
    Message localMessage = null;
    try
    {
      e locale = a;
      String str = b;
      localMessage = locale.a(str, paramString4, paramString5, paramString1, paramString6);
      localMessage.setType(paramString2);
      localMessage.setSubType(paramString3);
      paramString1 = localMessage.getData();
      paramString2 = new java/lang/StringBuilder;
      paramString3 = "2.0|";
      paramString2.<init>(paramString3);
      paramString3 = localMessage.getData();
      paramString3 = paramString3.getEncryptedBase64String();
      paramString2.append(paramString3);
      paramString2 = paramString2.toString();
      paramString1.setEncryptedBase64String(paramString2);
    }
    catch (f paramString1)
    {
      paramString1.printStackTrace();
    }
    return localMessage;
  }
  
  public Message a(String paramString1, String paramString2, String paramString3, JSONObject paramJSONObject)
  {
    Object localObject1 = null;
    String str1 = "txnId";
    try
    {
      String str2 = paramJSONObject.getString(str1);
      str1 = "credential";
      String str3 = paramJSONObject.getString(str1);
      str1 = "appId";
      str1 = paramJSONObject.getString(str1);
      Object localObject2 = "deviceId";
      localObject2 = paramJSONObject.getString((String)localObject2);
      Object localObject3 = "mobileNumber";
      paramJSONObject = paramJSONObject.getString((String)localObject3);
      localObject3 = "DBH in encryptor";
      Object localObject4 = c;
      if (localObject4 == null) {
        localObject4 = "null";
      } else {
        localObject4 = localObject4.toString();
      }
      ad.b((String)localObject3, (String)localObject4);
      localObject3 = c;
      String str4 = ((i)localObject3).a(str1, (String)localObject2, paramJSONObject);
      ad.b("K0 in encryptor", str4);
      paramString1 = Pattern.compile("\\{([^}]*)\\}").matcher(paramString1);
      paramJSONObject = new java/lang/StringBuffer;
      paramJSONObject.<init>();
      boolean bool = paramString1.find();
      if (bool)
      {
        localObject1 = paramString1.group();
        int i = ((String)localObject1).length();
        int j = 1;
        i -= j;
        localObject3 = ((String)localObject1).substring(j, i);
        localObject2 = this;
        localObject4 = paramString2;
        localObject1 = a((String)localObject3, paramString2, paramString3, str2, str3, str4);
        paramString2 = ((Message)localObject1).getData().getEncryptedBase64String();
        paramString3 = "\n";
        str1 = "";
        paramString2 = Matcher.quoteReplacement(paramString2.replaceAll(paramString3, str1));
        paramString1.appendReplacement(paramJSONObject, paramString2);
      }
      int k = paramJSONObject.length();
      if (k > 0) {
        paramString1.appendTail(paramJSONObject);
      }
      if (localObject1 != null)
      {
        paramString1 = paramJSONObject.toString();
        paramJSONObject = String.valueOf(paramString1);
        paramString3 = "Encrypted Data: ".concat(paramJSONObject);
        ad.b("CommonLibrary", paramString3);
        paramString2 = ((Message)localObject1).getData();
        paramString2.setEncryptedBase64String(paramString1);
      }
      return (Message)localObject1;
    }
    catch (JSONException localJSONException)
    {
      localJSONException;
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     org.npci.upi.security.pinactivitycomponent.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */