package org.npci.upi.security.pinactivitycomponent;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.graphics.drawable.TransitionDrawable;
import android.support.v4.view.r;
import android.view.View;

class t
  extends AnimatorListenerAdapter
{
  t(GetCredential paramGetCredential, boolean paramBoolean, int paramInt) {}
  
  public void onAnimationEnd(Animator paramAnimator)
  {
    super.onAnimationEnd(paramAnimator);
    boolean bool = a;
    if (!bool)
    {
      paramAnimator = GetCredential.access$600(c);
      int i = 8;
      paramAnimator.setVisibility(i);
      GetCredential.access$700(c).setVisibility(i);
      paramAnimator = GetCredential.access$500(c);
      paramAnimator.resetTransition();
    }
  }
  
  public void onAnimationStart(Animator paramAnimator)
  {
    super.onAnimationStart(paramAnimator);
    boolean bool = a;
    int i = 300;
    float f1 = 4.2E-43F;
    if (bool)
    {
      GetCredential.access$500(c).startTransition(i);
      GetCredential.access$600(c).setVisibility(0);
      GetCredential.access$700(c).setVisibility(0);
      paramAnimator = GetCredential.access$600(c);
      float f2 = r.p(paramAnimator);
      i = 0;
      f1 = 0.0F;
      bool = f2 < 0.0F;
      if (!bool)
      {
        paramAnimator = GetCredential.access$600(c);
        f1 = b * -1;
        paramAnimator.setY(f1);
      }
    }
    else
    {
      paramAnimator = GetCredential.access$500(c);
      paramAnimator.reverseTransition(i);
    }
  }
}

/* Location:
 * Qualified Name:     org.npci.upi.security.pinactivitycomponent.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */