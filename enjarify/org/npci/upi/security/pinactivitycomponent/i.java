package org.npci.upi.security.pinactivitycomponent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.util.ArrayList;
import java.util.List;

public class i
  extends SQLiteOpenHelper
{
  public i(Context paramContext)
  {
    super(paramContext, "contactsManager", null, 1);
  }
  
  public String a(String paramString1, String paramString2, String paramString3)
  {
    paramString1 = a();
    if (paramString1 != null)
    {
      boolean bool = paramString1.isEmpty();
      if (!bool)
      {
        bool = false;
        return ((ar)paramString1.get(0)).a();
      }
    }
    paramString1 = null;
    return paramString1;
  }
  
  public List a()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Object localObject = getWritableDatabase();
    int i = 0;
    String str = null;
    Cursor localCursor = ((SQLiteDatabase)localObject).rawQuery("SELECT  * FROM contacts", null);
    boolean bool = localCursor.moveToFirst();
    if (bool) {
      do
      {
        localObject = new org/npci/upi/security/pinactivitycomponent/ar;
        ((ar)localObject).<init>();
        str = localCursor.getString(1);
        ((ar)localObject).a(str);
        str = localCursor.getString(2);
        ((ar)localObject).b(str);
        i = 3;
        str = localCursor.getString(i);
        ((ar)localObject).c(str);
        localArrayList.add(localObject);
        bool = localCursor.moveToNext();
      } while (bool);
    }
    return localArrayList;
  }
  
  void a(ar paramar)
  {
    SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
    ContentValues localContentValues = new android/content/ContentValues;
    localContentValues.<init>();
    String str = paramar.a();
    localContentValues.put("k0", str);
    str = paramar.b();
    localContentValues.put("token", str);
    paramar = paramar.c();
    localContentValues.put("date", paramar);
    localSQLiteDatabase.insert("contacts", null, localContentValues);
    localSQLiteDatabase.close();
  }
  
  public int b(ar paramar)
  {
    SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
    ContentValues localContentValues = new android/content/ContentValues;
    localContentValues.<init>();
    String str = paramar.a();
    localContentValues.put("k0", str);
    str = paramar.b();
    localContentValues.put("token", str);
    str = paramar.c();
    localContentValues.put("date", str);
    String[] arrayOfString = new String[1];
    paramar = paramar.a();
    arrayOfString[0] = paramar;
    return localSQLiteDatabase.update("contacts", localContentValues, "k0 = ?", arrayOfString);
  }
  
  public String b()
  {
    String str = "";
    List localList = a();
    if (localList != null)
    {
      boolean bool = localList.isEmpty();
      if (!bool) {
        str = ((ar)localList.get(0)).b();
      }
    }
    return str;
  }
  
  public void c()
  {
    ad.b("DB Handler", "Deleting all");
    SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
    localSQLiteDatabase.execSQL("delete from contacts");
    localSQLiteDatabase.close();
  }
  
  public void onCreate(SQLiteDatabase paramSQLiteDatabase)
  {
    paramSQLiteDatabase.execSQL("CREATE TABLE contacts(id INTEGER PRIMARY KEY,k0 TEXT,token TEXT,date TEXT)");
  }
  
  public void onUpgrade(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
    paramSQLiteDatabase.execSQL("DROP TABLE IF EXISTS contacts");
    onCreate(paramSQLiteDatabase);
  }
}

/* Location:
 * Qualified Name:     org.npci.upi.security.pinactivitycomponent.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */