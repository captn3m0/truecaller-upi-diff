package org.npci.upi.security.pinactivitycomponent;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import org.npci.upi.security.services.CLRemoteService.Stub;
import org.npci.upi.security.services.CLResultReceiver;

public class CLRemoteServiceImpl
  extends Service
{
  private CLRemoteService.Stub a = null;
  private g b = null;
  
  private Bundle a(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, CLResultReceiver paramCLResultReceiver)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putString("keyCode", paramString1);
    localBundle.putString("keyXmlPayload", paramString2);
    localBundle.putString("controls", paramString3);
    localBundle.putString("configuration", paramString4);
    localBundle.putString("salt", paramString5);
    localBundle.putString("payInfo", paramString6);
    localBundle.putString("trust", paramString7);
    localBundle.putString("languagePref", paramString8);
    paramString1 = new org/npci/upi/security/pinactivitycomponent/CLServerResultReceiver;
    paramString1.<init>(paramCLResultReceiver);
    v.a(paramString1);
    return localBundle;
  }
  
  public IBinder onBind(Intent paramIntent)
  {
    paramIntent = a;
    Context localContext;
    if (paramIntent == null)
    {
      paramIntent = new org/npci/upi/security/pinactivitycomponent/f;
      localContext = getBaseContext();
      paramIntent.<init>(this, localContext, null);
      a = paramIntent;
    }
    try
    {
      paramIntent = new org/npci/upi/security/pinactivitycomponent/g;
      localContext = getBaseContext();
      paramIntent.<init>(localContext);
      b = paramIntent;
      return a;
    }
    catch (Exception localException)
    {
      paramIntent = new java/lang/RuntimeException;
      paramIntent.<init>("Could not initialize service provider");
      throw paramIntent;
    }
  }
}

/* Location:
 * Qualified Name:     org.npci.upi.security.pinactivitycomponent.CLRemoteServiceImpl
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */