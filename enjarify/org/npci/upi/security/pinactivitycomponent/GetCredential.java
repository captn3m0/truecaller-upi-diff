package org.npci.upi.security.pinactivitycomponent;

import android.animation.Animator.AnimatorListener;
import android.animation.TimeInterpolator;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.drawable.TransitionDrawable;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.j;
import android.support.v4.view.r;
import android.support.v7.app.ActionBarActivity;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONObject;

public class GetCredential
  extends ActionBarActivity
{
  private static final String TAG = "GetCredential";
  private c clContext;
  private JSONObject configuration = null;
  private final Context context;
  private JSONObject controls = null;
  private JSONArray credAllowed = null;
  private ae currentFragment;
  private Thread.UncaughtExceptionHandler defaultHandler;
  private boolean doubleBackToExitPressedOnce;
  private v inputAnalyzer;
  private String languagePref;
  private int mActivityHeight;
  private ImageView mTransactionBarArrow;
  private View mTransactionDetailScroller;
  private View mTransactionDetailSpace;
  private TransitionDrawable mTransitionDrawable;
  private int numDigitsOfOTP;
  private JSONArray payInfoArray;
  private JSONObject salt = null;
  private u smsReceiver;
  
  public GetCredential()
  {
    JSONArray localJSONArray = new org/json/JSONArray;
    localJSONArray.<init>();
    payInfoArray = localJSONArray;
    languagePref = "en_US";
    currentFragment = null;
    context = this;
    doubleBackToExitPressedOnce = false;
    numDigitsOfOTP = 0;
    defaultHandler = null;
  }
  
  private void appInit()
  {
    Object localObject1 = new org/npci/upi/security/pinactivitycomponent/v;
    ((v)localObject1).<init>();
    inputAnalyzer = ((v)localObject1);
    try
    {
      localObject1 = new org/npci/upi/security/pinactivitycomponent/c;
      Object localObject2 = getApplicationContext();
      v localv = inputAnalyzer;
      ((c)localObject1).<init>((Context)localObject2, localv, this);
      clContext = ((c)localObject1);
      localObject1 = inputAnalyzer;
      localObject2 = getIntent();
      localObject2 = ((Intent)localObject2).getExtras();
      ((v)localObject1).a((Bundle)localObject2, this);
      return;
    }
    catch (d locald)
    {
      locald.a();
      return;
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
  }
  
  private void goBack()
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putString("error", "USER_ABORTED");
    getCLContext().d().send(0, localBundle);
    finish();
  }
  
  private boolean isATMPinFlow()
  {
    String str1 = "SMS|EMAIL|HOTP|TOTP";
    String[] tmp7_4 = new String[3];
    String[] tmp8_7 = tmp7_4;
    String[] tmp8_7 = tmp7_4;
    tmp8_7[0] = "ATMPIN";
    tmp8_7[1] = str1;
    tmp8_7[2] = "MPIN";
    String[] arrayOfString1 = tmp8_7;
    int i = 3;
    String[] arrayOfString2 = new String[i];
    JSONArray localJSONArray = credAllowed;
    if (localJSONArray != null)
    {
      int j = localJSONArray.length();
      if (j == i)
      {
        i = 0;
        str1 = null;
        j = 0;
        localJSONArray = null;
        int k = 0;
        int m = 0;
        boolean bool2;
        for (;;)
        {
          Object localObject = credAllowed;
          int n = ((JSONArray)localObject).length();
          bool2 = true;
          if (i >= n) {
            break;
          }
          try
          {
            localObject = credAllowed;
            localObject = ((JSONArray)localObject).get(i);
            localObject = (JSONObject)localObject;
            String str2 = "subtype";
            String str3 = "";
            localObject = ((JSONObject)localObject).optString(str2, str3);
            arrayOfString2[i] = localObject;
            localObject = arrayOfString2[i];
            str2 = arrayOfString1[0];
            boolean bool1 = ((String)localObject).matches(str2);
            if (bool1) {
              j = 1;
            }
            localObject = arrayOfString2[i];
            str2 = arrayOfString1[bool2];
            bool1 = ((String)localObject).matches(str2);
            if (bool1) {
              k = 1;
            }
            localObject = arrayOfString2[i];
            int i1 = 2;
            str2 = arrayOfString1[i1];
            bool1 = ((String)localObject).matches(str2);
            if (bool1) {
              m = 1;
            }
          }
          catch (Exception localException)
          {
            String str4 = TAG;
            ad.a(str4, localException);
          }
          i += 1;
        }
        if ((j != 0) && (k != 0) && (m != 0)) {
          return bool2;
        }
      }
    }
    return false;
  }
  
  private boolean isTransactionDetailsExpanded()
  {
    View localView = mTransactionDetailScroller;
    int i = localView.getVisibility();
    return i == 0;
  }
  
  private int pix(float paramFloat)
  {
    float f = getResourcesgetDisplayMetricsdensityDpi / 160;
    return (int)(paramFloat * f);
  }
  
  private void readArguments()
  {
    Object localObject1 = getIntent().getExtras();
    if (localObject1 != null)
    {
      Object localObject2 = "configuration";
      try
      {
        localObject2 = ((Bundle)localObject1).getString((String)localObject2);
        Object localObject3;
        if (localObject2 != null)
        {
          localObject3 = new org/json/JSONObject;
          ((JSONObject)localObject3).<init>((String)localObject2);
          configuration = ((JSONObject)localObject3);
        }
        localObject2 = "controls";
        localObject2 = ((Bundle)localObject1).getString((String)localObject2);
        if (localObject2 != null)
        {
          localObject3 = new org/json/JSONObject;
          ((JSONObject)localObject3).<init>((String)localObject2);
          controls = ((JSONObject)localObject3);
          localObject2 = controls;
          if (localObject2 != null)
          {
            localObject2 = controls;
            localObject3 = "CredAllowed";
            localObject2 = ((JSONObject)localObject2).getString((String)localObject3);
            if (localObject2 != null)
            {
              localObject3 = new org/json/JSONArray;
              ((JSONArray)localObject3).<init>((String)localObject2);
              credAllowed = ((JSONArray)localObject3);
            }
          }
        }
        localObject2 = "salt";
        localObject2 = ((Bundle)localObject1).getString((String)localObject2);
        if (localObject2 != null)
        {
          localObject3 = new org/json/JSONObject;
          ((JSONObject)localObject3).<init>((String)localObject2);
          salt = ((JSONObject)localObject3);
        }
        localObject2 = "payInfo";
        localObject2 = ((Bundle)localObject1).getString((String)localObject2);
        if (localObject2 != null)
        {
          localObject3 = new org/json/JSONArray;
          ((JSONArray)localObject3).<init>((String)localObject2);
          payInfoArray = ((JSONArray)localObject3);
        }
        localObject2 = "languagePref";
        localObject1 = ((Bundle)localObject1).getString((String)localObject2);
        if (localObject1 != null)
        {
          languagePref = ((String)localObject1);
          localObject1 = languagePref;
          localObject2 = "_";
          localObject1 = ((String)localObject1).split((String)localObject2);
          localObject2 = new java/util/Locale;
          localObject3 = languagePref;
          ((Locale)localObject2).<init>((String)localObject3);
          int i = localObject1.length;
          int j = 2;
          if (i == j)
          {
            localObject2 = new java/util/Locale;
            i = 0;
            localObject3 = null;
            localObject3 = localObject1[0];
            j = 1;
            localObject1 = localObject1[j];
            ((Locale)localObject2).<init>((String)localObject3, (String)localObject1);
          }
          Locale.setDefault((Locale)localObject2);
          localObject1 = new android/content/res/Configuration;
          ((Configuration)localObject1).<init>();
          locale = ((Locale)localObject2);
          localObject2 = getBaseContext();
          localObject2 = ((Context)localObject2).getResources();
          localObject3 = getBaseContext();
          localObject3 = ((Context)localObject3).getResources();
          localObject3 = ((Resources)localObject3).getDisplayMetrics();
          ((Resources)localObject2).updateConfiguration((Configuration)localObject1, (DisplayMetrics)localObject3);
        }
        return;
      }
      catch (Exception localException)
      {
        localObject2 = TAG;
        ad.a((String)localObject2, localException);
      }
    }
  }
  
  private void registerSMSReceiver()
  {
    IntentFilter localIntentFilter = new android/content/IntentFilter;
    localIntentFilter.<init>();
    Object localObject = "android.provider.Telephony.SMS_RECEIVED";
    try
    {
      localIntentFilter.addAction((String)localObject);
      int i = 999;
      localIntentFilter.setPriority(i);
      localObject = smsReceiver;
      registerReceiver((BroadcastReceiver)localObject, localIntentFilter);
      return;
    }
    finally
    {
      ad.a(TAG, "Failed to register SMS broadcast receiver (Ignoring)");
    }
  }
  
  private void setupKeyboard()
  {
    int i = R.id.fragmentTelKeyboard;
    Keypad localKeypad = (Keypad)findViewById(i);
    if (localKeypad != null)
    {
      o localo = new org/npci/upi/security/pinactivitycomponent/o;
      localo.<init>(this);
      localKeypad.setOnKeyPressCallback(localo);
    }
  }
  
  private void toggleTransactionDetails(boolean paramBoolean)
  {
    int i = 300;
    int j = 1127481344;
    float f1 = 180.0F;
    float f2 = 0.0F;
    Object localObject = null;
    ImageView localImageView;
    if (paramBoolean)
    {
      localImageView = mTransactionBarArrow;
      rotateView(0.0F, f1, i, localImageView);
    }
    else
    {
      localImageView = mTransactionBarArrow;
      rotateView(f1, 0.0F, i, localImageView);
    }
    i = Build.VERSION.SDK_INT;
    j = 14;
    f1 = 2.0E-44F;
    if (i > j)
    {
      localView = mTransactionDetailScroller;
      i = localView.getHeight();
      if (i == 0) {
        i = mActivityHeight;
      }
      mTransactionDetailScroller.clearAnimation();
      ViewPropertyAnimator localViewPropertyAnimator = mTransactionDetailScroller.animate();
      float f3;
      if (paramBoolean)
      {
        f3 = 0.0F;
        localImageView = null;
      }
      else
      {
        float f4 = i;
        f3 = -1.0F * f4;
      }
      localViewPropertyAnimator = localViewPropertyAnimator.y(f3);
      if (paramBoolean) {
        f2 = 1.0F;
      }
      localViewPropertyAnimator = localViewPropertyAnimator.alpha(f2).setDuration(300L);
      localObject = new android/view/animation/AccelerateInterpolator;
      ((AccelerateInterpolator)localObject).<init>();
      localViewPropertyAnimator = localViewPropertyAnimator.setInterpolator((TimeInterpolator)localObject);
      localObject = new org/npci/upi/security/pinactivitycomponent/t;
      ((t)localObject).<init>(this, paramBoolean, i);
      localViewPropertyAnimator.setListener((Animator.AnimatorListener)localObject);
      return;
    }
    View localView = mTransactionDetailScroller;
    if (paramBoolean) {
      paramBoolean = false;
    } else {
      paramBoolean = true;
    }
    localView.setVisibility(paramBoolean);
  }
  
  private void unregisterSMSReceiver()
  {
    try
    {
      u localu = smsReceiver;
      if (localu != null)
      {
        localu = smsReceiver;
        unregisterReceiver(localu);
        localu = null;
        smsReceiver = null;
      }
      return;
    }
    finally
    {
      ad.a(TAG, "Failed to unregister SMS receiver (Ignoring)");
    }
  }
  
  public boolean checkSMSReadPermission()
  {
    String str = "android.permission.READ_SMS";
    int i = checkCallingOrSelfPermission(str);
    return i == 0;
  }
  
  public boolean checkSMSReceivePermission()
  {
    String str = "android.permission.RECEIVE_SMS";
    int i = checkCallingOrSelfPermission(str);
    return i == 0;
  }
  
  public c getCLContext()
  {
    return clContext;
  }
  
  public void loadFragment(ae paramae, Bundle paramBundle, boolean paramBoolean)
  {
    try
    {
      j localj = getSupportFragmentManager();
      if (paramBundle != null) {
        paramae.setArguments(paramBundle);
      }
      paramBundle = localj.a();
      int i = R.id.main_inner_layout;
      paramBundle.b(i, paramae);
      if (paramBoolean)
      {
        Object localObject = paramae.getClass();
        localObject = ((Class)localObject).getSimpleName();
        paramBundle.a((String)localObject);
      }
      paramBundle.d();
      setCurrentFragment(paramae);
      return;
    }
    catch (Exception localException)
    {
      localException;
    }
  }
  
  public void onBackPressed()
  {
    boolean bool = doubleBackToExitPressedOnce;
    n localn = null;
    if (bool)
    {
      localObject = new android/os/Bundle;
      ((Bundle)localObject).<init>();
      ((Bundle)localObject).putString("error", "USER_ABORTED");
      getCLContext().d().send(0, (Bundle)localObject);
      super.onBackPressed();
      return;
    }
    doubleBackToExitPressedOnce = true;
    int i = R.string.back_button_exit_message;
    Object localObject = getString(i);
    Toast.makeText(this, (CharSequence)localObject, 0).show();
    localObject = new android/os/Handler;
    ((Handler)localObject).<init>();
    localn = new org/npci/upi/security/pinactivitycomponent/n;
    localn.<init>(this);
    ((Handler)localObject).postDelayed(localn, 2000L);
  }
  
  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = Thread.currentThread().getUncaughtExceptionHandler();
    defaultHandler = paramBundle;
    paramBundle = Thread.currentThread();
    Object localObject = new org/npci/upi/security/pinactivitycomponent/k;
    ((k)localObject).<init>();
    paramBundle.setUncaughtExceptionHandler((Thread.UncaughtExceptionHandler)localObject);
    readArguments();
    int i = R.layout.activity_pin_activity_component;
    setContentView(i);
    setupKeyboard();
    appInit();
    renderTransactionBar();
    renderTransactionDetails();
    boolean bool = isATMPinFlow();
    localObject = null;
    if (bool)
    {
      paramBundle = new org/npci/upi/security/pinactivitycomponent/a;
      paramBundle.<init>();
    }
    else
    {
      paramBundle = new org/npci/upi/security/pinactivitycomponent/ao;
      paramBundle.<init>();
    }
    Bundle localBundle = getIntent().getExtras();
    loadFragment(paramBundle, localBundle, false);
    int j = R.id.go_back;
    paramBundle = (TextView)findViewById(j);
    if (paramBundle != null)
    {
      localObject = new org/npci/upi/security/pinactivitycomponent/m;
      ((m)localObject).<init>(this);
      paramBundle.setOnClickListener((View.OnClickListener)localObject);
    }
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    Thread localThread = Thread.currentThread();
    Thread.UncaughtExceptionHandler localUncaughtExceptionHandler = defaultHandler;
    localThread.setUncaughtExceptionHandler(localUncaughtExceptionHandler);
  }
  
  public void onPause()
  {
    super.onPause();
    unregisterSMSReceiver();
  }
  
  public void onResume()
  {
    super.onResume();
    boolean bool = checkSMSReceivePermission();
    if (bool)
    {
      u localu = new org/npci/upi/security/pinactivitycomponent/u;
      localu.<init>(this, null);
      smsReceiver = localu;
      registerSMSReceiver();
    }
  }
  
  void renderTransactionBar()
  {
    Object localObject1 = "";
    Object localObject2 = configuration;
    if (localObject2 != null) {
      localObject1 = ((JSONObject)localObject2).optString("payerBankName");
    }
    localObject2 = salt;
    if (localObject2 == null)
    {
      new d(this, "l12", "l12.message");
      return;
    }
    localObject2 = ((JSONObject)localObject2).optString("txnAmount");
    String str = "";
    Object localObject3 = "";
    int i = str.equals(localObject3);
    if (i != 0)
    {
      i = 0;
      localObject3 = null;
      for (;;)
      {
        Object localObject4 = payInfoArray;
        int k = ((JSONArray)localObject4).length();
        if (i < k) {
          try
          {
            localObject4 = payInfoArray;
            localObject4 = ((JSONArray)localObject4).get(i);
            localObject4 = (JSONObject)localObject4;
            localObject5 = "name";
            localObject6 = "";
            localObject4 = ((JSONObject)localObject4).optString((String)localObject5, (String)localObject6);
            localObject5 = "payeeName";
            boolean bool1 = ((String)localObject4).equals(localObject5);
            if (bool1)
            {
              localObject4 = payInfoArray;
              localObject4 = ((JSONArray)localObject4).get(i);
              localObject4 = (JSONObject)localObject4;
              localObject5 = "value";
              localObject6 = "";
            }
            for (;;)
            {
              str = ((JSONObject)localObject4).optString((String)localObject5, (String)localObject6);
              break label415;
              localObject4 = payInfoArray;
              localObject4 = ((JSONArray)localObject4).get(i);
              localObject4 = (JSONObject)localObject4;
              localObject5 = "name";
              localObject6 = "";
              localObject4 = ((JSONObject)localObject4).optString((String)localObject5, (String)localObject6);
              localObject5 = "account";
              bool1 = ((String)localObject4).equals(localObject5);
              if (bool1)
              {
                localObject4 = payInfoArray;
                localObject4 = ((JSONArray)localObject4).get(i);
                localObject4 = (JSONObject)localObject4;
                localObject5 = "value";
                localObject6 = "";
              }
              else
              {
                localObject4 = payInfoArray;
                localObject4 = ((JSONArray)localObject4).get(i);
                localObject4 = (JSONObject)localObject4;
                localObject5 = "name";
                localObject6 = "";
                localObject4 = ((JSONObject)localObject4).optString((String)localObject5, (String)localObject6);
                localObject5 = "mobileNumber";
                bool1 = ((String)localObject4).equals(localObject5);
                if (!bool1) {
                  break;
                }
                localObject4 = payInfoArray;
                localObject4 = ((JSONArray)localObject4).get(i);
                localObject4 = (JSONObject)localObject4;
                localObject5 = "payinfo.mobilenumber.label";
                localObject6 = "";
              }
            }
          }
          catch (Exception localException)
          {
            localObject5 = TAG;
            ad.a((String)localObject5, localException);
            i += 1;
          }
        }
      }
    }
    label415:
    int j = R.id.transaction_bar_root;
    localObject3 = (LinearLayout)findViewById(j);
    int m = R.id.tv_acc_or_payee;
    TextView localTextView = (TextView)findViewById(m);
    int n = R.id.transaction_bar_title;
    Object localObject5 = (TextView)findViewById(n);
    int i1 = R.id.transaction_bar_info;
    Object localObject6 = (TextView)findViewById(i1);
    int i2 = R.id.transaction_bar_arrow;
    ImageView localImageView = (ImageView)findViewById(i2);
    mTransactionBarArrow = localImageView;
    ((TextView)localObject5).setText(str);
    str = "";
    boolean bool2 = ((String)localObject1).equals(str);
    if (!bool2) {
      localTextView.setText((CharSequence)localObject1);
    }
    localObject1 = "";
    boolean bool3 = ((String)localObject2).equals(localObject1);
    if (!bool3)
    {
      localObject2 = String.valueOf(localObject2);
      localObject1 = "₹ ".concat((String)localObject2);
      ((TextView)localObject6).setText((CharSequence)localObject1);
    }
    localObject1 = getWindowManager().getDefaultDisplay();
    localObject2 = new android/graphics/Point;
    ((Point)localObject2).<init>();
    int i3 = Build.VERSION.SDK_INT;
    m = 13;
    if (i3 >= m)
    {
      ((Display)localObject1).getSize((Point)localObject2);
      i4 = y;
    }
    else
    {
      i4 = ((Display)localObject1).getHeight();
    }
    mActivityHeight = i4;
    localObject1 = new org/npci/upi/security/pinactivitycomponent/p;
    ((p)localObject1).<init>(this);
    ((LinearLayout)localObject3).setOnClickListener((View.OnClickListener)localObject1);
    int i4 = R.id.transaction_details_scroller;
    localObject1 = findViewById(i4);
    mTransactionDetailScroller = ((View)localObject1);
    i4 = R.id.transaction_details_expanded_space;
    localObject1 = findViewById(i4);
    mTransactionDetailSpace = ((View)localObject1);
    localObject1 = mTransactionDetailScroller;
    localObject2 = new org/npci/upi/security/pinactivitycomponent/q;
    ((q)localObject2).<init>(this);
    ((View)localObject1).setOnTouchListener((View.OnTouchListener)localObject2);
    localObject1 = mTransactionDetailSpace;
    if (localObject1 != null)
    {
      localObject2 = new org/npci/upi/security/pinactivitycomponent/s;
      ((s)localObject2).<init>(this);
      ((View)localObject1).setOnTouchListener((View.OnTouchListener)localObject2);
    }
    i4 = R.id.transaction_info_root;
    localObject1 = (TransitionDrawable)findViewById(i4).getBackground();
    mTransitionDrawable = ((TransitionDrawable)localObject1);
    mTransitionDrawable.setCrossFadeEnabled(true);
  }
  
  void renderTransactionDetails()
  {
    int i = R.id.transaction_details_root;
    LinearLayout localLinearLayout = (LinearLayout)findViewById(i);
    View localView = null;
    int j = 0;
    LinearLayout.LayoutParams localLayoutParams = null;
    for (;;)
    {
      Object localObject1 = payInfoArray;
      int k = ((JSONArray)localObject1).length();
      if (j >= k) {
        break;
      }
      localObject1 = LayoutInflater.from(this);
      m = R.layout.layout_transaction_details_item;
      localObject1 = (ViewGroup)((LayoutInflater)localObject1).inflate(m, localLinearLayout, false);
      m = R.id.transaction_details_item_name;
      Object localObject2 = (TextView)((ViewGroup)localObject1).findViewById(m);
      int n = R.id.transaction_details_item_value;
      TextView localTextView = (TextView)((ViewGroup)localObject1).findViewById(n);
      JSONObject localJSONObject = payInfoArray.optJSONObject(j);
      String str = localJSONObject.optString("name").toUpperCase();
      ((TextView)localObject2).setText(str);
      localObject2 = localJSONObject.optString("value");
      localTextView.setText((CharSequence)localObject2);
      localLinearLayout.addView((View)localObject1);
      j += 1;
    }
    localView = new android/view/View;
    localView.<init>(this);
    localLayoutParams = new android/widget/LinearLayout$LayoutParams;
    int m = pix(3.0F);
    localLayoutParams.<init>(-1, m);
    localView.setLayoutParams(localLayoutParams);
    localView.setBackgroundColor(-16777216);
    r.o(localView);
    localLinearLayout.addView(localView);
  }
  
  public void rotateView(float paramFloat1, float paramFloat2, int paramInt, View paramView)
  {
    RotateAnimation localRotateAnimation = new android/view/animation/RotateAnimation;
    localRotateAnimation.<init>(paramFloat1, paramFloat2, 1, 0.5F, 1, 0.5F);
    LinearInterpolator localLinearInterpolator = new android/view/animation/LinearInterpolator;
    localLinearInterpolator.<init>();
    localRotateAnimation.setInterpolator(localLinearInterpolator);
    long l = paramInt;
    localRotateAnimation.setDuration(l);
    boolean bool = true;
    localRotateAnimation.setFillEnabled(bool);
    localRotateAnimation.setFillAfter(bool);
    paramView.startAnimation(localRotateAnimation);
  }
  
  public void setCurrentFragment(ae paramae)
  {
    currentFragment = paramae;
  }
  
  public void setNumDigitsOfOTP(int paramInt)
  {
    numDigitsOfOTP = paramInt;
  }
}

/* Location:
 * Qualified Name:     org.npci.upi.security.pinactivitycomponent.GetCredential
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */