package org.npci.upi.security.pinactivitycomponent;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import java.util.Date;

class u
  extends BroadcastReceiver
{
  private u(GetCredential paramGetCredential) {}
  
  private void a(Intent paramIntent)
  {
    paramIntent = paramIntent.getExtras();
    if (paramIntent != null)
    {
      paramIntent = (Object[])paramIntent.get("pdus");
      int i = paramIntent.length;
      SmsMessage[] arrayOfSmsMessage = new SmsMessage[i];
      int j = 0;
      for (;;)
      {
        int k = arrayOfSmsMessage.length;
        if (j >= k) {
          break;
        }
        Object localObject1 = SmsMessage.createFromPdu((byte[])paramIntent[j]);
        arrayOfSmsMessage[j] = localObject1;
        localObject1 = arrayOfSmsMessage[j].getOriginatingAddress().toUpperCase();
        Object localObject2 = arrayOfSmsMessage[j].getMessageBody().toUpperCase();
        Object localObject3 = new java/util/Date;
        long l = arrayOfSmsMessage[j].getTimestampMillis();
        ((Date)localObject3).<init>(l);
        localObject3 = new org/npci/upi/security/pinactivitycomponent/an;
        Object localObject4 = GetCredential.access$1000(a);
        ((an)localObject3).<init>((Context)localObject4);
        localObject4 = a;
        int m = GetCredential.access$1100((GetCredential)localObject4);
        localObject1 = ((an)localObject3).a(m, (String)localObject1, (String)localObject2);
        if (localObject1 != null)
        {
          localObject2 = GetCredential.access$200(a);
          ((ae)localObject2).b((am)localObject1);
        }
        j += 1;
      }
    }
  }
  
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    try
    {
      paramContext = paramIntent.getAction();
      String str = "android.provider.Telephony.SMS_RECEIVED";
      boolean bool = paramContext.equals(str);
      if (bool) {
        a(paramIntent);
      }
      return;
    }
    catch (Exception paramContext)
    {
      ad.a(GetCredential.access$900(), paramContext);
    }
  }
}

/* Location:
 * Qualified Name:     org.npci.upi.security.pinactivitycomponent.u
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */