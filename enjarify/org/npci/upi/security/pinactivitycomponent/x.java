package org.npci.upi.security.pinactivitycomponent;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class x
{
  private final SharedPreferences a;
  
  public x(Context paramContext)
  {
    paramContext = paramContext.getSharedPreferences("NPCIPreferences", 0);
    a = paramContext;
  }
  
  public void a(String paramString1, String paramString2)
  {
    SharedPreferences.Editor localEditor = a.edit();
    localEditor.putString(paramString1, paramString2);
    localEditor.commit();
  }
  
  public String b(String paramString1, String paramString2)
  {
    return a.getString(paramString1, paramString2);
  }
}

/* Location:
 * Qualified Name:     org.npci.upi.security.pinactivitycomponent.x
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */