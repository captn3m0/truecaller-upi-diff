package org.npci.upi.security.pinactivitycomponent;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class d
  extends Exception
{
  String a = "CLException";
  private String b;
  private String c;
  private Context d;
  
  public d(Context paramContext, String paramString1, String paramString2)
  {
    b = paramString1;
    c = paramString2;
    d = paramContext;
    a(paramContext, paramString2);
  }
  
  public d(Context paramContext, String paramString1, String paramString2, Throwable paramThrowable)
  {
    super(paramThrowable);
    b = paramString1;
    c = paramString2;
    d = paramContext;
    a(paramContext, paramString2);
  }
  
  public String a()
  {
    return c;
  }
  
  public void a(Context paramContext, String paramString)
  {
    Object localObject1 = new java/util/Properties;
    ((Properties)localObject1).<init>();
    Object localObject2 = paramContext.getAssets();
    String str = "cl-messages_en_us.properties";
    try
    {
      localObject2 = ((AssetManager)localObject2).open(str);
    }
    catch (IOException localIOException2)
    {
      localIOException2.getLocalizedMessage();
      i = 0;
      localObject2 = null;
    }
    try
    {
      ((Properties)localObject1).load((InputStream)localObject2);
    }
    catch (IOException localIOException1)
    {
      localIOException1.getLocalizedMessage();
    }
    ((Properties)localObject1).getProperty(paramString);
    paramString = paramContext.getResources();
    int j = R.string.error_msg;
    paramString = paramString.getString(j);
    paramContext = (Activity)paramContext;
    j = R.id.error_layout;
    localObject1 = (RelativeLayout)paramContext.findViewById(j);
    int i = R.id.error_message;
    paramContext = (TextView)paramContext.findViewById(i);
    ((RelativeLayout)localObject1).setVisibility(0);
    paramContext.setText(paramString);
  }
}

/* Location:
 * Qualified Name:     org.npci.upi.security.pinactivitycomponent.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */