package org.npci.upi.security.pinactivitycomponent;

import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.os.ResultReceiver;
import org.npci.upi.security.services.CLResultReceiver;

public class CLServerResultReceiver
  extends ResultReceiver
{
  private CLResultReceiver a;
  
  public CLServerResultReceiver(CLResultReceiver paramCLResultReceiver)
  {
    super(localHandler);
    a = paramCLResultReceiver;
  }
  
  protected void onReceiveResult(int paramInt, Bundle paramBundle)
  {
    super.onReceiveResult(paramInt, paramBundle);
    int i = 2;
    if (paramInt == i) {}
    try
    {
      localCLResultReceiver = a;
      localCLResultReceiver.triggerOtp(paramBundle);
      return;
    }
    catch (RemoteException localRemoteException)
    {
      CLResultReceiver localCLResultReceiver;
      localRemoteException;
    }
    localCLResultReceiver = a;
    localCLResultReceiver.sendCredential(paramBundle);
    return;
  }
}

/* Location:
 * Qualified Name:     org.npci.upi.security.pinactivitycomponent.CLServerResultReceiver
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */