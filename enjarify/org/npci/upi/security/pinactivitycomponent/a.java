package org.npci.upi.security.pinactivitycomponent;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ViewSwitcher;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import org.json.JSONArray;
import org.json.JSONObject;
import org.npci.upi.security.pinactivitycomponent.widget.FormItemPager;
import org.npci.upi.security.pinactivitycomponent.widget.FormItemView;
import org.npci.upi.security.pinactivitycomponent.widget.m;

public class a
  extends ae
  implements m
{
  private static final String q = "a";
  private HashMap r;
  private int s;
  private boolean t;
  private ViewSwitcher u;
  
  public a()
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    r = localHashMap;
    s = 0;
    t = false;
    u = null;
  }
  
  private void a(View paramView)
  {
    int i = R.id.switcherLayout1;
    LinearLayout localLinearLayout1 = (LinearLayout)paramView.findViewById(i);
    int j = R.id.switcherLayout2;
    LinearLayout localLinearLayout2 = (LinearLayout)paramView.findViewById(j);
    int k = R.id.view_switcher;
    paramView = (ViewSwitcher)paramView.findViewById(k);
    u = paramView;
    paramView = c;
    if (paramView != null)
    {
      int m = 0;
      paramView = null;
      for (;;)
      {
        Object localObject1 = c;
        k = ((JSONArray)localObject1).length();
        if (m >= k) {
          break;
        }
        try
        {
          localObject1 = c;
          localObject1 = ((JSONArray)localObject1).getJSONObject(m);
          Object localObject2 = "subtype";
          localObject2 = ((JSONObject)localObject1).getString((String)localObject2);
          Object localObject3 = "dLength";
          int n = ((JSONObject)localObject1).optInt((String)localObject3);
          if (n == 0)
          {
            n = 6;
          }
          else
          {
            localObject3 = "dLength";
            n = ((JSONObject)localObject1).optInt((String)localObject3);
          }
          Object localObject4 = "MPIN";
          boolean bool1 = ((String)localObject2).equals(localObject4);
          int i2;
          if (bool1)
          {
            i2 = R.string.npci_set_mpin_title;
            localObject2 = getString(i2);
            localObject2 = a((String)localObject2, m, n);
            int i1 = R.string.npci_confirm_mpin_title;
            localObject4 = getString(i1);
            localObject3 = a((String)localObject4, m, n);
            localObject4 = new java/util/ArrayList;
            ((ArrayList)localObject4).<init>();
            ((ArrayList)localObject4).add(localObject2);
            ((ArrayList)localObject4).add(localObject3);
            localObject2 = new org/npci/upi/security/pinactivitycomponent/widget/FormItemPager;
            localObject3 = getActivity();
            ((FormItemPager)localObject2).<init>((Context)localObject3);
            ((FormItemPager)localObject2).a((ArrayList)localObject4, this);
            ((FormItemPager)localObject2).setFormDataTag(localObject1);
            localObject1 = f;
            ((ArrayList)localObject1).add(localObject2);
            localLinearLayout2.addView((View)localObject2);
          }
          else
          {
            localObject4 = "";
            String str = "ATMPIN";
            boolean bool4 = ((String)localObject2).equals(str);
            if (bool4)
            {
              i2 = R.string.npci_atm_title;
              localObject4 = getString(i2);
            }
            else
            {
              str = "OTP";
              bool4 = str.equals(localObject2);
              if (!bool4)
              {
                str = "SMS";
                bool4 = str.equals(localObject2);
                if (!bool4)
                {
                  str = "EMAIL";
                  bool4 = str.equals(localObject2);
                  if (!bool4)
                  {
                    str = "HOTP";
                    bool4 = str.equals(localObject2);
                    if (!bool4)
                    {
                      str = "TOTP";
                      boolean bool2 = str.equals(localObject2);
                      if (!bool2) {
                        break label496;
                      }
                    }
                  }
                }
              }
              int i3 = R.string.npci_otp_title;
              localObject4 = getString(i3);
              g = m;
              localObject2 = getActivity();
              boolean bool3 = localObject2 instanceof GetCredential;
              if (bool3)
              {
                localObject2 = getActivity();
                localObject2 = (GetCredential)localObject2;
                bool3 = ((GetCredential)localObject2).checkSMSReadPermission();
                if (bool3) {
                  b(n);
                }
              }
            }
            label496:
            localObject2 = a((String)localObject4, m, n);
            ((FormItemView)localObject2).setFormDataTag(localObject1);
            localObject1 = f;
            ((ArrayList)localObject1).add(localObject2);
            localLinearLayout1.addView((View)localObject2);
          }
        }
        catch (Exception localException)
        {
          for (;;) {}
        }
        m += 1;
      }
    }
  }
  
  private void d()
  {
    int i = g;
    int k = -1;
    int n;
    if (i != k)
    {
      localObject1 = f;
      k = g;
      localObject1 = ((ArrayList)localObject1).get(k);
      bool1 = localObject1 instanceof FormItemView;
      if (bool1)
      {
        localObject1 = f;
        k = g;
        localObject1 = (FormItemView)((ArrayList)localObject1).get(k);
        str = ((FormItemView)localObject1).getInputValue();
        if (str != null)
        {
          k = str.length();
          n = ((FormItemView)localObject1).getInputLength();
          if (k == n) {}
        }
        else
        {
          k = R.string.invalid_otp;
          str = getString(k);
          b((View)localObject1, str);
          return;
        }
      }
    }
    boolean bool1 = false;
    Object localObject1 = null;
    k = 0;
    String str = null;
    Object localObject2;
    Object localObject3;
    int j;
    for (;;)
    {
      localObject2 = f;
      n = ((ArrayList)localObject2).size();
      if (k >= n) {
        break;
      }
      localObject2 = f.get(k);
      boolean bool2 = localObject2 instanceof FormItemView;
      if (bool2)
      {
        localObject2 = (FormItemView)f.get(k);
        localObject3 = ((FormItemView)localObject2).getInputValue();
        int i2 = ((String)localObject3).length();
        int i3 = ((FormItemView)localObject2).getInputLength();
        if (i2 != i3)
        {
          j = R.string.componentMessage;
          localObject1 = getString(j);
          b((View)localObject2, (String)localObject1);
          return;
        }
      }
      k += 1;
    }
    int m = t;
    if (m == 0)
    {
      m = 1;
      t = m;
      for (;;)
      {
        localObject2 = f;
        int i1 = ((ArrayList)localObject2).size();
        if (j >= i1) {
          break;
        }
        try
        {
          localObject2 = f;
          localObject2 = ((ArrayList)localObject2).get(j);
          localObject2 = (org.npci.upi.security.pinactivitycomponent.widget.a)localObject2;
          localObject2 = ((org.npci.upi.security.pinactivitycomponent.widget.a)localObject2).getFormDataTag();
          localObject2 = (JSONObject)localObject2;
          localObject3 = "type";
          localObject3 = ((JSONObject)localObject2).getString((String)localObject3);
          Object localObject4 = "subtype";
          localObject2 = ((JSONObject)localObject2).getString((String)localObject4);
          localObject4 = b;
          Object localObject5 = "credential";
          Object localObject6 = f;
          localObject6 = ((ArrayList)localObject6).get(j);
          localObject6 = (org.npci.upi.security.pinactivitycomponent.widget.a)localObject6;
          localObject6 = ((org.npci.upi.security.pinactivitycomponent.widget.a)localObject6).getInputValue();
          ((JSONObject)localObject4).put((String)localObject5, localObject6);
          localObject4 = p;
          localObject4 = (GetCredential)localObject4;
          localObject4 = ((GetCredential)localObject4).getCLContext();
          localObject4 = ((c)localObject4).a();
          localObject5 = b;
          localObject4 = ((aq)localObject4).a((JSONObject)localObject5);
          localObject5 = p;
          localObject5 = (GetCredential)localObject5;
          localObject5 = ((GetCredential)localObject5).getCLContext();
          localObject5 = ((c)localObject5).b();
          localObject6 = b;
          localObject3 = ((j)localObject5).a((String)localObject4, (String)localObject3, (String)localObject2, (JSONObject)localObject6);
          if (localObject3 != null)
          {
            localObject4 = r;
            localObject3 = w.a(localObject3);
            ((HashMap)localObject4).put(localObject2, localObject3);
          }
        }
        catch (Exception localException)
        {
          localObject3 = q;
          ad.a((String)localObject3, localException);
        }
        j += 1;
      }
      localObject1 = new android/os/Bundle;
      ((Bundle)localObject1).<init>();
      localObject3 = r;
      ((Bundle)localObject1).putSerializable("credBlocks", (Serializable)localObject3);
      ResultReceiver localResultReceiver = ((GetCredential)p).getCLContext().d();
      localResultReceiver.send(m, (Bundle)localObject1);
      localObject1 = ((GetCredential)p).getCLContext().c();
      ((Activity)localObject1).finish();
    }
  }
  
  private void e()
  {
    int i = g;
    int m = -1;
    if (i != m)
    {
      localObject1 = f;
      m = g;
      localObject1 = ((ArrayList)localObject1).get(m);
      j = localObject1 instanceof FormItemView;
      if (j != 0)
      {
        localObject1 = f;
        m = g;
        localObject1 = (FormItemView)((ArrayList)localObject1).get(m);
        a((FormItemView)localObject1);
        ((FormItemView)localObject1).setNonMaskedField();
      }
    }
    int j = 0;
    Object localObject1 = null;
    ArrayList localArrayList = f;
    m = localArrayList.size();
    while (j < m)
    {
      int n = g;
      if (j != n)
      {
        org.npci.upi.security.pinactivitycomponent.widget.a locala = (org.npci.upi.security.pinactivitycomponent.widget.a)f.get(j);
        Object localObject2 = getActivity();
        int i1 = R.drawable.ic_visibility_on;
        Drawable localDrawable1 = android.support.v4.content.b.a((Context)localObject2, i1);
        localObject2 = getActivity();
        i1 = R.drawable.ic_visibility_off;
        Drawable localDrawable2 = android.support.v4.content.b.a((Context)localObject2, i1);
        int i2 = R.string.action_hide;
        Object localObject3 = getString(i2);
        i2 = R.string.action_show;
        String str = getString(i2);
        b localb = new org/npci/upi/security/pinactivitycomponent/b;
        localObject2 = localb;
        localb.<init>(this, locala, (String)localObject3, str, localDrawable2, localDrawable1);
        boolean bool1 = true;
        boolean bool2 = true;
        localObject2 = locala;
        localObject3 = localb;
        locala.a(str, localDrawable1, localb, 0, bool1, bool2);
      }
      int k;
      j += 1;
    }
  }
  
  public void a()
  {
    int i = s;
    int j = 1;
    Object localObject;
    if (i == 0)
    {
      localObject = f;
      k = s + j;
      ((org.npci.upi.security.pinactivitycomponent.widget.a)((ArrayList)localObject).get(k)).a();
      i = s + j;
      s = i;
      return;
    }
    int k = 2;
    if (i == j)
    {
      localObject = f;
      int m = 0;
      String str1 = null;
      localObject = (FormItemView)((ArrayList)localObject).get(0);
      i = ((FormItemView)localObject).getInputLength();
      String str2 = ((org.npci.upi.security.pinactivitycomponent.widget.a)f.get(0)).getInputValue();
      int n = str2.length();
      String str3;
      if (i != n)
      {
        localObject = (View)f.get(0);
        j = R.string.npci_otp_title;
        str3 = getString(j);
        b((View)localObject, str3);
        return;
      }
      localObject = (FormItemView)f.get(j);
      i = ((FormItemView)localObject).getInputLength();
      str1 = ((org.npci.upi.security.pinactivitycomponent.widget.a)f.get(j)).getInputValue();
      m = str1.length();
      if (i != m)
      {
        localObject = (View)f.get(j);
        j = R.string.npci_atm_title;
        str3 = getString(j);
        b((View)localObject, str3);
        return;
      }
      localObject = u;
      if (localObject != null)
      {
        ((ViewSwitcher)localObject).showNext();
        s = k;
        return;
      }
    }
    i = s;
    if (i == k)
    {
      localObject = f;
      j = s;
      localObject = (org.npci.upi.security.pinactivitycomponent.widget.a)((ArrayList)localObject).get(j);
      boolean bool = ((org.npci.upi.security.pinactivitycomponent.widget.a)localObject).a();
      if (bool) {
        d();
      }
      return;
    }
    d();
  }
  
  public void a(int paramInt)
  {
    Object localObject = f.get(paramInt);
    boolean bool = localObject instanceof FormItemPager;
    if (!bool) {
      s = paramInt;
    }
  }
  
  public void a(int paramInt, String paramString)
  {
    int i = g;
    int j = -1;
    if (i != j)
    {
      i = g;
      if (i == paramInt)
      {
        Object localObject = f;
        i = g;
        localObject = ((ArrayList)localObject).get(i);
        paramInt = localObject instanceof FormItemView;
        if (paramInt != 0)
        {
          localObject = this.i;
          a((Timer)localObject);
          localObject = f;
          i = g;
          ((FormItemView)((ArrayList)localObject).get(i)).a(false);
          localObject = f;
          j = g;
          localObject = (FormItemView)((ArrayList)localObject).get(j);
          String str = "";
          ((FormItemView)localObject).a(str, null, false, false);
          localObject = f;
          i = g;
          localObject = (FormItemView)((ArrayList)localObject).get(i);
          paramString = getActivity();
          j = R.drawable.ic_tick_ok;
          paramString = android.support.v4.content.b.a(paramString, j);
          j = 1;
          ((FormItemView)localObject).a(paramString, j);
        }
      }
    }
  }
  
  public void a(View paramView, String paramString)
  {
    b(paramView, paramString);
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    int i = R.layout.fragment_atmpin;
    return paramLayoutInflater.inflate(i, paramViewGroup, false);
  }
  
  public void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    b();
    a(paramView);
    e();
  }
}

/* Location:
 * Qualified Name:     org.npci.upi.security.pinactivitycomponent.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */