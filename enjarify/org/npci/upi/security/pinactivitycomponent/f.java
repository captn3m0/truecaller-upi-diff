package org.npci.upi.security.pinactivitycomponent;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import org.npci.upi.security.services.CLRemoteService.Stub;
import org.npci.upi.security.services.CLResultReceiver;

class f
  extends CLRemoteService.Stub
{
  private Context b = null;
  
  private f(CLRemoteServiceImpl paramCLRemoteServiceImpl, Context paramContext)
  {
    b = paramContext;
  }
  
  public String getChallenge(String paramString1, String paramString2)
  {
    return CLRemoteServiceImpl.a(a).a(paramString1, paramString2);
  }
  
  public void getCredential(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, CLResultReceiver paramCLResultReceiver)
  {
    Object localObject1 = a;
    Object localObject2 = paramString1;
    Object localObject3 = paramString2;
    localObject1 = CLRemoteServiceImpl.a((CLRemoteServiceImpl)localObject1, paramString1, paramString2, paramString3, paramString4, paramString5, paramString6, paramString7, paramString8, paramCLResultReceiver);
    localObject2 = new android/content/Intent;
    localObject3 = b;
    ((Intent)localObject2).<init>((Context)localObject3, GetCredential.class);
    ((Intent)localObject2).setFlags(268435456);
    ((Intent)localObject2).putExtras((Bundle)localObject1);
    b.startActivity((Intent)localObject2);
  }
  
  public boolean registerApp(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    return CLRemoteServiceImpl.a(a).a(paramString1, paramString2, paramString3, paramString4);
  }
}

/* Location:
 * Qualified Name:     org.npci.upi.security.pinactivitycomponent.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */