package org.npci.upi.security.pinactivitycomponent;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.support.v4.content.b;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TableLayout;
import android.widget.TableLayout.LayoutParams;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;

public class Keypad
  extends TableLayout
{
  private int a = 61;
  private int b;
  private int c;
  private float d;
  private ac e;
  
  public Keypad(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public Keypad(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    a(paramAttributeSet);
    a();
  }
  
  private int a(float paramFloat)
  {
    float f = getResourcesgetDisplayMetricsdensityDpi / 160;
    return (int)(paramFloat * f);
  }
  
  private void a()
  {
    int i = b;
    setBackgroundColor(i);
    TableLayout.LayoutParams localLayoutParams = new android/widget/TableLayout$LayoutParams;
    ImageView localImageView = null;
    localLayoutParams.<init>(-1, 0, 1.0F);
    boolean bool = true;
    int j = 0;
    Object localObject1 = null;
    int k = 1;
    float f1 = Float.MIN_VALUE;
    float f2;
    for (;;)
    {
      int m = 1077936128;
      f2 = 3.0F;
      n = 2;
      i1 = 17;
      f3 = 2.4E-44F;
      int i2 = 3;
      if (j >= i2) {
        break;
      }
      TableRow localTableRow = new android/widget/TableRow;
      Object localObject2 = getContext();
      localTableRow.<init>((Context)localObject2);
      localTableRow.setLayoutParams(localLayoutParams);
      localTableRow.setWeightSum(f2);
      m = k;
      k = 0;
      f1 = 0.0F;
      localObject3 = null;
      while (k < i2)
      {
        localObject2 = new android/widget/TextView;
        Object localObject4 = getContext();
        ((TextView)localObject2).<init>((Context)localObject4);
        ((TextView)localObject2).setGravity(i1);
        localObject4 = getItemParams();
        ((TextView)localObject2).setLayoutParams((ViewGroup.LayoutParams)localObject4);
        int i3 = c;
        ((TextView)localObject2).setTextColor(i3);
        float f4 = d;
        ((TextView)localObject2).setTextSize(n, f4);
        localObject4 = String.valueOf(m);
        ((TextView)localObject2).setText((CharSequence)localObject4);
        ((TextView)localObject2).setClickable(bool);
        setClickFeedback((View)localObject2);
        localObject4 = new org/npci/upi/security/pinactivitycomponent/y;
        ((y)localObject4).<init>(this, m);
        ((TextView)localObject2).setOnClickListener((View.OnClickListener)localObject4);
        localTableRow.addView((View)localObject2);
        m += 1;
        k += 1;
      }
      addView(localTableRow);
      j += 1;
      k = m;
    }
    localImageView = new android/widget/ImageView;
    localObject1 = getContext();
    localImageView.<init>((Context)localObject1);
    j = R.drawable.ic_action_backspace;
    localImageView.setImageResource(j);
    localObject1 = getItemParams();
    localImageView.setLayoutParams((ViewGroup.LayoutParams)localObject1);
    localImageView.setClickable(bool);
    setClickFeedback(localImageView);
    localObject1 = new org/npci/upi/security/pinactivitycomponent/z;
    ((z)localObject1).<init>(this);
    localImageView.setOnClickListener((View.OnClickListener)localObject1);
    localObject1 = new android/widget/TextView;
    Object localObject3 = getContext();
    ((TextView)localObject1).<init>((Context)localObject3);
    localObject3 = getItemParams();
    ((TextView)localObject1).setLayoutParams((ViewGroup.LayoutParams)localObject3);
    ((TextView)localObject1).setGravity(i1);
    ((TextView)localObject1).setText("0");
    k = c;
    ((TextView)localObject1).setTextColor(k);
    f1 = d;
    ((TextView)localObject1).setTextSize(n, f1);
    ((TextView)localObject1).setClickable(bool);
    setClickFeedback((View)localObject1);
    localObject3 = new org/npci/upi/security/pinactivitycomponent/aa;
    ((aa)localObject3).<init>(this);
    ((TextView)localObject1).setOnClickListener((View.OnClickListener)localObject3);
    localObject3 = new android/widget/ImageView;
    Object localObject5 = getContext();
    ((ImageView)localObject3).<init>((Context)localObject5);
    int n = R.drawable.ic_action_submit;
    ((ImageView)localObject3).setImageResource(n);
    localObject5 = ImageView.ScaleType.CENTER_INSIDE;
    ((ImageView)localObject3).setScaleType((ImageView.ScaleType)localObject5);
    ((ImageView)localObject3).setAdjustViewBounds(bool);
    localObject5 = getItemParams();
    float f3 = a;
    int i1 = (int)(a(f3) * 1.25F);
    height = i1;
    ((ImageView)localObject3).setLayoutParams((ViewGroup.LayoutParams)localObject5);
    ((ImageView)localObject3).setClickable(bool);
    setClickFeedback((View)localObject3);
    Object localObject6 = new org/npci/upi/security/pinactivitycomponent/ab;
    ((ab)localObject6).<init>(this);
    ((ImageView)localObject3).setOnClickListener((View.OnClickListener)localObject6);
    localObject6 = new android/widget/TableRow;
    localObject5 = getContext();
    ((TableRow)localObject6).<init>((Context)localObject5);
    ((TableRow)localObject6).setLayoutParams(localLayoutParams);
    ((TableRow)localObject6).setWeightSum(f2);
    ((TableRow)localObject6).addView(localImageView);
    ((TableRow)localObject6).addView((View)localObject1);
    ((TableRow)localObject6).addView((View)localObject3);
    addView((View)localObject6);
  }
  
  private void a(AttributeSet paramAttributeSet)
  {
    Context localContext = getContext();
    Object localObject = R.styleable.Keypad;
    paramAttributeSet = localContext.obtainStyledAttributes(paramAttributeSet, (int[])localObject, 0, 0);
    int i = R.styleable.Keypad_keypad_bg_color;
    localObject = getContext();
    int j = R.color.npci_keypad_bg_color;
    int k = b.c((Context)localObject, j);
    i = paramAttributeSet.getColor(i, k);
    b = i;
    i = R.styleable.Keypad_key_digit_color;
    localObject = getContext();
    j = R.color.npci_key_digit_color;
    k = b.c((Context)localObject, j);
    i = paramAttributeSet.getColor(i, k);
    c = i;
    i = R.styleable.Keypad_key_digit_size;
    float f = paramAttributeSet.getDimensionPixelSize(i, 36);
    d = f;
    i = R.styleable.Keypad_key_digit_height;
    k = a;
    i = paramAttributeSet.getDimensionPixelSize(i, k);
    a = i;
    paramAttributeSet.recycle();
  }
  
  private TableRow.LayoutParams getItemParams()
  {
    TableRow.LayoutParams localLayoutParams = new android/widget/TableRow$LayoutParams;
    float f = a;
    int i = a(f);
    localLayoutParams.<init>(0, i, 1.0F);
    return localLayoutParams;
  }
  
  private void setClickFeedback(View paramView)
  {
    TypedValue localTypedValue = new android/util/TypedValue;
    localTypedValue.<init>();
    Resources.Theme localTheme = getContext().getTheme();
    int i = R.attr.selectableItemBackground;
    localTheme.resolveAttribute(i, localTypedValue, true);
    int j = resourceId;
    paramView.setBackgroundResource(j);
  }
  
  public void setOnKeyPressCallback(ac paramac)
  {
    e = paramac;
  }
}

/* Location:
 * Qualified Name:     org.npci.upi.security.pinactivitycomponent.Keypad
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */