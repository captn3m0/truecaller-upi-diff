package org.npci.upi.security.pinactivitycomponent;

import android.os.Process;
import java.io.PrintWriter;
import java.io.StringWriter;

public class k
  implements Thread.UncaughtExceptionHandler
{
  String a = "\n";
  
  public void uncaughtException(Thread paramThread, Throwable paramThrowable)
  {
    paramThread = new java/io/StringWriter;
    paramThread.<init>();
    PrintWriter localPrintWriter = new java/io/PrintWriter;
    localPrintWriter.<init>(paramThread);
    paramThrowable.printStackTrace(localPrintWriter);
    Process.killProcess(Process.myPid());
  }
}

/* Location:
 * Qualified Name:     org.npci.upi.security.pinactivitycomponent.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */