package org.npci.upi.security.pinactivitycomponent;

public final class R$string
{
  public static final int abc_action_bar_home_description = 2131887345;
  public static final int abc_action_bar_up_description = 2131887346;
  public static final int abc_action_menu_overflow_description = 2131887347;
  public static final int abc_action_mode_done = 2131887348;
  public static final int abc_activity_chooser_view_see_all = 2131887349;
  public static final int abc_activitychooserview_choose_application = 2131887350;
  public static final int abc_capital_off = 2131887351;
  public static final int abc_capital_on = 2131887352;
  public static final int abc_search_hint = 2131887375;
  public static final int abc_searchview_description_clear = 2131887376;
  public static final int abc_searchview_description_query = 2131887377;
  public static final int abc_searchview_description_search = 2131887378;
  public static final int abc_searchview_description_submit = 2131887379;
  public static final int abc_searchview_description_voice = 2131887380;
  public static final int abc_shareactionprovider_share_with = 2131887381;
  public static final int abc_shareactionprovider_share_with_application = 2131887382;
  public static final int abc_toolbar_collapse_description = 2131887383;
  public static final int action_hide = 2131887396;
  public static final int action_resend = 2131887398;
  public static final int action_show = 2131887399;
  public static final int app_name = 2131887452;
  public static final int back_button_exit_message = 2131887465;
  public static final int componentMessage = 2131887730;
  public static final int detecting_otp = 2131887925;
  public static final int dismiss = 2131887934;
  public static final int error_msg = 2131888014;
  public static final int go_back = 2131888168;
  public static final int info = 2131888228;
  public static final int info_pins_dont_match = 2131888232;
  public static final int invalid_otp = 2131888274;
  public static final int not_right = 2131888409;
  public static final int npci_atm_title = 2131888450;
  public static final int npci_confirm_mpin_title = 2131888451;
  public static final int npci_mpin_title = 2131888453;
  public static final int npci_new_mpin_title = 2131888454;
  public static final int npci_otp_title = 2131888455;
  public static final int npci_set_mpin_title = 2131888456;
  public static final int status_bar_notification_info_overflow = 2131888855;
}

/* Location:
 * Qualified Name:     org.npci.upi.security.pinactivitycomponent.R.string
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */