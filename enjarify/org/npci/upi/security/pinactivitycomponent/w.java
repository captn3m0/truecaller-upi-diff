package org.npci.upi.security.pinactivitycomponent;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;

public class w
{
  public static String a(Object paramObject)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    a(paramObject, localStringBuilder);
    return localStringBuilder.toString();
  }
  
  private static void a(Object paramObject, StringBuilder paramStringBuilder)
  {
    if (paramObject == null)
    {
      paramStringBuilder.append("null");
      return;
    }
    Object localObject1 = paramObject.getClass();
    boolean bool1 = ((Class)localObject1).isArray();
    int j = 0;
    int k = 1;
    int m;
    int n;
    if (bool1)
    {
      localObject1 = "[";
      paramStringBuilder.append((String)localObject1);
      for (;;)
      {
        m = Array.getLength(paramObject);
        if (j >= m) {
          break;
        }
        a(Array.get(paramObject, j), paramStringBuilder);
        localObject1 = ",";
        paramStringBuilder.append((String)localObject1);
        j += 1;
      }
      n = paramStringBuilder.length() - k;
      m = paramStringBuilder.length();
      paramStringBuilder.replace(n, m, "]");
      return;
    }
    Object localObject2 = String.class;
    bool1 = localObject1.equals(localObject2);
    if (bool1)
    {
      paramStringBuilder.append("\"");
      paramStringBuilder.append(paramObject);
      paramStringBuilder.append("\"");
      return;
    }
    bool1 = ((Class)localObject1).isPrimitive();
    if (!bool1)
    {
      localObject2 = Integer.class;
      bool1 = localObject1.equals(localObject2);
      if (!bool1)
      {
        localObject2 = Long.class;
        bool1 = localObject1.equals(localObject2);
        if (!bool1)
        {
          localObject2 = Short.class;
          bool1 = localObject1.equals(localObject2);
          if (!bool1)
          {
            localObject2 = Double.class;
            bool1 = localObject1.equals(localObject2);
            if (!bool1)
            {
              localObject2 = Float.class;
              bool1 = localObject1.equals(localObject2);
              if (!bool1)
              {
                localObject2 = BigDecimal.class;
                bool1 = localObject1.equals(localObject2);
                if (!bool1)
                {
                  localObject2 = "{";
                  try
                  {
                    paramStringBuilder.append((String)localObject2);
                    localObject1 = ((Class)localObject1).getDeclaredFields();
                    int i = localObject1.length;
                    while (j < i)
                    {
                      Object localObject3 = localObject1[j];
                      int i1 = ((Field)localObject3).getModifiers();
                      boolean bool2 = Modifier.isStatic(i1);
                      if (!bool2)
                      {
                        ((Field)localObject3).setAccessible(k);
                        String str = "\"";
                        paramStringBuilder.append(str);
                        str = ((Field)localObject3).getName();
                        paramStringBuilder.append(str);
                        str = "\":";
                        paramStringBuilder.append(str);
                        localObject3 = ((Field)localObject3).get(paramObject);
                        a(localObject3, paramStringBuilder);
                        localObject3 = ",";
                        paramStringBuilder.append((String)localObject3);
                      }
                      j += 1;
                    }
                    n = paramStringBuilder.length() - k;
                    m = paramStringBuilder.length();
                    localObject2 = "}";
                    paramStringBuilder.replace(n, m, (String)localObject2);
                    return;
                  }
                  catch (Exception localException)
                  {
                    localException.printStackTrace();
                    return;
                  }
                }
              }
            }
          }
        }
      }
    }
    paramObject = String.valueOf(paramObject);
    paramStringBuilder.append((String)paramObject);
  }
}

/* Location:
 * Qualified Name:     org.npci.upi.security.pinactivitycomponent.w
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */