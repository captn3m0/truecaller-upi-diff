package org.npci.upi.security.pinactivitycomponent;

import android.content.Context;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class an
{
  private static final String b = "org.npci.upi.security.pinactivitycomponent.an";
  JSONArray a = null;
  private Context c;
  private x d;
  private List e;
  
  public an(Context paramContext)
  {
    c = paramContext;
    Object localObject1 = new org/npci/upi/security/pinactivitycomponent/x;
    Object localObject2 = c;
    ((x)localObject1).<init>((Context)localObject2);
    d = ((x)localObject1);
    localObject1 = new org/json/JSONArray;
    ((JSONArray)localObject1).<init>();
    a = ((JSONArray)localObject1);
    localObject1 = "npci_otp_rules.json";
    paramContext = l.a((String)localObject1, paramContext);
    if (paramContext != null) {
      try
      {
        localObject1 = new org/json/JSONArray;
        localObject2 = new java/lang/String;
        ((String)localObject2).<init>(paramContext);
        ((JSONArray)localObject1).<init>((String)localObject2);
        a = ((JSONArray)localObject1);
        return;
      }
      catch (Exception paramContext)
      {
        localObject1 = b;
        ad.a((String)localObject1, paramContext);
      }
    }
  }
  
  public static String a(String paramString)
  {
    paramString = paramString.toLowerCase();
    Object localObject = "MD5";
    try
    {
      localObject = MessageDigest.getInstance((String)localObject);
      byte[] arrayOfByte = paramString.getBytes();
      int i = paramString.length();
      ((MessageDigest)localObject).update(arrayOfByte, 0, i);
      paramString = new java/math/BigInteger;
      int j = 1;
      localObject = ((MessageDigest)localObject).digest();
      paramString.<init>(j, (byte[])localObject);
      int k = 16;
      for (paramString = paramString.toString(k);; paramString = ((String)localObject).concat(paramString))
      {
        k = paramString.length();
        j = 32;
        if (k >= j) {
          break;
        }
        localObject = "0";
        paramString = String.valueOf(paramString);
      }
      return paramString;
    }
    catch (Exception paramString)
    {
      ad.a(b, paramString);
    }
    return null;
  }
  
  private String a(ArrayList paramArrayList)
  {
    int i = paramArrayList.size();
    if (i <= 0) {
      return null;
    }
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    String str = (String)paramArrayList.get(0);
    ((StringBuilder)localObject).append(str);
    localObject = ((StringBuilder)localObject).toString();
    int j = 1;
    for (;;)
    {
      int k = paramArrayList.size();
      if (j >= k) {
        break;
      }
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localStringBuilder.append((String)localObject);
      localStringBuilder.append(",");
      localObject = (String)paramArrayList.get(j);
      localStringBuilder.append((String)localObject);
      localObject = localStringBuilder.toString();
      j += 1;
    }
    return (String)localObject;
  }
  
  private am a(int paramInt, String paramString1, String paramString2, JSONObject paramJSONObject)
  {
    Object localObject1 = "sender";
    try
    {
      localObject1 = paramJSONObject.getJSONArray((String)localObject1);
      boolean bool1 = a(paramString2, (JSONArray)localObject1);
      if (!bool1) {
        return null;
      }
      paramString2 = "message";
      paramString2 = paramJSONObject.getString(paramString2);
      bool1 = a(paramString1, paramString2);
      if (!bool1) {
        return null;
      }
      if (paramInt != 0)
      {
        paramString2 = new java/lang/StringBuilder;
        paramJSONObject = "\\d{";
        paramString2.<init>(paramJSONObject);
        paramString2.append(paramInt);
        localObject2 = "}";
        paramString2.append((String)localObject2);
        localObject2 = paramString2.toString();
      }
      else
      {
        localObject2 = "otp";
        localObject2 = paramJSONObject.get((String)localObject2);
        localObject2 = (String)localObject2;
      }
      Object localObject2 = Pattern.compile((String)localObject2);
      localObject2 = ((Pattern)localObject2).matcher(paramString1);
      paramString2 = new org/npci/upi/security/pinactivitycomponent/am;
      paramString2.<init>();
      paramString2.a(paramString1);
      boolean bool2 = ((Matcher)localObject2).find();
      if (bool2)
      {
        int i = ((Matcher)localObject2).groupCount();
        if (i >= 0)
        {
          i = 0;
          paramString1 = null;
          localObject2 = ((Matcher)localObject2).group(0);
          paramString2.b((String)localObject2);
          return paramString2;
        }
      }
    }
    catch (JSONException localJSONException) {}
    return null;
  }
  
  private boolean a(String paramString1, String paramString2)
  {
    int i = 2;
    paramString2 = Pattern.compile(paramString2, i);
    paramString1 = paramString2.matcher(paramString1);
    boolean bool = paramString1.find();
    return bool;
  }
  
  private boolean a(String paramString, JSONArray paramJSONArray)
  {
    int i = 0;
    for (;;)
    {
      int j = paramJSONArray.length();
      if (i >= j) {
        break;
      }
      Object localObject = paramJSONArray.getString(i);
      int k = 2;
      localObject = Pattern.compile((String)localObject, k).matcher(paramString);
      boolean bool = ((Matcher)localObject).find();
      if (bool) {
        return true;
      }
      i += 1;
    }
    return false;
  }
  
  private boolean b(String paramString)
  {
    Object localObject = e;
    if (localObject == null)
    {
      localObject = d;
      String str1 = "";
      localObject = ((x)localObject).b("msgID", str1);
      String str2 = ",";
      localObject = Arrays.asList(((String)localObject).split(str2));
      e = ((List)localObject);
    }
    return e.contains(paramString);
  }
  
  private boolean b(am paramam)
  {
    String str = paramam.c();
    boolean bool1 = b(str);
    if (!bool1)
    {
      paramam = c(paramam);
      boolean bool2 = b(paramam);
      if (!bool2) {
        return true;
      }
    }
    return false;
  }
  
  private String c(am paramam)
  {
    return a(paramam.a());
  }
  
  private void c(String paramString)
  {
    if (paramString == null) {
      return;
    }
    Object localObject1 = d.b("msgID", "");
    Object localObject2 = new java/util/ArrayList;
    String str = ",";
    localObject1 = Arrays.asList(((String)localObject1).split(str));
    ((ArrayList)localObject2).<init>((Collection)localObject1);
    boolean bool = ((ArrayList)localObject2).contains(paramString);
    if (!bool)
    {
      int i = ((ArrayList)localObject2).size();
      int j = 10;
      if (i >= j)
      {
        i = 0;
        localObject1 = null;
        ((ArrayList)localObject2).remove(0);
      }
      ((ArrayList)localObject2).add(paramString);
      paramString = a((ArrayList)localObject2);
      localObject1 = d;
      localObject2 = "msgID";
      ((x)localObject1).a((String)localObject2, paramString);
    }
  }
  
  /* Error */
  public am a(int paramInt, long paramLong)
  {
    // Byte code:
    //   0: ldc -2
    //   2: invokestatic 260	android/net/Uri:parse	(Ljava/lang/String;)Landroid/net/Uri;
    //   5: astore 4
    //   7: iconst_4
    //   8: anewarray 44	java/lang/String
    //   11: dup
    //   12: dup2
    //   13: iconst_0
    //   14: ldc_w 262
    //   17: aastore
    //   18: iconst_1
    //   19: ldc_w 264
    //   22: aastore
    //   23: iconst_2
    //   24: ldc_w 266
    //   27: aastore
    //   28: dup
    //   29: iconst_3
    //   30: ldc_w 268
    //   33: aastore
    //   34: astore 5
    //   36: iconst_0
    //   37: istore 6
    //   39: aconst_null
    //   40: astore 7
    //   42: iconst_0
    //   43: anewarray 4	java/lang/Object
    //   46: astore 8
    //   48: ldc_w 270
    //   51: aload 8
    //   53: invokestatic 274	java/lang/String:format	(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    //   56: astore 8
    //   58: ldc_w 276
    //   61: astore 9
    //   63: aload_0
    //   64: getfield 25	org/npci/upi/security/pinactivitycomponent/an:c	Landroid/content/Context;
    //   67: astore 10
    //   69: aload 10
    //   71: invokevirtual 282	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   74: astore 10
    //   76: iconst_1
    //   77: istore 11
    //   79: iload 11
    //   81: anewarray 44	java/lang/String
    //   84: astore 12
    //   86: lload_2
    //   87: invokestatic 285	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   90: astore 13
    //   92: aload 12
    //   94: iconst_0
    //   95: aload 13
    //   97: aastore
    //   98: aload 10
    //   100: astore 7
    //   102: aload 12
    //   104: astore 10
    //   106: aload 7
    //   108: aload 4
    //   110: aload 5
    //   112: aload 8
    //   114: aload 12
    //   116: aload 9
    //   118: invokevirtual 291	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   121: astore 13
    //   123: aload 13
    //   125: invokeinterface 296 1 0
    //   130: istore 14
    //   132: iload 14
    //   134: ifeq +126 -> 260
    //   137: aload 13
    //   139: iload 11
    //   141: invokeinterface 297 2 0
    //   146: astore 15
    //   148: iconst_2
    //   149: istore 6
    //   151: aload 13
    //   153: iload 6
    //   155: invokeinterface 297 2 0
    //   160: astore 7
    //   162: aload_0
    //   163: iload_1
    //   164: aload 15
    //   166: aload 7
    //   168: invokevirtual 300	org/npci/upi/security/pinactivitycomponent/an:a	(ILjava/lang/String;Ljava/lang/String;)Lorg/npci/upi/security/pinactivitycomponent/am;
    //   171: astore 15
    //   173: aload 15
    //   175: ifnull -52 -> 123
    //   178: ldc_w 262
    //   181: astore 7
    //   183: aload 13
    //   185: aload 7
    //   187: invokeinterface 304 2 0
    //   192: istore 6
    //   194: aload 13
    //   196: iload 6
    //   198: invokeinterface 308 2 0
    //   203: lstore 16
    //   205: lload 16
    //   207: invokestatic 313	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   210: astore 7
    //   212: aload 7
    //   214: invokestatic 104	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   217: astore 7
    //   219: aload 15
    //   221: aload 7
    //   223: invokevirtual 315	org/npci/upi/security/pinactivitycomponent/am:c	(Ljava/lang/String;)V
    //   226: aload_0
    //   227: aload 15
    //   229: invokespecial 318	org/npci/upi/security/pinactivitycomponent/an:b	(Lorg/npci/upi/security/pinactivitycomponent/am;)Z
    //   232: istore 6
    //   234: iload 6
    //   236: ifeq -113 -> 123
    //   239: aload_0
    //   240: aload 15
    //   242: invokevirtual 321	org/npci/upi/security/pinactivitycomponent/an:a	(Lorg/npci/upi/security/pinactivitycomponent/am;)V
    //   245: aload 13
    //   247: ifnull +10 -> 257
    //   250: aload 13
    //   252: invokeinterface 324 1 0
    //   257: aload 15
    //   259: areturn
    //   260: aload 13
    //   262: ifnull +77 -> 339
    //   265: goto +67 -> 332
    //   268: astore 18
    //   270: goto +16 -> 286
    //   273: astore 18
    //   275: aconst_null
    //   276: astore 13
    //   278: goto +30 -> 308
    //   281: astore 18
    //   283: aconst_null
    //   284: astore 13
    //   286: getstatic 52	org/npci/upi/security/pinactivitycomponent/an:b	Ljava/lang/String;
    //   289: astore 15
    //   291: aload 15
    //   293: aload 18
    //   295: invokestatic 57	org/npci/upi/security/pinactivitycomponent/ad:a	(Ljava/lang/String;Ljava/lang/Exception;)V
    //   298: aload 13
    //   300: ifnull +39 -> 339
    //   303: goto +29 -> 332
    //   306: astore 18
    //   308: aload 13
    //   310: ifnull +10 -> 320
    //   313: aload 13
    //   315: invokeinterface 324 1 0
    //   320: aload 18
    //   322: athrow
    //   323: pop
    //   324: aconst_null
    //   325: astore 13
    //   327: aload 13
    //   329: ifnull +10 -> 339
    //   332: aload 13
    //   334: invokeinterface 324 1 0
    //   339: aconst_null
    //   340: areturn
    //   341: pop
    //   342: goto -15 -> 327
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	345	0	this	an
    //   0	345	1	paramInt	int
    //   0	345	2	paramLong	long
    //   5	104	4	localUri	android.net.Uri
    //   34	77	5	arrayOfString1	String[]
    //   37	160	6	i	int
    //   232	3	6	bool1	boolean
    //   40	182	7	localObject1	Object
    //   46	67	8	localObject2	Object
    //   61	56	9	str	String
    //   67	38	10	localObject3	Object
    //   77	63	11	j	int
    //   84	31	12	arrayOfString2	String[]
    //   90	243	13	localObject4	Object
    //   130	3	14	bool2	boolean
    //   146	146	15	localObject5	Object
    //   203	3	16	l	long
    //   268	1	18	localException1	Exception
    //   273	1	18	localObject6	Object
    //   281	13	18	localException2	Exception
    //   306	15	18	localObject7	Object
    //   323	1	21	localSecurityException1	SecurityException
    //   341	1	22	localSecurityException2	SecurityException
    // Exception table:
    //   from	to	target	type
    //   123	130	268	java/lang/Exception
    //   139	146	268	java/lang/Exception
    //   153	160	268	java/lang/Exception
    //   166	171	268	java/lang/Exception
    //   185	192	268	java/lang/Exception
    //   196	203	268	java/lang/Exception
    //   205	210	268	java/lang/Exception
    //   212	217	268	java/lang/Exception
    //   221	226	268	java/lang/Exception
    //   227	232	268	java/lang/Exception
    //   240	245	268	java/lang/Exception
    //   63	67	273	finally
    //   69	74	273	finally
    //   79	84	273	finally
    //   86	90	273	finally
    //   95	98	273	finally
    //   116	121	273	finally
    //   63	67	281	java/lang/Exception
    //   69	74	281	java/lang/Exception
    //   79	84	281	java/lang/Exception
    //   86	90	281	java/lang/Exception
    //   95	98	281	java/lang/Exception
    //   116	121	281	java/lang/Exception
    //   123	130	306	finally
    //   139	146	306	finally
    //   153	160	306	finally
    //   166	171	306	finally
    //   185	192	306	finally
    //   196	203	306	finally
    //   205	210	306	finally
    //   212	217	306	finally
    //   221	226	306	finally
    //   227	232	306	finally
    //   240	245	306	finally
    //   286	289	306	finally
    //   293	298	306	finally
    //   63	67	323	java/lang/SecurityException
    //   69	74	323	java/lang/SecurityException
    //   79	84	323	java/lang/SecurityException
    //   86	90	323	java/lang/SecurityException
    //   95	98	323	java/lang/SecurityException
    //   116	121	323	java/lang/SecurityException
    //   123	130	341	java/lang/SecurityException
    //   139	146	341	java/lang/SecurityException
    //   153	160	341	java/lang/SecurityException
    //   166	171	341	java/lang/SecurityException
    //   185	192	341	java/lang/SecurityException
    //   196	203	341	java/lang/SecurityException
    //   205	210	341	java/lang/SecurityException
    //   212	217	341	java/lang/SecurityException
    //   221	226	341	java/lang/SecurityException
    //   227	232	341	java/lang/SecurityException
    //   240	245	341	java/lang/SecurityException
  }
  
  public am a(int paramInt, String paramString1, String paramString2)
  {
    int i = 0;
    for (;;)
    {
      am localam = null;
      try
      {
        Object localObject = a;
        int j = ((JSONArray)localObject).length();
        if (i < j)
        {
          localObject = a;
          localObject = ((JSONArray)localObject).getJSONObject(i);
          localam = a(paramInt, paramString2, paramString1, (JSONObject)localObject);
          if (localam != null) {
            return localam;
          }
          i += 1;
        }
        else
        {
          return null;
        }
      }
      catch (Exception localException)
      {
        ad.a(b, localException);
      }
    }
    return null;
  }
  
  public void a(am paramam)
  {
    String str = paramam.c();
    if (str != null) {
      paramam = paramam.c();
    } else {
      paramam = c(paramam);
    }
    c(paramam);
  }
}

/* Location:
 * Qualified Name:     org.npci.upi.security.pinactivitycomponent.an
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */