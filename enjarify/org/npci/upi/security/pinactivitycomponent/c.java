package org.npci.upi.security.pinactivitycomponent;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetManager;
import android.os.ResultReceiver;
import in.org.npci.commonlibrary.e;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

public class c
{
  private Map a;
  private Context b;
  private Properties c;
  private Properties d;
  private Properties e;
  private aq f;
  private j g;
  private Locale h;
  private e i;
  private Activity j;
  private v k;
  private i l;
  
  public c(Context paramContext, v paramv, Activity paramActivity)
  {
    Object localObject1 = new java/util/HashMap;
    ((HashMap)localObject1).<init>();
    a = ((Map)localObject1);
    k = paramv;
    localObject1 = paramv.b();
    h = ((Locale)localObject1);
    b = paramContext;
    paramContext = paramv.c();
    i = paramContext;
    j = paramActivity;
    paramContext = a("cl-app.properties");
    e = paramContext;
    paramContext = a("validation.properties");
    c = paramContext;
    paramContext = a("version.properties");
    d = paramContext;
    paramContext = h;
    Object localObject2;
    if (paramContext != null)
    {
      paramActivity = a;
      paramContext = paramContext.getLanguage();
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>("cl-messages_");
      localObject2 = h.getLanguage();
      ((StringBuilder)localObject1).append((String)localObject2);
      localObject2 = ".properties";
      ((StringBuilder)localObject1).append((String)localObject2);
      localObject1 = ((StringBuilder)localObject1).toString();
      localObject1 = a((String)localObject1);
      paramActivity.put(paramContext, localObject1);
    }
    else
    {
      paramContext = new java/util/Locale;
      paramContext.<init>("en_US");
      paramActivity = a;
      localObject1 = paramContext.getLanguage();
      localObject2 = new java/lang/StringBuilder;
      String str = "cl-messages_";
      ((StringBuilder)localObject2).<init>(str);
      paramContext = paramContext.getLanguage();
      ((StringBuilder)localObject2).append(paramContext);
      ((StringBuilder)localObject2).append(".properties");
      paramContext = ((StringBuilder)localObject2).toString();
      paramContext = a(paramContext);
      paramActivity.put(localObject1, paramContext);
    }
    paramContext = paramv.d();
    l = paramContext;
    paramContext = new org/npci/upi/security/pinactivitycomponent/aq;
    paramContext.<init>(this);
    f = paramContext;
    if (paramv != null)
    {
      paramContext = paramv.c();
      if (paramContext != null)
      {
        paramContext = paramv.a();
        if (paramContext != null)
        {
          paramContext = new org/npci/upi/security/pinactivitycomponent/j;
          paramActivity = i;
          localObject1 = l;
          paramv = paramv.a();
          paramContext.<init>(paramActivity, (i)localObject1, paramv);
          g = paramContext;
        }
      }
    }
  }
  
  public Properties a(String paramString)
  {
    Properties localProperties = new java/util/Properties;
    localProperties.<init>();
    try
    {
      localObject = b;
      localObject = ((Context)localObject).getAssets();
      paramString = ((AssetManager)localObject).open(paramString);
      localProperties.load(paramString);
    }
    catch (IOException paramString)
    {
      Object localObject = "AssetsPropertyReader";
      paramString = paramString.toString();
      ad.a((String)localObject, paramString);
    }
    return localProperties;
  }
  
  public aq a()
  {
    return f;
  }
  
  public String b(String paramString)
  {
    Properties localProperties = d;
    if (localProperties != null) {
      return localProperties.getProperty(paramString);
    }
    return null;
  }
  
  public j b()
  {
    Object localObject1 = g;
    if (localObject1 == null)
    {
      localObject1 = k;
      if (localObject1 != null)
      {
        localObject1 = ((v)localObject1).c();
        i = ((e)localObject1);
        localObject1 = new org/npci/upi/security/pinactivitycomponent/j;
        localObject2 = k.c();
        localObject3 = k.d();
        localObject4 = k.a();
        ((j)localObject1).<init>((e)localObject2, (i)localObject3, (String)localObject4);
        g = ((j)localObject1);
      }
    }
    ad.b("Common Library", "get Encryptor");
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("Input Analyzer :");
    Object localObject3 = k;
    ((StringBuilder)localObject2).append(localObject3);
    localObject2 = ((StringBuilder)localObject2).toString();
    ad.b("Common Library", (String)localObject2);
    localObject1 = "Common Library";
    localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("Input Analyzer Key Code:");
    localObject3 = k;
    ((StringBuilder)localObject2).append(localObject3);
    localObject2 = ((StringBuilder)localObject2).toString();
    localObject3 = null;
    if (localObject2 == null) {
      localObject2 = null;
    } else {
      localObject2 = k.a();
    }
    ad.b((String)localObject1, (String)localObject2);
    localObject1 = "Common Library";
    localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("Input Analyzer Common Library:");
    Object localObject4 = k;
    ((StringBuilder)localObject2).append(localObject4);
    localObject2 = ((StringBuilder)localObject2).toString();
    if (localObject2 != null)
    {
      localObject2 = k.c();
      localObject3 = localObject2.toString();
    }
    ad.b((String)localObject1, (String)localObject3);
    return g;
  }
  
  public Activity c()
  {
    return j;
  }
  
  public ResultReceiver d()
  {
    v localv = k;
    if (localv == null) {
      return null;
    }
    return localv.e();
  }
}

/* Location:
 * Qualified Name:     org.npci.upi.security.pinactivitycomponent.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */