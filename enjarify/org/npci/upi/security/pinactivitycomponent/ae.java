package org.npci.upi.security.pinactivitycomponent;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v4.content.b;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout.LayoutParams;
import android.widget.PopupWindow;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Timer;
import org.json.JSONArray;
import org.json.JSONObject;
import org.npci.upi.security.pinactivitycomponent.widget.FormItemEditText;
import org.npci.upi.security.pinactivitycomponent.widget.FormItemView;
import org.npci.upi.security.pinactivitycomponent.widget.a;
import org.npci.upi.security.pinactivitycomponent.widget.m;

public abstract class ae
  extends Fragment
  implements m
{
  protected JSONObject a = null;
  protected JSONObject b = null;
  protected JSONArray c = null;
  protected Timer d = null;
  protected long e = 45000L;
  protected ArrayList f;
  protected int g;
  protected PopupWindow h;
  protected Timer i;
  protected Timer j;
  protected Handler k;
  protected Runnable l;
  protected JSONObject m;
  protected JSONArray n;
  protected long o;
  protected Context p;
  private boolean q;
  
  public ae()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    f = localArrayList;
    g = -1;
    i = null;
    m = null;
    JSONArray localJSONArray = new org/json/JSONArray;
    localJSONArray.<init>();
    n = localJSONArray;
    o = 3000L;
    q = false;
  }
  
  private void d()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    int i1 = 0;
    Object localObject1 = null;
    int i2 = 0;
    JSONArray localJSONArray = null;
    JSONObject localJSONObject1 = null;
    JSONObject localJSONObject2 = null;
    JSONObject localJSONObject3 = null;
    for (;;)
    {
      Object localObject2 = c;
      int i3 = ((JSONArray)localObject2).length();
      if (i2 >= i3) {
        break;
      }
      try
      {
        localObject2 = c;
        localObject2 = ((JSONArray)localObject2).get(i2);
        localObject2 = (JSONObject)localObject2;
        localObject3 = "subtype";
        String str = "";
        localObject2 = ((JSONObject)localObject2).optString((String)localObject3, str);
        localObject3 = "ATMPIN";
        boolean bool2 = ((String)localObject2).equals(localObject3);
        if (bool2)
        {
          localObject3 = c;
          localObject1 = ((JSONArray)localObject3).getJSONObject(i2);
        }
        localObject3 = "OTP|SMS|HOTP|TOTP";
        bool2 = ((String)localObject2).matches((String)localObject3);
        if (bool2)
        {
          localObject3 = c;
          localJSONObject1 = ((JSONArray)localObject3).getJSONObject(i2);
        }
        localObject3 = "MPIN";
        bool2 = ((String)localObject2).equals(localObject3);
        if (bool2)
        {
          localObject3 = c;
          localJSONObject2 = ((JSONArray)localObject3).getJSONObject(i2);
        }
        localObject3 = "NMPIN";
        boolean bool1 = ((String)localObject2).equals(localObject3);
        if (bool1)
        {
          localObject2 = c;
          localJSONObject3 = ((JSONArray)localObject2).getJSONObject(i2);
        }
      }
      catch (Exception localException)
      {
        Object localObject3 = "NPCIFragment";
        ad.a((String)localObject3, localException);
      }
      i2 += 1;
    }
    localJSONArray = c;
    i2 = localJSONArray.length();
    int i4 = 3;
    if ((i2 == i4) && (localObject1 != null) && (localJSONObject1 != null) && (localJSONObject2 != null))
    {
      localArrayList.add(localJSONObject1);
      localArrayList.add(localObject1);
      localArrayList.add(localJSONObject2);
    }
    localObject1 = c;
    i1 = ((JSONArray)localObject1).length();
    i2 = 2;
    if ((i1 == i2) && (localJSONObject1 != null) && (localJSONObject2 != null))
    {
      localArrayList.add(localJSONObject1);
      localArrayList.add(localJSONObject2);
    }
    localObject1 = c;
    i1 = ((JSONArray)localObject1).length();
    if ((i1 == i2) && (localJSONObject2 != null) && (localJSONObject3 != null))
    {
      localArrayList.add(localJSONObject2);
      localArrayList.add(localJSONObject3);
    }
    i1 = localArrayList.size();
    if (i1 > 0)
    {
      localObject1 = new org/json/JSONArray;
      ((JSONArray)localObject1).<init>(localArrayList);
      c = ((JSONArray)localObject1);
    }
  }
  
  int a(float paramFloat)
  {
    float f1 = getResourcesgetDisplayMetricsdensityDpi / 160;
    return (int)(paramFloat * f1);
  }
  
  protected FormItemView a(String paramString, int paramInt1, int paramInt2)
  {
    LinearLayout.LayoutParams localLayoutParams = new android/widget/LinearLayout$LayoutParams;
    int i1 = -1;
    float f1 = 0.0F / 0.0F;
    localLayoutParams.<init>(i1, -2);
    FormItemView localFormItemView = new org/npci/upi/security/pinactivitycomponent/widget/FormItemView;
    Object localObject1 = getActivity();
    localFormItemView.<init>((Context)localObject1);
    localObject1 = c;
    int i2 = ((JSONArray)localObject1).length();
    int i3 = 1;
    float f2 = Float.MIN_VALUE;
    if (i2 == i3)
    {
      localFormItemView.setActionBarPositionTop(i3);
      i2 = a(240.0F);
      width = i2;
      f1 = 40.0F;
      i2 = a(f1);
      topMargin = i2;
      localFormItemView.getFormInputView().setCharSize(0.0F);
      localObject1 = localFormItemView.getFormInputView();
      float f3 = a(15.0F);
      ((FormItemEditText)localObject1).setSpace(f3);
      localObject1 = localFormItemView.getFormInputView();
      f3 = 32.0F;
      float f4 = a(f3);
      ((FormItemEditText)localObject1).setFontSize(f4);
      localObject1 = localFormItemView.getFormInputView();
      int i5 = a(f3);
      ((FormItemEditText)localObject1).setPadding(0, i5, 0, 0);
      localObject1 = localFormItemView.getFormInputView();
      i5 = 4;
      f4 = 5.6E-45F;
      int[] arrayOfInt = new int[i5];
      arrayOfInt[0] = 0;
      int i6 = a(f3);
      arrayOfInt[i3] = i6;
      arrayOfInt[2] = 0;
      f3 = 4.2E-45F;
      arrayOfInt[3] = 0;
      ((FormItemEditText)localObject1).setMargin(arrayOfInt);
      localFormItemView.getFormInputView().setLineStrokeCentered(i3);
      localObject1 = localFormItemView.getFormInputView();
      int i4 = a(2.0F);
      f2 = i4;
      ((FormItemEditText)localObject1).setLineStrokeSelected(f2);
      localObject1 = localFormItemView.getFormInputView();
      Object localObject2 = getActivity();
      i6 = R.color.form_item_input_colors_transparent;
      localObject2 = b.b((Context)localObject2, i6);
      ((FormItemEditText)localObject1).setColorStates((ColorStateList)localObject2);
    }
    localFormItemView.setLayoutParams(localLayoutParams);
    localFormItemView.setInputLength(paramInt2);
    localFormItemView.setFormItemListener(this);
    localFormItemView.setTitle(paramString);
    localFormItemView.setFormItemTag(paramInt1);
    return localFormItemView;
  }
  
  public abstract void a();
  
  protected void a(Timer paramTimer)
  {
    if (paramTimer != null) {
      try
      {
        paramTimer.cancel();
        return;
      }
      catch (Exception paramTimer)
      {
        String str = "NPCIFragment";
        ad.a(str, paramTimer);
      }
    }
  }
  
  protected void a(am paramam)
  {
    try
    {
      int i1 = g;
      int i2 = -1;
      if (i1 != i2)
      {
        i1 = 1;
        q = i1;
        Object localObject = f;
        i2 = g;
        localObject = ((ArrayList)localObject).get(i2);
        localObject = (a)localObject;
        paramam = paramam.b();
        ((a)localObject).setText(paramam);
      }
      return;
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
  }
  
  protected void a(FormItemView paramFormItemView)
  {
    Timer localTimer = new java/util/Timer;
    localTimer.<init>();
    i = localTimer;
    localTimer = i;
    al localal = new org/npci/upi/security/pinactivitycomponent/al;
    localal.<init>(this);
    long l1 = e;
    localTimer.schedule(localal, l1);
    paramFormItemView.a("", null, null, 0, false, false);
    paramFormItemView.a(null, false);
    int i1 = R.string.detecting_otp;
    String str = getString(i1);
    boolean bool = true;
    paramFormItemView.a(str, null, bool, false);
    paramFormItemView.a(bool);
  }
  
  protected void b()
  {
    Object localObject1 = getArguments();
    Object localObject2;
    if (localObject1 != null) {
      localObject2 = "configuration";
    }
    try
    {
      localObject2 = ((Bundle)localObject1).getString((String)localObject2);
      Object localObject3;
      if (localObject2 != null)
      {
        localObject3 = new org/json/JSONObject;
        ((JSONObject)localObject3).<init>((String)localObject2);
        a = ((JSONObject)localObject3);
      }
      localObject2 = "controls";
      localObject2 = ((Bundle)localObject1).getString((String)localObject2);
      if (localObject2 != null)
      {
        localObject3 = new org/json/JSONObject;
        ((JSONObject)localObject3).<init>((String)localObject2);
        m = ((JSONObject)localObject3);
        localObject2 = m;
        localObject3 = "CredAllowed";
        localObject2 = ((JSONObject)localObject2).getString((String)localObject3);
        if (localObject2 != null)
        {
          localObject3 = new org/json/JSONArray;
          ((JSONArray)localObject3).<init>((String)localObject2);
          c = ((JSONArray)localObject3);
          d();
        }
      }
      localObject2 = "salt";
      localObject2 = ((Bundle)localObject1).getString((String)localObject2);
      if (localObject2 != null)
      {
        localObject3 = new org/json/JSONObject;
        ((JSONObject)localObject3).<init>((String)localObject2);
        b = ((JSONObject)localObject3);
      }
      localObject2 = "payInfo";
      localObject1 = ((Bundle)localObject1).getString((String)localObject2);
      if (localObject1 != null)
      {
        localObject2 = new org/json/JSONArray;
        ((JSONArray)localObject2).<init>((String)localObject1);
        n = ((JSONArray)localObject2);
      }
      return;
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
  }
  
  public void b(int paramInt)
  {
    Object localObject1 = getActivity();
    if (localObject1 != null)
    {
      localObject1 = getActivity();
      boolean bool = localObject1 instanceof GetCredential;
      if (bool)
      {
        localObject1 = (GetCredential)getActivity();
        ((GetCredential)localObject1).setNumDigitsOfOTP(paramInt);
      }
    }
    localObject1 = new org/npci/upi/security/pinactivitycomponent/an;
    Object localObject2 = getActivity();
    ((an)localObject1).<init>((Context)localObject2);
    localObject2 = new java/util/Timer;
    ((Timer)localObject2).<init>();
    d = ((Timer)localObject2);
    Timer localTimer = d;
    af localaf = new org/npci/upi/security/pinactivitycomponent/af;
    localaf.<init>(this, (an)localObject1, paramInt);
    localTimer.scheduleAtFixedRate(localaf, 100, 2000L);
  }
  
  protected void b(View paramView, String paramString)
  {
    Object localObject = h;
    if (localObject != null) {
      ((PopupWindow)localObject).dismiss();
    }
    localObject = getActivity().getLayoutInflater();
    int i1 = R.layout.layout_popup_view;
    localObject = ((LayoutInflater)localObject).inflate(i1, null);
    i1 = R.id.popup_text;
    ((TextView)((View)localObject).findViewById(i1)).setText(paramString);
    paramString = new android/widget/PopupWindow;
    int i2 = a(60.0F);
    paramString.<init>((View)localObject, -2, i2);
    h = paramString;
    paramString = h;
    i1 = R.style.PopupAnimation;
    paramString.setAnimationStyle(i1);
    h.showAtLocation(paramView, 17, 0, 0);
    int i3 = R.id.popup_button;
    paramView = ((View)localObject).findViewById(i3);
    paramString = new org/npci/upi/security/pinactivitycomponent/ah;
    paramString.<init>(this);
    paramView.setOnClickListener(paramString);
    paramView = new java/util/Timer;
    paramView.<init>();
    j = paramView;
    paramView = new android/os/Handler;
    paramString = Looper.getMainLooper();
    paramView.<init>(paramString);
    k = paramView;
    paramView = new org/npci/upi/security/pinactivitycomponent/ai;
    paramView.<init>(this);
    l = paramView;
    paramView = k;
    paramString = l;
    long l1 = o;
    paramView.postDelayed(paramString, l1);
  }
  
  public void b(am paramam)
  {
    a(paramam);
  }
  
  protected void c()
  {
    Object localObject1 = a;
    String str = "false";
    localObject1 = ((JSONObject)localObject1).optString("resendOTPFeature", str);
    Object localObject2 = "false";
    boolean bool = ((String)localObject1).equals(localObject2);
    if (!bool)
    {
      bool = q;
      if (!bool)
      {
        localObject1 = getActivity();
        localObject2 = new org/npci/upi/security/pinactivitycomponent/aj;
        ((aj)localObject2).<init>(this);
        ((f)localObject1).runOnUiThread((Runnable)localObject2);
      }
    }
  }
  
  public void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    p = paramContext;
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    Object localObject = d;
    a((Timer)localObject);
    localObject = i;
    a((Timer)localObject);
    localObject = j;
    a((Timer)localObject);
    localObject = k;
    if (localObject != null)
    {
      Runnable localRunnable = l;
      if (localRunnable != null) {
        ((Handler)localObject).removeCallbacks(localRunnable);
      }
    }
    localObject = h;
    if (localObject != null) {
      ((PopupWindow)localObject).dismiss();
    }
  }
  
  public void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    paramView = getActivity();
    boolean bool = paramView instanceof GetCredential;
    if (bool)
    {
      paramView = (GetCredential)getActivity();
      paramView.setCurrentFragment(this);
    }
  }
}

/* Location:
 * Qualified Name:     org.npci.upi.security.pinactivitycomponent.ae
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */