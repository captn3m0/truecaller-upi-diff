package org.npci.upi.security.pinactivitycomponent;

import android.content.Context;
import android.util.Base64;
import in.org.npci.commonlibrary.l;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

public class g
{
  private Context a;
  private String b = "";
  private i c = null;
  private h d;
  private String e;
  
  public g(Context paramContext)
  {
    Object localObject = "";
    e = ((String)localObject);
    try
    {
      a = paramContext;
      localObject = new org/npci/upi/security/pinactivitycomponent/i;
      ((i)localObject).<init>(paramContext);
      c = ((i)localObject);
      paramContext = new org/npci/upi/security/pinactivitycomponent/h;
      paramContext.<init>();
      d = paramContext;
      return;
    }
    catch (Exception paramContext)
    {
      paramContext.printStackTrace();
      throw paramContext;
    }
  }
  
  private boolean b(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    try
    {
      Object localObject1 = c;
      localObject1 = ((i)localObject1).b();
      Object localObject2 = "Token in CL";
      ad.b((String)localObject2, (String)localObject1);
      localObject2 = d;
      localObject1 = ((h)localObject2).b((String)localObject1);
      paramString1 = Base64.decode(paramString1, 0);
      localObject2 = d;
      paramString1 = ((h)localObject2).b(paramString1, (byte[])localObject1);
      paramString1 = Base64.encodeToString(paramString1, 0);
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      ((StringBuilder)localObject1).append(paramString2);
      paramString2 = "|";
      ((StringBuilder)localObject1).append(paramString2);
      ((StringBuilder)localObject1).append(paramString3);
      paramString2 = "|";
      ((StringBuilder)localObject1).append(paramString2);
      ((StringBuilder)localObject1).append(paramString4);
      paramString2 = ((StringBuilder)localObject1).toString();
      paramString3 = "CL Hmac Msg";
      ad.b(paramString3, paramString2);
      paramString3 = d;
      paramString2 = paramString3.a(paramString2);
      paramString2 = Base64.encodeToString(paramString2, 0);
      paramString3 = "CL Hash";
      ad.b(paramString3, paramString2);
      boolean bool = paramString2.equalsIgnoreCase(paramString1);
      return bool;
    }
    catch (Exception paramString1)
    {
      paramString1.printStackTrace();
    }
    catch (UnsupportedEncodingException paramString1)
    {
      paramString1.printStackTrace();
    }
    catch (InvalidKeyException paramString1)
    {
      paramString1.printStackTrace();
    }
    catch (InvalidAlgorithmParameterException paramString1)
    {
      paramString1.printStackTrace();
    }
    catch (BadPaddingException paramString1)
    {
      paramString1.printStackTrace();
    }
    catch (IllegalBlockSizeException paramString1)
    {
      paramString1.printStackTrace();
    }
    catch (NoSuchAlgorithmException paramString1)
    {
      paramString1.printStackTrace();
    }
    return false;
  }
  
  public String a()
  {
    Object localObject = "AES";
    try
    {
      localObject = KeyGenerator.getInstance((String)localObject);
      int i = 256;
      ((KeyGenerator)localObject).init(i);
      localObject = ((KeyGenerator)localObject).generateKey();
      localObject = ((SecretKey)localObject).getEncoded();
      h localh = d;
      return localh.a((byte[])localObject);
    }
    catch (Exception localException)
    {
      localException;
    }
    return null;
  }
  
  public String a(String paramString1, String paramString2)
  {
    String str1 = "";
    String str2 = "";
    Object localObject1 = new java/text/SimpleDateFormat;
    ((SimpleDateFormat)localObject1).<init>("dd/MM/yyyy");
    Object localObject2 = new java/sql/Date;
    long l = System.currentTimeMillis();
    ((java.sql.Date)localObject2).<init>(l);
    localObject1 = ((SimpleDateFormat)localObject1).format((java.util.Date)localObject2);
    try
    {
      localObject2 = b();
      b = ((String)localObject2);
      str1 = a();
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
    try
    {
      Object localObject3 = new org/npci/upi/security/pinactivitycomponent/h;
      ((h)localObject3).<init>();
      Object localObject4 = "initial";
      boolean bool = paramString1.equalsIgnoreCase((String)localObject4);
      Object localObject5;
      if (bool)
      {
        paramString1 = c;
        paramString1.c();
        paramString1 = c;
        paramString1 = paramString1.a();
        int i = paramString1.size();
        if (i > 0)
        {
          paramString1 = c;
          localObject4 = new org/npci/upi/security/pinactivitycomponent/ar;
          localObject5 = b;
          ((ar)localObject4).<init>(str1, (String)localObject5, (String)localObject1);
          paramString1.b((ar)localObject4);
        }
        else
        {
          paramString1 = c;
          localObject4 = new org/npci/upi/security/pinactivitycomponent/ar;
          localObject5 = b;
          ((ar)localObject4).<init>(str1, (String)localObject5, (String)localObject1);
          paramString1.a((ar)localObject4);
        }
        paramString1 = new java/lang/StringBuilder;
        paramString1.<init>();
        localObject1 = b;
        paramString1.append((String)localObject1);
        localObject1 = "|";
        paramString1.append((String)localObject1);
        paramString1.append(str1);
        localObject1 = "|";
        paramString1.append((String)localObject1);
        paramString1.append(paramString2);
        str2 = paramString1.toString();
        paramString1 = l.a();
        str2 = ((h)localObject3).a(str2, paramString1);
        paramString1 = "K0 in Challenge";
        ad.b(paramString1, str1);
        paramString1 = "token in Challenge";
        paramString2 = b;
        ad.b(paramString1, paramString2);
      }
      else
      {
        paramString1 = c;
        paramString1 = paramString1.a();
        localObject4 = null;
        paramString1 = paramString1.get(0);
        paramString1 = (ar)paramString1;
        paramString1 = paramString1.a();
        localObject5 = new java/lang/StringBuilder;
        ((StringBuilder)localObject5).<init>();
        String str3 = b;
        ((StringBuilder)localObject5).append(str3);
        str3 = "|";
        ((StringBuilder)localObject5).append(str3);
        ((StringBuilder)localObject5).append(str1);
        str3 = "|";
        ((StringBuilder)localObject5).append(str3);
        ((StringBuilder)localObject5).append(paramString2);
        str2 = ((StringBuilder)localObject5).toString();
        paramString2 = "K0 in Challenge";
        ad.b(paramString2, str1);
        paramString2 = "token in Challenge";
        localObject5 = b;
        ad.b(paramString2, (String)localObject5);
        paramString1 = ((h)localObject3).b(paramString1);
        paramString2 = str2.getBytes();
        paramString1 = ((h)localObject3).a(paramString2, paramString1);
        str2 = Base64.encodeToString(paramString1, 0);
        paramString1 = c;
        paramString1.c();
        paramString1 = c;
        paramString2 = new org/npci/upi/security/pinactivitycomponent/ar;
        localObject3 = b;
        paramString2.<init>(str1, (String)localObject3, (String)localObject1);
        paramString1.a(paramString2);
      }
    }
    catch (Exception paramString1)
    {
      paramString1.printStackTrace();
    }
    return str2;
  }
  
  public boolean a(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    String str = "hmac";
    ad.b(str, paramString4);
    boolean bool = b(paramString4, paramString1, paramString2, paramString3);
    return bool;
  }
  
  public String b()
  {
    Object localObject = "AES";
    try
    {
      localObject = KeyGenerator.getInstance((String)localObject);
      int i = 256;
      ((KeyGenerator)localObject).init(i);
      localObject = ((KeyGenerator)localObject).generateKey();
      localObject = ((SecretKey)localObject).getEncoded();
      h localh = d;
      return localh.a((byte[])localObject);
    }
    catch (Exception localException)
    {
      localException;
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     org.npci.upi.security.pinactivitycomponent.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */