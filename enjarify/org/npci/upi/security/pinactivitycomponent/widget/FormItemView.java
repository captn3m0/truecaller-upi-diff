package org.npci.upi.security.pinactivitycomponent.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v4.view.r;
import android.support.v4.view.u;
import android.support.v4.view.v;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewPropertyAnimator;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import java.lang.ref.WeakReference;
import org.npci.upi.security.pinactivitycomponent.R.id;
import org.npci.upi.security.pinactivitycomponent.R.layout;
import org.npci.upi.security.pinactivitycomponent.R.styleable;

public class FormItemView
  extends LinearLayout
  implements a
{
  private boolean a = false;
  private String b;
  private String c;
  private int d;
  private TextView e;
  private FormItemEditText f;
  private m g;
  private int h;
  private Object i;
  private LinearLayout j;
  private Button k;
  private ProgressBar l;
  private ImageView m;
  private String n = "";
  private boolean o = false;
  private boolean p;
  private boolean q;
  private RelativeLayout r;
  
  public FormItemView(Context paramContext)
  {
    super(paramContext);
    a(paramContext, null);
  }
  
  public FormItemView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    a(paramContext, paramAttributeSet);
  }
  
  public u a(View paramView, boolean paramBoolean)
  {
    paramView = r.n(paramView);
    float f1 = 0.0F;
    Object localObject1 = null;
    float f2 = 1.0F;
    float f3;
    if (paramBoolean)
    {
      f3 = 1.0F;
    }
    else
    {
      f3 = 0.0F;
      localObject2 = null;
    }
    Object localObject3 = (View)a.get();
    if (localObject3 != null)
    {
      localObject3 = ((View)localObject3).animate();
      ((ViewPropertyAnimator)localObject3).scaleY(f3);
    }
    if (paramBoolean) {
      f1 = 1.0F;
    }
    Object localObject2 = (View)a.get();
    if (localObject2 != null)
    {
      localObject2 = ((View)localObject2).animate();
      ((ViewPropertyAnimator)localObject2).scaleX(f1);
    }
    localObject1 = new android/view/animation/AccelerateInterpolator;
    ((AccelerateInterpolator)localObject1).<init>();
    paramView = paramView.a((Interpolator)localObject1);
    localObject1 = new org/npci/upi/security/pinactivitycomponent/widget/l;
    ((l)localObject1).<init>(this, paramBoolean);
    paramView = paramView.a((v)localObject1);
    if (!paramBoolean) {
      f2 = 0.5F;
    }
    return paramView.a(f2);
  }
  
  public void a(Context paramContext, AttributeSet paramAttributeSet)
  {
    int[] arrayOfInt = R.styleable.FormItemView;
    paramAttributeSet = paramContext.obtainStyledAttributes(paramAttributeSet, arrayOfInt);
    arrayOfInt = null;
    if (paramAttributeSet != null)
    {
      int i1 = R.styleable.FormItemView_formTitle;
      String str = paramAttributeSet.getString(i1);
      b = str;
      i1 = R.styleable.FormItemView_formValidationError;
      str = paramAttributeSet.getString(i1);
      c = str;
      i1 = R.styleable.FormItemView_formInputLength;
      int i2 = 6;
      i1 = paramAttributeSet.getInteger(i1, i2);
      d = i1;
      i1 = R.styleable.FormItemView_formActionOnTop;
      boolean bool1 = paramAttributeSet.getBoolean(i1, false);
      p = bool1;
      paramAttributeSet.recycle();
    }
    int i3 = R.layout.layout_form_item;
    inflate(paramContext, i3, this);
    int i4 = R.id.form_item_root;
    paramContext = (RelativeLayout)findViewById(i4);
    r = paramContext;
    i4 = R.id.form_item_action_bar;
    paramContext = (LinearLayout)findViewById(i4);
    j = paramContext;
    i4 = R.id.form_item_title;
    paramContext = (TextView)findViewById(i4);
    e = paramContext;
    i4 = R.id.form_item_input;
    paramContext = (FormItemEditText)findViewById(i4);
    f = paramContext;
    i4 = R.id.form_item_button;
    paramContext = (Button)findViewById(i4);
    k = paramContext;
    i4 = R.id.form_item_progress;
    paramContext = (ProgressBar)findViewById(i4);
    l = paramContext;
    i4 = R.id.form_item_image;
    paramContext = (ImageView)findViewById(i4);
    m = paramContext;
    f.setInputType(0);
    paramContext = b;
    setTitle(paramContext);
    i4 = d;
    setInputLength(i4);
    paramContext = f;
    paramAttributeSet = new org/npci/upi/security/pinactivitycomponent/widget/j;
    paramAttributeSet.<init>(this);
    paramContext.addTextChangedListener(paramAttributeSet);
    paramContext = f;
    paramAttributeSet = new org/npci/upi/security/pinactivitycomponent/widget/k;
    paramAttributeSet.<init>(this);
    paramContext.setOnTouchListener(paramAttributeSet);
    boolean bool2 = p;
    setActionBarPositionTop(bool2);
  }
  
  public void a(Drawable paramDrawable, boolean paramBoolean)
  {
    if (paramDrawable != null)
    {
      ImageView localImageView = m;
      localImageView.setImageDrawable(paramDrawable);
    }
    paramDrawable = m;
    a(paramDrawable, paramBoolean);
  }
  
  public void a(String paramString, Drawable paramDrawable, View.OnClickListener paramOnClickListener, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    if (!bool)
    {
      localButton = k;
      localButton.setText(paramString);
    }
    paramString = k;
    bool = false;
    Button localButton = null;
    Drawable localDrawable1;
    if (paramInt == 0) {
      localDrawable1 = paramDrawable;
    } else {
      localDrawable1 = null;
    }
    int i1 = 1;
    Drawable localDrawable2;
    if (paramInt == i1)
    {
      localDrawable2 = paramDrawable;
    }
    else
    {
      i1 = 0;
      localDrawable2 = null;
    }
    int i2 = 2;
    Drawable localDrawable3;
    if (paramInt == i2)
    {
      localDrawable3 = paramDrawable;
    }
    else
    {
      i2 = 0;
      localDrawable3 = null;
    }
    int i3 = 3;
    if (paramInt != i3) {
      paramDrawable = null;
    }
    paramString.setCompoundDrawablesWithIntrinsicBounds(localDrawable1, localDrawable2, localDrawable3, paramDrawable);
    k.setOnClickListener(paramOnClickListener);
    k.setEnabled(paramBoolean2);
    paramString = k;
    a(paramString, paramBoolean1);
  }
  
  public void a(String paramString, View.OnClickListener paramOnClickListener, boolean paramBoolean1, boolean paramBoolean2)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    if (!bool)
    {
      Button localButton = k;
      localButton.setText(paramString);
    }
    paramString = k;
    a(paramString, paramBoolean1);
    k.setEnabled(paramBoolean2);
    k.setOnClickListener(paramOnClickListener);
  }
  
  public void a(boolean paramBoolean)
  {
    Object localObject = l;
    u localu = a((View)localObject, paramBoolean);
    localObject = new android/view/animation/AccelerateDecelerateInterpolator;
    ((AccelerateDecelerateInterpolator)localObject).<init>();
    localu.a((Interpolator)localObject);
    localu.b();
  }
  
  public boolean a()
  {
    f.requestFocus();
    return true;
  }
  
  public boolean b()
  {
    boolean bool = o;
    String str1;
    if (!bool)
    {
      bool = true;
      o = bool;
      str1 = n;
      setText(str1);
    }
    else
    {
      bool = false;
      o = false;
      str1 = n;
      String str2 = "●";
      str1 = str1.replaceAll(".", str2);
      FormItemEditText localFormItemEditText = f;
      localFormItemEditText.setText(str1);
    }
    return o;
  }
  
  public Object getFormDataTag()
  {
    return i;
  }
  
  public FormItemEditText getFormInputView()
  {
    return f;
  }
  
  public m getFormItemListener()
  {
    return g;
  }
  
  public int getInputLength()
  {
    return d;
  }
  
  public String getInputValue()
  {
    boolean bool = a;
    if (!bool)
    {
      bool = o;
      if (!bool) {
        return n;
      }
    }
    return f.getText().toString();
  }
  
  public void setActionBarPositionTop(boolean paramBoolean)
  {
    q = paramBoolean;
    RelativeLayout.LayoutParams localLayoutParams = (RelativeLayout.LayoutParams)j.getLayoutParams();
    boolean bool = q;
    int i2 = 8;
    int i3 = 10;
    if (bool)
    {
      localLayoutParams.addRule(i3);
      localLayoutParams.addRule(i2, 0);
    }
    else
    {
      localLayoutParams.addRule(i3, 0);
      int i1 = R.id.form_item_input;
      localLayoutParams.addRule(i2, i1);
    }
    j.setLayoutParams(localLayoutParams);
  }
  
  public void setFormDataTag(Object paramObject)
  {
    i = paramObject;
  }
  
  public void setFormItemListener(m paramm)
  {
    g = paramm;
  }
  
  public void setFormItemTag(int paramInt)
  {
    h = paramInt;
  }
  
  public void setInputLength(int paramInt)
  {
    f.setMaxLength(paramInt);
    d = paramInt;
  }
  
  public void setNonMaskedField()
  {
    a = true;
  }
  
  public void setText(String paramString)
  {
    f.setText(paramString);
    FormItemEditText localFormItemEditText = f;
    int i1 = paramString.length();
    localFormItemEditText.setSelection(i1);
  }
  
  public void setTitle(String paramString)
  {
    e.setText(paramString);
    b = paramString;
  }
}

/* Location:
 * Qualified Name:     org.npci.upi.security.pinactivitycomponent.widget.FormItemView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */