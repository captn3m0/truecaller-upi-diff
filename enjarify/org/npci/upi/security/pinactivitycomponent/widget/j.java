package org.npci.upi.security.pinactivitycomponent.widget;

import android.text.Editable;
import android.text.TextWatcher;

class j
  implements TextWatcher
{
  j(FormItemView paramFormItemView) {}
  
  public void afterTextChanged(Editable paramEditable)
  {
    Object localObject = a;
    boolean bool = FormItemView.e((FormItemView)localObject);
    if (bool)
    {
      localObject = a;
      paramEditable = paramEditable.toString();
      FormItemView.a((FormItemView)localObject, paramEditable);
      return;
    }
    localObject = a;
    bool = FormItemView.f((FormItemView)localObject);
    if (bool)
    {
      localObject = a;
      paramEditable = paramEditable.toString();
      FormItemView.a((FormItemView)localObject, paramEditable);
      return;
    }
    int i = paramEditable.length();
    if (i == 0)
    {
      FormItemView.a(a, "");
      return;
    }
    localObject = FormItemView.g(a);
    i = ((String)localObject).length();
    int j = paramEditable.length();
    if (i > j)
    {
      paramEditable = a;
      localObject = FormItemView.g(paramEditable);
      int m = FormItemView.g(a).length() + -1;
      localObject = ((String)localObject).substring(0, m);
      FormItemView.a(paramEditable, (String)localObject);
      return;
    }
    localObject = paramEditable.toString();
    int k = paramEditable.length() + -1;
    i = ((String)localObject).charAt(k);
    k = 9679;
    if (i != k)
    {
      paramEditable = a;
      String str = FormItemView.g(paramEditable);
      localObject = String.valueOf(i);
      localObject = str.concat((String)localObject);
      FormItemView.a(paramEditable, (String)localObject);
      paramEditable = FormItemView.g(a).replaceAll(".", "●");
      FormItemView.b(a).setText(paramEditable);
      return;
    }
    localObject = FormItemView.b(a);
    int n = paramEditable.length();
    ((FormItemEditText)localObject).setSelection(n);
  }
  
  public void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
  
  public void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
  {
    paramCharSequence = FormItemView.a(a);
    if (paramCharSequence == null) {
      return;
    }
    paramCharSequence = FormItemView.b(a).getText();
    if (paramCharSequence != null)
    {
      paramCharSequence = FormItemView.b(a).getText();
      int i = paramCharSequence.length();
      FormItemView localFormItemView = a;
      paramInt1 = FormItemView.c(localFormItemView);
      if (i >= paramInt1)
      {
        paramCharSequence = FormItemView.a(a);
        localFormItemView = a;
        paramInt1 = FormItemView.d(localFormItemView);
        String str = FormItemView.b(a).getText().toString();
        paramCharSequence.a(paramInt1, str);
      }
    }
  }
}

/* Location:
 * Qualified Name:     org.npci.upi.security.pinactivitycomponent.widget.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */