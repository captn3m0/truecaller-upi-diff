package org.npci.upi.security.pinactivitycomponent.widget;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;

class f
  implements ValueAnimator.AnimatorUpdateListener
{
  f(FormItemEditText paramFormItemEditText, int paramInt) {}
  
  public void onAnimationUpdate(ValueAnimator paramValueAnimator)
  {
    paramValueAnimator = (Float)paramValueAnimator.getAnimatedValue();
    float[] arrayOfFloat = FormItemEditText.d(b);
    int i = a;
    float f = paramValueAnimator.floatValue();
    arrayOfFloat[i] = f;
    b.invalidate();
  }
}

/* Location:
 * Qualified Name:     org.npci.upi.security.pinactivitycomponent.widget.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */