package org.npci.upi.security.pinactivitycomponent.widget;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.text.Editable;

class h
  implements Animator.AnimatorListener
{
  h(FormItemEditText paramFormItemEditText) {}
  
  public void onAnimationCancel(Animator paramAnimator) {}
  
  public void onAnimationEnd(Animator paramAnimator)
  {
    paramAnimator = FormItemEditText.c(a);
    Editable localEditable = a.getText();
    paramAnimator.a(localEditable);
  }
  
  public void onAnimationRepeat(Animator paramAnimator) {}
  
  public void onAnimationStart(Animator paramAnimator) {}
}

/* Location:
 * Qualified Name:     org.npci.upi.security.pinactivitycomponent.widget.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */