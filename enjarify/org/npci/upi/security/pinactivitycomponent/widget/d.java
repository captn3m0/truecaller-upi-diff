package org.npci.upi.security.pinactivitycomponent.widget;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Paint;

class d
  implements ValueAnimator.AnimatorUpdateListener
{
  d(FormItemEditText paramFormItemEditText) {}
  
  public void onAnimationUpdate(ValueAnimator paramValueAnimator)
  {
    paramValueAnimator = (Integer)paramValueAnimator.getAnimatedValue();
    Paint localPaint = FormItemEditText.b(a);
    int i = paramValueAnimator.intValue();
    localPaint.setAlpha(i);
    a.invalidate();
  }
}

/* Location:
 * Qualified Name:     org.npci.upi.security.pinactivitycomponent.widget.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */