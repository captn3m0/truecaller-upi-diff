package org.npci.upi.security.pinactivitycomponent.widget;

import android.graphics.drawable.Drawable;
import android.view.View.OnClickListener;

public abstract interface a
{
  public abstract void a(String paramString, Drawable paramDrawable, View.OnClickListener paramOnClickListener, int paramInt, boolean paramBoolean1, boolean paramBoolean2);
  
  public abstract boolean a();
  
  public abstract boolean b();
  
  public abstract Object getFormDataTag();
  
  public abstract String getInputValue();
  
  public abstract void setText(String paramString);
}

/* Location:
 * Qualified Name:     org.npci.upi.security.pinactivitycomponent.widget.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */