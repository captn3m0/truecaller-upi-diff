package org.npci.upi.security.pinactivitycomponent.widget;

import android.text.Editable;
import android.view.View;
import android.view.View.OnLongClickListener;

class c
  implements View.OnLongClickListener
{
  c(FormItemEditText paramFormItemEditText) {}
  
  public boolean onLongClick(View paramView)
  {
    paramView = a;
    int i = paramView.getText().length();
    paramView.setSelection(i);
    return true;
  }
}

/* Location:
 * Qualified Name:     org.npci.upi.security.pinactivitycomponent.widget.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */