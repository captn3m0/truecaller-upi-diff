package org.npci.upi.security.pinactivitycomponent.widget;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.support.v4.view.r;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputFilter.LengthFilter;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.ActionMode.Callback;
import android.view.View.OnClickListener;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.animation.OvershootInterpolator;
import android.widget.EditText;
import java.util.Locale;
import org.npci.upi.security.pinactivitycomponent.R.attr;
import org.npci.upi.security.pinactivitycomponent.R.styleable;

public class FormItemEditText
  extends EditText
{
  private int[][] A;
  private int[] B;
  private ColorStateList C;
  private String a = null;
  private StringBuilder b = null;
  private String c = null;
  private int d = 0;
  private float e = 24.0F;
  private float f;
  private float g = 4.0F;
  private float h = 8.0F;
  private int i;
  private RectF[] j;
  private float[] k;
  private Paint l;
  private Paint m;
  private Paint n;
  private Drawable o;
  private Rect p;
  private boolean q;
  private View.OnClickListener r;
  private i s;
  private boolean t;
  private float u;
  private float v;
  private Paint w;
  private boolean x;
  private boolean y;
  private ColorStateList z;
  
  public FormItemEditText(Context paramContext)
  {
    super(paramContext);
    int i1 = 4;
    i = i1;
    Object localObject = new android/graphics/Rect;
    ((Rect)localObject).<init>();
    p = ((Rect)localObject);
    q = false;
    s = null;
    u = 1.0F;
    v = 2.0F;
    x = false;
    y = false;
    paramContext = new int[i1][];
    int i2 = 1;
    int[] arrayOfInt1 = new int[i2];
    arrayOfInt1[0] = 16842913;
    paramContext[0] = arrayOfInt1;
    arrayOfInt1 = new int[i2];
    arrayOfInt1[0] = 16842914;
    paramContext[i2] = arrayOfInt1;
    arrayOfInt1 = new int[i2];
    arrayOfInt1[0] = 16842908;
    paramContext[2] = arrayOfInt1;
    localObject = new int[i2];
    localObject[0] = -16842908;
    paramContext[3] = localObject;
    A = paramContext;
    paramContext = new int[i1];
    Context tmp179_178 = paramContext;
    Context tmp180_179 = tmp179_178;
    Context tmp180_179 = tmp179_178;
    tmp180_179[0] = -16711936;
    tmp180_179[1] = -65536;
    tmp180_179[2] = -16777216;
    tmp180_179[3] = -7829368;
    B = paramContext;
    paramContext = new android/content/res/ColorStateList;
    int[][] arrayOfInt = A;
    int[] arrayOfInt2 = B;
    paramContext.<init>(arrayOfInt, arrayOfInt2);
    C = paramContext;
  }
  
  public FormItemEditText(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    int i1 = 4;
    i = i1;
    Object localObject1 = new android/graphics/Rect;
    ((Rect)localObject1).<init>();
    p = ((Rect)localObject1);
    q = false;
    s = null;
    u = 1.0F;
    v = 2.0F;
    x = false;
    y = false;
    Object localObject2 = new int[i1][];
    int i2 = 1;
    int[] arrayOfInt1 = new int[i2];
    arrayOfInt1[0] = 16842913;
    localObject2[0] = arrayOfInt1;
    arrayOfInt1 = new int[i2];
    arrayOfInt1[0] = 16842914;
    localObject2[i2] = arrayOfInt1;
    arrayOfInt1 = new int[i2];
    arrayOfInt1[0] = 16842908;
    localObject2[2] = arrayOfInt1;
    localObject1 = new int[i2];
    localObject1[0] = -16842908;
    localObject2[3] = localObject1;
    A = ((int[][])localObject2);
    localObject2 = new int[i1];
    Object tmp194_192 = localObject2;
    Object tmp195_194 = tmp194_192;
    Object tmp195_194 = tmp194_192;
    tmp195_194[0] = -16711936;
    tmp195_194[1] = -65536;
    tmp195_194[2] = -16777216;
    tmp195_194[3] = -7829368;
    B = ((int[])localObject2);
    localObject2 = new android/content/res/ColorStateList;
    int[][] arrayOfInt = A;
    int[] arrayOfInt2 = B;
    ((ColorStateList)localObject2).<init>(arrayOfInt, arrayOfInt2);
    C = ((ColorStateList)localObject2);
    a(paramContext, paramAttributeSet);
  }
  
  public FormItemEditText(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    int i1 = 4;
    i = i1;
    Object localObject1 = new android/graphics/Rect;
    ((Rect)localObject1).<init>();
    p = ((Rect)localObject1);
    q = false;
    s = null;
    u = 1.0F;
    v = 2.0F;
    x = false;
    y = false;
    Object localObject2 = new int[i1][];
    int i2 = 1;
    int[] arrayOfInt1 = new int[i2];
    arrayOfInt1[0] = 16842913;
    localObject2[0] = arrayOfInt1;
    arrayOfInt1 = new int[i2];
    arrayOfInt1[0] = 16842914;
    localObject2[i2] = arrayOfInt1;
    arrayOfInt1 = new int[i2];
    arrayOfInt1[0] = 16842908;
    localObject2[2] = arrayOfInt1;
    localObject1 = new int[i2];
    localObject1[0] = -16842908;
    localObject2[3] = localObject1;
    A = ((int[][])localObject2);
    localObject2 = new int[i1];
    Object tmp199_197 = localObject2;
    Object tmp200_199 = tmp199_197;
    Object tmp200_199 = tmp199_197;
    tmp200_199[0] = -16711936;
    tmp200_199[1] = -65536;
    tmp200_199[2] = -16777216;
    tmp200_199[3] = -7829368;
    B = ((int[])localObject2);
    localObject2 = new android/content/res/ColorStateList;
    int[][] arrayOfInt = A;
    int[] arrayOfInt2 = B;
    ((ColorStateList)localObject2).<init>(arrayOfInt, arrayOfInt2);
    C = ((ColorStateList)localObject2);
    a(paramContext, paramAttributeSet);
  }
  
  public FormItemEditText(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2)
  {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    int i1 = 4;
    i = i1;
    Object localObject1 = new android/graphics/Rect;
    ((Rect)localObject1).<init>();
    p = ((Rect)localObject1);
    q = false;
    s = null;
    u = 1.0F;
    v = 2.0F;
    x = false;
    y = false;
    Object localObject2 = new int[i1][];
    int i2 = 1;
    int[] arrayOfInt1 = new int[i2];
    arrayOfInt1[0] = 16842913;
    localObject2[0] = arrayOfInt1;
    arrayOfInt1 = new int[i2];
    arrayOfInt1[0] = 16842914;
    localObject2[i2] = arrayOfInt1;
    arrayOfInt1 = new int[i2];
    arrayOfInt1[0] = 16842908;
    localObject2[2] = arrayOfInt1;
    localObject1 = new int[i2];
    localObject1[0] = -16842908;
    localObject2[3] = localObject1;
    A = ((int[][])localObject2);
    localObject2 = new int[i1];
    Object tmp201_199 = localObject2;
    Object tmp202_201 = tmp201_199;
    Object tmp202_201 = tmp201_199;
    tmp202_201[0] = -16711936;
    tmp202_201[1] = -65536;
    tmp202_201[2] = -16777216;
    tmp202_201[3] = -7829368;
    B = ((int[])localObject2);
    localObject2 = new android/content/res/ColorStateList;
    int[][] arrayOfInt = A;
    int[] arrayOfInt2 = B;
    ((ColorStateList)localObject2).<init>(arrayOfInt, arrayOfInt2);
    C = ((ColorStateList)localObject2);
    a(paramContext, paramAttributeSet);
  }
  
  private int a(int... paramVarArgs)
  {
    return C.getColorForState(paramVarArgs, -7829368);
  }
  
  private void a(Context paramContext, AttributeSet paramAttributeSet)
  {
    float f1 = u;
    f1 = a(f1);
    u = f1;
    f1 = v;
    f1 = a(f1);
    v = f1;
    f1 = e;
    f1 = a(f1);
    e = f1;
    f1 = h;
    int i1 = a(f1);
    f1 = i1;
    h = f1;
    Object localObject1 = R.styleable.FormItemEditText;
    boolean bool1 = false;
    localObject1 = paramContext.obtainStyledAttributes(paramAttributeSet, (int[])localObject1, 0, 0);
    try
    {
      Object localObject2 = new android/util/TypedValue;
      ((TypedValue)localObject2).<init>();
      int i2 = R.styleable.FormItemEditText_pinAnimationType;
      ((TypedArray)localObject1).getValue(i2, (TypedValue)localObject2);
      int i4 = data;
      d = i4;
      i4 = R.styleable.FormItemEditText_pinCharacterMask;
      localObject2 = ((TypedArray)localObject1).getString(i4);
      a = ((String)localObject2);
      i4 = R.styleable.FormItemEditText_pinRepeatedHint;
      localObject2 = ((TypedArray)localObject1).getString(i4);
      c = ((String)localObject2);
      i4 = R.styleable.FormItemEditText_pinLineStroke;
      float f2 = u;
      float f3 = ((TypedArray)localObject1).getDimension(i4, f2);
      u = f3;
      i4 = R.styleable.FormItemEditText_pinLineStrokeSelected;
      f2 = v;
      f3 = ((TypedArray)localObject1).getDimension(i4, f2);
      v = f3;
      i4 = R.styleable.FormItemEditText_pinLineStrokeCentered;
      boolean bool2 = ((TypedArray)localObject1).getBoolean(i4, false);
      t = bool2;
      int i5 = R.styleable.FormItemEditText_pinCharacterSize;
      i2 = 0;
      f2 = 0.0F;
      f3 = ((TypedArray)localObject1).getDimension(i5, 0.0F);
      f = f3;
      i5 = R.styleable.FormItemEditText_pinCharacterSpacing;
      f2 = e;
      f3 = ((TypedArray)localObject1).getDimension(i5, f2);
      e = f3;
      i5 = R.styleable.FormItemEditText_pinTextBottomPadding;
      f2 = h;
      f3 = ((TypedArray)localObject1).getDimension(i5, f2);
      h = f3;
      i5 = R.styleable.FormItemEditText_pinBackgroundIsSquare;
      int i3 = q;
      boolean bool3 = ((TypedArray)localObject1).getBoolean(i5, i3);
      q = bool3;
      int i6 = R.styleable.FormItemEditText_pinBackgroundDrawable;
      localObject2 = ((TypedArray)localObject1).getDrawable(i6);
      o = ((Drawable)localObject2);
      i6 = R.styleable.FormItemEditText_pinLineColors;
      localObject2 = ((TypedArray)localObject1).getColorStateList(i6);
      if (localObject2 != null) {
        C = ((ColorStateList)localObject2);
      }
      ((TypedArray)localObject1).recycle();
      localObject1 = new android/graphics/Paint;
      localObject2 = getPaint();
      ((Paint)localObject1).<init>((Paint)localObject2);
      l = ((Paint)localObject1);
      localObject1 = new android/graphics/Paint;
      localObject2 = getPaint();
      ((Paint)localObject1).<init>((Paint)localObject2);
      m = ((Paint)localObject1);
      localObject1 = new android/graphics/Paint;
      localObject2 = getPaint();
      ((Paint)localObject1).<init>((Paint)localObject2);
      n = ((Paint)localObject1);
      localObject1 = new android/graphics/Paint;
      localObject2 = getPaint();
      ((Paint)localObject1).<init>((Paint)localObject2);
      w = ((Paint)localObject1);
      localObject1 = w;
      f3 = u;
      ((Paint)localObject1).setStrokeWidth(f3);
      f1 = f;
      setFontSize(f1);
      localObject1 = new android/util/TypedValue;
      ((TypedValue)localObject1).<init>();
      paramContext = paramContext.getTheme();
      i6 = R.attr.colorControlActivated;
      i3 = 1;
      f2 = Float.MIN_VALUE;
      paramContext.resolveAttribute(i6, (TypedValue)localObject1, i3);
      int i7 = data;
      localObject1 = B;
      localObject1[0] = i7;
      i7 = -7829368;
      localObject1[i3] = i7;
      localObject1[2] = i7;
      setBackgroundResource(0);
      localObject1 = "maxLength";
      i6 = 4;
      f3 = 5.6E-45F;
      i7 = paramAttributeSet.getAttributeIntValue("http://schemas.android.com/apk/res/android", (String)localObject1, i6);
      i = i7;
      float f4 = i;
      g = f4;
      paramContext = new org/npci/upi/security/pinactivitycomponent/widget/b;
      paramContext.<init>(this);
      super.setOnClickListener(paramContext);
      paramContext = new org/npci/upi/security/pinactivitycomponent/widget/c;
      paramContext.<init>(this);
      super.setOnLongClickListener(paramContext);
      i7 = getInputType();
      int i10 = 128;
      i7 &= i10;
      if (i7 == i10)
      {
        paramContext = a;
        boolean bool4 = TextUtils.isEmpty(paramContext);
        if (!bool4) {}
      }
      do
      {
        paramContext = "●";
        a = paramContext;
        break;
        int i8 = getInputType();
        i10 = 16;
        i8 &= i10;
        if (i8 != i10) {
          break;
        }
        paramContext = a;
        bool5 = TextUtils.isEmpty(paramContext);
      } while (bool5);
      paramContext = a;
      boolean bool5 = TextUtils.isEmpty(paramContext);
      if (!bool5)
      {
        paramContext = getMaskChars();
        b = paramContext;
      }
      paramContext = getPaint();
      paramAttributeSet = "|";
      localObject1 = p;
      paramContext.getTextBounds(paramAttributeSet, 0, i3, (Rect)localObject1);
      int i9 = d;
      if (i9 >= 0) {
        bool1 = true;
      }
      x = bool1;
      return;
    }
    finally
    {
      ((TypedArray)localObject1).recycle();
    }
  }
  
  private void a(CharSequence paramCharSequence)
  {
    Object localObject1 = m;
    int i1 = 125;
    ((Paint)localObject1).setAlpha(i1);
    int i2 = 2;
    localObject1 = new int[i2];
    Object tmp22_21 = localObject1;
    tmp22_21[0] = 125;
    tmp22_21[1] = 'ÿ';
    localObject1 = ValueAnimator.ofInt((int[])localObject1);
    long l1 = 150L;
    ((ValueAnimator)localObject1).setDuration(l1);
    Object localObject2 = new org/npci/upi/security/pinactivitycomponent/widget/d;
    ((d)localObject2).<init>(this);
    ((ValueAnimator)localObject1).addUpdateListener((ValueAnimator.AnimatorUpdateListener)localObject2);
    localObject2 = new android/animation/AnimatorSet;
    ((AnimatorSet)localObject2).<init>();
    int i3 = paramCharSequence.length();
    int i4 = i;
    if (i3 == i4)
    {
      paramCharSequence = s;
      if (paramCharSequence != null)
      {
        paramCharSequence = new org/npci/upi/security/pinactivitycomponent/widget/e;
        paramCharSequence.<init>(this);
        ((AnimatorSet)localObject2).addListener(paramCharSequence);
      }
    }
    paramCharSequence = new Animator[1];
    paramCharSequence[0] = localObject1;
    ((AnimatorSet)localObject2).playTogether(paramCharSequence);
    ((AnimatorSet)localObject2).start();
  }
  
  private void a(CharSequence paramCharSequence, int paramInt)
  {
    float[] arrayOfFloat = k;
    float f1 = j[paramInt].bottom;
    float f2 = h;
    f1 -= f2;
    arrayOfFloat[paramInt] = f1;
    int i1 = 2;
    Object localObject1 = new float[i1];
    f2 = k[paramInt];
    float f3 = getPaint().getTextSize();
    f2 += f3;
    f3 = 0.0F;
    localObject1[0] = f2;
    f2 = k[paramInt];
    int i2 = 1;
    localObject1[i2] = f2;
    localObject1 = ValueAnimator.ofFloat((float[])localObject1);
    long l1 = 300L;
    ((ValueAnimator)localObject1).setDuration(l1);
    Object localObject2 = new android/view/animation/OvershootInterpolator;
    ((OvershootInterpolator)localObject2).<init>();
    ((ValueAnimator)localObject1).setInterpolator((TimeInterpolator)localObject2);
    localObject2 = new org/npci/upi/security/pinactivitycomponent/widget/f;
    ((f)localObject2).<init>(this, paramInt);
    ((ValueAnimator)localObject1).addUpdateListener((ValueAnimator.AnimatorUpdateListener)localObject2);
    Object localObject3 = m;
    int i3 = 255;
    f2 = 3.57E-43F;
    ((Paint)localObject3).setAlpha(i3);
    localObject3 = new int[i1];
    Object tmp181_179 = localObject3;
    tmp181_179[0] = 0;
    tmp181_179[1] = 'ÿ';
    localObject3 = ValueAnimator.ofInt((int[])localObject3);
    ((ValueAnimator)localObject3).setDuration(l1);
    localObject2 = new org/npci/upi/security/pinactivitycomponent/widget/g;
    ((g)localObject2).<init>(this);
    ((ValueAnimator)localObject3).addUpdateListener((ValueAnimator.AnimatorUpdateListener)localObject2);
    localObject2 = new android/animation/AnimatorSet;
    ((AnimatorSet)localObject2).<init>();
    int i4 = paramCharSequence.length();
    int i5 = i;
    if (i4 == i5)
    {
      paramCharSequence = s;
      if (paramCharSequence != null)
      {
        paramCharSequence = new org/npci/upi/security/pinactivitycomponent/widget/h;
        paramCharSequence.<init>(this);
        ((AnimatorSet)localObject2).addListener(paramCharSequence);
      }
    }
    paramCharSequence = new Animator[i1];
    paramCharSequence[0] = localObject1;
    paramCharSequence[i2] = localObject3;
    ((AnimatorSet)localObject2).playTogether(paramCharSequence);
    ((AnimatorSet)localObject2).start();
  }
  
  private void a(boolean paramBoolean1, boolean paramBoolean2)
  {
    boolean bool1 = y;
    int i1 = 1;
    Object localObject;
    int[] arrayOfInt;
    if (bool1)
    {
      localObject = w;
      arrayOfInt = new int[i1];
      arrayOfInt[0] = 16842914;
      paramBoolean2 = a(arrayOfInt);
      ((Paint)localObject).setColor(paramBoolean2);
      return;
    }
    Paint localPaint = w;
    boolean bool2 = isFocused();
    float f1;
    if (bool2) {
      f1 = v;
    } else {
      f1 = u;
    }
    localPaint.setStrokeWidth(f1);
    if (paramBoolean1)
    {
      localObject = w;
      arrayOfInt = new int[i1];
      arrayOfInt[0] = 16842913;
      paramBoolean2 = a(arrayOfInt);
      ((Paint)localObject).setColor(paramBoolean2);
      return;
    }
    if (paramBoolean2)
    {
      paramBoolean1 = isFocused();
      if (paramBoolean1)
      {
        localObject = new int[i1];
        paramBoolean2 = 16842918;
        localObject[0] = paramBoolean2;
      }
      else
      {
        localObject = new int[i1];
        paramBoolean2 = -16842918;
        localObject[0] = paramBoolean2;
      }
      paramBoolean1 = a((int[])localObject);
      w.setColor(paramBoolean1);
      return;
    }
    paramBoolean1 = isFocused();
    if (paramBoolean1)
    {
      localObject = new int[i1];
      paramBoolean2 = 16842908;
      localObject[0] = paramBoolean2;
    }
    else
    {
      localObject = new int[i1];
      paramBoolean2 = -16842908;
      localObject[0] = paramBoolean2;
    }
    paramBoolean1 = a((int[])localObject);
    w.setColor(paramBoolean1);
  }
  
  private void b(boolean paramBoolean1, boolean paramBoolean2)
  {
    boolean bool1 = y;
    int i3 = 1;
    if (bool1)
    {
      localDrawable1 = o;
      arrayOfInt1 = new int[i3];
      int i1 = 16842914;
      arrayOfInt1[0] = i1;
    }
    for (;;)
    {
      localDrawable1.setState(arrayOfInt1);
      return;
      boolean bool2 = isFocused();
      if (!bool2) {
        break;
      }
      Drawable localDrawable2 = o;
      int[] arrayOfInt2 = new int[i3];
      int i4 = 16842908;
      arrayOfInt2[0] = i4;
      localDrawable2.setState(arrayOfInt2);
      i2 = 2;
      if (paramBoolean2)
      {
        localDrawable1 = o;
        arrayOfInt1 = new int[i2];
        int[] tmp99_97 = arrayOfInt1;
        tmp99_97[0] = 16842908;
        tmp99_97[1] = 16842913;
      }
      else
      {
        if (!paramBoolean1) {
          return;
        }
        localDrawable1 = o;
        arrayOfInt1 = new int[i2];
        int[] tmp128_126 = arrayOfInt1;
        tmp128_126[0] = 16842908;
        tmp128_126[1] = 16842912;
      }
    }
    Drawable localDrawable1 = o;
    int[] arrayOfInt1 = new int[i3];
    int i2 = -16842908;
    arrayOfInt1[0] = i2;
    localDrawable1.setState(arrayOfInt1);
  }
  
  private CharSequence getFullText()
  {
    String str = a;
    if (str == null) {
      return getText();
    }
    return getMaskChars();
  }
  
  private StringBuilder getMaskChars()
  {
    Object localObject = b;
    if (localObject == null)
    {
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      b = ((StringBuilder)localObject);
    }
    localObject = getText();
    int i1 = ((Editable)localObject).length();
    for (;;)
    {
      StringBuilder localStringBuilder = b;
      int i2 = localStringBuilder.length();
      if (i2 == i1) {
        break;
      }
      localStringBuilder = b;
      i2 = localStringBuilder.length();
      if (i2 < i1)
      {
        localStringBuilder = b;
        String str = a;
        localStringBuilder.append(str);
      }
      else
      {
        localStringBuilder = b;
        int i3 = localStringBuilder.length() + -1;
        localStringBuilder.deleteCharAt(i3);
      }
    }
    return b;
  }
  
  int a(float paramFloat)
  {
    float f1 = getResourcesgetDisplayMetricsdensityDpi / 160;
    return (int)(paramFloat * f1);
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    CharSequence localCharSequence = getFullText();
    int i1 = localCharSequence.length();
    float[] arrayOfFloat = new float[i1];
    getPaint().getTextWidths(localCharSequence, 0, i1, arrayOfFloat);
    Object localObject1 = c;
    int i2 = 0;
    float f1 = 0.0F;
    Object localObject2 = null;
    Object localObject3;
    Object localObject4;
    int i8;
    float f2;
    float f3;
    float f4;
    if (localObject1 != null)
    {
      int i5 = ((String)localObject1).length();
      localObject1 = new float[i5];
      localObject3 = getPaint();
      localObject4 = c;
      ((TextPaint)localObject3).getTextWidths((String)localObject4, (float[])localObject1);
      int i6 = localObject1.length;
      i2 = 0;
      f1 = 0.0F;
      localObject2 = null;
      i8 = 0;
      f2 = 0.0F;
      localObject4 = null;
      while (i2 < i6)
      {
        f3 = localObject1[i2];
        f2 += f3;
        i2 += 1;
      }
      f4 = f2;
    }
    else
    {
      f4 = 0.0F;
    }
    int i9 = 0;
    for (;;)
    {
      float f5 = i9;
      f1 = g;
      boolean bool2 = f5 < f1;
      if (!bool2) {
        break;
      }
      localObject1 = o;
      boolean bool3 = true;
      if (localObject1 != null)
      {
        if (i9 < i1)
        {
          bool2 = true;
          f5 = Float.MIN_VALUE;
        }
        else
        {
          bool2 = false;
          f5 = 0.0F;
          localObject1 = null;
        }
        if (i9 == i1)
        {
          i2 = 1;
          f1 = Float.MIN_VALUE;
        }
        else
        {
          i2 = 0;
          f1 = 0.0F;
          localObject2 = null;
        }
        b(bool2, i2);
        localObject1 = o;
        localObject2 = j[i9];
        f1 = left;
        int i3 = (int)f1;
        localObject3 = j[i9];
        f6 = top;
        i7 = (int)f6;
        localObject4 = j[i9];
        f2 = right;
        i8 = (int)f2;
        RectF localRectF = j[i9];
        f3 = bottom;
        int i10 = (int)f3;
        ((Drawable)localObject1).setBounds(i3, i7, i8, i10);
        localObject1 = o;
        ((Drawable)localObject1).draw(paramCanvas);
      }
      localObject1 = j[i9];
      f5 = left;
      f1 = f;
      int i7 = 1073741824;
      float f6 = 2.0F;
      f1 /= f6;
      f5 += f1;
      float f7;
      Paint localPaint;
      if (i1 > i9)
      {
        boolean bool1 = x;
        if (bool1)
        {
          int i4 = i1 + -1;
          if (i9 == i4)
          {
            i8 = i9 + 1;
            f1 = arrayOfFloat[i9] / f6;
            f3 = f5 - f1;
            localObject1 = k;
            f7 = localObject1[i9];
            localPaint = m;
            break label524;
          }
        }
        i8 = i9 + 1;
        f1 = arrayOfFloat[i9] / f6;
        f3 = f5 - f1;
        localObject1 = k;
        f7 = localObject1[i9];
        localPaint = l;
        label524:
        localObject1 = paramCanvas;
        localObject2 = localCharSequence;
        i7 = i9;
        paramCanvas.drawText(localCharSequence, i9, i8, f3, f7, localPaint);
      }
      else
      {
        localObject2 = c;
        if (localObject2 != null)
        {
          f6 = f4 / f6;
          f5 -= f6;
          localObject3 = k;
          f6 = localObject3[i9];
          localObject4 = n;
          paramCanvas.drawText((String)localObject2, f5, f6, (Paint)localObject4);
        }
      }
      localObject1 = o;
      if (localObject1 == null)
      {
        if (i9 < i1)
        {
          bool2 = true;
          f5 = Float.MIN_VALUE;
        }
        else
        {
          bool2 = false;
          f5 = 0.0F;
          localObject1 = null;
        }
        if (i9 != i1) {
          bool3 = false;
        }
        a(bool2, bool3);
        f6 = j[i9].left;
        f2 = j[i9].top;
        f3 = j[i9].right;
        localObject1 = j[i9];
        f7 = bottom;
        localPaint = w;
        localObject2 = paramCanvas;
        paramCanvas.drawLine(f6, f2, f3, f7, localPaint);
      }
      i9 += 1;
    }
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    Object localObject1 = getTextColors();
    z = ((ColorStateList)localObject1);
    localObject1 = z;
    if (localObject1 != null)
    {
      localObject2 = m;
      paramInt1 = ((ColorStateList)localObject1).getDefaultColor();
      ((Paint)localObject2).setColor(paramInt1);
      localObject1 = l;
      localObject2 = z;
      paramInt2 = ((ColorStateList)localObject2).getDefaultColor();
      ((Paint)localObject1).setColor(paramInt2);
      localObject1 = n;
      paramInt2 = getCurrentHintTextColor();
      ((Paint)localObject1).setColor(paramInt2);
    }
    paramInt1 = getWidth();
    paramInt2 = r.k(this);
    paramInt1 -= paramInt2;
    paramInt2 = r.j(this);
    paramInt1 -= paramInt2;
    float f1 = e;
    paramInt3 = 1065353216;
    float f2 = 1.0F;
    paramInt4 = 0;
    float f3 = 2.0F;
    int i1 = f1 < 0.0F;
    if (i1 < 0)
    {
      f4 = paramInt1;
      f1 = g * f3 - f2;
    }
    for (f4 /= f1;; f4 = (f4 - f1) / f5)
    {
      f = f4;
      break;
      f5 = f;
      i1 = f5 < 0.0F;
      if (i1 != 0) {
        break;
      }
      f4 = paramInt1;
      f5 = g;
      f2 = f5 - f2;
      f1 *= f2;
    }
    float f4 = g;
    Object localObject2 = new RectF[(int)f4];
    j = ((RectF[])localObject2);
    localObject1 = new float[(int)f4];
    k = ((float[])localObject1);
    paramInt1 = getHeight();
    paramInt2 = getPaddingBottom();
    paramInt1 -= paramInt2;
    paramInt2 = getPaddingTop();
    paramInt1 -= paramInt2;
    localObject2 = Locale.getDefault();
    paramInt2 = android.support.v4.e.f.a((Locale)localObject2);
    paramInt3 = 0;
    f2 = 0.0F;
    i1 = 1;
    float f5 = Float.MIN_VALUE;
    if (paramInt2 == i1)
    {
      paramInt2 = 1;
      f1 = Float.MIN_VALUE;
    }
    else
    {
      paramInt2 = 0;
      f1 = 0.0F;
      localObject2 = null;
    }
    float f6;
    if (paramInt2 != 0)
    {
      i1 = -1;
      f5 = 0.0F / 0.0F;
      paramInt2 = getWidth();
      int i2 = r.j(this);
      f1 = paramInt2 - i2;
      f6 = f;
      f1 -= f6;
      paramInt2 = (int)f1;
    }
    else
    {
      paramInt2 = r.j(this);
    }
    for (;;)
    {
      f6 = paramInt3;
      float f7 = g;
      boolean bool1 = f6 < f7;
      if (!bool1) {
        break;
      }
      Object localObject3 = j;
      RectF localRectF = new android/graphics/RectF;
      f1 = paramInt2;
      float f8 = paramInt1;
      float f9 = f + f1;
      localRectF.<init>(f1, f8, f9, f8);
      localObject3[paramInt3] = localRectF;
      localObject3 = o;
      if (localObject3 != null)
      {
        bool1 = q;
        if (bool1)
        {
          localObject3 = j[paramInt3];
          int i3 = getPaddingTop();
          f7 = i3;
          top = f7;
          localObject3 = j;
          localRectF = localObject3[paramInt3];
          localObject3 = localObject3[paramInt3];
          f6 = ((RectF)localObject3).height() + f1;
          right = f6;
        }
        else
        {
          localObject3 = j[paramInt3];
          f7 = top;
          Rect localRect = p;
          int i4 = localRect.height();
          f8 = i4;
          f9 = h * f3;
          f8 += f9;
          f7 -= f8;
          top = f7;
        }
      }
      f6 = e;
      boolean bool2 = f6 < 0.0F;
      if (bool2)
      {
        f6 = i1;
        f7 = f;
        f6 = f6 * f7 * f3;
        f1 += f6;
      }
      else
      {
        f7 = i1;
        f8 = f + f6;
        f7 *= f8;
        f1 += f7;
      }
      paramInt2 = (int)f1;
      localObject3 = k;
      localRectF = j[paramInt3];
      f7 = bottom;
      f8 = h;
      f7 -= f8;
      localObject3[paramInt3] = f7;
      bool1 = t;
      if (bool1)
      {
        localObject3 = j;
        localRectF = localObject3[paramInt3];
        f6 = top / f3;
        top = f6;
        localObject3 = j;
        localRectF = localObject3[paramInt3];
        localObject3 = localObject3[paramInt3];
        f6 = bottom / f3;
        bottom = f6;
      }
      paramInt3 += 1;
    }
  }
  
  protected void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
  {
    boolean bool = false;
    setError(false);
    RectF[] arrayOfRectF = j;
    if (arrayOfRectF != null)
    {
      bool = x;
      if (bool)
      {
        int i1 = d;
        int i2 = -1;
        if (i1 == i2)
        {
          invalidate();
          return;
        }
        if (paramInt3 > paramInt2)
        {
          if (i1 == 0)
          {
            a(paramCharSequence);
            return;
          }
          a(paramCharSequence, paramInt1);
        }
        return;
      }
    }
    i locali = s;
    if (locali != null)
    {
      paramInt1 = paramCharSequence.length();
      paramInt2 = i;
      if (paramInt1 == paramInt2)
      {
        locali = s;
        locali.a(paramCharSequence);
      }
    }
  }
  
  public void setAnimateText(boolean paramBoolean)
  {
    x = paramBoolean;
  }
  
  public void setCharSize(float paramFloat)
  {
    f = paramFloat;
    invalidate();
  }
  
  public void setColorStates(ColorStateList paramColorStateList)
  {
    C = paramColorStateList;
    invalidate();
  }
  
  public void setCustomSelectionActionModeCallback(ActionMode.Callback paramCallback)
  {
    paramCallback = new java/lang/RuntimeException;
    paramCallback.<init>("setCustomSelectionActionModeCallback() not supported.");
    throw paramCallback;
  }
  
  public void setError(boolean paramBoolean)
  {
    y = paramBoolean;
  }
  
  public void setFontSize(float paramFloat)
  {
    l.setTextSize(paramFloat);
    m.setTextSize(paramFloat);
    n.setTextSize(paramFloat);
  }
  
  public void setLineStroke(float paramFloat)
  {
    u = paramFloat;
    invalidate();
  }
  
  public void setLineStrokeCentered(boolean paramBoolean)
  {
    t = paramBoolean;
    invalidate();
  }
  
  public void setLineStrokeSelected(float paramFloat)
  {
    v = paramFloat;
    invalidate();
  }
  
  public void setMargin(int[] paramArrayOfInt)
  {
    ViewGroup.MarginLayoutParams localMarginLayoutParams = (ViewGroup.MarginLayoutParams)getLayoutParams();
    int i1 = paramArrayOfInt[0];
    int i2 = paramArrayOfInt[1];
    int i3 = paramArrayOfInt[2];
    int i4 = paramArrayOfInt[3];
    localMarginLayoutParams.setMargins(i1, i2, i3, i4);
    setLayoutParams(localMarginLayoutParams);
  }
  
  public void setMaxLength(int paramInt)
  {
    i = paramInt;
    float f1 = paramInt;
    g = f1;
    InputFilter[] arrayOfInputFilter = new InputFilter[1];
    InputFilter.LengthFilter localLengthFilter = new android/text/InputFilter$LengthFilter;
    localLengthFilter.<init>(paramInt);
    arrayOfInputFilter[0] = localLengthFilter;
    setFilters(arrayOfInputFilter);
  }
  
  public void setOnClickListener(View.OnClickListener paramOnClickListener)
  {
    r = paramOnClickListener;
  }
  
  public void setOnPinEnteredListener(i parami)
  {
    s = parami;
  }
  
  public void setSpace(float paramFloat)
  {
    e = paramFloat;
    invalidate();
  }
}

/* Location:
 * Qualified Name:     org.npci.upi.security.pinactivitycomponent.widget.FormItemEditText
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */