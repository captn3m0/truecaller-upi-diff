package org.npci.upi.security.pinactivitycomponent.widget;

import android.text.Editable;
import android.view.View;
import android.view.View.OnClickListener;

class b
  implements View.OnClickListener
{
  b(FormItemEditText paramFormItemEditText) {}
  
  public void onClick(View paramView)
  {
    Object localObject = a;
    Editable localEditable = ((FormItemEditText)localObject).getText();
    int i = localEditable.length();
    ((FormItemEditText)localObject).setSelection(i);
    localObject = FormItemEditText.a(a);
    if (localObject != null)
    {
      localObject = FormItemEditText.a(a);
      ((View.OnClickListener)localObject).onClick(paramView);
    }
  }
}

/* Location:
 * Qualified Name:     org.npci.upi.security.pinactivitycomponent.widget.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */