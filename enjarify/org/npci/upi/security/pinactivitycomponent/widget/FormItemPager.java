package org.npci.upi.security.pinactivitycomponent.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.v4.view.r;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewPropertyAnimator;
import android.widget.FrameLayout;
import java.util.ArrayList;
import org.npci.upi.security.pinactivitycomponent.R.string;

public class FormItemPager
  extends FrameLayout
  implements a
{
  private ArrayList a;
  private int b;
  private int c;
  private Object d;
  
  public FormItemPager(Context paramContext)
  {
    super(paramContext);
  }
  
  public FormItemPager(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  public FormItemPager(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  private void a(View paramView)
  {
    int i = Build.VERSION.SDK_INT;
    int j = 12;
    if (i >= j)
    {
      paramView.animate().x(0.0F);
      return;
    }
    r.a(paramView, 0.0F);
  }
  
  private void a(View paramView, boolean paramBoolean)
  {
    if (paramBoolean) {
      paramBoolean = c * -1;
    } else {
      paramBoolean = c;
    }
    int i = Build.VERSION.SDK_INT;
    int j = 12;
    if (i >= j)
    {
      paramView = paramView.animate();
      f = paramBoolean;
      paramView.x(f);
      return;
    }
    float f = paramBoolean;
    r.a(paramView, f);
  }
  
  private void e()
  {
    int i = 0;
    for (;;)
    {
      Object localObject = a;
      int j = ((ArrayList)localObject).size();
      if (i >= j) {
        break;
      }
      localObject = (FormItemView)a.get(i);
      String str = "";
      ((FormItemView)localObject).setText(str);
      i += 1;
    }
    d();
  }
  
  public void a(String paramString, Drawable paramDrawable, View.OnClickListener paramOnClickListener, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
  {
    int i = 0;
    for (;;)
    {
      Object localObject1 = a;
      int j = ((ArrayList)localObject1).size();
      if (i >= j) {
        break;
      }
      localObject1 = a.get(i);
      Object localObject2 = localObject1;
      localObject2 = (FormItemView)localObject1;
      ((FormItemView)localObject2).a(paramString, paramDrawable, paramOnClickListener, paramInt, paramBoolean1, paramBoolean2);
      i += 1;
    }
  }
  
  public void a(ArrayList paramArrayList, m paramm)
  {
    a = paramArrayList;
    paramArrayList = a;
    int i = 0;
    Object localObject = null;
    paramArrayList = (View)paramArrayList.get(0);
    addView(paramArrayList);
    ((FormItemView)a.get(0)).setFormItemListener(paramm);
    b = 0;
    paramArrayList = getResources().getDisplayMetrics();
    int j = widthPixels;
    c = j;
    j = 1;
    for (;;)
    {
      localObject = a;
      i = ((ArrayList)localObject).size();
      if (j >= i) {
        break;
      }
      localObject = (FormItemView)a.get(j);
      ((FormItemView)localObject).setFormItemListener(paramm);
      int k = c;
      float f = k;
      r.a((View)localObject, f);
      addView((View)localObject);
      j += 1;
    }
  }
  
  public boolean a()
  {
    Object localObject1 = a;
    int i = b;
    localObject1 = ((FormItemView)((ArrayList)localObject1).get(i)).getInputValue();
    Object localObject2 = a;
    int j = b;
    localObject2 = (FormItemView)((ArrayList)localObject2).get(j);
    i = ((FormItemView)localObject2).getInputLength();
    j = ((String)localObject1).length();
    if (i == j)
    {
      i = b;
      Object localObject3 = a;
      j = ((ArrayList)localObject3).size();
      int m = 1;
      j -= m;
      if (i == j)
      {
        localObject2 = a;
        j = b;
        ((FormItemView)((ArrayList)localObject2).get(j)).requestFocus();
        i = 0;
        localObject2 = null;
        for (;;)
        {
          localObject3 = a;
          j = ((ArrayList)localObject3).size();
          if (i >= j) {
            break;
          }
          localObject3 = ((FormItemView)a.get(i)).getInputValue();
          boolean bool1 = ((String)localObject3).equals(localObject1);
          if (!bool1)
          {
            e();
            localObject1 = ((FormItemView)a.get(i)).getFormItemListener();
            localObject2 = getContext();
            int k = R.string.info_pins_dont_match;
            localObject2 = ((Context)localObject2).getString(k);
            ((m)localObject1).a(this, (String)localObject2);
            return false;
          }
          i += 1;
        }
        return m;
      }
      boolean bool2 = c();
      if (!bool2) {
        return m;
      }
      return false;
    }
    localObject1 = a;
    i = b;
    ((FormItemView)((ArrayList)localObject1).get(i)).requestFocus();
    return false;
  }
  
  public boolean b()
  {
    ArrayList localArrayList = a;
    int i = b;
    return ((FormItemView)localArrayList.get(i)).b();
  }
  
  public boolean c()
  {
    int i = b;
    ArrayList localArrayList = a;
    int j = localArrayList.size();
    int k = 1;
    j -= k;
    if (i >= j) {
      return false;
    }
    Object localObject = a;
    j = b;
    localObject = (FormItemView)((ArrayList)localObject).get(j);
    a((View)localObject, k);
    localObject = a;
    j = b + k;
    localObject = (FormItemView)((ArrayList)localObject).get(j);
    a((View)localObject);
    i = b + k;
    b = i;
    localObject = a;
    j = b;
    ((FormItemView)((ArrayList)localObject).get(j)).requestFocus();
    return k;
  }
  
  public boolean d()
  {
    int i = b;
    int j = 0;
    if (i == 0) {
      return false;
    }
    Object localObject = (FormItemView)a.get(i);
    a((View)localObject, false);
    localObject = a;
    j = b;
    int k = 1;
    j -= k;
    localObject = (FormItemView)((ArrayList)localObject).get(j);
    a((View)localObject);
    i = b - k;
    b = i;
    localObject = a;
    j = b;
    ((FormItemView)((ArrayList)localObject).get(j)).requestFocus();
    return k;
  }
  
  public Object getFormDataTag()
  {
    Object localObject = d;
    if (localObject == null) {
      localObject = ((FormItemView)a.get(0)).getFormDataTag();
    }
    return localObject;
  }
  
  public String getInputValue()
  {
    return ((FormItemView)a.get(0)).getInputValue();
  }
  
  public void setFormDataTag(Object paramObject)
  {
    d = paramObject;
  }
  
  public void setText(String paramString)
  {
    int i = 0;
    for (;;)
    {
      Object localObject = a;
      int j = ((ArrayList)localObject).size();
      if (i >= j) {
        break;
      }
      localObject = (FormItemView)a.get(i);
      ((FormItemView)localObject).setText(paramString);
      i += 1;
    }
  }
}

/* Location:
 * Qualified Name:     org.npci.upi.security.pinactivitycomponent.widget.FormItemPager
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */