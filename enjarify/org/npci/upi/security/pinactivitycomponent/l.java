package org.npci.upi.security.pinactivitycomponent;

import android.content.Context;
import android.content.res.AssetManager;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class l
{
  private static ByteArrayOutputStream a(ByteArrayOutputStream paramByteArrayOutputStream, InputStream paramInputStream)
  {
    int i = 4096;
    byte[] arrayOfByte = new byte[i];
    for (;;)
    {
      int j = paramInputStream.read(arrayOfByte);
      int k = -1;
      if (j == k) {
        break;
      }
      k = 0;
      paramByteArrayOutputStream.write(arrayOfByte, 0, j);
    }
    paramByteArrayOutputStream.close();
    paramInputStream.close();
    return paramByteArrayOutputStream;
  }
  
  public static byte[] a(String paramString, Context paramContext)
  {
    try
    {
      ByteArrayOutputStream localByteArrayOutputStream = new java/io/ByteArrayOutputStream;
      localByteArrayOutputStream.<init>();
      paramContext = paramContext.getAssets();
      String str = "npci/";
      paramString = String.valueOf(paramString);
      paramString = str.concat(paramString);
      paramString = paramContext.open(paramString);
      paramString = a(localByteArrayOutputStream, paramString);
      return paramString.toByteArray();
    }
    catch (Exception paramString)
    {
      paramContext = new java/lang/RuntimeException;
      paramContext.<init>(paramString);
      throw paramContext;
    }
    catch (IOException paramString)
    {
      paramContext = new java/lang/RuntimeException;
      paramContext.<init>(paramString);
      throw paramContext;
    }
    catch (FileNotFoundException paramString)
    {
      paramContext = new java/lang/RuntimeException;
      paramContext.<init>(paramString);
      throw paramContext;
    }
  }
}

/* Location:
 * Qualified Name:     org.npci.upi.security.pinactivitycomponent.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */