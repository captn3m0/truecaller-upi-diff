package org.c.a.a.a;

import java.lang.reflect.Array;

public final class a
{
  public static final Object[] a = new Object[0];
  public static final Class[] b = new Class[0];
  public static final String[] c = new String[0];
  public static final long[] d = new long[0];
  public static final Long[] e = new Long[0];
  public static final int[] f = new int[0];
  public static final Integer[] g = new Integer[0];
  public static final short[] h = new short[0];
  public static final Short[] i = new Short[0];
  public static final byte[] j = new byte[0];
  public static final Byte[] k = new Byte[0];
  public static final double[] l = new double[0];
  public static final Double[] m = new Double[0];
  public static final float[] n = new float[0];
  public static final Float[] o = new Float[0];
  public static final boolean[] p = new boolean[0];
  public static final Boolean[] q = new Boolean[0];
  public static final char[] r = new char[0];
  public static final Character[] s = new Character[0];
  
  private static Object a(Object paramObject, Class paramClass)
  {
    if (paramObject != null)
    {
      int i1 = Array.getLength(paramObject);
      Object localObject = paramObject.getClass().getComponentType();
      int i2 = i1 + 1;
      localObject = Array.newInstance((Class)localObject, i2);
      System.arraycopy(paramObject, 0, localObject, 0, i1);
      return localObject;
    }
    return Array.newInstance(paramClass, 1);
  }
  
  public static boolean a(byte[] paramArrayOfByte)
  {
    if (paramArrayOfByte != null)
    {
      int i1 = paramArrayOfByte.length;
      if (i1 != 0) {
        return false;
      }
    }
    return true;
  }
  
  public static boolean a(int[] paramArrayOfInt, int paramInt)
  {
    int i1 = -1;
    if (paramArrayOfInt != null)
    {
      i2 = 0;
      for (;;)
      {
        int i3 = paramArrayOfInt.length;
        if (i2 >= i3) {
          break;
        }
        i3 = paramArrayOfInt[i2];
        if (paramInt == i3) {
          break label41;
        }
        i2 += 1;
      }
    }
    int i2 = -1;
    label41:
    return i2 != i1;
  }
  
  public static boolean a(long[] paramArrayOfLong)
  {
    if (paramArrayOfLong != null)
    {
      int i1 = paramArrayOfLong.length;
      if (i1 != 0) {
        return false;
      }
    }
    return true;
  }
  
  public static boolean a(Object[] paramArrayOfObject)
  {
    if (paramArrayOfObject != null)
    {
      int i1 = paramArrayOfObject.length;
      if (i1 != 0) {
        return false;
      }
    }
    return true;
  }
  
  public static boolean a(Object[] paramArrayOfObject, Object paramObject)
  {
    int i1 = -1;
    if (paramArrayOfObject != null)
    {
      if (paramObject == null)
      {
        i2 = 0;
        paramObject = null;
        for (;;)
        {
          int i3 = paramArrayOfObject.length;
          if (i2 >= i3) {
            break;
          }
          localObject1 = paramArrayOfObject[i2];
          if (localObject1 == null) {
            break label119;
          }
          i2 += 1;
        }
      }
      Object localObject1 = paramArrayOfObject.getClass().getComponentType();
      int i4 = ((Class)localObject1).isInstance(paramObject);
      if (i4 != 0)
      {
        i4 = 0;
        localObject1 = null;
        for (;;)
        {
          int i6 = paramArrayOfObject.length;
          if (i4 >= i6) {
            break;
          }
          Object localObject2 = paramArrayOfObject[i4];
          boolean bool = paramObject.equals(localObject2);
          if (bool)
          {
            i2 = i4;
            break label119;
          }
          int i5;
          i4 += 1;
        }
      }
    }
    int i2 = -1;
    label119:
    return i2 != i1;
  }
  
  public static long[] a(Long[] paramArrayOfLong)
  {
    if (paramArrayOfLong == null) {
      return null;
    }
    int i1 = paramArrayOfLong.length;
    if (i1 == 0) {
      return d;
    }
    i1 = paramArrayOfLong.length;
    long[] arrayOfLong = new long[i1];
    int i2 = 0;
    for (;;)
    {
      int i3 = paramArrayOfLong.length;
      if (i2 >= i3) {
        break;
      }
      Long localLong = paramArrayOfLong[i2];
      long l1 = localLong.longValue();
      arrayOfLong[i2] = l1;
      i2 += 1;
    }
    return arrayOfLong;
  }
  
  public static Integer[] a(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null) {
      return null;
    }
    int i1 = paramArrayOfInt.length;
    if (i1 == 0) {
      return g;
    }
    i1 = paramArrayOfInt.length;
    Integer[] arrayOfInteger = new Integer[i1];
    int i2 = 0;
    for (;;)
    {
      int i3 = paramArrayOfInt.length;
      if (i2 >= i3) {
        break;
      }
      i3 = paramArrayOfInt[i2];
      Integer localInteger = Integer.valueOf(i3);
      arrayOfInteger[i2] = localInteger;
      i2 += 1;
    }
    return arrayOfInteger;
  }
  
  public static Object[] a(Object[] paramArrayOfObject1, Object... paramVarArgs)
  {
    if (paramArrayOfObject1 == null) {
      return c(paramVarArgs);
    }
    if (paramVarArgs == null) {
      return c(paramArrayOfObject1);
    }
    Class localClass = paramArrayOfObject1.getClass().getComponentType();
    int i1 = paramArrayOfObject1.length;
    int i2 = paramVarArgs.length;
    i1 += i2;
    Object localObject = (Object[])Array.newInstance(localClass, i1);
    i2 = paramArrayOfObject1.length;
    System.arraycopy(paramArrayOfObject1, 0, localObject, 0, i2);
    try
    {
      int i3 = paramArrayOfObject1.length;
      i2 = paramVarArgs.length;
      System.arraycopy(paramVarArgs, 0, localObject, i3, i2);
      return (Object[])localObject;
    }
    catch (ArrayStoreException paramArrayOfObject1)
    {
      paramVarArgs = paramVarArgs.getClass().getComponentType();
      boolean bool = localClass.isAssignableFrom(paramVarArgs);
      if (!bool)
      {
        localObject = new java/lang/IllegalArgumentException;
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>("Cannot store ");
        paramVarArgs = paramVarArgs.getName();
        localStringBuilder.append(paramVarArgs);
        localStringBuilder.append(" in an array of ");
        paramVarArgs = localClass.getName();
        localStringBuilder.append(paramVarArgs);
        paramVarArgs = localStringBuilder.toString();
        ((IllegalArgumentException)localObject).<init>(paramVarArgs, paramArrayOfObject1);
        throw ((Throwable)localObject);
      }
      throw paramArrayOfObject1;
    }
  }
  
  public static String[] a(String[] paramArrayOfString)
  {
    boolean bool = a(paramArrayOfString);
    if (bool) {
      return c;
    }
    return paramArrayOfString;
  }
  
  public static boolean b(long[] paramArrayOfLong)
  {
    if (paramArrayOfLong != null)
    {
      int i1 = paramArrayOfLong.length;
      if (i1 != 0) {
        return true;
      }
    }
    return false;
  }
  
  public static boolean b(Object[] paramArrayOfObject)
  {
    if (paramArrayOfObject != null)
    {
      int i1 = paramArrayOfObject.length;
      if (i1 != 0) {
        return true;
      }
    }
    return false;
  }
  
  public static int[] b(int[] paramArrayOfInt, int paramInt)
  {
    Class localClass = Integer.TYPE;
    paramArrayOfInt = (int[])a(paramArrayOfInt, localClass);
    int i1 = paramArrayOfInt.length + -1;
    paramArrayOfInt[i1] = paramInt;
    return paramArrayOfInt;
  }
  
  public static Object[] b(Object[] paramArrayOfObject, Object paramObject)
  {
    Class localClass;
    if (paramArrayOfObject != null)
    {
      localClass = paramArrayOfObject.getClass().getComponentType();
    }
    else
    {
      if (paramObject == null) {
        break label44;
      }
      localClass = paramObject.getClass();
    }
    paramArrayOfObject = (Object[])a(paramArrayOfObject, localClass);
    int i1 = paramArrayOfObject.length + -1;
    paramArrayOfObject[i1] = paramObject;
    return paramArrayOfObject;
    label44:
    paramArrayOfObject = new java/lang/IllegalArgumentException;
    paramArrayOfObject.<init>("Arguments cannot both be null");
    throw paramArrayOfObject;
  }
  
  private static Object[] c(Object[] paramArrayOfObject)
  {
    if (paramArrayOfObject == null) {
      return null;
    }
    return (Object[])paramArrayOfObject.clone();
  }
}

/* Location:
 * Qualified Name:     org.c.a.a.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */