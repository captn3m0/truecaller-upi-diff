package org.c.a.a.a;

public final class c
{
  public static int a(CharSequence paramCharSequence1, CharSequence paramCharSequence2, int paramInt)
  {
    paramCharSequence1 = paramCharSequence1.toString();
    paramCharSequence2 = paramCharSequence2.toString();
    return paramCharSequence1.indexOf(paramCharSequence2, paramInt);
  }
  
  static boolean a(CharSequence paramCharSequence1, boolean paramBoolean, int paramInt1, CharSequence paramCharSequence2, int paramInt2)
  {
    boolean bool1 = paramCharSequence1 instanceof String;
    int j;
    if (bool1)
    {
      bool1 = paramCharSequence2 instanceof String;
      if (bool1)
      {
        localObject1 = paramCharSequence1;
        localObject1 = (String)paramCharSequence1;
        Object localObject2 = paramCharSequence2;
        localObject2 = (String)paramCharSequence2;
        boolean bool2 = paramBoolean;
        j = paramInt1;
        return ((String)localObject1).regionMatches(paramBoolean, paramInt1, (String)localObject2, 0, paramInt2);
      }
    }
    bool1 = false;
    int k = 0;
    Object localObject1 = null;
    for (;;)
    {
      int i = paramInt2 + -1;
      if (paramInt2 <= 0) {
        break;
      }
      paramInt2 = paramInt1 + 1;
      paramInt1 = ((CharSequence)paramCharSequence1).charAt(paramInt1);
      j = k + 1;
      k = paramCharSequence2.charAt(k);
      if (paramInt1 != k)
      {
        if (!paramBoolean) {
          return false;
        }
        int m = Character.toUpperCase(paramInt1);
        int n = Character.toUpperCase(k);
        if (m != n)
        {
          paramInt1 = Character.toLowerCase(paramInt1);
          k = Character.toLowerCase(k);
          if (paramInt1 != k) {
            return false;
          }
        }
      }
      paramInt1 = paramInt2;
      paramInt2 = i;
      k = j;
    }
    return true;
  }
}

/* Location:
 * Qualified Name:     org.c.a.a.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */