package org.c.a.a.a;

public final class m
{
  public static void a(boolean paramBoolean, String paramString, Object... paramVarArgs)
  {
    if (paramBoolean) {
      return;
    }
    IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
    paramString = String.format(paramString, paramVarArgs);
    localIllegalArgumentException.<init>(paramString);
    throw localIllegalArgumentException;
  }
}

/* Location:
 * Qualified Name:     org.c.a.a.a.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */