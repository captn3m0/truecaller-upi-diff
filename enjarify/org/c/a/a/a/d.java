package org.c.a.a.a;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class d
{
  public static final String a = ".";
  public static final String b = "$";
  private static final Map c;
  private static final Map d;
  private static final Map e;
  private static final Map f;
  
  static
  {
    Object localObject1 = new java/util/HashMap;
    ((HashMap)localObject1).<init>();
    c = (Map)localObject1;
    Object localObject2 = Boolean.TYPE;
    ((Map)localObject1).put(localObject2, Boolean.class);
    localObject1 = c;
    localObject2 = Byte.TYPE;
    ((Map)localObject1).put(localObject2, Byte.class);
    localObject1 = c;
    localObject2 = Character.TYPE;
    ((Map)localObject1).put(localObject2, Character.class);
    localObject1 = c;
    localObject2 = Short.TYPE;
    ((Map)localObject1).put(localObject2, Short.class);
    localObject1 = c;
    localObject2 = Integer.TYPE;
    ((Map)localObject1).put(localObject2, Integer.class);
    localObject1 = c;
    localObject2 = Long.TYPE;
    ((Map)localObject1).put(localObject2, Long.class);
    localObject1 = c;
    localObject2 = Double.TYPE;
    ((Map)localObject1).put(localObject2, Double.class);
    localObject1 = c;
    localObject2 = Float.TYPE;
    Object localObject3 = Float.class;
    ((Map)localObject1).put(localObject2, localObject3);
    localObject1 = c;
    localObject2 = Void.TYPE;
    ((Map)localObject1).put(localObject2, localObject2);
    localObject1 = new java/util/HashMap;
    ((HashMap)localObject1).<init>();
    d = (Map)localObject1;
    localObject1 = c.keySet().iterator();
    boolean bool2;
    Object localObject4;
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      localObject2 = (Class)((Iterator)localObject1).next();
      localObject3 = (Class)c.get(localObject2);
      bool2 = localObject2.equals(localObject3);
      if (!bool2)
      {
        localObject4 = d;
        ((Map)localObject4).put(localObject3, localObject2);
      }
    }
    localObject1 = new java/util/HashMap;
    ((HashMap)localObject1).<init>();
    ((Map)localObject1).put("int", "I");
    ((Map)localObject1).put("boolean", "Z");
    ((Map)localObject1).put("float", "F");
    ((Map)localObject1).put("long", "J");
    ((Map)localObject1).put("short", "S");
    ((Map)localObject1).put("byte", "B");
    ((Map)localObject1).put("double", "D");
    ((Map)localObject1).put("char", "C");
    ((Map)localObject1).put("void", "V");
    localObject2 = new java/util/HashMap;
    ((HashMap)localObject2).<init>();
    localObject3 = ((Map)localObject1).entrySet().iterator();
    for (;;)
    {
      bool2 = ((Iterator)localObject3).hasNext();
      if (!bool2) {
        break;
      }
      localObject4 = (Map.Entry)((Iterator)localObject3).next();
      Object localObject5 = ((Map.Entry)localObject4).getValue();
      localObject4 = ((Map.Entry)localObject4).getKey();
      ((Map)localObject2).put(localObject5, localObject4);
    }
    e = Collections.unmodifiableMap((Map)localObject1);
    f = Collections.unmodifiableMap((Map)localObject2);
  }
  
  public static String a(Class paramClass)
  {
    if (paramClass == null) {
      return "";
    }
    paramClass = paramClass.getName();
    boolean bool1 = k.b(paramClass);
    if (bool1) {
      return "";
    }
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    Object localObject = "[";
    boolean bool2 = paramClass.startsWith((String)localObject);
    int j = 0;
    int k = 1;
    if (bool2)
    {
      for (;;)
      {
        i = paramClass.charAt(0);
        m = 91;
        if (i != m) {
          break;
        }
        paramClass = paramClass.substring(k);
        localObject = "[]";
        localStringBuilder.append((String)localObject);
      }
      int i = paramClass.charAt(0);
      m = 76;
      if (i == m)
      {
        i = paramClass.length() - k;
        i = paramClass.charAt(i);
        m = 59;
        if (i == m)
        {
          i = paramClass.length() - k;
          paramClass = paramClass.substring(k, i);
        }
      }
      localObject = f;
      boolean bool3 = ((Map)localObject).containsKey(paramClass);
      if (bool3)
      {
        localObject = f;
        paramClass = (String)((Map)localObject).get(paramClass);
      }
    }
    char c1 = '.';
    int m = paramClass.lastIndexOf(c1);
    int n = -1;
    if (m != n) {
      j = m + 1;
    }
    char c2 = '$';
    j = paramClass.indexOf(c2, j);
    m += k;
    paramClass = paramClass.substring(m);
    if (j != n) {
      paramClass = paramClass.replace(c2, c1);
    }
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    ((StringBuilder)localObject).append(paramClass);
    ((StringBuilder)localObject).append(localStringBuilder);
    return ((StringBuilder)localObject).toString();
  }
}

/* Location:
 * Qualified Name:     org.c.a.a.a.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */