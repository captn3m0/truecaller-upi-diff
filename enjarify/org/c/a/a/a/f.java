package org.c.a.a.a;

public enum f
{
  private final float l;
  private final String m;
  
  static
  {
    Object localObject = new org/c/a/a/a/f;
    float f1 = 1.5F;
    ((f)localObject).<init>("JAVA_0_9", 0, f1, "0.9");
    a = (f)localObject;
    localObject = new org/c/a/a/a/f;
    int i1 = 1;
    ((f)localObject).<init>("JAVA_1_1", i1, 1.1F, "1.1");
    b = (f)localObject;
    localObject = new org/c/a/a/a/f;
    int i2 = 2;
    ((f)localObject).<init>("JAVA_1_2", i2, 1.2F, "1.2");
    c = (f)localObject;
    localObject = new org/c/a/a/a/f;
    int i3 = 3;
    ((f)localObject).<init>("JAVA_1_3", i3, 1.3F, "1.3");
    d = (f)localObject;
    localObject = new org/c/a/a/a/f;
    int i4 = 4;
    ((f)localObject).<init>("JAVA_1_4", i4, 1.4F, "1.4");
    e = (f)localObject;
    localObject = new org/c/a/a/a/f;
    int i5 = 5;
    ((f)localObject).<init>("JAVA_1_5", i5, f1, "1.5");
    f = (f)localObject;
    localObject = new org/c/a/a/a/f;
    int i6 = 6;
    ((f)localObject).<init>("JAVA_1_6", i6, 1.6F, "1.6");
    g = (f)localObject;
    localObject = new org/c/a/a/a/f;
    int i7 = 7;
    ((f)localObject).<init>("JAVA_1_7", i7, 1.7F, "1.7");
    h = (f)localObject;
    localObject = new org/c/a/a/a/f;
    int i8 = 8;
    ((f)localObject).<init>("JAVA_1_8", i8, 1.8F, "1.8");
    i = (f)localObject;
    localObject = new org/c/a/a/a/f;
    int i9 = 9;
    ((f)localObject).<init>("JAVA_1_9", i9, 1.9F, "1.9");
    j = (f)localObject;
    localObject = new org/c/a/a/a/f;
    float f2 = a();
    String str = Float.toString(a());
    int i10 = 10;
    ((f)localObject).<init>("JAVA_RECENT", i10, f2, str);
    k = (f)localObject;
    localObject = new f[11];
    f localf = a;
    localObject[0] = localf;
    localf = b;
    localObject[i1] = localf;
    localf = c;
    localObject[i2] = localf;
    localf = d;
    localObject[i3] = localf;
    localf = e;
    localObject[i4] = localf;
    localf = f;
    localObject[i5] = localf;
    localf = g;
    localObject[i6] = localf;
    localf = h;
    localObject[i7] = localf;
    localf = i;
    localObject[i8] = localf;
    localf = j;
    localObject[i9] = localf;
    localf = k;
    localObject[i10] = localf;
    n = (f[])localObject;
  }
  
  private f(float paramFloat, String paramString1)
  {
    l = paramFloat;
    m = paramString1;
  }
  
  private static float a()
  {
    String str = System.getProperty("java.version", "2.0");
    float f1 = b(str);
    boolean bool = f1 < 0.0F;
    if (bool) {
      return f1;
    }
    return 2.0F;
  }
  
  static f a(String paramString)
  {
    String str = "0.9";
    boolean bool1 = str.equals(paramString);
    if (bool1) {
      return a;
    }
    str = "1.1";
    bool1 = str.equals(paramString);
    if (bool1) {
      return b;
    }
    str = "1.2";
    bool1 = str.equals(paramString);
    if (bool1) {
      return c;
    }
    str = "1.3";
    bool1 = str.equals(paramString);
    if (bool1) {
      return d;
    }
    str = "1.4";
    bool1 = str.equals(paramString);
    if (bool1) {
      return e;
    }
    str = "1.5";
    bool1 = str.equals(paramString);
    if (bool1) {
      return f;
    }
    str = "1.6";
    bool1 = str.equals(paramString);
    if (bool1) {
      return g;
    }
    str = "1.7";
    bool1 = str.equals(paramString);
    if (bool1) {
      return h;
    }
    str = "1.8";
    bool1 = str.equals(paramString);
    if (bool1) {
      return i;
    }
    str = "1.9";
    bool1 = str.equals(paramString);
    if (bool1) {
      return j;
    }
    bool1 = false;
    str = null;
    if (paramString == null) {
      return null;
    }
    float f1 = b(paramString);
    double d1 = f1;
    double d2 = 1.0D;
    Double.isNaN(d1);
    d1 -= d2;
    boolean bool2 = d1 < d2;
    if (bool2)
    {
      int i1 = paramString.indexOf('.');
      int i2 = 44;
      int i3 = paramString.indexOf(i2);
      i1 = Math.max(i1, i3);
      i3 = paramString.length();
      i2 = paramString.indexOf(i2, i1);
      i2 = Math.max(i3, i2);
      i1 += 1;
      paramString = paramString.substring(i1, i2);
      float f2 = Float.parseFloat(paramString);
      i1 = 1063675494;
      f1 = 0.9F;
      boolean bool3 = f2 < f1;
      if (bool3) {
        return k;
      }
    }
    return null;
  }
  
  private static float b(String paramString)
  {
    Object localObject = "\\.";
    paramString = paramString.split((String)localObject);
    int i1 = paramString.length;
    int i2 = 2;
    if (i1 >= i2) {
      try
      {
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>();
        i2 = 0;
        String str = null;
        str = paramString[0];
        ((StringBuilder)localObject).append(str);
        i2 = 46;
        ((StringBuilder)localObject).append(i2);
        int i3 = 1;
        paramString = paramString[i3];
        ((StringBuilder)localObject).append(paramString);
        paramString = ((StringBuilder)localObject).toString();
        return Float.parseFloat(paramString);
      }
      catch (NumberFormatException localNumberFormatException) {}
    }
    return -1.0F;
  }
  
  public final String toString()
  {
    return m;
  }
}

/* Location:
 * Qualified Name:     org.c.a.a.a.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */