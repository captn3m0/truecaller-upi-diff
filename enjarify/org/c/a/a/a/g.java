package org.c.a.a.a;

import java.util.Locale;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public final class g
{
  private static final ConcurrentMap a;
  private static final ConcurrentMap b;
  
  static
  {
    ConcurrentHashMap localConcurrentHashMap = new java/util/concurrent/ConcurrentHashMap;
    localConcurrentHashMap.<init>();
    a = localConcurrentHashMap;
    localConcurrentHashMap = new java/util/concurrent/ConcurrentHashMap;
    localConcurrentHashMap.<init>();
    b = localConcurrentHashMap;
  }
  
  public static Locale a(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    boolean bool1 = paramString.isEmpty();
    if (bool1)
    {
      paramString = new java/util/Locale;
      paramString.<init>("", "");
      return paramString;
    }
    Object localObject = "#";
    bool1 = paramString.contains((CharSequence)localObject);
    if (!bool1)
    {
      int i = paramString.length();
      int i1 = 2;
      if (i >= i1)
      {
        char c = '\000';
        String str1 = null;
        int i3 = paramString.charAt(0);
        int i4 = 95;
        int i5 = 3;
        int i6 = 1;
        boolean bool6;
        int i2;
        if (i3 == i4)
        {
          if (i >= i5)
          {
            c = paramString.charAt(i6);
            i1 = paramString.charAt(i1);
            bool6 = Character.isUpperCase(c);
            if (bool6)
            {
              boolean bool5 = Character.isUpperCase(i1);
              if (bool5)
              {
                if (i == i5)
                {
                  localObject = new java/util/Locale;
                  paramString = paramString.substring(i6, i5);
                  ((Locale)localObject).<init>("", paramString);
                  return (Locale)localObject;
                }
                i2 = 5;
                if (i >= i2)
                {
                  i = paramString.charAt(i5);
                  if (i == i4)
                  {
                    localObject = new java/util/Locale;
                    str1 = paramString.substring(i6, i5);
                    paramString = paramString.substring(4);
                    ((Locale)localObject).<init>("", str1, paramString);
                    return (Locale)localObject;
                  }
                  localObject = new java/lang/IllegalArgumentException;
                  paramString = String.valueOf(paramString);
                  paramString = "Invalid locale format: ".concat(paramString);
                  ((IllegalArgumentException)localObject).<init>(paramString);
                  throw ((Throwable)localObject);
                }
                localObject = new java/lang/IllegalArgumentException;
                paramString = String.valueOf(paramString);
                paramString = "Invalid locale format: ".concat(paramString);
                ((IllegalArgumentException)localObject).<init>(paramString);
                throw ((Throwable)localObject);
              }
            }
            localObject = new java/lang/IllegalArgumentException;
            paramString = String.valueOf(paramString);
            paramString = "Invalid locale format: ".concat(paramString);
            ((IllegalArgumentException)localObject).<init>(paramString);
            throw ((Throwable)localObject);
          }
          localObject = new java/lang/IllegalArgumentException;
          paramString = String.valueOf(paramString);
          paramString = "Invalid locale format: ".concat(paramString);
          ((IllegalArgumentException)localObject).<init>(paramString);
          throw ((Throwable)localObject);
        }
        String[] arrayOfString = paramString.split("_", -1);
        i4 = arrayOfString.length - i6;
        String str2;
        int n;
        switch (i4)
        {
        default: 
          break;
        case 2: 
          localObject = arrayOfString[0];
          boolean bool2 = k.h((CharSequence)localObject);
          if (bool2)
          {
            localObject = arrayOfString[0];
            int j = ((String)localObject).length();
            if (j != i2)
            {
              localObject = arrayOfString[0];
              j = ((String)localObject).length();
              if (j != i5) {
                break;
              }
            }
            else
            {
              localObject = arrayOfString[i6];
              j = ((String)localObject).length();
              if (j != 0)
              {
                localObject = arrayOfString[i6];
                j = ((String)localObject).length();
                if (j == i2)
                {
                  localObject = arrayOfString[i6];
                  boolean bool3 = k.i((CharSequence)localObject);
                  if (!bool3) {
                    break;
                  }
                }
              }
              else
              {
                localObject = arrayOfString[i2];
                int k = ((String)localObject).length();
                if (k > 0)
                {
                  paramString = new java/util/Locale;
                  localObject = arrayOfString[0];
                  str1 = arrayOfString[i6];
                  str2 = arrayOfString[i2];
                  paramString.<init>((String)localObject, str1, str2);
                  return paramString;
                }
              }
            }
          }
          break;
        case 1: 
          localObject = arrayOfString[0];
          boolean bool4 = k.h((CharSequence)localObject);
          if (bool4)
          {
            localObject = arrayOfString[0];
            int m = ((String)localObject).length();
            if (m != i2)
            {
              localObject = arrayOfString[0];
              m = ((String)localObject).length();
              if (m != i5) {}
            }
            else
            {
              localObject = arrayOfString[i6];
              m = ((String)localObject).length();
              if (m == i2)
              {
                localObject = arrayOfString[i6];
                n = k.i((CharSequence)localObject);
                if (n != 0)
                {
                  paramString = new java/util/Locale;
                  localObject = arrayOfString[0];
                  str2 = arrayOfString[i6];
                  paramString.<init>((String)localObject, str2);
                  return paramString;
                }
              }
            }
          }
          localObject = new java/lang/IllegalArgumentException;
          paramString = String.valueOf(paramString);
          paramString = "Invalid locale format: ".concat(paramString);
          ((IllegalArgumentException)localObject).<init>(paramString);
          throw ((Throwable)localObject);
        case 0: 
          bool6 = k.h(paramString);
          if ((bool6) && ((n == i2) || (n == i5)))
          {
            localObject = new java/util/Locale;
            ((Locale)localObject).<init>(paramString);
            return (Locale)localObject;
          }
          localObject = new java/lang/IllegalArgumentException;
          paramString = String.valueOf(paramString);
          paramString = "Invalid locale format: ".concat(paramString);
          ((IllegalArgumentException)localObject).<init>(paramString);
          throw ((Throwable)localObject);
        }
        localObject = new java/lang/IllegalArgumentException;
        paramString = String.valueOf(paramString);
        paramString = "Invalid locale format: ".concat(paramString);
        ((IllegalArgumentException)localObject).<init>(paramString);
        throw ((Throwable)localObject);
      }
      localObject = new java/lang/IllegalArgumentException;
      paramString = String.valueOf(paramString);
      paramString = "Invalid locale format: ".concat(paramString);
      ((IllegalArgumentException)localObject).<init>(paramString);
      throw ((Throwable)localObject);
    }
    localObject = new java/lang/IllegalArgumentException;
    paramString = String.valueOf(paramString);
    paramString = "Invalid locale format: ".concat(paramString);
    ((IllegalArgumentException)localObject).<init>(paramString);
    throw ((Throwable)localObject);
  }
}

/* Location:
 * Qualified Name:     org.c.a.a.a.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */