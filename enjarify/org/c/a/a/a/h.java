package org.c.a.a.a;

public final class h
{
  public static final h.a a;
  
  static
  {
    h.a locala = new org/c/a/a/a/h$a;
    locala.<init>();
    a = locala;
  }
  
  public static int a(Comparable paramComparable1, Comparable paramComparable2)
  {
    if (paramComparable1 == paramComparable2) {
      return 0;
    }
    if (paramComparable1 == null) {
      return 1;
    }
    if (paramComparable2 == null) {
      return -1;
    }
    return paramComparable1.compareTo(paramComparable2);
  }
  
  public static Object a(Object paramObject1, Object paramObject2)
  {
    if (paramObject1 != null) {
      return paramObject1;
    }
    return paramObject2;
  }
  
  public static Object a(Object... paramVarArgs)
  {
    int i = paramVarArgs.length;
    int j = 0;
    while (j < i)
    {
      Object localObject = paramVarArgs[j];
      if (localObject != null) {
        return localObject;
      }
      j += 1;
    }
    return null;
  }
  
  public static String a(Object paramObject)
  {
    if (paramObject == null) {
      return "";
    }
    return paramObject.toString();
  }
  
  public static String a(Object paramObject, String paramString)
  {
    if (paramObject == null) {
      return paramString;
    }
    return paramObject.toString();
  }
  
  public static void a(StringBuffer paramStringBuffer, Object paramObject)
  {
    if (paramObject != null)
    {
      String str = paramObject.getClass().getName();
      paramStringBuffer.append(str);
      paramStringBuffer.append('@');
      paramObject = Integer.toHexString(System.identityHashCode(paramObject));
      paramStringBuffer.append((String)paramObject);
      return;
    }
    paramStringBuffer = new java/lang/NullPointerException;
    paramStringBuffer.<init>("Cannot get the toString of a null identity");
    throw paramStringBuffer;
  }
  
  public static int b(Object... paramVarArgs)
  {
    int i = 0;
    int j = 1;
    for (;;)
    {
      int k = 3;
      if (i >= k) {
        break;
      }
      Object localObject = paramVarArgs[i];
      if (localObject == null)
      {
        k = 0;
        localObject = null;
      }
      else
      {
        k = localObject.hashCode();
      }
      j = j * 31 + k;
      i += 1;
    }
    return j;
  }
  
  public static boolean b(Object paramObject1, Object paramObject2)
  {
    if (paramObject1 == paramObject2) {
      return true;
    }
    if ((paramObject1 != null) && (paramObject2 != null)) {
      return paramObject1.equals(paramObject2);
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     org.c.a.a.a.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */