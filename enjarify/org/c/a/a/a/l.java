package org.c.a.a.a;

import java.io.PrintStream;

public final class l
{
  public static final String A;
  public static final String B;
  public static final String C;
  public static final String D;
  public static final String E;
  public static final String F;
  public static final String G;
  public static final String H;
  public static final String I;
  public static final String J;
  public static final String K;
  public static final String L;
  public static final String M;
  public static final String N;
  public static final String O;
  public static final String P;
  public static final boolean Q;
  public static final boolean R;
  public static final boolean S;
  public static final boolean T;
  public static final boolean U;
  public static final boolean V;
  public static final boolean W;
  public static final boolean X;
  public static final boolean Y;
  public static final boolean Z;
  public static final String a = b("awt.toolkit");
  public static final boolean aA = b(H, "Windows 2003");
  public static final boolean aB = b(H, "Windows Server 2008");
  public static final boolean aC = b(H, "Windows Server 2012");
  public static final boolean aD = b(H, "Windows 95");
  public static final boolean aE = b(H, "Windows 98");
  public static final boolean aF = b(H, "Windows Me");
  public static final boolean aG = b(H, "Windows NT");
  public static final boolean aH = b(H, "Windows XP");
  public static final boolean aI = b(H, "Windows Vista");
  public static final boolean aJ = b(H, "Windows 7");
  public static final boolean aK = b(H, "Windows 8");
  private static final f aL;
  public static final boolean aa;
  public static final boolean ab;
  public static final boolean ac;
  public static final boolean ad;
  public static final boolean ae;
  public static final boolean af;
  public static final boolean ag;
  public static final boolean ah;
  public static final boolean ai;
  public static final boolean aj;
  public static final boolean ak;
  public static final boolean al;
  public static final boolean am;
  public static final boolean an;
  public static final boolean ao;
  public static final boolean ap;
  public static final boolean aq;
  public static final boolean ar;
  public static final boolean as;
  public static final boolean at;
  public static final boolean au;
  public static final boolean av;
  public static final boolean aw;
  public static final boolean ax;
  public static final boolean ay;
  public static final boolean az;
  public static final String b = b("file.encoding");
  public static final String c = b("file.separator");
  public static final String d = b("java.awt.fonts");
  public static final String e = b("java.awt.graphicsenv");
  public static final String f = b("java.awt.headless");
  public static final String g = b("java.awt.printerjob");
  public static final String h = b("java.class.path");
  public static final String i = b("java.class.version");
  public static final String j = b("java.compiler");
  public static final String k = b("java.endorsed.dirs");
  public static final String l = b("java.ext.dirs");
  public static final String m = b("java.home");
  public static final String n = b("java.io.tmpdir");
  public static final String o = b("java.library.path");
  public static final String p = b("java.runtime.name");
  public static final String q = b("java.runtime.version");
  public static final String r = b("java.specification.name");
  public static final String s = b("java.specification.vendor");
  public static final String t;
  public static final String u;
  public static final String v;
  public static final String w;
  public static final String x;
  public static final String y;
  public static final String z;
  
  static
  {
    String str1 = b("java.specification.version");
    t = str1;
    aL = f.a(str1);
    u = b("java.util.prefs.PreferencesFactory");
    v = b("java.vendor");
    w = b("java.vendor.url");
    x = b("java.version");
    y = b("java.vm.info");
    z = b("java.vm.name");
    A = b("java.vm.specification.name");
    B = b("java.vm.specification.vendor");
    C = b("java.vm.specification.version");
    D = b("java.vm.vendor");
    E = b("java.vm.version");
    F = b("line.separator");
    G = b("os.arch");
    H = b("os.name");
    I = b("os.version");
    J = b("path.separator");
    str1 = b("user.country");
    if (str1 == null) {
      str1 = "user.region";
    } else {
      str1 = "user.country";
    }
    K = b(str1);
    L = b("user.dir");
    M = b("user.home");
    N = b("user.language");
    O = b("user.name");
    P = b("user.timezone");
    Q = a("1.1");
    R = a("1.2");
    S = a("1.3");
    T = a("1.4");
    U = a("1.5");
    V = a("1.6");
    W = a("1.7");
    X = a("1.8");
    Y = a("1.9");
    Z = b(H, "AIX");
    aa = b(H, "HP-UX");
    ab = b(H, "OS/400");
    ac = b(H, "Irix");
    str1 = "Linux";
    boolean bool1 = b(H, str1);
    boolean bool2 = false;
    if (!bool1)
    {
      str1 = "LINUX";
      str2 = H;
      bool1 = b(str2, str1);
      if (!bool1)
      {
        bool1 = false;
        str1 = null;
        break label545;
      }
    }
    bool1 = true;
    label545:
    ad = bool1;
    ae = b(H, "Mac");
    af = b(H, "Mac OS X");
    ag = a("Mac OS X", "10.0");
    ah = a("Mac OS X", "10.1");
    ai = a("Mac OS X", "10.2");
    aj = a("Mac OS X", "10.3");
    ak = a("Mac OS X", "10.4");
    al = a("Mac OS X", "10.5");
    am = a("Mac OS X", "10.6");
    an = a("Mac OS X", "10.7");
    ao = a("Mac OS X", "10.8");
    ap = a("Mac OS X", "10.9");
    aq = a("Mac OS X", "10.10");
    ar = b(H, "FreeBSD");
    as = b(H, "OpenBSD");
    at = b(H, "NetBSD");
    au = b(H, "OS/2");
    av = b(H, "Solaris");
    str1 = "SunOS";
    String str2 = H;
    aw = b(str2, str1);
    bool1 = Z;
    if (!bool1)
    {
      bool1 = aa;
      if (!bool1)
      {
        bool1 = ac;
        if (!bool1)
        {
          bool1 = ad;
          if (!bool1)
          {
            bool1 = af;
            if (!bool1)
            {
              bool1 = av;
              if (!bool1)
              {
                bool1 = aw;
                if (!bool1)
                {
                  bool1 = ar;
                  if (!bool1)
                  {
                    bool1 = as;
                    if (!bool1)
                    {
                      bool1 = at;
                      if (!bool1) {
                        break label863;
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    bool2 = true;
    label863:
    ax = bool2;
    ay = b(H, "Windows");
    az = b(H, "Windows 2000");
  }
  
  private static boolean a(String paramString)
  {
    String str = t;
    if (str == null) {
      return false;
    }
    return str.startsWith(paramString);
  }
  
  private static boolean a(String paramString1, String paramString2)
  {
    String str1 = H;
    String str2 = I;
    if ((str1 != null) && (str2 != null))
    {
      boolean bool1 = b(str1, paramString1);
      if (bool1)
      {
        bool1 = k.b(str2);
        boolean bool2 = true;
        if (bool1)
        {
          bool1 = false;
          paramString1 = null;
        }
        else
        {
          paramString1 = paramString2.split("\\.");
          paramString2 = str2.split("\\.");
          int i1 = 0;
          str2 = null;
          for (;;)
          {
            int i2 = paramString1.length;
            int i3 = paramString2.length;
            i2 = Math.min(i2, i3);
            if (i1 >= i2) {
              break;
            }
            Object localObject1 = paramString1[i1];
            Object localObject2 = paramString2[i1];
            boolean bool3 = ((String)localObject1).equals(localObject2);
            if (!bool3)
            {
              bool1 = false;
              paramString1 = null;
              break label144;
            }
            i1 += 1;
          }
          bool1 = true;
        }
        label144:
        if (bool1) {
          return bool2;
        }
      }
      return false;
    }
    return false;
  }
  
  private static String b(String paramString)
  {
    try
    {
      return System.getProperty(paramString);
    }
    catch (SecurityException localSecurityException)
    {
      PrintStream localPrintStream = System.err;
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>("Caught a SecurityException reading the system property '");
      localStringBuilder.append(paramString);
      localStringBuilder.append("'; the SystemUtils property value will default to null.");
      paramString = localStringBuilder.toString();
      localPrintStream.println(paramString);
    }
    return null;
  }
  
  private static boolean b(String paramString1, String paramString2)
  {
    if (paramString1 == null) {
      return false;
    }
    return paramString1.startsWith(paramString2);
  }
}

/* Location:
 * Qualified Name:     org.c.a.a.a.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */