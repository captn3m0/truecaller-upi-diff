package org.c.a.a.a;

import java.util.Random;

public final class i
{
  private static final Random a;
  
  static
  {
    Random localRandom = new java/util/Random;
    localRandom.<init>();
    a = localRandom;
  }
  
  public static String a()
  {
    return a(32, false, true);
  }
  
  public static String a(int paramInt)
  {
    return a(paramInt, true, false);
  }
  
  private static String a(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2, Random paramRandom)
  {
    if (paramInt1 == 0) {
      return "";
    }
    if (paramInt1 >= 0)
    {
      int i;
      if ((!paramBoolean1) && (!paramBoolean2))
      {
        i = -1 >>> 1;
      }
      else
      {
        i = 123;
        paramInt2 = 32;
      }
      char[] arrayOfChar = new char[paramInt1];
      i -= paramInt2;
      for (;;)
      {
        int j = paramInt1 + -1;
        if (paramInt1 == 0) {
          break;
        }
        paramInt1 = (char)(paramRandom.nextInt(i) + paramInt2);
        boolean bool;
        if (paramBoolean1)
        {
          bool = Character.isLetter(paramInt1);
          if (bool) {}
        }
        else if (paramBoolean2)
        {
          bool = Character.isDigit(paramInt1);
          if (bool) {}
        }
        else
        {
          if ((paramBoolean1) || (paramBoolean2)) {
            break label261;
          }
        }
        int k = 128;
        int m = 55296;
        int n = 56320;
        if (paramInt1 >= n)
        {
          int i1 = 57343;
          if (paramInt1 <= i1)
          {
            if (j == 0) {
              break label261;
            }
            arrayOfChar[j] = paramInt1;
            paramInt1 = j + -1;
            j = (char)(paramRandom.nextInt(k) + m);
            arrayOfChar[paramInt1] = j;
            continue;
          }
        }
        if (paramInt1 >= m)
        {
          m = 56191;
          if (paramInt1 <= m)
          {
            if (j == 0) {
              break label261;
            }
            k = (char)(paramRandom.nextInt(k) + n);
            arrayOfChar[j] = k;
            j += -1;
            arrayOfChar[j] = paramInt1;
            paramInt1 = j;
            continue;
          }
        }
        k = 56192;
        if (paramInt1 >= k)
        {
          k = 56319;
          if (paramInt1 <= k) {}
        }
        else
        {
          arrayOfChar[j] = paramInt1;
          paramInt1 = j;
          continue;
        }
        label261:
        paramInt1 = j + 1;
      }
      str = new java/lang/String;
      str.<init>(arrayOfChar);
      return str;
    }
    IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Requested random string length ");
    localStringBuilder.append(paramInt1);
    localStringBuilder.append(" is less than 0.");
    String str = localStringBuilder.toString();
    localIllegalArgumentException.<init>(str);
    throw localIllegalArgumentException;
  }
  
  private static String a(int paramInt, boolean paramBoolean1, boolean paramBoolean2)
  {
    Random localRandom = a;
    return a(paramInt, 0, paramBoolean1, paramBoolean2, localRandom);
  }
  
  public static String b(int paramInt)
  {
    boolean bool = true;
    return a(paramInt, bool, bool);
  }
}

/* Location:
 * Qualified Name:     org.c.a.a.a.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */