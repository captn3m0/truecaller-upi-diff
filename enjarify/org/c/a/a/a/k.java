package org.c.a.a.a;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class k
{
  private static String a(char paramChar, int paramInt)
  {
    char[] arrayOfChar = new char[paramInt];
    paramInt += -1;
    while (paramInt >= 0)
    {
      arrayOfChar[paramInt] = paramChar;
      paramInt += -1;
    }
    String str = new java/lang/String;
    str.<init>(arrayOfChar);
    return str;
  }
  
  public static String a(Iterable paramIterable, char paramChar)
  {
    if (paramIterable == null) {
      return null;
    }
    return a(paramIterable.iterator(), paramChar);
  }
  
  public static String a(Iterable paramIterable, String paramString)
  {
    if (paramIterable == null) {
      return null;
    }
    return a(paramIterable.iterator(), paramString);
  }
  
  private static String a(String paramString, int paramInt)
  {
    if (paramString == null) {
      return null;
    }
    if (paramInt <= 0) {
      return "";
    }
    int i = paramString.length();
    int j = 1;
    if ((paramInt != j) && (i != 0))
    {
      int k = 0;
      if (i == j)
      {
        m = 8192;
        if (paramInt <= m) {
          return a(paramString.charAt(0), paramInt);
        }
      }
      int m = i * paramInt;
      StringBuilder localStringBuilder;
      switch (i)
      {
      default: 
        localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>(m);
        break;
      case 2: 
        i = paramString.charAt(0);
        int n = paramString.charAt(j);
        char[] arrayOfChar = new char[m];
        for (paramInt = paramInt * 2 + -2; paramInt >= 0; paramInt = paramInt + -1 + -1)
        {
          arrayOfChar[paramInt] = i;
          k = paramInt + 1;
          arrayOfChar[k] = n;
        }
        paramString = new java/lang/String;
        paramString.<init>(arrayOfChar);
        return paramString;
      case 1: 
        return a(paramString.charAt(0), paramInt);
      }
      while (k < paramInt)
      {
        localStringBuilder.append(paramString);
        k += 1;
      }
      return localStringBuilder.toString();
    }
    return paramString;
  }
  
  public static String a(String paramString, int paramInt, char paramChar)
  {
    if (paramString == null) {
      return null;
    }
    int i = paramString.length();
    i = paramInt - i;
    if (i <= 0) {
      return paramString;
    }
    int j = 8192;
    if (i > j)
    {
      String str = String.valueOf(paramChar);
      return a(paramString, paramInt, str);
    }
    return a(paramChar, i).concat(paramString);
  }
  
  private static String a(String paramString1, int paramInt, String paramString2)
  {
    if (paramString1 == null) {
      return null;
    }
    boolean bool = b(paramString2);
    if (bool) {
      paramString2 = " ";
    }
    int i = paramString2.length();
    int j = paramString1.length();
    j = paramInt - j;
    if (j <= 0) {
      return paramString1;
    }
    int k = 1;
    int m = 0;
    if (i == k)
    {
      k = 8192;
      if (j <= k)
      {
        char c = paramString2.charAt(0);
        return a(paramString1, paramInt, c);
      }
    }
    if (j == i) {
      return paramString2.concat(paramString1);
    }
    if (j < i) {
      return paramString2.substring(0, j).concat(paramString1);
    }
    char[] arrayOfChar = new char[j];
    paramString2 = paramString2.toCharArray();
    while (m < j)
    {
      k = m % i;
      k = paramString2[k];
      arrayOfChar[m] = k;
      m += 1;
    }
    paramString2 = new java/lang/String;
    paramString2.<init>(arrayOfChar);
    return paramString2.concat(paramString1);
  }
  
  public static String a(String paramString1, String paramString2, int paramInt)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(paramString1);
    localStringBuilder.append(paramString2);
    return b(a(localStringBuilder.toString(), paramInt), paramString2);
  }
  
  public static String a(String paramString1, String paramString2, String paramString3)
  {
    boolean bool = b(paramString1);
    if (!bool)
    {
      bool = b(paramString2);
      if (!bool)
      {
        int i = paramString3.length();
        int j = paramString1.length();
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>(j);
        int k = 0;
        char c1 = '\000';
        while (k < j)
        {
          char c2 = paramString1.charAt(k);
          int m = paramString2.indexOf(c2);
          if (m >= 0)
          {
            if (m < i)
            {
              c1 = paramString3.charAt(m);
              localStringBuilder.append(c1);
            }
            c1 = '\001';
          }
          else
          {
            localStringBuilder.append(c2);
          }
          k += 1;
        }
        if (c1 != 0) {
          return localStringBuilder.toString();
        }
        return paramString1;
      }
    }
    return paramString1;
  }
  
  private static String a(Iterator paramIterator, char paramChar)
  {
    if (paramIterator == null) {
      return null;
    }
    boolean bool1 = paramIterator.hasNext();
    if (!bool1) {
      return "";
    }
    Object localObject = paramIterator.next();
    boolean bool2 = paramIterator.hasNext();
    if (!bool2) {
      return h.a(localObject);
    }
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    int i = 256;
    localStringBuilder.<init>(i);
    if (localObject != null) {
      localStringBuilder.append(localObject);
    }
    for (;;)
    {
      bool1 = paramIterator.hasNext();
      if (!bool1) {
        break;
      }
      localStringBuilder.append(paramChar);
      localObject = paramIterator.next();
      if (localObject != null) {
        localStringBuilder.append(localObject);
      }
    }
    return localStringBuilder.toString();
  }
  
  private static String a(Iterator paramIterator, String paramString)
  {
    if (paramIterator == null) {
      return null;
    }
    boolean bool1 = paramIterator.hasNext();
    if (!bool1) {
      return "";
    }
    Object localObject = paramIterator.next();
    boolean bool2 = paramIterator.hasNext();
    if (!bool2) {
      return h.a(localObject);
    }
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    int i = 256;
    localStringBuilder.<init>(i);
    if (localObject != null) {
      localStringBuilder.append(localObject);
    }
    for (;;)
    {
      bool1 = paramIterator.hasNext();
      if (!bool1) {
        break;
      }
      if (paramString != null) {
        localStringBuilder.append(paramString);
      }
      localObject = paramIterator.next();
      if (localObject != null) {
        localStringBuilder.append(localObject);
      }
    }
    return localStringBuilder.toString();
  }
  
  public static String a(int[] paramArrayOfInt, int paramInt)
  {
    if (paramArrayOfInt == null) {
      return null;
    }
    int i = paramInt + 0;
    if (i <= 0) {
      return "";
    }
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    i *= 16;
    localStringBuilder.<init>(i);
    i = 0;
    while (i < paramInt)
    {
      if (i > 0)
      {
        char c = ',';
        localStringBuilder.append(c);
      }
      int j = paramArrayOfInt[i];
      localStringBuilder.append(j);
      i += 1;
    }
    return localStringBuilder.toString();
  }
  
  public static String a(Object[] paramArrayOfObject, String paramString)
  {
    if (paramArrayOfObject == null) {
      return null;
    }
    int i = paramArrayOfObject.length;
    return a(paramArrayOfObject, paramString, i);
  }
  
  private static String a(Object[] paramArrayOfObject, String paramString, int paramInt)
  {
    if (paramArrayOfObject == null) {
      return null;
    }
    if (paramString == null) {
      paramString = "";
    }
    int i = paramInt + 0;
    if (i <= 0) {
      return "";
    }
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    i *= 16;
    localStringBuilder.<init>(i);
    i = 0;
    while (i < paramInt)
    {
      if (i > 0) {
        localStringBuilder.append(paramString);
      }
      Object localObject = paramArrayOfObject[i];
      if (localObject != null)
      {
        localObject = paramArrayOfObject[i];
        localStringBuilder.append(localObject);
      }
      i += 1;
    }
    return localStringBuilder.toString();
  }
  
  public static boolean a(CharSequence paramCharSequence1, CharSequence paramCharSequence2)
  {
    if (paramCharSequence1 == paramCharSequence2) {
      return true;
    }
    if ((paramCharSequence1 != null) && (paramCharSequence2 != null))
    {
      boolean bool = paramCharSequence1 instanceof String;
      if (bool)
      {
        bool = paramCharSequence2 instanceof String;
        if (bool) {
          return paramCharSequence1.equals(paramCharSequence2);
        }
      }
      int i = ((CharSequence)paramCharSequence1).length();
      int j = paramCharSequence2.length();
      i = Math.max(i, j);
      return c.a((CharSequence)paramCharSequence1, false, 0, paramCharSequence2, i);
    }
    return false;
  }
  
  public static boolean a(CharSequence paramCharSequence1, CharSequence paramCharSequence2, boolean paramBoolean)
  {
    if ((paramCharSequence1 != null) && (paramCharSequence2 != null))
    {
      int i = paramCharSequence2.length();
      int j = paramCharSequence1.length();
      if (i > j) {
        return false;
      }
      i = paramCharSequence2.length();
      return c.a(paramCharSequence1, paramBoolean, 0, paramCharSequence2, i);
    }
    return (paramCharSequence1 == null) && (paramCharSequence2 == null);
  }
  
  public static boolean a(CharSequence... paramVarArgs)
  {
    boolean bool = c(paramVarArgs);
    return !bool;
  }
  
  public static String[] a(String paramString, char paramChar)
  {
    if (paramString == null) {
      return null;
    }
    int i = paramString.length();
    if (i == 0) {
      return a.c;
    }
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    int j = 0;
    int k = 0;
    String str = null;
    int m = 0;
    while (j < i)
    {
      char c = paramString.charAt(j);
      if (c == paramChar)
      {
        if (k != 0)
        {
          str = paramString.substring(m, j);
          localArrayList.add(str);
          k = 0;
          str = null;
        }
        m = j + 1;
        j = m;
      }
      else
      {
        j += 1;
        k = 1;
      }
    }
    if (k != 0)
    {
      paramString = paramString.substring(m, j);
      localArrayList.add(paramString);
    }
    paramString = new String[localArrayList.size()];
    return (String[])localArrayList.toArray(paramString);
  }
  
  public static String[] a(String paramString1, String paramString2)
  {
    if (paramString1 == null) {
      return null;
    }
    int i = paramString1.length();
    if (i == 0) {
      return a.c;
    }
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    int j = paramString2.length();
    int k = -1;
    int m = 1;
    int i4;
    if (j == m)
    {
      int n = paramString2.charAt(0);
      j = 0;
      i1 = 0;
      str = null;
      i2 = 0;
      i3 = 1;
      while (j < i)
      {
        i4 = paramString1.charAt(j);
        if (i4 == n)
        {
          if (i2 != 0)
          {
            i2 = i3 + 1;
            if (i3 == k) {
              j = i;
            }
            str = paramString1.substring(i1, j);
            localArrayList.add(str);
            i3 = i2;
            i2 = 0;
          }
          i1 = j + 1;
          j = i1;
        }
        else
        {
          j += 1;
          i2 = 1;
        }
      }
    }
    j = 0;
    int i1 = 0;
    String str = null;
    int i2 = 0;
    int i3 = 1;
    while (j < i)
    {
      i4 = paramString1.charAt(j);
      i4 = paramString2.indexOf(i4);
      if (i4 >= 0)
      {
        if (i2 != 0)
        {
          i2 = i3 + 1;
          if (i3 == k) {
            j = i;
          }
          str = paramString1.substring(i1, j);
          localArrayList.add(str);
          i3 = i2;
          i2 = 0;
        }
        i1 = j + 1;
        j = i1;
      }
      else
      {
        j += 1;
        i2 = 1;
      }
    }
    if (i2 != 0)
    {
      paramString1 = paramString1.substring(i1, j);
      localArrayList.add(paramString1);
    }
    paramString1 = new String[localArrayList.size()];
    return (String[])localArrayList.toArray(paramString1);
  }
  
  private static String b(String paramString1, String paramString2)
  {
    boolean bool = b(paramString1);
    if (!bool)
    {
      bool = b(paramString2);
      if (!bool)
      {
        bool = paramString1.endsWith(paramString2);
        if (bool)
        {
          int i = paramString1.length();
          int j = paramString2.length();
          i -= j;
          return paramString1.substring(0, i);
        }
        return paramString1;
      }
    }
    return paramString1;
  }
  
  public static boolean b(CharSequence paramCharSequence)
  {
    if (paramCharSequence != null)
    {
      int i = paramCharSequence.length();
      if (i != 0) {
        return false;
      }
    }
    return true;
  }
  
  public static boolean b(CharSequence paramCharSequence1, CharSequence paramCharSequence2)
  {
    boolean bool = true;
    if ((paramCharSequence1 != null) && (paramCharSequence2 != null))
    {
      if (paramCharSequence1 == paramCharSequence2) {
        return bool;
      }
      int i = paramCharSequence1.length();
      int j = paramCharSequence2.length();
      if (i != j) {
        return false;
      }
      i = paramCharSequence1.length();
      return c.a(paramCharSequence1, bool, 0, paramCharSequence2, i);
    }
    if (paramCharSequence1 == paramCharSequence2) {
      return bool;
    }
    return false;
  }
  
  public static boolean b(CharSequence... paramVarArgs)
  {
    boolean bool1 = a.a(paramVarArgs);
    boolean bool2 = true;
    if (bool1) {
      return bool2;
    }
    bool1 = false;
    int i = 0;
    while (i <= 0)
    {
      CharSequence localCharSequence = paramVarArgs[0];
      boolean bool3 = d(localCharSequence);
      if (bool3) {
        return bool2;
      }
      i += 1;
    }
    return false;
  }
  
  public static String c(String paramString, Locale paramLocale)
  {
    if (paramString == null) {
      return null;
    }
    return paramString.toUpperCase(paramLocale);
  }
  
  public static boolean c(CharSequence paramCharSequence)
  {
    boolean bool = b(paramCharSequence);
    return !bool;
  }
  
  public static boolean c(CharSequence paramCharSequence1, CharSequence paramCharSequence2)
  {
    if ((paramCharSequence1 != null) && (paramCharSequence2 != null))
    {
      int i = paramCharSequence2.length();
      int j = paramCharSequence1.length() - i;
      int k = 0;
      while (k <= j)
      {
        boolean bool1 = true;
        boolean bool2 = c.a(paramCharSequence1, bool1, k, paramCharSequence2, i);
        if (bool2) {
          return bool1;
        }
        k += 1;
      }
      return false;
    }
    return false;
  }
  
  private static boolean c(CharSequence... paramVarArgs)
  {
    boolean bool1 = a.a(paramVarArgs);
    boolean bool2 = true;
    if (bool1) {
      return bool2;
    }
    int i = paramVarArgs.length;
    int j = 0;
    while (j < i)
    {
      CharSequence localCharSequence = paramVarArgs[j];
      boolean bool3 = b(localCharSequence);
      if (bool3) {
        return bool2;
      }
      j += 1;
    }
    return false;
  }
  
  public static CharSequence d(CharSequence paramCharSequence1, CharSequence paramCharSequence2)
  {
    boolean bool = d(paramCharSequence1);
    if (bool) {
      return paramCharSequence2;
    }
    return paramCharSequence1;
  }
  
  public static String d(String paramString, Locale paramLocale)
  {
    if (paramString == null) {
      return null;
    }
    return paramString.toLowerCase(paramLocale);
  }
  
  public static boolean d(CharSequence paramCharSequence)
  {
    boolean bool1 = true;
    if (paramCharSequence != null)
    {
      int i = paramCharSequence.length();
      if (i != 0)
      {
        int j = 0;
        while (j < i)
        {
          boolean bool2 = Character.isWhitespace(paramCharSequence.charAt(j));
          if (!bool2) {
            return false;
          }
          j += 1;
        }
        return bool1;
      }
    }
    return bool1;
  }
  
  public static CharSequence e(CharSequence paramCharSequence1, CharSequence paramCharSequence2)
  {
    boolean bool = b(paramCharSequence1);
    if (bool) {
      return paramCharSequence2;
    }
    return paramCharSequence1;
  }
  
  public static boolean e(CharSequence paramCharSequence)
  {
    boolean bool = d(paramCharSequence);
    return !bool;
  }
  
  public static int f(CharSequence paramCharSequence)
  {
    if (paramCharSequence == null) {
      return 0;
    }
    return paramCharSequence.length();
  }
  
  public static boolean f(CharSequence paramCharSequence1, CharSequence paramCharSequence2)
  {
    if (paramCharSequence1 == null) {
      return false;
    }
    int i = paramCharSequence2.length();
    int j = paramCharSequence1.length();
    if (i > j) {
      return false;
    }
    i = paramCharSequence1.length();
    j = paramCharSequence2.length();
    i -= j;
    j = paramCharSequence2.length();
    return c.a(paramCharSequence1, false, i, paramCharSequence2, j);
  }
  
  public static boolean g(CharSequence paramCharSequence)
  {
    boolean bool1 = b(paramCharSequence);
    if (bool1) {
      return false;
    }
    int i = paramCharSequence.length();
    int j = 0;
    while (j < i)
    {
      boolean bool2 = Character.isDigit(paramCharSequence.charAt(j));
      if (!bool2) {
        return false;
      }
      j += 1;
    }
    return true;
  }
  
  public static boolean h(CharSequence paramCharSequence)
  {
    if (paramCharSequence != null)
    {
      boolean bool1 = b(paramCharSequence);
      if (!bool1)
      {
        int i = paramCharSequence.length();
        int j = 0;
        while (j < i)
        {
          boolean bool2 = Character.isLowerCase(paramCharSequence.charAt(j));
          if (!bool2) {
            return false;
          }
          j += 1;
        }
        return true;
      }
    }
    return false;
  }
  
  public static String i(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    return paramString.trim();
  }
  
  public static boolean i(CharSequence paramCharSequence)
  {
    if (paramCharSequence != null)
    {
      boolean bool1 = b(paramCharSequence);
      if (!bool1)
      {
        int i = paramCharSequence.length();
        int j = 0;
        while (j < i)
        {
          boolean bool2 = Character.isUpperCase(paramCharSequence.charAt(j));
          if (!bool2) {
            return false;
          }
          j += 1;
        }
        return true;
      }
    }
    return false;
  }
  
  public static String j(String paramString)
  {
    if (paramString == null) {
      return "";
    }
    return paramString.trim();
  }
  
  public static String k(String paramString)
  {
    boolean bool1 = b(paramString);
    if (bool1) {
      return paramString;
    }
    int i = paramString.length();
    char[] arrayOfChar = new char[i];
    int j = 0;
    int k = 0;
    while (j < i)
    {
      boolean bool2 = Character.isWhitespace(paramString.charAt(j));
      if (!bool2)
      {
        int m = k + 1;
        int n = paramString.charAt(j);
        arrayOfChar[k] = n;
        k = m;
      }
      j += 1;
    }
    if (k == i) {
      return paramString;
    }
    paramString = new java/lang/String;
    paramString.<init>(arrayOfChar, 0, k);
    return paramString;
  }
  
  public static String l(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    return paramString.toUpperCase();
  }
  
  public static String m(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    return paramString.toLowerCase();
  }
  
  public static String n(String paramString)
  {
    if (paramString == null) {
      paramString = "";
    }
    return paramString;
  }
}

/* Location:
 * Qualified Name:     org.c.a.a.a.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */