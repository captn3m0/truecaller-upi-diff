package org.c.a.a.a.a;

public final class a
{
  private static final ThreadLocal b;
  public int a = 0;
  private final int c = 37;
  
  static
  {
    ThreadLocal localThreadLocal = new java/lang/ThreadLocal;
    localThreadLocal.<init>();
    b = localThreadLocal;
  }
  
  private a a(short paramShort)
  {
    int i = a;
    int j = c;
    i = i * j + paramShort;
    a = i;
    return this;
  }
  
  private a a(byte[] paramArrayOfByte)
  {
    int j;
    if (paramArrayOfByte == null)
    {
      int i = a;
      j = c;
      i *= j;
      a = i;
    }
    else
    {
      j = paramArrayOfByte.length;
      int k = 0;
      while (k < j)
      {
        int m = paramArrayOfByte[k];
        int n = a;
        int i1 = c;
        n = n * i1 + m;
        a = n;
        k += 1;
      }
    }
    return this;
  }
  
  private a a(char[] paramArrayOfChar)
  {
    int j;
    if (paramArrayOfChar == null)
    {
      int i = a;
      j = c;
      i *= j;
      a = i;
    }
    else
    {
      j = paramArrayOfChar.length;
      int k = 0;
      while (k < j)
      {
        int m = paramArrayOfChar[k];
        int n = a;
        int i1 = c;
        n = n * i1 + m;
        a = n;
        k += 1;
      }
    }
    return this;
  }
  
  private a a(double[] paramArrayOfDouble)
  {
    int j;
    if (paramArrayOfDouble == null)
    {
      int i = a;
      j = c;
      i *= j;
      a = i;
    }
    else
    {
      j = paramArrayOfDouble.length;
      int k = 0;
      while (k < j)
      {
        double d = paramArrayOfDouble[k];
        long l = Double.doubleToLongBits(d);
        a(l);
        k += 1;
      }
    }
    return this;
  }
  
  private a a(float[] paramArrayOfFloat)
  {
    int j;
    if (paramArrayOfFloat == null)
    {
      int i = a;
      j = c;
      i *= j;
      a = i;
    }
    else
    {
      j = paramArrayOfFloat.length;
      int k = 0;
      while (k < j)
      {
        float f = paramArrayOfFloat[k];
        int m = a;
        int n = c;
        m *= n;
        int i1 = Float.floatToIntBits(f);
        m += i1;
        a = m;
        k += 1;
      }
    }
    return this;
  }
  
  private a a(int[] paramArrayOfInt)
  {
    int j;
    if (paramArrayOfInt == null)
    {
      int i = a;
      j = c;
      i *= j;
      a = i;
    }
    else
    {
      j = paramArrayOfInt.length;
      int k = 0;
      while (k < j)
      {
        int m = paramArrayOfInt[k];
        a(m);
        k += 1;
      }
    }
    return this;
  }
  
  private a a(long[] paramArrayOfLong)
  {
    int j;
    if (paramArrayOfLong == null)
    {
      int i = a;
      j = c;
      i *= j;
      a = i;
    }
    else
    {
      j = paramArrayOfLong.length;
      int k = 0;
      while (k < j)
      {
        long l = paramArrayOfLong[k];
        a(l);
        k += 1;
      }
    }
    return this;
  }
  
  private a a(Object[] paramArrayOfObject)
  {
    int j;
    if (paramArrayOfObject == null)
    {
      int i = a;
      j = c;
      i *= j;
      a = i;
    }
    else
    {
      j = paramArrayOfObject.length;
      int k = 0;
      while (k < j)
      {
        Object localObject = paramArrayOfObject[k];
        a(localObject);
        k += 1;
      }
    }
    return this;
  }
  
  private a a(short[] paramArrayOfShort)
  {
    int j;
    if (paramArrayOfShort == null)
    {
      int i = a;
      j = c;
      i *= j;
      a = i;
    }
    else
    {
      j = paramArrayOfShort.length;
      int k = 0;
      while (k < j)
      {
        short s = paramArrayOfShort[k];
        a(s);
        k += 1;
      }
    }
    return this;
  }
  
  private a a(boolean[] paramArrayOfBoolean)
  {
    int j;
    if (paramArrayOfBoolean == null)
    {
      int i = a;
      j = c;
      i *= j;
      a = i;
    }
    else
    {
      j = paramArrayOfBoolean.length;
      int k = 0;
      while (k < j)
      {
        int m = paramArrayOfBoolean[k];
        int n = a;
        int i1 = c;
        n *= i1;
        m ^= 0x1;
        n += m;
        a = n;
        k += 1;
      }
    }
    return this;
  }
  
  public final a a(int paramInt)
  {
    int i = a;
    int j = c;
    i = i * j + paramInt;
    a = i;
    return this;
  }
  
  public final a a(long paramLong)
  {
    int i = a;
    int j = c;
    i *= j;
    long l = paramLong >> 32;
    int k = (int)(paramLong ^ l);
    i += k;
    a = i;
    return this;
  }
  
  public final a a(Object paramObject)
  {
    int i;
    if (paramObject == null)
    {
      i = a;
      int j = c;
      i *= j;
      a = i;
    }
    else
    {
      Class localClass = paramObject.getClass();
      boolean bool = localClass.isArray();
      if (bool)
      {
        bool = paramObject instanceof long[];
        if (bool)
        {
          paramObject = (long[])paramObject;
          a((long[])paramObject);
        }
        else
        {
          bool = paramObject instanceof int[];
          if (bool)
          {
            paramObject = (int[])paramObject;
            a((int[])paramObject);
          }
          else
          {
            bool = paramObject instanceof short[];
            if (bool)
            {
              paramObject = (short[])paramObject;
              a((short[])paramObject);
            }
            else
            {
              bool = paramObject instanceof char[];
              if (bool)
              {
                paramObject = (char[])paramObject;
                a((char[])paramObject);
              }
              else
              {
                bool = paramObject instanceof byte[];
                if (bool)
                {
                  paramObject = (byte[])paramObject;
                  a((byte[])paramObject);
                }
                else
                {
                  bool = paramObject instanceof double[];
                  if (bool)
                  {
                    paramObject = (double[])paramObject;
                    a((double[])paramObject);
                  }
                  else
                  {
                    bool = paramObject instanceof float[];
                    if (bool)
                    {
                      paramObject = (float[])paramObject;
                      a((float[])paramObject);
                    }
                    else
                    {
                      bool = paramObject instanceof boolean[];
                      if (bool)
                      {
                        paramObject = (boolean[])paramObject;
                        a((boolean[])paramObject);
                      }
                      else
                      {
                        paramObject = (Object[])paramObject;
                        a((Object[])paramObject);
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      else
      {
        int k = a;
        int m = c;
        k *= m;
        i = paramObject.hashCode();
        k += i;
        a = k;
      }
    }
    return this;
  }
  
  public final int hashCode()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     org.c.a.a.a.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */