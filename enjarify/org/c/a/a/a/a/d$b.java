package org.c.a.a.a.a;

final class d$b
  extends d
{
  private static final long serialVersionUID = 1L;
  private String y = "\"";
  
  d$b()
  {
    i = false;
    k = false;
    l = "{";
    a("}");
    r = "[";
    s = "]";
    b(",");
    n = ":";
    t = "null";
    w = "\"<";
    x = ">\"";
    u = "\"<size=";
    v = ">\"";
  }
  
  private Object readResolve()
  {
    return d.g;
  }
  
  protected final void a(StringBuffer paramStringBuffer, Object paramObject)
  {
    if (paramObject == null)
    {
      a(paramStringBuffer);
      return;
    }
    Object localObject = paramObject.getClass();
    Class localClass = String.class;
    if (localObject == localClass)
    {
      paramObject = (String)paramObject;
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>("\"");
      ((StringBuilder)localObject).append((String)paramObject);
      ((StringBuilder)localObject).append("\"");
      paramObject = ((StringBuilder)localObject).toString();
      paramStringBuffer.append((String)paramObject);
      return;
    }
    paramStringBuffer.append(paramObject);
  }
  
  protected final void a(StringBuffer paramStringBuffer, String paramString)
  {
    if (paramString != null)
    {
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      String str = y;
      localStringBuilder.append(str);
      localStringBuilder.append(paramString);
      paramString = y;
      localStringBuilder.append(paramString);
      paramString = localStringBuilder.toString();
      super.a(paramStringBuffer, paramString);
      return;
    }
    paramStringBuffer = new java/lang/UnsupportedOperationException;
    paramStringBuffer.<init>("Field names are mandatory when using JsonToStringStyle");
    throw paramStringBuffer;
  }
  
  public final void a(StringBuffer paramStringBuffer, String paramString, Object paramObject, Boolean paramBoolean)
  {
    if (paramString != null)
    {
      boolean bool = a(paramBoolean);
      if (bool)
      {
        super.a(paramStringBuffer, paramString, paramObject, paramBoolean);
        return;
      }
      paramStringBuffer = new java/lang/UnsupportedOperationException;
      paramStringBuffer.<init>("FullDetail must be true when using JsonToStringStyle");
      throw paramStringBuffer;
    }
    paramStringBuffer = new java/lang/UnsupportedOperationException;
    paramStringBuffer.<init>("Field names are mandatory when using JsonToStringStyle");
    throw paramStringBuffer;
  }
}

/* Location:
 * Qualified Name:     org.c.a.a.a.a.d.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */