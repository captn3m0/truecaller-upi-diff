package org.c.a.a.a.a;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Arrays;

public final class b
  extends c
{
  protected String[] a;
  private boolean e = false;
  private boolean f = false;
  private Class g = null;
  
  private b(Object paramObject, d paramd)
  {
    super(paramObject, paramd);
  }
  
  public static String a(Object paramObject)
  {
    return a(paramObject, null);
  }
  
  public static String a(Object paramObject, d paramd)
  {
    b localb = new org/c/a/a/a/a/b;
    localb.<init>(paramObject, paramd);
    return localb.toString();
  }
  
  private void a(Class paramClass)
  {
    boolean bool1 = paramClass.isArray();
    Object localObject1;
    Object localObject2;
    if (bool1)
    {
      paramClass = c;
      localObject1 = d;
      localObject2 = b;
      ((d)localObject1).b((StringBuffer)localObject2, paramClass);
      return;
    }
    paramClass = paramClass.getDeclaredFields();
    bool1 = true;
    AccessibleObject.setAccessible(paramClass, bool1);
    int i = paramClass.length;
    int j = 0;
    while (j < i)
    {
      Object localObject3 = paramClass[j];
      String str = ((Field)localObject3).getName();
      Object localObject4 = ((Field)localObject3).getName();
      int k = ((String)localObject4).indexOf('$');
      int n = -1;
      Object localObject5;
      int m;
      if (k != n)
      {
        k = 0;
        localObject4 = null;
      }
      else
      {
        boolean bool2 = Modifier.isTransient(((Field)localObject3).getModifiers());
        if (bool2)
        {
          bool2 = f;
          if (!bool2)
          {
            bool2 = false;
            localObject4 = null;
            break label221;
          }
        }
        bool2 = Modifier.isStatic(((Field)localObject3).getModifiers());
        if (bool2)
        {
          bool2 = e;
          if (!bool2)
          {
            bool2 = false;
            localObject4 = null;
            break label221;
          }
        }
        localObject4 = a;
        if (localObject4 != null)
        {
          localObject5 = ((Field)localObject3).getName();
          m = Arrays.binarySearch((Object[])localObject4, localObject5);
          if (m >= 0)
          {
            m = 0;
            localObject4 = null;
            break label221;
          }
        }
        m = 1;
      }
      label221:
      if (m != 0) {
        try
        {
          localObject4 = c;
          localObject3 = ((Field)localObject3).get(localObject4);
          localObject4 = d;
          localObject5 = b;
          ((d)localObject4).a((StringBuffer)localObject5, str, localObject3, null);
        }
        catch (IllegalAccessException paramClass)
        {
          localObject1 = new java/lang/InternalError;
          localObject2 = new java/lang/StringBuilder;
          ((StringBuilder)localObject2).<init>("Unexpected IllegalAccessException: ");
          paramClass = paramClass.getMessage();
          ((StringBuilder)localObject2).append(paramClass);
          paramClass = ((StringBuilder)localObject2).toString();
          ((InternalError)localObject1).<init>(paramClass);
          throw ((Throwable)localObject1);
        }
      }
      j += 1;
    }
  }
  
  public final String toString()
  {
    Object localObject = c;
    if (localObject == null) {
      return d.t;
    }
    localObject = c.getClass();
    a((Class)localObject);
    for (;;)
    {
      Class localClass = ((Class)localObject).getSuperclass();
      if (localClass == null) {
        break;
      }
      localClass = g;
      if (localObject == localClass) {
        break;
      }
      localObject = ((Class)localObject).getSuperclass();
      a((Class)localObject);
    }
    return super.toString();
  }
}

/* Location:
 * Qualified Name:     org.c.a.a.a.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */