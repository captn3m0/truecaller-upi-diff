package org.c.a.a.a.a;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Map;
import java.util.WeakHashMap;
import org.c.a.a.a.h;

public abstract class d
  implements Serializable
{
  public static final d a;
  public static final d b;
  public static final d c;
  public static final d d;
  public static final d e;
  public static final d f;
  public static final d g;
  private static final long serialVersionUID = -2587890625525655916L;
  private static final ThreadLocal y;
  private boolean A;
  private boolean B;
  boolean h;
  boolean i;
  boolean j;
  boolean k;
  String l;
  String m;
  String n;
  boolean o;
  boolean p;
  String q;
  String r;
  String s;
  String t;
  String u;
  String v;
  String w;
  String x;
  private String z;
  
  static
  {
    Object localObject = new org/c/a/a/a/a/d$a;
    ((d.a)localObject).<init>();
    a = (d)localObject;
    localObject = new org/c/a/a/a/a/d$c;
    ((d.c)localObject).<init>();
    b = (d)localObject;
    localObject = new org/c/a/a/a/a/d$e;
    ((d.e)localObject).<init>();
    c = (d)localObject;
    localObject = new org/c/a/a/a/a/d$f;
    ((d.f)localObject).<init>();
    d = (d)localObject;
    localObject = new org/c/a/a/a/a/d$g;
    ((d.g)localObject).<init>();
    e = (d)localObject;
    localObject = new org/c/a/a/a/a/d$d;
    ((d.d)localObject).<init>();
    f = (d)localObject;
    localObject = new org/c/a/a/a/a/d$b;
    ((d.b)localObject).<init>();
    g = (d)localObject;
    localObject = new java/lang/ThreadLocal;
    ((ThreadLocal)localObject).<init>();
    y = (ThreadLocal)localObject;
  }
  
  protected d()
  {
    boolean bool = true;
    h = bool;
    i = bool;
    j = false;
    k = bool;
    l = "[";
    m = "]";
    n = "=";
    o = false;
    p = false;
    q = ",";
    r = "{";
    z = ",";
    A = bool;
    s = "}";
    B = bool;
    t = "<null>";
    u = "<size=";
    v = ">";
    w = "<";
    x = ">";
  }
  
  private static Map a()
  {
    return (Map)y.get();
  }
  
  static void a(Object paramObject)
  {
    if (paramObject != null)
    {
      Object localObject = a();
      if (localObject == null)
      {
        localObject = y;
        localWeakHashMap = new java/util/WeakHashMap;
        localWeakHashMap.<init>();
        ((ThreadLocal)localObject).set(localWeakHashMap);
      }
      localObject = a();
      WeakHashMap localWeakHashMap = null;
      ((Map)localObject).put(paramObject, null);
    }
  }
  
  private void a(StringBuffer paramStringBuffer, int paramInt)
  {
    String str1 = u;
    paramStringBuffer.append(str1);
    paramStringBuffer.append(paramInt);
    String str2 = v;
    paramStringBuffer.append(str2);
  }
  
  private void a(StringBuffer paramStringBuffer, String paramString, Object paramObject, boolean paramBoolean)
  {
    boolean bool = c(paramObject);
    if (bool)
    {
      bool = paramObject instanceof Number;
      if (!bool)
      {
        bool = paramObject instanceof Boolean;
        if (!bool)
        {
          bool = paramObject instanceof Character;
          if (!bool)
          {
            h.a(paramStringBuffer, paramObject);
            return;
          }
        }
      }
    }
    a(paramObject);
    bool = paramObject instanceof Collection;
    if (bool) {
      if (paramBoolean) {
        paramString = (String)paramObject;
      }
    }
    try
    {
      paramString = (Collection)paramObject;
      paramStringBuffer.append(paramString);
      break label768;
      paramString = (String)paramObject;
      paramString = (Collection)paramObject;
      int i1 = paramString.size();
      a(paramStringBuffer, i1);
      break label768;
      bool = paramObject instanceof Map;
      if (bool)
      {
        if (paramBoolean)
        {
          paramString = (String)paramObject;
          paramString = (Map)paramObject;
          paramStringBuffer.append(paramString);
        }
        else
        {
          paramString = (String)paramObject;
          paramString = (Map)paramObject;
          i1 = paramString.size();
          a(paramStringBuffer, i1);
        }
      }
      else
      {
        bool = paramObject instanceof long[];
        if (bool)
        {
          if (paramBoolean)
          {
            paramString = (String)paramObject;
            paramString = (long[])paramObject;
            paramString = (long[])paramString;
            a(paramStringBuffer, paramString);
          }
          else
          {
            paramString = (String)paramObject;
            paramString = (long[])paramObject;
            paramString = (long[])paramString;
            i1 = paramString.length;
            a(paramStringBuffer, i1);
          }
        }
        else
        {
          bool = paramObject instanceof int[];
          if (bool)
          {
            if (paramBoolean)
            {
              paramString = (String)paramObject;
              paramString = (int[])paramObject;
              paramString = (int[])paramString;
              a(paramStringBuffer, paramString);
            }
            else
            {
              paramString = (String)paramObject;
              paramString = (int[])paramObject;
              paramString = (int[])paramString;
              i1 = paramString.length;
              a(paramStringBuffer, i1);
            }
          }
          else
          {
            bool = paramObject instanceof short[];
            if (bool)
            {
              if (paramBoolean)
              {
                paramString = (String)paramObject;
                paramString = (short[])paramObject;
                paramString = (short[])paramString;
                a(paramStringBuffer, paramString);
              }
              else
              {
                paramString = (String)paramObject;
                paramString = (short[])paramObject;
                paramString = (short[])paramString;
                i1 = paramString.length;
                a(paramStringBuffer, i1);
              }
            }
            else
            {
              bool = paramObject instanceof byte[];
              if (bool)
              {
                if (paramBoolean)
                {
                  paramString = (String)paramObject;
                  paramString = (byte[])paramObject;
                  paramString = (byte[])paramString;
                  a(paramStringBuffer, paramString);
                }
                else
                {
                  paramString = (String)paramObject;
                  paramString = (byte[])paramObject;
                  paramString = (byte[])paramString;
                  i1 = paramString.length;
                  a(paramStringBuffer, i1);
                }
              }
              else
              {
                bool = paramObject instanceof char[];
                if (bool)
                {
                  if (paramBoolean)
                  {
                    paramString = (String)paramObject;
                    paramString = (char[])paramObject;
                    paramString = (char[])paramString;
                    a(paramStringBuffer, paramString);
                  }
                  else
                  {
                    paramString = (String)paramObject;
                    paramString = (char[])paramObject;
                    paramString = (char[])paramString;
                    i1 = paramString.length;
                    a(paramStringBuffer, i1);
                  }
                }
                else
                {
                  bool = paramObject instanceof double[];
                  if (bool)
                  {
                    if (paramBoolean)
                    {
                      paramString = (String)paramObject;
                      paramString = (double[])paramObject;
                      paramString = (double[])paramString;
                      a(paramStringBuffer, paramString);
                    }
                    else
                    {
                      paramString = (String)paramObject;
                      paramString = (double[])paramObject;
                      paramString = (double[])paramString;
                      i1 = paramString.length;
                      a(paramStringBuffer, i1);
                    }
                  }
                  else
                  {
                    bool = paramObject instanceof float[];
                    if (bool)
                    {
                      if (paramBoolean)
                      {
                        paramString = (String)paramObject;
                        paramString = (float[])paramObject;
                        paramString = (float[])paramString;
                        a(paramStringBuffer, paramString);
                      }
                      else
                      {
                        paramString = (String)paramObject;
                        paramString = (float[])paramObject;
                        paramString = (float[])paramString;
                        i1 = paramString.length;
                        a(paramStringBuffer, i1);
                      }
                    }
                    else
                    {
                      bool = paramObject instanceof boolean[];
                      if (bool)
                      {
                        if (paramBoolean)
                        {
                          paramString = (String)paramObject;
                          paramString = (boolean[])paramObject;
                          paramString = (boolean[])paramString;
                          a(paramStringBuffer, paramString);
                        }
                        else
                        {
                          paramString = (String)paramObject;
                          paramString = (boolean[])paramObject;
                          paramString = (boolean[])paramString;
                          i1 = paramString.length;
                          a(paramStringBuffer, i1);
                        }
                      }
                      else
                      {
                        Class localClass = paramObject.getClass();
                        bool = localClass.isArray();
                        if (bool)
                        {
                          if (paramBoolean)
                          {
                            Object localObject = paramObject;
                            localObject = (Object[])paramObject;
                            localObject = (Object[])localObject;
                            a(paramStringBuffer, paramString, (Object[])localObject);
                          }
                          else
                          {
                            paramString = (String)paramObject;
                            paramString = (Object[])paramObject;
                            paramString = (Object[])paramString;
                            i1 = paramString.length;
                            a(paramStringBuffer, i1);
                          }
                        }
                        else if (paramBoolean) {
                          a(paramStringBuffer, paramObject);
                        } else {
                          c(paramStringBuffer, paramObject);
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      label768:
      return;
    }
    finally
    {
      b(paramObject);
    }
  }
  
  private void a(StringBuffer paramStringBuffer, String paramString, Object[] paramArrayOfObject)
  {
    String str1 = r;
    paramStringBuffer.append(str1);
    int i1 = 0;
    str1 = null;
    for (;;)
    {
      int i2 = paramArrayOfObject.length;
      if (i1 >= i2) {
        break;
      }
      Object localObject = paramArrayOfObject[i1];
      if (i1 > 0)
      {
        String str2 = z;
        paramStringBuffer.append(str2);
      }
      if (localObject == null)
      {
        a(paramStringBuffer);
      }
      else
      {
        boolean bool = A;
        a(paramStringBuffer, paramString, localObject, bool);
      }
      i1 += 1;
    }
    paramString = s;
    paramStringBuffer.append(paramString);
  }
  
  private void a(StringBuffer paramStringBuffer, byte[] paramArrayOfByte)
  {
    String str1 = r;
    paramStringBuffer.append(str1);
    int i1 = 0;
    str1 = null;
    for (;;)
    {
      int i2 = paramArrayOfByte.length;
      if (i1 >= i2) {
        break;
      }
      if (i1 > 0)
      {
        String str2 = z;
        paramStringBuffer.append(str2);
      }
      i2 = paramArrayOfByte[i1];
      paramStringBuffer.append(i2);
      i1 += 1;
    }
    paramArrayOfByte = s;
    paramStringBuffer.append(paramArrayOfByte);
  }
  
  private void a(StringBuffer paramStringBuffer, char[] paramArrayOfChar)
  {
    String str1 = r;
    paramStringBuffer.append(str1);
    int i1 = 0;
    str1 = null;
    for (;;)
    {
      int i2 = paramArrayOfChar.length;
      if (i1 >= i2) {
        break;
      }
      if (i1 > 0)
      {
        String str2 = z;
        paramStringBuffer.append(str2);
      }
      i2 = paramArrayOfChar[i1];
      paramStringBuffer.append(i2);
      i1 += 1;
    }
    paramArrayOfChar = s;
    paramStringBuffer.append(paramArrayOfChar);
  }
  
  private void a(StringBuffer paramStringBuffer, double[] paramArrayOfDouble)
  {
    String str1 = r;
    paramStringBuffer.append(str1);
    int i1 = 0;
    str1 = null;
    for (;;)
    {
      int i2 = paramArrayOfDouble.length;
      if (i1 >= i2) {
        break;
      }
      if (i1 > 0)
      {
        String str2 = z;
        paramStringBuffer.append(str2);
      }
      double d1 = paramArrayOfDouble[i1];
      paramStringBuffer.append(d1);
      i1 += 1;
    }
    paramArrayOfDouble = s;
    paramStringBuffer.append(paramArrayOfDouble);
  }
  
  private void a(StringBuffer paramStringBuffer, float[] paramArrayOfFloat)
  {
    String str1 = r;
    paramStringBuffer.append(str1);
    int i1 = 0;
    str1 = null;
    for (;;)
    {
      int i2 = paramArrayOfFloat.length;
      if (i1 >= i2) {
        break;
      }
      if (i1 > 0)
      {
        String str2 = z;
        paramStringBuffer.append(str2);
      }
      float f1 = paramArrayOfFloat[i1];
      paramStringBuffer.append(f1);
      i1 += 1;
    }
    paramArrayOfFloat = s;
    paramStringBuffer.append(paramArrayOfFloat);
  }
  
  private void a(StringBuffer paramStringBuffer, int[] paramArrayOfInt)
  {
    String str1 = r;
    paramStringBuffer.append(str1);
    int i1 = 0;
    str1 = null;
    for (;;)
    {
      int i2 = paramArrayOfInt.length;
      if (i1 >= i2) {
        break;
      }
      if (i1 > 0)
      {
        String str2 = z;
        paramStringBuffer.append(str2);
      }
      i2 = paramArrayOfInt[i1];
      paramStringBuffer.append(i2);
      i1 += 1;
    }
    paramArrayOfInt = s;
    paramStringBuffer.append(paramArrayOfInt);
  }
  
  private void a(StringBuffer paramStringBuffer, long[] paramArrayOfLong)
  {
    String str1 = r;
    paramStringBuffer.append(str1);
    int i1 = 0;
    str1 = null;
    for (;;)
    {
      int i2 = paramArrayOfLong.length;
      if (i1 >= i2) {
        break;
      }
      if (i1 > 0)
      {
        String str2 = z;
        paramStringBuffer.append(str2);
      }
      long l1 = paramArrayOfLong[i1];
      paramStringBuffer.append(l1);
      i1 += 1;
    }
    paramArrayOfLong = s;
    paramStringBuffer.append(paramArrayOfLong);
  }
  
  private void a(StringBuffer paramStringBuffer, short[] paramArrayOfShort)
  {
    String str1 = r;
    paramStringBuffer.append(str1);
    int i1 = 0;
    str1 = null;
    for (;;)
    {
      int i2 = paramArrayOfShort.length;
      if (i1 >= i2) {
        break;
      }
      if (i1 > 0)
      {
        String str2 = z;
        paramStringBuffer.append(str2);
      }
      i2 = paramArrayOfShort[i1];
      paramStringBuffer.append(i2);
      i1 += 1;
    }
    paramArrayOfShort = s;
    paramStringBuffer.append(paramArrayOfShort);
  }
  
  private void a(StringBuffer paramStringBuffer, boolean[] paramArrayOfBoolean)
  {
    String str1 = r;
    paramStringBuffer.append(str1);
    int i1 = 0;
    str1 = null;
    for (;;)
    {
      int i2 = paramArrayOfBoolean.length;
      if (i1 >= i2) {
        break;
      }
      if (i1 > 0)
      {
        String str2 = z;
        paramStringBuffer.append(str2);
      }
      i2 = paramArrayOfBoolean[i1];
      paramStringBuffer.append(i2);
      i1 += 1;
    }
    paramArrayOfBoolean = s;
    paramStringBuffer.append(paramArrayOfBoolean);
  }
  
  static void b(Object paramObject)
  {
    if (paramObject != null)
    {
      Map localMap = a();
      if (localMap != null)
      {
        localMap.remove(paramObject);
        boolean bool = localMap.isEmpty();
        if (bool)
        {
          paramObject = y;
          ((ThreadLocal)paramObject).remove();
        }
      }
    }
  }
  
  private void c(StringBuffer paramStringBuffer, Object paramObject)
  {
    String str = w;
    paramStringBuffer.append(str);
    paramObject = org.c.a.a.a.d.a(paramObject.getClass());
    paramStringBuffer.append((String)paramObject);
    paramObject = x;
    paramStringBuffer.append((String)paramObject);
  }
  
  private static boolean c(Object paramObject)
  {
    Map localMap = a();
    if (localMap != null)
    {
      boolean bool = localMap.containsKey(paramObject);
      if (bool) {
        return true;
      }
    }
    return false;
  }
  
  protected final void a(String paramString)
  {
    if (paramString == null) {
      paramString = "";
    }
    m = paramString;
  }
  
  protected final void a(StringBuffer paramStringBuffer)
  {
    String str = t;
    paramStringBuffer.append(str);
  }
  
  protected void a(StringBuffer paramStringBuffer, Object paramObject)
  {
    paramStringBuffer.append(paramObject);
  }
  
  protected void a(StringBuffer paramStringBuffer, String paramString)
  {
    boolean bool = h;
    if ((bool) && (paramString != null))
    {
      paramStringBuffer.append(paramString);
      paramString = n;
      paramStringBuffer.append(paramString);
    }
  }
  
  public void a(StringBuffer paramStringBuffer, String paramString, Object paramObject, Boolean paramBoolean)
  {
    a(paramStringBuffer, paramString);
    if (paramObject == null)
    {
      a(paramStringBuffer);
    }
    else
    {
      boolean bool = a(paramBoolean);
      a(paramStringBuffer, paramString, paramObject, bool);
    }
    b(paramStringBuffer);
  }
  
  protected final boolean a(Boolean paramBoolean)
  {
    if (paramBoolean == null) {
      return B;
    }
    return paramBoolean.booleanValue();
  }
  
  protected final void b(String paramString)
  {
    if (paramString == null) {
      paramString = "";
    }
    q = paramString;
  }
  
  final void b(StringBuffer paramStringBuffer)
  {
    String str = q;
    paramStringBuffer.append(str);
  }
  
  protected final void b(StringBuffer paramStringBuffer, Object paramObject)
  {
    String str1 = r;
    paramStringBuffer.append(str1);
    int i1 = Array.getLength(paramObject);
    int i2 = 0;
    while (i2 < i1)
    {
      Object localObject = Array.get(paramObject, i2);
      String str2;
      if (i2 > 0)
      {
        str2 = z;
        paramStringBuffer.append(str2);
      }
      if (localObject == null)
      {
        a(paramStringBuffer);
      }
      else
      {
        str2 = null;
        boolean bool = A;
        a(paramStringBuffer, null, localObject, bool);
      }
      i2 += 1;
    }
    paramObject = s;
    paramStringBuffer.append((String)paramObject);
  }
}

/* Location:
 * Qualified Name:     org.c.a.a.a.a.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */