package org.c.a.a.a.b;

import java.math.BigDecimal;
import java.math.BigInteger;
import org.c.a.a.a.k;

public final class a
{
  public static final Long a = Long.valueOf(0L);
  public static final Long b = Long.valueOf(1L);
  public static final Long c = Long.valueOf(-1);
  public static final Integer d = Integer.valueOf(0);
  public static final Integer e;
  public static final Integer f;
  public static final Short g;
  public static final Short h;
  public static final Short i;
  public static final Byte j;
  public static final Byte k;
  public static final Byte l;
  public static final Double m = Double.valueOf(0.0D);
  public static final Double n = Double.valueOf(1.0D);
  public static final Double o = Double.valueOf(-1.0D);
  public static final Float p = Float.valueOf(0.0F);
  public static final Float q = Float.valueOf(1.0F);
  public static final Float r = Float.valueOf(-1.0F);
  
  static
  {
    byte b1 = 1;
    e = Integer.valueOf(b1);
    byte b2 = -1;
    f = Integer.valueOf(b2);
    g = Short.valueOf((short)0);
    h = Short.valueOf(b1);
    i = Short.valueOf(b2);
    j = Byte.valueOf((byte)0);
    k = Byte.valueOf(b1);
    l = Byte.valueOf(b2);
  }
  
  public static int a(String paramString)
  {
    return a(paramString, 0);
  }
  
  public static int a(String paramString, int paramInt)
  {
    if (paramString == null) {
      return paramInt;
    }
    try
    {
      return Integer.parseInt(paramString);
    }
    catch (NumberFormatException localNumberFormatException) {}
    return paramInt;
  }
  
  public static Number b(String paramString)
  {
    int i1 = 0;
    Object localObject1 = null;
    if (paramString == null) {
      return null;
    }
    boolean bool3 = k.d(paramString);
    if (!bool3)
    {
      String str1 = "-0x";
      String str2 = "-0X";
      String str3 = "#";
      String str4 = "-#";
      String[] tmp40_37 = new String[6];
      String[] tmp41_40 = tmp40_37;
      String[] tmp41_40 = tmp40_37;
      tmp41_40[0] = "0x";
      tmp41_40[1] = "0X";
      String[] tmp50_41 = tmp41_40;
      String[] tmp50_41 = tmp41_40;
      tmp50_41[2] = str1;
      tmp50_41[3] = str2;
      tmp50_41[4] = str3;
      String[] tmp63_50 = tmp50_41;
      tmp63_50[5] = str4;
      Object localObject2 = tmp63_50;
      int i7 = 0;
      int i8 = 0;
      String str5 = null;
      for (;;)
      {
        int i11 = 6;
        if (i8 >= i11) {
          break;
        }
        str1 = localObject2[i8];
        boolean bool6 = paramString.startsWith(str1);
        if (bool6)
        {
          i4 = str1.length() + 0;
          break label138;
        }
        i8 += 1;
      }
      int i4 = 0;
      float f1 = 0.0F;
      localObject2 = null;
      label138:
      i8 = 16;
      if (i4 > 0)
      {
        i1 = i4;
        for (;;)
        {
          int i12 = paramString.length();
          if (i4 >= i12) {
            break;
          }
          i7 = paramString.charAt(i4);
          i13 = 48;
          if (i7 != i13) {
            break;
          }
          i1 += 1;
          i4 += 1;
        }
        i4 = paramString.length() - i1;
        if (i4 <= i8)
        {
          i1 = 55;
          if ((i4 != i8) || (i7 <= i1))
          {
            int i9 = 8;
            if ((i4 <= i9) && ((i4 != i9) || (i7 <= i1))) {
              return f(paramString);
            }
            return g(paramString);
          }
        }
        return h(paramString);
      }
      i4 = paramString.length();
      int i13 = 1;
      i4 -= i13;
      i4 = paramString.charAt(i4);
      int i15 = 46;
      int i17 = paramString.indexOf(i15);
      int i22 = paramString.indexOf('e');
      int i25 = paramString.indexOf('E');
      i22 = i22 + i25 + i13;
      int i27;
      String str6;
      label422:
      int i28;
      if (i17 >= 0)
      {
        if (i22 >= 0)
        {
          if (i22 >= i17)
          {
            int i26 = paramString.length();
            if (i22 <= i26)
            {
              i27 = i17 + 1;
              str6 = paramString.substring(i27, i22);
              break label422;
            }
          }
          localObject1 = new java/lang/NumberFormatException;
          localObject2 = new java/lang/StringBuilder;
          ((StringBuilder)localObject2).<init>();
          ((StringBuilder)localObject2).append(paramString);
          ((StringBuilder)localObject2).append(" is not a valid number.");
          paramString = ((StringBuilder)localObject2).toString();
          ((NumberFormatException)localObject1).<init>(paramString);
          throw ((Throwable)localObject1);
        }
        else
        {
          i27 = i17 + 1;
          str6 = paramString.substring(i27);
        }
        str3 = b(paramString, i17);
        i28 = str6.length();
      }
      else
      {
        if (i22 >= 0)
        {
          i17 = paramString.length();
          if (i22 <= i17)
          {
            str3 = b(paramString, i22);
          }
          else
          {
            localObject1 = new java/lang/NumberFormatException;
            localObject2 = new java/lang/StringBuilder;
            ((StringBuilder)localObject2).<init>();
            ((StringBuilder)localObject2).append(paramString);
            ((StringBuilder)localObject2).append(" is not a valid number.");
            paramString = ((StringBuilder)localObject2).toString();
            ((NumberFormatException)localObject1).<init>(paramString);
            throw ((Throwable)localObject1);
          }
        }
        else
        {
          i17 = paramString.length();
          str3 = b(paramString, i17);
        }
        i27 = 0;
        str6 = null;
        i28 = 0;
      }
      boolean bool8 = Character.isDigit(i4);
      double d1 = 0.0D;
      int i10;
      int i23;
      label651:
      label779:
      label859:
      int i14;
      if ((!bool8) && (i4 != i15))
      {
        if (i22 >= 0)
        {
          i10 = paramString.length() - i13;
          if (i22 < i10)
          {
            i22 += i13;
            i1 = paramString.length() - i13;
            localObject1 = paramString.substring(i23, i1);
          }
        }
        i10 = paramString.length() - i13;
        str5 = paramString.substring(0, i10);
        boolean bool7 = c(str3);
        if (bool7)
        {
          bool7 = c((String)localObject1);
          if (bool7)
          {
            bool7 = true;
            break label651;
          }
        }
        bool7 = false;
        str2 = null;
        i17 = 68;
        int i16;
        boolean bool4;
        if (i4 != i17)
        {
          int i18 = 70;
          if (i4 != i18)
          {
            int i19 = 76;
            if (i4 != i19)
            {
              int i20 = 100;
              if (i4 == i20) {
                break label859;
              }
              int i21 = 102;
              if (i4 != i21)
              {
                i16 = 108;
                if (i4 != i16) {
                  break label912;
                }
              }
            }
            else
            {
              if ((str6 == null) && (localObject1 == null))
              {
                i1 = str5.charAt(0);
                int i5 = 45;
                f1 = 6.3E-44F;
                boolean bool1;
                if (i1 == i5)
                {
                  localObject1 = str5.substring(i13);
                  bool1 = j((String)localObject1);
                  if (bool1) {}
                }
                else
                {
                  bool1 = j(str5);
                  if (!bool1) {
                    break label779;
                  }
                }
                try
                {
                  return g(str5);
                }
                catch (NumberFormatException localNumberFormatException1)
                {
                  return h(str5);
                }
              }
              localObject1 = new java/lang/NumberFormatException;
              localObject2 = new java/lang/StringBuilder;
              ((StringBuilder)localObject2).<init>();
              ((StringBuilder)localObject2).append(paramString);
              ((StringBuilder)localObject2).append(" is not a valid number.");
              paramString = ((StringBuilder)localObject2).toString();
              ((NumberFormatException)localObject1).<init>(paramString);
              throw ((Throwable)localObject1);
            }
          }
          try
          {
            localObject1 = d(str5);
            bool4 = ((Float)localObject1).isInfinite();
            if (!bool4)
            {
              f1 = ((Float)localObject1).floatValue();
              bool4 = f1 < 0.0F;
              if ((bool4) || (i16 != 0)) {
                return (Number)localObject1;
              }
            }
          }
          catch (NumberFormatException localNumberFormatException2) {}
        }
        try
        {
          localObject1 = e(str5);
          bool4 = ((Double)localObject1).isInfinite();
          if (!bool4)
          {
            f1 = ((Double)localObject1).floatValue();
            double d2 = f1;
            i14 = d2 < d1;
            if ((i14 != 0) || (i16 != 0)) {
              return (Number)localObject1;
            }
          }
        }
        catch (NumberFormatException localNumberFormatException3)
        {
          try
          {
            return i(str5);
          }
          catch (NumberFormatException localNumberFormatException4) {}
        }
        label912:
        localObject1 = new java/lang/NumberFormatException;
        localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>();
        ((StringBuilder)localObject2).append(paramString);
        ((StringBuilder)localObject2).append(" is not a valid number.");
        paramString = ((StringBuilder)localObject2).toString();
        ((NumberFormatException)localObject1).<init>(paramString);
        throw ((Throwable)localObject1);
      }
      if (i23 >= 0)
      {
        int i6 = paramString.length() - i14;
        if (i23 < i6)
        {
          int i24;
          i23 += i14;
          int i2 = paramString.length();
          localObject1 = paramString.substring(i24, i2);
        }
      }
      if ((str6 == null) && (localObject1 == null)) {
        try
        {
          return f(paramString);
        }
        catch (NumberFormatException localNumberFormatException5)
        {
          try
          {
            return g(paramString);
          }
          catch (NumberFormatException localNumberFormatException6)
          {
            return h(paramString);
          }
        }
      }
      boolean bool5 = c(str3);
      if (bool5)
      {
        boolean bool2 = c((String)localObject1);
        if (bool2) {
          i7 = 1;
        }
      }
      int i3 = 7;
      if (i28 <= i3) {
        try
        {
          localObject1 = d(paramString);
          bool5 = ((Float)localObject1).isInfinite();
          if (!bool5)
          {
            f1 = ((Float)localObject1).floatValue();
            bool5 = f1 < 0.0F;
            if ((bool5) || (i7 != 0)) {
              return (Number)localObject1;
            }
          }
        }
        catch (NumberFormatException localNumberFormatException7) {}
      }
      if (i28 <= i10) {
        try
        {
          localObject1 = e(paramString);
          bool5 = ((Double)localObject1).isInfinite();
          if (!bool5)
          {
            double d3 = ((Double)localObject1).doubleValue();
            bool5 = d3 < d1;
            if ((bool5) || (i7 != 0)) {
              return (Number)localObject1;
            }
          }
        }
        catch (NumberFormatException localNumberFormatException8) {}
      }
      return i(paramString);
    }
    paramString = new java/lang/NumberFormatException;
    paramString.<init>("A blank string is not a valid number");
    throw paramString;
  }
  
  private static String b(String paramString, int paramInt)
  {
    int i1 = paramString.charAt(0);
    int i2 = 1;
    int i3 = 45;
    if (i1 != i3)
    {
      i3 = 43;
      if (i1 != i3)
      {
        i1 = 0;
        break label38;
      }
    }
    i1 = 1;
    label38:
    if (i1 != 0) {
      return paramString.substring(i2, paramInt);
    }
    return paramString.substring(0, paramInt);
  }
  
  private static boolean c(String paramString)
  {
    int i1 = 1;
    if (paramString == null) {
      return i1;
    }
    int i2 = paramString.length() - i1;
    while (i2 >= 0)
    {
      int i3 = paramString.charAt(i2);
      int i4 = 48;
      if (i3 != i4) {
        return false;
      }
      i2 += -1;
    }
    int i5 = paramString.length();
    if (i5 > 0) {
      return i1;
    }
    return false;
  }
  
  private static Float d(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    return Float.valueOf(paramString);
  }
  
  private static Double e(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    return Double.valueOf(paramString);
  }
  
  private static Integer f(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    return Integer.decode(paramString);
  }
  
  private static Long g(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    return Long.decode(paramString);
  }
  
  private static BigInteger h(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    int i1 = 10;
    Object localObject = "-";
    boolean bool = paramString.startsWith((String)localObject);
    int i3 = 0;
    int i4 = 1;
    if (bool) {
      i3 = 1;
    } else {
      i4 = 0;
    }
    localObject = "0x";
    bool = paramString.startsWith((String)localObject, i3);
    int i5 = 16;
    if (!bool)
    {
      localObject = "0X";
      bool = paramString.startsWith((String)localObject, i3);
      if (!bool)
      {
        localObject = "#";
        bool = paramString.startsWith((String)localObject, i3);
        if (bool)
        {
          i3 += 1;
          i1 = 16;
          break label154;
        }
        localObject = "0";
        bool = paramString.startsWith((String)localObject, i3);
        if (!bool) {
          break label154;
        }
        int i2 = paramString.length();
        i5 = i3 + 1;
        if (i2 <= i5) {
          break label154;
        }
        i1 = 8;
        i3 = i5;
        break label154;
      }
    }
    i3 += 2;
    i1 = 16;
    label154:
    localObject = new java/math/BigInteger;
    paramString = paramString.substring(i3);
    ((BigInteger)localObject).<init>(paramString, i1);
    if (i4 != 0) {
      return ((BigInteger)localObject).negate();
    }
    return (BigInteger)localObject;
  }
  
  private static BigDecimal i(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    boolean bool = k.d(paramString);
    if (!bool)
    {
      Object localObject1 = paramString.trim();
      Object localObject2 = "--";
      bool = ((String)localObject1).startsWith((String)localObject2);
      if (!bool)
      {
        localObject1 = new java/math/BigDecimal;
        ((BigDecimal)localObject1).<init>(paramString);
        return (BigDecimal)localObject1;
      }
      localObject1 = new java/lang/NumberFormatException;
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      ((StringBuilder)localObject2).append(paramString);
      ((StringBuilder)localObject2).append(" is not a valid number.");
      paramString = ((StringBuilder)localObject2).toString();
      ((NumberFormatException)localObject1).<init>(paramString);
      throw ((Throwable)localObject1);
    }
    paramString = new java/lang/NumberFormatException;
    paramString.<init>("A blank string is not a valid number");
    throw paramString;
  }
  
  private static boolean j(String paramString)
  {
    int i1 = k.b(paramString);
    if (i1 != 0) {
      return false;
    }
    i1 = 0;
    for (;;)
    {
      int i3 = paramString.length();
      if (i1 >= i3) {
        break;
      }
      boolean bool = Character.isDigit(paramString.charAt(i1));
      if (!bool) {
        return false;
      }
      int i2;
      i1 += 1;
    }
    return true;
  }
}

/* Location:
 * Qualified Name:     org.c.a.a.a.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */