package org.c.a.a.a;

import java.util.Random;

public final class j
{
  private static final Random a;
  
  static
  {
    Random localRandom = new java/util/Random;
    localRandom.<init>();
    a = localRandom;
  }
  
  public static int a()
  {
    Object[] arrayOfObject = new Object[0];
    boolean bool = true;
    m.a(bool, "Start value must be smaller or equal to end value.", arrayOfObject);
    arrayOfObject = new Object[0];
    m.a(bool, "Both range values must be non-negative.", arrayOfObject);
    return a.nextInt(2) + 0;
  }
  
  public static long a(long paramLong1, long paramLong2)
  {
    boolean bool1 = true;
    Object[] arrayOfObject1 = null;
    boolean bool2 = paramLong2 < paramLong1;
    if (!bool2)
    {
      bool2 = true;
    }
    else
    {
      bool2 = false;
      str1 = null;
    }
    String str2 = "Start value must be smaller or equal to end value.";
    Object[] arrayOfObject2 = new Object[0];
    m.a(bool2, str2, arrayOfObject2);
    long l = 0L;
    double d1 = 0.0D;
    boolean bool3 = paramLong1 < l;
    if (!bool3)
    {
      bool2 = true;
    }
    else
    {
      bool2 = false;
      str1 = null;
    }
    str2 = "Both range values must be non-negative.";
    arrayOfObject2 = new Object[0];
    m.a(bool2, str2, arrayOfObject2);
    bool2 = paramLong1 < paramLong2;
    if (!bool2) {
      return paramLong1;
    }
    double d2 = paramLong1;
    double d3 = paramLong2;
    bool2 = d3 < d2;
    if (!bool2)
    {
      bool2 = true;
    }
    else
    {
      bool2 = false;
      str1 = null;
    }
    str2 = "Start value must be smaller or equal to end value.";
    arrayOfObject2 = new Object[0];
    m.a(bool2, str2, arrayOfObject2);
    l = 0L;
    d1 = 0.0D;
    bool3 = d2 < d1;
    Random localRandom;
    if (bool3)
    {
      bool1 = false;
      localRandom = null;
    }
    String str1 = "Both range values must be non-negative.";
    arrayOfObject1 = new Object[0];
    m.a(bool1, str1, arrayOfObject1);
    bool1 = d2 < d3;
    if (bool1)
    {
      Double.isNaN(d3);
      Double.isNaN(d2);
      d3 -= d2;
      localRandom = a;
      double d4 = localRandom.nextDouble();
      d3 *= d4;
      Double.isNaN(d2);
      d2 += d3;
    }
    return d2;
  }
}

/* Location:
 * Qualified Name:     org.c.a.a.a.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */