package org.c.a.a.a.c;

import java.io.Serializable;
import java.nio.CharBuffer;
import java.util.Iterator;
import org.c.a.a.a.h;

public final class a
  implements Serializable, Appendable, CharSequence
{
  private static final long serialVersionUID = 7628716375283629643L;
  protected char[] a;
  protected int b;
  private String c;
  
  public a()
  {
    this(32);
  }
  
  public a(int paramInt)
  {
    if (paramInt <= 0) {
      paramInt = 32;
    }
    char[] arrayOfChar = new char[paramInt];
    a = arrayOfChar;
  }
  
  public a(String paramString)
  {
    int i = 32;
    if (paramString == null)
    {
      paramString = new char[i];
      a = paramString;
      return;
    }
    char[] arrayOfChar = new char[paramString.length() + i];
    a = arrayOfChar;
    a(paramString);
  }
  
  private int a(int paramInt1, int paramInt2)
  {
    if (paramInt1 >= 0)
    {
      int i = b;
      if (paramInt2 > i) {
        paramInt2 = i;
      }
      if (paramInt1 <= paramInt2) {
        return paramInt2;
      }
      StringIndexOutOfBoundsException localStringIndexOutOfBoundsException1 = new java/lang/StringIndexOutOfBoundsException;
      localStringIndexOutOfBoundsException1.<init>("end < start");
      throw localStringIndexOutOfBoundsException1;
    }
    StringIndexOutOfBoundsException localStringIndexOutOfBoundsException2 = new java/lang/StringIndexOutOfBoundsException;
    localStringIndexOutOfBoundsException2.<init>(paramInt1);
    throw localStringIndexOutOfBoundsException2;
  }
  
  private a a()
  {
    String str = c;
    if (str == null) {
      return this;
    }
    return a(str);
  }
  
  private a a(CharSequence paramCharSequence)
  {
    if (paramCharSequence == null) {
      return a();
    }
    boolean bool1 = paramCharSequence instanceof a;
    int n = 0;
    int i1;
    int i2;
    char[] arrayOfChar1;
    int i3;
    if (bool1)
    {
      paramCharSequence = (a)paramCharSequence;
      if (paramCharSequence == null) {
        return a();
      }
      int i = paramCharSequence.length();
      if (i > 0)
      {
        i1 = length();
        i2 = i1 + i;
        b(i2);
        paramCharSequence = a;
        arrayOfChar1 = a;
        System.arraycopy(paramCharSequence, 0, arrayOfChar1, i1, i);
        i3 = b + i;
        b = i3;
      }
      return this;
    }
    boolean bool2 = paramCharSequence instanceof StringBuilder;
    if (bool2)
    {
      paramCharSequence = (StringBuilder)paramCharSequence;
      if (paramCharSequence == null) {
        return a();
      }
      int j = paramCharSequence.length();
      if (j > 0)
      {
        i1 = length();
        i2 = i1 + j;
        b(i2);
        arrayOfChar1 = a;
        paramCharSequence.getChars(0, j, arrayOfChar1, i1);
        i3 = b + j;
        b = i3;
      }
      return this;
    }
    boolean bool3 = paramCharSequence instanceof StringBuffer;
    if (bool3)
    {
      paramCharSequence = (StringBuffer)paramCharSequence;
      if (paramCharSequence == null) {
        return a();
      }
      int k = paramCharSequence.length();
      if (k > 0)
      {
        i1 = length();
        i2 = i1 + k;
        b(i2);
        arrayOfChar1 = a;
        paramCharSequence.getChars(0, k, arrayOfChar1, i1);
        i3 = b + k;
        b = i3;
      }
      return this;
    }
    boolean bool4 = paramCharSequence instanceof CharBuffer;
    if (bool4)
    {
      paramCharSequence = (CharBuffer)paramCharSequence;
      if (paramCharSequence == null) {
        return a();
      }
      bool4 = paramCharSequence.hasArray();
      if (bool4)
      {
        int m = paramCharSequence.remaining();
        n = length();
        i1 = n + m;
        b(i1);
        char[] arrayOfChar2 = paramCharSequence.array();
        i2 = paramCharSequence.arrayOffset();
        i3 = paramCharSequence.position();
        i2 += i3;
        paramCharSequence = a;
        System.arraycopy(arrayOfChar2, i2, paramCharSequence, n, m);
        i3 = b + m;
        b = i3;
      }
      else
      {
        paramCharSequence = paramCharSequence.toString();
        a(paramCharSequence);
      }
      return this;
    }
    paramCharSequence = paramCharSequence.toString();
    return a(paramCharSequence);
  }
  
  private a b(int paramInt)
  {
    char[] arrayOfChar1 = a;
    int i = arrayOfChar1.length;
    if (paramInt > i)
    {
      paramInt *= 2;
      char[] arrayOfChar2 = new char[paramInt];
      a = arrayOfChar2;
      arrayOfChar2 = a;
      i = b;
      System.arraycopy(arrayOfChar1, 0, arrayOfChar2, 0, i);
    }
    return this;
  }
  
  public final a a(int paramInt)
  {
    if (paramInt >= 0)
    {
      int i = b;
      if (paramInt < i)
      {
        int j = paramInt + 1;
        char[] arrayOfChar = a;
        i -= j;
        System.arraycopy(arrayOfChar, j, arrayOfChar, paramInt, i);
        paramInt = b + -1;
        b = paramInt;
        return this;
      }
    }
    StringIndexOutOfBoundsException localStringIndexOutOfBoundsException = new java/lang/StringIndexOutOfBoundsException;
    localStringIndexOutOfBoundsException.<init>(paramInt);
    throw localStringIndexOutOfBoundsException;
  }
  
  public final a a(int paramInt1, int paramInt2, String paramString)
  {
    paramInt2 = a(paramInt1, paramInt2);
    int i;
    if (paramString == null) {
      i = 0;
    } else {
      i = paramString.length();
    }
    int j = paramInt2 - paramInt1;
    int k = b - j + i;
    if (i != j)
    {
      b(k);
      char[] arrayOfChar1 = a;
      int m = paramInt1 + i;
      int n = b - paramInt2;
      System.arraycopy(arrayOfChar1, paramInt2, arrayOfChar1, m, n);
      b = k;
    }
    if (i > 0)
    {
      char[] arrayOfChar2 = a;
      paramString.getChars(0, i, arrayOfChar2, paramInt1);
    }
    return this;
  }
  
  public final a a(Iterable paramIterable, String paramString)
  {
    if (paramIterable != null)
    {
      paramString = h.a(paramString);
      paramIterable = paramIterable.iterator();
      for (;;)
      {
        boolean bool1 = paramIterable.hasNext();
        if (!bool1) {
          break;
        }
        Object localObject = paramIterable.next();
        if (localObject == null)
        {
          a();
        }
        else
        {
          boolean bool2 = localObject instanceof CharSequence;
          if (bool2)
          {
            localObject = (CharSequence)localObject;
            a((CharSequence)localObject);
          }
          else
          {
            localObject = localObject.toString();
            a((String)localObject);
          }
        }
        bool1 = paramIterable.hasNext();
        if (bool1) {
          a(paramString);
        }
      }
    }
    return this;
  }
  
  public final a a(String paramString)
  {
    if (paramString == null) {
      return a();
    }
    int i = paramString.length();
    if (i > 0)
    {
      int j = length();
      int k = j + i;
      b(k);
      k = 0;
      char[] arrayOfChar = a;
      paramString.getChars(0, i, arrayOfChar, j);
      int m = b + i;
      b = m;
    }
    return this;
  }
  
  public final char charAt(int paramInt)
  {
    if (paramInt >= 0)
    {
      int i = length();
      if (paramInt < i) {
        return a[paramInt];
      }
    }
    StringIndexOutOfBoundsException localStringIndexOutOfBoundsException = new java/lang/StringIndexOutOfBoundsException;
    localStringIndexOutOfBoundsException.<init>(paramInt);
    throw localStringIndexOutOfBoundsException;
  }
  
  public final boolean equals(Object paramObject)
  {
    int i = paramObject instanceof a;
    if (i != 0)
    {
      paramObject = (a)paramObject;
      i = 1;
      if (this != paramObject)
      {
        int j = b;
        int m = b;
        if (j != m) {
          return false;
        }
        char[] arrayOfChar = a;
        paramObject = a;
        int k;
        j -= i;
        while (k >= 0)
        {
          int n = arrayOfChar[k];
          int i1 = paramObject[k];
          if (n != i1) {
            return false;
          }
          k += -1;
        }
      }
      return i;
    }
    return false;
  }
  
  public final int hashCode()
  {
    char[] arrayOfChar = a;
    int i = b + -1;
    int j = 0;
    while (i >= 0)
    {
      j *= 31;
      int k = arrayOfChar[i];
      j += k;
      i += -1;
    }
    return j;
  }
  
  public final int length()
  {
    return b;
  }
  
  public final CharSequence subSequence(int paramInt1, int paramInt2)
  {
    if (paramInt1 >= 0)
    {
      int i = b;
      if (paramInt2 <= i)
      {
        if (paramInt1 <= paramInt2)
        {
          paramInt2 = a(paramInt1, paramInt2);
          localObject = new java/lang/String;
          char[] arrayOfChar = a;
          paramInt2 -= paramInt1;
          ((String)localObject).<init>(arrayOfChar, paramInt1, paramInt2);
          return (CharSequence)localObject;
        }
        Object localObject = new java/lang/StringIndexOutOfBoundsException;
        paramInt2 -= paramInt1;
        ((StringIndexOutOfBoundsException)localObject).<init>(paramInt2);
        throw ((Throwable)localObject);
      }
      StringIndexOutOfBoundsException localStringIndexOutOfBoundsException1 = new java/lang/StringIndexOutOfBoundsException;
      localStringIndexOutOfBoundsException1.<init>(paramInt2);
      throw localStringIndexOutOfBoundsException1;
    }
    StringIndexOutOfBoundsException localStringIndexOutOfBoundsException2 = new java/lang/StringIndexOutOfBoundsException;
    localStringIndexOutOfBoundsException2.<init>(paramInt1);
    throw localStringIndexOutOfBoundsException2;
  }
  
  public final String toString()
  {
    String str = new java/lang/String;
    char[] arrayOfChar = a;
    int i = b;
    str.<init>(arrayOfChar, 0, i);
    return str;
  }
}

/* Location:
 * Qualified Name:     org.c.a.a.a.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */