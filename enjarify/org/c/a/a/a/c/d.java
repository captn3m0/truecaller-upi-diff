package org.c.a.a.a.c;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public final class d
{
  public static final c a = c.a("${");
  public static final c b = c.a("}");
  public static final c c = c.a(":-");
  private char d;
  private c e;
  private c f;
  private c g;
  private b h;
  private boolean i;
  
  public d()
  {
    this(null, localc1, localc2);
  }
  
  public d(Map paramMap)
  {
    this(paramMap, localc1, localc2);
  }
  
  private d(b paramb, c paramc1, c paramc2)
  {
    this(paramb, paramc1, paramc2, localc);
  }
  
  private d(b paramb, c paramc1, c paramc2, c paramc3)
  {
    h = paramb;
    if (paramc1 != null)
    {
      e = paramc1;
      if (paramc2 != null)
      {
        f = paramc2;
        d = '$';
        g = paramc3;
        return;
      }
      paramb = new java/lang/IllegalArgumentException;
      paramb.<init>("Variable suffix matcher must not be null!");
      throw paramb;
    }
    paramb = new java/lang/IllegalArgumentException;
    paramb.<init>("Variable prefix matcher must not be null!");
    throw paramb;
  }
  
  private int a(a parama, int paramInt1, int paramInt2, List paramList)
  {
    d locald = this;
    a locala = parama;
    int j = paramInt1;
    int k = paramInt2;
    Object localObject1 = e;
    Object localObject2 = f;
    int m = d;
    c localc = g;
    boolean bool = i;
    int n;
    float f1;
    if (paramList == null)
    {
      n = 1;
      f1 = Float.MIN_VALUE;
    }
    else
    {
      n = 0;
      f1 = 0.0F;
    }
    char[] arrayOfChar = a;
    int i1 = j + k;
    Object localObject3 = paramList;
    Object localObject4 = arrayOfChar;
    int i2 = 0;
    int i3 = 0;
    int i4 = j;
    while (i4 < i1)
    {
      int i5 = ((c)localObject1).a((char[])localObject4, i4, i1);
      if (i5 == 0)
      {
        i4 += 1;
      }
      else
      {
        if (i4 > j)
        {
          i6 = i4 + -1;
          i7 = localObject4[i6];
          if (i7 == m)
          {
            locala.a(i6);
            localObject4 = a;
            i2 += -1;
            i1 += -1;
            i3 = 1;
            continue;
          }
        }
        int i7 = i4 + i5;
        int i6 = i7;
        int i8 = 0;
        for (;;)
        {
          if (i6 >= i1) {
            break label806;
          }
          if (bool)
          {
            i9 = ((c)localObject1).a((char[])localObject4, i6, i1);
            if (i9 != 0)
            {
              i8 += 1;
              i6 += i9;
              continue;
            }
          }
          int i9 = ((c)localObject2).a((char[])localObject4, i6, i1);
          if (i9 == 0)
          {
            i6 += 1;
          }
          else
          {
            if (i8 == 0)
            {
              localObject5 = localObject2;
              localObject2 = new java/lang/String;
              i8 = i6 - i4;
              i10 = m;
              m = i8 - i5;
              ((String)localObject2).<init>((char[])localObject4, i7, m);
              if (bool)
              {
                localObject6 = new org/c/a/a/a/c/a;
                ((a)localObject6).<init>((String)localObject2);
                i11 = ((a)localObject6).length();
                locald.a((a)localObject6, i11);
                localObject2 = ((a)localObject6).toString();
              }
              i6 += i9;
              Object localObject7;
              if (localc != null)
              {
                localObject7 = ((String)localObject2).toCharArray();
                i12 = n;
                f2 = f1;
                m = 0;
                localObject6 = null;
                for (;;)
                {
                  n = localObject7.length;
                  if (m >= n) {
                    break;
                  }
                  if (!bool)
                  {
                    n = localObject7.length;
                    n = ((c)localObject1).a((char[])localObject7, m, n);
                    if (n != 0)
                    {
                      localObject8 = localObject1;
                      break label531;
                    }
                  }
                  n = localc.a((char[])localObject7, m);
                  if (n != 0)
                  {
                    localObject8 = localObject1;
                    localObject1 = null;
                    localObject7 = ((String)localObject2).substring(0, m);
                    m += n;
                    localObject6 = ((String)localObject2).substring(m);
                    localObject2 = localObject7;
                    break label537;
                  }
                  localObject8 = localObject1;
                  m += 1;
                }
                localObject8 = localObject1;
              }
              else
              {
                localObject8 = localObject1;
                i12 = n;
                f2 = f1;
              }
              label531:
              m = 0;
              Object localObject6 = null;
              label537:
              if (localObject3 == null)
              {
                localObject1 = new java/util/ArrayList;
                ((ArrayList)localObject1).<init>();
                localObject7 = new java/lang/String;
                ((String)localObject7).<init>((char[])localObject4, j, k);
                ((List)localObject1).add(localObject7);
              }
              else
              {
                localObject1 = localObject3;
              }
              a((String)localObject2, (List)localObject1);
              ((List)localObject1).add(localObject2);
              localObject2 = locald.a((String)localObject2);
              if (localObject2 == null) {
                localObject2 = localObject6;
              }
              if (localObject2 != null)
              {
                m = ((String)localObject2).length();
                locala.a(i4, i6, (String)localObject2);
                i11 = locald.a(locala, i4, m, (List)localObject1) + m;
                m = i6 - i4;
                i11 -= m;
                i6 += i11;
                i1 += i11;
                i2 += i11;
                localObject2 = a;
                localObject4 = localObject2;
                i4 = i6;
                i3 = 1;
              }
              else
              {
                i4 = i6;
              }
              int i11 = ((List)localObject1).size() - 1;
              ((List)localObject1).remove(i11);
              localObject3 = localObject1;
              localObject2 = localObject5;
              m = i10;
              n = i12;
              f1 = f2;
              localObject1 = localObject8;
              break;
            }
            Object localObject8 = localObject1;
            Object localObject5 = localObject2;
            i10 = m;
            i12 = n;
            f2 = f1;
            i8 += -1;
            i6 += i9;
          }
        }
        label806:
        int i10 = m;
        i4 = i6;
      }
    }
    int i12 = n;
    float f2 = f1;
    m = 1;
    if (n != 0)
    {
      if (i3 != 0) {
        return m;
      }
      return 0;
    }
    return i2;
  }
  
  private String a(String paramString)
  {
    b localb = h;
    if (localb == null) {
      return null;
    }
    return localb.a(paramString);
  }
  
  private static void a(String paramString, List paramList)
  {
    boolean bool = paramList.contains(paramString);
    if (!bool) {
      return;
    }
    paramString = new org/c/a/a/a/c/a;
    paramString.<init>(256);
    paramString.a("Infinite loop in property interpolation of ");
    String str = (String)paramList.remove(0);
    paramString.a(str);
    paramString.a(": ");
    paramString.a(paramList, "->");
    paramList = new java/lang/IllegalStateException;
    paramString = paramString.toString();
    paramList.<init>(paramString);
    throw paramList;
  }
  
  public final boolean a(a parama, int paramInt)
  {
    int j = a(parama, 0, paramInt, null);
    return j > 0;
  }
}

/* Location:
 * Qualified Name:     org.c.a.a.a.c.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */