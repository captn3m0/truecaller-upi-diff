package org.c.a.a.a.c;

import java.util.Arrays;

final class c$b
  extends c
{
  private final char[] a;
  
  c$b(char[] paramArrayOfChar)
  {
    paramArrayOfChar = (char[])paramArrayOfChar.clone();
    a = paramArrayOfChar;
    Arrays.sort(a);
  }
  
  public final int a(char[] paramArrayOfChar, int paramInt1, int paramInt2)
  {
    char[] arrayOfChar = a;
    char c = paramArrayOfChar[paramInt1];
    int i = Arrays.binarySearch(arrayOfChar, c);
    if (i >= 0) {
      return 1;
    }
    return 0;
  }
}

/* Location:
 * Qualified Name:     org.c.a.a.a.c.c.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */