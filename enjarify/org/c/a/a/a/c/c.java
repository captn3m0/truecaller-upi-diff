package org.c.a.a.a.c;

import org.c.a.a.a.k;

public abstract class c
{
  private static final c a;
  private static final c b;
  private static final c c;
  private static final c d;
  private static final c e;
  private static final c f;
  private static final c g;
  private static final c h;
  private static final c i;
  
  static
  {
    Object localObject = new org/c/a/a/a/c/c$a;
    ((c.a)localObject).<init>(',');
    a = (c)localObject;
    localObject = new org/c/a/a/a/c/c$a;
    ((c.a)localObject).<init>('\t');
    b = (c)localObject;
    localObject = new org/c/a/a/a/c/c$a;
    ((c.a)localObject).<init>(' ');
    c = (c)localObject;
    localObject = new org/c/a/a/a/c/c$b;
    char[] arrayOfChar = " \t\n\r\f".toCharArray();
    ((c.b)localObject).<init>(arrayOfChar);
    d = (c)localObject;
    localObject = new org/c/a/a/a/c/c$e;
    ((c.e)localObject).<init>();
    e = (c)localObject;
    localObject = new org/c/a/a/a/c/c$a;
    ((c.a)localObject).<init>('\'');
    f = (c)localObject;
    localObject = new org/c/a/a/a/c/c$a;
    ((c.a)localObject).<init>('"');
    g = (c)localObject;
    localObject = new org/c/a/a/a/c/c$b;
    arrayOfChar = "'\"".toCharArray();
    ((c.b)localObject).<init>(arrayOfChar);
    h = (c)localObject;
    localObject = new org/c/a/a/a/c/c$c;
    ((c.c)localObject).<init>();
    i = (c)localObject;
  }
  
  public static c a(String paramString)
  {
    boolean bool = k.b(paramString);
    if (bool) {
      return i;
    }
    c.d locald = new org/c/a/a/a/c/c$d;
    locald.<init>(paramString);
    return locald;
  }
  
  public final int a(char[] paramArrayOfChar, int paramInt)
  {
    int j = paramArrayOfChar.length;
    return a(paramArrayOfChar, paramInt, j);
  }
  
  public abstract int a(char[] paramArrayOfChar, int paramInt1, int paramInt2);
}

/* Location:
 * Qualified Name:     org.c.a.a.a.c.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */