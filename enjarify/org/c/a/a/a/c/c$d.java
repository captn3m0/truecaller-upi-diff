package org.c.a.a.a.c;

final class c$d
  extends c
{
  private final char[] a;
  
  c$d(String paramString)
  {
    paramString = paramString.toCharArray();
    a = paramString;
  }
  
  public final int a(char[] paramArrayOfChar, int paramInt1, int paramInt2)
  {
    char[] arrayOfChar1 = a;
    int i = arrayOfChar1.length;
    int j = paramInt1 + i;
    if (j > paramInt2) {
      return 0;
    }
    paramInt2 = paramInt1;
    paramInt1 = 0;
    for (;;)
    {
      char[] arrayOfChar2 = a;
      int k = arrayOfChar2.length;
      if (paramInt1 >= k) {
        break;
      }
      j = arrayOfChar2[paramInt1];
      k = paramArrayOfChar[paramInt2];
      if (j != k) {
        return 0;
      }
      paramInt1 += 1;
      paramInt2 += 1;
    }
    return i;
  }
}

/* Location:
 * Qualified Name:     org.c.a.a.a.c.c.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */