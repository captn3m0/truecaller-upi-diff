package org.c.a.a.a.c;

import java.util.Map;

final class b$a
  extends b
{
  private final Map a;
  
  b$a(Map paramMap)
  {
    a = paramMap;
  }
  
  public final String a(String paramString)
  {
    Map localMap = a;
    if (localMap == null) {
      return null;
    }
    paramString = localMap.get(paramString);
    if (paramString == null) {
      return null;
    }
    return paramString.toString();
  }
}

/* Location:
 * Qualified Name:     org.c.a.a.a.c.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */