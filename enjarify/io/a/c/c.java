package io.a.c;

public final class c
{
  public static Object a(Object paramObject1, Object paramObject2)
  {
    if (paramObject1 != null) {
      return paramObject1;
    }
    paramObject1 = new java/lang/NullPointerException;
    paramObject2 = String.valueOf(paramObject2);
    ((NullPointerException)paramObject1).<init>((String)paramObject2);
    throw ((Throwable)paramObject1);
  }
  
  private static String a(String paramString, Object... paramVarArgs)
  {
    if (paramVarArgs == null) {
      return paramString;
    }
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    int i = paramString.length();
    int j = paramVarArgs.length * 16;
    i += j;
    localStringBuilder.<init>(i);
    i = 0;
    Object localObject = null;
    j = 0;
    for (;;)
    {
      k = paramVarArgs.length;
      if (i >= k) {
        break;
      }
      String str = "%s";
      k = paramString.indexOf(str, j);
      int m = -1;
      if (k == m) {
        break;
      }
      localStringBuilder.append(paramString, j, k);
      j = i + 1;
      localObject = paramVarArgs[i];
      localStringBuilder.append(localObject);
      i = k + 2;
      int n = j;
      j = i;
      i = n;
    }
    int k = paramString.length();
    localStringBuilder.append(paramString, j, k);
    int i1 = paramVarArgs.length;
    if (i < i1)
    {
      paramString = " [";
      localStringBuilder.append(paramString);
      i1 = i + 1;
      localObject = paramVarArgs[i];
      localStringBuilder.append(localObject);
      for (;;)
      {
        i = paramVarArgs.length;
        if (i1 >= i) {
          break;
        }
        localObject = ", ";
        localStringBuilder.append((String)localObject);
        i = i1 + 1;
        paramString = paramVarArgs[i1];
        localStringBuilder.append(paramString);
        i1 = i;
      }
      i1 = 93;
      localStringBuilder.append(i1);
    }
    return localStringBuilder.toString();
  }
  
  public static void a(boolean paramBoolean, Object paramObject)
  {
    if (paramBoolean) {
      return;
    }
    IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
    paramObject = String.valueOf(paramObject);
    localIllegalArgumentException.<init>((String)paramObject);
    throw localIllegalArgumentException;
  }
  
  public static void a(boolean paramBoolean, String paramString, Object... paramVarArgs)
  {
    if (paramBoolean) {
      return;
    }
    IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
    paramString = a(paramString, paramVarArgs);
    localIllegalArgumentException.<init>(paramString);
    throw localIllegalArgumentException;
  }
  
  public static boolean b(Object paramObject1, Object paramObject2)
  {
    if (paramObject1 == null) {
      return paramObject2 == null;
    }
    return paramObject1.equals(paramObject2);
  }
}

/* Location:
 * Qualified Name:     io.a.c.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */