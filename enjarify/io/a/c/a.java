package io.a.c;

import java.lang.reflect.Constructor;
import java.util.ServiceConfigurationError;

public final class a
{
  public static Object a(Class paramClass1, Class paramClass2)
  {
    try
    {
      paramClass2 = paramClass1.asSubclass(paramClass2);
      localObject1 = null;
      localObject2 = new Class[0];
      paramClass2 = paramClass2.getConstructor((Class[])localObject2);
      localObject1 = new Object[0];
      return paramClass2.newInstance((Object[])localObject1);
    }
    catch (Exception paramClass2)
    {
      Object localObject1 = new java/util/ServiceConfigurationError;
      Object localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("Provider ");
      paramClass1 = paramClass1.getName();
      ((StringBuilder)localObject2).append(paramClass1);
      ((StringBuilder)localObject2).append(" could not be instantiated.");
      paramClass1 = ((StringBuilder)localObject2).toString();
      ((ServiceConfigurationError)localObject1).<init>(paramClass1, paramClass2);
      throw ((Throwable)localObject1);
    }
  }
}

/* Location:
 * Qualified Name:     io.a.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */