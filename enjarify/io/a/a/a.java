package io.a.a;

final class a
  extends c
{
  private final long a = 0L;
  private final int b = 0;
  
  public final long a()
  {
    return a;
  }
  
  public final int b()
  {
    return b;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (paramObject == this) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof c;
    if (bool2)
    {
      paramObject = (c)paramObject;
      long l1 = a;
      long l2 = ((c)paramObject).a();
      bool2 = l1 < l2;
      if (!bool2)
      {
        int i = b;
        int j = ((c)paramObject).b();
        if (i == j) {
          return bool1;
        }
      }
      return false;
    }
    return false;
  }
  
  public final int hashCode()
  {
    long l1 = a;
    long l2 = l1 >>> 32;
    int i = (int)(l1 ^ l2 ^ 0xF4243) * 1000003;
    return b ^ i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Timestamp{seconds=");
    long l = a;
    localStringBuilder.append(l);
    localStringBuilder.append(", nanos=");
    int i = b;
    localStringBuilder.append(i);
    localStringBuilder.append("}");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     io.a.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */