package io.a.a;

import java.math.BigInteger;

final class b
{
  private static final BigInteger a = BigInteger.valueOf(Long.MAX_VALUE);
  private static final BigInteger b = BigInteger.valueOf(Long.MIN_VALUE);
  
  static int a(long paramLong1, long paramLong2)
  {
    boolean bool = paramLong1 < paramLong2;
    if (bool) {
      return -1;
    }
    bool = paramLong1 < paramLong2;
    if (!bool) {
      return 0;
    }
    return 1;
  }
}

/* Location:
 * Qualified Name:     io.a.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */