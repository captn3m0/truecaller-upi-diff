package io.a.b.a.a;

import io.a.d.c.a;
import io.a.d.c.b;
import io.a.e.g;

public final class a
{
  public static final c.b A = n;
  public static final c.b B = o;
  public static final c.a C = c.a.a("grpc.io/server/sent_bytes_per_rpc", "Total bytes sent across all response messages per RPC", "By");
  public static final c.a D = c.a.a("grpc.io/server/received_bytes_per_rpc", "Total bytes received across all messages per RPC", "By");
  public static final c.a E = c.a.a("grpc.io/server/sent_bytes_per_method", "Total bytes sent per method, recorded real-time as bytes are sent.", "By");
  public static final c.a F = c.a.a("grpc.io/server/received_bytes_per_method", "Total bytes received per method, recorded real-time as bytes are received.", "By");
  public static final c.b G = c.b.a("grpc.io/server/sent_messages_per_method", "Total messages sent per method.", "1");
  public static final c.b H = c.b.a("grpc.io/server/received_messages_per_method", "Total messages received per method.", "1");
  public static final c.b I = c.b.a("grpc.io/server/sent_messages_per_rpc", "Number of messages sent in each RPC", "1");
  public static final c.b J = c.b.a("grpc.io/server/received_messages_per_rpc", "Number of messages received in each RPC", "1");
  public static final c.a K = c.a.a("grpc.io/server/server_latency", "Time between first byte of request received to last byte of response sent, or terminal error.", "ms");
  public static final c.b L = c.b.a("grpc.io/server/started_rpcs", "Number of started server RPCs.", "1");
  public static final c.b M = c.b.a("grpc.io/server/error_count", "RPC Errors", "1");
  public static final c.a N = D;
  public static final c.a O = C;
  public static final c.a P = c.a.a("grpc.io/server/server_elapsed_time", "Server elapsed time in msecs", "ms");
  public static final c.a Q = K;
  public static final c.a R = c.a.a("grpc.io/server/uncompressed_request_bytes", "Uncompressed Request bytes", "By");
  public static final c.a S = c.a.a("grpc.io/server/uncompressed_response_bytes", "Uncompressed Response bytes", "By");
  public static final c.b T = L;
  public static final c.b U = c.b.a("grpc.io/server/finished_count", "Number of server RPCs (streams) finished", "1");
  public static final c.b V = J;
  public static final c.b W = I;
  public static final g a = g.a("canonical_status");
  public static final g b = g.a("method");
  public static final g c = g.a("grpc_client_status");
  public static final g d = g.a("grpc_server_status");
  public static final g e = g.a("grpc_client_method");
  public static final g f = g.a("grpc_server_method");
  public static final c.a g = c.a.a("grpc.io/client/sent_bytes_per_rpc", "Total bytes sent across all request messages per RPC", "By");
  public static final c.a h = c.a.a("grpc.io/client/received_bytes_per_rpc", "Total bytes received across all response messages per RPC", "By");
  public static final c.a i = c.a.a("grpc.io/client/sent_bytes_per_method", "Total bytes sent per method, recorded real-time as bytes are sent.", "By");
  public static final c.a j = c.a.a("grpc.io/client/received_bytes_per_method", "Total bytes received per method, recorded real-time as bytes are received.", "By");
  public static final c.b k = c.b.a("grpc.io/client/sent_messages_per_method", "Total messages sent per method.", "1");
  public static final c.b l = c.b.a("grpc.io/client/received_messages_per_method", "Total messages received per method.", "1");
  public static final c.a m = c.a.a("grpc.io/client/roundtrip_latency", "Time between first byte of request sent to last byte of response received, or terminal error.", "ms");
  public static final c.b n = c.b.a("grpc.io/client/sent_messages_per_rpc", "Number of messages sent in the RPC", "1");
  public static final c.b o = c.b.a("grpc.io/client/received_messages_per_rpc", "Number of response messages received per RPC", "1");
  public static final c.a p = c.a.a("grpc.io/client/server_latency", "Server latency in msecs", "ms");
  public static final c.b q = c.b.a("grpc.io/client/started_rpcs", "Number of started client RPCs.", "1");
  public static final c.b r = c.b.a("grpc.io/client/error_count", "RPC Errors", "1");
  public static final c.a s = g;
  public static final c.a t = h;
  public static final c.a u = m;
  public static final c.a v = p;
  public static final c.a w = c.a.a("grpc.io/client/uncompressed_request_bytes", "Uncompressed Request bytes", "By");
  public static final c.a x = c.a.a("grpc.io/client/uncompressed_response_bytes", "Uncompressed Response bytes", "By");
  public static final c.b y = q;
  public static final c.b z = c.b.a("grpc.io/client/finished_count", "Number of client RPCs (streams) finished", "1");
}

/* Location:
 * Qualified Name:     io.a.b.a.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */