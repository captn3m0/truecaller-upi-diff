package io.a.e;

import java.util.HashMap;
import java.util.Iterator;

public abstract class e
{
  protected abstract Iterator a();
  
  public boolean equals(Object paramObject)
  {
    boolean bool1 = paramObject instanceof e;
    if (!bool1) {
      return false;
    }
    paramObject = (e)paramObject;
    Object localObject1 = a();
    paramObject = ((e)paramObject).a();
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    int i;
    boolean bool2;
    Object localObject2;
    for (;;)
    {
      i = 1;
      if (localObject1 == null) {
        break;
      }
      bool2 = ((Iterator)localObject1).hasNext();
      if (!bool2) {
        break;
      }
      localObject2 = (d)((Iterator)localObject1).next();
      boolean bool3 = localHashMap.containsKey(localObject2);
      Integer localInteger2;
      if (bool3)
      {
        Integer localInteger1 = (Integer)localHashMap.get(localObject2);
        int k = localInteger1.intValue() + i;
        localInteger2 = Integer.valueOf(k);
        localHashMap.put(localObject2, localInteger2);
      }
      else
      {
        localInteger2 = Integer.valueOf(i);
        localHashMap.put(localObject2, localInteger2);
      }
    }
    while (paramObject != null)
    {
      bool1 = ((Iterator)paramObject).hasNext();
      if (!bool1) {
        break;
      }
      localObject1 = (d)((Iterator)paramObject).next();
      bool2 = localHashMap.containsKey(localObject1);
      if (!bool2) {
        return false;
      }
      localObject2 = (Integer)localHashMap.get(localObject1);
      int j = ((Integer)localObject2).intValue();
      if (j > i)
      {
        j += -1;
        localObject2 = Integer.valueOf(j);
        localHashMap.put(localObject1, localObject2);
      }
      else
      {
        localHashMap.remove(localObject1);
      }
    }
    return localHashMap.isEmpty();
  }
  
  public final int hashCode()
  {
    Iterator localIterator = a();
    int i = 0;
    if (localIterator == null) {
      return 0;
    }
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      d locald = (d)localIterator.next();
      if (locald != null)
      {
        int j = locald.hashCode();
        i += j;
      }
    }
    return i;
  }
  
  public String toString()
  {
    return "TagContext";
  }
}

/* Location:
 * Qualified Name:     io.a.e.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */