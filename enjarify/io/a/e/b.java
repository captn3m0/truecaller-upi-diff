package io.a.e;

final class b
  extends h
{
  private final String a;
  
  b(String paramString)
  {
    if (paramString != null)
    {
      a = paramString;
      return;
    }
    paramString = new java/lang/NullPointerException;
    paramString.<init>("Null asString");
    throw paramString;
  }
  
  public final String a()
  {
    return a;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {
      return true;
    }
    boolean bool = paramObject instanceof h;
    if (bool)
    {
      paramObject = (h)paramObject;
      String str = a;
      paramObject = ((h)paramObject).a();
      return str.equals(paramObject);
    }
    return false;
  }
  
  public final int hashCode()
  {
    return a.hashCode() ^ 0xF4243;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("TagValue{asString=");
    String str = a;
    localStringBuilder.append(str);
    localStringBuilder.append("}");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     io.a.e.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */