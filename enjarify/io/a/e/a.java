package io.a.e;

final class a
  extends g
{
  private final String a;
  
  a(String paramString)
  {
    if (paramString != null)
    {
      a = paramString;
      return;
    }
    paramString = new java/lang/NullPointerException;
    paramString.<init>("Null name");
    throw paramString;
  }
  
  public final String a()
  {
    return a;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {
      return true;
    }
    boolean bool = paramObject instanceof g;
    if (bool)
    {
      paramObject = (g)paramObject;
      String str = a;
      paramObject = ((g)paramObject).a();
      return str.equals(paramObject);
    }
    return false;
  }
  
  public final int hashCode()
  {
    return a.hashCode() ^ 0xF4243;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("TagKey{name=");
    String str = a;
    localStringBuilder.append(str);
    localStringBuilder.append("}");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     io.a.e.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */