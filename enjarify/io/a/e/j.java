package io.a.e;

import io.a.c.a;
import io.a.e.a.d;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class j
{
  private static final Logger a = Logger.getLogger(j.class.getName());
  private static final k b = a(k.class.getClassLoader());
  
  public static i a()
  {
    return b.a();
  }
  
  private static k a(ClassLoader paramClassLoader)
  {
    boolean bool = true;
    Object localObject1 = "io.opencensus.impl.tags.TagsComponentImpl";
    try
    {
      localObject1 = Class.forName((String)localObject1, bool, paramClassLoader);
      localObject3 = k.class;
      localObject1 = a.a((Class)localObject1, (Class)localObject3);
      return (k)localObject1;
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      Object localObject3 = a;
      Level localLevel = Level.FINE;
      String str = "Couldn't load full implementation for TagsComponent, now trying to load lite implementation.";
      ((Logger)localObject3).log(localLevel, str, localClassNotFoundException);
      Object localObject2 = "io.opencensus.impllite.tags.TagsComponentImplLite";
      try
      {
        paramClassLoader = Class.forName((String)localObject2, bool, paramClassLoader);
        localObject4 = k.class;
        paramClassLoader = a.a(paramClassLoader, (Class)localObject4);
        return (k)paramClassLoader;
      }
      catch (ClassNotFoundException paramClassLoader)
      {
        Object localObject4 = a;
        localObject2 = Level.FINE;
        ((Logger)localObject4).log((Level)localObject2, "Couldn't load lite implementation for TagsComponent, now using default implementation for TagsComponent.", paramClassLoader);
        paramClassLoader = new io/a/e/c$f;
        paramClassLoader.<init>((byte)0);
      }
    }
    return paramClassLoader;
  }
  
  public static d b()
  {
    return b.b();
  }
}

/* Location:
 * Qualified Name:     io.a.e.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */