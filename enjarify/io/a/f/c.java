package io.a.f;

final class c
  extends j
{
  private final io.a.a.c a;
  private final j.b b;
  private final long c;
  private final long d;
  private final long e;
  
  private c(io.a.a.c paramc, j.b paramb, long paramLong1, long paramLong2, long paramLong3)
  {
    a = paramc;
    b = paramb;
    c = paramLong1;
    d = paramLong2;
    e = paramLong3;
  }
  
  public final io.a.a.c a()
  {
    return a;
  }
  
  public final j.b b()
  {
    return b;
  }
  
  public final long c()
  {
    return c;
  }
  
  public final long d()
  {
    return d;
  }
  
  public final long e()
  {
    return e;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (paramObject == this) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof j;
    if (bool2)
    {
      paramObject = (j)paramObject;
      Object localObject1 = a;
      if (localObject1 == null)
      {
        localObject1 = ((j)paramObject).a();
        if (localObject1 != null) {
          break label160;
        }
      }
      else
      {
        localObject2 = ((j)paramObject).a();
        bool2 = localObject1.equals(localObject2);
        if (!bool2) {
          break label160;
        }
      }
      localObject1 = b;
      Object localObject2 = ((j)paramObject).b();
      bool2 = ((j.b)localObject1).equals(localObject2);
      if (bool2)
      {
        long l1 = c;
        long l2 = ((j)paramObject).c();
        bool2 = l1 < l2;
        if (!bool2)
        {
          l1 = d;
          l2 = ((j)paramObject).d();
          bool2 = l1 < l2;
          if (!bool2)
          {
            l1 = e;
            l2 = ((j)paramObject).e();
            boolean bool3 = l1 < l2;
            if (!bool3) {
              return bool1;
            }
          }
        }
      }
      label160:
      return false;
    }
    return false;
  }
  
  public final int hashCode()
  {
    io.a.a.c localc = a;
    if (localc == null)
    {
      i = 0;
      localc = null;
    }
    else
    {
      i = localc.hashCode();
    }
    int j = 1000003;
    int i = (i ^ j) * j;
    int k = b.hashCode();
    long l1 = (i ^ k) * j;
    long l2 = c;
    i = 32;
    long l3 = l2 >>> i;
    l2 ^= l3;
    l1 = (int)(l1 ^ l2) * j;
    l2 = d;
    l3 = l2 >>> i;
    l2 ^= l3;
    long l4 = (int)(l1 ^ l2) * j;
    long l5 = e;
    long l6 = l5 >>> i;
    l5 ^= l6;
    return (int)(l4 ^ l5);
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("NetworkEvent{kernelTimestamp=");
    Object localObject = a;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", type=");
    localObject = b;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", messageId=");
    long l = c;
    localStringBuilder.append(l);
    localStringBuilder.append(", uncompressedMessageSize=");
    l = d;
    localStringBuilder.append(l);
    localStringBuilder.append(", compressedMessageSize=");
    l = e;
    localStringBuilder.append(l);
    localStringBuilder.append("}");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     io.a.f.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */