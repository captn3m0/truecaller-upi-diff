package io.a.f;

import java.util.Collections;
import java.util.List;

public abstract class t
{
  static t a(List paramList)
  {
    int i = paramList.size();
    int j = 32;
    if (i <= j)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject = null;
    }
    if (i != 0)
    {
      localObject = new io/a/f/d;
      paramList = Collections.unmodifiableList(paramList);
      ((d)localObject).<init>(paramList);
      return (t)localObject;
    }
    paramList = new java/lang/IllegalStateException;
    Object localObject = String.valueOf("Invalid size");
    paramList.<init>((String)localObject);
    throw paramList;
  }
  
  public abstract List a();
}

/* Location:
 * Qualified Name:     io.a.f.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */