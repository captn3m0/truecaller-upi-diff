package io.a.f;

import io.a.c.a;
import io.a.f.b.b;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class u
{
  private static final Logger a = Logger.getLogger(u.class.getName());
  private static final p b = a(p.class.getClassLoader());
  
  private static p a(ClassLoader paramClassLoader)
  {
    boolean bool = true;
    Object localObject1 = "io.opencensus.impl.trace.TraceComponentImpl";
    try
    {
      localObject1 = Class.forName((String)localObject1, bool, paramClassLoader);
      localObject3 = p.class;
      localObject1 = a.a((Class)localObject1, (Class)localObject3);
      return (p)localObject1;
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      Object localObject3 = a;
      Level localLevel = Level.FINE;
      String str = "Couldn't load full implementation for TraceComponent, now trying to load lite implementation.";
      ((Logger)localObject3).log(localLevel, str, localClassNotFoundException);
      Object localObject2 = "io.opencensus.impllite.trace.TraceComponentImplLite";
      try
      {
        paramClassLoader = Class.forName((String)localObject2, bool, paramClassLoader);
        localObject4 = p.class;
        paramClassLoader = a.a(paramClassLoader, (Class)localObject4);
        return (p)paramClassLoader;
      }
      catch (ClassNotFoundException paramClassLoader)
      {
        Object localObject4 = a;
        localObject2 = Level.FINE;
        ((Logger)localObject4).log((Level)localObject2, "Couldn't load lite implementation for TraceComponent, now using default implementation for TraceComponent.", paramClassLoader);
      }
    }
    return p.c();
  }
  
  public static s a()
  {
    return b.a();
  }
  
  public static b b()
  {
    return b.b();
  }
}

/* Location:
 * Qualified Name:     io.a.f.u
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */