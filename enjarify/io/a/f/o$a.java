package io.a.f;

import java.util.List;

public enum o$a
{
  final int r;
  
  static
  {
    Object localObject = new io/a/f/o$a;
    ((a)localObject).<init>("OK", 0, 0);
    a = (a)localObject;
    localObject = new io/a/f/o$a;
    int i1 = 1;
    ((a)localObject).<init>("CANCELLED", i1, i1);
    b = (a)localObject;
    localObject = new io/a/f/o$a;
    int i2 = 2;
    ((a)localObject).<init>("UNKNOWN", i2, i2);
    c = (a)localObject;
    localObject = new io/a/f/o$a;
    int i3 = 3;
    ((a)localObject).<init>("INVALID_ARGUMENT", i3, i3);
    d = (a)localObject;
    localObject = new io/a/f/o$a;
    int i4 = 4;
    ((a)localObject).<init>("DEADLINE_EXCEEDED", i4, i4);
    e = (a)localObject;
    localObject = new io/a/f/o$a;
    int i5 = 5;
    ((a)localObject).<init>("NOT_FOUND", i5, i5);
    f = (a)localObject;
    localObject = new io/a/f/o$a;
    int i6 = 6;
    ((a)localObject).<init>("ALREADY_EXISTS", i6, i6);
    g = (a)localObject;
    localObject = new io/a/f/o$a;
    int i7 = 7;
    ((a)localObject).<init>("PERMISSION_DENIED", i7, i7);
    h = (a)localObject;
    localObject = new io/a/f/o$a;
    int i8 = 8;
    ((a)localObject).<init>("RESOURCE_EXHAUSTED", i8, i8);
    i = (a)localObject;
    localObject = new io/a/f/o$a;
    int i9 = 9;
    ((a)localObject).<init>("FAILED_PRECONDITION", i9, i9);
    j = (a)localObject;
    localObject = new io/a/f/o$a;
    int i10 = 10;
    ((a)localObject).<init>("ABORTED", i10, i10);
    k = (a)localObject;
    localObject = new io/a/f/o$a;
    int i11 = 11;
    ((a)localObject).<init>("OUT_OF_RANGE", i11, i11);
    l = (a)localObject;
    localObject = new io/a/f/o$a;
    int i12 = 12;
    ((a)localObject).<init>("UNIMPLEMENTED", i12, i12);
    m = (a)localObject;
    localObject = new io/a/f/o$a;
    int i13 = 13;
    ((a)localObject).<init>("INTERNAL", i13, i13);
    n = (a)localObject;
    localObject = new io/a/f/o$a;
    i13 = 14;
    ((a)localObject).<init>("UNAVAILABLE", i13, i13);
    o = (a)localObject;
    localObject = new io/a/f/o$a;
    ((a)localObject).<init>("DATA_LOSS", 15, 15);
    p = (a)localObject;
    localObject = new io/a/f/o$a;
    ((a)localObject).<init>("UNAUTHENTICATED", 16, 16);
    q = (a)localObject;
    localObject = new a[17];
    a locala = a;
    localObject[0] = locala;
    locala = b;
    localObject[i1] = locala;
    locala = c;
    localObject[i2] = locala;
    locala = d;
    localObject[i3] = locala;
    locala = e;
    localObject[i4] = locala;
    locala = f;
    localObject[i5] = locala;
    locala = g;
    localObject[i6] = locala;
    locala = h;
    localObject[i7] = locala;
    locala = i;
    localObject[i8] = locala;
    locala = j;
    localObject[i9] = locala;
    locala = k;
    localObject[i10] = locala;
    locala = l;
    localObject[i11] = locala;
    locala = m;
    localObject[12] = locala;
    locala = n;
    localObject[13] = locala;
    locala = o;
    localObject[14] = locala;
    locala = p;
    localObject[15] = locala;
    locala = q;
    localObject[16] = locala;
    s = (a[])localObject;
  }
  
  private o$a(int paramInt1)
  {
    r = paramInt1;
  }
  
  public final o a()
  {
    List localList = o.a();
    int i1 = r;
    return (o)localList.get(i1);
  }
}

/* Location:
 * Qualified Name:     io.a.f.o.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */