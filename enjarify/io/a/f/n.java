package io.a.f;

public final class n
  implements Comparable
{
  public static final n a;
  private final long b = 0L;
  
  static
  {
    n localn = new io/a/f/n;
    localn.<init>();
    a = localn;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (paramObject == this) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof n;
    if (!bool2) {
      return false;
    }
    paramObject = (n)paramObject;
    long l1 = b;
    long l2 = b;
    boolean bool3 = l1 < l2;
    if (!bool3) {
      return bool1;
    }
    return false;
  }
  
  public final int hashCode()
  {
    long l1 = b;
    long l2 = l1 >>> 32;
    return (int)(l1 ^ l2);
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("SpanId{spanId=");
    char[] arrayOfChar = new char[16];
    f.a(b, arrayOfChar, 0);
    String str = new java/lang/String;
    str.<init>(arrayOfChar);
    localStringBuilder.append(str);
    localStringBuilder.append("}");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     io.a.f.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */