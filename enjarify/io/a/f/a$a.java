package io.a.f;

final class a$a
  extends h.a
{
  private Boolean a;
  private o b;
  
  public final h.a a(o paramo)
  {
    b = paramo;
    return this;
  }
  
  public final h.a a(boolean paramBoolean)
  {
    Boolean localBoolean = Boolean.valueOf(paramBoolean);
    a = localBoolean;
    return this;
  }
  
  public final h a()
  {
    Object localObject1 = "";
    Object localObject2 = a;
    if (localObject2 == null)
    {
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      ((StringBuilder)localObject2).append((String)localObject1);
      ((StringBuilder)localObject2).append(" sampleToLocalSpanStore");
      localObject1 = ((StringBuilder)localObject2).toString();
    }
    boolean bool = ((String)localObject1).isEmpty();
    if (bool)
    {
      localObject1 = new io/a/f/a;
      bool = a.booleanValue();
      o localo = b;
      ((a)localObject1).<init>(bool, localo, (byte)0);
      return (h)localObject1;
    }
    localObject2 = new java/lang/IllegalStateException;
    localObject1 = String.valueOf(localObject1);
    localObject1 = "Missing required properties:".concat((String)localObject1);
    ((IllegalStateException)localObject2).<init>((String)localObject1);
    throw ((Throwable)localObject2);
  }
}

/* Location:
 * Qualified Name:     io.a.f.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */