package io.a.f;

import java.util.Arrays;

final class f
{
  private static final char[] a;
  private static final byte[] b;
  
  static
  {
    int i = 512;
    Object localObject = new char[i];
    int j = 0;
    int k = 0;
    String str1 = null;
    int i1;
    for (;;)
    {
      int n = 256;
      if (k >= n) {
        break;
      }
      String str2 = "0123456789abcdef";
      int i2 = k >>> 4;
      i1 = str2.charAt(i2);
      localObject[k] = i1;
      i1 = k | 0x100;
      String str3 = "0123456789abcdef";
      int i3 = k & 0xF;
      i2 = str3.charAt(i3);
      localObject[i1] = i2;
      k += 1;
    }
    a = (char[])localObject;
    i = 128;
    localObject = new byte[i];
    k = -1;
    Arrays.fill((byte[])localObject, k);
    for (;;)
    {
      int m = 16;
      if (j >= m) {
        break;
      }
      str1 = "0123456789abcdef";
      m = str1.charAt(j);
      i1 = (byte)j;
      localObject[m] = i1;
      j += 1;
    }
    b = (byte[])localObject;
  }
  
  private static void a(byte paramByte, char[] paramArrayOfChar, int paramInt)
  {
    paramByte &= 0xFF;
    char[] arrayOfChar = a;
    int i = arrayOfChar[paramByte];
    paramArrayOfChar[paramInt] = i;
    paramInt += 1;
    paramByte |= 0x100;
    paramByte = arrayOfChar[paramByte];
    paramArrayOfChar[paramInt] = paramByte;
  }
  
  static void a(long paramLong, char[] paramArrayOfChar, int paramInt)
  {
    long l1 = paramLong >> 56;
    long l2 = 255L;
    a((byte)(int)(l1 & l2), paramArrayOfChar, paramInt);
    byte b1 = (byte)(int)(paramLong >> 48 & l2);
    int i = paramInt + 2;
    a(b1, paramArrayOfChar, i);
    b1 = (byte)(int)(paramLong >> 40 & l2);
    i = paramInt + 4;
    a(b1, paramArrayOfChar, i);
    b1 = (byte)(int)(paramLong >> 32 & l2);
    i = paramInt + 6;
    a(b1, paramArrayOfChar, i);
    b1 = (byte)(int)(paramLong >> 24 & l2);
    i = paramInt + 8;
    a(b1, paramArrayOfChar, i);
    b1 = (byte)(int)(paramLong >> 16 & l2);
    i = paramInt + 10;
    a(b1, paramArrayOfChar, i);
    b1 = (byte)(int)(paramLong >> 8 & l2);
    i = paramInt + 12;
    a(b1, paramArrayOfChar, i);
    byte b2 = (byte)(int)(paramLong & l2);
    paramInt += 14;
    a(b2, paramArrayOfChar, paramInt);
  }
}

/* Location:
 * Qualified Name:     io.a.f.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */