package io.a.f;

public final class q
  implements Comparable
{
  public static final q a;
  private final long b;
  private final long c;
  
  static
  {
    q localq = new io/a/f/q;
    localq.<init>();
    a = localq;
  }
  
  private q()
  {
    long l = 0L;
    b = l;
    c = l;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (paramObject == this) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof q;
    if (!bool2) {
      return false;
    }
    paramObject = (q)paramObject;
    long l1 = b;
    long l2 = b;
    bool2 = l1 < l2;
    if (!bool2)
    {
      l1 = c;
      l2 = c;
      boolean bool3 = l1 < l2;
      if (!bool3) {
        return bool1;
      }
    }
    return false;
  }
  
  public final int hashCode()
  {
    long l1 = b;
    int i = 32;
    long l2 = l1 >>> i;
    int j = ((int)(l1 ^ l2) + 31) * 31;
    l2 = c;
    long l3 = l2 >>> i;
    int k = (int)(l2 ^ l3);
    return j + k;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("TraceId{traceId=");
    char[] arrayOfChar = new char[32];
    f.a(b, arrayOfChar, 0);
    f.a(c, arrayOfChar, 16);
    String str = new java/lang/String;
    str.<init>(arrayOfChar);
    localStringBuilder.append(str);
    localStringBuilder.append("}");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     io.a.f.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */