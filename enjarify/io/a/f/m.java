package io.a.f;

import java.util.Arrays;

public final class m
{
  public static final m b;
  private static final t c;
  final r a;
  private final q d;
  private final n e;
  private final t f;
  
  static
  {
    Object localObject1 = new io/a/f/t$a;
    Object localObject2 = t.a.a();
    n localn = null;
    ((t.a)localObject1).<init>((t)localObject2, (byte)0);
    localObject2 = b;
    if (localObject2 == null) {
      localObject1 = a;
    } else {
      localObject1 = t.a(b);
    }
    c = (t)localObject1;
    localObject1 = new io/a/f/m;
    localObject2 = q.a;
    localn = n.a;
    r localr = r.a;
    t localt = c;
    ((m)localObject1).<init>((q)localObject2, localn, localr, localt);
    b = (m)localObject1;
  }
  
  private m(q paramq, n paramn, r paramr, t paramt)
  {
    d = paramq;
    e = paramn;
    a = paramr;
    f = paramt;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (paramObject == this) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof m;
    if (!bool2) {
      return false;
    }
    paramObject = (m)paramObject;
    Object localObject1 = d;
    Object localObject2 = d;
    bool2 = ((q)localObject1).equals(localObject2);
    if (bool2)
    {
      localObject1 = e;
      localObject2 = e;
      bool2 = ((n)localObject1).equals(localObject2);
      if (bool2)
      {
        localObject1 = a;
        paramObject = a;
        boolean bool3 = ((r)localObject1).equals(paramObject);
        if (bool3) {
          return bool1;
        }
      }
    }
    return false;
  }
  
  public final int hashCode()
  {
    Object[] arrayOfObject = new Object[3];
    Object localObject = d;
    arrayOfObject[0] = localObject;
    localObject = e;
    arrayOfObject[1] = localObject;
    localObject = a;
    arrayOfObject[2] = localObject;
    return Arrays.hashCode(arrayOfObject);
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("SpanContext{traceId=");
    Object localObject = d;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", spanId=");
    localObject = e;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", traceOptions=");
    localObject = a;
    localStringBuilder.append(localObject);
    localStringBuilder.append("}");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     io.a.f.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */