package io.a.f;

import io.a.c.c;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Map;
import java.util.Set;

public abstract class k
{
  private static final Map a = ;
  private static final Set d = Collections.unmodifiableSet(EnumSet.noneOf(k.a.class));
  public final m b;
  private final Set c;
  
  protected k(m paramm)
  {
    Object localObject = (m)c.a(paramm, "context");
    b = ((m)localObject);
    localObject = d;
    c = ((Set)localObject);
    paramm = a;
    boolean bool = paramm.a();
    if (bool)
    {
      paramm = c;
      localObject = k.a.a;
      bool = paramm.contains(localObject);
      if (!bool)
      {
        bool = false;
        paramm = null;
        break label75;
      }
    }
    bool = true;
    label75:
    c.a(bool, "Span is sampled, but does not have RECORD_EVENTS set.");
  }
  
  public abstract void a(h paramh);
  
  public void a(i parami)
  {
    c.a(parami, "messageEvent");
    c.a(parami, "event");
    parami = (i)parami;
    Object localObject = parami.a();
    i.b localb = i.b.b;
    if (localObject == localb) {
      localObject = j.b.b;
    } else {
      localObject = j.b.a;
    }
    long l = parami.b();
    c.a locala = new io/a/f/c$a;
    locala.<init>();
    String str = "type";
    localObject = (j.b)c.a(localObject, str);
    if (localObject != null)
    {
      a = ((j.b)localObject);
      localObject = locala.a(l);
      l = 0L;
      localObject = ((j.a)localObject).b(l).c(l);
      l = parami.c();
      localObject = ((j.a)localObject).b(l);
      l = parami.d();
      parami = ((j.a)localObject).c(l).a();
      a(parami);
      return;
    }
    parami = new java/lang/NullPointerException;
    parami.<init>("Null type");
    throw parami;
  }
  
  public void a(j paramj)
  {
    c.a(paramj, "event");
    paramj = (j)paramj;
    Object localObject = paramj.b();
    j.b localb = j.b.b;
    if (localObject == localb) {
      localObject = i.b.b;
    } else {
      localObject = i.b.a;
    }
    long l = paramj.c();
    localObject = i.a((i.b)localObject, l);
    l = paramj.d();
    localObject = ((i.a)localObject).b(l);
    l = paramj.e();
    paramj = ((i.a)localObject).c(l).a();
    a(paramj);
  }
}

/* Location:
 * Qualified Name:     io.a.f.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */