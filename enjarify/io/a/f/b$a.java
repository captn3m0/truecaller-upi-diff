package io.a.f;

final class b$a
  extends i.a
{
  private i.b a;
  private Long b;
  private Long c;
  private Long d;
  
  final i.a a(long paramLong)
  {
    Long localLong = Long.valueOf(paramLong);
    b = localLong;
    return this;
  }
  
  final i.a a(i.b paramb)
  {
    if (paramb != null)
    {
      a = paramb;
      return this;
    }
    paramb = new java/lang/NullPointerException;
    paramb.<init>("Null type");
    throw paramb;
  }
  
  public final i a()
  {
    Object localObject1 = "";
    Object localObject2 = a;
    if (localObject2 == null)
    {
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      ((StringBuilder)localObject2).append((String)localObject1);
      ((StringBuilder)localObject2).append(" type");
      localObject1 = ((StringBuilder)localObject2).toString();
    }
    localObject2 = b;
    if (localObject2 == null)
    {
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      ((StringBuilder)localObject2).append((String)localObject1);
      ((StringBuilder)localObject2).append(" messageId");
      localObject1 = ((StringBuilder)localObject2).toString();
    }
    localObject2 = c;
    if (localObject2 == null)
    {
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      ((StringBuilder)localObject2).append((String)localObject1);
      ((StringBuilder)localObject2).append(" uncompressedMessageSize");
      localObject1 = ((StringBuilder)localObject2).toString();
    }
    localObject2 = d;
    if (localObject2 == null)
    {
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      ((StringBuilder)localObject2).append((String)localObject1);
      ((StringBuilder)localObject2).append(" compressedMessageSize");
      localObject1 = ((StringBuilder)localObject2).toString();
    }
    boolean bool = ((String)localObject1).isEmpty();
    if (bool)
    {
      localObject1 = new io/a/f/b;
      i.b localb = a;
      long l1 = b.longValue();
      long l2 = c.longValue();
      long l3 = d.longValue();
      ((b)localObject1).<init>(localb, l1, l2, l3, (byte)0);
      return (i)localObject1;
    }
    localObject2 = new java/lang/IllegalStateException;
    localObject1 = String.valueOf(localObject1);
    localObject1 = "Missing required properties:".concat((String)localObject1);
    ((IllegalStateException)localObject2).<init>((String)localObject1);
    throw ((Throwable)localObject2);
  }
  
  public final i.a b(long paramLong)
  {
    Long localLong = Long.valueOf(paramLong);
    c = localLong;
    return this;
  }
  
  public final i.a c(long paramLong)
  {
    Long localLong = Long.valueOf(paramLong);
    d = localLong;
    return this;
  }
}

/* Location:
 * Qualified Name:     io.a.f.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */