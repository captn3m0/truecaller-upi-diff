package io.a.f;

final class c$a
  extends j.a
{
  j.b a;
  private io.a.a.c b;
  private Long c;
  private Long d;
  private Long e;
  
  final j.a a(long paramLong)
  {
    Long localLong = Long.valueOf(paramLong);
    c = localLong;
    return this;
  }
  
  public final j a()
  {
    Object localObject1 = "";
    Object localObject2 = a;
    if (localObject2 == null)
    {
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      ((StringBuilder)localObject2).append((String)localObject1);
      ((StringBuilder)localObject2).append(" type");
      localObject1 = ((StringBuilder)localObject2).toString();
    }
    localObject2 = c;
    if (localObject2 == null)
    {
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      ((StringBuilder)localObject2).append((String)localObject1);
      ((StringBuilder)localObject2).append(" messageId");
      localObject1 = ((StringBuilder)localObject2).toString();
    }
    localObject2 = d;
    if (localObject2 == null)
    {
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      ((StringBuilder)localObject2).append((String)localObject1);
      ((StringBuilder)localObject2).append(" uncompressedMessageSize");
      localObject1 = ((StringBuilder)localObject2).toString();
    }
    localObject2 = e;
    if (localObject2 == null)
    {
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      ((StringBuilder)localObject2).append((String)localObject1);
      ((StringBuilder)localObject2).append(" compressedMessageSize");
      localObject1 = ((StringBuilder)localObject2).toString();
    }
    boolean bool = ((String)localObject1).isEmpty();
    if (bool)
    {
      localObject1 = new io/a/f/c;
      io.a.a.c localc = b;
      j.b localb = a;
      long l1 = c.longValue();
      long l2 = d.longValue();
      long l3 = e.longValue();
      ((c)localObject1).<init>(localc, localb, l1, l2, l3, (byte)0);
      return (j)localObject1;
    }
    localObject2 = new java/lang/IllegalStateException;
    localObject1 = String.valueOf(localObject1);
    localObject1 = "Missing required properties:".concat((String)localObject1);
    ((IllegalStateException)localObject2).<init>((String)localObject1);
    throw ((Throwable)localObject2);
  }
  
  public final j.a b(long paramLong)
  {
    Long localLong = Long.valueOf(paramLong);
    d = localLong;
    return this;
  }
  
  public final j.a c(long paramLong)
  {
    Long localLong = Long.valueOf(paramLong);
    e = localLong;
    return this;
  }
}

/* Location:
 * Qualified Name:     io.a.f.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */