package io.a.f;

final class a
  extends h
{
  private final boolean b;
  private final o c;
  
  private a(boolean paramBoolean, o paramo)
  {
    b = paramBoolean;
    c = paramo;
  }
  
  public final boolean a()
  {
    return b;
  }
  
  public final o b()
  {
    return c;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (paramObject == this) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof h;
    if (bool2)
    {
      paramObject = (h)paramObject;
      bool2 = b;
      boolean bool3 = ((h)paramObject).a();
      if (bool2 == bool3)
      {
        o localo = c;
        if (localo == null)
        {
          paramObject = ((h)paramObject).b();
          if (paramObject != null) {
            break label83;
          }
        }
        else
        {
          paramObject = ((h)paramObject).b();
          boolean bool4 = localo.equals(paramObject);
          if (!bool4) {
            break label83;
          }
        }
        return bool1;
      }
      label83:
      return false;
    }
    return false;
  }
  
  public final int hashCode()
  {
    boolean bool = b;
    if (bool) {
      i = 1231;
    } else {
      i = 1237;
    }
    int j = 1000003;
    int i = (i ^ j) * j;
    o localo = c;
    if (localo == null)
    {
      j = 0;
      localo = null;
    }
    else
    {
      j = localo.hashCode();
    }
    return i ^ j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("EndSpanOptions{sampleToLocalSpanStore=");
    boolean bool = b;
    localStringBuilder.append(bool);
    localStringBuilder.append(", status=");
    o localo = c;
    localStringBuilder.append(localo);
    localStringBuilder.append("}");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     io.a.f.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */