package io.a.f.b;

import io.a.f.m;
import java.text.ParseException;

public abstract class a
{
  static final a.a a;
  
  static
  {
    a.a locala = new io/a/f/b/a$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  static a a()
  {
    return a;
  }
  
  private m b(byte[] paramArrayOfByte)
  {
    try
    {
      return a(paramArrayOfByte);
    }
    catch (c paramArrayOfByte)
    {
      ParseException localParseException = new java/text/ParseException;
      paramArrayOfByte = paramArrayOfByte.toString();
      localParseException.<init>(paramArrayOfByte, 0);
      throw localParseException;
    }
  }
  
  public m a(byte[] paramArrayOfByte)
  {
    try
    {
      return b(paramArrayOfByte);
    }
    catch (ParseException paramArrayOfByte)
    {
      c localc = new io/a/f/b/c;
      localc.<init>("Error while parsing.", paramArrayOfByte);
      throw localc;
    }
  }
  
  public byte[] a(m paramm)
  {
    return a(paramm);
  }
}

/* Location:
 * Qualified Name:     io.a.f.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */