package io.a.f;

import java.util.List;

final class d
  extends t
{
  private final List a;
  
  d(List paramList)
  {
    if (paramList != null)
    {
      a = paramList;
      return;
    }
    paramList = new java/lang/NullPointerException;
    paramList.<init>("Null entries");
    throw paramList;
  }
  
  public final List a()
  {
    return a;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {
      return true;
    }
    boolean bool = paramObject instanceof t;
    if (bool)
    {
      paramObject = (t)paramObject;
      List localList = a;
      paramObject = ((t)paramObject).a();
      return localList.equals(paramObject);
    }
    return false;
  }
  
  public final int hashCode()
  {
    return a.hashCode() ^ 0xF4243;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Tracestate{entries=");
    List localList = a;
    localStringBuilder.append(localList);
    localStringBuilder.append("}");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     io.a.f.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */