package io.a.f;

import java.util.Arrays;

public final class r
{
  public static final r a;
  private final byte b = 0;
  
  static
  {
    r localr = new io/a/f/r;
    localr.<init>();
    a = localr;
  }
  
  final boolean a()
  {
    int i = b;
    int j = 1;
    i &= j;
    if (i != 0) {
      return j;
    }
    return false;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (paramObject == this) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof r;
    if (!bool2) {
      return false;
    }
    paramObject = (r)paramObject;
    int i = b;
    int j = b;
    if (i == j) {
      return bool1;
    }
    return false;
  }
  
  public final int hashCode()
  {
    byte[] arrayOfByte = new byte[1];
    int i = b;
    arrayOfByte[0] = i;
    return Arrays.hashCode(arrayOfByte);
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("TraceOptions{sampled=");
    boolean bool = a();
    localStringBuilder.append(bool);
    localStringBuilder.append("}");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     io.a.f.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */