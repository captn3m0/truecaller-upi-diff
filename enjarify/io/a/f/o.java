package io.a.f;

import io.a.c.c;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.TreeMap;

public final class o
{
  public static final o a = o.a.a.a();
  public static final o b = o.a.b.a();
  public static final o c = o.a.c.a();
  public static final o d = o.a.d.a();
  public static final o e = o.a.e.a();
  public static final o f = o.a.f.a();
  public static final o g = o.a.g.a();
  public static final o h = o.a.h.a();
  public static final o i = o.a.q.a();
  public static final o j = o.a.i.a();
  public static final o k = o.a.j.a();
  public static final o l = o.a.k.a();
  public static final o m = o.a.l.a();
  public static final o n = o.a.m.a();
  public static final o o = o.a.n.a();
  public static final o p = o.a.o.a();
  public static final o q = o.a.p.a();
  private static final List t;
  public final o.a r;
  public final String s;
  
  static
  {
    Object localObject1 = new java/util/TreeMap;
    ((TreeMap)localObject1).<init>();
    Object localObject2 = o.a.values();
    int i1 = localObject2.length;
    int i2 = 0;
    while (i2 < i1)
    {
      o.a locala = localObject2[i2];
      int i3 = r;
      Object localObject3 = Integer.valueOf(i3);
      o localo = new io/a/f/o;
      localo.<init>(locala, null);
      localObject3 = (o)((TreeMap)localObject1).put(localObject3, localo);
      if (localObject3 == null)
      {
        i2 += 1;
      }
      else
      {
        localObject1 = new java/lang/IllegalStateException;
        localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>("Code value duplication between ");
        String str = r.name();
        ((StringBuilder)localObject2).append(str);
        ((StringBuilder)localObject2).append(" & ");
        str = locala.name();
        ((StringBuilder)localObject2).append(str);
        localObject2 = ((StringBuilder)localObject2).toString();
        ((IllegalStateException)localObject1).<init>((String)localObject2);
        throw ((Throwable)localObject1);
      }
    }
    localObject2 = new java/util/ArrayList;
    localObject1 = ((TreeMap)localObject1).values();
    ((ArrayList)localObject2).<init>((Collection)localObject1);
    t = Collections.unmodifiableList((List)localObject2);
  }
  
  public o(o.a parama, String paramString)
  {
    parama = (o.a)c.a(parama, "canonicalCode");
    r = parama;
    s = paramString;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (paramObject == this) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof o;
    if (!bool2) {
      return false;
    }
    paramObject = (o)paramObject;
    Object localObject = r;
    o.a locala = r;
    if (localObject == locala)
    {
      localObject = s;
      paramObject = s;
      boolean bool3 = c.b(localObject, paramObject);
      if (bool3) {
        return bool1;
      }
    }
    return false;
  }
  
  public final int hashCode()
  {
    Object[] arrayOfObject = new Object[2];
    Object localObject = r;
    arrayOfObject[0] = localObject;
    localObject = s;
    arrayOfObject[1] = localObject;
    return Arrays.hashCode(arrayOfObject);
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Status{canonicalCode=");
    Object localObject = r;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", description=");
    localObject = s;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append("}");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     io.a.f.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */