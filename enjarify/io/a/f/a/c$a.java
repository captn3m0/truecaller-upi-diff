package io.a.f.a;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

final class c$a
  extends c
{
  private static final c.b a;
  private final Set b;
  
  static
  {
    Map localMap1 = Collections.emptyMap();
    Map localMap2 = Collections.emptyMap();
    a locala = new io/a/f/a/a;
    HashMap localHashMap = new java/util/HashMap;
    localMap1 = (Map)io.a.c.c.a(localMap1, "numbersOfLatencySampledSpans");
    localHashMap.<init>(localMap1);
    localMap1 = Collections.unmodifiableMap(localHashMap);
    localHashMap = new java/util/HashMap;
    localMap2 = (Map)io.a.c.c.a(localMap2, "numbersOfErrorSampledSpans");
    localHashMap.<init>(localMap2);
    localMap2 = Collections.unmodifiableMap(localHashMap);
    locala.<init>(localMap1, localMap2);
    a = locala;
  }
  
  private c$a()
  {
    HashSet localHashSet = new java/util/HashSet;
    localHashSet.<init>();
    b = localHashSet;
  }
}

/* Location:
 * Qualified Name:     io.a.f.a.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */