package io.a.f.a;

import java.util.Map;

final class a
  extends c.b
{
  private final Map a;
  private final Map b;
  
  a(Map paramMap1, Map paramMap2)
  {
    if (paramMap1 != null)
    {
      a = paramMap1;
      if (paramMap2 != null)
      {
        b = paramMap2;
        return;
      }
      paramMap1 = new java/lang/NullPointerException;
      paramMap1.<init>("Null numbersOfErrorSampledSpans");
      throw paramMap1;
    }
    paramMap1 = new java/lang/NullPointerException;
    paramMap1.<init>("Null numbersOfLatencySampledSpans");
    throw paramMap1;
  }
  
  public final Map a()
  {
    return a;
  }
  
  public final Map b()
  {
    return b;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (paramObject == this) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof c.b;
    if (bool2)
    {
      paramObject = (c.b)paramObject;
      Map localMap1 = a;
      Map localMap2 = ((c.b)paramObject).a();
      bool2 = localMap1.equals(localMap2);
      if (bool2)
      {
        localMap1 = b;
        paramObject = ((c.b)paramObject).b();
        boolean bool3 = localMap1.equals(paramObject);
        if (bool3) {
          return bool1;
        }
      }
      return false;
    }
    return false;
  }
  
  public final int hashCode()
  {
    int i = a.hashCode();
    int j = 1000003;
    i = (i ^ j) * j;
    j = b.hashCode();
    return i ^ j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("PerSpanNameSummary{numbersOfLatencySampledSpans=");
    Map localMap = a;
    localStringBuilder.append(localMap);
    localStringBuilder.append(", numbersOfErrorSampledSpans=");
    localMap = b;
    localStringBuilder.append(localMap);
    localStringBuilder.append("}");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     io.a.f.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */