package io.a.f;

final class b
  extends i
{
  private final i.b a;
  private final long b;
  private final long c;
  private final long d;
  
  private b(i.b paramb, long paramLong1, long paramLong2, long paramLong3)
  {
    a = paramb;
    b = paramLong1;
    c = paramLong2;
    d = paramLong3;
  }
  
  public final i.b a()
  {
    return a;
  }
  
  public final long b()
  {
    return b;
  }
  
  public final long c()
  {
    return c;
  }
  
  public final long d()
  {
    return d;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (paramObject == this) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof i;
    if (bool2)
    {
      paramObject = (i)paramObject;
      i.b localb1 = a;
      i.b localb2 = ((i)paramObject).a();
      bool2 = localb1.equals(localb2);
      if (bool2)
      {
        long l1 = b;
        long l2 = ((i)paramObject).b();
        bool2 = l1 < l2;
        if (!bool2)
        {
          l1 = c;
          l2 = ((i)paramObject).c();
          bool2 = l1 < l2;
          if (!bool2)
          {
            l1 = d;
            l2 = ((i)paramObject).d();
            boolean bool3 = l1 < l2;
            if (!bool3) {
              return bool1;
            }
          }
        }
      }
      return false;
    }
    return false;
  }
  
  public final int hashCode()
  {
    int i = a.hashCode();
    int j = 1000003;
    long l1 = (i ^ j) * j;
    long l2 = b;
    i = 32;
    long l3 = l2 >>> i;
    l2 ^= l3;
    l1 = (int)(l1 ^ l2) * j;
    l2 = c;
    l3 = l2 >>> i;
    l2 ^= l3;
    long l4 = (int)(l1 ^ l2) * j;
    long l5 = d;
    long l6 = l5 >>> i;
    l5 ^= l6;
    return (int)(l4 ^ l5);
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("MessageEvent{type=");
    i.b localb = a;
    localStringBuilder.append(localb);
    localStringBuilder.append(", messageId=");
    long l = b;
    localStringBuilder.append(l);
    localStringBuilder.append(", uncompressedMessageSize=");
    l = c;
    localStringBuilder.append(l);
    localStringBuilder.append(", compressedMessageSize=");
    l = d;
    localStringBuilder.append(l);
    localStringBuilder.append("}");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     io.a.f.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */