package io.a.d;

import io.a.c.a;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class f
{
  private static final Logger a = Logger.getLogger(f.class.getName());
  private static final g b = a(g.class.getClassLoader());
  
  private static g a(ClassLoader paramClassLoader)
  {
    boolean bool = true;
    Object localObject1 = "io.opencensus.impl.stats.StatsComponentImpl";
    try
    {
      localObject1 = Class.forName((String)localObject1, bool, paramClassLoader);
      localObject3 = g.class;
      localObject1 = a.a((Class)localObject1, (Class)localObject3);
      return (g)localObject1;
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      Object localObject3 = a;
      Level localLevel = Level.FINE;
      String str = "Couldn't load full implementation for StatsComponent, now trying to load lite implementation.";
      ((Logger)localObject3).log(localLevel, str, localClassNotFoundException);
      Object localObject2 = "io.opencensus.impllite.stats.StatsComponentImplLite";
      try
      {
        paramClassLoader = Class.forName((String)localObject2, bool, paramClassLoader);
        localObject4 = g.class;
        paramClassLoader = a.a(paramClassLoader, (Class)localObject4);
        return (g)paramClassLoader;
      }
      catch (ClassNotFoundException paramClassLoader)
      {
        Object localObject4 = a;
        localObject2 = Level.FINE;
        ((Logger)localObject4).log((Level)localObject2, "Couldn't load lite implementation for StatsComponent, now using default implementation for StatsComponent.", paramClassLoader);
        paramClassLoader = new io/a/d/e$b;
        paramClassLoader.<init>((byte)0);
      }
    }
    return paramClassLoader;
  }
  
  public static h a()
  {
    return b.a();
  }
}

/* Location:
 * Qualified Name:     io.a.d.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */