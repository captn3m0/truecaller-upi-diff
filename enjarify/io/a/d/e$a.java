package io.a.d;

import io.a.c.c;
import io.a.e.e;
import java.util.logging.Level;
import java.util.logging.Logger;

final class e$a
  extends d
{
  private static final Logger a = Logger.getLogger(a.class.getName());
  private boolean b;
  
  public final d a(double paramDouble)
  {
    double d = 0.0D;
    boolean bool1 = paramDouble < d;
    if (bool1)
    {
      boolean bool2 = true;
      b = bool2;
    }
    return this;
  }
  
  public final d a(long paramLong)
  {
    long l = 0L;
    boolean bool1 = paramLong < l;
    if (bool1)
    {
      boolean bool2 = true;
      b = bool2;
    }
    return this;
  }
  
  public final void a(e parame)
  {
    Object localObject = "tags";
    c.a(parame, localObject);
    boolean bool = b;
    if (bool)
    {
      parame = a;
      localObject = Level.WARNING;
      String str = "Dropping values, value to record must be non-negative.";
      parame.log((Level)localObject, str);
    }
  }
}

/* Location:
 * Qualified Name:     io.a.d.e.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */