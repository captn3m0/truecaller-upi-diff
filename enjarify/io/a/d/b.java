package io.a.d;

final class b
  extends c.b
{
  private final String a;
  private final String b;
  private final String c;
  
  b(String paramString1, String paramString2, String paramString3)
  {
    if (paramString1 != null)
    {
      a = paramString1;
      if (paramString2 != null)
      {
        b = paramString2;
        if (paramString3 != null)
        {
          c = paramString3;
          return;
        }
        paramString1 = new java/lang/NullPointerException;
        paramString1.<init>("Null unit");
        throw paramString1;
      }
      paramString1 = new java/lang/NullPointerException;
      paramString1.<init>("Null description");
      throw paramString1;
    }
    paramString1 = new java/lang/NullPointerException;
    paramString1.<init>("Null name");
    throw paramString1;
  }
  
  public final String a()
  {
    return a;
  }
  
  public final String b()
  {
    return b;
  }
  
  public final String c()
  {
    return c;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (paramObject == this) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof c.b;
    if (bool2)
    {
      paramObject = (c.b)paramObject;
      String str1 = a;
      String str2 = ((c.b)paramObject).a();
      bool2 = str1.equals(str2);
      if (bool2)
      {
        str1 = b;
        str2 = ((c.b)paramObject).b();
        bool2 = str1.equals(str2);
        if (bool2)
        {
          str1 = c;
          paramObject = ((c.b)paramObject).c();
          boolean bool3 = str1.equals(paramObject);
          if (bool3) {
            return bool1;
          }
        }
      }
      return false;
    }
    return false;
  }
  
  public final int hashCode()
  {
    int i = a.hashCode();
    int j = 1000003;
    i = (i ^ j) * j;
    int k = b.hashCode();
    i = (i ^ k) * j;
    j = c.hashCode();
    return i ^ j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("MeasureLong{name=");
    String str = a;
    localStringBuilder.append(str);
    localStringBuilder.append(", description=");
    str = b;
    localStringBuilder.append(str);
    localStringBuilder.append(", unit=");
    str = c;
    localStringBuilder.append(str);
    localStringBuilder.append("}");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     io.a.d.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */