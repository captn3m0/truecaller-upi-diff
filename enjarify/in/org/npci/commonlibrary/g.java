package in.org.npci.commonlibrary;

public enum g
{
  private final int i;
  private final String j;
  
  static
  {
    Object localObject = new in/org/npci/commonlibrary/g;
    ((g)localObject).<init>("KEY_CODE_EMPTY", 0, 1001, "Your organization key is empty. Please provide a organization key.");
    a = (g)localObject;
    localObject = new in/org/npci/commonlibrary/g;
    int m = 1;
    ((g)localObject).<init>("KEY_CODE_INVALID", m, 1002, "Your organization key is invalid. Please contact your system administrator or UPI support team.");
    b = (g)localObject;
    localObject = new in/org/npci/commonlibrary/g;
    int n = 2;
    ((g)localObject).<init>("PUBLICKEY_NOT_FOUND", n, 1003, "Public key file not found please contact your system administrator UPI support team");
    c = (g)localObject;
    localObject = new in/org/npci/commonlibrary/g;
    int i1 = 3;
    ((g)localObject).<init>("PARSER_MISCONFIG", i1, 1004, "XML Parser configuration error.Keys.xml may not be formatted correctly.");
    d = (g)localObject;
    localObject = new in/org/npci/commonlibrary/g;
    int i2 = 4;
    ((g)localObject).<init>("XML_PATH_ERROR", i2, 1005, "XML File is not found or cannot be read.");
    e = (g)localObject;
    localObject = new in/org/npci/commonlibrary/g;
    int i3 = 5;
    ((g)localObject).<init>("KEYS_NOT_VALID", i3, 1006, "Keys are not valid. Please contact your system administrator UPI support team");
    f = (g)localObject;
    localObject = new in/org/npci/commonlibrary/g;
    int i4 = 6;
    ((g)localObject).<init>("UNKNOWN_ERROR", i4, 1007, "Unknown error occurred.");
    g = (g)localObject;
    localObject = new in/org/npci/commonlibrary/g;
    int i5 = 7;
    ((g)localObject).<init>("TRUST_NOT_VALID", i5, 1008, "Trust is not valid");
    h = (g)localObject;
    localObject = new g[8];
    g localg = a;
    localObject[0] = localg;
    localg = b;
    localObject[m] = localg;
    localg = c;
    localObject[n] = localg;
    localg = d;
    localObject[i1] = localg;
    localg = e;
    localObject[i2] = localg;
    localg = f;
    localObject[i3] = localg;
    localg = g;
    localObject[i4] = localg;
    localg = h;
    localObject[i5] = localg;
    k = (g[])localObject;
  }
  
  private g(int paramInt1, String paramString1)
  {
    i = paramInt1;
    j = paramString1;
  }
  
  public final String a()
  {
    return j;
  }
  
  public final int b()
  {
    return i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    int m = i;
    localStringBuilder.append(m);
    localStringBuilder.append(": ");
    String str = j;
    localStringBuilder.append(str);
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     in.org.npci.commonlibrary.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */