package in.org.npci.commonlibrary;

import java.io.Serializable;

public class Message
  implements Serializable
{
  private static final long serialVersionUID = -8585407394073013324L;
  private Data data;
  private String subType;
  private String type;
  
  public Message(String paramString1, String paramString2, Data paramData)
  {
    type = paramString1;
    subType = paramString2;
    data = paramData;
  }
  
  public Data getData()
  {
    return data;
  }
  
  public String getSubType()
  {
    return subType;
  }
  
  public String getType()
  {
    return type;
  }
  
  public void setData(Data paramData)
  {
    data = paramData;
  }
  
  public void setSubType(String paramString)
  {
    subType = paramString;
  }
  
  public void setType(String paramString)
  {
    type = paramString;
  }
}

/* Location:
 * Qualified Name:     in.org.npci.commonlibrary.Message
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */