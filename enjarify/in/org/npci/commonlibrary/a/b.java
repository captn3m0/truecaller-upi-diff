package in.org.npci.commonlibrary.a;

import java.io.PrintStream;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class b
{
  private Certificate a;
  
  public b()
  {
    Object localObject = "signer.crt";
    try
    {
      localObject = b((String)localObject);
      a = ((Certificate)localObject);
      return;
    }
    catch (CertificateException localCertificateException)
    {
      System.out.println("Error in loading exception");
      localCertificateException.printStackTrace();
    }
  }
  
  /* Error */
  private Certificate b(String paramString)
  {
    // Byte code:
    //   0: ldc 39
    //   2: invokestatic 45	java/security/cert/CertificateFactory:getInstance	(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;
    //   5: astore_2
    //   6: aload_0
    //   7: invokevirtual 49	java/lang/Object:getClass	()Ljava/lang/Class;
    //   10: invokevirtual 55	java/lang/Class:getClassLoader	()Ljava/lang/ClassLoader;
    //   13: aload_1
    //   14: invokevirtual 61	java/lang/ClassLoader:getResourceAsStream	(Ljava/lang/String;)Ljava/io/InputStream;
    //   17: astore_1
    //   18: new 63	java/io/BufferedInputStream
    //   21: astore_3
    //   22: aload_3
    //   23: aload_1
    //   24: invokespecial 66	java/io/BufferedInputStream:<init>	(Ljava/io/InputStream;)V
    //   27: aload_2
    //   28: aload_3
    //   29: invokevirtual 70	java/security/cert/CertificateFactory:generateCertificate	(Ljava/io/InputStream;)Ljava/security/cert/Certificate;
    //   32: astore_2
    //   33: aload_3
    //   34: invokevirtual 75	java/io/InputStream:close	()V
    //   37: aload_1
    //   38: invokevirtual 75	java/io/InputStream:close	()V
    //   41: aload_2
    //   42: areturn
    //   43: astore_2
    //   44: aload_3
    //   45: invokevirtual 75	java/io/InputStream:close	()V
    //   48: aload_1
    //   49: invokevirtual 75	java/io/InputStream:close	()V
    //   52: aload_2
    //   53: athrow
    //   54: pop
    //   55: goto -18 -> 37
    //   58: pop
    //   59: goto -18 -> 41
    //   62: pop
    //   63: goto -15 -> 48
    //   66: pop
    //   67: goto -15 -> 52
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	70	0	this	b
    //   0	70	1	paramString	String
    //   5	37	2	localObject1	Object
    //   43	10	2	localObject2	Object
    //   21	24	3	localBufferedInputStream	java.io.BufferedInputStream
    //   54	1	5	localIOException1	java.io.IOException
    //   58	1	6	localIOException2	java.io.IOException
    //   62	1	7	localIOException3	java.io.IOException
    //   66	1	8	localIOException4	java.io.IOException
    // Exception table:
    //   from	to	target	type
    //   28	32	43	finally
    //   33	37	54	java/io/IOException
    //   37	41	58	java/io/IOException
    //   44	48	62	java/io/IOException
    //   48	52	66	java/io/IOException
  }
  
  public boolean a(String paramString)
  {
    Object localObject1 = a;
    boolean bool = false;
    if (localObject1 == null)
    {
      localObject1 = "signer.crt";
      try
      {
        localObject1 = b((String)localObject1);
        a = ((Certificate)localObject1);
      }
      catch (CertificateException paramString)
      {
        System.out.println("Error in loading certificate.");
        paramString.printStackTrace();
        return false;
      }
    }
    try
    {
      localObject1 = a.a(paramString);
      localObject2 = a;
      localObject2 = ((Certificate)localObject2).getPublicKey();
      bool = a.a((Document)localObject1, (PublicKey)localObject2);
    }
    catch (Exception paramString)
    {
      paramString.printStackTrace();
    }
    catch (ParserConfigurationException localParserConfigurationException) {}catch (SAXException localSAXException) {}
    Object localObject2 = System.err;
    String str = "Parsing failed for message:";
    paramString = String.valueOf(paramString);
    paramString = str.concat(paramString);
    ((PrintStream)localObject2).println(paramString);
    localSAXException.printStackTrace();
    return bool;
  }
}

/* Location:
 * Qualified Name:     in.org.npci.commonlibrary.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */