package in.org.npci.commonlibrary.a;

import java.io.StringReader;
import java.security.PublicKey;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.xml.security.signature.XMLSignature;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class a
{
  public static Document a(String paramString)
  {
    Object localObject = DocumentBuilderFactory.newInstance();
    ((DocumentBuilderFactory)localObject).setNamespaceAware(true);
    localObject = ((DocumentBuilderFactory)localObject).newDocumentBuilder();
    InputSource localInputSource = new org/xml/sax/InputSource;
    StringReader localStringReader = new java/io/StringReader;
    localStringReader.<init>(paramString);
    localInputSource.<init>(localStringReader);
    return ((DocumentBuilder)localObject).parse(localInputSource);
  }
  
  public static boolean a(Document paramDocument, PublicKey paramPublicKey)
  {
    Object localObject = "http://www.w3.org/2000/09/xmldsig#";
    String str = "Signature";
    paramDocument = paramDocument.getElementsByTagNameNS((String)localObject, str);
    int i = paramDocument.getLength();
    if (i != 0)
    {
      localObject = new org/apache/xml/security/signature/XMLSignature;
      paramDocument = (Element)paramDocument.item(0);
      ((XMLSignature)localObject).<init>(paramDocument, "");
      return ((XMLSignature)localObject).a(paramPublicKey);
    }
    paramDocument = new java/lang/Exception;
    paramDocument.<init>("Signature not found");
    throw paramDocument;
  }
}

/* Location:
 * Qualified Name:     in.org.npci.commonlibrary.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */