package in.org.npci.commonlibrary;

import java.security.MessageDigest;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class i
{
  Cipher a;
  byte[] b;
  byte[] c;
  
  public i()
  {
    Object localObject = Cipher.getInstance("AES/CBC/PKCS5Padding");
    a = ((Cipher)localObject);
    localObject = new byte[32];
    b = ((byte[])localObject);
    localObject = new byte[16];
    c = ((byte[])localObject);
  }
  
  public byte[] a(String paramString)
  {
    MessageDigest localMessageDigest = MessageDigest.getInstance("SHA-256");
    paramString = paramString.getBytes("UTF-8");
    localMessageDigest.update(paramString);
    return localMessageDigest.digest();
  }
  
  public byte[] a(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2)
  {
    SecretKeySpec localSecretKeySpec = new javax/crypto/spec/SecretKeySpec;
    localSecretKeySpec.<init>(paramArrayOfByte2, "AES");
    paramArrayOfByte2 = new byte[16];
    IvParameterSpec localIvParameterSpec = new javax/crypto/spec/IvParameterSpec;
    localIvParameterSpec.<init>(paramArrayOfByte2);
    paramArrayOfByte2 = Cipher.getInstance("AES/CBC/PKCS5Padding");
    paramArrayOfByte2.init(1, localSecretKeySpec, localIvParameterSpec);
    return paramArrayOfByte2.doFinal(paramArrayOfByte1);
  }
  
  public byte[] b(String paramString)
  {
    int i = paramString.length() / 2;
    byte[] arrayOfByte = new byte[i];
    int j = 0;
    for (;;)
    {
      int k = arrayOfByte.length;
      if (j >= k) {
        break;
      }
      k = j * 2;
      int m = k + 2;
      String str = paramString.substring(k, m);
      m = 16;
      k = (byte)Integer.parseInt(str, m);
      arrayOfByte[j] = k;
      j += 1;
    }
    return arrayOfByte;
  }
  
  public byte[] b(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2)
  {
    SecretKeySpec localSecretKeySpec = new javax/crypto/spec/SecretKeySpec;
    localSecretKeySpec.<init>(paramArrayOfByte2, "AES");
    paramArrayOfByte2 = new byte[16];
    IvParameterSpec localIvParameterSpec = new javax/crypto/spec/IvParameterSpec;
    localIvParameterSpec.<init>(paramArrayOfByte2);
    paramArrayOfByte2 = Cipher.getInstance("AES/CBC/PKCS5Padding");
    paramArrayOfByte2.init(2, localSecretKeySpec, localIvParameterSpec);
    return paramArrayOfByte2.doFinal(paramArrayOfByte1);
  }
}

/* Location:
 * Qualified Name:     in.org.npci.commonlibrary.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */