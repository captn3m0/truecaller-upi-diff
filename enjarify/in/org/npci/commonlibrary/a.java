package in.org.npci.commonlibrary;

import java.io.UnsupportedEncodingException;

public class a
{
  public static byte[] a(String paramString, int paramInt)
  {
    return a(paramString.getBytes(), paramInt);
  }
  
  public static byte[] a(byte[] paramArrayOfByte, int paramInt)
  {
    int i = paramArrayOfByte.length;
    return a(paramArrayOfByte, 0, i, paramInt);
  }
  
  public static byte[] a(byte[] paramArrayOfByte, int paramInt1, int paramInt2, int paramInt3)
  {
    c localc = new in/org/npci/commonlibrary/c;
    int i = paramInt2 * 3 / 4;
    byte[] arrayOfByte1 = new byte[i];
    localc.<init>(paramInt3, arrayOfByte1);
    paramInt3 = 1;
    boolean bool = localc.a(paramArrayOfByte, paramInt1, paramInt2, paramInt3);
    if (bool)
    {
      int j = b;
      byte[] arrayOfByte2 = a;
      paramInt1 = arrayOfByte2.length;
      if (j == paramInt1) {
        return a;
      }
      paramArrayOfByte = new byte[b];
      arrayOfByte2 = a;
      paramInt2 = b;
      System.arraycopy(arrayOfByte2, 0, paramArrayOfByte, 0, paramInt2);
      return paramArrayOfByte;
    }
    paramArrayOfByte = new java/lang/IllegalArgumentException;
    paramArrayOfByte.<init>("bad base-64");
    throw paramArrayOfByte;
  }
  
  public static String b(byte[] paramArrayOfByte, int paramInt)
  {
    try
    {
      String str = new java/lang/String;
      paramArrayOfByte = c(paramArrayOfByte, paramInt);
      localObject = "US-ASCII";
      str.<init>(paramArrayOfByte, (String)localObject);
      return str;
    }
    catch (UnsupportedEncodingException paramArrayOfByte)
    {
      Object localObject = new java/lang/AssertionError;
      ((AssertionError)localObject).<init>(paramArrayOfByte);
      throw ((Throwable)localObject);
    }
  }
  
  public static byte[] b(byte[] paramArrayOfByte, int paramInt1, int paramInt2, int paramInt3)
  {
    d locald = new in/org/npci/commonlibrary/d;
    byte[] arrayOfByte = null;
    locald.<init>(paramInt3, null);
    paramInt3 = paramInt2 / 3 * 4;
    boolean bool1 = d;
    int i;
    if (bool1)
    {
      i = paramInt2 % 3;
      if (i > 0) {
        paramInt3 += 4;
      }
    }
    else
    {
      i = paramInt2 % 3;
      switch (i)
      {
      default: 
        break;
      case 2: 
        paramInt3 += 3;
        break;
      case 1: 
        paramInt3 += 2;
      }
    }
    boolean bool2 = e;
    int k = 1;
    if ((bool2) && (paramInt2 > 0))
    {
      int j = (paramInt2 + -1) / 57 + k;
      boolean bool3 = f;
      int m;
      if (bool3) {
        m = 2;
      } else {
        m = 1;
      }
      j *= m;
      paramInt3 += j;
    }
    arrayOfByte = new byte[paramInt3];
    a = arrayOfByte;
    locald.a(paramArrayOfByte, paramInt1, paramInt2, k);
    boolean bool4 = a;
    if (!bool4)
    {
      int n = b;
      if (n != paramInt3)
      {
        paramArrayOfByte = new java/lang/AssertionError;
        paramArrayOfByte.<init>();
        throw paramArrayOfByte;
      }
    }
    return a;
  }
  
  public static byte[] c(byte[] paramArrayOfByte, int paramInt)
  {
    int i = paramArrayOfByte.length;
    return b(paramArrayOfByte, 0, i, paramInt);
  }
}

/* Location:
 * Qualified Name:     in.org.npci.commonlibrary.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */