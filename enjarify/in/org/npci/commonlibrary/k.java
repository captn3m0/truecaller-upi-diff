package in.org.npci.commonlibrary;

import java.io.IOException;
import java.io.PrintStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

class k
  extends DefaultHandler
{
  private static List a;
  private static j b = null;
  private static String c = null;
  
  static
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    a = localArrayList;
  }
  
  public k() {}
  
  public k(String paramString)
  {
    Object localObject = SAXParserFactory.newInstance();
    try
    {
      localObject = ((SAXParserFactory)localObject).newSAXParser();
      InputSource localInputSource = new org/xml/sax/InputSource;
      StringReader localStringReader = new java/io/StringReader;
      localStringReader.<init>(paramString);
      localInputSource.<init>(localStringReader);
      ((SAXParser)localObject).parse(localInputSource, this);
      return;
    }
    catch (ParserConfigurationException|SAXException|IOException localParserConfigurationException)
    {
      paramString = new in/org/npci/commonlibrary/f;
      localObject = g.d;
      paramString.<init>((g)localObject);
      throw paramString;
    }
  }
  
  public List a()
  {
    return a;
  }
  
  public void characters(char[] paramArrayOfChar, int paramInt1, int paramInt2)
  {
    c = String.copyValueOf(paramArrayOfChar, paramInt1, paramInt2).trim();
  }
  
  public void endElement(String paramString1, String paramString2, String paramString3)
  {
    int i = paramString3.hashCode();
    int k = 106079;
    boolean bool;
    if (i != k)
    {
      k = 492250706;
      if (i == k)
      {
        paramString1 = "keyValue";
        bool = paramString3.equals(paramString1);
        if (bool)
        {
          bool = true;
          break label78;
        }
      }
    }
    else
    {
      paramString1 = "key";
      bool = paramString3.equals(paramString1);
      if (bool)
      {
        bool = false;
        paramString1 = null;
        break label78;
      }
    }
    int j = -1;
    switch (j)
    {
    default: 
      break;
    case 1: 
      paramString1 = b;
      paramString2 = c;
      paramString1.c(paramString2);
      break;
    case 0: 
      label78:
      paramString1 = a;
      paramString2 = b;
      paramString1.add(paramString2);
    }
  }
  
  protected void finalize()
  {
    System.out.println("KeyParser Destroyed");
  }
  
  public void startElement(String paramString1, String paramString2, String paramString3, Attributes paramAttributes)
  {
    int i = paramString3.hashCode();
    int k = 106079;
    if (i == k)
    {
      paramString1 = "key";
      boolean bool = paramString3.equals(paramString1);
      if (bool)
      {
        bool = false;
        paramString1 = null;
        break label46;
      }
    }
    int j = -1;
    label46:
    if (j == 0)
    {
      paramString1 = new in/org/npci/commonlibrary/j;
      paramString1.<init>();
      b = paramString1;
      paramString2 = paramAttributes.getValue("ki");
      paramString1.a(paramString2);
      paramString1 = b;
      paramString2 = paramAttributes.getValue("owner");
      paramString1.b(paramString2);
    }
  }
}

/* Location:
 * Qualified Name:     in.org.npci.commonlibrary.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */