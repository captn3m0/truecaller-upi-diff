package in.org.npci.commonlibrary;

import in.org.npci.commonlibrary.a.b;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import org.apache.xml.security.Init;

public class e
{
  private static List a;
  private i b;
  private k c;
  private b d;
  private String e;
  
  public e(String paramString)
  {
    Object localObject = "";
    e = ((String)localObject);
    Init.b();
    try
    {
      localObject = new in/org/npci/commonlibrary/a/b;
      ((b)localObject).<init>();
      d = ((b)localObject);
      localObject = d;
      boolean bool = ((b)localObject).a(paramString);
      if (bool)
      {
        localObject = System.out;
        String str = "XML Validated";
        ((PrintStream)localObject).println(str);
        localObject = new in/org/npci/commonlibrary/k;
        ((k)localObject).<init>(paramString);
        c = ((k)localObject);
        paramString = c;
        paramString = paramString.a();
        a = paramString;
        try
        {
          paramString = new in/org/npci/commonlibrary/i;
          paramString.<init>();
          b = paramString;
          return;
        }
        catch (NoSuchPaddingException paramString) {}catch (NoSuchAlgorithmException paramString) {}
        paramString.printStackTrace();
        paramString = new in/org/npci/commonlibrary/f;
        localObject = g.g;
        paramString.<init>((g)localObject);
        throw paramString;
      }
      paramString = System.out;
      localObject = "XML Not Validated";
      paramString.println((String)localObject);
      paramString = new in/org/npci/commonlibrary/f;
      localObject = g.f;
      paramString.<init>((g)localObject);
      throw paramString;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
      paramString = new in/org/npci/commonlibrary/f;
      localObject = g.g;
      paramString.<init>((g)localObject);
      throw paramString;
    }
    catch (f paramString)
    {
      paramString.printStackTrace();
      throw paramString;
    }
  }
  
  private String a(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    int i = 500;
    localStringBuilder.<init>(i);
    try
    {
      i locali1 = b;
      i locali2 = b;
      paramString3 = locali2.a(paramString3);
      locali2 = b;
      paramString4 = locali2.b(paramString4);
      paramString3 = locali1.a(paramString3, paramString4);
      int j = 2;
      paramString3 = a.b(paramString3, j);
      localStringBuilder.append(paramString2);
      paramString2 = "|";
      localStringBuilder.append(paramString2);
      localStringBuilder.append(paramString1);
      paramString1 = "|";
      localStringBuilder.append(paramString1);
      localStringBuilder.append(paramString3);
      return localStringBuilder.toString();
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
      paramString1 = new in/org/npci/commonlibrary/f;
      paramString2 = g.g;
      paramString1.<init>(paramString2);
      throw paramString1;
    }
  }
  
  private byte[] a(String paramString)
  {
    paramString = paramString.getBytes();
    try
    {
      Object localObject1 = e;
      localObject1 = b((String)localObject1);
      Object localObject2 = "RSA/ECB/PKCS1Padding";
      localObject2 = Cipher.getInstance((String)localObject2);
      int i = 1;
      ((Cipher)localObject2).init(i, (Key)localObject1);
      paramString = ((Cipher)localObject2).doFinal(paramString);
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
      paramString = null;
    }
    return paramString;
  }
  
  private PublicKey b(String paramString)
  {
    paramString = a.a(paramString.getBytes("utf-8"), 2);
    X509EncodedKeySpec localX509EncodedKeySpec = new java/security/spec/X509EncodedKeySpec;
    localX509EncodedKeySpec.<init>(paramString);
    return KeyFactory.getInstance("RSA").generatePublic(localX509EncodedKeySpec);
  }
  
  public Message a(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    if (paramString1 != null)
    {
      boolean bool1 = paramString1.isEmpty();
      if (bool1)
      {
        paramString1 = new in/org/npci/commonlibrary/f;
        paramString2 = g.a;
        paramString1.<init>(paramString2);
        throw paramString1;
      }
    }
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    Object localObject2 = a.iterator();
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject2).hasNext();
      if (!bool2) {
        break;
      }
      j localj = (j)((Iterator)localObject2).next();
      String str = localj.b();
      boolean bool3 = str.equals(paramString1);
      if (bool3) {
        ((List)localObject1).add(localj);
      }
    }
    int i = ((List)localObject1).size();
    if (i != 0)
    {
      paramString1 = new java/util/Random;
      paramString1.<init>();
      int j = ((List)localObject1).size();
      i = paramString1.nextInt(j);
      paramString1 = (j)((List)localObject1).get(i);
      localObject1 = paramString1.c();
      e = ((String)localObject1);
      paramString2 = a(paramString2, paramString3, paramString4, paramString5);
      paramString2 = a.b(a(paramString2), 2);
      paramString3 = new in/org/npci/commonlibrary/Message;
      localObject1 = new in/org/npci/commonlibrary/Data;
      localObject2 = paramString1.a();
      paramString1 = paramString1.b();
      ((Data)localObject1).<init>((String)localObject2, paramString1, paramString2);
      paramString3.<init>("", "", (Data)localObject1);
      return paramString3;
    }
    paramString1 = new in/org/npci/commonlibrary/f;
    paramString2 = g.b;
    paramString1.<init>(paramString2);
    throw paramString1;
  }
  
  public void a(String paramString1, String paramString2, String paramString3)
  {
    try
    {
      i locali = new in/org/npci/commonlibrary/i;
      locali.<init>();
      paramString2 = locali.a(paramString2);
      int i = 2;
      paramString2 = a.b(paramString2, i);
      paramString1 = a.a(paramString1, i);
      paramString3 = locali.b(paramString3);
      paramString1 = locali.b(paramString1, paramString3);
      paramString1 = a.b(paramString1, i);
      if (paramString1 != null)
      {
        boolean bool = paramString1.equalsIgnoreCase(paramString2);
        if (!bool)
        {
          paramString1 = new in/org/npci/commonlibrary/f;
          paramString2 = g.h;
          paramString1.<init>(paramString2);
          throw paramString1;
        }
      }
      return;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
      paramString1 = new in/org/npci/commonlibrary/f;
      paramString2 = g.g;
      paramString1.<init>(paramString2);
      throw paramString1;
    }
    catch (InvalidAlgorithmParameterException paramString1) {}catch (UnsupportedEncodingException paramString1) {}catch (BadPaddingException paramString1) {}catch (IllegalBlockSizeException paramString1) {}catch (NoSuchPaddingException paramString1) {}catch (NoSuchAlgorithmException paramString1) {}catch (InvalidKeyException paramString1) {}
    paramString1.printStackTrace();
    paramString1 = new in/org/npci/commonlibrary/f;
    paramString2 = g.g;
    paramString1.<init>(paramString2);
    throw paramString1;
  }
}

/* Location:
 * Qualified Name:     in.org.npci.commonlibrary.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */