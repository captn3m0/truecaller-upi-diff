package in.org.npci.commonlibrary;

import java.io.Serializable;

public class Data
  implements Serializable
{
  private static final long serialVersionUID = 5218660121564017090L;
  private String code;
  private String encryptedBase64String;
  private String hmac;
  private String ki;
  private String pid;
  private String skey;
  private String type;
  
  Data(String paramString1, String paramString2, String paramString3)
  {
    ki = paramString1;
    code = paramString2;
    encryptedBase64String = paramString3;
  }
  
  Data(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7)
  {
    ki = paramString1;
    code = paramString2;
    skey = paramString3;
    type = paramString4;
    pid = paramString5;
    hmac = paramString6;
    encryptedBase64String = paramString7;
  }
  
  public String getCode()
  {
    return code;
  }
  
  public String getEncryptedBase64String()
  {
    return encryptedBase64String;
  }
  
  public String getHmac()
  {
    return hmac;
  }
  
  public String getKi()
  {
    return ki;
  }
  
  public String getPid()
  {
    return pid;
  }
  
  public String getSkey()
  {
    return skey;
  }
  
  public String getType()
  {
    return type;
  }
  
  public void setCode(String paramString)
  {
    code = paramString;
  }
  
  public void setEncryptedBase64String(String paramString)
  {
    encryptedBase64String = paramString;
  }
  
  public void setHmac(String paramString)
  {
    hmac = paramString;
  }
  
  public void setKi(String paramString)
  {
    ki = paramString;
  }
  
  public void setPid(String paramString)
  {
    pid = paramString;
  }
  
  public void setSkey(String paramString)
  {
    skey = paramString;
  }
  
  public void setType(String paramString)
  {
    type = paramString;
  }
}

/* Location:
 * Qualified Name:     in.org.npci.commonlibrary.Data
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */