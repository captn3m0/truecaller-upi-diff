package e;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executor;
import okhttp3.e.a;
import okhttp3.u;
import okhttp3.y;

public final class s$a
{
  public boolean a;
  private final o b;
  private e.a c;
  private u d;
  private final List e;
  private final List f;
  private Executor g;
  
  public s$a()
  {
    this(localo);
  }
  
  private s$a(o paramo)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    e = localArrayList;
    localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    f = localArrayList;
    b = paramo;
  }
  
  private a a(e.a parama)
  {
    parama = (e.a)w.a(parama, "factory == null");
    c = parama;
    return this;
  }
  
  public final a a()
  {
    a = false;
    return this;
  }
  
  public final a a(c.a parama)
  {
    List localList = f;
    parama = w.a(parama, "factory == null");
    localList.add(parama);
    return this;
  }
  
  public final a a(f.a parama)
  {
    List localList = e;
    parama = w.a(parama, "factory == null");
    localList.add(parama);
    return this;
  }
  
  public final a a(String paramString)
  {
    w.a(paramString, "baseUrl == null");
    paramString = u.f(paramString);
    return a(paramString);
  }
  
  public final a a(u paramu)
  {
    w.a(paramu, "baseUrl == null");
    Object localObject = d;
    String str = "";
    int i = ((List)localObject).size() + -1;
    localObject = ((List)localObject).get(i);
    boolean bool = str.equals(localObject);
    if (bool)
    {
      d = paramu;
      return this;
    }
    localObject = new java/lang/IllegalArgumentException;
    paramu = String.valueOf(paramu);
    paramu = "baseUrl must end in /: ".concat(paramu);
    ((IllegalArgumentException)localObject).<init>(paramu);
    throw ((Throwable)localObject);
  }
  
  public final a a(y paramy)
  {
    paramy = (e.a)w.a(paramy, "client == null");
    return a(paramy);
  }
  
  public final s b()
  {
    Object localObject1 = d;
    if (localObject1 != null)
    {
      localObject1 = c;
      Object localObject2;
      if (localObject1 == null)
      {
        localObject1 = new okhttp3/y;
        ((y)localObject1).<init>();
        localObject2 = localObject1;
      }
      else
      {
        localObject2 = localObject1;
      }
      localObject1 = g;
      Object localObject3;
      if (localObject1 == null)
      {
        localObject1 = b.b();
        localObject3 = localObject1;
      }
      else
      {
        localObject3 = localObject1;
      }
      localObject1 = new java/util/ArrayList;
      Object localObject4 = f;
      ((ArrayList)localObject1).<init>((Collection)localObject4);
      localObject4 = b.a((Executor)localObject3);
      ((List)localObject1).addAll((Collection)localObject4);
      localObject4 = new java/util/ArrayList;
      int i = e.size() + 1;
      int j = b.d();
      i += j;
      ((ArrayList)localObject4).<init>(i);
      Object localObject5 = new e/a;
      ((a)localObject5).<init>();
      ((List)localObject4).add(localObject5);
      localObject5 = e;
      ((List)localObject4).addAll((Collection)localObject5);
      localObject5 = b.c();
      ((List)localObject4).addAll((Collection)localObject5);
      s locals = new e/s;
      localObject5 = d;
      List localList1 = Collections.unmodifiableList((List)localObject4);
      List localList2 = Collections.unmodifiableList((List)localObject1);
      boolean bool = a;
      localObject4 = locals;
      locals.<init>((e.a)localObject2, (u)localObject5, localList1, localList2, (Executor)localObject3, bool);
      return locals;
    }
    localObject1 = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject1).<init>("Base URL required.");
    throw ((Throwable)localObject1);
  }
}

/* Location:
 * Qualified Name:     e.s.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */