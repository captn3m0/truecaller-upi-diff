package e;

import d.c;
import java.util.regex.Pattern;
import okhttp3.ab.a;
import okhttp3.ac;
import okhttp3.r.a;
import okhttp3.t;
import okhttp3.t.a;
import okhttp3.u;
import okhttp3.u.a;
import okhttp3.w;
import okhttp3.x;
import okhttp3.x.a;

final class p
{
  static final Pattern a = Pattern.compile("(.*/)?(\\.|%2e|%2E){1,2}(/.*)?");
  private static final char[] g;
  String b;
  final ab.a c;
  final t.a d;
  x.a e;
  ac f;
  private final String h;
  private final u i;
  private u.a j;
  private w k;
  private final boolean l;
  private r.a m;
  
  static
  {
    char[] arrayOfChar = new char[16];
    arrayOfChar[0] = 48;
    arrayOfChar[1] = 49;
    arrayOfChar[2] = 50;
    arrayOfChar[3] = 51;
    arrayOfChar[4] = 52;
    arrayOfChar[5] = 53;
    arrayOfChar[6] = 54;
    arrayOfChar[7] = 55;
    arrayOfChar[8] = 56;
    arrayOfChar[9] = 57;
    arrayOfChar[10] = 65;
    arrayOfChar[11] = 66;
    arrayOfChar[12] = 67;
    arrayOfChar[13] = 68;
    arrayOfChar[14] = 69;
    arrayOfChar[15] = 70;
    g = arrayOfChar;
  }
  
  p(String paramString1, u paramu, String paramString2, t paramt, w paramw, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    h = paramString1;
    i = paramu;
    b = paramString2;
    paramString1 = new okhttp3/ab$a;
    paramString1.<init>();
    c = paramString1;
    k = paramw;
    l = paramBoolean1;
    if (paramt != null)
    {
      paramString1 = paramt.a();
      d = paramString1;
    }
    else
    {
      paramString1 = new okhttp3/t$a;
      paramString1.<init>();
      d = paramString1;
    }
    if (paramBoolean2)
    {
      paramString1 = new okhttp3/r$a;
      paramString1.<init>();
      m = paramString1;
      return;
    }
    if (paramBoolean3)
    {
      paramString1 = new okhttp3/x$a;
      paramString1.<init>();
      e = paramString1;
      paramString1 = e;
      paramu = x.e;
      paramString1.a(paramu);
    }
  }
  
  static String a(String paramString, boolean paramBoolean)
  {
    int n = paramString.length();
    c localc1 = null;
    int i1 = 0;
    while (i1 < n)
    {
      int i2 = paramString.codePointAt(i1);
      int i3 = 47;
      int i4 = -1;
      int i5 = 127;
      int i6 = 32;
      int i7 = 37;
      int i8;
      if ((i2 >= i6) && (i2 < i5))
      {
        String str1 = " \"<>^`{}|\\?#";
        i8 = str1.indexOf(i2);
        if ((i8 == i4) && ((paramBoolean) || ((i2 != i3) && (i2 != i7))))
        {
          i2 = Character.charCount(i2);
          i1 += i2;
          continue;
        }
      }
      c localc2 = new d/c;
      localc2.<init>();
      localc2.a(paramString, 0, i1);
      localc1 = null;
      while (i1 < n)
      {
        i8 = paramString.codePointAt(i1);
        int i9;
        if (paramBoolean)
        {
          i9 = 9;
          if (i8 != i9)
          {
            i9 = 10;
            if (i8 != i9)
            {
              i9 = 12;
              if (i8 != i9)
              {
                i9 = 13;
                if (i8 == i9) {}
              }
            }
          }
        }
        else
        {
          if ((i8 >= i6) && (i8 < i5))
          {
            String str2 = " \"<>^`{}|\\?#";
            i9 = str2.indexOf(i8);
            if ((i9 == i4) && ((paramBoolean) || ((i8 != i3) && (i8 != i7))))
            {
              localc2.a(i8);
              break label372;
            }
          }
          if (localc1 == null)
          {
            localc1 = new d/c;
            localc1.<init>();
          }
          localc1.a(i8);
          for (;;)
          {
            boolean bool = localc1.e();
            if (bool) {
              break;
            }
            int i10 = localc1.h() & 0xFF;
            localc2.b(i7);
            char[] arrayOfChar = g;
            int i11 = i10 >> 4 & 0xF;
            int i12 = arrayOfChar[i11];
            localc2.b(i12);
            arrayOfChar = g;
            i10 &= 0xF;
            i10 = arrayOfChar[i10];
            localc2.b(i10);
          }
        }
        label372:
        i8 = Character.charCount(i8);
        i1 += i8;
      }
      return localc2.p();
    }
    return paramString;
  }
  
  final ab.a a()
  {
    Object localObject1 = j;
    if (localObject1 != null)
    {
      localObject1 = ((u.a)localObject1).b();
    }
    else
    {
      localObject1 = i;
      localObject2 = b;
      localObject1 = ((u)localObject1).c((String)localObject2);
      if (localObject1 == null) {
        break label194;
      }
    }
    Object localObject2 = f;
    if (localObject2 == null)
    {
      localObject3 = m;
      if (localObject3 != null)
      {
        localObject2 = ((r.a)localObject3).a();
      }
      else
      {
        localObject3 = e;
        if (localObject3 != null)
        {
          localObject2 = ((x.a)localObject3).a();
        }
        else
        {
          boolean bool = l;
          if (bool)
          {
            bool = false;
            localObject3 = new byte[0];
            localObject2 = ac.a(null, (byte[])localObject3);
          }
        }
      }
    }
    Object localObject3 = k;
    if (localObject3 != null)
    {
      Object localObject4;
      if (localObject2 != null)
      {
        localObject4 = new e/p$a;
        ((p.a)localObject4).<init>((ac)localObject2, (w)localObject3);
        localObject2 = localObject4;
      }
      else
      {
        localObject4 = d;
        String str = "Content-Type";
        localObject3 = ((w)localObject3).toString();
        ((t.a)localObject4).a(str, (String)localObject3);
      }
    }
    localObject1 = c.a((u)localObject1);
    localObject3 = d.a();
    localObject1 = ((ab.a)localObject1).a((t)localObject3);
    localObject3 = h;
    return ((ab.a)localObject1).a((String)localObject3, (ac)localObject2);
    label194:
    localObject1 = new java/lang/IllegalArgumentException;
    localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("Malformed URL. Base: ");
    localObject3 = i;
    ((StringBuilder)localObject2).append(localObject3);
    ((StringBuilder)localObject2).append(", Relative: ");
    localObject3 = b;
    ((StringBuilder)localObject2).append((String)localObject3);
    localObject2 = ((StringBuilder)localObject2).toString();
    ((IllegalArgumentException)localObject1).<init>((String)localObject2);
    throw ((Throwable)localObject1);
  }
  
  final void a(String paramString1, String paramString2)
  {
    Object localObject = "Content-Type";
    boolean bool = ((String)localObject).equalsIgnoreCase(paramString1);
    if (bool) {
      try
      {
        paramString1 = w.a(paramString2);
        k = paramString1;
        return;
      }
      catch (IllegalArgumentException paramString1)
      {
        localObject = new java/lang/IllegalArgumentException;
        paramString2 = String.valueOf(paramString2);
        paramString2 = "Malformed content type: ".concat(paramString2);
        ((IllegalArgumentException)localObject).<init>(paramString2, paramString1);
        throw ((Throwable)localObject);
      }
    }
    d.a(paramString1, paramString2);
  }
  
  final void a(String paramString1, String paramString2, boolean paramBoolean)
  {
    Object localObject1 = b;
    if (localObject1 != null)
    {
      u localu = i;
      localObject1 = localu.d((String)localObject1);
      j = ((u.a)localObject1);
      localObject1 = j;
      if (localObject1 != null)
      {
        localObject1 = null;
        b = null;
      }
      else
      {
        paramString1 = new java/lang/IllegalArgumentException;
        paramString2 = new java/lang/StringBuilder;
        paramString2.<init>("Malformed URL. Base: ");
        Object localObject2 = i;
        paramString2.append(localObject2);
        paramString2.append(", Relative: ");
        localObject2 = b;
        paramString2.append((String)localObject2);
        paramString2 = paramString2.toString();
        paramString1.<init>(paramString2);
        throw paramString1;
      }
    }
    if (paramBoolean)
    {
      j.b(paramString1, paramString2);
      return;
    }
    j.a(paramString1, paramString2);
  }
  
  final void a(t paramt, ac paramac)
  {
    e.a(paramt, paramac);
  }
  
  final void b(String paramString1, String paramString2, boolean paramBoolean)
  {
    if (paramBoolean)
    {
      m.b(paramString1, paramString2);
      return;
    }
    m.a(paramString1, paramString2);
  }
}

/* Location:
 * Qualified Name:     e.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */