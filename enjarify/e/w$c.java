package e;

import java.lang.reflect.Type;
import java.lang.reflect.WildcardType;

final class w$c
  implements WildcardType
{
  private final Type a;
  private final Type b;
  
  w$c(Type[] paramArrayOfType1, Type[] paramArrayOfType2)
  {
    int i = paramArrayOfType2.length;
    int j = 1;
    if (i <= j)
    {
      i = paramArrayOfType2.length;
      if (i == j)
      {
        Object localObject = paramArrayOfType2[0];
        if (localObject != null)
        {
          w.c(paramArrayOfType2[0]);
          paramArrayOfType1 = paramArrayOfType1[0];
          localObject = Object.class;
          if (paramArrayOfType1 == localObject)
          {
            paramArrayOfType1 = paramArrayOfType2[0];
            b = paramArrayOfType1;
            a = Object.class;
            return;
          }
          paramArrayOfType1 = new java/lang/IllegalArgumentException;
          paramArrayOfType1.<init>();
          throw paramArrayOfType1;
        }
        paramArrayOfType1 = new java/lang/NullPointerException;
        paramArrayOfType1.<init>();
        throw paramArrayOfType1;
      }
      paramArrayOfType2 = paramArrayOfType1[0];
      if (paramArrayOfType2 != null)
      {
        w.c(paramArrayOfType1[0]);
        b = null;
        paramArrayOfType1 = paramArrayOfType1[0];
        a = paramArrayOfType1;
        return;
      }
      paramArrayOfType1 = new java/lang/NullPointerException;
      paramArrayOfType1.<init>();
      throw paramArrayOfType1;
    }
    paramArrayOfType1 = new java/lang/IllegalArgumentException;
    paramArrayOfType1.<init>();
    throw paramArrayOfType1;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = paramObject instanceof WildcardType;
    if (bool1)
    {
      paramObject = (WildcardType)paramObject;
      boolean bool2 = w.a(this, (Type)paramObject);
      if (bool2) {
        return true;
      }
    }
    return false;
  }
  
  public final Type[] getLowerBounds()
  {
    Type localType = b;
    if (localType != null)
    {
      Type[] arrayOfType = new Type[1];
      arrayOfType[0] = localType;
      return arrayOfType;
    }
    return w.a;
  }
  
  public final Type[] getUpperBounds()
  {
    Type[] arrayOfType = new Type[1];
    Type localType = a;
    arrayOfType[0] = localType;
    return arrayOfType;
  }
  
  public final int hashCode()
  {
    Type localType = b;
    int i;
    if (localType != null) {
      i = localType.hashCode() + 31;
    } else {
      i = 1;
    }
    int j = a.hashCode() + 31;
    return i ^ j;
  }
  
  public final String toString()
  {
    Object localObject1 = b;
    if (localObject1 != null)
    {
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>("? super ");
      localObject2 = w.b(b);
      ((StringBuilder)localObject1).append((String)localObject2);
      return ((StringBuilder)localObject1).toString();
    }
    localObject1 = a;
    Object localObject2 = Object.class;
    if (localObject1 == localObject2) {
      return "?";
    }
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("? extends ");
    localObject2 = w.b(a);
    ((StringBuilder)localObject1).append((String)localObject2);
    return ((StringBuilder)localObject1).toString();
  }
}

/* Location:
 * Qualified Name:     e.w.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */