package e;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.List;

public final class j
{
  private final Method a;
  private final List b;
  
  j(Method paramMethod, List paramList)
  {
    a = paramMethod;
    paramMethod = Collections.unmodifiableList(paramList);
    b = paramMethod;
  }
  
  public final Method a()
  {
    return a;
  }
  
  public final String toString()
  {
    Object[] arrayOfObject = new Object[3];
    Object localObject = a.getDeclaringClass().getName();
    arrayOfObject[0] = localObject;
    localObject = a.getName();
    arrayOfObject[1] = localObject;
    localObject = b;
    arrayOfObject[2] = localObject;
    return String.format("%s.%s() %s", arrayOfObject);
  }
}

/* Location:
 * Qualified Name:     e.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */