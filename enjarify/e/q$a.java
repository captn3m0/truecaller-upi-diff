package e;

import e.b.a;
import e.b.d;
import e.b.i;
import e.b.j;
import e.b.q;
import e.b.r;
import e.b.v;
import e.b.x;
import e.b.y;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.URI;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import okhttp3.t.a;
import okhttp3.x.b;

final class q$a
{
  private static final Pattern x = Pattern.compile("\\{([a-zA-Z][a-zA-Z0-9_-]*)\\}");
  private static final Pattern y = Pattern.compile("[a-zA-Z][a-zA-Z0-9_-]*");
  final s a;
  final Method b;
  final Annotation[] c;
  final Annotation[][] d;
  final Type[] e;
  boolean f;
  boolean g;
  boolean h;
  boolean i;
  boolean j;
  boolean k;
  boolean l;
  boolean m;
  String n;
  boolean o;
  boolean p;
  boolean q;
  String r;
  okhttp3.t s;
  okhttp3.w t;
  Set u;
  n[] v;
  boolean w;
  
  q$a(s params, Method paramMethod)
  {
    a = params;
    b = paramMethod;
    params = paramMethod.getAnnotations();
    c = params;
    params = paramMethod.getGenericParameterTypes();
    e = params;
    params = paramMethod.getParameterAnnotations();
    d = params;
  }
  
  private n a(int paramInt, Type paramType, Annotation[] paramArrayOfAnnotation, Annotation paramAnnotation)
  {
    boolean bool1 = paramAnnotation instanceof y;
    int i1 = 1;
    boolean bool3;
    if (bool1)
    {
      a(paramInt, paramType);
      bool2 = m;
      if (!bool2)
      {
        bool2 = i;
        if (!bool2)
        {
          bool2 = j;
          if (!bool2)
          {
            bool2 = k;
            if (!bool2)
            {
              bool2 = l;
              if (!bool2)
              {
                paramArrayOfAnnotation = r;
                if (paramArrayOfAnnotation == null)
                {
                  m = i1;
                  paramArrayOfAnnotation = okhttp3.u.class;
                  if (paramType != paramArrayOfAnnotation)
                  {
                    paramArrayOfAnnotation = String.class;
                    if (paramType != paramArrayOfAnnotation)
                    {
                      paramArrayOfAnnotation = URI.class;
                      if (paramType != paramArrayOfAnnotation)
                      {
                        bool2 = paramType instanceof Class;
                        if (bool2)
                        {
                          paramArrayOfAnnotation = "android.net.Uri";
                          paramType = ((Class)paramType).getName();
                          bool3 = paramArrayOfAnnotation.equals(paramType);
                          if (bool3) {}
                        }
                        else
                        {
                          paramType = b;
                          paramArrayOfAnnotation = new Object[0];
                          throw w.a(paramType, paramInt, "@Url must be okhttp3.HttpUrl, String, java.net.URI, or android.net.Uri type.", paramArrayOfAnnotation);
                        }
                      }
                    }
                  }
                  paramType = new e/n$n;
                  paramArrayOfAnnotation = b;
                  paramType.<init>(paramArrayOfAnnotation, paramInt);
                  return paramType;
                }
                paramType = b;
                paramArrayOfAnnotation = new Object[i1];
                paramAnnotation = n;
                paramArrayOfAnnotation[0] = paramAnnotation;
                throw w.a(paramType, paramInt, "@Url cannot be used with @%s URL", paramArrayOfAnnotation);
              }
              paramType = b;
              paramArrayOfAnnotation = new Object[0];
              throw w.a(paramType, paramInt, "A @Url parameter must not come after a @QueryMap.", paramArrayOfAnnotation);
            }
            paramType = b;
            paramArrayOfAnnotation = new Object[0];
            throw w.a(paramType, paramInt, "A @Url parameter must not come after a @QueryName.", paramArrayOfAnnotation);
          }
          paramType = b;
          paramArrayOfAnnotation = new Object[0];
          throw w.a(paramType, paramInt, "A @Url parameter must not come after a @Query.", paramArrayOfAnnotation);
        }
        paramType = b;
        paramArrayOfAnnotation = new Object[0];
        throw w.a(paramType, paramInt, "@Path parameters may not be used with @Url.", paramArrayOfAnnotation);
      }
      paramType = b;
      paramArrayOfAnnotation = new Object[0];
      throw w.a(paramType, paramInt, "Multiple @Url method annotations found.", paramArrayOfAnnotation);
    }
    bool1 = paramAnnotation instanceof e.b.s;
    int i5 = 2;
    Object localObject1;
    String str;
    Object localObject2;
    Object localObject3;
    if (bool1)
    {
      a(paramInt, paramType);
      bool1 = j;
      if (!bool1)
      {
        bool1 = k;
        if (!bool1)
        {
          bool1 = l;
          if (!bool1)
          {
            bool1 = m;
            if (!bool1)
            {
              localObject1 = r;
              if (localObject1 != null)
              {
                i = i1;
                paramAnnotation = (e.b.s)paramAnnotation;
                str = paramAnnotation.a();
                localObject1 = y.matcher(str);
                bool1 = ((Matcher)localObject1).matches();
                if (bool1)
                {
                  localObject1 = u;
                  bool1 = ((Set)localObject1).contains(str);
                  if (bool1)
                  {
                    f localf = a.b(paramType, paramArrayOfAnnotation);
                    paramType = new e/n$i;
                    localObject2 = b;
                    boolean bool5 = paramAnnotation.b();
                    localObject3 = paramType;
                    paramType.<init>((Method)localObject2, paramInt, str, localf, bool5);
                    return paramType;
                  }
                  paramType = b;
                  paramArrayOfAnnotation = new Object[i5];
                  paramAnnotation = r;
                  paramArrayOfAnnotation[0] = paramAnnotation;
                  paramArrayOfAnnotation[i1] = str;
                  throw w.a(paramType, paramInt, "URL \"%s\" does not contain \"{%s}\".", paramArrayOfAnnotation);
                }
                paramType = b;
                paramArrayOfAnnotation = new Object[i5];
                paramAnnotation = x.pattern();
                paramArrayOfAnnotation[0] = paramAnnotation;
                paramArrayOfAnnotation[i1] = str;
                throw w.a(paramType, paramInt, "@Path parameter name must match %s. Found: %s", paramArrayOfAnnotation);
              }
              paramType = b;
              paramArrayOfAnnotation = new Object[i1];
              paramAnnotation = n;
              paramArrayOfAnnotation[0] = paramAnnotation;
              throw w.a(paramType, paramInt, "@Path can only be used with relative url on @%s", paramArrayOfAnnotation);
            }
            paramType = b;
            paramArrayOfAnnotation = new Object[0];
            throw w.a(paramType, paramInt, "@Path parameters may not be used with @Url.", paramArrayOfAnnotation);
          }
          paramType = b;
          paramArrayOfAnnotation = new Object[0];
          throw w.a(paramType, paramInt, "A @Path parameter must not come after a @QueryMap.", paramArrayOfAnnotation);
        }
        paramType = b;
        paramArrayOfAnnotation = new Object[0];
        throw w.a(paramType, paramInt, "A @Path parameter must not come after a @QueryName.", paramArrayOfAnnotation);
      }
      paramType = b;
      paramArrayOfAnnotation = new Object[0];
      throw w.a(paramType, paramInt, "A @Path parameter must not come after a @Query.", paramArrayOfAnnotation);
    }
    bool1 = paramAnnotation instanceof e.b.t;
    boolean bool6;
    Class localClass;
    Object localObject4;
    Object localObject5;
    if (bool1)
    {
      a(paramInt, paramType);
      paramAnnotation = (e.b.t)paramAnnotation;
      localObject1 = paramAnnotation.a();
      bool6 = paramAnnotation.b();
      localClass = w.a(paramType);
      j = i1;
      localObject4 = Iterable.class;
      i1 = ((Class)localObject4).isAssignableFrom(localClass);
      if (i1 != 0)
      {
        i1 = paramType instanceof ParameterizedType;
        if (i1 != 0)
        {
          paramType = (ParameterizedType)paramType;
          localObject5 = w.a(0, paramType);
          localObject5 = a.b((Type)localObject5, paramArrayOfAnnotation);
          paramType = new e/n$j;
          paramType.<init>((String)localObject1, (f)localObject5, bool6);
          return paramType.a();
        }
        paramType = b;
        paramArrayOfAnnotation = new java/lang/StringBuilder;
        paramArrayOfAnnotation.<init>();
        paramAnnotation = localClass.getSimpleName();
        paramArrayOfAnnotation.append(paramAnnotation);
        paramArrayOfAnnotation.append(" must include generic type (e.g., ");
        paramAnnotation = localClass.getSimpleName();
        paramArrayOfAnnotation.append(paramAnnotation);
        paramArrayOfAnnotation.append("<String>)");
        paramArrayOfAnnotation = paramArrayOfAnnotation.toString();
        paramAnnotation = new Object[0];
        throw w.a(paramType, paramInt, paramArrayOfAnnotation, paramAnnotation);
      }
      paramInt = localClass.isArray();
      if (paramInt != 0)
      {
        localObject5 = a(localClass.getComponentType());
        localObject5 = a.b((Type)localObject5, paramArrayOfAnnotation);
        paramType = new e/n$j;
        paramType.<init>((String)localObject1, (f)localObject5, bool6);
        return paramType.b();
      }
      localObject5 = a.b(paramType, paramArrayOfAnnotation);
      paramType = new e/n$j;
      paramType.<init>((String)localObject1, (f)localObject5, bool6);
      return paramType;
    }
    bool1 = paramAnnotation instanceof v;
    if (bool1)
    {
      a(paramInt, paramType);
      paramAnnotation = (v)paramAnnotation;
      bool6 = paramAnnotation.a();
      localObject1 = w.a(paramType);
      k = i1;
      localObject4 = Iterable.class;
      i1 = ((Class)localObject4).isAssignableFrom((Class)localObject1);
      if (i1 != 0)
      {
        i1 = paramType instanceof ParameterizedType;
        if (i1 != 0)
        {
          paramType = (ParameterizedType)paramType;
          localObject5 = w.a(0, paramType);
          localObject5 = a.b((Type)localObject5, paramArrayOfAnnotation);
          paramType = new e/n$l;
          paramType.<init>((f)localObject5, bool6);
          return paramType.a();
        }
        paramType = b;
        paramArrayOfAnnotation = new java/lang/StringBuilder;
        paramArrayOfAnnotation.<init>();
        paramAnnotation = ((Class)localObject1).getSimpleName();
        paramArrayOfAnnotation.append(paramAnnotation);
        paramArrayOfAnnotation.append(" must include generic type (e.g., ");
        paramAnnotation = ((Class)localObject1).getSimpleName();
        paramArrayOfAnnotation.append(paramAnnotation);
        paramArrayOfAnnotation.append("<String>)");
        paramArrayOfAnnotation = paramArrayOfAnnotation.toString();
        paramAnnotation = new Object[0];
        throw w.a(paramType, paramInt, paramArrayOfAnnotation, paramAnnotation);
      }
      paramInt = ((Class)localObject1).isArray();
      if (paramInt != 0)
      {
        localObject5 = a(((Class)localObject1).getComponentType());
        localObject5 = a.b((Type)localObject5, paramArrayOfAnnotation);
        paramType = new e/n$l;
        paramType.<init>((f)localObject5, bool6);
        return paramType.b();
      }
      localObject5 = a.b(paramType, paramArrayOfAnnotation);
      paramType = new e/n$l;
      paramType.<init>((f)localObject5, bool6);
      return paramType;
    }
    bool1 = paramAnnotation instanceof e.b.u;
    boolean bool4;
    if (bool1)
    {
      a(paramInt, paramType);
      localObject1 = w.a(paramType);
      l = i1;
      localClass = Map.class;
      bool4 = localClass.isAssignableFrom((Class)localObject1);
      if (bool4)
      {
        localClass = Map.class;
        paramType = w.a(paramType, (Class)localObject1, localClass);
        bool1 = paramType instanceof ParameterizedType;
        if (bool1)
        {
          paramType = (ParameterizedType)paramType;
          localObject1 = w.a(0, paramType);
          localClass = String.class;
          if (localClass == localObject1)
          {
            paramType = w.a(i1, paramType);
            paramType = a.b(paramType, paramArrayOfAnnotation);
            paramArrayOfAnnotation = new e/n$k;
            localObject1 = b;
            bool6 = ((e.b.u)paramAnnotation).a();
            paramArrayOfAnnotation.<init>((Method)localObject1, paramInt, paramType, bool6);
            return paramArrayOfAnnotation;
          }
          paramType = b;
          paramArrayOfAnnotation = String.valueOf(localObject1);
          paramArrayOfAnnotation = "@QueryMap keys must be of type String: ".concat(paramArrayOfAnnotation);
          paramAnnotation = new Object[0];
          throw w.a(paramType, paramInt, paramArrayOfAnnotation, paramAnnotation);
        }
        paramType = b;
        paramArrayOfAnnotation = new Object[0];
        throw w.a(paramType, paramInt, "Map must include generic types (e.g., Map<String, String>)", paramArrayOfAnnotation);
      }
      paramType = b;
      paramArrayOfAnnotation = new Object[0];
      throw w.a(paramType, paramInt, "@QueryMap parameter type must be Map.", paramArrayOfAnnotation);
    }
    bool1 = paramAnnotation instanceof i;
    int i2;
    if (bool1)
    {
      a(paramInt, paramType);
      paramAnnotation = ((i)paramAnnotation).a();
      localObject1 = w.a(paramType);
      localObject4 = Iterable.class;
      i2 = ((Class)localObject4).isAssignableFrom((Class)localObject1);
      if (i2 != 0)
      {
        i2 = paramType instanceof ParameterizedType;
        if (i2 != 0)
        {
          paramType = (ParameterizedType)paramType;
          localObject5 = w.a(0, paramType);
          localObject5 = a.b((Type)localObject5, paramArrayOfAnnotation);
          paramType = new e/n$d;
          paramType.<init>(paramAnnotation, (f)localObject5);
          return paramType.a();
        }
        paramType = b;
        paramArrayOfAnnotation = new java/lang/StringBuilder;
        paramArrayOfAnnotation.<init>();
        paramAnnotation = ((Class)localObject1).getSimpleName();
        paramArrayOfAnnotation.append(paramAnnotation);
        paramArrayOfAnnotation.append(" must include generic type (e.g., ");
        paramAnnotation = ((Class)localObject1).getSimpleName();
        paramArrayOfAnnotation.append(paramAnnotation);
        paramArrayOfAnnotation.append("<String>)");
        paramArrayOfAnnotation = paramArrayOfAnnotation.toString();
        paramAnnotation = new Object[0];
        throw w.a(paramType, paramInt, paramArrayOfAnnotation, paramAnnotation);
      }
      paramInt = ((Class)localObject1).isArray();
      if (paramInt != 0)
      {
        localObject5 = a(((Class)localObject1).getComponentType());
        localObject5 = a.b((Type)localObject5, paramArrayOfAnnotation);
        paramType = new e/n$d;
        paramType.<init>(paramAnnotation, (f)localObject5);
        return paramType.b();
      }
      localObject5 = a.b(paramType, paramArrayOfAnnotation);
      paramType = new e/n$d;
      paramType.<init>(paramAnnotation, (f)localObject5);
      return paramType;
    }
    bool1 = paramAnnotation instanceof j;
    if (bool1)
    {
      paramAnnotation = okhttp3.t.class;
      if (paramType == paramAnnotation)
      {
        paramType = new e/n$f;
        paramArrayOfAnnotation = b;
        paramType.<init>(paramArrayOfAnnotation, paramInt);
        return paramType;
      }
      a(paramInt, paramType);
      paramAnnotation = w.a(paramType);
      localObject1 = Map.class;
      bool1 = ((Class)localObject1).isAssignableFrom(paramAnnotation);
      if (bool1)
      {
        localObject1 = Map.class;
        paramType = w.a(paramType, paramAnnotation, (Class)localObject1);
        bool6 = paramType instanceof ParameterizedType;
        if (bool6)
        {
          paramType = (ParameterizedType)paramType;
          paramAnnotation = w.a(0, paramType);
          localObject1 = String.class;
          if (localObject1 == paramAnnotation)
          {
            paramType = w.a(i2, paramType);
            paramType = a.b(paramType, paramArrayOfAnnotation);
            paramArrayOfAnnotation = new e/n$e;
            paramAnnotation = b;
            paramArrayOfAnnotation.<init>(paramAnnotation, paramInt, paramType);
            return paramArrayOfAnnotation;
          }
          paramType = b;
          paramArrayOfAnnotation = String.valueOf(paramAnnotation);
          paramArrayOfAnnotation = "@HeaderMap keys must be of type String: ".concat(paramArrayOfAnnotation);
          paramAnnotation = new Object[0];
          throw w.a(paramType, paramInt, paramArrayOfAnnotation, paramAnnotation);
        }
        paramType = b;
        paramArrayOfAnnotation = new Object[0];
        throw w.a(paramType, paramInt, "Map must include generic types (e.g., Map<String, String>)", paramArrayOfAnnotation);
      }
      paramType = b;
      paramArrayOfAnnotation = new Object[0];
      throw w.a(paramType, paramInt, "@HeaderMap parameter type must be Map.", paramArrayOfAnnotation);
    }
    bool1 = paramAnnotation instanceof e.b.c;
    if (bool1)
    {
      a(paramInt, paramType);
      bool1 = p;
      if (bool1)
      {
        paramAnnotation = (e.b.c)paramAnnotation;
        localObject1 = paramAnnotation.a();
        bool6 = paramAnnotation.b();
        f = i2;
        localObject4 = w.a(paramType);
        localClass = Iterable.class;
        bool4 = localClass.isAssignableFrom((Class)localObject4);
        if (bool4)
        {
          bool4 = paramType instanceof ParameterizedType;
          if (bool4)
          {
            paramType = (ParameterizedType)paramType;
            localObject5 = w.a(0, paramType);
            localObject5 = a.b((Type)localObject5, paramArrayOfAnnotation);
            paramType = new e/n$b;
            paramType.<init>((String)localObject1, (f)localObject5, bool6);
            return paramType.a();
          }
          paramType = b;
          paramArrayOfAnnotation = new java/lang/StringBuilder;
          paramArrayOfAnnotation.<init>();
          paramAnnotation = ((Class)localObject4).getSimpleName();
          paramArrayOfAnnotation.append(paramAnnotation);
          paramArrayOfAnnotation.append(" must include generic type (e.g., ");
          paramAnnotation = ((Class)localObject4).getSimpleName();
          paramArrayOfAnnotation.append(paramAnnotation);
          paramArrayOfAnnotation.append("<String>)");
          paramArrayOfAnnotation = paramArrayOfAnnotation.toString();
          paramAnnotation = new Object[0];
          throw w.a(paramType, paramInt, paramArrayOfAnnotation, paramAnnotation);
        }
        paramInt = ((Class)localObject4).isArray();
        if (paramInt != 0)
        {
          localObject5 = a(((Class)localObject4).getComponentType());
          localObject5 = a.b((Type)localObject5, paramArrayOfAnnotation);
          paramType = new e/n$b;
          paramType.<init>((String)localObject1, (f)localObject5, bool6);
          return paramType.b();
        }
        localObject5 = a.b(paramType, paramArrayOfAnnotation);
        paramType = new e/n$b;
        paramType.<init>((String)localObject1, (f)localObject5, bool6);
        return paramType;
      }
      paramType = b;
      paramArrayOfAnnotation = new Object[0];
      throw w.a(paramType, paramInt, "@Field parameters can only be used with form encoding.", paramArrayOfAnnotation);
    }
    bool1 = paramAnnotation instanceof d;
    if (bool1)
    {
      a(paramInt, paramType);
      bool1 = p;
      if (bool1)
      {
        localObject1 = w.a(paramType);
        localClass = Map.class;
        bool4 = localClass.isAssignableFrom((Class)localObject1);
        if (bool4)
        {
          localClass = Map.class;
          paramType = w.a(paramType, (Class)localObject1, localClass);
          bool1 = paramType instanceof ParameterizedType;
          if (bool1)
          {
            paramType = (ParameterizedType)paramType;
            localObject1 = w.a(0, paramType);
            localClass = String.class;
            if (localClass == localObject1)
            {
              paramType = w.a(i2, paramType);
              paramType = a.b(paramType, paramArrayOfAnnotation);
              f = i2;
              paramArrayOfAnnotation = new e/n$c;
              localObject1 = b;
              bool6 = ((d)paramAnnotation).a();
              paramArrayOfAnnotation.<init>((Method)localObject1, paramInt, paramType, bool6);
              return paramArrayOfAnnotation;
            }
            paramType = b;
            paramArrayOfAnnotation = String.valueOf(localObject1);
            paramArrayOfAnnotation = "@FieldMap keys must be of type String: ".concat(paramArrayOfAnnotation);
            paramAnnotation = new Object[0];
            throw w.a(paramType, paramInt, paramArrayOfAnnotation, paramAnnotation);
          }
          paramType = b;
          paramArrayOfAnnotation = new Object[0];
          throw w.a(paramType, paramInt, "Map must include generic types (e.g., Map<String, String>)", paramArrayOfAnnotation);
        }
        paramType = b;
        paramArrayOfAnnotation = new Object[0];
        throw w.a(paramType, paramInt, "@FieldMap parameter type must be Map.", paramArrayOfAnnotation);
      }
      paramType = b;
      paramArrayOfAnnotation = new Object[0];
      throw w.a(paramType, paramInt, "@FieldMap parameters can only be used with form encoding.", paramArrayOfAnnotation);
    }
    bool1 = paramAnnotation instanceof q;
    if (bool1)
    {
      a(paramInt, paramType);
      bool1 = q;
      if (bool1)
      {
        paramAnnotation = (q)paramAnnotation;
        g = i2;
        localObject1 = paramAnnotation.a();
        localObject3 = w.a(paramType);
        boolean bool7 = ((String)localObject1).isEmpty();
        if (bool7)
        {
          paramArrayOfAnnotation = Iterable.class;
          bool2 = paramArrayOfAnnotation.isAssignableFrom((Class)localObject3);
          if (bool2)
          {
            bool2 = paramType instanceof ParameterizedType;
            if (bool2)
            {
              paramType = (ParameterizedType)paramType;
              paramType = w.a(0, paramType);
              paramArrayOfAnnotation = x.b.class;
              paramType = w.a(paramType);
              bool3 = paramArrayOfAnnotation.isAssignableFrom(paramType);
              if (bool3) {
                return n.m.a.a();
              }
              paramType = b;
              paramArrayOfAnnotation = new Object[0];
              throw w.a(paramType, paramInt, "@Part annotation must supply a name or use MultipartBody.Part parameter type.", paramArrayOfAnnotation);
            }
            paramType = b;
            paramArrayOfAnnotation = new java/lang/StringBuilder;
            paramArrayOfAnnotation.<init>();
            paramAnnotation = ((Class)localObject3).getSimpleName();
            paramArrayOfAnnotation.append(paramAnnotation);
            paramArrayOfAnnotation.append(" must include generic type (e.g., ");
            paramAnnotation = ((Class)localObject3).getSimpleName();
            paramArrayOfAnnotation.append(paramAnnotation);
            paramArrayOfAnnotation.append("<String>)");
            paramArrayOfAnnotation = paramArrayOfAnnotation.toString();
            paramAnnotation = new Object[0];
            throw w.a(paramType, paramInt, paramArrayOfAnnotation, paramAnnotation);
          }
          bool3 = ((Class)localObject3).isArray();
          if (bool3)
          {
            paramType = ((Class)localObject3).getComponentType();
            paramArrayOfAnnotation = x.b.class;
            bool3 = paramArrayOfAnnotation.isAssignableFrom(paramType);
            if (bool3) {
              return n.m.a.b();
            }
            paramType = b;
            paramArrayOfAnnotation = new Object[0];
            throw w.a(paramType, paramInt, "@Part annotation must supply a name or use MultipartBody.Part parameter type.", paramArrayOfAnnotation);
          }
          paramType = x.b.class;
          bool3 = paramType.isAssignableFrom((Class)localObject3);
          if (bool3) {
            return n.m.a;
          }
          paramType = b;
          paramArrayOfAnnotation = new Object[0];
          throw w.a(paramType, paramInt, "@Part annotation must supply a name or use MultipartBody.Part parameter type.", paramArrayOfAnnotation);
        }
        int i6 = 4;
        localObject2 = new String[i6];
        localObject2[0] = "Content-Disposition";
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        str = "form-data; name=\"";
        localStringBuilder.<init>(str);
        localStringBuilder.append((String)localObject1);
        localStringBuilder.append("\"");
        localObject1 = localStringBuilder.toString();
        localObject2[i2] = localObject1;
        localObject2[bool4] = "Content-Transfer-Encoding";
        paramAnnotation = paramAnnotation.b();
        localObject2[3] = paramAnnotation;
        paramAnnotation = okhttp3.t.a((String[])localObject2);
        localObject1 = Iterable.class;
        bool1 = ((Class)localObject1).isAssignableFrom((Class)localObject3);
        if (bool1)
        {
          bool1 = paramType instanceof ParameterizedType;
          if (bool1)
          {
            paramType = (ParameterizedType)paramType;
            paramType = w.a(0, paramType);
            localObject1 = x.b.class;
            localObject4 = w.a(paramType);
            bool1 = ((Class)localObject1).isAssignableFrom((Class)localObject4);
            if (!bool1)
            {
              localObject1 = a;
              localObject4 = c;
              paramType = ((s)localObject1).a(paramType, paramArrayOfAnnotation, (Annotation[])localObject4);
              paramArrayOfAnnotation = new e/n$g;
              localObject1 = b;
              paramArrayOfAnnotation.<init>((Method)localObject1, paramInt, paramAnnotation, paramType);
              return paramArrayOfAnnotation.a();
            }
            paramType = b;
            paramArrayOfAnnotation = new Object[0];
            throw w.a(paramType, paramInt, "@Part parameters using the MultipartBody.Part must not include a part name in the annotation.", paramArrayOfAnnotation);
          }
          paramType = b;
          paramArrayOfAnnotation = new java/lang/StringBuilder;
          paramArrayOfAnnotation.<init>();
          paramAnnotation = ((Class)localObject3).getSimpleName();
          paramArrayOfAnnotation.append(paramAnnotation);
          paramArrayOfAnnotation.append(" must include generic type (e.g., ");
          paramAnnotation = ((Class)localObject3).getSimpleName();
          paramArrayOfAnnotation.append(paramAnnotation);
          paramArrayOfAnnotation.append("<String>)");
          paramArrayOfAnnotation = paramArrayOfAnnotation.toString();
          paramAnnotation = new Object[0];
          throw w.a(paramType, paramInt, paramArrayOfAnnotation, paramAnnotation);
        }
        bool1 = ((Class)localObject3).isArray();
        if (bool1)
        {
          paramType = a(((Class)localObject3).getComponentType());
          localObject1 = x.b.class;
          bool1 = ((Class)localObject1).isAssignableFrom(paramType);
          if (!bool1)
          {
            localObject1 = a;
            localObject4 = c;
            paramType = ((s)localObject1).a(paramType, paramArrayOfAnnotation, (Annotation[])localObject4);
            paramArrayOfAnnotation = new e/n$g;
            localObject1 = b;
            paramArrayOfAnnotation.<init>((Method)localObject1, paramInt, paramAnnotation, paramType);
            return paramArrayOfAnnotation.b();
          }
          paramType = b;
          paramArrayOfAnnotation = new Object[0];
          throw w.a(paramType, paramInt, "@Part parameters using the MultipartBody.Part must not include a part name in the annotation.", paramArrayOfAnnotation);
        }
        localObject1 = x.b.class;
        bool1 = ((Class)localObject1).isAssignableFrom((Class)localObject3);
        if (!bool1)
        {
          localObject1 = a;
          localObject4 = c;
          paramType = ((s)localObject1).a(paramType, paramArrayOfAnnotation, (Annotation[])localObject4);
          paramArrayOfAnnotation = new e/n$g;
          localObject1 = b;
          paramArrayOfAnnotation.<init>((Method)localObject1, paramInt, paramAnnotation, paramType);
          return paramArrayOfAnnotation;
        }
        paramType = b;
        paramArrayOfAnnotation = new Object[0];
        throw w.a(paramType, paramInt, "@Part parameters using the MultipartBody.Part must not include a part name in the annotation.", paramArrayOfAnnotation);
      }
      paramType = b;
      paramArrayOfAnnotation = new Object[0];
      throw w.a(paramType, paramInt, "@Part parameters can only be used with multipart encoding.", paramArrayOfAnnotation);
    }
    bool1 = paramAnnotation instanceof r;
    if (bool1)
    {
      a(paramInt, paramType);
      bool1 = q;
      if (bool1)
      {
        g = i2;
        localObject1 = w.a(paramType);
        localClass = Map.class;
        bool4 = localClass.isAssignableFrom((Class)localObject1);
        if (bool4)
        {
          localClass = Map.class;
          paramType = w.a(paramType, (Class)localObject1, localClass);
          bool1 = paramType instanceof ParameterizedType;
          if (bool1)
          {
            paramType = (ParameterizedType)paramType;
            localObject1 = w.a(0, paramType);
            localClass = String.class;
            if (localClass == localObject1)
            {
              paramType = w.a(i2, paramType);
              localObject1 = x.b.class;
              localObject4 = w.a(paramType);
              bool1 = ((Class)localObject1).isAssignableFrom((Class)localObject4);
              if (!bool1)
              {
                localObject1 = a;
                localObject4 = c;
                paramType = ((s)localObject1).a(paramType, paramArrayOfAnnotation, (Annotation[])localObject4);
                paramAnnotation = (r)paramAnnotation;
                paramArrayOfAnnotation = new e/n$h;
                localObject1 = b;
                paramAnnotation = paramAnnotation.a();
                paramArrayOfAnnotation.<init>((Method)localObject1, paramInt, paramType, paramAnnotation);
                return paramArrayOfAnnotation;
              }
              paramType = b;
              paramArrayOfAnnotation = new Object[0];
              throw w.a(paramType, paramInt, "@PartMap values cannot be MultipartBody.Part. Use @Part List<Part> or a different value type instead.", paramArrayOfAnnotation);
            }
            paramType = b;
            paramArrayOfAnnotation = String.valueOf(localObject1);
            paramArrayOfAnnotation = "@PartMap keys must be of type String: ".concat(paramArrayOfAnnotation);
            paramAnnotation = new Object[0];
            throw w.a(paramType, paramInt, paramArrayOfAnnotation, paramAnnotation);
          }
          paramType = b;
          paramArrayOfAnnotation = new Object[0];
          throw w.a(paramType, paramInt, "Map must include generic types (e.g., Map<String, String>)", paramArrayOfAnnotation);
        }
        paramType = b;
        paramArrayOfAnnotation = new Object[0];
        throw w.a(paramType, paramInt, "@PartMap parameter type must be Map.", paramArrayOfAnnotation);
      }
      paramType = b;
      paramArrayOfAnnotation = new Object[0];
      throw w.a(paramType, paramInt, "@PartMap parameters can only be used with multipart encoding.", paramArrayOfAnnotation);
    }
    bool1 = paramAnnotation instanceof a;
    if (bool1)
    {
      a(paramInt, paramType);
      bool6 = p;
      if (!bool6)
      {
        bool6 = q;
        if (!bool6)
        {
          bool6 = h;
          if (!bool6) {
            try
            {
              paramAnnotation = a;
              localObject1 = c;
              paramType = paramAnnotation.a(paramType, paramArrayOfAnnotation, (Annotation[])localObject1);
              h = i2;
              paramArrayOfAnnotation = new e/n$a;
              paramAnnotation = b;
              paramArrayOfAnnotation.<init>(paramAnnotation, paramInt, paramType);
              return paramArrayOfAnnotation;
            }
            catch (RuntimeException paramArrayOfAnnotation)
            {
              paramAnnotation = b;
              localObject1 = new Object[i2];
              localObject1[0] = paramType;
              throw w.a(paramAnnotation, paramArrayOfAnnotation, paramInt, "Unable to create @Body converter for %s", (Object[])localObject1);
            }
          }
          paramType = b;
          paramArrayOfAnnotation = new Object[0];
          throw w.a(paramType, paramInt, "Multiple @Body method annotations found.", paramArrayOfAnnotation);
        }
      }
      paramType = b;
      paramArrayOfAnnotation = new Object[0];
      throw w.a(paramType, paramInt, "@Body parameters cannot be used with form or multi-part encoding.", paramArrayOfAnnotation);
    }
    boolean bool2 = paramAnnotation instanceof x;
    if (bool2)
    {
      a(paramInt, paramType);
      paramType = w.a(paramType);
      int i3 = paramInt + -1;
      while (i3 >= 0)
      {
        paramAnnotation = v[i3];
        bool1 = paramAnnotation instanceof n.o;
        int i4;
        if (bool1)
        {
          paramAnnotation = a;
          bool6 = paramAnnotation.equals(paramType);
          if (bool6)
          {
            paramAnnotation = b;
            localObject1 = new java/lang/StringBuilder;
            ((StringBuilder)localObject1).<init>("@Tag type ");
            paramType = paramType.getName();
            ((StringBuilder)localObject1).append(paramType);
            ((StringBuilder)localObject1).append(" is duplicate of parameter #");
            i3 += i2;
            ((StringBuilder)localObject1).append(i4);
            ((StringBuilder)localObject1).append(" and would always overwrite its value.");
            paramType = ((StringBuilder)localObject1).toString();
            paramArrayOfAnnotation = new Object[0];
            throw w.a(paramAnnotation, paramInt, paramType, paramArrayOfAnnotation);
          }
        }
        i4 += -1;
      }
      localObject5 = new e/n$o;
      ((n.o)localObject5).<init>(paramType);
      return (n)localObject5;
    }
    return null;
  }
  
  private static Class a(Class paramClass)
  {
    Class localClass = Boolean.TYPE;
    if (localClass == paramClass) {
      return Boolean.class;
    }
    localClass = Byte.TYPE;
    if (localClass == paramClass) {
      return Byte.class;
    }
    localClass = Character.TYPE;
    if (localClass == paramClass) {
      return Character.class;
    }
    localClass = Double.TYPE;
    if (localClass == paramClass) {
      return Double.class;
    }
    localClass = Float.TYPE;
    if (localClass == paramClass) {
      return Float.class;
    }
    localClass = Integer.TYPE;
    if (localClass == paramClass) {
      return Integer.class;
    }
    localClass = Long.TYPE;
    if (localClass == paramClass) {
      return Long.class;
    }
    localClass = Short.TYPE;
    if (localClass == paramClass) {
      return Short.class;
    }
    return paramClass;
  }
  
  private void a(int paramInt, Type paramType)
  {
    boolean bool = w.d(paramType);
    if (!bool) {
      return;
    }
    Method localMethod = b;
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = paramType;
    throw w.a(localMethod, paramInt, "Parameter type must not include a type variable or wildcard: %s", arrayOfObject);
  }
  
  final n a(int paramInt, Type paramType, Annotation[] paramArrayOfAnnotation, boolean paramBoolean)
  {
    if (paramArrayOfAnnotation != null)
    {
      int i1 = paramArrayOfAnnotation.length;
      localObject1 = null;
      int i2 = 0;
      while (i2 < i1)
      {
        Object localObject2 = paramArrayOfAnnotation[i2];
        localObject2 = a(paramInt, paramType, paramArrayOfAnnotation, (Annotation)localObject2);
        if (localObject2 != null) {
          if (localObject1 == null)
          {
            localObject1 = localObject2;
          }
          else
          {
            paramType = b;
            paramArrayOfAnnotation = new Object[0];
            throw w.a(paramType, paramInt, "Multiple Retrofit annotations found, only one allowed.", paramArrayOfAnnotation);
          }
        }
        i2 += 1;
      }
    }
    Object localObject1 = null;
    if (localObject1 == null)
    {
      if (paramBoolean) {
        try
        {
          paramType = w.a(paramType);
          paramArrayOfAnnotation = c.d.c.class;
          if (paramType == paramArrayOfAnnotation)
          {
            boolean bool = true;
            w = bool;
            return null;
          }
        }
        catch (NoClassDefFoundError localNoClassDefFoundError) {}
      }
      paramType = b;
      paramArrayOfAnnotation = new Object[0];
      throw w.a(paramType, paramInt, "No Retrofit annotation found.", paramArrayOfAnnotation);
    }
    return (n)localObject1;
  }
  
  final okhttp3.t a(String[] paramArrayOfString)
  {
    Object localObject1 = new okhttp3/t$a;
    ((t.a)localObject1).<init>();
    int i1 = paramArrayOfString.length;
    int i2 = 0;
    while (i2 < i1)
    {
      String str1 = paramArrayOfString[i2];
      int i3 = str1.indexOf(':');
      int i4 = -1;
      int i5 = 1;
      if ((i3 != i4) && (i3 != 0))
      {
        i4 = str1.length() - i5;
        if (i3 != i4)
        {
          String str2 = str1.substring(0, i3);
          i3 += 1;
          str1 = str1.substring(i3).trim();
          Object localObject2 = "Content-Type";
          boolean bool = ((String)localObject2).equalsIgnoreCase(str2);
          if (bool) {
            try
            {
              localObject2 = okhttp3.w.a(str1);
              t = ((okhttp3.w)localObject2);
            }
            catch (IllegalArgumentException paramArrayOfString)
            {
              localObject1 = b;
              Object[] arrayOfObject = new Object[i5];
              arrayOfObject[0] = str1;
              throw w.a((Method)localObject1, paramArrayOfString, "Malformed content type: %s", arrayOfObject);
            }
          } else {
            ((t.a)localObject1).a(str2, str1);
          }
          i2 += 1;
          continue;
        }
      }
      paramArrayOfString = b;
      localObject1 = new Object[i5];
      localObject1[0] = str1;
      throw w.a(paramArrayOfString, "@Headers value must be in the form \"Name: Value\". Found: \"%s\"", (Object[])localObject1);
    }
    return ((t.a)localObject1).a();
  }
  
  final void a(String paramString1, String paramString2, boolean paramBoolean)
  {
    String str = n;
    boolean bool1 = true;
    if (str == null)
    {
      n = paramString1;
      o = paramBoolean;
      boolean bool2 = paramString2.isEmpty();
      if (bool2) {
        return;
      }
      boolean bool3 = paramString2.indexOf('?');
      paramBoolean = true;
      if (bool3 != paramBoolean)
      {
        paramBoolean = paramString2.length() - bool1;
        if (bool3 < paramBoolean)
        {
          int i1;
          bool3 += bool1;
          paramString1 = paramString2.substring(i1);
          localObject = x.matcher(paramString1);
          paramBoolean = ((Matcher)localObject).find();
          if (paramBoolean)
          {
            paramString2 = b;
            localObject = new Object[bool1];
            localObject[0] = paramString1;
            throw w.a(paramString2, "URL query string \"%s\" must not have replace block. For dynamic query parameters use @Query.", (Object[])localObject);
          }
        }
      }
      r = paramString2;
      paramString1 = x.matcher(paramString2);
      paramString2 = new java/util/LinkedHashSet;
      paramString2.<init>();
      for (;;)
      {
        paramBoolean = paramString1.find();
        if (!paramBoolean) {
          break;
        }
        localObject = paramString1.group(bool1);
        paramString2.add(localObject);
      }
      u = paramString2;
      return;
    }
    paramString2 = b;
    Object localObject = new Object[2];
    localObject[0] = str;
    localObject[bool1] = paramString1;
    throw w.a(paramString2, "Only one HTTP method is allowed. Found: %s and %s.", (Object[])localObject);
  }
}

/* Location:
 * Qualified Name:     e.q.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */