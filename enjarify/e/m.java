package e;

import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Optional;

final class m
  extends f.a
{
  static final f.a a;
  
  static
  {
    m localm = new e/m;
    localm.<init>();
    a = localm;
  }
  
  public final f a(Type paramType, Annotation[] paramArrayOfAnnotation, s params)
  {
    Class localClass1 = w.a(paramType);
    Class localClass2 = Optional.class;
    if (localClass1 != localClass2) {
      return null;
    }
    paramType = (ParameterizedType)paramType;
    paramType = w.a(0, paramType);
    paramType = params.a(paramType, paramArrayOfAnnotation);
    paramArrayOfAnnotation = new e/m$a;
    paramArrayOfAnnotation.<init>(paramType);
    return paramArrayOfAnnotation;
  }
}

/* Location:
 * Qualified Name:     e.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */