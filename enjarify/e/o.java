package e;

import android.os.Build.VERSION;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executor;

class o
{
  private static final o a = ;
  
  static o a()
  {
    return a;
  }
  
  private static o e()
  {
    Object localObject = "android.os.Build";
    try
    {
      Class.forName((String)localObject);
      int i = Build.VERSION.SDK_INT;
      if (i != 0)
      {
        localObject = new e/o$a;
        ((o.a)localObject).<init>();
        return (o)localObject;
      }
    }
    catch (ClassNotFoundException localClassNotFoundException1)
    {
      localObject = "java.util.Optional";
      try
      {
        Class.forName((String)localObject);
        localObject = new e/o$b;
        ((o.b)localObject).<init>();
        return (o)localObject;
      }
      catch (ClassNotFoundException localClassNotFoundException2)
      {
        localObject = new e/o;
        ((o)localObject).<init>();
      }
    }
    return (o)localObject;
  }
  
  Object a(Method paramMethod, Class paramClass, Object paramObject, Object... paramVarArgs)
  {
    paramMethod = new java/lang/UnsupportedOperationException;
    paramMethod.<init>();
    throw paramMethod;
  }
  
  List a(Executor paramExecutor)
  {
    g localg = new e/g;
    localg.<init>(paramExecutor);
    return Collections.singletonList(localg);
  }
  
  boolean a(Method paramMethod)
  {
    return false;
  }
  
  Executor b()
  {
    return null;
  }
  
  List c()
  {
    return Collections.emptyList();
  }
  
  int d()
  {
    return 0;
  }
}

/* Location:
 * Qualified Name:     e.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */