package e;

import com.google.firebase.perf.network.FirebasePerfOkHttpClient;
import java.io.IOException;
import okhttp3.ab;
import okhttp3.ad;
import okhttp3.ad.a;
import okhttp3.ae;
import okhttp3.e;
import okhttp3.e.a;

final class l
  implements b
{
  private final q a;
  private final Object[] b;
  private final e.a c;
  private final f d;
  private volatile boolean e;
  private e f;
  private Throwable g;
  private boolean h;
  
  l(q paramq, Object[] paramArrayOfObject, e.a parama, f paramf)
  {
    a = paramq;
    b = paramArrayOfObject;
    c = parama;
    d = paramf;
  }
  
  private l f()
  {
    l locall = new e/l;
    q localq = a;
    Object[] arrayOfObject = b;
    e.a locala = c;
    f localf = d;
    locall.<init>(localq, arrayOfObject, locala, localf);
    return locall;
  }
  
  private e g()
  {
    Object localObject1 = c;
    Object localObject2 = a;
    Object[] arrayOfObject = b;
    localObject2 = ((q)localObject2).a(arrayOfObject);
    localObject1 = ((e.a)localObject1).a((ab)localObject2);
    if (localObject1 != null) {
      return (e)localObject1;
    }
    localObject1 = new java/lang/NullPointerException;
    ((NullPointerException)localObject1).<init>("Call.Factory returned null.");
    throw ((Throwable)localObject1);
  }
  
  final r a(ad paramad)
  {
    Object localObject1 = g;
    paramad = paramad.e();
    Object localObject2 = new e/l$b;
    okhttp3.w localw = ((ae)localObject1).a();
    long l = ((ae)localObject1).b();
    ((l.b)localObject2).<init>(localw, l);
    g = ((ae)localObject2);
    paramad = paramad.a();
    int i = c;
    int j = 200;
    if (i >= j)
    {
      j = 300;
      if (i < j)
      {
        j = 204;
        if (i != j)
        {
          j = 205;
          if (i != j)
          {
            localObject2 = new e/l$a;
            ((l.a)localObject2).<init>((ae)localObject1);
            try
            {
              localObject1 = d;
              localObject1 = ((f)localObject1).a(localObject2);
              return r.a(localObject1, paramad);
            }
            catch (RuntimeException localRuntimeException)
            {
              throw localRuntimeException;
            }
          }
        }
        ((ae)localObject1).close();
        return r.a(null, paramad);
      }
    }
    try
    {
      localObject2 = w.a((ae)localObject1);
      paramad = r.a((ae)localObject2, paramad);
      return paramad;
    }
    finally
    {
      ((ae)localObject1).close();
    }
  }
  
  public final void a(d paramd)
  {
    Object localObject = "callback == null";
    w.a(paramd, (String)localObject);
    try
    {
      boolean bool1 = h;
      if (!bool1)
      {
        bool1 = true;
        h = bool1;
        localObject = f;
        Throwable localThrowable1 = g;
        if ((localObject == null) && (localThrowable1 == null)) {
          try
          {
            e locale = g();
            f = locale;
            localObject = locale;
          }
          finally
          {
            w.a(localThrowable2);
            g = localThrowable2;
          }
        }
        if (localThrowable2 != null)
        {
          paramd.onFailure(this, localThrowable2);
          return;
        }
        boolean bool2 = e;
        if (bool2) {
          ((e)localObject).c();
        }
        l.1 local1 = new e/l$1;
        local1.<init>(this, paramd);
        FirebasePerfOkHttpClient.enqueue((e)localObject, local1);
        return;
      }
      paramd = new java/lang/IllegalStateException;
      localObject = "Already executed.";
      paramd.<init>((String)localObject);
      throw paramd;
    }
    finally {}
  }
  
  public final boolean a()
  {
    boolean bool1 = e;
    boolean bool2 = true;
    if (bool1) {
      return bool2;
    }
    try
    {
      e locale = f;
      if (locale != null)
      {
        locale = f;
        bool1 = locale.d();
        if (bool1) {}
      }
      else
      {
        bool2 = false;
      }
      return bool2;
    }
    finally {}
  }
  
  public final ab b()
  {
    try
    {
      Object localObject1 = f;
      if (localObject1 != null)
      {
        localObject1 = ((e)localObject1).a();
        return (ab)localObject1;
      }
      localObject1 = g;
      Object localObject3;
      Object localObject4;
      if (localObject1 != null)
      {
        localObject1 = g;
        boolean bool = localObject1 instanceof IOException;
        if (!bool)
        {
          localObject1 = g;
          bool = localObject1 instanceof RuntimeException;
          if (bool)
          {
            localObject1 = g;
            localObject1 = (RuntimeException)localObject1;
            throw ((Throwable)localObject1);
          }
          localObject1 = g;
          localObject1 = (Error)localObject1;
          throw ((Throwable)localObject1);
        }
        localObject1 = new java/lang/RuntimeException;
        localObject3 = "Unable to create request.";
        localObject4 = g;
        ((RuntimeException)localObject1).<init>((String)localObject3, (Throwable)localObject4);
        throw ((Throwable)localObject1);
      }
      try
      {
        localObject1 = g();
        f = ((e)localObject1);
        localObject1 = ((e)localObject1).a();
        return (ab)localObject1;
      }
      catch (IOException localIOException)
      {
        g = localIOException;
        localObject3 = new java/lang/RuntimeException;
        localObject4 = "Unable to create request.";
        ((RuntimeException)localObject3).<init>((String)localObject4, localIOException);
        throw ((Throwable)localObject3);
      }
      catch (Error localError) {}catch (RuntimeException localRuntimeException) {}
      w.a(localRuntimeException);
      g = localRuntimeException;
      throw localRuntimeException;
    }
    finally {}
  }
  
  public final r c()
  {
    try
    {
      boolean bool1 = h;
      if (!bool1)
      {
        bool1 = true;
        h = bool1;
        Object localObject1 = g;
        if (localObject1 != null)
        {
          localObject1 = g;
          bool1 = localObject1 instanceof IOException;
          if (!bool1)
          {
            localObject1 = g;
            bool1 = localObject1 instanceof RuntimeException;
            if (bool1)
            {
              localObject1 = g;
              localObject1 = (RuntimeException)localObject1;
              throw ((Throwable)localObject1);
            }
            localObject1 = g;
            localObject1 = (Error)localObject1;
            throw ((Throwable)localObject1);
          }
          localObject1 = g;
          localObject1 = (IOException)localObject1;
          throw ((Throwable)localObject1);
        }
        localObject1 = f;
        if (localObject1 == null)
        {
          try
          {
            localObject1 = g();
            f = ((e)localObject1);
          }
          catch (Error localError) {}catch (RuntimeException localRuntimeException) {}catch (IOException localIOException) {}
          w.a(localIOException);
          g = localIOException;
          throw localIOException;
        }
        boolean bool2 = e;
        if (bool2) {
          localIOException.c();
        }
        localObject2 = FirebasePerfOkHttpClient.execute(localIOException);
        return a((ad)localObject2);
      }
      Object localObject2 = new java/lang/IllegalStateException;
      String str = "Already executed.";
      ((IllegalStateException)localObject2).<init>(str);
      throw ((Throwable)localObject2);
    }
    finally {}
  }
  
  public final void d()
  {
    boolean bool = true;
    e = bool;
    try
    {
      e locale = f;
      if (locale != null) {
        locale.c();
      }
      return;
    }
    finally {}
  }
}

/* Location:
 * Qualified Name:     e.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */