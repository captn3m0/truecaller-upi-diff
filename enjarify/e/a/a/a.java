package e.a.a;

import e.f.a;
import e.s;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

public final class a
  extends f.a
{
  private final com.google.gson.f a;
  
  private a(com.google.gson.f paramf)
  {
    a = paramf;
  }
  
  public static a a()
  {
    com.google.gson.f localf = new com/google/gson/f;
    localf.<init>();
    return a(localf);
  }
  
  public static a a(com.google.gson.f paramf)
  {
    if (paramf != null)
    {
      a locala = new e/a/a/a;
      locala.<init>(paramf);
      return locala;
    }
    paramf = new java/lang/NullPointerException;
    paramf.<init>("gson == null");
    throw paramf;
  }
  
  public final e.f a(Type paramType)
  {
    Object localObject = a;
    paramType = com.google.gson.c.a.a(paramType);
    paramType = ((com.google.gson.f)localObject).a(paramType);
    localObject = new e/a/a/b;
    com.google.gson.f localf = a;
    ((b)localObject).<init>(localf, paramType);
    return (e.f)localObject;
  }
  
  public final e.f a(Type paramType, Annotation[] paramArrayOfAnnotation, s params)
  {
    paramArrayOfAnnotation = a;
    paramType = com.google.gson.c.a.a(paramType);
    paramType = paramArrayOfAnnotation.a(paramType);
    paramArrayOfAnnotation = new e/a/a/c;
    params = a;
    paramArrayOfAnnotation.<init>(params, paramType);
    return paramArrayOfAnnotation;
  }
}

/* Location:
 * Qualified Name:     e.a.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */