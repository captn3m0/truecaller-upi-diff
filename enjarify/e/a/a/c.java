package e.a.a;

import com.google.gson.m;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.v;
import java.io.Reader;
import okhttp3.ae;

final class c
  implements e.f
{
  private final com.google.gson.f a;
  private final v b;
  
  c(com.google.gson.f paramf, v paramv)
  {
    a = paramf;
    b = paramv;
  }
  
  private Object a(ae paramae)
  {
    Object localObject1 = a;
    Object localObject3 = paramae.f();
    localObject1 = ((com.google.gson.f)localObject1).a((Reader)localObject3);
    try
    {
      localObject3 = b;
      localObject3 = ((v)localObject3).a((JsonReader)localObject1);
      localObject1 = ((JsonReader)localObject1).peek();
      JsonToken localJsonToken = JsonToken.END_DOCUMENT;
      if (localObject1 == localJsonToken) {
        return localObject3;
      }
      localObject1 = new com/google/gson/m;
      localObject3 = "JSON document was not fully consumed.";
      ((m)localObject1).<init>((String)localObject3);
      throw ((Throwable)localObject1);
    }
    finally
    {
      paramae.close();
    }
  }
}

/* Location:
 * Qualified Name:     e.a.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */