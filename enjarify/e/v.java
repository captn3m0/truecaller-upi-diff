package e;

import java.lang.annotation.Annotation;

final class v
  implements u
{
  private static final u a;
  
  static
  {
    v localv = new e/v;
    localv.<init>();
    a = localv;
  }
  
  static Annotation[] a(Annotation[] paramArrayOfAnnotation)
  {
    Object localObject = u.class;
    boolean bool = w.a(paramArrayOfAnnotation, (Class)localObject);
    if (bool) {
      return paramArrayOfAnnotation;
    }
    int i = paramArrayOfAnnotation.length;
    int j = 1;
    localObject = new Annotation[i + j];
    u localu = a;
    localObject[0] = localu;
    int k = paramArrayOfAnnotation.length;
    System.arraycopy(paramArrayOfAnnotation, 0, localObject, j, k);
    return (Annotation[])localObject;
  }
  
  public final Class annotationType()
  {
    return u.class;
  }
  
  public final boolean equals(Object paramObject)
  {
    return paramObject instanceof u;
  }
  
  public final int hashCode()
  {
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("@");
    String str = u.class.getName();
    localStringBuilder.append(str);
    localStringBuilder.append("()");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     e.v
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */