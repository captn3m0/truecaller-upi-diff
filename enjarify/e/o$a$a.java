package e;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.Executor;

final class o$a$a
  implements Executor
{
  private final Handler a;
  
  o$a$a()
  {
    Handler localHandler = new android/os/Handler;
    Looper localLooper = Looper.getMainLooper();
    localHandler.<init>(localLooper);
    a = localHandler;
  }
  
  public final void execute(Runnable paramRunnable)
  {
    a.post(paramRunnable);
  }
}

/* Location:
 * Qualified Name:     e.o.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */