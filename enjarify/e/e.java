package e;

import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.concurrent.CompletableFuture;

final class e
  extends c.a
{
  static final c.a a;
  
  static
  {
    e locale = new e/e;
    locale.<init>();
    a = locale;
  }
  
  public final c a(Type paramType, Annotation[] paramArrayOfAnnotation, s params)
  {
    paramArrayOfAnnotation = w.a(paramType);
    params = CompletableFuture.class;
    if (paramArrayOfAnnotation != params) {
      return null;
    }
    boolean bool1 = paramType instanceof ParameterizedType;
    if (bool1)
    {
      paramType = (ParameterizedType)paramType;
      bool1 = false;
      paramArrayOfAnnotation = null;
      paramType = w.a(0, paramType);
      params = w.a(paramType);
      Class localClass = r.class;
      if (params != localClass)
      {
        paramArrayOfAnnotation = new e/e$a;
        paramArrayOfAnnotation.<init>(paramType);
        return paramArrayOfAnnotation;
      }
      boolean bool2 = paramType instanceof ParameterizedType;
      if (bool2)
      {
        paramType = (ParameterizedType)paramType;
        paramType = w.a(0, paramType);
        paramArrayOfAnnotation = new e/e$b;
        paramArrayOfAnnotation.<init>(paramType);
        return paramArrayOfAnnotation;
      }
      paramType = new java/lang/IllegalStateException;
      paramType.<init>("Response must be parameterized as Response<Foo> or Response<? extends Foo>");
      throw paramType;
    }
    paramType = new java/lang/IllegalStateException;
    paramType.<init>("CompletableFuture return type must be parameterized as CompletableFuture<Foo> or CompletableFuture<? extends Foo>");
    throw paramType;
  }
}

/* Location:
 * Qualified Name:     e.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */