package e;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles.Lookup;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executor;

final class o$b
  extends o
{
  final Object a(Method paramMethod, Class paramClass, Object paramObject, Object... paramVarArgs)
  {
    int i = 2;
    Object localObject = new Class[i];
    localObject[0] = Class.class;
    Class localClass = Integer.TYPE;
    boolean bool = true;
    localObject[bool] = localClass;
    Constructor localConstructor = MethodHandles.Lookup.class.getDeclaredConstructor((Class[])localObject);
    localConstructor.setAccessible(bool);
    Object[] arrayOfObject = new Object[i];
    arrayOfObject[0] = paramClass;
    localObject = Integer.valueOf(-1);
    arrayOfObject[bool] = localObject;
    return ((MethodHandles.Lookup)localConstructor.newInstance(arrayOfObject)).unreflectSpecial(paramMethod, paramClass).bindTo(paramObject).invokeWithArguments(paramVarArgs);
  }
  
  final List a(Executor paramExecutor)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>(2);
    Object localObject = e.a;
    localArrayList.add(localObject);
    localObject = new e/g;
    ((g)localObject).<init>(paramExecutor);
    localArrayList.add(localObject);
    return Collections.unmodifiableList(localArrayList);
  }
  
  final boolean a(Method paramMethod)
  {
    return paramMethod.isDefault();
  }
  
  final List c()
  {
    return Collections.singletonList(m.a);
  }
  
  final int d()
  {
    return 1;
  }
}

/* Location:
 * Qualified Name:     e.o.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */