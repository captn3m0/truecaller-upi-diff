package e;

import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Type;

final class w$a
  implements GenericArrayType
{
  private final Type a;
  
  w$a(Type paramType)
  {
    a = paramType;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = paramObject instanceof GenericArrayType;
    if (bool1)
    {
      paramObject = (GenericArrayType)paramObject;
      boolean bool2 = w.a(this, (Type)paramObject);
      if (bool2) {
        return true;
      }
    }
    return false;
  }
  
  public final Type getGenericComponentType()
  {
    return a;
  }
  
  public final int hashCode()
  {
    return a.hashCode();
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    String str = w.b(a);
    localStringBuilder.append(str);
    localStringBuilder.append("[]");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     e.w.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */