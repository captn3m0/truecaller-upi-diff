package e;

import okhttp3.ab;
import okhttp3.ab.a;
import okhttp3.ad;
import okhttp3.ad.a;
import okhttp3.ae;
import okhttp3.t;
import okhttp3.z;

public final class r
{
  public final ad a;
  public final Object b;
  public final ae c;
  
  private r(ad paramad, Object paramObject, ae paramae)
  {
    a = paramad;
    b = paramObject;
    c = paramae;
  }
  
  public static r a(Object paramObject)
  {
    Object localObject1 = new okhttp3/ad$a;
    ((ad.a)localObject1).<init>();
    c = 200;
    d = "OK";
    Object localObject2 = z.b;
    b = ((z)localObject2);
    localObject2 = new okhttp3/ab$a;
    ((ab.a)localObject2).<init>();
    localObject2 = ((ab.a)localObject2).a("http://localhost/").a();
    a = ((ab)localObject2);
    localObject1 = ((ad.a)localObject1).a();
    return a(paramObject, (ad)localObject1);
  }
  
  public static r a(Object paramObject, ad paramad)
  {
    Object localObject = "rawResponse == null";
    w.a(paramad, (String)localObject);
    boolean bool = paramad.c();
    if (bool)
    {
      localObject = new e/r;
      ((r)localObject).<init>(paramad, paramObject, null);
      return (r)localObject;
    }
    paramObject = new java/lang/IllegalArgumentException;
    ((IllegalArgumentException)paramObject).<init>("rawResponse must be successful response");
    throw ((Throwable)paramObject);
  }
  
  public static r a(Object paramObject, t paramt)
  {
    w.a(paramt, "headers == null");
    Object localObject = new okhttp3/ad$a;
    ((ad.a)localObject).<init>();
    c = 200;
    d = "OK";
    z localz = z.b;
    b = localz;
    paramt = ((ad.a)localObject).a(paramt);
    localObject = new okhttp3/ab$a;
    ((ab.a)localObject).<init>();
    localObject = ((ab.a)localObject).a("http://localhost/").a();
    a = ((ab)localObject);
    paramt = paramt.a();
    return a(paramObject, paramt);
  }
  
  public static r a(ae paramae, ad paramad)
  {
    w.a(paramae, "body == null");
    Object localObject = "rawResponse == null";
    w.a(paramad, (String)localObject);
    boolean bool = paramad.c();
    if (!bool)
    {
      localObject = new e/r;
      ((r)localObject).<init>(paramad, null, paramae);
      return (r)localObject;
    }
    paramae = new java/lang/IllegalArgumentException;
    paramae.<init>("rawResponse should not be successful response");
    throw paramae;
  }
  
  public final ad a()
  {
    return a;
  }
  
  public final int b()
  {
    return a.c;
  }
  
  public final String c()
  {
    return a.d;
  }
  
  public final boolean d()
  {
    return a.c();
  }
  
  public final Object e()
  {
    return b;
  }
  
  public final ae f()
  {
    return c;
  }
  
  public final String toString()
  {
    return a.toString();
  }
}

/* Location:
 * Qualified Name:     e.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */