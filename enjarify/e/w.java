package e;

import d.c;
import d.e;
import java.lang.annotation.Annotation;
import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.Arrays;
import okhttp3.ae;

public final class w
{
  static final Type[] a = new Type[0];
  
  public static Class a(Type paramType)
  {
    for (;;)
    {
      localObject = "type == null";
      a(paramType, (String)localObject);
      boolean bool = paramType instanceof Class;
      if (bool) {
        return (Class)paramType;
      }
      bool = paramType instanceof ParameterizedType;
      if (bool)
      {
        paramType = ((ParameterizedType)paramType).getRawType();
        bool = paramType instanceof Class;
        if (bool) {
          return (Class)paramType;
        }
        paramType = new java/lang/IllegalArgumentException;
        paramType.<init>();
        throw paramType;
      }
      bool = paramType instanceof GenericArrayType;
      localStringBuilder = null;
      if (bool) {
        return Array.newInstance(a(((GenericArrayType)paramType).getGenericComponentType()), 0).getClass();
      }
      bool = paramType instanceof TypeVariable;
      if (bool) {
        return Object.class;
      }
      bool = paramType instanceof WildcardType;
      if (!bool) {
        break;
      }
      paramType = ((WildcardType)paramType).getUpperBounds()[0];
    }
    Object localObject = new java/lang/IllegalArgumentException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Expected a Class, ParameterizedType, or GenericArrayType, but <");
    localStringBuilder.append(paramType);
    localStringBuilder.append("> is of type ");
    paramType = paramType.getClass().getName();
    localStringBuilder.append(paramType);
    paramType = localStringBuilder.toString();
    ((IllegalArgumentException)localObject).<init>(paramType);
    throw ((Throwable)localObject);
  }
  
  static Object a(Object paramObject, String paramString)
  {
    if (paramObject != null) {
      return paramObject;
    }
    paramObject = new java/lang/NullPointerException;
    ((NullPointerException)paramObject).<init>(paramString);
    throw ((Throwable)paramObject);
  }
  
  static RuntimeException a(Method paramMethod, int paramInt, String paramString, Object... paramVarArgs)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(paramString);
    localStringBuilder.append(" (parameter #");
    paramInt += 1;
    localStringBuilder.append(paramInt);
    localStringBuilder.append(")");
    String str = localStringBuilder.toString();
    return a(paramMethod, null, str, paramVarArgs);
  }
  
  static RuntimeException a(Method paramMethod, String paramString, Object... paramVarArgs)
  {
    return a(paramMethod, null, paramString, paramVarArgs);
  }
  
  static RuntimeException a(Method paramMethod, Throwable paramThrowable, int paramInt, String paramString, Object... paramVarArgs)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(paramString);
    localStringBuilder.append(" (parameter #");
    paramInt += 1;
    localStringBuilder.append(paramInt);
    localStringBuilder.append(")");
    String str = localStringBuilder.toString();
    return a(paramMethod, paramThrowable, str, paramVarArgs);
  }
  
  static RuntimeException a(Method paramMethod, Throwable paramThrowable, String paramString, Object... paramVarArgs)
  {
    paramString = String.format(paramString, paramVarArgs);
    paramVarArgs = new java/lang/IllegalArgumentException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(paramString);
    localStringBuilder.append("\n    for method ");
    paramString = paramMethod.getDeclaringClass().getSimpleName();
    localStringBuilder.append(paramString);
    localStringBuilder.append(".");
    paramMethod = paramMethod.getName();
    localStringBuilder.append(paramMethod);
    paramMethod = localStringBuilder.toString();
    paramVarArgs.<init>(paramMethod, paramThrowable);
    return paramVarArgs;
  }
  
  public static Type a(int paramInt, ParameterizedType paramParameterizedType)
  {
    Type[] arrayOfType = paramParameterizedType.getActualTypeArguments();
    if (paramInt >= 0)
    {
      int i = arrayOfType.length;
      if (paramInt < i)
      {
        localObject = arrayOfType[paramInt];
        boolean bool = localObject instanceof WildcardType;
        if (bool) {
          return ((WildcardType)localObject).getUpperBounds()[0];
        }
        return (Type)localObject;
      }
    }
    IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Index ");
    localStringBuilder.append(paramInt);
    localStringBuilder.append(" not in range [0,");
    paramInt = arrayOfType.length;
    localStringBuilder.append(paramInt);
    localStringBuilder.append(") for ");
    localStringBuilder.append(paramParameterizedType);
    Object localObject = localStringBuilder.toString();
    localIllegalArgumentException.<init>((String)localObject);
    throw localIllegalArgumentException;
  }
  
  static Type a(ParameterizedType paramParameterizedType)
  {
    paramParameterizedType = paramParameterizedType.getActualTypeArguments()[0];
    boolean bool = paramParameterizedType instanceof WildcardType;
    if (bool) {
      return ((WildcardType)paramParameterizedType).getLowerBounds()[0];
    }
    return paramParameterizedType;
  }
  
  static Type a(Type paramType, Class paramClass1, Class paramClass2)
  {
    boolean bool = paramClass2.isAssignableFrom(paramClass1);
    if (bool)
    {
      paramClass2 = b(paramType, paramClass1, paramClass2);
      return a(paramType, paramClass1, paramClass2);
    }
    paramType = new java/lang/IllegalArgumentException;
    paramType.<init>();
    throw paramType;
  }
  
  /* Error */
  private static Type a(Type paramType1, Class paramClass, Type paramType2)
  {
    // Byte code:
    //   0: aload_2
    //   1: instanceof 49
    //   4: istore_3
    //   5: iconst_0
    //   6: istore 4
    //   8: iload_3
    //   9: ifeq +167 -> 176
    //   12: aload_2
    //   13: checkcast 49	java/lang/reflect/TypeVariable
    //   16: astore_2
    //   17: aload_2
    //   18: invokeinterface 142 1 0
    //   23: astore 5
    //   25: aload 5
    //   27: instanceof 17
    //   30: istore 6
    //   32: iload 6
    //   34: ifeq +13 -> 47
    //   37: aload 5
    //   39: checkcast 17	java/lang/Class
    //   42: astore 5
    //   44: goto +8 -> 52
    //   47: iconst_0
    //   48: istore_3
    //   49: aconst_null
    //   50: astore 5
    //   52: aload 5
    //   54: ifnull +104 -> 158
    //   57: aload_0
    //   58: aload_1
    //   59: aload 5
    //   61: invokestatic 135	e/w:b	(Ljava/lang/reflect/Type;Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/reflect/Type;
    //   64: astore 7
    //   66: aload 7
    //   68: instanceof 19
    //   71: istore 8
    //   73: iload 8
    //   75: ifeq +83 -> 158
    //   78: aload 5
    //   80: invokevirtual 146	java/lang/Class:getTypeParameters	()[Ljava/lang/reflect/TypeVariable;
    //   83: astore 5
    //   85: aload 5
    //   87: arraylength
    //   88: istore 8
    //   90: iload 4
    //   92: iload 8
    //   94: if_icmpge +54 -> 148
    //   97: aload 5
    //   99: iload 4
    //   101: aaload
    //   102: astore 9
    //   104: aload_2
    //   105: aload 9
    //   107: invokevirtual 150	java/lang/Object:equals	(Ljava/lang/Object;)Z
    //   110: istore 8
    //   112: iload 8
    //   114: ifeq +25 -> 139
    //   117: aload 7
    //   119: checkcast 19	java/lang/reflect/ParameterizedType
    //   122: astore 7
    //   124: aload 7
    //   126: invokeinterface 118 1 0
    //   131: iload 4
    //   133: aaload
    //   134: astore 5
    //   136: goto +25 -> 161
    //   139: iload 4
    //   141: iconst_1
    //   142: iadd
    //   143: istore 4
    //   145: goto -60 -> 85
    //   148: new 152	java/util/NoSuchElementException
    //   151: astore_0
    //   152: aload_0
    //   153: invokespecial 153	java/util/NoSuchElementException:<init>	()V
    //   156: aload_0
    //   157: athrow
    //   158: aload_2
    //   159: astore 5
    //   161: aload 5
    //   163: aload_2
    //   164: if_acmpne +6 -> 170
    //   167: aload 5
    //   169: areturn
    //   170: aload 5
    //   172: astore_2
    //   173: goto -173 -> 0
    //   176: aload_2
    //   177: instanceof 17
    //   180: istore_3
    //   181: iload_3
    //   182: ifeq +56 -> 238
    //   185: aload_2
    //   186: astore 5
    //   188: aload_2
    //   189: checkcast 17	java/lang/Class
    //   192: astore 5
    //   194: aload 5
    //   196: invokevirtual 157	java/lang/Class:isArray	()Z
    //   199: istore 6
    //   201: iload 6
    //   203: ifeq +35 -> 238
    //   206: aload 5
    //   208: invokevirtual 160	java/lang/Class:getComponentType	()Ljava/lang/Class;
    //   211: astore_2
    //   212: aload_0
    //   213: aload_1
    //   214: aload_2
    //   215: invokestatic 138	e/w:a	(Ljava/lang/reflect/Type;Ljava/lang/Class;Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
    //   218: astore_0
    //   219: aload_2
    //   220: aload_0
    //   221: if_acmpne +6 -> 227
    //   224: aload 5
    //   226: areturn
    //   227: new 162	e/w$a
    //   230: astore_1
    //   231: aload_1
    //   232: aload_0
    //   233: invokespecial 165	e/w$a:<init>	(Ljava/lang/reflect/Type;)V
    //   236: aload_1
    //   237: areturn
    //   238: aload_2
    //   239: instanceof 31
    //   242: istore_3
    //   243: iload_3
    //   244: ifeq +43 -> 287
    //   247: aload_2
    //   248: checkcast 31	java/lang/reflect/GenericArrayType
    //   251: astore_2
    //   252: aload_2
    //   253: invokeinterface 34 1 0
    //   258: astore 5
    //   260: aload_0
    //   261: aload_1
    //   262: aload 5
    //   264: invokestatic 138	e/w:a	(Ljava/lang/reflect/Type;Ljava/lang/Class;Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
    //   267: astore_0
    //   268: aload 5
    //   270: aload_0
    //   271: if_acmpne +5 -> 276
    //   274: aload_2
    //   275: areturn
    //   276: new 162	e/w$a
    //   279: astore_1
    //   280: aload_1
    //   281: aload_0
    //   282: invokespecial 165	e/w$a:<init>	(Ljava/lang/reflect/Type;)V
    //   285: aload_1
    //   286: areturn
    //   287: aload_2
    //   288: instanceof 19
    //   291: istore_3
    //   292: iconst_1
    //   293: istore 6
    //   295: iload_3
    //   296: ifeq +160 -> 456
    //   299: aload_2
    //   300: checkcast 19	java/lang/reflect/ParameterizedType
    //   303: astore_2
    //   304: aload_2
    //   305: invokeinterface 169 1 0
    //   310: astore 5
    //   312: aload_0
    //   313: aload_1
    //   314: aload 5
    //   316: invokestatic 138	e/w:a	(Ljava/lang/reflect/Type;Ljava/lang/Class;Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
    //   319: astore 9
    //   321: aload 9
    //   323: aload 5
    //   325: if_acmpeq +8 -> 333
    //   328: iconst_1
    //   329: istore_3
    //   330: goto +8 -> 338
    //   333: iconst_0
    //   334: istore_3
    //   335: aconst_null
    //   336: astore 5
    //   338: aload_2
    //   339: invokeinterface 118 1 0
    //   344: astore 10
    //   346: aload 10
    //   348: arraylength
    //   349: istore 11
    //   351: iload 4
    //   353: iload 11
    //   355: if_icmpge +73 -> 428
    //   358: aload 10
    //   360: iload 4
    //   362: aaload
    //   363: astore 12
    //   365: aload_0
    //   366: aload_1
    //   367: aload 12
    //   369: invokestatic 138	e/w:a	(Ljava/lang/reflect/Type;Ljava/lang/Class;Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
    //   372: astore 12
    //   374: aload 10
    //   376: iload 4
    //   378: aaload
    //   379: astore 13
    //   381: aload 12
    //   383: aload 13
    //   385: if_acmpeq +34 -> 419
    //   388: iload_3
    //   389: ifne +23 -> 412
    //   392: aload 10
    //   394: invokevirtual 174	[Ljava/lang/reflect/Type;:clone	()Ljava/lang/Object;
    //   397: astore 5
    //   399: aload 5
    //   401: astore 10
    //   403: aload 5
    //   405: checkcast 170	[Ljava/lang/reflect/Type;
    //   408: astore 10
    //   410: iconst_1
    //   411: istore_3
    //   412: aload 10
    //   414: iload 4
    //   416: aload 12
    //   418: aastore
    //   419: iload 4
    //   421: iconst_1
    //   422: iadd
    //   423: istore 4
    //   425: goto -74 -> 351
    //   428: iload_3
    //   429: ifeq +25 -> 454
    //   432: new 176	e/w$b
    //   435: astore_0
    //   436: aload_2
    //   437: invokeinterface 23 1 0
    //   442: astore_1
    //   443: aload_0
    //   444: aload 9
    //   446: aload_1
    //   447: aload 10
    //   449: invokespecial 179	e/w$b:<init>	(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)V
    //   452: aload_0
    //   453: areturn
    //   454: aload_2
    //   455: areturn
    //   456: aload_2
    //   457: instanceof 51
    //   460: istore_3
    //   461: iload_3
    //   462: ifeq +158 -> 620
    //   465: aload_2
    //   466: checkcast 51	java/lang/reflect/WildcardType
    //   469: astore_2
    //   470: aload_2
    //   471: invokeinterface 127 1 0
    //   476: astore 5
    //   478: aload_2
    //   479: invokeinterface 55 1 0
    //   484: astore 9
    //   486: aload 5
    //   488: arraylength
    //   489: istore 14
    //   491: iload 14
    //   493: iload 6
    //   495: if_icmpne +63 -> 558
    //   498: aload 5
    //   500: iconst_0
    //   501: aaload
    //   502: astore 9
    //   504: aload_0
    //   505: aload_1
    //   506: aload 9
    //   508: invokestatic 138	e/w:a	(Ljava/lang/reflect/Type;Ljava/lang/Class;Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
    //   511: astore_0
    //   512: aload 5
    //   514: iconst_0
    //   515: aaload
    //   516: astore_1
    //   517: aload_0
    //   518: aload_1
    //   519: if_acmpeq +99 -> 618
    //   522: new 181	e/w$c
    //   525: astore_1
    //   526: iload 6
    //   528: anewarray 8	java/lang/reflect/Type
    //   531: astore_2
    //   532: aload_2
    //   533: iconst_0
    //   534: ldc 4
    //   536: aastore
    //   537: iload 6
    //   539: anewarray 8	java/lang/reflect/Type
    //   542: astore 5
    //   544: aload 5
    //   546: iconst_0
    //   547: aload_0
    //   548: aastore
    //   549: aload_1
    //   550: aload_2
    //   551: aload 5
    //   553: invokespecial 184	e/w$c:<init>	([Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)V
    //   556: aload_1
    //   557: areturn
    //   558: aload 9
    //   560: arraylength
    //   561: istore_3
    //   562: iload_3
    //   563: iload 6
    //   565: if_icmpne +53 -> 618
    //   568: aload 9
    //   570: iconst_0
    //   571: aaload
    //   572: astore 5
    //   574: aload_0
    //   575: aload_1
    //   576: aload 5
    //   578: invokestatic 138	e/w:a	(Ljava/lang/reflect/Type;Ljava/lang/Class;Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
    //   581: astore_0
    //   582: aload 9
    //   584: iconst_0
    //   585: aaload
    //   586: astore_1
    //   587: aload_0
    //   588: aload_1
    //   589: if_acmpeq +29 -> 618
    //   592: new 181	e/w$c
    //   595: astore_1
    //   596: iload 6
    //   598: anewarray 8	java/lang/reflect/Type
    //   601: astore_2
    //   602: aload_2
    //   603: iconst_0
    //   604: aload_0
    //   605: aastore
    //   606: getstatic 10	e/w:a	[Ljava/lang/reflect/Type;
    //   609: astore_0
    //   610: aload_1
    //   611: aload_2
    //   612: aload_0
    //   613: invokespecial 184	e/w$c:<init>	([Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)V
    //   616: aload_1
    //   617: areturn
    //   618: aload_2
    //   619: areturn
    //   620: aload_2
    //   621: checkcast 8	java/lang/reflect/Type
    //   624: areturn
    //   625: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	626	0	paramType1	Type
    //   0	626	1	paramClass	Class
    //   0	626	2	paramType2	Type
    //   4	458	3	bool1	boolean
    //   561	5	3	i	int
    //   6	418	4	j	int
    //   23	554	5	localObject1	Object
    //   30	567	6	k	int
    //   64	61	7	localObject2	Object
    //   71	3	8	bool2	boolean
    //   88	7	8	m	int
    //   110	3	8	bool3	boolean
    //   102	481	9	localObject3	Object
    //   344	104	10	localObject4	Object
    //   349	7	11	n	int
    //   363	54	12	localType	Type
    //   379	5	13	localObject5	Object
    //   489	7	14	i1	int
    // Exception table:
    //   from	to	target	type
    //   576	581	625	finally
  }
  
  static ae a(ae paramae)
  {
    c localc = new d/c;
    localc.<init>();
    paramae.c().a(localc);
    okhttp3.w localw = paramae.a();
    long l = paramae.b();
    return ae.a(localw, l, localc);
  }
  
  static void a(Class paramClass)
  {
    boolean bool = paramClass.isInterface();
    if (bool)
    {
      paramClass = paramClass.getInterfaces();
      int i = paramClass.length;
      if (i <= 0) {
        return;
      }
      paramClass = new java/lang/IllegalArgumentException;
      paramClass.<init>("API interfaces must not extend other interfaces.");
      throw paramClass;
    }
    paramClass = new java/lang/IllegalArgumentException;
    paramClass.<init>("API declarations must be interfaces.");
    throw paramClass;
  }
  
  static void a(Throwable paramThrowable)
  {
    boolean bool = paramThrowable instanceof VirtualMachineError;
    if (!bool)
    {
      bool = paramThrowable instanceof ThreadDeath;
      if (!bool)
      {
        bool = paramThrowable instanceof LinkageError;
        if (!bool) {
          return;
        }
        throw ((LinkageError)paramThrowable);
      }
      throw ((ThreadDeath)paramThrowable);
    }
    throw ((VirtualMachineError)paramThrowable);
  }
  
  static boolean a(Type paramType1, Type paramType2)
  {
    boolean bool1;
    Object localObject1;
    Object localObject2;
    boolean bool3;
    for (;;)
    {
      bool1 = true;
      if (paramType1 == paramType2) {
        return bool1;
      }
      bool2 = paramType1 instanceof Class;
      if (bool2) {
        return paramType1.equals(paramType2);
      }
      bool2 = paramType1 instanceof ParameterizedType;
      if (bool2)
      {
        bool2 = paramType2 instanceof ParameterizedType;
        if (!bool2) {
          return false;
        }
        paramType1 = (ParameterizedType)paramType1;
        paramType2 = (ParameterizedType)paramType2;
        localObject1 = paramType1.getOwnerType();
        localObject2 = paramType2.getOwnerType();
        if (localObject1 != localObject2)
        {
          if (localObject1 != null)
          {
            bool2 = localObject1.equals(localObject2);
            if (!bool2) {}
          }
        }
        else
        {
          localObject1 = paramType1.getRawType();
          localObject2 = paramType2.getRawType();
          bool2 = localObject1.equals(localObject2);
          if (bool2)
          {
            paramType1 = paramType1.getActualTypeArguments();
            paramType2 = paramType2.getActualTypeArguments();
            bool3 = Arrays.equals(paramType1, paramType2);
            if (bool3) {
              return bool1;
            }
          }
        }
        return false;
      }
      bool2 = paramType1 instanceof GenericArrayType;
      if (!bool2) {
        break;
      }
      bool1 = paramType2 instanceof GenericArrayType;
      if (!bool1) {
        return false;
      }
      paramType1 = (GenericArrayType)paramType1;
      paramType2 = (GenericArrayType)paramType2;
      paramType1 = paramType1.getGenericComponentType();
      paramType2 = paramType2.getGenericComponentType();
    }
    boolean bool2 = paramType1 instanceof WildcardType;
    if (bool2)
    {
      bool2 = paramType2 instanceof WildcardType;
      if (!bool2) {
        return false;
      }
      paramType1 = (WildcardType)paramType1;
      paramType2 = (WildcardType)paramType2;
      localObject1 = paramType1.getUpperBounds();
      localObject2 = paramType2.getUpperBounds();
      bool2 = Arrays.equals((Object[])localObject1, (Object[])localObject2);
      if (bool2)
      {
        paramType1 = paramType1.getLowerBounds();
        paramType2 = paramType2.getLowerBounds();
        bool3 = Arrays.equals(paramType1, paramType2);
        if (bool3) {
          return bool1;
        }
      }
      return false;
    }
    bool2 = paramType1 instanceof TypeVariable;
    if (bool2)
    {
      bool2 = paramType2 instanceof TypeVariable;
      if (!bool2) {
        return false;
      }
      paramType1 = (TypeVariable)paramType1;
      paramType2 = (TypeVariable)paramType2;
      localObject1 = paramType1.getGenericDeclaration();
      localObject2 = paramType2.getGenericDeclaration();
      if (localObject1 == localObject2)
      {
        paramType1 = paramType1.getName();
        paramType2 = paramType2.getName();
        bool3 = paramType1.equals(paramType2);
        if (bool3) {
          return bool1;
        }
      }
      return false;
    }
    return false;
  }
  
  static boolean a(Annotation[] paramArrayOfAnnotation, Class paramClass)
  {
    int i = paramArrayOfAnnotation.length;
    int j = 0;
    while (j < i)
    {
      Annotation localAnnotation = paramArrayOfAnnotation[j];
      boolean bool = paramClass.isInstance(localAnnotation);
      if (bool) {
        return true;
      }
      j += 1;
    }
    return false;
  }
  
  static String b(Type paramType)
  {
    boolean bool = paramType instanceof Class;
    if (bool) {
      return ((Class)paramType).getName();
    }
    return paramType.toString();
  }
  
  private static Type b(Type paramType, Class paramClass1, Class paramClass2)
  {
    if (paramClass2 == paramClass1) {
      return paramType;
    }
    boolean bool1 = paramClass2.isInterface();
    Object localObject;
    if (bool1)
    {
      paramType = paramClass1.getInterfaces();
      int i = 0;
      int j = paramType.length;
      for (;;)
      {
        if (i >= j) {
          break label107;
        }
        Class localClass = paramType[i];
        if (localClass == paramClass2) {
          return paramClass1.getGenericInterfaces()[i];
        }
        localClass = paramType[i];
        boolean bool3 = paramClass2.isAssignableFrom(localClass);
        if (bool3)
        {
          paramClass1 = paramClass1.getGenericInterfaces()[i];
          paramType = paramType[i];
          localObject = paramClass1;
          paramClass1 = paramType;
          paramType = (Type)localObject;
          break;
        }
        i += 1;
      }
    }
    label107:
    bool1 = paramClass1.isInterface();
    if (!bool1) {
      for (;;)
      {
        paramType = Object.class;
        if (paramClass1 == paramType) {
          return paramClass2;
        }
        paramType = paramClass1.getSuperclass();
        if (paramType == paramClass2) {
          return paramClass1.getGenericSuperclass();
        }
        boolean bool2 = paramClass2.isAssignableFrom(paramType);
        if (bool2)
        {
          localObject = paramClass1.getGenericSuperclass();
          paramClass1 = paramType;
          paramType = (Type)localObject;
          break;
        }
        paramClass1 = paramType;
      }
    }
    return paramClass2;
  }
  
  static void c(Type paramType)
  {
    boolean bool1 = paramType instanceof Class;
    if (bool1)
    {
      paramType = (Class)paramType;
      boolean bool2 = paramType.isPrimitive();
      if (bool2)
      {
        paramType = new java/lang/IllegalArgumentException;
        paramType.<init>();
        throw paramType;
      }
    }
  }
  
  static boolean d(Type paramType)
  {
    boolean bool3;
    for (;;)
    {
      boolean bool1 = paramType instanceof Class;
      localIllegalArgumentException = null;
      if (bool1) {
        return false;
      }
      bool1 = paramType instanceof ParameterizedType;
      bool3 = true;
      if (bool1)
      {
        paramType = ((ParameterizedType)paramType).getActualTypeArguments();
        int i = paramType.length;
        int j = 0;
        while (j < i)
        {
          Type localType = paramType[j];
          boolean bool4 = d(localType);
          if (bool4) {
            return bool3;
          }
          j += 1;
        }
        return false;
      }
      bool2 = paramType instanceof GenericArrayType;
      if (!bool2) {
        break;
      }
      paramType = ((GenericArrayType)paramType).getGenericComponentType();
    }
    boolean bool2 = paramType instanceof TypeVariable;
    if (bool2) {
      return bool3;
    }
    bool2 = paramType instanceof WildcardType;
    if (bool2) {
      return bool3;
    }
    String str;
    if (paramType == null) {
      str = "null";
    } else {
      str = paramType.getClass().getName();
    }
    IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Expected a Class, ParameterizedType, or GenericArrayType, but <");
    localStringBuilder.append(paramType);
    localStringBuilder.append("> is of type ");
    localStringBuilder.append(str);
    paramType = localStringBuilder.toString();
    localIllegalArgumentException.<init>(paramType);
    throw localIllegalArgumentException;
  }
}

/* Location:
 * Qualified Name:     e.w
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */