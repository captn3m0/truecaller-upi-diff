package e;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import okhttp3.ab;
import okhttp3.ab.a;
import okhttp3.t;
import okhttp3.u;
import okhttp3.w;

final class q
{
  final String a;
  final boolean b;
  private final Method c;
  private final u d;
  private final String e;
  private final t f;
  private final w g;
  private final boolean h;
  private final boolean i;
  private final boolean j;
  private final n[] k;
  
  q(q.a parama)
  {
    Object localObject = b;
    c = ((Method)localObject);
    localObject = a.b;
    d = ((u)localObject);
    localObject = n;
    a = ((String)localObject);
    localObject = r;
    e = ((String)localObject);
    localObject = s;
    f = ((t)localObject);
    localObject = t;
    g = ((w)localObject);
    boolean bool1 = o;
    h = bool1;
    bool1 = p;
    i = bool1;
    bool1 = q;
    j = bool1;
    localObject = v;
    k = ((n[])localObject);
    boolean bool2 = w;
    b = bool2;
  }
  
  final ab a(Object[] paramArrayOfObject)
  {
    Object localObject1 = k;
    int m = paramArrayOfObject.length;
    int n = localObject1.length;
    if (m == n)
    {
      localObject2 = new e/p;
      String str = a;
      Object localObject3 = d;
      Object localObject4 = e;
      t localt = f;
      w localw = g;
      boolean bool1 = h;
      boolean bool2 = i;
      boolean bool3 = j;
      Object localObject5 = localObject2;
      ((p)localObject2).<init>(str, (u)localObject3, (String)localObject4, localt, localw, bool1, bool2, bool3);
      boolean bool4 = b;
      if (bool4) {
        m += -1;
      }
      localObject5 = new java/util/ArrayList;
      ((ArrayList)localObject5).<init>(m);
      int i1 = 0;
      str = null;
      while (i1 < m)
      {
        localObject3 = paramArrayOfObject[i1];
        ((List)localObject5).add(localObject3);
        localObject3 = localObject1[i1];
        localObject4 = paramArrayOfObject[i1];
        ((n)localObject3).a((p)localObject2, localObject4);
        i1 += 1;
      }
      paramArrayOfObject = ((p)localObject2).a();
      j localj = new e/j;
      localObject2 = c;
      localj.<init>((Method)localObject2, (List)localObject5);
      return paramArrayOfObject.a(j.class, localj).a();
    }
    paramArrayOfObject = new java/lang/IllegalArgumentException;
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("Argument count (");
    ((StringBuilder)localObject2).append(m);
    ((StringBuilder)localObject2).append(") doesn't match expected count (");
    int i2 = localObject1.length;
    ((StringBuilder)localObject2).append(i2);
    ((StringBuilder)localObject2).append(")");
    localObject1 = ((StringBuilder)localObject2).toString();
    paramArrayOfObject.<init>((String)localObject1);
    throw paramArrayOfObject;
  }
}

/* Location:
 * Qualified Name:     e.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */