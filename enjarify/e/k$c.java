package e;

import c.d.c;
import c.e;
import c.g.b.k;
import c.o;
import c.p;
import java.lang.reflect.Method;
import okhttp3.ab;

public final class k$c
  implements d
{
  k$c(kotlinx.coroutines.j paramj) {}
  
  public final void onFailure(b paramb, Throwable paramThrowable)
  {
    k.b(paramb, "call");
    k.b(paramThrowable, "t");
    paramb = (c)a;
    paramThrowable = o.d(p.a(paramThrowable));
    paramb.b(paramThrowable);
  }
  
  public final void onResponse(b paramb, r paramr)
  {
    k.b(paramb, "call");
    Object localObject1 = "response";
    k.b(paramr, (String)localObject1);
    boolean bool = paramr.d();
    if (bool)
    {
      paramr = paramr.e();
      if (paramr == null)
      {
        paramb = paramb.b();
        paramr = j.class;
        paramb = paramb.a(paramr);
        if (paramb == null) {
          k.a();
        }
        k.a(paramb, "call.request().tag(Invocation::class.java)!!");
        paramb = ((j)paramb).a();
        paramr = new c/e;
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>("Response from ");
        k.a(paramb, "method");
        Object localObject2 = paramb.getDeclaringClass();
        k.a(localObject2, "method.declaringClass");
        localObject2 = ((Class)localObject2).getName();
        ((StringBuilder)localObject1).append((String)localObject2);
        ((StringBuilder)localObject1).append('.');
        paramb = paramb.getName();
        ((StringBuilder)localObject1).append(paramb);
        ((StringBuilder)localObject1).append(" was null but response body type was declared as non-null");
        paramb = ((StringBuilder)localObject1).toString();
        paramr.<init>(paramb);
        paramb = (c)a;
        localObject1 = o.a;
        paramr = o.d(p.a((Throwable)paramr));
        paramb.b(paramr);
        return;
      }
      paramb = (c)a;
      localObject1 = o.a;
      paramr = o.d(paramr);
      paramb.b(paramr);
      return;
    }
    paramb = (c)a;
    localObject1 = new e/h;
    ((h)localObject1).<init>(paramr);
    localObject1 = (Throwable)localObject1;
    paramr = o.a;
    paramr = o.d(p.a((Throwable)localObject1));
    paramb.b(paramr);
  }
}

/* Location:
 * Qualified Name:     e.k.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */