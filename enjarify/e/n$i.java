package e;

import java.lang.reflect.Method;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

final class n$i
  extends n
{
  private final Method a;
  private final int b;
  private final String c;
  private final f d;
  private final boolean e;
  
  n$i(Method paramMethod, int paramInt, String paramString, f paramf, boolean paramBoolean)
  {
    a = paramMethod;
    b = paramInt;
    paramMethod = (String)w.a(paramString, "name == null");
    c = paramMethod;
    d = paramf;
    e = paramBoolean;
  }
  
  final void a(p paramp, Object paramObject)
  {
    if (paramObject != null)
    {
      localObject1 = c;
      localObject2 = d;
      paramObject = (String)((f)localObject2).a(paramObject);
      boolean bool = e;
      String str1 = b;
      if (str1 != null)
      {
        localObject2 = p.a((String)paramObject, bool);
        str1 = b;
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        String str2 = "{";
        localStringBuilder.<init>(str2);
        localStringBuilder.append((String)localObject1);
        localStringBuilder.append("}");
        localObject1 = localStringBuilder.toString();
        localObject1 = str1.replace((CharSequence)localObject1, (CharSequence)localObject2);
        localObject2 = p.a.matcher((CharSequence)localObject1);
        bool = ((Matcher)localObject2).matches();
        if (!bool)
        {
          b = ((String)localObject1);
          return;
        }
        paramp = new java/lang/IllegalArgumentException;
        paramObject = String.valueOf(paramObject);
        paramObject = "@Path parameters shouldn't perform path traversal ('.' or '..'): ".concat((String)paramObject);
        paramp.<init>((String)paramObject);
        throw paramp;
      }
      paramp = new java/lang/AssertionError;
      paramp.<init>();
      throw paramp;
    }
    paramp = a;
    int i = b;
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("Path parameter \"");
    Object localObject2 = c;
    ((StringBuilder)localObject1).append((String)localObject2);
    ((StringBuilder)localObject1).append("\" value must not be null.");
    localObject1 = ((StringBuilder)localObject1).toString();
    localObject2 = new Object[0];
    throw w.a(paramp, i, (String)localObject1, (Object[])localObject2);
  }
}

/* Location:
 * Qualified Name:     e.n.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */