package e;

import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.concurrent.Executor;

final class g
  extends c.a
{
  private final Executor a;
  
  g(Executor paramExecutor)
  {
    a = paramExecutor;
  }
  
  public final c a(Type paramType, Annotation[] paramArrayOfAnnotation, s params)
  {
    params = w.a(paramType);
    Class localClass = b.class;
    Executor localExecutor = null;
    if (params != localClass) {
      return null;
    }
    boolean bool1 = paramType instanceof ParameterizedType;
    if (bool1)
    {
      bool1 = false;
      paramType = (ParameterizedType)paramType;
      paramType = w.a(0, paramType);
      params = u.class;
      boolean bool2 = w.a(paramArrayOfAnnotation, params);
      if (!bool2) {
        localExecutor = a;
      }
      paramArrayOfAnnotation = new e/g$1;
      paramArrayOfAnnotation.<init>(this, paramType, localExecutor);
      return paramArrayOfAnnotation;
    }
    paramType = new java/lang/IllegalArgumentException;
    paramType.<init>("Call return type must be parameterized as Call<Foo> or Call<? extends Foo>");
    throw paramType;
  }
}

/* Location:
 * Qualified Name:     e.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */