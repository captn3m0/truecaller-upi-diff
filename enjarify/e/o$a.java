package e;

import android.os.Build.VERSION;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executor;

final class o$a
  extends o
{
  final List a(Executor paramExecutor)
  {
    if (paramExecutor != null)
    {
      g localg = new e/g;
      localg.<init>(paramExecutor);
      int i = Build.VERSION.SDK_INT;
      int j = 24;
      if (i >= j)
      {
        paramExecutor = new c.a[2];
        c.a locala = e.a;
        paramExecutor[0] = locala;
        paramExecutor[1] = localg;
        return Arrays.asList(paramExecutor);
      }
      return Collections.singletonList(localg);
    }
    paramExecutor = new java/lang/AssertionError;
    paramExecutor.<init>();
    throw paramExecutor;
  }
  
  final boolean a(Method paramMethod)
  {
    int i = Build.VERSION.SDK_INT;
    int j = 24;
    if (i < j) {
      return false;
    }
    return paramMethod.isDefault();
  }
  
  public final Executor b()
  {
    o.a.a locala = new e/o$a$a;
    locala.<init>();
    return locala;
  }
  
  final List c()
  {
    int i = Build.VERSION.SDK_INT;
    int j = 24;
    if (i >= j) {
      return Collections.singletonList(m.a);
    }
    return Collections.emptyList();
  }
  
  final int d()
  {
    int i = Build.VERSION.SDK_INT;
    int j = 24;
    if (i >= j) {
      return 1;
    }
    return 0;
  }
}

/* Location:
 * Qualified Name:     e.o.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */