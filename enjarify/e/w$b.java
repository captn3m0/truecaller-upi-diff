package e;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;

final class w$b
  implements ParameterizedType
{
  private final Type a;
  private final Type b;
  private final Type[] c;
  
  w$b(Type paramType1, Type paramType2, Type... paramVarArgs)
  {
    boolean bool1 = paramType2 instanceof Class;
    int j = 0;
    Type localType;
    Object localObject;
    if (bool1)
    {
      bool1 = true;
      boolean bool2;
      if (paramType1 == null)
      {
        bool2 = true;
      }
      else
      {
        bool2 = false;
        localType = null;
      }
      localObject = paramType2;
      localObject = ((Class)paramType2).getEnclosingClass();
      if (localObject != null) {
        bool1 = false;
      }
      if (bool2 != bool1)
      {
        paramType1 = new java/lang/IllegalArgumentException;
        paramType1.<init>();
        throw paramType1;
      }
    }
    int i = paramVarArgs.length;
    while (j < i)
    {
      localType = paramVarArgs[j];
      localObject = "typeArgument == null";
      w.a(localType, (String)localObject);
      w.c(localType);
      j += 1;
    }
    a = paramType1;
    b = paramType2;
    paramType1 = (Type[])paramVarArgs.clone();
    c = paramType1;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = paramObject instanceof ParameterizedType;
    if (bool1)
    {
      paramObject = (ParameterizedType)paramObject;
      boolean bool2 = w.a(this, (Type)paramObject);
      if (bool2) {
        return true;
      }
    }
    return false;
  }
  
  public final Type[] getActualTypeArguments()
  {
    return (Type[])c.clone();
  }
  
  public final Type getOwnerType()
  {
    return a;
  }
  
  public final Type getRawType()
  {
    return b;
  }
  
  public final int hashCode()
  {
    Type[] arrayOfType = c;
    int i = Arrays.hashCode(arrayOfType);
    int j = b.hashCode();
    i ^= j;
    Type localType = a;
    if (localType != null)
    {
      j = localType.hashCode();
    }
    else
    {
      j = 0;
      localType = null;
    }
    return i ^ j;
  }
  
  public final String toString()
  {
    Object localObject = c;
    int i = localObject.length;
    if (i == 0) {
      return w.b(b);
    }
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    int j = localObject.length;
    int k = 1;
    j = (j + k) * 30;
    localStringBuilder.<init>(j);
    localObject = w.b(b);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append("<");
    localObject = w.b(c[0]);
    localStringBuilder.append((String)localObject);
    for (;;)
    {
      localObject = c;
      j = localObject.length;
      if (k >= j) {
        break;
      }
      localStringBuilder.append(", ");
      localObject = w.b(c[k]);
      localStringBuilder.append((String)localObject);
      k += 1;
    }
    localStringBuilder.append(">");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     e.w.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */