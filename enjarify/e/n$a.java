package e;

import java.io.IOException;
import java.lang.reflect.Method;
import okhttp3.ac;

final class n$a
  extends n
{
  private final Method a;
  private final int b;
  private final f c;
  
  n$a(Method paramMethod, int paramInt, f paramf)
  {
    a = paramMethod;
    b = paramInt;
    c = paramf;
  }
  
  final void a(p paramp, Object paramObject)
  {
    Object[] arrayOfObject = null;
    if (paramObject != null) {
      try
      {
        localObject = c;
        localObject = ((f)localObject).a(paramObject);
        localObject = (ac)localObject;
        f = ((ac)localObject);
        return;
      }
      catch (IOException paramp)
      {
        Object localObject = a;
        int i = b;
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>("Unable to convert ");
        localStringBuilder.append(paramObject);
        localStringBuilder.append(" to RequestBody");
        paramObject = localStringBuilder.toString();
        arrayOfObject = new Object[0];
        throw w.a((Method)localObject, paramp, i, (String)paramObject, arrayOfObject);
      }
    }
    paramp = a;
    int j = b;
    arrayOfObject = new Object[0];
    throw w.a(paramp, j, "Body parameter value must not be null.", arrayOfObject);
  }
}

/* Location:
 * Qualified Name:     e.n.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */