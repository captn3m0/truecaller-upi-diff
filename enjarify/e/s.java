package e;

import e.b.e;
import e.b.g;
import e.b.h;
import e.b.k;
import e.b.l;
import e.b.m;
import e.b.p;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Proxy;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import okhttp3.ad;
import okhttp3.e.a;
import okhttp3.u;

public final class s
{
  final e.a a;
  final u b;
  final List c;
  final List d;
  final Executor e;
  final boolean f;
  private final Map g;
  
  s(e.a parama, u paramu, List paramList1, List paramList2, Executor paramExecutor, boolean paramBoolean)
  {
    ConcurrentHashMap localConcurrentHashMap = new java/util/concurrent/ConcurrentHashMap;
    localConcurrentHashMap.<init>();
    g = localConcurrentHashMap;
    a = parama;
    b = paramu;
    c = paramList1;
    d = paramList2;
    e = paramExecutor;
    f = paramBoolean;
  }
  
  private f b(Type paramType, Annotation[] paramArrayOfAnnotation1, Annotation[] paramArrayOfAnnotation2)
  {
    w.a(paramType, "type == null");
    String str = "parameterAnnotations == null";
    w.a(paramArrayOfAnnotation1, str);
    w.a(paramArrayOfAnnotation2, "methodAnnotations == null");
    paramArrayOfAnnotation1 = c;
    int i = paramArrayOfAnnotation1.indexOf(null) + 1;
    paramArrayOfAnnotation2 = c;
    int j = paramArrayOfAnnotation2.size();
    int k = i;
    while (k < j)
    {
      f localf = ((f.a)c.get(k)).a(paramType);
      if (localf != null) {
        return localf;
      }
      k += 1;
    }
    paramArrayOfAnnotation2 = new java/lang/StringBuilder;
    str = "Could not locate RequestBody converter for ";
    paramArrayOfAnnotation2.<init>(str);
    paramArrayOfAnnotation2.append(paramType);
    paramArrayOfAnnotation2.append(".\n");
    paramArrayOfAnnotation2.append("  Tried:");
    paramType = c;
    int m = paramType.size();
    while (i < m)
    {
      paramArrayOfAnnotation2.append("\n   * ");
      str = ((f.a)c.get(i)).getClass().getName();
      paramArrayOfAnnotation2.append(str);
      i += 1;
    }
    paramType = new java/lang/IllegalArgumentException;
    paramArrayOfAnnotation1 = paramArrayOfAnnotation2.toString();
    paramType.<init>(paramArrayOfAnnotation1);
    throw paramType;
  }
  
  private void b(Class paramClass)
  {
    o localo = o.a();
    paramClass = paramClass.getDeclaredMethods();
    int i = paramClass.length;
    int j = 0;
    while (j < i)
    {
      Method localMethod = paramClass[j];
      boolean bool = localo.a(localMethod);
      if (!bool)
      {
        bool = Modifier.isStatic(localMethod.getModifiers());
        if (!bool) {
          a(localMethod);
        }
      }
      j += 1;
    }
  }
  
  public final f a(Type paramType, Annotation[] paramArrayOfAnnotation)
  {
    w.a(paramType, "type == null");
    w.a(paramArrayOfAnnotation, "annotations == null");
    List localList = c;
    int i = localList.indexOf(null) + 1;
    Object localObject = c;
    int j = ((List)localObject).size();
    int k = i;
    while (k < j)
    {
      f localf = ((f.a)c.get(k)).a(paramType, paramArrayOfAnnotation, this);
      if (localf != null) {
        return localf;
      }
      k += 1;
    }
    paramArrayOfAnnotation = new java/lang/StringBuilder;
    localObject = "Could not locate ResponseBody converter for ";
    paramArrayOfAnnotation.<init>((String)localObject);
    paramArrayOfAnnotation.append(paramType);
    paramArrayOfAnnotation.append(".\n");
    paramArrayOfAnnotation.append("  Tried:");
    paramType = c;
    int m = paramType.size();
    while (i < m)
    {
      paramArrayOfAnnotation.append("\n   * ");
      localObject = ((f.a)c.get(i)).getClass().getName();
      paramArrayOfAnnotation.append((String)localObject);
      i += 1;
    }
    paramType = new java/lang/IllegalArgumentException;
    paramArrayOfAnnotation = paramArrayOfAnnotation.toString();
    paramType.<init>(paramArrayOfAnnotation);
    throw paramType;
  }
  
  public final f a(Type paramType, Annotation[] paramArrayOfAnnotation1, Annotation[] paramArrayOfAnnotation2)
  {
    return b(paramType, paramArrayOfAnnotation1, paramArrayOfAnnotation2);
  }
  
  final t a(Method paramMethod)
  {
    ??? = (t)g.get(paramMethod);
    if (??? != null) {
      return (t)???;
    }
    synchronized (g)
    {
      Object localObject2 = g;
      localObject2 = ((Map)localObject2).get(paramMethod);
      localObject2 = (t)localObject2;
      if (localObject2 == null)
      {
        localObject2 = new e/q$a;
        ((q.a)localObject2).<init>(this, paramMethod);
        Object localObject3 = c;
        int i = localObject3.length;
        f localf = null;
        int j = 0;
        Object localObject4 = null;
        int k;
        Object localObject5;
        Object localObject6;
        Object localObject7;
        for (;;)
        {
          k = 1;
          if (j >= i) {
            break;
          }
          localObject5 = localObject3[j];
          boolean bool3 = localObject5 instanceof e.b.b;
          if (bool3)
          {
            localObject6 = "DELETE";
            localObject5 = (e.b.b)localObject5;
            localObject5 = ((e.b.b)localObject5).a();
            ((q.a)localObject2).a((String)localObject6, (String)localObject5, false);
          }
          else
          {
            bool3 = localObject5 instanceof e.b.f;
            if (bool3)
            {
              localObject6 = "GET";
              localObject5 = (e.b.f)localObject5;
              localObject5 = ((e.b.f)localObject5).a();
              ((q.a)localObject2).a((String)localObject6, (String)localObject5, false);
            }
            else
            {
              bool3 = localObject5 instanceof g;
              if (bool3)
              {
                localObject6 = "HEAD";
                localObject5 = (g)localObject5;
                localObject5 = ((g)localObject5).a();
                ((q.a)localObject2).a((String)localObject6, (String)localObject5, false);
              }
              else
              {
                bool3 = localObject5 instanceof e.b.n;
                if (bool3)
                {
                  localObject7 = "PATCH";
                  localObject5 = (e.b.n)localObject5;
                  localObject5 = ((e.b.n)localObject5).a();
                  ((q.a)localObject2).a((String)localObject7, (String)localObject5, k);
                }
                else
                {
                  bool3 = localObject5 instanceof e.b.o;
                  if (bool3)
                  {
                    localObject7 = "POST";
                    localObject5 = (e.b.o)localObject5;
                    localObject5 = ((e.b.o)localObject5).a();
                    ((q.a)localObject2).a((String)localObject7, (String)localObject5, k);
                  }
                  else
                  {
                    bool3 = localObject5 instanceof p;
                    if (bool3)
                    {
                      localObject7 = "PUT";
                      localObject5 = (p)localObject5;
                      localObject5 = ((p)localObject5).a();
                      ((q.a)localObject2).a((String)localObject7, (String)localObject5, k);
                    }
                    else
                    {
                      bool3 = localObject5 instanceof m;
                      if (bool3)
                      {
                        localObject6 = "OPTIONS";
                        localObject5 = (m)localObject5;
                        localObject5 = ((m)localObject5).a();
                        ((q.a)localObject2).a((String)localObject6, (String)localObject5, false);
                      }
                      else
                      {
                        bool3 = localObject5 instanceof h;
                        if (bool3)
                        {
                          localObject5 = (h)localObject5;
                          localObject6 = ((h)localObject5).a();
                          localObject7 = ((h)localObject5).b();
                          boolean bool4 = ((h)localObject5).c();
                          ((q.a)localObject2).a((String)localObject6, (String)localObject7, bool4);
                        }
                        else
                        {
                          bool3 = localObject5 instanceof k;
                          if (bool3)
                          {
                            localObject5 = (k)localObject5;
                            localObject6 = ((k)localObject5).a();
                            int m = localObject6.length;
                            if (m != 0)
                            {
                              localObject6 = ((q.a)localObject2).a((String[])localObject6);
                              s = ((okhttp3.t)localObject6);
                            }
                            else
                            {
                              paramMethod = b;
                              localObject2 = "@Headers annotation is empty.";
                              localObject3 = new Object[0];
                              paramMethod = w.a(paramMethod, (String)localObject2, (Object[])localObject3);
                              throw paramMethod;
                            }
                          }
                          else
                          {
                            bool3 = localObject5 instanceof l;
                            boolean bool5;
                            if (bool3)
                            {
                              bool5 = p;
                              if (!bool5)
                              {
                                q = k;
                              }
                              else
                              {
                                paramMethod = b;
                                localObject2 = "Only one encoding annotation is allowed.";
                                localObject3 = new Object[0];
                                paramMethod = w.a(paramMethod, (String)localObject2, (Object[])localObject3);
                                throw paramMethod;
                              }
                            }
                            else
                            {
                              bool5 = localObject5 instanceof e;
                              if (bool5)
                              {
                                bool5 = q;
                                if (!bool5)
                                {
                                  p = k;
                                }
                                else
                                {
                                  paramMethod = b;
                                  localObject2 = "Only one encoding annotation is allowed.";
                                  localObject3 = new Object[0];
                                  paramMethod = w.a(paramMethod, (String)localObject2, (Object[])localObject3);
                                  throw paramMethod;
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
          j += 1;
        }
        localObject3 = n;
        if (localObject3 != null)
        {
          boolean bool7 = o;
          if (!bool7)
          {
            bool7 = q;
            if (!bool7)
            {
              bool7 = p;
              if (bool7)
              {
                paramMethod = b;
                localObject2 = "FormUrlEncoded can only be specified on HTTP methods with request body (e.g., @POST).";
                localObject3 = new Object[0];
                paramMethod = w.a(paramMethod, (String)localObject2, (Object[])localObject3);
                throw paramMethod;
              }
            }
            else
            {
              paramMethod = b;
              localObject2 = "Multipart can only be specified on HTTP methods with request body (e.g., @POST).";
              localObject3 = new Object[0];
              paramMethod = w.a(paramMethod, (String)localObject2, (Object[])localObject3);
              throw paramMethod;
            }
          }
          localObject3 = d;
          int i1 = localObject3.length;
          Object localObject8 = new n[i1];
          v = ((n[])localObject8);
          i = i1 + -1;
          j = 0;
          localObject4 = null;
          Object localObject9;
          Class localClass;
          while (j < i1)
          {
            localObject5 = v;
            localObject7 = e;
            localObject7 = localObject7[j];
            localObject9 = d;
            localObject9 = localObject9[j];
            boolean bool9;
            if (j == i)
            {
              bool9 = true;
            }
            else
            {
              bool9 = false;
              localClass = null;
            }
            localObject7 = ((q.a)localObject2).a(j, (Type)localObject7, (Annotation[])localObject9, bool9);
            localObject5[j] = localObject7;
            j += 1;
          }
          localObject3 = r;
          if (localObject3 == null)
          {
            bool8 = m;
            if (!bool8)
            {
              paramMethod = b;
              localObject3 = "Missing either @%s URL or @Url parameter.";
              localObject8 = new Object[k];
              localObject2 = n;
              localObject8[0] = localObject2;
              paramMethod = w.a(paramMethod, (String)localObject3, (Object[])localObject8);
              throw paramMethod;
            }
          }
          boolean bool8 = p;
          if (!bool8)
          {
            bool8 = q;
            if (!bool8)
            {
              bool8 = o;
              if (!bool8)
              {
                bool8 = h;
                if (bool8)
                {
                  paramMethod = b;
                  localObject2 = "Non-body HTTP method cannot contain @Body.";
                  localObject3 = new Object[0];
                  paramMethod = w.a(paramMethod, (String)localObject2, (Object[])localObject3);
                  throw paramMethod;
                }
              }
            }
          }
          bool8 = p;
          if (bool8)
          {
            bool8 = f;
            if (!bool8)
            {
              paramMethod = b;
              localObject2 = "Form-encoded method must contain at least one @Field.";
              localObject3 = new Object[0];
              paramMethod = w.a(paramMethod, (String)localObject2, (Object[])localObject3);
              throw paramMethod;
            }
          }
          bool8 = q;
          if (bool8)
          {
            bool8 = g;
            if (!bool8)
            {
              paramMethod = b;
              localObject2 = "Multipart method must contain at least one @Part.";
              localObject3 = new Object[0];
              paramMethod = w.a(paramMethod, (String)localObject2, (Object[])localObject3);
              throw paramMethod;
            }
          }
          localObject3 = new e/q;
          ((q)localObject3).<init>((q.a)localObject2);
          localObject2 = paramMethod.getGenericReturnType();
          boolean bool1 = w.d((Type)localObject2);
          label1269:
          boolean bool2;
          if (!bool1)
          {
            localObject8 = Void.TYPE;
            if (localObject2 != localObject8)
            {
              boolean bool10 = b;
              localObject8 = paramMethod.getAnnotations();
              boolean bool6;
              if (bool10)
              {
                localObject4 = paramMethod.getGenericParameterTypes();
                int n = localObject4.length - k;
                localObject4 = localObject4[n];
                localObject4 = (ParameterizedType)localObject4;
                localObject4 = w.a((ParameterizedType)localObject4);
                localObject5 = w.a((Type)localObject4);
                localObject7 = r.class;
                if (localObject5 == localObject7)
                {
                  bool6 = localObject4 instanceof ParameterizedType;
                  if (bool6)
                  {
                    localObject4 = (ParameterizedType)localObject4;
                    localObject4 = w.a(0, (ParameterizedType)localObject4);
                    bool6 = true;
                    break label1269;
                  }
                }
                bool6 = false;
                localObject5 = null;
                localObject7 = new e/w$b;
                localObject9 = null;
                localClass = b.class;
                localObject6 = new Type[k];
                localObject6[0] = localObject4;
                ((w.b)localObject7).<init>(null, localClass, (Type[])localObject6);
                localObject8 = v.a((Annotation[])localObject8);
              }
              else
              {
                localObject7 = paramMethod.getGenericReturnType();
                bool6 = false;
                localObject5 = null;
              }
              localObject8 = i.a(this, paramMethod, (Type)localObject7, (Annotation[])localObject8);
              localObject4 = ((c)localObject8).a();
              localObject6 = ad.class;
              if (localObject4 != localObject6)
              {
                localObject6 = r.class;
                if (localObject4 != localObject6)
                {
                  localObject6 = a;
                  localObject7 = "HEAD";
                  bool2 = ((String)localObject6).equals(localObject7);
                  if (bool2)
                  {
                    localObject6 = Void.class;
                    bool2 = localObject6.equals(localObject4);
                    if (!bool2)
                    {
                      localObject2 = "HEAD method must use Void as response type.";
                      localObject3 = new Object[0];
                      paramMethod = w.a(paramMethod, (String)localObject2, (Object[])localObject3);
                      throw paramMethod;
                    }
                  }
                  localf = i.a(this, paramMethod, (Type)localObject4);
                  localObject4 = a;
                  if (!bool10)
                  {
                    localObject2 = new e/i$a;
                    ((i.a)localObject2).<init>((q)localObject3, (e.a)localObject4, localf, (c)localObject8);
                  }
                  else if (bool6)
                  {
                    localObject2 = new e/i$c;
                    ((i.c)localObject2).<init>((q)localObject3, (e.a)localObject4, localf, (c)localObject8);
                  }
                  else
                  {
                    localObject2 = new e/i$b;
                    ((i.b)localObject2).<init>((q)localObject3, (e.a)localObject4, localf, (c)localObject8);
                  }
                  localObject3 = g;
                  ((Map)localObject3).put(paramMethod, localObject2);
                }
                else
                {
                  localObject2 = "Response must include generic type (e.g., Response<String>)";
                  localObject3 = new Object[0];
                  paramMethod = w.a(paramMethod, (String)localObject2, (Object[])localObject3);
                  throw paramMethod;
                }
              }
              else
              {
                localObject2 = new java/lang/StringBuilder;
                localObject3 = "'";
                ((StringBuilder)localObject2).<init>((String)localObject3);
                localObject3 = w.a((Type)localObject4);
                localObject3 = ((Class)localObject3).getName();
                ((StringBuilder)localObject2).append((String)localObject3);
                localObject3 = "' is not a valid response body type. Did you mean ResponseBody?";
                ((StringBuilder)localObject2).append((String)localObject3);
                localObject2 = ((StringBuilder)localObject2).toString();
                localObject3 = new Object[0];
                paramMethod = w.a(paramMethod, (String)localObject2, (Object[])localObject3);
                throw paramMethod;
              }
            }
            else
            {
              localObject2 = "Service methods cannot return void.";
              localObject3 = new Object[0];
              paramMethod = w.a(paramMethod, (String)localObject2, (Object[])localObject3);
              throw paramMethod;
            }
          }
          else
          {
            localObject3 = "Method return type must not include a type variable or wildcard: %s";
            localObject8 = new Object[bool2];
            localObject8[0] = localObject2;
            paramMethod = w.a(paramMethod, (String)localObject3, (Object[])localObject8);
            throw paramMethod;
          }
        }
        else
        {
          paramMethod = b;
          localObject2 = "HTTP method annotation is required (e.g., @GET, @POST, etc.).";
          localObject3 = new Object[0];
          paramMethod = w.a(paramMethod, (String)localObject2, (Object[])localObject3);
          throw paramMethod;
        }
      }
      return (t)localObject2;
    }
  }
  
  public final Object a(Class paramClass)
  {
    w.a(paramClass);
    boolean bool = f;
    if (bool) {
      b(paramClass);
    }
    ClassLoader localClassLoader = paramClass.getClassLoader();
    Class[] arrayOfClass = new Class[1];
    arrayOfClass[0] = paramClass;
    s.1 local1 = new e/s$1;
    local1.<init>(this, paramClass);
    return Proxy.newProxyInstance(localClassLoader, arrayOfClass, local1);
  }
  
  public final f b(Type paramType, Annotation[] paramArrayOfAnnotation)
  {
    Object localObject = "type == null";
    w.a(paramType, (String)localObject);
    w.a(paramArrayOfAnnotation, "annotations == null");
    paramType = c;
    int i = paramType.size();
    int j = 0;
    paramArrayOfAnnotation = null;
    while (j < i)
    {
      localObject = c;
      ((List)localObject).get(j);
      j += 1;
    }
    return a.d.a;
  }
}

/* Location:
 * Qualified Name:     e.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */