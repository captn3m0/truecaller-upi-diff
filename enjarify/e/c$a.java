package e;

import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public abstract class c$a
{
  public static Class a(Type paramType)
  {
    return w.a(paramType);
  }
  
  public static Type a(ParameterizedType paramParameterizedType)
  {
    return w.a(0, paramParameterizedType);
  }
  
  public abstract c a(Type paramType, Annotation[] paramArrayOfAnnotation, s params);
}

/* Location:
 * Qualified Name:     e.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */