package e;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

final class s$1
  implements InvocationHandler
{
  private final o c;
  private final Object[] d;
  
  s$1(s params, Class paramClass)
  {
    params = o.a();
    c = params;
    params = new Object[0];
    d = params;
  }
  
  public final Object invoke(Object paramObject, Method paramMethod, Object[] paramArrayOfObject)
  {
    Object localObject = paramMethod.getDeclaringClass();
    Class localClass = Object.class;
    if (localObject == localClass) {
      return paramMethod.invoke(this, paramArrayOfObject);
    }
    localObject = c;
    boolean bool = ((o)localObject).a(paramMethod);
    if (bool)
    {
      localObject = c;
      localClass = a;
      return ((o)localObject).a(paramMethod, localClass, paramObject, paramArrayOfObject);
    }
    paramObject = b.a(paramMethod);
    if (paramArrayOfObject == null) {
      paramArrayOfObject = d;
    }
    return ((t)paramObject).a(paramArrayOfObject);
  }
}

/* Location:
 * Qualified Name:     e.s.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */