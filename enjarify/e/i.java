package e;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.List;
import okhttp3.e.a;

abstract class i
  extends t
{
  private final q a;
  private final e.a b;
  private final f c;
  
  i(q paramq, e.a parama, f paramf)
  {
    a = paramq;
    b = parama;
    c = paramf;
  }
  
  static c a(s params, Method paramMethod, Type paramType, Annotation[] paramArrayOfAnnotation)
  {
    int i = 1;
    Object localObject1 = "returnType == null";
    try
    {
      w.a(paramType, (String)localObject1);
      localObject1 = "annotations == null";
      w.a(paramArrayOfAnnotation, (String)localObject1);
      localObject1 = d;
      int j = 0;
      Object localObject2 = null;
      int k = ((List)localObject1).indexOf(null) + i;
      localObject2 = d;
      j = ((List)localObject2).size();
      int m = k;
      while (m < j)
      {
        Object localObject3 = d;
        localObject3 = ((List)localObject3).get(m);
        localObject3 = (c.a)localObject3;
        localObject3 = ((c.a)localObject3).a(paramType, paramArrayOfAnnotation, params);
        if (localObject3 != null) {
          return (c)localObject3;
        }
        m += 1;
      }
      paramArrayOfAnnotation = new java/lang/StringBuilder;
      localObject2 = "Could not locate call adapter for ";
      paramArrayOfAnnotation.<init>((String)localObject2);
      paramArrayOfAnnotation.append(paramType);
      localObject2 = ".\n";
      paramArrayOfAnnotation.append((String)localObject2);
      localObject2 = "  Tried:";
      paramArrayOfAnnotation.append((String)localObject2);
      localObject2 = d;
      j = ((List)localObject2).size();
      while (k < j)
      {
        Object localObject4 = "\n   * ";
        paramArrayOfAnnotation.append((String)localObject4);
        localObject4 = d;
        localObject4 = ((List)localObject4).get(k);
        localObject4 = (c.a)localObject4;
        localObject4 = localObject4.getClass();
        localObject4 = ((Class)localObject4).getName();
        paramArrayOfAnnotation.append((String)localObject4);
        k += 1;
      }
      params = new java/lang/IllegalArgumentException;
      paramArrayOfAnnotation = paramArrayOfAnnotation.toString();
      params.<init>(paramArrayOfAnnotation);
      throw params;
    }
    catch (RuntimeException params)
    {
      paramArrayOfAnnotation = new Object[i];
      paramArrayOfAnnotation[0] = paramType;
      throw w.a(paramMethod, params, "Unable to create call adapter for %s", paramArrayOfAnnotation);
    }
  }
  
  static f a(s params, Method paramMethod, Type paramType)
  {
    Object localObject = paramMethod.getAnnotations();
    try
    {
      return params.a(paramType, (Annotation[])localObject);
    }
    catch (RuntimeException params)
    {
      localObject = new Object[1];
      localObject[0] = paramType;
      throw w.a(paramMethod, params, "Unable to create converter for %s", (Object[])localObject);
    }
  }
  
  protected abstract Object a(b paramb, Object[] paramArrayOfObject);
  
  final Object a(Object[] paramArrayOfObject)
  {
    l locall = new e/l;
    q localq = a;
    e.a locala = b;
    f localf = c;
    locall.<init>(localq, paramArrayOfObject, locala, localf);
    return a(locall, paramArrayOfObject);
  }
}

/* Location:
 * Qualified Name:     e.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */